/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 01:10:00 PM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpeDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOAutorizarOperacionesImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOAutorizarOperacionesImpl extends DAOAutorizarOperacionesImpPrt2 implements DAOAutorizarOperaciones {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 3973656069183905840L;

	/** The Constant QUERY_INICIO_BUSQUEDA_PAGINADA. */
	private static final String QUERY_INICIO_BUSQUEDA_PAGINADA ="select ID_FOLIO, IMPORTE_ABONO, ID_TEMPLATE, CVE_USUARIO_CAP, USR_AUTORIZA, ESTATUS, FCH_CAPTURA, USR_APARTA, REFE_TRANSFER, OPERACION_APARTADA, COD_ERROR, MSG_ERROR, RN from (select TMP.ID_FOLIO, TMP.IMPORTE_ABONO, TMP.ID_TEMPLATE, TMP.CVE_USUARIO_CAP, TMP.USR_AUTORIZA, TMP.ESTATUS, TMP.FCH_CAPTURA, TMP.USR_APARTA, TMP.REFE_TRANSFER, TMP.OPERACION_APARTADA, TMP.COD_ERROR, TMP.MSG_ERROR, rownum RN from(";

	/** The Constant QUERY_FIN_BUSQUEDA_PAGINADA. */
	private static final String QUERY_FIN_BUSQUEDA_PAGINADA =") TMP ) where RN between ? and ?";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_OPE_PEND_AUT. */
	private static final String QUERY_OPE_PEND_AUT = "select tmpl.USR_AUTORIZA USR_AUT_OPE, oper.ID_FOLIO, oper.IMPORTE_ABONO, oper.ID_TEMPLATE, oper.CVE_USUARIO_CAP, oper.USR_AUTORIZA, oper.ESTATUS, oper.FCH_CAPTURA, oper.USR_APARTA,oper.REFE_TRANSFER, oper.OPERACION_APARTADA, oper.COD_ERROR, oper.MSG_ERROR, oper.IMPORTE_CARGO, oper.NUM_CUENTA_ORD, oper.NUM_CUENTA_REC from TRAN_CAP_OPER oper join TRAN_CAP_USR_TEMPLATE tmpl on oper.id_template=tmpl.id_template where trim(tmpl.USR_AUTORIZA) = ? and oper.ESTATUS = 'PA'";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_OPE_PEND_AUT_ORD_FECH. */
	private static final String QUERY_OPE_PEND_AUT_ORD_FECH = " order by ID_FOLIO desc";
	
	/** The Constant QUERY_COUNT_OPE_PEND_AUT. */
	private static final String QUERY_COUNT_OPE_PEND_AUT = "select count(1) TOTAL  from TRAN_CAP_OPER oper join TRAN_CAP_USR_TEMPLATE tmpl on oper.id_template = tmpl.id_template where trim(tmpl.USR_AUTORIZA) = ? and oper.ESTATUS = 'PA'";
	
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_APARTAR_OPERACION = "update TRAN_CAP_OPER set OPERACION_APARTADA = 2, USR_APARTA = ? WHERE TRIM(ID_FOLIO) = ?";
	
	/** The Constant QUERY_DESAPARTAR_OPERACION. */
	private static final String QUERY_DESAPARTAR_OPERACION = "update TRAN_CAP_OPER oper set oper.OPERACION_APARTADA = 0, oper.USR_APARTA = NULL WHERE TRIM(oper.USR_APARTA) = ? AND TRIM(oper.ID_FOLIO) = ?";

	/** The Constant QUERY_INI_CONSUL_OPER_APART. */
	private static final String QUERY_CONSUL_TODO_OPER_APART = "select tmpl.USR_AUTORIZA USR_AUT_OPE, oper.ID_FOLIO, oper.IMPORTE_ABONO, oper.ID_TEMPLATE, oper.CVE_USUARIO_CAP, oper.USR_AUTORIZA, oper.ESTATUS, oper.FCH_CAPTURA, oper.USR_APARTA,oper.REFE_TRANSFER, oper.OPERACION_APARTADA, oper.COD_ERROR, oper.MSG_ERROR, oper.IMPORTE_CARGO, oper.NUM_CUENTA_ORD, oper.NUM_CUENTA_REC from TRAN_CAP_OPER oper join TRAN_CAP_USR_TEMPLATE tmpl on oper.id_template=tmpl.id_template WHERE TRIM(tmpl.USR_AUTORIZA) = ? AND TRIM(oper.OPERACION_APARTADA) = 2 AND TRIM(oper.USR_APARTA) = ?";
	
	/** The Constant QUERY_INI_CONSUL_OPER_APART. */
	private static final String QUERY_INI_CONSUL_OPER_APART = "select ID_FOLIO, IMPORTE_ABONO, ID_TEMPLATE, CVE_USUARIO_CAP, USR_AUTORIZA, ESTATUS, FCH_CAPTURA, USR_APARTA, REFE_TRANSFER from ( ";

	/** The Constant QUERY_FIN_CONSUL_OPER_APART. */
	private static final String QUERY_FIN_CONSUL_OPER_APART = " ) where TRIM(OPERACION_APARTADA) = 2 AND TRIM(USR_APARTA) = ?";

	/** The Constant QUERY_AUTORIZAR_OPERACIONES. */
	private static final String QUERY_AUTORIZAR_OPERACIONES = "update TRAN_CAP_OPER set ESTATUS = 'AU', USR_AUTORIZA = ? WHERE TRIM(ID_FOLIO) = ?";
	
	/** The Constant QUERY_REPARAR_OPERACIONES. */
	private static final String QUERY_REPARAR_OPERACIONES = "update TRAN_CAP_OPER set ESTATUS = 'RE', USR_AUTORIZA = ? WHERE TRIM(ID_FOLIO) = ?";
	
	/** The Constant QUERY_CANCELAR_OPERACIONES. */
	private static final String QUERY_CANCELAR_OPERACIONES = "update TRAN_CAP_OPER set ESTATUS = 'CA', USR_AUTORIZA = ? WHERE TRIM(ID_FOLIO) = ?";

	/** Propiedad del tipo String que almacena el valor de QUERY_ADMIN_SUPERVISOR. */
	private static final String QUERY_ADMIN_SUPERVISOR ="SELECT COUNT(1) CONT FROM TR_SEG_CAT_PERFIL SCP, TR_SEG_CFG_USUARIO_PERFIL SCUP"
			+" WHERE SCP.TXT_PERFIL_SAM = 'grp_appnal_admin' "
			+"AND SCUP.CVE_PERFIL = SCP.CVE_PERFIL AND TRIM(SCUP.CVE_USUARIO) = ?";
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#consultarOperacionesPendientes(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	@Override
	public BeanResAutorizarOpeDAO consultarOperacionesPendientes(ArchitechSessionBean sessionBean,
			BeanReqAutorizarOpe req) {
		final BeanResAutorizarOpeDAO beanResponseDAO = new BeanResAutorizarOpeDAO();
	    Utilerias utilerias = Utilerias.getUtilerias();
	    List<HashMap<String,Object>> listResul = new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;
    	String queryPendientes = "";
    	String queryCount = "";
    	String filtrosQuery = "";
    	
    	try{
    		List<Object> parametros = new ArrayList<Object>();

    		parametros.add(sessionBean.getUsuario());
	    	filtrosQuery = armarParametrizacionOpePend(parametros,req);
	    	//armar query
	    	queryCount = QUERY_COUNT_OPE_PEND_AUT + filtrosQuery;

	    	//consultar total de operaciones
	    	responseDTO = HelperDAO.consultar(queryCount, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

	    	if(responseDTO.getResultQuery()!= null){
	    		listResul = responseDTO.getResultQuery();
	    		for(HashMap<String,Object> map:listResul){
	    			//obtnener total
	    			beanResponseDAO.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
	    		}
	    	}
	    	//si total es mayor a 0
	    	if(beanResponseDAO.getTotalReg() > 0){
	    		//obtner registro de inicio y fin de pagina
	    		parametros.add(req.getPaginador().getRegIni());
	    		parametros.add(req.getPaginador().getRegFin());

		    	//armar query
	    		queryPendientes = QUERY_INICIO_BUSQUEDA_PAGINADA + QUERY_OPE_PEND_AUT + filtrosQuery + QUERY_OPE_PEND_AUT_ORD_FECH + QUERY_FIN_BUSQUEDA_PAGINADA;
	    		//Consulta de Operaciones Pendientes de Autorizar con Paginacion
		        responseDTO = HelperDAO.consultar(queryPendientes, parametros,
		        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		        
		        //si hay resultados de la consulta
		        if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
		        	//obtner lista de resultados del query
		        	listResul = responseDTO.getResultQuery();
		        	beanResponseDAO.setListOperacionesPendAutorizar(
		        			//metodo que convierte la lista del query en arrayList
		        			obtnenerListaOperaciones(listResul,sessionBean.getUsuario(),true) );
		        	
					//se obtiene el codigo de error del query
		            beanResponseDAO.setCodError(responseDTO.getCodeError());
		            beanResponseDAO.setMsgError(responseDTO.getMessageError());
		        }
	    	} else {
	    		//se envia codigo de respuesta "query ejecutado exitosamente"
	    		beanResponseDAO.setCodError(Errores.CODE_SUCCESFULLY);
			}
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		//se envia codigo error EC00011B
    		beanResponseDAO.setMsgError(Errores.DESC_EC00011B);
	        beanResponseDAO.setCodError(Errores.EC00011B);
    	}
	    return beanResponseDAO;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#apartarOPeraciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe, java.util.List)
	 */
	@Override
	public BeanResAutorizarOpeDAO apartarOPeraciones(ArchitechSessionBean sessionBean,String folio) {
		BeanResAutorizarOpeDAO responseApartarOpe=new BeanResAutorizarOpeDAO();
  	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		String usuario=sessionBean.getUsuario();

		try{
			parametros.add(usuario);
			parametros.add(folio);
			//se ejecuta query que aparta las operaciones
			responseDTO = HelperDAO.actualizar(QUERY_APARTAR_OPERACION, parametros,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene el codigo de error del query
        	responseApartarOpe.setCodError(responseDTO.getCodeError());
        	responseApartarOpe.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
        	showException(e);
    		//se envia codigo error EC00011B
        	responseApartarOpe.setCodError(Errores.EC00011B);
        	responseApartarOpe.setMsgError(Errores.DESC_EC00011B);
        }
		return responseApartarOpe;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#desapartarOPeraciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	@Override
	public BeanResAutorizarOpeDAO desapartarOPeraciones(ArchitechSessionBean sessionBean, String folio){
		BeanResAutorizarOpeDAO responseDesapartarOpe=new BeanResAutorizarOpeDAO();
  	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		String usuario=sessionBean.getUsuario();

  	  	try{
			parametros.add(usuario);
			parametros.add(folio);
	    	
	    	//ejecutar query para desapartar operacion
	    	responseDTO = HelperDAO.actualizar(QUERY_DESAPARTAR_OPERACION, parametros,
		        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene el codigo de error del query
	    	responseDesapartarOpe.setCodError(responseDTO.getCodeError());
        	responseDesapartarOpe.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
        	showException(e);
    		//se envia codigo error EC00011B
	        responseDesapartarOpe.setCodError(Errores.EC00011B);
	        responseDesapartarOpe.setMsgError(Errores.DESC_EC00011B);
	    }
  	  	return responseDesapartarOpe;
	}

	/**
	 * Autorizar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope dao
	 */
	@Override
	public BeanResAutorizarOpeDAO autorizarOperaciones(ArchitechSessionBean sessionBean, String folio){
		BeanResAutorizarOpeDAO responseAutorizarOpe=new BeanResAutorizarOpeDAO();
  	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		String usuario=sessionBean.getUsuario();

		try{
			//se envian el usuario logado y folio como parametros para el query
			parametros.add(usuario);
			parametros.add(folio);

	    	//ejecutar query para autorizar operacion
			responseDTO = HelperDAO.actualizar(QUERY_AUTORIZAR_OPERACIONES, parametros,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene el codigo de error del query
			responseAutorizarOpe.setCodError(responseDTO.getCodeError());
        	responseAutorizarOpe.setMsgError(responseDTO.getMessageError());
	    } catch (ExceptionDataAccess e) {
	    	showException(e);
    		//se envia codigo error EC00011B
	    	responseAutorizarOpe.setCodError(Errores.EC00011B);
	    	responseAutorizarOpe.setMsgError(Errores.DESC_EC00011B);
	    }
		return responseAutorizarOpe;
	}

	/**
	 * Reparar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope dao
	 */
	@Override
	public BeanResAutorizarOpeDAO repararOperaciones(ArchitechSessionBean sessionBean, String folio){
		BeanResAutorizarOpeDAO responseRepararOpe=new BeanResAutorizarOpeDAO();
  	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		String usuario=sessionBean.getUsuario();

		try{
			//se envian el usuario logado y folio como parametros para el query
			parametros.add(usuario);
			parametros.add(folio);
			
	    	//ejecutar query para reparar operacion
			responseDTO = HelperDAO.actualizar(QUERY_REPARAR_OPERACIONES, parametros,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene el codigo de error del query
			responseRepararOpe.setCodError(responseDTO.getCodeError());
        	responseRepararOpe.setMsgError(responseDTO.getMessageError());
	    } catch (ExceptionDataAccess e) {
	    	showException(e);
    		//se envia codigo error EC00011B
	    	responseRepararOpe.setCodError(Errores.EC00011B);
	    	responseRepararOpe.setMsgError(Errores.DESC_EC00011B);
	    }
		return responseRepararOpe;
	}

	/**
	 * Cancelar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope dao
	 */
	@Override
	public BeanResAutorizarOpeDAO cancelarOperaciones(ArchitechSessionBean sessionBean, String folio) {
		BeanResAutorizarOpeDAO responseCancelarOpe=new BeanResAutorizarOpeDAO();
  	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		String usuario=sessionBean.getUsuario();
		
		try{
			//se envian el usuario logado y folio como parametros para el query
			parametros.add(usuario);
			parametros.add(folio);

	    	//ejecutar query para cancelar operacion
			responseDTO = HelperDAO.actualizar(QUERY_CANCELAR_OPERACIONES, parametros,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene el codigo de error del query
			responseCancelarOpe.setCodError(responseDTO.getCodeError());
        	responseCancelarOpe.setMsgError(responseDTO.getMessageError());
	    } catch (ExceptionDataAccess e) {
	    	showException(e);
    		//se envia codigo error EC00011B
	    	responseCancelarOpe.setCodError(Errores.EC00011B);
	    	responseCancelarOpe.setMsgError(Errores.DESC_EC00011B);
	    }
		return responseCancelarOpe;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#consultaOperacionesApartadas(mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe, java.util.List, java.lang.String)
	 */
	@Override
	public BeanResAutorizarOpeDAO consultaOperacionesApartadas(
				BeanReqAutorizarOpe req, List<Object> params, ArchitechSessionBean sessionBean) {
	    BeanResAutorizarOpeDAO response = new BeanResAutorizarOpeDAO();
	    List<HashMap<String,Object>> listResul = new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = new ResponseMessageDataBaseDTO();
		String usuario = sessionBean.getUsuario();
		String usrAutorizaTemplate = usuario;
		String filtrosQuery = "";
		String query = "";
		try{
			//parametro para consultar las operaciones sobre las que tiene permisos el usr logeado
  	  		params.add(usrAutorizaTemplate);
			//se envia el usuario logado como parametro para el query
  	  		params.add(usuario);
	    	filtrosQuery = armarParametrizacionOpeApart(params,req);
  	  		
	    	//se genera query para consultar operaciones apartadas
	    	query= QUERY_CONSUL_TODO_OPER_APART + filtrosQuery + QUERY_OPE_PEND_AUT_ORD_FECH ;

	    	//se consultan operaciones apartadas
	        responseDTO = HelperDAO.consultar(query, params,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//se obtiene lista de resultados del query
	        listResul = responseDTO.getResultQuery();
			response.setListOperacionesApartadas(
	        		//metodo que convierte lista del query en arrayList
					obtnenerListaOperaciones(listResul, usuario,false));
	        response.setCodError(responseDTO.getCodeError());
  	  	} catch (ExceptionDataAccess e) {
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
	        response.setCodError(Errores.EC00011B);
		}
		return response;
	}
	
	/**
	 * Consulta operaciones apartadas por pagina.
	 *
	 * @param req the req
	 * @param params the params
	 * @param sessionBean the session bean
	 * @return the list
	 */
	@Override
	public BeanResAutorizarOpeDAO consultaOperacionesApartadasPorPagina(
				BeanReqAutorizarOpe req, List<Object> params, ArchitechSessionBean sessionBean) {
	    BeanResAutorizarOpeDAO response = new BeanResAutorizarOpeDAO();
	    List<HashMap<String,Object>> listResul = new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;

		//se obtiene usuario logado en la aplicacion
		String usuario = sessionBean.getUsuario();
		String filtrosQuery = "";
		String query = "";
		try{
			//para usuario para validar si usr logado tiene permisos sobre operaciones pendientes
  	  		params.add(usuario);
	    	filtrosQuery = armarParametrizacionOpeApart(params,req);
	    	params.add(req.getPaginador().getRegIni());
	    	params.add(req.getPaginador().getRegFin());

	    	/**
	    	//consulta si el usr logeado es Admin
			if( usrAdmin == 1 ){
				//generar query para consultar operaciones apartadas para usr Admin
		    	query= QUERY_INI_CONSUL_OPER_APART + QUERY_INICIO_BUSQUEDA_PAGINADA + QUERY_OPE_PEND_AUT + 
		    			filtrosQuery + QUERY_OPE_PEND_AUT_ORD_FECH + QUERY_FIN_BUSQUEDA_PAGINADA + QUERY_FIN_CONSUL_OPER_APART_USR_ADMIN;
				//consultar operaciones apartadas para usr Admin
		        responseDTO = HelperDAO.consultar(query, params,
		        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			}else{
			*/
			//se envia el usuario logado como parametro para el query
  	  		params.add(usuario);
			//generar query ´para consultar operaciones apartadas para otro tipo de usr
	    	query= QUERY_INI_CONSUL_OPER_APART + QUERY_INICIO_BUSQUEDA_PAGINADA + QUERY_OPE_PEND_AUT + 
	    			filtrosQuery + QUERY_OPE_PEND_AUT_ORD_FECH + QUERY_FIN_BUSQUEDA_PAGINADA + QUERY_FIN_CONSUL_OPER_APART;
			//consultar operaciones apartadas para otro tipo de usr
	        responseDTO = HelperDAO.consultar(query, params,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//se obtiene lista de resultados del query
	        listResul = responseDTO.getResultQuery();
	        response.setListOperacionesApartadas(
	        		//metodo que convierte lista del query en arrayList
	        		obtnenerListaOperaciones(listResul, usuario,false));
	        response.setCodError(responseDTO.getCodeError());
  	  	} catch (ExceptionDataAccess e) {
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
	        response.setCodError(Errores.EC00011B);
		}
		return response;
	}

	/**
	 * Consultar admin o supervisor.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res base
	 */
	public BeanResAutorizarOpeDAO consultarUsrAdmin(ArchitechSessionBean sessionBean){
		BeanResAutorizarOpeDAO res = new BeanResAutorizarOpeDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		//se obtiene usuario logado en la aplicacion
		String usuario=sessionBean.getUsuario();

		List<Object> parametros = new ArrayList<Object>();
		try{
			//se obtiene el usuario logeado
	    	parametros.add(usuario);
	    	//se consulta si es usuario administrador
			responseDTO = HelperDAO.consultar(QUERY_ADMIN_SUPERVISOR, parametros,
					HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());

			//si hay resultados de busqueda
			if(responseDTO.getResultQuery()!= null){
				res.setCodError(Errores.CODE_SUCCESFULLY);
				list = responseDTO.getResultQuery();
				//metodo que verifica si usr es administrador
				verificarUsrAdmin(res,list,utilerias);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
    		res.setMsgError(Errores.DESC_EC00011B);
	        res.setCodError(Errores.EC00011B);
        }
		return res;
	}
}
