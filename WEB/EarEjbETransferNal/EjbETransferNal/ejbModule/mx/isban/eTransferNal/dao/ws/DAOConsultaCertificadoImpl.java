/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaCertificadoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	By 	      					Company 	Description
 * -------  ----------- --------------------------	---------- 	----------------------
 *   1.0    16/02/2016  Indra FSW   				ISBAN 		Creacion
 * 	 1.1	05/07/2018	Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */

package mx.isban.eTransferNal.dao.ws;

//Imports java
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
//Imports javax
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
//Imports etransferNal
import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificadoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
//Imports metrics
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;
//Imports banxico
import banxico.ies.ccbm.ws.WSCertificadoBMc;

/**
 * Class: DAOConsultaCertificadoImpl
 * @author ING. Juan Manuel Fuentes Ramos. CSA
 * Plan Delta Sucursales. 2018 - 06
 * Implementacion monitoreo metrics
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaCertificadoImpl extends Architech implements DAOConsultaCertificado{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -7900477703957979441L;
	/** Propiedad del tipo ResourceBundle que almacena el valor de res. */
	private static final ResourceBundle RES = ResourceBundle.getBundle("ruta");
	/**Para bandera de error*/
	private boolean error = false;
	/** Para guardar la wsdl*/
	private transient String strUrl = null;

	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** Bitacorizacion de metricas */
	@EJB private BitacorizaMetrics boMetrics;
	/** Utilerias Metricas MetricsSenderInit*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	/** The timeIn*/
	private transient String timeIn = null;
	/** The timeOut*/
	private transient String timeOut = null;
	/** Codigo de error del WS*/
	private transient String codeError = null;
	/** Variable de error para servicio externo*/
	private transient String errWS = "N";
	// END [ADD: JMFR CSA Monitoreo Metrics]

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.isban.eTransferNal.dao.ws.DAOConsultaCertificado#consultaCertificado(mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado)
	 */
	@Override
	public BeanResConsultaCertificadoDAO consultaCertificado(BeanReqConsultaCertificado req){
		//Se crea objeto de respuesta
		BeanResConsultaCertificadoDAO beanResConsultaCertificadoDAO = new BeanResConsultaCertificadoDAO();
		senderInit.initMetrics();//Se inicializan dto's para metricas
		// se obtiene tiempo de inicio de peticion
		timeIn = String.valueOf(System.currentTimeMillis());
		//Se cargan parametros de conexion servicio externo
		Map<String, String> map = load();
		//Se obtiene codigo de error al obtener parametros de entrada
		String strCodError = map.get(Constantes.COD_ERROR);
		//Se valida que el flujo fue exisoto de lo contrario retorna codigo de error
		if(!Errores.OK00000V.equals(strCodError)){
			beanResConsultaCertificadoDAO.setCodError(strCodError);
			error = true;//Se indica que hubo error (Se elimina return)
			codeError = strCodError;//Se asigna codigo de error.
			errWS = "S";//Se asigna estatus de error
			// se obtiene tiempo de respuesta
			timeOut = String.valueOf(System.currentTimeMillis());
			insertaSExterno();// Se ejecuta insert asincrono para peticion servicio externo
		}
		String strPuerto =map.get("PUERTO");//Puerto de conexion
		String strHost =map.get("HOST");//Host destino
		if(!error){//Si no hubo error al obtener parametros de conexion se ejecuta Ws externo
			ejecutaWS(req, beanResConsultaCertificadoDAO, strPuerto, strHost);
		}
		// se retorna respuesta del servicio
        return beanResConsultaCertificadoDAO;
	}

	/**
	 * Metodo para ejecutar peticion a WS externo
	 *
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * Implementacion modulo metrics
	 *
	 * @param req peticion al servicio
	 * @param beanResConsultaCertificadoDAO respuesta del servicio
	 * @param strPuerto puerto del WS externo
	 * @param strHost host destino WS externo
	 */
	private void ejecutaWS(BeanReqConsultaCertificado req,
			BeanResConsultaCertificadoDAO beanResConsultaCertificadoDAO,
			String strPuerto, String strHost) {
		List<String> respuestaList = null;//Objeto para respuesta del servicio externo
		URL url = null;//Objeto para conexion via wsdl
		try{
			//Se arma url servicio externo
			strUrl = "http://"+strHost+":"+strPuerto+"/WSCertificadoBMc/WSCertificadoBMc?wsdl";
			info ("Alcanzo la IP"+strUrl+":numCertificado:["+req.getNumCertificado()+"]");
			//Se crea la conexion al servicio externo
			url = getURL(strUrl);
	        QName qname = new QName("http://ws.ccbm.ies.banxico/", "WSCertificadoBMc");
	        Service service = Service.create(url, qname);
        	WSCertificadoBMc calc = (WSCertificadoBMc)service.getPort(WSCertificadoBMc.class);
        	byte[] arr = null;
        	String certificado = null;
        	Charset charset = Charset.forName("UTF-8");
    		arr = limpiaCadena(req.getNumCertificado(),"").getBytes(charset);
    		//Se arma peticion
    		certificado = limpiaCadena(new String(arr, charset),"");
    		//Se ejecuta peticion a servicio externo
    		respuestaList = calc.consultaCertificado(certificado);
    		//Se valida que la respuesta sea exitosa
    		if(respuestaList!=null){
	    		for(String temp:respuestaList){//Se imprime log con la respuesta
	            	info("NumCertificado:"+certificado +"valores:"+temp);
	            }
	    		//Se guarda informacion en bean de respuesta
	            beanResConsultaCertificadoDAO.setCertResponse(respuestaList);
	            beanResConsultaCertificadoDAO.setCodError(Errores.OK00000V);
	            codeError = "OK0";
    		}else{//En caso de error se asigna codigo de error al bean de respuesta
    			beanResConsultaCertificadoDAO.setCodError(Errores.ED00101V);
    			codeError = "101V";//Se asigna codigo de error
    			errWS = "S";//Se asigna estatus de error
    		}
		}catch (MalformedURLException e) {
			info ("Error en la url: MalformedURLException");
			beanResConsultaCertificadoDAO.setCodError(Errores.ED00099V);
			showException(e);//Se imprime log de error
			insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje()+
					DAOConsultaCertificadoImpl.class.getCanonicalName() + " " + Errores.ED00099V,
					e.getMessage() + " " + Arrays.toString(e.getStackTrace()));//Monitor metrics
			codeError = "99V";//Se asigna codigo de error
			errWS = "S";//Se asigna estatus de error
		}catch (Exception e) {
			info ("Error de IO:");
			beanResConsultaCertificadoDAO.setCodError(Errores.ED00100V);
			showException(e);//Se imprime log de error
			insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje() +
					DAOConsultaCertificadoImpl.class.getCanonicalName() + " " + Errores.ED00100V,
					e.getMessage() + " " + Arrays.toString(e.getStackTrace()));//Monitoreo metrics
			codeError = "100V";//Se asigna codigo de error
			errWS = "S";//Se asigna estatus de error
		} finally{
			// se obtiene tiempo de respuesta
			timeOut = String.valueOf(System.currentTimeMillis());
			insertaSExterno();// Se ejecuta insert asincrono para peticion servicio externo
		}
	}

	/**
	 * Metodo que sirve para obtener las propiedad del archivo de configuracion
	 *
	 * @return Map del tipo Map<String, String> con los valores de las propiedades
	 */
	private Map<String, String> load(){
		Map<String, String> map = new HashMap<String,String>();
		File f = null;
		String ruta = RES.getString("ruta");
		f = new File(ruta);//Se asigna ruta del archivo
		if(f.exists()){//Se valida exista archivo de configuracion
			leeConfiguracion(map, f);
		}else{//En caso de que no localiza archivo de configuracion
			error("No existe el archivo");
			map.put(Constantes.COD_ERROR, Errores.ED00096V);//Se asigna codigo de error
			insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje()+
					DAOConsultaCertificadoImpl.class.getCanonicalName() + " " + Errores.ED00096V,
					"No existe el archivo: " + ruta);//Monitor metrics
		}
		return map;
	}

	/**
	 * Metodo para leer configuracion para ws externo
	 *
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 *
	 * @param map objecto para guardar configuracion
	 * @param f achivo de lectura
	 */
	private void leeConfiguracion(Map<String, String> map, File f) {
		String strPuerto ="";//Para el puerto de conextion
		String strTimeOut ="";//Para el timeout
		String strHost ="";//Host destino
		InputStream fileIn = null;
		Properties properties = new Properties();
		try {//Se comienza lectura de archivo de configuracion
			fileIn = new FileInputStream(f);
			properties.load(fileIn);
			strTimeOut = properties.getProperty("TimeWSCertificadoBMc");
			strHost = properties.getProperty("HostWSCertificadoBMc");
			map.put("HOST", strHost);
			strPuerto = properties.getProperty("PuertoWSCertificadoBMc");
			map.put("PUERTO", strPuerto);
			if(strTimeOut==null  || "".equals(strTimeOut.trim())){
				strTimeOut= "10000";//Se asigna timeout default en caso de no indicarse
			}
			map.put("TIME_OUT", strTimeOut);
			//Se asigna codigo de operacion exitosa
			map.put(Constantes.COD_ERROR,Errores.OK00000V);
		} catch (FileNotFoundException e) {//Error archivo no localizado en la ruta especificada o se encuentra ocupada
			map.put(Constantes.COD_ERROR, Errores.ED00096V);//Se asigna codigo de error
			insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje()+
					DAOConsultaCertificadoImpl.class.getCanonicalName() + " " + Errores.ED00096V,
					e.getMessage() + " " + Arrays.toString(e.getStackTrace()));//Monitor metrics
			showException(e);//Se informa en log la traza del error
		} catch (IOException e) {//Error de lectura en archivo de configuracion
			showException(e);//Se informa en log la traza del error
			map.put(Constantes.COD_ERROR, Errores.ED00097V);//Se asigna codigo de error
			insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje()+
					DAOConsultaCertificadoImpl.class.getCanonicalName() + " " + Errores.ED00097V,
					e.getMessage() + " " + Arrays.toString(e.getStackTrace()));//Monitor metrics
		}finally{//Se cierra archivo
			try {
				if(fileIn!=null){
					fileIn.close();
				}
			} catch (IOException e) {//En case de error por cierre de archivo
				showException(e);//Se informa en log el error
				insertaError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError(),
						EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje()+
						DAOConsultaCertificadoImpl.class.getCanonicalName(),
						e.getMessage() + " " +Arrays.toString(e.getStackTrace()));//Monitor metrics
			}
		}
	}

	/**
	 * @param strUrl String con la url
	 * @return URL Objeto que abstrae una URL
	 * @throws MalformedURLException Exception de url invalido
	 */
	private URL getURL(String strUrl) throws MalformedURLException{
		Map<String, String> map = load();
		final int timeOutWS = Integer.parseInt(map.get("TIME_OUT").trim());
		info ("TimeOut WSCertificadoBMc:"+timeOutWS);

		URL url = new URL(null,strUrl,
                new URLStreamHandler() { // Anonymous (inline) class
                @Override
                protected URLConnection openConnection(URL url) throws IOException {
                URL clone_url = new URL(url.toString());
                HttpURLConnection clone_urlconnection = (HttpURLConnection) clone_url.openConnection();
                // TimeOut settings
                clone_urlconnection.setConnectTimeout(timeOutWS );
                clone_urlconnection.setReadTimeout(timeOutWS);
                return(clone_urlconnection);
                }

                @Override
                protected URLConnection openConnection(URL url,Proxy p) throws IOException {
                URL clone_url = new URL(url.toString());
                HttpURLConnection clone_urlconnection = (HttpURLConnection) clone_url.openConnection();
                // TimeOut settings
                clone_urlconnection.setConnectTimeout( timeOutWS);
                clone_urlconnection.setReadTimeout(timeOutWS);

                return(clone_urlconnection);
                }
            });

		return url;
	}

	/**
	 * Metodo que sirve para limpiar una cadena
	 * @param cadena String con la cadena a limpiar
	 * @param cadRemplace String con la cadena que se va a ser la suistitucion
	 * @return String con la cadena de respuesta
	 */
	private String limpiaCadena(String cadena,String cadRemplace){
		String cad = "";
		if(cadena!=null){
			cad = cadena.trim().replaceAll("[^0-9A-F]",cadRemplace);
		}
		return  cad;
	}

	/**
	 * Metodo para insertar error monitoreo metrics
	 *
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 *
	 * @param codeError codigo de error
	 * @param msgError mensaje de error
	 * @param traza traza completa del error
	 */
	private void insertaError(String codeError, String msgError, String traza){
		boMetrics.bitacorizaError(senderInit.getDTOError(codeError,msgError,traza));
	}

	/**
	 * Metodo para ejecutar insert asincrono
	 * Monitoreo servicios externos.
	 *
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 */
	private void insertaSExterno() {
		boMetrics.bitacorizaWs(senderInit.getMetricsWS(codeError, "WSCertificadoBMc", strUrl,
				"POST","SOAP",timeIn,timeOut,errWS));
	}
}