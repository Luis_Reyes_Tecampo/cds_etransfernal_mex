/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOIntFinancierosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015   ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancierosHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasIntFinancierosHis;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOIntFinancierosImpl.
 * 
 * Implementa los metodos de acceso a la Base de Datos
 * para realizar los procesos instanciados
 */

/**
 * @author SNGEnterprise
 * Fecha 29/30/2020
 * Hora 00:00:00
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOIntFinancierosHisImpl extends Architech implements DAOIntFinancierosHis{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 12743178256710661L;

	/** La constante QUERY_CONSULTA_FINANHIS
	 * 
	 *  Se establece la variable que contiene la consulta a realizar a la BD 
	 *  private: indica que solo se puede acceder a ella con una herencia y no desde 
	 *  cualquier otra clase o instancia
	 */
	private static final String QUERY_CONSULTA_FINANHIS = "SELECT TMP.* FROM ( SELECT TMP.*, ROWNUM AS RN, (SELECT COUNT(1)FROM COMU_INTERME_FIN_HIS [FILTRO_WH_AND]) AS CONT "
			+ " FROM (SELECT CVE_INTERME, TO_CHAR(FCH_MODIF,'DD/MM/YYYY HH24:MI:SS') FCH_MODIF, TIPO_MODIF, USUARIO, TIPO_INTERME_ORI, "
			+ " TIPO_INTERME_NVO, NUM_CECOBAN_ORI, NUM_CECOBAN_NVO, NOMBRE_CORTO_ORI, NOMBRE_CORTO_NVO, "
			+ " NOMBRE_LARGO_ORI, NOMBRE_LARGO_NVO, NUM_BANXICO_ORI, NUM_BANXICO_NVO "
			+ " FROM COMU_INTERME_FIN_HIS   [FILTRO_WH_AND] )TMP [FILTRO_WH_AND] ) TMP ";
	
	/** La constante QUERY_CONSULTA_PAGINADO. 
	 * private: indica que solo se puede acceder a ella con una herencia 
	 * */
	private static final String FILTRO_PAGINADO_FINANHIS = " WHERE RN BETWEEN ? AND ? ";  
	
	/**
	 * Constante QUERY_FINAHIS : contiene una cadena de mas constantes
	 * las cuales juntas de sempe�an una accion 
	 * 
	 * private: indica que solo se puede acceder a ella con una herencia 
	 * */
	private static final String  QUERY_FINAHIS = QUERY_CONSULTA_FINANHIS + FILTRO_PAGINADO_FINANHIS;
	   
	/** 
	 * La variable que contiene informacion con respecto a: utilerias utileriasIntFinancierosHis. 
	 * private: indica que a traves de una instancia no es accesible el metodo.
	 * Al heredar el metodo se convierte en inaccesible.
	 * */
	private UtileriasIntFinancierosHis utileriasIntFinancierosHis = UtileriasIntFinancierosHis.getInstancia();

	/** 
	 * La variable que contiene informacion con respecto a: utilerias. 
     * private: indica que a traves de una instancia no es accesible el metodo. 
     * Al heredar el metodo se convierte en inaccesible.
     */
	private Utilerias utilerias = Utilerias.getUtilerias();   	

	/**
	 * (sin javadoc)
	 * 
	 * @see mx.isban.etranfernal.dao.catalogos.DAOIntFinancierosHisImpl
	 * public. indica que se puede accesder al metodo desde cualquier instancia 
	 * 
	 * @param ArchitechSessionBean : session
	 * @param BeanIntFinancieroHistorico : beanIntFinancieroHistorico
	 * @param BeanPaginador : beanPaginador
	 * @param BeanResIntFinancierosHist : response
	 * @param BeanIntFinancieroHistorico : lstFinanHis
	 * @return response
	 * 
	 */
	@Override
	public BeanResIntFinancierosHist consultar(ArchitechSessionBean session,
			BeanIntFinancieroHistorico beanIntFinancieroHistorico, BeanPaginador beanPaginador) {
		
		BeanResIntFinancierosHist response = new BeanResIntFinancierosHist();
		List<BeanIntFinancieroHistorico> lstFinanHis = Collections.emptyList();
		List<HashMap<String,Object>> queryResponse = null;
		
		StringBuilder filtro= new StringBuilder();
		
		utileriasIntFinancierosHis.getFiltro(beanIntFinancieroHistorico, "WHERE", filtro);
		utileriasIntFinancierosHis.getFiltro(beanIntFinancieroHistorico, "AND",filtro);
		
		try{

			ResponseMessageDataBaseDTO responseDTO =  HelperDAO.consultar(QUERY_FINAHIS.replaceAll("\\[FILTRO_WH_AND\\]", filtro.toString()),
					utileriasIntFinancierosHis.getPaginadorXParamHis(beanPaginador), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName()); 
			
			if (responseDTO.getResultQuery()!=null && !responseDTO.getResultQuery().isEmpty()){
				lstFinanHis = new ArrayList <BeanIntFinancieroHistorico>();
				queryResponse = responseDTO.getResultQuery();
				
				for (HashMap<String,Object> map:queryResponse){
					BeanIntFinancieroHistorico intFinancieroHistorico = new BeanIntFinancieroHistorico();
					intFinancieroHistorico.setCveInterme(utilerias.getString(map.get("CVE_INTERME")));
					intFinancieroHistorico.setFchModif(utilerias.getString(map.get("FCH_MODIF")));
					intFinancieroHistorico.setTipoModif(utilerias.getString(map.get("TIPO_MODIF")));
					intFinancieroHistorico.setUsuario(utilerias.getString(map.get("USUARIO")));
					intFinancieroHistorico.setTipoIntermeOri(utilerias.getString(map.get("TIPO_INTERME_ORI")));
					intFinancieroHistorico.setTipoIntermeNvo(utilerias.getString(map.get("TIPO_INTERME_NVO")));
					intFinancieroHistorico.setNumCecobanOri(utilerias.getString(map.get("NUM_CECOBAN_ORI")));
					intFinancieroHistorico.setNumCecobanNvo(utilerias.getString(map.get("NUM_CECOBAN_NVO")));
					intFinancieroHistorico.setNombreCortoOri(utilerias.getString(map.get("NOMBRE_CORTO_ORI")));
					intFinancieroHistorico.setNombreCortoNvo(utilerias.getString(map.get("NOMBRE_CORTO_NVO")));
					intFinancieroHistorico.setNombreLargoOri(utilerias.getString(map.get("NOMBRE_LARGO_ORI")));
					intFinancieroHistorico.setNombreLargoNvo(utilerias.getString(map.get("NOMBRE_LARGO_NVO")));
					intFinancieroHistorico.setNumBanxicoOri(utilerias.getString(map.get("NUM_BANXICO_ORI")));
					intFinancieroHistorico.setNumBanxicoNvo(utilerias.getString(map.get("NUM_BANXICO_NVO")));

					lstFinanHis.add(intFinancieroHistorico);
					response.setTotalReg(utilerias.getInteger(map.get("CONT")));
				}
			}
			response.setIntFinancieroHistoricos(lstFinanHis);
			response.setBeanError(utileriasIntFinancierosHis.getError(responseDTO));
		}catch(ExceptionDataAccess e){
			//Muestra la excepcion generada
			showException(e);
			response.setBeanError(utileriasIntFinancierosHis.getError(null));
		}
		return response;
	}

}
