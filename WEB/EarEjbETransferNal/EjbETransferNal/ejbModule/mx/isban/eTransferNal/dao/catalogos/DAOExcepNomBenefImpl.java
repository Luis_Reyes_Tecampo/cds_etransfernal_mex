/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOExcepCtasFideicomisoImpl.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      08/09/2019 10:17:24 AM     Uriel Alfredo Botello R.    VSF        Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepNomBenef;
import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasExcepNomBenef;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOExcepCtasFideicomisoImpl.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOExcepNomBenefImpl extends Architech implements DAOExcepNomBenef {

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2714268270823223705L;

	/** La constante FILTRO_CTA. */
	private static final String FILTRO_NOM = "\\[FILTRO_NOM\\]";
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private transient Utilerias utilerias = Utilerias.getUtilerias();
	
	/** La variable que contiene informacion con respecto a: util ctas fideico. */
	private transient UtileriasExcepNomBenef utilNomBenef = UtileriasExcepNomBenef.getInstancia();
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_NOMBRE_FIDEICO_EXENTOS "
			.concat("WHERE TRIM(NOMBRE) = TRIM(UPPER(?))");
	

	/**
	 * Consulta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Valor de filtros
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepNomBenef consultaNombres(ArchitechSessionBean sessionBean,BeanExcepNomBenef beanExcepNomBenef) {
		BeanExcepNomBenef response = new BeanExcepNomBenef();
		/** Se declaran variables */
		ResponseMessageDataBaseDTO responseDTO = null;
		String query = "SELECT REG.INDICE, REG.NOMBRE FROM (SELECT ROWNUM INDICE, NOMBRE FROM TRAN_NOMBRE_FIDEICO_EXENTOS [FILTRO_NOM]"
				+ " ORDER BY NOMBRE) REG "
				+ " WHERE REG.INDICE BETWEEN ? AND ?";
		List<Object> parametros = new ArrayList<Object>();
		/** Se hace filtrado con los datos de entrada */
		String filtro = utilNomBenef.getFiltro(beanExcepNomBenef.getBeanFilter(), "WHERE");
		query = query.replaceAll(FILTRO_NOM, filtro);
		/** Se asignan los parametros */
		parametros.add(beanExcepNomBenef.getBeanPaginador().getRegIni());
		parametros.add(beanExcepNomBenef.getBeanPaginador().getRegFin());
		
		try {
			/** ejecuta consulta */
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			List<HashMap<String,Object>> list = null;
			List<BeanNomBeneficiario> listaCuentas = new ArrayList<BeanNomBeneficiario>();
			/** Valida la respuesta de consulta */
			if(responseDTO.getResultQuery()!= null && responseDTO.getResultQuery().size() > 0){
	    		//se asigna el resultado
				list = responseDTO.getResultQuery();    				
				/**inicializa el map**/
				listaCuentas = recorreResultado(list);
				/** asigna datos de salida */
				response.setListaNombres(listaCuentas);
				response.getBeanError().setCodError(Errores.OK00000V);
				response.getBeanError().setMsgError(responseDTO.getMessageError());
			 } else {
				 /** Si no encontro información regresa mensaje */
				response.getBeanError().setCodError(Errores.ED00011V);
				response.getBeanError().setMsgError(Errores.DESC_ED00011V);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			 }
		} catch (ExceptionDataAccess e) {
			/** si fallo la conexion a BD */
			showException(e);
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorna la respuesta **/
		return response;
	}
	
	/**
	 * Recorre resultado.
	 *
	 * @param list El objeto: list con informacion
	 * @return Objeto list con informacion seteada
	 */
	public List<BeanNomBeneficiario> recorreResultado(List<HashMap<String,Object>> list) {
		/** inicializa variables */
		List<BeanNomBeneficiario> listaNombres = new ArrayList<BeanNomBeneficiario>();
		/** recorre resultado de consulta */
		for(Map<String,Object> map:list){
			if(map.size() > 0) {
				/** recupera informacion de la consulta */
				BeanNomBeneficiario nombreBean = new BeanNomBeneficiario();
				nombreBean.setNombre(utilerias.getString(map.get("NOMBRE")).trim());
				listaNombres.add(nombreBean);
			}
		}
		/** Retorna la respuesta **/
		return listaNombres;
	}


	/**
	 * Alta nombre.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nombre El objeto: nombre  --> Valor del registro
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepNomBenef altaNombre(ArchitechSessionBean sessionBean, String nombre) {
		/** define variables */
		String query = "INSERT INTO TRAN_NOMBRE_FIDEICO_EXENTOS(NOMBRE) VALUES(TRIM(UPPER(?)))";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(nombre.trim());
		return ejecutaBasico(query, parametros);
	}


	/**
	 * Baja nombre.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nombre El objeto: nombre --> Valor del registro
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepNomBenef bajaNombre(ArchitechSessionBean sessionBean, String nombre) {
		/** se declaran variables */
		String query = "DELETE FROM TRAN_NOMBRE_FIDEICO_EXENTOS WHERE TRIM(NOMBRE) = TRIM(UPPER(?))";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(nombre.trim());
		return ejecutaBasico(query, parametros);
	}
	
	/**
	 * Ejecuta basico.
	 *
	 * @param query El objeto: query a ejecutar
	 * @param parametros El objeto: parametros a ingresar en el query
	 * @return Objeto bean excep ctas fideicomiso en respuesta a la operacion
	 */
	private BeanExcepNomBenef ejecutaBasico(String query,List<Object> parametros) {
		BeanExcepNomBenef response = new BeanExcepNomBenef();
		try {
			/** ejecuta consulta */
			HelperDAO.eliminar(query,parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.getBeanError().setCodError(Errores.OK00000V);
			response.getBeanError().setMsgError(Errores.DESC_OK00000V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} catch (ExceptionDataAccess e) {
			/** entra aqui si hubo un problema de conexion o similar */
			showException(e);
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorna la respuesta **/
		return response;
	}
	

	/**
	 * Total registros.
	 *
	 * @param nombre El objeto: nombre --> Valor buscado
	 * @return Objeto int --> total de registros obtenidos
	 */
	@Override
	public int totalRegistros(String nombre) {
		/** se declaran variables */
		int total = 0;
		String query = "SELECT COUNT(1) TOTAL FROM TRAN_NOMBRE_FIDEICO_EXENTOS [FILTRO_NOM]";
		/** se establece el filtrado */
		if(nombre == null ||nombre.isEmpty()) {
			query = query.replaceAll(FILTRO_NOM, "");
		} else {
			query = query.replaceAll(FILTRO_NOM, " WHERE UPPER(TRIM(NOMBRE)) like  UPPER(").concat("'%".concat(nombre.trim()).concat("%')"));
		}
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			/** ejecuta la consulta */
			List<HashMap<String,Object>> list = null;
			responseDTO = HelperDAO.consultar(query, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** obtiene los resultados */
			if(responseDTO.getResultQuery()!= null && responseDTO.getResultQuery().size() > 0){
				list = responseDTO.getResultQuery();
				/** setea el dato de retorno */
				total = Integer.parseInt(utilerias.getString(list.get(0).get("TOTAL")));
			}
		} catch (ExceptionDataAccess e) {
			/** entra aqui si hubo un problema de conexion */
			showException(e);
		}
		return total;
	}


	/**
	 * Modificacionnombres.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nomOld El objeto: nom old --> Valor actual
	 * @param nomNew El objeto: nom new --> Valor nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepNomBenef modificacionNombres(ArchitechSessionBean sessionBean, String nomOld,
			String nomNew) {
		/** se declaran variables */
		BeanExcepNomBenef response = new BeanExcepNomBenef();
		int respDuplicado = -1;
		BeanExcepNomBenef beanExcepNomBenef = new BeanExcepNomBenef();
		beanExcepNomBenef.getBeanFilter().setNombre(nomNew);
		beanExcepNomBenef.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String query = "UPDATE TRAN_NOMBRE_FIDEICO_EXENTOS SET NOMBRE = TRIM(UPPER(?)) WHERE TRIM(NOMBRE) = TRIM(UPPER(?))";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(nomNew.trim());
		parametros.add(nomOld.trim());
		
		respDuplicado = buscarRegistro(beanExcepNomBenef.getBeanFilter().getNombre(), sessionBean);
		
		if(respDuplicado == 0) {
			try {
				/** se ejecuta consulta */
				HelperDAO.actualizar(query,parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.getBeanError().setCodError(Errores.OK00000V);
				response.getBeanError().setMsgError(Errores.DESC_OK00000V);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			} catch (ExceptionDataAccess e) {
				/** entra aqui si ocurrio un problema de conexion */
				showException(e);
				response.getBeanError().setCodError(Errores.EC00011B);
				response.getBeanError().setMsgError(Errores.DESC_EC00011B);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		} else {
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError(Errores.DESC_ED00130V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		/** Retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 * 
	 * * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param nomNew El objeto: nom new --> Valor a buscar
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto int --> total de registros obtenidos
	 */
	@Override
	public int buscarRegistro(String nomNew, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = new ArrayList<Object>();
			parametros.add(nomNew);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
	

}
