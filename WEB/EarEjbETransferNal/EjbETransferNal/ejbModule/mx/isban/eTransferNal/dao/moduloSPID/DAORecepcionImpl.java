/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAORecepcionImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBloqueo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDOpciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para la Recepcion
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepcionImpl extends Architech implements DAORecepcion {

	/**
	 *
	 */
	private static final long serialVersionUID = 1041913510555848982L;

	/**
	 *
	 */
	private static final StringBuilder CONSULTA_DET_RECEPCION_HIS =
			new StringBuilder().append("SELECT T.* FROM ( ")
				.append("SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, ")
				.append("R.folio_paquete,R.folio_pago,R.topologia, ")
				.append("R.enviar,R.cda,R.estatus_banxico, ")
				.append("R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, ")
				.append("R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, ")
				.append("R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, ")
				.append("R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, ")
				.append("R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, ")
				.append("R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, ")
				.append("R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, ")
				.append("R.prioridad,R.long_rastreo,R.folio_paq_dev, ")
				.append("R.folio_pago_dev,R.refe_transfer,R.monto, ")
				.append("TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, ")
				.append("R.direccion_ip,R.cde,R.nombre_ord, ")
				.append("R.nombre_rec,R.domicilio_ord,R.concepto_pago, D.DESCRIPCION AS mot_dev_desc, TO_CHAR(H.FCH_AVISO_ABONO,'DD/MM/YYYY HH24:MI:SS') FECHA_CAPTURA ")
				.append("FROM TRAN_SPID_REC_HIS R ")
				.append("LEFT OUTER JOIN COMU_DETALLE_CD D ON D.CODIGO_GLOBAL = R.MOTIVO_DEVOL AND D.TIPO_GLOBAL = 'TR_DEVSPID' ")
				.append("LEFT JOIN TRAN_SPID_HORAS_HIS H ON ")
				.append("(R.FCH_OPERACION  = H.FCH_OPERACION AND ") 
				.append("R.CVE_MI_INSTITUC = H.CVE_INST_BENEF AND ")
				.append("R.CVE_INST_ORD    = H.CVE_INST_ORD AND ") 
				.append("R.FOLIO_PAQUETE   = H.FOLIO_PAQUETE  AND ")
				.append("R.FOLIO_PAGO      = H.FOLIO_PAGO AND H.TIPO = 'R') ")
				.append("WHERE R.CVE_MI_INSTITUC = ? ")
				.append("AND TRUNC(R.FCH_OPERACION) = TO_DATE(?,'dd-mm-yyyy') ")
				.append("AND R.CVE_INST_ORD = ? ")
				.append("AND R.FOLIO_PAQUETE = ? ")
				.append("AND R.FOLIO_PAGO = ? ")
				.append(" ) T");

	@Override
	public BeanResRecepcionDAO obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean) {

		BeanResRecepcionDAO beanResRecepcionDAO = new BeanResRecepcionDAO();
		List<HashMap<String,Object>> list = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	BeanReceptoresSPID beanDetReceptorSPID = new BeanReceptoresSPID();
    	Utilerias utilerias = Utilerias.getUtilerias();

    	try {
        	responseDTO = HelperDAO.consultar(CONSULTA_DET_RECEPCION_HIS.toString(), Arrays.asList(
        			new Object[] { beanLlave.getCveMiInstituc(),
        					beanLlave.getFchOperacion(),
        					beanLlave.getCveInstOrd(),
        					beanLlave.getFolioPaquete(),
        					beanLlave.getFolioPago() })
        			, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();

        		 if(!list.isEmpty()){
    			 	for (Map<String, Object> mapResult : list){
    			 		beanDetReceptorSPID = llenaRecepcion(mapResult);

    			 		llenarDescripcionDevolucion(beanDetReceptorSPID, utilerias, mapResult);

    			 		beanResRecepcionDAO.setBeanReceptorSPID(beanDetReceptorSPID);
    		        	beanResRecepcionDAO.setCodError(responseDTO.getCodeError());
    		        	beanResRecepcionDAO.setMsgError(responseDTO.getMessageError());
    		        	return beanResRecepcionDAO;
    			 	}
    			 }
    		 }
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return null;
	}

	/**
	 * @param beanDetReceptorSPID Objeto del tipo BeanReceptoresSPID
	 * @param utilerias Objeto del tipo Utilerias
	 * @param mapResult Objeto del tipo Map<String, Object>
	 */
	private void llenarDescripcionDevolucion(BeanReceptoresSPID beanDetReceptorSPID, Utilerias utilerias,
			Map<String, Object> mapResult) {
		if (beanDetReceptorSPID.getEnvDev() != null && mapResult.get("MOT_DEV_DESC") != null){
			beanDetReceptorSPID.getEnvDev().setMotivoDevDesc(utilerias.getString(mapResult.get("MOT_DEV_DESC")));
		}
	}

	/**
	 * @param map Objeto del tipo Map<String,Object>
	 * @return BeanReceptoresSPID
	 */
	private BeanReceptoresSPID llenaRecepcion(Map<String,Object> map){
    	BeanSPIDBloqueo beanSPIDBloqueo = new BeanSPIDBloqueo();
    	BeanReceptoresSPID beanReceptoresSPID = new BeanReceptoresSPID();
    	beanReceptoresSPID.setLlave(seteaLlave(map));
    	beanReceptoresSPID.setDatGenerales(seteaDatosGenerales(map));
    	beanReceptoresSPID.setEstatus(seteaEstatus(map));
    	beanReceptoresSPID.setBancoOrd(seteaBancoOrd(map));
    	beanReceptoresSPID.setBancoRec(seteaBancoRec(map));
    	beanReceptoresSPID.setEnvDev(seteaEnvDev(map));
    	beanReceptoresSPID.setRastreo(seteaRastreo(map));
    	beanReceptoresSPID.setFchPagos(seteaFchPago(map));
    	beanReceptoresSPID.setOpciones(new BeanSPIDOpciones());
    	beanReceptoresSPID.setBloqueo(beanSPIDBloqueo);
    	return beanReceptoresSPID;
    }

	/**
     * Metodo que setea los campos llave
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDLlave Bean de respuesta
     */
    private BeanSPIDLlave seteaLlave(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
    	beanSPIDLlave.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
    	beanSPIDLlave.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
    	beanSPIDLlave.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
    	beanSPIDLlave.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
    	beanSPIDLlave.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
    	return beanSPIDLlave;
    }

    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDDatGenerales Bean de respuesta de datos generales
     */
    private BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
    	BigDecimal monto = utilerias.getBigDecimal(map.get("MONTO"));
    	beanSPIDDatGenerales.setTopologia(utilerias.getString(map.get("TOPOLOGIA")));
    	beanSPIDDatGenerales.setPrioridad(utilerias.getString(map.get("PRIORIDAD")));
    	beanSPIDDatGenerales.setTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
    	beanSPIDDatGenerales.setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")));
    	beanSPIDDatGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
    	beanSPIDDatGenerales.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
    	beanSPIDDatGenerales.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
    	beanSPIDDatGenerales.setCde(utilerias.getString(map.get("CDE")));
    	beanSPIDDatGenerales.setCda(utilerias.getString(map.get("CDA")));
    	beanSPIDDatGenerales.setEnviar(utilerias.getString(map.get("ENVIAR")));
    	beanSPIDDatGenerales.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
    	beanSPIDDatGenerales.setFechaCaptura(utilerias.getString(map.get("FECHA_CAPTURA")));
    	
    	
    	beanSPIDDatGenerales.setMonto(monto);
    	return beanSPIDDatGenerales;
    }

    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEstatus Bean de respuesta de estatus
     */
    private BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
    	beanSPIDEstatus.setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")));
    	beanSPIDEstatus.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
    	beanSPIDEstatus.setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));
    	return beanSPIDEstatus;
    }

    /**
     * Metodo que setea los campos del banco ordenante
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
    	beanSPIDBancoOrd.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
    	beanSPIDBancoOrd.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
    	beanSPIDBancoOrd.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
    	beanSPIDBancoOrd.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
    	beanSPIDBancoOrd.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
    	beanSPIDBancoOrd.setDomicilioOrd(utilerias.getString(map.get("DOMICILIO_ORD")));
    	beanSPIDBancoOrd.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
    	beanSPIDBancoOrd.setCodPostalOrd(utilerias.getString(map.get("COD_POSTAL_ORD")));
    	beanSPIDBancoOrd.setFchConstitOrd(utilerias.getString(map.get("FCH_CONSTIT_ORD")));

    	return beanSPIDBancoOrd;
    }

    /**
     * Metodo que setea los campos de rastreo
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
    	beanSPIDRastreo.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
    	beanSPIDRastreo.setCveRastreoOri(utilerias.getString(map.get("CVE_RASTREO_ORI")));
    	beanSPIDRastreo.setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")));
    	beanSPIDRastreo.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
    	return beanSPIDRastreo;
    }

    /**
     * Metodo que setea los campos del Banco Receptor
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
    	beanSPIDBancoRec.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
    	beanSPIDBancoRec.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaTran(utilerias.getString(map.get("NUM_CUENTA_TRAN")));
    	beanSPIDBancoRec.setRfcRec(utilerias.getString(map.get("RFC_REC")));

    	return beanSPIDBancoRec;
    }

    /**
     * Metodo que setea los campos de las fechas de pago
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
     */
    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
    	beanSPIDFchPagos.setFchAceptPago(utilerias.getString(map.get("FCH_ACEPT_PAGO")));
    	beanSPIDFchPagos.setHoraAceptPago(utilerias.getString(map.get("HORA_ACEPT_PAGO")));
    	beanSPIDFchPagos.setFchInstrucPago(utilerias.getString(map.get("FCH_INSTRUC_PAGO")));
    	beanSPIDFchPagos.setHoraInstrucPago(utilerias.getString(map.get("HORA_INSTRUC_PAGO")));
    	return beanSPIDFchPagos;
    }

    /**
     * Metodo que setea el folio paquete y pago de devolucion
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEnvDev Bean de respuesta
     */
    private BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
    	beanSPIDEnvDev.setMotivoDev(utilerias.getString(map.get("MOTIVO_DEVOL")));
    	beanSPIDEnvDev.setFolioPaqDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")));
    	beanSPIDEnvDev.setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")));

    	return beanSPIDEnvDev;
    }
}