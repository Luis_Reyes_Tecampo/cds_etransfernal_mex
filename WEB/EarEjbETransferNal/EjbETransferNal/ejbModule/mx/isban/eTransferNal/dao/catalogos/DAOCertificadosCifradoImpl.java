/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCertificadosCifradoImpl.java
 * 
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-25-2018 04:48:36 PM Juan Jesus Beltran R. Vector Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCertificadoCifrado;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOCertificadosCifradoImpl.
 * 
 * Implementa los metodos de acceso BAse de Datos, 
 * 
 * Los cuales proveen a la logica de negocio 
 * comunicacion con la tabla de Oracle
 *
 * @author FSW-Vector
 * @since 08-25-2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCertificadosCifradoImpl extends Architech implements DAOCertificadosCifrado {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4631696196287459824L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY CANAL ASC";
	
	/** La constante QUERY_CONSULTA_CERTIFICADOS. */
	private static final String QUERY_CONSULTA_TODO = "SELECT * FROM( SELECT TC.*, ROWNUM AS RN FROM ("
			+ "	SELECT CANAL, ALIAS_KS, NUM_CERTIF, TO_CHAR(FCH_VENCIMIENTO,'DD-MM-YYYY') FCH_VENCIMIENTO, ALG_DIGEST, ALG_SIMETRICO, TEC_ORIGEN, CONTADOR.CONT " 
			+ "    	FROM (SELECT COUNT(1) CONT "
			+ "	FROM TRAN_CERTIF_KS TC [FILTRO_WH]) CONTADOR, TRAN_CERTIF_KS [FILTRO_WH]) TC ) TC ";
	
	/** La constante QUERY_CONSULTA_PAGINADO. */
	private static final String QUERY_CONSULTA_PAGINADO = QUERY_CONSULTA_TODO + FILTRO_PAGINADO + ORDEN;	
	
	/** La constante CONSULTA_EXISTENCIA. */
	public static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_CERTIF_KS  WHERE UPPER(TRIM(CANAL)) = UPPER(TRIM(?)) ";
	
	/** La constante CONSULTA_INSERTAR. */
	public static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_CERTIF_KS (CANAL, ALIAS_KS, NUM_CERTIF, FCH_VENCIMIENTO, ALG_DIGEST, ALG_SIMETRICO, TEC_ORIGEN)" 
												  + " VALUES (UPPER(?),TRIM(?), ?, TO_DATE(?, 'DD-MM-YYYY'), UPPER(?), UPPER(?), UPPER(?))";
	/** La constante QUERY_DELETE. */
	private static final String QUERY_DELETE = "DELETE TRAN_CERTIF_KS WHERE UPPER(TRIM(CANAL)) = UPPER(TRIM(?)) ";
	
	/** La constante CONSULTA_UPDATE. */
	private static final String CONSULTA_UPDATE = "UPDATE TRAN_CERTIF_KS SET ALIAS_KS=TRIM(?),NUM_CERTIF=?,FCH_VENCIMIENTO=TO_DATE(?,'DD-MM-YYYY'),ALG_DIGEST=UPPER(?),ALG_SIMETRICO=UPPER(?),TEC_ORIGEN=UPPER(?)"
			+ " WHERE UPPER(TRIM(CANAL)) = UPPER(TRIM(?)) ";
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private Utilerias utilerias = Utilerias.getUtilerias();
	
	/** La variable que contiene informacion con respecto a: utilerias certificados. */
	private UtileriasCertificadoCifrado utileriasCertificados = UtileriasCertificadoCifrado.getInstancia();
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado#consultaCertificados(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 */
	@Override
	public BeanCertificadosCifrado consultar(ArchitechSessionBean session,
			BeanCertificadoCifrado beanFilter, BeanPaginador beanPaginador) {
		//Obejtos que sirven para generar la respuesta del servicio
		BeanCertificadosCifrado response = new BeanCertificadosCifrado();
		List<BeanCertificadoCifrado> certificados = Collections.emptyList();
		//Obejtos que sirven para realizar la consulta y procesar el resultado
		List<HashMap<String, Object>> queryResponse = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		//Agrega los parametros del Query
		String filtroAnd = utileriasCertificados.getFiltro(beanFilter, "AND");
		String filtroWhere = utileriasCertificados.getFiltro(beanFilter, "WHERE");
		try {
			//Realiza la consulta
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_PAGINADO.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(
        			new Object[] { beanPaginador.getRegIni(),
 						   beanPaginador.getRegFin() }), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				//Se inicializa la lista un vez que se valido que la cosnulta produjo resultados
				certificados = new ArrayList<BeanCertificadoCifrado>();
				queryResponse = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : queryResponse) {
					//Inicializo el objeto certificado para comenzar a ingresar los datos
					BeanCertificadoCifrado certificado = new BeanCertificadoCifrado();
					//Agrega cada propiedad
					certificado.setCanal(utilerias.getString(map.get("CANAL")));
					certificado.setAlias(utilerias.getString(map.get("ALIAS_KS")));
					certificado.setNumCertificado(utilerias.getString(map.get("NUM_CERTIF")));
					certificado.setFchVencimiento(utilerias.getString(map.get("FCH_VENCIMIENTO")));
					certificado.setAlgDigest(utilerias.getString(map.get("ALG_DIGEST")));
					certificado.setAlgSimetrico(utilerias.getString(map.get("ALG_SIMETRICO")));
					certificado.setTecOrigen(utilerias.getString(map.get("TEC_ORIGEN")));
					certificado.setEstatus(utileriasCertificados.getEstatus(certificado.getFchVencimiento()));
					//Agrega el certificado
					certificados.add(certificado);
					//Ingresa el Total de Registros
					response.setTotalReg(utilerias.getInteger(map.get("CONT")));
				}
			}
			//Agrega la lista completa
			response.setCertificados(certificados);
			//Agrega el BeanError
			response.setBeanError(utileriasCertificados.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
			response.setBeanError(utileriasCertificados.getError(null));
		}
		//Response DAO
		return response;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado#consultarCombo(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificados, java.lang.String)
	 */
	@Override
	public BeanCombosCertificadoCifrado consultarCombo(ArchitechSessionBean session, BeanCombosCertificadoCifrado beanCertificados, String consulta) {
		//Objeto a responder
		BeanCombosCertificadoCifrado combos = new BeanCombosCertificadoCifrado();
		List<BeanCatalogo> lista = Collections.emptyList();
		ResponseMessageDataBaseDTO responseDTO = null;
		Map<String, String> mapa = null;
		
		try {
			//Agrega los parametros del Query
			mapa = utileriasCertificados.validaConsulta(consulta);
			//Realiza la consulta
			responseDTO = HelperDAO.consultar(mapa.get("consulta"), Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				lista = utileriasCertificados.recorrerLista(responseDTO, mapa);
				combos = utileriasCertificados.validaLista(beanCertificados, lista, mapa, consulta);
			}
			//Agrega el BeanError
			combos.setBeanError(utileriasCertificados.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
		}
		//Response DAO
		return combos;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado#agregarCertificado(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado)
	 */
	@Override
	public BeanResBase agregarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado) {
		//Objeto a responder
		BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			//Agrega los parametros del Query
			parametros = utileriasCertificados.agregarParametros(beanCertificado, UtileriasCertificadoCifrado.ALTA);
			int existencia = buscarRegistro(beanCertificado);
			if(existencia == 0) {
				//Realiza la consulta
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
			response.setCodError(Errores.EC00011B);
		}
		//Response DAO
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado#editarCertificado(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado)
	 */
	@Override
	public BeanResBase editarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado) {
		//Objeto a responder
		BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			//Agrega los parametros del Query
			parametros = utileriasCertificados.agregarParametros(beanCertificado, UtileriasCertificadoCifrado.MODIFICACION);
			//Realiza la consulta
			responseDTO = HelperDAO.actualizar(CONSULTA_UPDATE, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.setCodError(responseDTO.getCodeError());
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada y setea el codigo de error
			showException(e);
			response.setCodError(Errores.EC00011B);
		}
		//Response DAO
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado#eliminarCertificados(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado)
	 */
	@Override
	public BeanResBase eliminarCertificados(ArchitechSessionBean session,
			BeanCertificadoCifrado beanCertificado) {
		final BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			//Realiza la consulta
			responseDTO = HelperDAO.eliminar(QUERY_DELETE, Arrays.asList(new Object[] { beanCertificado.getCanal() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			//Muestra la excepcion generada
			response.setCodError(Errores.EC00011B);
		}
		//Response DAO
		return response;
	}
	
	/**
	 * Buscar registro.
	 * 
	 * Metodo privado para validar si 
	 * existe el registro a dar de alta 
	 *
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto int
	 */
	private int buscarRegistro(BeanCertificadoCifrado beanCertificado) {
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		//Objeto a responder
		int contador = 0;
		try {
			//Agrega los parametros del Query
			parametros = utileriasCertificados.agregarParametros(beanCertificado, UtileriasCertificadoCifrado.BUSQUEDA);
			//Realiza la consulta
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
		}
		//Response DAO
		return contador;
	}
	
}
