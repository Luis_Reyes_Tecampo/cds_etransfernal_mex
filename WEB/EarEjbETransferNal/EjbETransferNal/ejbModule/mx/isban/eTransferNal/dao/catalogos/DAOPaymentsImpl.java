package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultasPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasConsultaPayments;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOPaymentsImpl.
 * 
 * Implementa los metodos de acceso BAse de Datos, 
 * 
 * Los cuales proveen a la logica de negocio 
 * comunicacion con la tabla de Oracle
 *
 * @author FSW-SNG Enterprise
 * @since 07-12-2020
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOPaymentsImpl extends Architech implements DAOPayments{
	
	/** La constante serialVersionUID. 
	 * El proceso de serializaci�n asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calcular� un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	 * private: indica que a trav�s de una instancia no es accesible el m�todo. Al heredar el m�todo se convierte en inaccesible.
	 * En esta constante se asigna la consulta que se ejecutara para presentar los medios de entrega y presentarlos en el 
	 * combo de lapantalla de pagos duplicados.
	*/
	private static final long serialVersionUID = -4944318991855433026L;
	
	/** La constante QUERY_CONSULTA_CD
	 * private: indica que a trav�s de una instancia no es accesible el m�todo. Al heredar el m�todo se convierte en inaccesible.
	 * En esta constante se asigna la consulta que se ejecutara para presentar los medios de entrega y presentarlos en el 
	 * grid de lapantalla de pagos duplicados.
	 */
	private static final String QUERY_CONSULTA_CD = "SELECT CVE_MEDIO_ENT, DESCRIPCION, CTRL_DUPLICADOS, CONT, RN, "
			+ "TRIM(CVE_MEDIO_ENT)||'-'||TRIM(DESCRIPCION) AS DETALLES , CD "
			+ "FROM (SELECT CVE_MEDIO_ENT, DESCRIPCION,CTRL_DUPLICADOS as CD, CASE CTRL_DUPLICADOS  "
			+ "WHEN '0' THEN 'DESACTIVADO' WHEN '1' THEN 'ACTIVADO' END AS CTRL_DUPLICADOS,  "
			+ "(SELECT COUNT(*) AS CONT FROM COMU_MEDIOS_ENT  WHERE CTRL_DUPLICADOS LIKE [FILTRO_LK] [FILTRO_AND] "
			+ ") AS CONT,  ROWNUM AS RN FROM COMU_MEDIOS_ENT  WHERE CTRL_DUPLICADOS LIKE [FILTRO_LK] [FILTRO_AND]"
			+ ") COMU_MEDIOS_ENT WHERE CD LIKE [FILTRO_LK] [FILTRO_AND]";
	
	/** La constante QUERY_CONSULTA_COMBO
	 * private: indica que a trav�s de una instancia no es accesible el m�todo. Al heredar el m�todo se convierte en inaccesible.
	 */
	private static final String QUERY_CONSULTA_COMBO = "SELECT CVE_MEDIO_ENT, DESCRIPCION, CTRL_DUPLICADOS, CONT, RN, TRIM(CVE_MEDIO_ENT)||'-'||TRIM(DESCRIPCION) AS DETALLES , "
			+ "CD  FROM (SELECT CVE_MEDIO_ENT, DESCRIPCION,CTRL_DUPLICADOS as CD, CASE CTRL_DUPLICADOS  "
			+ "WHEN '0' THEN 'DESACTIVADO' WHEN '1' THEN 'ACTIVADO' END AS CTRL_DUPLICADOS,  (SELECT COUNT(*) AS CONT FROM COMU_MEDIOS_ENT  "
			+ "WHERE CTRL_DUPLICADOS LIKE [FILTRO_CD]  "
			+ ") AS CONT,  ROWNUM AS RN FROM COMU_MEDIOS_ENT  "
			+ "WHERE CTRL_DUPLICADOS LIKE [FILTRO_CD] "
			+ ") COMU_MEDIOS_ENT WHERE CD LIKE [FILTRO_CD]";
	
	
	/** La constante FILTRO_PAGINADOL. */
	private static final String FILTRO_PAGI = " AND RN BETWEEN ? AND ? ";
	
	/** La constante ORDEN. */
	private static final String ORDEN = " ORDER BY RN";
	
	/** La constante QUERY_CONSULTA_PAGINADO. */
	private static final String QUERY_CONSULTA_PAGINADO= QUERY_CONSULTA_CD + FILTRO_PAGI + ORDEN;
	
	/** La constante QUERY_CONSULTA_PAGINADO. */
	private static final String QUERY_CONSULTA= QUERY_CONSULTA_COMBO  + ORDEN;
       
	 /** La constante QUERY_DELETE. */
	private static final String QUERY_UDATE = "UPDATE COMU_MEDIOS_ENT SET CTRL_DUPLICADOS=UPPER(TRIM(?)) WHERE TRIM(CVE_MEDIO_ENT) = UPPER(TRIM(?)) ";
	
	@EJB
	private DAOExportar daoExportar;
	@EJB
	private BOPistaAuditora boPistaAuditora;
	@EJB
	private DAOPayments daoPayments;
	
	/** La variable que contiene informacion con respecto a: utilerias. 
     * private: indica que a trav�s de una instancia no es accesible el m�todo. Al heredar el m�todo se convierte en inaccesible.
     */
	private Utilerias utilerias = Utilerias.getUtilerias();
	
	/** La variable que contiene informacion con respecto a: utilerias Payments. 
	 * private: indica que a trav�s de una instancia no es accesible el m�todo. Al heredar el m�todo se convierte en inaccesible.
	 * */
	private UtileriasConsultaPayments utileriasPayments = UtileriasConsultaPayments.getInstancia();
	

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOPayments#consultarPayments(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayments, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 * public: indica que es un m�todo accesible a trav�s de una instancia del objeto.
	 * Este metodo se utiliza para consultar todos los registros de pagos duplicados los cuales se presentan 
	 * en la pantalla de activacion desactivacion de canales unicos.
	 * @param session El objeto session, se btiene la sesion 
	 * @param beanFilter en este objeto se utiliza para obtener los datos de la vista (JSP).
	 * @param beanPaginador este objeto se utiliza para la paginacion. 
	 * @return BeanConsultasPayme Bean de respuesta
	 */
	@Override
	public BeanConsultasPayme consultarPayments(ArchitechSessionBean session, BeanConsultaPayme beanFilter,BeanPaginador beanPaginador) {
		
		BeanConsultasPayme response = new BeanConsultasPayme ();
		List<BeanConsultaPayme> lstConsultaPayments = Collections.emptyList();
		List<HashMap<String, Object>> queryResponse = null;
		
		String newSql = utileriasPayments.getSqlCatalogo(beanFilter, QUERY_CONSULTA_PAGINADO);
		
		
		try{
			ResponseMessageDataBaseDTO responseDTO = HelperDAO.consultar(newSql,
					utileriasPayments.getPaginadorXParam(beanPaginador),
					HelperDAO.CANAL_GFI_DS,this.getClass().getName()); 
	 
		
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()){
				lstConsultaPayments = new ArrayList<BeanConsultaPayme>();
				queryResponse = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : queryResponse){
					BeanConsultaPayme beanConsultaPayments = new BeanConsultaPayme();
					beanConsultaPayments.setClave(utilerias.getString(map.get("CVE_MEDIO_ENT")));
					beanConsultaPayments.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
					beanConsultaPayments.setActDes(utilerias.getString(map.get("CTRL_DUPLICADOS")));
					beanConsultaPayments.setDetalles(utilerias.getString(map.get("DETALLES")));
					lstConsultaPayments.add(beanConsultaPayments);
					response.setTotalReg(utilerias.getInteger(map.get("CONT")));
				}
			}
			response.setMedEntrega(lstConsultaPayments);
			response.setBeanError(utileriasPayments.getError(responseDTO));
		}catch (ExceptionDataAccess e){
			showException(e);
			response.setBeanError(utileriasPayments.getError(null));
		}
		
		return response;
	}	
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOPayments#consultarPayments(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayments, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 * public: indica que es un m�todo accesible a trav�s de una instancia del objeto.
	 * Este metodo se utiliza para modificar alguno de los registros de pagos duplicados los cuales se presentan 
	 * en la pantalla de activacion desactivacion de canales unicos.
	 * @param session El objeto session, se btiene la sesion 
	 * @param beanConsultaPayments en este objeto se utiliza para obtener los datos de la vista (JSP).
	 * @return BeanResBase Bean de respuesta
	 */
	@Override
	public BeanResBase editarPayments(ArchitechSessionBean session, BeanConsultaPayme beanConsultaPayments) {
		BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		
		if (beanConsultaPayments.getActDes() != null && !beanConsultaPayments.getActDes().isEmpty()
				&& "ACTIVADO".equalsIgnoreCase(beanConsultaPayments.getActDes().trim())) {
			beanConsultaPayments.setActDes("1");
		}else if (beanConsultaPayments.getActDes() != null && !beanConsultaPayments.getActDes().isEmpty()
				&& "DESACTIVADO".equalsIgnoreCase(beanConsultaPayments.getActDes().trim())) {
			beanConsultaPayments.setActDes("0");
		}
		
		try{
			parametros = utileriasPayments.agregarParametros(beanConsultaPayments, utileriasPayments.MODIFICACION);
			
			responseDTO = HelperDAO.actualizar(QUERY_UDATE, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());			
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada y setea el codigo de error
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			
		}
		//Response DAO
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOPayments#consultarPayments(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayments, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 * public: indica que es un m�todo accesible a trav�s de una instancia del objeto.
	 * Este metodo se utiliza para consulta los registros de pagos duplicados los cuales se presentan 
	 * en la pantalla de activacion desactivacion de canales unicos.
	 * @param session El objeto session, se btiene la sesion 
	 * @param beanFilter en este objeto se utiliza para obtener los datos de la vista (JSP).
	 * @param beanPaginador en este objeto se utiliza para obtener los datos de la vista (JSP).
	 * @return BeanConsultasPayme Bean de respuesta
	 */
	@Override
	public BeanConsultasPayme consultarCombo(ArchitechSessionBean session, BeanConsultaPayme beanFilter, BeanPaginador beanPaginador) {
		BeanConsultasPayme response = new BeanConsultasPayme();
		List<BeanConsultaPayme> lstConsulPayments = Collections.emptyList();
		List<HashMap<String, Object>> queryResponse = null;
		
		String newSql = utileriasPayments.getSqlCombo(beanFilter, QUERY_CONSULTA);
		
		
		try{
			
			ResponseMessageDataBaseDTO responseDTO = HelperDAO.consultar(newSql, Collections.emptyList(),
					HelperDAO.CANAL_GFI_DS,this.getClass().getName()); 
		
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()){
				lstConsulPayments = new ArrayList<BeanConsultaPayme>();
				queryResponse = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : queryResponse){
					BeanConsultaPayme beanConsultaPayments = new BeanConsultaPayme();
					beanConsultaPayments.setClave(utilerias.getString(map.get("CVE_MEDIO_ENT")));
					beanConsultaPayments.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
					beanConsultaPayments.setActDes(utilerias.getString(map.get("CTRL_DUPLICADOS")));
					beanConsultaPayments.setDetalles(utilerias.getString(map.get("DETALLES")));
					lstConsulPayments.add(beanConsultaPayments);
				}
			}
			response.setListaCMEnt(lstConsulPayments);
			response.setBeanError(utileriasPayments.getError(responseDTO));
		}catch (ExceptionDataAccess e){
			showException(e);
			response.setBeanError(utileriasPayments.getError(null));
		}
		
		return response;
	
	}
	
	
}
