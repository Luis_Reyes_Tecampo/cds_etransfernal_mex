/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOTopologiaImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveMedio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveOperacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologiaDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTopologia;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para la configuraci�n de topologias
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOTopologiaImpl extends Architech implements DAOTopologia {
	
	/**Propiedad del tipo long que conserva el estado del objeto*/
	private static final long serialVersionUID = -5011442685652552539L;
	
	/**
	 * 
	 */
	private static final String TOPO_PRI = "TOPO_PRI";
	
	/**Propiedad del tipo StringBuilder para la consulta de topologias*/
	private static final StringBuilder CONSULTA_TOPOLOGIAS = new 
			StringBuilder().append("SELECT * FROM (") 
      			   					      .append("SELECT r.*, ROWNUM RNUM, COUNT (*) OVER () CONT ") 
  			   					      	  .append("FROM (")
  			   					      	  		.append("SELECT CVE_MEDIO_ENT, CVE_OPERACION, TOPO_PRI, IMPORTE_MINIMO ") 
  			   					      	  		.append("FROM TRAN_SPID_TOPOL [ORDENAMIENTO]) R ")
  			   					      	  		.append(")") 
		      	  		   .append("WHERE RNUM BETWEEN ? AND ?");
	
	/**Propiedad del tipo StringBuilder para la consulta de clave operacion*/
	private static final StringBuilder CONSULTA_CVE_OPER = new
			StringBuilder().append("SELECT CVE_OPERACION, DESCRIPCION ")
						   .append("FROM TRAN_CVE_OPER");
	
	/**Propiedad del tipo StringBuilder para la consulta de clave operacion*/
	private static final StringBuilder CONSULTA_CVE_MEDIO_ENT = new
			StringBuilder().append("SELECT CVE_MEDIO_ENT, DESCRIPCION ")
						   .append("FROM COMU_MEDIOS_ENT");
	
	/**Propiedad del tipo StringBuilder para la consulta de insercion topologias*/
	private static final StringBuilder INSERTA_TOPOLOGIA = new 
			StringBuilder().append("INSERT INTO TRAN_SPID_TOPOL(CVE_MEDIO_ENT, CVE_OPERACION, TOPO_PRI, IMPORTE_MINIMO) ")
						   .append("VALUES(?, ?, ?, ?)");
	
	/**Propiedad del tipo StringBuilder para la conteo de registros*/
	private static final StringBuilder CONTAR_REGISTROS = new 
			StringBuilder().append("SELECT COUNT(*) AS CONT ")
						   .append("FROM TRAN_SPID_TOPOL ")
						   .append("WHERE CVE_MEDIO_ENT = ? AND CVE_OPERACION = ? AND TOPO_PRI = ?");	
	
	/**
	 * Metodo que obtiene las topologias
	 * @param beanPaginador Obejeto de tipo @see BeanPaginador
	 * @param architechSessionBean Obejeto del tipo @see ArchitechSessionBean
	 * @param sortField Obejeto del tipo @see String
	 * @param sortType Obejeto del tipo @see String
	 * @return BeanResTopologiaDAO
	 */
	@Override
	public BeanResTopologiaDAO obtenerTopologias(BeanPaginador beanPaginador, 
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		BeanTopologia beanTopologia = null;
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanTopologia> listaTopologias = new ArrayList<BeanTopologia>();
		
		try {
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			responseDTO = HelperDAO.consultar(CONSULTA_TOPOLOGIAS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), 
					Arrays.asList(new Object[] {
						beanPaginador.getRegIni(),
						beanPaginador.getRegFin()
					}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {
					beanTopologia = new BeanTopologia();
					
					beanTopologia.setClaveMedio(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
					beanTopologia.setClaveOperacion(utilerias.getString(mapResult.get("CVE_OPERACION")));
					
					beanTopologia.setPrioridad(String.valueOf(utilerias.getString(mapResult.get(TOPO_PRI)).charAt(1)));
					beanTopologia.setTopologia(String.valueOf(utilerias.getString(mapResult.get(TOPO_PRI)).charAt(0)));
					beanTopologia.setImporteMinimo(utilerias.getBigDecimal(mapResult.get("IMPORTE_MINIMO")).toPlainString());
					
					listaTopologias.add(beanTopologia);
					
					beanResTopologiaDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
				}
			}
			
			beanResTopologiaDAO.setListaTopologias(listaTopologias);
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.EC00011B);
		}
		
		return beanResTopologiaDAO;
	}
	
	/**
	 * @param cveMedioEnt Objeto del tipo String
	 * @param cveOperacion Objeto del tipo String
	 * @param topoPri Objeto del tipo Stirng
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResTopologiaDAO
	 */
	@Override
	public BeanResTopologiaDAO obtenerTopologia(String cveMedioEnt,
			String cveOperacion, String topoPri, 
			ArchitechSessionBean architechSessionBean) {
		BeanTopologia beanTopologia = null;
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanTopologia> listaTopologias = new ArrayList<BeanTopologia>();
		
		try {
			StringBuilder consultaTopologia = new 
					StringBuilder().append("SELECT CVE_MEDIO_ENT, CVE_OPERACION, TOPO_PRI, IMPORTE_MINIMO ") 
						      	   .append("FROM TRAN_SPID_TOPOL ")
						      	   .append("WHERE CVE_MEDIO_ENT = '").append(cveMedioEnt).append("' ")
						      	   .append("AND CVE_OPERACION = '").append(cveOperacion).append("' ")
						      	   .append("AND TOPO_PRI = '").append(topoPri).append("'");
			
			responseDTO = HelperDAO.consultar(consultaTopologia.toString(),
					Arrays.asList(new Object[] {}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {
					beanTopologia = new BeanTopologia();
					
					beanTopologia.setClaveMedio(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
					beanTopologia.setClaveOperacion(utilerias.getString(mapResult.get("CVE_OPERACION")));
					
					beanTopologia.setPrioridad(String.valueOf(utilerias.getString(mapResult.get(TOPO_PRI)).charAt(1)));
					beanTopologia.setTopologia(String.valueOf(utilerias.getString(mapResult.get(TOPO_PRI)).charAt(0)));
					beanTopologia.setImporteMinimo(utilerias.getBigDecimal(mapResult.get("IMPORTE_MINIMO")).toPlainString());
					
					listaTopologias.add(beanTopologia);
					
					beanResTopologiaDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
				}
			}
			
			beanResTopologiaDAO.setListaTopologias(listaTopologias);
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.EC00011B);
		}		
		return beanResTopologiaDAO;
	}

	@Override
	/**
	 * Metodo que obtiene los datos para los combos de cveOperacion y cveMedio
	 * @param architechSessionBean Obejeto del tipo @see ArchitechSessionBean
	 * @return BeanResTopologiaDAO
	 * @throws Exception Excepcion de negocio
	 */
	public BeanResTopologiaDAO obtenerCombosTopologia(
			ArchitechSessionBean architechBean) {
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanClaveMedio> listaCveMedio = new ArrayList<BeanClaveMedio>();
		List<BeanClaveOperacion> listaCveOperacion = new ArrayList<BeanClaveOperacion>();
		BeanClaveMedio claveMedio = null;
		BeanClaveOperacion beanClaveOperacion = null;
		try {
			responseDTO = HelperDAO.consultar(CONSULTA_CVE_OPER.toString(), 
											  Arrays.asList(new Object[]{}), 
											  HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {
					beanClaveOperacion = new BeanClaveOperacion();
					
					beanClaveOperacion.setClaveOperacion(utilerias.getString(mapResult.get("CVE_OPERACION")));
					beanClaveOperacion.setDescripcion(utilerias.getString(mapResult.get("DESCRIPCION")));
					
					listaCveOperacion.add(beanClaveOperacion);
				}
			}
			
			responseDTO = HelperDAO.consultar(CONSULTA_CVE_MEDIO_ENT.toString(), 
					  Arrays.asList(new Object[]{}), 
					  HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {
					claveMedio = new BeanClaveMedio();
					
					claveMedio.setClaveMedio(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
					claveMedio.setDescripcion(utilerias.getString(mapResult.get("DESCRIPCION")));
					
					listaCveMedio.add(claveMedio);
				}
			}
			beanResTopologiaDAO.setListaCveMedio(listaCveMedio);
			beanResTopologiaDAO.setListaCveOperacion(listaCveOperacion);
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.EC00011B);
		}
		
		return beanResTopologiaDAO;
	}

	@Override
	/**
	 * Metodo que guarda una nueva topologia
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param architechSessionBean Obejeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Obejeto de tipo @see BeanPaginador
	 * @return BeanResTopologiaDAO
	 * @throws Exception Excepcion de negocio
	 */
	public BeanResTopologiaDAO guardarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean){
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;

		try {
			responseDTO = HelperDAO.insertar(INSERTA_TOPOLOGIA.toString(), 
					Arrays.asList(new Object[] {
						beanTopologia.getClaveMedio(),
						beanTopologia.getClaveOperacion(),
						beanTopologia.getTopologia()+beanTopologia.getPrioridad(),
						new BigDecimal(beanTopologia.getImporteMinimo())
					}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.ED00061V);
		}
		
		return beanResTopologiaDAO;
	}

	@Override
	/**
	 * Metodo para verificar si no hay un registro repetido
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @return boolean
	 * @throws Exception excepcion de negocio
	 */
	public boolean isRepetido(BeanTopologia beanTopologia) {
		boolean isRepetido = false;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		try {
			responseDTO = HelperDAO.consultar(CONTAR_REGISTROS.toString(), 
					Arrays.asList( new Object[] {
						beanTopologia.getClaveMedio(), 
						beanTopologia.getClaveOperacion(),
						beanTopologia.getTopologia()+beanTopologia.getPrioridad()
					}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {									
					isRepetido = utilerias.getInteger(mapResult.get("CONT")) > 0;
				}
			}
			
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return isRepetido;
	}

	@Override
	/**
	 * Metodo que elimina una topologia
	 * @param beanReqTopologia Obejto del tipo @see BeanReqTopologia
	 * @param architechSessionBean Obejeto del tipo @see ArchitechSessionBean
	 * @return BeanResTopologiaDAO
	 * @throws Exception Excepcion de negocio
	 */
	public BeanResTopologiaDAO eliminarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean) {
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		StringBuilder eliminarTopologia = new 
				StringBuilder().append("DELETE FROM TRAN_SPID_TOPOL ")
							   .append("WHERE CVE_MEDIO_ENT = '").append(beanTopologia.getClaveMedio().split(" ")[0].trim()).append("' ")
							   .append("AND CVE_OPERACION = '").append(beanTopologia.getClaveOperacion().split(" ")[0].trim()).append("' ")
							   .append("AND TOPO_PRI = '").append(beanTopologia.getTopologia()+""+beanTopologia.getPrioridad()).append("'");
		
		try {
			responseDTO = HelperDAO.eliminar(eliminarTopologia.toString(), 
					Arrays.asList(new Object[]{}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.EC00011B);
		}
		
		return beanResTopologiaDAO;
	}

	@Override
	/**
	 * Metodo que actualiza una topologia
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param architechSessionBean Obejeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Obejeto de tipo @see BeanPaginador
	 * @param cveMedio Objeto del tipo @see String
	 * @param cveOperacion objeto del tipo @see String
	 * @param toPri Obejto del tipo @see String
	 * @return BeanResTopologiaDAO
	 * @throws Exception Excepcion de negocio
	 */
	public BeanResTopologiaDAO guardarEdicionTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean, String cveMedio, String cveOperacion, String toPri) {
		final BeanResTopologiaDAO beanResTopologiaDAO = new BeanResTopologiaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		StringBuilder actualizarTopologia = new 
				StringBuilder().append("UPDATE TRAN_SPID_TOPOL ") 
							   .append("SET CVE_MEDIO_ENT = ?, CVE_OPERACION = ?, TOPO_PRI = ?, IMPORTE_MINIMO = ? ") 
							   .append("WHERE CVE_MEDIO_ENT = '").append(cveMedio.split(" ")[0].trim()).append("' ")
							   .append("AND CVE_OPERACION = '").append(cveOperacion.split(" ")[0].trim()).append("' ")
							   .append("AND TOPO_PRI = '").append(toPri).append("' ");

		try {
			responseDTO = HelperDAO.actualizar(actualizarTopologia.toString(), 
					Arrays.asList(new Object[] {
						beanTopologia.getClaveMedio(),
						beanTopologia.getClaveOperacion(),
						beanTopologia.getTopologia()+beanTopologia.getPrioridad(),
						new BigDecimal(beanTopologia.getImporteMinimo())
					}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResTopologiaDAO.setCodError(responseDTO.getCodeError());
			beanResTopologiaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResTopologiaDAO.setCodError(Errores.EC00011B);
		}
		
		return beanResTopologiaDAO;
	}
	
	/**
	 * 
	 * @param architechSessionBeans tipo architechSessionBeans
	 * @return tipo BigDecimal
	 */
	public BigDecimal obtenerImporteTotalTopo(ArchitechSessionBean architechSessionBeans) {
	  	ResponseMessageDataBaseDTO responseDTO = null;
	  	BigDecimal totalImporte = BigDecimal.ZERO;
	  	
	  	try {              	
	  		StringBuilder query = new StringBuilder().append("SELECT SUM(IMPORTE_MINIMO) MONTO_TOTAL ") 
						      	   .append("FROM TRAN_SPID_TOPOL MONTO_TOTAL  ");
	  		
	      	responseDTO = HelperDAO.consultar(query.toString(), Arrays.asList(
	      			new Object[] { }),
	        			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	    	if(responseDTO.getResultQuery()!= null){
	    		totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
	    	}
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}

}
