/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOMantenimientoCertificadoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificadoDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificadoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion para el mantenimiento de certificados.
 *
 * @author FSW-Vector
 * @since 7/01/2020
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMantenimientoCertificadoImpl extends Architech implements DAOMantenimientoCertificado {
	
	/** Propiedad de tipo long que conserva es estado del objeto. */
	private static final long serialVersionUID = 2741916527704484957L;
	
	/** Propiedad de tipo string que guarda la tabla TRAN_SPEI_CERTIF. */
	private static final String TRAN_SPEI_CERTIF = "TRAN_SPEI_CERTIF";
	
	/** Propiedad de tipo string que guarda la tabla TRAN_SPID_CERTIF. */
	private static final String TRAN_SPID_CERTIF = "TRAN_SPID_CERTIF";
	
	/** Propiedad del tipo String para los datos de la consulta. */
	private static final String SELECT ="SELECT CVE_MI_INSTITUC, NUM_CERTIFICADO, ESTADO_CERT, PALABRA_CLAVE, SW_MANT, FACUL_FIRMA, "+
										"FACUL_MANT, USUARIO_ALTA, USUARIO_MODIF, FCH_REGISTRO, FCH_MODIF, "+
										"SW_DEFAULT_FIRMA, SW_DEFAULT_MANT, MENSAJE "+
										"FROM ";
	
	/** La constante INSERT. 
	 * query que contiene todos los campos para realizar el insert
	 * a la pantalla mantenimiento de certificados
	 * */
	private static final String INSERT="(CVE_MI_INSTITUC, NUM_CERTIFICADO,"+
										"ESTADO_CERT, SW_MANT, SW_DEFAULT_FIRMA, SW_DEFAULT_MANT, FACUL_FIRMA, FACUL_MANT, "+
										"USUARIO_ALTA, FCH_REGISTRO, PALABRA_CLAVE, MENSAJE) "+
										"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?, ?)";
	
	/** Propiedad de tipo StringBuilder que sirve para realizar consulta de certificados
	 * cuando el modulo seleccionado es el spei. */
	private static final StringBuilder CONSULTA_MANTENIMIENTOS_SPEI = new 
			StringBuilder().append("SELECT * FROM ( ")
               			   				  .append("SELECT R.*, ROWNUM RNUM, COUNT(*) OVER () CONT ")
               			   				  .append("FROM ( ")
               			   				  		.append(SELECT) 
               			   				  			.append(TRAN_SPEI_CERTIF)
               			   				  			.append(" ")
               			   				  		.append(") R ")
                								.append(") ")
								 .append("WHERE RNUM BETWEEN ? AND ?")
								 .append(" ")
								 .append("ORDER BY FCH_REGISTRO");	
	
	/** La constante CONSULTA_MANTENIMIENTOS_SPID
	 * cuando el modulo seleccionado es el spid*/
	private static final StringBuilder CONSULTA_MANTENIMIENTOS_SPID = new 
			StringBuilder().append("SELECT * FROM ( ")
               			   				  .append("SELECT R.*, ROWNUM RNUM, COUNT(*) OVER () CONT ")
               			   				  .append("FROM ( ")
               			   				  		.append(SELECT) 
               			   				  			.append(TRAN_SPID_CERTIF)
               			   				  			.append(" ")
               			   				  			.append(") R ")
                								.append(") ")
								 .append("WHERE RNUM BETWEEN ? AND ?")
								 .append(" ")
								 .append("ORDER BY FCH_REGISTRO");	
	
	/** Propiedad de tipo StringBuilder que sirve para realizar una insercion de certificado
	 * cuando el modulo seleccionado es le spei*/
	private static final StringBuilder INSERTA_CERTIFICADO_SPEI = new
			StringBuilder().append("INSERT INTO ")
							.append(TRAN_SPEI_CERTIF)
							.append("")
							.append(INSERT);
	
	/** La constante INSERTA_CERTIFICADO_SPID
	 * cuando el modulo seleccionado es el spid */
	private static final StringBuilder INSERTA_CERTIFICADO_SPID = new
			StringBuilder().append("INSERT INTO ")
							.append(TRAN_SPID_CERTIF)
							.append("")
							.append(INSERT);
	
	/** Propiedad de tipo StringBuilder que sirve para realizar un contero de certificados
	  * cuando el modulo seleccionado es el spei */
	private static final StringBuilder CONTADOR_CERTIFICADO_SPEI = new
			StringBuilder().append("SELECT COUNT(*) AS CONT FROM TRAN_SPEI_CERTIF ")
						   .append("WHERE CVE_MI_INSTITUC = ? AND NUM_CERTIFICADO = ?");	
	
	/** La constante CONTADOR_CERTIFICADO_SPID. */
	private static final StringBuilder CONTADOR_CERTIFICADO_SPID = new
			StringBuilder().append("SELECT COUNT(*) AS CONT FROM TRAN_SPID_CERTIF ")
						   .append("WHERE CVE_MI_INSTITUC = ? AND NUM_CERTIFICADO = ?");	
	
	/** Propiedad que almacen el parametro enviado
	  * cuando el modulo seleccionado es el spei */
	private static final String MANTENIMIENTOCERTIFICADOSPEI ="mantenimientoCertificadoSPEI";
	
	/** Propiedad que almacena en el objeto de tipo bean el campo FACUL_FIRMA. */
	private static final String FACUL_FIRMA = "FACUL_FIRMA";
	
	/** Propiedad que almacena en el objeto de tipo bean el campo FACUL_MANT. */
	private static final String FACUL_MANT = "FACUL_MANT";
	
	/** Propiedad que almacena en el objeto de tipo bean el campo FCH_MODIF. */
	private static final String FCH_MODIF = "FCH_MODIF";
	
	/** Propiedad que almacena en el objeto de tipo bean el campo SW_DEFAULT_FIRMA. */
	private static final String SW_DEFAULT_FIRMA = "SW_DEFAULT_FIRMA";
	
	/** Propiedad que almacena en el objeto de tipo bean el campo SW_DEFAULT_MANT. */
	private static final String SW_DEFAULT_MANT = "SW_DEFAULT_MANT";
	
	/** La constante CONT_CERT_ELIMINAR. */
	private static final StringBuilder CONT_CERT_ELIMINAR = new
			StringBuilder().append("SELECT COUNT(*) AS CONT FROM TRAN_SPEI_CERTIF ")
						   .append("WHERE ESTADO_CERT = 'A' AND FACUL_FIRMA = 'S' AND SW_DEFAULT_FIRMA = 'S' AND ")
						   .append("CVE_MI_INSTITUC = ? AND TRIM(NUM_CERTIFICADO) = ?");	
	
	/** La constante CERT_ACT. */
	private static final String CERT_ACT = "El certificado esta activo, no es posible eliminarlo";
	@Override
	/**
	 * Metodo que obtiene los certificados registrados
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMantenimientoCertificadoDAO
	 * @throws Exception
	 */
	public BeanResMantenimientoCertificadoDAO obtenerTopologias(BeanPaginador beanPaginador,
			ArchitechSessionBean architechBean, String nomPantalla) {
		final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = new BeanResMantenimientoCertificadoDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanMantenimientoCertificado> listaMantenimientos = new ArrayList<BeanMantenimientoCertificado>();
		
		try {
			/**
			 * variable de tipo string
			 * definida como una cadena vacia
			 * Convierte las constantes definidas a cadenas para ser usadas 
	    	 * en la construccion del query en el dao
			 */
			String query = "";
			if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
				query = CONSULTA_MANTENIMIENTOS_SPEI.toString();
			}else {
				query = CONSULTA_MANTENIMIENTOS_SPID.toString();
			}
			/**
			 * listado de las opciones para realizar la paginacion
			 * inicio, siguiente, anterior y final
			 * */
			List<Object> params = new ArrayList<Object>();
			params.add(beanPaginador.getRegIni());
			params.add(beanPaginador.getRegFin());
			responseDTO = HelperDAO.consultar(query, params,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**
			 * validacion del resultado del query
			 * */
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				llenaCertificados(beanResMantenimientoCertificadoDAO, utilerias, responseDTO, listaMantenimientos);
			}
			/**
			 * manejo de errores
			 * se asigna el tipo de error:
			 * codigo de error
			 * mensaje de error
			 * */
			beanResMantenimientoCertificadoDAO.setListaMantenimientoCertificado(listaMantenimientos);
			beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
			beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/**
			 * muestra las excepciones
			 * */
			showException(e);
			beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
		}
		/**
		 * regresa el resultado al ejecutar el query
		 * */
		return beanResMantenimientoCertificadoDAO;
	}
	
	@Override
	/**
	 * Metodo que obtiene los certificados registrados
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMantenimientoCertificadoDAO
	 * @throws Exception
	 */
	public BeanResMantenimientoCertificadoDAO obtenerMantenimientoCertificado(String cveMiInstituc,
			String numCertificado,
			ArchitechSessionBean architechBean, String nomPantalla) {
		/**
		 * Instancia al bean beanResConfiguracionParametrosDAO
		 * 
		 * */
		final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = new BeanResMantenimientoCertificadoDAO();
		/**
    	 * instancia de ResponseMessageDataBaseDTO
    	 * igual a null
    	 * */
		Utilerias utilerias = Utilerias.getUtilerias();
		/**
    	 * instancia de ResponseMessageDataBaseDTO
    	 * igual a null
    	 * */
		ResponseMessageDataBaseDTO responseDTO = null;
		/**
		 * declaracion de lista de tipo BeanMantenimientoCertificado
		 * */
		List<BeanMantenimientoCertificado> listaMantenimientos = new ArrayList<BeanMantenimientoCertificado>();
		/**
		 * valida el parametro recibido
		 * cuando este es igual a spei el select se ejecuta en la tabla TRAN_SPEI_CERTIF
		 * cuando este es igual a spid el select se ejecuta en la tabla TRAN_SPID_CERTIF
		 * toma como condicionales los valores almacenados en las constantes CVE_MI_INSTITUC y NUM_CERTIFICADO
		 * para el filtrado de los registros
		 * */
		try {
			if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
				
				StringBuilder consultaMantenimiento = new 
						StringBuilder().append(SELECT) 
  				  						.append(TRAN_SPEI_CERTIF)
  				  						.append(" ")
			   				  			.append("WHERE CVE_MI_INSTITUC = '").append(cveMiInstituc).append("' ")
			   				  			.append("AND NUM_CERTIFICADO = '").append(numCertificado).append("'");
				
				responseDTO = HelperDAO.consultar(consultaMantenimiento.toString(), 
						Collections.emptyList(),
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				/**
				 * valida si la consulta esta vacia, es decir si trae datos
				 * si no esta vacia llena los campos de la pantalla con la informacion 
				 * correspondiente
				 * */
				if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
					llenaCertificados(beanResMantenimientoCertificadoDAO, utilerias, responseDTO, listaMantenimientos);
				}
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
				beanResMantenimientoCertificadoDAO.setListaMantenimientoCertificado(listaMantenimientos);
				beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
				beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				
			}else {
				/**
				 * cuando este es igual a spid el select se ejecuta en la tabla TRAN_SPID_CERTIF
				 * toma como condicionales los valores almacenados en las constantes CVE_MI_INSTITUC y NUM_CERTIFICADO
				 * */
					StringBuilder consultaMantenimiento = new 
							StringBuilder().append(SELECT) 
		  									.append(TRAN_SPID_CERTIF)
		  									.append(" ")
				   				  			.append("WHERE CVE_MI_INSTITUC = '").append(cveMiInstituc).append("' ")
				   				  			.append("AND NUM_CERTIFICADO = '").append(numCertificado).append("'");
					
					responseDTO = HelperDAO.consultar(consultaMantenimiento.toString(), 
							Collections.emptyList(),
							HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					/**
					 * valida si la consulta esta vacia, es decir si trae datos
					 * si no esta vacia llena los campos de la pantalla con la informacion 
					 * correspondiente
					 * */
					if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
						llenaCertificados(beanResMantenimientoCertificadoDAO, utilerias, responseDTO, listaMantenimientos);
					}
					/**
					 * manejo de errores en caso de habarlos.
					 * se obtiene el codigo de error y el mensaje
					 * */
					beanResMantenimientoCertificadoDAO.setListaMantenimientoCertificado(listaMantenimientos);
					beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
					beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				
			}
			
			/**
			 * manejo de excepciones en caso de haber error
			 * */
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
		}
		/**
		 * regresa el resultado del query ejecutado
		 * */
		return beanResMantenimientoCertificadoDAO;
	}

/**
 * Llena certificados.
 *
 * @param beanResMantenimientoCertificadoDAO Objeto del tipo BeanResMantenimientoCertificadoDAO
 * @param utilerias Objeto del tipo Utilerias
 * @param responseDTO Objeto del tipo ResponseMessageDataBaseDTO
 * @param listaMantenimientos Objeto del tipo List<BeanMantenimientoCertificado>
 */
	private void llenaCertificados(final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO,
			Utilerias utilerias, ResponseMessageDataBaseDTO responseDTO,
			List<BeanMantenimientoCertificado> listaMantenimientos) {
		llenaCertificado(beanResMantenimientoCertificadoDAO, utilerias, responseDTO, listaMantenimientos);
	}

	/**
	 * Llena certificado.
	 *
	 * @param beanResMantenimientoCertificadoDAO Objeto del tipo BeanResMantenimientoCertificadoDAO
	 * @param utilerias Objeto del tipo Utilerias
	 * @param responseDTO Objeto del tipo ResponseMessageDataBaseDTO
	 * @param listaMantenimientos Objeto del tipo List<BeanMantenimientoCertificado>
	 */
	private void llenaCertificado(final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO,
			Utilerias utilerias, ResponseMessageDataBaseDTO responseDTO,
			List<BeanMantenimientoCertificado> listaMantenimientos) {
		BeanMantenimientoCertificado beanMantenimientoCertificado;
		for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {
			
			/**
			 * instancia del objeto beanMantenimientoCertificado
			 * **/
			beanMantenimientoCertificado = new BeanMantenimientoCertificado();
			beanMantenimientoCertificado.setBeanMantenimientoCertificadoDos(new BeanMantenimientoCertificadoDos());
			
			beanMantenimientoCertificado.setClaveIns(utilerias.getString(mapResult.get("CVE_MI_INSTITUC")));
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setNumeroCertificado(utilerias.getString(mapResult.get("NUM_CERTIFICADO")));
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setEstado(getEstado(utilerias.getString(mapResult.get("ESTADO_CERT"))));
			
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setPalabraClave(utilerias.getString(mapResult.get("PALABRA_CLAVE")));
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setMantenimiento(utilerias.getString(mapResult.get("SW_MANT")));
								
			beanMantenimientoCertificado.setSFacultadFirma(utilerias.getString(mapResult.get(FACUL_FIRMA)));
			beanMantenimientoCertificado.setSFacultadMant(utilerias.getString(mapResult.get(FACUL_MANT)));
			/**
			 * validaciones para el campo FACUL_FIRMA, que este no sea nulo
			 * **/
			if (utilerias.getString(mapResult.get(FACUL_FIRMA)) != null && 
					!"".equals(utilerias.getString(mapResult.get(FACUL_FIRMA)))){
				beanMantenimientoCertificado.setFacultadFirma(true);
			}
			/**
			 * validaciones para el campo FACUL_MANT, que este no sea nulo
			 * **/
			if (utilerias.getString(mapResult.get(FACUL_MANT)) != null && 
					!"".equals(utilerias.getString(mapResult.get(FACUL_MANT)))){ 
				beanMantenimientoCertificado.setFacultadMant(true);
			}
			/**
			 * validaciones para el campo FCH_MODIF, si este es igual a nulo se pone como alta, si se modifica se marca como modificado
			 * **/
			if (utilerias.getString(mapResult.get(FCH_MODIF)) == null ||
					"".equals(utilerias.getString(mapResult.get(FCH_MODIF)))) { 
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setUsuario(utilerias.getString(mapResult.get("USUARIO_ALTA")));
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setFechaAlta(utilerias.formateaFecha(
						utilerias.getDate(mapResult.get("FCH_REGISTRO")), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
			} else {	
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setFechaAlta(utilerias.formateaFecha(
						utilerias.getDate(mapResult.get("FCH_REGISTRO")), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setUsuarioModifica(utilerias.getString(mapResult.get("USUARIO_MODIF")));
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setFechaModificacion(utilerias.formateaFecha(
						utilerias.getDate(mapResult.get(FCH_MODIF)), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
			}
			/**
			 * validaciones para el campo SW_DEFAULT_FIRMA, que este no sea nulo
			 * **/
			if (utilerias.getString(mapResult.get(SW_DEFAULT_FIRMA)) != null && 
					!"".equals(utilerias.getString(mapResult.get(SW_DEFAULT_FIRMA)))){
				beanMantenimientoCertificado.setDefaultFirma(true);
			}
			/**
			 * validaciones para el campo SW_DEFAULT_MANT, que este no sea nulo
			 * **/
			if (utilerias.getString(mapResult.get(SW_DEFAULT_MANT)) != null && 
					!"".equals(utilerias.getString(mapResult.get(SW_DEFAULT_MANT)))){
				beanMantenimientoCertificado.setDefaultMant(true);					
			}
			/**
			 * obtiene y asigna la informacion para los campos SW_DEFAULT_MANT y SW_DEFAULT_FIRMA
			 * respecto a las utilerias
			 * */
			beanMantenimientoCertificado.setSDefaultMant(utilerias.getString(mapResult.get(SW_DEFAULT_MANT)));
			beanMantenimientoCertificado.setSDefaultFirma(utilerias.getString(mapResult.get(SW_DEFAULT_FIRMA)));
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setMensaje(utilerias.getString(mapResult.get("MENSAJE")));
								
			listaMantenimientos.add(beanMantenimientoCertificado);
			beanResMantenimientoCertificadoDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
		}
	}

	@Override
	/**
	 * Metodo que elimina un certificado
	 * @param beanMantenimientoCertificado Objeto del tipo @see BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResMantenimientoCertificadoDAO Objeto del tipo BeanResMantenimientoCertificadoDAO
	 */
	public BeanResMantenimientoCertificadoDAO eliminarTopologia(
			BeanMantenimientoCertificado beanMantenimientoCertificado, ArchitechSessionBean architechBean, String nomPantalla) {
		final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = new BeanResMantenimientoCertificadoDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
			if (!permiteEliminar(beanMantenimientoCertificado, nomPantalla)) {
				StringBuilder eliminaMantenimiento = new 
						StringBuilder().append("DELETE FROM TRAN_SPEI_CERTIF ");
				
				try {
					eliminaMantenimiento.append(" WHERE CVE_MI_INSTITUC = ");
					eliminaMantenimiento.append("'").append(beanMantenimientoCertificado.getClaveIns()).append("'");
					eliminaMantenimiento.append(" AND NUM_CERTIFICADO = ");
					eliminaMantenimiento.append("'").append(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado()).append("'");
					
					responseDTO = HelperDAO.eliminar(eliminaMantenimiento.toString(), 
							Collections.emptyList(), 
							HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					/**
					 * manejo de errores en caso de habarlos.
					 * se obtiene el codigo de error y el mensaje
					 * */
					beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
					beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
					/**
					 * manejo de errores en caso de habarlos.
					 * se obtiene el codigo de error y el mensaje
					 * */	
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
				}
				/**
				 * regresa el resultado del query ejecutado
				 * */
				return beanResMantenimientoCertificadoDAO;
			} else {
				info(CERT_ACT);
				beanResMantenimientoCertificadoDAO.setCodError(Errores.ED00139V);
				return beanResMantenimientoCertificadoDAO;
			}
		}else {
			
			StringBuilder eliminaMantenimiento = new 
					StringBuilder().append("DELETE FROM TRAN_SPID_CERTIF ");
			
			try {
				eliminaMantenimiento.append(" WHERE CVE_MI_INSTITUC = ");
				eliminaMantenimiento.append("'").append(beanMantenimientoCertificado.getClaveIns()).append("'");
				eliminaMantenimiento.append(" AND NUM_CERTIFICADO = ");
				eliminaMantenimiento.append("'").append(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado()).append("'");
				
				responseDTO = HelperDAO.eliminar(eliminaMantenimiento.toString(), 
						Collections.emptyList(), 
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
				beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
				beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
			} catch (ExceptionDataAccess e) {
				showException(e);
				beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
			}
			/**
			 * regresa el resultado del query ejecutado
			 * */
			return beanResMantenimientoCertificadoDAO;
			
		}
		
		
	}
	
	@Override
	/**
	 * Metodo que verifica si hay elementos repetidos en la BD
	 * @param beanMantenimientoCertificado Obejto del tipo @see BeanMantenimientoCertificado
	 * @return isRepetido Objeto del tipo boolean
	 */
	public boolean isRepetido(BeanMantenimientoCertificado beanMantenimientoCertificado, String nomPantalla) {
		boolean isRepetido = false;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		String query = "";
		if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
			CONTADOR_CERTIFICADO_SPEI.toString();
		}else {
			CONTADOR_CERTIFICADO_SPID.toString();
		}
		
		try {
			List<Object> params = new ArrayList<Object>();
			params.add(beanMantenimientoCertificado.getClaveIns());
			params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
			responseDTO = HelperDAO.consultar(query,params, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {									
					isRepetido = utilerias.getInteger(mapResult.get("CONT")) > 0;
				}
			}
			/**
			 * manejo de errores en caso de habarlos.
			 * se obtiene el codigo de error y el mensaje
			 * */
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return isRepetido;
	}

	@Override
	/**
	 * Metodo que guarda un ceritifcado 
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResMantenimientoCertificadoDAO Objeto del tipo BeanResMantenimientoCertificadoDAO
	 */
	public BeanResMantenimientoCertificadoDAO guardarMantenimiento(
			BeanMantenimientoCertificado beanMantenimientoCertificado, ArchitechSessionBean architechBean, String nomPantalla){
		final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = new BeanResMantenimientoCertificadoDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> params = null;
			if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {

				try {
					params = new ArrayList<Object>();
					params.add(beanMantenimientoCertificado.getClaveIns());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
					params.add("P");
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento());
					params.add(beanMantenimientoCertificado.getSDefaultFirma());
					params.add(beanMantenimientoCertificado.getSDefaultMant());
					params.add(beanMantenimientoCertificado.getSFacultadFirma());
					params.add(beanMantenimientoCertificado.getSFacultadMant());
					params.add(architechBean.getUsuario());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
					responseDTO = HelperDAO.insertar(INSERTA_CERTIFICADO_SPEI.toString(), params, 
							HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					
					beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
					beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResMantenimientoCertificadoDAO.setCodError(Errores.ED00061V);
				}
				/**
				 * regresa el resultado del query ejecutado
				 * */
				return beanResMantenimientoCertificadoDAO;
			}else {
				try {
					params = new ArrayList<Object>();
					params.add(beanMantenimientoCertificado.getClaveIns());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
					params.add("P");
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento());
					params.add(beanMantenimientoCertificado.getSDefaultFirma());
					params.add(beanMantenimientoCertificado.getSDefaultMant());
					params.add(beanMantenimientoCertificado.getSFacultadFirma());
					params.add(beanMantenimientoCertificado.getSFacultadMant());
					params.add(architechBean.getUsuario());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave());
					params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
				responseDTO = HelperDAO.insertar(INSERTA_CERTIFICADO_SPID.toString(), params, 
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
				beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
				beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
			} catch (ExceptionDataAccess e) {
				showException(e);
				beanResMantenimientoCertificadoDAO.setCodError(Errores.ED00061V);
			}
				/**
				 * regresa el resultado del query ejecutado
				 * */
			return beanResMantenimientoCertificadoDAO;
				
			}
			
			
	}
	
	@Override
	/**
	 * Metodo que guarda la edicion de un certificado
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param numeroCertOld Objeto del tipo @see String
	 * @param cveInsOld  Objeto del tipo @see String
	 * @return beanResMantenimientoCertificadoDAO Objeto del tipo BeanResMantenimientoCertificadoDAO
	 */
	public BeanResMantenimientoCertificadoDAO guardarEdicionMantenimiento(
			BeanMantenimientoCertificado beanMantenimientoCertificado, ArchitechSessionBean architechBean,
			String numeroCertOld, String cveInsOld, String nomPantalla) {
		final BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = new BeanResMantenimientoCertificadoDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
			
			StringBuilder actualizarCertificado = new 
					StringBuilder().append("UPDATE TRAN_SPEI_CERTIF ") 
							 	   .append("SET NUM_CERTIFICADO = ?, SW_MANT = ?,  SW_DEFAULT_FIRMA = ?, ") 
					 			   .append("SW_DEFAULT_MANT = ?, FACUL_FIRMA = ?,  FACUL_MANT = ?, ")
			 					   .append("USUARIO_MODIF = ?, FCH_MODIF = SYSDATE, PALABRA_CLAVE = ?, MENSAJE = ? ");

			try {
				actualizarCertificado.append(" WHERE CVE_MI_INSTITUC = '").append(cveInsOld).append("'");
				actualizarCertificado.append(" AND NUM_CERTIFICADO = '").append(numeroCertOld).append("'");
				
				List<Object> params = new ArrayList<Object>();
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento());
				params.add(beanMantenimientoCertificado.getSDefaultFirma());
				params.add(beanMantenimientoCertificado.getSDefaultMant());
				params.add(beanMantenimientoCertificado.getSFacultadFirma());
				params.add(beanMantenimientoCertificado.getSFacultadMant());
				params.add(architechBean.getUsuario());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
				responseDTO = HelperDAO.actualizar(actualizarCertificado.toString(), params, 
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
				beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
				beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
			} catch (ExceptionDataAccess e) {
				showException(e);
				beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
			}
			/**
			 * regresa el resultado del query ejecutado
			 * */
			return beanResMantenimientoCertificadoDAO;
			
		}else {
			
			StringBuilder actualizarCertificado = new 
					StringBuilder().append("UPDATE TRAN_SPID_CERTIF ") 
							 	   .append("SET NUM_CERTIFICADO = ?, SW_MANT = ?,  SW_DEFAULT_FIRMA = ?, ") 
					 			   .append("SW_DEFAULT_MANT = ?, FACUL_FIRMA = ?,  FACUL_MANT = ?, ")
			 					   .append("USUARIO_MODIF = ?, FCH_MODIF = SYSDATE, PALABRA_CLAVE = ?, MENSAJE = ? ");

			try {
				actualizarCertificado.append(" WHERE CVE_MI_INSTITUC = '").append(cveInsOld).append("'");
				actualizarCertificado.append(" AND NUM_CERTIFICADO = '").append(numeroCertOld).append("'");
				
				List<Object> params = new ArrayList<Object>();
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento());
				params.add(beanMantenimientoCertificado.getSDefaultFirma() );
				params.add(beanMantenimientoCertificado.getSDefaultMant());
				params.add(beanMantenimientoCertificado.getSFacultadFirma());
				params.add(beanMantenimientoCertificado.getSFacultadMant());
				params.add(architechBean.getUsuario());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave());
				params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
				responseDTO = HelperDAO.actualizar(actualizarCertificado.toString(),params, 
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
				beanResMantenimientoCertificadoDAO.setCodError(responseDTO.getCodeError());
				beanResMantenimientoCertificadoDAO.setMsgError(responseDTO.getMessageError());
				/**
				 * manejo de errores en caso de habarlos.
				 * se obtiene el codigo de error y el mensaje
				 * */
			} catch (ExceptionDataAccess e) {
				showException(e);
				beanResMantenimientoCertificadoDAO.setCodError(Errores.EC00011B);
			}
			/**
			 * regresa el resultado del query ejecutado
			 * */
			return beanResMantenimientoCertificadoDAO;
			
		}
		
		
	}
	
	/**
	 * Metodo privado para obtener el string del estado del certificado.
	 *
	 * @param estado Objeto del tipo @see String
	 * @return estatus Objeto del tipo String
	 */
	private String getEstado(String estado) {	
		String estatus =  null;
		if ("A".equals(estado)) {
			estatus = "ACTIVO";
		} 
		if ("B".equals(estado)) {
			estatus = "BAJA";
		} 
		if ("P".equals(estado)) {
			estatus = "ALTA PENDIENTE";
		} 
		if ("E".equals(estado)) {
			estatus = "ERROR";
		} 
		if ("I".equals(estado)) {
			estatus = "INACTIVO";
		}
		/**
		 * retorna el estatus del certificado
		 * */
		return estatus;
	}

	/**
	 * Metodo que verifica si el certificado no esta activo.
	 * De esta forma se podra eliminar el registro.
	 * 
	 * @param beanMantenimientoCertificado Obejto del tipo @see BeanMantenimientoCertificado
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return boolean
	 */
	@Override
	public boolean permiteEliminar(BeanMantenimientoCertificado beanMantenimientoCertificado, String nomPantalla) {
		boolean isActivo = false;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		String query = CONT_CERT_ELIMINAR.toString();
		try {
			List<Object> params = new ArrayList<Object>();
			params.add(Integer.parseInt(beanMantenimientoCertificado.getClaveIns()));
			params.add(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado());
			responseDTO = HelperDAO.consultar(query,params, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {				
				for(Map<String, Object> mapResult : responseDTO.getResultQuery()) {									
					isActivo = utilerias.getInteger(mapResult.get("CONT")) > 0;
				}
			}
			/**
			 * manejo de errores en caso de habarlos.
			 * se obtiene el codigo de error y el mensaje
			 * */
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return isActivo;
	}
}
