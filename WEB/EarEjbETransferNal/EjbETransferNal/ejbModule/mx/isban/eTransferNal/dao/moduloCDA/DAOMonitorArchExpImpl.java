/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorArchExpImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 18:47:01 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsMonitorArchExp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExpDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga obtener la informacion para el Monitor de
 * archivos a exportar
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorArchExpImpl extends Architech implements
		DAOMonitorArchExp {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = -5241282484074804552L;

	/**
	 * Constante que almacena la consulta del monitor de archivos a exportar
	 */
	private final static String QUERY_CONSULTA_MON_ARCHIVO_EXP =

	" SELECT TMP.MODULO,TMP.SUBMODULO,TMP.NOMBRE_ARCHIVO,"
			+ "            CASE(TMP.ESTATUS) WHEN 'PE' THEN 'PENDIENTE' "
			+ "              WHEN 'EP' THEN 'EN PROCESO' "
			+ "              WHEN 'OK' THEN 'FINALIZADO' "
			+ "              WHEN 'KO' THEN 'CON ERROR' END ESTATUS, "
			+ " TMP.NUMERO_REGISTROS,"
			+ " TMP.FECHA_EXPORTA,TMP.CONT "
			+ " FROM ( "
			+ " SELECT TMP.MODULO, "
			+ " TMP.SUBMODULO,TMP.NOMBRE_ARCHIVO,TMP.ESTATUS,TMP.NUMERO_REGISTROS, "
			+ " TMP.FECHA_EXPORTA,TMP.ORDEN,TMP.CONT, ROWNUM AS RN "
			+ " FROM ( "
			+ " SELECT CDA.MODULO,CDA.SUBMODULO,CDA.NOMBRE_ARCHIVO,CDA.ESTATUS,"
			+ "            CASE(CDA.ESTATUS) WHEN 'PE' THEN 3 "
			+ "              WHEN 'EP' THEN 1 "
			+ "              WHEN 'OK' THEN 2 "
			+ "              WHEN 'KO' THEN 4 END ORDEN, "
			+ " CDA.NUMERO_REGISTROS,CDA.FECHA_EXPORTA,CONTADOR.CONT "
			+ " FROM (SELECT COUNT(1) CONT FROM TRAN_SPEI_EXP_CDA) CONTADOR, TRAN_SPEI_EXP_CDA CDA "
			+ " ORDER BY ORDEN ASC, FECHA_EXPORTA  )TMP "
			+ " )TMP " + " WHERE RN BETWEEN ? AND ? "
			+ " ORDER BY ORDEN ASC, FECHA_EXPORTA ";

	/**
	 * Metodo que permite consultar el monitor de archivos exportar
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMonitorArchExpDAO Objeto del tipo
	 *         BeanResConsMonitorArchExpDAO
	 */
	public BeanResConsMonitorArchExpDAO consultarMonitorArchExp(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) {
		final BeanResConsMonitorArchExpDAO beanResConsMonitorArchExpDAO = new BeanResConsMonitorArchExpDAO();
		List<HashMap<String, Object>> list = null;
		List<BeanConsMonitorArchExp> listBeanConsMonitorArchExp = Collections
				.emptyList();
		BeanConsMonitorArchExp beanConsMonitorArchExp = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_MON_ARCHIVO_EXP,
					Arrays.asList(new Object[] { beanPaginador.getRegIni(),
							beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listBeanConsMonitorArchExp = new ArrayList<BeanConsMonitorArchExp>();
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					beanConsMonitorArchExp = new BeanConsMonitorArchExp();
					beanConsMonitorArchExp.setModulo(utilerias.getString(map
							.get("MODULO")));
					beanConsMonitorArchExp.setSubModulo(utilerias.getString(map
							.get("SUBMODULO")));
					beanConsMonitorArchExp.setNomArchivo(utilerias
							.getString(map.get("NOMBRE_ARCHIVO")));
					beanConsMonitorArchExp.setEstatus(utilerias.getString(map
							.get("ESTATUS")));
					beanConsMonitorArchExp.setNumRegistros(utilerias
							.getString(map.get("NUMERO_REGISTROS")));
					beanConsMonitorArchExp.setFechaCarga(utilerias
							.formateaFecha(map.get("FECHA_EXPORTA"),
									Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));

					beanResConsMonitorArchExpDAO.setTotalReg(utilerias
							.getInteger(map.get("CONT")));
					listBeanConsMonitorArchExp.add(beanConsMonitorArchExp);
				}
			}
			beanResConsMonitorArchExpDAO
					.setListBeanConsMonitorArchExp(listBeanConsMonitorArchExp);
			beanResConsMonitorArchExpDAO
					.setCodError(responseDTO.getCodeError());
			beanResConsMonitorArchExpDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsMonitorArchExpDAO.setCodError(Errores.EC00011B);
		}
		return beanResConsMonitorArchExpDAO;
	}

}
