/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMantenimientoTemplates.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 28 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayoutTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsultaTemplatesDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.UtileriasDatosMtoTemplate;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * 
 * Implementacion DAO que actua como firma de la capa de persistencia
 *
 */
//Implementacion DAO que actua como firma de la capa de persistencia
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMantenimientoTemplatesImpl extends Architech implements DAOMantenimientoTemplates {

	//Propiedad del tipo long que almacena el valor de serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 1772605628359389915L;
	
	//Propiedad del tipo string que almacena la consulta:  obtener el total de Templates
	/** Propiedad del tipo string que almacena la consulta:  obtener el total de Templates*/
	private static final String QUERY_GET_TOTAL_TEMPLATES= "Select COUNT(1) as TOTAL FROM TRAN_CAP_TEMPLATE";
	
	//Propiedad del tipo string que almacena la consulta: obtener todos los templates paginados
	/** Propiedad del tipo string que almacena la consulta: obtener todos los templates paginados*/
	private static final String QUERY_GET_ALL_TEMPLATES_INICIO = "Select  F.ID_REG,"
			+ " F.ID_TEMPLATE,F.ID_LAYOUT,F.ESTATUS,F.USR_ALTA,F.USR_AUTORIZA,F.FEC_CAPTURA,F.FEC_MODIFICA  FROM"			
			+" (Select rownum as ID_REG,"
			+" T.ID_TEMPLATE,T.ID_LAYOUT,T.ESTATUS,T.USR_ALTA,T.USR_AUTORIZA,T.FEC_CAPTURA,T.FEC_MODIFICA FROM("
			+" Select ID_TEMPLATE, ID_LAYOUT, ESTATUS,"
			+" USR_ALTA, USR_AUTORIZA, FEC_CAPTURA, FEC_MODIFICA FROM TRAN_CAP_TEMPLATE ";
	
	//Propiedad del tipo string que almacena la consulta: obtener todos los templates paginados
		/** Propiedad del tipo string que almacena la consulta: obtener todos los templates paginados*/
		private static final String QUERY_GET_ALL_TEMPLATES_FIN = " ORDER BY FEC_CAPTURA DESC) T) F"			
				+" WHERE ID_REG BETWEEN ? AND ?";
	
	//Propiedad del tipo string que almacena la consulta: obtener todos los templates con condicion de estado y paginados
	/** Propiedad del tipo string que almacena la consulta: obtener todos los templates con condicion de estado y paginados*/
	private static final String QUERY_COND_ESTATUS ="WHERE TRIM(ESTATUS)= TRIM(?)";
	
	//Propiedad del tipo string complemento: condicionar el estado de los templates
	/** Propiedad del tipo string complemento: condicionar el estado de los templates*/
	private static final String COMP_WESTATUS="  WHERE ESTATUS = ?";
	
	//Propiedad del tipo string que almacena la consulta: Insertar en la tabla de template
	/** Propiedad del tipo string que almacena la consulta: Insertar en la tabla de template*/
	private static final String QUERY_INSERT_TEMPLATE = " INSERT INTO TRAN_CAP_TEMPLATE (ID_TEMPLATE,ID_LAYOUT,ESTATUS,USR_ALTA,USR_AUTORIZA,FEC_CAPTURA, FEC_MODIFICA) " 
					+ "VALUES (UPPER(?), ?, ?, ?, NULL, SYSDATE, NULL)";
	
	//Propiedad del tipo string que almacena la consulta: Verificar que un template no exista en la BD
	/** Propiedad del tipo string que almacena la consulta: Verificar que un template no exista en la BD*/
	private static final String QUERY_DUP_TEMPLATE = "Select COUNT(1) AS TOTAL FROM  TRAN_CAP_TEMPLATE WHERE TRIM(ID_TEMPLATE) = TRIM(?)"; 
	
	//Propiedad del tipo string que almacena la consulta: Insertar los usuarios de un template
	/** Propiedad del tipo string que almacena la consulta: Insertar los usuarios de un template*/
	private static final String QUERY_INSERT_USUARIO = "INSERT INTO TRAN_CAP_USR_TEMPLATE (ID_TEMPLATE, USR_CAPTURA, USR_AUTORIZA) VALUES(?, ?, ?)";
	
	//Propiedad del tipo string que almacena la consulta: Insertar los campos de un template
	/** Propiedad del tipo string que almacena la consulta: Insertar los campos de un template*/
	private static final String QUERY_INSERT_CAMPO = "INSERT INTO TRAN_CAP_DET_TEMPLATE(ID_CAMPO, ID_TEMPLATE, CAMPO, OBLIGATORIO, CONSTANTE) VALUES (?,?,?,?,?)";
	
	//Propiedad del tipo string que almacena la consulta: Eliminar template
	/** Propiedad del tipo string que almacena la consulta: Eliminar template*/
	private static final String QUERY_DEL_TEMPLATE = "Delete FROM TRAN_CAP_TEMPLATE Where TRIM(ID_TEMPLATE) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Eliminar usuarios del template
	/** Propiedad del tipo string que almacena la consulta: Eliminar usuarios del template*/
	private static final String QUERY_DEL_USRSTEMPLATE = "Delete FROM TRAN_CAP_USR_TEMPLATE Where TRIM(ID_TEMPLATE) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Eliminar un usuario especifico del template
	/** Propiedad del tipo string que almacena la consulta: Eliminar un usuario especifico del template*/
	private static final String QUERY_DEL_USERTEMPLATE = "Delete FROM TRAN_CAP_USR_TEMPLATE "
			+ "Where TRIM(ID_TEMPLATE) = TRIM (?) AND TRIM(USR_AUTORIZA) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Obtener campos del template
	/** Propiedad del tipo string que almacena la consulta: Obtener campos del template*/
	private static final String QUERY_DEL_CAMPOSTEMPLATE = "Delete FROM TRAN_CAP_DET_TEMPLATE Where TRIM(ID_TEMPLATE) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Obtener si el usuario ya existe en el template
	/** Propiedad del tipo string que almacena la consulta: Obtener si el usuario ya existe en el template*/
	private static final String QUERY_GET_TOTAL_USUARIOTEMPLATE= "Select COUNT(1) AS TOTAL FROM TRAN_CAP_USR_TEMPLATE  "
			+ "WHERE TRIM(ID_TEMPLATE) = TRIM(?) AND TRIM(USR_AUTORIZA) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Actualizar la fecha de modificacion del template
	/** Propiedad del tipo string que almacena la consulta: Actualizar la fecha de modificacion del template*/
	private static final String QUERY_UPDATE_TEMPLATE = "UPDATE TRAN_CAP_TEMPLATE SET ESTATUS=?, FEC_MODIFICA = SYSDATE WHERE TRIM(ID_TEMPLATE) = TRIM(?)";

	//Query que determina si el usuario es administador o supervisor
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_ELIMINAR_FOLIO
	 */
	private static final String QUERY_ADMIN_SUPERVISOR ="SELECT COUNT(1) CONT FROM TR_SEG_CAT_PERFIL SCP, TR_SEG_CFG_USUARIO_PERFIL SCUP"
			+" WHERE SCP.TXT_PERFIL_SAM IN ('grp_appnal_admin','grp_appnal_super') "
			+"AND SCUP.CVE_PERFIL = SCP.CVE_PERFIL AND TRIM(SCUP.CVE_USUARIO) = ?";
	
	//Constante para referenciar el total de registros
	/** Constante para referenciar el total de registros */
	private static final String TOTAL = "TOTAL";
	
	//Metodo para hacer la consulta de templates
	@Override
	public BeanResConsultaTemplatesDAO consultarTemplates(
			BeanReqConsultaTemplates beanReq, ArchitechSessionBean session) {
		
		//Creacion de objetos de respuesta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String,Object>> listMapRes = new ArrayList<HashMap<String,Object>>();
		List<Object> listParam =new ArrayList<Object>();		
		BeanResConsultaTemplatesDAO beanResDAO = new BeanResConsultaTemplatesDAO();
		
		//Creacion de objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();		
		int numReg=0;
		
		boolean isAdminSupervisor = false;
		//Se revisa si es usuario administrador o supervisor
		BeanResBase beanResAdminSupervisor = consultarAdminOSupervisor(session);
		if(Errores.CODE_SUCCESFULLY.equals(beanResAdminSupervisor.getCodError())){
			isAdminSupervisor = true;
		}
		
    	StringBuilder queryTotalTemplates = new StringBuilder();

		 try{
			//Se revisa si hay filtros para la consulta
			if(beanReq.getFiltroEstado().isEmpty()){
		    	queryTotalTemplates.append(QUERY_GET_TOTAL_TEMPLATES);
				if(!isAdminSupervisor){
					queryTotalTemplates.append(" where TRIM(USR_ALTA) = ");
					queryTotalTemplates.append("'"+session.getUsuario()+"'");
				}
			}else{
		    	queryTotalTemplates.append(QUERY_GET_TOTAL_TEMPLATES + COMP_WESTATUS);
				if(!isAdminSupervisor){
					queryTotalTemplates.append(" and TRIM(USR_ALTA) = ");
					queryTotalTemplates.append("'"+session.getUsuario()+"'");
				}
				//Existen filtros en el requerimiento. AC | PD
				listParam.add(beanReq.getFiltroEstado());
			}
			
			responseDTO = HelperDAO.consultar(queryTotalTemplates.toString(), listParam,
					 HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se valida que haya respuesta
			 if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				 listMapRes = responseDTO.getResultQuery();
				 
				 //Se obtiene el numero total
				 HashMap<String, Object> first = listMapRes.get(0);
				 numReg = utilerias.getInteger(first.get(TOTAL));	
				 beanResDAO.setTotalRegistros(numReg);
				 
				 //Si la respuesta es mayor que cero es exitoso
				if(numReg==0){
					 beanResDAO.setCodError(Errores.ED00011V);
					 beanResDAO.setMsgError(Errores.DESC_ED00011V);
					 return beanResDAO;
				 }
			 }
			
			 //Continua despues de las validaciones
			 continuaConsulta(beanReq, listParam, isAdminSupervisor, session, beanResDAO);
			
		  } catch (ExceptionDataAccess e){
			  //Si existe una excepcion se setea el codigo de error EC00011B
			 showException(e);
			 beanResDAO.setCodError(Errores.EC00011B);
			 beanResDAO.setMsgError(Errores.DESC_EC00011B);
		 }
		 
		return beanResDAO;	
	}
	
	private void continuaConsulta(BeanReqConsultaTemplates beanReq, List<Object> listParam, boolean isAdminSupervisor,
			ArchitechSessionBean session, BeanResConsultaTemplatesDAO beanResDAO){
		
		//Creacion de objetos de apoyo
		ResponseMessageDataBaseDTO responseDTO = null;
		UtileriasDatosMtoTemplate utileriasDatos=UtileriasDatosMtoTemplate.getUtilerias();
		
		//Creacion de objetos de respuesta
		List<BeanTemplate> listaTemplates = new ArrayList<BeanTemplate>();		

		try{
			//Reutiliza la lista de parametros
			if(beanReq.getFiltroEstado().isEmpty()){
				listParam.clear();
			}

			//Preparar el paginador. 
			if(null != beanReq.getPaginador()){
				listParam.add(beanReq.getPaginador().getRegIni());
				listParam.add(beanReq.getPaginador().getRegFin());
			}else{
				//Si no hay pagina se pone 1
				listParam.add(1);
				listParam.add(1);
			}

			//Se agrega al query
			StringBuilder queryTemplates = new StringBuilder();
			queryTemplates.append(QUERY_GET_ALL_TEMPLATES_INICIO);

			//No existe filtro, obtener todos los templates			 
			if(!beanReq.getFiltroEstado().isEmpty()){
				//Si existen filtros se consulta con los mismos
				queryTemplates.append(QUERY_COND_ESTATUS);
				//Se valida supervisor
				if(!isAdminSupervisor){
					queryTemplates.append(" and TRIM(USR_ALTA) = ");
					queryTemplates.append("'"+session.getUsuario()+"'");
				}
			}else{
				//Se valida supervisor
				if(!isAdminSupervisor){
					queryTemplates.append(" where TRIM(USR_ALTA) = ");
					queryTemplates.append("'"+session.getUsuario()+"'");
				}
			}

			queryTemplates.append(QUERY_GET_ALL_TEMPLATES_FIN);

			responseDTO = HelperDAO.consultar(queryTemplates.toString(), 
					listParam,HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Extraer datos de la consulta
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				utileriasDatos.extraerDatosTemplates(responseDTO, listaTemplates);
			}

			//Se setea la lista en el bean de respuesta
			beanResDAO.setListaTemplates(listaTemplates);

			//Establecer codigos proceso			 
			beanResDAO.setCodError(responseDTO.getCodeError());				 
			beanResDAO.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}
	}


	//Metodo para guardar el template
	@Override
	public BeanResBase guardarTemplate(BeanReqAltaEditTemplate beanReq,
			ArchitechSessionBean session) {		
		
		//Se crean objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResBase beanResDAO = new BeanResBase();
		
		//Se crean objetos de respuesta
		List<HashMap<String, Object>> listMapRes = new ArrayList<HashMap<String,Object>>();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		
		String idTemplateMayus =beanReq.getTemplate().getIdTemplate().toUpperCase();
		int numResults=0;
		
		//Convertir a mayusculas el ID_TEMPLATE. Estandar de BD
		beanReq.getTemplate().setIdTemplate(idTemplateMayus);
		
		try{
			//Verificar duplicado de template
			responseDTO = HelperDAO.consultar(QUERY_DUP_TEMPLATE,
					Arrays.asList(new Object[]{beanReq.getTemplate().getIdTemplate()}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se valida que respuesta no sea nulo ni venga vacia
			if(responseDTO.getResultQuery()!=null && !responseDTO.getResultQuery().isEmpty()){
				listMapRes = responseDTO.getResultQuery();
				
				//Se obtiene el num total de registros
				HashMap<String, Object> first = listMapRes.get(0);
				numResults = utilerias.getInteger(first.get(TOTAL));
				
				//Si es mayor que cero es una respuesta valida
				if(numResults>0){
					beanResDAO.setCodError(Errores.ED00061V);
					return beanResDAO;
				}
			}
			
			//Obtener y asociar datos a los objetos involucrados. 
			utileriasDatos.ajustarDatosTemplate(beanReq, true, session);
			utileriasDatos.ajustarDatosUsuariosAut(beanReq, session);
			utileriasDatos.ajustarDatosCampos(beanReq);
			
			//Insertar el registro. TRAN_CAP_TEMPLATE
			responseDTO = HelperDAO.insertar(QUERY_INSERT_TEMPLATE, 
					Arrays.asList(new Object[]{
							beanReq.getTemplate().getIdTemplate(),
							beanReq.getTemplate().getIdLayout(),
							beanReq.getTemplate().getEstatus(),
							beanReq.getTemplate().getUsrAlta()
							}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se setean codigo y mensaje de error
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
			
			//Insertar los registros. TRAN_CAP_USR_TEMPLATE. 
			for(BeanUsuarioAutorizaTemplate usuario:beanReq.getTemplate().getUsuariosAutorizan()){
				responseDTO = HelperDAO.insertar(QUERY_INSERT_USUARIO, 
						Arrays.asList(new Object[]{
								usuario.getIdTemplate(),								
								usuario.getUsrCaptura(),
								usuario.getUsrAutoriza()
								}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());

				//Se setean codigo y mensaje de error
				beanResDAO.setCodError(responseDTO.getCodeError());
				beanResDAO.setMsgError(responseDTO.getMessageError());
				
			}
			
			//Insertar los registros. TRAN_CAP_DET_TEMPLATE. 
			for(BeanCampoLayoutTemplate campo: beanReq.getTemplate().getCampos()){
				responseDTO = HelperDAO.insertar(QUERY_INSERT_CAMPO, 
						Arrays.asList(new Object[]{
								campo.getIdCampo(),								
								campo.getIdTemplate(),
								campo.getCampo(),
								campo.getObligatorio(),
								campo.getConstante()
								}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				
				//Se setean codigo y mensaje de error
				beanResDAO.setCodError(responseDTO.getCodeError());
				beanResDAO.setMsgError(responseDTO.getMessageError());
			}
						
		}catch(ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}
		//Se retorna el bean
		return beanResDAO;
	}

	//Metodo para eliminar el template
	@Override
	public BeanResBase eliminarTemplate(BeanTemplate template, ArchitechSessionBean session) {
		ResponseMessageDataBaseDTO responseDTO = null;		
		BeanResBase beanResDAO = new BeanResBase();
		
		try{
			//Eliminar los campos del template
			responseDTO = HelperDAO.eliminar(QUERY_DEL_CAMPOSTEMPLATE,
					Arrays.asList(new Object[] { template.getIdTemplate()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
			
			//Eliminar los usuarios del template
			responseDTO = HelperDAO.eliminar(QUERY_DEL_USRSTEMPLATE,
					Arrays.asList(new Object[] { template.getIdTemplate()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
			
			//Eliminar el template
			responseDTO = HelperDAO.eliminar(QUERY_DEL_TEMPLATE,
					Arrays.asList(new Object[] { template.getIdTemplate()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se setean codigo y mensaje de error
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}
		//Se retorna el bean
		return beanResDAO;
	}
	
	//Metodo para actualizar fecha de template
	@Override
	public BeanResBase actualizarFechaTemplate(BeanReqAltaEditTemplate beanReq,
			ArchitechSessionBean session) {
		
		//Se crean objetos de soporte
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResBase beanResDAO = new BeanResBase();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		
		//A. layout, usrCaptura, estatus => BEANREQ  template
		utileriasDatos.ajustarDatosTemplate(beanReq, false, session);
		
		try{
			//Consultar si el usuario existe en la BD
			responseDTO = HelperDAO.actualizar(QUERY_UPDATE_TEMPLATE,
					Arrays.asList(new Object[] { beanReq.getTemplate().getEstatus(), beanReq.getTemplate().getIdTemplate()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se setean codigo y mensaje de error
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
			
		} catch (ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			 showException(e);
			 beanResDAO.setCodError(Errores.EC00011B);
			 beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}
		//Se retorna el bean
		 return beanResDAO;
	}

	//Metodo para verificar la existencia de un usuario
	@Override
	public BeanResBase verificarExistenciaUsuario(BeanUsuarioAutorizaTemplate beanUsuario,
			ArchitechSessionBean session) {
		
		//Se crean objetos de soporte
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = null;
		
		//Se crea objetos de respuesta
		BeanResBase beanResDAO = new BeanResBase();
		
		Utilerias utilerias = Utilerias.getUtilerias();
		int numResults=0;
				
		try{
			//Se hace la consulta a la base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_TOTAL_USUARIOTEMPLATE,
					Arrays.asList(new Object[] { beanUsuario.getIdTemplate(), beanUsuario.getUsrAutoriza()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Se obtiene el resultado
			listMapRes = responseDTO.getResultQuery();
			if(listMapRes != null && !listMapRes.isEmpty()){
				HashMap<String, Object> first = listMapRes.get(0);
				numResults = utilerias.getInteger(first.get(TOTAL));
			}	
			
			//Se valida que haya mas de un resultado
			if(numResults>0){
				beanResDAO.setCodError(Errores.CMMT005V);
				return beanResDAO;
			}	
					
			//Se setean cod y mensaje de error
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}		
		//Se retorna el bean
		return beanResDAO;
	}
	
	//Metodo para actualizar la lista de usuarios
	@Override
	public BeanResBase actualizarUsuarios(BeanReqAltaEditTemplate beanReq,
			ArchitechSessionBean session) {		
		//INFO. Lista usuarios => usuarios a eliminar, 
		//template.usuariosAutoriza => Usuarios a crear
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResBase beanResDAO = new BeanResBase();
		String idTemplate = beanReq.getTemplate().getIdTemplate();
		
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();		
		utileriasDatos.ajustarDatosUsuariosAut(beanReq, session);
		
		try{
			
			//Insertar los nuevos registros en. TRAN_CAP_USR_TEMPLATE
			for(BeanUsuarioAutorizaTemplate usuario:beanReq.getTemplate().getUsuariosAutorizan()){
				responseDTO = HelperDAO.insertar(QUERY_INSERT_USUARIO, 
						Arrays.asList(new Object[]{
								usuario.getIdTemplate(),								
								usuario.getUsrCaptura(),
								usuario.getUsrAutoriza()
								}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
				//Se setean cod y mensaje de error
				beanResDAO.setCodError(responseDTO.getCodeError());
				beanResDAO.setMsgError(responseDTO.getMessageError());
			}
			
			//Eliminar los usuarios obsoletos
			for(String usuario: beanReq.getListaUsuarios()){
				responseDTO = HelperDAO.eliminar(QUERY_DEL_USERTEMPLATE, 
						Arrays.asList(new Object[]{
								idTemplate,
								usuario
								}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
				//Se setean cod y mensaje de error
				beanResDAO.setCodError(responseDTO.getCodeError());
				beanResDAO.setMsgError(responseDTO.getMessageError());
			}
		
								
		}catch(ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}
		//Se retorna el bean
		return beanResDAO;
	}
	
	/**
	 * Consultar admin o supervisor.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res base
	 */
	//Funcion que consulta si el usuario es adminsitrador o supervisor
	private BeanResBase consultarAdminOSupervisor(ArchitechSessionBean sessionBean){

		//Se crean los objetos necesarios para ejecutar el query
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		//Se crea bean de respuesta
		BeanResBase res=new BeanResBase();
		List<Object> parametros = new ArrayList<Object>();
		try{
			//Consulta a la base de datos
			parametros.add(sessionBean.getUsuario());
			String query=QUERY_ADMIN_SUPERVISOR;
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());

			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
			}
		} catch (ExceptionDataAccess e) {
			//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
			res.setMsgError(Errores.DESC_EC00011B);
			res.setCodError(Errores.EC00011B);
		}

		//Se valida que la lista no sea nula ni venga vacia
		if(list != null && !list.isEmpty()){
			HashMap<String,Object> map = list.get(0);
			Integer num=utilerias.getInteger(map.get("CONT"));
			if(num > 0){
				res.setCodError(Errores.CODE_SUCCESFULLY);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}
		}
		//Se devuelve el bean
		return res;
	}
}