package mx.isban.eTransferNal.dao.contingencia;



import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.contingencia.BeanArchivoDuplicado;
import mx.isban.eTransferNal.beans.contingencia.BeanCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanConsultaOperContingentes;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConFecha;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanalesD;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 * The Class DAOConsultaOperContingentesImpl.
 *
 * @author FSW-Vector
 * @since 25/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaOperContingentesImpl extends Architech implements DAOConsultaOperContingentes{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 945194699544669184L;
	
	/** La constante TABLA_NORMAL. */
	private static  String TABLA_NORMAL= "TRAN_ARCH_CANALES";
	
	/** La constante TABLA_HISTORICA. */
	private static  String TABLA_HISTORICA= "TRAN_ARCH_CANALES_HIS";
	
	/** La variable que contiene informacion con respecto a: query consulta archivos. */
	private static String QUERY_CONSULTA_ARCHIVOS="SELECT NOMBRE_ARCH,ESTATUS,NUM_PAGOS,NUM_PAGOS_ERR,TOTAL_MONTO,TOTAL_MONTO_ERR," + 
			"CVE_USUARIO_ALTA,MENSAJE,CVE_MEDIO_ENT,DESCRIPCION,TO_CHAR(FCH_CAPTURA,'dd/MM/yyyy')FCH_CAPTURA," + 
			"TO_CHAR(FCH_PROCESO,'dd/MM/yyyy')FCH_PROCESO FROM TRAN_ARCH_CANALES ";
	
	/** La variable que contiene informacion con respecto a: filtro historico. */
	private static String FILTRO_HISTORICO =" WHERE FCH_CAPTURA = TO_DATE(?,'dd/MM/yyyy')";
	
	/** La constante QUERY_CONSULTA_CANAL. */
	private static  String QUERY_CONSULTA_CANAL = "SELECT tcc.cve_medio_ent CVE, tcc.activo ACT, cme.descripcion DES "+
	   " FROM TRAN_CONTIG_CANALES  tcc, COMU_MEDIOS_ENT cme "+
	   " WHERE  TCC.CVE_MEDIO_ENT = CME.CVE_MEDIO_ENT AND TCC.ACTIVO = 1";
	
	
	/** La constante QUERY_ACTUALIZAR_NOMBRE. */
	private static  String QUERY_ACTUALIZAR_NOMBRE = "UPDATE TRAN_ARCH_CANALES SET  ESTATUS = ?, FCH_CAPTURA = TO_DATE(?,'DD-MM-YYYY'), CVE_USUARIO_ALTA = ? " +
		"WHERE TRIM(NOMBRE_ARCH) = ? ";
	
	
	/** La constante QUERY_FECHA_OPERACION. */
	private static  String QUERY_FECHA_OPERACION =	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX  WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	/** La constante QUERY_CONSULTA_DUPLICIDAD. */
	private static  String QUERY_CONSULTA_DUPLICIDAD = "SELECT * FROM TRAN_ARCH_CANALES WHERE TRIM(NOMBRE_ARCH) = ? ";
	
		
	/** La constante QUERY_UPDATE_NOMBRA_PROC. */
	private static  String QUERY_UPDATE_NOMBRA_PROC = "update TRAN_ARCH_CANALES SET ESTATUS = '2', AUTORIZA='1'  WHERE NOMBRE_ARCH LIKE ?";
						
	
	/** La constante QUERY_INSERT_NOMBRA_PROC. */
	private static  String QUERY_INSERT_NOMBRA_PROC = 
		"INSERT INTO TRAN_ARCH_CANALES (NOMBRE_ARCH, ESTATUS, CVE_USUARIO_ALTA, FCH_CAPTURA, AUTORIZA, CVE_MEDIO_ENT,DESCRIPCION) VALUES (?,?,?,TO_DATE(?,'DD-MM-YYYY'),'0',?,?)";

	/** buscar descripcion del archivo *. */
	private static  String QUERY_BUSCAR_DESCRIPCION="SELECT descripcion FROM comu_medios_ent WHERE trim(CVE_MEDIO_ENT)=?";
	
	/** variable nombre archivo *. */
	private static  String NOMBRE_ARCHIVO="CONTIG_CANALES_";
	
	/**
	 * Consulta oper contingentes.
	 *
	 * @param arquite El objeto: arquite
	 * @param beanPaginador El objeto: bean paginador
	 * @param bean El objeto: bean
	 * @return Objeto bean res consulta oper contingente
	 */
	@Override
	public BeanResConsultaOperContingente consultaOperContingentes(ArchitechSessionBean arquite,BeanPaginador beanPaginador,BeanReqConsultaOperContingente bean) {
		/**se inicializan los objetos**/
		BeanResConsultaOperContingente beanResArchRespuesta = new BeanResConsultaOperContingente();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		
		BeanResBase beanBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();
		List<BeanConsultaOperContingentes> listBean = new ArrayList<BeanConsultaOperContingentes>();
		/**se asigna el query a la tabla normal**/
		String query = QUERY_CONSULTA_ARCHIVOS;
		
		/**Se pregunta si viene historica para realizar la busqueda en la tabla his **/
		if(bean.isHistorico()) {
			/**se remplaza la tabla y se agrega el filtro con fecha captura **/
			query = query.replace(TABLA_NORMAL,TABLA_HISTORICA) + FILTRO_HISTORICO;
			/**se asigna el parametro**/
			listaParametros.add(bean.getFechaOperacion());
		}
		try {
			/**se realiza la consulta**/
			responseDTO = HelperDAO.consultar(query,listaParametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			/**Se pregunta si vienen datos **/
			if (responseDTO.getResultQuery() != null&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				/**se asigna lista de datos**/
				listBean= recorrerLista(list);
				/**se regresa los codigos de exito**/
				beanResArchRespuesta.setListaBeanResConsOper(listBean);
				beanBase.setCodError(Errores.OK00000V);
				beanResArchRespuesta.setBeanbase(beanBase);
			}else {
				/**Se manda el mensaje de no hay informacion**/
				beanBase.setCodError(Errores.ED00011V);
				beanBase.setTipoError(Errores.TIPO_MSJ_INFO);
				beanBase.setMsgError(Errores.DESC_ED00011V);
				beanResArchRespuesta.setBeanbase(beanBase);
			}
		} catch (ExceptionDataAccess e) {
			/**si hay un problema se regresa el codigo de error**/
			showException(e);
			beanBase.setCodError(Errores.EC00011B);
			beanResArchRespuesta.setBeanbase(beanBase);
		}
		
		/**se regresa el objeto**/
		return beanResArchRespuesta;
	}
	
	
	/**
	 * Recorrer lista.
	 *
	 * @param list El objeto: list
	 * @return Objeto list
	 */
	private List<BeanConsultaOperContingentes> recorrerLista(List<HashMap<String, Object>> list){
		List<BeanConsultaOperContingentes> listBean = new ArrayList<BeanConsultaOperContingentes>();
		/**se implementa el objeto utilerias**/
		Utilerias utilerias = Utilerias.getUtilerias();
		/**se recorre la lista y se asigna a un objeto**/
		for (HashMap<String, Object> map : list) {
			BeanConsultaOperContingentes beanSalida = new BeanConsultaOperContingentes();
			beanSalida.setNombreArchivo(utilerias.getString(map.get("NOMBRE_ARCH")));
			beanSalida.setEstatus(utilerias.getString(map.get("ESTATUS")));
			beanSalida.setNumPagos(utilerias.getString(map.get("NUM_PAGOS")));
			beanSalida.setNumPagosErr(utilerias.getString(map.get("NUM_PAGOS_ERR")));
			beanSalida.setTotalMonto(utilerias.getString(map.get("TOTAL_MONTO")));
			beanSalida.setTotalMontoErr(utilerias.getString(map.get("TOTAL_MONTO_ERR")));
			beanSalida.setCveUsuarioAlta(utilerias.getString(map.get("CVE_USUARIO_ALTA")));
			beanSalida.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
			beanSalida.setFchProceso(utilerias.getString(map.get("FCH_PROCESO")));
			beanSalida.setMensaje(utilerias.getString(map.get("MENSAJE")));		
			/**en caso de que se insertaran en null obtiene los datos**/
			if(!utilerias.getString(map.get("NOMBRE_ARCH")).isEmpty()) {
				beanSalida.setCveMedioEnt(obtenerCVe(utilerias.getString(map.get("NOMBRE_ARCH"))));
				beanSalida.setDescripcion(obtenerDescripcion(utilerias.getString(map.get("NOMBRE_ARCH"))));
			}else {
				beanSalida.setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")));
				beanSalida.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
			}
			/**se asginan a la lista**/
			listBean.add(beanSalida);					
		}
		return listBean;
	}
	
	/**
	 * Consultar canal.
	 *
	 * @param session El objeto: session
	 * @return Objeto bean res cons canales D
	 */
	@Override
	public BeanResConsCanalesD consultarCanal(ArchitechSessionBean session) {
		/**se inicializan los objetos**/
		ResponseMessageDataBaseDTO responseDTO = null;	
		BeanResConsCanalesD beanResConsCanalesDAO = new BeanResConsCanalesD();
		List<Object> listaParametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		List<BeanCanales> listBean = new ArrayList<BeanCanales>();
		
		try {
			/**Inicia peticion a consulta.*/
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CANAL,
					listaParametros, HelperDAO.CANAL_GFI_DS, this.getClass()
							.getName());
			
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				/**se recorre el la lista y se asgina al aobjeto**/
				for (HashMap<String, Object> resultMap : list) {
					BeanCanales beanResCanal = new BeanCanales();
					beanResCanal.setIdCanal(resultMap.get("CVE")
							.toString().trim());
					beanResCanal.setNombreCanal(resultMap.get("DES")
							.toString().trim());				
					listBean.add(beanResCanal);
				}
				/**se asigna el codigo de exito**/
				beanResConsCanalesDAO.setBeanResCanalList(listBean);
				beanResConsCanalesDAO.setCodError(Errores.OK00000V);	
			}
		} catch (ExceptionDataAccess e) {
			/**se asigna el codigo de error**/
			showException(e);
			beanResConsCanalesDAO.setCodError(Errores.EC00011B);
		}

		
		return beanResConsCanalesDAO;

	}

	/**
	 * Consulta fecha operacion.
	 *
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res con fecha
	 */
   	public BeanResConFecha consultaFechaOperacion(ArchitechSessionBean architechSessionBean){
   		/**se inicializan los objetos**/
	      final BeanResConFecha beanResConsFechaOpDAO = new BeanResConFecha();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      try {
	    	  /**Inicia peticion a consulta.*/
	    	responseDTO = HelperDAO.consultar(QUERY_FECHA_OPERACION, Collections.emptyList(), 
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				/**se recorre el resultado de la consulta**/
				for(HashMap<String,Object> map:list){
					/**se asigna al objeto fecha**/
					beanResConsFechaOpDAO.setFechaOperacion(
							utilerias.formateaFecha(map.get("FCH_OPERACION"), 
									Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
					/**se asigna elcodigo de exito**/
					beanResConsFechaOpDAO.setCodError(responseDTO.getCodeError());
					beanResConsFechaOpDAO.setMsgError(responseDTO.getMessageError());
	  		  }
			}
		} catch (ExceptionDataAccess e) {
			/**se asigna codigo de error si ocurre algo **/
	        showException(e);
	        beanResConsFechaOpDAO.setCodError(Errores.EC00011B);
		}
	      return beanResConsFechaOpDAO;
	   }

   	/**
	  * Valida archivo dupl.
	  *
	  * @param beanArchivo El objeto: bean archivo
	  * @param session El objeto: session
	  * @param bean El objeto: bean
	  * @return Objeto bean archivo duplicado
	  */
	@Override
	public BeanArchivoDuplicado validaArchivoDupl(BeanConsultaOperContingentes beanArchivo,
			ArchitechSessionBean session, BeanReqConsultaOperContingente bean) {
		/**se inicializan los objetos**/
		BeanArchivoDuplicado beanResDuplicadoArchDAO = new BeanArchivoDuplicado();
		beanResDuplicadoArchDAO.setEsDuplicado(Boolean.FALSE);
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(beanArchivo.getNombreArchivo());
		/**se asigna la consulta a la tabla normal**/
		String query = QUERY_CONSULTA_DUPLICIDAD;
		/**se pregunta si viene de la pantalla historica**/
		if(bean.isHistorico()) {
			/**reemplaza la tabla normal por la historica en la consutla**/
			query = query.replace(TABLA_NORMAL, TABLA_HISTORICA);
		}
				
				
		try {	
			/**Inicia peticion a consulta.*/
			responseDTO = HelperDAO.consultar(query,
					parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**se asigna el resultado**/
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				beanResDuplicadoArchDAO.setEsDuplicado(Boolean.TRUE);
			}
			/**se asigna codigo de exito **/
			beanResDuplicadoArchDAO.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**se asigna codigo de error**/
			showException(e);
			beanResDuplicadoArchDAO.setCodError(Errores.EC00011B);
		}

		return beanResDuplicadoArchDAO;
	}

	/**
	 * Registra archivo.
	 *
	 * @param beanArchivo El objeto: bean archivo
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res base
	 */
	@Override
	public BeanResBase registraArchivo(BeanConsultaOperContingentes beanArchivo,
			ArchitechSessionBean session,BeanReqConsultaOperContingente bean) {
		/**se inicializan los objetos**/
		BeanResBase beanResBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();
		listaParametros.add(beanArchivo.getNombreArchivo());
		listaParametros.add(0);
		listaParametros.add(session.getUsuario());
		listaParametros.add(beanArchivo.getFchOperacion());
		listaParametros.add(obtenerCVe(beanArchivo.getNombreArchivo()));
		listaParametros.add(obtenerDescripcion(beanArchivo.getNombreArchivo()));
		/**se llama a la consulta en la tabla normal**/
		String query = QUERY_INSERT_NOMBRA_PROC;
		/**se pregunta de que pantalla viene**/
		if(bean.isHistorico()) {
			/**se reemplaza la tabla normal a historica**/
			query = query.replace(TABLA_NORMAL, TABLA_HISTORICA);
		}
		
		try {
			/**se ejecuta la insertción del dato**/
			HelperDAO.insertar(query, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**asignacion del codigo de exito**/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**asignacion del codigo de error**/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase;
	}
	
	/**
	 * Obtener C ve.
	 *
	 * @author FSW-Vector
	 * @param nombreArchivo El objeto: nombre archivo
	 * @return Objeto string
	 * @since 07/11/2018
	 */
	private String obtenerCVe(String nombreArchivo) {
		/**se inicializa**/
		String cve="";
		/**se reemplaza el texto**/
		nombreArchivo = nombreArchivo.replace(NOMBRE_ARCHIVO, "");
		/**se obtiene la clave**/
		cve = nombreArchivo.substring(0, 4);
		return cve;
		
	}
	
	
	/**
	 * Obtener descripcion.
	 *
	 * @param nombreArchivo El objeto: nombre archivo
	 * @return Objeto string
	 */
	private String obtenerDescripcion(String nombreArchivo) {
		/**se inicializa**/
		String descripcion="";
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(obtenerCVe(nombreArchivo).trim());
		List<HashMap<String,Object>> list = null;
		/**se implementa el objeto utilerias**/
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
	    	  /**Inicia peticion a consulta.*/
	    	responseDTO = HelperDAO.consultar(QUERY_BUSCAR_DESCRIPCION, parametros, 
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				/**se recorre el resultado de la consulta**/
				for(HashMap<String,Object> map:list){
					descripcion = utilerias.getString(map.get("DESCRIPCION"));
	  		  }
			}
		} catch (ExceptionDataAccess e) {
			/**se asigna codigo de error si ocurre algo **/
	        showException(e);	       
		}
		
		return descripcion;
		
	}
	
	/**
	 * Actualiza estatus.
	 *
	 * @param beanArchivo El objeto: bean archivo
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res base
	 */
	@Override
	public BeanResBase actualizaEstatus(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session,BeanReqConsultaOperContingente bean) {
		/**se inicializan los objetos**/
		BeanResBase beanResBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();
		StringBuilder archivo = new StringBuilder();
		archivo.append("%"+beanArchivo.getNombreArchivo()+"%");
		listaParametros.add(archivo.toString());
		/**se realiza la consulta a la tabla normal**/		
		String query = QUERY_UPDATE_NOMBRA_PROC;
		/**se pregunta si viene de la pantalla historica**/
		if(bean.isHistorico()) {
			/**reemplaza la tabla normal por la historica **/
			query = query.replace(TABLA_NORMAL, TABLA_HISTORICA);
		}
		
		try {
			/**Inicia peticion a consulta.*/
			HelperDAO.actualizar(query, listaParametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**asigna el codigo de exito **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**asigna el codigo de error**/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase;
		
	}
	
	/**
     * Actualiza nombre.
     *
     * @param beanArchivo El objeto: bean archivo
     * @param session El objeto: session
     * @return Objeto bean res base
     */
	@Override
	public BeanResBase actualizaNombre(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session) {
		/**se inicializan los objetos**/
		BeanResBase beanResBase = new BeanResBase();
		/**inicializa los parametros **/
		List<Object> listaParametros = new ArrayList<Object>();
		/**asigna los parametros **/
		listaParametros.add(0);
		listaParametros.add(beanArchivo.getFchOperacion());
		listaParametros.add(session.getUsuario());
		listaParametros.add(beanArchivo.getNombreArchivo());
		try {
			/**Inicia peticion a consulta.*/
			HelperDAO.actualizar(QUERY_ACTUALIZAR_NOMBRE, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**asignacion del codigo de error**/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**asignacion del codigo de exito**/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase;
	}
	




}
