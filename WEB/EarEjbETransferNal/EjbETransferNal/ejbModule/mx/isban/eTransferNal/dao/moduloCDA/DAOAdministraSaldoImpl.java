/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOAdministraSaldoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la administracion de saldo
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOAdministraSaldoImpl extends Architech implements DAOAdministraSaldo {
	 /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -8630983599342540879L;
	
	/** Constante privada del tipo String que almacena el query para 
	 * el saldo principal y el saldo no reservado*/
	private static final String QUERY_S_PRINCIPAL = " SELECT to_char(SALDO,'999,999,999,999,990.00') SALDO"+
													"   FROM TRAN_SPEI_SALDO "+
													"  WHERE CVE_MI_INSTITUC = "+
													"   (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX " +
													"      WHERE CV_PARAMETRO = 'ENTIDAD_SPEI')";
	
	/** Constante privada del tipo String que almacena el query para 
	 * el saldo alterno*/
	private static final String QUERY_S_ALTERNO =   " SELECT to_char(SALDO,'999,999,999,999,990.00') SALDO "+
													"   FROM TRAN_SPEI_SALDO "+
													"  WHERE CVE_MI_INSTITUC = "+
													"   (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX " +
													"      WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')";

	
	/** Constante privada del tipo String que almacena el query para 
	 * el saldo calculado*/
	private static final String QUERY_S_CALCULADO = 
		" SELECT to_char(( "+
		"        NVL(SALDO_INICIAL,0) + "+
		"        NVL((SELECT SUM(TS.MONTO) FROM TRAN_TRSIAC_SPEI TS, TRAN_SPEI_CTRL SC"+
		"        WHERE TS.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC"+
		"        AND TS.CVE_MI_INSTITUC =(SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')"+
		"        AND TS.FCH_OPERACION = SC.FCH_OPERACION"+
		"        ),0) +"+
		"        NVL((SELECT SUM(SR.MONTO) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC"+
		"        WHERE SR.CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')"+
		"        AND SR.FCH_OPERACION     = SC.FCH_OPERACION"+
		"        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC"+
		"        AND ((SR.ESTATUS_TRANSFER  = 'PA'"+
		"        AND SR.ESTATUS_BANXICO  = 'LQ')"+
		"        OR (SR.ESTATUS_TRANSFER='RE' AND SR.ESTATUS_BANXICO='LQ'"+
		"        AND SR.MOTIVO_DEVOL IS NULL))"+
		"        ),0) +"+
		"        NVL((SELECT SUM(SR.MONTO) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC"+
		"        WHERE SR.CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')"+
		"        AND SR.FCH_OPERACION     = SC.FCH_OPERACION"+
		"        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC"+
		"        AND SR.ESTATUS_TRANSFER  = 'TR'"+
		"        AND SR.ESTATUS_BANXICO  = 'LQ'"+
		"        ),0) + "+
		"        NVL((SELECT SUM(SR.MONTO) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC"+
		"        where SR.cve_mi_instituc = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')"+
		"        AND SR.FCH_OPERACION     = SC.FCH_OPERACION"+
		"        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC"+
		"        AND SR.ESTATUS_TRANSFER  = 'RE'"+
		"        AND SR.ESTATUS_BANXICO  = 'LQ'"+
		"        AND SR.MOTIVO_DEVOL IS NOT NULL"+
		"        ),0) +"+
		"        NVL((select sum(importe_abono) from tran_mensaje"+
		"        WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA'))"+
		"        AND ESTATUS IN('PV','LI','EN','PR')"+
		"        ),0)"+
		" ) - "+
		"("+
		"        NVL((SELECT SUM(IMPORTE_ABONO) FROM TRAN_MENSAJE"+
		"        WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA'))"+
		"        and estatus = 'CO'),0) +"+
		"        NVL((SELECT SUM(IMPORTE_ABONO) FROM TRAN_MENSAJE"+
		"        WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPEI FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA'))"+
		"        and estatus = 'CO'),0)"+
		" ),'999,999,999,999,990.00') "+
		" AS SALDO_CALCULADO"+
		" FROM TRAN_SPEI_SALDO "+
		" WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')";
	
	/** La constante QUERY_COUNT_CALCULADO. */
	private static final String QUERY_COUNT_CALCULADO = " SELECT (COUNT(1) + "+
			"	        NVL((SELECT COUNT(1) FROM TRAN_TRSIAC_SPEI TS, TRAN_SPEI_CTRL SC "+
			"	        WHERE TS.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC "+
			"	        AND TS.CVE_MI_INSTITUC =(SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA') "+
			"	        AND TS.FCH_OPERACION = SC.FCH_OPERACION "+
			"	        ),0) + "+
			"	        NVL((SELECT COUNT(1) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC "+
			"	        WHERE SR.CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA') "+
			"	        AND SR.FCH_OPERACION     = SC.FCH_OPERACION "+
			"	        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC "+
			"	        AND ((SR.ESTATUS_TRANSFER  = 'PA' "+
			"	        AND SR.ESTATUS_BANXICO  = 'LQ') "+
			"	        OR (SR.ESTATUS_TRANSFER='RE' AND SR.ESTATUS_BANXICO='LQ' "+
			"	        AND SR.MOTIVO_DEVOL IS NULL)) "+
			"	        ),0) + "+
			"	        NVL((SELECT COUNT(1) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC "+
			"	        WHERE SR.CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA') "+
			"	        AND SR.FCH_OPERACION     = SC.FCH_OPERACION "+
			"	        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC "+
			"	        AND SR.ESTATUS_TRANSFER  = 'TR' "+
			"	        AND SR.ESTATUS_BANXICO  = 'LQ' "+
			"	        ),0) +  "+
			"	        NVL((SELECT COUNT(1) FROM TRAN_SPEI_REC SR, TRAN_SPEI_CTRL SC "+
			"	        where SR.cve_mi_instituc = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA') "+
			"	        AND SR.FCH_OPERACION     = SC.FCH_OPERACION "+
			"	        AND SR.CVE_MI_INSTITUC = SC.CVE_MI_INSTITUC "+
			"	        AND SR.ESTATUS_TRANSFER  = 'RE' "+
			"	        AND SR.ESTATUS_BANXICO  = 'LQ' "+
			"	        AND SR.MOTIVO_DEVOL IS NOT NULL "+
			"	        ),0) + "+
			"	        NVL((SELECT COUNT(1) from tran_mensaje "+
			"	        WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')) "+
			"	        AND ESTATUS IN('PV','LI','EN','PR') "+
			"	        ),0)) - ( "+
			"	        (SELECT COUNT(1) FROM TRAN_MENSAJE "+
			"	        WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')) "+
			"	        and estatus = 'CO') + "+
			"	        (SELECT COUNT(1) FROM TRAN_MENSAJE "+
			"	        WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPEI FROM TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA')) "+
			"	        and estatus = 'CO')) "+
			"	 AS TOTAL "+
			"	 FROM TRAN_SPEI_SALDO  "+
			"	 WHERE CVE_MI_INSTITUC = (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPEI_CA') "+
		    " GROUP BY 1 ";
	
	/** La constante QUERY_S_DEV. */
	private static final String QUERY_S_DEV = "SELECT count(1) TOTAL, TO_CHAR(nvl(sum(monto),0), '999,999,999,999,990.00') SALDO " +
			"FROM tran_spei_Rec " +
			"WHERE cve_mi_instituc = " +
			"(SELECT cv_valor FROM tran_contig_unix WHERE cv_parametro = 'ENTIDAD_SPEI_CA') " +
			"		AND tipo_pago        = 1 AND estatus_transfer = 'DV'AND fch_operacion    = " +
			"		(SELECT fch_operacion " +
			"				FROM tran_spei_ctrl " +
			"				WHERE cve_mi_instituc = " +
			"				(SELECT cv_valor FROM tran_contig_unix WHERE cv_parametro = 'ENTIDAD_SPEI_CA')) ";
	
	/**Constante con la cadena descError*/
    private static final String CEROS = "0.00";
    
    /** La constante CERO. */
    private static final String CERO = "0";
    
    /** La constante UNO. */
    private static final String UNO = "1";
	

	 /**Metodo que sirve para consultar los saldos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	   */
	public BeanAdministraSaldoDAO obtenerSaldoPrincipal(ArchitechSessionBean architechSessionBean){
		 Utilerias utilerias = Utilerias.getUtilerias();
		 BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
	     List<HashMap<String,Object>> list = null;
	     ResponseMessageDataBaseDTO responseDTO = null;
	     try {
		    	responseDTO = HelperDAO.consultar(QUERY_S_PRINCIPAL, Collections.emptyList(), 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){
						beanAdministraSaldoDAO.setSaldoPrincipal(utilerias.getString(map.get("SALDO")).trim());
						beanAdministraSaldoDAO.setSaldoPrincipalCount(UNO);
		  		  	}
				}else{
					beanAdministraSaldoDAO.setSaldoPrincipalCount(CERO);
					beanAdministraSaldoDAO.setSaldoPrincipal(CEROS);
				}
				beanAdministraSaldoDAO.setCodError(responseDTO.getCodeError());
				beanAdministraSaldoDAO.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			showException(e);
			beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
		}
			return  beanAdministraSaldoDAO;
	}

	/**Metodo que sirve para consultar los saldos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	   */
	public BeanAdministraSaldoDAO obtenerSaldoAlterno(ArchitechSessionBean architechSessionBean){
		 Utilerias utilerias = Utilerias.getUtilerias();
		 BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
	     List<HashMap<String,Object>> list = null;
	     ResponseMessageDataBaseDTO responseDTO = null;
	     try {
		    	responseDTO = HelperDAO.consultar(QUERY_S_ALTERNO, Collections.emptyList(), 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){
						beanAdministraSaldoDAO.setSaldoAlterno(utilerias.getString(map.get("SALDO")).trim());
						beanAdministraSaldoDAO.setSaldoAlternoCount(UNO);
		  		  	}
				}else{
					beanAdministraSaldoDAO.setSaldoAlternoCount(CERO);
					beanAdministraSaldoDAO.setSaldoAlterno(CEROS);
				}
				beanAdministraSaldoDAO.setCodError(responseDTO.getCodeError());
				beanAdministraSaldoDAO.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			showException(e);
			beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
		}
			return  beanAdministraSaldoDAO;
	}
	
	/**Metodo que sirve para consultar los saldos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	   */
	public BeanAdministraSaldoDAO obtenerSaldoCalculado(ArchitechSessionBean architechSessionBean){
		Utilerias utilerias = Utilerias.getUtilerias();
		 BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
	     List<HashMap<String,Object>> list = null;
	     ResponseMessageDataBaseDTO responseDTO = null;
	     try {
		    	responseDTO = HelperDAO.consultar(QUERY_S_CALCULADO, Collections.emptyList(), 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){
						beanAdministraSaldoDAO.setSaldoCalculado(utilerias.getString(map.get("SALDO_CALCULADO")).trim());
		  		  	}
				}else{
					beanAdministraSaldoDAO.setSaldoCalculado(CEROS);
				}
				responseDTO = HelperDAO.consultar(QUERY_COUNT_CALCULADO, Collections.emptyList(), 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){
						beanAdministraSaldoDAO.setSaldoCalculadoCount(utilerias.getString(map.get("TOTAL")).trim());
		  		  	}
				}else{
					beanAdministraSaldoDAO.setSaldoCalculadoCount(CERO);
				}
				beanAdministraSaldoDAO.setCodError(responseDTO.getCodeError());
				beanAdministraSaldoDAO.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			showException(e);
			beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
		}
			return  beanAdministraSaldoDAO;
	}

	@Override
	public BeanAdministraSaldoDAO obtenerSaldoDevoluciones(
			ArchitechSessionBean architechSessionBean) {
		Utilerias utilerias = Utilerias.getUtilerias();
		 BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
	     List<HashMap<String,Object>> list = null;
	     ResponseMessageDataBaseDTO responseDTO = null;
	     try {
		    	responseDTO = HelperDAO.consultar(QUERY_S_DEV, Collections.emptyList(), 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){
						beanAdministraSaldoDAO.setSaldoDevoluciones(utilerias.getString(map.get("SALDO")).trim());
						beanAdministraSaldoDAO.setSaldoDevolucionesCount(utilerias.getString(map.get("TOTAL")).trim());
		  		  	}
				}else{
					beanAdministraSaldoDAO.setSaldoDevoluciones(CEROS);
					beanAdministraSaldoDAO.setSaldoDevolucionesCount(CERO);
				}
				beanAdministraSaldoDAO.setCodError(responseDTO.getCodeError());
				beanAdministraSaldoDAO.setMsgError(responseDTO.getMessageError());
		}catch (ExceptionDataAccess e) {
			showException(e);
			beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
		}
		return  beanAdministraSaldoDAO;
	}
}
