/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonCargaArchCanContingImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:31:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonCargaArchCanConting;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContingDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga obtener la informacion para la
 * funcionalidad del Monitor de carga de archivos historico
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonCargaArchCanContingImpl extends Architech implements DAOMonCargaArchCanConting {

	/**
	 * Constante del version serial
	 */
	private static final long serialVersionUID = 1913849133132655956L;
	
	/** La constante CONS_FROM. */
	private static final String CONS_FROM = " FROM ( ";

	/**
	 * Constante del tipo String que almacena la consulta pasa obtener los
	 * registros de archivos de contingencia diario
	 */
	private static final String QUERY_CONSULTA_MON_CARGA_ARCH_CAN_CONTIG = 
		 " SELECT TMP.NOMBRE_ARCH, TMP.ESTATUS, TMP.NUM_PAGOS,"+ 
	       " TMP.NUM_PAGOS_ERR, TMP.TOTAL_MONTO, TMP.TOTAL_MONTO_ERR, "+ 
	       " TMP.CONT "+ 
	     CONS_FROM +
	             " SELECT TMP.*, ROWNUM AS RN "+ 
	                    CONS_FROM +
	                            " SELECT NOMBRE_ARCH, ESTATUS, NUM_PAGOS, "+
	                                  " NUM_PAGOS_ERR, TOTAL_MONTO, TOTAL_MONTO_ERR, "+ 
	                                    " CONTADOR.CONT "+ 
	                            CONS_FROM +
	                               " SELECT  COUNT(1) CONT "+
	                               " FROM TRAN_ARCH_CANALES CANALES "+
	                               " WHERE FCH_CAPTURA = TO_DATE(?,'DD-MM-YYYY') " +
		                         " ) CONTADOR , TRAN_ARCH_CANALES ADM "+
		                         " WHERE FCH_CAPTURA = TO_DATE(?,'DD-MM-YYYY') " +
		                " ) TMP "+ 
		            " ) TMP "+
		           " WHERE RN BETWEEN ? AND ? ";

	


	/**
	 * Metodo DAO para obtener la informacion del monitor de carga de archivos
	 * historicos
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param fchOperacion String con la fecha de operacion 
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonCargaArchCanContingDAO Objeto del tipo
	 *         BeanResMonCargaArchCanContingDAO
	 * */
	public BeanResMonCargaArchCanContingDAO consultarMonCargaArchCanConting(
			BeanPaginador beanPaginador,String fchOperacion,
			ArchitechSessionBean architechSessionBean) {
		/** Se declaran las variables de objeto a utilizar **/
		final BeanResMonCargaArchCanContingDAO beanResMonCargaArchCanContingDAO = new BeanResMonCargaArchCanContingDAO();
		BeanMonCargaArchCanConting beanMonCargaArchCanConting = null;
		List<HashMap<String, Object>> list = null;
		List<BeanMonCargaArchCanConting> listBeanMonCargaArchCanContig = Collections
				.emptyList();
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se hace una intancia de la clase Utilerias**/
		Utilerias utilerias = Utilerias.getUtilerias();
		List<Object> parametros = null;
		try {
			/** Se inicializa la lista  para obtener los parametros**/	
			parametros = agregarList(fchOperacion, beanPaginador);
			/** Se ejecuta la operacion IDA **/
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_MON_CARGA_ARCH_CAN_CONTIG, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion del reultado de la operacion **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listBeanMonCargaArchCanContig = new ArrayList<BeanMonCargaArchCanConting>();
				list = responseDTO.getResultQuery();
				/** Se recorre el resultado para almacenar los registros obtenidos **/
				for (HashMap<String, Object> map : list) {
					/** Se inicializa un bean por cada registro obtenido **/
					beanMonCargaArchCanConting = new BeanMonCargaArchCanConting();
					/** Se setean los valores ak bean**/
					beanMonCargaArchCanConting.setNombreArchivo(utilerias.getString(map.get("NOMBRE_ARCH")));
					beanMonCargaArchCanConting.setEstatus(utilerias.getString(map.get("ESTATUS")));
					
					beanMonCargaArchCanConting.setNumPagos(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("NUM_PAGOS")), Utilerias.FORMATO_NUMBER));
					beanMonCargaArchCanConting.setTotalMonto(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("TOTAL_MONTO")), Utilerias.FORMATO_DECIMAL_NUMBER));
					beanMonCargaArchCanConting.setNumPagosErr(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("NUM_PAGOS_ERR")), Utilerias.FORMATO_NUMBER));
					beanMonCargaArchCanConting.setTotalMontoErr(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("TOTAL_MONTO_ERR")), Utilerias.FORMATO_DECIMAL_NUMBER));
					/** Se agrega el bean ya llenado a la lista **/
					listBeanMonCargaArchCanContig.add(beanMonCargaArchCanConting);					
				}
			}
			/** Se asgina la lista obtenida al bean de respuesta **/
			beanResMonCargaArchCanContingDAO.setListBeanMonCargaArchCanContig(listBeanMonCargaArchCanContig);
			beanResMonCargaArchCanContingDAO.setCodError(responseDTO.getCodeError());
			beanResMonCargaArchCanContingDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			beanResMonCargaArchCanContingDAO.setCodError(Errores.EC00011B);
			beanResMonCargaArchCanContingDAO.setMsgError(Errores.DESC_EC00011B);

		}
		/** Retorno del metodo **/
		return beanResMonCargaArchCanContingDAO;
	}

	/**
	 * Agregar list.
	 *
	 * @param fchOperacion El objeto: fch operacion
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto list
	 */
	private List<Object> agregarList(String fchOperacion, BeanPaginador beanPaginador) {
		/** Se crea la lista **/
		List<Object> params = new ArrayList<Object>();
		/** Se asignan los parametros **/
		params.add(fchOperacion);
		params.add(fchOperacion);
		params.add(beanPaginador.getRegIni());
		params.add(beanPaginador.getRegFin());
		/** Se retorna la lista **/
		return params;
	}
}
