/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 18:24:31 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico.
 *
 * @author FSW-Vector
 * @since 14/01/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGenerarArchxFchCDAHistImpl extends Architech implements DAOGenerarArchxFchCDAHist {

	/** Constante del Serial version. */
	private static final long serialVersionUID = -570796511152231897L;
	
	/** Constante que almacena el query de la fecha de operacion. */
	private static final String QUERY_FECHA_OPERACION =
	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	/**
	 * Constante del tipo String que almacena la consulta que calcula el id_peticion
	 */
	private static final String QUERY_CONSULTA_CONSULTA_ID_PETICION = 
		" SELECT (NVL(MAX(TO_NUMBER(ID_PETICION)),0)+1) ID_PETICION FROM TRAN_SPEI_CTG_CDA";
	
	/**
	 * Constante que almacena el query de insert en la tabla TRAN_SPEI_CTG_CDA
	 */
	private static final String QUERY_INSERT_TRAN_SPEI_CTG_CDA = 
		" INSERT INTO TRAN_SPEI_CTG_CDA (FCH_OPERACION,ID_PETICION, TIPO_PETICION, USUARIO, SESION, MODULO, NOMBRE_ARCHIVO, FCH_HORA, ESTATUS) "+
	    " VALUES (sysdate,?, ?, ?, ?, ?, ?, TO_DATE(?,'dd-MM-yyyy'), ?)";
	
   /**
    * Metodo DAO para obtener la fecha de operacion.
    *
    * @param modulo El objeto: modulo
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
    */
   public BeanResConsFechaOpDAO consultaFechaOperacion(String modulo, ArchitechSessionBean architechSessionBean){
      final BeanResConsFechaOpDAO beanResConsFechGenArch = new BeanResConsFechaOpDAO();
      List<HashMap<String,Object>> listGenArch = null;
      ResponseMessageDataBaseDTO responseDTOGenArch = null;
      Utilerias utilerias = Utilerias.getUtilerias();
      String consultaEst = QUERY_FECHA_OPERACION;
      /** Se valida el modulo para cambiar la consulta si es necesario, para apuntarla a la tabla correcta **/
		if ("2".equals(modulo)) {
			consultaEst = consultaEst.replaceAll("TRAN_SPEI_CTRL", "TRAN_SPID_CTRL").replaceAll("ENTIDAD_SPEI", "ENTIDAD_SPID");
		}
      try {
    	  /** Se reliza la operacion con el IDA **/
    	  responseDTOGenArch = HelperDAO.consultar(consultaEst, Collections.emptyList(), 
    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
    	/** Se valida el reultado de la oepracion **/
		if(responseDTOGenArch.getResultQuery()!= null && !responseDTOGenArch.getResultQuery().isEmpty()){
			/** Se asigna el resultado de la consuta en una lista **/
			listGenArch = responseDTOGenArch.getResultQuery();
			/** Se recorre la lista **/
			for(HashMap<String,Object> map:listGenArch){
				beanResConsFechGenArch.setFechaOperacion(
						utilerias.formateaFecha(map.get("FCH_OPERACION"), 
								Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
				beanResConsFechGenArch.setCodError(responseDTOGenArch.getCodeError());
				beanResConsFechGenArch.setMsgError(responseDTOGenArch.getMessageError());
  		  }
		}
	} catch (ExceptionDataAccess e) {
		/** Manejo de excepciones **/
        showException(e);
        beanResConsFechGenArch.setCodError(Errores.EC00011B);
	}
      /** Retorno del metodo **/
      return beanResConsFechGenArch;
   }
   
   /**
    * Metodo DAO para registrar el archivo a generar del CDA historico.
    *
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDAO
    */
    public BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean){
    	final BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = new BeanResGenArchCDAHistDAO();
       List<HashMap<String,Object>> list = null;
       ResponseMessageDataBaseDTO responseDTO = null;
       Integer idPeticion = -1;
       Utilerias utilerias = Utilerias.getUtilerias();
       List<Object> parametros = null;
       try{
    	   /** Inovcacion del IDA para consultar **/
    	   responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CONSULTA_ID_PETICION, 
    			   Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
    	   list = responseDTO.getResultQuery();
      	   if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
      	      for(HashMap<String,Object> map:list){
      			idPeticion = utilerias.getInteger(map.get("ID_PETICION"));
      		  }
      	      /** Se invoca al metodo que setea los parametros en una lista **/
      	      parametros = crearLista(architechSessionBean, beanReqGenArchCDAHist, idPeticion);
      	      /** Inovcacion del IDA para insertar **/
              responseDTO = HelperDAO.insertar(QUERY_INSERT_TRAN_SPEI_CTG_CDA, parametros,
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
              beanResGenArchCDAHistDAO.setIdPeticion(idPeticion.toString());
              beanResGenArchCDAHistDAO.setCodError(responseDTO.getCodeError());
              beanResGenArchCDAHistDAO.setMsgError(responseDTO.getMessageError());
	          
      	   }
       } catch (ExceptionDataAccess e) {
          showException(e);
          beanResGenArchCDAHistDAO.setCodError(Errores.EC00011B);
       }
       /** Retorno del metodo **/
       return beanResGenArchCDAHistDAO;
    }
    
    /**
     * Crear lista.
     *
     * @param architechSessionBean El objeto: architech session bean
     * @param beanReqGenArchXFchCDAHist El objeto: bean req gen arch X fch CDA hist
     * @param idPeticion El objeto: id peticion
     * @return Objeto list
     */
    private List<Object> crearLista(ArchitechSessionBean architechSessionBean, BeanReqGenArchXFchCDAHist beanReqGenArchXFchCDAHist, Integer idPeticion){
    	/** Se crea una lista **/
    	List<Object> params = new ArrayList<Object>();
    	/** Se asignan los parametros **/
    	params.add(idPeticion);
    	params.add("D");
    	params.add(architechSessionBean.getUsuario());
    	params.add(architechSessionBean.getIdSesion());
    	params.add("GENARCCDAH");
    	params.add(beanReqGenArchXFchCDAHist.getNombreArchivo());
    	params.add(beanReqGenArchXFchCDAHist.getFechaOp());
    	params.add("PE");
    	/** Retorno del metodo **/
    	return params;
    }
}
