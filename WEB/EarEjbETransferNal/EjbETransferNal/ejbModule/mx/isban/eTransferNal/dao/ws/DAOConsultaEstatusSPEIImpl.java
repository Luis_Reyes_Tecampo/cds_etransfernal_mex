/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOConsultaEstatusSPEIImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:36:56 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.dao.ws;
/**
 * Seccion de Imports
 */
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.helper.HelperDAO;
import net.sf.json.JSONObject;

/**
 * Class DAOConsultaEstatusSPEIImpl. 
 * 
 * Realiza la consulta a la BD GFI 
 * Tabla: TRAN_ESTATUS_SPEI
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaEstatusSPEIImpl extends Architech implements DAOConsultaEstatusSPEI{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6764937031503704823L;
	
	/** La constante QUERY_CONSULTA. */
	private static final String QUERY_CONSULTA = "SELECT TRIM(VALOR_FALTA) TIMESTAMP, TRIM(CVE_PARAM_FALTA) ESTATUS FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO = 'SPEI_ONLINE' ";
	
	/** La constante COD_ERROR. */
	private static final String COD_ERROR = "COD_ERROR"; 
	
	/** La constante DESCRIPCION. */
	private static final String DESCRIPCION = "DESCRIPCION";
	
	/** La constante KO. */
	private static final String KO = "KO";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/**
	 * Consulta estatus SPEI.
	 *
	 * @param session El objeto: session
	 * @return Objeto res entrada string json DTO
	 * @see mx.isban.eTransferNal.dao.ws.DAOConsultaEstatusSPEI#consultaEstatusSPEI(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public EntradaStringJsonDTO consultaEstatusSPEI() {
		info("Salio de la llamada consultaEstatusSPEI");
		//Obejtos que sirven para generar la respuesta del servicio
		EntradaStringJsonDTO response = new EntradaStringJsonDTO();
		JSONObject jsonObject = new JSONObject();
		//Obejtos que sirven para realizar la consulta y procesar el resultado
		List<HashMap<String, Object>> queryResponse = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			//Realiza la consulta
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA, Arrays.asList(
        			new Object[] {}), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			//Valida la respuesta
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				queryResponse = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : queryResponse) {					
					//Agrega cada propiedad
					map.put(COD_ERROR, OK);
					map.put(DESCRIPCION, "CONSULTA EJECUTADA");
					jsonObject = JSONObject.fromMap(map);
				}
			} else {
				//No hay datos
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put(COD_ERROR, KO);
				map.put(DESCRIPCION, "LA CONSULTA NO DEVOLVIO RESULTADOS");
				jsonObject = JSONObject.fromMap(map);
			}
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(COD_ERROR, KO);
			map.put(DESCRIPCION, e.getMessage().toUpperCase());
			jsonObject = JSONObject.fromMap(map);
		}
		//Response DAO
		response.setCadenaJson(jsonObject.toString());
		info("Salio de la llamada consultaEstatusSPEI: " + response.getCadenaJson());
		//Response
		return response;
	}

}
