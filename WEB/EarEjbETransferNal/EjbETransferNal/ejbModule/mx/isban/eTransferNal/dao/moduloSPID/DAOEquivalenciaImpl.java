/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOEquivalenciaImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalenciaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveOperacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 *Clase del tipo DAO que se encarga de obtener la informaci�n de la configuraci�n de equivalencias
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOEquivalenciaImpl extends Architech implements DAOEquivalencia {

	/**
	 * Constante privada que almacena el query de registros de equivalencias
	 */
	private static final StringBuilder QUERY_REGISTROS_EQUIVALENCIAS = 
			new StringBuilder("SELECT T.* FROM ( SELECT T.*, ROWNUM R, COUNT (*) OVER () CONT FROM (")
			.append("SELECT CVE_OPERACION, TIPO_PAGO, TIPO_O_CLASIF_OPER, DESCRIPCION, HORARIO ")
			.append("FROM TRAN_SPID_EQ [ORDENAMIENTO]) T ) T WHERE R BETWEEN ? AND ? "); 

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -2542793158953177893L;
	
	/**
	 * Constante privada que almacena el query condulta clave operaci�n
	 */
	private static final StringBuilder CONSULTA_CVE_OPER = new StringBuilder()
			.append("SELECT CVE_OPERACION, DESCRIPCION ").append("FROM TRAN_CVE_OPER");
	
	/**
	 * Constante privada que almacena el query insertar equivalencias
	 */
	private static final StringBuilder INSERT_EQUIVALENCIA = new StringBuilder()
			.append("INSERT INTO TRAN_SPID_EQ (CVE_OPERACION, TIPO_PAGO, TIPO_O_CLASIF_OPER, HORARIO, DESCRIPCION)")
			.append(" VALUES (?,?,?,?,?)");

	
	/**Metodo que sirve para consultar las equivalencias
	   * @param beanPaginador Objeto del tipo BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return beanResEquivalenciaDAO
	   */
	@Override
	public BeanResEquivalenciaDAO listarEquivalencias(BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = null;
		Integer zero = Integer.valueOf(0);
		Utilerias utilerias = Utilerias.getUtilerias();
		
		try {
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			responseDTO = HelperDAO.consultar(QUERY_REGISTROS_EQUIVALENCIAS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), 
					Arrays.asList(new Object[] { beanPaginador.getRegIni(), beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
			beanResEquivalenciaDAO = new BeanResEquivalenciaDAO();
			beanResEquivalenciaDAO.setBeanEquivalencias(new ArrayList<BeanEquivalencia>());
			if (list != null && !list.isEmpty()) {
				for (Map<String, Object> mapa : list) {
					beanResEquivalenciaDAO.getBeanEquivalencias().add(mapearResultBase(mapa));
					
					if (zero.equals(beanResEquivalenciaDAO.getTotalReg())) {
						beanResEquivalenciaDAO.setTotalReg(utilerias.getInteger(mapa.get("CONT")));
					}
				}
			}
			beanResEquivalenciaDAO.setCodError(responseDTO.getCodeError());
			beanResEquivalenciaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return beanResEquivalenciaDAO;
	}
	
	/**Metodo que sirve para consultar las equivalencias
	   * @param cveOperacion Objeto del tipo String
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return beanResEquivalenciaDAO
	   */
	@Override
	public BeanResEquivalenciaDAO obtenerEquivalencia(String cveOperacion,
			ArchitechSessionBean architechSessionBean) {
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = null;
		try {
			StringBuilder queryEquivalencia = new StringBuilder().append("SELECT CVE_OPERACION, TIPO_PAGO, ")
				.append("TIPO_O_CLASIF_OPER, DESCRIPCION, HORARIO FROM TRAN_SPID_EQ WHERE CVE_OPERACION = '")
				.append(cveOperacion).append("'");
			
			responseDTO = HelperDAO.consultar(queryEquivalencia.toString(), Arrays.asList(new Object[] {}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
			beanResEquivalenciaDAO = new BeanResEquivalenciaDAO();
			beanResEquivalenciaDAO.setBeanEquivalencias(new ArrayList<BeanEquivalencia>());
			if (list != null && !list.isEmpty()) {
				for (Map<String, Object> mapa : list) {
					beanResEquivalenciaDAO.getBeanEquivalencias().add(mapearResultBase(mapa));
					beanResEquivalenciaDAO.setCodError(responseDTO.getCodeError());
					beanResEquivalenciaDAO.setMsgError(responseDTO.getMessageError());
				}
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
		}

		return beanResEquivalenciaDAO;
	}

	/**Metodo que sirve para insertar una nueva equivalencia
	   * @param beanEquivalencia Objeto del tipo BeanEquivalencia
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return beanResEquivalenciaDAO
	   */
	@Override
	public BeanResEquivalenciaDAO crearEquivalencia(BeanEquivalencia beanEquivalencia, ArchitechSessionBean architechSessionBean) {
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = new BeanResEquivalenciaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
		try {
			responseDTO = HelperDAO.insertar(INSERT_EQUIVALENCIA.toString(), 
					Arrays.asList(new Object[] {beanEquivalencia.getClaveOper(), 
							                    beanEquivalencia.getTipoPago(),
							                    beanEquivalencia.getClasificacion(),
							                    beanEquivalencia.getHorario(),
							                    beanEquivalencia.getDescripcion()}), HelperDAO.CANAL_GFI_DS, 
					                            this.getClass().getName());
			beanResEquivalenciaDAO.setCodError(responseDTO.getCodeError());
			beanResEquivalenciaDAO.setMsgError(responseDTO.getMessageError());
		} catch(ExceptionDataAccess e) {
			showException(e);
		}
		return beanResEquivalenciaDAO;
	}
	
	/**Metodo que sirve para eliminar una equivalencia
	   * @param beanEquivalencia Objeto del tipo BeanEquivalencia
	   * @param beanPaginador Objeto del tipo BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return beanResEquivalenciaDAO
	   */
	@Override
	public BeanResEquivalenciaDAO eliminarEquivalencia(BeanEquivalencia beanEquivalencia, BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) {
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = new BeanResEquivalenciaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		StringBuilder eliminarEquivalenciaQuery = new StringBuilder().append("DELETE FROM TRAN_SPID_EQ WHERE ")
				.append("CVE_OPERACION = '" + beanEquivalencia.getClaveOper() +"' ")
				.append(" AND TIPO_PAGO = "+beanEquivalencia.getTipoPago()+" ")
				.append("AND TIPO_O_CLASIF_OPER = '" + beanEquivalencia.getClasificacion() + "'")
				.append("AND HORARIO = '" + beanEquivalencia.getHorario() + "'");
		
		try {
			responseDTO = HelperDAO.eliminar(eliminarEquivalenciaQuery.toString(), 
					Arrays.asList(new Object[] {}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResEquivalenciaDAO.setCodError(responseDTO.getCodeError());
			beanResEquivalenciaDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		
		return beanResEquivalenciaDAO;
	}
	
	/**Metodo que sirve para editar una equivalencia
	   * @param beanEquivalencia Objeto del tipo BeanEquivalencia
	   * @param beanPaginador Objeto del tipo BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param cveOperacion Objeto del tipo String
	   * @param tipoPago Objeto del tipo String
	   * @param clasificacion Objeto del tipo String
	   * @return beanResEquivalenciaDAO
	   */
	@Override
	public BeanResEquivalenciaDAO editarEquivalencia(BeanEquivalencia beanEquivalencia, BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String cveOperacion, String tipoPago, String clasificacion) {
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = new BeanResEquivalenciaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		StringBuilder updateEquivalencia = new 
				StringBuilder().append("UPDATE TRAN_SPID_EQ ") 
							   .append("SET CVE_OPERACION = ?, TIPO_PAGO = ?, TIPO_O_CLASIF_OPER = ?, HORARIO = ?, DESCRIPCION = ?")
							   .append("WHERE CVE_OPERACION = '").append(cveOperacion.split(" ")[0].trim()).append("' ")
							   .append(" AND TIPO_PAGO = '").append(tipoPago.split(" ")[0].trim()).append("' ")
							   .append(" AND TIPO_O_CLASIF_OPER = '").append(clasificacion).append("' ");
		
		try {
			responseDTO = HelperDAO.actualizar(updateEquivalencia.toString(), 
					Arrays.asList(new Object[] {beanEquivalencia.getClaveOper(), 
		                    beanEquivalencia.getTipoPago(),
		                    beanEquivalencia.getClasificacion(),
		                    beanEquivalencia.getHorario(),
		                    beanEquivalencia.getDescripcion()}), 
					        HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResEquivalenciaDAO.setCodError(responseDTO.getCodeError());
			beanResEquivalenciaDAO.setMsgError(responseDTO.getMessageError());
		} catch(ExceptionDataAccess e) {
			showException(e);
			beanResEquivalenciaDAO.setCodError(Errores.EC00011B);
		}
		
		return beanResEquivalenciaDAO;
	}

	/**Metodo que sirve para mapear el resultado
	   * @param mapaResultado Objeto del tipo Map<String, Object>
	   * @return beanEquivalencia
	   */
	private BeanEquivalencia mapearResultBase(Map<String, Object> mapaResultado) {
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanEquivalencia beanEquivalencia = new BeanEquivalencia();
		beanEquivalencia.setClaveOper(utilerias.getString(mapaResultado.get("CVE_OPERACION")));
		beanEquivalencia.setTipoPago(utilerias.getString(mapaResultado.get("TIPO_PAGO")));
		beanEquivalencia.setClasificacion(utilerias.getString(mapaResultado.get("TIPO_O_CLASIF_OPER")));
		beanEquivalencia.setDescripcion(utilerias.getString(mapaResultado.get("DESCRIPCION")));
		beanEquivalencia.setHorario(utilerias.getString(mapaResultado.get("HORARIO")));
		return beanEquivalencia;
	}

	/**Metodo que sirve para obtener la lista de claves de operaci�n
	   * @return listaCveOperacion
	   */
	@Override
	public List<BeanClaveOperacion> obtenerListaClaveOperacion() {
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanClaveOperacion> listaCveOperacion = new ArrayList<BeanClaveOperacion>();
		BeanClaveOperacion claveOperacion = null;
		try {
			responseDTO = HelperDAO.consultar(CONSULTA_CVE_OPER.toString(), Arrays.asList(new Object[] {}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				for (Map<String, Object> mapResult : responseDTO.getResultQuery()) {
					claveOperacion = new BeanClaveOperacion();

					claveOperacion.setClaveOperacion(utilerias.getString(mapResult.get("CVE_OPERACION")));
					claveOperacion.setDescripcion(utilerias.getString(mapResult.get("DESCRIPCION")));

					listaCveOperacion.add(claveOperacion);
				}
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return listaCveOperacion;
	}

}
