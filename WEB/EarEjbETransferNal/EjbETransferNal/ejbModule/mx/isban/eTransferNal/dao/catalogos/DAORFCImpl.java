/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORFCImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 12:01:27 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;
import mx.isban.eTransferNal.beans.catalogos.BeanResRFC;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasRFC;

/**
 * Class DAORFCImpl.
 *
 * Clase que contiene los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORFCImpl extends Architech implements DAORFC {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 694675231570602081L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante CONSULTA_ALL. */
	private static final String CONSULTA_TODO = "SELECT DISTINCT "
			.concat("RFC, ")
			.concat("TOTAL, ")  
			.concat("RN ") 
			.concat("FROM (") 
			.concat("SELECT DISTINCT ") 
			.concat("RFC, ") 
			.concat("(SELECT COUNT(1) AS CONT FROM TRAN_RFC_SPID_EXENTOS [FILTRO_WH]) AS TOTAL, ") 
			.concat("ROWNUM AS RN ") 
			.concat("FROM TRAN_RFC_SPID_EXENTOS [FILTRO_WH]) ");
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_RFC_SPID_EXENTOS "
			.concat("WHERE TRIM(RFC) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_RFC_SPID_EXENTOS(RFC) "
			.concat("VALUES(TRIM(UPPER(?)))	");
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR = "UPDATE TRAN_RFC_SPID_EXENTOS SET  "
			.concat("RFC = TRIM(UPPER(?)) WHERE TRIM(RFC) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_ELIMINAR. */
	private static final String CONSULTA_ELIMINAR = "DELETE FROM TRAN_RFC_SPID_EXENTOS "
			.concat("WHERE TRIM(RFC) = TRIM(UPPER(?))");
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasCatalogos utilerias = UtileriasCatalogos.getInstancia();
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasRFC utileriasRFC = UtileriasRFC.getInstancia();
	
	/**
	 * Consultar.
	 * 
	 * Consultar los registros del catalogo.
	 *
	 * @param beanRFC El objeto: bean RFC  --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador El objeto: bean paginador  --> Objeto para realizar el paginado
	 * @param session El objeto: session  --> El objeto session
	 * @return Objeto bean bloques  --> Resultado obtenido en la consulta
	 */
	@Override
	public BeanResRFC consultar(BeanRFC beanRFC, BeanPaginador beanPaginador, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResRFC response = new BeanResRFC();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanRFC> rfcRes = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utileriasRFC.getFiltro(beanRFC, "AND");
		String filtroWhere = utileriasRFC.getFiltro(beanRFC, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				rfcRes = new ArrayList<BeanRFC>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanRFC rfc = new BeanRFC();
					/** Seteo de datos **/
					rfc.setRfc(utilerias.getString(map.get("RFC")));
					/** Se añade el objeto a la lista**/
					rfcRes.add(rfc);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setRfcs(rfcRes);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Agregar.
	 * 
	 * Agrega un nuevo registro al catalogo.
	 * 
	 * @param beanRFC El objeto: bean RFC  --> Objeto con los datos del nuevo registro
	 * @param session El objeto: session   --> El objeto session
	 * @return Objeto bean res base  --> Resultado de la operacion
	 */
	@Override
	public BeanResBase agregar(BeanRFC beanRFC, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasRFC.agregarParametros(beanRFC, null, ConstantesCatalogos.ALTA);
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(beanRFC);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Editar.
	 * 
	 * Mofifica un registro del catalogo.
	 *
	 * @param beanRFC El objeto: bean RFC  --> Objeto con los datos del nuevo registro
	 * @param anterior El objeto: anterior  --> Objeto con los datos del registro a editar
	 * @param session El objeto: session   --> El objeto session
	 * @return Objeto bean res base --> Resultado de la operacion
	 */
	@Override
	public BeanResBase editar(BeanRFC beanRFC, BeanRFC datoAnterior, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			int existencia = 0;
			existencia = buscarRegistro(beanRFC);
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utileriasRFC.agregarParametros(beanRFC, datoAnterior, ConstantesCatalogos.MODIFICACION);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(CONSULTA_MODIFICAR, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Eliminar.
	 * 
	 * Elimina un registro del catalogo.
	 *
	 * @param beanRFC El objeto: bean RFC --> Objeto con los datos registro a eliminar
	 * @param session El objeto: session   --> El objeto session
	 * @return Objeto bean res base --> Resultado de la operacion
	 */
	@Override
	public BeanResBase eliminar(BeanRFC beanRFC, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasRFC.agregarParametros(beanRFC, null, ConstantesCatalogos.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(CONSULTA_ELIMINAR, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 * 
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param beanRFC El objeto: bean RFC  --> Contiene los datos de un registro a buscar
	 * @return Objeto int  --> Resultado de la busqueda
	 */
	private int buscarRegistro(BeanRFC beanRFC) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasRFC.agregarParametros(beanRFC, null, ConstantesCatalogos.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
}
