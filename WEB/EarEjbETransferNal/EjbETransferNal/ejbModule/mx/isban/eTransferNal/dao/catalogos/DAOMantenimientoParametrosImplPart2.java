/**
 * Clase DAOMantenimientoParametrosImplPart2
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.UtileriasImplMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOMantenimientoParametrosImplPart2.
 */
public class DAOMantenimientoParametrosImplPart2 extends Architech {
	
	/** Variable de Serializacion. */
	private static final long serialVersionUID = -4923867621981595489L;
	
	/** The Constant DESCR. */
	private static final String DESCR = "DESCRIPCION";
	
	/** The Constant IS_NULL. */
	private static final String IS_NULL = " is Null ";
	
	/** The Constant TRIM_VALFALTA. */
	private static final String TRIM_VALFALTA = "TRIM(VALOR_FALTA)";	
	
	/**
	 * Obtnener lista parametros.
	 *
	 * @param listResult the list result
	 * @return the list
	 */
	List<BeanMantenimientoParametros> obtnenerListaParametros(List<HashMap<String,Object>> listResult){
		List<BeanMantenimientoParametros> listParams=new ArrayList<BeanMantenimientoParametros>();
  	  	Utilerias utilerias = Utilerias.getUtilerias();

  	  	//se recorre la lista de resultados del query
		for(HashMap<String,Object> map:listResult){
			BeanMantenimientoParametros bean = new BeanMantenimientoParametros();
			bean.setCveParametro(utilerias.getString(map.get("CVE_PARAMETRO")));
			bean.setParametroFalta(utilerias.getString(map.get("CVE_PARAM_FALTA")));
			bean.setValorFalta(utilerias.getString(map.get("VALOR_FALTA")));
			bean.setTipoParametro(utilerias.getString(map.get("TIPO_PARAMETRO")));
			bean.setDescripParam(utilerias.getString(map.get(DESCR )));
			//se agrega el bean a la lista
			listParams.add(bean);
		}
		// se retorna la lista
		return listParams;
	}
	
	/**
	 * Obtner lista opciones.
	 *
	 * @param listResult the list result
	 * @return the list
	 */
	List<BeanMantenimientoParametros> obtnerListaOpciones(List<HashMap<String,Object>> listResult){
		List<BeanMantenimientoParametros> listOpciones=new ArrayList<BeanMantenimientoParametros>();
		BeanMantenimientoParametros bean = new BeanMantenimientoParametros();
	    Utilerias utilerias = Utilerias.getUtilerias();
		bean.setOpcion("Seleccione");
		bean.setValorOpcion("");
		listOpciones.add(bean);
		
  	  	//se recorre la lista de resultados del query
		for(HashMap<String,Object> map:listResult){
			bean = new BeanMantenimientoParametros();
			bean.setOpcion(utilerias.getString(map.get(DESCR )));
			bean.setValorOpcion(utilerias.getString(map.get("CODIGO_GLOBAL")));
			//se agrega el bean a la lista
			listOpciones.add(bean);
		}
		//se retorna la lista
		return listOpciones;
	}
	
	/**
	 * Obtner datos parametro.
	 *
	 * @param listResult the list result
	 * @return the list
	 */
	List<BeanMantenimientoParametros> obtnerDatosParametro(List<HashMap<String,Object>> listResult){
		List<BeanMantenimientoParametros> datosParam=new ArrayList<BeanMantenimientoParametros>();
  	  	Utilerias utilerias = Utilerias.getUtilerias();

  	  	//se recorre la lista de resultados del query
		for(HashMap<String,Object> map:listResult){
			BeanMantenimientoParametros bean = new BeanMantenimientoParametros();
			bean.setCveParametro(utilerias.getString(map.get("CVE_PARAMETRO")));
			bean.setDescripParam(utilerias.getString(map.get(DESCR )));
			bean.setTipoParametro(utilerias.getString(map.get("TIPO_PARAMETRO")));
			bean.setTipoDato(utilerias.getString(map.get("TIPO_DATO")));
			bean.setLongitud(utilerias.getString(map.get("LONGITUD")));
			bean.setValorFalta(utilerias.getString(map.get("VALOR_FALTA")));
			bean.setTrama(utilerias.getString(map.get("DEFINE_TRAMA")));
			bean.setCola(utilerias.getString(map.get("DEFINE_COLA")));
			bean.setMecanismo(utilerias.getString(map.get("DEFINE_MECANISMO")));
			bean.setPosicion(utilerias.getString(map.get("POSICION")));
			bean.setParametroFalta(utilerias.getString(map.get("CVE_PARAM_FALTA")));
			bean.setPrograma(utilerias.getString(map.get("PROGRAMA")));
			//se agrega el bean a la lista
			datosParam.add(bean);
		}
		//se retorna la lista 
		return datosParam;
	}
	
	/**
	 * Armar parametrizacion select.
	 *
	 * @param parametros the parametros
	 * @param req the req
	 * @return the string
	 */
	String armarParametrizacionSelect(List<Object> parametros, BeanReqMantenimientoParametros req){
		UtileriasImplMantenimientoParametros utilsImplMantParam = UtileriasImplMantenimientoParametros.getUtilerias();
		StringBuilder builder = new StringBuilder();
		String equalInt = " = ? ";
		String vacio = "";
		boolean isFirst = true;
		
		if( !vacio.equals(req.getParametro()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);
			
			//se concatena el campo cveParametro
			builder.append("TRIM(CVE_PARAMETRO)");
			builder.append(equalInt);
			parametros.add(req.getParametro());
		}
		if( !vacio.equals(req.getDescripParam()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo descripcion
			builder.append("TRIM(DESCRIPCION)");
			builder.append(equalInt);
			parametros.add(req.getDescripParam());
		}
		if( !vacio.equals(req.getTipoParametro()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo tipoParametro
			builder.append("TRIM(TIPO_PARAMETRO)");
			builder.append(equalInt);
			parametros.add(req.getTipoParametro());
		}
		if( !vacio.equals(req.getTipoDato()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo tipoDato
			builder.append("TRIM(TIPO_DATO)");
			builder.append(equalInt);
			parametros.add(req.getTipoDato());
		}
		if( !vacio.equals(req.getLongitudDato()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo longitud
			builder.append("TRIM(LONGITUD)");
			builder.append(equalInt);
			parametros.add(req.getLongitudDato());
		}
		if( !vacio.equals(req.getValorFalta()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo valorFalta
			builder.append(TRIM_VALFALTA);
			builder.append(equalInt);
			parametros.add(req.getValorFalta());
		}
		
		//se llama a metodo para armar parametrizacion de combos Trama, Cola y Mecanismo
		isFirst = armarParametrizacionCombos(builder,parametros, req, isFirst);
		
		if( !vacio.equals(req.getPosicion()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo posicion
			builder.append("TRIM(POSICION)");
			builder.append(equalInt);
			parametros.add(req.getPosicion());
		}
		if( !vacio.equals(req.getParametroFalta()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo cveParametroFalta
			builder.append("TRIM(CVE_PARAM_FALTA)");
			builder.append(equalInt);
			parametros.add(req.getParametroFalta());
		}
		if( !vacio.equals(req.getPrograma()) ){
			//se define si el parametro es el primero
			isFirst = utilsImplMantParam.addAnd(builder,isFirst);

			//se concatena el campo programa
			builder.append("TRIM(PROGRAMA)");
			builder.append(equalInt);
			parametros.add(req.getPrograma());
		}
		//se retorna la parametrizacion generada
		return builder.toString();
	}

	/**
	 * Armar parametrizacion combos.
	 *
	 * @param builder the builder
	 * @param parametros the parametros
	 * @param req the req
	 * @param isFirst the is first
	 * @return true, if successful
	 */
	boolean armarParametrizacionCombos(StringBuilder builder,
			List<Object> parametros, BeanReqMantenimientoParametros req, boolean isFirst){
		UtileriasImplMantenimientoParametros utilsImplMantParam = UtileriasImplMantenimientoParametros.getUtilerias();
		String equalInt = " = ? ";
		String vacio = "";
		boolean vaPrimero=isFirst;
		
		if( !vacio.equals(req.getTrama()) ){
			if( "Nulo".equals(req.getTrama()) ){
				//se concatena is Null a la parametrizacion
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_TRAMA)");
				builder.append(IS_NULL);
			}else{
				//se define si el parametro es el primero
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_TRAMA)");
				builder.append(equalInt);
				parametros.add(req.getTrama());
			}
		}
		if( !vacio.equals(req.getCola()) ){
			if( "Nulo".equals(req.getCola()) ){
				//se concatena is Null a la parametrizacion
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_COLA)");
				builder.append(IS_NULL);
			} else {
				//se define si el parametro es el primero
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_COLA)");
				builder.append(equalInt);
				parametros.add(req.getCola());
			}
		}
		if( !vacio.equals(req.getMecanismo()) ){
			if( "Nulo".equals(req.getMecanismo()) ){
				//se concatena is Null a la parametrizacion
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_MECANISMO)");
				builder.append(IS_NULL);
			} else {
				//se define si el parametro es el primero
				vaPrimero = utilsImplMantParam.addAnd(builder,vaPrimero);
				builder.append("TRIM(DEFINE_MECANISMO)");
				builder.append(equalInt);
				parametros.add(req.getMecanismo());
			}
		}
		return vaPrimero;
	}

}
