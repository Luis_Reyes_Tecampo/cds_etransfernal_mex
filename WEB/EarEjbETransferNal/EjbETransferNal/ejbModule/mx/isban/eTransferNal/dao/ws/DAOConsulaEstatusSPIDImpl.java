/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsulaEstatusSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/03/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
/**
 * @author cchong
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsulaEstatusSPIDImpl extends Architech implements DAOConsulaEstatusSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5890219845545762100L;

	@Override
	public ResBeanEjecTranDAO ejecutar(String trama) {
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(
				trama, "ESTATDET", "ARQ_MENSAJERIA");
		return resBeanEjecTranDAO;
	}

}
