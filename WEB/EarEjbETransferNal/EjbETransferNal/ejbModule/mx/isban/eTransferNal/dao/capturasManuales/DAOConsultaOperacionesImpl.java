package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase de acceso a datos para modulo consulta de operaciones
 */
//Clase de acceso a datos para modulo consulta de operaciones
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaOperacionesImpl extends Architech implements DAOConsultaOperaciones{

	//Propiedad del serialVersionUID
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7382808378323702850L;

	//query que prepara el inicio de la consulta para soportar la paginacion
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INICIO_BUSQUEDA
	 */
	private static final String QUERY_INICIO_BUSQUEDA ="select ID_FOLIO, IMPORTE_ABONO, ID_TEMPLATE, "
			+ "CVE_USUARIO_CAP, USR_AUTORIZA, ESTATUS, FCH_CAPTURA, USR_APARTA, OPERACION_APARTADA, "
			+ "REFE_TRANSFER, COD_ERROR, MSG_ERROR, IMPORTE_CARGO, NUM_CUENTA_ORD, NUM_CUENTA_REC "
			+ "from (select TMP.ID_FOLIO, TMP.IMPORTE_ABONO, TMP.ID_TEMPLATE, TMP.CVE_USUARIO_CAP, "
			+ "TMP.USR_AUTORIZA, TMP.ESTATUS, TMP.FCH_CAPTURA, TMP.USR_APARTA, TMP.OPERACION_APARTADA, "
			+ "TMP.REFE_TRANSFER, TMP.COD_ERROR, TMP.MSG_ERROR, TMP.IMPORTE_CARGO, TMP.NUM_CUENTA_ORD, "
			+ "TMP.NUM_CUENTA_REC, rownum RN from(";
	
	//Query que cierra la consulta para soportar la paginacion
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_FIN_BUSQUEDA
	 */
	private static final String QUERY_FIN_BUSQUEDA =") TMP ) where RN between ? and ?";

	//Query que cuenta los resultados para soportar la paginacion
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_BUSQUEDA_TRAN_CAP_OPER
	 */
	private static final String QUERY_COUNT_BUSQUEDA_TRAN_CAP_OPER ="SELECT COUNT(1) CONT from TRAN_CAP_OPER ";

	//Query que aparta la operacion
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_APARTAR_OPERACION = "update TRAN_CAP_OPER set OPERACION_APARTADA = 1, USR_APARTA = ? WHERE ID_FOLIO = ?";

	//Query que desaparta una operacion
	/** The Constant QUERY_DESAPARTAR_OPERACION. */
	private static final String QUERY_DESAPARTAR_OPERACION = "update TRAN_CAP_OPER set OPERACION_APARTADA = 0, "
			+ "USR_APARTA = NULL WHERE TRIM(USR_APARTA) = ? AND TRIM(ID_FOLIO) = ?";
	
	//Query que consulta si una operacion esta apartada
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_CONSUL_OPER_APART = "select ID_FOLIO from TRAN_CAP_OPER where OPERACION_APARTADA = 1 AND TRIM(USR_APARTA) = ?";
	
	//Query que busca los datos de las operaciones
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_BUSQUEDA_TRAN_CAP_OPER
	 */
	private static final String QUERY_BUSQUEDA_TRAN_CAP_OPER ="select ID_FOLIO, IMPORTE_ABONO, ID_TEMPLATE, "
			+ "CVE_USUARIO_CAP, USR_AUTORIZA, ESTATUS, FCH_CAPTURA, USR_APARTA, OPERACION_APARTADA, REFE_TRANSFER, "
			+ "COD_ERROR, MSG_ERROR, IMPORTE_CARGO, NUM_CUENTA_ORD, NUM_CUENTA_REC from TRAN_CAP_OPER";

	//Query complementario para ordenar la busqueda
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_BUSQUEDA_ORDER
	 */
	private static final String QUERY_BUSQUEDA_ORDER =" ORDER BY FCH_CAPTURA DESC";

	//Query para eliminar un folio
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_ELIMINAR_FOLIO
	 */
	private static final String QUERY_ELIMINAR_FOLIO ="DELETE FROM TRAN_CAP_OPER WHERE ID_FOLIO = ?";

	//Query que determina si el usuario es administador o supervisor
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_ELIMINAR_FOLIO
	 */
	private static final String QUERY_ADMIN_SUPERVISOR ="SELECT COUNT(1) CONT FROM TR_SEG_CAT_PERFIL SCP, TR_SEG_CFG_USUARIO_PERFIL SCUP"
			+" WHERE SCP.TXT_PERFIL_SAM IN ('grp_appnal_admin','grp_appnal_super') "
			+"AND SCUP.CVE_PERFIL = SCP.CVE_PERFIL AND TRIM(SCUP.CVE_USUARIO) = ?";
	
	//Query que inserta el query para generar la exportacion de los registros
	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String QUERY_INSERT_CONSULTA_EXPORTAR_TODO =
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,"
		+ "ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";

	//Constante que tiene las columnas que se exportaran
	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String COLUMNAS_CONSULTA_CAPTURAS_MANUALES =
		"Folio,Importe,Template,Usr Captura,Usr Aut/Cancela,Estatus,Fecha Captura,Usr Apartado,Ref Transfer,Cod Error,Msg Error";

	//Query que genera las columnas para exportar los registros
	/**
	 * Constante para la consulta de exportar
	 */
	private static final String QUERY_CONSULTA_EXPORTAR_TODO =
		"SELECT ID_FOLIO || ',' || IMPORTE_ABONO || ',' || ID_TEMPLATE || ',' || CVE_USUARIO_CAP || ',' ||"+
		" USR_AUTORIZA || ',' || ESTATUS || ',' || to_char(FCH_CAPTURA,'DD/MM/YYYY') || ',' || USR_APARTA || ',' || REFE_TRANSFER || ',' || "+
		" COD_ERROR || ',' || MSG_ERROR " +
		"FROM TRAN_CAP_OPER ";

	//Metodo que se encarga de consultar las operaciones basadas en los filtros
	@Override
	public BeanResConsOperaciones consultarOperaciones(ArchitechSessionBean session,BeanReqConsOperaciones req){
		final BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();

		//Aqui se setea el paginador para que se mantenga entre pantallas
		beanResConsOperaciones.setPaginador(req.getPaginador());
		
		//lista de beans que devolvera el metodo
		List<BeanConsOperaciones> listBeanConsOperaciones= new ArrayList<BeanConsOperaciones>();

		//Se crean los objetos necesarios para ejecutar el query
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		String queryParam = null;
		String query = null;

		try {
	    	 //Consulta a la base de datos
			List<Object> parametros = new ArrayList<Object>();
			queryParam = armaConsultaParametrizada(parametros,req,session, false);
			query = QUERY_COUNT_BUSQUEDA_TRAN_CAP_OPER+queryParam;

			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Se agrega el total de registros que servira para hacer la consulta con paginacion
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanResConsOperaciones.setTotalReg(utilerias.getInteger(map.get("CONT")));
				}
			}

			//Se valida que exista al menos un registro
			if(beanResConsOperaciones.getTotalReg()>0){
				parametros.add(beanResConsOperaciones.getPaginador().getRegIni());
				parametros.add(beanResConsOperaciones.getPaginador().getRegFin());

				//Se compone el query con el inicio, el query, los parametros y el cierre
				query = QUERY_INICIO_BUSQUEDA+QUERY_BUSQUEDA_TRAN_CAP_OPER+queryParam+QUERY_BUSQUEDA_ORDER+QUERY_FIN_BUSQUEDA;
				responseDTO = HelperDAO.consultar(query,parametros,
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());

				//Se valida que respuesta no sea nula ni vacia
				list = responseDTO.getResultQuery();
				beanResConsOperaciones.setCodError(responseDTO.getCodeError());
				beanResConsOperaciones.setMsgError(responseDTO.getMessageError());
			}else{
				list=null;
				//No es un error que venga vacío simplemente se devuelve asi y mas adelante se validara
				beanResConsOperaciones.setCodError(Errores.CODE_SUCCESFULLY);
				beanResConsOperaciones.setMsgError(Errores.OK00000V);
			}

		} catch (ExceptionDataAccess e) {
	    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
			beanResConsOperaciones.setCodError(Errores.EC00011B);
			beanResConsOperaciones.setMsgError(Errores.DESC_EC00011B);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		//Se valida que la lista no sea nula ni venga vacia
		if(list != null && !list.isEmpty()){
			for(HashMap<String,Object> map:list){
				BeanConsOperaciones beanConsOperaciones=descomponeConsulta(session, map);
				listBeanConsOperaciones.add(beanConsOperaciones);
				String usuarioApartado=utilerias.getString(map.get("USR_APARTA"));
				if("1".equals(utilerias.getString(map.get("OPERACION_APARTADA"))) && usuarioApartado.equals(session.getUsuario())){
					beanResConsOperaciones.setPuedeEliminar(true);
				}
			}
		}
		
		//Se setea la lista de beans
		beanResConsOperaciones.setListBeanConsOperaciones(listBeanConsOperaciones);

		//Mapeo de propiedades en comun
		beanResConsOperaciones.setUsuarioCaptura(req.getUsuarioCaptura());
		beanResConsOperaciones.setUsuarioAutoriza(req.getUsuarioAutoriza());
		beanResConsOperaciones.setEstatusOperacion(req.getEstatusOperacion());
		beanResConsOperaciones.setTemplate(req.getTemplate());

		return beanResConsOperaciones;
	}
	
	/**
	 * Metodo que genera un objeto del tipo BeanMovimientoCDA a partir de la respuesta de la consulta.
	 *
	 * @param sessionBean the session bean
	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	 * @return BeanMovimientoCDA Objeto del tipo BeanMovimientoCDA
	 */
	//Clase que se encarga de mapear el diccionario de respuesta al bean
	private BeanConsOperaciones descomponeConsulta(ArchitechSessionBean sessionBean, Map<String,Object> map){
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanConsOperaciones beanConsOperaciones = null;
		beanConsOperaciones = new BeanConsOperaciones();
		beanConsOperaciones.setFolio(utilerias.getString(map.get("ID_FOLIO")));
		beanConsOperaciones.setImporte(utilerias.getString(map.get("IMPORTE_ABONO")));
		beanConsOperaciones.setTemplate(utilerias.getString(map.get("ID_TEMPLATE")));
		beanConsOperaciones.setUsrCaptura(utilerias.getString(map.get("CVE_USUARIO_CAP")));
		beanConsOperaciones.setUsrAutoriza(utilerias.getString(map.get("USR_AUTORIZA")));
		String estatus=utilerias.getString(map.get("ESTATUS"));
		beanConsOperaciones.setEstatus(utilerias.getString(map.get("ESTATUS")));
		beanConsOperaciones.setFechaCaptura(utilerias.formateaFecha(map.get("FCH_CAPTURA"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		String usuarioApartado=utilerias.getString(map.get("USR_APARTA"));
		beanConsOperaciones.setUsrApartado(usuarioApartado);
		//Se valida que el usuario actual sea quien ha apartado la operacion
		boolean apartadoPorMi = usuarioApartado!=null && usuarioApartado.equals(sessionBean.getUsuario());
		String valorApartado=utilerias.getString(map.get("OPERACION_APARTADA"));
		//Se valida que la operacion pueda ser apartada
		boolean validaEstatus=!("CA".equals(estatus) || "CO".equals(estatus));
		beanConsOperaciones.setApartado("1".equals(valorApartado) && apartadoPorMi && validaEstatus);
		beanConsOperaciones.setStatusOperacionApartada(valorApartado);
		beanConsOperaciones.setRefTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
		beanConsOperaciones.setMsgErrorOpe(utilerias.getString(map.get("MSG_ERROR")));
		beanConsOperaciones.setCodErrorOpe(utilerias.getString(map.get("COD_ERROR")));
		//Pista
		beanConsOperaciones.setMonto(utilerias.getString(map.get("IMPORTE_CARGO")));
		beanConsOperaciones.setCuentaOrdenante(utilerias.getString(map.get("NUM_CUENTA_ORD")));
		beanConsOperaciones.setCuentaReceptora(utilerias.getString(map.get("NUM_CUENTA_REC")));
		
		//Se regresa el beanConsOperaciones
		return beanConsOperaciones;
	}

	/**
	 * Metodo para sirve para generar el query y parametrizar la consulta.
	 *
	 * @param parametros Objeto del tipo @see List<Object>
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param session the session
	 * @param hasWhere the has where
	 * @return String Objeto del tipo @see String
	 */
	//Clase encargada de agregar al query los filtros seleccionados
	public String armaConsultaParametrizada(List<Object> parametros, BeanReqConsOperaciones beanReqConsOperaciones, ArchitechSessionBean session, boolean hasWhere){
		StringBuilder builder = new StringBuilder();
		String vacio = "";
		String equalInt=" = ?";
		boolean isFirst = !hasWhere;
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getUsuarioCaptura())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(CVE_USUARIO_CAP)");
			builder.append(equalInt);
			parametros.add(beanReqConsOperaciones.getUsuarioCaptura());
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getUsuarioAutoriza())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(USR_AUTORIZA)");
			builder.append(equalInt);
			parametros.add(beanReqConsOperaciones.getUsuarioAutoriza());
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getEstatusOperacion()) && !("Seleccione".equals(beanReqConsOperaciones.getEstatusOperacion()))){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(ESTATUS)");
			//Se controla el caso en que el usuario eliga la opcion de otro			
			if("Otro".equals(beanReqConsOperaciones.getEstatusOperacion())){
				builder.append("NOT IN ('PA', 'AU', 'CO', 'CA', 'RE', 'PV', 'LI', 'EN', 'DV')");
			}else{
				//Si no se selecciona otro se setea la opcion seleccionada
				builder.append(equalInt);
				parametros.add(beanReqConsOperaciones.getEstatusOperacion());				
			}
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getTemplate())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(ID_TEMPLATE)");
			builder.append(equalInt);
			parametros.add(beanReqConsOperaciones.getTemplate());
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getFechaCaptura())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRUNC(FCH_CAPTURA)");
			builder.append(" = TO_DATE(?, 'dd-MM-yyyy')");
			parametros.add(beanReqConsOperaciones.getFechaCaptura());
		}
		//Se revisa si es usuario administrador o supervisor		
		BeanResBase beanResAdminSupervisor=consultarAdminOSupervisor(session);
		if(!Errores.CODE_SUCCESFULLY.equals(beanResAdminSupervisor.getCodError())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(USR_MODIFICA)");
			builder.append(equalInt);
			parametros.add(session.getUsuario());
		}
		
		//Se regresa el string
		return builder.toString();
	}
	
	/**
	 * Adds the and.
	 *
	 * @param builder the builder
	 * @param isFirst the is first
	 * @return true, if successful
	 */
	//Funcion encargada de agregar where o and dependiente si es la primer validacion o no
	private boolean addAnd(StringBuilder builder, boolean isFirst){
		boolean result = isFirst;
		if(result){
			//Cuando ya no es el primer filtro se agrega where
			builder.append(" WHERE ");
			result=false;
		}else{
			//Cuando es el primer filtro se agrega and
			builder.append(" AND ");
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#consultarOperacionesApartadas(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	//Funcion que consulta los folios apartados
	@Override
	public BeanResConsOperaciones consultarFoliosApartados(ArchitechSessionBean session, BeanReqConsOperaciones req) {
		BeanResConsOperaciones res=new BeanResConsOperaciones();
		final List<BeanConsOperaciones> listaBeanConsOperaciones = new ArrayList<BeanConsOperaciones>();
		List<Object> parametros = new ArrayList<Object>();
		
		//Se crean los objetos necesarios para ejecutar el query
		Utilerias utilerias = Utilerias.getUtilerias();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	    	//Consulta a la base de datos
	    	 String query=QUERY_CONSUL_OPER_APART;
    		 parametros.add(session.getUsuario());
	    	 
	    	 if(req != null){
	    		 query += armaConsultaParametrizada(parametros,req,session, true);
	    	 }
	    	 
	         responseDTO = HelperDAO.consultar(query,parametros,
	        		 HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	            list = responseDTO.getResultQuery();
	            //Se agrega un bean con el folio que se eliminara
	            for(HashMap<String,Object> map:list){
	            	BeanConsOperaciones bean = new BeanConsOperaciones();
	            	bean.setFolio(utilerias.getString(map.get("ID_FOLIO")));
	            	listaBeanConsOperaciones.add(bean);
	            }
	            res.setCodError(Errores.CODE_SUCCESFULLY);
	            res.setTipoError(Errores.TIPO_MSJ_INFO);
	         }else{
	        	 res.setCodError(Errores.ED00011V);
	        	 res.setMsgError(Errores.DESC_ED00011V);
	        	 res.setTipoError(Errores.TIPO_MSJ_INFO); 
	         }
	      } catch (ExceptionDataAccess e) {
		    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
	         showException(e);
	         res.setCodError(Errores.EC00011B);
	         res.setMsgError(Errores.DESC_EC00011B);
	         res.setTipoError(Errores.TIPO_MSJ_ERROR);
	      }
	      //Se setea la lista de beanconsoperaciones
	      res.setListBeanConsOperaciones(listaBeanConsOperaciones);
	      return res;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOConsultaOperaciones#apartarFolio(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones)
	 */
	//Funciona que aparta o desparta un folio
	@Override
	public BeanResBase apartarDesapartarFolio(ArchitechSessionBean sessionBean, BeanConsOperaciones beanConsOperaciones, Boolean apartando){
		
		//Se crean los objetos necesarios para ejecutar el query
		BeanResBase res=new BeanResBase();
		String usuario = sessionBean.getUsuario();
		String folio = beanConsOperaciones.getFolio();
		try{
			//Consulta a la base de datos
			if(apartando){
				String query = QUERY_APARTAR_OPERACION;
				HelperDAO.actualizar(query, Arrays.asList(new Object[] { usuario,
						folio}), HelperDAO.CANAL_GFI_DS, this
						.getClass().getName());
				//Se devuelve codigo de error exitoso ya que no se necesita procesar nada mas
				res.setCodError(Errores.CODE_SUCCESFULLY);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				String query=QUERY_DESAPARTAR_OPERACION;
				HelperDAO.actualizar(query, Arrays.asList(
						new Object[]{usuario, folio}
						),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				//Se devuelve codigo de error exitoso ya que no se necesita procesar nada mas
				res.setCodError(Errores.CODE_SUCCESFULLY);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}
		} catch (ExceptionDataAccess e) {
	    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
			res.setCodError(Errores.EC00011B);
			res.setMsgError(Errores.DESC_EC00011B);
			res.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones#consultarOperacionesApartadas(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	//Funcion que elimina un folio
	@Override
	public BeanResBase eliminarFolio(ArchitechSessionBean sessionBean, BeanConsOperaciones beanConsOperaciones){
		
		//Se crean los objetos necesarios para ejecutar el query
		BeanResBase res=new BeanResBase();
		List<Object> parametros = new ArrayList<Object>();
	      try{
	    	//Consulta a la base de datos
	    	 String query=QUERY_ELIMINAR_FOLIO;
	    	 parametros.add(beanConsOperaciones.getFolio());
	         HelperDAO.eliminar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         res.setCodError(Errores.CODE_SUCCESFULLY);
	         res.setTipoError(Errores.TIPO_MSJ_INFO);
	      } catch (ExceptionDataAccess e) {
		    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
	         showException(e);
	         res.setCodError(Errores.EC00011B);
	         res.setMsgError(Errores.DESC_EC00011B);
	         res.setTipoError(Errores.TIPO_MSJ_ERROR);
	      }
	      return res;
	}
	
	/**
	 * Consultar admin o supervisor.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res base
	 */
	//Funcion que consulta si el usuario es adminsitrador o supervisor
	private BeanResBase consultarAdminOSupervisor(ArchitechSessionBean sessionBean){
		
		//Se crean los objetos necesarios para ejecutar el query
		BeanResBase res=new BeanResBase();
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;

		List<Object> parametros = new ArrayList<Object>();
	      try{
	    	//Consulta a la base de datos
	    	 String query=QUERY_ADMIN_SUPERVISOR;
	    	 parametros.add(sessionBean.getUsuario());
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());

			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
			}
	      } catch (ExceptionDataAccess e) {
		    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
	         showException(e);
	         res.setCodError(Errores.EC00011B);
	         res.setMsgError(Errores.DESC_EC00011B);
	         res.setTipoError(Errores.TIPO_MSJ_ERROR);
	      }
	      
	      //Se valida que la lista no sea nula ni venga vacia
	      if(list != null && !list.isEmpty()){
				for(HashMap<String,Object> map:list){
					Integer num=utilerias.getInteger(map.get("CONT"));
					if(num > 0){
						res.setCodError(Errores.CODE_SUCCESFULLY);
						res.setTipoError(Errores.TIPO_MSJ_INFO);
					}
				}
	      }
	      
	      return res;
	}
	

    /**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     *
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @param req the req
     * @param beanResConsOperaciones the bean res cons operaciones
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	//Funcion que se encarga de generar el registro para exportar los registros
    public BeanResBase exportarTodo(ArchitechSessionBean sessionBean, BeanReqConsOperaciones req, BeanResConsOperaciones beanResConsOperaciones) {
    	
		//Se crean los objetos necesarios para ejecutar el query
    	BeanResBase res  = new BeanResBase();
        ResponseMessageDataBaseDTO responseDTO = null;
        Utilerias utilerias = Utilerias.getUtilerias();
        
        //Se genera el nombre del archivo para exportar
        Date fecha = new Date();
        StringBuilder builder = new StringBuilder();
        String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        String nombreArchivo = "CAPTURAS_MANUALES_CONSULTA_OPERACIONES_".concat(fechaActual);
        nombreArchivo = nombreArchivo.concat(".csv");

        builder.append(QUERY_CONSULTA_EXPORTAR_TODO);
        builder.append(generarConsultaExportarTodo(req, sessionBean));

        try{
        	//Consulta a la base de datos
        	responseDTO = HelperDAO.insertar(QUERY_INSERT_CONSULTA_EXPORTAR_TODO,
      			   Arrays.asList(new Object[]{
      					   sessionBean.getUsuario(),
      					   sessionBean.getIdSesion(),
      					   "CAPT_MAN",
      					   "CONSULTA_OPERACIONES",
      					   nombreArchivo,
      					   req.getTotalReg(),
      					   "PE",
      					   builder.toString(),
      					 COLUMNAS_CONSULTA_CAPTURAS_MANUALES}),
      			   HelperDAO.CANAL_GFI_DS, this.getClass().getName());
        	beanResConsOperaciones.setNombreArchivo(nombreArchivo);
        	res.setCodError(responseDTO.getCodeError());
        	res.setMsgError(responseDTO.getMessageError());
        }catch (ExceptionDataAccess e) {
	    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
        	showException(e);
        	res.setCodError(Errores.EC00011B);
	         res.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return res;
    }

    /**
     * Metodo para generar la consulta de exportar todo.
     *
     * @param beanReqConsOperaciones the bean req cons operaciones
     * @param session the session
     * @return String Objeto del tipo @see String
     */
    //Funcion que se encarga de crear el query para exportar los registros
    public String generarConsultaExportarTodo(BeanReqConsOperaciones beanReqConsOperaciones, ArchitechSessionBean session){
    	StringBuilder builder = new StringBuilder();
		String vacio = "";
		String equal=" = ";
		boolean isFirst=true;

		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getUsuarioCaptura())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(CVE_USUARIO_CAP)");
			builder.append(equal);
			builder.append("'"+beanReqConsOperaciones.getUsuarioCaptura()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getUsuarioAutoriza())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(USR_AUTORIZA)");
			builder.append(equal);
			builder.append("'"+beanReqConsOperaciones.getUsuarioAutoriza()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getEstatusOperacion()) && !("Seleccione".equals(beanReqConsOperaciones.getEstatusOperacion()))){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(ESTATUS)");
			//Se controla el caso en que el usuario eliga la opcion de otro
			if("Otro".equals(beanReqConsOperaciones.getEstatusOperacion())){
				builder.append("NOT IN ('PA', 'AU', 'CO', 'CA', 'RE', 'PV', 'LI', 'EN', 'DV')");
			}else{
				//Si no se selecciona otro se setea la opcion seleccionada
				builder.append(equal);
				builder.append("'"+beanReqConsOperaciones.getEstatusOperacion()+"'");
			}
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getTemplate())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(ID_TEMPLATE)");
			builder.append(equal);
			builder.append("'"+beanReqConsOperaciones.getTemplate()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(beanReqConsOperaciones.getFechaCaptura())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRUNC(FCH_CAPTURA)");
			builder.append(" = TO_DATE(");
			builder.append("'"+beanReqConsOperaciones.getFechaCaptura()+"'");
			builder.append(", 'dd-MM-yyyy')");
		}
		//Se revisa si es usuario administrador o supervisor
		BeanResBase beanResAdminSupervisor=consultarAdminOSupervisor(session);
		if(!Errores.CODE_SUCCESFULLY.equals(beanResAdminSupervisor.getCodError())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(USR_MODIFICA)");
			builder.append(equal);
			builder.append("'"+session.getUsuario()+"'");
		}
		//Se agrega el orden
		builder.append(QUERY_BUSQUEDA_ORDER);
		
		return builder.toString();
    }

}
