/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCuentasFiduciarioImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     17/09/2019 01:49:09 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCuentasFideico;

/**
 * Class DAOCuentasFiduciarioImpl.
 *
 * Clase que implementa la interfaz DAO para aplicar
 * los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCuentasFiduciarioImpl extends Architech implements DAOCuentasFiduciario {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -561198126795088160L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante CONSULTA_ALL. */
	private static final String CONSULTA_TODO = "SELECT DISTINCT "
			.concat("NUM_CUENTA, ")
			.concat("TOTAL, ")  
			.concat("RN ") 
			.concat("FROM (") 
			.concat("SELECT DISTINCT ") 
			.concat("NUM_CUENTA, ") 
			.concat("(SELECT COUNT(1) AS CONT FROM TRAN_CTAS_FIDUCIARIO [FILTRO_WH]) AS TOTAL, ") 
			.concat("ROWNUM AS RN ") 
			.concat("FROM TRAN_CTAS_FIDUCIARIO [FILTRO_WH]) ");
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
		
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasCatalogos utilerias = UtileriasCatalogos.getInstancia();
	
	/** La variable que contiene informacion con respecto a: utilerias cuentas. */
	private static UtileriasCuentasFideico utileriasCuentas = UtileriasCuentasFideico.getInstancia();
	
	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador objeto --> paginador --> Objeto para realizar el paginado
	 * @param session objeto --> bean sesion --> El objeto session
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 */
	@Override
	public BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanCuentas response = new BeanCuentas();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanCuenta> cuentas = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utileriasCuentas.getFiltro(beanCuenta, "AND");
		String filtroWhere = utileriasCuentas.getFiltro(beanCuenta, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				cuentas = new ArrayList<BeanCuenta>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanCuenta cuenta = new BeanCuenta();
					/** Seteo de datos **/
					cuenta.setCuenta(utilerias.getString(map.get("NUM_CUENTA")));
					/** Se añade el objeto a la lista**/
					cuentas.add(cuenta);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setCuentas(cuentas);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}
	
}
