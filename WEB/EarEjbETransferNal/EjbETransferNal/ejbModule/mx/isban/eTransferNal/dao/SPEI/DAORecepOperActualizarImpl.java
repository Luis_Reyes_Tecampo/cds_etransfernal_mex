/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORecepOperActualizarImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   3/10/2018 01:33:22 PM Modificado:Lily Marlenne Angeles Bravo. VectorFSW Modificado
 */

package mx.isban.eTransferNal.dao.SPEI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion
 * para la consulta de Movimientos CDAs.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperActualizarImpl  extends Architech implements DAORecepOperActualizar{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -8167141597887988495L;
	
	/** Propiedad del tipo String que almacena el valor de QUERY_INSERT. */
	private static final String QUERY_INSERT =
		" INSERT INTO TRAN_SPEI_HORAS_MAN (FOLIO, TIPO, FCH_OPERACION, CVE_INST_ORD, CVE_MI_INSTITUC, FOLIO_PAQUETE, FOLIO_PAGO, REFERENCIA, FCH_CAPTURA, TIPO2 ) "+
		" VALUES (folio_man.Nextval,'N',TO_DATE(?,'dd-mm-yyyy'), ?, ?, ?, ?, ?, TO_DATE(?,'dd-mm-yyyy HH24:MI:SS'), 'A' ) ";
	
	
	/**
     * Actualiza trans spei rec.
     *
     * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
     * @param architechSessionBean El objeto: architech session bean
     * @param historico El objeto: historico
     * @return Objeto bean BeanResBase
     */
    @Override
    public BeanResBase actualizaTransSpeiRec(BeanTranSpeiRecOper beanReqActTranSpeiRec,
   		 ArchitechSessionBean architechSessionBean,boolean historico){
    	/**incializan las variables **/
    	BeanResBase beanResBase = new BeanResBase();
    	Utilerias util = Utilerias.getUtilerias();
    	String query= ConstantesRecepOperacionApartada.QUERY_UPDATE_RECUPERAR_DIA;
    	/**realiza la consulta **/
    	if(historico) {				
    		query =ConstantesRecepOperacionApartada.QUERY_UPDATE_RECUPERAR_HTO;
		}
    	
       try{
    	    //se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			//se asigna los datos para el parametro
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getError().getCodError());
			//se formatea el motivo de devolucion
			parametros.add(util.formateaStringAEntero(beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol(),ConstantesRecepOperacion.CEROS));
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago());
			
			
    	   /**inicializa la consulta **/
          HelperDAO.actualizar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
          beanResBase.setCodError(Errores.OK00000V);
       } catch (ExceptionDataAccess e) {
    	   /**en caso de error **/
          showException(e);
          beanResBase.setCodError(Errores.EC00011B);
          beanResBase.setMsgError(Errores.DESC_EC00011B);
       }
       /**regresa el objeto **/
       return beanResBase;
    }

    /**
     * Actualiza motivo rec.
     *
     * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
     * @param architechSessionBean El objeto: architech session bean
     * @param pantalla El objeto: pantalla
     * @return Objeto bean res act tran spei rec DAO
     */
    @Override
    public BeanResBase actualizaMotivoRec(BeanTranSpeiRecOper beanReqActTranSpeiRec, ArchitechSessionBean architechSessionBean, String pantalla){
    	/**inicializa las varaibles **/
    	BeanResBase beanResBase = new BeanResBase();
    	Utilerias util = Utilerias.getUtilerias();    	
    	String query = ConstantesRecepOperacionApartada.QUERY_UPDATE_MOTIVO;
    	
    	if(pantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO) 
    			|| pantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_HTO)) {
    		 /**inicializa la consulta **/
    		query = query.replace(ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI, ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI_HTO);
		}	
    	
    	
    	/**realiza la consulta **/
        try{
        	//se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			//se asigna los datos para el parametro
			parametros.add(util.formateaStringAEntero(beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol(),ConstantesRecepOperacion.CEROS));
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago());
			
        	 /**inicializa la consulta **/
           HelperDAO.actualizar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           //asigna el ensaje de existo
           beanResBase.setCodError(Errores.OK00000V);
           beanResBase.setTipoError(Errores.TIPO_MSJ_INFO);
        } catch (ExceptionDataAccess e) {
        	/**en caso de error **/
           showException(e);
           beanResBase.setCodError(Errores.EC00011B);
           beanResBase.setTipoError(Errores.TIPO_MSJ_ERROR);
        }
        /**regresa el objeto **/
        return beanResBase;
     }

    /**
     * Rechazar.
     *
     * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
     * @param architechSessionBean El objeto: architech session bean
     * @param historico El objeto: historico
     * @return Objeto beanResBase
     */
    @Override
    public BeanResBase rechazar(BeanTranSpeiRecOper beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean,boolean historico){
    	/**iniccializa las variables **/
    	BeanResBase bean = new BeanResBase();
    	Utilerias util = Utilerias.getUtilerias();
    	String query;
    	if(historico) {
    		 /**inicializa la consulta **/
    		query = ConstantesRecepOperacionApartada.QUERY_UPDATE_MOTIVO_RECHAZO_HTO;
		}	else {
			 /**inicializa la consulta **/
    		query = ConstantesRecepOperacionApartada.QUERY_UPDATE_MOTIVO_RECHAZO;
		}
    	/**realiza la consulta **/
        try{
        	//se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			//se asigna los datos para el parametro
			parametros.add(util.formateaStringAEntero(beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol(),ConstantesRecepOperacion.CEROS));
			parametros.add(beanReqActTranSpeiRec.getEstatus());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago());
					
        	 /**inicializa la consulta **/
           HelperDAO.actualizar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           //asigna el codigo de exito
           bean.setCodError(Errores.OK00000V);
           bean.setTipoError(Errores.TIPO_MSJ_INFO);
        } catch (ExceptionDataAccess e) {
        	/**en caso de error **/
           showException(e);
           //asigna el codigo de error
           bean.setCodError(Errores.EC00011B);          
           bean.setTipoError(Errores.TIPO_MSJ_ERROR);
        }
        /**regresa el objeto **/
        return bean;
     }

    /**
     * Guarda tran spei horas man.
     *
     * @param beanReqActTranSpeiRec bean del tipo BeanReqActTranSpeiRec
     * @param architechSessionBean Objeto de ArchitechSessionBean
     */
    @Override
    public BeanResGuardaInstDAO guardaTranSpeiHorasMan(BeanTranSpeiRecOper beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
    	/**iniciliza la variable **/
          final BeanResGuardaInstDAO beanResGuardaInstDAO = new BeanResGuardaInstDAO();
          ResponseMessageDataBaseDTO responseDTO = null;
           try{
				//se crea elobjeto para pasar los parametros
				List<Object> parametros = new ArrayList<Object>();
				//se asigna los datos para el parametro
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer());
				parametros.add(beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getFchCaptura());
				  			
				/**inicializa la consulta **/
				responseDTO = HelperDAO.insertar(QUERY_INSERT,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				//se asigna codigos de error
				beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
				beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());

          } catch (ExceptionDataAccess e) {
        	  /**en caso de error **/
             showException(e);
             //se asigna codigos de error
             beanResGuardaInstDAO.setCodError(Errores.EC00011B);
             beanResGuardaInstDAO.setMsgError(Errores.DESC_EC00011B);
          }
          
          return beanResGuardaInstDAO;
       }

   
    
	 
	 /**
		 * Consulta error.
		 *
		 * @param architechSessionBean El objeto: architech session bean
		 * @param codigoError El objeto: codigo error
		 * @return Objeto bean res base
		 */
		@Override
		public BeanResBase consultaError(ArchitechSessionBean architechSessionBean, String  codigoError){
			BeanResBase beanResBase= new BeanResBase();
		      List<HashMap<String,Object>> list = null;
		      ResponseMessageDataBaseDTO responseDTO = null;
		      Utilerias utilerias = Utilerias.getUtilerias();
		      /**se asigna el valor del codigo de error que se trae en al funcion **/
		      List<Object> parametros = new ArrayList<Object>();
		      parametros.add(codigoError);
		      try {
		    	  /**se genera la consulta para obtener el mensaje de la tabla comun_error**/
		    	responseDTO = HelperDAO.consultar(ConstantesRecepOperacion.CONSULTA_ERROR, parametros, 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		    	/**se asignan los valores obtenidos**/
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					list = responseDTO.getResultQuery();
					for(HashMap<String,Object> map:list){					
						beanResBase.setCodError(utilerias.getString(map.get("COD_ERROR")));
						beanResBase.setMsgError(utilerias.getString(map.get("MSG_ERROR")));
		  		  }
				} else {
					beanResBase.setCodError(codigoError);
					beanResBase.setMsgError(String.format("No se encontr\u00f3 mensaje para el c\u00f3digo [%s]", codigoError));
				}
			} catch (ExceptionDataAccess e) {
		        showException(e);
		        beanResBase.setCodError(Errores.EC00011B);
		        beanResBase.setMsgError(Errores.DESC_EC00011B);
			}
		      return beanResBase;
		 }

}