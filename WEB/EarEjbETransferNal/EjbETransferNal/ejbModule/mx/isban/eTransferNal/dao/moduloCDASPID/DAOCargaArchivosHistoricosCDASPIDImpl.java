/**
 * Clase: DAO-Imp Clase que implementa la interfaz para la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre 2016
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.EnumQuerysModuloCADSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase DAOCargaArchivosHistoricosCDASPIDImpl.
 */
//Clase DAOCargaArchivosHistoricosCDASPIDImpl
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCargaArchivosHistoricosCDASPIDImpl 
		extends Architech implements DAOCargaArchivosHistoricosCDASPID{

	/**
	 * La variable de serializacion
	 */
	//La variable de serializacion
	private static final long serialVersionUID = -7874453427365588368L;

	//Metodo que se encarga de obtener el estatus del archivo
	@Override
	public BeanResMonitorCargaArchHistCADSPID obtenerEstatusArchivo(
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean) {
		debug("Iniciando la capa de Datos para la pantalla: " +
			"Monitor Carga Archivos Historicos CADSPID");
		
		//Se crean objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta = 
				new BeanResMonitorCargaArchHistCADSPID();
		
		//Se crean objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String, Object>> list = null;
		List<BeanMonitorCargaArchHistCADSPID> lista = 
				new ArrayList<BeanMonitorCargaArchHistCADSPID>();
		
		//Se obtiene el query correspondiente del enum
		EnumQuerysModuloCADSPID query = EnumQuerysModuloCADSPID.QUERY_PANT_CARG_ARCH_HIST_CADSPID;
		String queryConsulta = query.getQuery().toString();
		
		//Se agregan los parametros para el query
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(paginador.getRegIni().toString());
		parametros.add(paginador.getRegFin().toString());
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			//Se ejecuta el query
			responseDTO = HelperDAO.consultar(queryConsulta, parametros, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//Se valida que la lista no sea nula y que traiga datos
			if(responseDTO.getResultQuery()!=null &&
					!responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				//Si la lista trae datos se mapean
				respuesta.setListaResultado(llenarRegistros(
						list, lista, respuesta, 
						utilerias));
			}
			
			//Se setea la respuesta del responseDTO al objeto respuesta
			respuesta.setCodigoError(responseDTO.getCodeError());
			respuesta.setMensajeError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess eda) {
			//Si existe un error se setea el codigo EC00011B
			error(String.format("Ha ocurrido un error al ejecutar el Query: %s", queryConsulta));
			showException(eda);
			respuesta.setCodigoError(Errores.EC00011B);
			respuesta.setMensajeError(Errores.TIPO_MSJ_ERROR );
		}
		
		//Se devuelve objeto respuesta
		return respuesta;
	}

	/**
	 * Metodo para llenar la lista de respuesta
	 * 
	 * @param list El parametro list
	 * @param lista El parametro lista
	 * @param respuesta El parametro respuesta
	 * @param utilerias El parametro utilerías
	 * @return La respuesta generada
	 */
	//Metodo para llenar la lista de respuesta
	private List<BeanMonitorCargaArchHistCADSPID> llenarRegistros(
			
			//Se crean los objetos de apoyo
			List<HashMap<String, Object>> list, 
			List<BeanMonitorCargaArchHistCADSPID> lista,
			BeanResMonitorCargaArchHistCADSPID respuesta,
			Utilerias utilerias) {
		
		//Se recorre cada elemento de la lista
		for(HashMap<String, Object> temp : list) {
			//Se mapea el bean datoC a partir del diccionario
			BeanMonitorCargaArchHistCADSPID datoC =
					new BeanMonitorCargaArchHistCADSPID();
			datoC.setIdArchivo(utilerias.getString(temp.get("ID_PETICION")));
			datoC.setNombreArchivo(utilerias.getString(temp.get("NOMBRE_ARCHIVO")));
			datoC.setEstatusArchivo(utilerias.getString(temp.get("ESTATUS")));
			lista.add(datoC);
			//Se setea el numero de registros obtenidos
			respuesta.setRegistrosTotales(
					utilerias.getInteger(
							temp.get("CONT")));
		}
		
		//Se regresa la lista de objetos ya mapeados
		return lista;
	}

	

}
