/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOPagosImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:28:07 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasPagos;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class DAOPagosImpl.
 *
 * Clase tipo DAO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * de accceso a los datos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOPagosImpl extends Architech implements DAOPagos{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2592353063019330993L;
	
	/** La constante CVE_INTERME_ORD. */
	private static final String CVE_INTERME_ORD= "CVE_INTERME_ORD";
	
	/** Constante FROM de uso comun en queries. */
	private static final String FROM= "FROM (";
	
	/** CAMPO CVE_MECANISMO. */
	private static final String CVE_MECANISMO = "TM.CVE_MECANISMO";
	
	/** CAMPO ESTATUS. */
	private static final String ESTATUS = "TM.ESTATUS";
	
	/** Constante WHERE para uso comun en queries. */
	private static final String WHERE = "WHERE ";
	
	/** Constante AND para uso comun en queries. */
	private static final String AND = "AND ";
	
	/** Constante IN para uso comun en queries. */
	private static final String IN = " IN [";
	
	/** Constante de llave para uso comun en queries. */
	private static final String OPEN_SP = " = '[";
	
	/** Constante de cierre de llave para uso comun en queries. */
	private static final String CLOSE_SP = "]' ";
	
	/** La constante CONST_COUNT. */
	private static final String CONST_COUNT = "SELECT COUNT(1) CONT ";
	
	/** La constante CONST_CONTADOR. */
	private static final String CONST_CONTADOR = ") CONTADOR, TRAN_MENSAJE TM, [TB_EXTRA] TMS   ";
	
	/** La constante CONST_MECANISMO. */
	private static final String CONST_MECANISMO = "\\[TM.CVE_MECANISMO\\]";
	
	/** La constante CONST_ESTATUS. */
	private static final String CONST_ESTATUS = "\\[TM.ESTATUS\\]";
	
	/** La constante CONST_TABLE. */
	private static final String CONST_TABLE = "\\[TB_EXTRA\\]";
	
	/** La constante ESPACIO. */
	private static  final String ESPACIO = "";
	
	/** La constante CONSULTA_ORDENES. */
	private static final StringBuilder CONSULTA_ORDENES = 
			new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") 
			    .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") 
			    .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") 
			    .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        
			    .append(FROM) 
			       .append("SELECT TM.*, ROWNUM AS RN ")
			        .append(FROM)
			            .append("SELECT TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO,")
			            .append("TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC,")
			            .append("TM.IMPORTE_ABONO, ").append(ESTATUS).append(", TM.COMENTARIO3, TM.REFERENCIA_MECA, CONTADOR.CONT ") 
			            .append(FROM)
			                .append(CONST_COUNT)
			            .append("FROM TRAN_MENSAJE TM, [TB_EXTRA] TMS ")
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
		            .append("AND TM.REFERENCIA = TMS.REFERENCIA [PARAM_ORIGEN]")
			        .append(") CONTADOR, TRAN_MENSAJE TM, [TB_EXTRA] TMS ")
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			        .append("AND TM.REFERENCIA = TMS.REFERENCIA [PARAM_ORIGEN]")
			      .append(") TM ")
			     .append(")TM ")
			    .append("WHERE RN BETWEEN ? AND ? ");
	

	/** La constante CONSULTA_ORDENES_SUMATORIA. */
	private static final StringBuilder CONSULTA_ORDENES_SUMATORIA = 
			new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
			            .append(FROM)
			                .append(CONST_COUNT)
			            .append("FROM TRAN_MENSAJE TM, [TB_EXTRA] TMS  ")
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			        .append(CONST_CONTADOR)
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP).append(" AND TM.REFERENCIA = TMS.REFERENCIA ");
			     
	/** La constante CONSULTA_DETALLE_PAGO. */
	private static final StringBuilder CONSULTA_DETALLE_PAGO =
			 new StringBuilder().append("SELECT TM.REFERENCIA, TM.ESTATUS, TM.CVE_EMPRESA, TM.CVE_MEDIO_ENT, TM.CVE_USUARIO_CAP, ")
			 					.append("TM.REFERENCIA_CON, TM.CVE_TRANSFE, TM.CVE_PTO_VTA, TM.REFERENCIA_MED, TM.REFERENCIA_MECA, ")
			 					.append("TM.FORMA_LIQ, TM.CVE_COLA, TM.CVE_OPERACION, TM.CVE_USUARIO_SOL, TM.CVE_INTERME_ORD, TM.CVE_PTO_VTA_ORD, ")
			 					.append("TM.NUM_CUENTA_ORD, TM.CVE_DIVISA_ORD, TM.IMPORTE_CARGO, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")
			 					.append("TM.CVE_PTO_VTA_REC,  TM.CVE_DIVISA_REC, TM.NUM_CUENTA_REC, TM.CVE_SWIFT_COR, TM.COMENTARIO1, TM.COMENTARIO2, ")
			 					.append("TM.COMENTARIO3, TM.SUCURSAL_BCO_REC, TM.CVE_DEVOLUCION, TMS.MOTIVO_DEVOL, TMS.FCH_INSTRUC_PAGO, TMS.HORA_INSTRUC_PAGO, TMS.REFE_NUMERICA, ")
			 					.append("CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TMS.FCH_CAPTURA, TMS.CONCEPTO_PAGO, TMS.CVE_RASTREO, TMS.DIRECCION_IP, ")
			 					.append("TMS.TIPO_CUENTA_ORD, TMS.COD_POSTAL_ORD, TMS.FCH_CONSTIT_ORD, TMS.RFC_ORD, TMS.NOMBRE_ORD, ")
			 					.append("TMS.DOMICILIO_ORD, TMS.TIPO_CUENTA_REC, TMS.RFC_REC AS RFC_REC, TMS.NOMBRE_REC, TMS.CONCEPTO_PAGO  AS CONCEPTO_PAGO ")
			 					.append("FROM TRAN_MENSAJE TM LEFT JOIN [TB_EXTRA] TMS ON TM.REFERENCIA = TMS.REFERENCIA ")
			 					.append("WHERE TM.REFERENCIA = ? ");
	
	

	private static final UtileriasPagos utils = UtileriasPagos.getUtils();
	
	/** La constante TB_SPEI. */
	private static final String TB_SPEI = "TRAN_SPEI_ENV";
	
	/** La constante TB_SPID. */
	private static final String TB_SPID = "TRAN_MENSAJE_SPID";
	
	/** La constante MONTO_TOTAL. */
	private static final String MONTO_TOTAL = "MONTO_TOTAL";
	
	/**
	 * Obtener listado pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo El objeto: modulo
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param beanPaginador El objeto: bean paginador
	 * @param session El objeto: session
	 * @return Objeto bean res listado pagos DAO
	 */
	@Override
	public BeanResListadoPagosDAO obtenerListadoPagos(BeanModulo modulo, String cveMecanismo, String estatus,
			BeanPaginador beanPaginador, ArchitechSessionBean session) {
		/** Se crea el objeto BeanResListadoPagosDAO*/
		final BeanResListadoPagosDAO beanResListadoPagosDAO = new BeanResListadoPagosDAO();
		/** Se declara el objeto List<HashMap<String,Object>>*/
		List<HashMap<String,Object>> list = null;
		/** Se crea el objeto utilerias*/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	
    	/** Se creal el objeto ResponseMessageDataBaseDTO*/
    	ResponseMessageDataBaseDTO responseDTO = null;
    	/** Se crea lista List<BeanPago>  */
    	List<BeanPago> listaBeanPagos = new ArrayList<BeanPago>();
    	
    	try {
    		/** se arma el query*/
    		String query = CONSULTA_ORDENES.toString().replaceAll(CONST_MECANISMO, cveMecanismo).replaceAll(CONST_ESTATUS, estatus);
    		/** Se obtiene el origen*/
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		query = query.replaceAll("\\[PARAM_ORIGEN\\]", origen);
    		/** se valique query*/
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		
    		/** se obtiene consulta */
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, TB_SPEI);
    		} else {
    			query = query.replaceAll(CONST_TABLE, TB_SPID);
    		}
    		
    		/** Se declara variable params*/
    		List<Object> params = new ArrayList<Object>();
    		
    		/** se setea los parametros*/
    		params.add(beanPaginador.getRegIni());
    		params.add( beanPaginador.getRegFin());
    		/** se ejecuta la consulta*/
        	responseDTO = HelperDAO.consultar(query, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	
        	/** Se valida el resultado de la consulta*/
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		/** lista de resultadi*/
        		 list = responseDTO.getResultQuery();
        		 /** Valida si la lista esta vacia*/
        		 if(!list.isEmpty()){
        			 listaBeanPagos = utils.llenarListadoPagos(beanResListadoPagosDAO, list, utilerias);
        			 }
        		 }
        	/** se Setean los codigos de error en el */
        	/** response al igual que la lista bean de pagos*/
        	beanResListadoPagosDAO.setListBeanPago(listaBeanPagos);
        	beanResListadoPagosDAO.setCodError(responseDTO.getCodeError());
        	beanResListadoPagosDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		/** Ocurrio un error*/
    		showException(e);
    		beanResListadoPagosDAO.setCodError(Errores.EC00011B);
    		beanResListadoPagosDAO.setMsgError(Errores.DESC_EC00011B);
    	}
    	/** Return response*/
		return beanResListadoPagosDAO;
	}

	/**
	 * Buscar pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo El objeto: modulo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param paginador El objeto: paginador
	 * @param session El objeto: session
	 * @return Objeto bean res listado pagos DAO
	 */
	@Override
	public BeanResListadoPagosDAO buscarPagos(BeanModulo modulo, String field, String valor, String cveMecanismo,
			String estatus, BeanPaginador paginador, ArchitechSessionBean architechSessionBean) {
		
		/** Se crea el objeto BeanResListadoPagosDAO tipo final*/
		final BeanResListadoPagosDAO beanResListadoPagos = new BeanResListadoPagosDAO();
		/** se crea el objeto List<HashMap<String,Object>>  */
		List<HashMap<String,Object>> list = null;
		/** se crea el objeto utilerias*/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	/** Se crea el objeto responseDTO*/
    	ResponseMessageDataBaseDTO responseDTOBusPag = null;
    	/** se crea el objeto listaBeanPagos*/
    	List<BeanPago> listaBeanPagos = new ArrayList<BeanPago>();
    	
    	try {         
    		/** se crea variable queryCo*/
    		StringBuilder queryCo = new StringBuilder();
    		/** se crea variable queryCompleto*/
    		StringBuilder queryCompleto = new StringBuilder();
    		/** se agregan las condiciones*/
    		utils.condicionFields(field, valor, queryCo);
    		
    		/** Obtiene query completo*/
    		queryCompleto = utils.obtenerFormatoQuery(queryCo);
    		
    		/** se continua con armado de query*/
    		String query = queryCompleto.toString().replaceAll(CONST_MECANISMO, cveMecanismo).replaceAll(CONST_ESTATUS, estatus);
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		/** Se agregan parametros a query*/
    		query = query.replaceAll("\\[PARAM_ORIGEN\\]", origen);
    		/** Se obtiene query*/
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		/** Se valida cual es el modulo*/
    		/** para seleccionar el query correcto*/
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, TB_SPEI);
    		} else {
    			query = query.replaceAll(CONST_TABLE, TB_SPID);
    		}
    		/** se declara la variable params*/
    		List<Object> params = new ArrayList<Object>();
    		/** Seteando parametros de paginacion*/
    		params.add(paginador.getRegIni());
    		params.add( paginador.getRegFin());
    		/** Se ejecuta el query*/
    		responseDTOBusPag = HelperDAO.consultar(query, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTOBusPag.getResultQuery()!= null && !responseDTOBusPag.getResultQuery().isEmpty()){
        		 list = responseDTOBusPag.getResultQuery();
        		 /** Se valida resultado vacio*/
        		 if(!list.isEmpty()){
        			 listaBeanPagos = llenarPagos(beanResListadoPagos, list, utilerias);
        		 }
        	}
        	/** Seteando valores al response [listaBeanPagos]*/
        	beanResListadoPagos.setListBeanPago(listaBeanPagos);
        	/** Seteando valores al response [errores]*/
        	beanResListadoPagos.setCodError(responseDTOBusPag.getCodeError());
        	beanResListadoPagos.setMsgError(responseDTOBusPag.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		/** ocurrio un error*/
    		showException(e);
    		beanResListadoPagos.setCodError(Errores.EC00011B);
    		beanResListadoPagos.setMsgError(Errores.DESC_EC00011B);
    	}
    	/** return response*/
		return beanResListadoPagos;
	}

	
	
	/**
	 * Llenar pagos.
	 *
	 * @param beanResListadoPagosDAO El objeto: bean res listado pagos DAO
	 * @param list El objeto: list
	 * @param utilerias El objeto: utilerias
	 * @return Objeto list
	 */
	private List<BeanPago> llenarPagos(final BeanResListadoPagosDAO beanResListadoPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		/** Se declara variable zero*/
		Integer zeroPagos = Integer.valueOf(0);
		/** Se declara branPagos*/
		BeanPago pagos;
		/** Se declara listaBeanPagos*/
		List<BeanPago> listaBeanPagos;
		listaBeanPagos = new ArrayList<BeanPago>();
		 
		/** Se itera resultado*/
		 for(Map<String,Object> mapResult : list){
			 pagos = new BeanPago();
			 pagos.setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
			 pagos.setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
			 pagos.setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
			 pagos.setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
			 pagos.setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
			 pagos.setFechaCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
			 pagos.setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD)));
			 pagos.setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
			 pagos.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
			 pagos.setEstatus(utilerias.getString(mapResult.get("ESTATUS")));
			 pagos.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
			 pagos.setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
			 pagos.setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
			 /** Seteando cada bean en la lista*/
			 listaBeanPagos.add(pagos);
			 /** validando total de registros*/
			 if (zeroPagos.equals(beanResListadoPagosDAO.getTotalReg())) {
				 beanResListadoPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			 }
		 }
		 /** return lista response*/
		return listaBeanPagos;
	}

	/**
	 * Obtener listado pagos montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	@Override
	public BigDecimal obtenerListadoPagosMontos(BeanModulo modulo, String cveMecanismo, String estatus,
			ArchitechSessionBean session) {
		/** Se declara variable totalImporte*/
		BigDecimal totalImporte = BigDecimal.ZERO;
		/** se declara vairiable responseDTO*/
		ResponseMessageDataBaseDTO responseDTO = null;
    	
    	try {
    		/** Se arma query*/
    		String query = CONSULTA_ORDENES_SUMATORIA.toString().replaceAll(CONST_MECANISMO, cveMecanismo).replaceAll(CONST_ESTATUS, estatus);
    		/** Valida el modulo SPEI/SPID*/
    		/** Dependiendo e valor de la variable modulo, */
    		/** Se selecciona el query*/
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, TB_SPEI);
    		} else {
    			query = query.replaceAll(CONST_TABLE, TB_SPID);
    		}
    		/** valiadndo moduo/query*/
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		/** Se obtiene el origen*/
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		/** Se valida query*/
    		query = query.concat(origen);
    		/** se ejcuta consulta*/
        	responseDTO = HelperDAO.consultar(query,
        			Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	/** se valida resultado*/
        	final String validaTotal =  responseDTO.getResultQuery().get(0).get(MONTO_TOTAL).toString();
        	if (validaTotal != null && !"".equals(validaTotal)) {
        		totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get(MONTO_TOTAL);
        	}
    	} catch (ExceptionDataAccess e) {
    		/** ocurrio un error*/
    		error("Ocurrio un error al obtener el importe");
    		showException(e);
    	}
    	/** return totalImporte*/
		return totalImporte;
	}

	/**
	 * Buscar pagos montos.
	 *
	 * Buscar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	@Override
	public BigDecimal buscarPagosMontos(BeanModulo modulo, String field, String valor, String cveMecanismo,
			String estatus, ArchitechSessionBean session) {
		/** Se declara totalImporte*/
		BigDecimal totalImporte = BigDecimal.ZERO;
		/** Se declara responseDTO*/
		ResponseMessageDataBaseDTO responseDTO = null;
    	    	
    	try {              	
    		/** Se declara variable queryCo*/
    		StringBuilder queryCo = new StringBuilder();
    		/** se declara variabel queryCompleto*/
    		StringBuilder queryCompleto = new StringBuilder();
    		/** condicioes fields*/
    		utils.condicionFields(field, valor, queryCo);
    		/** Se arma query completo */
    		queryCompleto = utils.obtenerFormatoQueryMontos(queryCo);
    		/** Se obtiene query completo*/
    		String query = queryCompleto.toString().replaceAll(CONST_MECANISMO, cveMecanismo).replaceAll(CONST_ESTATUS, estatus);
    		/**  se valida modulo*/
    		/** SPEI/SPID*/
    		/** Depende del modulo se selecciona tabla */
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, TB_SPEI);
    		} else {
    			query = query.replaceAll(CONST_TABLE, TB_SPID);
    		}
    		/** Se valida Query/modulo*/
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		/** Se obtiene origen*/
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		/** Se concatena el query/origen*/
    		query = query.concat(origen);
    		/** Se ejcuta la consulta*/
        	responseDTO = HelperDAO.consultar(query, 
        			Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	/** Se obtiene total importe*/
        	totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get(MONTO_TOTAL);
        	
    	} catch (ExceptionDataAccess e) {
    		/** ocurrio un error*/
    		error("Ocurrio un error al obtener el importe");
    		showException(e);
    	}
    	/** return totalImporrte*/
		return totalImporte;
	}
	

	/**
	 * Consultar detalle pago.
	 *
	 * Consulta el detalle de pago 
	 *
	 * @param referencia El objeto: referencia
	 * @param modulo El objeto: modulo
	 * @param session El objeto: session
	 * @return Objeto bean detalle pago
	 */
	@Override
	public BeanDetallePago consultarDetallePago(String referencia, BeanModulo modulo, ArchitechSessionBean session) {
		/**  se declara beanDetallePago*/
		BeanDetallePago beanDetallePago = new BeanDetallePago();
		/** Se declara responseDTO*/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** se declara utilerias*/
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			/** Se declara query*/
			String query = CONSULTA_DETALLE_PAGO.toString();
			/** Se valida modulo/query*/
			if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, TB_SPEI).replaceAll("TMS.CONCEPTO_PAGO", "TMS.MONTO_COMISION")
    					.replaceAll("TMS.RFC_REC", "TMS.REFE_ADICIONAL1")
		    			.replaceAll("TMS.DOMICILIO_ORD,", ESPACIO)
		    			.replaceAll("TMS.RFC_ORD,", ESPACIO)
		    			.replaceAll("TMS.FCH_CONSTIT_ORD,", ESPACIO)
		    			.replaceAll("TMS.COD_POSTAL_ORD,", ESPACIO)
		    			.replaceAll("TMS.DIRECCION_IP,", ESPACIO)
		    			.replaceAll("TMS.HORA_INSTRUC_PAGO,", ESPACIO)
		    			.replaceAll("TMS.FCH_INSTRUC_PAGO,", ESPACIO);
    		} else {
    			query = query.replaceAll(CONST_TABLE, TB_SPID);
    		}
			/** Se declara params */
			final List<Object> params = new ArrayList<Object>();
			/** seteando parametros referencia*/
			params.add(referencia);
			/** Se ejecuta consulta*/
			responseDTO = HelperDAO.consultar(
					query, params, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** se itera resultado*/
			if(responseDTO.getResultQuery()!=null && !responseDTO.getResultQuery().isEmpty()) {
				List<HashMap<String, Object>> listaResultados = responseDTO.getResultQuery();
				Map<String, Object> mapaResultado = listaResultados.get(0);
				/** LLenar datos generales*/
					utils.llenarDatosGeneralesPago(beanDetallePago, utilerias, mapaResultado, modulo);				
			}
		}catch(ExceptionDataAccess e) {
			/** ocurrio un error*/
			showException(e);
			beanDetallePago.setCodError(Errores.EC00011B);
			beanDetallePago.setDescError(Errores.DESC_EC00011B);
		}
		/** return response bean*/
		return beanDetallePago;
	}
	
}
