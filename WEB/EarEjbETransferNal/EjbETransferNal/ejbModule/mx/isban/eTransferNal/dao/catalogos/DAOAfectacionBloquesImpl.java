/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOAfectacionBloquesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     22/07/2019 11:52:28 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.catalogos.BeanBloques;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasAfectacionBloques;

/**
 * Class DAOAfectacionBloquesImpl.
 *
 * Clase que contiene los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 22/07/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOAfectacionBloquesImpl extends Architech implements DAOAfectacionBloques{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1072885434899195597L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	

	/** La constante ORDER. */
	private static final String ORDER = " ORDER BY NUM_BLOQUE ASC";
	
	/** La constante CONSULTA_ALL. */
	private static final String CONSULTA_TODO = "SELECT DISTINCT "
			.concat("NUM_BLOQUE, ")
			.concat("VALOR, ")
			.concat("DESCRIPCION, ") 
			.concat("TOTAL, ")  
			.concat("RN ") 
			.concat("FROM (") 
			.concat("SELECT DISTINCT ") 
			.concat("NUM_BLOQUE, ") 
			.concat("VALOR,") 
			.concat("DESCRIPCION,") 
			.concat("(SELECT COUNT(1) AS CONT FROM TRAN_AFECTA_BLOQUES [FILTRO_WH]) AS TOTAL, ") 
			.concat("ROWNUM AS RN ") 
			.concat("FROM TRAN_AFECTA_BLOQUES [FILTRO_WH] "+ORDER+") ");
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_AFECTA_BLOQUES "
			.concat("WHERE TRIM(NUM_BLOQUE) = UPPER(TRIM(?)) AND TRIM(VALOR) = UPPER(TRIM(?)) ");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_AFECTA_BLOQUES(NUM_BLOQUE,VALOR,DESCRIPCION) "
			.concat("VALUES(TRIM(?),TRIM(UPPER(?)),TRIM(UPPER(?)))");
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR = "UPDATE TRAN_AFECTA_BLOQUES SET  "
			.concat("VALOR = TRIM(UPPER(?)), DESCRIPCION = TRIM(UPPER(?)) WHERE TRIM(NUM_BLOQUE)")
			.concat("= TRIM(?) AND TRIM(VALOR) = UPPER(TRIM(?))");
	
	/** La constante CONSULTA_ELIMINAR. */
	private static final String CONSULTA_ELIMINAR = "DELETE FROM TRAN_AFECTA_BLOQUES "
			.concat("WHERE TRIM(NUM_BLOQUE) = UPPER(TRIM(?)) AND TRIM(VALOR) = UPPER(TRIM(?)) ");
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasAfectacionBloques utilerias = UtileriasAfectacionBloques.getInstancia();
	
	/**
	 * Consulta registros.
	 *
	 * Metodo publico utilizado mostra los registros del catalogo
	 * 
	 * @param beanPaginador --> objeto que permite paginar
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	@Override
	public BeanBloques consultar(BeanBloque beanBloque, BeanPaginador beanPaginador, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanBloques response = new BeanBloques();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanBloque> bloques = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utilerias.getFiltro(beanBloque, "AND");
		String filtroWhere = utilerias.getFiltro(beanBloque, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				bloques = new ArrayList<BeanBloque>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanBloque bloque = new BeanBloque();
					/** Seteo de datos **/
					bloque.setNumBloque(utilerias.getString(map.get("NUM_BLOQUE")));
					bloque.setValor((utilerias.getString(map.get("VALOR"))));
					bloque.setDescripcion((utilerias.getString(map.get("DESCRIPCION"))));
					/** Se añade el objeto a la lista**/
					bloques.add(bloque);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setBloques(bloques);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Agregar registros.
	 *
	 * Metodo publico utilizado para agregar registros
	 * al catalogo.
	 * 
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	@Override
	public BeanResBase agregar(BeanBloque beanBloque, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanBloque, UtileriasAfectacionBloques.ALTA);
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(beanBloque);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Editar registros.
	 *
	 * Metodo publico utilizado para editar registros
	 * del catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	@Override
	public BeanResBase editar(BeanBloque beanBloque, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			String [] dataValor = beanBloque.getValor().split(Pattern.quote("|"));
			int existencia = 0;
			beanBloque.setValor(dataValor[0]);
			existencia = buscarRegistro(beanBloque);
			beanBloque.setValor(dataValor[0].concat("|".concat(dataValor[1])));
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utilerias.agregarParametros(beanBloque, UtileriasAfectacionBloques.MODIFICACION);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(CONSULTA_MODIFICAR, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Eliminar registros.
	 *
	 * Metodo publico utilizado para eliminar registros
	 * en el catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	@Override
	public BeanResBase eliminar(BeanBloque beanBloque, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanBloque, UtileriasAfectacionBloques.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(CONSULTA_ELIMINAR, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 *
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 * 
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return contador --> regresa el contador
	 */
	private int buscarRegistro(BeanBloque beanBloque) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanBloque, UtileriasAfectacionBloques.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
}
