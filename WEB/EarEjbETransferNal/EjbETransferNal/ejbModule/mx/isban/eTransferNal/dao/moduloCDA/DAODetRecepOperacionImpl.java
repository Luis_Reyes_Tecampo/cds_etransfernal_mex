/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAODetRecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015 18:02 INDRA     ISBAN 		Creacion
 *   1.1   17/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsHorasHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOpera;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpApartadas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga obtener la informacion para el detalle de
 * recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 1/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAODetRecepOperacionImpl extends Architech implements DAODetRecepOperacion {

	/** * Constante privada del Serial version. */
	private static final long serialVersionUID = 6030127345167349695L;

	/** La variable que contiene informacion con respecto a: util. */
	private UtileriasRecepcionOpApartadas util = UtileriasRecepcionOpApartadas.getInstance();

	/** La variable que contiene informacion con respecto a: util. */
	private UtileriasRecepcionOperacion utilRecep = UtileriasRecepcionOperacion.getInstance();

	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas utilConsul = UtileriasRecepcionOpConsultas.getInstance();

	/**
 	 * Generar autorizacion Historica
 	 *
 	 * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
 	 * @param architechSessionBean El objeto: architech session bean
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto con la respuesta
 	 */
	@Override
	public BeanDetRecepOperacionDAO generarAutorizacionHTO(BeanDetRecepOperacionDAO bean,
			ArchitechSessionBean architechSessionBean, boolean historico, BeanReqTranSpeiRec beanReqTranSpeiRec) {
		BeanDetRecepOperacionDAO detalleBean = new BeanDetRecepOperacionDAO();
		BeanDetRecepOperacionDAO detalleBeanAnt = new BeanDetRecepOperacionDAO();
		BeanConsHorasHist beanConsHorasHist = new BeanConsHorasHist();
		BeanResBase beanError = new BeanResBase();
		// Se crean las variables para guardar los datos de la operacion seleccionada
		String fechaOpe = bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion();
		String fecha = bean.getBeanDetRecepOperacion().getBeanCambios().getFchCaptura().substring(0, 10);
		String folioPaq = bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete();
		String folioPgo = bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago();
		String statusTransf = bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusTransfer();
		String cveInstu = bean.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion();
		String cveInstOrd = bean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd();
		BeanDetRecepOperacionDAO beanBus = new BeanDetRecepOperacionDAO();
		beanBus = bean;
		// se inicializa los datos generales
		beanBus.getBeanDetRecepOperacion().setDetalleGral(new BeanDetRecepOpera());
		beanBus.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(fecha);
		beanBus.getBeanDetRecepOperacion().getDetalleGral().setFolioPaquete(folioPaq.replace("-", ""));
		beanBus.getBeanDetRecepOperacion().getDetalleGral().setFolioPago(folioPgo.replace("-", ""));
		beanBus.getBeanDetRecepOperacion().getDetalleGral().setCveInstOrd(cveInstOrd);
		beanBus.getBeanDetRecepOperacion().getDetalleGral().setCveMiInstitucion(cveInstu);
		// se llama la funcion para consulta el detalle de recepcion
		for (int i = 0; i < beanReqTranSpeiRec.getListBeanTranSpeiRec().size(); i++) {
			if (beanReqTranSpeiRec.getListBeanTranSpeiRec().get(i).isSelecDevolucion()) {
				detalleBeanAnt
						.setBeanDetRecepOperacion(beanReqTranSpeiRec.getListBeanTranSpeiRec().get(i).getBeanDetalle());
			}
		}
		detalleBean = detalleBeanAnt;
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Instancias de objetos de respuesta servicio **/
		detalleBean.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(fechaOpe);
		detalleBean.getBeanDetRecepOperacion().getDetEstatus().setEstatusTransfer(statusTransf);
		try {
			/** si el estatus es RE asigna la cve pago para dev ext **/
			if (("RE").equals(statusTransf)) {
				detalleBean.getBeanDetRecepOperacion().getDetEstatus().setCvePago("ENVIAR");
			}
			/** consulta a tabla de horas historicas por referencia */
			beanConsHorasHist = consultaHorasHistorica(detalleBean);
			if (beanConsHorasHist.getBeanErrors().getCodError().equals(Errores.OK00000V)) {
				String folioPago = "-" + beanConsHorasHist.getBeanCvesHoras().getFolioPago();
				String folioPaquete = "-" + beanConsHorasHist.getBeanCvesHoras().getFolioPaquete();
				beanConsHorasHist.getBeanCvesHoras().setFolioPago(folioPago);
				beanConsHorasHist.getBeanCvesHoras().setFolioPaquete(folioPaquete);
				/** genera insert en tabla de horas del dia */
				beanError = insertaHoras(beanConsHorasHist);
				if (beanError.getCodError().equals(Errores.EC00011B)) {
					detalleBeanAnt.getBeanDetRecepOperacion().getError().setCodError(beanError.getCodError());
					detalleBeanAnt.getBeanDetRecepOperacion().getError().setMsgError(beanError.getMsgError());
					return detalleBeanAnt;
				}
			} else {
				detalleBeanAnt.getBeanDetRecepOperacion().getError().setCodError(Errores.EC00011B);
				detalleBeanAnt.getBeanDetRecepOperacion().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
				return detalleBeanAnt;
			}
			/** si viene hisotrico y el estatus es diferente asigna signo negativo **/
			if (historico && !("TR").equals(statusTransf)) {
				detalleBean.getBeanDetRecepOperacion().getDetalleGral()
						.setFolioPaquete("-" + beanBus.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
				detalleBean.getBeanDetRecepOperacion().getDetalleGral()
						.setFolioPago("-" + beanBus.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
			}

			/** genera consulta **/
			String query = generarConsultaAutorizar(detalleBean);
			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(cveInstu);
			parametros.add(cveInstOrd);
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
			/** Se ejecuta el alta de la operacion **/
			responseDTO = HelperDAO.insertar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se sete el mensaje y codigo de error **/
			detalleBeanAnt.getBeanDetRecepOperacion().getError().setCodError(responseDTO.getCodeError());
			detalleBeanAnt.getBeanDetRecepOperacion().getError().setMsgError(responseDTO.getMessageError());
			detalleBeanAnt.getBeanDetRecepOperacion().getError().setTipoError(Errores.TIPO_MSJ_INFO);
		} catch (ExceptionDataAccess e) {
			showException(e);
			detalleBeanAnt.getBeanDetRecepOperacion().getError().setCodError(Errores.EC00011B);
			detalleBeanAnt.getBeanDetRecepOperacion().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return detalleBeanAnt;
	}

	/**
	 * Generar consulta autorizar.
	 *
	 * @param bean El objeto: bean
	 * @return Objeto string
	 */
	public String generarConsultaAutorizar(BeanDetRecepOperacionDAO bean) {
		String query = "";
		StringBuilder inicio = new StringBuilder();
		StringBuilder values = new StringBuilder();
		/** inicial la estrutucra del insert **/
		inicio.append("INSERT INTO TRAN_SPEI_REC(FCH_OPERACION,CVE_MI_INSTITUC,CVE_INST_ORD,FOLIO_PAQUETE,FOLIO_PAGO");
		values.append(" VALUES(to_date('").append(bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion())
				.append("','dd-MM-yyyy'),?,?,?,?");
		/** en caso de que exita la topologia **/
		if (bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusBanxico() != null
				&& bean.getBeanDetRecepOperacion().getDetalleTopo().getTopologia().length() > 0) {
			inicio.append(",TOPOLOGIA");
			values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getTopologia()).append("'");
		}
		/** en caso de que exita la estatus tranfer **/
		if (bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusTransfer() != null
				&& bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusTransfer().length() > 0) {
			inicio.append(",ESTATUS_TRANSFER");
			values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusTransfer())
					.append("'");
		}
		/** en caso de que exita la tipo cuenta orden **/
		if (bean.getBeanDetRecepOperacion().getOrdenante().getTipoCuentaOrd() != null
				&& bean.getBeanDetRecepOperacion().getOrdenante().getTipoCuentaOrd().length() > 0) {
			inicio.append(",TIPO_CUENTA_ORD");
			values.append(",'").append(bean.getBeanDetRecepOperacion().getOrdenante().getTipoCuentaOrd()).append("'");
		}
		/** en caso de que exita la tipo cuenta rec **/
		if (bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec() != null
				&& bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec().length() > 0) {
			inicio.append(",TIPO_CUENTA_REC");
			values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec()).append("'");
		}
		query = util.generarConsultaAuto(bean, inicio, values);
		return query;
	}

	/**
	  * Valida si Existe operacion.
	  *
	  * @param architechSessionBean El objeto: architech session bean de la arquitectura
	  * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
	  * @return Objeto la respuesta basica
	  */
	@Override
	public BeanResBase existeOperacion(ArchitechSessionBean architechSessionBean, BeanTranSpeiRecOper bean) {
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanResBase response = new BeanResBase();
		String query = ConstantesRecepOperacionApartada.EXISTE_OPERACION_AUTORIZADA;
		/** Se realiza la consulta **/
		try {
			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
			// se realiza la consulta
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response = utilConsul.obtenerExiste(responseDTO);
			if(!Errores.ED00133V.equals(response.getCodError())) {
				query = ConstantesRecepOperacionApartada.EXISTE_OPERACION_BLOQUEO;
				responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response = utilConsul.obtenerExiste(responseDTO);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Se regresa el resultado **/
		return response;
	}

	/**
 	 * Realiza la actualizacion de la 
 	 * Cuenta Receptora
 	 *
 	 * @param architechSessionBean El objeto: architech session bean de la arquitectura
 	 * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
 	 * @param parametros El objeto: los parametros para ejecutar el query
 	 * @param pantalla El objeto: el identificador para validar el modulo (Alterna / Principal)
 	 * @return Objeto de respuesta
 	 */
	@Override
	public BeanDetRecepOperacionDAO updateCtaReceptora(ArchitechSessionBean architechSessionBean,
			BeanDetRecepOperacionDAO bean, List<Object> parametros, String pantalla) {
		/** Realiza la consulta **/
		String query = ConstantesRecepOperacion.QUERY_UPDATE_CTA_RECEPTORA + ConstantesRecepOperacion.FILTROS
				+ " AND cve_inst_ord = ? ";
		if (pantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO)
				|| pantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_HTO)) {
			/** inicializa la consulta **/
			query = query.replace(ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI,
					ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI_HTO);
		}
		bean.getBeanDetRecepOperacion().setError(new BeanResBase());
		ResponseMessageDataBaseDTO responseDTO = null;
		/** si hay algun error actualiza el codigo **/
		try {
			/** acutaliza el registro **/
			responseDTO = HelperDAO.actualizar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			bean.getBeanDetRecepOperacion().getError().setCodError(responseDTO.getCodeError());
			bean.getBeanDetRecepOperacion().getError().setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			bean.getBeanDetRecepOperacion().getError().setCodError(Errores.EC00011B);
			bean.getBeanDetRecepOperacion().getError().setMsgError(Errores.DESC_EC00011B);
		}
		/** regresa el objeto **/
		return bean;
	}

	/**
 	 * Eliminar registro.
 	 *
 	 * @param architechSessionBean El objeto: architech session bean de la aarquitectura
 	 * @param bean El objeto: el objeto con la informacion de la operacion a eliminar
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto bean de respuesta
 	 */
	@Override
	public BeanTranSpeiRecOper eliminarRegistro(ArchitechSessionBean architechSessionBean, BeanTranSpeiRecOper bean,
			boolean historico) {
		// se llama la funcion para eliminar apartado
		BeanResBase eliAPart = eliminaRegApart(architechSessionBean, bean, historico);
		if (!eliAPart.getCodError().equals(Errores.EC00011B)) {
			// se inicializa los parametros
			List<Object> parametros = new ArrayList<Object>();
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
			/** Realiza la consulta **/
			String query = ConstantesRecepOperacionApartada.ELIMINAR_RECEPCION_HISTORICA;
			if (historico) {
				query = query.replace(ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI_HTO,
						ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI);
			}
			bean.getBeanDetalle().setError(new BeanResBase());
			ResponseMessageDataBaseDTO responseDTO = null;
			/** si hay algun error actualiza el codigo **/
			try {
				/** acutaliza el registro **/
				responseDTO = HelperDAO.eliminar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				bean.getBeanDetalle().getError().setCodError(responseDTO.getCodeError());
				bean.getBeanDetalle().getError().setMsgError(responseDTO.getMessageError());
			} catch (ExceptionDataAccess e) {
				showException(e);
				bean.getBeanDetalle().getError().setCodError(Errores.EC00011B);
				bean.getBeanDetalle().getError().setMsgError(Errores.DESC_EC00011B);
			}
		} else {
			bean.getBeanDetalle().getError().setCodError(Errores.EC00011B);
			bean.getBeanDetalle().getError().setMsgError(Errores.DESC_EC00011B);
		}
		/** regresa el objeto **/
		return bean;
	}

	/**
	 * Eliminar registro.
	 *
	 * @param architechSessionBean El objeto: architech session bean
	 * @param bean                 El objeto: bean
	 * @return Objeto bean tran spei rec oper
	 */
	private BeanResBase eliminaRegApart(ArchitechSessionBean architechSessionBean, BeanTranSpeiRecOper bean,
			boolean historico) {
		// se inicializan parametros
		String query = ConstantesRecepOperacionApartada.QUERY_ELIMINAR_OPERA_APARTADA_DIA;
		// se iniciliza el catalogo de errores
		BeanResBase resul = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		// se inicializa los parametros
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
		parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
		parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
		parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
		/** si hay algun error actualiza el codigo **/
		try {
			parametros.add(architechSessionBean.getUsuario());
			if (!historico) {
				// se hace la cnsulta
				query = ConstantesRecepOperacionApartada.QUERY_ELIMINAR_OPERA_APARTADA_HTO;
			}
			responseDTO = HelperDAO.eliminar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			resul.setCodError(responseDTO.getCodeError());
			resul.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			resul.setCodError(Errores.EC00011B);
			resul.setMsgError(Errores.DESC_EC00011B);
		}
		/** regresa el objeto **/
		return resul;
	}

	/**
	  * Metodo que valida si Existe la operacion
	  *
	  * @param bean El objeto: el objeto con la infromacion de la oppperacion
	  * @param historico El objeto: la bandera que identifica si es historico
	  * @return Objeto bean de respuesta
	  */
	@Override
	public BeanTranSpeiRecOper existeBeanOper(BeanTranSpeiRecOper bean, boolean historico) {
		ResponseMessageDataBaseDTO responseDTO = null;
		BeanTranSpeiRecOper response = new BeanTranSpeiRecOper();
		response.setBeanDetalle(new BeanDetRecepOperacion());
		response.getBeanDetalle().setError(new BeanResBase());
		String query = ConstantesRecepOperacionApartada.QUERY_EXITE_DTO_DIA;

		if (historico) {
			query = ConstantesRecepOperacionApartada.QUERY_EXITE_DTO_HTO;
		}

		/** Se realiza la consulta **/
		try {
			// se crea elobjeto para pasar los parametros
			List<HashMap<String, Object>> list = null;
			List<Object> parametros = new ArrayList<Object>();
			List<BeanTranSpeiRecOper> beanTranSpeiRecList = new ArrayList<BeanTranSpeiRecOper>();
			// se asigna los datos para el parametro
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
			// se realiza la consulta
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
			/** En caso que el sesultado traiga informacion **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				beanTranSpeiRecList = new ArrayList<BeanTranSpeiRecOper>();
				/** Recorre la lista **/
				BeanResConsTranSpeiRecDAO res = utilRecep.continuaRecorer(list, beanTranSpeiRecList, new BeanReqTranSpeiRec());
				response = res.getBeanTranSpeiRecList().get(0);
				response.getBeanDetalle().setError(new BeanResBase());
				response.getBeanDetalle().getError().setCodError(Errores.ED00137V);
			} else {
				response.getBeanDetalle().getError().setCodError(Errores.OK00000V);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.getBeanDetalle().getError().setCodError(Errores.EC00011B);
			response.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Se regresa el resultado **/
		return response;
	}

	/**
 	 * Nuevo registro historico.
 	 *
 	 * @param bean El objeto: El objeto con la informacion d ela operacion
 	 * @param architechSessionBean El objeto: architech session bean de la arquitectura
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto bean con el contenido de la operacion actualizada
 	 */
	@Override
	public BeanTranSpeiRecOper nuevoRegistroHTO(BeanTranSpeiRecOper bean, ArchitechSessionBean architechSessionBean,
			boolean historico) {
		BeanDetRecepOperacionDAO detalleBean = new BeanDetRecepOperacionDAO();
		bean.getBeanDetalle().setError(new BeanResBase());
		detalleBean.setBeanDetRecepOperacion(bean.getBeanDetalle());
		ResponseMessageDataBaseDTO responseDTO = null;
		try {

			/** genera consulta **/
			String query = generarConsultaAutorizar(detalleBean);
			if (historico) {
				query = query.replaceAll(ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI,
						ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI_HTO);
			}
			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
			/** Se ejecuta el alta de la operacion **/
			responseDTO = HelperDAO.insertar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se sete el mensaje y codigo de error **/
			bean.getBeanDetalle().getError().setCodError(responseDTO.getCodeError());
			bean.getBeanDetalle().getError().setMsgError(responseDTO.getMessageError());
			bean.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_INFO);
		} catch (ExceptionDataAccess e) {
			showException(e);
			bean.getBeanDetalle().getError().setCodError(Errores.EC00011B);
			bean.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return bean;
	}

	/**
	 * Consulta horas historica.
	 *
	 * @param detalleBean the detalle bean
	 * @return the bean cons horas hist
	 */
	private BeanConsHorasHist consultaHorasHistorica(BeanDetRecepOperacionDAO detalleBean) {
		BeanConsHorasHist bean = new BeanConsHorasHist();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			String query = ConstantesRecepOperacionApartada.CONSULTA_HORAS_HISTORICA;
			List<HashMap<String, Object>> list = null;
			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion());
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd());
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion());
			parametros
					.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete().replaceAll("-", ""));
			parametros.add(detalleBean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago().replaceAll("-", ""));
			/** Se ejecuta el alta de la operacion **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** asigna el recultado **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					bean = utilRecep.asignaDatosTabHorasSPEI(bean, map);
				}
				bean.getBeanErrors().setCodError(Errores.OK00000V);
			} else {
				/** Asigna los cosdigos **/
				bean.getBeanErrors().setCodError(Errores.EC00011B);
				bean.getBeanErrors().setMsgError(Errores.TIPO_MSJ_ERROR);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			bean.getBeanErrors().setCodError(Errores.EC00011B);
			bean.getBeanErrors().setMsgError(Errores.TIPO_MSJ_ERROR);
		}
		return bean;
	}

	/**
	 * Inserta horas.
	 *
	 * @param beanConsHorasHist the bean cons horas hist
	 * @return the bean res base
	 */
	private BeanResBase insertaHoras(BeanConsHorasHist beanConsHorasHist) {
		BeanResBase bean = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			String query = ConstantesRecepOperacionApartada.INSERT_HORAS;

			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchOperacion());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getuVersion());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveInstOrd());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveInstBenef());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getFolioPaquete());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getFolioPago());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getTipo());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getReferencia());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchCaptura());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchEnvio());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchAcuse());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchConfirmacion());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchRecepcion());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchAprobacion());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveRastreo());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveRastreoOri());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getDifAcuseAprobacion());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getDifRecepcionAcuse());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getDifCapturaRecepcion());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveTransfe());
			parametros.add(beanConsHorasHist.getBeanCvesHoras().getCveMedioEnt());
			parametros.add(beanConsHorasHist.getBeanFechasHoras().getFchCapturaRec());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getTipoPago());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getSwActHisLs());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getSwActHisEn());
			parametros.add(beanConsHorasHist.getBeanGeneralHoras().getSwActHisCo());
			parametros.add("N");

			/** Se ejecuta el alta de la operacion **/
			responseDTO = HelperDAO.insertar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se sete el mensaje y codigo de error **/
			bean.setCodError(responseDTO.getCodeError());
			bean.setMsgError(responseDTO.getMessageError());
			bean.setTipoError(Errores.TIPO_MSJ_INFO);

		} catch (ExceptionDataAccess e) {
			showException(e);
			bean.setCodError(Errores.EC00011B);
			bean.setMsgError(Errores.TIPO_MSJ_ERROR);
		}
		return bean;
	}

	/**
	 * Consulta detalle operacion.
	 *
	 * Metodo para consultar los datos del nuevo registro de 
	 * operacion y mostrarlos en pantalla.
	 * 
	 * @param bean El objeto: bean, con la informacion de la operacion de la que se requiere ver el detalle 
	 * @param architechSessionBean El objeto: architech session bean de la aquiterctura
	 * @return Objeto bean detalle operacion (Contiene los datos del detalle de la operacion)
	 */
	@Override
	public BeanDetRecepOperacionDAO consultaDetRecepcionOp(BeanDetRecepOperacionDAO bean,
			ArchitechSessionBean architechSessionBean) {
		/** inicializa las varaibles **/
		Utilerias utilerias = Utilerias.getUtilerias();
		/** codigo de error **/
		BeanResBase error = new BeanResBase();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		String query = ConstantesRecepOperacion.QUERY_CONSULTA_REF;
		try {
			debug("Consultando***");
			// se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			// se asigna los datos para el parametro
			parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion());
			parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion());
			parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd());
			parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
			parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
			/** realiza la consulta **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** asigna el recultado **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				final String referencia = utilerias.getString(list.get(0).get(ConstantesRecepOperacion.REFERENCIA)).trim();
				final String estatus = utilerias.getString(list.get(0).get(ConstantesRecepOperacion.ESTATUS)).trim();
				final String paquete = utilerias.getString(list.get(0).get(ConstantesRecepOperacion.PAQUETE)).trim();
				final String pago = utilerias.getString(list.get(0).get(ConstantesRecepOperacion.PAGO)).trim();
				bean.getBeanDetRecepOperacion().setDetEstatus(new BeanDetRecepOp());
				bean.getBeanDetRecepOperacion().getDetEstatus().setRefeTransfer(referencia);
				bean.getBeanDetRecepOperacion().getDetEstatus().setEstatusTransfer(estatus);
				bean.getBeanDetRecepOperacion().setDetalleGral(new BeanDetRecepOpera());
				bean.getBeanDetRecepOperacion().getDetalleGral().setFolioPaquete(paquete);
				bean.getBeanDetRecepOperacion().getDetalleGral().setFolioPago(pago);
				error.setCodError(Errores.OK00000V);
			} else {
				error.setCodError(Errores.EC00011B);
			}
			/** Asigna los cosdigos **/
			bean.getBeanDetRecepOperacion().setError(error);
		} catch (ExceptionDataAccess e) {
			/** en caso de error **/
			showException(e);
			error.setCodError(Errores.EC00011B);
			bean.getBeanDetRecepOperacion().setError(error);
		}
		// nombre pantalla
		bean.getBeanDetRecepOperacion().setNombrePantalla(bean.getBeanDetRecepOperacion().getNombrePantalla());
		/** regresa el objeto **/
		return bean;
	}

}