/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConfiguracionParametrosImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;


import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfParamAra;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfParamFec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametrosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase que se encanga de obtener la informacion para la configuracion de parametros
 *y de acutalizar los datos del registro.
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConfiguracionParametrosImpl extends Architech implements DAOConfiguracionParametros {
	
	/** 
	 * La constante serialVersionUID. 
	 * 
	 * */
	private static final long serialVersionUID = -8060887756289298023L;
	
	/** La constante where
	 * que hace de condicion para el campo institucion 
	 * 
	 * */
	private static final String CVE_MI_INSTITUC = "WHERE CVE_MI_INSTITUC = ?";

	/**
	 * constante para ejecutar el query de parametros 
	 * operativos para el modulo spid
	 * el cual trae todos los campos de la tabla
	 */
	private static final StringBuilder CONSULTA = 
			new StringBuilder().append("SELECT CVE_MI_INSTITUC, CVE_MECA_SPID CVE_MECA_SP, CVE_MECA_DEVOL, CVE_MECA_TRASP, DESTINAT_SPID DESTINAT_SP,")
					.append("DESTINAT_ARA, PUERTO_FEC_INI, PUERTO_FEC_FIN, PUERTO_ARA_INI, PUERTO_ARA_FIN,")
					.append("DIREC_IP_FEC, DIREC_IP_ARA, LOGIN, PASSWORD, NOMBRE_INSTITUC, CERTIFICADO_ARA ")
					.append("FROM TRAN_SPID_PARAM ")
					.append(CVE_MI_INSTITUC);
	
	/**
	 * constante para  ejecutar el query de parametros 
	 * operativos para el modulo spei
	 * el cual trae todos los campos de la tabla
	 */
	private static final StringBuilder CONSULTASPEI = 
			new StringBuilder().append("SELECT CVE_MI_INSTITUC, CVE_MECA_SPEI CVE_MECA_SP, CVE_MECA_DEVOL, CVE_MECA_TRASP, DESTINAT_SPEI DESTINAT_SP,")
					.append("DESTINAT_ARA, PUERTO_FEC_INI, PUERTO_FEC_FIN, PUERTO_ARA_INI, PUERTO_ARA_FIN,")
					.append("DIREC_IP_FEC, DIREC_IP_ARA, LOGIN, PASSWORD, NOMBRE_INSTITUC, CERTIFICADO_ARA ")
					.append("FROM TRAN_SPEI_PARAM ")
					.append(CVE_MI_INSTITUC);
	
	/**
	 * Modificacion de datos
	 * para la patalla parametros operativos para el modulo spid
	 * campos que pueden ser modificados
	 */
	private static final StringBuilder UPDATE = 
			new StringBuilder().append("UPDATE TRAN_SPID_PARAM SET ")
					.append("CVE_MECA_SPID = ?,")
					.append("CVE_MECA_DEVOL = ?,")
					.append("CVE_MECA_TRASP = ?,")
					.append("DESTINAT_SPID = ?,")
					.append("DESTINAT_ARA = ?,")
					.append("PUERTO_FEC_INI = ?,")
					.append("PUERTO_FEC_FIN = ?,")
					.append("PUERTO_ARA_INI = ?,")
					.append("PUERTO_ARA_FIN = ?,")
					.append("DIREC_IP_FEC = ?,")
					.append("DIREC_IP_ARA = ?,")
					.append("LOGIN = ?,")
					.append("PASSWORD = ?,")
					.append("NOMBRE_INSTITUC = ?,")
					.append("CERTIFICADO_ARA = ?")
					.append(CVE_MI_INSTITUC);
	
	/**
	 * Modificacion de datos
	 * para la patalla parametros operativos para el modulo spei
	 * campos que pueden ser modificados
	 */
	private static final StringBuilder UPDATESPEI = 
			new StringBuilder().append("UPDATE TRAN_SPEI_PARAM SET ")
					.append("CVE_MECA_SPEI = ?,")
					.append("CVE_MECA_DEVOL = ?,")
					.append("CVE_MECA_TRASP = ?,")
					.append("DESTINAT_SPEI = ?,")
					.append("DESTINAT_ARA = ?,")
					.append("PUERTO_FEC_INI = ?,")
					.append("PUERTO_FEC_FIN = ?,")
					.append("PUERTO_ARA_INI = ?,")
					.append("PUERTO_ARA_FIN = ?,")
					.append("DIREC_IP_FEC = ?,")
					.append("DIREC_IP_ARA = ?,")
					.append("LOGIN = ?,")
					.append("PASSWORD = ?,")
					.append("NOMBRE_INSTITUC = ?,")
					.append("CERTIFICADO_ARA = ?")
					.append(CVE_MI_INSTITUC);
	/**
	 * Metodo para obtener configuracion de parameros.
	 *
	 * @param cveInstitucion   Objeto del tipo @see Long
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 */
	@Override
	public BeanResConfiguracionParametrosDAO obtenerConfiguracionParametros(
			Long cveInstitucion, ArchitechSessionBean architechSessionBean, String nomPantalla) {
		/**
		 * Instancia al bean beanResConfiguracionParametrosDAO
		 * 
		 * */
		final BeanResConfiguracionParametrosDAO beanResConfiguracionParametrosDAO = new BeanResConfiguracionParametrosDAO();
		/**
		 * Lista de tipo HashMap
		 * lista nula
		 * */
		List<HashMap<String,Object>> list = null;
		/**
    	 * instancia de ResponseMessageDataBaseDTO
    	 * igual a null
    	 * */
    	ResponseMessageDataBaseDTO responseDTO = null;
    	/**
    	 * instancia de BeanConfiguracionParametros
    	 * igual a null
    	 * */
    	BeanConfiguracionParametros configuracionParametros = null;
    	try {
    		/**
    		 * definicion de la variable definirConsulta
    		 * de tipo string,
    		 * como una cadena vacia
    		 * 
    		 * */
    		String definirConsulta = "";
    		/**
    		 *  Ejecuta el query para el modulo spei o spid
    		 * dependiendo del menu seleccionado.
    		 * Convierte las constantes definidas a cadenas para ser usadas 
	    	 * en la construccion del query en el dao
    		 */
    		if ("parametrosOperativosSPEI".equals(nomPantalla)) {
    			definirConsulta = CONSULTASPEI.toString();
    		}else{
				definirConsulta = CONSULTA.toString();
			}
    		List<Object> params = new ArrayList<Object>();
    		params.add(cveInstitucion);
        	responseDTO = HelperDAO.consultar(definirConsulta,params, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	/**
    		 * validacion de la lista,
    		 * se valida que no este vacia y mapea cada uno de los campos del registro
    		 * con los campos definidos en el dao
    		 */
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();
        		 
        		 if(!list.isEmpty()){        			 
        			 configuracionParametros = getParams(list);
        		 }
        	}
        	/**
    		 * Obtencion de errores
    		 * se obtiene el codigo y el mensaje de error
    		 */
        	beanResConfiguracionParametrosDAO.setConfiguracionParametros(configuracionParametros);
        	beanResConfiguracionParametrosDAO.setCodError(responseDTO.getCodeError());
        	beanResConfiguracionParametrosDAO.setMsgError(responseDTO.getMessageError());
        	/**
    		 * Manejo de excepcion de errores, se muestra el codigo del error
    		 */
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		beanResConfiguracionParametrosDAO.setCodError(Errores.EC00011B); 
    	}
    	/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */
		return beanResConfiguracionParametrosDAO;
	}
	
	/**
	 * Obtener el objeto: params.
	 *
	 * Metodo para mapear los parametros obtenidos 
	 * 
	 * @param list El objeto: list con el map obtenido en la consulta
	 * @return El objeto: params
	 */
	private BeanConfiguracionParametros getParams(List<HashMap<String,Object>> list) {
		BeanConfiguracionParametros configuracionParametros = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		for(Map<String,Object> mapResult : list){
			 configuracionParametros = new BeanConfiguracionParametros();
			 configuracionParametros.setCveCESIF(Long.parseLong(""+mapResult.get("CVE_MI_INSTITUC")+""));
			 configuracionParametros.setOrdPago(utilerias.getString(mapResult.get("CVE_MECA_SP")));
			 configuracionParametros.setDevoluciones(utilerias.getString(mapResult.get("CVE_MECA_DEVOL")));
			 
			 configuracionParametros.setFec(new BeanConfParamFec());
			 configuracionParametros.setAra(new BeanConfParamAra());
			 
			 configuracionParametros.setTrasSPIDSIAC(utilerias.getString(mapResult.get("CVE_MECA_TRASP")));
			 configuracionParametros.getFec().setFecDestinatario(utilerias.getString(mapResult.get("DESTINAT_SP")));
			 configuracionParametros.getAra().setAraDestinatario(utilerias.getString(mapResult.get("DESTINAT_ARA")));
			 configuracionParametros.getFec().setFecInicial(utilerias.getString(mapResult.get("PUERTO_FEC_INI")));
			 configuracionParametros.getFec().setFecFinal(utilerias.getString(mapResult.get("PUERTO_FEC_FIN")));
			 configuracionParametros.getAra().setAraInicial(utilerias.getString(mapResult.get("PUERTO_ARA_INI")));
			 configuracionParametros.getAra().setAraFinal(utilerias.getString(mapResult.get("PUERTO_ARA_FIN")));
			 configuracionParametros.getFec().setFecDireccionIP(utilerias.getString(mapResult.get("DIREC_IP_FEC")));
			 configuracionParametros.getAra().setAraDireccionIP(utilerias.getString(mapResult.get("DIREC_IP_ARA")));
			 configuracionParametros.getFec().setFecLogin(utilerias.getString(mapResult.get("LOGIN")));
			 configuracionParametros.getFec().setFecPassword(utilerias.getString(mapResult.get("PASSWORD")));
			 configuracionParametros.setNomInstit(utilerias.getString(mapResult.get("NOMBRE_INSTITUC")));
			 configuracionParametros.getAra().setAraCertificado(utilerias.getString(mapResult.get("CERTIFICADO_ARA")));
		 }
		return configuracionParametros;
	}
	/**
	 * Metodo para guardar configuracion de parametros.
	 *
	 * @param configuracionParametros  Objeto del tipo @see BeanConfiguracionParametros
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 */
	@Override
	public BeanResConfiguracionParametrosDAO guardarConfiguracionParametros(
			BeanConfiguracionParametros configuracionParametros, ArchitechSessionBean architechSessionBean, String nomPantalla) {
		
		BeanResConfiguracionParametrosDAO beanResConfiguracionParametrosDAO = new BeanResConfiguracionParametrosDAO();
	       try{	
	    	   /**definicion de la variable definirConsulta
	    		 * de tipo string,
	    		 * como una cadena vacia
	    		 * */
	    	   String definirConsulta = "";
	    	   /**
	    		 * Ejecuta el query para el modulo spei o spid
	    		 * dependiendo del menu seleccionado.
	    		 * Convierte las constantes definidas a cadenas para ser usadas 
	    		 * en la construccion del query en el dao
	    		 */
	    		if ("parametrosOperativosSPEI".equals(nomPantalla)) {
	    			definirConsulta = UPDATESPEI.toString();
	    		}else {
					definirConsulta = UPDATE.toString();
				}
	    		/**
		    	    * Array que obtiene los campos que han sido modificados
		    	    * para realizar la ejecucion del update.
		    	    * Se obtienen los campos que se pueden modificar
		    	    */
	    	  List<Object> params = new ArrayList<Object>();
	    	  params.add(configuracionParametros.getOrdPago());
	    	  params.add(configuracionParametros.getDevoluciones());
	    	  params.add(configuracionParametros.getTrasSPIDSIAC());
	    	  params.add(configuracionParametros.getFec().getFecDestinatario());
	    	  params.add(configuracionParametros.getAra().getAraDestinatario());
	    	  params.add(configuracionParametros.getFec().getFecInicial());
	    	  params.add(configuracionParametros.getFec().getFecFinal());
	    	  params.add(configuracionParametros.getAra().getAraInicial());
	    	  params.add(configuracionParametros.getAra().getAraFinal());
	    	  params.add(configuracionParametros.getFec().getFecDireccionIP());
	    	  params.add(configuracionParametros.getAra().getAraDireccionIP());
	    	  params.add(configuracionParametros.getFec().getFecLogin());
	    	  params.add(configuracionParametros.getFec().getFecPassword());
	    	  params.add(configuracionParametros.getNomInstit());
	    	  params.add(configuracionParametros.getAra().getAraCertificado());
	    	  params.add(configuracionParametros.getCveCESIF());
	          ResponseMessageDataBaseDTO responseDTO = HelperDAO.actualizar(definirConsulta, params, 
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	          beanResConfiguracionParametrosDAO.setConfiguracionParametros(configuracionParametros);
	          beanResConfiguracionParametrosDAO.setCodError(responseDTO.getCodeError());
	          /**
		         *Manejo de excepciones de errores 
		         *Muestra las excepciones
		         */
	       } catch (ExceptionDataAccess e) {
	          showException(e);
	          beanResConfiguracionParametrosDAO.setCodError(Errores.EC00011B);
	       }
	       /**
	    	 * Regresa los datos obtenidos de la ejecucion del query
	    	 * */
	       return beanResConfiguracionParametrosDAO;
	}
}
