/**
 * Clase: DAO-Imp Clase que implementa la interfaz para la pantalla
 * Monitor Carga Archivos Historicos Detalle CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Enero 2016
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanDetalleArchivoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.EnumQuerysModuloCADSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOCargaArchivosHistoricosDetalleCDASPIDImpl.
 */
//The Class DAOCargaArchivosHistoricosDetalleCDASPIDImpl.
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCargaArchivosHistoricosDetalleCDASPIDImpl 
		extends Architech implements DAOCargaArchivosHistoricosDetalleCDASPID{

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = -8948007193218058585L;

	//Metodo para consultar detalles de un archivo
	@Override
	public BeanResMonitorCargaArchHistCADSPID consutarDetallesArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			BeanPaginador paginador, ArchitechSessionBean sessionBean) {

		//Creacion de objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		
		//Creacion de objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanDetalleArchivoCDASPID> lista = new
				ArrayList<BeanDetalleArchivoCDASPID>();
		List<Object> parametros = new ArrayList<Object>();
		//se establece el string del query
		EnumQuerysModuloCADSPID queryConsulta = EnumQuerysModuloCADSPID.
				QUERY_PANT_CARG_ARCH_HIST_DET_CADSPID;
		EnumQuerysModuloCADSPID queryConteo = EnumQuerysModuloCADSPID.
				QUERY_CONSULTA_REG_ARCH_ENCON_CADSPID;
		
		//Se agregan los parametros
		parametros.add(datosEntranda.getIdArchivo());
		parametros.add(datosEntranda.getIdArchivo());
		parametros.add(paginador.getRegIni());
		parametros.add(paginador.getRegFin());
		//Se setea query string
		String queryDetalle = queryConsulta.getQuery().toString();
		String queryConteoReg = queryConteo.getQuery().toString();
		try {
			//Se ejecuta la consulta a la base de datos
			responseDTO = HelperDAO.consultar(queryDetalle, parametros, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se valida que respuesta no sea nula ni vacia
			if(responseDTO.getResultQuery()!=null ||
					!responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				
				//Se mapea cada objeto de la lista
				for(HashMap<String, Object> temp : list) {
					
					//Se setean los datos en el bean datoObt
					BeanDetalleArchivoCDASPID datoObt = mapeoBeanDetalleArchivoCDASPID(temp);
					lista.add(datoObt);
					
					//Se setea el numero de registros en el obj respuesta
					respuesta.setRegistrosTotales(utilerias
							.getInteger(temp.get("CONT")));
				}
				
				//Se setea la lista al objeto respuesta
				respuesta.setListaDetalle(lista);
			}
			
			//Se hace la siguiente consulta a la base
			responseDTO = HelperDAO.consultar(queryConteoReg, 
					Arrays.asList(new Object[] {datosEntranda.getIdArchivo()}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
			
			//Se setean lo codigos y mensajes de error
			respuesta.setCodigoError(responseDTO.getCodeError());
			respuesta.setMensajeError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess eda) {
			
			//Se controla la excepcion y se setea el cod de error EC00011B
			error(String.format("Ha ocurrido un error al ejecutar el query: %s", queryDetalle));
			showException(eda);
			respuesta.setCodigoError(Errores.EC00011B);
			respuesta.setMensajeError(Errores.TIPO_MSJ_ERROR );
		}
		
		agregarALista(list, respuesta);
		
		//Se regresa el objeto de respuesta
		return respuesta;
	}
	
	/**
	 * Agregar a lista.
	 *
	 * @param list the list
	 * @param respuesta the respuesta
	 */
	private void agregarALista(List<HashMap<String, Object>> list, BeanResMonitorCargaArchHistCADSPID respuesta){
		// se valida que la lista no venga nula o vacia
		if (list != null && !list.isEmpty()) {
			//Se crean variables para reusar
			String opeGen = "OPE_GEN";
			String opeXGgen = "OPE_X_GEN";
			for (HashMap<String, Object> map : list) {
				//Se agrega a la respuesta el num de registros encontrados
				String regEncontrados = "";
				if(map.get(opeGen)==null || "".equals(map.get(opeGen))){
					regEncontrados = "0";
				}else {
					map.get(opeGen).toString();
				}
				respuesta.setRegistrosEncontrados(regEncontrados);
				
				//Se agrega a la respuesta el num de archivos
				String registrosArchivos = "";
				if(map.get(opeXGgen)==null || "".equals(map.get(opeXGgen))){
					registrosArchivos = "0";
				}else{
					registrosArchivos = map.get(opeXGgen).toString();
				}
				respuesta.setRegistrosArchivo(registrosArchivos);
			}
		}
	}
	
	/**
	 * Mapeo bean detalle archivo cdaspid.
	 *
	 * @param temp the temp
	 * @return the bean detalle archivo cdaspid
	 */
	//Metodo para mapear el bean de archivo spid
	private BeanDetalleArchivoCDASPID mapeoBeanDetalleArchivoCDASPID(HashMap<String, Object> temp){
		Utilerias utilerias = Utilerias.getUtilerias();
		//Se setean los datos en el bean datoObt
		BeanDetalleArchivoCDASPID datoObt = new
				BeanDetalleArchivoCDASPID();
		datoObt.setIdArchivo(utilerias.getString(temp.get("ID_PETICION")));					//Peticion
		datoObt.setClaveRastreo(utilerias.getString(temp.get("CVE_RASTREO")));				//Clave rastreo
		datoObt.setCuentaBeneficiario(utilerias.getString(temp.get("NUM_CUENTA_REC")));		//Numero cuenta receptora
		//Se le da formato al monto
		datoObt.setMontoPago(utilerias.formateaDecimales(temp.get("MONTO"),					//Monto
				Utilerias.FORMATO_DECIMAL_NUMBER));
		datoObt.setInstitucionEmisora(utilerias.getString(temp.get("NOMBRE_BCO_ORD")));		//Nombre bco ordenante
		datoObt.setFechaOperacion(utilerias.getString(temp.get("FCH_OPERACION")));			//Fecha operacion
		datoObt.setClaveInstitucion(utilerias.getString(temp.get("CVE_MI_INSTITUC")));		//Clave mi institucion
		datoObt.setInstitucionOrdinaria(utilerias.getString(temp.get("CVE_INST_ORD")));		//Clave institucion ordenante
		datoObt.setFolioPaquete(utilerias.getString(temp.get("FOLIO_PAQUETE")));			//Folio paquete
		datoObt.setFolioPago(utilerias.getString(temp.get("FOLIO_PAGO")));					//Folio pago
		
		//Se devuelve el objeto
		return datoObt;
	}
	
	//Metodo para generar el archivo historico
	@Override
	public BeanResMonitorCargaArchHistCADSPID generarArchivoHistorico(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda, 
			ArchitechSessionBean sessionBean) {
		
		//Se crean objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		EnumQuerysModuloCADSPID query = EnumQuerysModuloCADSPID.
				QUERY_GENERA_ARCHIVO_HIST_DET_CADSPID;
		
		//Se crean objetos de apoyo
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		String queryUpdate = query.getQuery().toString();
		
		//Se añaden los parametros
		parametros.add(datosEntranda.getIdArchivo());
		try {
			//Se ejecuta la actualizacion a base de datos
			responseDTO = HelperDAO.actualizar(queryUpdate, parametros, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se setea un codigo de respuesta exitoso
			respuesta.setCodigoError(Errores.CODE_SUCCESFULLY);
			respuesta.setMensajeError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess eda) {
			//Si hay un error se setea en el objeto de respuesta el cod EC000011B
			showException(eda);
			respuesta.setCodigoError(Errores.EC00011B);
			respuesta.setMensajeError(Errores.TIPO_MSJ_ERROR);
		} 
		
		//Se regresa el objeto respuesta
		return respuesta;
	}

	//Metodo para eliminar un archivo seleccionado
	@Override
	public BeanResMonitorCargaArchHistCADSPID eliminarSelecionArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean) {
		
		//Se crean objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		EnumQuerysModuloCADSPID queryDelete = EnumQuerysModuloCADSPID.
				QUERY_MARCADO_ELIMINADO_HIST_DET_CADSPID;
		EnumQuerysModuloCADSPID queryUpdate = EnumQuerysModuloCADSPID.
				QUERY_UPDATE_NUM_REG_ARCHIVO_HIST_DET_CADSPID;
		
		//Se crean objetos de apoyo
		ResponseMessageDataBaseDTO respuestaDTO = null;
		String queryElimina = queryDelete.getQuery();
		String queryActualiza = queryUpdate.getQuery();
		
		//Contandor de archivos
		int contador = 0;
		try {
			//Se recorre la lista de objetos
			info("TamaÃ±o de la lista a eliminar en el DAO: "+datosEntranda.getListaDetalle().size());
			for(BeanDetalleArchivoCDASPID datos : datosEntranda.getListaDetalle()) {
				
				//Se añaden los parametros de cada objeto
				List<Object> parametros = new ArrayList<Object>();
				info("Impre: "+datos.getIdArchivo());
				parametros.add(datos.getIdArchivo());
				parametros.add(datos.getClaveRastreo());
				parametros.add(datos.getFechaOperacion());
				parametros.add(datos.getClaveInstitucion());
				parametros.add(datos.getInstitucionOrdinaria());
				parametros.add(datos.getFolioPaquete());
				parametros.add(datos.getFolioPago());
				
				//Se ejecuta la actualizacion
				respuestaDTO = HelperDAO.actualizar(queryElimina, parametros,
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				
				//Se setea el cod y mensaje de error a partir de la respuestaDTO
				respuesta.setCodigoError(respuestaDTO.getCodeError());
				respuesta.setMensajeError(respuesta.getMensajeError());
				//Se aumenta el contador
				contador++;
			} 
			
			//Se ejecuta la actualizacion
			respuestaDTO = HelperDAO.actualizar(queryActualiza,
					Arrays.asList(new Object[] { Integer.valueOf(contador),
							datosEntranda.getIdArchivo() }), 
							HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			//Se setea el cod y mensaje de error a partir de la respuestaDTO
			respuesta.setCodigoError(respuestaDTO.getCodeError());
			respuesta.setMensajeError(respuestaDTO.getMessageError());
			info("Respuesta actualiza: "+respuesta.getCodigoError());
		}catch (ExceptionDataAccess e) {
			//Si hay un error se setea en el objeto de respuesta el cod EC000011B
				showException(e);
				respuesta.setCodigoError(Errores.EC00011B);
				respuesta.setMensajeError(Errores.TIPO_MSJ_ERROR);
			}
		//Se regresa el objeto respuesta
		return respuesta;
	}
	

}
