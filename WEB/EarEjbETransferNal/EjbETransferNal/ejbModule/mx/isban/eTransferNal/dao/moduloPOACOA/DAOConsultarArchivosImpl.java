/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultarArchivosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResArchRespuesta;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaArchRespuesta;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOConsultarArchivosImpl.
 *
 * @author ooamador
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultarArchivosImpl extends Architech implements DAOConsultarArchivos {
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 1996094815357998766L;

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * los archivos de respuesta
	 */
	private static final String QUERY_CONSULTA_ARCHIVOS = 
		 " SELECT TMP.NOMBRE_ARCH, TMP.ESTATUS, TMP.NUM_PAGOS, "+
	      " TMP.TOTAL_MONTO, TMP.MENSAJE, "+
	      " TMP.CONT "+
	      "  FROM ( "+
	            " SELECT TMP.*, ROWNUM AS RN "+
	                   " FROM ( "+
	                           " SELECT NOMBRE_ARCH, ESTATUS, NUM_PAGOS, "+
	                                 " TOTAL_MONTO, MENSAJE, "+
	                                 " CONTADOR.CONT "+
	                           " FROM ( "+
	                              " SELECT  COUNT(1) CONT "+
	                              " FROM TRAN_POA_COA_REC CANALES "+
	                              " WHERE FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
		                          " ) CONTADOR , TRAN_POA_COA_REC ADM "+ 
		                          " WHERE FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+ 
		                " ) TMP "+
		            " ) TMP "+
		            " WHERE RN BETWEEN ? AND ? ";
	

	/**
	 * Consulta de archivos a procesar.
	 *
	 * @param beanPaginador Bean para realizar la paginacion.
	 * @param fchOperacion String con la fecha de operacion
	 * @param modulo El objeto: modulo
	 * @param architechSessionBean Datos de session
	 * @return BeanResConsultaArchRespuesta Bean de respuesta
	 */
	@Override
	public BeanResConsultaArchRespuesta consultarArchivosProc(
			BeanPaginador beanPaginador,String fchOperacion, String modulo,
			ArchitechSessionBean architechSessionBean) {
		/** Se declaran las variables de respuesta **/
		final BeanResConsultaArchRespuesta beanResConsultaArchRespuesta = new BeanResConsultaArchRespuesta();
		BeanResArchRespuesta beanResArchRespuesta = null;
		List<HashMap<String, Object>> list = null;
		List<BeanResArchRespuesta> listBeanResArchRespuesta = Collections.emptyList();
		/** Declaracion del objeto de respuesta IDA  **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Declaracion de la lista para alamcenar los parametros que se iran en la consulta **/
		List<Object> parametros = Collections.emptyList();
		/** Se realiza la instancia a la clase utilerias */
		Utilerias utilerias = Utilerias.getUtilerias();
		/** Se declara un avariable que contiene la consulta actual **/
		String consultaEstatus = QUERY_CONSULTA_ARCHIVOS;
		/** Se valida el parametro para indentificar el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con el valor de la tabla correspondiente **/
			consultaEstatus = consultaEstatus.replaceAll("TRAN_POA_COA_REC", "TRAN_POA_COA_SPID_REC");
		}
		try {
			/** Se invoca al metodo para llenar la lista con los parametros **/
			parametros = agregarLista(fchOperacion, beanPaginador);
			/** Se ejecuta  la oepracion IDA **/
			responseDTO = HelperDAO.consultar(consultaEstatus,parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se valida el resultado de la operacion **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				/** Se inicializa la lista **/
				listBeanResArchRespuesta = new ArrayList<BeanResArchRespuesta>();
				/** Se le asigana el resultado de la operacion a la lista **/
				list = responseDTO.getResultQuery();
				/** Se comienza a recorrer la lista**/
				for (HashMap<String, Object> map : list) {
					/** Se declara un bean para almacenar cada registro de la lista **/
					beanResArchRespuesta = new BeanResArchRespuesta();
					/** Se setean los valores **/ 
					beanResArchRespuesta.setNombreArchivo(utilerias.getString(map.get("NOMBRE_ARCH")));
					beanResArchRespuesta.setEstatus(utilerias.getString(map.get("ESTATUS")));
					beanResArchRespuesta.setNumPagos(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("NUM_PAGOS")), Utilerias.FORMATO_NUMBER));
					beanResArchRespuesta.setTotalMonto(
							utilerias.formateaDecimales(utilerias.getBigDecimal(map.get("TOTAL_MONTO")), Utilerias.FORMATO_DECIMAL_NUMBER));
					beanResArchRespuesta.setDescripcion(utilerias.getString(map.get("MENSAJE")));
					/** Se asigna el bean ya llenado a la lista **/
					listBeanResArchRespuesta.add(beanResArchRespuesta);	
					beanResConsultaArchRespuesta.setTotalReg(utilerias.getInteger(map.get("CONT")));
				}
			}
			/** Se setea la lista al objeto de respuesta **/
			beanResConsultaArchRespuesta.setListaBeanResArchProc(listBeanResArchRespuesta);
			/** Se le asignan los codigos de la operacion al objeto de respuesta **/
			beanResConsultaArchRespuesta.setCodError(responseDTO.getCodeError());
			beanResConsultaArchRespuesta.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResConsultaArchRespuesta.setCodError(Errores.EC00011B);
			beanResConsultaArchRespuesta.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno del metodo **/
		return beanResConsultaArchRespuesta;
	}

	/**
	 * Agregar lista.
	 *
	 * @param fchOperacion El objeto: fch operacion
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto list
	 */
	private List<Object> agregarLista(String fchOperacion, BeanPaginador beanPaginador) {
		/** Se crea la lista **/
		List<Object> params = new ArrayList<Object>();
		/** Se asignan los parametros **/
		params.add(fchOperacion);
		params.add(fchOperacion);
		params.add(beanPaginador.getRegIni());
		params.add(beanPaginador.getRegFin());
		/** Se retorna la lista **/
		return params;
	}
}
