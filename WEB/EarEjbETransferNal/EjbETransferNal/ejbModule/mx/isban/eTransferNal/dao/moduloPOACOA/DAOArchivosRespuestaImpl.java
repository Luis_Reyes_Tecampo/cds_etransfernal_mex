/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOArchivosRespuestaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOArchivosRespuestaImpl.
 *
 * @author ooamador
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOArchivosRespuestaImpl extends Architech implements DAOArchivosRespuesta {
	
	/**
	 * Serial Number
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Query para obtener la institucion
	 */
	private static final String QUERY_CONS_MI_INSTITUCION = 
        " SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
              " WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL ";
	
	/** Query para registrar el nombre de archivo. */
	private static final String QUERY_INSERT_NOMBRA_RESP = 
		"INSERT INTO TRAN_POA_COA_REC (CVE_MI_INSTITUC,FCH_OPERACION,NOMBRE_ARCH, ESTATUS, CIFRADO, FCH_CAPTURA) VALUES (?,TO_DATE(?,'DD-MM-YYYY'),?,?,?,SYSDATE)";
	
	/** Query para consultar la duplicidad de nombre. */
	private static final String QUERY_CONSULTA_DUPLICIDAD = 
		"SELECT * FROM TRAN_POA_COA_REC WHERE TRIM(NOMBRE_ARCH) = ? AND CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY')";
	
	/***
	 * Constante del tipo String del query para actualizar el nombre de archivo
	 */
	private static final String QUERY_ACTUALIZAR_NOMBRE = 
		"UPDATE TRAN_POA_COA_REC SET ESTATUS = ?, " +
		"CIFRADO = ?, FCH_CAPTURA = SYSDATE WHERE CVE_MI_INSTITUC = ? AND TRIM(NOMBRE_ARCH) = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') ";
	
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	private static final String QUERY_FECHA_OPERACION =
	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	private static final String QUERY_CONSULTA_ACT_POA_COA =
		" SELECT CVE_MI_INSTITUC, NO_HAY_RETORNO, FCH_OPERACION, ACTIVACION, FASE, GENERAR "+
	    " FROM TRAN_POA_COA_CTRL WHERE CVE_MI_INSTITUC = ? AND "+ 
			" FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') ";

	/** La constante COSN_MODACTUAL. */
	private static final String COSN_MODACTUAL = "TRAN_POA_COA_REC";
	
	/** La constante CONS_MODSPID. */
	private static final String CONS_MODSPID = "TRAN_POA_COA_SPID_REC";

	/**
	 * Inserta el nombre del archivo en Base de Datos.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param fchOperacion String con la fecha de operacion
	 * @param institucion Institucion financiera
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase registarNombreArchivo(String nombreArchivo, String fchOperacion, String institucion, String modulo,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se declara una lista para setear los parametros SQL  **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los valores a la lista **/
		listaParametros.add(institucion);
		listaParametros.add(fchOperacion);
		listaParametros.add(nombreArchivo);
		listaParametros.add(0);
		listaParametros.add(0);
		/** Se declara una variable para evaluar la consulta **/
		String consultaEstatus = QUERY_INSERT_NOMBRA_RESP;
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus = consultaEstatus.replaceAll(COSN_MODACTUAL, CONS_MODSPID);
		}
		
		try {
			/** Se ejecuta la operacion de insertar **/
			HelperDAO.insertar(consultaEstatus, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno del metodo **/
		return beanResBase;
	}
	

	/**
	    * Metodo que valida el inicio del POA/COA .
	    *
	    * @param cveInstitucion String con la cve de la institucion
	    * @param fecha String con la fecha de operacion
	    * @param modulo El objeto: modulo
	    * @param architectSessionBean Objeto de la arquitectura
	    * @return BeanResValInicioPOACOA Bean de respuesta del tipo BeanResValInicioPOACOA
	    */
	@Override
	public BeanResValInicioPOACOA validaInicioPOACOA(String cveInstitucion, String fecha, String modulo,
			ArchitechSessionBean architectSessionBean){
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		/** Se crea una instancia a la clase Utilerias **/
		 Utilerias utilerias = Utilerias.getUtilerias();
		 /** Se crea el bean de respuesta **/
		 BeanResValInicioPOACOA beanResValInicioPOACOA = new BeanResValInicioPOACOA();
		 /** Se declara una lista para guardar la informacion de la operacion **/
	     List<HashMap<String,Object>> list = null;
	     /** De declara la instancia para utilizar el IDA **/
	     ResponseMessageDataBaseDTO responseDTO = null;
	     /** Se declara una variable para evaluar la consulta **/
	     String consultaEstatus = QUERY_CONSULTA_ACT_POA_COA;
	     /** Se valida el modulo **/
	     if ("2".equals(modulo)) {
	    	 /** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
	    	 consultaEstatus = consultaEstatus.replaceAll("TRAN_POA_COA_CTRL", "TRAN_POA_COA_SPID_CTRL");
	     }
	     try {
	    	 /** Se asignan los parametros a la lista **/
	    	 parametros.add(cveInstitucion);
	    	 parametros.add(fecha);
	    	 /**  Se ejecuta la operacion de consulta **/
		    	responseDTO = HelperDAO.consultar(consultaEstatus, parametros, 
		    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		    	/**  Se valida el resultado de la operacion **/
				if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
					/**  Se crea una lista para guardar el resultado de la operacion**/
					list = responseDTO.getResultQuery();
					/**  Se recorre la lista **/
					for(HashMap<String,Object> map:list){
						/**  Se setean los valores obtenidos**/
						beanResValInicioPOACOA.setCveMiInstitucion(utilerias.getString(map.get("CVE_MI_INSTITUC")));
						beanResValInicioPOACOA.setNoHayRetorno(utilerias.getString(map.get("NO_HAY_RETORNO")));
						beanResValInicioPOACOA.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
						beanResValInicioPOACOA.setActivacion(utilerias.getString(map.get("ACTIVACION")));
						beanResValInicioPOACOA.setFase(utilerias.getString(map.get("FASE")));
						beanResValInicioPOACOA.setGenerar(utilerias.getString(map.get("GENERAR")));
		  		  	}
				}
				/**  Se setea el codigo de error **/
				beanResValInicioPOACOA.setCodError(Errores.OK00000V);
		}catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResValInicioPOACOA.setCodError(Errores.EC00011B);
			beanResValInicioPOACOA.setMsgError(Errores.DESC_EC00011B);
		}
	     /**  Retorno del metodo **/
		return beanResValInicioPOACOA;
	}
	
	
	
	/**
	 * Obtiene la institucion .
	 *
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResConsMiInstDAO
	 */
	@Override
	public BeanResConsMiInstDAO consultaMiInstitucion(String modulo, ArchitechSessionBean sessionBean) {
		 /** Se crea el bean de respuesta **/
	      BeanResConsMiInstDAO beanResConsMiInstDAO =  new BeanResConsMiInstDAO();
	      /** Se declara una lista para guardar la informacion de la operacion **/
	      List<HashMap<String,Object>> list = null;
	      /** Se crea una instancia a la clase Utilerias **/
	      Utilerias utilerias = Utilerias.getUtilerias();
	      /** De declara la instancia para utilizar el IDA **/
	      ResponseMessageDataBaseDTO responseDTO = null;
	      String query = null;
	      /** Se declara una variable para evaluar la consulta **/
	      String queryEstatus = QUERY_CONS_MI_INSTITUCION;
	      /** Se valida el modulo **/
	      if ("2".equals(modulo)) {
	    	  /** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
	    	  queryEstatus = queryEstatus.replaceAll("ENTIDAD_SPEI", "ENTIDAD_SPID");
	      }
	      try {                   
	            query = queryEstatus;
	            /**  Se ejecuta la operacion de consulta **/
	            responseDTO = HelperDAO.consultar(query, Collections.emptyList(),
	                          HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	            /**  Se valida el resultado de la operacion **/
	            if(responseDTO.getResultQuery()!= null){
	            	/**  Se crea una lista para guardar el resultado de la operacion**/
	                   list = responseDTO.getResultQuery();
	                   Map<String,Object> mapResult = list.get(0);
	                   /** Se obtiene el valor **/
	                        beanResConsMiInstDAO.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
	            }
	      } catch (ExceptionDataAccess e) {
	    	  /**  Manejo de expceciones **/
	            showException(e);
	            beanResConsMiInstDAO.setCodError(Errores.EC00011B);
	            beanResConsMiInstDAO.setMsgError(Errores.DESC_EC00011B);
	      }
	        /**  Retorno del metodo **/
	        return beanResConsMiInstDAO;     
	    }

	/**
	 * Metodo que se encarga de consultar para saber si hay dup�licados.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param institucion Institucion financiera
	 * @param fchOperacion String fchOperacion
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO bean de respuesta
	 */
	@Override
	public BeanResDuplicadoArchDAO consultaDuplicidadNombre(String nombreArchivo,
			String institucion,String fchOperacion, String modulo, ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO = new BeanResDuplicadoArchDAO();
		beanResDuplicadoArchDAO.setEsDuplicado(Boolean.FALSE);
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();

		/** Se asignan los valores a la lista **/
		parametros.add(nombreArchivo);
		parametros.add(institucion);
		parametros.add(fchOperacion);
		 /** Se declara una variable para evaluar la consulta **/
		String consultaEstatus = QUERY_CONSULTA_DUPLICIDAD;
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus =  consultaEstatus.replaceAll(COSN_MODACTUAL, CONS_MODSPID);
		}
		try {	
			 /**  Se ejecuta la operacion de consulta **/
			responseDTO = HelperDAO.consultar(consultaEstatus,
					parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se valida el resultado de la operacion **/
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				beanResDuplicadoArchDAO.setEsDuplicado(Boolean.TRUE);
			}
			/**  Se setea el codigo de error **/
			beanResDuplicadoArchDAO.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResDuplicadoArchDAO.setCodError(Errores.EC00011B);
			beanResDuplicadoArchDAO.setMsgError(Errores.DESC_EC00011B);
		}
		/**  Retorno del metodo **/
		return beanResDuplicadoArchDAO;
	}



	/**
	 * Actualiza el nombre del archivo en Base de Datos.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param institucion Institucion financiera
	 * @param fchOperacion String con la fecha de operacion
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase actualizarNombreArchivo(String nombreArchivo,
			String institucion,String fchOperacion, String modulo, ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los valores a la lista **/
		listaParametros.add(0);
		listaParametros.add(0);
		listaParametros.add(institucion);
		listaParametros.add(nombreArchivo);
		listaParametros.add(fchOperacion);
		 /** Se declara una variable para evaluar la consulta **/
		String consultaEstatus = QUERY_ACTUALIZAR_NOMBRE;
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus =  consultaEstatus.replaceAll(COSN_MODACTUAL, CONS_MODSPID);
		}
		try {
			 /**  Se ejecuta la operacion  **/
			HelperDAO.actualizar(consultaEstatus, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		/**  Retorno del metodo **/
		return beanResBase;
	}
	
	/**
	* Metodo DAO para obtener la fecha de operacion.
	*
	* @param modulo El objeto: modulo
	* @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	* @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
	*/
	@Override
   public BeanResConsFechaOpDAO consultaFechaOperacion(String modulo, ArchitechSessionBean architechSessionBean){
		/** Se crea el bean de respuesta **/
      final BeanResConsFechaOpDAO beanResConsFechaOpArchRes = new BeanResConsFechaOpDAO();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTOArchRes = null;
      Utilerias utilerias = Utilerias.getUtilerias();
      /** Se declara una variable para evaluar la consulta **/
      String consultaEstatus = QUERY_FECHA_OPERACION;
      /** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus = consultaEstatus.replaceAll("TRAN_SPEI_CTRL", "TRAN_SPID_CTRL").replaceAll("ENTIDAD_SPEI", "ENTIDAD_SPID");
		}
      try {
    	  /**  Se ejecuta la operacion de consulta **/
    	  responseDTOArchRes = HelperDAO.consultar(consultaEstatus, Collections.emptyList(), 
    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
    	/**  Se valida el resultado de la operacion **/
		if(responseDTOArchRes.getResultQuery()!= null && !responseDTOArchRes.getResultQuery().isEmpty()){
			/**  Se crea una lista para guardar el resultado de la operacion**/
			list = responseDTOArchRes.getResultQuery();
			/** Se recorre la lista **/
			for(HashMap<String,Object> map:list){
				/**  Se formatea la fecha  **/
				beanResConsFechaOpArchRes.setFechaOperacion(
						utilerias.formateaFecha(map.get("FCH_OPERACION"), 
								Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
				/**  Seteo de codigos de error **/
				beanResConsFechaOpArchRes.setCodError(responseDTOArchRes.getCodeError());
				beanResConsFechaOpArchRes.setMsgError(responseDTOArchRes.getMessageError());
  		  }
		}
	} catch (ExceptionDataAccess e) {
		/**  Manejo de expceciones **/
        showException(e);
        beanResConsFechaOpArchRes.setCodError(Errores.EC00011B);
        beanResConsFechaOpArchRes.setMsgError(Errores.DESC_EC00011B);
	}
      /**  Retorno del metodo **/
      return beanResConsFechaOpArchRes;
   }

}
