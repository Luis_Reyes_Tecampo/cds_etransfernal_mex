/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOOrdenesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:27:35 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasOrdenes;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class DAOOrdenesImpl.
 *
 * Clase tipo DAO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * de accceso a los datos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOOrdenesImpl extends Architech implements DAOOrdenes{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5754999546244854866L;

	/** La constante CONSULTA_ORDENES_REPARACION. */
	private static final StringBuilder CONSULTA_ORDENES_REPARACION =
			new StringBuilder()
			.append("SELECT TM.* ")
			.append("FROM ( ")
				.append("SELECT TM.*, ROWNUM R ")
				.append("FROM ( ")
					.append("SELECT DISTINCT(TM.REFERENCIA), TM.CVE_COLA, TM.CVE_TRANSFE, ")
					.append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ")
					.append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")			
					.append("TM.ESTATUS, TM.COMENTARIO3, TMS.FOLIO_PAQUETE, TMS.FOLIO_PAGO, C.CONT ")
					.append("FROM TRAN_MENSAJE TM ")
					.append("JOIN [TB_EXTRA] TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM JOIN [TB_EXTRA] TMS ON TMS.REFERENCIA = TM.REFERENCIA WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR' [PARAM_ORIGEN]) C ")
					.append("WHERE CVE_MECANISMO='SPID' ")
					.append("AND ESTATUS='PR' ")
					.append("[PARAM_ORIGEN]")
					.append("[ORDENAMIENTO]")
				.append(") TM ")
			.append(") TM ")
			.append("WHERE TM.R BETWEEN ? AND ?");
	
	/** La constante CONSULTA_ORDENES_REPARACION_MONTOS. */
	private static final StringBuilder CONSULTA_ORDENES_REPARACION_MONTOS =
			new StringBuilder()
			.append(" SELECT SUM(TM.IMPORTE_ABONO) as MONTO_TOTAL ")
					.append("FROM TRAN_MENSAJE TM ")
					.append("JOIN [TB_EXTRA] TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM JOIN [TB_EXTRA] TMS ON TMS.REFERENCIA = TM.REFERENCIA WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR' [PARAM_ORIGEN]) C ")
					.append("WHERE CVE_MECANISMO='SPID' ")
					.append("AND ESTATUS='PR' ")
					.append("[PARAM_ORIGEN]")
					.append("[ORDENAMIENTO]");
	
	/** La constante UPDATE_ORDENES_REPARACION. */
	private static final StringBuilder UPDATE_ORDENES_REPARACION = 
			new StringBuilder().append("UPDATE TRAN_MENSAJE SET ESTATUS='LI' ")
							   .append("WHERE CVE_MECANISMO = 'SPID' ")
							   .append("AND REFERENCIA = ? ");
	
	/** La constante CONST_TABLE. */
	private static final String CONST_TABLE = "\\[TB_EXTRA\\]";
	
	/** La constante utils. */
	private static final UtileriasOrdenes utils = UtileriasOrdenes.getUtils();
	
	/**
	 * Obtener ordenes reparacion.
	 *
	 * Consultar los registros disponibles
	 * 
	 * @param modulo El objeto: modulo
	 * @param beanPaginador El objeto: bean paginador
	 * @param session El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @return Objeto bean res orden reparacion DAO
	 */
	@Override
	public BeanResOrdenReparacionDAO obtenerOrdenesReparacion(BeanModulo modulo, BeanPaginador beanPaginador,
			ArchitechSessionBean session, String sortField, String sortType) {
		//se declara el objeto BeanResOrdenReparacionDAO
		BeanResOrdenReparacionDAO beanResOrdenReparacionDAO= new BeanResOrdenReparacionDAO();
		//se declara el objeto ResponseMessageDataBaseDTO
    	ResponseMessageDataBaseDTO responseDTO = null;
    	try {
    		//Declara varieble string
    		String ordenamiento = "";
    		
    		//Se validan los parametros de entrada
    		//se agrega order by de la consulta
    		if (sortField != null && !"".equals(sortField)){
    			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
    		} else {
    			ordenamiento = " ORDER BY REFERENCIA ASC";
    		}
    		//Se arma la 
    		String query = CONSULTA_ORDENES_REPARACION.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento);
    		//Validando tipo de query
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, "TRAN_SPEI_ENV");
    		} else {
    			query = query.replaceAll(CONST_TABLE, "TRAN_MENSAJE_SPID");
    		}
    		//Se valida query
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		//obtiene origen
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		query = query.replaceAll("\\[PARAM_ORIGEN\\]", origen);
    		//Declara params
    		final List<Object> params = new ArrayList<Object>();
    		//Seteando parametros
    		params.add(beanPaginador.getRegIni());
    		params.add(beanPaginador.getRegFin());
    		//Ejecutando consulta
        	responseDTO = HelperDAO.consultar(query, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	//Parseando resultado de query
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 beanResOrdenReparacionDAO = utils.seteaDatos(beanResOrdenReparacionDAO, responseDTO);
        	} else {
        		beanResOrdenReparacionDAO.setListBeanOrdenReparacion(new ArrayList<BeanOrdenReparacion>());
        	}
        	//Seteando codigos de error
        	beanResOrdenReparacionDAO.setCodError(responseDTO.getCodeError());
        	beanResOrdenReparacionDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		//Ocurrio un error
    		showException(e);
    		beanResOrdenReparacionDAO.setCodError(Errores.EC00011B);
    		beanResOrdenReparacionDAO.setMsgError(Errores.DESC_EC00011B);
    	}
    	//return response
		return beanResOrdenReparacionDAO;
	}

	/**
	 * Obtener ordenes reparacion montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param session El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @return Objeto big decimal
	 */
	@Override
	public BigDecimal obtenerOrdenesReparacionMontos(BeanModulo modulo, ArchitechSessionBean session, String sortField,
			String sortType) {
		//se declara variable totalImporte
		BigDecimal totalImporte = BigDecimal.ZERO;
		//Se declara la variable responseDTO
		ResponseMessageDataBaseDTO responseDTO = null;
    	
    	try {
    		//declara variables
    		String ordenamiento = "";
    		
    		//Validando el armado del query
    		if (sortField != null && !"".equals(sortField)){
    			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
    		}
    		//Validando tipo de consulta
    		String query = CONSULTA_ORDENES_REPARACION_MONTOS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento);
    		if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll(CONST_TABLE, "TRAN_SPEI_ENV");
    		} else {
    			query = query.replaceAll(CONST_TABLE, "TRAN_MENSAJE_SPID");
    		}
    		//Valida query
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		//Agregando parametros
    		query = query.replaceAll("\\[PARAM_ORIGEN\\]", origen);
    		//Ejecuta consulta
        	responseDTO = HelperDAO.consultar(query, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	
        	//Obtiene importe Total
        	totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
    	} catch (ExceptionDataAccess e) {
    		//Ocurrio un error
    		error("Ocurrio un error al obtener el importe");
    		showException(e);
    		
    	}
    	//return importe
		return totalImporte;
	}
	
	/**
	 * Actualizar odenes reparacion.
	 *
	 * Envia el registro para actualizarlo. 
	 * 
	 * @param modulo El objeto: modulo
	 * @param beanOrdenReparacion El objeto: bean orden reparacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res orden reparacion DAO
	 */
	@Override
	public BeanResOrdenReparacionDAO actualizarOdenesReparacion(BeanModulo modulo,
			BeanOrdenReparacion beanOrdenReparacion, ArchitechSessionBean architechSessionBean) {
		//Creacion de objeto beanResOrdenReparacionDAO
		BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = new BeanResOrdenReparacionDAO();
	       try{	
	    	   //Obtiene query
	    	   String query = UPDATE_ORDENES_REPARACION.toString();
	    	   //Se valida query
	    	   query = ValidadorMonitor.cambiaQuery(modulo, query);
	    	   //Se declara lista params
	    	   final List<Object> params = new ArrayList<Object>();
	    	   //Setea parametros
	    	   params.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia());
	    	   //Ejecuta query
	          ResponseMessageDataBaseDTO responseDTO = HelperDAO.actualizar(query, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	        
	          //Seteando codigo de error
	          beanResOrdenReparacionDAO.setCodError(responseDTO.getCodeError());
	       } catch (ExceptionDataAccess e) {
	    	   //Ocurrio un error
	          showException(e);
	          beanResOrdenReparacionDAO.setCodError(Errores.EC00011B);
	          beanResOrdenReparacionDAO.setMsgError(Errores.DESC_EC00011B);
	       }
	       //return response
	       return beanResOrdenReparacionDAO;
	}

}
