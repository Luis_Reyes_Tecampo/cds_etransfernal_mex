/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: DAOConsultaDetalladaImpl.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      					Company 	Description
 * ------- 	----------------------	--------------------------	-----------	----------------------
 * 1.0.0   	07/02/2017 05:03:37 PM 	Juan Jesus Beltran R. 		Isban 		Creacion
 * 1.0.1	04/04/2018 12:00:00 AM  Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */
package mx.isban.eTransferNal.dao.ws;
//Imports javax
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
//Imports etransfernal
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.metrics.dto.DTOMetricsE;
//Imports metrics
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
/**
 * Class DAOConsultaDetalladaImpl.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaDetalladaImpl extends Architech implements DAOConsulaDetallada{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -6764937031503704823L;
	/** constante de nombre de servicio */
	private static final String TINFODET =  "TINFODET";
	
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** Bitacorizacion de metricas */
	@EJB private BitacorizaMetrics boMetrics;
	/** Utilerias Metricas MetricsSenderInit*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	/** The timeIn*/
	private transient String timeIn = null;
	/** The timeOut*/
	private transient String timeOut = null;
	/** Codigo de error del WS*/
	private transient String codeError = null;
	/** Variable de error para servicio externo*/
	private transient String errWS = "N";
	// END [ADD: JMFR CSA Monitoreo Metrics]

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.ws.DAOConsulaDetallada#consultaDetalle(java.lang.String)
	 */
	@Override
	public ResBeanEjecTranDAO consultaDetalle(String trama) {
		// Se crea objeto de respuesta
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		// Se obtiene instancia de utileria para ejecuciones MQ
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		senderInit.initMetrics();//Se inicializan dto's para metricas
		// se obtiene tiempo de inicio de peticion
		timeIn = String.valueOf(System.currentTimeMillis());
		// se ejecuta la transaccion
		resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, TINFODET, "ARQ_MENSAJERIA");
		// se obtiene tiempo de respuesta
		timeOut = String.valueOf(System.currentTimeMillis());
		// Se valida que la operacion fue exitosa
		if (ResBeanEjecTranDAO.COD_EXITO.equalsIgnoreCase(resBeanEjecTranDAO.getCodError())) {
			codeError = ResBeanEjecTranDAO.COD_EXITO;
		}else {// en caso de error se inicia monitoreo metrics para error servicio externo
			errWS = "S"; //Se asigna estatus de error para peticion servicio externo
			showError(utileriasMQ.getMetricsE());
			boMetrics.bitacorizaError(senderInit.getDTOError(utileriasMQ.getMetricsE().getCodeError(), 
					utileriasMQ.getMetricsE().getMsgError(),utileriasMQ.getMetricsE().getTraza()));
			codeError = "30D"; // Se asigna codigo de error para peticion servicio externo
		}
		insertaSExterno();// Se ejecuta insert asincrono para peticion servicio externo
		// se retorna respuesta del servicio
		return resBeanEjecTranDAO;
	}
	
	/**
	 * Metodo para ejecutar insert asincrono 
	 * Monitoreo servicios externos.
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 */
	private void insertaSExterno() {
		boMetrics.bitacorizaWs(senderInit.getMetricsWS(codeError,TINFODET,
				"GENERICMQ.ARQ_MENSAJERIA: jms/FactoryMensajeriaNacional - jms/QueueSendMensajeriaNacional - jms/QueueReceiveMensajeriaNacional", 
				"MQ","JMS",timeIn,timeOut,errWS));
	}
	
	/**
	 * Metodo para obtener datos del error.
	 * @param metricsE bean con los parametros de error
	 */
	private void showError(DTOMetricsE metricsError) {
		if(metricsError.isError()){
			info(metricsError.getMsgError());
			info(metricsError.getCodeError());
			info(metricsError.getTraza());
		}
	}
}