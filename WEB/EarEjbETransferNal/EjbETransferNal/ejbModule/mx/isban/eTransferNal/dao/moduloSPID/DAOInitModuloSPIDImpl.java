/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOInitModuloSPIDImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.Collections;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResInitModuloSPIDDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para el InitModuloSPID
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOInitModuloSPIDImpl extends Architech implements DAOInitModuloSPID {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7354899016552148572L;
	
	/**
	 * 
	 */
	private static final String ENTIDAD_SPID = "ENTIDAD_SPID";
	
	/**
	 * 
	 */
	private static final StringBuilder CONSULTA_CVE_FCH = 
			new StringBuilder().append("SELECT TCU.CV_VALOR, TSC.FCH_OPERACION ")
							   .append("FROM TRAN_CONTIG_UNIX TCU, TRAN_SPID_CTRL TSC ")
							   .append("WHERE TCU.CV_VALOR=TSC.CVE_MI_INSTITUC AND TCU.CV_PARAMETRO = '")
							   .append(ENTIDAD_SPID).append("'");
	
	/**
	 * Metodo encargado de obtener la informacion.
	 *
	 * @param tipo El objeto: tipo SPEI o SPID
	 * @return obtenerInformacionHeaders Objeto del tipo @see BeanResInitModuloSPIDDAO
	 * @throws BusinessException exception
	 */
	@Override
	public BeanResInitModuloSPIDDAO obtenerInformacionHeaders(String tipo) {
		final BeanResInitModuloSPIDDAO beanResInitModuloSPIDDAO= new BeanResInitModuloSPIDDAO();
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	
    	String query = CONSULTA_CVE_FCH.toString();
    	
    	try {              	
    		if ("SPEI".equalsIgnoreCase(tipo)) {
    			query = query.replaceAll("SPID", "SPEI");
    		}
        	responseDTO = HelperDAO.consultar(query, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){       		 
        			 
        			 for(Map<String,Object> mapResult : responseDTO.getResultQuery()){
        				 beanResInitModuloSPIDDAO.setCveInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
        				 beanResInitModuloSPIDDAO.setFechaOperacion(utilerias.formateaFecha(
        						 mapResult.get("FCH_OPERACION") , Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
    				 }
        	}
        	
        	beanResInitModuloSPIDDAO.setCodError(responseDTO.getCodeError());
        	beanResInitModuloSPIDDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		beanResInitModuloSPIDDAO.setCodError(Errores.EC00011B);
    	}
		return beanResInitModuloSPIDDAO;
	}

}
