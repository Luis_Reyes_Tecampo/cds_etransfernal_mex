/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOTraspasoFondosSPIDSIACImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para el traspaso de Fondos SPID SIAC
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOTraspasoFondosSPIDSIACImpl extends Architech implements DAOTraspasoFondosSPIDSIAC{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5892135789673341974L;
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_SALDO
	 */
	private static final String QUERY_SALDO =
		"SELECT CVE_MI_INSTITUC,SALDO FROM TRAN_SPID_SALDO S WHERE CVE_MI_INSTITUC = "+
		"   (SELECT CV_VALOR FROM TRAN_CONTIG_UNIX " +
		"      WHERE CV_PARAMETRO = 'ENTIDAD_SPID')";
	
	@Override
    public BeanAdministraSaldoDAO consultaSaldo(ArchitechSessionBean sessionBean) {
    	BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<Object> parametros = Collections.emptyList();
    	String query = null;
    	try {              	
    		query = QUERY_SALDO;
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanAdministraSaldoDAO.setSaldoPrincipal(utilerias.getString(map.get("SALDO")).trim());
	  		  	}
			}else{
				beanAdministraSaldoDAO.setSaldoPrincipal("0");
			}
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  		beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
	  	}
        return beanAdministraSaldoDAO;	
    }
}
