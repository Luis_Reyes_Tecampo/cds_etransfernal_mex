/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOExcepCtasFideicomisoImpl.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      08/09/2019 10:17:24 AM     Uriel Alfredo Botello R.    VSF        Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepCtasFideicomiso;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasExcepCtasFideico;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOExcepCtasFideicomisoImpl.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOExcepCtasFideicomisoImpl extends Architech implements DAOExcepCtasFideicomiso {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2637175630111952510L;
	
	/** La constante FILTRO_CTA. */
	private static final String FILTRO_CTA = "\\[FILTRO_CTA\\]";
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private transient Utilerias utilerias = Utilerias.getUtilerias();
	
	/** La variable que contiene informacion con respecto a: util ctas fideico. */
	private transient UtileriasExcepCtasFideico utilCtasFideico = UtileriasExcepCtasFideico.getInstancia();
	

	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_CTAS_FIDEICO_EXENTAS "
			.concat("WHERE TRIM(NUM_CUENTA) = TRIM(UPPER(?))");
	
	/**
	 * Consulta cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param beanCtasFideico El objeto: bean ctas fideico --> Objeto con datos de filtrado
	 * @return Objeto bean excep ctas fideicomiso --> Objeto de respuesta
	 */
	@Override
	public BeanExcepCtasFideicomiso consultaCuentas(ArchitechSessionBean sessionBean,BeanExcepCtasFideicomiso beanCtasFideico) {
		BeanExcepCtasFideicomiso response = new BeanExcepCtasFideicomiso();
		/** Se declaran variables */
		ResponseMessageDataBaseDTO responseDTO = null;
		String query = "SELECT REG.INDICE, REG.NUM_CUENTA FROM (SELECT ROWNUM INDICE, NUM_CUENTA FROM TRAN_CTAS_FIDEICO_EXENTAS [FILTRO_CTA]) REG "
				+ " WHERE REG.INDICE BETWEEN ? AND ?";
		List<Object> parametros = new ArrayList<Object>();
		/** Se hace filtrado con los datos de entrada */
		String filtro = utilCtasFideico.getFiltro(beanCtasFideico.getBeanFilter(), "WHERE");
		query = query.replaceAll(FILTRO_CTA, filtro);
		/** Se asignan los parametros */
		parametros.add(beanCtasFideico.getBeanPaginador().getRegIni());
		parametros.add(beanCtasFideico.getBeanPaginador().getRegFin());
		
		try {
			/** ejecuta consulta */
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			List<HashMap<String,Object>> list = null;
			List<BeanCtaFideicomiso> listaCuentas = new ArrayList<BeanCtaFideicomiso>();
			/** Valida la respuesta de consulta */
			if(responseDTO.getResultQuery()!= null && responseDTO.getResultQuery().size() > 0){
	    		//se asigna el resultado
				list = responseDTO.getResultQuery();    				
				/**inicializa el map**/
				listaCuentas = recorreResultado(list);
				/** asigna datos de salida */
				response.setListaCuentas(listaCuentas);
				response.getBeanError().setCodError(Errores.OK00000V);
				response.getBeanError().setMsgError(responseDTO.getMessageError());
			 } else {
				 /** Si no encontro información regresa mensaje */
				response.getBeanError().setCodError(Errores.ED00011V);
				response.getBeanError().setMsgError(Errores.DESC_ED00011V);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			 }
		} catch (ExceptionDataAccess e) {
			/** si fallo la conexion a BD */
			showException(e);
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorna la respuesta **/
		return response;
	}
	
	/**
	 * Recorre resultado.
	 *
	 * @param list El objeto: list de informacion encontrada
	 * @return Objeto list con informacion seteada
	 */
	public List<BeanCtaFideicomiso> recorreResultado(List<HashMap<String,Object>> list) {
		/** inicializa variables */
		List<BeanCtaFideicomiso> listaCuentas = new ArrayList<BeanCtaFideicomiso>();
		/** recorre resultado de consulta */
		for(Map<String,Object> map:list){
			if(map.size() > 0) {
				/** recupera informacion de la consulta */
				BeanCtaFideicomiso cuentaBean = new BeanCtaFideicomiso();
				cuentaBean.setCuenta(utilerias.getString(map.get("NUM_CUENTA")).trim());
				listaCuentas.add(cuentaBean);
			}
		}
		/** Retorna la respuesta **/
		return listaCuentas;
	}

	/**
	 * Alta cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param cuenta El objeto: cuenta --> Valor del registro
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepCtasFideicomiso altaCuentas(ArchitechSessionBean sessionBean, String cuenta) {
		/** define variables */
		String query = "INSERT INTO TRAN_CTAS_FIDEICO_EXENTAS(NUM_CUENTA) VALUES(?)";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(cuenta);
		return ejecutaBasico(query, parametros);
	}

	/**
	 * Baja cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param cuenta El objeto: cuenta --> Valor del registro
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepCtasFideicomiso bajaCuentas(ArchitechSessionBean sessionBean, String cuenta) {
		/** se declaran variables */
		String query = "DELETE FROM TRAN_CTAS_FIDEICO_EXENTAS WHERE TRIM(NUM_CUENTA) = ?";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(cuenta);
		return ejecutaBasico(query, parametros);
	}
	
	/**
	 * Ejecuta basico.
	 *
	 * @param query El objeto: query a ejecutar
	 * @param parametros El objeto: parametros a ingresar en el query
	 * @return Objeto bean excep ctas fideicomiso en respuesta a la operacion
	 */
	private BeanExcepCtasFideicomiso ejecutaBasico(String query,List<Object> parametros) {
		BeanExcepCtasFideicomiso response = new BeanExcepCtasFideicomiso();
		try {
			/** ejecuta consulta */
			HelperDAO.eliminar(query,parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.getBeanError().setCodError(Errores.OK00000V);
			response.getBeanError().setMsgError(Errores.DESC_OK00000V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} catch (ExceptionDataAccess e) {
			/** entra aqui si hubo un problema de conexion o similar */
			showException(e);
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorna la respuesta **/
		return response;
	}
	
	/**
	 * Consulta total registros.
	 *
	 * @param cuenta El objeto: cuenta --> Valor buscado
	 * @return Objeto int --> total de registros obtenidos
	 */
	@Override
	public int totalRegistros(String cuenta) {
		/** se declaran variables */
		int total = 0;
		String query = "SELECT COUNT(1) TOTAL FROM TRAN_CTAS_FIDEICO_EXENTAS [FILTRO_CTA]";
		/** se establece el filtrado */
		if(cuenta == null ||cuenta.isEmpty()) {
			query = query.replaceAll(FILTRO_CTA, "");
		} else {
			query = query.replaceAll(FILTRO_CTA, " WHERE TRIM(NUM_CUENTA) LIKE ").concat("'%".concat(cuenta.trim()).concat("%'"));
		}
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			/** ejecuta la consulta */
			List<HashMap<String,Object>> list = null;
			responseDTO = HelperDAO.consultar(query, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** obtiene los resultados */
			if(responseDTO.getResultQuery()!= null && responseDTO.getResultQuery().size() > 0){
				list = responseDTO.getResultQuery();
				/** setea el dato de retorno */
				total = Integer.parseInt(utilerias.getString(list.get(0).get("TOTAL")));
			}
		} catch (ExceptionDataAccess e) {
			/** entra aqui si hubo un problema de conexion */
			showException(e);
		}
		return total;
	}

	/**
	 * Modificacion cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param ctaOld El objeto: cta old --> Valor actual
	 * @param ctaNew El objeto: cta new --> Valor nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	@Override
	public BeanExcepCtasFideicomiso modificacionCuentas(ArchitechSessionBean sessionBean, String ctaOld,
			String ctaNew) {
		/** se declaran variables */
		BeanExcepCtasFideicomiso response = new BeanExcepCtasFideicomiso();
		int respDuplicado = -1;
		BeanExcepCtasFideicomiso beanCtasFideico = new BeanExcepCtasFideicomiso();
		beanCtasFideico.getBeanFilter().setCuenta(ctaNew);
		beanCtasFideico.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String query = "UPDATE TRAN_CTAS_FIDEICO_EXENTAS SET NUM_CUENTA = ? WHERE TRIM(NUM_CUENTA) = ?";
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(ctaNew.trim());
		parametros.add(ctaOld.trim());
		
		respDuplicado = buscarRegistro(ctaNew,sessionBean);
		
		if(respDuplicado == 0) {
			try {
				/** se ejecuta consulta */
				HelperDAO.actualizar(query,parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.getBeanError().setCodError(Errores.OK00000V);
				response.getBeanError().setMsgError(Errores.DESC_OK00000V);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			} catch (ExceptionDataAccess e) {
				/** entra aqui si ocurrio un problema de conexion */
				showException(e);
				response.getBeanError().setCodError(Errores.EC00011B);
				response.getBeanError().setMsgError(Errores.DESC_EC00011B);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		} else {
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError(Errores.DESC_ED00130V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		/** Retorna la respuesta **/
		return response;
	}
	
	/**
	 * Buscar registro.
	 * 
	 * * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param ctaNew El objeto: cta new --> Valor buscado
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto int --> Respuesta entera de la busqueda
	 */
	@Override
	public int buscarRegistro(String ctaNew, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = new ArrayList<Object>();
			parametros.add(ctaNew);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
}
