/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOUsuarioImpl.java
*
* Control de versiones:
*
* Version Date/Hour           By       Company Description
* ------- ------------------- -------- ------- --------------
* 1.0     22/06/2011 18:07:17 O Acosta ISBAN   Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioPerfilesDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanUsuario;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Implementacion del Acceso a Datos para los Usuarios con
 * acceso a Transfer.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOUsuarioImp extends Architech
        implements DAOUsuario {


    /**
	 * 
	 */
	private static final long serialVersionUID = -1949345351481832482L;
	
	/**Constante privada que almacena la consulta de usuario*/
	private final static String QUERY_CONSULTA_USUARIO =
    	"SELECT CVE_USUARIO,ESTATUS_USR,FCH_ULT_ACC,FCH_ACT FROM TR_SEG_CAT_USUARIO WHERE TRIM(CVE_USUARIO) = TRIM(?)";
	
	/**Constante privada que almacena la actualiza usuario*/
	private final static String QUERY_UPDATE_USUARIO =
    	" UPDATE TR_SEG_CAT_USUARIO SET ESTATUS_USR = ?, SERVICIO=?, CVE_USUARIO_OPER=?, "+
    	" FLAG_BLOQUEOSAM =?, FLAG_ELIMINASAM=?, "+
    	" FCH_ULT_ACC = sysdate, FCH_ACT = sysdate   " +
    	" WHERE TRIM(CVE_USUARIO) = ? ";
	
	/**Constante privada que almacena el query insert*/
	private final static String QUERY_INSERT_USUARIO =
		"INSERT INTO TR_SEG_CAT_USUARIO (" +
		" CVE_USUARIO,ESTATUS_USR,FCH_ULT_ACC," +
		" FCH_ACT,SERVICIO,CVE_USUARIO_OPER," +
		" FLAG_BLOQUEOSAM,FLAG_ELIMINASAM) " +
		" VALUES( ?,?,sysdate,sysdate,?,?,?,?) ";
	


	/**Constante que almacena el insert de usuario perfil*/
	private final static String QUERY_INSERT_USUARIO_PERFIL =
		" INSERT INTO TR_SEG_CFG_USUARIO_PERFIL (CVE_USUARIO, CVE_PERFIL,FCH_ACT,SERVICIO,CVE_USUARIO_OPER) "+
		" VALUES (?,?,sysdate,?,?) ";
	
	/**Constante que almacena el insert de usuario perfil*/
	private final static String QUERY_DELETE_USUARIO_PERFIL =
		" DELETE TR_SEG_CFG_USUARIO_PERFIL WHERE TRIM(CVE_USUARIO) = ? AND CVE_PERFIL = ?";
	
	/**Constante que almacena la consulta de perfiles*/
	private final static String QUERY_CONSULTA_USUARIO_PERFIL =
		" SELECT UP.CVE_PERFIL, CP.CVE_MODULO, CP.TXT_PERFIL_SAM,CP.TXT_PERFIL_TRFP "+
		"	FROM TR_SEG_CFG_USUARIO_PERFIL UP, TR_SEG_CAT_PERFIL CP "+
		"	 WHERE UP.CVE_PERFIL = CP.CVE_PERFIL AND "+
		"	 TRIM(UP.CVE_USUARIO) = ? ";


    /** {@inheritDoc} */
    public BeanResUsuarioDAO consultaUsuarioPorClave(String clave,
            ArchitechSessionBean sessionBean) throws BusinessException {
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
    	BeanUsuario beanUsuario = null;
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_USUARIO, 
	    			Arrays.asList(new Object[]{clave}), 
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanUsuario = new BeanUsuario();
					beanUsuario.setCveUsuario(utilerias.getString(map.get("CVE_USUARIO")));
					beanUsuario.setEstatusUsuario(utilerias.getString(map.get("ESTATUS_USR")));
					beanUsuario.setServicio(utilerias.getString(map.get("SERVICIO")));
					beanUsuario.setFechaUltimoAcceso(utilerias.getString(map.get("FCH_ULT_ACC")));
					beanUsuario.setFechaUltimoAcceso(utilerias.getString(map.get("FCH_ACT")));
					beanResUsuarioDAO.setUsuario(beanUsuario);
	  		  	}
			}
			beanResUsuarioDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioDAO.setCodError(Errores.EC00011B);
		}
		return beanResUsuarioDAO;
    }
    
    /** {@inheritDoc} */
    public void actualizaUsuario(BeanUsuario usuario,
            ArchitechSessionBean sessionBean) throws BusinessException {
        debug("Inicia la actualizacion de usuario");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        ResponseMessageDataBaseDTO responseDTO = null;   
        try {
	    	responseDTO = HelperDAO.actualizar(QUERY_UPDATE_USUARIO, 
	    			Arrays.asList(new Object[]{usuario.getEstatusUsuario(),
	    					usuario.getServicio(), usuario.getCveUsuario(),usuario.getFlagBloqueoSam(),
	    					usuario.getFlagEliminaSam(), usuario.getCveUsuario().trim()}), 
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			beanResUsuarioDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioDAO.setCodError(Errores.EC00011B);
		}
        debug("Termina DAO :: actualizaUsuario usuario");
    }


      /** {@inheritDoc} */
    public BeanResUsuarioDAO insertaUsuario(BeanUsuario usuario,
            ArchitechSessionBean sessionBean) throws BusinessException {
        debug("Inicia la insercion de usuario");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try {
	    	responseDTO = HelperDAO.insertar(QUERY_INSERT_USUARIO, 
	    			Arrays.asList(new Object[]{usuario.getCveUsuario(),usuario.getEstatusUsuario(),
	    					usuario.getServicio(),usuario.getCveUsuarioUltimaOperacion(),
	    					usuario.getFlagBloqueoSam(),usuario.getFlagEliminaSam()		
	    			}),
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			beanResUsuarioDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioDAO.setCodError(Errores.EC00011B);
		}
        debug("Termina DAO :: insertar usuario");
        return beanResUsuarioDAO;
    }

   
    /** {@inheritDoc} */
    public BeanResUsuarioPerfilesDAO consultaUsuarioPerfiles(String cveUsuario,
            ArchitechSessionBean sessionBean) throws BusinessException {
        debug("Inicia la consultaUsuarioPerfiles");
        BeanResUsuarioPerfilesDAO beanResUsuarioPerfilesDAO=new BeanResUsuarioPerfilesDAO();
        Utilerias utilerias = Utilerias.getUtilerias();
        BeanPerfil beanPerfil = null;
        List<HashMap<String,Object>> list = null;
        List<BeanPerfil> listPerfiles = Collections.emptyList();
	    ResponseMessageDataBaseDTO responseDTO = null;
        try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_USUARIO_PERFIL, 
	    			Arrays.asList(new Object[]{cveUsuario}),
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
	    	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    		listPerfiles = new ArrayList<BeanPerfil>();
	    		list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanPerfil = new BeanPerfil();
					beanPerfil.setCvePerfil(utilerias.getString(map.get("CVE_PERFIL")));
					beanPerfil.setCveModulo(utilerias.getString(map.get("CVE_MODULO")));
					beanPerfil.setTxtPerfilSam(utilerias.getString(map.get("TXT_PERFIL_SAM")));
					beanPerfil.setTxtPerfilTrfp(utilerias.getString(map.get("TXT_PERFIL_TRFP")));
					listPerfiles.add(beanPerfil);		
				}
				beanResUsuarioPerfilesDAO.setListPerfiles(listPerfiles);
	    	}
			beanResUsuarioPerfilesDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioPerfilesDAO.setMsgError(responseDTO.getMessageError());				
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioPerfilesDAO.setCodError(Errores.EC00011B);
		}
        debug("Termina DAO :: insertar consultaUsuarioPerfiles");
        return beanResUsuarioPerfilesDAO;
    }
    
    /** {@inheritDoc} */
    public BeanResUsuarioDAO insertaUsuarioPerfiles(BeanUsuario usuario,List<BeanPerfil> perfiles,
            ArchitechSessionBean sessionBean) throws BusinessException {
        debug("Inicia la insercion de insertaUsuarioPerfiles");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        for(BeanPerfil beanPerfil: perfiles){
        	insertaUsuarioPerfil(usuario,beanPerfil);
        }
        debug("Termina DAO :: insertar insertaUsuarioPerfiles");
        return beanResUsuarioDAO;
    }
    
    /** {@inheritDoc} */
    public BeanResUsuarioDAO insertaUsuarioPerfil(BeanUsuario usuario,BeanPerfil perfil) throws BusinessException {
        debug("Inicia la insercion de insertaUsuarioPerfil");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try {
	    	responseDTO = HelperDAO.insertar(QUERY_INSERT_USUARIO_PERFIL, 
	    			Arrays.asList(new Object[]{usuario.getCveUsuario(),perfil.getCvePerfil(),usuario.getServicio(),
	    					usuario.getCveUsuario()
	    			}),
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			beanResUsuarioDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioDAO.setCodError(Errores.EC00011B);
		}
        debug("Termina DAO :: insertar insertaUsuarioPerfil");
        return beanResUsuarioDAO;
    }
    /** {@inheritDoc} */
    public BeanResUsuarioDAO eliminarUsuarioPerfiles(BeanUsuario usuario,List<BeanPerfil> perfiles,
            ArchitechSessionBean sessionBean) throws BusinessException {
        debug("Inicia la insercion de eliminarUsuarioPerfiles");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        for(BeanPerfil beanPerfil: perfiles){
        	eliminarUsuarioPerfil(usuario,beanPerfil);
        }
        debug("Termina DAO :: insertar eliminarUsuarioPerfiles");
        return beanResUsuarioDAO;
    }
    /** {@inheritDoc} */
    public BeanResUsuarioDAO eliminarUsuarioPerfil(BeanUsuario usuario,BeanPerfil perfil) throws BusinessException {
        debug("Inicia la insercion de eliminarUsuarioPerfil");
        BeanResUsuarioDAO beanResUsuarioDAO = new BeanResUsuarioDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try {
	    	responseDTO = HelperDAO.insertar(QUERY_DELETE_USUARIO_PERFIL, 
	    			Arrays.asList(new Object[]{usuario.getCveUsuario(),perfil.getCvePerfil()
	    			}),
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			beanResUsuarioDAO.setCodError(responseDTO.getCodeError());
			beanResUsuarioDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResUsuarioDAO.setCodError(Errores.EC00011B);
		}
        debug("Termina DAO :: insertar eliminarUsuarioPerfil");
        return beanResUsuarioDAO;
    }
  
}
