/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOHorariosSPEIImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanHorariosSPEI;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIBO;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el catalogo de horarios para envio de pagos por SPEI
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOHorariosSPEIImpl extends Architech implements DAOHorariosSPEI {
	
	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = 2144018011403203522L;
	/**
	 * Constante privada del tipo String que almacena el query para el catalogo
	 */
	private static final String QUERY_BUSQUEDA =  "SELECT TMP.CVE_MEDIO_ENT,TMP.CVE_TRANSFE, TMP.CVE_OPERACION,"
												 +"      TMP.TOPO_PRI, TMP.HORA_INICIO, TMP.HORA_CIERRE, TMP.FLG_INHAB,"
												 +"      TMP.CONT"
												 +" FROM ("
												 +"        SELECT TMP.*, ROWNUM AS RN"
												 +"          FROM ("
												 +"                  SELECT CVE_MEDIO_ENT  ,CVE_TRANSFE" 
												 +"                        ,CVE_OPERACION ,TOPO_PRI "
												 +"                        ,HORA_INICIO ,HORA_CIERRE "
												 +"                        ,FLG_INHAB  ,CONTADOR.CONT"
												 +"                    FROM ( "
												 +"                           SELECT COUNT(1) CONT"
												 +"                             FROM TRAN_SPEI_CAT_HENV CHS"
												 +"                            WHERE CVE_MEDIO_ENT = NVL(?,CVE_MEDIO_ENT)"
												 +"                            		 AND CVE_TRANSFE = NVL(?,CVE_TRANSFE)"
												 +"                                  AND CVE_OPERACION = NVL(?,CVE_OPERACION)"
												 +"                         ) CONTADOR , TRAN_SPEI_CAT_HENV CHS"
												 +"	                        WHERE TRIM(CVE_MEDIO_ENT) = NVL(?,TRIM(CVE_MEDIO_ENT))"
												 +"                           AND TRIM(CVE_TRANSFE) = NVL(?,TRIM(CVE_TRANSFE))"
												 +"                           AND TRIM(CVE_OPERACION) = NVL(?,TRIM(CVE_OPERACION))"
												 +"                      ORDER BY CVE_MEDIO_ENT"
												 +"	                ) TMP"
												 +"	           ) TMP"
												 +"	          WHERE RN BETWEEN ? AND ?"
												 +"	          ORDER BY CVE_MEDIO_ENT, CVE_TRANSFE";
	/**
	 * Constante privada del tipo String que almacena el query para realizar el insert
	 */
	private static final String QUERY_AGREGAR  = "INSERT INTO TRAN_SPEI_CAT_HENV( CVE_MEDIO_ENT ,CVE_TRANSFE ,CVE_OPERACION " +
    											 ",TOPO_PRI  ,HORA_INICIO	,HORA_CIERRE ,FLG_INHAB )   " +
    											 " VALUES (?,?,?,?,?,?,?)";
	/**
	 * Constante privada del tipo String que almacena el query para realizar el update
	 */
	private static final String  QUERY_MODIFICAR = "UPDATE TRAN_SPEI_CAT_HENV SET" +
	"            TOPO_PRI=? , HORA_INICIO=? , 	HORA_CIERRE=? , FLG_INHAB=? "+
	"   WHERE   TRIM(CVE_MEDIO_ENT)=? AND 	TRIM(CVE_TRANSFE)=? AND TRIM(CVE_OPERACION)=?  ";
	/**
	 * Constante privada del tipo String que almacena el query para realizar el delete
	 */
	private static final String  QUERY_ELIMINA ="DELETE TRAN_SPEI_CAT_HENV"+
	"   WHERE   trim(CVE_MEDIO_ENT)=trim(?) AND trim(CVE_TRANSFE)=(?) AND trim(CVE_OPERACION)=trim(?) ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_KEY
	 */
	private static final String  QUERY_CONSULTA_KEY =
	 " SELECT CVE_MEDIO_ENT, CVE_TRANSFE,CVE_OPERACION, TOPO_PRI, HORA_INICIO, HORA_CIERRE,FLG_INHAB "+ 
	 " FROM TRAN_SPEI_CAT_HENV "+ 
	 " WHERE TRIM(CVE_MEDIO_ENT) = ? AND TRIM(CVE_TRANSFE) = ? AND TRIM(CVE_OPERACION) = ? ";
	

	
	/**
	 * Constante privada del tipo String que almacena el query para el listado de medios de entrega
	 */
	private static final String QUERY_LST_MED_ENT = "SELECT CVE_MEDIO_ENT CVE ,DESCRIPCION FROM  COMU_MEDIOS_ENT ORDER BY CVE_MEDIO_ENT ASC";
	/**
	 * Constante privada del tipo String que almacena el query para el listado de transfer
	 */
	private static final String QUERY_LST_TRANSFER = "SELECT CVE_TRANSFE CVE ,DESCRIPCION FROM  TRAN_TRANSFERENC ORDER BY CVE_TRANSFE ASC";
	/**
	 * Constante privada del tipo String que almacena el query para el listado de operacion
	 */
	private static final String QUERY_LST_OPERACION = "SELECT CVE_OPERACION CVE,DESCRIPCION FROM  TRAN_CVE_OPER ORDER BY CVE_OPERACION ASC";
	
	/**Constante con el CERO*/
	private static final String CERO = "00";
	
	/**Constante con el DEFAULT*/
	private static final String DEFAULT = "DEFAULT";
	
	
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIDAO Objeto del tipo BeanResHorariosSPEIDAO
	   */
	public BeanResHorariosSPEIDAO consultaHorariosSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean){
		  StringBuilder stringBuilder = null;
		  BeanResHorariosSPEIDAO beanResultado = new BeanResHorariosSPEIDAO();
		  BeanHorariosSPEI beanHora = null; 
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<BeanHorariosSPEI> listBean = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	    	  
	    	  if(bean.getCveMedioEntrega()== null){
	    		  bean.setCveMedioEntrega("null");
		     }
	    	  if(bean.getCveTransferencia()== null){
	    		  bean.setCveTransferencia("null");
		     }
	    	  if(bean.getCveOperacion()== null){
	    		  bean.setCveOperacion("null");
		     }
	         responseDTO = HelperDAO.consultar(QUERY_BUSQUEDA, Arrays.asList(
	        		 new Object[]{
	        				 bean.getCveMedioEntrega().trim(),bean.getCveTransferencia().trim(),bean.getCveOperacion().trim(),
	        				 bean.getCveMedioEntrega().trim(),bean.getCveTransferencia().trim(),bean.getCveOperacion().trim(),
	        				 bean.getPaginador().getRegIni(),bean.getPaginador().getRegFin()
	        		 }
	         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	        	 listBean = new ArrayList<BeanHorariosSPEI>();
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	beanHora = new BeanHorariosSPEI();	 
	            	beanHora.setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")));
	            	beanHora.setCveTransfe(utilerias.getString(map.get("CVE_TRANSFE")));
	            	beanHora.setCveOperacion(utilerias.getString(map.get("CVE_OPERACION")));
	            	beanHora.setCveTopoPri(utilerias.getString(map.get("TOPO_PRI")));
	            	if(CERO.equals(utilerias.getString(map.get("CVE_MEDIO_ENT")))){
	            		beanHora.setCveMedioEnt(DEFAULT);
	            	}
	            	if(CERO.equals(utilerias.getString(map.get("CVE_TRANSFE")))){
	            		beanHora.setCveTransfe(DEFAULT);
	            	}
	            	if(CERO.equals(utilerias.getString(map.get("CVE_OPERACION")))){
	            		beanHora.setCveOperacion(DEFAULT);
	            	}
	            	if(CERO.equals(utilerias.getString(map.get("TOPO_PRI")))){
	            		beanHora.setCveTopoPri(DEFAULT);
	            	}
					beanHora.setHoraIncio(utilerias.getString(map.get("HORA_INICIO")));
					beanHora.setHoraCierre(utilerias.getString(map.get("HORA_CIERRE")));
					
					stringBuilder = new StringBuilder(utilerias.getString(map.get("HORA_INICIO")));
					stringBuilder.insert(2, ':');
					beanHora.setHoraIncio(stringBuilder.toString());
					
					stringBuilder = new StringBuilder(utilerias.getString(map.get("HORA_CIERRE")));
					stringBuilder.insert(2, ':');
					beanHora.setHoraCierre(stringBuilder.toString());
					
					beanHora.setFlgInhabil(utilerias.getString(map.get("FLG_INHAB")));
	            	listBean.add(beanHora);    	
	            	beanResultado.setTotalReg(utilerias.getInteger(map.get("CONT")));
	            }
	         }
	         beanResultado.setListHorariosSPEI(listBean);
	         beanResultado.setCodError(responseDTO.getCodeError()); 
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResultado.setCodError(Errores.EC00011B);
	      }
	      return beanResultado;
		}
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO agregaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean){
		
		BeanResHorariosSPEIDAO beanResultado = new BeanResHorariosSPEIDAO();
		
		String horaInicio =bean.getHorarioSPEI().getHoraIncio().replace(":", "");
		String horaCierre =bean.getHorarioSPEI().getHoraCierre().replace(":", "");
	
		
		ResponseMessageDataBaseDTO responseDTO = null;
			    try {   	
			  	 responseDTO = HelperDAO.insertar(QUERY_AGREGAR,
			  			 Arrays.asList(new Object[]{bean.getHorarioSPEI().getCveMedioEnt() ,bean.getHorarioSPEI().getCveTransfe(),
			  					 bean.getHorarioSPEI().getCveOperacion(),bean.getHorarioSPEI().getCveTopoPri(),
			  					horaInicio,horaCierre,bean.getHorarioSPEI().getFlgInhabil()}), 
			     HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					  	beanResultado.setCodError(responseDTO.getCodeError());
					  	beanResultado.setMsgError(responseDTO.getMessageError());		
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResultado.setCodError(Errores.EC00011B);
				}    
		return beanResultado;
	}
	
	
	/**Metodo que sirve para modificar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO modificaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean){
	
		String horaInicio =bean.getHorarioSPEI().getHoraIncio().replace(":", "");
		String horaCierre =bean.getHorarioSPEI().getHoraCierre().replace(":", "");
		
		BeanResHorariosSPEIDAO beanResultado = new BeanResHorariosSPEIDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
			    try {
			  	 responseDTO = HelperDAO.actualizar(QUERY_MODIFICAR, 
			  			 Arrays.asList(new Object[]{
			  					bean.getHorarioSPEI().getCveTopoPri(),horaInicio,horaCierre,
			  					bean.getHorarioSPEI().getFlgInhabil(),
			  					bean.getHorarioSPEI().getCveMedioEnt() ,
			  					bean.getHorarioSPEI().getCveTransfe(),bean.getHorarioSPEI().getCveOperacion()}), 
			     HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					  	beanResultado.setCodError(responseDTO.getCodeError());
					  	beanResultado.setMsgError(responseDTO.getMessageError());		
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResultado.setCodError(Errores.EC00011B);
				}    
		return beanResultado;
		}
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO eliminaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean){
		String def = "DEFAULT"; 
		BeanResHorariosSPEIDAO beanResultado = new BeanResHorariosSPEIDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
			    try {
			    	for (BeanHorariosSPEI beanHorarioSPEI: bean.getListHorariosSPEI()) {
			    		String cveMedioEnt = def.equals(beanHorarioSPEI.getCveMedioEnt()) ? "00":beanHorarioSPEI.getCveMedioEnt();
			    		String cveTransfe= def.equals(beanHorarioSPEI.getCveTransfe()) ? "00":beanHorarioSPEI.getCveTransfe();
			    		String cveOpera= def.equals(beanHorarioSPEI.getCveOperacion()) ? "00":beanHorarioSPEI.getCveOperacion();
			    		
						  	 responseDTO = HelperDAO.actualizar(QUERY_ELIMINA, 
						  			 Arrays.asList(new Object[]{
						  					cveMedioEnt,
						  					cveTransfe,
						  					cveOpera
						  			}), 
						            HelperDAO.CANAL_GFI_DS, this.getClass().getName());
								  	beanResultado.setCodError(responseDTO.getCodeError());
								  	beanResultado.setMsgError(responseDTO.getMessageError());	
			    	}
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResultado.setCodError(Errores.EC00011B);
				}    
		return beanResultado;
		}
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanHorariosSPEI
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO consultaHorarioSPEI(BeanHorariosSPEI bean,ArchitechSessionBean architechSessionBean){
		StringBuilder stringBuilder = null;
		String def = "DEFAULT"; 
		  Utilerias utilerias = Utilerias.getUtilerias();
		  BeanResHorariosSPEIDAO beanResHorarioSPEI = new BeanResHorariosSPEIDAO();
		  BeanHorariosSPEI beanHorario = null;
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      
	      String cveMedioEnt = def.equals(bean.getCveMedioEnt()) ? "00":bean.getCveMedioEnt();
  		  String cveTransfe= def.equals(bean.getCveTransfe()) ? "00":bean.getCveTransfe();
  		  String cveOpera= def.equals(bean.getCveOperacion()) ? "00":bean.getCveOperacion();
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_KEY, 
	    			Arrays.asList(new Object[]{
	    					cveMedioEnt,cveTransfe,cveOpera}),
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanHorario = new BeanHorariosSPEI();
					beanHorario.setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")).trim());
					beanHorario.setCveTransfe(utilerias.getString(map.get("CVE_TRANSFE")).trim());
					beanHorario.setCveOperacion(utilerias.getString(map.get("CVE_OPERACION")).trim());
					beanHorario.setCveTopoPri(utilerias.getString(map.get("TOPO_PRI")).trim());
					stringBuilder = new StringBuilder(utilerias.getString(map.get("HORA_INICIO")));
					stringBuilder.insert(2, ':');
					beanHorario.setHoraIncio(stringBuilder.toString());
					
					stringBuilder = new StringBuilder(utilerias.getString(map.get("HORA_CIERRE")));
					stringBuilder.insert(2, ':');
					beanHorario.setHoraCierre(stringBuilder.toString());
					beanHorario.setFlgInhabil(utilerias.getString(map.get("FLG_INHAB")).trim());
					beanResHorarioSPEI.setHorarioSPEI(beanHorario);
	  		  	}
				beanResHorarioSPEI.setCodError(responseDTO.getCodeError());
				beanResHorarioSPEI.setMsgError(responseDTO.getMessageError());
			}else{
				beanResHorarioSPEI.setCodError(Errores.ED00011V); 
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResHorarioSPEI.setCodError(Errores.EC00011B);
		}
		
			return  beanResHorarioSPEI;
		}
	
	/**Metodo que sirve para consultar listas
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIBO consultaListas(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean){
		  String queryLst ="";
		  BeanResHorariosSPEIBO beanResultado = new BeanResHorariosSPEIBO();
		  BeanCatalogo beanCatalogo = null; 
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<BeanCatalogo> listBean = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	    	  
	    	  if("LST_MED".equals(bean.getConLista())){
	    		  queryLst = QUERY_LST_MED_ENT;
	          }else if("LST_TRA".equals(bean.getConLista())){
	        	  queryLst = QUERY_LST_TRANSFER;
	          }else{
	        	  queryLst = QUERY_LST_OPERACION;
	          }
	    	  

	    		  responseDTO = HelperDAO.consultar(queryLst, Collections.emptyList(),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	 	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	 	        	 listBean = new ArrayList<BeanCatalogo>();
	 	            list = responseDTO.getResultQuery();
	 	            for(HashMap<String,Object> map:list){
	 	            	beanCatalogo = new BeanCatalogo();
	 	            	beanCatalogo.setCve(utilerias.getString(map.get("CVE")).trim());
	 	            	beanCatalogo.setDescripcion(utilerias.getString(map.get("DESCRIPCION")).trim());
	 	            	listBean.add(beanCatalogo);    	
	 	            }
	 	         }
	 	         if("LST_MED".equals(bean.getConLista())){
	 	        	 beanResultado.setListMedioEntrega(listBean);
	 	          }else if("LST_TRA".equals(bean.getConLista())){
	 	        	  beanResultado.setListTrasnfer(listBean);
	 	          }else{
	 	        	  beanResultado.setListOperacion(listBean);
	 	          }
	 	         beanResultado.setCodError(responseDTO.getCodeError());
	 	         beanResultado.setMsgError(responseDTO.getMessageError());
	      } catch (ExceptionDataAccess e) { 
	         showException(e);
	         beanResultado.setCodError(Errores.EC00011B);
	      }
	      return beanResultado;
		}

}
