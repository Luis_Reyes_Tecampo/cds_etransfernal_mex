/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 10:03:36 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonCDAHistSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el monitor CDA
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorCDAHistSPIDImpl extends Architech implements DAOMonitorCDAHistSPID {
	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = -5407710756080645537L;	
	/**
	 * Constante para identificar la tabla de donde se obtienen
	 * las transacciones SPEI
	 */
	private static final String FROM_TRAN_SPEI_REC_HIS = "FROM TRAN_SPID_REC_HIS";
	/**
	 * Constante para identificar la tabla de donde se obtienen
	 * las transacciones SPEI CDA
	 */
	private static final String FROM_TRAN_SPEI_CDA_HIS= "FROM TRAN_SPID_CDA_HIS";

	/**
	 * Constante para almacenar string UNION_ALL
	 */
	//UNION_ALL
	private static final String UNION_ALL = "UNION ALL";
	/**
	 * Constante para almacenar string MONTO
	 */
	//Monto
	private static final String MONTO = "MONTO";
	/**
	 * Constante para almacenar string CANTIDAD
	 */
	//Cantidad
	private static final String CANTIDAD = "CANTIDAD";
	/**
	 * Constante privada de tipo String que almacena el query
	 * para obtener la CDA's agrupadas, con su correspondiente
	 * monto y volumen, la consulta incluye el estado de conexion
	 * para saber si el monitor CDA se encuentra en contingencia 
	 */
	private static final String QUERY_MONITOR_CDA_HIST = 				
		"SELECT 'RECIBIDAS' TIPO, COUNT(*)  CANTIDAD, NVL(SUM(MONTO),0) MONTO "+
		FROM_TRAN_SPEI_REC_HIS + " " +
		" WHERE (tipo_pago = 1 OR tipo_pago = 12) AND estatus_transfer = 'TR' " +
		" AND FCH_OPERACION = TO_DATE(?, 'dd-mm-yyyy') "+
		UNION_ALL + " " +
		"SELECT 'PENDIENTES' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO "+
		FROM_TRAN_SPEI_CDA_HIS + " " +
		" WHERE (estatus = '0' OR estatus = '3') "+
		" AND FCH_OPERACION  = TO_DATE(?,'dd-mm-yyyy') "+
		UNION_ALL + " " +
		"SELECT 'ENVIADAS' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA_HIS + " " +
		"WHERE estatus = '1' " +
		" AND FCH_OPERACION  = TO_DATE(?, 'dd-mm-yyyy') "+
		UNION_ALL + " " +
		"SELECT 'CONFIRMADAS' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA_HIS + " " +
		"WHERE estatus = '2' " +
		"  AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		UNION_ALL + " " +
		"SELECT 'CONTINGENCIA' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA_HIS + " " +
		"WHERE estatus = '4' " +
		" AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy')  "+
		UNION_ALL + " " +
		"SELECT 'RECHAZADAS' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA_HIS + " " +
		"WHERE estatus = '5'  "+
	    " AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy' ) ";
	

	/**
	 * Metodo que sirve para consultar los movimientos CDA agrupados por monto y volumen
	 *  @param beanReqMonCDAHist Bean con los parametros de la consulta
	 *  @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 *  @return BeanResMonitorCdaHistDAO Objeto del tipo @see BeanResMonitorCdaHistDAO
	 */
	//Metodo que sirve para consultar los movimientos CDA agrupados por monto y volumen
	@Override
	public BeanResMonitorCdaHistDAOSPID consultarCDAAgrupadasSPID(final BeanReqMonCDAHistSPID beanReqMonCDAHist,
			ArchitechSessionBean architechSessionBean){
		
		final BeanResMonitorCdaHistDAOSPID beanResMonCdaDAO = new BeanResMonitorCdaHistDAOSPID();
		List<HashMap<String,Object>> list = null;
	    ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
	    
		//Se hace la consulta a la base de datos
	    try {
	      	responseDTO = HelperDAO.consultar(QUERY_MONITOR_CDA_HIST, 
	      			Arrays.asList(new Object[]{
	      					beanReqMonCDAHist.getFechaOperacion(),beanReqMonCDAHist.getFechaOperacion(),
	      					beanReqMonCDAHist.getFechaOperacion(),beanReqMonCDAHist.getFechaOperacion(),
	      					beanReqMonCDAHist.getFechaOperacion(),beanReqMonCDAHist.getFechaOperacion()
	      			}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    			list = responseDTO.getResultQuery();
	
	    			beanResMonCdaDAO.setMontoCDARechazadas(
	    					utilerias.formateaDecimales(list.get(5).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDARechazadas
	    			beanResMonCdaDAO.setVolumenCDAOrdRecAplicadas(
	    					utilerias.formateaDecimales(list.get(0).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDAOrdRecAplicadas
	    			beanResMonCdaDAO.setMontoCDAPendEnviar(
	    					utilerias.formateaDecimales(list.get(1).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDAPendEnviar
	    			beanResMonCdaDAO.setVolumenCDAPendEnviar(
	    					utilerias.formateaDecimales(list.get(1).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDAPendEnviar
	    			beanResMonCdaDAO.setMontoCDAEnviadas(
	    					utilerias.formateaDecimales(list.get(2).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDAEnviadas
	    			beanResMonCdaDAO.setMontoCDAOrdRecAplicadas(
	    					utilerias.formateaDecimales(list.get(0).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDAOrdRecAplicadas
	    			beanResMonCdaDAO.setVolumenCDAEnviadas(
	    					utilerias.formateaDecimales(list.get(2).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDAEnviadas
	    			beanResMonCdaDAO.setMontoCDAConfirmadas(
	    					utilerias.formateaDecimales(list.get(3).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDAConfirmadas
	    			beanResMonCdaDAO.setVolumenCDAConfirmadas(
	    					utilerias.formateaDecimales(list.get(3).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDAConfirmadas
	    			beanResMonCdaDAO.setMontoCDAContingencia(
	    					utilerias.formateaDecimales(list.get(4).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));	//MontoCDAContingencia
	    			beanResMonCdaDAO.setVolumenCDAContingencia(
	    					utilerias.formateaDecimales(list.get(4).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDAContingencia
	    			beanResMonCdaDAO.setVolumenCDARechazadas(
	    					utilerias.formateaDecimales(list.get(5).get(CANTIDAD),Utilerias.FORMATO_NUMBER));		//VolumenCDARechazadas

	    			//Se setea cod y msg de error
	    			beanResMonCdaDAO.setCodError(responseDTO.getCodeError());
	    			beanResMonCdaDAO.setMsgError(responseDTO.getMessageError());
	    			
	    		}
	    	} catch (ExceptionDataAccess e) {
	    		//Se controla la excepcion
	    		showException(e);
	    		beanResMonCdaDAO.setCodError(Errores.EC00011B);
	    		beanResMonCdaDAO.setMsgError(Errores.DESC_EC00011B);
	    	}
	    return beanResMonCdaDAO;
	}


}
