/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOComunesSPEIImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.comun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOComunesSPEIImpl extends Architech implements DAOComunesSPEI{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -3822203519492552797L;
	
	 /**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONS_MI_INSTITUCION
	 */
	private static final String QUERY_CONS_MI_INSTITUCION = 
		" SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
        " WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL ";
	
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	private static final String QUERY_FECHA_OPERACION_SPID =
	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	
	/**
     * Metodo que se encarga de realizar la consulta de los movimientos CDA
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMiInstDAO Objeto del tipo @see BeanResConsMiInstDAO
     */
    public BeanResConsMiInstDAO consultaMiInstitucion(ArchitechSessionBean sessionBean) {
    	BeanResConsMiInstDAO beanResConsMiInstDAO =  new BeanResConsMiInstDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<Object> parametros = new ArrayList<Object>();
    	String query = null;
    	try {              	
    		query = QUERY_CONS_MI_INSTITUCION;
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 Map<String,Object> mapResult = list.get(0);
        		 	 beanResConsMiInstDAO.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
        		 }
        	}
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		beanResConsMiInstDAO.setCodError(Errores.EC00011B);
  	}
        return beanResConsMiInstDAO;	
    }
	
	
   /**Metodo DAO para obtener la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   *    */
   public BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean){
      final BeanResConsFechaOpDAO beanResConsFechaOpDAO = new BeanResConsFechaOpDAO();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      Utilerias utilerias = Utilerias.getUtilerias();
      try {
    	responseDTO = HelperDAO.consultar(QUERY_FECHA_OPERACION_SPID, Collections.emptyList(), 
    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			list = responseDTO.getResultQuery();
			for(HashMap<String,Object> map:list){
				beanResConsFechaOpDAO.setFechaOperacion(
						utilerias.formateaFecha(map.get("FCH_OPERACION"), 
								Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
				beanResConsFechaOpDAO.setCodError(responseDTO.getCodeError());
				beanResConsFechaOpDAO.setMsgError(responseDTO.getMessageError());
  		  }
		}
	} catch (ExceptionDataAccess e) {
        showException(e);
        beanResConsFechaOpDAO.setCodError(Errores.EC00011B);
	}
      return beanResConsFechaOpDAO;
   }
	   
	  

}
