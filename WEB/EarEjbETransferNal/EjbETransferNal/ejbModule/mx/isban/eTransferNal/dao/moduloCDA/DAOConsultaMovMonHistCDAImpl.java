/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovMonCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovMonHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDatosGenerales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaOrdenante;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovHistCdaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Session Bean implementation class DAOConsultaMovHistCDAImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaMovMonHistCDAImpl extends Architech implements DAOConsultaMovMonHistCDA {

    /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -1581076479590719820L;

	/**Constante TIPO_PAGO*/
	private static final String TIPO_PAGO = "TIPO_PAGO";
	/**Constante TIPO_PAGO*/
	private static final String FCH_HORA_ABONO ="FCH_HORA_ABONO";
	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * la cuenta de regisros
	 */
	private static final String QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_CONSULTA =
		" SELECT COUNT(1) CONT "+
        " FROM TRAN_SPEI_CDA_HIS T1  "+
        " WHERE "+
        " T1.FCH_OPERACION BETWEEN " +
        " TO_DATE(?, 'dd-MM-yyyy') AND TO_DATE(?, 'dd-MM-yyyy') ";

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el listado de movimientos CDA
	 */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA=

		" SELECT TMP.REFERENCIA, TMP.FCH_OPERACION, "+
		 " TMP.FOLIO_PAQUETE, TMP.FOLIO_PAGO, "+
		 " TMP.CVE_MI_INSTITUC, TMP.CVE_INST_ORD, "+
		 " TMP.NOMBRE_BCO_ORD, TMP.CVE_RASTREO, "+
		 " TMP.NUM_CUENTA_REC, TMP.MONTO, "+
		 " TMP.FCH_HORA_ABONO, TMP.FCH_HORA_ENVIO,TMP.HORA_ENVIO, "+
		 " CASE(TMP.ESTATUS) "+
		 "   WHEN '0' THEN 'PENDIENTE' "+
		 "   WHEN '1' THEN 'ENVIADO' "+
		 "   WHEN '2' THEN 'CONFIRMADO' "+
		 "   WHEN '3' THEN 'PENDIENTE' "+
		 "   WHEN '4' THEN 'CONTINGENCIA' "+
		 "   WHEN '5' then 'RECHAZADO' "+
		 " END CDA, TMP.COD_ERROR, TO_CHAR(TMP.FCH_CDA,'yyyymmddhh24miss') FCH_CDA, TMP.TIPO_PAGO"+
		 " from "+
		 "   (select "+
		 "     ROW_NUMBER() over (order by T1.FCH_OPERACION, T1.HORA_ENVIO, T1.FCH_CDA) RN, "+
		 "     T1.REFERENCIA,T1.FCH_OPERACION, "+
		 "     T1.FOLIO_PAQUETE,T1.FOLIO_PAGO, "+
		 "     T1.CVE_MI_INSTITUC,T1.CVE_INST_ORD, "+
		 "     NVL(T1.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD ,T1.CVE_RASTREO, "+
		 "     T1.NUM_CUENTA_REC,T1.MONTO, "+
		 "     T1.FCH_HORA_ABONO,T1.FCH_HORA_ENVIO,T1.HORA_ENVIO, "+
		 "     T1.ESTATUS, T1.COD_ERROR, T1.FCH_CDA, T1.TIPO_PAGO "+
		 "   FROM TRAN_SPEI_CDA_HIS T1 "+
	     "   WHERE "+
	     "   T1.FCH_OPERACION between TO_DATE(?,'dd-MM-yyyy') and TO_DATE(?,'dd-MM-yyyy') ";


/*		" SELECT TMP.REFE_TRANSFER,TMP.FCH_OPERACION, TMP.FOLIO_PAQUETE,TMP.FOLIO_PAGO, "+
	    " TMP.CVE_MI_INSTITUC,TMP.CVE_INST_ORD, TMP.NOMBRE_BCO_ORD,TMP.CVE_RASTREO, "+
	    " TMP.NUM_CUENTA_REC,TMP.MONTO, TMP.FCH_CAPTURA,TMP.FCH_HORA_ENVIO, "+
	    "   CASE (TMP.CDA) WHEN '0' THEN 'PENDIENTE' "+
	    "     WHEN '1' THEN 'ENVIADO' "+
	    "     WHEN '2' THEN 'CONFIRMADO' "+
	    "     WHEN '3' THEN 'PENDIENTE' "+
	    "     WHEN '4' THEN 'CONTINGENCIA' "+
	    "     WHEN '5' THEN 'RECHAZADO' "+
	    "   END CDA, TMP.CODIGO_ERROR "+
	    " FROM (SELECT TMP.*, ROWNUM AS RN "+
	           " FROM (" +
	           "*+ FIRST_ROWS(20) /"+
	           " SELECT T1.REFE_TRANSFER,T1.FCH_OPERACION, T1.FOLIO_PAQUETE,T1.FOLIO_PAGO, "+
	                      " T1.CVE_MI_INSTITUC,T1.CVE_INST_ORD, T2.NOMBRE_BCO_ORD,T1.CVE_RASTREO, "+
	                      " T1.NUM_CUENTA_REC,T1.MONTO, T1.FCH_CAPTURA,T2.FCH_HORA_ENVIO, "+
	                      " T1.CDA,T1.CODIGO_ERROR "+
	                  " FROM TRAN_SPEI_REC_HIS T1, "+
	                      " TRAN_SPEI_CDA_HIS T2 "+
	                " WHERE " +
	                  " T1.FCH_OPERACION BETWEEN "+
	                  " TO_DATE(?, 'dd-MM-yyyy') AND TO_DATE(?, 'dd-MM-yyyy') "+
	                  " AND T1.TIPO_PAGO = 1 "+
	                  " AND T1.FCH_OPERACION = T2.FCH_OPERACION "+
	                  " AND T1.CVE_MI_INSTITUC = T2.CVE_MI_INSTITUC "+
	                  " AND T1.CVE_INST_ORD = T2.CVE_INST_ORD "+
	                  " AND T1.FOLIO_PAQUETE = T2.FOLIO_PAQUETE "+
	                  " AND T1.FOLIO_PAGO = T2.FOLIO_PAGO ";
*/

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el listado de movimientos CDA
	 */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_2 =
	" ) TMP "+
	" where RN between ? and ? "+
	" order by TMP.FCH_OPERACION, TMP.HORA_ENVIO,TMP.FCH_CDA ";

		/*
		" ORDER BY T1.FCH_OPERACION, T2.FCH_HORA_ENVIO) TMP) TMP "+
		" WHERE RN BETWEEN ? AND ? "+
		" ORDER BY TMP.FCH_OPERACION, TMP.FCH_HORA_ENVIO ";
		*/



	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String QUERY_INSERT_CONSULTA_EXPORTAR_TODO =
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";

	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String COLUMNAS_CONSULTA_MOV_CDA =
		"Refere,Fecha Ope,Folio paq,Folio pago,Clave SPEI ordenante del abono," +
		"Inst Emisora,Cve rastreo,Cta Beneficiario,Monto,Fecha Abono,Hr Abono, Hr Envio," + 
		"Estatus CDA,Cod. Error,Tipo Pago";

	/**
	 * Constante para la consulta de exportar todos
	 */
	private static final String QUERY_CONSULTA_EXPORTAR_TODO =

		"SELECT T1.REFERENCIA || ',' || to_char(T1.FCH_OPERACION,'DD/MM/YYYY') || ',' || T1.FOLIO_PAQUETE || ',' || T1.FOLIO_PAGO || ',' ||"+
		" T1.CVE_INST_ORD || ',' || NVL(T1.NOMBRE_BCO_ORD,'') || ',' || T1.CVE_RASTREO || ',' || T1.NUM_CUENTA_REC || ',' || T1.MONTO || ',' || "+
		" to_char(T1.FCH_HORA_ABONO, 'DD/MM/YYYY') || ',' || to_char(T1.FCH_HORA_ABONO, 'HH24:MI:SS') || ',' || to_char(T1.FCH_HORA_ENVIO,'HH24:MI:SS') || ',' || " +
		" CASE(T1.ESTATUS) WHEN '0' THEN 'PENDIENTE' " +
		"WHEN '1' THEN 'ENVIADO' " +
		"WHEN '2' THEN 'CONFIRMADO' " +
		"WHEN '3' THEN 'PENDIENTE' " +
		"WHEN '4' THEN 'CONTINGENCIA' " +
		"WHEN '5' THEN 'RECHAZADO' ELSE '' END" +
		"|| ',' || T1.COD_ERROR || ',' || T1.TIPO_PAGO "+
		"FROM TRAN_SPEI_CDA_HIS T1 "+
		"WHERE ";

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el detalle de un movimiento CDA
	 */
	private static final String QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA =
	    " SELECT  CDA_HIS.REFERENCIA, CDA_HIS.FCH_OPERACION, CDA_HIS.CVE_INST_ORD, "+
        " CDA_HIS.CVE_RASTREO, CDA_HIS.NUM_CUENTA_REC, CDA_HIS.MONTO, "+
        " CDA_HIS.FCH_HORA_ABONO, NVL(CDA_HIS.FCH_HORA_ENVIO,'') FCH_HORA_ENVIO, NVL(CDA_HIS.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD, "+
        " CASE(CDA_HIS.ESTATUS) WHEN '0' THEN 'PENDIENTE' "+
        "   WHEN '1' THEN 'ENVIADO'  "+
        "   WHEN '2' THEN 'CONFIRMADO' "+
        "   WHEN '3' THEN 'PENDIENTE'  "+
        "   WHEN '4' THEN 'CONTINGENCIA' "+
        "   WHEN '5' THEN 'RECHAZADO' END CDA, "+
        " CDA_HIS.NOMBRE_ORD, CDA_HIS.NUM_CUENTA_ORD, "+
        " CDA_HIS.TIPO_CUENTA_ORD,CDA_HIS.RFC_ORD, NVL(CDA_HIS.NOMBRE_BCO_REC,'') NOMBRE_BCO_REC, "+
        " CDA_HIS.TIPO_CUENTA_REC, CDA_HIS.NOMBRE_REC NOMBRE_REC, CDA_HIS.CONCEPTO_PAGO, "+
        " CDA_HIS.IVA, CDA_HIS.RFC_REC RFC_REC, CDA_HIS.TIPO_PAGO "+
        " FROM TRAN_SPEI_CDA_HIS CDA_HIS "+
        " WHERE "+
        " CDA_HIS.FCH_OPERACION =  TO_DATE(?,'dd/mm/yyyy') "+
        " AND CDA_HIS.CVE_MI_INSTITUC  = ? "+
        " AND CDA_HIS.CVE_INST_ORD = ? "+
        " AND CDA_HIS.FOLIO_PAQUETE = ? "+
        " AND CDA_HIS.FOLIO_PAGO = ? "+
        " AND CDA_HIS.REFERENCIA = ? "+
	    " AND CDA_HIS.FCH_CDA = TO_DATE(?,'YYYYMMDDHH24MISS') ";



	/**
     * Metodo que se encarga de realizar la consulta de movimientos historicos CDA
     * @param beanReqConsMovHistCDA  Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovHistCdaDAO consultarMovHistCDA(BeanReqConsMovMonHistCDA beanReqConsMovHistCDA, ArchitechSessionBean sessionBean) {
    	BeanResConsMovHistCdaDAO beanResConHistCdaDAO =  new BeanResConsMovHistCdaDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanMovimientoCDA> listBeanMovimientoCDA =  null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	String queryParam = null;
    	String query = null;
        try {
        	List<Object> parametros = new ArrayList<Object>();
        	parametros.add(beanReqConsMovHistCDA.getFechaOpeInicio());
        	parametros.add(beanReqConsMovHistCDA.getFechaOpeFin());
        	queryParam = armaConsultaParametrizada(parametros,beanReqConsMovHistCDA);
        	query = QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_CONSULTA+queryParam;

        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		for(HashMap<String,Object> map:list){
        			beanResConHistCdaDAO.setTotalReg(utilerias.getInteger(map.get("CONT")));
        		}
        	}

        	if(beanResConHistCdaDAO.getTotalReg()>0){


	        	parametros.add(beanReqConsMovHistCDA.getPaginador().getRegIni());
	        	parametros.add(beanReqConsMovHistCDA.getPaginador().getRegFin());

	        	query = QUERY_CONSULTA_MOVIMIENTOS_CDA+queryParam+QUERY_CONSULTA_MOVIMIENTOS_CDA_2;
	      	    responseDTO = HelperDAO.consultar(query,parametros,
	      			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );


	      	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	      		 list = responseDTO.getResultQuery();
	      		listBeanMovimientoCDA = new ArrayList<BeanMovimientoCDA>();
	      		  for(HashMap<String,Object> map:list){
	      			  listBeanMovimientoCDA.add(descomponeConsulta(map));
	      		  }
	      		beanResConHistCdaDAO.setListBeanMovimientoCDA(listBeanMovimientoCDA);
	      	  }

	      	  beanResConHistCdaDAO.setCodError(responseDTO.getCodeError());
	      	  beanResConHistCdaDAO.setMsgError(responseDTO.getMessageError());
        	}else{
        		beanResConHistCdaDAO.setCodError(Errores.CODE_SUCCESFULLY);
        		beanResConHistCdaDAO.setMsgError(Errores.OK00000V);
    	      	listBeanMovimientoCDA =  Collections.emptyList();
    	      	beanResConHistCdaDAO.setListBeanMovimientoCDA(listBeanMovimientoCDA);
        	}
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		beanResConHistCdaDAO.setCodError(Errores.EC00011B);
  	}
  		return beanResConHistCdaDAO;
    }

    /**
     * Metodo que genera un objeto del tipo BeanMovimientoCDA a partir de la respuesta de la consulta
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanMovimientoCDA Objeto del tipo BeanMovimientoCDA
     */
    private BeanMovimientoCDA descomponeConsulta(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanMovimientoCDA beanMovimientoCDA = null;
    	beanMovimientoCDA = new BeanMovimientoCDA();
		beanMovimientoCDA.setReferencia(utilerias.getString(map.get("REFERENCIA")));
		beanMovimientoCDA.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanMovimientoCDA.setFolioPaq(utilerias.getString(map.get("FOLIO_PAQUETE")));
		beanMovimientoCDA.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
		beanMovimientoCDA.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));
		beanMovimientoCDA.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
		beanMovimientoCDA.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
		beanMovimientoCDA.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
		beanMovimientoCDA.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));
		beanMovimientoCDA.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
		beanMovimientoCDA.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanMovimientoCDA.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDA.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDA.setEstatusCDA(utilerias.getString(map.get("CDA")));
		beanMovimientoCDA.setCodError(utilerias.getString(map.get("COD_ERROR")));
		beanMovimientoCDA.setFchCDA(utilerias.getString(map.get("FCH_CDA")));
		beanMovimientoCDA.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));

		return beanMovimientoCDA;
    }


    /**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovHistCdaDAO guardarConsultaMovHistExpTodo(BeanReqConsMovMonHistCDA beanReqConsMovHistCDA, ArchitechSessionBean sessionBean) {

    	BeanResConsMovHistCdaDAO beanResConsMovHistCdaDAO  = new BeanResConsMovHistCdaDAO();
         ResponseMessageDataBaseDTO responseDTO = null;
         Utilerias utilerias = Utilerias.getUtilerias();
         Date fecha = new Date();
         StringBuilder builder = new StringBuilder();
         String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
         String nombreArchivo = "CDA_CON_MOV_CDAS_HIS_".concat(fechaActual);
         nombreArchivo = nombreArchivo.concat(".csv");

         builder.append(QUERY_CONSULTA_EXPORTAR_TODO);
     	 builder.append(generarConsultaExportarTodo(beanReqConsMovHistCDA));
     	 builder.append(" ORDER BY T1.FCH_OPERACION,T1.FCH_HORA_ENVIO");

         try{
         	responseDTO = HelperDAO.insertar(QUERY_INSERT_CONSULTA_EXPORTAR_TODO,
       			   Arrays.asList(new Object[]{
       					   sessionBean.getUsuario(),
       					   sessionBean.getIdSesion(),
       					   "CDA",
       					   "CONSULTA_MOV_HISTORICOS_CDAS",
       					   nombreArchivo,
       					   beanReqConsMovHistCDA.getTotalReg(),
       					   "PE",
       					   builder.toString(),
       					   COLUMNAS_CONSULTA_MOV_CDA}),
       			   HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
         	beanResConsMovHistCdaDAO.setNombreArchivo(nombreArchivo);
         	beanResConsMovHistCdaDAO.setCodError(responseDTO.getCodeError());
         	beanResConsMovHistCdaDAO.setMsgError(responseDTO.getMessageError());
         }catch (ExceptionDataAccess e) {
         	showException(e);
         	beanResConsMovHistCdaDAO.setCodError(Errores.EC00011B);
		}
		return beanResConsMovHistCdaDAO;
    }

    /**
     * Metodo encargado de consultar el detalle de un movimiento CDA
     * @param beanReqConsMovCDA Objeto del tipo @see Object BeanReqConsMovMonCDADet
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovDetHistCdaDAO Objeto del tipo @see BeanResConsMovDetHistCdaDAO
     */
    public BeanResConsMovDetHistCdaDAO consultarMovDetHistCDA(BeanReqConsMovMonCDADet beanReqConsMovCDA,
    		ArchitechSessionBean architechSessionBean) {

    	BeanResConsMovDetHistCdaDAO beanResConsMovDetHistCdaDAO = new BeanResConsMovDetHistCdaDAO();
    	BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales = new BeanResConsMovCdaDatosGenerales();
    	BeanResConsMovCdaOrdenante  beanMovCdaOrdenante = new BeanResConsMovCdaOrdenante();
    	BeanResConsMovCdaBeneficiario beanMovCdaBeneficiario =  new BeanResConsMovCdaBeneficiario();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
        ResponseMessageDataBaseDTO responseDTO = null;
        int numReferencia = Integer.parseInt(beanReqConsMovCDA.getReferencia());
    	int cveMiInstitucLink = Integer.parseInt(beanReqConsMovCDA.getCveMiInstitucLink());
    	int cveSpeiLink = Integer.parseInt(beanReqConsMovCDA.getCveSpeiLink());
    	int folioPaqLink = Integer.parseInt(beanReqConsMovCDA.getFolioPaqLink());
    	int folioPagoLink = Integer.parseInt(beanReqConsMovCDA.getFolioPagoLink());

    	String fechaCDA = beanReqConsMovCDA.getFchCDALink();

        try {
       	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA,
       			Arrays.asList(new Object[]{beanReqConsMovCDA.getFechaOpeLink(),cveMiInstitucLink,cveSpeiLink,
       					folioPaqLink,folioPagoLink,numReferencia,fechaCDA}),
				 HelperDAO.CANAL_GFI_DS, this.getClass().getName());

   		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
   			list = responseDTO.getResultQuery();
   			for(HashMap<String,Object> map:list){

   				beanMovCdaDatosGenerales.setReferencia(utilerias.getString(map.get("REFERENCIA")));
   				beanMovCdaDatosGenerales.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
   				beanMovCdaDatosGenerales.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
   				beanMovCdaDatosGenerales.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));
   				beanMovCdaDatosGenerales.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));
   				beanMovCdaDatosGenerales.setEstatusCDA(utilerias.getString(map.get("CDA")));
   				beanMovCdaDatosGenerales.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));
   				beanMovCdaDatosGenerales.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
   				
   				beanMovCdaOrdenante.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));
   				beanMovCdaOrdenante.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
   				beanMovCdaOrdenante.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
   				beanMovCdaOrdenante.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
   				beanMovCdaOrdenante.setCtaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
   				beanMovCdaOrdenante.setRfcCurpOrdenante(utilerias.getString(map.get("RFC_ORD")));

   				beanMovCdaBeneficiario.setNomInstRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));
   				beanMovCdaBeneficiario.setNombreBened(utilerias.getString(map.get("NOMBRE_REC")));
   				beanMovCdaBeneficiario.setTipoCuentaBened(utilerias.getString(map.get("TIPO_CUENTA_REC")));
   				beanMovCdaBeneficiario.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));
   				beanMovCdaBeneficiario.setRfcCurpBeneficiario(utilerias.getString(map.get("RFC_REC")));
   				beanMovCdaBeneficiario.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
   				beanMovCdaBeneficiario.setImporteIVA(utilerias.formateaDecimales(map.get("IVA"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
   				beanMovCdaBeneficiario.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
   				

   				beanResConsMovDetHistCdaDAO.setBeanMovCdaDatosGenerales(beanMovCdaDatosGenerales);
   				beanResConsMovDetHistCdaDAO.setBeanConsMovCdaOrdenante(beanMovCdaOrdenante);
   				beanResConsMovDetHistCdaDAO.setBeanConsMovCdaBeneficiario(beanMovCdaBeneficiario);

   				beanResConsMovDetHistCdaDAO.setCodError(responseDTO.getCodeError());
   				beanResConsMovDetHistCdaDAO.setMsgError(responseDTO.getMessageError());
     		  }
   		}
       	} catch (ExceptionDataAccess e) {
       		showException(e);
       		beanResConsMovDetHistCdaDAO.setCodError(Errores.EC00011B);
       	}

       return beanResConsMovDetHistCdaDAO;
    }

    /**
     * Metodo para generar la consulta de exportar todo
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @return String Objeto del tipo @see String
     */
    public String generarConsultaExportarTodo(BeanReqConsMovMonHistCDA beanReqConsMovHistCDA){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	String estatusCDA = "C";

    	if(beanReqConsMovHistCDA.getFechaOpeInicio()!= null && beanReqConsMovHistCDA.getFechaOpeFin()!= null){
	    	builder.append(" t1.FCH_OPERACION BETWEEN TO_DATE('");
	    	builder.append(beanReqConsMovHistCDA.getFechaOpeInicio());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("') AND TO_DATE('");
	    	builder.append(beanReqConsMovHistCDA.getFechaOpeFin());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("')");
    	}

    	if(beanReqConsMovHistCDA.getOpeContingencia()){
        	builder.append("AND T1.MODALIDAD = '");
        	builder.append(estatusCDA);
        	builder.append("'");
    	}
    	if(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ") ;
    		builder.append(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
    	}

    	if(beanReqConsMovHistCDA.getHrAbonoIni() != null && beanReqConsMovHistCDA.getHrAbonoFin() != null &&
    			(!vacio.equals(beanReqConsMovHistCDA.getHrAbonoIni())) && (!vacio.equals(beanReqConsMovHistCDA.getHrAbonoFin())) ){
			  builder.append("AND T1.HORA_ABONO  between ");
		      builder.append(beanReqConsMovHistCDA.getHrAbonoIni().replaceAll(":", ""));
		      builder.append(" and ");
		      builder.append(beanReqConsMovHistCDA.getHrAbonoFin().replaceAll(":", ""));
    	}

	  	builder.append(generarConsultaExportarTodo2(beanReqConsMovHistCDA));
    	return builder.toString();
    }

    /**
     * Metodo para sirve para generar el query y parametrizar la consulta
     * @param parametros Objeto del tipo @see List<Object>
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @return String Objeto del tipo @see String
     */
    public String armaConsultaParametrizada(List<Object> parametros, BeanReqConsMovMonHistCDA beanReqConsMovHistCDA){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	String estatusCDA = "C";


    	if(beanReqConsMovHistCDA.getHrAbonoIni() != null && beanReqConsMovHistCDA.getHrAbonoFin() != null &&
    			(!vacio.equals(beanReqConsMovHistCDA.getHrAbonoIni())) && (!vacio.equals(beanReqConsMovHistCDA.getHrAbonoFin())) ){
			  builder.append(" AND T1.HORA_ABONO ");
			  builder.append("between ? and ? ");
			  parametros.add(beanReqConsMovHistCDA.getHrAbonoIni().replaceAll(":", ""));
			  parametros.add(beanReqConsMovHistCDA.getHrAbonoFin().replaceAll(":", ""));
    	}

    	if(beanReqConsMovHistCDA.getHrEnvioIni() != null && beanReqConsMovHistCDA.getHrEnvioFin() != null &&
	  			(!vacio.equals(beanReqConsMovHistCDA.getHrEnvioIni())) && (!vacio.equals(beanReqConsMovHistCDA.getHrEnvioFin()))){
	  		builder.append(" AND T1.HORA_ENVIO " +
	  				" between ? and ? ");
	  		parametros.add(beanReqConsMovHistCDA.getHrEnvioIni().replaceAll(":", ""));
	  		parametros.add(beanReqConsMovHistCDA.getHrEnvioFin().replaceAll(":", ""));
	  	}
	  	if(beanReqConsMovHistCDA.getMontoPago() != null  && (!vacio.equals(beanReqConsMovHistCDA.getMontoPago()))){
	  		 builder.append(" AND T1.MONTO = ? ");
	  		 String montoPago = beanReqConsMovHistCDA.getMontoPago().replaceAll(",", "");
	  		 parametros.add(montoPago);
	  	}
    	if(beanReqConsMovHistCDA.getOpeContingencia()){
        	builder.append("AND T1.MODALIDAD = ? ");
        	parametros.add(estatusCDA);
    	}
    	if(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ? ") ;
    		parametros.add(beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
    	}


	  	if(beanReqConsMovHistCDA.getCtaBeneficiario() != null && (!vacio.equals(beanReqConsMovHistCDA.getCtaBeneficiario()))){
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM(?)");
	  		 parametros.add("%"+beanReqConsMovHistCDA.getCtaBeneficiario());
	  	}
	  	if(beanReqConsMovHistCDA.getNombreInstEmisora() != null &&(!vacio.equals(beanReqConsMovHistCDA.getNombreInstEmisora()))){
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER(?)");
	  		parametros.add("%"+beanReqConsMovHistCDA.getNombreInstEmisora());
	  	}
	  	if(beanReqConsMovHistCDA.getCveRastreo() != null && (!vacio.equals(beanReqConsMovHistCDA.getCveRastreo()))){
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM(?)");
	  		parametros.add("%"+beanReqConsMovHistCDA.getCveRastreo());
	  	}
	  	if(beanReqConsMovHistCDA.getTipoPago() != null && (!vacio.equals(beanReqConsMovHistCDA.getTipoPago()))){
	  		builder.append(" AND T1.TIPO_PAGO = ?");
	  		parametros.add(beanReqConsMovHistCDA.getTipoPago());
	  	}
	  	
	  	if(beanReqConsMovHistCDA.getEstatusCDA() != null && (!vacio.equals(beanReqConsMovHistCDA.getEstatusCDA()))){
	  		builder.append(" AND T1.ESTATUS ");
	  		String val[] = beanReqConsMovHistCDA.getEstatusCDA().split(",");
	  		if(val.length==1){
	  			builder.append(" = ?");
	  			parametros.add(beanReqConsMovHistCDA.getEstatusCDA());
	  		}else if(val.length>1){
	  			builder.append(" in ( ?, ?) ");
	  			parametros.add(val[0]);
	  			parametros.add(val[1]);
	  		} 
	  	}
    	return builder.toString();
    }

    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo
     * @param beanReqConsMovHistCDA objeto del tipo @see BeanReqConsMovMonHistCDA
     * @return String objeto del tipo @see String
     */
    private String generarConsultaExportarTodo2(BeanReqConsMovMonHistCDA beanReqConsMovHistCDA){

    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

    	if(beanReqConsMovHistCDA.getHrEnvioIni() != null && beanReqConsMovHistCDA.getHrEnvioFin() != null &&
	  			(!vacio.equals(beanReqConsMovHistCDA.getHrEnvioIni())) && (!vacio.equals(beanReqConsMovHistCDA.getHrEnvioFin()))){
	  		builder.append(" AND T1.HORA_ENVIO between ");
	          builder.append(beanReqConsMovHistCDA.getHrEnvioIni().replaceAll(":", ""));
	          builder.append(" and ");
	          builder.append(beanReqConsMovHistCDA.getHrEnvioFin().replaceAll(":", ""));

	  	}
	  	if(beanReqConsMovHistCDA.getMontoPago() != null  && (!vacio.equals(beanReqConsMovHistCDA.getMontoPago()))){
	  		 builder.append(" AND T1.MONTO = ");
	  		 String montoPago = beanReqConsMovHistCDA.getMontoPago().replaceAll(",", "");
	  	     builder.append(montoPago);
	  	}
	  	if(beanReqConsMovHistCDA.getCtaBeneficiario() != null && (!vacio.equals(beanReqConsMovHistCDA.getCtaBeneficiario()))){
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM('%");
	  	     builder.append(beanReqConsMovHistCDA.getCtaBeneficiario());
	  	     builder.append("')");
	  	}
	  	if(beanReqConsMovHistCDA.getNombreInstEmisora() != null &&(!vacio.equals(beanReqConsMovHistCDA.getNombreInstEmisora()))){
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER('%");
	          builder.append(beanReqConsMovHistCDA.getNombreInstEmisora());
	          builder.append("')");
	  	}
	  	if(beanReqConsMovHistCDA.getCveRastreo() != null && (!vacio.equals(beanReqConsMovHistCDA.getCveRastreo()))){
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM('%");
	      	builder.append(beanReqConsMovHistCDA.getCveRastreo());
	      	builder.append("')");
	  	}
	  	if(beanReqConsMovHistCDA.getEstatusCDA() != null && (!vacio.equals(beanReqConsMovHistCDA.getEstatusCDA()))){
	  		
	  		String val[] = beanReqConsMovHistCDA.getEstatusCDA().split(",");
	  		if(val.length==1){
	  			builder.append(" AND T1.ESTATUS = ");	
	  			builder.append(beanReqConsMovHistCDA.getEstatusCDA());
	  		}else if(val.length>1){
	  			builder.append(" AND T1.ESTATUS IN ( ");	
	  			builder.append(val[0]);
	  			builder.append(",");
	  			builder.append(val[1]);
	  			builder.append(" ) ");
	  		} 
	  	}
    	return builder.toString();
    }


}