/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentralImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   21/03/2017 04:35:28 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.capturasManuales.UtileriasCapturasManuales;

/**
 * Class DAOCapturaCentralImpl.
 *
 * @author FSW-Vector
 * @since 21/03/2017
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCapturaCentralImpl extends DAOBaseCapturasManualesImpl implements DAOCapturaCentral {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -59340974412400776L;	

	/** La constante QUERY_GET_CAMPOS. */
	private static final String QUERY_GET_CAMPOS = 
			" select TCL.CAMPO, TCL.DESCRIPCION, " +
			"case when TCDT.OBLIGATORIO is null then TCL.OBLIGATORIO else TCDT.OBLIGATORIO end as OBLIGATORIO, " +
			"case when TCDT.CONSTANTE is null then TCL.CONSTANTE else TCDT.CONSTANTE end as CONSTANTE, " +
			"TCL.TIPO,TCL.query   " +
			"from TRAN_CAP_LAYOUT TCL " +
			"inner join TRAN_CAP_TEMPLATE TCT " +
			"on TCL.ID_LAYOUT = TCT.ID_LAYOUT " +
			"FULL OUTER join TRAN_CAP_DET_TEMPLATE TCDT " +
			"on TCL.CAMPO= TCDT.CAMPO " +
			"AND TCDT.ID_TEMPLATE = TCT.ID_TEMPLATE " +
			"where trim(TCT.ID_TEMPLATE) = ? order by OBLIGATORIO DESC, TCL.ID_CAMPO" ;
	
	/** La constante QUERY_GENERA_FOLIO. */
	private static final String QUERY_GENERA_FOLIO = " select SEQ_CAP_OPER.NEXTVAL FOLIO from DUAL ";
	
	/** La constante QUERY_OBTIENE_TODOS_TEMPLATES. */
	private static final String QUERY_OBTIENE_TODOS_TEMPLATES = "select ID_TEMPLATE, ID_LAYOUT from TRAN_CAP_TEMPLATE " +
			"where ESTATUS = 'AC' ORDER BY ID_TEMPLATE ASC";
	
	/** La constante QUERY_OBTIENE_TEMP. */
	private static final String QUERY_OBTIENE_TEMP = " select ID_LAYOUT, ESTATUS, USR_ALTA from TRAN_CAP_TEMPLATE " +
			"where trim(ID_TEMPLATE) = ? ";
	
	/** La constante QUERY_INSERT. */
	private static final String QUERY_INSERT = "INSERT INTO TRAN_CAP_OPER ("
			+ " ID_FOLIO, FCH_CAPTURA, ESTATUS, ID_TEMPLATE,"
			+ " ID_LAYOUT, USR_MODIFICA, COD_POSTAL_ORD,"
			+ " COMENTARIO1, COMENTARIO2, COMENTARIO3, COMENTARIO4, CONCEPTO_PAGO, CONCEPTO_PAGO2, CVE_DIVISA_ORD,"
			+ " CVE_DIVISA_REC, CVE_EMPRESA, CVE_INTERME_ORD, CVE_INTERME_REC, CVE_MEDIO_ENT, CVE_OPERACION, CVE_PTO_VTA,"
			+ " CVE_PTO_VTA_ORD, CVE_PTO_VTA_REC, CVE_RASTREO, CVE_TRANSFE, CVE_USUARIO_CAP, CVE_USUARIO_SOL, DIRECCION_IP,"
			+ " DOMICILIO_ORD, FCH_CONSTIT_ORD, FCH_INSTRUC_PAGO, FOLIO_PAGO, FOLIO_PAQUETE, HORA_INSTRUC_PAGO, IMPORTE_ABONO,"
			+ " IMPORTE_CARGO, MOTIVO_DEVOL, NOMBRE_ORD, NOMBRE_REC, NOMBRE_REC2, NUM_CUENTA_ORD, NUM_CUENTA_REC, REFERENCIA_MED,"
			+ " RFC_ORD, RFC_REC, RFC_REC2, TIPO_CAMBIO, TIPO_CUENTA_ORD, TIPO_CUENTA_REC, TIPO_CUENTA_REC2, CIUDAD_BCO_REC, CVE_ABA,"
			+ " CVE_PAIS_BCO_REC, FORMA_LIQ, IMPORTE_DLS, NOMBRE_BCO_REC, NUM_CUENTA_REC2, PLAZA_BANXICO, SUCURSAL_BCO_REC, BUC_CLIENTE,"
			+ " IMPORTE_IVA, DIR_IP_GEN_TRAN, DIR_IP_FIRMA, UETR_SWIFT, CAMPO_SWIFT1, CAMPO_SWIFT2, REFE_ADICIONAL1, REFE_NUMERICA)"
			+ " VALUES (?,SYSDATE,'PA',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	/** La constante QUERY_OBTIENE_OPER. */
	private static final String QUERY_OBTIENE_OPER = " SELECT COD_POSTAL_ORD codpostalord,COMENTARIO1 comentario1,COMENTARIO2 comentario2,COMENTARIO3"
			+ "	comentario3,COMENTARIO4	comentario4,CONCEPTO_PAGO conceptopago,CONCEPTO_PAGO2 conceptopago2,CVE_DIVISA_ORD cvedivisaord,CVE_DIVISA_REC"
			+ " cvedivisarec,CVE_EMPRESA cveempresa,CVE_INTERME_ORD cveintermeord,CVE_INTERME_REC cveintermerec,CVE_MEDIO_ENT cvemedioent,CVE_OPERACION"
			+ " cveoperacion,CVE_PTO_VTA cveptovta,CVE_PTO_VTA_ORD cveptovtaord,CVE_PTO_VTA_REC cveptovtarec,CVE_RASTREO cverastreo,CVE_TRANSFE"
			+ "	cvetransfe,CVE_USUARIO_CAP cveusuariocap,CVE_USUARIO_SOL cveusuariosol,DIRECCION_IP direccionip,DOMICILIO_ORD domicilioord,FCH_CONSTIT_ORD"
			+ " fchconstitord,FCH_INSTRUC_PAGO fchinstrucpago,FOLIO_PAGO foliopago,FOLIO_PAQUETE foliopaquete,HORA_INSTRUC_PAGO	"
			+ " horainstrucpago,IMPORTE_ABONO importeabono,IMPORTE_CARGO importecargo,MOTIVO_DEVOL motivodevol,NOMBRE_ORD nombreord,NOMBRE_REC "
			+ " nombrerec,NOMBRE_REC2 nombrerec2,NUM_CUENTA_ORD numcuentaord,NUM_CUENTA_REC numcuentarec,REFERENCIA_MED	"
			+ " referenciamed,RFC_ORD rfcord,RFC_REC rfcrec,RFC_REC2 rfcrec2,TIPO_CAMBIO tipocambio,TIPO_CUENTA_ORD	tipocuentaord,TIPO_CUENTA_REC "
			+ " tipocuentarec,TIPO_CUENTA_REC2 tipocuentarec2,CIUDAD_BCO_REC ciudadbcorec,CVE_ABA cveaba,CVE_PAIS_BCO_REC cvepaisbcorec,FORMA_LIQ "
			+ " formaliq,IMPORTE_DLS importedls,NOMBRE_BCO_REC nombrebcorec,NUM_CUENTA_REC2 numcuentarec2,PLAZA_BANXICO plazabanxico,SUCURSAL_BCO_REC "
			+ " sucursalbcorec, BUC_CLIENTE buccliente, IMPORTE_IVA" 
			+ " importeiva, DIR_IP_GEN_TRAN diripgentran, DIR_IP_FIRMA diripfirma, UETR_SWIFT uetrswift, CAMPO_SWIFT1 camposwift1, CAMPO_SWIFT2"
			+ " camposwift2, REFE_ADICIONAL1 refeadicional1, REFE_NUMERICA refenumerica FROM TRAN_CAP_OPER WHERE ID_FOLIO = ? ";
	
	/** La constante ALTA. */
	private static final String ALTA = "alta";
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private UtileriasCapturasManuales utileriasCap = UtileriasCapturasManuales.getInstancia();

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOCapturaCentral#consultaTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanCapturaCentralResponse consultaTodosTemplates(BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) {
		//Crea los nuevos objetos de respuesta
		BeanCapturaCentralResponse response = new BeanCapturaCentralResponse();
		//Crea intancia del objeto de Consulta BD
	    ResponseMessageDataBaseDTO responseDTO = null;
	       //Nuevo objeto para respuesta de BD
		 List<HashMap<String,Object>> list = null;
	    List<String> listTemplates = new ArrayList<String>();
	    List<String> listLayouts = new ArrayList<String>();
	    try{
	    	//Realiza el Query
	       responseDTO = HelperDAO.consultar(QUERY_OBTIENE_TODOS_TEMPLATES, new ArrayList<Object>(),
	    		   HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	       //Genera el Objeto de respuesta con sus atributos
	       
		     //Valida si se debe traer informacion del layout 
			 if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				 //Obtiene el resultado del query
				 list = responseDTO.getResultQuery();
				 //Recorre el registro obtenido
			 }
			 //Agrega datos de consulta para no perderlos
			 response.setCodError(responseDTO.getCodeError());
		     response.setMsgError(responseDTO.getMessageError());
		     
	       //Cacha las Excepciones BD
	    } catch (ExceptionDataAccess e) {
	        showException(e);
	        response.setCodError(Errores.EC00011B);
	        response.setMsgError(Errores.DESC_EC00011B);
	    }
	    /** Instancia del metodo complementario**/
	    utileriasCap.validaOperacion(list, listTemplates, listLayouts);
	    response.setListLayouts(listLayouts);
	    response.setListTemplates(listTemplates);
	    //Regresa la respuesta
	    return response;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOCapturaCentral#consultaTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanCapturaCentralResponse consultaTemplate(BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) {
		//Crea los nuevos objetos de respuesta
		BeanCapturaCentralResponse response = new BeanCapturaCentralResponse();
		//Crea intancia del objeto de Consulta BD
	    ResponseMessageDataBaseDTO responseDTO = null;
	    List<Object> param = null;
	    try{
	    	param = new ArrayList<Object>();
	    	param.add(request.getTemplate().trim().toUpperCase());
	    	//Realiza el Query
	       responseDTO = HelperDAO.consultar(QUERY_OBTIENE_TEMP, param,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	       //Genera el Objeto de respuesta con sus atributos
	       response = getResponse(response, responseDTO, request);
	       if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())){
	    	   validate(response, request, architechSessionBean);
	       }
	       //Cacha las Excepciones BD
	    } catch (ExceptionDataAccess e) {
	        showException(e);
	        response.setCodError(Errores.EC00011B);
	        response.setMsgError(Errores.DESC_EC00011B);
	    }
	    //Regresa la respuesta
	    return response;
	}
	
	/**
	 * Validate.
	 *
	 * @param response the response
	 * @param request the request
	 */
	private void validate(BeanCapturaCentralResponse response, BeanCapturaCentralRequest request, ArchitechSessionBean architechSessionBean){
		if(null != response.getIdLayout() && !StringUtils.EMPTY.equals(response.getIdLayout())) {
			response.setLayout(llenaCamposTemplate(request, architechSessionBean));
			//Si se trata de un Alta de Operacion se genera el Folio
			if (ALTA.equals(request.getStep())) {
				response.setFolio(generaFolio());
			}
			//Si no se genero el folio regresa un error
			if ((null == response.getFolio() && ALTA.equals(request.getStep())) 
					|| null == response.getLayout() || response.getLayout().isEmpty()) {
				response.setCodError(Errores.EC00011B);
				return;
			}
		}
	}
	
	/**
	 * Obtener el objeto: response.
	 *
	 * @param response El objeto: response
	 * @param responseDTO El objeto: response dto
	 * @param request El objeto: request
	 * @return El objeto: response
	 */
	private BeanCapturaCentralResponse getResponse(BeanCapturaCentralResponse response,
			ResponseMessageDataBaseDTO responseDTO, BeanCapturaCentralRequest request){
		//Nuevo objeto para respuesta de BD
		 List<HashMap<String,Object>> list = null;
	     //Valida si se debe traer informacion del layout 
		 if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			 //Obtiene el resultado del query
			 list = responseDTO.getResultQuery();
			 //Recorre el registro obtenido
			 for(HashMap<String,Object> map:list){
				 response.setIdLayout(UTILS.getString(map.get("ID_LAYOUT")));
				 response.setUsrCaptura(UTILS.getString(map.get("USR_ALTA")));
			 }
			 response.setTemplate(request.getTemplate().toUpperCase());
		 }
		 //Agrega datos de consulta para no perderlos
		 response.setCodError(responseDTO.getCodeError());
	     response.setMsgError(responseDTO.getMessageError());
	     //Regresa la respuesta
		 return response;
	}
	
	/**
	 * Llena campos template.
	 *
	 * @param request El objeto: request
	 * @return Objeto list
	 */
	private List<BeanLayout> llenaCamposTemplate(BeanCapturaCentralRequest request, ArchitechSessionBean architechSessionBean){
		//Nueva instancia para respuesta del metodo
		List<BeanLayout> layout = Collections.emptyList();
		//Nueva instancia para respuesta consulta 
		List<HashMap<String,Object>> list = null;
		//Nuevo objeto para respuesta de BD
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> params = null;
		BeanCapturaCentralResponse beanResDAO = new BeanCapturaCentralResponse();
		try{
			params = new ArrayList<Object>();
			params.add(request.getTemplate().trim().toUpperCase());
			//Se realiza el query
		   responseDTO = HelperDAO.consultar(QUERY_GET_CAMPOS, params,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
		   //Cacha la excepcion
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
			beanResDAO.setCodError(Errores.EC00011B);
		}
		
		//Se valida la respuesta de BD
		if(list != null && !list.isEmpty()){
			//Nuevos objetos para almacenar valores de consulta
			layout = new ArrayList<BeanLayout>();
			//Se recorre la tabla obtenida de la consulta
			utileriasCap.continuaOperacion(list, layout, architechSessionBean);
			
			//Valida folio
			if (null != request.getIdFolio() || !StringUtils.EMPTY.equals(request.getIdFolio())) {
				layout = getOperacion(layout, request.getIdFolio(), request.isEditable());
			}
		}
		//Regresa la respuesta del servicio
		return layout;
	}
	
	/**
	 * Genera folio.
	 *
	 * @return Objeto string
	 */
	protected String generaFolio() {
		//Variable para almacenar el folio generado
	      String folio = null;
	      //Almacena el folio generado
	      List<HashMap<String,Object>> list = null;
	      //Nuevo objeto de respuesta BD
	      ResponseMessageDataBaseDTO responseDTO = null;
	      List<Object> params = null;
	      BeanCapturaCentralResponse beanResDAO = new BeanCapturaCentralResponse();
	      try{
	    	  params = Collections.emptyList();
	    	  //Consulta el folio de BD
	         responseDTO = HelperDAO.consultar(QUERY_GENERA_FOLIO, params,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         //Valida el resultado de BD
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	            list = responseDTO.getResultQuery();
	            //Obtiene el resultado
	            for(HashMap<String,Object> map:list) {
	            	folio = UTILS.getString(map.get("FOLIO"));
	            }
	         }
	         //Cacha la excepcion
	         } catch (ExceptionDataAccess e) {
	        	 showException(e);
	 			 beanResDAO.setMsgError(Errores.DESC_EC00011B);
				 beanResDAO.setCodError(Errores.EC00011B);
			}
	      //Regresa la respuesta
	      return folio;
	}

	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.capturasManuales.DAOCapturaCentral#altaCapturaManualSPID(mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase altaCapturaManualSPID(BeanOperacion operacion, 
			ArchitechSessionBean architechSessionBean) {
		  //Instancias de objetos de respuesta servicio
		  final BeanResBase response = new BeanResBase();
		  //Instancia de objeto de respuesta de BD
		  ResponseMessageDataBaseDTO responseDTO = null;
		  List<Object> params = null;
		  try{
			  params = utileriasCap.asignaValores(operacion, architechSessionBean);
			  //Se ejecuta el alta de la operacion
	        responseDTO = HelperDAO.insertar(QUERY_INSERT, params, 
	        HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	        //Se sete el mensaje y codigo de error
	        response.setCodError(responseDTO.getCodeError());
	        response.setMsgError(responseDTO.getMessageError());
	        //Se cacha la excepcion
	     } catch (ExceptionDataAccess e) {
	        showException(e);
	        response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
	     }
		  //Retorna la respuesta
	     return response;
	}
	
	
	/**
	 * Obtener el objeto: operacion.
	 *
	 * @param layout El objeto: layout
	 * @param idFolio El objeto: id folio
	 * @param isEditable El objeto: is editable
	 * @return El objeto: operacion
	 */
	protected List<BeanLayout> getOperacion(List<BeanLayout> layout, String idFolio, boolean isEditable){
		List<HashMap<String,Object>> list = null;
	    ResponseMessageDataBaseDTO responseDTO = null;
	    List<BeanLayout> layoutTmp = new ArrayList<BeanLayout>(layout);
		BeanCapturaCentralResponse beanResDAO = new BeanCapturaCentralResponse();
		List<Object> param = null;
	    try{
	    	param = new ArrayList<Object>();
	    	param.add(idFolio);
	    	//Realiza la consulta
	       responseDTO = HelperDAO.consultar(QUERY_OBTIENE_OPER, param,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	       //Valida el resultado y datos obtenidos 
	       if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	          list = responseDTO.getResultQuery();
	          //Recorre la tabla obtenida del query
	          for(HashMap<String,Object> map:list) {
	        	  layoutTmp = actualizaValor(layout, map, isEditable);
	          }
	       }
	       //Valida la Excepcion
	       } catch (ExceptionDataAccess e) {
	    	   showException(e);
	    	   beanResDAO.setMsgError(Errores.DESC_EC00011B);
	    	   beanResDAO.setCodError(Errores.EC00011B);
	      }
	    //Retorna la respuesta
	    return layoutTmp;
	}
	

	/**
	 * Actualiza valor.
	 *
	 * @param layout El objeto: layout
	 * @param operacion El objeto: operacion
	 * @param isEditable El objeto: is editable
	 * @return Objeto list
	 */
	protected List<BeanLayout> actualizaValor(List<BeanLayout> layout, Map<String,Object> operacion, boolean isEditable){
		//Recorre el layout para actualizar los valores con los de la opracion actual
		for (BeanLayout beanLayout : layout) {
			debug("CAMPO: " + beanLayout.getCampo().toUpperCase());
			//Valida variable por variable de la lista  
			if (operacion.containsKey(beanLayout.getCampo().toUpperCase())) {
				if (isEditable) {
					beanLayout.setValorActual(UTILS.getString(operacion.get(beanLayout.getCampo().toUpperCase())));
				} else {
					String constante=UTILS.getString(operacion.get(beanLayout.getCampo().toUpperCase()));
					//Agrega un espacio para que se reconzca como no editable en el front
					beanLayout.setConstante(validateConstante(constante));
				}
			}
		}
		return layout;
	}
	
	/**
	 * Validate constante.
	 *
	 * @param req the req
	 * @return the string
	 */
	protected String validateConstante(String req){
		String constante = req;
		if (null == constante || StringUtils.EMPTY.equals(constante)){
			constante = " ";
		}
		return constante;
	}
	
}
