/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 19 Abril 2018
 *
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMotDevolucion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionFiltros;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga obtener la informacion de las operaciones
 * de la cuenta principal y alterna.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperacionImpl extends Architech implements DAORecepOperacion {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1683481359799594799L;
	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas utilConsultas = UtileriasRecepcionOpConsultas.getInstance();
	/** La constante util. */
	public static final UtileriasRecepcionOperacion utilRecep = UtileriasRecepcionOperacion.getInstance();
	/** La constante util. */
	public static final UtileriasRecepcionFiltros utilFiltro = UtileriasRecepcionFiltros.getInstance();
	/** La constante FECHA. */
	private static final String FECHA = "\\[FILTRO_FECHA\\]";
    /** La constante FECHA_DATE. */
    private static final String FECHA_DATE = "\\[FILTRO_FECHA_DATE\\]";
	/** La constante VACIO. */
	private static final String VACIO = "";

	/**
	 * Metodo que se encarga de realizar la consulta de los movimientos CDA.
	 *
	 * @param clave       String con la clave de la institucion
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMiInstDAO Objeto del tipo @see BeanResConsMiInstDAO
	 */
	@Override
	public BeanResConsMiInstDAO consultaMiInstitucion(String clave, ArchitechSessionBean sessionBean) {
		/** Se inicializan las variables **/
		BeanResConsMiInstDAO beanResConsMiInstDAO = new BeanResConsMiInstDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		// se asigna la operacion a la cual buscar la fecha
		if (clave.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA)
				|| clave.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO)) {
			parametros.add("ENTIDAD_SPEI_CA");
		} else {
			parametros.add("ENTIDAD_SPEI");
		}

		String query = null;
		/** Se realiza la consulta **/
		try {
			/** sellama el query **/
			query = ConstantesRecepOperacion.QUERY_CONS_MI_INSTITUCION;
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se asigna los valores si el resultado es exitoso **/
			if (responseDTO.getResultQuery() != null) {
				list = responseDTO.getResultQuery();
				if (!list.isEmpty()) {
					/** inicializa el map **/
					Map<String, Object> mapResult = list.get(0);
					/** se obtienen el instituto **/
					beanResConsMiInstDAO.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
				}
			}
			// se asigna el mensaje de exito
			beanResConsMiInstDAO.setCodError(Errores.OK00000V);
			beanResConsMiInstDAO.setMsgError(Errores.DESC_OK00000V);
			/** Si ahy algun error se cacha e el tray **/
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsMiInstDAO.setCodError(Errores.EC00011B);
			beanResConsMiInstDAO.setMsgError(Errores.DESC_EC00011B);

		}
		/** Se regresa el resultado **/
		return beanResConsMiInstDAO;
	}

	/**
	 * Metodo que se encarga de realizar la consulta de los motivos de devoluciones.
	 *
	 * @param sessionBean        Objeto del tipo @see ArchitechSessionBean
	 * @param obQuery            El objeto: ob query
	 * @param hto                El objeto: hto
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return BeanResConsMotDevolucionDAO Objeto del tipo @see
	 *         BeanResConsMotDevolucionDAO
	 */
	@Override
	public BeanResConsMotDevolucionDAO consultaDevoluciones(ArchitechSessionBean sessionBean, int obQuery, boolean hto,
			BeanReqTranSpeiRec beanReqTranSpeiRec) {
		/** Se inicializa alas variables **/
		List<BeanMotDevolucion> beanMotDevolucionList = Collections.emptyList();
		BeanMotDevolucion beanMotDevolucion = null;
		BeanResConsMotDevolucionDAO beanResConsMotDevolucionDAO = new BeanResConsMotDevolucionDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		String query = ConstantesRecepOperacion.QUERY_DEVOLUCIONES;
		if (obQuery == 2) {
			query = ConstantesRecepOperacion.QUERY_BUSQUEDA;
		}

		if (obQuery == 3) {

			query = ConstantesRecepOperacionApartada.QUERY_USUARIOS;
			if (hto) {
				/** Consulta la institucion **/
				BeanResConsMiInstDAO beanResConsMiInstDAO = consultaMiInstitucion(
						beanReqTranSpeiRec.getBeanComun().getNombrePantalla(), sessionBean);
				beanReqTranSpeiRec.getBeanComun().setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
				// asigna los paramentros de institucion
				parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
				// asigna los parametros de fecha de operacion
				parametros.add(beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
				// reemplaza las atablas la fecha
				query = query
						.replace(ConstantesRecepOperacionApartada.TABLA_APARTA,
								ConstantesRecepOperacionApartada.TABLA_APARTA_HTO)
						.replaceAll(ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI,
								ConstantesRecepOperacionApartada.TABLA_TRAN_SPEI_HTO)
						.replaceAll(FECHA, " AND T1.FCH_OPER_PK = TO_DATE(?,'dd-mm-yyyy') ");
			} else {
				// asigna la insitutncion
				parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
				// reemplaza la fecha
				query = query.replaceAll(FECHA, " ");
			}
		}
		if (obQuery == 4) {
			query = ConstantesRecepOperacion.QUERY_CONSULTA_TIPO_PAGO;
		}

		/** Se llama al query **/
		try {
			/** Se llama la funcion del devoluciones **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** pregunta si la consulta tiene resultado **/
			if (responseDTO.getResultQuery() != null) {
				beanMotDevolucionList = new ArrayList<BeanMotDevolucion>();
				list = responseDTO.getResultQuery();
				/** Se recorre la fila **/
				for (Map<String, Object> map : list) {
					beanMotDevolucion = new BeanMotDevolucion();
					beanMotDevolucion.setSecuencial(utilerias.getString(map.get("SECUENCIAL")));
					beanMotDevolucion.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
					beanMotDevolucionList.add(beanMotDevolucion);
				}
				/** Se setea el resultado **/
				beanResConsMotDevolucionDAO.setBeanMotDevolucionList(beanMotDevolucionList);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsMotDevolucionDAO.setCodError(Errores.EC00011B);
			beanResConsMotDevolucionDAO.setMsgError(Errores.DESC_EC00011B);

		}
		/** regresa el objeto obtenido **/
		return beanResConsMotDevolucionDAO;
	}

	/**
	 * Metodo que se encarga de realizar la consulta de todas las transferencias.
	 * 
	 *
	 * @param beanReqTranSpeiRec  Objeto del tipo @see BeanReqConsTranSpeiRec
	 * @param parametros          objeto del tipo List<Object> con los parametros
	 * @param sessionBean         Objeto del tipo @see ArchitechSessionBean
	 * @param historico           parametro para el historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return BeanResConsTranSpeiRecDAO Objeto del tipo @see
	 *         BeanResConsTranSpeiRecDAO
	 */
	@Override
	public BeanResConsTranSpeiRecDAO consultaTransferencia(BeanReqTranSpeiRec beanReqTranSpeiRec,
			List<Object> parametros, ArchitechSessionBean sessionBean, boolean historico,
			BeanTranSpeiRecOper beanTranSpeiRecOper) {
		/** Se inicializan las variables **/
		BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO = new BeanResConsTranSpeiRecDAO();
		String queryTodos = "";
		String queryCount = "";
		int totalRegistros = 0;
		if (historico) {
			/** llama la funcion historico **/
			queryTodos = utilConsultas.getQueryConsultaHTO(beanReqTranSpeiRec).replaceAll(FECHA, ConstantesRecepOperacionApartada.FECHA_OPERACION_HTO);
            queryTodos = queryTodos.replaceAll(FECHA_DATE, ConstantesRecepOperacionApartada.FECHA_OPERACION_HTO_DATE);
			queryCount = ConstantesRecepOperacionApartada.CONTADOR_HIST.replaceAll(FECHA, ConstantesRecepOperacionApartada.FECHA_OPERACION_HTO);
		} else {
			queryTodos = ConstantesRecepOperacionApartada.QUERY_CONSULTA_TODAS.replaceAll(FECHA, VACIO);
		}
		
		StringBuilder filtroAndTodos = new StringBuilder();
		if (beanTranSpeiRecOper.getBeanDetalle() != null) {
			filtroAndTodos.append(utilFiltro.getFiltro(beanTranSpeiRecOper, "AND"));
		}
		if (beanTranSpeiRecOper.getUsuario() != null && !beanTranSpeiRecOper.getUsuario().isEmpty()
				&& !ConstantesRecepOperacionApartada.TODAS.equals(beanTranSpeiRecOper.getUsuario())) {
			utilFiltro.getFiltroXParam(filtroAndTodos, "AND", "COD_USR_APART", beanTranSpeiRecOper.getUsuario());
		}

		/** Se realiza la consulta **/
		String filtroFinal = filtroAndTodos.toString();
		if (historico) {
	        filtroFinal = filtroFinal.replaceAll("TSR.NUM_CUENTA_REC", "TSRH.NUM_CUENTA_REC");
	        queryCount = queryCount.replaceAll("\\[FILTRO_AND\\]", filtroFinal);
		}
		queryTodos = queryTodos.replaceAll("\\[FILTRO_AND\\]", filtroFinal);
		if (historico) {
			totalRegistros = getTotalRegistros(queryCount, historico, parametros, queryTodos);
			if (totalRegistros == 0) {
				beanResConsTranSpeiRecDAO.setCodError(Errores.OK00000V);
				beanResConsTranSpeiRecDAO.setBeanTranSpeiRecList(new ArrayList<BeanTranSpeiRecOper>()); 
			}
		}
		if (historico && totalRegistros > 0) {
			parametros.add(beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
			beanResConsTranSpeiRecDAO = consultarTodos(parametros, beanResConsTranSpeiRecDAO, queryTodos,
					beanReqTranSpeiRec);			
			beanResConsTranSpeiRecDAO.setTotalReg(totalRegistros);
		} else if (!historico) {
			beanResConsTranSpeiRecDAO = consultarTodos(parametros, beanResConsTranSpeiRecDAO, queryTodos,
					beanReqTranSpeiRec);
		}
		/** Regresa el objeto obtenido **/
		return beanResConsTranSpeiRecDAO;
	}

	/**
	 * Gets the total registros.
	 *
	 * @param queryCount the query count
	 * @param historico the historico
	 * @param parametros the parametros
	 * @param queryTodos the query todos
	 * @return the total registros
	 */
	private int getTotalRegistros(String queryCount, boolean historico, List<Object> parametros, String queryTodos) {
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			ResponseMessageDataBaseDTO responseDTO = null;
			/** Se realiza la consulta **/
			responseDTO = HelperDAO.consultar(queryCount, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
			/** Se asigna la lista **/
			List<HashMap<String, Object>> list = responseDTO.getResultQuery();
			/** En caso que el sesultado traiga informacion **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Recorre la lista **/
				/** recorre la lista **/
				for (HashMap<String, Object> map : list) {
					return utilerias.getInteger(map.get("CONT"));
				}
			} else {
				return 0;
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return 0;
	}

	/**
	 * Consultar todos.
	 *
	 * @param parametros             El objeto: parametros
	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
	 * @param queryTodos             El objeto: query todos
	 * @param beanReqTranSpeiRec     El objeto: bean req tran spei rec
	 * @return Objeto bean res cons tran spei rec DAO
	 */
	private BeanResConsTranSpeiRecDAO consultarTodos(List<Object> parametros,
			BeanResConsTranSpeiRecDAO beanResConsTranSpeiRec, String queryTodos,
			BeanReqTranSpeiRec beanReqTranSpeiRec) {

		BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO = new BeanResConsTranSpeiRecDAO();
		List<BeanTranSpeiRecOper> beanTranSpeiRecList = new ArrayList<BeanTranSpeiRecOper>();
		
		/** Se asigna los parametros para la busqueda **/
		if (beanReqTranSpeiRec.getPaginador().getAccion().isEmpty()) {
			parametros.add(1);
			parametros.add(20);
		} else {
			parametros.add(beanReqTranSpeiRec.getPaginador().getRegIni());
			parametros.add(beanReqTranSpeiRec.getPaginador().getRegFin());			
		}
		//Se ejecuta el query
		try {
			List<HashMap<String, Object>> list = null;
			ResponseMessageDataBaseDTO responseDTO = null;
			/** Se realiza la consulta **/
			responseDTO = HelperDAO.consultar(queryTodos, parametros, HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Se asigna la lista **/
			list = responseDTO.getResultQuery();
			/** En caso que el sesultado traiga informacion **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				beanTranSpeiRecList = new ArrayList<BeanTranSpeiRecOper>();
				/** Recorre la lista **/
				beanResConsTranSpeiRecDAO = utilRecep.continuaRecorer(list, beanTranSpeiRecList, beanReqTranSpeiRec);
			} else {
				/** asignancion de errores **/
				beanResConsTranSpeiRecDAO.setCodError(Errores.OK00000V);
				beanTranSpeiRecList = Collections.emptyList();
				beanResConsTranSpeiRecDAO.setBeanTranSpeiRecList(beanTranSpeiRecList);
			}
			/** Asigna los codigos **/
			beanResConsTranSpeiRecDAO.setCodError(Errores.OK00000V);
			beanResConsTranSpeiRecDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsTranSpeiRecDAO.setCodError(Errores.EC00011B);
			beanResConsTranSpeiRecDAO.setMsgError(Errores.DESC_EC00011B);

		}
		return beanResConsTranSpeiRecDAO;
	}

	/**
	 * Metodo DAO para obtener la fecha de operacion.
	 *
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
	 */
	@Override
	public BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean) {
		final BeanResConsFechaOpDAO beanResConsFechaOpDAO = new BeanResConsFechaOpDAO();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			/** se genera la consulta para obtener la feha de operacion **/
			responseDTO = HelperDAO.consultar(ConstantesRecepOperacion.QUERY_FECHA_OPERACION_SPID,
					Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//Extrae la Fecha de operacion 
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					beanResConsFechaOpDAO.setFechaOperacion(utilerias.formateaFecha(map.get("FCH_OPERACION"),
							Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
					beanResConsFechaOpDAO.setCodError(responseDTO.getCodeError());
					beanResConsFechaOpDAO.setMsgError(responseDTO.getMessageError());
				}
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsFechaOpDAO.setCodError(Errores.EC00011B);
			beanResConsFechaOpDAO.setMsgError(Errores.DESC_EC00011B);
		}
		return beanResConsFechaOpDAO;
	}

}