/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOLiquidezIntradiaImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/10/2019 12:01:27 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;



import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidacionesCombo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaReq;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasLiquidezIntradia;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasLiquidezIntradiaAux;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasLiquidezIntradiaValidar;



 /**
 * Clase que contiene los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * Clase DAOLiquidezIntradiaImpl.
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOLiquidezIntradiaImpl extends Architech implements DAOLiquidezIntradia {

	/** La constante  serialVersionUID. */
	private static final long serialVersionUID = 2267852377542609133L;

	/** La constante  utilerias. */
	private static UtileriasLiquidezIntradia utilerias = UtileriasLiquidezIntradia.getInstancia();
	
	/** La constante  utileria aux. */
	private  static UtileriasLiquidezIntradiaAux utileriaAux =  UtileriasLiquidezIntradiaAux.getUtils(); 	
	
	/** La constante  utileria aux. */
	private static UtileriasLiquidezIntradiaValidar utileriaValidar = UtileriasLiquidezIntradiaValidar.getUtileriasValidar();
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia#consultar(mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia, java.lang.String, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	/**
	 * consultar registros
	 * 
	 * Metodo publico utilizado para buscar registros
	 *
	 * @param beanFilter El objeto: bean filter - contiene los campos para realizar el filtro
	 * @param canal the canal - Contiene el canal al que se realiza la consulta
	 * @param beanPaginador El objeto: bean paginador - Contiene las variables de paginacion
	 * @param session El objeto: session - Contiene atributos de la sesion
	 * @return bean liquidez intradia req - Contiene el resultado de la consulta
	 */
	@Override
	public BeanLiquidezIntradiaReq consultar(BeanLiquidezIntradia beanFilter, String canal,BeanPaginador beanPaginador,
			ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanLiquidezIntradiaReq response = new BeanLiquidezIntradiaReq();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanLiquidezIntradia> liquidaciones = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utilerias.getFiltro(beanFilter, "AND", canal);
		String filtroWhere = utilerias.getFiltro(beanFilter, "AND ", canal);
		String consulta = utileriaAux.validaConsultaPrincipal(canal);
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(consulta.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), validaCanal(canal),
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				liquidaciones = new ArrayList<BeanLiquidezIntradia>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanLiquidezIntradia liquidacion = new BeanLiquidezIntradia();
					/** Seteo de datos **/
					liquidacion.setCveTransFe(utilerias.getString(map.get("CVE_TRASFE")));
					liquidacion.setEstatus(utilerias.getString(map.get("ESTATUS")));

					liquidacion.setCveMecanismo(utilerias.getString(map.get("CVE_MECANISMO")));

					/** Se añade el objeto a la lista**/
					liquidaciones.add(liquidacion);
					/** Seteo del total **/ 
					
					response.setTotalReg(utileriaAux.getInteger(map.get("TOTAL")));
					
					
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setListaBeanLiquidezIntradia(liquidaciones);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia#agregar(mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	/**
	 * agregar registros
	 * 
	 * Metodo publico utilizado para agregar registros
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a agregar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 */
	@Override
	public BeanResBase agregar(BeanLiquidezIntradia liquidacion,String canal, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		String consulta =utileriaAux.validaInsert(canal);
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(liquidacion, UtileriasLiquidezIntradia.ALTA);
			if("NA".equals(canal)){
				parametros.add("DF");
			}
			
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(liquidacion,canal);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(consulta, parametros, validaCanal(canal), this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;

	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia#editar(mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia, mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	/**
	 * editar registros
	 * 
	 * Metodo publico utilizado para editar registros
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a editar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param anterior objeto anterior - Contiene los atributos del objeto anterior
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 */
	@Override
	public BeanResBase editar(BeanLiquidezIntradia liquidez, BeanLiquidezIntradia anterior, String canal,ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		String consulta =utileriaAux.validaUpdate(canal);
		
		try {
			int existencia = 0;
			existencia = buscarRegistro(liquidez,canal);
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utilerias.agregarParametros(liquidez, UtileriasLiquidezIntradia.MODIFICACION);
				parametros = utilerias.agregarParametrosOld(anterior, parametros);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(consulta, parametros, validaCanal(canal),
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia#eliminar(mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	/**
	 * eliminar registros
	 * 
	 * Metodo publico utilizado para editar registros
	 *
	 * @param liquidaciones objeto liquidaciones - Contiene los atributos a eliminar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean eliminar response - Objeto que retorna resultado de la eliminacion
	 */
	@Override
	public BeanResBase eliminar(BeanLiquidezIntradia liquidez,String canal, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		String consulta =utileriaAux.validaDelete(canal);
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(liquidez, UtileriasLiquidezIntradia.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(consulta, parametros,validaCanal(canal), this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}



	/**
	 * Buscar registro.
	 * 
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param beanNotificacion El objeto: bean notificacion
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @return Objeto int - Variabe que contiene el numeo de registros
	 */
	private int buscarRegistro(BeanLiquidezIntradia beanNotificacion,String canal) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		String consulta = utileriaAux.validaExistencia(canal);
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanNotificacion, UtileriasLiquidezIntradia.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(consulta, parametros, validaCanal(canal), this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utileriaAux.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia#consultarListas(int, mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	/**
	 * consultar listas
	 * 
	 * Metodo publico utilizado para la consulta de listas
	 *
	 * @param tipo objeto tipo - Objeto que define el tipo de consulta
	 * @param beanFilter objeto bean filter  - contiene los campos para realizar el filtro
	 * @param session objeto session - Contiene atributos de la sesion
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @return objeto bean liquidaciones combo  - Contiene el resultado de la consulta
	 */
	@Override
	public BeanLiquidacionesCombo consultarListas(int tipo, BeanLiquidezIntradia beanFilter,ArchitechSessionBean session,String canal) {
		/** Declaracion del objeto de salida **/
		BeanLiquidacionesCombo listas = new BeanLiquidacionesCombo();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanCatalogo> list = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		try {
			for (int i = 0; i < 3; i++) {
				
				responseDTO = HelperDAO.consultar(utileriaValidar.evaluaConsulta(tipo,beanFilter, i,canal), Collections.emptyList(), validaCanal(canal),this.getClass().getName());
				if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
					list = new ArrayList<BeanCatalogo>();
					/** Se obtiene rel resultado de la consulta **/
					queryResponse = responseDTO.getResultQuery();
					list = getLista(queryResponse, list);
				}
				listas = utileriaAux.setLista(i, listas, list);
			}
		} catch (Exception e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		return listas;
	}

	/**
	 * Obtener el objeto: lista.
	 *
	 * Recorre el resultado de la consulta y setea los
	 * resultados a una lista
	 * 
	 * @param queryResponse El objeto: query response - Resultadod de la consulta
	 * @param list El objeto: list -  Parametro lista de beancatalogo
	 * @return El objeto: lista - Lista que contiene atributos del catalogo consultado
	 */
	private List<BeanCatalogo> getLista(List<HashMap<String, Object>> queryResponse, List<BeanCatalogo> list) {
		BeanCatalogo catalogo = null;
		/** Se recorre la respuesta  **/
		for (HashMap<String, Object> map : queryResponse) {
			catalogo = new  BeanCatalogo();
			/** Seteo de datos **/
			catalogo.setCve(utilerias.getString(map.get("CVE")));
			catalogo.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
			
			list.add(catalogo);
		}
		return list;
	}
	
 /**
  * Valida canal.
  *
  * @param canal objeto canal - Se identifica el canal que se consulta
  * @return objeto string - Resultado de tipo string 
  */
 private String validaCanal(String canal){
	 String canalReq ="";
	 if("NA".equals(canal)){
		 canalReq =HelperDAO.CANAL_GFI_DS;
		 
	 }else{
		 canalReq =HelperDAO.CANAL_TRANSFER_PLUS;
	 }
	 return canalReq;
 }
 

}
