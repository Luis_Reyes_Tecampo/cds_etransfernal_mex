/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOHistorialMensajesImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajes;

import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase que se encarga de obtener la información para la historia del mensaje
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOHistorialMensajesImpl  extends Architech implements DAOHistorialMensajes {
	/***Query para sacar el historial de mensajes*/
	public static final StringBuilder CONSULTA_HISTORIAL = 
			new StringBuilder()
			.append(" SELECT H.* FROM (")
			.append(" SELECT H.*, ROWNUM R FROM ( ")
			.append(" SELECT T2.ESTATUS DEL, ")
			.append(" CASE T2.ESTATUS ")
		    .append(" WHEN 'LB' ")
		    .append(" THEN 'Liberado' ")
		    .append(" WHEN 'DE' ")
		    .append(" THEN 'Detenido' ")
		    .append(" WHEN 'CA' ")
		    .append(" THEN 'Cancelado' ")
		    .append(" WHEN 'EN' ")
		    .append(" THEN 'Enviado' ")
		    .append(" WHEN 'CO' ")
		    .append(" THEN 'Confirmado' ")
		    .append(" WHEN 'DV' ")
		    .append(" THEN 'Devuelto' ")
		    .append(" ELSE ' ' ")
		    .append(" END ESTATUS, ")
		    .append(" T2.ESTATUS_POS AL, ")
		    .append(" CASE T2.ESTATUS_POS ")
		    .append(" WHEN 'LB' ")
		    .append(" THEN 'Liberado' ")
		    .append(" WHEN 'DE' ")
		    .append(" THEN 'Detenido' ")
		    .append(" WHEN 'CA' ")
		    .append(" THEN 'Cancelado' ")
		    .append(" WHEN 'EN' ")
		    .append(" THEN 'Enviado' ")
		    .append(" WHEN 'CO' ")
		    .append(" THEN 'Confirmado' ")
		    .append(" WHEN 'DV' ")
		    .append(" THEN 'Devuelto' ")
		    .append(" ELSE ' ' ")
		    .append(" END ESTATUS2, ")
		    .append(" T2.CVE_USUARIO USUARIO, ")
		    .append(" TO_CHAR(T2.FCH_MODIFICACION,'DD/MM/YYYY HH24:MI:SS') FECHA, ")
            .append(" D.CONT ")
            .append(" FROM TRAN_MENSAJE T1, ")
            .append(" TRAN_HIST_MENS T2 , ")
            .append(" (SELECT COUNT(*) CONT FROM  TRAN_MENSAJE T3, ")
            .append(" TRAN_HIST_MENS T4 WHERE   T3.REFERENCIA= T4.REFERENCIA  ")
            .append(" AND T3.REFERENCIA=?" )
            .append( " ) D ")
            .append(" WHERE T1.REFERENCIA= T2.REFERENCIA   ")
            .append(" AND T1.REFERENCIA=? [ORDENAMIENTO] ")
            .append( " )H")
            .append( " )H WHERE R BETWEEN ? AND ? ");

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 175403690411263434L;
	
	
	/**Metodo que sirve para consultar el historial del mensaje
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param beanReqHistorialMensajes Objeto del tipo BeanReqHistorialMensajes
	   * @return beanHistorialMensajesDAO Objeto del tipo BeanHistorialMensajesDAO
	   */
	@Override
	public BeanHistorialMensajesDAO obtenerHistorial(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes) {
		String ref="";
		ref=beanReqHistorialMensajes.getRef();
		
		List<HashMap<String, Object>> list = null;
		
		Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<Object> parametros = new ArrayList<Object>();
    	parametros.add(ref);
    	parametros.add(ref);
    	parametros.add(paginador.getRegIni());
    	parametros.add(paginador.getRegFin());
    	final BeanHistorialMensajesDAO beanHistorialMensajesDAO = new BeanHistorialMensajesDAO();
    	
		
		List <BeanHistorialMensajes> listaHistorialMensajes = null;
		try {
			String sortField = getSortField(beanReqHistorialMensajes);
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(beanReqHistorialMensajes.getSortType()).toString();
			}
			
			responseDTO = HelperDAO.consultar(CONSULTA_HISTORIAL.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
			
			if(responseDTO.getResultQuery()!=null){
				list=responseDTO.getResultQuery();
				
				if(!list.isEmpty()){
					listaHistorialMensajes = new ArrayList<BeanHistorialMensajes>();
					
					for(Map<String,Object> listMap : list){
						
						llenarhistorialMensajes(utilerias, beanHistorialMensajesDAO, listaHistorialMensajes, listMap);
					}
				}
				
			}
			beanHistorialMensajesDAO.setBeanHistorialMensajes(listaHistorialMensajes);
			beanHistorialMensajesDAO.setCodError(responseDTO.getCodeError());
			beanHistorialMensajesDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
    		showException(e);
    	
    	}
		return beanHistorialMensajesDAO;
	}

	/**
	 * @param beanReqHistorialMensajes Objeto del tipo BeanReqHistorialMensajes
	 * @return String
	 */
	private String getSortField(BeanReqHistorialMensajes beanReqHistorialMensajes) {
		String sortField = "";
		
		if ("del".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "DEL";
		} else if ("estatus".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "ESTATUS";
		} else if ("al".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "AL";
		} else if ("estatus2".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "ESTATUS2";
		} else if ("usuario".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "USUARIO";
		} else if ("fecha".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "FECHA";
		}
		return sortField;
	}

	/**Metodo que sirve para consultar el historial del mensaje
	   * @param utilerias Objeto del tipo @see Utilerias
	   * @param beanHistorialMensajesDAO Objeto del tipo BeanHistorialMensajesDAO
	   * @param listaHistorialMensajes Objeto del tipo List<BeanHistorialMensajes>
	   * @param listMap Objeto del tipo Map<String, Object>
	   */
	private void llenarhistorialMensajes(Utilerias utilerias, final BeanHistorialMensajesDAO beanHistorialMensajesDAO,
			List<BeanHistorialMensajes> listaHistorialMensajes, Map<String, Object> listMap) {
		Map<String, Object> mapResult = listMap;
		Integer zero = Integer.valueOf(0);
		
		BeanHistorialMensajes beanHistorialMensajes = new BeanHistorialMensajes();
		
		beanHistorialMensajes.setDel(utilerias.getString(mapResult.get("DEL")).toString());
		beanHistorialMensajes.setAl(utilerias.getString(mapResult.get("AL")).toString());
		beanHistorialMensajes.setEstatus2(utilerias.getString(mapResult.get("ESTATUS2")).toString());
		beanHistorialMensajes.setUsuario(utilerias.getString(mapResult.get("USUARIO")));
		beanHistorialMensajes.setFecha(utilerias.getString(mapResult.get("FECHA")).toString());
		
		listaHistorialMensajes.add(beanHistorialMensajes);
		if(zero.equals(beanHistorialMensajesDAO.getTotalReg())){
			beanHistorialMensajesDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			
		}
	}


}
