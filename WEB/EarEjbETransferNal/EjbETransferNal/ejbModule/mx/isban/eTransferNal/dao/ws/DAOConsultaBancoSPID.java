/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: DAOConsultaBancoSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 01:42:45 PM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.ws.BeanResConsultaBancoSPIDDAO;

/**
 * Interface del tipo DAO que tiene los metodos para el flujo de consulta de bancos SPID.
 */
@Local
public interface DAOConsultaBancoSPID {

	/**
	 * Consulta banco spid.
	 *
	 * @return the bean res consulta banco spiddao
	 */
	BeanResConsultaBancoSPIDDAO consultaBancoSPID();
}