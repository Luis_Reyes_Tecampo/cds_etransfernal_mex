package mx.isban.eTransferNal.dao.admonSaldosImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResUsrPassDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.ws.BeanResCutSpeiDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.admonSaldos.DAOConsultaNominaCUTDia;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaNominaCUTDiaImpl extends Architech implements DAOConsultaNominaCUTDia{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -284317392435554478L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_USR_PASS
	 */
	private static final String QUERY_USR_PASS = "select LOGIN, PASSWORD from TRAN_SPEI_PARAM WHERE CVE_MI_INSTITUC = (select cv_valor from tran_contig_unix where cv_parametro  = 'ENTIDAD_SPEI')";
	
	/** La constante QUERY_INST. */
	private static final String QUERY_INST = " select cv_valor as INST from tran_contig_unix where cv_parametro  = 'ENTIDAD_SPEI_CA' ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DATOS_CUT
	 */
	private static final String QUERY_DATOS_CUT = " SELECT  /*+ index(TRAN_SPEI_REC TRAN_SPEI_REC_PK)*/SUM(monto)  AS MONTO_REC_CUT, COUNT(tipo_pago) AS CANT_REC_CUT from tran_spei_rec "+
			" where tipo_pago = 12 "+
			" and cve_mi_instituc = ? "+
		    " and fch_operacion = TO_DATE(?,'dd-mm-yyyy')";

	
	@Override
	public BeanResUsrPassDAO consultaUsrPass(ArchitechSessionBean sessionBean){
	      final BeanResUsrPassDAO beanResUsrPassDAO = new BeanResUsrPassDAO();
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	         responseDTO = HelperDAO.consultar(QUERY_USR_PASS, Arrays.asList(
	        		 new Object[]{}
	         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	beanResUsrPassDAO.setUsuario(utilerias.getString(map.get("LOGIN")));
	            	beanResUsrPassDAO.setContrasena(utilerias.getString(map.get("PASSWORD")));
	            }
	            beanResUsrPassDAO.setCodError(Errores.OK00000V);
	            beanResUsrPassDAO.setTipoError(Errores.TIPO_MSJ_INFO);
	            beanResUsrPassDAO.setMsgError(responseDTO.getMessageError());
	         }else{
	        	 beanResUsrPassDAO.setCodError(Errores.ED00011V);
	        	 beanResUsrPassDAO.setTipoError(Errores.TIPO_MSJ_INFO); 
	         }
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResUsrPassDAO.setCodError(Errores.EC00011B);
	      }
	      return beanResUsrPassDAO;
	}
	@Override
	public BeanResCutSpeiDAO consultaNominaCUT(ArchitechSessionBean sessionBean, 
			BeanResConsFechaOpDAO beanResConsFechaOpDAO){
	      final BeanResCutSpeiDAO beanResCutSpeiDAO = new BeanResCutSpeiDAO();
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      String institucion = "";
	      try{
	    	  responseDTO = HelperDAO.consultar(QUERY_INST, Arrays.asList(
		        		 new Object[]{}
		         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    		  list = responseDTO.getResultQuery();
		            for(HashMap<String,Object> map:list){
		            	institucion = (utilerias.getString(map.get("INST")));
		            }
	    	  }
	         responseDTO = HelperDAO.consultar(QUERY_DATOS_CUT, Arrays.asList(
	        		 new Object[]{institucion, beanResConsFechaOpDAO.getFechaOperacion()}
	         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	beanResCutSpeiDAO.setMonto(utilerias.getBigDecimal(map.get("MONTO_REC_CUT")));
	            	beanResCutSpeiDAO.setCantidad(utilerias.getInteger(map.get("CANT_REC_CUT")));
	            }
	            beanResCutSpeiDAO.setCodError(Errores.OK00000V);
	            beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_INFO);
	            beanResCutSpeiDAO.setMsgError(responseDTO.getMessageError());
	         }else{
	        	 beanResCutSpeiDAO.setCodError(Errores.ED00011V);
	        	 beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_INFO); 
	         }
	      
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResCutSpeiDAO.setCodError(Errores.EC00011B);
	         beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_ALERT);
	      }
	      return beanResCutSpeiDAO;
	}

}
