/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORecepOperacionSPID2Impl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para la consulta de Movimientos CDAs
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperacionSPID2Impl  extends Architech implements DAORecepOperacionSPID2{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8167141597887988495L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE
	 */
	private static final String QUERY_UPDATE =
		"UPDATE TRAN_SPID_REC SET ESTATUS_TRANSFER=?,CODIGO_ERROR =?, MOTIVO_DEVOL=?, REFE_TRANSFER=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+ 
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE
	 */
	private static final String QUERY_UPDATE_ENVIAR =
		"UPDATE TRAN_SPID_REC SET ENVIAR=?, ESTATUS_TRANSFER=?,CODIGO_ERROR =?, MOTIVO_DEVOL=?, REFE_TRANSFER=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+ 
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE_MOTIVO
	 */
	private static final String QUERY_UPDATE_MOTIVO =
		"UPDATE TRAN_SPID_REC SET MOTIVO_DEVOL=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+ 
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE_MOTIVO_RECHAZO
	 */
	private static final String QUERY_UPDATE_MOTIVO_RECHAZO =
		"UPDATE TRAN_SPID_REC SET MOTIVO_DEVOL=?, ESTATUS_TRANSFER=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+ 
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";
	

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT
	 */
	private static final String QUERY_INSERT = " INSERT INTO TRAN_SPID_HORAS (fch_operacion,cve_inst_ord,cve_inst_benef, "+
			" folio_paquete,folio_pago,tipo,cve_rastreo, "+
			" cve_rastreo_ori,cve_transfe,cve_medio_ent) "+
			" VALUES (TO_DATE(?,'DD-MM-YYYY'),?,?, "+
			" ?,?,?,?, ?,?,?) ";
	

    @Override
    public BeanResActTranSpeiRecDAO actualizaTransSpeiRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
   		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
       try{
    	   if("RE".equals(beanReqActTranSpeiRec.getEstatus())||"DV".equals(beanReqActTranSpeiRec.getEstatus())){
    		   String envia = "S";
    		   if("RE".equals(beanReqActTranSpeiRec.getEstatus())){
    			   envia = "N";   
    		   }
    		   HelperDAO.actualizar(QUERY_UPDATE_ENVIAR, 
    				   Arrays.asList(new Object[]{
    						   envia,
    						   beanReqActTranSpeiRec.getEstatus(),
    						   beanReqActTranSpeiRec.getCodigoError(),
    						   util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
    						   beanReqActTranSpeiRec.getReferenciaTransfer(),
    						   beanReqActTranSpeiRec.getFechaOperacion(),
    						   beanReqActTranSpeiRec.getCveMiInstituc(),
    						   beanReqActTranSpeiRec.getCveInstOrd(),
    						   beanReqActTranSpeiRec.getFolioPaquete(),
    						   beanReqActTranSpeiRec.getFolioPago()
    						   }),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
    	   }else{
    		   HelperDAO.actualizar(QUERY_UPDATE, 
    				   Arrays.asList(new Object[]{
    						   beanReqActTranSpeiRec.getEstatus(),
    						   beanReqActTranSpeiRec.getCodigoError(),
    						   util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
    						   beanReqActTranSpeiRec.getReferenciaTransfer(),
    						   beanReqActTranSpeiRec.getFechaOperacion(),
    						   beanReqActTranSpeiRec.getCveMiInstituc(),
    						   beanReqActTranSpeiRec.getCveInstOrd(),
    						   beanReqActTranSpeiRec.getFolioPaquete(),
    						   beanReqActTranSpeiRec.getFolioPago()
    						   }),HelperDAO.CANAL_GFI_DS, this.getClass().getName());   
    	   }
    	   
           
          
          beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
       } catch (ExceptionDataAccess e) {
          showException(e);
          beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
       }
       return beanResActTranSpeiRecDAO;
    }
    
    /**
     * Metodo que actualiza el motivo de rechazo
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO actualizaMotivoRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
        try{
           HelperDAO.actualizar(QUERY_UPDATE_MOTIVO, 
 		   Arrays.asList(new Object[]{
 				  util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
 				   beanReqActTranSpeiRec.getFechaOperacion(),
 				   beanReqActTranSpeiRec.getCveMiInstituc(),
 				   beanReqActTranSpeiRec.getCveInstOrd(),
 				   beanReqActTranSpeiRec.getFolioPaquete(),
 				   beanReqActTranSpeiRec.getFolioPago()
 				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
        }
        return beanResActTranSpeiRecDAO;
     }
    
    /**
     * Metodo que rechaza una operacion
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO rechazar(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
        try{
           HelperDAO.actualizar(QUERY_UPDATE_MOTIVO_RECHAZO, 
 		   Arrays.asList(new Object[]{
 				  util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
 				  beanReqActTranSpeiRec.getEstatus(),
 				   beanReqActTranSpeiRec.getFechaOperacion(),
 				   beanReqActTranSpeiRec.getCveMiInstituc(),
 				   beanReqActTranSpeiRec.getCveInstOrd(),
 				   beanReqActTranSpeiRec.getFolioPaquete(),
 				   beanReqActTranSpeiRec.getFolioPago()
 				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
        }
        return beanResActTranSpeiRecDAO;
     }

    @Override
    public void guardaTranSpeiHorasMan(String cveTranfe,BeanReceptoresSPID beanReceptoresSPID,  BeanReqActTranSpeiRec beanReqActTranSpeiRec, 
      		 String tipoOperacion,ArchitechSessionBean architechSessionBean){
          final BeanResGuardaInstDAO beanResGuardaInstDAO = new BeanResGuardaInstDAO();
          ResponseMessageDataBaseDTO responseDTO = null;
          try{
        		  responseDTO = HelperDAO.insertar(QUERY_INSERT, 
        		   		   Arrays.asList(new Object[]{
        		   				   
        		   				beanReceptoresSPID.getLlave().getFchOperacion(),
        		   				beanReceptoresSPID.getLlave().getCveInstOrd(),
        		   				beanReceptoresSPID.getLlave().getCveMiInstituc(),
        		   				
        		   				beanReceptoresSPID.getLlave().getFolioPaquete(),
        		   				beanReceptoresSPID.getLlave().getFolioPago(),
        		   				tipoOperacion,
        		   				
        		   				beanReceptoresSPID.getRastreo().getCveRastreo(),
        		   				beanReceptoresSPID.getRastreo().getCveRastreoOri(),
        		   				cveTranfe,
        		   				"SPID"
        		   		   }), 
        		             HelperDAO.CANAL_GFI_DS, this.getClass().getName());
        		             beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
        		             beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());
        	  
          } catch (ExceptionDataAccess e) {
             showException(e);
             beanResGuardaInstDAO.setCodError(Errores.EC00011B);
          }
       }
	
	
}
