/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 10:03:36 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResEstadoConexionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el monitor CDA
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorCDAImpl extends Architech implements DAOMonitorCDA {

	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = -5407710756080645537L;
	/**
	 * Constante privada del tipo String que almacena el query para saber si hay contingencia
	 */
	private static final String QUERY_CONTINGENCIA =
	
	" SELECT 'DESCONEXION' TIPO, TRAN_SPEI_CTRL.BYTES_RECIBIDOS VALOR "+
	" FROM TRAN_SPEI_CTRL "+
	" WHERE TRAN_SPEI_CTRL.CVE_MI_INSTITUC = "+
	"  (SELECT (-1) * TRAN_CONTIG_UNIX.CV_VALOR "+
	"  FROM TRAN_CONTIG_UNIX "+
	"  WHERE TRAN_CONTIG_UNIX.CV_PARAMETRO = 'ENTIDAD_SPEI' "+
	"  ) " +
	" UNION " +
	" SELECT 'CONTINGENCIA' TIPO, TO_NUMBER(CV_VALOR) VALOR "+
	"  FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ECDA_PROC'";
	
	/**
	 * Constante para identificar la tabla de donde se obtienen
	 * las transacciones SPEI
	 */
	private static final String FROM_TRAN_SPEI_REC = "FROM TRAN_SPEI_REC";
	/**
	 * Constante para identificar la tabla de donde se obtienen
	 * las transacciones SPEI CDA
	 */
	private static final String FROM_TRAN_SPEI_CDA = "FROM TRAN_SPEI_CDA";

	/**
	 * Constante para almacenar string UNION_ALL
	 */
	private static final String UNION_ALL = "UNION ALL";
	/**
	 * Constante para almacenar string MONTO
	 */
	private static final String MONTO = "MONTO";
	/**
	 * Constante para almacenar string CANTIDAD
	 */
	private static final String CANTIDAD = "CANTIDAD";
	/**
	 * Constante privada de tipo String que almacena el query
	 * para obtener la CDA's agrupadas, con su correspondiente
	 * monto y volumen, la consulta incluye el estado de conexion
	 * para saber si el monitor CDA se encuentra en contingencia 
	 */
	private static final String QUERY_MONITOR_CDA = 				
		"SELECT 'RECIBIDAS' TIPO, COUNT(1)  CANTIDAD, NVL(SUM(MONTO),0) MONTO "+
		FROM_TRAN_SPEI_REC + " " +
		"WHERE tipo_pago IN (SELECT VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE 'TPAGO_SPEI_CDA%') AND estatus_transfer = 'TR' " +
		UNION_ALL + " " +
		"SELECT 'PENDIENTES' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO "+
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus in ('0', '6') "+
		UNION_ALL + " " +
		"SELECT 'ENVIADAS' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus = '1' " +
		UNION_ALL + " " +
		"SELECT 'CONFIRMADAS' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus = '2' " +
		UNION_ALL + " " +
		"SELECT 'ENV_CONTINGENCIA' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus = '3' " +
		UNION_ALL + " " +
		"SELECT 'CONF_CONTINGENCIA' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus = '4'  " +
		UNION_ALL + " " +
		"SELECT 'RECHAZADAS' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " +
		FROM_TRAN_SPEI_CDA + " " +
		"WHERE estatus = '5'  " +
		UNION_ALL + " " +
		"SELECT 'ESTADO_CONEXION',tran_spei_ctrl.BYTES_RECIBIDOS, NULL " +
		"FROM tran_spei_ctrl " +
		"WHERE tran_spei_ctrl.CVE_MI_INSTITUC =" +
		 "(SELECT (-1) * tran_contig_unix.CV_VALOR " +
		  "FROM tran_contig_unix " +
		   "WHERE tran_contig_unix.CV_PARAMETRO = 'ENTIDAD_SPEI' " +
		  ") "+
		  UNION_ALL + " " +
		  "SELECT 'CONTINGENCIA' TIPO, TO_NUMBER(CV_VALOR) MONTO, NULL"+
			"  FROM TRAN_CONTIG_UNIX "+
			" WHERE CV_PARAMETRO = 'ECDA_PROC' " +
		UNION_ALL + " " +
		"SELECT 'TOTAL_PROCESADAS' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " + FROM_TRAN_SPEI_CDA + " WHERE estatus in ('0','1','2','5','6') " +
		UNION_ALL + " " +
		"SELECT 'TOTAL_CONTINGENCIA' TIPO, COUNT(1) CANTIDAD, NVL(SUM(MONTO),0) MONTO " + FROM_TRAN_SPEI_CDA + " WHERE estatus in ('3','4')";
	
	/**
	 * Constante privada de tipo String que almacena el query
	 * para solicitar la terminacion administrativa del servicio CDA
	 */
    private static final String QUERY_ACTUALIZAR_SERVICIO_CDA = 
    	"UPDATE TRAN_CONTIG_UNIX " +
    	"SET CV_VALOR  = -1 " +
    	"WHERE CV_PARAMETRO = 'ECDA_PROC'";
	
   /**Metodo que se encarga para saber si esta en contingencia o no
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResEstadoConexionDAO Objeto del tipo BeanResEstadoConexionDAO
   */
   public BeanResEstadoConexionDAO consultaEstadoConexion(ArchitechSessionBean architechSessionBean){
	  final BeanResEstadoConexionDAO beanResEstadoConexionDAO = new BeanResEstadoConexionDAO();
      Utilerias utilerias = Utilerias.getUtilerias();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      try {
    	responseDTO = HelperDAO.consultar(QUERY_CONTINGENCIA, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			list = responseDTO.getResultQuery();
			for(HashMap<String,Object> map:list){
				String tipo = utilerias.getString(map.get("TIPO"));
				if("DESCONEXION".equals(tipo)){
					beanResEstadoConexionDAO.setBytesRecibidos(utilerias.getInteger(map.get("VALOR")));
				}else if("CONTINGENCIA".equals(tipo)){
					beanResEstadoConexionDAO.setContingencia(utilerias.getInteger(map.get("VALOR")));
				}
  		  }
		beanResEstadoConexionDAO.setCodError(responseDTO.getCodeError());
		beanResEstadoConexionDAO.setMsgError(responseDTO.getMessageError());

		}
	} catch (ExceptionDataAccess e) {
		showException(e);
		beanResEstadoConexionDAO.setCodError(Errores.EC00011B);
	}
      
     return beanResEstadoConexionDAO;
   }

	/**
	 * Metodo que sirve para consultar los movimientos CDA agrupados por monto y volumen
	 *  @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 *  @return BeanResMonitorCdaDAO Objeto del tipo @see BeanResMonitorCdaDAO
	 */
	@Override
	public BeanResMonitorCdaDAO consultarCDAAgrupadas(
			ArchitechSessionBean architechSessionBean){
		
		final BeanResMonitorCdaDAO beanResMonCdaDAO = new BeanResMonitorCdaDAO();
		List<HashMap<String,Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    int conectado =  0;
	    int contingencia =  0;
	    
	    try {
	      	responseDTO = HelperDAO.consultar(QUERY_MONITOR_CDA, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    			list = responseDTO.getResultQuery();
	
	    			beanResMonCdaDAO.setMontoCDAOrdRecAplicadas(
	    					utilerias.formateaDecimales(list.get(0).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAOrdRecAplicadas(
	    					utilerias.formateaDecimales(list.get(0).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAPendEnviar(
	    					utilerias.formateaDecimales(list.get(1).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAPendEnviar(
	    					utilerias.formateaDecimales(list.get(1).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAEnviadas(
	    					utilerias.formateaDecimales(list.get(2).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAEnviadas(
	    					utilerias.formateaDecimales(list.get(2).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAConfirmadas(
	    					utilerias.formateaDecimales(list.get(3).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAConfirmadas(
	    					utilerias.formateaDecimales(list.get(3).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAEnvContingencia(
	    					utilerias.formateaDecimales(list.get(4).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAEnvContingencia(
	    					utilerias.formateaDecimales(list.get(4).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAConfContingencia(
	    					utilerias.formateaDecimales(list.get(5).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAConfContingencia(
	    					utilerias.formateaDecimales(list.get(5).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDARechazadas(
	    					utilerias.formateaDecimales(list.get(6).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDARechazadas(
	    					utilerias.formateaDecimales(list.get(6).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			conectado =  utilerias.getInteger(list.get(7).get(CANTIDAD));
	    			contingencia =  utilerias.getInteger(list.get(8).get(CANTIDAD));
	    			
	    			beanResMonCdaDAO.setMontoCDAProcesadas(
	    					utilerias.formateaDecimales(list.get(9).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAProcesadas(
	    					utilerias.formateaDecimales(list.get(9).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			beanResMonCdaDAO.setMontoCDAContingencia(
	    					utilerias.formateaDecimales(list.get(10).get(MONTO),Utilerias.FORMATO_DECIMAL_NUMBER));
	    			beanResMonCdaDAO.setVolumenCDAContingencia(
	    					utilerias.formateaDecimales(list.get(10).get(CANTIDAD),Utilerias.FORMATO_NUMBER));
	    			
	    			if(conectado != -1){
	    				beanResMonCdaDAO.setExisteConexion(true);
	    			}
	    			if(contingencia == -1){
	    				beanResMonCdaDAO.setExisteContingencia(true);
	    			}
	    			    				
	    			beanResMonCdaDAO.setCodError(responseDTO.getCodeError());
	    			beanResMonCdaDAO.setMsgError(responseDTO.getMessageError());
	    		}
	    	} catch (ExceptionDataAccess e) {
	    		showException(e);
	    		beanResMonCdaDAO.setCodError(Errores.EC00011B);
	    	}
	    return beanResMonCdaDAO;
	}

	/**
	 * Metodo que sirve para actualiza la tabla de parametros para solicitar
	 * la detencion del servicio CDA.
	 *  @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 *  @return BeanResMonitorCdaDAO Objeto del tipo @see BeanResMonitorCdaDAO
	 */
	@Override
	public BeanResMonitorCdaDAO detenerServicioCDA(
			ArchitechSessionBean architechSessionBean){
		final BeanResMonitorCdaDAO beanResMonCdaDAO = new BeanResMonitorCdaDAO();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    try {
	  	 responseDTO = HelperDAO.actualizar(QUERY_ACTUALIZAR_SERVICIO_CDA,Collections.emptyList(), 
	     HelperDAO.CANAL_GFI_DS, this.getClass().getName());
					beanResMonCdaDAO.setCodError(responseDTO.getCodeError());
					beanResMonCdaDAO.setMsgError(responseDTO.getMessageError());		
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResMonCdaDAO.setCodError(Errores.EC00011B);
		}    
	    return beanResMonCdaDAO;	
	}

}
