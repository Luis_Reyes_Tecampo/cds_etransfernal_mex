/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOPagoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPagoDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenantePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptorPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para el listado de pagos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOPagoImpl extends Architech implements DAOPago {
	
	/**
	 * Consulta para buscar las ordenes y modulo SPID
	 */
	private static final String CVE_INTERME_ORD= "CVE_INTERME_ORD";
	/**
	 * Constante FROM de uso comun en queries
	 */
	private static final String FROM= "FROM (";
	
	/**
	 * CAMPO CVE_MECANISMO
	 */
	private static final String CVE_MECANISMO = "CVE_MECANISMO";
	
	/**
	 * CAMPO ESTATUS
	 */
	private static final String ESTATUS = "ESTATUS";
	
	/**
	 * Constante WHERE para uso comun en queries
	 */
	private static final String WHERE = "WHERE ";
	
	/**
	 * Constante AND para uso comun en queries
	 */
	private static final String AND = "AND ";
	
	/**
	 * Constante IN para uso comun en queries
	 */
	private static final String IN = " IN [";
	
	/**
	 * Constante de llave para uso comun en queries
	 */
	private static final String OPEN_SP = " = '[";
	
	/**
	 * Constante de cierre de llave para uso comun en queries 
	 */
	private static final String CLOSE_SP = "]' ";
	
	/**
	 * Querie para consulta de ordenes
	 */
	private static final StringBuilder CONSULTA_ORDENES = 
			new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") 
			    .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") 
			    .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") 
			    .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        
			    .append(FROM) 
			       .append("SELECT TM.*, ROWNUM AS RN ")
			        .append(FROM)
			            .append("SELECT REFERENCIA, CVE_COLA, CVE_TRANSFE, CVE_MECANISMO,")
			            .append("CVE_MEDIO_ENT, FCH_CAPTURA, CVE_INTERME_ORD, CVE_INTERME_REC,")
			            .append("IMPORTE_ABONO, ").append(ESTATUS).append(", COMENTARIO3, REFERENCIA_MECA, CONTADOR.CONT ") 
			            .append(FROM)
			                .append("SELECT COUNT(1) CONT ")
			            .append("FROM TRAN_MENSAJE TM ")
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			        .append(") CONTADOR, TRAN_MENSAJE ")
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			      .append(") TM ")
			     .append(")TM ")
			    .append("WHERE RN BETWEEN ? AND ? ");
	
	/**
	 * Querie para consulta de ordenes para la sumatoria
	 */
	private static final StringBuilder CONSULTA_ORDENES_SUMATORIA = 
			new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
			            .append(FROM)
			                .append("SELECT COUNT(1) CONT ")
			            .append("FROM TRAN_MENSAJE TM ")
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			        .append(") CONTADOR, TRAN_MENSAJE ")
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP);
			     
	
	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos las ordenes de Reparacion exportadas
	 */
	private static final StringBuilder INSERT_TODOS_ORDENES = 
			new StringBuilder().append("INSERT INTO TRAN_SPEI_EXP_CDA")
							   .append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
                               .append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * Consulta del detalle de pago
	 */
	private static final StringBuilder CONSULTA_DETALLE_PAGO =
			 new StringBuilder().append("SELECT TM.REFERENCIA, TM.ESTATUS, TM.CVE_EMPRESA, TM.CVE_MEDIO_ENT, TM.CVE_USUARIO_CAP, ")
			 					.append("TM.REFERENCIA_CON, TM.CVE_TRANSFE, TM.CVE_PTO_VTA, TM.REFERENCIA_MED, TM.REFERENCIA_MECA, ")
			 					.append("TM.FORMA_LIQ, TM.CVE_COLA, TM.CVE_OPERACION, TM.CVE_USUARIO_SOL, TM.CVE_INTERME_ORD, TM.CVE_PTO_VTA_ORD, ")
			 					.append("TM.NUM_CUENTA_ORD, TM.CVE_DIVISA_ORD, TM.IMPORTE_CARGO, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")
			 					.append("TM.CVE_PTO_VTA_REC,  TM.CVE_DIVISA_REC, TM.NUM_CUENTA_REC, TM.CVE_SWIFT_COR, TM.COMENTARIO1, TM.COMENTARIO2, ")
			 					.append("TM.COMENTARIO3, TM.SUCURSAL_BCO_REC, TM.CVE_DEVOLUCION, TMS.MOTIVO_DEVOL, TMS.FCH_INSTRUC_PAGO, TMS.HORA_INSTRUC_PAGO, TMS.REFE_NUMERICA, ")
			 					.append("CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TMS.FCH_CAPTURA, TMS.CONCEPTO_PAGO, TMS.CVE_RASTREO, TMS.DIRECCION_IP, ")
			 					.append("TMS.TIPO_CUENTA_ORD, TMS.COD_POSTAL_ORD, TMS.FCH_CONSTIT_ORD, TMS.RFC_ORD, TMS.NOMBRE_ORD, ")
			 					.append("TMS.DOMICILIO_ORD, TMS.TIPO_CUENTA_REC, TMS.RFC_REC, TMS.NOMBRE_REC, TMS.CONCEPTO_PAGO ")
			 					.append("FROM TRAN_MENSAJE TM LEFT JOIN TRAN_MENSAJE_SPID TMS ON TM.REFERENCIA = TMS.REFERENCIA ")
			 					.append("WHERE TM.REFERENCIA = ? ");
	/**
	 * 
	 */
	private static final long serialVersionUID = 1041913510555848982L;

	@Override
	public BeanResListadoPagosDAO obtenerListadoPagos(
			String cveMecanismo, String estatus,
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean) {

		final BeanResListadoPagosDAO beanResListadoPagosDAO = new BeanResListadoPagosDAO();
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanPago> listaBeanPagos = new ArrayList<BeanPago>();
    	
    	try {
        	responseDTO = HelperDAO.consultar(CONSULTA_ORDENES.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[ESTATUS\\]", estatus), Arrays.asList(
        			new Object[] { beanPaginador.getRegIni(),
        						   beanPaginador.getRegFin() }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();
        		 
        		 if(!list.isEmpty()){
        			 listaBeanPagos = llenarListadoPagos(beanResListadoPagosDAO, list, utilerias);
        			 }
        		 }
        	beanResListadoPagosDAO.setListBeanPago(listaBeanPagos);
        	beanResListadoPagosDAO.setCodError(responseDTO.getCodeError());
        	beanResListadoPagosDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return beanResListadoPagosDAO;
	}
	
	@Override
	public BigDecimal obtenerListadoPagosMontos(
			String cveMecanismo, String estatus,ArchitechSessionBean architechSessionBean) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		ResponseMessageDataBaseDTO responseDTO = null;
    	
    	try {
        	responseDTO = HelperDAO.consultar(CONSULTA_ORDENES_SUMATORIA.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[ESTATUS\\]", estatus), Arrays.asList(
        			new Object[] { }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	
        	totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return totalImporte;
	}

	/**Metodo que sirve para llenar el listado de pagos
	 * * @param beanResListadoPagosDAO Objeto del tipo BeanResListadoPagosDAO
	   * @param list Objeto del tipo List<HashMap<String, Object>>
	   * @param utilerias Objeto del tipo Utilerias
	   * @return List<BeanPago> Objeto del tipo List<BeanPago>
	   */
	private List<BeanPago> llenarListadoPagos(final BeanResListadoPagosDAO beanResListadoPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		BeanPago beanPago;
		List<BeanPago> listaBeanPagos;
	    listaBeanPagos = new ArrayList<BeanPago>();
	    Integer cero = 0;
        			 
        			 for(Map<String,Object> mapResult : list){
        				 beanPago = new BeanPago();
        				 beanPago.setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
        				 beanPago.setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
        				 beanPago.setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
        				 beanPago.setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
        				 beanPago.setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
        				 beanPago.setFechaCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
        				 beanPago.setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD)));
        				 beanPago.setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
        				 beanPago.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
        				 beanPago.setEstatus(utilerias.getString(mapResult.get("ESTATUS")));
        				 beanPago.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
        				 beanPago.setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
        				 beanPago.setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
        				 
        				 listaBeanPagos.add(beanPago);
        				 
        				 if (cero.equals(beanResListadoPagosDAO.getTotalReg())){
        					 beanResListadoPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
						 }
        			 }
		return listaBeanPagos;
	}

	/**
	 * Metodo que sirve para exportar el listado de pagos
	 * @param tipoOrden Objeto del tipo String
	 * @param cveMecanismo Objeto del tipo String
	 * @param estatus Objeto del tipo String
	 * @param beanReqListadoPagos Objeto del tipo BeanReqListadoPagos
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean 
	 * @return BeanResListadoPagosDAO Objeto del tipo BeanResListadoPagosDAO
	 */
	public BeanResListadoPagosDAO exportarTodoListadoPagos(String tipoOrden, String cveMecanismo, String estatus, BeanReqListadoPagos beanReqListadoPagos, 
    		ArchitechSessionBean architechSessionBean){
		
    	StringBuilder query = new StringBuilder().append("SELECT TM.REFERENCIA,")
				   .append("TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO,") 
				   .append("TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, TM.CVE_INTERME_ORD,")
			   	   .append("TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, TM.ESTATUS ");
    	StringBuilder builder = new StringBuilder();
    	final BeanResListadoPagosDAO beanResListadoPagosDAO = new BeanResListadoPagosDAO();
    	Utilerias utilerias = Utilerias.getUtilerias();
        final Date fecha = new Date();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
        	builder.append(query.toString().replaceAll("\\,", " || ',' || "))
	   	   	   .append("FROM TRAN_MENSAJE TM ") 
	   	   	   .append("WHERE TM.ESTATUS IN [").append(ESTATUS).append("] AND TM.CVE_MECANISMO = '[CVE_MECANISMO]'");
        	
        	final String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        	
        	final String nombreArchivo = "SPID_" + obtenerNombre(tipoOrden) + fechaActual+".csv";
        	beanResListadoPagosDAO.setNombreArchivo(nombreArchivo);
    	   
        	responseDTO = HelperDAO.insertar(INSERT_TODOS_ORDENES.toString(),
        			Arrays.asList(new Object[]{architechSessionBean.getUsuario(),
        				                     architechSessionBean.getIdSesion(),
        				                     "SPID",
        				                     "LISTADO PAGOS",
        				                     nombreArchivo,
        				                     beanReqListadoPagos.getTotalReg(),
        				                     "PE",
        				                     builder.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[ESTATUS\\]", estatus),
        				                     "REFE, COLA, CLAVE TRAN., MECAN, MEDIO ENT., FCH. CAP, ORD, REC, IMPORTE ABONO, EST."
        			}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           
        	beanResListadoPagosDAO.setCodError(responseDTO.getCodeError());
        	beanResListadoPagosDAO.setMsgError(responseDTO.getMessageError());

        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResListadoPagosDAO.setCodError(Errores.EC00011B); 
        }
        
        return beanResListadoPagosDAO;
	}
	
	
	/**Ganera la nomenclatura del archivo a exportar
	 * @param tipoOrden objeto del tipo String
	 * @return String
	 */
	private String obtenerNombre (String tipoOrden){
		String nom = "";
		
		if ("OPD".equals(tipoOrden)){
			nom = "OR_DV_";
		} else if ("TSS".equals(tipoOrden)){
			nom = "TR_SS_";
		} else if ("OEC".equals(tipoOrden)){
			nom = "OR_CO_";
		} else if ("DEC".equals(tipoOrden)){
			nom = "DV_CO_";
		} else if ("TES".equals(tipoOrden)){
			nom = "TR_ES_";
		} else if ("TEN".equals(tipoOrden)){
			nom = "TR_EN_";
		} else if ("TPR".equals(tipoOrden)){
			nom = "TR_PR_";
		} else if ("OES".equals(tipoOrden)){
			nom = "OR_ES_";
		} else if ("OEN".equals(tipoOrden)){
			nom = "OR_EN_";
		} 
		return nom;
	}
	
	/**
	 * Metodo que sirve para consultar el detalle de pago
	 * * @param referencia Objeto del tipo String 
	   * @return BeanDetallePago Objeto del tipo BeanDetallePago
	 */
	public BeanDetallePago consultarDetallePago(String referencia) {
		BeanDetallePago beanDetallePago = new BeanDetallePago();
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			responseDTO = HelperDAO.consultar(
					CONSULTA_DETALLE_PAGO.toString(), 
					Arrays.asList(new Object[]{referencia}), 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if(responseDTO.getResultQuery()!=null && !responseDTO.getResultQuery().isEmpty()) {
				List<HashMap<String, Object>> listaResultados = responseDTO.getResultQuery();
				Map<String, Object> mapaResultado = listaResultados.get(0);
				
					llenarDatosGeneralesPago(beanDetallePago, utilerias, mapaResultado);				
			}
		}catch(ExceptionDataAccess e) {
			showException(e);
			beanDetallePago.setCodError(e.getCode());
		}
		return beanDetallePago;
	}

	/**
	 * Metodo para llenar los datos generales
	 * @param beanDetallePago Objeto del tipo BeanDetallePago
	 * @param utilerias Objeto del tipo Utilerias
	 * @param mapaResultado Objeto del tipo Map<String, Object>
	 */
	private void llenarDatosGeneralesPago(BeanDetallePago beanDetallePago, Utilerias utilerias,
			Map<String, Object> mapaResultado) {
		BeanDatosGeneralesPago beanDatosGeneralesPago;
		BeanOrdenantePago beanOrdenantePago;
		BeanReceptorPago beanReceptorPago;
					beanDatosGeneralesPago = new BeanDatosGeneralesPago();
					beanOrdenantePago = new BeanOrdenantePago();
					beanReceptorPago = new BeanReceptorPago();
					
					datosGenerales(utilerias, mapaResultado, beanDatosGeneralesPago);
					
					beanOrdenantePago.setIntermediario(utilerias.getString(mapaResultado.get("CVE_INTERME_ORD")));
					beanOrdenantePago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA_ORD")));
					beanOrdenantePago.setNumeroCuenta(utilerias.getString(mapaResultado.get("NUM_CUENTA_ORD")));
					beanOrdenantePago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_ORD")));
					beanOrdenantePago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_ORD")));
					beanOrdenantePago.setImporteCargo(utilerias.getString(mapaResultado.get("IMPORTE_CARGO")));
					beanOrdenantePago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_ORD")));
					beanOrdenantePago.setCodigoPostal(utilerias.getString(mapaResultado.get("COD_POSTAL_ORD")));
					beanOrdenantePago.setFechaConstit(utilerias.getString(mapaResultado.get("FCH_CONSTIT_ORD")));
					beanOrdenantePago.setRfc(utilerias.getString(mapaResultado.get("RFC_ORD")));
					beanOrdenantePago.setDomicilio(utilerias.getString(mapaResultado.get("DOMICILIO_ORD")));
					
					beanReceptorPago.setIntermediario(utilerias.getString(mapaResultado.get("CVE_INTERME_REC")));
					beanReceptorPago.setNumeroCuentaReceptor(utilerias.getString(mapaResultado.get("NUM_CUENTA_REC")));
					beanReceptorPago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_REC")));
					beanReceptorPago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_REC")));
					beanReceptorPago.setImporteAbono(utilerias.getBigDecimal(mapaResultado.get("IMPORTE_ABONO")));
					beanReceptorPago.setClaveSwiftCorres(utilerias.getString(mapaResultado.get("CVE_SWIFT_COR")));
					beanReceptorPago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_REC")));
					beanReceptorPago.setRfc(utilerias.getString(mapaResultado.get("RFC_REC")));
					beanReceptorPago.setSucursal(utilerias.getString(mapaResultado.get("SUCURSAL_BCO_REC")));
					
					beanReceptorPago.setConceptoPago(utilerias.getString(mapaResultado.get("CONCEPTO_PAGO")));
					beanReceptorPago.setComentario1(utilerias.getString(mapaResultado.get("COMENTARIO1")));
					beanReceptorPago.setComentario2(utilerias.getString(mapaResultado.get("COMENTARIO2")));
					beanReceptorPago.setComentario3(utilerias.getString(mapaResultado.get("COMENTARIO3")));
					
					beanDetallePago.setBeanDatosGeneralesPago(beanDatosGeneralesPago);
					beanDetallePago.setBeanReceptorPago(beanReceptorPago);
					beanDetallePago.setBeanOrdenantePago(beanOrdenantePago);
	}

	/**Metodo para obtener los datos generales
	 * @param utilerias objeto del tipo Utilerias
	 * @param mapaResultado objeto del tipo Map<String, Object>
	 * @param beanDatosGeneralesPago objeto del tipo BeanDatosGeneralesPago
	 */
	private void datosGenerales(Utilerias utilerias, Map<String, Object> mapaResultado,
			BeanDatosGeneralesPago beanDatosGeneralesPago) {
		beanDatosGeneralesPago.setBeanDatosGeneralesPagoDos(new BeanDatosGeneralesPagoDos());
		
		beanDatosGeneralesPago.setClaveTransferencia(utilerias.getString(mapaResultado.get("CVE_TRANSFE")));
		beanDatosGeneralesPago.setFormaLiquidacion(utilerias.getString(mapaResultado.get("FORMA_LIQ")));
		beanDatosGeneralesPago.setReferencia(utilerias.getString(mapaResultado.get("REFERENCIA")));
		beanDatosGeneralesPago.setCola(utilerias.getString(mapaResultado.get("CVE_COLA")));
		beanDatosGeneralesPago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA")));
		beanDatosGeneralesPago.setEmpresa(utilerias.getString(mapaResultado.get("CVE_EMPRESA")));
		beanDatosGeneralesPago.setClaveOperacion(utilerias.getString(mapaResultado.get("CVE_OPERACION")));
		beanDatosGeneralesPago.setReferenciaMedio(utilerias.getString(mapaResultado.get("REFERENCIA_MED")));
		beanDatosGeneralesPago.setMedioEntrega(utilerias.getString(mapaResultado.get("CVE_MEDIO_ENT")));
		beanDatosGeneralesPago.setUsuarioSolicito(utilerias.getString(mapaResultado.get("CVE_USUARIO_SOL")));
		beanDatosGeneralesPago.setFechaCaptura(utilerias.getString(mapaResultado.get("FCH_CAPTURA")));
		beanDatosGeneralesPago.setUsuarioCapturo(utilerias.getString(mapaResultado.get("CVE_USUARIO_CAP")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setClaveDevolucion(utilerias.getString(mapaResultado.get("CVE_DEVOLUCION")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setRefeMecanismo(utilerias.getString(mapaResultado.get("REFERENCIA_MECA")));
		beanDatosGeneralesPago.setRefeConcent(utilerias.getString(mapaResultado.get("REFERENCIA_CON")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setMotivoDevolucion(utilerias.getString(mapaResultado.get("MOTIVO_DEVOL")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setFechaInstruccionPago(utilerias.getString(mapaResultado.get("FCH_INSTRUC_PAGO")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setHoraInstruccionPago(utilerias.getString(mapaResultado.get("HORA_INSTRUC_PAGO")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setRefeNumerica(utilerias.getString(mapaResultado.get("REFE_NUMERICA")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setFolioPaquete(utilerias.getString(mapaResultado.get("FOLIO_PAQUETE")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setFolioPago(utilerias.getString(mapaResultado.get("FOLIO_PAGO")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setClaveRastreo(utilerias.getString(mapaResultado.get("CVE_RASTREO")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setDireccionIP(utilerias.getString(mapaResultado.get("DIRECCION_IP")));
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setEstatus(utilerias.getString(mapaResultado.get("ESTATUS")));
	}
	
	@Override
	public BeanResListadoPagosDAO buscarPagos(
			String field, String valor, String cveMecanismo, String estatus,
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean) {
		final BeanResListadoPagosDAO beanResListadoPagosDAO = new BeanResListadoPagosDAO();
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanPago> listaBeanPagos = new ArrayList<BeanPago>();
    	
    	try {              	
    		StringBuilder queryCo = new StringBuilder();
    		StringBuilder queryCompleto = new StringBuilder();
    		condicionFields(field, valor, queryCo);
    		
    		queryCompleto = obtenerFormatoQuery(queryCo);
    		
        	responseDTO = HelperDAO.consultar(queryCompleto.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[ESTATUS\\]", estatus), Arrays.asList(
        			new Object[] { beanPaginador.getRegIni(),
        						   beanPaginador.getRegFin() }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();
        		 
        		 if(!list.isEmpty()){
        			 listaBeanPagos = llenarPagos(beanResListadoPagosDAO, list, utilerias);
        		 }
        	}
        	beanResListadoPagosDAO.setListBeanPago(listaBeanPagos);
        	beanResListadoPagosDAO.setCodError(responseDTO.getCodeError());
        	beanResListadoPagosDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		beanResListadoPagosDAO.setCodError(Errores.EC00011B);
    	}
		return beanResListadoPagosDAO;
	}
	
	
	@Override
	public BigDecimal buscarPagosMontos(
			String field, String valor, String cveMecanismo, String estatus,
		     ArchitechSessionBean architechSessionBean) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		ResponseMessageDataBaseDTO responseDTO = null;
    	    	
    	try {              	
    		StringBuilder queryCo = new StringBuilder();
    		StringBuilder queryCompleto = new StringBuilder();
    		condicionFields(field, valor, queryCo);
    		
    		queryCompleto = obtenerFormatoQueryMontos(queryCo);
    		
        	responseDTO = HelperDAO.consultar(queryCompleto.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[ESTATUS\\]", estatus), Arrays.asList(
        			new Object[] { }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return totalImporte;
	}
				
	/**
	 * Metodo para llenar los pagos
	 * @param beanResListadoPagosDAO Objeto del tipo BeanResListadoPagosDAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @return listaBeanPagos Objeto del tipo List<BeanPago>
	 */
	private List<BeanPago> llenarPagos(final BeanResListadoPagosDAO beanResListadoPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		Integer zero = Integer.valueOf(0);
		BeanPago beanPagos;
		List<BeanPago> listaBeanPagos;
		listaBeanPagos = new ArrayList<BeanPago>();
		 
		 for(Map<String,Object> mapResult : list){
			 beanPagos = new BeanPago();
			 beanPagos.setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
			 beanPagos.setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
			 beanPagos.setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
			 beanPagos.setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
			 beanPagos.setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
			 beanPagos.setFechaCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
			 beanPagos.setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD)));
			 beanPagos.setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
			 beanPagos.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
			 beanPagos.setEstatus(utilerias.getString(mapResult.get(ESTATUS)));
			 beanPagos.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
			 beanPagos.setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
			 beanPagos.setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
			 
			 listaBeanPagos.add(beanPagos);
			 
			 if (zero.equals(beanResListadoPagosDAO.getTotalReg())) {
				 beanResListadoPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			 }
		 }
		return listaBeanPagos;
	}

	/**
	 * Metodo para generar el formato del query
	 * 
	 * @param queryCo Objeto del tipo StringBuilder
	 * @return queryCompleto Objeto del tipo StringBuilder
	 */
	private StringBuilder obtenerFormatoQueryMontos(StringBuilder queryCo) {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
		   			   .append(FROM)
						   		  .append("SELECT COUNT(1) CONT ") 
				   				  .append("FROM TRAN_MENSAJE ") 
				   				.append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
							  .append(queryCo.toString())
							  .append(") CONTADOR, TRAN_MENSAJE ")
							  .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
					  .append(queryCo.toString());
		return queryCompleto;
			}
	
	/**
	 * Metodo para generar el formato del query
	 * 
	 * @param queryCo Objeto del tipo StringBuilder
	 * @return queryCompleto Objeto del tipo StringBuilder
	 */
	private StringBuilder obtenerFormatoQuery(StringBuilder queryCo) {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") 
		   .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") 
		   .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") 
		   .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        
		   .append(FROM) 
				 .append("SELECT TM.*, ROWNUM AS RN ")  
			 	 .append(FROM)
			 	 	   .append("SELECT REFERENCIA, CVE_COLA, CVE_TRANSFE, CVE_MECANISMO,")
			 	 	   .append("CVE_MEDIO_ENT, FCH_CAPTURA, CVE_INTERME_ORD, CVE_INTERME_REC,")
		 	 	   	   .append("IMPORTE_ABONO, ESTATUS, COMENTARIO3, REFERENCIA_MECA, CONTADOR.CONT ") 
		   			   .append(FROM)
						   		  .append("SELECT COUNT(1) CONT ") 
				   				  .append("FROM TRAN_MENSAJE ") 
				   				.append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
							  .append(queryCo.toString())
							  .append(") CONTADOR, TRAN_MENSAJE ")
							  .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
					  .append(queryCo.toString())
					  .append(") TM ")
			   .append(")TM ")
		   .append("WHERE RN BETWEEN ? AND ? ");
		return queryCompleto;
			}

	/**
	 * Metodo para generar las condiciones del query
	 * 
	 * @param field Objeto del tipo String
	 * @param valor Objeto del tipo String
	 * @param queryCo Objeto del tipo StringBuilder
	 */
	private void condicionFields(String field, String valor, StringBuilder queryCo) {
		if ("referencia".equals(field)) {
			queryCo.append(" AND REFERENCIA='").append(valor).append("'").toString();
		} else if ("cola".equals(field)) {
			queryCo.append(" AND CVE_COLA='").append(valor).append("'").toString();
		} else if ("cveTran".equals(field)) {
			queryCo.append(" AND CVE_TRANSFE='").append(valor).append("'").toString();
		} else if ("mecan".equals(field)) {
			queryCo.append(" AND CVE_MECANISMO='").append(valor).append("'").toString();
		} else if ("ord".equals(field)) {
			queryCo.append(" AND CVE_INTERME_ORD='").append(valor).append("'").toString();
		} else if ("rec".equals(field)) {
			queryCo.append(" AND CVE_INTERME_REC='").append(valor).append("'").toString();
		} else if ("imp".equals(field)) {
			queryCo.append(" AND IMPORTE_ABONO=").append(valor).append("").toString();
		}
	}
	
	
	
}
