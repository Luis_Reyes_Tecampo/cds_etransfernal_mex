/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOProgTraspasosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanHorarioTransferencia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la programacion de traspasos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOProgTraspasosImpl extends Architech implements DAOProgTraspasos{
	
	
	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = 690935010577120223L;
	/**
	 * Constante de consulta de datos
	 */
	private static final String QUERY_BUSQUEDA = "SELECT T.*,CONT.CONT  FROM (  "+
												 "	SELECT  HORA,' ' DESCRIPCION, ROWNUM INDICE"+
												 "	FROM TRAN_SPEI_TRAS_PROG "+
												 "	)  T, (select count(1) cont from TRAN_SPEI_TRAS_PROG ) CONT"+
												 "	WHERE  INDICE BETWEEN  ? AND ? ORDER BY 1";
	/**
	* Constante privada del tipo String que almacena el query para validar si existe el registro
	*/
	private static final String QUERY_COUNT  = "SELECT COUNT(*) CUENTA FROM TRAN_SPEI_TRAS_PROG WHERE CVE_MI_INSTITUC = ? AND HORA = ? ";
	
	/**
	* Constante privada del tipo String que almacena el query para realizar el insert
	*/
	private static final String QUERY_AGREGAR  = "INSERT INTO TRAN_SPEI_TRAS_PROG(CVE_MI_INSTITUC, HORA,CVE_USUARIO_ALTA,FCH_ALTA)   " +
			 											" VALUES (?,?,?,TO_DATE(?,'YYYY/MM/DD'))";
	
	/**
	 * Constante privada del tipo String que almacena el query para realizar el delete
	 */
	private static final String  QUERY_ELIMINA ="DELETE TRAN_SPEI_TRAS_PROG "+
												"   WHERE   CVE_MI_INSTITUC =? AND HORA=? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONS_MI_INSTITUCION
	 */
	private static final String QUERY_CONS_MI_INSTITUCION = 
		" SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE trim(CV_PARAMETRO) = ?";
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	 */
	public BeanProgTraspasosDAO consultaHorariosTransferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean){
		  BeanProgTraspasosDAO beanResultado = new BeanProgTraspasosDAO();
		  BeanHorarioTransferencia beanHora = null; 
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<BeanHorarioTransferencia> listBean = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	    	  debug("DAOHorariosSPEIImpl.:.consultaHorariosSPEI***");
	    	 
	         responseDTO = HelperDAO.consultar(QUERY_BUSQUEDA,Arrays.asList(
	        		 new Object[]{ bean.getPaginador().getRegIni(),bean.getPaginador().getRegFin()}),
	        		 HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	        	 listBean = new ArrayList<BeanHorarioTransferencia>();
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	beanHora = new BeanHorarioTransferencia();
	            	beanHora.setHora(utilerias.getString(map.get("HORA")).trim());
	            	beanHora.setDescripcion(utilerias.getString(map.get("DESCRIPCION")).trim());
	            	listBean.add(beanHora);    	
	            	beanResultado.setTotalReg(utilerias.getInteger(map.get("CONT")));
	            }
	         }
	         beanResultado.setListHorariosTr(listBean);
	         beanResultado.setCodError(responseDTO.getCodeError());
	         beanResultado.setMsgError(responseDTO.getMessageError());       
	      
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResultado.setCodError(Errores.EC00011B);
	      }
	      return beanResultado;
		}
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	 */
	public BeanProgTraspasosDAO agregaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean){
		Utilerias utilerias = Utilerias.getUtilerias();
		debug("DAOHorariosSPEIImpl.:.agregaHorarioSPEI");
		String fecha = null;
		BeanResConsMiInstDAO beanResConsMiInstDAO = null;
		beanResConsMiInstDAO = consultaMiInstitucion("ENTIDAD_SPEI_CA",architechSessionBean);
		Calendar cal = Calendar.getInstance(new Locale("es","MX"));
		fecha = utilerias.formateaFecha(cal.getTime(), Utilerias.FORMATO_YYYY_DIAG_MM_DIAG_DD);
		BeanProgTraspasosDAO beanResultado = new BeanProgTraspasosDAO();

		String hora = bean.getHora().replace(":", "");
		ResponseMessageDataBaseDTO responseDTO = null;
		try {   	
			responseDTO = HelperDAO.consultar(QUERY_COUNT, 
			    			Arrays.asList(new Object[]{beanResConsMiInstDAO.getMiInstitucion().trim(),hora}), 
				     HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			    	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			    		List<HashMap<String,Object>> list = responseDTO.getResultQuery();
			    		 int cuenta = utilerias.getInteger(list.get(0).get("CUENTA"));
			    		if(cuenta == 0){
					  	 responseDTO = HelperDAO.insertar(QUERY_AGREGAR,
					  			 Arrays.asList(new Object[]{beanResConsMiInstDAO.getMiInstitucion().trim(),hora,
					  					 architechSessionBean.getUsuario(),fecha}), 
					     HelperDAO.CANAL_GFI_DS, this.getClass().getName());
							  	beanResultado.setCodError(responseDTO.getCodeError());
							  	beanResultado.setMsgError(responseDTO.getMessageError());
			    		}else{
			    			beanResultado.setCodError(Errores.ED00061V);
			    		}
			    	}
			    
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResultado.setCodError(Errores.EC00011B);
		}    
		return beanResultado;
	}
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	 */
	public BeanProgTraspasosDAO eliminaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean){
		BeanResConsMiInstDAO beanResConsMiInstDAO = null;
		beanResConsMiInstDAO = consultaMiInstitucion("ENTIDAD_SPEI_CA",architechSessionBean);
		BeanProgTraspasosDAO beanResultado = new BeanProgTraspasosDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		
			    try {
			    	for (BeanHorarioTransferencia beanHorarioSPEI: bean.getListHorariosTr()) {
			    		String hora = beanHorarioSPEI.getHora().replace(":", "");
						  	 responseDTO = HelperDAO.actualizar(QUERY_ELIMINA, 
						  			 Arrays.asList(new Object[]{
						  					beanResConsMiInstDAO.getMiInstitucion(), hora,
						  			}), 
						            HelperDAO.CANAL_GFI_DS, this.getClass().getName());
								  	beanResultado.setCodError(responseDTO.getCodeError());
								  	beanResultado.setMsgError(responseDTO.getMessageError());	
			    	}
				} catch (ExceptionDataAccess e) {
					showException(e);
					beanResultado.setCodError(Errores.EC00011B);
				}    
		return beanResultado;
		}
	
	/**
     * Metodo que se encarga de realizar la consulta de los movimientos CDA
     * @param clave String con la clave de la institucion
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMiInstDAO Objeto del tipo @see BeanResConsMiInstDAO
     */
    public BeanResConsMiInstDAO consultaMiInstitucion(String clave,ArchitechSessionBean sessionBean) {
    	BeanResConsMiInstDAO beanResConsMiInstDAO =  new BeanResConsMiInstDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<Object> parametros = new ArrayList<Object>();
    	parametros.add(clave);
    	String query = null;
    	try {              	
    		query = QUERY_CONS_MI_INSTITUCION;
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 Map<String,Object> mapResult = list.get(0);
        		 	 beanResConsMiInstDAO.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
        		 }
        	}
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		beanResConsMiInstDAO.setCodError(Errores.EC00011B);
  	}
        return beanResConsMiInstDAO;	
    }
}
