/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOEmisionReporteImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPEICero;
/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanConsMiInstitucion;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanFechaHorario;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanListas;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanSpeiCero;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEICero.UtileriasModuloSPEICero;
import mx.isban.eTransferNal.utilerias.SPEICero.UtileriasSPEICero;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion siguiente:
 *  - Estatus Operacion
 *  - Hora Inicio SPEI CERO 
 *  - Horario cambio fecha operacion
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOEmisionReporteImpl extends Architech implements DAOEmisionReporte{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4070250906943395344L;
	
	/**
	 * variable de utilerias
	 */
	public static final UtileriasSPEICero util = new UtileriasSPEICero();
	/**
	 * @return the util
	 */
	public static UtileriasSPEICero getUtil() {
		return util;
	}
	
	/**
	 * Cons mi institucion.
	 *
	 * @param clave the clave
	 * @param sessionBean the session bean
	 * @param beanReqSpeiCero
	 * @return the bean cons mi institucion
	 */
	@Override
	public BeanConsMiInstitucion consMiInstitucion(String clave, ArchitechSessionBean sessionBean) {
			BeanConsMiInstitucion resMiInstitucion = new BeanConsMiInstitucion();
			
			List<HashMap<String,Object>> list = null;
			
			Utilerias utilerias = Utilerias.getUtilerias();
			
			/** Instancia que guarda la respuesta de la consulta de BD. */
			ResponseMessageDataBaseDTO responseDTO = null;
			
			/** Instancia ArrayList tipo Object que almacena los parametros a enviar a la BD . */
			List<Object> parametros = new ArrayList<Object>();
			parametros.add(clave);
			
			/** Obtenemos la Query a ejecutar . */
			String query = UtileriasModuloSPEICero.QUERY_CONS_MI_INSTITUCION;
			
			try {
				/** Metodo que ejecuta la query, parametros de entrada:
				 *  - Query
				 *  - Parametros de Entrada
				 *  - ID de Canal a ejecutar 
				 *  - Class Name
				 **/
				responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				
				if(responseDTO.getResultQuery()!= null){
    				list = responseDTO.getResultQuery();
    				if(!list.isEmpty()){
    					Map<String,Object> mapResult = list.get(0);
    					resMiInstitucion.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
    				}
    			}
				
			} catch (ExceptionDataAccess e) {
				/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
	        	showException(e);
	        	resMiInstitucion.setCodError(Errores.EC00011B);
	        	resMiInstitucion.setMsgError(Errores.DESC_EC00011B);
			}

		return resMiInstitucion;
	}

	
	/**
	 * Cons horario cambio.
	 *
	 * @param sessionBean the session bean
	 * @param entrada the entrada
	 * @return the bean res emi reporte
	 */
	@Override
	public BeanFechaHorario consHorarioCambio(ArchitechSessionBean sessionBean, BeanConsMiInstitucion entrada){
		BeanFechaHorario horarios = new BeanFechaHorario();
		try {
			BeanFechaHorario fechaOpe = new BeanFechaHorario();
			/**se asignan las fechas de spericero y operacion **/
			horarios = util.fechaSPEI(entrada);
			fechaOpe = util.fechaOperacion(entrada);
			horarios.setHoraFecOperacion(fechaOpe.getHoraFecOperacion());
		}catch(ExceptionDataAccess e){
			/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
        	showException(e);
        	horarios.setCodError(Errores.EC00011B);
        	horarios.setMsgError(Errores.DESC_EC00011B);
		}
		return horarios;
	}
	
	


	/**
	 * Cons estatus opera.
	 *
	 * @param sessionBean the session bean
	 * @param entrada the res estatus opera
	 * @param historico
	 * @return the bean res emi reporte
	 */
	@Override
	public BeanResSpeiCero incializa(ArchitechSessionBean sessionBean, BeanConsMiInstitucion entrada, boolean historico) {
		BeanResSpeiCero beanResSpeiCero= new BeanResSpeiCero();
		/**llama la funcion para los estatus **/
		beanResSpeiCero.setListaEstatusOpera(util.buscaEstatus(sessionBean));
		BeanFechaHorario hra = new BeanFechaHorario();
		
		if(!historico) {
			/**llama la funcion para las horas**/	
			hra =  consHorarioCambio( sessionBean,  entrada) ;	
			beanResSpeiCero.setHoraFecOperacion(hra.getHoraFecOperacion());
			beanResSpeiCero.setHoraIniSpeiCero(hra.getHoraIniSpeiCero());
			
		}
		
		beanResSpeiCero.sethInicio1(obtenerLista(UtileriasModuloSPEICero.HRS));
		beanResSpeiCero.sethInicio2(obtenerLista(UtileriasModuloSPEICero.HRS));
		beanResSpeiCero.sethFin1(obtenerLista(UtileriasModuloSPEICero.MIN));
		beanResSpeiCero.sethFin2(obtenerLista(UtileriasModuloSPEICero.MIN));
		
		beanResSpeiCero.setCodError(Errores.OK00000V);
		return beanResSpeiCero;
	}
	
	/**
	 * funcion que realiza la busqueda de la pantalla de Emision rpt diaria
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public BeanResSpeiCero buscarDatos(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) {
		BeanResSpeiCero beanResSpeiCero= new BeanResSpeiCero();
		try {
			/**se arma el query **/
			String query = util.seleccionarQuery(beanReqSpeiCero);
			/** Instancia ArrayList tipo Object que almacena los parametros a enviar a la BD . */
			List<Object> parametros = new ArrayList<Object>();	
			List<BeanSpeiCero>  beanSpeiCero = new ArrayList<BeanSpeiCero>();
			
			/**Se asigna la institucion **/
			if(beanReqSpeiCero.isTodo()) {
				BeanConsMiInstitucion intAlterno=  consMiInstitucion(UtileriasModuloSPEICero.CTAALTERNA,  sessionBean);
				/**insitito principal **/
				parametros.add(beanReqSpeiCero.getmInstitucion());
				/**insitito alterno **/
				parametros.add(intAlterno.getMiInstitucion());
			}else {
				BeanConsMiInstitucion insti = new BeanConsMiInstitucion();
				if(beanReqSpeiCero.isInstiPrincipal()) {
					insti=  consMiInstitucion(UtileriasModuloSPEICero.CTAPRINCIPAL,  sessionBean);
				}else {
					insti=  consMiInstitucion(UtileriasModuloSPEICero.CTAALTERNA,  sessionBean);
				
				}
				parametros.add(insti.getMiInstitucion());
			}
			/**hora inicio **/
			parametros.add(beanReqSpeiCero.getHoraInicio());
			/**hora fin **/
			parametros.add(beanReqSpeiCero.getHoraFin());
			
			
			if(!beanReqSpeiCero.getEstatusOpera().equals(UtileriasModuloSPEICero.TODO)) {
				parametros.add(beanReqSpeiCero.getEstatusOpera().trim());	
				query =query +UtileriasModuloSPEICero.FILTROESTATUS;
			}else {
				query =query +UtileriasModuloSPEICero.FILTRO_ESTATUS_TODOS;
			}
			
			/** se asignan los valores de la busqueda**/
			beanResSpeiCero = util.setearValroes(beanReqSpeiCero);
			
			/**variable para guardar la hora **/
			BeanFechaHorario hr = new BeanFechaHorario();
			/**variable para guardar el insitituro **/
			BeanConsMiInstitucion instituto = new BeanConsMiInstitucion();
			/** Se busca la institucion**/	
			instituto = consMiInstitucion("ENTIDAD_SPEI", sessionBean);
			/**llama la funcion para las horas**/	
			hr =  consHorarioCambio( sessionBean,  instituto) ;	
			/**Se asigna los valores para las horas **/
			beanResSpeiCero.setHoraFecOperacion(hr.getHoraFecOperacion());
			beanResSpeiCero.setHoraIniSpeiCero(hr.getHoraIniSpeiCero());
			
			
			/**llama la funcion para los estatus **/
			beanResSpeiCero.setListaEstatusOpera(util.buscaEstatus(sessionBean));
			beanResSpeiCero.sethInicio1(obtenerLista(UtileriasModuloSPEICero.HRS));
			beanResSpeiCero.sethInicio2(obtenerLista(UtileriasModuloSPEICero.HRS));
			beanResSpeiCero.sethFin1(obtenerLista(UtileriasModuloSPEICero.MIN));
			beanResSpeiCero.sethFin2(obtenerLista(UtileriasModuloSPEICero.MIN));
		
			int cont = consultaCountBuscar( query,parametros) ;
			
			beanResSpeiCero.setTotalReg(cont);
			if(cont == 0){		        	
				beanResSpeiCero.setListaSpeiCero(beanSpeiCero);
				beanResSpeiCero.setCodError(Errores.ED00011V);
				beanResSpeiCero.setMsgError(Errores.DESC_ED00011V);
				beanResSpeiCero.setTipoError(Errores.TIPO_MSJ_INFO);
				return beanResSpeiCero;
			}		
			
			beanSpeiCero = listaDatosSPEI(beanReqSpeiCero,query,parametros);
			
			if(!beanSpeiCero.isEmpty()){				
				beanResSpeiCero.setListaSpeiCero(beanSpeiCero);
				beanResSpeiCero.setCodError(Errores.OK00000V);
			}else {
			   beanResSpeiCero.setListaSpeiCero(beanSpeiCero);
			   beanResSpeiCero.setCodError(Errores.ED00011V);
			   beanResSpeiCero.setMsgError(Errores.DESC_ED00011V);
			   beanResSpeiCero.setTipoError(Errores.TIPO_MSJ_INFO);
			   return beanResSpeiCero;
			}
		}catch (ExceptionDataAccess e) {
			/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
        	showException(e);
        	beanResSpeiCero.setCodError(Errores.EC00011B);
        	beanResSpeiCero.setMsgError(Errores.DESC_EC00011B);
		}
		return beanResSpeiCero;
	}
	
	/**
	 * funcion que arma la consulta del reporte
	 * @param beanReqSpeiCero
	 * @param sessionBean
	 * @return
	 */
	@Override
	public String consultaRptExportar(BeanReqSpeiCero beanReqSpeiCero,ArchitechSessionBean sessionBean) {
		/**se inicializa el string **/
		String consulta= util.seleccionarQuery(beanReqSpeiCero);
		/**Se asigna la institucion **/
		if(beanReqSpeiCero.isTodo()) {
			BeanConsMiInstitucion intAlterno=  consMiInstitucion(UtileriasModuloSPEICero.CTAALTERNA,  sessionBean);
			consulta = consulta +" AND TSR.CVE_MI_INSTITUC IN ( "+ beanReqSpeiCero.getmInstitucion()+ ","+intAlterno.getMiInstitucion() +")";
		}else {
			consulta = consulta + " AND  TSR.CVE_MI_INSTITUC = " + beanReqSpeiCero.getmInstitucion();
		}
		/**se asigna las fechas a la consulta **/
		consulta = consulta +UtileriasModuloSPEICero.FECHA +">='"+beanReqSpeiCero.getHoraInicio()
				+"'"+UtileriasModuloSPEICero.FECHA+"<'"+beanReqSpeiCero.getHoraFin()+"' ";
		/**se asignan el estatus **/
		if(!beanReqSpeiCero.getEstatusOpera().equals(UtileriasModuloSPEICero.TODO)) {
			consulta = consulta +UtileriasModuloSPEICero.ESTATUS+"('"+ beanReqSpeiCero.getEstatusOpera()+"') " ;
		}else {
			consulta = consulta+" AND trim(TM.ESTATUS) IN( "+ UtileriasModuloSPEICero.QUERY_CONS_ESTATUS_OPERA_TODO+") ";
		}
		/**se regresa la consulta armada**/
		return consulta;
	}
	
	/**
	 * funcion para buscar el total de registros
	 * 
	 * @param beanReqSpeiCero
	 * @param sessionBean
	 * @param query
	 * @return
	 */
	private int consultaCountBuscar(String query,List<Object> parametros)throws ExceptionDataAccess{
		/** Instancia que guarda la respuesta de la consulta de BD. */
		ResponseMessageDataBaseDTO responseDTO = null;	
		List<HashMap<String,Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		int cont =0;
		
		           
		/**Se arma la consulta**/
		String consultaB = UtileriasModuloSPEICero.COUNT_REGISTROS+query+UtileriasModuloSPEICero.COUNT_REGISTROS_FIN;			
		/**sE CONSULTA EL TOTAL DE REGISTROS PARA EL PAGINADOR**/
		responseDTO = HelperDAO.consultar(consultaB, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	          list = responseDTO.getResultQuery();
	          Map<String,Object> map = list.get(0);
	          cont = utilerias.getInteger(map.get("CONT"));
	          
    	  }
		
		
		return cont;	
	}
	
	/**
	 * funcion que trae los datos de la tabla 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @param query
	 * @return
	 */
	private List<BeanSpeiCero> listaDatosSPEI(BeanReqSpeiCero beanReqSpeiCero,String query,List<Object> parametros) throws ExceptionDataAccess{
		List<BeanSpeiCero>  beanSpeiCero = new ArrayList<BeanSpeiCero>();
		String consulta="";
		/** Instancia que guarda la respuesta de la consulta de BD. */
		ResponseMessageDataBaseDTO responseDTO = null;	
		List<HashMap<String,Object>> list = null;
			/**se pasan los valores para el paginador **/
		   if (null != beanReqSpeiCero.getPaginador()) {
			   parametros.add(beanReqSpeiCero.getPaginador().getRegIni());
			   parametros.add(beanReqSpeiCero.getPaginador().getRegFin());
    	   }else{
    		   parametros.add(1);
    		   parametros.add(1);  
    	   }
		   
		   consulta = UtileriasModuloSPEICero.QUERY_CONSULTA_SELECT + query + UtileriasModuloSPEICero.QUERY_CONSULTA_PAG;
			/**se ejecuta el query inicial **/
			responseDTO = HelperDAO.consultar(consulta, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				if(!list.isEmpty()){
					beanSpeiCero = util.separarConsulta(responseDTO, list);
				}
			}
		
		return beanSpeiCero;
				
	}	
	
	
	
	
	/**
	 * Funcion para realizar la exportacion
	 * @param bean
	 * @param sessionBean
	 * @return
	 */
	@Override
	public BeanResBase generarEdoCta(BeanReqSpeiCero bean,
			ArchitechSessionBean sessionBean) {
		/**Se crean los objetos necesarios para ejecutar el query**/
    	BeanResBase res  = new BeanResBase();          
        try{
        	ResponseMessageDataBaseDTO responseDTO = null; 
        	/**Consulta a la base de datos**/
        	responseDTO = HelperDAO.insertar(UtileriasModuloSPEICero.GENERAR_EDO_CTA,
      			   Arrays.asList(new Object[]{
      					   bean.getmInstitucion(),
      					   sessionBean.getUsuario()}),
      			   HelperDAO.CANAL_GFI_DS, this.getClass().getName());        	
        	info("respuesta del insert: "+responseDTO.getMessageError());
        	res.setCodError(Errores.OK00000V);
        	res.setMsgError(Errores.DESC_OK00000V);
        	res.setTipoError(Errores.TIPO_MSJ_INFO);
        }catch (ExceptionDataAccess e) {
        	/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
        	showException(e);
        	res.setCodError(Errores.EC00011B);
        	res.setMsgError(Errores.DESC_EC00011B);
		}
		return res;
	}	

	/**
	 * funcion para obtener las lista de hrs y min
	 * 
	 * @param num
	 * @return
	 */
	private List<BeanListas> obtenerLista(int num){
		List<BeanListas> lis = new ArrayList<BeanListas>();		
		for(int i = 0; i<num; i++) {
			BeanListas b = new BeanListas();
			String dto;
			if(i<10) {
				dto ="0"+String.valueOf(i);
			}else {
				dto=String.valueOf(i);
			}
			b.setValor(String.valueOf(dto));
			b.setClave(String.valueOf(dto));
			lis.add(b);
		}
		
		return lis;
	}
	
}