/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOEmisionReporteImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.exportar;
import java.util.ArrayList;
/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionFiltros;
import mx.isban.eTransferNal.utilerias.SPEICero.UtileriasModuloSPEICero;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion siguiente:
 *  - Estatus Operacion
 *  - Hora Inicio SPEI CERO 
 *  - Horario cambio fecha operacion
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOExportarImpl extends Architech implements DAOExportar{
	
	/**
	 * variable serializable
	 */
	private static final long serialVersionUID = -6756272778631555015L;
	/**
	 * variable response
	 */
	private ResponseMessageDataBaseDTO responseDTO;
	
	/** La constante util. */
	public static final UtileriasRecepcionFiltros utilFiltro =  UtileriasRecepcionFiltros.getInstance();
	/**
	 * Funcion para exportar.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res base
	 */
	@Override
	public BeanResBase exportarTodo(BeanExportarTodo bean,ArchitechSessionBean sessionBean) {
		    	
		/**Se crean los objetos necesarios para ejecutar el query**/
		return exportar(bean,sessionBean,new BeanTranSpeiRecOper());
	}
	
	/**
	 * Funcion para exportar.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res base
	 */
	@Override
	public BeanResBase exportarTodos(BeanExportarTodo bean,ArchitechSessionBean sessionBean,BeanTranSpeiRecOper beanTranSpeiRecOper) {
		    	
			/**Se crean los objetos necesarios para ejecutar el query**/
	    	return exportar(bean,sessionBean,beanTranSpeiRecOper );
	    }
	
	
	private BeanResBase exportar(BeanExportarTodo bean,
			ArchitechSessionBean sessionBean,BeanTranSpeiRecOper beanTranSpeiRecOper) {
		    	
			/**Se crean los objetos necesarios para ejecutar el query**/
	    	BeanResBase res  = new BeanResBase();
	        responseDTO = null;
	        Utilerias utilerias = Utilerias.getUtilerias();
	        
	        String filtroAnd = utilFiltro.getFiltro(beanTranSpeiRecOper, "AND");
	        
	        /**Se genera el nombre del archivo para exportar**/
	        Date fecha = new Date();
	        /**Se llama a la fecha para que le asigna fecha y codigo **/
	        String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
	        String nombreArchivo =  bean.getNombreRpt().concat(fechaActual);
	        nombreArchivo = nombreArchivo.concat(".csv");
	        try{
	        	
	        	//se crea elobjeto para pasar los parametros
				List<Object> parametros = new ArrayList<Object>();
				//se asigna los datos para el parametro
				parametros.add(sessionBean.getUsuario());
				parametros.add(sessionBean.getIdSesion());
				parametros.add(bean.getModulo());
				parametros.add(bean.getSubModulo());
				parametros.add(nombreArchivo);
				parametros.add(bean.getTotalReg());
				parametros.add(bean.getEstatus());
				parametros.add(bean.getConsultaExportar().replaceAll("\\[FILTRO_AND\\]", filtroAnd));
				parametros.add(bean.getColumnasReporte());
				
	        	/**Consulta a la base de datos**/
	        	responseDTO = HelperDAO.insertar(UtileriasModuloSPEICero.EXPORT_TODOS.toString(),parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	        	/**Asigna los codigos de exito **/
	        	res.setCodError(Errores.OK00002V);
	        	res.setMsgError(nombreArchivo);
	        	 res.setTipoError(Errores.TIPO_MSJ_INFO);
	        }catch (ExceptionDataAccess e) {
		    	/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
	        	showException(e);
	        	res.setCodError(Errores.EC00011B);
		        res.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
			return res;
	    }

	/**
	 * @return the responseDTO
	 */
	public ResponseMessageDataBaseDTO getResponseDTO() {
		return responseDTO;
	}

	/**
	 * @param responseDTO the responseDTO to set
	 */
	public void setResponseDTO(ResponseMessageDataBaseDTO responseDTO) {
		this.responseDTO = responseDTO;
	}
	
	
	
}