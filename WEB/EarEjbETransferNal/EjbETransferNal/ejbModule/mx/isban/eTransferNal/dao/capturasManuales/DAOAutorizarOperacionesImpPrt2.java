package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpeDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOAutorizarOperacionesImpPrt2.
 */
public class DAOAutorizarOperacionesImpPrt2 extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -6207020209491124552L;

	/** The constant QUERY_USRS_AUTORIZAN_TEMPLATE. */
	private static final String QUERY_USRS_AUTORIZAN_TEMPLATE = "select DISTINCT(USR_AUTORIZA) USRS_AUTORIZAN from TRAN_CAP_USR_TEMPLATE where TRIM(ID_TEMPLATE) = ?";

	/**
	 * Armar consulta parametrizada.
	 *
	 * @param parametros the parametros
	 * @param req the req
	 * @return the string
	 */
	//Armar consulta parametrizada.
	String armarParametrizacionOpePend(List<Object> parametros, BeanReqAutorizarOpe req){
		StringBuilder builder = new StringBuilder();
		String vacio = "";
		String equalInt=" = ? ";
		
		if(req.getTemplate() != null && (!vacio.equals(req.getTemplate()))){
			//se define si el parametro es el primero
			builder.append("AND TRIM(oper.ID_TEMPLATE)");
			builder.append(equalInt);
			parametros.add(req.getTemplate());
		}
		if(req.getFechaCaptura() != null && (!vacio.equals(req.getFechaCaptura()))){
			//se agrega AND al query
			builder.append("AND TRUNC(oper.FCH_CAPTURA)");
			builder.append(" = TO_DATE(?, 'dd-MM-yyyy')");
			parametros.add(req.getFechaCaptura());
		}
		
		return builder.toString();
	}
	
	/**
	 * Armar parametrizacion ope apart.
	 *
	 * @param parametros the parametros
	 * @param req the req
	 * @param esOpeApartadasPorPagina the es ope apartadas por pagina
	 * @return the string
	 */
	//Armar parametrizacion ope apart.
	String armarParametrizacionOpeApart(List<Object> parametros, BeanReqAutorizarOpe req){
		StringBuilder builder = new StringBuilder();
		String equalInt=" = ?";
		String vacio = "";
		
		if(req.getTemplate() != null && (!vacio.equals(req.getTemplate()))){
			//se concatena AND
			builder.append(" AND TRIM(oper.ID_TEMPLATE)");

			//se concatena Operador = y parametro
			builder.append(equalInt);
			parametros.add(req.getTemplate());
		}
		if(req.getFechaCaptura() != null && (!vacio.equals(req.getFechaCaptura()))){
			//se concatena AND
			builder.append(" AND TRUNC(oper.FCH_CAPTURA)");

			//se concatena parametro para recibir fecha
			builder.append(" = TO_DATE(?, 'dd-MM-yyyy')");
			parametros.add(req.getFechaCaptura());
		}
		
		return builder.toString();
	}
	
	/**
	 * Obtener lista operaciones.
	 *
	 * @param operaciones the operaciones
	 * @param usuario the usuario
	 * @return the list
	 */
	//Obtener lista operaciones.
	List<BeanAutorizarOpe> obtnenerListaOperaciones(List<HashMap<String,Object>> operaciones, String usuario, boolean opePendientes){
		BeanResAutorizarOpeDAO beanResponse=new BeanResAutorizarOpeDAO();
		List<BeanAutorizarOpe> list=new ArrayList<BeanAutorizarOpe>();
  	  	Utilerias utilerias = Utilerias.getUtilerias();
  	  	String usrsAutorizan = "";
		
		for(HashMap<String,Object> map:operaciones){
			BeanAutorizarOpe bean = new BeanAutorizarOpe();
			bean.setFolio(utilerias.getString(map.get("ID_FOLIO")));
			bean.setImporte(utilerias.getString(map.get("IMPORTE_ABONO")));
			bean.setTemplate(utilerias.getString(map.get("ID_TEMPLATE")));
			bean.setUsrAutoriza(utilerias.getString(map.get("USR_AUTORIZA")));
			bean.setUsrCaptura(utilerias.getString(map.get("CVE_USUARIO_CAP")));
			bean.setEstatus(utilerias.getString(map.get("ESTATUS")));
			bean.setFechaCaptura(utilerias.formateaFecha(map.get("FCH_CAPTURA"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
			bean.setUsrApartado(utilerias.getString(map.get("USR_APARTA")));
			bean.setRefTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
			bean.setCodErrorOpe(utilerias.getString(map.get("COD_ERROR")));
			bean.setMsgErrorOpe(utilerias.getString(map.get("MSG_ERROR")));
			bean.setStatusOperacionApartada(utilerias.getString(map.get("OPERACION_APARTADA")));
			bean.setMonto(utilerias.getString(map.get("IMPORTE_CARGO")));
			bean.setCuentaOrdenante(utilerias.getString(map.get("NUM_CUENTA_ORD")));
			bean.setCuentaReceptora(utilerias.getString(map.get("NUM_CUENTA_REC")));

			// si es lista de operaciones pendientes
			if(opePendientes){
				//metodo que consulta los usuarios que tienen permiso para autorizar el template
				usrsAutorizan = consultarUsrAutorizanTemplate(bean.getTemplate(), beanResponse);
				if( Errores.CODE_SUCCESFULLY.equals(beanResponse.getCodError()) ){
					bean.setUsrsAutorizanTemplate(usrsAutorizan);
				}
			}
			
			//si usuarioApartado es igual al usuario logeado
			if(usuario.equals(bean.getUsrApartado())) {
				bean.setApartado(true);
			} else {
				bean.setApartado(false);
			}
			
			list.add(bean);
		}
		return list;
	}
	
	/**
	 * Consultar usr autorizan template.
	 *
	 * @param idTemplate the id template
	 * @param beanResponse the bean response
	 * @return the string
	 */
	//Consultar usr autorizan template.
	private String consultarUsrAutorizanTemplate(String idTemplate, BeanResAutorizarOpeDAO beanResponse){
	    ResponseMessageDataBaseDTO responseDTO = new ResponseMessageDataBaseDTO();
  	  	Utilerias utilerias = Utilerias.getUtilerias();
	    List<HashMap<String,Object>> listResul = null;
		List<Object> params=new ArrayList<Object>();
		StringBuilder usuarios = new StringBuilder();
		try{
			//se envia el idTemplate como parametro para el query
  	  		params.add(idTemplate);
  	  		
	    	//se consultan usuario con permiso para autorizar operaciones
	        responseDTO = HelperDAO.consultar(QUERY_USRS_AUTORIZAN_TEMPLATE, params,
	        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//se obtiene lista de resultados del query
	        listResul = responseDTO.getResultQuery();
	        //se obtiene codigo de error de consulta
	        beanResponse.setCodError(responseDTO.getCodeError());
			
	        //si la lista no esta vacia ni es null
			if( listResul!=null && !listResul.isEmpty()){
				//se recorre la lista de resultados
				for(HashMap<String,Object> map:listResul){
					//se concatenan los usuarios
					usuarios.append( utilerias.getString(map.get("USRS_AUTORIZAN")) + ", " );
				}
			}
  	  	} catch (ExceptionDataAccess e) {
			showException(e);
			beanResponse.setMsgError(Errores.DESC_EC00011B);
			beanResponse.setCodError(Errores.EC00011B);
		}
		return usuarios.toString();
	}
	
	/**
	 * Verificar usr admin.
	 */
	//Verificar usr admin.
	void verificarUsrAdmin(BeanResAutorizarOpeDAO res,
			List<HashMap<String,Object>> list, Utilerias utilerias){
		for(HashMap<String,Object> map:list){
			//se obtiene el resultado del query
			Integer num=utilerias.getInteger(map.get("CONT"));
			//si es mayor a 0
			if(num > 0){
				//es usuario administrador
				res.setUsrAdmin("1");
			} else {
				//no es usuario administrador
				res.setUsrAdmin("0");
			}
		}
	}
}
