/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOConsultaCutSpeiWSServiceImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   14/04/2020 10:06:55 AM Juan Jesus Beltran R. Isban Modificacion
 */
package mx.isban.eTransferNal.dao.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.ws.BeanReqCutSpei;
import mx.isban.eTransferNal.beans.ws.BeanResCutSpeiDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

import org.banxico.dgobc.cutspei.ws.CutSpeiWS;

/**
 * Class DAOConsultaCutSpeiWSServiceImpl.
 * DAO que permite el consumo del WS de Banxico para NominaCUT
 *
 * @author FSW-Vector
 * @since 14/04/2020
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaCutSpeiWSServiceImpl extends Architech implements DAOConsultaCutSpeiWSService {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -7960589409671393635L;
	
	/** Propiedad del tipo ResourceBundle que almacena el valor de res. */
	private static final ResourceBundle RES = ResourceBundle.getBundle("ruta");

	/**
	 * Metodo que sirve para obtener las propiedad del archivo de configuracion.
	 *
	 * @return Map del tipo Map<String, String> con los valores de las propiedades
	 */
	private Map<String, String> load() {
		Map<String, String> map = new HashMap<String, String>();
		String strPuerto = "";
		String strTimeOut = "";
		String strHost = "";
		File f = null;
		String ruta = RES.getString("ruta");
		f = new File(ruta);
		if (!f.exists()) {
			error("No existe el archivo");
			map.put(Constantes.COD_ERROR, Errores.ED00096V);
			return map;
		}
		Properties properties = new Properties();
		InputStream fileIn = null;
		try {
			fileIn = new FileInputStream(f);
			properties.load(fileIn);
			strPuerto = properties.getProperty("PuertoCutSpeiWS");
			map.put("PUERTO", strPuerto);
			strHost = properties.getProperty("HostCutSpeiWS");
			map.put("HOST", strHost);
			strTimeOut = properties.getProperty("TimeCutSpeiWS");
			if (strTimeOut == null || "".equals(strTimeOut.trim())) {
				strTimeOut = "10000";
			}
			map.put("TIME_OUT", strTimeOut);
			map.put(Constantes.COD_ERROR, Errores.OK00000V);
		} catch (FileNotFoundException e) {
			showException(e);
			map.put(Constantes.COD_ERROR, Errores.ED00096V);

		} catch (IOException e) {
			showException(e);
			map.put(Constantes.COD_ERROR, Errores.ED00097V);
		} finally {
			try {
				if (fileIn != null) {
					fileIn.close();
				}
			} catch (IOException e) {
				showException(e);
			}
		}
		return map;
	}

	/**
	 * Obtener el objeto: url.
	 *
	 * @param strUrl String con la url
	 * @return URL Objeto que abstrae una URL
	 * @throws MalformedURLException Exception de url invalido
	 */
	private URL getURL(String strUrl) throws MalformedURLException {
		Map<String, String> map = load();
		final int timeOut = Integer.parseInt(map.get("TIME_OUT").trim());
		info("TimeOut CutSpeiWS:" + timeOut);
		URL url = new URL(null, strUrl, new URLStreamHandler() { // Anonymous (inline) class
			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				URL clone_url = new URL(url.toString());
				HttpURLConnection clone_urlconnection = (HttpURLConnection) clone_url.openConnection();
				// TimeOut settings
				clone_urlconnection.setConnectTimeout(timeOut);
				clone_urlconnection.setReadTimeout(timeOut);
				return (clone_urlconnection);
			}

			@Override
			protected URLConnection openConnection(URL url, Proxy p) throws IOException {
				URL clone_url = new URL(url.toString());
				HttpURLConnection clone_urlconnection = (HttpURLConnection) clone_url.openConnection();
				// TimeOut settings
				clone_urlconnection.setConnectTimeout(timeOut);
				clone_urlconnection.setReadTimeout(timeOut);

				return (clone_urlconnection);
			}
		});

		return url;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.ws.DAOConsultaCutSpeiWSService#obtSalt(mx.isban.eTransferNal.beans.ws.BeanReqCutSpei, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResCutSpeiDAO obtSalt(BeanReqCutSpei beanReqCutSpei, ArchitechSessionBean sessionBean) {
		BeanResCutSpeiDAO beanResCutSpeiDAO = new BeanResCutSpeiDAO();
		Map<String, String> map = load();
		String strCodError = "";
		String strUrl = "";
		URL url = null;
		strCodError = map.get(Constantes.COD_ERROR);
		if (!Errores.OK00000V.equals(strCodError)) {
			beanResCutSpeiDAO.setCodError(strCodError);
			beanResCutSpeiDAO.setCodError(Errores.TIPO_MSJ_ALERT);
			return beanResCutSpeiDAO;
		}
		String strPuerto = map.get("PUERTO");
		String strHost = map.get("HOST");
		;
		try {
			strUrl = "http://" + strHost + ":" + strPuerto + "/CUT-SPEI-war/cutSpeiWSService?WSDL";
			url = getURL(strUrl);
			QName qname = new QName("http://ws.cutspei.dgobc.banxico.org/", "cutSpeiWSService");
			Service service = Service.create(url, qname);
			CutSpeiWS calc = (CutSpeiWS) service.getPort(CutSpeiWS.class);
			String salt = calc.obtSalt(beanReqCutSpei.getUsuario());
			beanResCutSpeiDAO.setSalt(salt);
			beanResCutSpeiDAO.setCodError(Errores.OK00000V);
			beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_INFO);
		} catch (MalformedURLException e) {
			info("Error en la url: MalformedURLException");
			beanResCutSpeiDAO.setCodError(Errores.ED00099V);
			beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_ALERT);
			showException(e);
		}
		return beanResCutSpeiDAO;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.ws.DAOConsultaCutSpeiWSService#obtMontoPagosNominaTesofe(mx.isban.eTransferNal.beans.ws.BeanReqCutSpei, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResCutSpeiDAO obtMontoPagosNominaTesofe(BeanReqCutSpei beanReqCutSpei,
			ArchitechSessionBean sessionBean) {
		BeanResCutSpeiDAO beanResCutSpeiDAO = new BeanResCutSpeiDAO();
		Map<String, String> map = load();
		String strCodError = "";
		String strUrl = "";
		URL url = null;
		strCodError = map.get(Constantes.COD_ERROR);
		if (!Errores.OK00000V.equals(strCodError)) {
			beanResCutSpeiDAO.setCodError(strCodError);
			return beanResCutSpeiDAO;
		}
		String strPuerto = map.get("PUERTO");
		String strHost = map.get("HOST");
		try {
			strUrl = "http://" + strHost + ":" + strPuerto + "/CUT-SPEI-war/cutSpeiWSService?WSDL";
			url = getURL(strUrl);
			QName qname = new QName("http://ws.cutspei.dgobc.banxico.org/", "cutSpeiWSService");
			Service service = Service.create(url, qname);
			CutSpeiWS calc = (CutSpeiWS) service.getPort(CutSpeiWS.class);
			List<Object> list = calc.obtMontoPagosNominaTesofeStr(beanReqCutSpei.getUsuario(),
					beanReqCutSpei.getContrasena(), Integer.parseInt(beanReqCutSpei.getFchConsultar()));
			if (list != null && !list.isEmpty()) {
				beanResCutSpeiDAO.setCantidad(new Integer(list.get(0).toString()));
				beanResCutSpeiDAO.setMonto(new BigDecimal(list.get(1).toString().replaceAll(",", "")));
				beanResCutSpeiDAO.setCodError(Errores.OK00000V);
				beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_INFO);
			} else {
				beanResCutSpeiDAO.setMonto(new BigDecimal("0.00"));
				beanResCutSpeiDAO.setCodError(Errores.OK00000V);
			}
		} catch (MalformedURLException e) {
			info("Error en la url: MalformedURLException");
			beanResCutSpeiDAO.setCodError(Errores.ED00099V);
			beanResCutSpeiDAO.setTipoError(Errores.TIPO_MSJ_ALERT);
			showException(e);
		}
		return beanResCutSpeiDAO;
	}

}
