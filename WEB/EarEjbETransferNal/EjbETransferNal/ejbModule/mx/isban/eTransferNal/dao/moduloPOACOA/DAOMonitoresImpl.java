/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMonitoresImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:39:24 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesPOACOAMonitor;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConsultaMonitor;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasMonitor;

/**
 * Class DAOMonitoresImpl.
 *
 * Clase tipo DAO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * de accceso a los datos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 6/08/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitoresImpl extends Architech implements DAOMonitores{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6530457230367736439L;

	/** La constante utils. */
	private static final UtileriasMonitor utils = UtileriasMonitor.getUtils();
	
	/**
	 * Obtener monitor.
	 *
	 * Obtener la informacion del DAO
	 *  
	 * @param modulo El objeto: modulo
	 * @param reqSession El objeto: req session
	 * @param session El objeto: session
	 * @return Objeto bean monitor
	 */
	@Override
	public BeanMonitor obtenerMonitor(BeanModulo modulo, BeanSessionSPID reqSession, ArchitechSessionBean session) {
		//Se declaran BeanMonitor
		BeanMonitor response = null;
		//Se crea bean BeanResBase
		BeanResBase error = null;
		//se crea list
		List<HashMap<String,Object>> list = null;
		//Se crea objeto ResponseMessageDataBaseDTO
		ResponseMessageDataBaseDTO responseDTO = null;
		//Se obtiene query
		String query = ConsultaMonitor.CONSULTA_MONITOR;
		try {
			//Se completa query
			query = utils.completaQuery(query, modulo);
			// Se ejecuta consulta
			responseDTO = HelperDAO.consultar(query, new ArrayList<Object>(getParams()), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//Se valida resultado de consulta
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				//se obtiene el resultado del query
				list = responseDTO.getResultQuery();
				response = utils.getValores(list);
				error = new BeanResBase();
				//seteando codigo de errores
				error.setCodError(Errores.OK00000V);
				error.setMsgError(Errores.DESC_OK00000V);
				response.setError(error);
			}
			
		} catch (ExceptionDataAccess e) {
			//Ocurrio un error
			showException(e);
			response = new BeanMonitor();
			error = new BeanResBase();
			//srteando codigo de error en excepcion
			error.setCodError(Errores.EC00011B);
			error.setMsgError(Errores.DESC_EC00011B);
			//Seteando response en excepcion
			response.setError(error);
		}
		//return response
		return response;
	}
	
	/**
	 * Obtener el objeto: params.
	 *
	 * @return El objeto: params
	 */
	private List<String> getParams () {
		//metodo que retorna los parametros 
		//return arraylist
		return Arrays.asList(
				ConstantesPOACOAMonitor.CONS_PA,
				ConstantesPOACOAMonitor.CONS_LQ,
				ConstantesPOACOAMonitor.CONS_RE,
				ConstantesPOACOAMonitor.CONS_LQ,
				ConstantesPOACOAMonitor.CONS_TR,
				ConstantesPOACOAMonitor.CONS_LQ,
				ConstantesPOACOAMonitor.CONS_RE,
				ConstantesPOACOAMonitor.CONS_LQ,
				ConstantesPOACOAMonitor.CONS_PV,
				ConstantesPOACOAMonitor.CONS_LI,
				ConstantesPOACOAMonitor.CONS_EN,
				ConstantesPOACOAMonitor.CONS_PR,
				ConstantesPOACOAMonitor.CONS_CO,
				ConstantesPOACOAMonitor.CONS_CO,
				ConstantesPOACOAMonitor.CONS_CO,
				ConstantesPOACOAMonitor.CONS_PV,
				ConstantesPOACOAMonitor.CONS_LI,
				ConstantesPOACOAMonitor.CONS_EN,
				ConstantesPOACOAMonitor.CONS_PR,
				ConstantesPOACOAMonitor.CONS_PV,
				ConstantesPOACOAMonitor.CONS_LI,
				ConstantesPOACOAMonitor.CONS_EN,
				ConstantesPOACOAMonitor.CONS_PR);
	}
}
