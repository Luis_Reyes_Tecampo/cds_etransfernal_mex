/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMantenimientoTemplatesExternalImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Apr 8 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayoutTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAltaEditTemplateDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.UtileriasDatosMtoTemplate;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * 
 * Implementacion DAO que actua como firma de la capa de persistencia a servicios externos
 *
 */
//Implementacion DAO que actua como firma de la capa de persistencia a servicios externos
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMantenimientoTemplatesExternalImpl extends DAOBaseCapturasManualesImpl implements DAOMantenimientoTemplatesExt {

	//Propiedad del tipo long que almacena el valor de serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 1882605628359389915L;

	//Propiedad del tipo string que almacena la consulta: Obtener lista de layouts
	/** Propiedad del tipo string que almacena la consulta: Obtener lista de layouts*/
	private static final String QUERY_GET_LAYOUTS = "select ID_LAYOUT from TRAN_CAP_LAYOUT GROUP BY ID_LAYOUT";

	//Propiedad del tipo string que almacena la consulta: Obtener lista de usuarios candidatos a autorizacion de layout
	/** Propiedad del tipo string que almacena la consulta: Obtener lista de usuarios candidatos a autorizacion de layout*/
	private static final String QUERY_GET_USERS = "select CVE_USUARIO from TR_SEG_CAT_USUARIO where estatus_usr = 1 order by CVE_USUARIO";	

	//Propiedad del tipo string que almacena la consulta: Obtener campos de un layout
	/** Propiedad del tipo string que almacena la consulta: Obtener campos de un layout*/
	private static final String QUERY_GET_FIELDS_LAYOUT = "select ID_LAYOUT, ID_CAMPO, CAMPO, DESCRIPCION, OBLIGATORIO, CONSTANTE, TIPO, LONGITUD, QUERY "
			+ " from TRAN_CAP_LAYOUT WHERE TRIM(ID_LAYOUT) = TRIM(?) ";

	//Propiedad del tipo string que almacena la consulta: Obtener operaciones de un template
	/** Propiedad del tipo string que almacena la consulta: Obtener operaciones de un template*/
	private static final String QUERY_TEMPLATE_OPERACIONES = "Select  COUNT(1) AS TOTAL  FROM TRAN_CAP_OPER where TRIM(ID_TEMPLATE) = TRIM(?)";

	//Propiedad del tipo string que almacena la consulta: Numero de operaciones asigadas al usuario en el template
	/** Propiedad del tipo string que almacena la consulta: Numero de operaciones asigadas al usuario en el template*/
	private static final String QUERY_GET_TOTAL_USEROPERTEMP = "Select COUNT(1) AS TOTAL FROM TRAN_CAP_OPER WHERE TRIM(ID_TEMPLATE) = TRIM(?) AND TRIM(USR_AUTORIZA) = TRIM(?)";

	//Propiedad del tipo string que almacena la consulta: Actualizar los campos del template
	/** Propiedad del tipo string que almacena la consulta: Actualizar los campos del template*/
	private static final String QUERY_UPDATE_CAMPOTEMPLATE= "UPDATE TRAN_CAP_DET_TEMPLATE SET OBLIGATORIO =?, CONSTANTE=? WHERE TRIM(ID_TEMPLATE) = TRIM(?) AND TRIM(ID_CAMPO) = TRIM(?)" ;

	//Propiedad del tipo string que almacena la consulta: Obtener datos del template
	/** Propiedad del tipo string que almacena la consulta: Obtener datos del template*/
	private static final String QUERY_GET_TEMPLATE = "Select ID_TEMPLATE, ID_LAYOUT, ESTATUS, USR_ALTA, USR_AUTORIZA, "
			+ "FEC_CAPTURA, FEC_MODIFICA FROM TRAN_CAP_TEMPLATE Where TRIM(ID_TEMPLATE) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Obtener informacion de los usuarios de un template
	/** Propiedad del tipo string que almacena la consulta: Obtener informacion de los usuarios de un template*/
	private static final String QUERY_GET_USERS_BYTEMPLATE = "Select ID_TEMPLATE, USR_CAPTURA, USR_AUTORIZA FROM TRAN_CAP_USR_TEMPLATE"
			+ " Where TRIM(ID_TEMPLATE) = TRIM(?)";
	
	//Propiedad del tipo string que almacena la consulta: Obtener los campos de un template
	/** Propiedad del tipo string que almacena la consulta: Obtener los campos de un template*/
	private static final String QUERY_GET_CAMPOS_BYTEMPLATE =  "Select TEM.ID_CAMPO, TEM.ID_TEMPLATE, TEM.CAMPO, TEM.OBLIGATORIO, TEM.CONSTANTE,"
			+ " LAY.ID_LAYOUT, LAY.TIPO, LAY.LONGITUD, LAY.QUERY, LAY.DESCRIPCION "
			+ " FROM TRAN_CAP_DET_TEMPLATE TEM Left Join TRAN_CAP_LAYOUT LAY On TEM.ID_CAMPO = LAY.ID_CAMPO"
			+ " Where TRIM(TEM.ID_TEMPLATE) = TRIM(?) AND TRIM(LAY.ID_LAYOUT) = TRIM(?)";

	//Constante para referenciar el total de registros
	/** Constante para referenciar el total de registros */
	private static final String TOTAL = "TOTAL";

	//Constante para referenciar el ID_LAYOUT
	/** Constante para referenciar el ID_LAYOUT */
	private static final String ID_LAYOUT="ID_LAYOUT";

	//Metodo para obtener los layouts
	@Override
	public BeanResAltaEditTemplateDAO obtenerLayouts(
			BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session) {

		//Creacion de objetos para consulta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = null;
		Utilerias utilerias = Utilerias.getUtilerias();

		//Se crean los objetos de respuesta
		BeanResAltaEditTemplateDAO beanResDAO= new BeanResAltaEditTemplateDAO();
		List<String> listaLayouts = new ArrayList<String>();		

		try{
			//Se realiza la consulta a la base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_LAYOUTS, Arrays.asList(
					new Object[]{}
					),HelperDAO.CANAL_GFI_DS, this.getClass().getName());	

			//Se valida que resultado no sea nulo o vacio
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				listMapRes = new ArrayList<HashMap<String,Object>>();
				listMapRes = responseDTO.getResultQuery();

				//Se agrega a la lista de layouts los id
				for(HashMap<String,Object> map:listMapRes){
					listaLayouts.add(utilerias.getString(map.get(ID_LAYOUT)));					 
				}
			}

			//Se setea la lista en el bean
			beanResDAO.setListaLayouts(listaLayouts); 

			//Se setea el cod y msg de error de la consulta
			beanResDAO.setCodError(responseDTO.getCodeError());				 
			beanResDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e){
			//Si hay un error se setea el codigo EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResDAO;	
	}

	//Metodo para obtener los usuarios
	@Override
	public BeanResAltaEditTemplateDAO obtenerUsuarios(
			BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session) {

		//Creacion de objetos para consulta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = null;
		Utilerias utilerias = Utilerias.getUtilerias();

		//Se crean los objetos de respuesta
		BeanResAltaEditTemplateDAO beanResDAO = new BeanResAltaEditTemplateDAO();
		List<String> listaUsuarios = new ArrayList<String>();

		try {
			//Se realiza la consulta a la base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_USERS,
					Arrays.asList(new Object[] {}), HelperDAO.CANAL_TRANSFER_PLUS,
					this.getClass().getName());

			//Se valida que resultado no sea nulo o vacio
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listMapRes = new ArrayList<HashMap<String, Object>>();
				listMapRes = responseDTO.getResultQuery();

				//Se agrega a la lista de usuarios las claves
				for (HashMap<String, Object> map : listMapRes) {
					listaUsuarios.add(utilerias.getString(map.get("CVE_USUARIO")));
				}
			}

			//Se setea la lista en el bean
			beanResDAO.setListaUsuarios(listaUsuarios);

			//Se setea el cod y msg de error de la consulta
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			//Si hay un error se setea el codigo EC00011B
			showException(e);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
			beanResDAO.setCodError(Errores.EC00011B);
		}

		return beanResDAO;
	}

	//Metodo para obtener los campos del layout
	@Override
	public BeanResAltaEditTemplateDAO obtenerCamposLayout(
			BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session) {

		//Creacion de objetos para consulta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = new ArrayList<HashMap<String, Object>>();
		Utilerias utilerias = Utilerias.getUtilerias();

		//Se crean los objetos de respuesta
		BeanResAltaEditTemplateDAO beanResDAO = new BeanResAltaEditTemplateDAO();
		List<BeanCampoLayout> listaLayoutCampos = new ArrayList<BeanCampoLayout>();
		List<Object> listParam =new ArrayList<Object>();

		//Obtener el layout_id seleccionado
		listParam.add(beanReq.getSelectedLayout());

		try {
			//Se realiza la consulta a la base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_FIELDS_LAYOUT, 
					listParam,HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Se valida que resultado no sea nulo o vacio
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listMapRes = responseDTO.getResultQuery();
			}

			//Se setea el cod y msg de error de la consulta
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			//Si hay un error se setea el codigo EC00011B
			showException(e);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
			beanResDAO.setCodError(Errores.EC00011B);
		}
		
		//Se van agregando los bean a la lista
		for (HashMap<String, Object> map : listMapRes) {
			BeanCampoLayout temp = new BeanCampoLayout();					
			temp.setIdLayout(utilerias.getString(map.get(ID_LAYOUT)));					
			temp.setIdCampo(utilerias.getInteger(map.get("ID_CAMPO")));
			temp.setCampo(utilerias.getString(map.get("CAMPO")));
			temp.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
			temp.setObligatorio(utilerias.getString(map.get("OBLIGATORIO")));
			temp.setConstante(utilerias.getString(map.get("CONSTANTE")));
			temp.setTipo(utilerias.getString(map.get("TIPO")));
			temp.setLongitud(utilerias.getInteger(map.get("LONGITUD")));
			
			//Se obtienen los datos que van en el combo
			String query = utilerias.getString(map.get("QUERY"));
			if(null != query && !StringUtils.EMPTY.equals(query)){
				temp.setComboValores(getComboValores(query,session));
			}
			listaLayoutCampos.add(temp);
		}
		
		//Se setea la lista en el bean
		beanResDAO.setListaCamposMaster(listaLayoutCampos);

		return beanResDAO;
	}

	//Metodo para verificar las operaciones
	@Override
	public BeanResBase verificarOperaciones(BeanTemplate template,
			ArchitechSessionBean session) {

		//Creacion de objetos para consulta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = null;

		//Se crean los objetos de respuesta
		BeanResBase beanResDAO = new BeanResBase();
		Utilerias utilerias = Utilerias.getUtilerias();
		int numResults=0;

		try{
			//Se realiza la consulta a la base de datos
			//Verificar que el template no tenga operaciones asociadas.
			responseDTO = HelperDAO.consultar(QUERY_TEMPLATE_OPERACIONES,
					Arrays.asList(new Object[] { template.getIdTemplate()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			listMapRes = responseDTO.getResultQuery();
			HashMap<String, Object> first = listMapRes.get(0);
			numResults = utilerias.getInteger(first.get(TOTAL));

			//Se valida si existen layouts disponibles
			if(numResults>0){
				beanResDAO.setCodError(Errores.CMMT003V);
				return beanResDAO;
			}	

			//Se setea el cod y msg de error de la consulta
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
			//Si hay un error se setea el codigo EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResDAO;
	}

	//Metodo para verificar la existencia de operaciones con relacion del usuario al template
	@Override
	public BeanResBase verificarExistenciaOperacionesUsuarioTemplate(
			BeanUsuarioAutorizaTemplate beanUsuario,
			ArchitechSessionBean session) {

		//Creacion de objetos para consulta
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> listMapRes = null;

		//Se crean los objetos de respuesta
		BeanResBase beanResDAO = new BeanResBase();

		//Se crean objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();
		int numResults=0;

		try{
			//Se realiza la consulta a la base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_TOTAL_USEROPERTEMP,
					Arrays.asList(new Object[] { beanUsuario.getIdTemplate(), beanUsuario.getUsrAutoriza()}),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Se obtiene el num de registros
			listMapRes = responseDTO.getResultQuery();
			HashMap<String, Object> first = listMapRes.get(0);
			numResults = utilerias.getInteger(first.get(TOTAL));

			//El usuario ya existe en la tabla de operaciones
			if(numResults>0){
				beanResDAO.setCodError(Errores.CMMT006V);
				return beanResDAO;
			}	

			//Se setea el cod y msg de error de la consulta
			beanResDAO.setCodError(responseDTO.getCodeError());
			beanResDAO.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
			//Si hay un error se setea el codigo EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResDAO;
	}

	//Metodo para actualizar los campos
	@Override
	public BeanResBase actualizarCampos(BeanReqAltaEditTemplate beanReq,
			ArchitechSessionBean session) {

		//Se crean objetos de soporte
		ResponseMessageDataBaseDTO responseDTO = null;

		//Se crea objeto de respuesta
		BeanResBase beanResDAO = new BeanResBase();

		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		utileriasDatos.ajustarDatosCampos(beanReq);

		try{
			//Actualizar los registros en. TRAN_CAP_DET_TEMPLATE
			for(BeanCampoLayoutTemplate campo: beanReq.getTemplate().getCampos()){
				responseDTO = HelperDAO.actualizar(QUERY_UPDATE_CAMPOTEMPLATE, 
						Arrays.asList(new Object[]{
								campo.getObligatorio(),
								campo.getConstante(),
								campo.getIdTemplate(),
								campo.getIdCampo()
						}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());

				//Se setean cod y mensaje de error
				beanResDAO.setCodError(responseDTO.getCodeError());
				beanResDAO.setMsgError(responseDTO.getMessageError());
			}	

		}catch(ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResDAO;
	}


	//Metodo para obtener el template
	@Override
	public BeanResAltaEditTemplateDAO obtenerTemplate(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session) {

		//Se crean objetos de sopoerte
		ResponseMessageDataBaseDTO responseDTO = null;

		//Se crean objetos de respuesta
		List<BeanUsuarioAutorizaTemplate> listaAutorizan = new ArrayList<BeanUsuarioAutorizaTemplate>();
		List<BeanCampoLayout> listaLayoutCampos = new ArrayList<BeanCampoLayout>();
		List<BeanCampoLayoutTemplate> listaLayoutCamposTemplate = new ArrayList<BeanCampoLayoutTemplate>();
		List<String> listaUsuarios = new ArrayList<String>();
		String idTemplate ="", idLayout="";

		//Se crean objetos de respuesta
		BeanResAltaEditTemplateDAO beanResDAO= new BeanResAltaEditTemplateDAO();				
		BeanResAltaEditTemplateDAO temp= new BeanResAltaEditTemplateDAO();
		BeanResBase operacionesDAO = new BeanResBase();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();

		idTemplate = beanReq.getTemplate().getIdTemplate();

		try{
			//Se hace la consulta a base de datos
			responseDTO = HelperDAO.consultar(QUERY_GET_TEMPLATE, Arrays.asList(
					new Object[]{idTemplate}
					),HelperDAO.CANAL_GFI_DS, this.getClass().getName());	

			//A. Recuperar la informacion del template
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				utileriasDatos.extraerDatosTemplate(responseDTO, beanResDAO);
			}

			//Establecer los mensajes de la BD
			beanResDAO.setCodError(responseDTO.getCodeError());				 
			beanResDAO.setMsgError(responseDTO.getMessageError());

			//B. Verificar si el template tiene operaciones asociadas
			operacionesDAO = verificarOperaciones(beanResDAO.getTemplate(), session);			
			if(Errores.CMMT003V.equals(operacionesDAO.getCodError())){
				beanResDAO.getTemplate().setOperacionesAsociadas(true);
			}

			//C. Recuperar la lista de layouts. Debido a que el componente
			//estara disabled, solo anexamos el actual sin consultar BD
			idLayout = beanResDAO.getTemplate().getIdLayout();
			beanResDAO.setListaLayouts(Arrays.asList(idLayout));


			//D. Recuperar la informacion de los usuarios: disponibles. Usuarios pueden estar vacios 
			temp = obtenerUsuarios(beanReq, session);			
			listaUsuarios = temp.getListaUsuarios();

			//E. Obtener los usuarios autorizados del template
			responseDTO = HelperDAO.consultar(QUERY_GET_USERS_BYTEMPLATE, Arrays.asList(
					new Object[]{idTemplate}
					),HelperDAO.CANAL_GFI_DS, this.getClass().getName());	

			//Los usuarios de un template pueden estar vacios
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				utileriasDatos.extraerDatosUsuarios(responseDTO, listaAutorizan);
			}

			//Procesar las listas para eliminar duplicidad en ambas.
			utileriasDatos.removerAutorizadosDesDisponibles(listaUsuarios, listaAutorizan);
			beanResDAO.setListaUsuarios(listaUsuarios);
			beanResDAO.getTemplate().setUsuariosAutorizan(listaAutorizan);

			//Obtener los campos del template y moverlos a MasterCamposLayout
			responseDTO = HelperDAO.consultar(QUERY_GET_CAMPOS_BYTEMPLATE, Arrays.asList(
					new Object[]{idTemplate, idLayout }
					),HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//La lista de campos puede ir vacia y no representa un error
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				utileriasDatos.extraerDatosCamposTemplate(responseDTO,
						listaLayoutCampos, listaLayoutCamposTemplate,
						idTemplate,this, session);
			}

			//Se setean los resultados en el bean de respuesta
			beanResDAO.getTemplate().setCampos(listaLayoutCamposTemplate);
			beanResDAO.setListaCamposMaster(listaLayoutCampos);	

		} catch (ExceptionDataAccess e){
			//Si existe una excepcion se setea el codigo de error EC00011B
			showException(e);
			beanResDAO.setCodError(Errores.EC00011B);
			beanResDAO.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResDAO;
	}
}
