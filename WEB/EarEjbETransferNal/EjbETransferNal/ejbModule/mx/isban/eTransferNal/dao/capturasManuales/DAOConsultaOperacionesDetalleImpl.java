package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase que se encarga de llevar el control de acceso a datos para el detalle de una operación en el módulo Consulta Operaciones
 */
//Clase que se encarga de llevar el control de acceso a datos para el detalle de una operación en el módulo Consulta Operaciones
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaOperacionesDetalleImpl extends Architech implements DAOConsultaOperacionesDetalle{

	//Propiedad del serialVersionUID
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5826236310289774028L;

	//Query para consultar el template a partir de un folio
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_CONSUL_TEMPLATE = "select ID_TEMPLATE, ID_LAYOUT, IMPORTE_CARGO, NUM_CUENTA_ORD, NUM_CUENTA_REC from TRAN_CAP_OPER where TRIM(ID_FOLIO) = ?";

	//Query para saber si una operacion esta apartada
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_CONSUL_APARTADO = "select USR_APARTA from TRAN_CAP_OPER where TRIM(USR_APARTA) = ? AND TRIM(ID_FOLIO) = ?";
	
	//Query que obtiene el usuario que puede modificar el folio
	/** The Constant QUERY_APARTAR_OPERACION. */
	private static final String QUERY_CONSUL_USR_MODIFICA = "select ESTATUS from TRAN_CAP_OPER where TRIM(USR_MODIFICA) = ? AND TRIM(ID_FOLIO) = ?";
	
	//Query que actualiza las operaciones
	/** La constante QUERY_INSERT. */
	private static final String QUERY_UPDATE_OPERACION = " UPDATE TRAN_CAP_OPER SET ESTATUS = 'PA', "
			+ " COD_POSTAL_ORD = ?,COMENTARIO1 = ?,COMENTARIO2 = ?,COMENTARIO3 = ?,COMENTARIO4 = ?,CONCEPTO_PAGO = ?,CONCEPTO_PAGO2 = ?,CVE_DIVISA_ORD = ?,"
			+ " CVE_DIVISA_REC = ?,CVE_EMPRESA = ?,CVE_INTERME_ORD = ?,CVE_INTERME_REC = ?,CVE_MEDIO_ENT = ?,CVE_OPERACION = ?,CVE_PTO_VTA = ?,CVE_PTO_VTA_ORD = ?,"
			+ " CVE_PTO_VTA_REC = ?,CVE_RASTREO = ?,CVE_TRANSFE = ?,CVE_USUARIO_CAP = ?,CVE_USUARIO_SOL = ?,DIRECCION_IP = ?,DOMICILIO_ORD = ?,FCH_CONSTIT_ORD = ?,"
			+ " FCH_INSTRUC_PAGO = ?,FOLIO_PAGO = ?,FOLIO_PAQUETE = ?,HORA_INSTRUC_PAGO = ?,IMPORTE_ABONO = ?,IMPORTE_CARGO = ?,MOTIVO_DEVOL = ?,NOMBRE_ORD = ?,"
			+ " NOMBRE_REC = ?,NOMBRE_REC2 = ?,NUM_CUENTA_ORD = ?,NUM_CUENTA_REC = ?,REFERENCIA_MED = ?,RFC_ORD = ?,RFC_REC = ?,RFC_REC2 = ?,"
			+ " TIPO_CAMBIO = ?,TIPO_CUENTA_ORD = ?,TIPO_CUENTA_REC = ?,TIPO_CUENTA_REC2 = ?,CIUDAD_BCO_REC = ?,CVE_ABA = ?,CVE_PAIS_BCO_REC = ?,FORMA_LIQ = ?," 
			+ " IMPORTE_DLS = ?,NOMBRE_BCO_REC = ?,NUM_CUENTA_REC2 = ?,PLAZA_BANXICO = ?,SUCURSAL_BCO_REC = ?,"
			+ " BUC_CLIENTE = ?, IMPORTE_IVA = ?, DIR_IP_GEN_TRAN = ?,"
			+ " DIR_IP_FIRMA = ?, UETR_SWIFT = ?, CAMPO_SWIFT1 = ?, CAMPO_SWIFT2 = ?, REFE_ADICIONAL1 = ?, REFE_NUMERICA = ?"
			+ " WHERE TRIM(ID_FOLIO) = ?";
	
	
	//Metodo que se encarga de obtener el template a partir de un folio
	@Override
	public BeanResConsOperaciones obtenerTemplate(BeanReqConsOperaciones beanReqConsOperaciones,ArchitechSessionBean architechSessionBean){
		final BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
	
		//Se crean los objetos necesarios para ejecutar el query
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;

		List<Object> parametros = new ArrayList<Object>();
		try{
			//Consulta a la base de datos
			String query=QUERY_CONSUL_TEMPLATE;
			parametros.add(beanReqConsOperaciones.getFolio());
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
			}
			beanResConsOperaciones.setCodError(responseDTO.getCodeError());
			beanResConsOperaciones.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
			beanResConsOperaciones.setCodError(Errores.EC00011B);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}

		//Se valida que la lista no sea nula y no este vacía
		if(list != null && !list.isEmpty()){
			HashMap<String,Object> map = list.get(0);
			//Se setea por cada map el template y layout, solo debería tener un registro la respuesta
			String template=utilerias.getString(map.get("ID_TEMPLATE"));
			String layout=utilerias.getString(map.get("ID_LAYOUT"));
			beanResConsOperaciones.setTemplate(template);
			beanResConsOperaciones.setLayoutID(layout);
			
			//Pista
			beanResConsOperaciones.setMonto(utilerias.getString(map.get("IMPORTE_CARGO")));
			beanResConsOperaciones.setCuentaOrdenante(utilerias.getString(map.get("NUM_CUENTA_ORD")));
			beanResConsOperaciones.setCuentaReceptora(utilerias.getString(map.get("NUM_CUENTA_REC")));
		}
		return beanResConsOperaciones;
	}

	/**
	 * Permisos apartar.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res base
	 */
	//Metodo que se encarga de revisar que tenga los permisos para apartar dicho folio el usuario actual
	@Override
	public BeanResBase permisosApartar(ArchitechSessionBean sessionBean, String folio){
		
		//Se crean los objetos necesarios para ejecutar el query
		BeanResBase res=new BeanResBase();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;

		List<Object> parametros = new ArrayList<Object>();
	      try{
	    	  //Consulta a la base de datos
	    	 String query=QUERY_CONSUL_APARTADO;
	    	 parametros.add(sessionBean.getUsuario());
	    	 parametros.add(folio);
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				if(!list.isEmpty()){
					//Si nos regresa algo es que la operacion puede ser apartada
					res.setCodError(Errores.CODE_SUCCESFULLY);
					res.setTipoError(Errores.TIPO_MSJ_INFO);
				}else{
					//Si no se devuelve algo se pone el error
					res.setCodError(responseDTO.getCodeError());
					res.setMsgError(responseDTO.getMessageError());
				}
			}
	      } catch (ExceptionDataAccess e) {
	    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
	         showException(e);
	         res.setCodError(Errores.EC00011B);
	         res.setTipoError(Errores.TIPO_MSJ_ERROR);
	      }
	      return res;
	}

	/**
	 * Permisos modificar.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res base
	 */
	@Override
	//Metodo para saber si el usuario actual tiene permisos para modificar el folio
	public BeanResBase permisosModificar(ArchitechSessionBean sessionBean, String folio){
		
		//Se crean los objetos necesarios para ejecutar el query
		BeanResBase res=new BeanResBase();
		List<HashMap<String,Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		List<Object> parametros = new ArrayList<Object>();
		try{
			//Consulta a la base de datos
			String query=QUERY_CONSUL_USR_MODIFICA;
			parametros.add(sessionBean.getUsuario());
			parametros.add(folio);
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			//Si se obtuvo alguna respuesta se setea la lista
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
			}
		} catch (ExceptionDataAccess e) {
			//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
			res.setCodError(Errores.EC00011B);
			res.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		//Se valida que la lista no sea nula y no este vacía
		if(list != null && !list.isEmpty()){
            HashMap<String,Object> map=list.get(0);
			String estatus=utilerias.getString(map.get("ESTATUS"));
			boolean validaEstatus = !("CA".equals(estatus) || "CO".equals(estatus));
			//Se valida que la operacion no se encuentre ni cancelada ni confirmada
			if(validaEstatus){
				//Se toma como valida para modificar si pasa no esta ni cancelada ni confirmada
				res.setCodError(Errores.CODE_SUCCESFULLY);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				//Si esta cancelada o confirmada se pone el error ED00011V
				res.setCodError(Errores.ED00011V);
				res.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		}else{
			//Si no se pudo obtener la lista por alguna razon se setea el error ED00011V
			res.setCodError(Errores.ED00011V);
			res.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return res;
	}
	
	//Metodo que se encarga de guardar la operacion
	@Override
	public BeanResBase guardarOperacion(ArchitechSessionBean sessionBean, BeanOperacion operacion){
		
		//Se crean los objetos necesarios para ejecutar el query
		  final BeanResBase response = new BeanResBase();
		  ResponseMessageDataBaseDTO responseDTO = null;
		  List<Object> parametros = null;
		  try{
			//Se crea el arraylist de los parametros que se enviaran
			 parametros = llenaParametros(operacion);
			//Consulta a la base de datos
	        responseDTO = HelperDAO.actualizar(QUERY_UPDATE_OPERACION, 
			   parametros,
	        HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	        
	        //Solamente se agrega los codigos de error y mensajes de la consulta, no se procesa nada más al ser actualizacion
	        response.setCodError(responseDTO.getCodeError());
	        response.setMsgError(responseDTO.getMessageError());
	     } catch (ExceptionDataAccess e) {
	    	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
	        showException(e);
	        response.setCodError(Errores.EC00011B);
	        response.setTipoError(Errores.TIPO_MSJ_ERROR);
	     }
	     return response;
	}
	
	/**
	 * Llena parametros.
	 *
	 * @param operacion El objeto: operacion
	 * @return Objeto list
	 */
	private List<Object> llenaParametros(BeanOperacion operacion) {
		/** Se crea la lista que contendra los parametros **/
		List<Object> parametros = new ArrayList<Object>();
		/** Se asignan los datos a la lista **/
		   parametros.add(operacion.getCodPostalOrd());						                 //Codigo postal ordenante
		   parametros.add(operacion.getComentario1());						                 //Comentario 1
		   parametros.add(operacion.getComentario2());						                 //Comentario 2
		   parametros.add(operacion.getComentario3());						                 //Comentario 3
		   parametros.add(operacion.getComentario4());						                 //Comentario 4
		   parametros.add(operacion.getConceptoPago());						                 //Concepto Pago
		   parametros.add(operacion.getConceptoPago2());					                 //Concepto Pago 2
		   parametros.add(operacion.getCveDivisaOrd());						                 //Clave Divisa Ordenante
		   parametros.add(operacion.getCveDivisaRec());						                 //Clave Divisa Receptora
		   parametros.add(operacion.getCveEmpresa());						                 //Clave Empresa
		   parametros.add(operacion.getCveIntermeOrd());					                 //Clave Intermediaria Ordenante
		   parametros.add(operacion.getCveIntermeRec());					                 //Clave Intermediario Receptora
		   parametros.add(operacion.getCveMedioEnt());						                 //Clave Medio Ent
		   parametros.add(operacion.getCveOperacion());						                 //Clave Operacion
		   parametros.add(operacion.getCvePtoVta());						                 //Clave Punto de Venta
		   parametros.add(operacion.getCvePtoVtaOrd());						                 //Clave Punto de venta Ordenante
		   parametros.add(operacion.getCvePtoVtaRec());						                 //Clave punto de venta receptora
		   parametros.add(operacion.getCveRastreo());						                 //Clave rastreo
		   parametros.add(operacion.getCveTransfe());						                 //Clave transfe
		   parametros.add(operacion.getCveUsuarioCap());					                 //Clave usuario cap
		   parametros.add(operacion.getCveUsuarioSol());					                 //Clave usuario sol
		   parametros.add(operacion.getDireccionIp());						                 //Direccion ip
		   parametros.add(operacion.getDomicilioOrd());						                 //Domicilio Ordenante
		   parametros.add(operacion.getFchConstitOrd());					                 //Fecha consitucion ordenante
		   parametros.add(operacion.getFchInstrucPago());					                 //Fecha instruccion pago
		   parametros.add(operacion.getFolioPago());						                 //Folio pago
		   parametros.add(operacion.getFolioPaquete());						                 //Folio paquete
		   parametros.add(operacion.getHoraInstrucPago());					                 //Hora instruccion pago
		   parametros.add(operacion.getImporteAbono());						                 //Importe abono
		   parametros.add(operacion.getImporteCargo());						                 //Importe cargo
		   parametros.add(operacion.getMotivoDevol());						                 //Motivo devolucion
		   parametros.add(operacion.getNombreOrd());						                 //Nombre ordenante
		   parametros.add(operacion.getNombreRec());						                 //Nombre receptor
		   parametros.add(operacion.getNombreRec2());						                 //Nombre receptor 2
		   parametros.add(operacion.getNumCuentaOrd());						                 //Numero cuenta ordenante
		   parametros.add(operacion.getNumCuentaRec());						                 //Numero cuenta receptora
		   parametros.add(operacion.getReferenciaMed());					                 //Referencia med
		   parametros.add(operacion.getRfcOrd());							                 //Referencia ordenante
		   parametros.add(operacion.getRfcRec());							                 //Referencia receptora
		   parametros.add(operacion.getRfcRec2());							                 //Referencia receptora 2
		   parametros.add(operacion.getTipoCambio());						                 //Tipo de cambio
		   parametros.add(operacion.getTipoCuentaOrd());					                 //Tipo cuenta ordenante
		   parametros.add(operacion.getTipoCuentaRec());					                 //Tipo cuenta receptora
		   parametros.add(operacion.getTipoCuentaRec2());					                 //Tipo cuenta receptora 2
		   parametros.add(operacion.getCiudadBcoRec());						                 //Ciudad Bco Receptora
		   parametros.add(operacion.getCveAba());							                 //Clave aba
		   parametros.add(operacion.getCvePaisBcoRec());					                 //Clave pais bco receptora
		   parametros.add(operacion.getFormaLiq());							                 //Forma liq
		   parametros.add(operacion.getImporteDls());						                 //Importe dolares
		   parametros.add(operacion.getNombreBcoRec());						                 //Nombre bco receptora
		   parametros.add(operacion.getNumCuentaRec2());					                 //Numero cuenta receptora 2
		   parametros.add(operacion.getPlazaBanxico());						                 //Plaza banxico
		   parametros.add(operacion.getSucursalBcoRec());					                 //Sucursal bco receptora
		   parametros.add(operacion.getBeanDatosGenerales().getBucCliente());			     //Buc de cliente
		   parametros.add(operacion.getBeanDatosGenerales().getImporteIva());			     //Importe iva
		   parametros.add(operacion.getBeanDatosIp().getDirIpGenTran());					 //Ip generacion de tranferencia
		   parametros.add(operacion.getBeanDatosIp().getDirIpFirma());						 //Ip de firmado
		   parametros.add(operacion.getBeanDatosGenerales().getUetrSwift());				 //UetrSwift
		   parametros.add(operacion.getBeanDatosGenerales().getCampoSwift1());				 //CampoSwift1
		   parametros.add(operacion.getBeanDatosGenerales().getCampoSwift2());				 //CamposSwift2
		   parametros.add(operacion.getBeanDatosGenerales().getRefeAdicional1());			 //Referencia adicional
		   parametros.add(operacion.getRefeNumerica());									     //Referencia numerica
		   parametros.add(operacion.getIdFolio());						                     //id folio
		   /** Retorno del metodo **/
		   return parametros;
	}
}

	