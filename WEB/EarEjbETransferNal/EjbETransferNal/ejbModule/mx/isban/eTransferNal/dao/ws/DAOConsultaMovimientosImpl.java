/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovimientos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.constantes.modBIP.ConstantesConsultaMov;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;

/**
 * @author mcamarillo
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaMovimientosImpl extends Architech implements DAOConsultaMovimientos {
	
	/**
	 * Variable serializable
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param trama
	 * 			json convertido a string
	 * @return
	 * 			trama de respuesta de 390
	 */
	@Override
	public ResBeanEjecTranDAO ejecutar(final String trama) {
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(
				trama, ConstantesConsultaMov.SERVICIO_TUXEDO, ConstantesConsultaMov.CANAL);
		return resBeanEjecTranDAO;
	}
}
