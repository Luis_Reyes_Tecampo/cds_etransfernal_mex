/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOCancelacionPagosImp.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagosDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagosDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 *Clase que se encarga de obtener la información de la cancelación de pagos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCancelacionPagosImp  extends Architech implements DAOCancelacionPagos {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1759243510245774053L;
	
	/**
	 * 
	 */
	public static final StringBuilder SELECT_FIELD = new StringBuilder()
			.append(" TM.REFERENCIA, TM.CVE_COLA COLA,  ")
			.append(" TM.CVE_TRANSFE CREFERENCIA, ")
			.append(" TM.CVE_MECANISMO MECAN, ")
			.append(" TM.CVE_MEDIO_ENT MEDIOENT, ")
			.append(" TM.CVE_INTERME_ORD ORD,  ")
			.append(" TM.CVE_INTERME_REC REC,  ")
			.append(" TM.IMPORTE_ABONO IMPORTE_ABONO,  ")
			.append(" TM.ESTATUS EST, ")
			.append(" SUBSTR(TM.ESTATUS_SERVIDOR,1,1) TOPO, ")
			.append(" SUBSTR(TM.ESTATUS_SERVIDOR,2,1) PRI, ")
			.append(" TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) PAQUETE, TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) PAGO, ");
	/**
	 * 
	 */
	public static final StringBuilder SELECT_FIELD_EXPOR = new StringBuilder()
			.append(" TM.REFERENCIA ||','||TM.CVE_COLA ||','||  ")
			.append(" TM.CVE_TRANSFE ||','|| ")
			.append(" TM.CVE_MECANISMO ||','|| ")
			.append(" TM.CVE_MEDIO_ENT ||','|| ")
			.append(" TM.CVE_INTERME_ORD ||','||  ")
			.append(" TM.CVE_INTERME_REC ||','||  ")
			.append(" TM.IMPORTE_ABONO ||','||  ")
			.append(" TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ||','|| TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ||','|| ")
			.append(" TM.ESTATUS ||','|| ")
			.append(" SUBSTR(TM.ESTATUS_SERVIDOR,1,1) ||','|| ")
			.append(" SUBSTR(TM.ESTATUS_SERVIDOR,2,1) ");
	
	/**
	 * MONTO TOTAL
	 */
	public static final StringBuilder SELECT_FIELD_MONTO_TOTAL = new StringBuilder()
			.append(" SUM(TM.IMPORTE_ABONO) MONTO_TOTAL  ");
	
	/**
	 * 
	 */
	private static final String FROM_TRAN_MENSAJE_SPID_JOIN = " FROM TRAN_MENSAJE TM  ";
	
	/**
	 * 
	 */
	private static final String LEFT_JOIN_TRAN_SPID_CANC = " LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE ";
	
	/**Select de cancelaciones de pagos
	   */
	public static final StringBuilder CONSULTA_CANCELACIONP = 
			new StringBuilder()	
	.append(" SELECT H.* FROM( ")
	.append(" SELECT H.*, ROWNUM D FROM  ")
	.append(" (SELECT  ")
	.append(SELECT_FIELD.toString()) //RGMZ TODO
	.append(" S.CONT ")
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ")
	.append(" [ORDENAMIENTO])H) H WHERE D BETWEEN ? AND ?  ");
	
	/**Select MONTO TOTAL 
	   */
	public static final StringBuilder CONSULTA_CANCELACIONP_MONTO = 
			new StringBuilder()	
	.append(" SELECT  ")
	.append(SELECT_FIELD_MONTO_TOTAL.toString())
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");
	
	
	/**Select de cancelaciones de pagos filtrado por orden
	   */
	public static final StringBuilder CONSULTA_CANCELACIONORDEN = 
			new StringBuilder()	
	.append(" SELECT H.* FROM( ")
	.append(" SELECT H.*, ROWNUM D FROM  ")
	.append(" (SELECT  ")
	.append(SELECT_FIELD.toString()) //RGMZ TODO
	.append(" S.CONT ")
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ")
	.append(" [ORDENAMIENTO])H )H WHERE D BETWEEN ? AND ?  ");
	
	
	/**Select de Monto cancelaciones de pagos filtrado por orden
	   */
	public static final StringBuilder CONSULTA_CANCELACIONORDEN_MONTO = 
			new StringBuilder()	
	.append(" SELECT  ")
	.append(SELECT_FIELD_MONTO_TOTAL.toString())
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");
	
	/**Select de cancelaciones de pagos filtrado por traspasos
	   */
	public static final StringBuilder CONSULTA_CANCELACIONTRASPASOS = 
			new StringBuilder()	
	.append(" SELECT H.* FROM( ")
	.append(" SELECT H.*, ROWNUM D FROM  ")
	.append(" (SELECT  ")
	.append(SELECT_FIELD.toString()) // RGMZ TODO
	.append(" S.CONT ")
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ")
	.append(" [ORDENAMIENTO])H )H WHERE D BETWEEN ? AND ?  ");
	
	
	/**Select de cancelaciones de pagos filtrado por traspasos
	   */
	public static final StringBuilder CONSULTA_CANCELACIONTRASPASOS_MONTO = 
			new StringBuilder()	
	.append(" SELECT  ")
	.append(SELECT_FIELD_MONTO_TOTAL.toString())
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC)
	.append(" WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");
	
	
	/**Select de cancelaciones de pagos
	   */
	public static final StringBuilder CONSULTA_TODOSCANCELACIONP = 
			new StringBuilder()	
	.append(" SELECT  ")
	.append(SELECT_FIELD_EXPOR.toString()) 
	.append(FROM_TRAN_MENSAJE_SPID_JOIN)
	.append(" LEFT OUTER JOIN TRAN_SPID_CANC TSC ON CASE WHEN SUBSTR(TM.ESTATUS_SERVIDOR,1,1) = 'V' OR CVE_MECANISMO='TRSPISIA' THEN 0 ELSE TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) END = TSC.FOLIO_PAGO AND TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) = TSC.FOLIO_PAQUETE, ")
	.append(" ( SELECT COUNT(*) CONT FROM TRAN_MENSAJE TM ")
	.append(LEFT_JOIN_TRAN_SPID_CANC);
	
	/**insert de cancelaciones de pagos
	   */
	private static final StringBuilder INSERT_TODOS_CANCEL_PAGOS = 
			new StringBuilder().append("INSERT INTO TRAN_SPEI_EXP_CDA ")
							   .append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) ")
                               .append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	
	/**update de cancelaciones de pagos
	   */
	private static final StringBuilder INSERT_CANCELACIONP = 
			new StringBuilder().append("INSERT INTO TRAN_SPID_CANC ")
							   		.append("(FCH_OPERACION, ")
							   		.append("CVE_MI_INSTITUC, ")
							   		.append("FOLIO_PAQUETE, ")
							   		.append("FOLIO_PAGO, ")
							   		.append("SW_CANCEL, ")
							   		.append("ORDPAG_TRASP, ")
							   		.append("TOPOLOGIA, ")
							   		.append("USUARIO_CANCEL, ")
							   		.append("HORA_CANCEL, ")
							   		.append("ESTADO_CANCEL, ")
							   		.append("FOLIO_SOLICITUD, ")
							   		.append("CVE_INST_BENEF, ")
							   		.append("NUM_PAGOS_PAQ, ")
							   		.append("NUM_PAGOS_CANC, ")
							   		.append("MENSAJE) ")
							   .append("VALUES ")
							   		.append("(TO_DATE(?, 'DD-MM-YYYY'), ?, ?, ?, ?, ?, ?, ?, ?, '', '', '', '', '', '') ");
				
	
	
	/**Metodo que sirve para consultar cancelacion de pagos
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return bCancelacionPagosDAO Objeto del tipo BeanCancelacionPagosDAO
	   */
	@Override
	public BeanCancelacionPagosDAO obtenerCancelacionPagos(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		final BeanCancelacionPagosDAO bCancelacionPagosDAO = new BeanCancelacionPagosDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias =Utilerias.getUtilerias();
		List<Object> parametros = new ArrayList<Object>();
    	ResponseMessageDataBaseDTO responseDTO = null;
		
		List<BeanCancelacionPagos> listaCancelacionPagos=null;
		
		try{
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			parametros.add(beanPaginador.getRegIni());
	    	parametros.add(beanPaginador.getRegFin());
			responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONP.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
			

			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				
				list=responseDTO.getResultQuery();
				if(!list.isEmpty()){
					listaCancelacionPagos= new ArrayList<BeanCancelacionPagos>();
						llenarCancelacionPagos(bCancelacionPagosDAO, list, utilerias, listaCancelacionPagos);
				}
			
			}
			
			bCancelacionPagosDAO.setBeanCancelacionPagos(listaCancelacionPagos);
			bCancelacionPagosDAO.setCodError(responseDTO.getCodeError());
			bCancelacionPagosDAO.setMsgError(responseDTO.getMessageError());
			
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return bCancelacionPagosDAO;
	}

	/**Metodo que sirve para llenar el select
	 * @param beanCancelacionPagosDAO Objeto del tipo @see BeanCancelacionPagosDAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @param listaCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	 */
	private void llenarCancelacionPagos(final BeanCancelacionPagosDAO beanCancelacionPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias, List<BeanCancelacionPagos> listaCancelacionPagos) {
		BeanCancelacionPagos beanCancelacionPagos;
		Integer zero = Integer.valueOf(0);
		
		for(Map<String,Object> listMap : list){
		
			Map<String, Object> mapResult = listMap;
			beanCancelacionPagos = new BeanCancelacionPagos();
			beanCancelacionPagos.setBeanCancelacionPagosDos(new BeanCancelacionPagosDos());
		
			beanCancelacionPagos.getBeanCancelacionPagosDos().setReferencia(utilerias.getString(mapResult.get("REFERENCIA")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setCola(utilerias.getString(mapResult.get("COLA")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setCvnTran(utilerias.getString(mapResult.get("CREFERENCIA")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setMecan(utilerias.getString(mapResult.get("MECAN")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setMedioEnt(utilerias.getString(mapResult.get("MEDIOENT")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setOrd(utilerias.getString(mapResult.get("ORD")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setRec(utilerias.getString(mapResult.get("REC")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setPaq(utilerias.getString(mapResult.get("PAQUETE")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setPago(utilerias.getString(mapResult.get("PAGO")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setEst(utilerias.getString(mapResult.get("EST")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setTopo(utilerias.getString(mapResult.get("TOPO")).toString());
			beanCancelacionPagos.getBeanCancelacionPagosDos().setPri(utilerias.getString(mapResult.get("PRI")).toString());
			
			
			listaCancelacionPagos.add(beanCancelacionPagos);
			if(zero.equals(beanCancelacionPagosDAO.getTotalReg())){
				beanCancelacionPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));			
			}
			
		}
	}
	
	/**Metodo que sirve para consultar cancelacion de pagos filtrados por traspasos
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return beanCancelPagosDAO Objeto del tipo BeanCancelacionPagosDAO
	   */
	@Override
	public BeanCancelacionPagosDAO obtenerCancelacionPagosTraspasos(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		final BeanCancelacionPagosDAO beanCancelPagosDAO = new BeanCancelacionPagosDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias =Utilerias.getUtilerias();
		List<Object> parametros = new ArrayList<Object>();
    	ResponseMessageDataBaseDTO responseDTO = null;
		
		List<BeanCancelacionPagos> listaCancelacionPagos=null;
		
		try{
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			parametros.add(beanPaginador.getRegIni());
	    	parametros.add(beanPaginador.getRegFin());
			responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONTRASPASOS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
			

			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				
				list=responseDTO.getResultQuery();
				if(!list.isEmpty()){
					listaCancelacionPagos= new ArrayList<BeanCancelacionPagos>();
						llenarCancelacionPagos(beanCancelPagosDAO, list, utilerias, listaCancelacionPagos);
				}
			
			}
			
			beanCancelPagosDAO.setBeanCancelacionPagos(listaCancelacionPagos);
			beanCancelPagosDAO.setCodError(responseDTO.getCodeError());
			beanCancelPagosDAO.setMsgError(responseDTO.getMessageError());
			
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return beanCancelPagosDAO;
	}
	
	/**Metodo que sirve para consultar cancelacion de pagos
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return beanCancelacionPagosOrdenDAO Objeto del tipo BeanCancelacionPagosDAO
	   */
	@Override
	public BeanCancelacionPagosDAO obtenerCancelacionPagosOrden(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		final BeanCancelacionPagosDAO beanCancelacionPagosOrdenDAO = new BeanCancelacionPagosDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias =Utilerias.getUtilerias();
		List<Object> parametros = new ArrayList<Object>();
    	ResponseMessageDataBaseDTO responseDTO = null;
		
		
		List<BeanCancelacionPagos> listaCancelacionPagos=null;
		
		try{
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			parametros.add(beanPaginador.getRegIni());
	    	parametros.add(beanPaginador.getRegFin());
			responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONORDEN.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
			

			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				
				list=responseDTO.getResultQuery();
				if(!list.isEmpty()){
					listaCancelacionPagos= new ArrayList<BeanCancelacionPagos>();
						llenarCancelacionPagos(beanCancelacionPagosOrdenDAO, list, utilerias, listaCancelacionPagos);
				}
			
			}
			
			beanCancelacionPagosOrdenDAO.setBeanCancelacionPagos(listaCancelacionPagos);
			beanCancelacionPagosOrdenDAO.setCodError(responseDTO.getCodeError());
			beanCancelacionPagosOrdenDAO.setMsgError(responseDTO.getMessageError());
			
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		
		return beanCancelacionPagosOrdenDAO;
	}

	/**Metodo que sirve para consultar cancelacion de pagos
	   * @param beanCancelacionPagos Objeto del tipo @see BeanCancelacionPagos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return beanCancelacionPagosDAO Objeto del tipo BeanCancelacionPagosDAO
	   */
	@Override
	public BeanCancelacionPagosDAO actualizarCancelacionPagos(BeanCancelacionPagos beanCancelacionPagos,
			ArchitechSessionBean architechSessionBean) {

		BeanCancelacionPagosDAO beanCancelacionPagosDAO = new BeanCancelacionPagosDAO();
		Calendar calendario = Calendar.getInstance();
		String hora = calendario.get(Calendar.HOUR)+":"+calendario.get(Calendar.MINUTE)+":"+calendario.get(Calendar.SECOND);
		
	     try{
	         ResponseMessageDataBaseDTO responseDTO = HelperDAO.actualizar(INSERT_CANCELACIONP.toString(), 
	  			   Arrays.asList(new Object[]{
	  					   beanCancelacionPagos.getSFechaOperacion(),
	  					   beanCancelacionPagos.getSCveInstitucion(),
	  					   beanCancelacionPagos.getBeanCancelacionPagosDos().getPaq(),
	  					   "SPID".equals(beanCancelacionPagos.getBeanCancelacionPagosDos().getMecan()) && 
	  					   		"T".equals(beanCancelacionPagos.getBeanCancelacionPagosDos().getTopo().toUpperCase()) ? beanCancelacionPagos.getBeanCancelacionPagosDos().getPago() : 0,
	  					   "S",
	  					   "SPID".equals(beanCancelacionPagos.getBeanCancelacionPagosDos().getMecan()) ? "O" : "T",
	  					   beanCancelacionPagos.getBeanCancelacionPagosDos().getTopo(),
	  					   architechSessionBean.getUsuario(),
	  					   hora,
	  			   }), 
	  	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         beanCancelacionPagosDAO.setCodError(responseDTO.getCodeError());
	    	 
	    	 
	     } catch (ExceptionDataAccess e) {
	          showException(e);
	          beanCancelacionPagosDAO.setCodError(Errores.EC00011B);
	       }
		
		
		return beanCancelacionPagosDAO;
	}
	
	
	/**Metodo que sirve para el monto total
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	@Override
	public BigDecimal obtenerImporteTotalOrd( ArchitechSessionBean architechSessionBeans) {
	  	BigDecimal totalImporte = BigDecimal.ZERO;
	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String,Object>> list = null;
	  	try {              	
	  			  		responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONORDEN_MONTO.toString(), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	if(responseDTO.getResultQuery()!= null){
	      		 list = responseDTO.getResultQuery();
	      		 if(!list.isEmpty()){
	      			for(Map<String,Object> mapResult : list){
	      				totalImporte = totalImporte.add(new BigDecimal(Utilerias.getUtilerias().getString(mapResult.get("MONTO_TOTAL"))));
	      			 }
	      		 }
	      	}
	      	
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}
	
	
	/**Metodo que sirve para el monto total
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	@Override
	public BigDecimal obtenerImporteTotalTras( ArchitechSessionBean architechSessionBeans) {
	  	BigDecimal totalImporte = BigDecimal.ZERO;
	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String,Object>> list = null;
	  	try {              	
	  			  		responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONTRASPASOS_MONTO.toString(), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	if(responseDTO.getResultQuery()!= null){
	      		 list = responseDTO.getResultQuery();
	      		 if(!list.isEmpty()){
	      			for(Map<String,Object> mapResult : list){
	      				totalImporte = totalImporte.add(new BigDecimal(Utilerias.getUtilerias().getString(mapResult.get("MONTO_TOTAL"))));
	      			 }
	      		 }
	      	}
	      	
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}
	
	/**Metodo que sirve para el monto total
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	@Override
	public BigDecimal obtenerImporteTotal( ArchitechSessionBean architechSessionBeans) {
	  	BigDecimal totalImporte = BigDecimal.ZERO;
	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String,Object>> list = null;
	  	try {              	
	  			  		responseDTO = HelperDAO.consultar(CONSULTA_CANCELACIONP_MONTO.toString(), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	if(responseDTO.getResultQuery()!= null){
	      		 list = responseDTO.getResultQuery();
	      		 if(!list.isEmpty()){
	      			for(Map<String,Object> mapResult : list){
	      				totalImporte = totalImporte.add(new BigDecimal(Utilerias.getUtilerias().getString(mapResult.get("MONTO_TOTAL"))));
	      			 }
	      		 }
	      	}
	      	
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}

	
	/**Metodo que sirve para hacer insert de las cancelaciones de pagos
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return beanCancelacionPagosDAO Objeto del tipo BeanCancelacionPagosDAO
	   */
	@Override
	public BeanCancelacionPagosDAO exportarTodoCancelacionPagos(BeanReqCancelacionPagos beanReqCancelacionPagos,
			ArchitechSessionBean architechSessionBean, String opcion) {
		
		StringBuilder builder = new StringBuilder();
		final BeanCancelacionPagosDAO beanCancelacionPagosDAO = new BeanCancelacionPagosDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
        final Date fecha = new Date();
        ResponseMessageDataBaseDTO responseDTO = null;
        
        
        try{
        	builder.append(CONSULTA_TODOSCANCELACIONP); 
			if (null != opcion && opcion.length() >= 1) {
				switch (opcion.charAt(0)) {
				case '1': //ORDEN
					builder.append(" WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='SPID' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");;
					break;
				case '2': //Traspasos
					builder.append(" WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO='TRSPISIA' AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");
					break;
				case '3':
					builder.append(" WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL )S WHERE CVE_MECANISMO IN ('SPID','TRSPISIA') AND ESTATUS = 'EN' AND TSC.CVE_MI_INSTITUC IS NULL ");
					break;

				}
			}
        		
        	final String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        	final String nombreArchivo = "SPID_CANCELACION_PAGOS"+fechaActual+".csv";
        	beanCancelacionPagosDAO.setNombreArchivo(nombreArchivo);
        	
        	responseDTO = HelperDAO.insertar(INSERT_TODOS_CANCEL_PAGOS.toString(), 
        			Arrays.asList(new Object[]{architechSessionBean.getUsuario(),
        				                     architechSessionBean.getIdSesion(),
        				                     "SPID",
        				                     "CANCELACION PAGOS",
        				                     nombreArchivo,
        				                     beanReqCancelacionPagos.getTotalReg(),
        				                     "PE",
        				                     builder.toString(),
        				                     new StringBuilder()
     										.append("REFERENCIA, COLA, CVE. TRAN, MECAN, MEDIO ENT,")
     										.append(" ORD, REC, IMPORTE ABONO, PAQ, PAGO, EST., TOPO, PRI")
     				                     					.toString()
        				                     }), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           
        	beanCancelacionPagosDAO.setCodError(responseDTO.getCodeError());
        	beanCancelacionPagosDAO.setMsgError(responseDTO.getMessageError());

        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanCancelacionPagosDAO.setCodError(Errores.EC00011B); 
        }
        
        return beanCancelacionPagosDAO;
	}
        

}
