/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: DAOMttoMediosEntregaImpl.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/09/2018 04:40:45 PM Juan Jesus Beltran R. VectorFSW Creacion
 * 1.1   23/10/2020      Rodolfo Mendez Santader validacion de campos
 */
package mx.isban.eTransferNal.dao.catalogos;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCertificadoCifrado;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasMttoMediosEntrega;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOMttoMediosEntregaImpl.
 *
 * Implementa los metodos de acceso Base de Datos,
 *
 * Los cuales proveen a la logica de negocio
 * comunicacion con la tabla de Oracle
 *
 * @author FSW-Vector
 * @since 28/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMttoMediosEntregaImpl extends Architech implements DAOMttoMediosEntrega{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8506679628973413559L;


	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";

	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY CVE_MEDIO_ENT ASC";

	/** La constante QUERY_CONSULTA_CERTIFICADOS. */
	private static final String QUERY_CONSULTA_TODO = "SELECT CVE_MEDIO_ENT, U_VERSION, DESCRIPCION, COM_ANUAL_PF, COM_ANUAL_PM, CKK_SEGURIDAD, CANAL, IMP_MAX_SPEI_7X24, CONT  "
			+ "FROM( SELECT TM.CVE_MEDIO_ENT, TM.U_VERSION, TM.DESCRIPCION, TM.COM_ANUAL_PF, TM.COM_ANUAL_PM, TM.CKK_SEGURIDAD, TM.CANAL, TM.IMP_MAX_SPEI_7X24, TM.CONT, ROWNUM AS RN "
			+ "		FROM (SELECT CVE_MEDIO_ENT, U_VERSION, DESCRIPCION, COM_ANUAL_PF, COM_ANUAL_PM, CKK_SEGURIDAD, CANAL, IMP_MAX_SPEI_7X24, CONTADOR.CONT "
			+ "			FROM (SELECT COUNT(1) CONT FROM COMU_MEDIOS_ENT TM [FILTRO_WH]) CONTADOR, "
			+ "			COMU_MEDIOS_ENT [FILTRO_WH]) TM ) TM ";

	/** La constante QUERY_CONSULTA_PAGINADO. */
	private static final String QUERY_CONSULTA_PAGINADO = QUERY_CONSULTA_TODO + FILTRO_PAGINADO + ORDEN;

	/** La constante CONSULTA_INSERTAR. */
	public static final String CONSULTA_INSERTAR = "INSERT INTO COMU_MEDIOS_ENT (CVE_MEDIO_ENT, U_VERSION, DESCRIPCION, COM_ANUAL_PF, COM_ANUAL_PM, CKK_SEGURIDAD, CANAL, IMP_MAX_SPEI_7X24) VALUES (UPPER(?), '!', UPPER(?), ?, ?, ?, UPPER(?), ?)";

	/** La constante CONSULTA_EXISTENCIA. */
	public static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM COMU_MEDIOS_ENT  WHERE UPPER(TRIM(CVE_MEDIO_ENT)) = UPPER(TRIM(?)) ";

	/** La constante QUERY_DELETE. */
	private static final String QUERY_DELETE = "DELETE COMU_MEDIOS_ENT WHERE UPPER(TRIM(CVE_MEDIO_ENT)) = UPPER(TRIM(?)) ";

	/** La constante CONSULTA_UPDATE. */
	private static final String CONSULTA_UPDATE = "UPDATE COMU_MEDIOS_ENT SET CANAL=UPPER(?),DESCRIPCION=UPPER(?),COM_ANUAL_PF=?,COM_ANUAL_PM=?,CKK_SEGURIDAD=?, IMP_MAX_SPEI_7X24= ? "
			+ " WHERE UPPER(TRIM(CVE_MEDIO_ENT)) = UPPER(TRIM(?)) ";

	/** La variable que contiene informacion con respecto a: utilerias. */
	private Utilerias utilerias = Utilerias.getUtilerias();

	/** La variable que contiene informacion con respecto a: utilerias certificados. */
	private UtileriasMttoMediosEntrega utileriasMedios = UtileriasMttoMediosEntrega.getInstancia();

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosEntrega#consultar(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String, java.lang.String, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 */
	@Override
	public BeanMediosEntrega consultar(ArchitechSessionBean session, BeanMedioEntrega beanFiltro,
			BeanPaginador beanPaginador) {
		//Obejtos que sirven para generar la respuesta del servicio
				BeanMediosEntrega response = new BeanMediosEntrega();
				List<BeanMedioEntrega> medios = Collections.emptyList();
				//Obejtos que sirven para realizar la consulta y procesar el resultado
				List<HashMap<String, Object>> queryResponse = null;
				ResponseMessageDataBaseDTO responseDTO = null;
				//Agrega los parametros del Query
				String filtroAnd = utileriasMedios.getFiltro(beanFiltro, "AND");
				String filtroWhere = utileriasMedios.getFiltro(beanFiltro, "WHERE");
				try {
					//Realiza la consulta
					responseDTO = HelperDAO.consultar(QUERY_CONSULTA_PAGINADO.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
							.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(
		        			new Object[] { beanPaginador.getRegIni(),
		 						   beanPaginador.getRegFin() }), HelperDAO.CANAL_GFI_DS,
							this.getClass().getName());
					if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
						//Se inicializa la lista un vez que se valido que la cosnulta produjo resultados
						medios = new ArrayList<BeanMedioEntrega>();
						queryResponse = responseDTO.getResultQuery();
						info(queryResponse.toString());
						for (HashMap<String, Object> map : queryResponse) {
							//Inicializo el objeto certificado para comenzar a ingresar los datos
							BeanMedioEntrega medioEntrega = new BeanMedioEntrega();
							//Agrega cada propiedad
							medioEntrega.setCveMedioEntrega(utilerias.getString(map.get("CVE_MEDIO_ENT")));
							medioEntrega.setVersion(utilerias.getString(map.get("U_VERSION")));
							medioEntrega.setDesc(utilerias.getString(map.get("DESCRIPCION")));
							medioEntrega.setComAnualPF(utilerias.getString(map.get("COM_ANUAL_PF")));
							medioEntrega.setComAnualPM(utilerias.getString(map.get("COM_ANUAL_PM")));
							medioEntrega.setSeguridad(utilerias.getString(map.get("CKK_SEGURIDAD")));
							medioEntrega.setCanal(utilerias.getString(map.get("CANAL")));
							medioEntrega.setImporteMax(utilerias.getString(map.get("IMP_MAX_SPEI_7X24")));

							//Agrega el medio de entrega
							medios.add(medioEntrega);
							//Ingresa el Total de Registros
							response.setTotalReg(utilerias.getInteger(map.get("CONT")));
						}
					}
					//Agrega la lista completa
					response.setMediosEntrega(medios);
					//Agrega el BeanError
					response.setBeanError(utileriasMedios.getError(responseDTO));
				} catch (ExceptionDataAccess e) {
					//Muestra la excepcion generada
					showException(e);
					response.setBeanError(utileriasMedios.getError(null));
				}
				//Response DAO
				return response;
	}


	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosEntrega#agregarMedioEntrega(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega)
	 */
	@Override
	public BeanResBase agregarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio) {
		//Objeto a responder
				BeanResBase response = new BeanResBase();
				ResponseMessageDataBaseDTO responseDTO = null;
				List<Object> parametros;
				try {
					//Agrega los parametros del Query
					parametros = utileriasMedios.agregarParametros(beanMedio, UtileriasCertificadoCifrado.ALTA);
					int existencia = buscarRegistro(beanMedio);
					if(existencia == 0) {
						//Realiza la consulta
						responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
						response.setCodError(responseDTO.getCodeError());
					}else {
						response.setCodError(Errores.ED00130V);
					}
				} catch (ExceptionDataAccess e) {
					//Muestra la excepcion generada
					showException(e);
					response.setCodError(Errores.EC00011B);
				}
				//Response DAO
				return response;
	}

	/**
	 * Buscar registro.
	 *
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto int
	 */
	private int buscarRegistro(BeanMedioEntrega beanMedio) {
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		//Objeto a responder
		int contador = 0;
		try {
			//Agrega los parametros del Query
			parametros = utileriasMedios.agregarParametros(beanMedio, UtileriasMttoMediosEntrega.BUSQUEDA);
			//Realiza la consulta
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			//Muestra la excepcion generada
			showException(e);
		}
		//Response DAO
		return contador;
	}


	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosEntrega#eliminarMediosEntrega(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega)
	 */
	@Override
	public BeanResBase eliminarMediosEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio) {
		final BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			//Realiza la consulta
			responseDTO = HelperDAO.eliminar(QUERY_DELETE, Arrays.asList(new Object[] { beanMedio.getCveMedioEntrega() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			//Muestra la excepcion generada
			response.setCodError(Errores.EC00011B);
		}
		//Response DAO
		return response;
	}


	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosEntrega#editarMedioEntrega(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega)
	 */
	@Override
	public BeanResBase editarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio) {
		//Objeto a responder
		BeanResBase response = new BeanResBase();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			// Agrega los parametros del Query
			parametros = utileriasMedios.agregarParametros(beanMedio,
					UtileriasCertificadoCifrado.MODIFICACION);
			// Realiza la consulta
			responseDTO = HelperDAO.actualizar(CONSULTA_UPDATE, parametros, HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			response.setCodError(responseDTO.getCodeError());
		} catch (ExceptionDataAccess e) {
			// Muestra la excepcion generada y setea el codigo de error
			showException(e);
			response.setCodError(Errores.EC00011B);
		}
		// Response DAO
		return response;
	}
}