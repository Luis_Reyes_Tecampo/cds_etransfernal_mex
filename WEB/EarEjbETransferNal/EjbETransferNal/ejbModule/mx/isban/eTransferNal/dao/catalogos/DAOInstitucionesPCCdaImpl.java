/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOInstitucionesPCCdaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqInst;
import mx.isban.eTransferNal.beans.catalogos.BeanHabDesParticipanteDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanInstitucion;
import mx.isban.eTransferNal.beans.catalogos.BeanInstitucionPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqHabDesInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOInstitucionesPCCdaImpl extends Architech implements DAOInstitucionesPCCda{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -59340974412400776L;

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONS_INST
	 */
	private final static String QUERY_CONS_INST = " SELECT CVE_INTERME,NOMBRE_CORTO,NUM_BANXICO " +
	" FROM COMU_INTERME_FIN T WHERE FCH_BAJA IS NULL";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_INSERT_SPEI_PARTICIPANTE = 
		" INSERT INTO TRAN_SPEI_PARTICIP (CVE_INST, PARTICIPANTE_POA, VAL_TIPO_CUENTA, CVE_INTERME, CVE_USUARIO_MODIF, FCH_MODIF, NOMBRE) " +
		" VALUES (?,'0','0',?,?,SYSDATE,?)";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_BUSCA_DUP_INSERT_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_BUSCA_DUP_INSERT_SPEI_PARTICIPANTE = 
		" SELECT COUNT(*) CONT FROM TRAN_SPEI_PARTICIP T WHERE CVE_INST =? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de UPDATE_PARTICIPANTE_POA_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_UPDATE_PARTICIPANTE_POA_SPEI_PARTICIPANTE = 
		" UPDATE TRAN_SPEI_PARTICIP SET PARTICIPANTE_POA = ?, CVE_USUARIO_MODIF=?, FCH_MODIF=SYSDATE WHERE CVE_INST =?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de UPDATE_VALIDA_CUENTA_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_UPDATE_VALIDA_CUENTA_SPEI_PARTICIPANTE = 
		" UPDATE TRAN_SPEI_PARTICIP SET VAL_TIPO_CUENTA = ?, CVE_USUARIO_MODIF=?, FCH_MODIF=SYSDATE WHERE CVE_INST =?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_SELECT_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_SELECT_SPEI_PARTICIPANTE = 
	" SELECT TMP.* FROM ( SELECT TMP.*, ROWNUM AS RN FROM ( "+
		" SELECT CVE_INST, PARTICIPANTE_POA, VAL_TIPO_CUENTA VALIDA_CUENTA,CVE_INTERME, CVE_USUARIO_MODIF, TO_CHAR(FCH_MODIF,'DD-MM-YYYY') FCH, NOMBRE "+ 
		  " FROM TRAN_SPEI_PARTICIP T ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_SELECT_SPEI_PARTICIPANTE_ORDER
	 */
	private final static String QUERY_SELECT_SPEI_PARTICIPANTE_ORDER =		  
		  " ORDER BY T.NOMBRE ) TMP) TMP "+
		  " WHERE RN BETWEEN ? AND ? ORDER BY TMP.NOMBRE ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_SELECT_COUNT_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_SELECT_COUNT_SPEI_PARTICIPANTE = 
	" SELECT COUNT(*) CONT FROM TRAN_SPEI_PARTICIP T ";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DELETE_SPEI_PARTICIPANTE
	 */
	private final static String QUERY_DELETE_SPEI_PARTICIPANTE = 
		" DELETE TRAN_SPEI_PARTICIP WHERE CVE_INST =?";
	
	@Override
	public BeanResConsInstDAO consultaInst(ArchitechSessionBean architechSessionBean){
      final BeanResConsInstDAO beanResConsInstDAO = new BeanResConsInstDAO();
      BeanInstitucion beanInstitucion = null;
      Utilerias utilerias = Utilerias.getUtilerias();
      List<BeanInstitucion> listBeanInstitucion = Collections.emptyList();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      try{
         responseDTO = HelperDAO.consultar(QUERY_CONS_INST, Arrays.asList(
        		 new Object[]{}
         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        	 listBeanInstitucion = new ArrayList<BeanInstitucion>();
            list = responseDTO.getResultQuery();
            for(HashMap<String,Object> map:list){
            	beanInstitucion = new BeanInstitucion();
            	beanInstitucion.setCveInterme(utilerias.getString(map.get("CVE_INTERME")));
            	beanInstitucion.setNombre(utilerias.getString(map.get("NOMBRE_CORTO")));
            	beanInstitucion.setNumBanxico(utilerias.getString(map.get("NUM_BANXICO")));
            	listBeanInstitucion.add(beanInstitucion);    	
            }
         }
         beanResConsInstDAO.setListBeanInstitucion(listBeanInstitucion);
    	 beanResConsInstDAO.setCodError(responseDTO.getCodeError());
         beanResConsInstDAO.setMsgError(responseDTO.getMessageError());
      
      } catch (ExceptionDataAccess e) {
         showException(e);
         beanResConsInstDAO.setCodError(Errores.EC00011B);
      }
      return beanResConsInstDAO;
   }
	
	
	  @Override
	  public BeanResConsInstPOACOADAO buscarInstitucionesPOACOA(BeanConsReqInst beanConsReqInst, 
			   ArchitechSessionBean architechSessionBean){
		  String vacio ="";
		  int cont =0;
		  boolean isFirst = true;
		  StringBuilder build =  new StringBuilder();
		  List<Object> listParametros = new ArrayList<Object>();
	      final BeanResConsInstPOACOADAO beanResConsInstPOACOADAO = new BeanResConsInstPOACOADAO();
	      BeanInstitucionPOACOA beanInstitucionPOACOA = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<BeanInstitucionPOACOA> listBeanInstitucion = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	    	  
	    	  if(beanConsReqInst.getNumBanxico() != null && (!vacio.equals(beanConsReqInst.getNumBanxico().trim()))){  
	    		  build.append(" WHERE CVE_INST = ? ");
	    		  listParametros.add(beanConsReqInst.getNumBanxico());
	    		  isFirst = false;
	    	  }
	    	  if(beanConsReqInst.getNombre() != null && (!vacio.equals(beanConsReqInst.getNombre().trim()))){
	    		  if(!isFirst){
	    			  build.append(" AND TRIM(NOMBRE) = ? ");
	    		  }else{
	    			  build.append(" WHERE TRIM(NOMBRE) = ? ");
	    		  }
	    		  listParametros.add(beanConsReqInst.getNombre().trim());
	    	  }
	    	  responseDTO = HelperDAO.consultar(QUERY_SELECT_COUNT_SPEI_PARTICIPANTE+build.toString(), Arrays.asList(
	    			  listParametros.toArray(new Object[listParametros.size()])
		      ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
		          list = responseDTO.getResultQuery();
		          Map<String,Object> map = list.get(0);
		          cont = utilerias.getInteger(map.get("CONT"));
		          beanResConsInstPOACOADAO.setTotalReg(cont);
		          if(cont == 0){
		        	  beanResConsInstPOACOADAO.setCodError(Errores.ED00011V);
		        	  beanResConsInstPOACOADAO.setMsgError(Errores.DESC_ED00011V);
		        	  return beanResConsInstPOACOADAO;
		          }
	    	  }
	    	  listParametros.add(beanConsReqInst.getPaginador().getRegIni());
	    	  listParametros.add(beanConsReqInst.getPaginador().getRegFin());
	    	  responseDTO = HelperDAO.consultar(QUERY_SELECT_SPEI_PARTICIPANTE+build.toString()+QUERY_SELECT_SPEI_PARTICIPANTE_ORDER, Arrays.asList(
	    			  listParametros.toArray(new Object[listParametros.size()])
		      ),HelperDAO.CANAL_GFI_DS, this.getClass().getName()); 
	    	  
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	        	 listBeanInstitucion = new ArrayList<BeanInstitucionPOACOA>();
	            list = responseDTO.getResultQuery();
	            for(Map<String,Object> map:list){
	            	beanInstitucionPOACOA = descomponeConsulta(map);
	            	listBeanInstitucion.add(beanInstitucionPOACOA);    	
	            }
	         }
	         beanResConsInstPOACOADAO.setListBeanInstitucionPOACOA(listBeanInstitucion);
	         beanResConsInstPOACOADAO.setCodError(responseDTO.getCodeError());
	         beanResConsInstPOACOADAO.setMsgError(responseDTO.getMessageError());
	      
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResConsInstPOACOADAO.setCodError(Errores.EC00011B);
	      }
	      return beanResConsInstPOACOADAO;
	   }
	   
    /**
     * Metodo que genera un objeto del tipo BeanInstitucionPOACOA a partir de la respuesta de la consulta
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanInstitucionPOACOA Objeto del tipo BeanInstitucionPOACOA
     */
	private BeanInstitucionPOACOA descomponeConsulta(Map<String,Object> map){
		  Utilerias utilerias = Utilerias.getUtilerias();
		  BeanInstitucionPOACOA beanInstitucionPOACOA = null;
		  beanInstitucionPOACOA = new BeanInstitucionPOACOA();
      	beanInstitucionPOACOA.setCveInterme(utilerias.getString(map.get("CVE_INTERME")));
      	beanInstitucionPOACOA.setParticipantePOA(utilerias.getString(map.get("PARTICIPANTE_POA")));
      	beanInstitucionPOACOA.setValidarCuenta(utilerias.getString(map.get("VALIDA_CUENTA")));
      	beanInstitucionPOACOA.setNombre(utilerias.getString(map.get("NOMBRE")));
      	beanInstitucionPOACOA.setNumBanxico(utilerias.getString(map.get("CVE_INST")));
      	beanInstitucionPOACOA.setCveUsuario(utilerias.getString(map.get("CVE_USUARIO_MODIF")));
      	beanInstitucionPOACOA.setFechaMod(utilerias.getString(map.get("FCH")));
      	return beanInstitucionPOACOA;
	  }
	
	 @Override
     public BeanResGuardaInstDAO guardaInstitucionPOACOA(BeanReqInsertInst beanReqInsertInst, 
    		 ArchitechSessionBean architechSessionBean){
		 int cont =0;
		 Utilerias utilerias = Utilerias.getUtilerias();
		 List<HashMap<String,Object>> list = null;
        final BeanResGuardaInstDAO beanResGuardaInstDAO = new BeanResGuardaInstDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
        	responseDTO = HelperDAO.consultar(QUERY_BUSCA_DUP_INSERT_SPEI_PARTICIPANTE, Arrays.asList(new Object[]{
			beanReqInsertInst.getNumBanxico(),
			}),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    		list = responseDTO.getResultQuery();
	    		Map<String,Object> map = list.get(0);
	    		cont = utilerias.getInteger(map.get("CONT"));
	    		if(cont>0){
		          	beanResGuardaInstDAO.setCodError(Errores.ED00061V);
		          	return beanResGuardaInstDAO;
	          	}	  
	    	 }
        	
           responseDTO = HelperDAO.insertar(QUERY_INSERT_SPEI_PARTICIPANTE, 
		   Arrays.asList(new Object[]{
				   beanReqInsertInst.getNumBanxico(),
				   beanReqInsertInst.getCveInterme(),
				   architechSessionBean.getUsuario(),
				   beanReqInsertInst.getNombre()
				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaInstDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaInstDAO;
     }
	 
	 @Override
     public BeanHabDesParticipanteDAO habDesParticipantePOA(BeanReqHabDesInst beanReqHabDesInst, 
    		 ArchitechSessionBean architechSessionBean){
        final BeanHabDesParticipanteDAO beanHabDesParticipanteDAO = new BeanHabDesParticipanteDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.actualizar(QUERY_UPDATE_PARTICIPANTE_POA_SPEI_PARTICIPANTE, 
		   Arrays.asList(new Object[]{
				   beanReqHabDesInst.getHabDesValor(),
				   architechSessionBean.getUsuario(),
				   beanReqHabDesInst.getHabDesNumBanxico()
				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanHabDesParticipanteDAO.setCodError(responseDTO.getCodeError());
           beanHabDesParticipanteDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanHabDesParticipanteDAO.setCodError(Errores.EC00011B);
        }
        return beanHabDesParticipanteDAO;
     }
     @Override
     public BeanHabDesParticipanteDAO habDesValidacionCuenta(BeanReqHabDesInst beanReqHabDesInst, 
    		 ArchitechSessionBean architechSessionBean){
        final BeanHabDesParticipanteDAO beanHabDesParticipanteDAO = new BeanHabDesParticipanteDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.actualizar(QUERY_UPDATE_VALIDA_CUENTA_SPEI_PARTICIPANTE, 
		   Arrays.asList(new Object[]{
				   beanReqHabDesInst.getHabDesValor(),
				   architechSessionBean.getUsuario(),
				   beanReqHabDesInst.getHabDesNumBanxico()
				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanHabDesParticipanteDAO.setCodError(responseDTO.getCodeError());
           beanHabDesParticipanteDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanHabDesParticipanteDAO.setCodError(Errores.EC00011B);
        }
        return beanHabDesParticipanteDAO;
     }
     
 	/**
 	 * Metodo que sirve para marcar los registros como eliminados
 	 * 
 	 * @param beanInstitucionPOACOA
 	 *            Objeto del tipo @see BeanInstitucionPOACOA
 	 * @param architechSessionBean
 	 *            Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanResElimInstPOACOADAO Objeto del tipo
 	 *         BeanResElimInstPOACOADAO
 	 * */
 	public BeanResElimInstPOACOADAO elimCatInstFinancieras(
 			BeanInstitucionPOACOA beanInstitucionPOACOA,
 			ArchitechSessionBean architechSessionBean) {
 		final BeanResElimInstPOACOADAO beanResElimInstPOACOADAO = new BeanResElimInstPOACOADAO();
 		ResponseMessageDataBaseDTO responseDTO = null;
 		try {
 				responseDTO = HelperDAO.actualizar(QUERY_DELETE_SPEI_PARTICIPANTE,
 						Arrays.asList(new Object[] {
 								beanInstitucionPOACOA.getNumBanxico()
 						}),
 						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
 				beanResElimInstPOACOADAO.setCodError(responseDTO
 						.getCodeError());
 				beanResElimInstPOACOADAO.setMsgError(responseDTO
 						.getMessageError());

 		} catch (ExceptionDataAccess e) {
 			showException(e);
 			beanResElimInstPOACOADAO.setCodError(Errores.EC00011B);
 		}
 		return beanResElimInstPOACOADAO;
 	}
	
}
