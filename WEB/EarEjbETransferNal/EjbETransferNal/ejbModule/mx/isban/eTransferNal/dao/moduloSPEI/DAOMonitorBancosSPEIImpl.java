/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorBancosSPEIImpl.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 11:23:37	    1396920: Moises Navarro                 TCS         Creacion de DAOMonitorBancosSPEIImpl.java
 */
package mx.isban.eTransferNal.dao.moduloSPEI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.global.BeanNuevoPaginador;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanConsolEstatBancoSPEI;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanEstatusBancoSPEI;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanResponseMonBancSPEIDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Descripcion:Clase que define el comportamiento de los metodos de acceso a datos para el monitor de bancos SPEI
 * DAOMonitorBancosSPEIImpl.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    DAOMonitorBancosSPEIImpl.java
*/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorBancosSPEIImpl  extends Architech implements DAOMonitorBancosSPEI{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = -5286797290861412635L;
	/** Atributo que representa la variable QUERY_LISTA_MECANISMOS del tipo String */
	private static final StringBuilder QUERY_LISTA_MONBANC= new StringBuilder()
			.append("SELECT T.CVE_BANCO, T.ESTADO_INSTIT, T.ESTADO_RECEP, T.CVE_INTERME, T.NOMBRE_BANCO FROM ")
			.append("(SELECT T.CVE_BANCO, T.ESTADO_INSTIT, T.ESTADO_RECEP,T. CVE_INTERME, T.NOMBRE_BANCO, ROWNUM R FROM( ")
			.append("SELECT CVE_BANCO, ESTADO_INSTIT, ESTADO_RECEP, CVE_INTERME, NOMBRE_BANCO FROM TRAN_SPEI_BANC ORDER BY CVE_BANCO ")
			.append(")T )T WHERE R BETWEEN ? AND ?");
	/**Query que genera el consolidado de estatus
	 * de los bancos de monitor SPEI*/
	/** Atributo que representa la variable QUERY_RESUMEN_MONBANC del tipo StringBuilder */
	private static final StringBuilder QUERY_RESUMEN_MONBANC= new StringBuilder()
			.append("SELECT CONECTADO,DESCONECTADO,BLOQUEADO,BAJA,RECEPTIVO,NORECEPTIVO,INTERMEDIARIANOREG,TOTAL FROM ")
			.append("(SELECT COUNT(CVE_BANCO) CONECTADO FROM TRAN_SPEI_BANC WHERE ESTADO_INSTIT='C'), ")
			.append("(SELECT COUNT(CVE_BANCO) DESCONECTADO FROM TRAN_SPEI_BANC WHERE ESTADO_INSTIT='D'), ")
			.append("(SELECT COUNT(CVE_BANCO) BLOQUEADO FROM TRAN_SPEI_BANC WHERE ESTADO_INSTIT='L'), ")
			.append("(SELECT COUNT(CVE_BANCO) BAJA FROM TRAN_SPEI_BANC WHERE ESTADO_INSTIT='B'), ")
			.append("(SELECT COUNT(CVE_BANCO) RECEPTIVO FROM TRAN_SPEI_BANC WHERE ESTADO_RECEP='R'), ")
			.append("(SELECT COUNT(CVE_BANCO) NORECEPTIVO FROM TRAN_SPEI_BANC WHERE ESTADO_RECEP='N'), ")
			.append("(SELECT COUNT(CVE_BANCO) INTERMEDIARIANOREG FROM TRAN_SPEI_BANC WHERE CVE_INTERME IS NULL), ")
			.append("(SELECT COUNT(CVE_BANCO) TOTAL FROM TRAN_SPEI_BANC) ");
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.moduloSPEI.DAOMonitorBancosSPEI#consultaMonitorBancosSPEI(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResponseMonBancSPEIDAO consultaMonitorBancosSPEI(ArchitechSessionBean architech, BeanNuevoPaginador paginador) {
		/**Se generan onbjetos necesarios*/
		BeanResponseMonBancSPEIDAO response=new BeanResponseMonBancSPEIDAO();
	    List<HashMap<String,Object>> listResult= new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    Utilerias utilerias = Utilerias.getUtilerias();
		try{

	    	/**consultar total de operaciones*/
	    	responseDTO = HelperDAO.consultar(QUERY_LISTA_MONBANC.toString(), 
	    			Arrays.asList(new Object[] { paginador.getRegIni(), paginador.getRegFin() }),
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

        	/**obtner lista de resultados del query*/
	    	listResult = responseDTO.getResultQuery();
	    	List<BeanEstatusBancoSPEI> listaBanc= new ArrayList<BeanEstatusBancoSPEI>();
	    	/**Se mapea resultado*/
	    	for(HashMap<String,Object> map:listResult){
	    		/**Se genera BEAN para guardar un banco individual*/
	    		BeanEstatusBancoSPEI banco = new BeanEstatusBancoSPEI();
	    		
	    		banco.setBanco(utilerias.getString(map.get("CVE_BANCO")));
	    		banco.setConectado(utilerias.getString(map.get("ESTADO_INSTIT")));
	    		banco.setIntermediario(utilerias.getString(map.get("CVE_INTERME")));
	    		banco.setNombre(utilerias.getString(map.get("NOMBRE_BANCO")));
	    		banco.setReceptivo(utilerias.getString(map.get("ESTADO_RECEP")));
	    		/**Se determina semaforo de conexion*/
	    		determinaSemaforoConexion(banco);
	    		/**Se determina semaforo de recepcion*/
	    		determinaSemaforoReceptivo(banco);
				/**se agrega el bean a la lista*/
	    		listaBanc.add(banco);
			}
        	response.setListaBancos(listaBanc);
			/**se obtiene el codigo de error del query*/
        	response.setCodError(responseDTO.getCodeError());
        	response.setMsgError(responseDTO.getMessageError());
        	
		}catch(ExceptionDataAccess e){
    		/**se envia codigo error EC00011B*/
			showException(e);
			/**Se guarda codigo de error*/
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
    	}
		/**Se retorna la respuesta*/
		return response;
	}
	
	
	/**
	 * Descripcion   : Metodo que establece la descricion y semaforo de los estatus de conexion
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 19/03/2020
	 * @param banco banco a establecer
	 */
	private void determinaSemaforoConexion(BeanEstatusBancoSPEI banco){
		if("D".equalsIgnoreCase(banco.getConectado())){
			/**opcion desconectado*/
			banco.setDescripcionConectado("Desconectado");
			banco.setSemaforoConectado("#ff0000");
		}else if("C".equalsIgnoreCase(banco.getConectado())){
			/**opcion conectado*/
			banco.setDescripcionConectado("Conectado");
			banco.setSemaforoConectado("#00ff4c");
		}else if("L".equalsIgnoreCase(banco.getConectado())){
			/**opcion bloqueado*/
			banco.setDescripcionConectado("Bloqueado");
			banco.setSemaforoConectado("#0033ff");
		}else if("B".equalsIgnoreCase(banco.getConectado())){
			/**opcion baja*/
			banco.setDescripcionConectado("Baja");
			banco.setSemaforoConectado("#fff700");
		}else{
			/**opcion por defecto*/
			banco.setDescripcionConectado("");
			banco.setSemaforoConectado("#ffffff");
		}
	}
	
	/**
	 * Descripcion   : Metodo que es semaforo serecptivo para un banco del SPEI
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 19/03/2020
	 * @param banco banco a valorar
	 */
	private void determinaSemaforoReceptivo(BeanEstatusBancoSPEI banco){
		if("N".equalsIgnoreCase(banco.getReceptivo())){
			/**opcion no receptivo*/
			banco.setDescripcionReceptivo("No Receptivo");
			banco.setSemaforoReceptivo("#ff0000");
		}else if("R".equalsIgnoreCase(banco.getReceptivo())){
			/**opcion receptivo*/
			banco.setDescripcionReceptivo("Receptivo");
			banco.setSemaforoReceptivo("#00ff4c");
		}else{
			/**opcion por defecto*/
			banco.setDescripcionReceptivo("");
			banco.setSemaforoReceptivo("#ffffff");
		}
	}


	@Override
	public BeanResponseMonBancSPEIDAO resumenMonitorBancosSPEI(ArchitechSessionBean architech) {
		/**Se generan objetos necesarios*/
		BeanResponseMonBancSPEIDAO response=new BeanResponseMonBancSPEIDAO();
	    List<HashMap<String,Object>> listResult= new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    Utilerias utilerias = Utilerias.getUtilerias();
		try{
			List<Object> parametros = new ArrayList<Object>();

	    	/**consultar total de operaciones*/
	    	responseDTO = HelperDAO.consultar(QUERY_RESUMEN_MONBANC.toString(), parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

        	/**obtner lista de resultados del query*/
	    	listResult = responseDTO.getResultQuery();
	    	/**Se mapea el resultado hacia el bean*/
	    	for(HashMap<String,Object> map:listResult){
	    		/**Se genera bean de estatus de consolidado*/
	    		BeanConsolEstatBancoSPEI resumen = new BeanConsolEstatBancoSPEI();
	    		resumen.setBajas(Integer.parseInt(utilerias.getString(map.get("BAJA"))));
	    		resumen.setBloqueados(Integer.parseInt(utilerias.getString(map.get("BLOQUEADO"))));
	    		resumen.setConectados(Integer.parseInt(utilerias.getString(map.get("CONECTADO"))));
	    		resumen.setDesconectados(Integer.parseInt(utilerias.getString(map.get("DESCONECTADO"))));
	    		resumen.setIntermediarioNoReg(Integer.parseInt(utilerias.getString(map.get("INTERMEDIARIANOREG"))));
	    		resumen.setNoReceptivos(Integer.parseInt(utilerias.getString(map.get("NORECEPTIVO"))));
	    		resumen.setReceptivos(Integer.parseInt(utilerias.getString(map.get("RECEPTIVO"))));
	    		resumen.setTotal(Integer.parseInt(utilerias.getString(map.get("TOTAL"))));
	    		response.setConsolidadoEstatus(resumen);
			}
			/**se obtiene el codigo de error del query*/
        	response.setCodError(responseDTO.getCodeError());
        	response.setMsgError(responseDTO.getMessageError());
        	
		}catch(ExceptionDataAccess e){
    		/**se envia codigo error EC00011B*/
			showException(e);
			/**Se guarda codigo de error*/
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
    	}
		/**Se retorna la respuesta*/
		return response;
	}

}
