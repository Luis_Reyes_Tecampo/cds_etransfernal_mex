/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaSaldoBancoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBancoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la consulta de saldos por banco
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaSaldoBancoImpl extends Architech implements DAOConsultaSaldoBanco {

	/** Constante privada del tipo StringBuilder que almacena el query para 
	 * obtener los saldos*/
	private static final StringBuilder CONSULTA_SALDO_BANCO =
		new StringBuilder().append("SELECT TMPTBL.* FROM (SELECT TBL.*,ROWNUM R,C.* FROM ")
						   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),")
						   .append("SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ")
						   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ")
						   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS = 'CO'")
						   .append("GROUP BY CVE_INTERME_REC UNION ")
						   .append("SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ")
						   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = 'ENTIDAD_SPID') AND ESTATUS_BANXICO='LQ' ")
						   .append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ")
						   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME, CIF.NOMBRE_CORTO [ORDENAMIENTO]) TBL, ")
						   .append("(SELECT COUNT(*) CONT FROM ")
						   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ")
						   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ")
						   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS='CO' GROUP BY CVE_INTERME_REC ")
						   .append("UNION SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ")
						   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = 'ENTIDAD_SPID') AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ")
						   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME,CIF.NOMBRE_CORTO) )C ) TMPTBL WHERE R BETWEEN ? AND ?");
	
	/** Constante privada del tipo StringBuilder que almacena el query para 
	 * obtener los saldos*/
	private static final StringBuilder CONSULTA_SALDO_BANCO_MONTOS =
			new StringBuilder().append("SELECT sum(saldo) as MONTO_TOTAL FROM ")
			   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),")
			   .append("SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ")
			   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ")
			   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS = 'CO'")
			   .append("GROUP BY CVE_INTERME_REC UNION ")
			   .append("SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ")
			   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = 'ENTIDAD_SPID') AND ESTATUS_BANXICO='LQ' ")
			   .append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ")
			   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME, CIF.NOMBRE_CORTO [ORDENAMIENTO]) TBL, ")
			   .append("(SELECT COUNT(*) CONT FROM ")
			   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ")
			   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ")
			   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS='CO' GROUP BY CVE_INTERME_REC ")
			   .append("UNION SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ")
			   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = 'ENTIDAD_SPID') AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ")
			   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME,CIF.NOMBRE_CORTO) )C ");
						   					
	/**
	 * Consulta para buscar todos los saldos, se usa para exportar todo a archivo
	 */	
	private static final StringBuilder TOTAL_CONSULTA_SALDO_BANCO =
			new StringBuilder().append("SELECT INTERME || ',' || CIF.NOMBRE_CORTO || ',' || SUM(VOLUMEN_ENV) || ',' || SUM(IMPORTE_ENV) || ',' || ")
			   .append("SUM(VOLUMEN_REC) || ',' || SUM(IMPORTE_REC) || ',' || (SUM(IMPORTE_REC) - SUM(IMPORTE_ENV)) ")
			   .append("FROM (")
			   		.append("SELECT CVE_INTERME_REC INTERME, COUNT(*) VOLUMEN_ENV, ")
			   		.append("SUM(IMPORTE_ABONO) IMPORTE_ENV, 0 VOLUMEN_REC, 0.0 IMPORTE_REC ")
			   		.append("FROM TRAN_MENSAJE ")
			   			.append("WHERE CVE_MECANISMO IN ('SPID','DEVSPID') ")
			   			.append("AND ESTATUS = 'CO' ")
			   			.append("GROUP BY CVE_INTERME_REC ")
			   			.append("UNION ")
			   				.append("SELECT CVE_INTERME_ORD INTERME, 0 VOLUMEN_ENV, ")
			   				.append("0.0 IMPORTE_ENV, COUNT(*) VOLUMEN_REC, SUM(MONTO) IMPORTE_REC ")
			   				.append("FROM TRAN_SPID_REC ")
			   					.append("WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = 'ENTIDAD_SPID') ")
			   					.append("AND ESTATUS_BANXICO = 'LQ' ")
			   					.append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') ")
			   					.append("GROUP BY CVE_INTERME_ORD ")
			   					.append(") TM ")
			   					.append("JOIN COMU_INTERME_FIN CIF on CIF.CVE_INTERME = TM.INTERME ")
			   					.append("GROUP BY INTERME, CIF.NOMBRE_CORTO");
	
	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos los saldos exportados
	 */
	private static final StringBuilder INSERT_TODOS_CONSULTA_SALDO_BANCO = 
			new StringBuilder().append("INSERT INTO TRAN_SPEI_EXP_CDA ")
							   .append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) ")
                               .append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -4643659367323084429L;
	
	/**Metodo que sirve para consultar los saldos
	   * @param beanPaginador Objeto del tipo BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return BeanResConsultaSaldoBancoDAO  
	   */
	@Override
	public BeanResConsultaSaldoBancoDAO obtenerConsultaSaldoBanco(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {

		final BeanResConsultaSaldoBancoDAO beanResConsultaSaldoBancoDAO= new BeanResConsultaSaldoBancoDAO();
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanConsultaSaldoBanco> listaBeanConsultaSaldoBanco = new ArrayList<BeanConsultaSaldoBanco>();
    	
    	try {
    		String ordenamiento = "";
    		
    		if (sortField != null && !"".equals(sortField)){
    			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
    		}
    		
        	responseDTO = HelperDAO.consultar(CONSULTA_SALDO_BANCO.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays.asList(
        			new Object[] {beanPaginador.getRegIni(),
  						  beanPaginador.getRegFin()}), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();        		 
        		 if(!list.isEmpty()){      			 
        			 llenarConsultaSaldoBancos(beanResConsultaSaldoBancoDAO, list, utilerias,
							listaBeanConsultaSaldoBanco);
        		 }
        	}

        	beanResConsultaSaldoBancoDAO.setListaBeanConsultaSaldoBanco(listaBeanConsultaSaldoBanco);
        	beanResConsultaSaldoBancoDAO.setCodError(responseDTO.getCodeError());
        	beanResConsultaSaldoBancoDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return beanResConsultaSaldoBancoDAO;
	}
	
	/**Metodo que sirve para consultar los saldos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return BeanResConsultaSaldoBancoDAO  
	   */
	public BigDecimal obtenerConsultaSaldoBancoMontos(
			 ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
  	ResponseMessageDataBaseDTO responseDTO = null;
  	BigDecimal totalImporte = BigDecimal.ZERO;
  	try {
  		String ordenamiento = "";
  		
  		if (sortField != null && !"".equals(sortField)){
  			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
  		}
  		
      	responseDTO = HelperDAO.consultar(CONSULTA_SALDO_BANCO_MONTOS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays.asList(
      			new Object[] { }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
      	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
      		totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
      	}
      	
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  	}
		return totalImporte;
	}


	/**
	 * Metodo para llenar la consulta de Saldo Bancos
	 * 
	 * @param beanResConsultaSaldoBancoDAO objeto del tipo BeanResConsultaSaldoBancoDAO
	 * @param list objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias objeto del tipo Utilerias
	 * @param listaBeanConsultaSaldoBanco objeto del tipo List<BeanConsultaSaldoBanco>
	 */
	private void llenarConsultaSaldoBancos(final BeanResConsultaSaldoBancoDAO beanResConsultaSaldoBancoDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias,
			List<BeanConsultaSaldoBanco> listaBeanConsultaSaldoBanco) {
		BeanConsultaSaldoBanco beanConsultaSaldoBanco;
		Integer zero = Integer.valueOf(0);
		
		for(Map<String,Object> mapResult : list){
			 beanConsultaSaldoBanco = new BeanConsultaSaldoBanco();
			 beanConsultaSaldoBanco.setClaveBanco(utilerias.getString(mapResult.get("INTERME")));
			 beanConsultaSaldoBanco.setNombreBanco(utilerias.getString(mapResult.get("NOMBRE_CORTO")));
			 beanConsultaSaldoBanco.setVolumenOperacionesEnviadas(utilerias.getInteger(mapResult.get("SUM(VOLUMEN_ENV)")));
			 beanConsultaSaldoBanco.setImporteOperacionesEnviadas(utilerias.getBigDecimal(mapResult.get("SUM(IMPORTE_ENV)")));
			 beanConsultaSaldoBanco.setVolumenOperacionesRecibidas(utilerias.getInteger(mapResult.get("SUM(VOLUMEN_REC)")));
			 beanConsultaSaldoBanco.setImporteOperacionesRecibidas(utilerias.getBigDecimal(mapResult.get("SUM(IMPORTE_REC)")));
			 beanConsultaSaldoBanco.setSaldo(utilerias.getString(mapResult.get("SALDO")));
			 
			 listaBeanConsultaSaldoBanco.add(beanConsultaSaldoBanco);
			 if (zero.equals(beanResConsultaSaldoBancoDAO.getTotalReg())) {
				 beanResConsultaSaldoBancoDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			 }
		 }
	}
	
	
	/**Metodo que sirve para exportar todos los saldos
	   * @param beanReqConsultaSaldoBanco Objeto del tipo BeanReqConsultaSaldoBanco
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsultaSaldoBancoDAO
	   */
	public BeanResConsultaSaldoBancoDAO exportarTodoConsultaSaldoBanco(BeanReqConsultaSaldoBanco beanReqConsultaSaldoBanco, 
    		ArchitechSessionBean architechSessionBean){
    	StringBuilder builder = new StringBuilder();
    	final BeanResConsultaSaldoBancoDAO beanResConsultaSaldoBancoDAO = new BeanResConsultaSaldoBancoDAO();
    	Utilerias utilerias = Utilerias.getUtilerias();
        final Date fecha = new Date();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
        	builder.append(TOTAL_CONSULTA_SALDO_BANCO); 
        	final String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        	final String nombreArchivo = "SPID_CON_SAL_BAN_"+fechaActual+".csv";
        	beanResConsultaSaldoBancoDAO.setNombreArchivo(nombreArchivo);
    	   
        	responseDTO = HelperDAO.insertar(INSERT_TODOS_CONSULTA_SALDO_BANCO.toString(), 
        			Arrays.asList(new Object[]{architechSessionBean.getUsuario(),
        				                     architechSessionBean.getIdSesion(),
        				                     "SPID",
        				                     "CONSULTA_SALDO_BANCO",
        				                     nombreArchivo,
        				                     beanReqConsultaSaldoBanco.getTotalReg(),
        				                     "PE",
        				                     builder.toString(),
        									 " BANCO, NOMBRE, OPERACIONES ENVIADAS VOLUMEN, OPERACIONES ENVIADAS IMPORTE, "
        				                     +"OPERACIONES RECIBIDAS VOLUMEN, OPERACIONES RECIBIDAS IMPORTE, SALDO"}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           
        	beanResConsultaSaldoBancoDAO.setCodError(responseDTO.getCodeError());
        	beanResConsultaSaldoBancoDAO.setMsgError(responseDTO.getMessageError());

        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResConsultaSaldoBancoDAO.setCodError(Errores.EC00011B); 
        }
        
        return beanResConsultaSaldoBancoDAO;
	}
}
