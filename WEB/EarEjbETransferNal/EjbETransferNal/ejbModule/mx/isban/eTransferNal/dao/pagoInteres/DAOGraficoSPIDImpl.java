/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGraficoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    31/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.pagoInteres;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGraficoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanTotalesGraficoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.apache.log4j.Logger;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGraficoSPIDImpl extends Architech implements DAOGraficoSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 9137187258907681064L;
	/**
	 * Propiedad del tipo Logger que almacena el valor de LOG
	 */
	private static final Logger LOG = Logger.getLogger(DAOGraficoSPIDImpl.class);
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_RANGO
	 */
	private static final String QUERY_RANGO = "SELECT VALOR_FALTA FROM TRAN_PARAMETROS WHERE  CVE_PARAMETRO='GRAF_SPID_RANGO' AND CVE_PARAM_FALTA='RANGO_SPID'";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_PAGINACION
	 */
	private static final String QUERY_PAGINACION = "SELECT VALOR_FALTA from TRAN_PARAMETROS t WHERE CVE_PARAMETRO='GRAFICACION_SPID' AND CVE_PARAM_FALTA='PAGINACION_SPID'";
	
	/**
	 * Propiedad del tipo String que almacena el valor de HEADER
	 */
	private static final String HEADER = " SELECT * FROM ( SELECT R.DIFERENCIA,FECHA, ROWNUM RNUM, COUNT (*) OVER () CONT, SUM(importe_cargo) over() IMP_TOTAL from( ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FOOTER
	 */
	private static final String FOOTER = " ) r ) WHERE RNUM BETWEEN ? AND ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ENVIOS_NO_MOVILES
	 */
	private static final String ENVIOS_NO_MOVILES= 
		" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_ACUSE, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_ACUSE,'HH24:MI:SS') FECHA, "+
		"   case when dif_acuse_aprobacion is null then "+
		"     (SYSDATE - FCH_APROBACION)*86400 "+
		"   else "+
		"     dif_acuse_aprobacion  "+
		"   end DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_ACUSE, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"   dense_rank() over (partition by trunc(TSH.FCH_ACUSE, 'MI') "+
		"      order by case when dif_acuse_aprobacion is null then (SYSDATE - FCH_APROBACION)*86400 else dif_acuse_aprobacion end desc) dr "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='E' "+
		"     AND TSH.tipo_pago              =1 "+
		"     AND tm.cve_operacion LIKE 'TT%' "+
		"     AND TSH.FCH_ACUSE             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"     AND TSH.FCH_ACUSE             <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de ENVIOS_DEV_MOVILES
	 */
	private static final String ENVIOS_DEV_MOVILES= " SELECT * "+
		" FROM"+
		"  (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ("+
		" SELECT * FROM ("+
		" SELECT A.*, "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_ACUSE, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM "+
		" (select * from ("+
		" SELECT TO_CHAR(TSH.FCH_ACUSE,'HH24:MI:SS') FECHA,"+
		"  case when dif_recepcion_acuse is null then"+
		"    (SYSDATE - FCH_RECEPCION)*86400"+
		"  else"+
		"    dif_recepcion_acuse "+
		"  END DIFERENCIA,"+
		"  TSH.referencia,TSH.FCH_ACUSE,"+
		"  SUM(importe_cargo) over() IMP_TOTAL,"+
		"  COUNT (*) OVER () CONT,"+
		"  DENSE_RANK() OVER (PARTITION BY TRUNC(TSH.FCH_ACUSE, 'MI')"+
		"      ORDER BY CASE WHEN DIF_RECEPCION_ACUSE IS NULL THEN (SYSDATE - FCH_RECEPCION)*86400 ELSE DIF_RECEPCION_ACUSE END DESC) DR"+
		"    FROM tran_spid_horas TSH,"+
		"      tran_mensaje TM,"+
		"      tran_spid_rec R"+
		"    WHERE TM.referencia            = TSH.referencia"+
		"    AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR'"+
		"    AND TSH.tipo                   ='D' "+
		"    AND TSH.referencia             = R.refe_transfer"+
		"    AND TSH.fch_operacion          = R.fch_operacion"+
		"    AND TSH.tipo_pago              =0"+
		"    AND R.tipo_cuenta_ord         =10"+
		"    AND TM.cve_transfe             = '974'"+
		"    AND TSH.FCH_ACUSE             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI')"+
		"    AND TSH.FCH_ACUSE             <= To_date(?, 'YYYY-MM-DD HH24:MI')"+
		"    )"+
		"    WHERE DR = 1) A)"+
		"    WHERE DR1 = 1"+
		" )"+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT"+
		"  )"+
		" WHERE RNUM BETWEEN ? AND ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ENVIOS_DEV_NO_MOVILES
	 */
	private static final String ENVIOS_DEV_NO_MOVILES = " SELECT * "+
		" FROM"+
		"  (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ("+
		" SELECT * FROM ("+
		" SELECT A.*, "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_ACUSE, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM "+
		" (select * from ("+
		" SELECT TO_CHAR(TSH.FCH_ACUSE,'HH24:MI:SS') FECHA,"+
		"  case when dif_recepcion_acuse is null then"+
		"    (SYSDATE - FCH_RECEPCION)*86400"+
		"  else"+
		"    dif_recepcion_acuse "+
		"  END DIFERENCIA,"+
		"  TSH.referencia,TSH.FCH_ACUSE,"+
		"  SUM(importe_cargo) over() IMP_TOTAL,"+
		"  COUNT (*) OVER () CONT,"+
		"  DENSE_RANK() OVER (PARTITION BY TRUNC(TSH.FCH_ACUSE, 'MI')"+
		"      ORDER BY CASE WHEN DIF_RECEPCION_ACUSE IS NULL THEN (SYSDATE - FCH_RECEPCION)*86400 ELSE DIF_RECEPCION_ACUSE END DESC) DR"+
		"    FROM tran_spid_horas TSH,"+
		"      tran_mensaje TM,"+
		"      tran_spid_rec R"+
		"    WHERE TM.referencia            = TSH.referencia"+
		"    AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR'"+
		"    AND TSH.tipo                   ='D' "+
		"    AND TSH.referencia             = R.refe_transfer"+
		"    AND TSH.fch_operacion          = R.fch_operacion"+
		"    AND TSH.tipo_pago              =0"+
		"    AND R.tipo_cuenta_ord         <>10"+
		"    AND TM.cve_transfe             = '974'"+
		"    AND TSH.FCH_ACUSE             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI')"+
		"    AND TSH.FCH_ACUSE             <= To_date(?, 'YYYY-MM-DD HH24:MI')"+
		"    )"+
		"    WHERE DR = 1) A)"+
		"    WHERE DR1 = 1"+
		" )"+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT"+
		"  )"+
		" WHERE RNUM BETWEEN ? AND ?";
			
	
	/**
	 * Propiedad del tipo String que almacena el valor de RECEPCIONES_NO_MOVILES
	 */
	private static final String RECEPCIONES_NO_MOVILES =
		" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA, "+
		"   case when DIF_CAPTURA_RECEPCION is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     DIF_CAPTURA_RECEPCION  "+
		"   end DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_CAPTURA, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"     dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI') "+
		"         order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='R' "+
		"     AND TSH.tipo_pago              = R.tipo_pago "+
		"     AND TSH.referencia             = R.refe_transfer  "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =1 "+
		"     AND R.tipo_cuenta_ord         <>10 "+
		"     AND TM.cve_transfe             = '944' "+
		"     AND TSH.FCH_CAPTURA             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"     AND TSH.FCH_CAPTURA             <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de RECEPCIONES_MOVILES
	 */
	private static final String RECEPCIONES_MOVILES =" SELECT * "+
		" FROM"+
		"  (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ("+
		" SELECT * FROM ("+
		" SELECT A.*, "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM "+
		" (select * from ("+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA,"+
		"  case when DIF_CAPTURA_RECEPCION is null then"+
		"    (SYSDATE - FCH_RECEPCION)*86400"+
		"  else"+
		"    DIF_CAPTURA_RECEPCION "+
		"  end DIFERENCIA,"+
		"  TSH.referencia,TSH.FCH_CAPTURA,"+
		"  SUM(importe_cargo) over() IMP_TOTAL,"+
		"  COUNT (*) OVER () CONT,"+
		"    dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI')"+
		"        order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr"+
		"    FROM tran_spid_horas TSH,"+
		"      tran_mensaje TM,"+
		"      tran_spid_rec R"+
		"    WHERE TM.referencia            = TSH.referencia"+
		"    AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR'"+
		"    AND TSH.tipo                   ='R'"+
		"    AND TSH.tipo_pago              = R.tipo_pago"+
		"    AND TSH.referencia             = R.refe_transfer "+
		"    AND TSH.fch_operacion          = R.fch_operacion"+
		"    AND TSH.tipo_pago              =1"+
		"    AND R.tipo_cuenta_ord         =10"+
		"    AND TM.cve_transfe             = '944'"+
		"    AND TSH.FCH_CAPTURA             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI')"+
		"    AND TSH.FCH_CAPTURA             <= To_date(?, 'YYYY-MM-DD HH24:MI')"+
		"    )"+
		"    WHERE DR = 1) A)"+
		"    WHERE DR1 = 1"+
		" )"+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT"+
		"  )"+
		" WHERE RNUM BETWEEN ? AND ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCIONES_RECIBIDAS NO MOVILES
	 */
	private static final String DEVOLUCIONES_RECIBIDAS_NO_MOV =" SELECT * "+
		" FROM"+
		"  (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ("+
		" SELECT * FROM ("+
		" SELECT A.*, "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM "+
		" (select * from ("+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA,"+
		"  case when DIF_CAPTURA_RECEPCION is null then"+
		"    (SYSDATE - FCH_RECEPCION)*86400"+
		"  else"+
		"    DIF_CAPTURA_RECEPCION "+
		"  end DIFERENCIA,"+
		"  TSH.referencia,TSH.FCH_CAPTURA,"+
		"  SUM(importe_cargo) over() IMP_TOTAL,"+
		"  COUNT (*) OVER () CONT,"+
		"    dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI')"+
		"        order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr"+
		"    FROM tran_spid_horas TSH,"+
		"      tran_mensaje TM,"+
		"      tran_spid_rec R"+
		"    WHERE TM.referencia            = TSH.referencia"+
		"    AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR'"+
		"    AND TSH.tipo                   ='R'"+
		"    AND TSH.tipo_pago              = R.tipo_pago"+
		"    AND TSH.referencia             = R.refe_transfer"+
		"    AND TSH.fch_operacion          = R.fch_operacion"+
		"    AND TSH.tipo_pago              =0"+
		"    and NVL(R.TIPO_CUENTA_ORD,1)         <>10"+
		"    AND TM.CVE_TRANSFE             = '984'"+
		"      AND TSH.FCH_CAPTURA           >= TO_DATE(?, 'YYYY-MM-DD HH24:MI')"+
		"      AND TSH.FCH_CAPTURA           <= To_date(?, 'YYYY-MM-DD HH24:MI')"+
		"    )"+
		"    WHERE DR = 1) A)"+
		"    WHERE DR1 = 1"+
		" )"+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT"+
		"  )"+
		" WHERE RNUM BETWEEN ? AND ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCIONES_RECIBIDAS MOVILES
	 */
	private static final String DEVOLUCIONES_RECIBIDAS_MOV =" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA, "+
		"   case when DIF_CAPTURA_RECEPCION is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     DIF_CAPTURA_RECEPCION  "+
		"   end DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_CAPTURA, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"     dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI') "+
		"         order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='R' "+
		"     AND TSH.tipo_pago              = R.tipo_pago "+
		"     AND TSH.referencia             = R.refe_transfer "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =0 "+
		"     and NVL(R.TIPO_CUENTA_ORD,1)         =10 "+
		"     AND TM.CVE_TRANSFE             = '984' "+
		"       AND TSH.FCH_CAPTURA           >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"       AND TSH.FCH_CAPTURA           <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCIONES_RECIBIDAS EXT NO MOVILES
	 */
	private static final String DEVOLUCIONES_RECIBIDAS_EXT_NO_MOV =" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA, "+
		"   case when DIF_CAPTURA_RECEPCION is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     DIF_CAPTURA_RECEPCION  "+
		"   end DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_CAPTURA, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"     dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI') "+
		"         order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='R' "+
		"     AND TSH.tipo_pago              = R.tipo_pago "+
		"     AND TSH.referencia             = R.refe_transfer "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =16 "+
		"     AND NVL(R.TIPO_CUENTA_ORD,1)         <>10 "+
		"     AND TM.CVE_TRANSFE             = '984' "+
		"       AND TSH.FCH_CAPTURA           >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"       AND TSH.FCH_CAPTURA           <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCIONES_RECIBIDAS EXT MOVILES
	 */
	private static final String DEVOLUCIONES_RECIBIDAS_EXT_MOV =" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_CAPTURA, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_CAPTURA,'HH24:MI:SS') FECHA, "+
		"   case when DIF_CAPTURA_RECEPCION is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     DIF_CAPTURA_RECEPCION  "+
		"   end DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_CAPTURA, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"     dense_rank() over (partition by trunc(TSH.FCH_CAPTURA, 'MI') "+
		"         order by case when DIF_CAPTURA_RECEPCION is null then (SYSDATE - FCH_RECEPCION)*86400 else DIF_CAPTURA_RECEPCION end desc) dr "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='R' "+
		"     AND TSH.tipo_pago              = R.tipo_pago "+
		"     AND TSH.referencia             = R.refe_transfer "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =16 "+
		"     AND NVL(R.TIPO_CUENTA_ORD,1)         =10 "+
		"     AND TM.CVE_TRANSFE             = '984' "+
		"       AND TSH.FCH_CAPTURA           >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"       AND TSH.FCH_CAPTURA           <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ENVIOS_DEV_MOVILES EXTEMPORÁNEAS
	 */
	private static final String ENV_DEV_MOV_EXT =" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_ACUSE, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_ACUSE,'HH24:MI:SS') FECHA, "+
		"   case when dif_recepcion_acuse is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     dif_recepcion_acuse  "+
		"   END DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_ACUSE, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"   DENSE_RANK() OVER (PARTITION BY TRUNC(TSH.FCH_ACUSE, 'MI') "+
		"       ORDER BY CASE WHEN DIF_RECEPCION_ACUSE IS NULL THEN (SYSDATE - FCH_RECEPCION)*86400 ELSE DIF_RECEPCION_ACUSE END DESC) DR "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='D' "+
		"     AND TSH.referencia             = R.refe_transfer "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =16 "+
		"     AND R.tipo_cuenta_ord         =10 "+
		"     AND TM.CVE_TRANSFE             = '974' "+
		"     AND TSH.FCH_ACUSE             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"     AND TSH.FCH_ACUSE             <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ENVIOS_DEV_NO_MOVILES EXTEMPORÁNEAS
	 */
	private static final String ENV_DEV_NO_MOV_EXT =" SELECT * "+
		" FROM "+
		"   (select DIFERENCIA, FECHA, row_number() over (order by fecha) as RNUM, CONT, IMP_TOTAL "+
		" FROM ( "+
		" SELECT * FROM ( "+
		" SELECT A.*,  "+
		" DENSE_RANK() OVER (PARTITION BY TRUNC(FCH_ACUSE, 'MI'),DIFERENCIA  ORDER BY REFERENCIA ASC) DR1 "+
		" FROM  "+
		" (select * from ( "+
		" SELECT TO_CHAR(TSH.FCH_ACUSE,'HH24:MI:SS') FECHA, "+
		"   case when dif_recepcion_acuse is null then "+
		"     (SYSDATE - FCH_RECEPCION)*86400 "+
		"   else "+
		"     dif_recepcion_acuse  "+
		"   END DIFERENCIA, "+
		"   TSH.referencia,TSH.FCH_ACUSE, "+
		"   SUM(importe_cargo) over() IMP_TOTAL, "+
		"   COUNT (*) OVER () CONT, "+
		"   DENSE_RANK() OVER (PARTITION BY TRUNC(TSH.FCH_ACUSE, 'MI') "+
		"       ORDER BY CASE WHEN DIF_RECEPCION_ACUSE IS NULL THEN (SYSDATE - FCH_RECEPCION)*86400 ELSE DIF_RECEPCION_ACUSE END DESC) DR "+
		"     FROM tran_spid_horas TSH, "+
		"       tran_mensaje TM, "+
		"       tran_spid_rec R "+
		"     WHERE TM.referencia            = TSH.referencia "+
		"     AND NVL(TM.cve_operacion,' ') <> 'PAGINTAR' "+
		"     AND TSH.tipo                   ='D' "+
		"     AND TSH.referencia             = R.refe_transfer "+
		"     AND TSH.fch_operacion          = R.fch_operacion "+
		"     AND TSH.tipo_pago              =16 "+
		"     AND R.tipo_cuenta_ord         <>10 "+
		"     AND TM.CVE_TRANSFE             = '974' "+
		"     AND TSH.FCH_ACUSE             >= TO_DATE(?, 'YYYY-MM-DD HH24:MI') "+
		"     AND TSH.FCH_ACUSE             <= To_date(?, 'YYYY-MM-DD HH24:MI') "+
		"     ) "+
		"     WHERE DR = 1) A) "+
		"     WHERE DR1 = 1 "+
		" ) "+
		" GROUP BY FECHA, DIFERENCIA,IMP_TOTAL,CONT "+
		"   ) "+
		" WHERE RNUM BETWEEN ? AND ? ";
	
	@Override
	public BeanResGraficoDAO consultaDatosSPID(BeanReqGrafico req,ArchitechSessionBean sessionBean) {
		BeanTotalesGraficoDAO beanTotalesGraficoDAO = new BeanTotalesGraficoDAO(); 
		BeanResGraficoDAO beanResGraficoDAO;;
		beanResGraficoDAO = consultaPag();
		beanResGraficoDAO.setBeanTotalesGraficoDAO(beanTotalesGraficoDAO);
		Utilerias util = Utilerias.getUtilerias();
		Calendar calendar = Calendar.getInstance();
		Date fecha = calendar.getTime();
		String strFecha = util.formateaFecha(fecha, Utilerias.FORMATO_YYYY_GUION_MM_GUION_DD);
		StringBuilder builder = new StringBuilder();
		builder.append(strFecha).append(" ").append(req.getHoraInicio()).append(":").append(req.getMinutoInicio());
		StringBuilder builder2 = new StringBuilder();
		builder2.append(strFecha).append(" ").append(req.getHoraFin()).append(" ").append(req.getMinutoFin());
		if("EN".equals(req.getTipoPago())){
			consulta(ENVIOS_NO_MOVILES,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DEM".equals(req.getTipoPago())){
			consulta(ENVIOS_DEV_MOVILES,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DE".equals(req.getTipoPago())){
			consulta(ENVIOS_DEV_NO_MOVILES,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("RE".equals(req.getTipoPago())){
			consulta(RECEPCIONES_NO_MOVILES,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("RM".equals(req.getTipoPago())){			
			consulta(RECEPCIONES_MOVILES,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DR".equals(req.getTipoPago())){
			consulta(DEVOLUCIONES_RECIBIDAS_NO_MOV,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DRM".equals(req.getTipoPago())){
			consulta(DEVOLUCIONES_RECIBIDAS_MOV,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DRX".equals(req.getTipoPago())){
			consulta(DEVOLUCIONES_RECIBIDAS_EXT_NO_MOV,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DRXM".equals(req.getTipoPago())){
			consulta(DEVOLUCIONES_RECIBIDAS_EXT_MOV,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DEX".equals(req.getTipoPago())){
			consulta(ENV_DEV_NO_MOV_EXT,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}else if("DEXM".equals(req.getTipoPago())){
			consulta(ENV_DEV_MOV_EXT,builder.toString(),builder2.toString(),beanResGraficoDAO,beanTotalesGraficoDAO);
		}
		
		return beanResGraficoDAO;
	}
	
	/**
	 * @param fechaIni String con la fecha inicial del rango
	 * @param fechaFin String con la fecha Fin del rango
	 * @param beanResGraficoDAO Bean que almacena la respuesta
	 * @param beanTotalesGraficoDAO Bean que almacena los totales
	 */
	private void consulta(String query,String fechaIni,String fechaFin,BeanResGraficoDAO beanResGraficoDAO,BeanTotalesGraficoDAO beanTotalesGraficoDAO){
		int numReg =beanResGraficoDAO.getPaginacion();;
		int inicio = 1;
		int fin = numReg;
		int regFin =0;
		List<Object[]> datos = new ArrayList<Object[]>();
		Utilerias util = Utilerias.getUtilerias();
		Object arrayObject[] = null;
		LOG.info("Ejecuta Query consulta"+query);
		do{
			List<HashMap<String,Object>> list = consulta(fechaIni,fechaFin, inicio, fin, query);
			if(!list.isEmpty()){
				for(HashMap<String,Object> map:list){
					arrayObject = new Object[2];
					arrayObject[0] = util.getString(map.get("FECHA"));
					arrayObject[1] = util.getString(map.get("DIFERENCIA"));
					regFin = util.getInteger(map.get("CONT"));
					beanTotalesGraficoDAO.setMontoTotal(util.getBigDecimal(map.get("IMP_TOTAL")));
					beanTotalesGraficoDAO.setTotalOp(util.getBigDecimal(map.get("CONT")));
					datos.add(arrayObject);
	  		  	}
				inicio = inicio+numReg;
				fin = fin+numReg;
			}else{
				inicio = regFin;
			}
		}while(inicio<regFin);
		beanResGraficoDAO.setDatos(datos);

	}
	
	 /**
	 * Metodo privado que permite lanzar la consulta a la bd 
	 * @param fechaIni Es la fecha de inicio
	 * @param fechaFin Es la fecha de Fin
	 * @param inicio Es el registro de inicio en la paginacion
	 * @param fin Es el registro de fin en la paginacion
	 * @param query Es el query a ejecutar
	 * @return List<HashMap<String,Object>> Respuesta del Query
	 */
	private List<HashMap<String,Object>> consulta(String fechaIni,String fechaFin, int inicio, int fin, String query) {
	    	BeanAdministraSaldoDAO beanAdministraSaldoDAO = new BeanAdministraSaldoDAO();
	    	List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
	    	ResponseMessageDataBaseDTO responseDTO = null;
	    	List<Object> parametros = new ArrayList<Object>();
	    	try {
	    		parametros.add(fechaIni);
	    		parametros.add(fechaFin);
	    		parametros.add(inicio);
	    		parametros.add(fin);
	    		
	        	responseDTO = HelperDAO.consultar(query, parametros,
	          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	        	if(responseDTO.getResultQuery()!= null){
	        		list = responseDTO.getResultQuery();
				}
		  	} catch (ExceptionDataAccess e) {
		  		showException(e);
		  		beanAdministraSaldoDAO.setCodError(Errores.EC00011B);
		  	}
	        return list;	
	    }
	
	 /**
	 * Metodo privado que permite lanzar la consulta a la bd 
	 * @return BeanResGraficoDAO bean con el numero de paginas
	 */
	private BeanResGraficoDAO consultaPag() {
			BeanResGraficoDAO beanResGraficoDAO = new BeanResGraficoDAO();
			List<HashMap<String,Object>> list;
			ResponseMessageDataBaseDTO responseDTO = null;
	    	try {
	        	responseDTO = HelperDAO.consultar(QUERY_PAGINACION, Collections.emptyList(),
	          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	        	if(responseDTO.getResultQuery()!= null){
	        		list = responseDTO.getResultQuery();
	        		if(!list.isEmpty()){
	        			String str = (String)((Map<String,Object>)list.get(0)).get("VALOR_FALTA");
	        			int numPag = Integer.parseInt(str);
	        			beanResGraficoDAO.setPaginacion(numPag);
	        		}else{
	        			beanResGraficoDAO.setPaginacion(0);
	        		}
	        			
				}
		  	} catch (ExceptionDataAccess e) {
		  		showException(e);
		  		beanResGraficoDAO.setCodError(Errores.EC00011B);
		  	}
	        return beanResGraficoDAO;	
	    }
	
	 /**
	 * Metodo privado que permite lanzar la consulta a la bd 
	 * @return BeanResGraficoDAO bean con el rango en minutos
	 */
	public BeanResGraficoDAO consultaRango(ArchitechSessionBean sessionBean) {
			BeanResGraficoDAO beanResGraficoDAO = new BeanResGraficoDAO();
			List<HashMap<String,Object>> list;
			ResponseMessageDataBaseDTO responseDTO = null;
	    	try {
	        	responseDTO = HelperDAO.consultar(QUERY_RANGO, Collections.emptyList(),
	          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	        	if(responseDTO.getResultQuery()!= null){
	        		list = responseDTO.getResultQuery();
	        		if(!list.isEmpty()){
	        			String str = (String)((Map<String,Object>)list.get(0)).get("VALOR_FALTA");
	        			int numMinutos = Integer.parseInt(str);
	        			beanResGraficoDAO.setRango(numMinutos);
	        		}else{
	        			beanResGraficoDAO.setPaginacion(0);
	        		}
	        			
				}
		  	} catch (ExceptionDataAccess e) {
		  		showException(e);
		  		beanResGraficoDAO.setCodError(Errores.EC00011B);
		  	}
	        return beanResGraficoDAO;	
	    }
	
	



}
