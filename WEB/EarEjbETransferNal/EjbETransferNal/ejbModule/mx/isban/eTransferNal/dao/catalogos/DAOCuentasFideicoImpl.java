/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCuentasFideicoImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 01:00:59 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCuentasFideico;

/**
 * Class DAOCuentasFideicoImpl.
 *
 * Clase que implementa la interfaz DAO para aplicar
 * los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCuentasFideicoImpl extends Architech implements DAOCuentasFideico {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -561198126795088160L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante CONSULTA_ALL. */
	private static final String CONSULTA_TODO = "SELECT DISTINCT "
			.concat("NUM_CUENTA, ")
			.concat("TOTAL, ")  
			.concat("RN ") 
			.concat("FROM (") 
			.concat("SELECT DISTINCT ") 
			.concat("NUM_CUENTA, ") 
			.concat("(SELECT COUNT(1) AS CONT FROM TRAN_CTAS_FIDEICO [FILTRO_WH]) AS TOTAL, ") 
			.concat("ROWNUM AS RN ") 
			.concat("FROM TRAN_CTAS_FIDEICO [FILTRO_WH]) ");
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_CTAS_FIDEICO "
			.concat("WHERE TRIM(NUM_CUENTA) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_CTAS_FIDEICO(NUM_CUENTA) "
			.concat("VALUES(TRIM(UPPER(?)))	");
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR = "UPDATE TRAN_CTAS_FIDEICO SET  "
			.concat("NUM_CUENTA = TRIM(UPPER(?)) WHERE TRIM(NUM_CUENTA) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_ELIMINAR. */
	private static final String CONSULTA_ELIMINAR = "DELETE FROM TRAN_CTAS_FIDEICO "
			.concat("WHERE TRIM(NUM_CUENTA) = TRIM(UPPER(?))");
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasCatalogos utilerias = UtileriasCatalogos.getInstancia();
	
	/** La variable que contiene informacion con respecto a: utilerias cuentas. */
	private static UtileriasCuentasFideico utileriasCuentas = UtileriasCuentasFideico.getInstancia();
	
	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param session objeto --> bean sesion --> El objeto session
	 * @param paginador objeto --> paginador --> Objeto para realizar el paginado
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 */
	@Override
	public BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanCuentas response = new BeanCuentas();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanCuenta> cuentas = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utileriasCuentas.getFiltro(beanCuenta, "AND");
		String filtroWhere = utileriasCuentas.getFiltro(beanCuenta, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				cuentas = new ArrayList<BeanCuenta>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanCuenta cuenta = new BeanCuenta();
					/** Seteo de datos **/
					cuenta.setCuenta(utilerias.getString(map.get("NUM_CUENTA")));
					/** Se añade el objeto a la lista**/
					cuentas.add(cuenta);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setCuentas(cuentas);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Agregar registro.
	 * 
	 * Metodo publico utilizado para agregar un registro
	 * en el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos del nuevo registro
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	@Override
	public BeanResBase agregar(BeanCuenta beanCuenta, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasCuentas.agregarParametros(beanCuenta, null, ConstantesCatalogos.ALTA);
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(beanCuenta);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Editar registro.
	 * 
	 * Metodo publico utilizado para editar un registro
	 * del catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos del nuevo registro
	 * @param anterior El objeto --> anterior --> Objeto con los datos del registro a editar
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	@Override
	public BeanResBase editar(BeanCuenta beanCuenta, BeanCuenta anterior, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			int existencia = 0;
			existencia = buscarRegistro(beanCuenta);
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utileriasCuentas.agregarParametros(beanCuenta, anterior, ConstantesCatalogos.MODIFICACION);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(CONSULTA_MODIFICAR, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Eliminar registro.
	 * 
	 * Metodo publico utilizado para eliminar un registro
	 * del catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos registro a eliminar
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	@Override
	public BeanResBase eliminar(BeanCuenta beanCuenta, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasCuentas.agregarParametros(beanCuenta, null, ConstantesCatalogos.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(CONSULTA_ELIMINAR, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 * 
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Contiene los datos de un registro a buscar
	 * @return Objeto int --> Resultado de la busqueda
	 */
	private int buscarRegistro(BeanCuenta beanCuenta) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasCuentas.agregarParametros(beanCuenta, null, ConstantesCatalogos.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
	
}
