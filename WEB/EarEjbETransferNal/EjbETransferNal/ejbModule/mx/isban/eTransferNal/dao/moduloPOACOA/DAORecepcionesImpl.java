/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORecepcionesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:28:51 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasDetRecepciones;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class DAORecepcionesImpl.
 *
 * Clase tipo DAO que implementa su interfaz para llevar a cabo la logica de los
 * flujos de accceso a los datos del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepcionesImpl extends Architech implements DAORecepciones {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 6923387165818571768L;

	/** La constante QUERY_DETALLE. */
	private static final String QUERY_DETALLE = " SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "
			+ " R.folio_paquete,R.folio_pago,R.topologia, " + " R.enviar,R.cda,R.estatus_banxico, "
			+ " R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "
			+ " R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "
			+ " R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "
			+ " R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "
			+ " R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, " + " R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "
			+ " R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, " + " R.prioridad,R.long_rastreo,R.folio_paq_dev, "
			+ " R.folio_pago_dev,R.refe_transfer,R.monto, "
			+ " TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "
			+ " R.direccion_ip,R.cde,R.nombre_ord, "
			+ " R.nombre_rec,R.domicilio_ord,R.concepto_pago, B.CVE_USUARIO, B.FUNCIONALIDAD, " + " ROWNUM INDICE "
			+ " FROM TRAN_SPID_REC R LEFT JOIN TRAN_SPID_BLOQUEO B ON( [QUERY] ) " + " WHERE R.CVE_MI_INSTITUC = ? "
			+ " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') " + " AND  R.CVE_INST_ORD = ? "
			+ " AND  R.FOLIO_PAQUETE = ? " + " AND  R.FOLIO_PAGO = ? ";

	/** La constante ESPACIO. */
	private static  final String ESPACIO = "";
	
	/** La constante utils. */
	private static final UtileriasDetRecepciones utils = UtileriasDetRecepciones.getUtils();
	
	/**
	 * Obtener consulta recepciones.
	 *
	 * Consultar los registros disponibles. 
	 * 
	 * @param beanPaginador El objeto: bean paginador
	 * @param modulo El objeto: modulo
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto bean res consulta recepciones DAO
	 */
	@Override
	public BeanResConsultaRecepcionesDAO obtenerConsultaRecepciones(BeanPaginador beanPaginador, BeanModulo modulo,
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch,
			ArchitechSessionBean session) {
		/**  se declara la variable */
		String tipoOrden = "";
		tipoOrden = beanReqConsultaRecepciones.getTipoOrden();

		/** se genera la variable consultaRecepciones */
		StringBuilder consultaRecepciones = utils.generarFormatoQuery(beanReqConsultaRecepciones, tipoOrden, modulo);
		
		/** Se genera la variable beanResConsultaRecepcionesDAO */
		final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO = new BeanResConsultaRecepcionesDAO();
		/** Se genera la variable list */
		List<HashMap<String, Object>> listRecep = null;
		/** Se genera el objeto utilerias */
		Utilerias utilerias = Utilerias.getUtilerias();
		/** Se genera el objeto resonseDTO */
		ResponseMessageDataBaseDTO responseDTORecep = null;
		/** Se declara listaBeanConsultaRecepciones */
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones = null;
		/** Se declara parametros */
		List<Object> parametrosRecep = new ArrayList<Object>();
		/** Seteando parametros */
		parametrosRecep.add(cveInst);
		parametrosRecep.add(fch);
		parametrosRecep.add(cveInst);
		parametrosRecep.add(fch);
		parametrosRecep.add(beanPaginador.getRegIni());
		parametrosRecep.add(beanPaginador.getRegFin());

		try {
			/** Se daclara String query */
			String query = consultaRecepciones.toString();
			/** Se obtiene origen */
			String origen = ValidadorMonitor.getOrigen(modulo);
			/** Se valida el origen para el armado de la consulta */
			origen = origen.replace("TMS.", "");
			query = query.replaceAll("\\[ORIGEN\\]", origen);
			query = ValidadorMonitor.cambiaQuery(modulo, query);

			/** Se ejecuta la consulta */
			responseDTORecep = HelperDAO.consultar(query, parametrosRecep, HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			/** validando resultado de la consulta */
			if (responseDTORecep.getResultQuery() != null && !responseDTORecep.getResultQuery().isEmpty()) {
				/** validando resultado de consulta */
				listRecep = responseDTORecep.getResultQuery();
				if (!listRecep.isEmpty()) {
					/** Se llena el bean con el resultado de la consulta */
					listaBeanConsultaRecepciones = utils.llenarConsultaRecepciones(beanResConsultaRecepcionesDAO, listRecep,
							utilerias);
				}
			}

			/** Seteando lista a response */
			beanResConsultaRecepcionesDAO.setListaConsultaRecepciones(listaBeanConsultaRecepciones);
			/** <seteando codigos de error a bean response */
			beanResConsultaRecepcionesDAO.setCodError(responseDTORecep.getCodeError());
			beanResConsultaRecepcionesDAO.setMsgError(responseDTORecep.getMessageError());

		} catch (ExceptionDataAccess e) {
			/** ocurrio un error */
			showException(e);
			beanResConsultaRecepcionesDAO.setCodError(Errores.EC00011B);
			beanResConsultaRecepcionesDAO.setMsgError(Errores.DESC_EC00011B);
		}
		return beanResConsultaRecepcionesDAO;
	}

	/**
	 * Obtener importe total.
	 *
	 * Consultar el monto total.
	 * 
	 * @param modulo El objeto: modulo
	 * @param tipoOrden El objeto: tipo orden
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	@Override
	public BigDecimal obtenerImporteTotal(BeanModulo modulo, String tipoOrden, String cveInst, String fch,
			ArchitechSessionBean session) {
		/** se declara la variable totalImporte */
		BigDecimal totalImporte = BigDecimal.ZERO;
		/** Se declara la variable parametros */
		List<Object> parametros = new ArrayList<Object>();
		/** Se declara la variable responseDTO */
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se declara la variable list */
		List<HashMap<String, Object>> list = null;
		/** se setean los parametros */
		parametros.add(cveInst);
		parametros.add(fch);
		parametros.add(cveInst);
		parametros.add(fch);

		try {
			/** Se declara la variable query */
			String query = utils.generarQueryMonto(tipoOrden).toString();
			/** Se declara la variable origen */
			/** Se obtiene el origen */
			String origen = ValidadorMonitor.getOrigen(modulo);
			/** Se remplaza para el origen */
			origen = origen.replace("TMS.", "");
			/** se remplaza el origen */
			query = query.replaceAll("\\[ORIGEN\\]", origen);
			/** Se valida modulo/query */
			query = ValidadorMonitor.cambiaQuery(modulo, query);
			/** Se ejcuta la consulta */
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** se valida response null */
			if (responseDTO.getResultQuery() != null) {
				/** se se asigna el resultado a list */
				list = responseDTO.getResultQuery();
				/** se valida que la lista no este vacia */
				if (!list.isEmpty()) {
					/** se obtiene el totalImporte */
					totalImporte = utils.addImporte(totalImporte, list);
				}
			}

		} catch (ExceptionDataAccess e) {
			/** ocurrio un error */
			error("Ocurrio un error al obtener el importe");
			showException(e);
		}
		/** return totalImporte */
		return totalImporte;
	}
	
	/**
	 * Obtener det recepcion.
	 *
	 * Consultar detalle de la recepcion
	 * 
	 * @param beanLlave El objeto: bean llave
	 * @param modulo El objeto: modulo
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res recepcion DAO
	 */
	@Override
	public BeanResRecepcionDAO obtenerDetRecepcion(BeanSPIDLlave beanLlave, BeanModulo modulo,
			ArchitechSessionBean architechSessionBean) {
		/** Se declara la variable beanResRececionDAO */
		BeanResRecepcionDAO beanResRecepcionDAO = new BeanResRecepcionDAO();
		/** Se declara la variable list */
		List<HashMap<String, Object>> list = null;
		/** Se declara la variable responseDTO */
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			/** Se declara la variable params */
			List<Object> params = new ArrayList<Object>();
			
			/** seteando los parametros a la lista */
			params.add(beanLlave.getCveMiInstituc());
			params.add(beanLlave.getFchOperacion());
			params.add(beanLlave.getCveInstOrd());
			params.add(beanLlave.getFolioPaquete());
			params.add(beanLlave.getFolioPago());
			
			/** se obtiene query */
			String query = QUERY_DETALLE;
			/** validando modulo */
			query = utils.completaQuery(query, modulo);
			query = query.toUpperCase();
			/** se valida modulo */
			if (ValidadorMonitor.isSPEI(modulo)) {
				/**  se continua con el armado de query */
				query = query.replaceAll("B.CVE_USUARIO,", "B.COD_USR_APART,")
						.replaceAll("B.FUNCIONALIDAD,", ESPACIO)
						.replaceAll("R.CONCEPTO_PAGO,", ESPACIO)
						.replaceAll("R.DOMICILIO_ORD,", ESPACIO)
						.replaceAll("R.CLASIF_OPERACION,", ESPACIO)
						.replaceAll("R.ENVIAR,", ESPACIO)
						.replaceAll("R.COD_POSTAL_ORD,", ESPACIO)
						.replaceAll("R.FCH_CONSTIT_ORD,", ESPACIO)
						.replaceAll("R.FCH_INSTRUC_PAGO,", ESPACIO)
						.replaceAll("R.HORA_INSTRUC_PAGO,", ESPACIO)
						.replaceAll("R.FCH_ACEPT_PAGO,", ESPACIO)
						.replaceAll("R.HORA_ACEPT_PAGO,", ESPACIO)
						.replaceAll("R.DIRECCION_IP,", ESPACIO);
			}
			/** se ejecuta la conuslta */
			responseDTO = HelperDAO.consultar(query, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** se valida resultado de la consulta */
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** se obtiene lista */
				list = responseDTO.getResultQuery();
				/** se valida que la lista no este vacia */
				if (!list.isEmpty()) {
					/** se llena el response */
					beanResRecepcionDAO = utils.completaMetodo(list, beanResRecepcionDAO, responseDTO);
				}
			}
		} catch (ExceptionDataAccess e) {
			/** ocurrio un error */
			showException(e);
			beanResRecepcionDAO.setCodError(Errores.EC00011B);
			beanResRecepcionDAO.setMsgError(Errores.DESC_EC00011B);
		}
		/** return bean ResRecepcionDAO */
		return beanResRecepcionDAO;
	}
}
