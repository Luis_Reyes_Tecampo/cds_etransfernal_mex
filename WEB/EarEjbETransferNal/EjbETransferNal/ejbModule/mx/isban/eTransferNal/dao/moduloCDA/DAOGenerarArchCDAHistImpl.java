/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 18:24:31 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperSPIDRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGenerarArchCDAHistImpl extends Architech implements DAOGenerarArchCDAHist {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = -570796511152231897L;
	
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	private static final String QUERY_FECHA_OPERACION =
	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	/**
	 * Constante del tipo String que almacena la consulta que calcula el id_peticion
	 */
	private static final String QUERY_CONSULTA_CONSULTA_ID_PETICION = 
		" SELECT (NVL(MAX(TO_NUMBER(ID_PETICION)),0)+1) ID_PETICION FROM TRAN_SPEI_CTG_CDA";
	
	/**
	 * Constante que almacena el query de insert en la tabla TRAN_SPEI_CTG_CDA
	 */
	private static final String QUERY_INSERT_TRAN_SPEI_CTG_CDA = 
		" INSERT INTO TRAN_SPEI_CTG_CDA (FCH_OPERACION,ID_PETICION, TIPO_PETICION, USUARIO, SESION, MODULO, NOMBRE_ARCHIVO, FCH_HORA, ESTATUS) "+
	    " VALUES (sysdate,?, ?, ?, ?, ?, ?, TO_DATE(?,'dd-MM-yyyy'), ?)";
	
   /**Metodo DAO para obtener la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @param esSPID Indica si es SPID
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   *    */
   public BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean, boolean esSPID){
      final BeanResConsFechaOpDAO beanResConsFechaOpDAO = new BeanResConsFechaOpDAO();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      Utilerias utilerias = Utilerias.getUtilerias();
      try {
    	responseDTO = HelperDAO.consultar(HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_FECHA_OPERACION, esSPID), Collections.emptyList(), 
    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			list = responseDTO.getResultQuery();
			for(HashMap<String,Object> map:list){
				beanResConsFechaOpDAO.setFechaOperacion(
						utilerias.formateaFecha(map.get("FCH_OPERACION"), 
								Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
				beanResConsFechaOpDAO.setCodError(responseDTO.getCodeError());
				beanResConsFechaOpDAO.setMsgError(responseDTO.getMessageError());
  		  }
		}
	} catch (ExceptionDataAccess e) {
        showException(e);
        beanResConsFechaOpDAO.setCodError(Errores.EC00011B);
	}
      return beanResConsFechaOpDAO;
   }
   
   /**Metodo DAO para registrar el archivo a generar del CDA historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDAO
    *    */
    public BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean){
    	final BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = new BeanResGenArchCDAHistDAO();
       List<HashMap<String,Object>> list = null;
       ResponseMessageDataBaseDTO responseDTO = null;
       Integer idPeticion = -1;
       Utilerias utilerias = Utilerias.getUtilerias();
       try{
    	   responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CONSULTA_ID_PETICION, 
    			   Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
    	   list = responseDTO.getResultQuery();
      	   if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
      	      for(HashMap<String,Object> map:list){
      			idPeticion = utilerias.getInteger(map.get("ID_PETICION"));
      		  }
              responseDTO = HelperDAO.insertar(QUERY_INSERT_TRAN_SPEI_CTG_CDA, Arrays.asList(
            		  new Object[]{idPeticion,"D",architechSessionBean.getUsuario(),
            				  architechSessionBean.getIdSesion(),
        					  "GENARCCDAH", beanReqGenArchCDAHist.getNombreArchivo(),
        					  beanReqGenArchCDAHist.getFechaOperacion(),
            		  				"PE"}
              ),
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
              beanResGenArchCDAHistDAO.setIdPeticion(idPeticion.toString());
              beanResGenArchCDAHistDAO.setCodError(responseDTO.getCodeError());
              beanResGenArchCDAHistDAO.setMsgError(responseDTO.getMessageError());
	          
      	   }
       } catch (ExceptionDataAccess e) {
          showException(e);
          beanResGenArchCDAHistDAO.setCodError(Errores.EC00011B);
       }
       return beanResGenArchCDAHistDAO;
    }



}
