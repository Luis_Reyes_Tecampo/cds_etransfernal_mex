/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDatosGenerales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaOrdenante;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCdaDAOSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Session Bean implementation class DAOConsultaMovHistCDAImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaMovHistCDASPIDImpl extends DAOConsultaMovHistCDASPIDImpl2 implements DAOConsultaMovHistCDASPID {

    /** Constante del serial version. */
	private static final long serialVersionUID = -1581076479590719820L;
	
	/** * Constante del tipo String que almacena la consulta para obtener la cuenta de regisros. */
	private static final String QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_SPID =
		" SELECT COUNT(1) CONT FROM TRAN_SPID_CDA_HIS T1 WHERE T1.FCH_OPERACION BETWEEN TO_DATE(?, 'dd-MM-yyyy') AND TO_DATE(?, 'dd-MM-yyyy') ";

	/** * Constante del tipo String que almacena la consulta para obtener el listado de movimientos CDA. */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID=

		" SELECT TMP.REFERENCIA, TMP.FCH_OPERACION, "+
		 " TMP.FOLIO_PAQUETE, TMP.FOLIO_PAGO, TMP.CVE_MI_INSTITUC, TMP.CVE_INST_ORD, "+
		 " TMP.NOMBRE_BCO_ORD, TMP.CVE_RASTREO, TMP.NUM_CUENTA_REC, TMP.MONTO, TMP.FCH_HORA_ABONO, TMP.FCH_HORA_ENVIO,TMP.HORA_ENVIO, "+
		 " CASE(TMP.ESTATUS) "+
		 "   WHEN '0' THEN 'PENDIENTE' "+
		 "   WHEN '1' THEN 'ENVIADO' "+
		 "   WHEN '2' THEN 'CONFIRMADO' "+
		 "   WHEN '3' THEN 'PENDIENTE' "+
		 "   WHEN '4' THEN 'CONTINGENCIA' "+
		 "   WHEN '5' then 'RECHAZADO' "+
		 " END CDA, TMP.COD_ERROR, TO_CHAR(TMP.FCH_CDA,'yyyymmddhh24miss') FCH_CDA, TMP.TIPO_PAGO, "+
		 "     TMP.FOLIO_PAQCDA,TMP.FOLIO_CDA, TMP.MODALIDAD, TMP.HORA_ABONO, TMP.NOMBRE_ORD, TMP.TIPO_CUENTA_ORD,"+
		 "     TMP.NUM_CUENTA_ORD, TMP.RFC_ORD, TMP.NOMBRE_BCO_REC, TMP.NOMBRE_REC, TMP.TIPO_CUENTA_REC, TMP.RFC_REC,"+
		 "     TMP.NUM_SERIE_CERTIF, TMP.SELLO_DIGITAL, TMP.CLASIF_OPERACION, TMP.CONCEPTO_PAGO, "+
		 "     TMP.ALGORITMO_FIRMA, TMP.PADDING "+
		 "from (" +
		 "     select ROW_NUMBER() over (order by T1.FCH_OPERACION, T1.HORA_ENVIO, T1.FCH_CDA) RN, "+
		 "     T1.REFERENCIA,T1.FCH_OPERACION, T1.FOLIO_PAQUETE,T1.FOLIO_PAGO, T1.CVE_MI_INSTITUC,T1.CVE_INST_ORD, "+
		 "     NVL(T1.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD ,T1.CVE_RASTREO, T1.NUM_CUENTA_REC,T1.MONTO, "+
		 "     T1.FCH_HORA_ABONO,T1.FCH_HORA_ENVIO,T1.HORA_ENVIO, T1.ESTATUS, T1.COD_ERROR, T1.FCH_CDA, T1.TIPO_PAGO "+
		 "   FROM TRAN_SPID_CDA_HIS T1 WHERE T1.FCH_OPERACION between TO_DATE(?,'dd-MM-yyyy') and TO_DATE(?,'dd-MM-yyyy') ";

	/** * Constante del tipo String que almacena la consulta para obtener el listado de movimientos CDA. */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID_2 = " ) TMP where RN between ? and ? order by TMP.FCH_OPERACION, TMP.HORA_ENVIO,TMP.FCH_CDA ";

	/** Constante del tipo String que almacena la consulta para solicitar la exportacion de todos los registros realizados durante una consulta;. */
	private static final String QUERY_INSERT_CONSULTA_EXPORTAR_TODO_CDA_SPID =
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";

	/** Constante del tipo String que almacena la consulta para solicitar la exportacion de todos los registros realizados durante una consulta;. */
	private static final String COLUMNAS_CONSULTA_MOV_CDA_SPID = "Refere,Fecha Ope,Folio paq,Folio pago,Clave SPEI ordenante del abono, Inst Emisora,Cve rastreo,"+
	"Cta Beneficiario,Monto,Fecha Abono,Hr Abono, Hr Envio, Estatus CDA,Cod. Error,Tipo Pago, Folio Paq Cda,Folio Cda,Modalidad,Nombre Ordenante,"+
	"Tipo Cuenta Ordenante,Numero Cuenta Ordenante,Rfc Ordenante, Banco Ordenante, Nombre Receptor,Tipo Cuenta Rec,"+
	"Rfc Rec, Numero Serie Certif,Sello Digital,Clasificacion Operacion, Concepto Pago, Algoritmo Firma, Padding";

	/** Constante para la consulta de exportarTodos. */
	private static final String QUERY_CONSULTA_EXPORTAR_TODO_CDA_SPID =
		"SELECT T1.REFERENCIA || ',' || to_char(T1.FCH_OPERACION,'DD/MM/YYYY') || ',' || T1.FOLIO_PAQUETE || ',' || T1.FOLIO_PAGO || ',' ||"+
		" T1.CVE_INST_ORD || ',' || NVL(T1.NOMBRE_BCO_ORD,'') || ',' || T1.CVE_RASTREO || ',' || T1.NUM_CUENTA_REC || ',' || T1.MONTO || ',' || "+
		" to_char(T1.FCH_HORA_ABONO, 'DD/MM/YYYY') || ',' || to_char(T1.FCH_HORA_ABONO, 'HH24:MI:SS') || ',' || to_char(T1.FCH_HORA_ENVIO,'HH24:MI:SS') || ',' || " +
		" CASE(T1.ESTATUS) WHEN '0' THEN 'PENDIENTE' " +
		"WHEN '1' THEN 'ENVIADO' WHEN '2' THEN 'CONFIRMADO' " +
		"WHEN '3' THEN 'PENDIENTE' WHEN '4' THEN 'CONTINGENCIA' " +
		"WHEN '5' THEN 'RECHAZADO' ELSE '' END" +
		"|| ',' || T1.COD_ERROR || ',' || T1.TIPO_PAGO "+
		 "     TMP.FOLIO_PAQCDA || ',' || TMP.FOLIO_CDA || ',' || TMP.MODALIDAD || ',' || TMP.HORA_ABONO || ',' || TMP.NOMBRE_ORD || ',' || TMP.TIPO_CUENTA_ORD || ',' ||"+
		 "     TMP.NUM_CUENTA_ORD || ',' || TMP.RFC_ORD || ',' || TMP.NOMBRE_BCO_REC || ',' || TMP.NOMBRE_REC || ',' || TMP.TIPO_CUENTA_REC || ',' || TMP.RFC_REC || ',' ||"+
		 "     TMP.NUM_SERIE_CERTIF || ',' || TMP.SELLO_DIGITAL || ',' || TMP.CLASIF_OPERACION || ',' || TMP.CONCEPTO_PAGO || ',' || "+
		 "     TMP.ALGORITMO_FIRMA || ',' || TMP.PADDING "+
		"FROM TRAN_SPID_CDA_HIS T1 WHERE ";

	/** * Constante del tipo String que almacena la consulta para obtener el detalle de un movimiento CDA. */
	private static final String QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA_SPID =
	    " SELECT  CDA_HIS.REFERENCIA, CDA_HIS.FCH_OPERACION, CDA_HIS.CVE_INST_ORD, "+
        " CDA_HIS.CVE_RASTREO, CDA_HIS.NUM_CUENTA_REC, CDA_HIS.MONTO, "+
        " CDA_HIS.FCH_HORA_ABONO, NVL(CDA_HIS.FCH_HORA_ENVIO,'') FCH_HORA_ENVIO, NVL(CDA_HIS.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD, "+
        " CASE(CDA_HIS.ESTATUS) WHEN '0' THEN 'PENDIENTE' "+
        "   WHEN '1' THEN 'ENVIADO'  "+
        "   WHEN '2' THEN 'CONFIRMADO' "+
        "   WHEN '3' THEN 'PENDIENTE'  "+
        "   WHEN '4' THEN 'CONTINGENCIA' "+
        "   WHEN '5' THEN 'RECHAZADO' END CDA, "+
        " CDA_HIS.NOMBRE_ORD, CDA_HIS.NUM_CUENTA_ORD, CDA_HIS.TIPO_CUENTA_ORD,CDA_HIS.RFC_ORD, NVL(CDA_HIS.NOMBRE_BCO_REC,'') NOMBRE_BCO_REC, "+
        " CDA_HIS.TIPO_CUENTA_REC, CDA_HIS.NOMBRE_REC NOMBRE_REC, CDA_HIS.CONCEPTO_PAGO, CDA_HIS.RFC_REC RFC_REC, CDA_HIS.TIPO_PAGO "+
        " FROM TRAN_SPID_CDA_HIS CDA_HIS "+
        " WHERE CDA_HIS.FCH_OPERACION = TO_DATE(?,'dd/mm/yyyy') AND CDA_HIS.CVE_MI_INSTITUC = ? AND CDA_HIS.CVE_INST_ORD = ?" +
        " AND CDA_HIS.FOLIO_PAQUETE = ? AND CDA_HIS.FOLIO_PAGO = ? AND CDA_HIS.REFERENCIA = ? AND CDA_HIS.FCH_CDA = TO_DATE(?,'YYYYMMDDHH24MISS') ";

	/** Constante TIPO_PAGO. */
	private static final String TIPO_PAGO = "TIPO_PAGO";
	
	/** Constante TIPO_PAGO. */
	private static final String FCH_HORA_ABONO ="FCH_HORA_ABONO";
	
	/**
	 * Metodo que se encarga de realizar la consulta de movimientos historicos CDA.
	 *
	 * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
	 */
    public BeanResConsMovHistCdaDAOSPID consultarMovHistCDASPID(BeanReqConsMovHistCDA beanReqConsMovHistCDASPID, ArchitechSessionBean sessionBean) {
    	BeanResConsMovHistCdaDAOSPID resMovHistCDASPIDDAO =  new BeanResConsMovHistCdaDAOSPID();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanMovimientoCDASPID> listBeanMovimientoCDA = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	String queryParam = null;
    	String query = null;
        try {
        	//se genera lista de parametros
        	List<Object> parametros = new ArrayList<Object>();
        	parametros.add(beanReqConsMovHistCDASPID.getFechaOpeInicio());
        	parametros.add(beanReqConsMovHistCDASPID.getFechaOpeFin());
        	//se llama funcion para generar parametizacion de query 
        	queryParam = armaConsultaParametrizadaCDASPID(parametros,beanReqConsMovHistCDASPID);
        	query = QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_SPID+queryParam;

        	//se cuentan los movimientos hstoricos CDA SPID
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	//se consulta el total de registros de la consulta
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 // se obtiene la cuenta de movimientos historicos
        		for(HashMap<String,Object> map:list){
        			resMovHistCDASPIDDAO.setTotalReg(utilerias.getInteger(map.get("CONT")));
        		}
        	}

        	//si el total de registros es mayor a 0
        	if(resMovHistCDASPIDDAO.getTotalReg()>0){
        		//se agregan parametros de numeroRegistro de inicio y fin
	        	parametros.add(beanReqConsMovHistCDASPID.getPaginador().getRegIni());
	        	parametros.add(beanReqConsMovHistCDASPID.getPaginador().getRegFin());

	        	//se genera el query para consultar movimientos historicos CDA SPID
	        	query = QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID+queryParam+QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID_2;
	      	    responseDTO = HelperDAO.consultar(query,parametros,
	      			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

	      	  //si hay resultados de busqueda 
	      	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	      		  //se llama funcion para generar y setear arrayList de movimientos historicos
	      		  setearListaMovCDASPID(responseDTO, resMovHistCDASPIDDAO);
	      	  }

	      	  // se obtiene y setea codigo de error de la consulta
	      	  resMovHistCDASPIDDAO.setCodError(responseDTO.getCodeError());
	      	  resMovHistCDASPIDDAO.setMsgError(responseDTO.getMessageError());
        	}else{
        		// se envia codigo de error exitoso y lista de movimientos vacia
        		resMovHistCDASPIDDAO.setCodError(Errores.CODE_SUCCESFULLY);
        		resMovHistCDASPIDDAO.setMsgError(Errores.OK00000V);
    	      	listBeanMovimientoCDA =  Collections.emptyList();
    	      	resMovHistCDASPIDDAO.setListBeanMovimientoCDA(listBeanMovimientoCDA);
        	}
  	} catch (ExceptionDataAccess e) {
  		//se setea el codigo de error EC00011B
  		showException(e);
  		resMovHistCDASPIDDAO.setCodError(Errores.EC00011B);
  		resMovHistCDASPIDDAO.setMsgError(Errores.DESC_EC00011B);
  	}
  		return resMovHistCDASPIDDAO;
    }
    
    /**
     * Metodo para generar la consulta de exportar todo.
     *
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @param architechSessionBean the architech session bean
     * @return String Objeto del tipo @see String
     */
    public String generarConsultaExportarTodoSPID(BeanReqConsMovHistCDA beanReqConsMovHistCDASPID,
    		ArchitechSessionBean architechSessionBean){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	String estatusCDA = "C";

    	// si fechaOperacionInicio tiene un valor valido
    	if(beanReqConsMovHistCDASPID.getFechaOpeInicio()!= null && beanReqConsMovHistCDASPID.getFechaOpeFin()!= null){
        	// se concatena fechaOperacionInicio
	    	builder.append(" t1.FCH_OPERACION BETWEEN TO_DATE('");
	    	builder.append(beanReqConsMovHistCDASPID.getFechaOpeInicio());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("') AND TO_DATE('");
	    	builder.append(beanReqConsMovHistCDASPID.getFechaOpeFin());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("')");
    	}

    	// si operacionContingencia tiene un valor valido
    	if(beanReqConsMovHistCDASPID.getOpeContingencia()){
        	// se concatena operacionContingencia
        	builder.append("AND T1.MODALIDAD = '");
        	builder.append(estatusCDA);
        	builder.append("'");
    	}
    	// si claveSpeiOrdenanteAbono tiene un valor valido
    	if(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono()))){
        	// se concatena claveSpeiOrdenanteAbono
    		builder.append(" AND T1.CVE_INST_ORD = ") ;
    		builder.append(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono());
    	}
    	
    	// si horaAbonoInicio y horaAbonoFin tienen un valor valido
    	if(beanReqConsMovHistCDASPID.getHrAbonoIni() != null && beanReqConsMovHistCDASPID.getHrAbonoFin() != null &&
    			(!vacio.equals(beanReqConsMovHistCDASPID.getHrAbonoIni())) && (!vacio.equals(beanReqConsMovHistCDASPID.getHrAbonoFin())) ){
        	// se concatena horaAbonoInicio y horaAbonoFin
			  builder.append("AND T1.HORA_ABONO  between ");
		      builder.append(beanReqConsMovHistCDASPID.getHrAbonoIni().replaceAll(":", ""));
		      builder.append(" and ");
		      builder.append(beanReqConsMovHistCDASPID.getHrAbonoFin().replaceAll(":", ""));
    	}

    	// se llama segunda parte de metodo generarConsultaExportarTodoSPID
	  	builder.append(generarConsultaExportarTodoSPID2(beanReqConsMovHistCDASPID));
    	return builder.toString();
    }

    /**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovHistCdaDAOSPID guardarConsultaMovHistExpTodoSPID(BeanReqConsMovHistCDA beanReqConsMovHistCDA, ArchitechSessionBean sessionBean) {

    	BeanResConsMovHistCdaDAOSPID beanResConsMovHistCdaDAO  = new BeanResConsMovHistCdaDAOSPID();
         ResponseMessageDataBaseDTO responseDTO = null;
         Utilerias utilerias = Utilerias.getUtilerias();
         Date fecha = new Date();
         StringBuilder builder = new StringBuilder();
         String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
         String nombreArchivo = "CDA_CON_MOV_CDAS_HIS_".concat(fechaActual);
         nombreArchivo = nombreArchivo.concat(".csv");

         // se generar query que sera insertado para exportacion de registros
         builder.append(QUERY_CONSULTA_EXPORTAR_TODO_CDA_SPID);
     	 builder.append(generarConsultaExportarTodoSPID(beanReqConsMovHistCDA, sessionBean));
     	 builder.append(" ORDER BY T1.FCH_OPERACION,T1.FCH_HORA_ENVIO");

         try{
        	 //se realiza insert de la peticion de exportarcion
         	responseDTO = HelperDAO.insertar(QUERY_INSERT_CONSULTA_EXPORTAR_TODO_CDA_SPID,
         			// se genera lista de parametros para insert
       			   Arrays.asList(new Object[]{
       					   sessionBean.getUsuario(),
       					   sessionBean.getIdSesion(),
       					   "CDA",
       					   "CONSULTA_MOV_HISTORICOS_CDAS",
       					   nombreArchivo,
       					   beanReqConsMovHistCDA.getTotalReg(),
       					   "PE",
       					   builder.toString(),
       					   COLUMNAS_CONSULTA_MOV_CDA_SPID}),
       			   HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
         	//se obtiene codigo de error del insert a bd
         	beanResConsMovHistCdaDAO.setCodError(responseDTO.getCodeError());
         	beanResConsMovHistCdaDAO.setMsgError(responseDTO.getMessageError());
         	beanResConsMovHistCdaDAO.setNombreArchivo(nombreArchivo);
         }catch (ExceptionDataAccess e) {
       		//se setea el codigo de error EC00011B
         	beanResConsMovHistCdaDAO.setCodError(Errores.EC00011B);
         	beanResConsMovHistCdaDAO.setMsgError(Errores.DESC_EC00011B);
         	showException(e);
		}
		return beanResConsMovHistCdaDAO;
    }

    /**
     * Metodo encargado de consultar el detalle de un movimiento CDA.
     *
     * @param beanReqConsMovCDASPID the bean req cons mov cdaspid
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovDetHistCdaDAO Objeto del tipo @see BeanResConsMovDetHistCdaDAO
     */
    public BeanResConsMovDetHistCdaDAOSPID consultarMovDetHistCDASPID(BeanReqConsMovCDADet beanReqConsMovCDASPID,
    		ArchitechSessionBean architechSessionBean) {
    	BeanResConsMovDetHistCdaDAOSPID resConsMovHistDetCDADAO = new BeanResConsMovDetHistCdaDAOSPID();
    	BeanResConsMovCdaDatosGenerales beanDatosGeneralesMovCDA = new BeanResConsMovCdaDatosGenerales();
    	BeanResConsMovCdaOrdenante  beanMovCdaOrdenante = new BeanResConsMovCdaOrdenante();
    	BeanResConsMovCdaBeneficiario beanMovCdaBeneficiario =  new BeanResConsMovCdaBeneficiario();
    	Utilerias utilerias = Utilerias.getUtilerias();
        ResponseMessageDataBaseDTO responseDTO = null;
    	List<HashMap<String,Object>> list = null;
    	//se obtienen los parametros para realizar consulta del detalle
    	int cveMiInstitucLink = Integer.parseInt(beanReqConsMovCDASPID.getCveMiInstitucLink());
    	int folioPagoLink = Integer.parseInt(beanReqConsMovCDASPID.getFolioPagoLink());
    	int folioPaqLink = Integer.parseInt(beanReqConsMovCDASPID.getFolioPaqLink());
        int numReferencia = Integer.parseInt(beanReqConsMovCDASPID.getReferencia());
    	int cveSpeiLink = Integer.parseInt(beanReqConsMovCDASPID.getCveSpeiLink());

    	String fechaCDA = beanReqConsMovCDASPID.getFchCDALink();

        try {
        	// se consulta detalle de movimiento
        	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA_SPID,
        			Arrays.asList(new Object[]{beanReqConsMovCDASPID.getFechaOpeLink(),cveMiInstitucLink,cveSpeiLink,
        					folioPaqLink,folioPagoLink,numReferencia,fechaCDA}),
        					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

       	//si hay resultados de busqueda
   		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
   			list = responseDTO.getResultQuery();
   			// se recorre la lista de resultados
   			for(HashMap<String,Object> map:list){
   				//se obtienen y setean los datosGenerales obtnenidos de la consulta
   				beanDatosGeneralesMovCDA.setReferencia(utilerias.getString(map.get("REFERENCIA")));			//campo referencia
   				beanDatosGeneralesMovCDA.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));		//campo claveRastreo
   				beanDatosGeneralesMovCDA.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));	//campo fechaOperacion
   				beanDatosGeneralesMovCDA.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));			//campo horaAbono
   				beanDatosGeneralesMovCDA.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));			//campo fechaHoraEnvio
   				beanDatosGeneralesMovCDA.setEstatusCDA(utilerias.getString(map.get("CDA")));				//campo CDA
   				beanDatosGeneralesMovCDA.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));				//campo tipoPago
   				beanDatosGeneralesMovCDA.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));	//campo fechaAbono

   				//se obtienen y setean los movCDABeneficiario obtnenidos de la consulta
   				beanMovCdaBeneficiario.setNomInstRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));			//campo nombreBCORec
   				beanMovCdaBeneficiario.setNombreBened(utilerias.getString(map.get("NOMBRE_REC")));				//campo nombreRec
   				beanMovCdaBeneficiario.setTipoCuentaBened(utilerias.getString(map.get("TIPO_CUENTA_REC")));		//campo tipoCuentaRec
   				beanMovCdaBeneficiario.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));				//campo numCuentaRec
   				beanMovCdaBeneficiario.setRfcCurpBeneficiario(utilerias.getString(map.get("RFC_REC")));			//campo RFC
   				beanMovCdaBeneficiario.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));			//campo conceptoPago
   				beanMovCdaBeneficiario.setImporteIVA(utilerias.formateaDecimales(map.get("IVA"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));	//campo IVA
   				beanMovCdaBeneficiario.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));		//campo Monto

   				//se obtienen y setean los movCDAOrdenante obtnenidos de la consulta
   				beanMovCdaOrdenante.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));				//campo claveInstOrd
   				beanMovCdaOrdenante.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));		//campo nombreBCOOrd
   				beanMovCdaOrdenante.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));				//campo nombreOrd
   				beanMovCdaOrdenante.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));		//campo tipoCuentaOrdenante
   				beanMovCdaOrdenante.setCtaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));				//campo numeroCuenta
   				beanMovCdaOrdenante.setRfcCurpOrdenante(utilerias.getString(map.get("RFC_ORD")));			//campo RFCOrd

   				//se obtienen y setean el detalleMovimientoCDASPID obtnenidos de la consulta
   				resConsMovHistDetCDADAO.setBeanMovCdaDatosGenerales(beanDatosGeneralesMovCDA);
   				resConsMovHistDetCDADAO.setBeanConsMovCdaOrdenante(beanMovCdaOrdenante);
   				resConsMovHistDetCDADAO.setBeanConsMovCdaBeneficiario(beanMovCdaBeneficiario);

   				//se obtiene codigo y mensaje de error de la consulta
   				resConsMovHistDetCDADAO.setCodError(responseDTO.getCodeError());
   				resConsMovHistDetCDADAO.setMsgError(responseDTO.getMessageError());
     		  }
   		}
       	} catch (ExceptionDataAccess e) {
       		showException(e);
       		//se setea codigo de error EC00011B
       		resConsMovHistDetCDADAO.setCodError(Errores.EC00011B);
       		resConsMovHistDetCDADAO.setMsgError(Errores.DESC_EC00011B);
       	}

       return resConsMovHistDetCDADAO;
    }
}