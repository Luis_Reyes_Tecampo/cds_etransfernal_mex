/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: DAOConsultaBancoSPIDImp.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 01:48:45 AM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.dao.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaBancoSPIDDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.ws.BeanConsultaBancoSPID;

/**
 *
 * The Class DAOConsultaBancoSPIDImp.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaBancoSPIDImp extends Architech implements DAOConsultaBancoSPID {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 4535124848682955347L;

	/** The Constant QUERY_CONSULTA_BANCO_SPID. */
	private static final String QUERY_CONSULTA_BANCO_SPID = "SELECT LPAD(CIF.NUM_CECOBAN,4,0) as NUM_CECOBAN, CIF.CVE_INTERME, CIF.NOMBRE_CORTO, CIF.NOMBRE_LARGO FROM TRAN_SPID_INTERME TSI LEFT JOIN COMU_INTERME_FIN  CIF ON (TSI.CVE_INTERME = CIF.CVE_INTERME) ORDER BY NUM_CECOBAN";

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.ws.DAOConsultaBancoSPID#consultaBancoSPID(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResConsultaBancoSPIDDAO consultaBancoSPID() {
		//se inicia ejecucion de DAO consulta bancos SPID
		info("Entra al DAO de consultaBanco");
		final BeanResConsultaBancoSPIDDAO resBeanConsultaBancoDAO = new BeanResConsultaBancoSPIDDAO();
		List<BeanConsultaBancoSPID> listaBancosSPID=new ArrayList<BeanConsultaBancoSPID>();
		List<HashMap<String,Object>> listResult = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			//se ejecuta query consulta bancos SPID
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_BANCO_SPID, Arrays.asList(new Object[] {}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			//si el query devuelve resultados
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				listResult = responseDTO.getResultQuery();
				//se recorre la lista de resultados
				for(HashMap<String,Object> map: listResult){
					//bean lista de resultados
					BeanConsultaBancoSPID beanResponse=new BeanConsultaBancoSPID();
					beanResponse.setNumCECOBAN(utilerias.getString(map.get("NUM_CECOBAN")));	//se obtiene campo numero CECOBAN
					beanResponse.setCveInterme(utilerias.getString(map.get("CVE_INTERME")));	//se obtiene campo Clave Intermediario
					beanResponse.setNombreCorto(utilerias.getString(map.get("NOMBRE_CORTO")));	//se obtiene campo Nombre Corto
					beanResponse.setNombreLargo(utilerias.getString(map.get("NOMBRE_LARGO")));	//se obtiene campo Nombre Largo
					//se agrega bean a lista de bancos SPID
					listaBancosSPID.add(beanResponse);
				}
				// se setea codigo y mensaje de error OK00000V TRANSACCION EXITOSA
				resBeanConsultaBancoDAO.setCodError(Errores.OK00000V);
				resBeanConsultaBancoDAO.setMsgError(Errores.DESC_OK00000V);
			}else{
				// se setea codigo y mensaje de error ED00011V No se encontro informacion
				resBeanConsultaBancoDAO.setCodError(Errores.ED00011V);
				resBeanConsultaBancoDAO.setMsgError(Errores.DESC_ED00011V);
			}
			//seteo de la lista de bancos SPID
			resBeanConsultaBancoDAO.setListaBancosSPID(listaBancosSPID);
		} catch (ExceptionDataAccess e) {
			showException(e);
			//se setea codigo y mensaje de error EC00011B
			resBeanConsultaBancoDAO.setCodError(Errores.EC00011B);
			resBeanConsultaBancoDAO.setMsgError(Errores.DESC_EC00011B);
		}
		return resBeanConsultaBancoDAO;
	}
}