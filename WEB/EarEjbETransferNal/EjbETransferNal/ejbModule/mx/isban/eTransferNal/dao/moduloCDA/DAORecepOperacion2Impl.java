package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 *Clase del tipo DAO que se encarga  obtener la informacion
 *para la consulta de Movimientos CDAs
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperacion2Impl  extends Architech implements DAORecepOperacion2{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8167141597887988495L;

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE
	 */
	private static final String QUERY_UPDATE =
		"UPDATE TRAN_SPEI_REC SET ESTATUS_TRANSFER=?,CODIGO_ERROR =?, MOTIVO_DEVOL=?, REFE_TRANSFER=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE_MOTIVO
	 */
	private static final String QUERY_UPDATE_MOTIVO =
		"UPDATE TRAN_SPEI_REC SET MOTIVO_DEVOL=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE_MOTIVO_RECHAZO
	 */
	private static final String QUERY_UPDATE_MOTIVO_RECHAZO =
		"UPDATE TRAN_SPEI_REC SET MOTIVO_DEVOL=?, ESTATUS_TRANSFER=? WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? "+
			" AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT
	 */
	private static final String QUERY_INSERT =
		" INSERT INTO TRAN_SPEI_HORAS_MAN (FOLIO, TIPO, FCH_OPERACION, CVE_INST_ORD, CVE_MI_INSTITUC, FOLIO_PAQUETE, FOLIO_PAGO, REFERENCIA, FCH_CAPTURA, TIPO2 ) "+
		" VALUES (folio_man.Nextval,'N',TO_DATE(?,'dd-mm-yyyy'), ?, ?, ?, ?, ?, TO_DATE(?,'dd-mm-yyyy HH24:MI:SS'), 'A' ) ";




    @Override
    public BeanResActTranSpeiRecDAO actualizaTransSpeiRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
   		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
       try{
          HelperDAO.actualizar(QUERY_UPDATE,
		   Arrays.asList(new Object[]{
				   beanReqActTranSpeiRec.getEstatus(),
				   beanReqActTranSpeiRec.getCodigoError(),
				   util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
				   beanReqActTranSpeiRec.getReferenciaTransfer(),
				   beanReqActTranSpeiRec.getFechaOperacion(),
				   beanReqActTranSpeiRec.getCveMiInstituc(),
				   beanReqActTranSpeiRec.getCveInstOrd(),
				   beanReqActTranSpeiRec.getFolioPaquete(),
				   beanReqActTranSpeiRec.getFolioPago()
				   }),
          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
          beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
       } catch (ExceptionDataAccess e) {
          showException(e);
          beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
       }
       return beanResActTranSpeiRecDAO;
    }

    /**
     * Metodo que actualiza el motivo de rechazo
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO actualizaMotivoRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
        try{
           HelperDAO.actualizar(QUERY_UPDATE_MOTIVO,
 		   Arrays.asList(new Object[]{
 				  util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
 				   beanReqActTranSpeiRec.getFechaOperacion(),
 				   beanReqActTranSpeiRec.getCveMiInstituc(),
 				   beanReqActTranSpeiRec.getCveInstOrd(),
 				   beanReqActTranSpeiRec.getFolioPaquete(),
 				   beanReqActTranSpeiRec.getFolioPago()
 				   }),
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
        }
        return beanResActTranSpeiRecDAO;
     }

    /**
     * Metodo que rechaza una operacion
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO rechazar(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
    	Utilerias util = Utilerias.getUtilerias();
        try{
           HelperDAO.actualizar(QUERY_UPDATE_MOTIVO_RECHAZO,
 		   Arrays.asList(new Object[]{
 				  util.formateaStringAEntero(beanReqActTranSpeiRec.getMotivoDevol(),"00"),
 				  beanReqActTranSpeiRec.getEstatus(),
 				   beanReqActTranSpeiRec.getFechaOperacion(),
 				   beanReqActTranSpeiRec.getCveMiInstituc(),
 				   beanReqActTranSpeiRec.getCveInstOrd(),
 				   beanReqActTranSpeiRec.getFolioPaquete(),
 				   beanReqActTranSpeiRec.getFolioPago()
 				   }),
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
        }
        return beanResActTranSpeiRecDAO;
     }

    /**
     * @param beanReqActTranSpeiRec bean del tipo BeanReqActTranSpeiRec
     * @param architechSessionBean Objeto de ArchitechSessionBean
     */
    public void guardaTranSpeiHorasMan(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean){
          final BeanResGuardaInstDAO beanResGuardaInstDAO = new BeanResGuardaInstDAO();
          ResponseMessageDataBaseDTO responseDTO = null;
          try{
        		  responseDTO = HelperDAO.insertar(QUERY_INSERT,
        		   		   Arrays.asList(new Object[]{
        		   				beanReqActTranSpeiRec.getFechaOperacion(),
        		   				beanReqActTranSpeiRec.getCveInstOrd(),
        		   				beanReqActTranSpeiRec.getCveMiInstituc(),
        		   				beanReqActTranSpeiRec.getFolioPaquete(),
        		   				beanReqActTranSpeiRec.getFolioPago(),
        		   				beanReqActTranSpeiRec.getReferenciaTransfer(),
        		   				beanReqActTranSpeiRec.getFchCaptura(),
        		   		   }),
        		             HelperDAO.CANAL_GFI_DS, this.getClass().getName());
        		             beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
        		             beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());

          } catch (ExceptionDataAccess e) {
             showException(e);
             beanResGuardaInstDAO.setCodError(Errores.EC00011B);
          }
          //return null;
       }


}