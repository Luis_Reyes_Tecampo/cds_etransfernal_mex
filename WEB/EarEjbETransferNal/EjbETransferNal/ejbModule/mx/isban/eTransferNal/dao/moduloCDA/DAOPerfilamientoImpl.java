/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOPerfilamientoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 10:03:36 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsCatPerfilDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfilDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanServiciosTareas;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el perfilamiento
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOPerfilamientoImpl extends Architech implements DAOPerfilamiento {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = 2717336209642457237L;
	
	/**
	 * Constante que almacena la consulta de servicios que tiene un perfil
	 */
	private static final String QUERY_CONSULTA_PERFIL_SERVICIOS =
	" SELECT P.TXT_PERFIL_SAM, M.TXT_MODULO, TRIM(S.NOM_SERVICIO) NOM_SERVICIO"+
              " FROM TR_SEG_CAT_PERFIL P, TR_SEG_CAT_MODULO M, TR_SEG_CFG_MPSU C, "+
                   " TR_SEG_CAT_SERVICIO S "+
              " WHERE    "+
                      " P.CVE_MODULO = M.CVE_MODULO AND "+
                        " C.CVE_PERFIL = P.CVE_PERFIL AND "+
                        " S.CVE_SERVICIO = C.CVE_SERVICIO AND " +
                        " TRIM(P.TXT_PERFIL_SAM) =? ";

	/**
	 * Constante que almacena la consulta de tareas que tiene un perfil en un servicio dado
	 */
	private static final String QUERY_CONSULTA_SERVICIO_TAREAS =
	" SELECT P.TXT_PERFIL_SAM, M.TXT_MODULO, TRIM(S.NOM_SERVICIO) NOM_SERVICIO, TRIM(T.TXT_TAREA_SIST) TXT_TAREA_SIST"+
    " FROM TR_SEG_CAT_PERFIL P, TR_SEG_CAT_MODULO M, TR_SEG_CFG_MPSU C, "+
    " TR_SEG_CAT_SERVICIO S, TR_SEG_CFG_PERFIL_TAREA PT, "+
    " TR_SEG_CAT_TAREA_SIST T "+
    " WHERE   "+
       " P.CVE_MODULO = M.CVE_MODULO AND "+
       " C.CVE_PERFIL = P.CVE_PERFIL AND "+
       " S.CVE_SERVICIO = C.CVE_SERVICIO AND "+
       " TRIM(P.TXT_PERFIL_SAM) = ? AND "+
       " TRIM(S.NOM_SERVICIO) = ? AND "+
       " PT.CVE_PERFIL = P.CVE_PERFIL AND "+
       " PT.CVE_SERVICIO = S.CVE_SERVICIO AND "+
       " PT.ESTATUS_TAREA = 1 AND "+
       " T.CVE_TAREA_SIST = PT.CVE_TAREA_SIST ";

	/**
	 * Constante que almacena la consulta de perfiles
	 */
	private static final String QUERY_CONSULTA_CAT_PERFILES = "SELECT CVE_PERFIL,CVE_MODULO,TXT_PERFIL_SAM,TXT_PERFIL_TRFP " +
			" FROM TR_SEG_CAT_PERFIL ";
	
	/**
	 * Metodo que permite consultar el catalogo de perfiles
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsCatPerfilDAO consultaPerfiles(ArchitechSessionBean architechSessionBean){
		final BeanResConsCatPerfilDAO beanResConsCatPerfilDAO = new BeanResConsCatPerfilDAO();
		  BeanPerfil beanPerfil = null;
	      List<HashMap<String,Object>> list = null;
	      List<BeanPerfil> beanPerfilList = Collections.emptyList();
	      ResponseMessageDataBaseDTO responseDTO = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CAT_PERFILES, 
	    			Arrays.asList(new Object[]{}), 
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				beanPerfilList = new ArrayList<BeanPerfil>();
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanPerfil = new BeanPerfil();
					beanPerfil.setCvePerfil(utilerias.getString(map.get("CVE_PERFIL")));
					beanPerfil.setCveModulo(utilerias.getString(map.get("CVE_MODULO")));
					beanPerfil.setTxtPerfilSam(utilerias.getString(map.get("TXT_PERFIL_SAM")));
					beanPerfil.setTxtPerfilTrfp(utilerias.getString(map.get("TXT_PERFIL_TRFP")));
					beanPerfilList.add(beanPerfil);
	  		  	}
			}
			beanResConsCatPerfilDAO.setPerfilList(beanPerfilList);
			beanResConsCatPerfilDAO.setCodError(responseDTO.getCodeError());
			beanResConsCatPerfilDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsCatPerfilDAO.setCodError(Errores.EC00011B);
		}
		return beanResConsCatPerfilDAO;
	}
	
	/**
	 * Metodo que permite consultar los servicios que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsPerfilDAO consultaServicio(BeanReqConsPerfil beanReqConsPerfil, ArchitechSessionBean architechSessionBean){
		final BeanResConsPerfilDAO beanResConsPerfilDAO = new BeanResConsPerfilDAO();
		BeanServiciosTareas beanServiciosTareas = null;
	      List<HashMap<String,Object>> list = null;
	      List<BeanServiciosTareas> listBeanServiciosTareas = Collections.emptyList();
	      ResponseMessageDataBaseDTO responseDTO = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_PERFIL_SERVICIOS, 
	    			Arrays.asList(new Object[]{beanReqConsPerfil.getPerfil()}), 
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				listBeanServiciosTareas = new ArrayList<BeanServiciosTareas>();
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanServiciosTareas = new BeanServiciosTareas();
					beanServiciosTareas.setModulo(utilerias.getString(map.get("TXT_MODULO")));
					beanServiciosTareas.setPerfil(utilerias.getString(map.get("TXT_PERFIL_SAM")));
					beanServiciosTareas.setServicio(utilerias.getString(map.get("NOM_SERVICIO")));
					listBeanServiciosTareas.add(beanServiciosTareas);
	  		  	}
			}
			beanResConsPerfilDAO.setListBeanServiciosTareas(listBeanServiciosTareas);
			beanResConsPerfilDAO.setCodError(responseDTO.getCodeError());
			beanResConsPerfilDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsPerfilDAO.setCodError(Errores.EC00011B);
		}
		return beanResConsPerfilDAO;
	}
	
	/**
	 * Metodo que permite consultar las tareas en un servicios que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsPerfilDAO consultaServicioTareas(BeanReqConsPerfil beanReqConsPerfil, ArchitechSessionBean architechSessionBean){
		final BeanResConsPerfilDAO beanResConsPerfilDAO = new BeanResConsPerfilDAO();
		BeanServiciosTareas beanServiciosTareas = null;
	      List<HashMap<String,Object>> list = null;
	      List<BeanServiciosTareas> listBeanServiciosTareas = Collections.emptyList();
	      ResponseMessageDataBaseDTO responseDTO = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_SERVICIO_TAREAS, 
	    			Arrays.asList(new Object[]{beanReqConsPerfil.getPerfil(),beanReqConsPerfil.getServicio()}), 
	    			HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				listBeanServiciosTareas = new ArrayList<BeanServiciosTareas>();
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					beanServiciosTareas = new BeanServiciosTareas();
					beanServiciosTareas.setModulo(utilerias.getString(map.get("TXT_MODULO")));
					beanServiciosTareas.setPerfil(utilerias.getString(map.get("TXT_PERFIL_SAM")));
					beanServiciosTareas.setServicio(utilerias.getString(map.get("NOM_SERVICIO")));
					beanServiciosTareas.setTarea(utilerias.getString(map.get("TXT_TAREA_SIST")));
					listBeanServiciosTareas.add(beanServiciosTareas);
	  		  	}
			}
			beanResConsPerfilDAO.setListBeanServiciosTareas(listBeanServiciosTareas);
			beanResConsPerfilDAO.setCodError(responseDTO.getCodeError());
			beanResConsPerfilDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsPerfilDAO.setCodError(Errores.EC00011B);
		}
		return beanResConsPerfilDAO;
		
	}
}
