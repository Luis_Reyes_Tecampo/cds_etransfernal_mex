/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOOrdenReparacionImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacionDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para las ordenes por reparar
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOOrdenReparacionImpl extends Architech implements DAOOrdenesReparacion {
	
	/**
	 * Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPID
	 */
	private static final StringBuilder CONSULTA_ORDENES_REPARACION =
			new StringBuilder()
			.append("SELECT TM.* ")
			.append("FROM ( ")
				.append("SELECT TM.*, ROWNUM R ")
				.append("FROM ( ")
					.append("SELECT DISTINCT(TM.REFERENCIA), TM.CVE_COLA, TM.CVE_TRANSFE, ")
					.append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ")
					.append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")			
					.append("TM.ESTATUS, TM.COMENTARIO3, TMS.FOLIO_PAQUETE, TMS.FOLIO_PAGO, C.CONT ")
					.append("FROM TRAN_MENSAJE TM ")
					.append("JOIN TRAN_MENSAJE_SPID TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPID' ")
					.append("AND ESTATUS='PR' ")
					.append("[ORDENAMIENTO]")
				.append(") TM ")
			.append(") TM ")
			.append("WHERE TM.R BETWEEN ? AND ?");
	
	/**
	 * Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPID
	 */
	private static final StringBuilder CONSULTA_ORDENES_REPARACION_MONTOS =
			new StringBuilder()
			.append(" SELECT SUM(TM.IMPORTE_ABONO) as MONTO_TOTAL ")
					.append("FROM TRAN_MENSAJE TM ")
					.append("JOIN TRAN_MENSAJE_SPID TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPID' ")
					.append("AND ESTATUS='PR' ")
					.append("[ORDENAMIENTO]");
	
	/**
	 * Consulta para actualizar las ordenes de reparacion a estatus LI del modulo SPID
	 */
	private static final StringBuilder UPDATE_ORDENES_REPARACION = 
			new StringBuilder().append("UPDATE TRAN_MENSAJE SET ESTATUS='LI' ")
							   .append("WHERE CVE_MECANISMO = 'SPID' ")
							   .append("AND REFERENCIA = ? ");
	
	/**
	 * Consulta para buscar todas las ordenes de reparacion, se usa para exportar todo a archivo
	 */
	private static final StringBuilder TOTAL_CONSULTA_ORDENES_REPARACION = 
			new StringBuilder().append("SELECT TM.REFERENCIA || ',' ||")
							   .append("TM.CVE_COLA || ',' || TM.CVE_TRANSFE || ',' || TM.CVE_MECANISMO || ',' || ") 
							   .append("TM.CVE_MEDIO_ENT || ',' || TM.FCH_CAPTURA || ',' || TM.CVE_INTERME_ORD || ',' || ")
						   	   .append("TM.CVE_INTERME_REC || ',' || TM.IMPORTE_ABONO || ',' || TM.ESTATUS || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END || ',' || TM.COMENTARIO3 ") 
					   	   	   .append("FROM TRAN_MENSAJE TM, TRAN_MENSAJE_SPID TMS ") 
					   	   	   .append("WHERE TM.REFERENCIA = TMS.REFERENCIA AND TM.ESTATUS = 'PR' AND TM.CVE_MECANISMO='SPID'");
	
	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos las ordenes de Reparacion exportadas
	 */
	private static final StringBuilder INSERT_TODOS_ORDENES_REPARACION = 
			new StringBuilder().append("INSERT INTO TRAN_SPEI_EXP_CDA ")
							   .append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) ")
                               .append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * Prpiedad para matener el estado de un obejto a traves de las capas
	 */
	private static final long serialVersionUID = 1041913510555848982L;

	/**
	 * Metodo que obtiene las ordenes en estado PR
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return beanResOrdenReparacionDAO Obejeto del tipo @see BeanResOrdenReparacionDAO
	 */
	@Override
	public BeanResOrdenReparacionDAO obtenerOrdenesReparacion(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		BeanOrdenReparacion beanReparacionRes = null;
		final BeanResOrdenReparacionDAO beanResOrdenReparacionDAO= new BeanResOrdenReparacionDAO();
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanOrdenReparacion> listaBeanOrdenReparacion = new ArrayList<BeanOrdenReparacion>();
    	
    	try {
    		String ordenamiento = "";
    		
    		if (sortField != null && !"".equals(sortField)){
    			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
    		}
    		else
    		{
    			ordenamiento = " ORDER BY REFERENCIA ASC";
    		}
    		
        	responseDTO = HelperDAO.consultar(CONSULTA_ORDENES_REPARACION.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays.asList(
        			new Object[] { beanPaginador.getRegIni(),
 						   beanPaginador.getRegFin() }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();        		 
        		 if(!list.isEmpty()){      			 
        			 for(Map<String,Object> mapResult : list){
        				 beanReparacionRes = new BeanOrdenReparacion();
        				 beanReparacionRes.setBeanOrdenReparacionDos(new BeanOrdenReparacionDos());
        				 beanReparacionRes.getBeanOrdenReparacionDos().setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setFechaCaptura(utilerias.formateaFecha(utilerias.getDate(mapResult.get("FCH_CAPTURA")),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveIntermeOrd(utilerias.getString(mapResult.get("CVE_INTERME_ORD")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setEstatus(utilerias.getString(mapResult.get("ESTATUS")));
        				 beanReparacionRes.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
        				 beanReparacionRes.getBeanOrdenReparacionDos().setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
        				 
        				 listaBeanOrdenReparacion.add(beanReparacionRes);
        				 beanResOrdenReparacionDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
        			 }
        		 }
        	}

        	beanResOrdenReparacionDAO.setListBeanOrdenReparacion(listaBeanOrdenReparacion);
        	beanResOrdenReparacionDAO.setCodError(responseDTO.getCodeError());
        	beanResOrdenReparacionDAO.setMsgError(responseDTO.getMessageError());
        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		beanResOrdenReparacionDAO.setCodError(Errores.EC00011B);
    	}
		return beanResOrdenReparacionDAO;
	}
	
	/**
	 * Metodo que obtiene las ordenes en estado PR
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return beanResOrdenReparacionDAO Obejeto del tipo @see BeanResOrdenReparacionDAO
	 */
	@Override
	public BigDecimal obtenerOrdenesReparacionMontos(
			 ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		ResponseMessageDataBaseDTO responseDTO = null;
    	
    	
    	try {
    		String ordenamiento = "";
    		
    		if (sortField != null && !"".equals(sortField)){
    			ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
    		}
    		
        	responseDTO = HelperDAO.consultar(CONSULTA_ORDENES_REPARACION_MONTOS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays.asList(
        			new Object[] { }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	
        	totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    		
    	}
		return totalImporte;
	}
	
	/**
	 * Metodo que Actuliza las Ordenes de reparacion a estado LI
	 * @param beanOrdenReparacion Objeto del tipo @see BeanOrdenReparacion
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResOrdenReparacionDAO Obejeto del tipo @see BeanResOrdenReparacionDAO
	 */
	@Override
	public BeanResOrdenReparacionDAO actualizarOdenesReparacion(
			BeanOrdenReparacion beanOrdenReparacion, ArchitechSessionBean architechSessionBean) {
		BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = new BeanResOrdenReparacionDAO();
	       try{	
	          ResponseMessageDataBaseDTO responseDTO = HelperDAO.actualizar(UPDATE_ORDENES_REPARACION.toString(), 
			   Arrays.asList(new Object[]{beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia()}), 
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	          beanResOrdenReparacionDAO.setCodError(responseDTO.getCodeError());
	       } catch (ExceptionDataAccess e) {
	          showException(e);
	          beanResOrdenReparacionDAO.setCodError(Errores.EC00011B);
	       }
	       return beanResOrdenReparacionDAO;
	}

	/**
	 * Metodo que exporta todos los registros de Orden reparacion
	 * @param beanReqOrdeReparacion Objeto del tipo @see BeanReqOrdeReparacion
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResOrdenReparacionDAO Obejeto del tipo @see BeanResOrdenReparacionDAO
	 */
	public BeanResOrdenReparacionDAO exportarTodoOrdenReparacion(BeanReqOrdeReparacion beanReqOrdeReparacion, 
    		ArchitechSessionBean architechSessionBean){
    	StringBuilder builder = new StringBuilder();
    	final BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = new BeanResOrdenReparacionDAO();
    	Utilerias utilerias = Utilerias.getUtilerias();
        final Date fecha = new Date();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
        	builder.append(TOTAL_CONSULTA_ORDENES_REPARACION); 
        	final String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        	final String nombreArchivo = "SPID_ORD_REP_"+fechaActual+".csv";
        	beanResOrdenReparacionDAO.setNombreArchivo(nombreArchivo);
    	   
        	responseDTO = HelperDAO.insertar(INSERT_TODOS_ORDENES_REPARACION.toString(), 
        			Arrays.asList(new Object[]{architechSessionBean.getUsuario(),
        				                     architechSessionBean.getIdSesion(),
        				                     "SPID",
        				                     "ORDEN_REPARACION",
        				                     nombreArchivo,
        				                     beanReqOrdeReparacion.getTotalReg(),
        				                     "PE",
        				                     builder.toString(),
        				                     new StringBuilder()
        										.append("REFE, COLA, CLAVE TRAN., MECAN, MEDIO ENT., FCH. CAP, ORD, REC, IMPORTE ABONO, "
        				                     +"EST., FOLIO PAQUETE, FOLIO PAGO, MENSAJE ERROR")
        				                     					.toString()
        			}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           
        	beanResOrdenReparacionDAO.setCodError(responseDTO.getCodeError());
        	beanResOrdenReparacionDAO.setMsgError(responseDTO.getMessageError());

        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResOrdenReparacionDAO.setCodError(Errores.EC00011B); 
        }
        
        return beanResOrdenReparacionDAO;
	}
}
