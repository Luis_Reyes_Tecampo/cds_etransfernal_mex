/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCdaFiltrosParteDos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCdaFiltrosParteUno;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDatosGenerales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaOrdenante;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCdaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperSPIDRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para la consulta de Movimientos CDAs
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaMovCDAImpl extends Architech implements DAOConsultaMovCDA {
     

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 5061108675636259361L;
	
	/**Constante TIPO_PAGO*/
	private static final String TIPO_PAGO = "TIPO_PAGO";
	/**Constante TIPO_PAGO*/
	private static final String FCH_HORA_ABONO ="FCH_HORA_ABONO";
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	private static final String QUERY_FECHA_OPERACION =
	" (SELECT TRUNC(FCH_OPERACION,'dd') FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR )";

	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * la cuenta de regisros
	 */
	private static final String QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_CONSULTA = 
	 " SELECT COUNT(1) CONT "+
	   " FROM TRAN_SPEI_CDA T1 "+
	   " wHERE T1.FCH_OPERACION = TO_DATE(?,'dd/mm/yyyy')";

	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el listado de movimientos CDA
	 */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_CONSULTA = 
	" SELECT TMP.REFERENCIA,TMP.FCH_OPERACION, "+
    " TMP.FOLIO_PAQUETE,TMP.FOLIO_PAGO, "+
    " TMP.CVE_MI_INSTITUC,TMP.CVE_INST_ORD, "+
    " TMP.NOMBRE_BCO_ORD,TMP.CVE_RASTREO, "+
    " TMP.NUM_CUENTA_REC,TMP.MONTO, "+
    " TMP.FCH_HORA_ABONO,TMP.FCH_HORA_ENVIO,"+
    " CASE (TMP.ESTATUS) "+
      " WHEN '0' THEN 'PENDIENTE' "+
      " WHEN '1' THEN 'ENVIADO' "+
      " WHEN '2' THEN 'CONFIRMADO' "+
      " WHEN '3' THEN 'PENDIENTE' "+
      " WHEN '4' THEN 'CONTINGENCIA' "+
      " WHEN '5' THEN 'RECHAZADO' "+
      " END CDA, TMP.COD_ERROR, "+
      " TMP.TIPO_PAGO, "+
      "TMP.FCH_CDA, TMP.FOLIO_PAQCDA, TMP.FOLIO_CDA, TMP.MODALIDAD, "+
      "TMP.HORA_ABONO, TMP.NOMBRE_ORD, TMP.TIPO_CUENTA_ORD, TMP.NUM_CUENTA_ORD, "+
      "TMP.RFC_ORD, TMP.NOMBRE_BCO_REC, TMP.NOMBRE_REC, TMP.TIPO_CUENTA_REC, "+
      "TMP.RFC_REC, TMP.CONCEPTO_PAGO, TMP.IVA, TMP.NUM_SERIE_CERTIF, "+
      "TMP.SELLO_DIGITAL, TMP.NOMBRE_REC2, TMP.RFC_REC2, TMP.TIPO_CUENTA_REC2, "+
      "TMP.NUM_CUENTA_REC2, TMP.FOLIO_CODI, TMP.PAGO_COMISION, TMP.MONTO_COMISION, "+
      "TMP.NUM_CEL_ORD, TMP.DIG_VERIFI_ORD, TMP.NUM_CEL_REC, TMP.DIG_VERIFI_REC "+
      " FROM (SELECT TMP.*, ROWNUM AS RN "+
       " FROM (SELECT T1.REFERENCIA,T1.FCH_OPERACION, "+
               " T1.FOLIO_PAQUETE,T1.FOLIO_PAGO, "+
               " T1.CVE_MI_INSTITUC,T1.CVE_INST_ORD, "+
               " T1.NOMBRE_BCO_ORD,T1.CVE_RASTREO, "+
               " T1.NUM_CUENTA_REC,T1.MONTO, "+
               " T1.FCH_HORA_ABONO,T1.FCH_HORA_ENVIO, "+
               " T1.ESTATUS,T1.COD_ERROR,T1.HORA_ENVIO, T1.TIPO_PAGO "+
                " FROM TRAN_SPEI_CDA T1 "+
                " WHERe "+
                " T1.FCH_OPERACION  = TO_DATE(?,'dd/mm/yyyy') ";

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el listado de movimientos CDA
	 */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_2 = 
		" ORDER BY T1.HORA_ENVIO) TMP) TMP "+
		" WHeRE RN BETWEEN ? AND ? "+
		" ORDER BY TMP.HORA_ENVIO";
		
	/**
	 * Constante que almacena el query de la fecha de operacion 2
	 */
	private static final String QUERY_FECHA_OPERACION_2 = 
    " SELECT TO_CHAR(FCH_OPERACION,'dd/mm/yyyy') FCH_OPERACION"+
    " FROM TRAN_SPEI_CTRL CTL, "+
    " (SELECT NVL((SELECT CV_VALOR "+
              " FROM TRAN_CONTIG_UNIX "+
                  " WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'), "+
                 " 40014) CV_VALOR "+
        " FROM DUAL) TMP "+
    " WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR";

	/**
	 * Constante del tipo String que almacena la consulta para solicitar 
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String QUERY_INSERT_CONSULTA_EXPORTAR_TODO = 
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";	
	
	/**
	 * Constante del tipo String que almacena la consulta para solicitar 
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	private static final String COLUMNAS_CONSULTA_MOV_CDA = 
		"Refere,Fecha Ope,Folio paq,Folio pago,Clave SPEI ordenante del abono," +
		"Inst Emisora,Cve rastreo,Cta Beneficiario,Monto,Fecha Abono,Hr Abono, Hr Envio," + 
		"Estatus CDA,Cod. Error,Tipo Pago,Folio Paq Cda,Folio Cda,Modalidad,Nombre Ordenante,"+
		"Tipo Cuenta Ordenante,Numero Cuenta Ordenante,Rfc Ordenante, Banco Ordenante,Nombre Receptor,Tipo Cuenta Rec,"+
		"Rfc Rec,Concepto Pago,Iva,Numero Serie Certif,Sello Digital,Nombre Rec2,Rfc Rec2,Tipo Cuenta Rec2,"+
		"Numero Cuenta Rec2,Folio Codi,Pago Comision,Monto Comision,Numero Cel Ordenante,Dig Cel Ordenante,"+
		"Numero Cel Rec,Dig Verif Rec";
	
	
		
	/**
	 * Constante para la consulta de exportar todos
	 */
	private static final String QUERY_CONSULTA_EXPORTAR_TODO = 
		
		"SELECT T1.REFERENCIA || ',' || to_char(T1.FCH_OPERACION,'DD/MM/YYYY') || ',' || T1.FOLIO_PAQUETE || ',' || T1.FOLIO_PAGO || ',' ||"+ 
		" T1.CVE_INST_ORD || ',' || T1.NOMBRE_BCO_ORD || ',' || T1.CVE_RASTREO || ',' || T1.NUM_CUENTA_REC || ',' || T1.MONTO || ',' || "+
		" to_char(T1.FCH_HORA_ABONO, 'DD/MM/YYYY') || ',' || to_char(T1.FCH_HORA_ABONO, 'HH24:MI:SS') || ',' || to_char(T1.FCH_HORA_ENVIO,'HH24:MI:SS') || ',' || " +
		" CASE(T1.ESTATUS) WHEN '0' THEN 'PENDIENTE' " +
		"WHEN '1' THEN 'ENVIADO' " +
		"WHEN '2' THEN 'CONFIRMADO' " +
		"WHEN '3' THEN 'PENDIENTE' " +
		"WHEN '4' THEN 'CONTINGENCIA' " +
		"WHEN '5' THEN 'RECHAZADO' ELSE '' END" +
		" || ',' || T1.COD_ERROR || ',' || T1.TIPO_PAGO "+
		"|| ',' || T1.FOLIO_PAQCDA || ',' || T1.FOLIO_CDA || ',' || T1.MODALIDAD || ',' || T1.NOMBRE_ORD || ',' || T1.TIPO_CUENTA_ORD || ',' || T1.NUM_CUENTA_ORD || ',' || "+
		"T1.RFC_ORD || ',' || T1.NOMBRE_BCO_REC || ',' || T1.NOMBRE_REC || ',' || T1.TIPO_CUENTA_REC || ',' || T1.RFC_REC || ',' || T1.CONCEPTO_PAGO || ',' || T1.IVA || ',' || T1.NUM_SERIE_CERTIF|| ',' || T1.SELLO_DIGITAL || ',' || "+
		"T1.NOMBRE_REC2 || ',' || T1.RFC_REC2 || ',' || T1.TIPO_CUENTA_REC2 || ',' || T1.NUM_CUENTA_REC2 || ',' || T1.FOLIO_CODI || ',' ||  T1.PAGO_COMISION || ',' || T1.MONTO_COMISION || ',' || "+
		"T1.NUM_CEL_ORD || ',' || T1.DIG_VERIFI_ORD || ',' || T1.NUM_CEL_REC || ',' || T1.DIG_VERIFI_REC "+
		" FROM TRAN_SPEI_CDA T1 WHERE "; 
	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el detalle de un movimiento CDA
	 */
	private static final String QUERY_CONSULTA_DETALLE_MOVIMIENTO_CDA_SELECT = 
		" SELECT  CDA.REFERENCIA, CDA.FCH_OPERACION, CDA.CVE_INST_ORD, "+
	    " CDA.CVE_RASTREO, CDA.NUM_CUENTA_REC, CDA.MONTO, "+
	    " CDA.FCH_HORA_ABONO, CDA.FCH_HORA_ENVIO, CDA.NOMBRE_BCO_ORD, "+ 
	    " CASE(CDA.ESTATUS) WHEN '0' THEN 'PENDIENTE' "+
	    "   WHEN '1' THEN 'ENVIADO'  "+
	    "   WHEN '2' THEN 'CONFIRMADO' "+ 
	    "   WHEN '3' THEN 'PENDIENTE'  "+
	    "   WHEN '4' THEN 'CONTINGENCIA' "+ 
	    "   WHEN '5' THEN 'RECHAZADO' END CDA, "+  
	    " CDA.NOMBRE_ORD, CDA.NUM_CUENTA_ORD, "+
	    " CDA.TIPO_CUENTA_ORD,CDA.RFC_ORD, CDA.NOMBRE_BCO_REC, "+
	    " CDA.TIPO_CUENTA_REC, CDA.NOMBRE_REC NOMBRE_REC, CDA.CONCEPTO_PAGO, "+
	    "CDA.RFC_REC RFC_REC,CDA.TIPO_PAGO ";
	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * el detalle de un movimiento CDA
	 */
	private static final String QUERY_CONSULTA_DETALLE_MOVIMIENTO_CDA_FROM_WHERE =
	    " FROM TRAN_SPEI_CDA CDA where " +
	    " CDA.FCH_OPERACION =  TO_DATE(?,'dd/mm/yyyy') "+
	    " AND CDA.CVE_MI_INSTITUC  = ? "+
	    " AND CDA.CVE_INST_ORD = ? "+
	    " AND CDA.FOLIO_PAQUETE = ? "+
	    " AND CDA.FOLIO_PAGO = ? "+
	    " AND CDA.REFERENCIA = ? ";
	
	/**
	 * Obtiene el query para el detalle del movimiento CDA
	 * @param esSPID Indica si el movimiento es SPID
	 * @return Query
	 */
	private String getQueryDetalle(boolean esSPID) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(QUERY_CONSULTA_DETALLE_MOVIMIENTO_CDA_SELECT);
		
		if (! esSPID) {
			sql.append(",CDA.IVA ");
		}
		
		sql.append(QUERY_CONSULTA_DETALLE_MOVIMIENTO_CDA_FROM_WHERE);
		
		return HelperSPIDRecepcionOperacion.obtenerQuery(sql.toString(), esSPID);
	}
	/**
     * Metodo que se encarga de realizar la consulta de los movimientos CDA
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovCdaDAO consultarMovCDA(BeanReqConsMovCDA beanReqConsMovCDA,ArchitechSessionBean sessionBean) {
    	BeanResConsMovCdaDAO beanResConsMovCdaDAO =  new BeanResConsMovCdaDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanMovimientoCDA> listBeanMovimientoCDA =  null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	String query = null;
    	String queryParam = null;
    	String fechaOperacion = null;
    	try {              	
    		List<Object> parametros = new ArrayList<Object>();
    		responseDTO = HelperDAO.consultar(HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_FECHA_OPERACION_2, beanReqConsMovCDA.getEsSPID()),
    				parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

    		if(responseDTO.getResultQuery()!= null){
       		 list = responseDTO.getResultQuery();
       		for(HashMap<String,Object> map:list){
       			fechaOperacion = utilerias.getString(map.get("FCH_OPERACION"));
       		}
       	}
    		parametros.clear();
    		parametros.add(fechaOperacion);
        	queryParam = armaConsultaParametrizada(parametros,beanReqConsMovCDA);
    		query = HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_CONSULTA_COUNT_MOVIMIENTOS_CDA_CONSULTA + queryParam, beanReqConsMovCDA.getEsSPID());
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		for(HashMap<String,Object> map:list){
        			beanResConsMovCdaDAO.setTotalReg(utilerias.getInteger(map.get("CONT")));
        		}
        	}
        	if(beanResConsMovCdaDAO.getTotalReg()>0){
	        	query = HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_CONSULTA_MOVIMIENTOS_CDA_CONSULTA+queryParam+QUERY_CONSULTA_MOVIMIENTOS_CDA_2, beanReqConsMovCDA.getEsSPID());
	    		 	responseDTO = HelperDAO.consultar(query,obtenerParametrosPag(parametros, beanReqConsMovCDA), 
	      			HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	  list = responseDTO.getResultQuery();
	      	  
	      	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	      		  
	      		listBeanMovimientoCDA = new ArrayList<BeanMovimientoCDA>();
	      		
	      		  for(HashMap<String,Object> map:list){		  			  
	      			  listBeanMovimientoCDA.add(descomponeConsulta(map));   
	      		  }
	      		  beanResConsMovCdaDAO.setListBeanMovimientoCDA(listBeanMovimientoCDA);      		  
	      	  }
	      	beanResConsMovCdaDAO.setCodError(responseDTO.getCodeError());
	      	beanResConsMovCdaDAO.setMsgError(responseDTO.getMessageError());
        }else{
        	beanResConsMovCdaDAO.setCodError(Errores.CODE_SUCCESFULLY);
	      	beanResConsMovCdaDAO.setMsgError(Errores.OK00000V);
	      	listBeanMovimientoCDA = Collections.emptyList();
	      	beanResConsMovCdaDAO.setListBeanMovimientoCDA(listBeanMovimientoCDA);
        }		  
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		beanResConsMovCdaDAO.setCodError(Errores.EC00011B);
  	}
        return beanResConsMovCdaDAO;	
    }
    
    /**
     * Metodo que genera un objeto del tipo BeanMovimientoCDA a partir de la respuesta de la consulta
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanMovimientoCDA Objeto del tipo BeanMovimientoCDA
     */
    private BeanMovimientoCDA descomponeConsulta(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanMovimientoCDA beanMovimientoCDA = null;
    	beanMovimientoCDA = new BeanMovimientoCDA();
		beanMovimientoCDA.setReferencia(utilerias.getString(map.get("REFERENCIA")));
		beanMovimientoCDA.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanMovimientoCDA.setFolioPaq(utilerias.getString(map.get("FOLIO_PAQUETE")));
		beanMovimientoCDA.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
		beanMovimientoCDA.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
		beanMovimientoCDA.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));
		beanMovimientoCDA.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
		beanMovimientoCDA.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
		beanMovimientoCDA.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));
		beanMovimientoCDA.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
		beanMovimientoCDA.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanMovimientoCDA.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDA.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDA.setEstatusCDA(utilerias.getString(map.get("CDA")));
		beanMovimientoCDA.setCodError(utilerias.getString(map.get("COD_ERROR")));
		beanMovimientoCDA.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));
		beanMovimientoCDA.setFolioPaqCda(utilerias.getString(map.get("FOLIO_PAQCDA")));
		beanMovimientoCDA.setFolioCda(utilerias.getString(map.get("FOLIO_CDA")));
		beanMovimientoCDA.setModalidad(utilerias.getString(map.get("MODALIDAD")));
		beanMovimientoCDA.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
		beanMovimientoCDA.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
		beanMovimientoCDA.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
		beanMovimientoCDA.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
		beanMovimientoCDA.setNombreBcoRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));
		beanMovimientoCDA.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
		beanMovimientoCDA.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
		beanMovimientoCDA.setRfcRec(utilerias.getString(map.get("RFC_REC")));
		beanMovimientoCDA.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
		beanMovimientoCDA.setIva(utilerias.getString(map.get("IVA")));
		beanMovimientoCDA.setNumSerieCertif(utilerias.getString(map.get("NUM_SERIE_CERTIF")));
		beanMovimientoCDA.setSelloDigital(utilerias.getString(map.get("SELLO_DIGITAL")));
		beanMovimientoCDA.setNombreRec2(utilerias.getString(map.get("NOMBRE_REC2")));
		beanMovimientoCDA.setRfcRec2(utilerias.getString(map.get("RFC_REC2")));
		beanMovimientoCDA.setTipoCuentaRec2(utilerias.getString(map.get("TIPO_CUENTA_REC2")));
		beanMovimientoCDA.setNumCuentaRec2(utilerias.getString(map.get("NUM_CUENTA_REC2")));
		beanMovimientoCDA.setFolioCodi(utilerias.getString(map.get("FOLIO_CODI")));
		beanMovimientoCDA.setPagoComision(utilerias.getString(map.get("PAGO_COMISION")));
		beanMovimientoCDA.setMontoComision(utilerias.getString(map.get("MONTO_COMISION")));
		beanMovimientoCDA.setNumCelOrd(utilerias.getString(map.get("NUM_CEL_ORD")));
		beanMovimientoCDA.setDigVerifiOrd(utilerias.getString(map.get("DIG_VERIFI_ORD")));
		beanMovimientoCDA.setNumCelRec(utilerias.getString(map.get("NUM_CEL_REC")));
		beanMovimientoCDA.setDigVerifiRec(utilerias.getString(map.get("DIG_VERIFI_REC")));
		return beanMovimientoCDA;
    }

	/**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovCdaDAO guardarConsultaMovExpTodo(BeanReqConsMovCDA beanReqConsMovCDA, ArchitechSessionBean sessionBean) {
            BeanResConsMovCdaDAO beanResConsMovCdaDAO  = new BeanResConsMovCdaDAO();
            
            ResponseMessageDataBaseDTO responseDTO = null;
            Date fecha = new Date();
            Utilerias utilerias = Utilerias.getUtilerias();
            String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
            StringBuilder builder = new StringBuilder();
            builder.append(HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_CONSULTA_EXPORTAR_TODO, beanReqConsMovCDA.getEsSPID()));
            builder.append(generarConsultaExportarTodo(beanReqConsMovCDA));
            builder.append(" ORDER BY T1.FCH_HORA_ENVIO");
            String nombreArchivo = "CDA_CON_MOV_CDAS_"+fechaActual+".csv";
            
                        
            try{            	
            	responseDTO = HelperDAO.insertar(QUERY_INSERT_CONSULTA_EXPORTAR_TODO, 
          			   Arrays.asList(new Object[]{
          					   sessionBean.getUsuario(),
          					   sessionBean.getIdSesion(),
          					   "CDA",
          					   "CONSULTA_MOV_CDAS",
          					   nombreArchivo,
          					   beanReqConsMovCDA.getTotalReg(),
          					   "PE",
          					   builder.toString(),
          					   COLUMNAS_CONSULTA_MOV_CDA}), 
          			   HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
            		beanResConsMovCdaDAO.setNombreArchivo(nombreArchivo);
            		beanResConsMovCdaDAO.setCodError(responseDTO.getCodeError());
            		beanResConsMovCdaDAO.setMsgError(responseDTO.getMessageError()); 
            }catch (ExceptionDataAccess e) {
            	showException(e);
            	beanResConsMovCdaDAO.setCodError(Errores.EC00011B);
			}
			return beanResConsMovCdaDAO;
	}
    
    /**
     * Metodo encargado de consultar el detalle de un movimiento CDA
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDADet
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean 
     * @return BeanResConsMovDetCdaDAO Objeto del tipo @see BeanResConsMovDetCdaDAO
     */
    public BeanResConsMovDetCdaDAO consultarMovDetCDA(BeanReqConsMovCDADet beanReqConsMovCDA, ArchitechSessionBean architechSessionBean) {
    		
    	BeanResConsMovDetCdaDAO beanResConsMovDetCdaDAO = new BeanResConsMovDetCdaDAO();
    	BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales = new BeanResConsMovCdaDatosGenerales();
    	BeanResConsMovCdaOrdenante  beanMovCdaOrdenante = new BeanResConsMovCdaOrdenante();
    	BeanResConsMovCdaBeneficiario beanMovCdaBeneficiario =  new BeanResConsMovCdaBeneficiario();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
        ResponseMessageDataBaseDTO responseDTO = null;
        int numReferencia = Integer.parseInt(beanReqConsMovCDA.getReferencia());
    	int cveMiInstitucLink = Integer.parseInt(beanReqConsMovCDA.getCveMiInstitucLink());
    	int cveSpeiLink = Integer.parseInt(beanReqConsMovCDA.getCveSpeiLink());
    	int folioPaqLink = Integer.parseInt(beanReqConsMovCDA.getFolioPaqLink());
    	int folioPagoLink = Integer.parseInt(beanReqConsMovCDA.getFolioPagoLink());
    	boolean esSPID = Boolean.parseBoolean(beanReqConsMovCDA.getEsSPID());
        
        try {
       	responseDTO = HelperDAO.consultar(getQueryDetalle(esSPID), 
       			Arrays.asList(new Object[]{
       					beanReqConsMovCDA.getFechaOpeLink(),cveMiInstitucLink,cveSpeiLink,
					folioPaqLink,folioPagoLink,numReferencia}), 
				 HelperDAO.CANAL_GFI_DS, this.getClass().getName());
       	
   		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
   			list = responseDTO.getResultQuery();
   			for(HashMap<String,Object> map:list){
   				beanMovCdaDatosGenerales.setReferencia(utilerias.getString(map.get("REFERENCIA")));
   				beanMovCdaDatosGenerales.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
   				beanMovCdaDatosGenerales.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
   				beanMovCdaDatosGenerales.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));
   				beanMovCdaDatosGenerales.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));
   				beanMovCdaDatosGenerales.setEstatusCDA(utilerias.getString(map.get("CDA")));
   				beanMovCdaDatosGenerales.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
   				beanMovCdaDatosGenerales.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));
   				beanMovCdaOrdenante.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));
   				beanMovCdaOrdenante.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
   				beanMovCdaOrdenante.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
   				beanMovCdaOrdenante.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
   				beanMovCdaOrdenante.setCtaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
   				beanMovCdaOrdenante.setRfcCurpOrdenante(utilerias.getString(map.get("RFC_ORD")));
   				
   				beanMovCdaBeneficiario.setNomInstRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));
   				beanMovCdaBeneficiario.setNombreBened(utilerias.getString(map.get("NOMBRE_REC")));
   				beanMovCdaBeneficiario.setTipoCuentaBened(utilerias.getString(map.get("TIPO_CUENTA_REC")));
   				beanMovCdaBeneficiario.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));
   				beanMovCdaBeneficiario.setRfcCurpBeneficiario(utilerias.getString(map.get("RFC_REC")));
   				beanMovCdaBeneficiario.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
   				
   				beanMovCdaBeneficiario.setImporteIVA(
   						esSPID ?utilerias.formateaDecimales(map.get("IVA"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO):"0");
   				beanMovCdaBeneficiario.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
   				beanResConsMovDetCdaDAO.setBeanMovCdaDatosGenerales(beanMovCdaDatosGenerales);
   				beanResConsMovDetCdaDAO.setBeanConsMovCdaOrdenante(beanMovCdaOrdenante);
   				beanResConsMovDetCdaDAO.setBeanConsMovCdaBeneficiario(beanMovCdaBeneficiario);
     		  }
   			
   			beanResConsMovDetCdaDAO.setCodError(responseDTO.getCodeError());
			beanResConsMovDetCdaDAO.setMsgError(responseDTO.getMessageError());
   		}
   	} catch (ExceptionDataAccess e) {
   		showException(e);
   		beanResConsMovDetCdaDAO.setCodError(Errores.EC00011B);

   	} 
         return beanResConsMovDetCdaDAO;   

   	}         

    
    /**
     * Metodo para generar la consulta de exportar todo
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @return String Objeto del tipo @see String
     */
    private String generarConsultaExportarTodo(BeanReqConsMovCDA beanReqConsMovCDA){    	
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";    	
    	String estatusCDA = null;
    	String estatusContingencia = "4";
    	String estatusPendientes = "3";
    	String valEstatusPendientes = "0,3";
    	BeanReqConsMovCdaFiltrosParteUno beanCdaFiltrosParteUno = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno();
    	BeanReqConsMovCdaFiltrosParteDos beanCdaFiltrosParteDos = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos();
    	builder.append(" TRUNC(t1.FCH_OPERACION,'DD') = ");
		builder.append(HelperSPIDRecepcionOperacion.obtenerQuery(QUERY_FECHA_OPERACION, beanReqConsMovCDA.getEsSPID()));
    	if(beanCdaFiltrosParteDos.getOpeContingencia()){
    		estatusCDA = estatusContingencia;
    		builder.append(" AND T1.ESTATUS IN (");
        	builder.append(estatusCDA);
        	builder.append(")");
    	}else if(beanCdaFiltrosParteDos.getEstatus() != null && (!vacio.equals(beanCdaFiltrosParteDos.getEstatus()))){    		
    		if(estatusPendientes.equals(beanCdaFiltrosParteDos.getEstatus())){
    				estatusCDA = valEstatusPendientes;
    			}else{
    				estatusCDA = beanCdaFiltrosParteDos.getEstatus();
    			}
    		builder.append(" AND T1.ESTATUS IN (");
        	builder.append(estatusCDA);
        	builder.append(")");
    	}
    	if(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ") ; 
    		builder.append(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono());
    	}
    	if(beanCdaFiltrosParteUno.getHrAbonoIniDesde() != null && beanCdaFiltrosParteUno.getHrAbonoFinHasta() != null &&
    			(!vacio.equals(beanCdaFiltrosParteUno.getHrAbonoIniDesde())) && (!vacio.equals(beanCdaFiltrosParteUno.getHrAbonoFinHasta()))){
			  builder.append(" AND T1.HORA_ABONO between ");
		      builder.append(beanCdaFiltrosParteUno.getHrAbonoIniDesde().replaceAll(":", ""));
		      builder.append(" AND ");
		      builder.append(beanCdaFiltrosParteUno.getHrAbonoFinHasta().replaceAll(":", ""));
    	}    	
    	builder.append(generarConsultaExportarTodo2(beanReqConsMovCDA));
    	return builder.toString();
    }
    
    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo
     * @param beanReqConsMovCDA objeto del tipo @see BeanReqConsMovHistCDA
     * @return String objeto del tipo @see String
     */
    private String generarConsultaExportarTodo2(BeanReqConsMovCDA beanReqConsMovCDA){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	BeanReqConsMovCdaFiltrosParteUno beanCdaFiltrosParteUno = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno();
    	BeanReqConsMovCdaFiltrosParteDos beanCdaFiltrosParteDos = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos();

    	if(beanCdaFiltrosParteDos.getMontoPago() != null && (!vacio.equals(beanCdaFiltrosParteDos.getMontoPago()))){
    		 builder.append(" AND T1.MONTO = ");
    		 String monto = beanCdaFiltrosParteDos.getMontoPago();
    		 builder.append(monto.replaceAll(",", ""));
    	} 
    	if(beanCdaFiltrosParteDos.getCtaBeneficiario() != null &&(!vacio.equals(beanCdaFiltrosParteDos.getCtaBeneficiario()))){
    		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM('%");
    	     builder.append(beanCdaFiltrosParteDos.getCtaBeneficiario());
    	     builder.append("')");
    	}
    	if(beanCdaFiltrosParteDos.getCveRastreo() != null && (!vacio.equals(beanCdaFiltrosParteDos.getCveRastreo()))){
    		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM('%");
        	builder.append(beanCdaFiltrosParteDos.getCveRastreo());
        	builder.append("')");
    	}
    	if(beanCdaFiltrosParteDos.getRefTransfer() != null && (!vacio.equals(beanCdaFiltrosParteDos.getRefTransfer()))){
    		builder.append(" AND T1.REFERENCIA = ");
        	builder.append(beanCdaFiltrosParteDos.getRefTransfer());    	
        }
    	
    	if((beanCdaFiltrosParteUno.getHrEnvioIniDesde() != null && beanCdaFiltrosParteUno.getHrEnvioFinHasta() != null &&
    			!vacio.equals(beanCdaFiltrosParteUno.getHrEnvioIniDesde())) && (!vacio.equals(beanCdaFiltrosParteUno.getHrEnvioFinHasta()))){
       		builder.append(" AND T1.HORA_ENVIO between ");        
            builder.append(beanCdaFiltrosParteUno.getHrEnvioIniDesde().replaceAll(":", ""));
            builder.append(" AND ");
            builder.append(beanCdaFiltrosParteUno.getHrEnvioFinHasta().replaceAll(":", ""));
    	}
    	if(beanCdaFiltrosParteUno.getDesc() != null && (!vacio.equals(beanCdaFiltrosParteUno.getDesc()))){
    		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER('%");
            builder.append(beanCdaFiltrosParteUno.getDesc());
            builder.append("')");
    	}
    	
    	
    	return builder.toString();
    }
    
	/**
     * Metodo para sirve para generar el query y parametrizar la consulta
     * @param parametros Objeto del tipo @see List<Object>
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @return String Objeto del tipo @see String
     */
   private String armaConsultaParametrizada(List<Object> parametros, BeanReqConsMovCDA beanReqConsMovCDA){    	    	
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";    	
    	String estatusCDA = null;
    	String estatusContingencia = "4";
    	String estatusPendientes = "3";
    	String valEstatusPendientes = "0,3";
    	BeanReqConsMovCdaFiltrosParteUno beanCdaFiltrosParteUno = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno();
    	BeanReqConsMovCdaFiltrosParteDos beanCdaFiltrosParteDos = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos();
    	if(beanCdaFiltrosParteDos.getOpeContingencia()){
    		builder.append(" AND T1.ESTATUS = ?");        	
        	parametros.add(estatusContingencia);
    	}else if(beanCdaFiltrosParteDos.getEstatus() != null && (!vacio.equals(beanCdaFiltrosParteDos.getEstatus()))){    		
    		if(estatusPendientes.equals(beanCdaFiltrosParteDos.getEstatus())){
    				estatusCDA = valEstatusPendientes;
    				builder.append(" AND T1.ESTATUS IN (?,?)");
    				parametros.add("0");
    				parametros.add("3");
    			}else{
    				estatusCDA = beanCdaFiltrosParteDos.getEstatus();
    				builder.append(" AND T1.ESTATUS IN (?)");
    				parametros.add(estatusCDA);
    			}
    	}
    	if(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ? ") ; 
    		parametros.add(beanCdaFiltrosParteUno.getCveSpeiOrdenanteAbono());
    	}
    	if(beanCdaFiltrosParteUno.getHrAbonoIniDesde() != null && beanCdaFiltrosParteUno.getHrAbonoFinHasta() != null &&
    			(!vacio.equals(beanCdaFiltrosParteUno.getHrAbonoIniDesde())) && (!vacio.equals(beanCdaFiltrosParteUno.getHrAbonoFinHasta()))){
			  builder.append(" AND HORA_ABONO ");
			  builder.append(" between ?  and ? ");
		      parametros.add(beanCdaFiltrosParteUno.getHrAbonoIniDesde().replaceAll(":", ""));
		      parametros.add(beanCdaFiltrosParteUno.getHrAbonoFinHasta().replaceAll(":", ""));
    	}    	
       	if(beanCdaFiltrosParteDos.getMontoPago() != null && (!vacio.equals(beanCdaFiltrosParteDos.getMontoPago()))){
      		 builder.append(" AND T1.MONTO = ?");
      		 String monto = beanCdaFiltrosParteDos.getMontoPago();
      		 parametros.add(monto.replaceAll(",", ""));
      	} 
    	
    	
    	builder.append(armaConsultaParametrizada2(parametros, beanReqConsMovCDA));
    	return builder.toString();
    }
   
	/**
     * Metodo para sirve para generar el query y parametrizar la consulta
    * @param parametros Objeto del tipo @see List<Object>
    * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
    * @return String Objeto del tipo @see String
    */
  private String armaConsultaParametrizada2(List<Object> parametros, BeanReqConsMovCDA beanReqConsMovCDA){    	    	
   	StringBuilder builder = new StringBuilder("");
   	String vacio = "";    	
   	BeanReqConsMovCdaFiltrosParteUno beanCdaFiltrosParteUno = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno();
   	BeanReqConsMovCdaFiltrosParteDos beanCdaFiltrosParteDos = beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos();
   	if(beanCdaFiltrosParteDos.getRefTransfer() != null && (!vacio.equals(beanCdaFiltrosParteDos.getRefTransfer()))){
   		builder.append(" AND T1.REFERENCIA = ? ");
   		parametros.add(beanCdaFiltrosParteDos.getRefTransfer());    	
       }
   	if(beanCdaFiltrosParteDos.getCtaBeneficiario() != null &&(!vacio.equals(beanCdaFiltrosParteDos.getCtaBeneficiario()))){
   		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM(?)");
   		 parametros.add("%"+beanCdaFiltrosParteDos.getCtaBeneficiario());
   	}
   	
   	if(beanCdaFiltrosParteDos.getCveRastreo() != null && (!vacio.equals(beanCdaFiltrosParteDos.getCveRastreo()))){
   		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM(?)");
       	parametros.add("%"+beanCdaFiltrosParteDos.getCveRastreo());
   	}
   	
   	if((beanCdaFiltrosParteUno.getHrEnvioIniDesde() != null && beanCdaFiltrosParteUno.getHrEnvioFinHasta() != null &&
			!vacio.equals(beanCdaFiltrosParteUno.getHrEnvioIniDesde())) && (!vacio.equals(beanCdaFiltrosParteUno.getHrEnvioFinHasta()))){
		builder.append(" AND T1.HORA_ENVIO "); 
		builder.append(" between ? and ?");            
        parametros.add(beanCdaFiltrosParteUno.getHrEnvioIniDesde().replaceAll(":", ""));
	    parametros.add(beanCdaFiltrosParteUno.getHrEnvioFinHasta().replaceAll(":", ""));
	}
   	
   	if(beanCdaFiltrosParteUno.getDesc() != null && (!vacio.equals(beanCdaFiltrosParteUno.getDesc()))){
   		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER(?)");
   		parametros.add("%"+beanCdaFiltrosParteUno.getDesc());
   	}
   	if(beanCdaFiltrosParteUno.getTipoPago() != null && (!vacio.equals(beanCdaFiltrosParteUno.getTipoPago()))){
   		builder.append(" AND TIPO_PAGO = ?");
   		parametros.add(beanCdaFiltrosParteUno.getTipoPago());
   	}
   	return builder.toString();
   }

  	/**
  	 * Mertodo para obtener los parametros de paginacion
  	 * 
  	 * @param parametros El parametro parametros
  	 * @param pag El parametro pag
  	 * @return parametros Los parametros
  	 */
    private List<Object> obtenerParametrosPag(
    		List<Object> parametros,
    		BeanReqConsMovCDA pag) {
    	parametros.add(pag.getPaginador().getRegIni());
    	parametros.add(pag.getPaginador().getRegFin());
    	return parametros;
    }
    
   
}
