/**
 * Isban Mexico
 * Clase: DAOCatParticipantesSPIDImpl.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 09/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanParticipantesSPID;
import mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes;
import mx.isban.eTransferNal.beans.catalogos.BeanResParticipantesSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOCatParticipantesSPIDImpl extends Architech implements DAOCatParticipantesSPID{

	/**  Propiedad del tipo long que almacena el valor de serialVersionUID*/
	private static final long serialVersionUID = -648002797759084519L;

	/**  Propiedad del tipo String que almacena el valor de QUERY_CONSULTA*/
	private static final String QUERY_CONSULTA = "SELECT * FROM ("
												+ "SELECT  ROWNUM RNUM, CIF.CVE_INTERME, CIF.NUM_BANXICO, CIF.NOMBRE_LARGO, TSI.FCH_ALTA "
												+ " FROM TRAN_SPID_INTERME TSI, COMU_INTERME_FIN CIF "
												+ " WHERE TSI.CVE_INTERME = CIF.CVE_INTERME "
												+ "  AND TSI.CVE_INTERME = nvl(?,TSI.CVE_INTERME) "
												+ ") "
												+ "WHERE RNUM BETWEEN ? AND  ? ";
	
	/**  Propiedad del tipo String que almacena el valor de QUERY_ALTA*/
	private static final String QUERY_ALTA = "SELECT CVE_INTERME, NUM_BANXICO, NOMBRE_LARGO, FCH_ALTA "
											 + "FROM COMU_INTERME_FIN "
											 + "WHERE CVE_INTERME NOT IN (SELECT CVE_INTERME FROM TRAN_SPID_INTERME) "
											 + "AND FCH_BAJA IS NULL "
											 + "  AND UPPER(TRIM(CVE_INTERME)) = UPPER(TRIM(?)) ";
	
	
	/**  Propiedad del tipo String que almacena el valor de INSERT_INTER*/
	private static final String INSERT_INTER = "INSERT INTO TRAN_SPID_INTERME"
			 + " VALUES(?,(SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE CV_PARAMETRO = 'ENTIDAD_SPID'),SYSDATE, ?)";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DELETE
	 * 
	 */
	private static final String QUERY_DELETE = "DELETE FROM TRAN_SPID_INTERME WHERE UPPER(TRIM(CVE_INTERME)) = ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DELETE
	 * 
	 */
	private static final String QUERY_COUNT =  "SELECT COUNT(1) CONT "
												+ " FROM TRAN_SPID_INTERME TSI, COMU_INTERME_FIN CIF "
												+ " WHERE TSI.CVE_INTERME = CIF.CVE_INTERME "
												+ "  AND TSI.CVE_INTERME = nvl(?,TSI.CVE_INTERME)";
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCatParticipantesSPID#consultaParticipantes(java.lang.String, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResParticipantesSPID consultaParticipantes(BeanReqParticipantes cveIntermediario,
			ArchitechSessionBean architectSessionBean) {
		BeanResParticipantesSPID response = new BeanResParticipantesSPID();
		List<BeanParticipantesSPID> listBean = new ArrayList<BeanParticipantesSPID>();
		List<HashMap<String,Object>> list = null;
	    Utilerias utilerias = Utilerias.getUtilerias();
	    ResponseMessageDataBaseDTO responseDTO = null;

	    try {
	    	responseDTO = HelperDAO.consultar(QUERY_COUNT, Arrays.asList(new Object[]{cveIntermediario.getCveIntermediario()}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	
	    	if (responseDTO.getResultQuery() != null) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					response.setTotalReg(utilerias.getInteger(map
							.get("CONT")));
				}
			}
	    	if (response.getTotalReg() > 0) {
	    		
	    	}
	    	
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA, 
					Arrays.asList(new Object[]{cveIntermediario.getCveIntermediario(), cveIntermediario.getPaginador().getRegIni(), 
							cveIntermediario.getPaginador().getRegFin() }), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			

			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				for(HashMap<String, Object> map: list){
					BeanParticipantesSPID bean = new BeanParticipantesSPID();
					bean.setCveIntermediario(utilerias.getString(map.get("CVE_INTERME")));
					bean.setNumBanxico(utilerias.getString(map.get("NUM_BANXICO")));
					bean.setNombre(utilerias.getString(map.get("NOMBRE_LARGO")));
					bean.setFecha(utilerias.getString(map.get("FCH_ALTA")));					
					listBean.add(bean);
				}
				 response.setListParticipantes(listBean);
				 response.setCodError(Errores.CODE_SUCCESFULLY);
		    }else{
		    	response.setCodError(Errores.ED00011V);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCatParticipantesSPID#guardarCatalogo(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase guardarParticipante(BeanReqParticipantes request,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase response = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();
		listaParametros.add(request.getCveIntermediario());
		listaParametros.add(architectSessionBean.getUsuario());
		
		try {
			HelperDAO.insertar(INSERT_INTER, listaParametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
		}

		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCatParticipantesSPID#consultaPartiAlta(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResParticipantesSPID consultaPartiAlta(BeanReqParticipantes cveIntermediario,
			ArchitechSessionBean architectSessionBean) {
		BeanResParticipantesSPID response = new BeanResParticipantesSPID();
		List<BeanParticipantesSPID> listBean = new ArrayList<BeanParticipantesSPID>();
		List<HashMap<String,Object>> list = null;
	    Utilerias utilerias = Utilerias.getUtilerias();
	    ResponseMessageDataBaseDTO responseDTO = null;

	    try {
			responseDTO = HelperDAO.consultar(QUERY_ALTA, Arrays.asList(new Object[]{cveIntermediario.getCveIntermediario()}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				for(HashMap<String, Object> map: list){
					BeanParticipantesSPID bean = new BeanParticipantesSPID();
					bean.setCveIntermediario(utilerias.getString(map.get("CVE_INTERME")));
					bean.setNumBanxico(utilerias.getString(map.get("NUM_BANXICO")));
					bean.setNombre(utilerias.getString(map.get("NOMBRE_LARGO")));
					bean.setFecha(utilerias.getString(map.get("FCH_ALTA")));
					listBean.add(bean);
				}
				 response.setListParticipantes(listBean);
				 response.setCodError(Errores.OK00000V);
		    }else{
		    	response.setCodError(Errores.ED00011V);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOCatParticipantesSPID#eliminaParticipante(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase eliminaParticipante(BeanReqParticipantes request, ArchitechSessionBean architectSessionBean) {
		BeanResBase response = new BeanResBase();
		
		try {
			HelperDAO.eliminar(QUERY_DELETE, Arrays.asList(
					new Object[]{
							request.getCveIntermediario()}
					),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			response.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
		}

		return response;
	}

}
