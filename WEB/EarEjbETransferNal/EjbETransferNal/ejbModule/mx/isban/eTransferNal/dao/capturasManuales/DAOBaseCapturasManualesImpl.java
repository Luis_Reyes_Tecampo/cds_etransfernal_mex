/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOBaseCapturasManuales.java
 *  
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	31 Mayo 2017	Omar Zuniga Lagunas	Vector	Creacion 
 * */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCombo;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Implementacion DAO base capturas manuales
 */
//Implementacion DAO base capturas manuales
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBaseCapturasManualesImpl extends Architech implements DAOBaseCapturasManuales {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1571276379861750656L;

	/** La constante UTILS. */
	protected static final Utilerias UTILS = Utilerias.getUtilerias();
	
	/**
	 * Obtener el objeto: combo valores.
	 *
	 * @param query El objeto: query
	 * @param session the session
	 * @return El objeto: combo valores
	 */
	//Metodo que obtiene los valores cuando hay combos
	public List<BeanCombo> getComboValores(String query, ArchitechSessionBean session){
		//Instancia de lista que se regresa con los valores del combo 
		List<BeanCombo> vaoresCombo = new ArrayList<BeanCombo>();
		//Objeto de consulta
		List<HashMap<String,Object>> list = null;
		//Objeto de respuesta BD
	    ResponseMessageDataBaseDTO responseDTO = null;

		BeanCapturaCentralResponse beanResDAO = new BeanCapturaCentralResponse();
	    try{
	    	//Se realiza el query
	       responseDTO = HelperDAO.consultar(query, Arrays.asList(
	      		 new Object[]{}
	       ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	       //Se valida y proces la informacion consultada
	       if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	          list = responseDTO.getResultQuery();
	          //Crea un nuevo objeto para almacenar los valores del combo
	          vaoresCombo = new ArrayList<BeanCombo>();
	          BeanCombo combo;
	          for(HashMap<String,Object> map:list) {
	        	  //Nuevo valor del combo por cada registro
	        	  combo = new BeanCombo();
	        	  combo.setId(UTILS.getString(map.get("ID")));
	        	  combo.setValor(UTILS.getString(map.get("VALOR")));
	        	  vaoresCombo.add(combo);
	          }
	          //Agrega calores del combo
	          combo = new BeanCombo();
	          combo.setId(StringUtils.EMPTY);
        	  combo.setValor(StringUtils.EMPTY);
        	  vaoresCombo.add(combo);
	       }
	       //Se cacah la excecion 
	       } catch (ExceptionDataAccess e) {
	    	   showException(e);
	    	   beanResDAO.setMsgError(Errores.DESC_EC00011B);
	    	   beanResDAO.setCodError(Errores.EC00011B);
	      }
	    //Retorna la excepcion
	    return vaoresCombo;
	}
	
}