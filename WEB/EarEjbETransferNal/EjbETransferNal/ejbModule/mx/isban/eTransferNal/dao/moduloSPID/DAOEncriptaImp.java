/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOEncriptaImp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.security.KeyStoreException;
import java.util.HashMap;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.ConfigFactory;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResDesencripta;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEncripta;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.idc.core.appserver.IsbKeyStorePublicOps;
import mx.isban.idc.core.appserver.IsbProvider;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOEncriptaImp extends Architech implements DAOEncripta{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 2279941069041703380L;
	/**
	 * Constante que almacena el nombre del paquete para ejecutar la encriptacion o desencriptacion
	 */
	private static final String PAQUETE_TRAN_CRIPTO = "PKG_TRAN_CRIPTO_FRONT";
	
	/**
	 * Constante que almacena el nombre del paquete para ejecutar los catalogos
	 */
	private static final String SP_TRAN_CRIPTO = "ENCRIPCION";
	
		/**
	 * Constante que almacena el nombre del paquete para ejecutar los catalogos
	 */
	private static final String SP_TRAN_DESCRIPTO = "DESENCRIPCION";

	@Override
	public BeanResEncripta encripta(String msg,ArchitechSessionBean architechSessionBean) {
		BeanResEncripta beanResEncripta = new BeanResEncripta();
		Utilerias utilerias = Utilerias.getUtilerias();
		String alias = "alias_traKey";
		String llave="";
      
        RequestMessageDataBaseDTO  requestDTO = new RequestMessageDataBaseDTO();
        DataAccess                 ldtaDataase = null;

        try {
        
			IsbKeyStorePublicOps isbKey = IsbProvider.getInstance();
			byte array[] =isbKey.getSecretData(alias);
			if(array!=null && array.length>0){
				llave = new String(array);
			}
			 debug("::::::::::::::La llave es:"+llave);
             ldtaDataase = DataAccess.getInstance(requestDTO, this);
             requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_STORE_PROCEDURE);
             requestDTO.setCodeOperation(SP_TRAN_CRIPTO);
             requestDTO.setSpName(PAQUETE_TRAN_CRIPTO+"."+SP_TRAN_CRIPTO);
             requestDTO.addParam("CADENA", msg);
             requestDTO.addParam("LLAVE", llave);
             debug("::::::::::::::Paquete:"+PAQUETE_TRAN_CRIPTO);
             ResponseMessageDataBaseDTO responseDTO   = (ResponseMessageDataBaseDTO)ldtaDataase.execute(HelperDAO.CANAL_GFI_DS);
             if(responseDTO.getCodeError().equals(ConfigFactory.CODE_SUCCESFULLY)){
            	 HashMap<String,Object>            resultado   = responseDTO.getResultSP();
            	 beanResEncripta.setCodError( utilerias.getString(resultado.get("COD_RESP")));
            	 beanResEncripta.setMsgError( utilerias.getString(resultado.get("MENSAJE")));
            	 beanResEncripta.setCadEncriptada( utilerias.getString(resultado.get("ENCRIPCION")));            	 
             }
             debug("::::::::::::::Ejecuta Paquete:"+PAQUETE_TRAN_CRIPTO);
		} catch (ExceptionDataAccess e) {
			error("Error por ExceptionDataAccess:"+e.getMessage());
			showException(e);
		} catch (KeyStoreException e) {
			error("Error por KeyStoreException:"+e.getMessage());
			showException(e);
		}
		return beanResEncripta;
	}

	@Override
	public BeanResDesencripta desEncripta(String msg,
			ArchitechSessionBean architechSessionBean) {
		BeanResDesencripta beanResDesencripta = new BeanResDesencripta();
		Utilerias utilerias = Utilerias.getUtilerias();
		String alias = "alias_traKey";
		String llave="";
      
        RequestMessageDataBaseDTO  requestDTO = new RequestMessageDataBaseDTO();
        DataAccess                 ldtaDataase = null;

        try {
        	
				IsbKeyStorePublicOps isbKey = IsbProvider.getInstance();
				byte array[] =isbKey.getSecretData(alias);
				if(array!=null && array.length>0){
					llave = new String(array);
				}
			
              ldtaDataase = DataAccess.getInstance(requestDTO, this);
             requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_STORE_PROCEDURE);
             requestDTO.setCodeOperation(SP_TRAN_DESCRIPTO);
             requestDTO.setSpName(PAQUETE_TRAN_CRIPTO+"."+SP_TRAN_DESCRIPTO);
             requestDTO.addParam("CADENA_ENCRIP", msg);
             requestDTO.addParam("LLAVE", llave);
             ResponseMessageDataBaseDTO responseDTO   = (ResponseMessageDataBaseDTO)ldtaDataase.execute(HelperDAO.CANAL_GFI_DS);
             if(responseDTO.getCodeError().equals(ConfigFactory.CODE_SUCCESFULLY)){
            	 HashMap<String,Object>            resultado   = responseDTO.getResultSP();
            	 beanResDesencripta.setCodError( utilerias.getString(resultado.get("COD_RESP")));
            	 beanResDesencripta.setMsgError( utilerias.getString(resultado.get("MENSAJE")));
            	 beanResDesencripta.setCadDesencriptada(utilerias.getString(resultado.get("DESENCRIPCION")));
             }

		} catch (ExceptionDataAccess e) {
			showException(e);
		} catch (KeyStoreException e) {
			showException(e);
		}
		return beanResDesencripta;
	}

}
