/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOHistorialMsjImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:42:40 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasPagos;

/**
 * Class DAOHistorialMsjImpl.
 *
 * Clase utilizada para consultar el 
 * historico de los mensajes por referencia.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOHistorialMsjImpl extends Architech  implements DAOHistorialMsj {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2172989412504772252L;

	/** La constante CONSULTA_HISTORIAL. */
	public static final StringBuilder CONSULTA_HISTORIAL = 
			/** Creacion de la consulta
			 * 	principal para obtener el
			 * 	historial de los mensajes.
			 * 
			 **/
			new StringBuilder()
			.append(" SELECT H.* FROM (") /** Creacion de la consulta **/
			.append(" SELECT H.*, ROWNUM R FROM ( ")
			.append(" SELECT T2.ESTATUS DEL, ")
			.append(" CASE T2.ESTATUS ")
		    .append(" WHEN 'LB' ")
		    .append(" THEN 'Liberado' ")
		    .append(" WHEN 'DE' ")
		    .append(" THEN 'Detenido' ")
		    .append(" WHEN 'CA' ")
		    .append(" THEN 'Cancelado' ")
		    .append(" WHEN 'EN' ")
		    .append(" THEN 'Enviado' ")
		    .append(" WHEN 'CO' ")
		    .append(" THEN 'Confirmado' ")
		    .append(" WHEN 'DV' ")
		    .append(" THEN 'Devuelto' ")
		    .append(" ELSE ' ' ")
		    .append(" END ESTATUS, ")
		    .append(" T2.ESTATUS_POS AL, ")
		    .append(" CASE T2.ESTATUS_POS ")
		    .append(" WHEN 'LB' ")
		    .append(" THEN 'Liberado' ")
		    .append(" WHEN 'DE' ")
		    .append(" THEN 'Detenido' ")
		    .append(" WHEN 'CA' ")
		    .append(" THEN 'Cancelado' ")
		    .append(" WHEN 'EN' ")
		    .append(" THEN 'Enviado' ")
		    .append(" WHEN 'CO' ")
		    .append(" THEN 'Confirmado' ")
		    .append(" WHEN 'DV' ")
		    .append(" THEN 'Devuelto' ")
		    .append(" ELSE ' ' ")
		    .append(" END ESTATUS2, ")
		    .append(" T2.CVE_USUARIO USUARIO, ")
		    .append(" TO_CHAR(T2.FCH_MODIFICACION,'DD/MM/YYYY HH24:MI:SS') FECHA, ")
            .append(" D.CONT ")
            .append(" FROM TRAN_MENSAJE T1, ")
            .append(" TRAN_HIST_MENS T2 , ")
            .append(" (SELECT COUNT(*) CONT FROM  TRAN_MENSAJE T3, ")
            .append(" TRAN_HIST_MENS T4 WHERE   T3.REFERENCIA= T4.REFERENCIA  ")
            .append(" AND T3.REFERENCIA=?" )
            .append( " ) D ")
            .append(" WHERE T1.REFERENCIA= T2.REFERENCIA   ")
            .append(" AND T1.REFERENCIA=? [ORDENAMIENTO] ")
            .append( " )H")
            .append( " )H WHERE R BETWEEN ? AND ? ");
	
	/** La constante utils. */
	private static final UtileriasPagos utils = UtileriasPagos.getUtils();
	
	/**
	 * Obtener historial.
	 *
	 * Consultar el historial de los  pagos.
	 * 
	 * @param modulo El objeto: modulo
	 * @param paginador El objeto: paginador
	 * @param session El objeto: session
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @return Objeto bean historial mensajes DAO
	 */
	@Override
	public BeanHistorialMensajesDAO obtenerHistorial(BeanModulo modulo, BeanPaginador paginador,
			ArchitechSessionBean session, BeanReqHistorialMensajes beanReqHistorialMensajes) {
		/**se declara variable ref*/
		String refHist="";
		refHist=beanReqHistorialMensajes.getRef();
		/**se declara list*/
		List<HashMap<String, Object>> list = null;
		/**se declara utilerias*/
		Utilerias utilerias = Utilerias.getUtilerias();
		/**se declara responseDTO*/
    	ResponseMessageDataBaseDTO responseDTOHist = null;
    	/**se declara parametros*/
    	List<Object> parametrosHist = new ArrayList<Object>();
    	/**Seteando parametros*/
    	parametrosHist.add(refHist);
    	parametrosHist.add(refHist);
    	parametrosHist.add(paginador.getRegIni());
    	parametrosHist.add(paginador.getRegFin());
    	/** se declaran el objeto beanHistorialMensajesDAO*/
    	final BeanHistorialMensajesDAO beanHistorialMensajesDAO = new BeanHistorialMensajesDAO();
    	/**se declaran el objeto listaHistorialMensajes*/
    	List <BeanHistorialMensajes> listaHistorialMensajes = null;
		try {
			
			/**se declara la varible sortField*/
			String sortField = utils.getSortField(beanReqHistorialMensajes);
			/**se declara la variable ordenamiento*/
			String ordenamiento = "";
			
			/**Se valida orden by*/
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(beanReqHistorialMensajes.getSortType()).toString();
			}
			/**se ejcuta consulta */
			responseDTOHist = HelperDAO.consultar(CONSULTA_HISTORIAL.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), parametrosHist, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
			/**se valida resultado vacio*/
			if(responseDTOHist.getResultQuery()!=null){
				list=responseDTOHist.getResultQuery();
				/**Se valida lista vacia*/
				if(!list.isEmpty()){
					/**seteo de lista*/
					listaHistorialMensajes = new ArrayList<BeanHistorialMensajes>();
						/** Invocacion del metodo de llenado **/
						utils.llenarhistorialMensajes(utilerias, beanHistorialMensajesDAO, listaHistorialMensajes, list);
				}
			}
			/**seteand bean al response*/
			beanHistorialMensajesDAO.setBeanHistorialMensajes(listaHistorialMensajes);
			beanHistorialMensajesDAO.setCodError(responseDTOHist.getCodeError());
			beanHistorialMensajesDAO.setMsgError(responseDTOHist.getMessageError());
		} catch (ExceptionDataAccess e) {
			/**ocurrio un error*/
    		showException(e);
    		beanHistorialMensajesDAO.setCodError(Errores.EC00011B);
    		beanHistorialMensajesDAO.setMsgError(Errores.DESC_EC00011B);
    	}
		/**return beanHistorialMensajesDAO*/
		return beanHistorialMensajesDAO;
	}

}
