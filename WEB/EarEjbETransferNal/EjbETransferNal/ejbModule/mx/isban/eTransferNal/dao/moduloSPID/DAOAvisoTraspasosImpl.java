/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOAvisoTraspasosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Session Bean implementation class DAOConsultaMovHistCDAImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOAvisoTraspasosImpl extends Architech implements
		DAOAvisoTraspasos {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 1041913510555848982L;

	/***
	 * Constante del tipo String que almacena la consulta para obtener todos
	 */
	public static final StringBuilder CONSULTA_AVISO = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append("SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO")
			.append("||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,")
			.append("T.MONTO,T.REFERENCIA_SIAC, C.CONT ")
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener registros
	 * con fecha y cveinst
	 */
	public static final StringBuilder CONSULTA_AVISO_PAR = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append("SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO")
			.append("||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,")
			.append("T.MONTO,T.REFERENCIA_SIAC, C.CONT ")
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");

	/**
	 * Consulta para buscar todos los traspasos
	 */
	private static final StringBuilder TOTAL_CONSULTA_AVISO_TRAPASOS = new StringBuilder()
			.append("SELECT T.HORA_RECEPCION || ',' || T.FOLIO_SERVIDOR || ',' || T.TIPO_TRASPASO")
			.append(" || ',' || DECODE(T.TIPO_TRASPASO,3,'SIAC-SPEI',4,'SIDV-SIAC-SPEI','') || ',' || ")
			.append("T.MONTO || ',' || T.REFERENCIA_SIAC ")
			.append("FROM TRAN_TRSIAC_SPID T");

	/**
	 * Consulta de insercion en tabla con nombre de archivo de exportar todo
	 */
	private static final StringBuilder INSERT_TODOS_AVISO_TRASPASO = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA ")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) ")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * Consulta de sumatoria de montos
	 */
	public static final StringBuilder CONSULTA_AVISO_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) AS MONTO_TOTAL FROM (")
			.append("SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO")
			.append("||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,")
			.append("T.MONTO,T.REFERENCIA_SIAC, C.CONT ")
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener registros
	 * con fecha y cveinst
	 */
	public static final StringBuilder CONSULTA_AVISO_PAR_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) MONTO_TOTAL FROM (")
			.append("SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO")
			.append("||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,")
			.append("T.MONTO,T.REFERENCIA_SIAC, C.CONT ")
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ");

	/**
	 * Metodo que sirve para consultar los aviso traspaso * @param beanPaginador
	 * Objeto del tipo @see BeanPaginador
	 * 
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param sortField
	 *            Objeto del tipo String
	 * @param sortType
	 *            Objeto del tipo String
	 * @param cveInst
	 *            Objeto del tipo String
	 * @param fch
	 *            Objeto del tipo String
	 * @return beanResAvisoTraspasosDAO Objeto del tipo BeanResAvisoTraspasosDAO
	 */
	@Override
	public BeanResAvisoTraspasosDAO obtenerAviso(BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType, String cveInst, String fch) {
		BeanAvisoTraspasos beanAvisoTraspasos = null;
		final BeanResAvisoTraspasosDAO beanResAvisoTraspasosDAO = new BeanResAvisoTraspasosDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		List<BeanAvisoTraspasos> listaAvisoTraspasos = null;

		try {
			if (fch == null || fch.length() <= 0) {
				String ordenamiento = "";

				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(" ORDER BY ")
							.append(sortField).append(" ").append(sortType)
							.toString();
				}

				responseDTO = HelperDAO.consultar(CONSULTA_AVISO.toString()
						.replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays
						.asList(new Object[] { beanPaginador.getRegIni(),
								beanPaginador.getRegFin() }),
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			} else {
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(beanPaginador.getRegIni());
				parametros.add(beanPaginador.getRegFin());
				String ordenamiento = "";

				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(" ORDER BY ")
							.append(sortField).append(" ").append(sortType)
							.toString();
				}

				responseDTO = HelperDAO.consultar(CONSULTA_AVISO_PAR.toString()
						.replaceAll("\\[ORDENAMIENTO\\]", ordenamiento),
						parametros, HelperDAO.CANAL_GFI_DS, this.getClass()
								.getName());
			}
			Integer cero = 0;

			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				if (!list.isEmpty()) {
					listaAvisoTraspasos = new ArrayList<BeanAvisoTraspasos>();

					for (Map<String, Object> mapResult : list) {
						beanAvisoTraspasos = new BeanAvisoTraspasos();
						beanAvisoTraspasos.setHora(utilerias.getString(
								mapResult.get("HORA_RECEPCION")).toString());
						beanAvisoTraspasos.setFolio(utilerias
								.getString(mapResult.get("FOLIO_SERVIDOR")));
						beanAvisoTraspasos.setTypTrasp(utilerias
								.getString(mapResult.get("DSC")));
						beanAvisoTraspasos.setImporte(utilerias
								.getString(mapResult.get("MONTO")));
						beanAvisoTraspasos.setSIAC(utilerias
								.getString(mapResult.get("REFERENCIA_SIAC")));

						listaAvisoTraspasos.add(beanAvisoTraspasos);

						if (cero.equals(beanResAvisoTraspasosDAO.getTotalReg())) {
							beanResAvisoTraspasosDAO.setTotalReg(utilerias
									.getInteger(mapResult.get("CONT")));
						}
					}
				}
			}
			beanResAvisoTraspasosDAO.setListaConAviso(listaAvisoTraspasos);
			beanResAvisoTraspasosDAO.setCodError(responseDTO.getCodeError());
			beanResAvisoTraspasosDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return beanResAvisoTraspasosDAO;
	}

	/**
	 * Metodo que sirve para exportar todos los avisos traspaso * @param
	 * beanReqAvisoTraspasos Objeto del tipo BeanReqAvisoTraspasos
	 * 
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param cveInst
	 *            Objeto del tipo String
	 * @param fch
	 *            Objeto del tipo String
	 * @return BeanResAvisoTraspasosDAO Objeto del tipo BeanResAvisoTraspasosDAO
	 */
	@Override
	public BeanResAvisoTraspasosDAO exportarTodoAvisoTraspasos(
			BeanReqAvisoTraspasos beanReqAvisoTraspasos,
			ArchitechSessionBean architechSessionBean, String cveInst,
			String fch) {
		StringBuilder builder = new StringBuilder();
		final BeanResAvisoTraspasosDAO beanResAvisoTraspasosDAO = new BeanResAvisoTraspasosDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		final Date fecha = new Date();
		ResponseMessageDataBaseDTO responseDTO = null;

		try {
			builder.append(TOTAL_CONSULTA_AVISO_TRAPASOS);
			final String fechaActual = utilerias.formateaFecha(fecha,
					Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
			final String nombreArchivo = "SPID_AVISO_TRASPASO_" + fechaActual
					+ ".csv";

			responseDTO = HelperDAO.insertar(
					INSERT_TODOS_AVISO_TRASPASO.toString(),
					Arrays.asList(new Object[] {
							architechSessionBean.getUsuario(),
							architechSessionBean.getIdSesion(), "SPID",
							"AVISO_TRASPASOS", nombreArchivo,
							beanReqAvisoTraspasos.getTotalReg(), "PE",
							builder.toString(), "HORA, FOLIO SERVIDOR, TIPO DE TRASPASO, IMPORTE, REFERENCIA SIAC" }), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());

			beanResAvisoTraspasosDAO.setNombreArchivo(nombreArchivo);
			beanResAvisoTraspasosDAO.setCodError(responseDTO.getCodeError());
			beanResAvisoTraspasosDAO.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResAvisoTraspasosDAO.setCodError(Errores.EC00011B);
		}

		return beanResAvisoTraspasosDAO;
	}

	/**
	 * 
	 * @param architechSessionBean
	 *            tipo architechSessionBean
	 * @param sortField
	 *            tipo String
	 * @param sortType
	 *            tipo String
	 * @param cveInst
	 *            tipo String
	 * @param fch
	 *            tipo String
	 * @return BigDecimal
	 */
	public BigDecimal obtenerImporteTotal(
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType, String cveInst, String fch) {
		ResponseMessageDataBaseDTO responseDTO = null;
		BigDecimal totalImporte = BigDecimal.ZERO;
		List<Object> parametros = new ArrayList<Object>();

		try {
			if (fch == null || fch.length() <= 0) {
				String ordenamiento = "";

				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(" ORDER BY ")
							.append(sortField).append(" ").append(sortType)
							.toString();
				}

				responseDTO = HelperDAO.consultar(CONSULTA_AVISO_SUM.toString()
						.replaceAll("\\[ORDENAMIENTO\\]", ordenamiento), Arrays
						.asList(new Object[] {}), HelperDAO.CANAL_GFI_DS, this
						.getClass().getName());
			} else {
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(cveInst);
				parametros.add(fch);
				String ordenamiento = "";

				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(" ORDER BY ")
							.append(sortField).append(" ").append(sortType)
							.toString();
				}

				responseDTO = HelperDAO.consultar(
						CONSULTA_AVISO_PAR_SUM.toString().replaceAll(
								"\\[ORDENAMIENTO\\]", ordenamiento),
						parametros, HelperDAO.CANAL_GFI_DS, this.getClass()
								.getName());
			}
			if (responseDTO.getResultQuery() != null) {
				totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0)
						.get("MONTO_TOTAL");
			}

		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return totalImporte;
	}
}
