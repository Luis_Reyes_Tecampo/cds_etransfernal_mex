/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOMonitorBancosImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMonitorBancos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMonitorBancosDAO;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga obtener la informacion para monitor bancos
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorBancosImpl extends Architech implements DAOMonitorBancos {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -5308818609518255461L;

	/**
	 * Constante privada del tipo String que almacena el query para la consulta
	 * de bancos
	 */
	private static final StringBuilder CONSULTA_MONITOR_BANCOS = new StringBuilder().append("SELECT T.* FROM (SELECT T.*, ROWNUM R FROM( SELECT ")
			.append(" CVE_BANCO,").append(" NVL(CVE_INTERME, 'NR') CVE_INTERME,").append(" NOMBRE_BANCO,")
			.append(" ESTADO_INSTIT,").append("ESTADO_RECEP, C.CONT")
			.append(" FROM TRAN_SPID_BANC ,(SELECT COUNT(*) CONT FROM TRAN_SPID_BANC ) C")
			.append(" [ORDENAMIENTO])T )T WHERE R BETWEEN ? AND ?");

	/**
	 * Metodo que sirve para obtener los datos de Monitor Bancos
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param sortField Objeto del tipo String
	 * @param sortType Objeto del tipo String
	 * @return beanMonitorBancosDAO Objeto del tipo BeanMonitorBancosDAO
	 */
	@Override
	public BeanMonitorBancosDAO obtenerMonitorBancos(BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		final BeanMonitorBancosDAO beanMonitorBancosDAO = new BeanMonitorBancosDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanMonitorBancos> listaBeanMonitorBancos = new ArrayList<BeanMonitorBancos>(); 

		try {
			String ordenamiento = "";
			
			if (sortField != null && !"".equals(sortField)){
				ordenamiento = new StringBuilder(" ORDER BY ").append(sortField).append(" ").append(sortType).toString();
			}
			
			responseDTO = HelperDAO.consultar(CONSULTA_MONITOR_BANCOS.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento),
					Arrays.asList(new Object[] { beanPaginador.getRegIni(), beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				if (!list.isEmpty()) {
					listaBeanMonitorBancos = llenarMonitorBancos(beanMonitorBancosDAO, list, utilerias);
				}
			}
			beanMonitorBancosDAO.setListaMonitorBancos(listaBeanMonitorBancos);
			beanMonitorBancosDAO.setCodError(responseDTO.getCodeError());
			beanMonitorBancosDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return beanMonitorBancosDAO;
	}

	/**
	 * @param beanMonitorBancosDAO Objeto del tipo BeanMonitorBancosDAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @return List<BeanMonitorBancos>
	 */
	private List<BeanMonitorBancos> llenarMonitorBancos(final BeanMonitorBancosDAO beanMonitorBancosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		BeanMonitorBancos beanMonitorBancos;
		List<BeanMonitorBancos> listaBeanMonitorBancos;
		listaBeanMonitorBancos = new ArrayList<BeanMonitorBancos>();
		for (Map<String, Object> listMap : list) {
			Map<String, Object> mapResult = listMap;

			beanMonitorBancos = new BeanMonitorBancos();
			beanMonitorBancos.setCveBanco(utilerias.getString(mapResult.get("CVE_BANCO")));
			beanMonitorBancos.setCveInterme(utilerias.getString(mapResult.get("CVE_INTERME")));
			beanMonitorBancos.setNombreBanco(utilerias.getString(mapResult.get("NOMBRE_BANCO")));
			beanMonitorBancos.setEstadoInstit(utilerias.getString(mapResult.get("ESTADO_INSTIT")));
			beanMonitorBancos.setEstadoRecep(utilerias.getString(mapResult.get("ESTADO_RECEP")));
			listaBeanMonitorBancos.add(beanMonitorBancos);
			beanMonitorBancosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
		}
		return listaBeanMonitorBancos;
	}
}
