/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOBitacoraAdmonImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 10:39:58 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsBitAdmon2;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsReqBitacoraAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmonDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResExpBitAdmonDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la bitacora administrativa
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitacoraAdmonImpl extends Architech implements DAOBitacoraAdmon {

   /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -7360191979316119272L;
  /**
   * Constante que Consulta la bitacora administrativa
   */
   private static final String QUERY_CONSULTA_BITACORA_ADMIN = 
	   " SELECT TMP.SERVICIO,TMP.FECHA_OP, TMP.HORA_OP, "+
	          " TMP.DIR_IP, TMP.CAN_APLI, TMP.USUARIO, TMP.DATO_AFEC, "+
	          " TMP.VAL_ANT, TMP.VAL_NVO, TMP.TIPO_OPER, TMP.NUM_REG,TMP.NOM_ARCHIVO, "+
	          " TMP.COD_ERROR,TMP.DESC_COD_ERROR, TMP.FOLIO_OPER, TMP.ID_TOKEN, " +
	          " TMP.ID_OPERACION, TMP.ESTATUS_OPER, TMP.COD_OPER, TMP.ID_SESION, " +
              " TMP.TABLA_AFEC, TMP.INST_WEB, TMP.HOST_WEB, TMP.DATO_FIJO, " +
              " TMP.CONT "+
	   " FROM ( "+
	           " SELECT TMP.*, ROWNUM AS RN "+
	           "        FROM ( "+
	                          " SELECT SERV_TRAN SERVICIO, FCH_ACT FECHA_OP, HORA_ACT HORA_OP, "+
	                                  " DIR_IP, CAN_APLI, USER_OPER USUARIO, DATO_MODI DATO_AFEC, "+
	                                  " VAL_ANT, VAL_NVO, TIPO_OPER, NUM_REG, NOM_ARCHIVO, "+ 
	                                  " COD_ERROR,DESC_COD_ERROR, FOLIO_OPER, ID_TOKEN, " +
	                                  " ID_OPERACION, ESTATUS_OPER, COD_OPER, ID_SESION, " +
	                                  " TABLA_AFEC, INST_WEB, HOST_WEB, DATO_FIJO, " +
	                                  " CONTADOR.CONT "+
	                          " FROM ( " +
	                              " SELECT  COUNT(1) CONT "+
	                              " FROM TRAN_SPEI_BIT_ADM ADM "+
	                              " WHERE ADM.FCH_ACT BETWEEN ? AND ? "+
	                                      " AND ADM.USER_OPER LIKE ? "+
	                                      " AND ADM.SERV_TRAN LIKE ? "+
	                         " ) CONTADOR , TRAN_SPEI_BIT_ADM ADM "+
	                        " WHERE ADM.FCH_ACT BETWEEN ? AND ? "+
	                              " AND ADM.USER_OPER LIKE ? "+
	                              " AND ADM.SERV_TRAN LIKE ? "+
	                              " ORDER BY FCH_ACT "+
	                " ) TMP "+
	           " ) TMP "+
	          " WHERE RN BETWEEN ? AND ? "+
	          " ORDER BY FECHA_OP, HORA_OP";

   /**
    * Constante que permite exportar todos
    */
   private static final String QUERY_EXPORT_TODOS_BITACORA_ADMIN = 
   " INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)" +
                             " VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )";

   /**
    * Constante que almacena la consulta a exportar
    */
   private static final String QUERY_CONSULTA_EXP_TODOS =
	   " SELECT SERV_TRAN || ',' || TO_CHAR(FCH_ACT,'dd-mm-yyyy') || ',' || TO_CHAR(HORA_ACT,'hh24:mi:ss')  || ',' || "+
	   " DIR_IP|| ',' || CAN_APLI|| ',' || USER_OPER|| ',' || DATO_MODI || ',' || "+
	   " VAL_ANT|| ',' ||VAL_NVO|| ',' || TIPO_OPER|| ',' || NUM_REG|| ',' || NOM_ARCHIVO|| ',' || "+
	   " COD_ERROR|| ',' || DESC_COD_ERROR || ',' || FOLIO_OPER || ',' || ID_TOKEN || ',' || " +
       " ID_OPERACION || ',' || ESTATUS_OPER || ',' || COD_OPER || ',' || ID_SESION || ',' || " +
       " TABLA_AFEC || ',' || INST_WEB ||','|| HOST_WEB || ',' || DATO_FIJO "+
	   " FROM TRAN_SPEI_BIT_ADM ADM ";
   
   /**
    * Constante que inserta en la bitacora administrativa
    */
   private static final String QUERY_INSERT_BIT_ADMON =
   " INSERT INTO TRAN_SPEI_BIT_ADM( "+
   " FOLIO_OPER,NUM_REG,ID_TOKEN,TIPO_OPER,USER_OPER, "+
   " ID_OPERACION,FCH_ACT,HORA_ACT,ESTATUS_OPER, "+
   " COD_OPER,CAN_APLI,ID_SESION,DATO_MODI, "+
   " TABLA_AFEC,COD_ERROR,DESC_COD_ERROR,INST_WEB, "+
   " HOST_WEB,DIR_IP,SERV_TRAN,NOM_ARCHIVO, "+
   " DATO_FIJO,VAL_ANT,VAL_NVO  "+
   " ) VALUES ( "+
   " SEQ_TRAN_SPEI_BIT_ADM_FOL_OPER.NEXTVAL,?,?,?,?, "+
   " ?,TO_DATE(?,'dd-MM-yyyy'),TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),?,"+
   " ?,?,?,?,"+
   " ?,?,?,?,"+
   " ?,?,?,?,"+
   " ?,?,?"+
   " ) ";
   
	   
  /**Metodo que permite consultar la bitacora administrativa
   * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsBitAdmonDAO Objeto del tipo BeanResConsBitAdmonDAO
   *    */
   public BeanResConsBitAdmonDAO consultaBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, 
		   ArchitechSessionBean architechSessionBean){
	   BeanConsBitAdmon beanConsBitAdmon = null;
	   BeanConsBitAdmon2 beanConsBitAdmon2 = null;
      final BeanResConsBitAdmonDAO beanResConsBitAdmonDAO = new BeanResConsBitAdmonDAO();
      Utilerias utilerias = Utilerias.getUtilerias();
      List<BeanConsBitAdmon> listBeanConsBitAdmon = Collections.emptyList();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      try{
         
         responseDTO = HelperDAO.consultar(QUERY_CONSULTA_BITACORA_ADMIN, Arrays.asList(
        		 new Object[]{
        				 utilerias.stringToDate(beanConsReqBitacoraAdmon.getFechaAccionIni(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY), 
        				 utilerias.stringToDate(beanConsReqBitacoraAdmon.getFechaAccionFin(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY),
        				 beanConsReqBitacoraAdmon.getUsuario().toUpperCase()+"%",
        				 beanConsReqBitacoraAdmon.getServicio()+"%",
        				 utilerias.stringToDate(beanConsReqBitacoraAdmon.getFechaAccionIni(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY), 
        				 utilerias.stringToDate(beanConsReqBitacoraAdmon.getFechaAccionFin(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY),
        				 beanConsReqBitacoraAdmon.getUsuario().toUpperCase()+"%",
        				 beanConsReqBitacoraAdmon.getServicio()+"%",
        				 beanConsReqBitacoraAdmon.getPaginador().getRegIni(),
        				 beanConsReqBitacoraAdmon.getPaginador().getRegFin()
        		 }
         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        	listBeanConsBitAdmon = new ArrayList<BeanConsBitAdmon>();
            list = responseDTO.getResultQuery();
            for(HashMap<String,Object> map:list){
            	beanConsBitAdmon = new BeanConsBitAdmon();
            	beanConsBitAdmon.setServicio(utilerias.getString(map.get("SERVICIO")));
            	beanConsBitAdmon.setFechaOpe(utilerias.formateaFecha(map.get("FECHA_OP"), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
            	beanConsBitAdmon.setHrOpe(utilerias.formateaFecha(map.get("HORA_OP"),Utilerias.FORMATO_HH_MM));
            	beanConsBitAdmon.setIp(utilerias.getString(map.get("DIR_IP")));
            	beanConsBitAdmon.setModuloCanalOp(utilerias.getString(map.get("CAN_APLI")));
            	beanConsBitAdmon.setUsuario(utilerias.getString(map.get("USUARIO")));
            	beanConsBitAdmon.setDatoAfectado(utilerias.getString(map.get("DATO_AFEC")));
            	beanConsBitAdmon.setValAnterior(utilerias.getString(map.get("VAL_ANT")));
            	beanConsBitAdmon.setValNuevo(utilerias.getString(map.get("VAL_NVO")));
            	beanConsBitAdmon.setDescOperacion(utilerias.getString(map.get("TIPO_OPER")));
            	beanConsBitAdmon.setNumeroRegistros(utilerias.formateaDecimales(map.get("NUM_REG"), Utilerias.FORMATO_NUMBER));
            	beanConsBitAdmon.setNomArchivo(utilerias.getString(map.get("NOM_ARCHIVO")));
            	beanConsBitAdmon.setCodError(utilerias.getString(map.get("COD_ERROR")));
            	beanConsBitAdmon.setDescCodError(utilerias.getString(map.get("DESC_COD_ERROR")));
            	beanConsBitAdmon2 = new BeanConsBitAdmon2();
            	beanConsBitAdmon2.setFolioOper(utilerias.getString(map.get("FOLIO_OPER")));
            	beanConsBitAdmon2.setIdToken(utilerias.getString(map.get("ID_TOKEN")));
            	beanConsBitAdmon2.setIdOperacion(utilerias.getString(map.get("ID_OPERACION")));
            	beanConsBitAdmon2.setEstatusOperacion(utilerias.getString(map.get("ESTATUS_OPER")));
            	beanConsBitAdmon2.setCodOperacion(utilerias.getString(map.get("COD_OPER")));
            	beanConsBitAdmon2.setIdSesion(utilerias.getString(map.get("ID_SESION")));
            	beanConsBitAdmon2.setTablaAfectada(utilerias.getString(map.get("TABLA_AFEC")));
            	beanConsBitAdmon2.setInstanciaWeb(utilerias.getString(map.get("INST_WEB")));
            	beanConsBitAdmon2.setHostWeb(utilerias.getString(map.get("HOST_WEB")));
            	beanConsBitAdmon2.setDatoFijo(utilerias.getString(map.get("DATO_FIJO")));
            	beanConsBitAdmon.setBeanConsBitAdmon2(beanConsBitAdmon2);
            	listBeanConsBitAdmon.add(beanConsBitAdmon);    	
            	beanResConsBitAdmonDAO.setTotalReg(utilerias.getInteger(map.get("CONT")));
            }
         }
    	 beanResConsBitAdmonDAO.setListBeanConsBitAdmon(listBeanConsBitAdmon);
         beanResConsBitAdmonDAO.setCodError(responseDTO.getCodeError());
         beanResConsBitAdmonDAO.setMsgError(responseDTO.getMessageError());
      
     
         
      
      } catch (ExceptionDataAccess e) {
         showException(e);
         beanResConsBitAdmonDAO.setCodError(Errores.EC00011B);
      }
      return beanResConsBitAdmonDAO;
   }
   
   /**Metodo que sirve para realizar el exportar todo
    * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResExpBitAdmonDAO Objeto del tipo BeanResExpBitAdmonDAO
    *    */
    public BeanResExpBitAdmonDAO expTodosBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, 
    		ArchitechSessionBean architechSessionBean){
    	StringBuilder builder = new StringBuilder();
    	final BeanResExpBitAdmonDAO beanResExpBitAdmon = new BeanResExpBitAdmonDAO();
    	Utilerias utilerias = Utilerias.getUtilerias();
        final Date fecha = new Date();
       ResponseMessageDataBaseDTO responseDTO = null;
       try{
    	   builder.append(QUERY_CONSULTA_EXP_TODOS);
    	   builder.append(" WHERE ADM.FCH_ACT BETWEEN ");
    	   builder.append(" TO_DATE('");
    	   builder.append(beanConsReqBitacoraAdmon.getParamFecha3Antes());
    	   builder.append("','");
    	   builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
    	   builder.append("')");
    	   builder.append(" AND ");
    	   
    	   builder.append(" TO_DATE('");
    	   builder.append(beanConsReqBitacoraAdmon.getParamFechaFin());
    	   builder.append("','");
    	   builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
    	   builder.append("') ");
    	   if(beanConsReqBitacoraAdmon.getParamUsuario()!=null &&
    			   !"".equals(beanConsReqBitacoraAdmon.getParamUsuario().trim())){
	    	   builder.append(" AND ADM.USER_OPER LIKE '");
	    	   builder.append(beanConsReqBitacoraAdmon.getParamUsuario().trim().toUpperCase());
	    	   builder.append("%'");
    	   }
    	   if(beanConsReqBitacoraAdmon.getParamServicio() !=null && 
    			   !"".equals(beanConsReqBitacoraAdmon.getParamServicio())){
	    	   builder.append(" AND ADM.SERV_TRAN LIKE '");
	    	   builder.append(beanConsReqBitacoraAdmon.getParamServicio().trim());
	    	   builder.append("%'");
    	   }   
    	   
           
           final String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
           final String nombreArchivo = "CDA_CON_BIT_"+fechaActual+".csv";
           beanResExpBitAdmon.setNomArchivo(nombreArchivo);
    	   
           responseDTO = HelperDAO.insertar(QUERY_EXPORT_TODOS_BITACORA_ADMIN, 
        		  Arrays.asList(new Object[]{architechSessionBean.getUsuario(),
        				                     architechSessionBean.getIdSesion(),
        				                     "CDA",
        				                     "BITACORA_ADMIN",
        				                     nombreArchivo,
        				                     beanConsReqBitacoraAdmon.getTotalReg(),
        				                     "PE",
        				                     builder.toString(),
        				                     "SERVICIO,FECHA DE OP,HR DE OPE,IP,MODULO/CANAL/OPERACION," +
        				                     "USUARIO,DATO AFECT,VALOR ANTERIOR, VALOR NUEVO, " +
        				                     "DESC. OP., NUM_REG, NOM ARCHIVO, COD. ERROR, DESC. COD ERROR," +
        				                     "FOLIO OPERACION, ID TOKEM, ID OPERACION, ESTATUS OPERACION," +
        				                     "CODIGO DE OPERACION, ID SESION, TABLA AFECTADA, INSTANCIA WEB," +
        				                     "HOST WEB, DATO FIJO"
        		  }), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           
           beanResExpBitAdmon.setCodError(responseDTO.getCodeError());
           beanResExpBitAdmon.setMsgError(responseDTO.getMessageError());

       } catch (ExceptionDataAccess e) {
          showException(e);
          beanResExpBitAdmon.setCodError(Errores.EC00011B); 
       }
       return beanResExpBitAdmon;
    }
    
    /**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitacora Objeto del tipo @see BeanReqInsertBitacora
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitacora beanReqInsertBitacora, ArchitechSessionBean architechSessionBean){
        final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
        Utilerias utilerias = Utilerias.getUtilerias();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.insertar(QUERY_INSERT_BIT_ADMON, 
        		   Arrays.asList(new Object[]{
        				   beanReqInsertBitacora.getNumeroRegistros(),
        				   "",
        				   beanReqInsertBitacora.getDescOperacion(),
        				   architechSessionBean.getUsuario(),
        				   
        				   "",
        				   
        				   utilerias.formateaFecha(beanReqInsertBitacora.getFechaHora(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY),
        				   utilerias.formateaFecha(beanReqInsertBitacora.getFechaHora(),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS),
        				   beanReqInsertBitacora.getEstatusOperacion(),
        				   
        				   beanReqInsertBitacora.getCodOperacion(),
        				   architechSessionBean.getNombreAplicacion(),
        				   architechSessionBean.getIdSesion(),
        				   "",
        				   
        				   beanReqInsertBitacora.getTablaAfectada(),
        				   beanReqInsertBitacora.getCodError(),
        				   beanReqInsertBitacora.getDescCodError(),
        				   architechSessionBean.getNombreServidor(),
        				   
        				   architechSessionBean.getIPServidor(),
        				   architechSessionBean.getIPCliente(),
        				   beanReqInsertBitacora.getServicio(),
        				   beanReqInsertBitacora.getNomArchivo(),
        				   
        				   
        				   beanReqInsertBitacora.getDatoFijo(),
        				   beanReqInsertBitacora.getValAnt(),
        				   beanReqInsertBitacora.getValNuevo()
        				   }), 
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaBitacoraDAO;
     }



}
