/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0    Dic 29 11:30:00 CST 2016 	Alan Garcia Villagran   Vector 	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico de SPID
**/
//Clase del tipo DAO que se encarga  obtener la informacion para
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGenerarArchCDAHistSPIDImpl extends Architech implements DAOGenerarArchCDAHistSPID {

	/**
	 * Variable de serializacion
	 */
	private static final long serialVersionUID = -8965605139781324588L;
	
	/**
	 * Constante que almacena el query de la fecha de operacion SPID
	 */
	//QUERY_FECHA_OPERACION
	private static final String QUERY_FECHA_OPERACION = 
	" SELECT FCH_OPERACION FROM TRAN_SPID_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPID'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";

	/**
	 * Constante del tipo String que almacena la consulta que calcula el id_peticion
	 */
	//QUERY_CONSULTA_ID_PETICION
	private static final String QUERY_CONSULTA_ID_PETICION = 
			" SELECT (NVL(MAX(TO_NUMBER(ID_PETICION)),0)+1) ID_PETICION FROM TRAN_SPID_CTG_CDA";

	/**
	 * Constante que almacena el query de insert en la tabla TRAN_SPEI_CTG_CDA
	 */
	//QUERY_INSERT_TRAN_SPID_CTG_CDA
	private static final String QUERY_INSERT_TRAN_SPID_CTG_CDA = 
			" INSERT INTO TRAN_SPID_CTG_CDA (FCH_OPERACION,ID_PETICION, TIPO_PETICION, USUARIO, SESION, MODULO, NOMBRE_ARCHIVO, FCH_HORA, ESTATUS) "+
				    " VALUES (sysdate,?, ?, ?, ?, ?, ?, TO_DATE(?,'dd-MM-yyyy'), ?)";

	/**Metodo DAO para obtener la fecha de operacion
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
	   */
	//Metodo DAO para obtener la fecha de operacion
	public BeanResConsFechaOpDAO consultaFechaOperacionSPID(ArchitechSessionBean architechSessionBean) {
		//Se crean objetos de respuesta
		final BeanResConsFechaOpDAO beanResConsFechaOpSPIDDAO = new BeanResConsFechaOpDAO();
		
		//Se crean objetos de apoyo
	    List<HashMap<String,Object>> list = null;
	    ResponseMessageDataBaseDTO responseDTO = null;
	    Utilerias utilerias = Utilerias.getUtilerias();
	    try {
	    	//Se hace la consulta a la base de datos
	    	responseDTO = HelperDAO.consultar(QUERY_FECHA_OPERACION, Collections.emptyList(), 
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	//Se valida que la respuesta no sea nula ni vacia
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				for(HashMap<String,Object> map:list){
					//Se setea la fecha de operacion, cod y msg de error
					beanResConsFechaOpSPIDDAO.setFechaOperacion(
							utilerias.formateaFecha(map.get("FCH_OPERACION"), 
									Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
					beanResConsFechaOpSPIDDAO.setCodError(responseDTO.getCodeError());
					beanResConsFechaOpSPIDDAO.setMsgError(responseDTO.getMessageError());
				}
			}
	    } catch (ExceptionDataAccess e) {
	    	//Se controla la excepcion y se setea el cod de error EC00011B
	    	showException(e);
	        beanResConsFechaOpSPIDDAO.setCodError(Errores.EC00011B);
	        beanResConsFechaOpSPIDDAO.setMsgError(Errores.DESC_EC00011B);
		}
	    return beanResConsFechaOpSPIDDAO;
	}
   
	/**Metodo DAO para registrar el archivo a generar del CDA historico
	    * @param beanReqGenArchCDASPIDHist Objeto del tipo @see BeanReqGenArchCDAHist
	    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDAO
	    */
	//Metodo DAO para registrar el archivo a generar del CDA historico
	public BeanResGenArchCDAHistDAO generarArchHistCDASPID(BeanReqGenArchCDAHist beanReqGenArchCDASPIDHist,
			ArchitechSessionBean architechSessionBean) {
		//Se crean objetos de respuesta
		final BeanResGenArchCDAHistDAO beanResGenArchCDAHistSPIDDAO = new BeanResGenArchCDAHistDAO();
		
		//Se crean objetos de apoyo
        List<HashMap<String,Object>> list = null;
        ResponseMessageDataBaseDTO responseDTO = null;
        Integer idPeticion = -1;
        Utilerias utilerias = Utilerias.getUtilerias();
        try{
        	//Se hace la consulta a la base de datos
     	   responseDTO = HelperDAO.consultar(QUERY_CONSULTA_ID_PETICION, 
     			   Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
     	   list = responseDTO.getResultQuery();
     	//Se valida que la respuesta no sea nula ni vacia
       	   if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
       	      for(HashMap<String,Object> map:list){
       			idPeticion = utilerias.getInteger(map.get("ID_PETICION"));
       		  }
       	      //Se hace el insert a la base de datos
              responseDTO = HelperDAO.insertar(QUERY_INSERT_TRAN_SPID_CTG_CDA, Arrays.asList(
            		  new Object[]{idPeticion,"D",architechSessionBean.getUsuario(),
            				  architechSessionBean.getIdSesion(),
         					  "ARCDASPIDH", beanReqGenArchCDASPIDHist.getNombreArchivo(),
         					  beanReqGenArchCDASPIDHist.getFechaOperacion(), "PE"}
            		  ),
 	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
              //Se setea la peticion, msg y cod de error
              beanResGenArchCDAHistSPIDDAO.setIdPeticion(idPeticion.toString());
              beanResGenArchCDAHistSPIDDAO.setCodError(responseDTO.getCodeError());
              beanResGenArchCDAHistSPIDDAO.setMsgError(responseDTO.getMessageError());
       	   }
        } catch (ExceptionDataAccess e) {
        	//Se controla la excepcion y se setea el cod de error EC00011B
           showException(e);
           beanResGenArchCDAHistSPIDDAO.setCodError(Errores.EC00011B);
           beanResGenArchCDAHistSPIDDAO.setMsgError(Errores.DESC_EC00011B);
        }
        return beanResGenArchCDAHistSPIDDAO;
	}
}
