/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGraficoSPEIImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    31/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.pagoInteres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanEstatus;
import mx.isban.eTransferNal.beans.pagoInteres.BeanMedioEnt;
import mx.isban.eTransferNal.beans.pagoInteres.BeanRangoTiempo;
import mx.isban.eTransferNal.beans.pagoInteres.BeanRangoTiempoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResEstatusDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResMedioEntDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePagoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResTipoPagoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanTipoPago;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORepPagoInteresCompSPEIImpl extends Architech implements DAORepPagoInteresCompSPEI{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5996463108543796769L;

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_ESTATUS
	 */
	private static final String QUERY_ESTATUS = "SELECT CVE_PARAM_FALTA CVE_ESTATUS, DESCRIPCION FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE 'ESTATUS_PAGINT%'";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_TIPO_PAGO
	 */
	private static final String QUERY_TIPO_PAGO = "SELECT CVE_PARAM_FALTA TIPO_PAGO, DESCRIPCION FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE 'TIPO_TRANSFEPI%'";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_MEDIO_ENT
	 */
	private static final String QUERY_MEDIO_ENT = "SELECT CVE_MEDIO_ENT, DESCRIPCION FROM COMU_MEDIOS_ENT ORDER BY DESCRIPCION DESC";
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_RANGO
	 */
	private static final String QUERY_RANGO = "SELECT CVE_PARAM_FALTA,VALOR_FALTA FROM TRAN_PARAMETROS WHERE  CVE_PARAMETRO LIKE 'R_P_SPEI_INT%' ORDER BY 1";
	
	
	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos los envios
	 * exportados
	 */
	private static final StringBuilder EXPORT_TODOS = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_REP_PAGO_CAMPOS = " SELECT T.REFERENCIA REF_PAGO, T.REFERENCIA_INTERES REF_INTERES, T.CVE_RASTREO, T.TIPO,M.CVE_MEDIO_ENT,"+ " T.MONTO, T.MONTO_INTERES, "+
	" DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_APROBACION,'dd/mm/yyyy HH24mi'),  "+
	"                     'RE', TO_CHAR(T.FCH_RECEPCION,'dd/mm/yyyy HH24mi') , "+
	"                     'DE', TO_CHAR(T.FCH_APROBACION,'dd/mm/yyyy HH24mi'), "+
	"                     'DR', TO_CHAR(T.FCH_RECEPCION,'dd/mm/yyyy HH24mi')) FCH_INICIO, "+
	" DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi'), "+
	"                     'RE', TO_CHAR(T.FCH_CAPTURA,'dd/mm/yyyy HH24mi'), "+
	" 	                  'DE', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi'), "+
	"                     'DR', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi')) FCH_FIN, "+
	" DECODE(T.TIPO, 'EN', T.DIF_ACUSE_APROBACION, "+
	"                     'RE', T.DIF_CAPTURA_RECEPCION, "+
	"                     'DE', T.DIF_ACUSE_APROBACION, "+
	"                     'DR', T.DIF_RECEPCION_ACUSE) DIFERENCIA, "+
	" T.ESTATUS "; 
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY_ENV
	 */
	private static final String QUERY_REP_PAGO_BODY_ENV = QUERY_REP_PAGO_CAMPOS+
	" FROM TRAN_SPEI_INTERES T, TRAN_MENSAJE M"+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY_ENV_HIS
	 */
	private static final String QUERY_REP_PAGO_BODY_ENV_HIS = QUERY_REP_PAGO_CAMPOS+
	" FROM TRAN_SPEI_INTERES_HIS T, TRAN_MENSAJE_HIS M "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA "+
	" AND M.FCH_CAPTURA > T.FCH_OPERACION ";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_REP_PAGO_BODY = QUERY_REP_PAGO_CAMPOS+
	" FROM TRAN_SPEI_INTERES T, TRAN_MENSAJE M,TRAN_SPEI_REC R "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA AND "+
	" T.referencia = R.refe_transfer AND "+ 
	" T.fch_operacion = R.fch_operacion ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_REP_PAGO_BODY_HIS = QUERY_REP_PAGO_CAMPOS+
	" FROM TRAN_SPEI_INTERES_HIS T, TRAN_MENSAJE_HIS M, TRAN_SPEI_REC_HIS R "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA AND "+
	" T.referencia = R.refe_transfer AND "+
	" T.fch_operacion = R.fch_operacion "+
	" AND M.FCH_CAPTURA > T.FCH_OPERACION ";    
	
	/**
	 * Propiedad del tipo String que almacena los campos del query
	 */
	private static final String QUERY_EXP_CAMPOS = " SELECT T.REFERENCIA||','|| T.REFERENCIA_INTERES||','|| T.CVE_RASTREO||','|| T.TIPO||','||M.CVE_MEDIO_ENT||','||"+
	" T.MONTO||','||T.MONTO_INTERES||','|| "+  
	" DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_APROBACION,'dd/mm/yyyy HH24mi'),  "+
	"                     'RE', TO_CHAR(T.FCH_RECEPCION,'dd/mm/yyyy HH24mi'), "+
	"                     'DE', TO_CHAR(T.FCH_APROBACION,'dd/mm/yyyy HH24mi'), "+
	"                     'DR', TO_CHAR(T.FCH_RECEPCION,'dd/mm/yyyy HH24mi')) ||','|| "+
	" DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi'), "+
	"                     'RE', TO_CHAR(T.FCH_CAPTURA,'dd/mm/yyyy HH24mi'), "+
	" 	                  'DE', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi'), "+
	"                     'DR', TO_CHAR(T.FCH_ACUSE,'dd/mm/yyyy HH24mi')) ||','|| "+
	" DECODE(T.TIPO, 'EN', T.DIF_ACUSE_APROBACION, "+
	"                     'RE', T.DIF_CAPTURA_RECEPCION, "+
	"                     'DE', T.DIF_ACUSE_APROBACION, "+
	"                     'DR', T.DIF_RECEPCION_ACUSE) ||','|| "+
	" T.ESTATUS ";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_EXP_BODY_ENV = QUERY_EXP_CAMPOS+
	" FROM TRAN_SPEI_INTERES T, TRAN_MENSAJE M  "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_EXP_BODY_HIS_ENV= QUERY_EXP_CAMPOS+
	" FROM TRAN_SPEI_INTERES_HIS T, TRAN_MENSAJE_HIS M "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA "+
	" AND M.FCH_CAPTURA > T.FCH_OPERACION ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_EXP_BODY = QUERY_EXP_CAMPOS+
	" FROM TRAN_SPEI_INTERES T, TRAN_MENSAJE M,TRAN_SPEI_REC R "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA AND "+
	" T.referencia = R.refe_transfer AND "+
	" T.fch_operacion = R.fch_operacion ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_BODY
	 */
	private static final String QUERY_EXP_BODY_HIS= QUERY_EXP_CAMPOS+
	" FROM TRAN_SPEI_INTERES_HIS T, TRAN_MENSAJE_HIS M, TRAN_SPEI_REC_HIS R "+
	" WHERE "+
	" T.REFERENCIA = M.REFERENCIA AND "+
	" T.referencia = R.refe_transfer AND "+
	" T.fch_operacion = R.fch_operacion "+
	" AND M.FCH_CAPTURA > T.FCH_OPERACION ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_HEADER
	 */
	private static final String QUERY_REP_PAGO_HEADER = " SELECT * FROM (SELECT M.*, ROWNUM RNUM, COUNT (*) OVER () CONT FROM (";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_REP_PAGO_FOOTER
	 */
	private static final String QUERY_REP_PAGO_FOOTER = " ) M )  WHERE RNUM BETWEEN ? AND ?  ";
	
	
	@Override
	public BeanResEstatusDAO consultaEstatus(ArchitechSessionBean session) {
		BeanResEstatusDAO beanResEstatusDAO =  new BeanResEstatusDAO();
		BeanEstatus beanEstatus = null;
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanEstatus> estatusList = Collections.emptyList();
    	ResponseMessageDataBaseDTO responseDTO = null;
        try {
        	List<Object> parametros = Collections.emptyList();
        	responseDTO = HelperDAO.consultar(QUERY_ESTATUS, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			estatusList = new ArrayList<BeanEstatus>();
	        		for(HashMap<String,Object> map:list){
	        			beanEstatus = new BeanEstatus();
	        			beanEstatus.setCveEstatus(utilerias.getString(map.get("CVE_ESTATUS")));
	        			beanEstatus.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
	        			estatusList.add(beanEstatus);
	        		}
	        		 beanResEstatusDAO.setEstatusList(estatusList);
	             	 beanResEstatusDAO.setCodError(responseDTO.getCodeError());
	             	 beanResEstatusDAO.setMsgError(responseDTO.getMessageError());
        		 }else{
        		 	 beanResEstatusDAO.setCodError(Errores.CODE_SUCCESFULLY);
            	 	 beanResEstatusDAO.setMsgError(Errores.OK00000V);
            	 	 beanResEstatusDAO.setEstatusList(estatusList);
	            }
        	}
        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResEstatusDAO.setCodError(Errores.EC00011B);
        }
  		return beanResEstatusDAO;
	}

	@Override
	public BeanResMedioEntDAO consultaMedioEnt(ArchitechSessionBean session) {
		BeanResMedioEntDAO beanResMedioEntDAO =  new BeanResMedioEntDAO();
		BeanMedioEnt beanMedioEnt = null;
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanMedioEnt> medioEntList = Collections.emptyList();
    	ResponseMessageDataBaseDTO responseDTO = null;
        try {
        	List<Object> parametros = Collections.emptyList();
        	responseDTO = HelperDAO.consultar(QUERY_MEDIO_ENT, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 medioEntList = new ArrayList<BeanMedioEnt>();
	        		for(HashMap<String,Object> map:list){
	        			beanMedioEnt = new BeanMedioEnt();
	        			beanMedioEnt.setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")));
	        			beanMedioEnt.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
	        			medioEntList.add(beanMedioEnt);
	        		}
	        		beanResMedioEntDAO.setMedioEntList(medioEntList);
	        		beanResMedioEntDAO.setCodError(responseDTO.getCodeError());
	        		beanResMedioEntDAO.setMsgError(responseDTO.getMessageError());
        		 }else{
        			 beanResMedioEntDAO.setCodError(Errores.CODE_SUCCESFULLY);
        			 beanResMedioEntDAO.setMsgError(Errores.OK00000V);
        			 beanResMedioEntDAO.setMedioEntList(medioEntList);
	            }
        	}
        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResMedioEntDAO.setCodError(Errores.EC00011B);
        }
  		return beanResMedioEntDAO;
	}
	
	@Override
	public BeanResTipoPagoDAO consultaTipoPago(ArchitechSessionBean session) {
		BeanResTipoPagoDAO beanResTipoPagoDAO =  new BeanResTipoPagoDAO();
		BeanTipoPago beanTipoPago = null;
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanTipoPago> beanTipoPagoList = Collections.emptyList();
    	ResponseMessageDataBaseDTO responseDTO = null;
        try {
        	List<Object> parametros = Collections.emptyList();
        	responseDTO = HelperDAO.consultar(QUERY_TIPO_PAGO, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 beanTipoPagoList = new ArrayList<BeanTipoPago>();
	        		for(HashMap<String,Object> map:list){
	        			beanTipoPago = new BeanTipoPago();
	        			beanTipoPago.setCveTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
	        			beanTipoPago.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
	        			beanTipoPagoList.add(beanTipoPago);
	        		}
	        		beanResTipoPagoDAO.setTipoPagoList(beanTipoPagoList);
	        		beanResTipoPagoDAO.setCodError(responseDTO.getCodeError());
	        		beanResTipoPagoDAO.setMsgError(responseDTO.getMessageError());
        		 }else{
        			 beanResTipoPagoDAO.setCodError(Errores.CODE_SUCCESFULLY);
        			 beanResTipoPagoDAO.setMsgError(Errores.OK00000V);
        			 beanResTipoPagoDAO.setTipoPagoList(beanTipoPagoList);
	            }
        	}
        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResTipoPagoDAO.setCodError(Errores.EC00011B);
        }
  		return beanResTipoPagoDAO;
	}
	
	@Override
	public BeanRangoTiempoDAO consultaRango(ArchitechSessionBean session) {
		BeanRangoTiempoDAO beanRangoTiempoDAO =  new BeanRangoTiempoDAO();
		BeanRangoTiempo beanRangoTiempo = null;
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanRangoTiempo> listRangoTiempo = Collections.emptyList();
    	ResponseMessageDataBaseDTO responseDTO = null;
        try {
        	List<Object> parametros = Collections.emptyList();
        	responseDTO = HelperDAO.consultar(QUERY_RANGO, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 listRangoTiempo = new ArrayList<BeanRangoTiempo>();
	        		for(HashMap<String,Object> map:list){
	        			beanRangoTiempo = new BeanRangoTiempo();
	        			beanRangoTiempo.setEpoca(utilerias.getString(map.get("CVE_PARAM_FALTA")));
	        			beanRangoTiempo.setRango(Integer.valueOf(utilerias.getString(map.get("VALOR_FALTA"))));
	        			listRangoTiempo.add(beanRangoTiempo);
	        		}
	        		beanRangoTiempoDAO.setListRangoTiempo(listRangoTiempo);
	        		beanRangoTiempoDAO.setCodError(responseDTO.getCodeError());
	        		beanRangoTiempoDAO.setMsgError(responseDTO.getMessageError());
        		 }else{
        			 beanRangoTiempoDAO.setCodError(Errores.CODE_SUCCESFULLY);
        			 beanRangoTiempoDAO.setMsgError(Errores.OK00000V);
        			 beanRangoTiempoDAO.setListRangoTiempo(listRangoTiempo);
	            }
        	}
        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanRangoTiempoDAO.setCodError(Errores.EC00011B);
        }
  		return beanRangoTiempoDAO;
	}

	/**
	 * Metodo privado que sirve para armar el query
	 * @param req Bean con los parametros
	 * @param parametros List<Object> Lista donde se almacenan los valores de los parametros
	 * @return String con el query
	 */
	private String armaQuery(BeanReqReportePago req,List<Object> parametros){
		Utilerias utilerias = Utilerias.getUtilerias();
		StringBuilder strBuider = new StringBuilder();
		strBuider.append(QUERY_REP_PAGO_HEADER);
		if("A".equals(req.getOpcion())){
			if("EN".equals(req.getTipoPago())){
				strBuider.append(QUERY_REP_PAGO_BODY_ENV);
			}else{
				strBuider.append(QUERY_REP_PAGO_BODY);
			}
		}else{
			if("EN".equals(req.getTipoPago())){
				strBuider.append(QUERY_REP_PAGO_BODY_ENV_HIS);
			}else{
				strBuider.append(QUERY_REP_PAGO_BODY_HIS);
			}
		}
		strBuider.append(armaTipoPago(req));
		if(req.getEstatus()!=null && !"".equals(req.getEstatus())){
			strBuider.append(" AND T.ESTATUS = ? ");
			parametros.add(req.getEstatus());
		}
		if(req.getMedioEnt()!=null && !"".equals(req.getMedioEnt())){
			strBuider.append(" AND TRIM(M.CVE_MEDIO_ENT) = ? ");
			parametros.add(req.getMedioEnt());
		}
		
		if(req.getDifMinutos()!=null && !"".equals(req.getDifMinutos())){
			int seg =0;
			strBuider.append(" AND DECODE(T.TIPO, 'EN', T.DIF_ACUSE_APROBACION, 'RE', T.DIF_CAPTURA_RECEPCION, 'DE', T.DIF_ACUSE_APROBACION, 'DR', T.DIF_RECEPCION_ACUSE,-1) ");
			strBuider.append(req.getSigDifMinutos());
			strBuider.append(" ?");
			seg = Integer.valueOf(req.getDifMinutos())*60;
			parametros.add(seg);
		}
		if("A".equals(req.getOpcion())){
			StringBuilder strBuilder = new StringBuilder();
			StringBuilder strBuilder2 = new StringBuilder();
			strBuilder.append(req.getHorario().getHoraInicio()).append(req.getHorario().getMinutoInicio());
			strBuilder2.append(req.getHorario().getHoraFin()).append(req.getHorario().getMinutoFin());
			
			strBuider.append(" AND DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_APROBACION,'HH24mi'), 'RE', TO_CHAR(T.FCH_RECEPCION,'HH24mi'), 'DE', TO_CHAR(T.FCH_APROBACION,'HH24mi'), 'DR', TO_CHAR(T.FCH_RECEPCION,'HH24mi')) ").
			append(" BETWEEN ? AND ? ");
			parametros.add(strBuilder.toString());
			parametros.add(strBuilder2.toString());
		}else{
			Date date = utilerias.cadenaToFecha(req.getHorario().getFechaInicio(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			date = cal.getTime();
			String fecha = utilerias.formateaFecha(date, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			strBuider.append(" AND M.FCH_CAPTURA <= to_date(?,'dd-mm-yyyy')");
			parametros.add(fecha);
			if(req.getHorario().getFechaInicio()!=null && !"".equals(req.getHorario().getFechaInicio()) ){
				strBuider.append(" AND T.FCH_OPERACION = to_date(?,'dd-mm-yyyy') ");
				parametros.add(req.getHorario().getFechaInicio());
			}
		}
		if(req.getImporteInicio()!=null && !"".equals(req.getImporteInicio())){
			strBuider.append(" AND T.MONTO_INTERES BETWEEN ? AND ? ");
			parametros.add(req.getImporteInicio());
			parametros.add(req.getImporteFin());
		}
		strBuider.append(QUERY_REP_PAGO_FOOTER);
		parametros.add(req.getPaginador().getRegIni());
    	parametros.add(req.getPaginador().getRegFin());
    	return strBuider.toString();
	}
	
	@Override
	public BeanResReportePagoDAO consultaReportePago(BeanReqReportePago req,
			ArchitechSessionBean session) {
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanResReportePagoDAO beanResReportePagoDAO = new BeanResReportePagoDAO();
		BeanReportePago reportePago = null;
		List<HashMap<String,Object>> list = null;
		List<BeanReportePago> beanReportePagoList = Collections.emptyList();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros = new ArrayList<Object>();
		String query = armaQuery(req,parametros);
    	try {
        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 
        			 beanReportePagoList = new ArrayList<BeanReportePago>();
	        		for(HashMap<String,Object> map:list){
	        			reportePago = new BeanReportePago();
	        			reportePago.setRefPago(utilerias.getString(map.get("REF_PAGO")));
	        			reportePago.setRefInteres(utilerias.getString(map.get("REF_INTERES")));
	        			reportePago.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
	        			reportePago.setTipoPago(utilerias.getString(map.get("TIPO")));
	        			reportePago.setMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")));
	        			reportePago.setImporteTransf(utilerias.getString(map.get("MONTO")));
	        			reportePago.setImporteInteres(utilerias.getString(map.get("MONTO_INTERES")));
	        			reportePago.setFechaInicio(utilerias.getString(map.get("FCH_INICIO")));
	        			reportePago.setFechaFin(utilerias.getString(map.get("FCH_FIN")));
	        			reportePago.setDiferencia(utilerias.getString(map.get("DIFERENCIA")));
	        			reportePago.setEstatus(utilerias.getString(map.get("ESTATUS")));
	        			beanResReportePagoDAO.setTotalReg(utilerias.getString(map.get("CONT")));
	        			beanReportePagoList.add(reportePago);
	        			
	        		}
	        		beanResReportePagoDAO.setReportePagoList(beanReportePagoList);
	        		beanResReportePagoDAO.setCodError(Errores.OK00000V);
	        		beanResReportePagoDAO.setMsgError(responseDTO.getMessageError());
    		 }else{
    			 beanResReportePagoDAO.setCodError(responseDTO.getCodeError());
        		beanResReportePagoDAO.setMsgError(responseDTO.getMessageError());
    			 beanResReportePagoDAO.setReportePagoList(beanReportePagoList);
            }
        	
        } catch (ExceptionDataAccess e) {
        	showException(e);
        	beanResReportePagoDAO.setCodError(Errores.EC00011B);
        }
  		return beanResReportePagoDAO;
	}
	/**
	 * Metodo privado que sirve para armar el query
	 * @param req Bean con los parametros
	 * @return String con el query
	 */
	private String armaQueryExpTodo(BeanReqReportePago req){
		Utilerias util = Utilerias.getUtilerias();
		StringBuilder strBuider = new StringBuilder();
		if("A".equals(req.getOpcion())){
			if("EN".equals(req.getTipoPago())){
				strBuider.append(QUERY_EXP_BODY_ENV);
			}else{
				strBuider.append(QUERY_EXP_BODY);
			}
		}else{
			if("EN".equals(req.getTipoPago())){
				strBuider.append(QUERY_EXP_BODY_HIS_ENV);
			}else{
				strBuider.append(QUERY_EXP_BODY_HIS);
			}
		}
		
		strBuider.append(armaTipoPago(req));
		
		if(req.getEstatus()!=null && !"".equals(req.getEstatus())){
			strBuider.append(" AND T.ESTATUS = '");
			strBuider.append(req.getEstatus());
			strBuider.append("'");
		}
		if(req.getMedioEnt()!=null && !"".equals(req.getMedioEnt())){
			strBuider.append(" AND TRIM(M.CVE_MEDIO_ENT) =  '");
			strBuider.append(req.getMedioEnt());
			strBuider.append("'");
		}
		
		if(req.getDifMinutos()!=null && !"".equals(req.getDifMinutos())){
			int seg =0;
			strBuider.append(" AND DECODE(T.TIPO, 'EN', T.DIF_ACUSE_APROBACION, 'RE', T.DIF_CAPTURA_RECEPCION, 'DE', T.DIF_ACUSE_APROBACION, 'DR', T.DIF_RECEPCION_ACUSE,-1) ");
			strBuider.append(req.getSigDifMinutos());
			seg = Integer.valueOf(req.getDifMinutos())*60;
			strBuider.append(seg);
		}
		if("A".equals(req.getOpcion())){
			StringBuilder strBuilder = new StringBuilder();
			StringBuilder strBuilder2 = new StringBuilder();
			strBuilder.append(req.getHorario().getHoraInicio()).append(req.getHorario().getMinutoInicio());
			strBuilder2.append(req.getHorario().getHoraFin()).append(req.getHorario().getMinutoFin());
			strBuider.append(" AND DECODE(T.TIPO, 'EN', TO_CHAR(T.FCH_APROBACION,'HH24mi'), 'RE', TO_CHAR(T.FCH_RECEPCION,'HH24mi'), 'DE', TO_CHAR(T.FCH_APROBACION,'HH24mi'), 'DR', TO_CHAR(T.FCH_RECEPCION,'HH24mi')) ").
			append(" BETWEEN ");
			strBuider.append(strBuilder.toString());
			strBuider.append(" AND ");
			strBuider.append(strBuilder2.toString());
			
		}else{
			Date date = util.cadenaToFecha(req.getHorario().getFechaInicio(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			date = cal.getTime();
			String fecha = util.formateaFecha(date, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			strBuider.append(" AND M.FCH_CAPTURA <= to_date('");
			strBuider.append(fecha);
			strBuider.append("','dd-mm-yyyy')");
			if(req.getHorario().getFechaInicio()!=null && !"".equals(req.getHorario().getFechaInicio())){
				strBuider.append(" AND T.FCH_OPERACION = to_date('").append(req.getHorario().getFechaInicio()).append("','dd-mm-yyyy')");
			}
		}
		if(req.getImporteInicio()!=null && !"".equals(req.getImporteInicio())){
			strBuider.append(" AND T.MONTO_INTERES BETWEEN ");
			strBuider.append(req.getImporteInicio());
			strBuider.append(" AND ");
			strBuider.append(req.getImporteFin());
		}
		return strBuider.toString();
	}

	@Override
	public BeanResReportePagoDAO expTodoReportePago(BeanReqReportePago req,
			ArchitechSessionBean session) {
		Utilerias utilerias =Utilerias.getUtilerias(); 
		BeanResReportePagoDAO beanResReportePagoDAO = new BeanResReportePagoDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		String query = armaQueryExpTodo(req);
		Calendar cal = Calendar.getInstance();
		final String fechaActual = utilerias.formateaFecha(cal.getTime(),Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        final String nombreArchivo = "PINTSPEI"+fechaActual+".csv";
		try {
			responseDTO = HelperDAO.insertar(
					EXPORT_TODOS.toString(),
					Arrays.asList(new Object[] {
							session.getUsuario(),
							session.getIdSesion(), "PINTSPEI",
							"PAGO_INTERES", nombreArchivo,
							req.getTotalReg(), "PE",
							query, 
							"REF_PAGO, REFERENCIA, REF_INTERES, CVE_RASTREO, TIPO_PAGO, CVE_MEDIO_ENT, MONTO, MONTO_INTERES, DIFERENCIA, ESTATUS " }), 
							HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			beanResReportePagoDAO.setCodError(Errores.OK00000V);
			beanResReportePagoDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResReportePagoDAO.setCodError(Errores.EC00011B);
		}
		return beanResReportePagoDAO;
	}
	
	/**
	 * Metodo que arma la parte de tipo de pago
	 * @param req Bean con los parametros
	 * @return String con el segmento de query
	 */
	private String armaTipoPago(BeanReqReportePago req){
		StringBuilder str = new StringBuilder();
		if("EN".equals(req.getTipoPago())){
			str.append(" AND T.tipo='EN'");
			str.append(" AND T.tipo_pago=1 ");
		}else if("DEM".equals(req.getTipoPago())){			
			str.append(" AND T.tipo='DE' ");
			str.append(" AND T.tipo_pago=0 ");
			str.append(" AND R.tipo_cuenta_ord=10 ");
			str.append(" AND M.cve_transfe = '074' ");
		}else if("DE".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DE' ");
			str.append(" AND T.tipo_pago=0 "); 
			str.append(" AND R.tipo_cuenta_ord<>10"); 
			str.append(" AND M.cve_transfe = '074' ");
		}else if("RE".equals(req.getTipoPago())){
			str.append(" AND T.tipo='RE' ");
			str.append(" AND T.tipo_pago=1 "); 
			str.append(" AND R.tipo_cuenta_ord<>10"); 
			str.append(" AND M.cve_transfe = '044' ");	
		}else if("RM".equals(req.getTipoPago())){			
			str.append(" AND T.tipo='RE' ");
			str.append(" AND T.tipo_pago=1 "); 
			str.append(" AND R.tipo_cuenta_ord = 10"); 
			str.append(" AND M.cve_transfe = '044' ");
		}else if("DR".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DR' ");
			str.append(" AND T.tipo_pago=0 "); 
			str.append(" AND NVL(R.tipo_cuenta_ord,1) <> 10 "); 
			str.append(" AND M.cve_transfe = '084' ");
		}else {
			armaTipoPago(req,str);
		}
		return str.toString();
	}
	
	/**
	 * Metodo que arma la parte de tipo de pago
	 * @param req Bean con los parametros
	 * @str Objeto StringBuilder para almacenar el query
	 */
	private void armaTipoPago(BeanReqReportePago req,StringBuilder str){
		if("DRM".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DR' ");
			str.append(" AND T.tipo_pago=0 "); 
			str.append(" AND NVL(R.tipo_cuenta_ord,1) = 10"); 
			str.append(" AND M.cve_transfe = '084' ");
		}else if("DRX".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DX' ");
			str.append(" AND T.tipo_pago=16 "); 
			str.append(" AND NVL(R.tipo_cuenta_ord,1) <> 10"); 
			str.append(" AND M.cve_transfe = '084' ");
		}else if("DRXM".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DX' ");
			str.append(" AND T.tipo_pago=16 "); 
			str.append(" AND NVL(R.tipo_cuenta_ord,1) = 10"); 
			str.append(" AND M.cve_transfe = '084' ");
		}else if("DEX".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DX' ");
			str.append(" AND T.tipo_pago=16 "); 
			str.append(" AND R.tipo_cuenta_ord <>10"); 
			str.append(" AND M.cve_transfe = '074' ");
		}else if("DEXM".equals(req.getTipoPago())){
			str.append(" AND T.tipo='DX' ");
			str.append(" AND T.tipo_pago=16 "); 
			str.append(" AND R.tipo_cuenta_ord =10"); 
			str.append(" AND M.cve_transfe = '074' ");
		}
	}


}
