/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOAvisosImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/09/2019 02:09:48 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasAvisos;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class DAOAvisosImpl.
 *
 * Clase tipo DAO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * de accceso a los datos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOAvisosImpl extends Architech implements DAOAvisos{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2764031743644116963L;

	/** La constante CONST_Q1_SELECT. */
	private static final String CONST_Q1_SELECT = "SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO";
	
	/** La constante CONST_Q2_DECODE. */
	private static final String CONST_Q2_DECODE = "||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,";
	
	/** La constante CONST_Q3_MONTO. */
	private static final String CONST_Q3_MONTO = "T.MONTO,T.REFERENCIA_SIAC, C.CONT ";
	
	/** La constante CONSULTA_AVISO. */
	private static final StringBuilder CONSULTA_AVISO = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append(CONST_Q1_SELECT)
			.append(CONST_Q2_DECODE)
			.append(CONST_Q3_MONTO)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");
	
	/** La constante CONSULTA_AVISO_PAR. */
	public static final StringBuilder CONSULTA_AVISO_PAR = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append(CONST_Q1_SELECT)
			.append(CONST_Q2_DECODE)
			.append(CONST_Q3_MONTO)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");
	
	/** La constante CONSULTA_AVISO_SUM. */
	public static final StringBuilder CONSULTA_AVISO_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) AS MONTO_TOTAL FROM (")
			.append(CONST_Q1_SELECT)
			.append(CONST_Q2_DECODE)
			.append(CONST_Q3_MONTO)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ");
	
	/** La constante CONSULTA_AVISO_PAR_SUM. */
	public static final StringBuilder CONSULTA_AVISO_PAR_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) MONTO_TOTAL FROM (")
			.append(CONST_Q1_SELECT)
			.append(CONST_Q2_DECODE)
			.append(CONST_Q3_MONTO)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ");
	
	/** La constante ORDENAMIENTO. */
	private static final String ORDENAMIENTO = "\\[ORDENAMIENTO\\]";
	
	/** La constante ORDER. */
	private static final String ORDER = " ORDER BY ";
	
	/** La constante utils. */
	private static final UtileriasAvisos utils = UtileriasAvisos.getUtils();
	
	/**
	 * Obtener aviso.
	 *
	 * Consultar la lista de los avisos disponibles.
	 * 
	 * @param beanPaginador El objeto: bean paginador
	 * @param modulo El objeto: modulo
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto bean res aviso traspasos DAO
	 */
	@Override
	public BeanResAvisoTraspasosDAO obtenerAviso(BeanPaginador beanPaginador, BeanModulo modulo, String sortField,
			String sortType, String cveInst, String fch, ArchitechSessionBean architechSessionBean) {
		//Se delcaran las variable response BeanResAvisoTraspasosDAO
		final BeanResAvisoTraspasosDAO response = new BeanResAvisoTraspasosDAO();
		//Se declara ResponseMessageDataBaseDTO
		ResponseMessageDataBaseDTO responseDTO = null;
		
		//Se declara objetos parametros
		List<Object> parametros = new ArrayList<Object>();
		try {
			//se validan los parametros
			if (fch == null || fch.length() <= 0) {
				String ordenamiento = "";
				//Validando vacio sortField
				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(ORDER)
							.append(sortField).append(" ").append(sortType)
							.toString();
				}
				//se manda llamar el metodo cambia query
				final String queryOperacion = ValidadorMonitor.cambiaQuery(modulo, CONSULTA_AVISO.toString()
						.replaceAll(ORDENAMIENTO, ordenamiento));
				// se declara objeto para params
				final List<Object> params = new ArrayList<Object>();
				params.add(beanPaginador.getRegIni());
				params.add(beanPaginador.getRegFin());
				// se ejecuta la consulta
				responseDTO = HelperDAO.consultar(queryOperacion, params,
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			} else {
				//se agregan los parametros
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(beanPaginador.getRegIni());
				parametros.add(beanPaginador.getRegFin());
				String ordenamiento = "";
				//validando vacio
				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(ORDER)
							.append(sortField).append(" ").append(sortType)
							.toString();
				}
				//Query cinsulta aviso
				final String queryOperacion = ValidadorMonitor.cambiaQuery(modulo, CONSULTA_AVISO_PAR.toString()
						.replaceAll(ORDENAMIENTO, ordenamiento)); 
				//se obtiene el resultado de la consulta
				responseDTO = HelperDAO.consultar(queryOperacion, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			}
			
			//seteando  al objeto avisos
			seteaAvisos(responseDTO, response);
		} catch (ExceptionDataAccess e) {
			//ha ocurrido un error
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
		}
		//return response
		return response;
	}

	/**
	 * Setea avisos.
	 *
	 * @param responseDTO El objeto: response DTO
	 * @param response El objeto: response
	 */
	private void seteaAvisos(ResponseMessageDataBaseDTO responseDTO,
			BeanResAvisoTraspasosDAO response) {
		//Se declaran los objetos list
		List<HashMap<String, Object>> list = null;
		//se declara el objeto Utilerias
		Utilerias utilerias = Utilerias.getUtilerias();
		//Se declara BeanAvisoTraspasos
		BeanAvisoTraspasos beanAvisoTraspasos = null;
		//se declara objeto List<BeanAvisoTraspasos> 
		List<BeanAvisoTraspasos> listaAvisoTraspasos = null;
		//validando el response
		if (responseDTO.getResultQuery() != null
				&& !responseDTO.getResultQuery().isEmpty()) {
			list = responseDTO.getResultQuery();
			//se valida lista vacia
			if (!list.isEmpty()) {
				listaAvisoTraspasos = new ArrayList<BeanAvisoTraspasos>();
				//Iteracion de la lista
				for (Map<String, Object> mapResult : list) {
					beanAvisoTraspasos = new BeanAvisoTraspasos();
					beanAvisoTraspasos.setHora(utilerias.getString(mapResult.get("HORA_RECEPCION")));
					beanAvisoTraspasos.setFolio(utilerias.getString(mapResult.get("FOLIO_SERVIDOR")));
					beanAvisoTraspasos.setTypTrasp(utilerias.getString(mapResult.get("DSC")));
					beanAvisoTraspasos.setImporte(utilerias.getString(mapResult.get("MONTO")));
					beanAvisoTraspasos.setSIAC(utilerias.getString(mapResult.get("REFERENCIA_SIAC")));
					listaAvisoTraspasos.add(beanAvisoTraspasos);
					response = utils.validaCantidad(response, mapResult);
				}
				//setando bean al response
				response.setListaConAviso(listaAvisoTraspasos);
				//setando errores al response
				response.setCodError(responseDTO.getCodeError());
				response.setMsgError(responseDTO.getMessageError());
			}
		}
		//seteando lista al response
		response.setListaConAviso(listaAvisoTraspasos);
		//Seteando codigos de error
		response.setCodError(responseDTO.getCodeError());
		response.setMsgError(responseDTO.getMessageError());
	}
	
	/**
	 * Obtener importe total.
	 *
	 * Obetner el importe total para mostrar.
	 * 
	 * @param session El objeto: session
	 * @param modulo El objeto: modulo
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @return Objeto big decimal
	 */
	@Override
	public BigDecimal obtenerImporteTotal(ArchitechSessionBean session, BeanModulo modulo, String sortField, String sortType,
			String cveInst, String fch) {
		//creacion de objetos ResponseMessageDataBaseDTO
		ResponseMessageDataBaseDTO responseDTO = null;
		//creacion de objeto totalImporte
		BigDecimal totalImporte = BigDecimal.ZERO;
		//creacion de objeto List<Object>
		List<Object> parametros = new ArrayList<Object>();

		try {
			//Validando parametros
			if (fch == null || fch.length() <= 0) {
				String ordenamiento = "";
				// validando vacios
				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(ORDER)
							.append(sortField).append(" ").append(sortType)
							.toString();
				}
				//se obtiene query
				final String queryOperacion = ValidadorMonitor.cambiaQuery(modulo, CONSULTA_AVISO_SUM.toString()
						.replaceAll(ORDENAMIENTO, ordenamiento));
				//Objeto response obtiene resultado
				responseDTO = HelperDAO.consultar(queryOperacion, Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this
						.getClass().getName());
			} else {
				//setando parametros
				parametros.add(cveInst);
				parametros.add(fch);
				parametros.add(cveInst);
				parametros.add(fch);
				String ordenamiento = "";
				
				//validando vacio
				if (!"".equals(sortField)) {
					ordenamiento = new StringBuilder(ORDER)
							.append(sortField).append(" ").append(sortType)
							.toString();
				}
				//se obtiene query
				final String queryOperacion = ValidadorMonitor.cambiaQuery(modulo, CONSULTA_AVISO_PAR_SUM.toString().replaceAll(
						ORDENAMIENTO, ordenamiento));
				responseDTO = HelperDAO.consultar(queryOperacion,
						parametros, HelperDAO.CANAL_GFI_DS, this.getClass()
								.getName());
			}
			//validando resultado
			if (responseDTO.getResultQuery() != null) {
				totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0)
						.get("MONTO_TOTAL");
			}

		} catch (ExceptionDataAccess e) {
			//sucedio un error
			error("Ocurrio un error al obtener el importe");
			showException(e);
		}
		//return total
		return totalImporte;
	}

}
