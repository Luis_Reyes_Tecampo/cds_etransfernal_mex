package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOConsultaMovHistCDASPIDImpl2.
 */
public class DAOConsultaMovHistCDASPIDImpl2 extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 4940645079863618552L;

	/** Constante TIPO_PAGO. */
	private static final String TIPO_PAGO = "TIPO_PAGO";
	
	/** Constante TIPO_PAGO. */
	private static final String FCH_HORA_ABONO ="FCH_HORA_ABONO";

    /**
     * Metodo que genera un objeto del tipo BeanMovimientoCDA a partir de la respuesta de la consulta.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanMovimientoCDA Objeto del tipo BeanMovimientoCDA
     */
    private BeanMovimientoCDASPID descomponeConsulta(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanMovimientoCDASPID beanMovimientoCDASPID = null;
    	beanMovimientoCDASPID = new BeanMovimientoCDASPID();
    	beanMovimientoCDASPID.setReferencia(utilerias.getString(map.get("REFERENCIA")));
    	beanMovimientoCDASPID.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
    	beanMovimientoCDASPID.setFolioPaq(utilerias.getString(map.get("FOLIO_PAQUETE")));
    	beanMovimientoCDASPID.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
    	beanMovimientoCDASPID.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));
    	beanMovimientoCDASPID.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
    	beanMovimientoCDASPID.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
    	beanMovimientoCDASPID.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
    	beanMovimientoCDASPID.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));
    	beanMovimientoCDASPID.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));
		beanMovimientoCDASPID.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanMovimientoCDASPID.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDASPID.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));
		beanMovimientoCDASPID.setEstatusCDA(utilerias.getString(map.get("CDA")));
		beanMovimientoCDASPID.setCodError(utilerias.getString(map.get("COD_ERROR")));
		beanMovimientoCDASPID.setFchCDA(utilerias.getString(map.get("FCH_CDA")));
		beanMovimientoCDASPID.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));
		beanMovimientoCDASPID.setFolioPaqCda(utilerias.getString(map.get("FOLIO_PAQCDA")));
		beanMovimientoCDASPID.setFolioCda(utilerias.getString(map.get("FOLIO_CDA")));
		beanMovimientoCDASPID.setModalidad(utilerias.getString(map.get("MODALIDAD")));
		beanMovimientoCDASPID.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
		beanMovimientoCDASPID.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
		beanMovimientoCDASPID.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
		beanMovimientoCDASPID.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
		beanMovimientoCDASPID.setNombreBcoRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));
		beanMovimientoCDASPID.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
		beanMovimientoCDASPID.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
		beanMovimientoCDASPID.setRfcRec(utilerias.getString(map.get("RFC_REC")));
		beanMovimientoCDASPID.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
		beanMovimientoCDASPID.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
		beanMovimientoCDASPID.setNumSerieCertif(utilerias.getString(map.get("NUM_SERIE_CERTIF")));
		beanMovimientoCDASPID.setSelloDigital(utilerias.getString(map.get("SELLO_DIGITAL")));
		beanMovimientoCDASPID.setAlgoritmoFirma(utilerias.getString(map.get("ALGORITMO_FIRMA")));
		beanMovimientoCDASPID.setPadding(utilerias.getString(map.get("PADDING")));

		return beanMovimientoCDASPID;
    }

    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    String armaConsultaParametrizadaCDASPID(List<Object> parametros, BeanReqConsMovHistCDA beanReqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

    	// si horaAbonoInicio tiene valor valido
    	if(beanReqConsMovHistCDASPID.getHrAbonoIni() != null && beanReqConsMovHistCDASPID.getHrAbonoFin() != null &&
    			(!vacio.equals(beanReqConsMovHistCDASPID.getHrAbonoIni())) && (!vacio.equals(beanReqConsMovHistCDASPID.getHrAbonoFin())) ){
			  builder.append(" AND T1.HORA_ABONO ");
			  builder.append("between ? and ? ");
			  parametros.add(beanReqConsMovHistCDASPID.getHrAbonoIni().replaceAll(":", ""));
			  parametros.add(beanReqConsMovHistCDASPID.getHrAbonoFin().replaceAll(":", ""));
    	}

    	// si horaEnvioInicio tiene valor valido
    	if(beanReqConsMovHistCDASPID.getHrEnvioIni() != null && beanReqConsMovHistCDASPID.getHrEnvioFin() != null &&
	  			(!vacio.equals(beanReqConsMovHistCDASPID.getHrEnvioIni())) && (!vacio.equals(beanReqConsMovHistCDASPID.getHrEnvioFin()))){
	  		builder.append(" AND T1.HORA_ENVIO " +
	  				" between ? and ? ");
	  		parametros.add(beanReqConsMovHistCDASPID.getHrEnvioIni().replaceAll(":", ""));
	  		parametros.add(beanReqConsMovHistCDASPID.getHrEnvioFin().replaceAll(":", ""));
	  	}
	  	
    	//se llama segunda parte del metodo armaConsultaParametrizadaCDASPID
	  	builder.append(armaConsultaParametrizadaCDASPID2(parametros, beanReqConsMovHistCDASPID));
	  	
    	return builder.toString();
    }

    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    private String armaConsultaParametrizadaCDASPID2(List<Object> parametros, BeanReqConsMovHistCDA beanReqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String estatusCDA = "C";
    	String vacio = "";

    	// si montoPago tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getMontoPago() != null  && (!vacio.equals(beanReqConsMovHistCDASPID.getMontoPago()))){
	  		 builder.append(" AND T1.MONTO = ? ");
	  		 String montoPago = beanReqConsMovHistCDASPID.getMontoPago().replaceAll(",", "");
	  		 parametros.add(montoPago);
	  	}
    	// si operacionContingencia es verdadero
    	if(beanReqConsMovHistCDASPID.getOpeContingencia()){
        	builder.append("AND T1.MODALIDAD = ? ");
        	parametros.add(estatusCDA);
    	}
    	// si claveSpeiOrdenanteAbono tiene valor valido
    	if(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ? ") ;
    		parametros.add(beanReqConsMovHistCDASPID.getCveSpeiOrdenanteAbono());
    	}

    	//se llama tercera parte del metodo armaConsultaParametrizadaCDASPID
	  	builder.append(armaConsultaParametrizadaCDASPID3(parametros, beanReqConsMovHistCDASPID));
	  	
    	return builder.toString();
    }
    
    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    private String armaConsultaParametrizadaCDASPID3(List<Object> parametros, BeanReqConsMovHistCDA beanReqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

    	// si cuentaBeneficiario tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getCtaBeneficiario() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCtaBeneficiario()))){
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM(?)");
	  		 parametros.add("%"+beanReqConsMovHistCDASPID.getCtaBeneficiario());
	  	}
    	// si nombreInstitucionEmisora tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getNombreInstEmisora() != null &&(!vacio.equals(beanReqConsMovHistCDASPID.getNombreInstEmisora()))){
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER(?)");
	  		parametros.add("%"+beanReqConsMovHistCDASPID.getNombreInstEmisora());
	  	}
    	// si claveRastreo tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getCveRastreo() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCveRastreo()))){
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM(?)");
	  		parametros.add("%"+beanReqConsMovHistCDASPID.getCveRastreo());
	  	}
    	// si tipoPago tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getTipoPago() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getTipoPago()))){
	  		builder.append(" AND T1.TIPO_PAGO = ?");
	  		parametros.add(beanReqConsMovHistCDASPID.getTipoPago());
	  	}
	  	
    	return builder.toString();
    }
    
    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo.
     *
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @return String objeto del tipo @see String
     */
    String generarConsultaExportarTodoSPID2(BeanReqConsMovHistCDA beanReqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

    	// si horaEnvioInicio tiene valor valido
    	if(beanReqConsMovHistCDASPID.getHrEnvioIni() != null && beanReqConsMovHistCDASPID.getHrEnvioFin() != null &&
	  			(!vacio.equals(beanReqConsMovHistCDASPID.getHrEnvioIni())) && (!vacio.equals(beanReqConsMovHistCDASPID.getHrEnvioFin()))){
        	// se concatena horaEnvioInicio y horaEnvioFin
	  		builder.append(" AND T1.HORA_ENVIO between ");
	          builder.append(beanReqConsMovHistCDASPID.getHrEnvioIni().replaceAll(":", ""));
	          builder.append(" and ");
	          builder.append(beanReqConsMovHistCDASPID.getHrEnvioFin().replaceAll(":", ""));

	  	}
    	// si montoPago tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getMontoPago() != null  && (!vacio.equals(beanReqConsMovHistCDASPID.getMontoPago()))){
        	// se concatena montoPago
	  		 builder.append(" AND T1.MONTO = ");
	  		 String montoPago = beanReqConsMovHistCDASPID.getMontoPago().replaceAll(",", "");
	  	     builder.append(montoPago);
	  	}
    	// si cuentaBeneficiario tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getCtaBeneficiario() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCtaBeneficiario()))){
        	// se concatena cuentaBeneficiario
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM('%");
	  	     builder.append(beanReqConsMovHistCDASPID.getCtaBeneficiario());
	  	     builder.append("')");
	  	}

    	// se llama tercera parte de metodo generarConsultaExportarTodoSPID
	  	builder.append(generarConsultaExportarTodoSPID3(beanReqConsMovHistCDASPID));

	  	// se retorna cadena generada al metodo generarConsultaExportarTodoSPID
    	return builder.toString();
    }
    
    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo.
     *
     * @param beanReqConsMovHistCDASPID the bean req cons mov hist cdaspid
     * @return String objeto del tipo @see String
     */
    private String generarConsultaExportarTodoSPID3(BeanReqConsMovHistCDA beanReqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

    	// si nombreInstitucionEmisora tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getNombreInstEmisora() != null &&(!vacio.equals(beanReqConsMovHistCDASPID.getNombreInstEmisora()))){
        	// se concatena nombreInstitucionEmisora
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER('%");
	          builder.append(beanReqConsMovHistCDASPID.getNombreInstEmisora());
	          builder.append("')");
	  	}

    	// si claveRastreo tiene valor valido
	  	if(beanReqConsMovHistCDASPID.getCveRastreo() != null && (!vacio.equals(beanReqConsMovHistCDASPID.getCveRastreo()))){
        	// se concatena claveRastreo
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM('%");
	      	builder.append(beanReqConsMovHistCDASPID.getCveRastreo());
	      	builder.append("')");
	  	}
	  	
	  	// se retorna cadena generada al metodo generarConsultaExportarTodoSPID2
    	return builder.toString();
    }
    
    /**
     * Setear lista mov cdaspid.
     *
     * @param responseDTO the response dto
     * @param resMovHistCDASPIDDAO the res mov hist cdaspiddao
     */
    void setearListaMovCDASPID(ResponseMessageDataBaseDTO responseDTO,
    		BeanResConsMovHistCdaDAOSPID resMovHistCDASPIDDAO){
    	List<BeanMovimientoCDASPID> listBeanMovimientoCDASPID = new ArrayList<BeanMovimientoCDASPID>();
    	List<HashMap<String,Object>> list = responseDTO.getResultQuery();
    	
 		for(HashMap<String,Object> map:list){
 			//se llama funcion para generar el arrayList
 			listBeanMovimientoCDASPID.add(descomponeConsulta(map));
 		}
 		//se setea el arrayList de movimientos
 		resMovHistCDASPIDDAO.setListBeanMovimientoCDA(listBeanMovimientoCDASPID);
    }
}
