/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCargaArchHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:31:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMonCargaArchHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga obtener la informacion para la
 * funcionalidad del Monitor de carga de archivos historico
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorCargaArchHistImpl extends Architech implements
		DAOMonitorCargaArchHist {

	/**
	 * Constante del version serial
	 */
	private static final long serialVersionUID = 1913849133132655956L;

	/**
	 * Constante del tipo String que almacena la consulta pasa obtener los
	 * registros de archivos de contingencia diario
	 */
	private static final String QUERY_CONSULTA_CARGA_ARCH_HIST = " SELECT TMP.ID_PETICION, TMP.NOMBRE_ARCHIVO,TMP.ESTATUS,TMP.CONT "
			+ " FROM ( "
			+ "     SELECT TMP.*, ROWNUM AS RN "
			+ "            FROM ( "
			+ "            SELECT ID_PETICION, NOMBRE_ARCHIVO,"
			+ "            CASE(ESTATUS) WHEN 'PE' THEN 'PENDIENTE' "
			+ "              WHEN 'EP' THEN 'EN PROCESO' "
			+ "              WHEN 'OK' THEN 'FINALIZADO' "
			+ "              WHEN 'KO' THEN 'CON ERROR' END ESTATUS, "
			+ "            CASE(ESTATUS) WHEN 'PE' THEN 3"
			+ "              WHEN 'EP' THEN 1 "
			+ "              WHEN 'OK' THEN 2 "
			+ "              WHEN 'KO' THEN 4 END ORDEN, "			
			+ "              CONTADOR.CONT "
			+ "            FROM (SELECT COUNT(1) CONT FROM TRAN_SPEI_CTG_CDA WHERE TIPO_PETICION='D') CONTADOR, TRAN_SPEI_CTG_CDA "
			+ "            WHERE TIPO_PETICION='D' "
			+ "            ORDER BY ORDEN ASC, NOMBRE_ARCHIVO  "
			+ "            )TMP "
			+ "      ) TMP "
			+ "     WHERE RN BETWEEN ? AND ? "
			+ " ORDER BY ORDEN ASC, NOMBRE_ARCHIVO ";

	/**
	 * Metodo DAO para obtener la informacion del monitor de carga de archivos
	 * historicos
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonCargaArchHistDAO Objeto del tipo
	 *         BeanResMonCargaArchHistDAO
	 * */
	public BeanResMonCargaArchHistDAO consultarMonitorCargaArchHist(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) {
		final BeanResMonCargaArchHistDAO beanResMonCargaArchHistDAO = new BeanResMonCargaArchHistDAO();
		BeanMonCargaArchHist beanMonCargaArchHist = null;
		List<HashMap<String, Object>> list = null;
		List<BeanMonCargaArchHist> listBeanMonCargaArchHist = Collections
				.emptyList();
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CARGA_ARCH_HIST,
					Arrays.asList(new Object[] { beanPaginador.getRegIni(),
							beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listBeanMonCargaArchHist = new ArrayList<BeanMonCargaArchHist>();
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					beanMonCargaArchHist = new BeanMonCargaArchHist();
					beanMonCargaArchHist.setIdPeticion(utilerias.getString(map
							.get("ID_PETICION")));
					beanMonCargaArchHist.setNomArchivo(utilerias.getString(map
							.get("NOMBRE_ARCHIVO")));
					beanMonCargaArchHist.setEstatusCargaArch(utilerias
							.getString(map.get("ESTATUS")));
					listBeanMonCargaArchHist.add(beanMonCargaArchHist);
					beanResMonCargaArchHistDAO.setTotalReg(utilerias
							.getInteger(map.get("CONT")));
				}
			}
			beanResMonCargaArchHistDAO
					.setListBeanMonCargaArchHist(listBeanMonCargaArchHist);
			beanResMonCargaArchHistDAO.setCodError(responseDTO.getCodeError());
			beanResMonCargaArchHistDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResMonCargaArchHistDAO.setCodError(Errores.EC00011B);

		}
		return beanResMonCargaArchHistDAO;
	}

}
