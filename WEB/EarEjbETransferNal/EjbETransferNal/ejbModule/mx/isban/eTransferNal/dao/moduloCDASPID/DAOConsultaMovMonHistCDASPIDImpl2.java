package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonHistCDASPID;

/**
 * The Class DAOConsultaMovMonHistCDASPIDImpl2.
 */
public class DAOConsultaMovMonHistCDASPIDImpl2 extends Architech {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 8699843634542591593L;

    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    String armaConsultaParametrizada(List<Object> parametros, BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

	  	//si horaAbonoInicio tiene un valor valido
    	if(reqConsMovHistCDASPID.getHrAbonoIni() != null && reqConsMovHistCDASPID.getHrAbonoFin() != null &&
    			(!vacio.equals(reqConsMovHistCDASPID.getHrAbonoIni())) && (!vacio.equals(reqConsMovHistCDASPID.getHrAbonoFin())) ){
    		//se concatena horaAbono
			  builder.append(" AND T1.HORA_ABONO ");
			  builder.append("between ? and ? ");
			  parametros.add(reqConsMovHistCDASPID.getHrAbonoIni().replaceAll(":", ""));
			  parametros.add(reqConsMovHistCDASPID.getHrAbonoFin().replaceAll(":", ""));
    	}

	  	//si horaEnvioInicio tiene un valor valido
    	if(reqConsMovHistCDASPID.getHrEnvioIni() != null && reqConsMovHistCDASPID.getHrEnvioFin() != null &&
	  			(!vacio.equals(reqConsMovHistCDASPID.getHrEnvioIni())) && (!vacio.equals(reqConsMovHistCDASPID.getHrEnvioFin()))){
    		//se concatena horaEnvio
	  		builder.append(" AND T1.HORA_ENVIO " +
	  				" between ? and ? ");
	  		parametros.add(reqConsMovHistCDASPID.getHrEnvioIni().replaceAll(":", ""));
	  		parametros.add(reqConsMovHistCDASPID.getHrEnvioFin().replaceAll(":", ""));
	  	}
	  	
	  	builder.append( armaConsultaParametrizada2(parametros,reqConsMovHistCDASPID) );
	  	
    	return builder.toString();
    }
    
    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    private String armaConsultaParametrizada2(List<Object> parametros, BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	String estatusCDA = "C";

	  	//si montoPago tiene un valor valido
    	if(reqConsMovHistCDASPID.getMontoPago() != null  && (!vacio.equals(reqConsMovHistCDASPID.getMontoPago()))){
    		//se concatena monto
	  		builder.append(" AND T1.MONTO = ? ");
	  		String montoPago = reqConsMovHistCDASPID.getMontoPago().replaceAll(",", "");
	  		parametros.add(montoPago);
	  	}
	  	//si operacionContingencia tiene un valor valido
    	if(reqConsMovHistCDASPID.getOpeContingencia()){
    		//se concatena modalidad
        	builder.append("AND T1.MODALIDAD = ? ");
        	parametros.add(estatusCDA);
    	}
	  	//si claveSpeiOrdenanteAbono tiene un valor valido
    	if(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono()))){
    		//se concatena claveInstitucionOrdenante
    		builder.append(" AND T1.CVE_INST_ORD = ? ") ;
    		parametros.add(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono());
    	}

	  	//si cuentaBeneficiario tiene un valor valido
	  	if(reqConsMovHistCDASPID.getCtaBeneficiario() != null && (!vacio.equals(reqConsMovHistCDASPID.getCtaBeneficiario()))){
    		//se concatena numeroCuentaRec
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM(?)");
	  		 parametros.add("%"+reqConsMovHistCDASPID.getCtaBeneficiario());
	  	}
	  	//si nombreInstitucionEmisora tiene un valor valido
	  	if(reqConsMovHistCDASPID.getNombreInstEmisora() != null &&(!vacio.equals(reqConsMovHistCDASPID.getNombreInstEmisora()))){
    		//se concatena nombreBCOOrd
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER(?)");
	  		parametros.add("%"+reqConsMovHistCDASPID.getNombreInstEmisora());
	  	}
	  	
	  	builder.append( armaConsultaParametrizada3(parametros, reqConsMovHistCDASPID) );
	  	
    	return builder.toString();
    }
    
    /**
     * Metodo para sirve para generar el query y parametrizar la consulta.
     *
     * @param parametros Objeto del tipo @see List<Object>
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @return String Objeto del tipo @see String
     */
    private String armaConsultaParametrizada3(List<Object> parametros, BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

	  	//si claveRatreo tiene un valor valido
	  	if(reqConsMovHistCDASPID.getCveRastreo() != null && (!vacio.equals(reqConsMovHistCDASPID.getCveRastreo()))){
    		//se concatena claveRastreo
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM(?)");
	  		parametros.add("%"+reqConsMovHistCDASPID.getCveRastreo());
	  	}
	  	//si tipoPago tiene un valor valido
	  	if(reqConsMovHistCDASPID.getTipoPago() != null && (!vacio.equals(reqConsMovHistCDASPID.getTipoPago()))){
    		//se concatena tipoPago
	  		builder.append(" AND T1.TIPO_PAGO = ?");
	  		parametros.add(reqConsMovHistCDASPID.getTipoPago());
	  	}

	  	//si estatusCDA tiene un valor valido
	  	if(reqConsMovHistCDASPID.getEstatusCDA() != null && (!vacio.equals(reqConsMovHistCDASPID.getEstatusCDA()))){
    		//se concatena estatus
	  		builder.append(" AND T1.ESTATUS ");
	  		String[] val = reqConsMovHistCDASPID.getEstatusCDA().split(",");
	  		if(val.length==1){
	  			builder.append(" = ?");
	  			parametros.add(reqConsMovHistCDASPID.getEstatusCDA());
	  		}else if(val.length>1){
	  			builder.append(" in ( ?, ?) ");
	  			parametros.add(val[0]);
	  			parametros.add(val[1]);
	  		} 
	  	}
    	return builder.toString();
    }

    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo.
     *
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @return String objeto del tipo @see String
     */
    String generarConsultaExportarTodo2(BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

	  	//si horaEnvioInicio tiene un valor valido
    	if(reqConsMovHistCDASPID.getHrEnvioIni() != null && reqConsMovHistCDASPID.getHrEnvioFin() != null &&
	  			(!vacio.equals(reqConsMovHistCDASPID.getHrEnvioIni())) && (!vacio.equals(reqConsMovHistCDASPID.getHrEnvioFin()))){
	  		builder.append(" AND T1.HORA_ENVIO between ");
	          builder.append(reqConsMovHistCDASPID.getHrEnvioIni().replaceAll(":", ""));
	          builder.append(" and ");
	          builder.append(reqConsMovHistCDASPID.getHrEnvioFin().replaceAll(":", ""));

	  	}
	  	//si getMontoPago tiene un valor valido
	  	if(reqConsMovHistCDASPID.getMontoPago() != null  && (!vacio.equals(reqConsMovHistCDASPID.getMontoPago()))){
	  		 builder.append(" AND T1.MONTO = ");
	  		 String montoPago = reqConsMovHistCDASPID.getMontoPago().replaceAll(",", "");
	  	     builder.append(montoPago);
	  	}
	  	//si cuentaBeneficiario tiene un valor valido
	  	if(reqConsMovHistCDASPID.getCtaBeneficiario() != null && (!vacio.equals(reqConsMovHistCDASPID.getCtaBeneficiario()))){
	  		 builder.append(" AND TRIM(T1.NUM_CUENTA_REC) LIKE TRIM('%");
	  	     builder.append(reqConsMovHistCDASPID.getCtaBeneficiario());
	  	     builder.append("')");
	  	}
	  	
	  	builder.append( generarConsultaExportarTodo3(reqConsMovHistCDASPID) );
	  	
    	return builder.toString();
    }

    /**
     * Metodo para generar la segunda parte de la consulta de exportarTodo.
     *
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @return String objeto del tipo @see String
     */
    private String generarConsultaExportarTodo3(BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";

	  	//si nombreInstitucionEmisora tiene un valor valido
	  	if(reqConsMovHistCDASPID.getNombreInstEmisora() != null &&(!vacio.equals(reqConsMovHistCDASPID.getNombreInstEmisora()))){
	  		builder.append(" AND UPPER(T1.NOMBRE_BCO_ORD) LIKE UPPER('%");
	          builder.append(reqConsMovHistCDASPID.getNombreInstEmisora());
	          builder.append("')");
	  	}
	  	//si claveRastreo tiene un valor valido
	  	if(reqConsMovHistCDASPID.getCveRastreo() != null && (!vacio.equals(reqConsMovHistCDASPID.getCveRastreo()))){
	  		builder.append(" AND TRIM(T1.CVE_RASTREO) LIKE TRIM('%");
	      	builder.append(reqConsMovHistCDASPID.getCveRastreo());
	      	builder.append("')");
	  	}
	  	//si estatusCDA tiene un valor valido
	  	if(reqConsMovHistCDASPID.getEstatusCDA() != null && (!vacio.equals(reqConsMovHistCDASPID.getEstatusCDA()))){
	  		String[] val = reqConsMovHistCDASPID.getEstatusCDA().split(",");
	  		if(val.length==1){
	  			builder.append(" AND T1.ESTATUS = ");	
	  			builder.append(reqConsMovHistCDASPID.getEstatusCDA());
	  		}else if(val.length>1){
	  			builder.append(" AND T1.ESTATUS IN ( ");	
	  			builder.append(val[0]);
	  			builder.append(",");
	  			builder.append(val[1]);
	  			builder.append(" ) ");
	  		} 
	  	}
    	return builder.toString();
    }
}
