/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaRecepcionesImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepcionesDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBloqueo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDOpciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga obtener la informacion para la consulta de
 * recepciones
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaRecepcionesImpl extends Architech implements DAOConsultaRecepciones {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 2988110814370576895L;

	/**
	 * Constante privada del tipo String que almacena el query para la consulta
	 * de recepciones
	 */
	private static final StringBuilder TOTAL_CONSULTA_RECEPCIONES = new StringBuilder().append("SELECT ")
			.append(" TOPOLOGIA || ',' || TIPO_PAGO || ',' || ESTATUS_BANXICO || '|' || ESTATUS_TRANSFER || ',' || FCH_OPERACION || ',' ||")
			.append(" FCH_CAPTURA || ',' || CVE_INST_ORD || ',' || CVE_INTERME_ORD || ',' || FOLIO_PAQUETE || ',' || FOLIO_PAGO || ',' || ")
			.append(" NUM_CUENTA_REC || ',' || MONTO || ',' ||")
			.append(" MOTIVO_DEVOL || ',' || CODIGO_ERROR || ',' || NUM_CUENTA_TRAN").append(" FROM TRAN_SPID_REC");
	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos las
	 * recepciones exportadas
	 */
	private static final StringBuilder INSERT_TODOS_CONSULTA_RECEPCIONES = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * JOIN BLOQUEO
	 */
	private static final String QUERY =
	        " R.FCH_OPERACION = B.FCH_OPERACION "+
	        " AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
	        " AND R.CVE_INST_ORD = B.CVE_INST_ORD "+
	        " AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
	        " AND R.FOLIO_PAGO = B.FOLIO_PAGO ";
	
	/**
	 * CONSULTA DETALLE
	 */
	private static final String QUERY_DETALLE =	
	" SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "+
	" R.folio_paquete,R.folio_pago,R.topologia, "+
	" R.enviar,R.cda,R.estatus_banxico, "+
	" R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "+
	" R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "+
	" R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "+
	" R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "+
	" R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, "+
	" R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "+
	" R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, "+
	" R.prioridad,R.long_rastreo,R.folio_paq_dev, "+
	" R.folio_pago_dev,R.refe_transfer,R.monto, "+
	" TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "+
	" R.direccion_ip,R.cde,R.nombre_ord, "+        
	" R.nombre_rec,R.domicilio_ord,R.concepto_pago, B.CVE_USUARIO, B.FUNCIONALIDAD, "+     
   " ROWNUM INDICE "+
   " FROM TRAN_SPID_REC R LEFT JOIN TRAN_SPID_BLOQUEO B ON( " +
    QUERY +
   " ) "+
   " WHERE R.CVE_MI_INSTITUC = ? "+
   " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') " +
   " AND  R.CVE_INST_ORD = ? " +
   " AND  R.FOLIO_PAQUETE = ? " +
   " AND  R.FOLIO_PAGO = ? ";
	
	/**
	 * Metodo que sirve para consultar las Recepciones
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 * @param cveInst
	 *            cadena con la clave institucion
	 * @param fch
	 *            cadena con la fecha de recepcion
	 * @return beanResConsultaRecepcionesDAO Objeto del tipo
	 *         BeanResConsultaRecepcionesDAO
	 */
	@Override
	public BeanResConsultaRecepcionesDAO obtenerConsultaRecepciones(BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, BeanReqConsultaRecepciones beanReqConsultaRecepciones,
			String cveInst, String fch) {
		String tipoOrden = "";
		tipoOrden = beanReqConsultaRecepciones.getTipoOrden();

		StringBuilder consultaRecepciones = generarFormatoQuery(beanReqConsultaRecepciones, tipoOrden);

		final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO = new BeanResConsultaRecepcionesDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones = null;
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(cveInst);
		parametros.add(fch);
		parametros.add(cveInst);
		parametros.add(fch);
		parametros.add(beanPaginador.getRegIni());
		parametros.add(beanPaginador.getRegFin());

		try {

			responseDTO = HelperDAO.consultar(consultaRecepciones.toString(), parametros, HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());

			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				if (!list.isEmpty()) {
        			 listaBeanConsultaRecepciones = llenarConsultaRecepciones(beanResConsultaRecepcionesDAO, list,
							utilerias);
        		 }
        	}     	

        	beanResConsultaRecepcionesDAO.setListaConsultaRecepciones(listaBeanConsultaRecepciones);
        	beanResConsultaRecepcionesDAO.setCodError(responseDTO.getCodeError());
        	beanResConsultaRecepcionesDAO.setMsgError(responseDTO.getMessageError());

    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return beanResConsultaRecepcionesDAO;
	}
	
	public BeanResRecepcionDAO obtenerDetRecepcion(BeanSPIDLlave beanLlave,
			ArchitechSessionBean architechSessionBean) {
		
		BeanResRecepcionDAO beanResRecepcionDAO = new BeanResRecepcionDAO();
		List<HashMap<String,Object>> list = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	BeanReceptoresSPID beanDetReceptorSPID = new BeanReceptoresSPID();
    	
    	try {
        	responseDTO = HelperDAO.consultar(QUERY_DETALLE.toString(), Arrays.asList(
        			new Object[] { beanLlave.getCveMiInstituc(),
        					beanLlave.getFchOperacion(),
        					beanLlave.getCveInstOrd(),
        					beanLlave.getFolioPaquete(),
        					beanLlave.getFolioPago() })
        			, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();
        		 
        		 if(!list.isEmpty()){
    			 	for (Map<String, Object> mapResult : list){
    			 		beanDetReceptorSPID = descomponeConsulta(mapResult);
    			 		beanResRecepcionDAO.setBeanReceptorSPID(beanDetReceptorSPID);
    		        	beanResRecepcionDAO.setCodError(responseDTO.getCodeError());
    		        	beanResRecepcionDAO.setMsgError(responseDTO.getMessageError());
    		        	return beanResRecepcionDAO;
    			 	}
    			 }
    		 }        	
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return null;
	}
	
	/**
     * Metodo que genera un objeto del tipo BeanTranSpeiRec a partir de la respuesta de la consulta
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanReceptoresSPID Objeto del tipo BeanReceptoresSPID
     */
    private BeanReceptoresSPID descomponeConsulta(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBloqueo beanSPIDBloqueo = new BeanSPIDBloqueo();
    	BeanReceptoresSPID beanReceptoresSPID = new BeanReceptoresSPID();
    	beanReceptoresSPID.setLlave(seteaLlave(map));
    	beanReceptoresSPID.setDatGenerales(seteaDatosGenerales(map));
    	beanReceptoresSPID.setEstatus(seteaEstatus(map));
    	beanReceptoresSPID.setBancoOrd(seteaBancoOrd(map));
    	beanReceptoresSPID.setBancoRec(seteaBancoRec(map));
    	beanReceptoresSPID.setEnvDev(seteaEnvDev(map));
    	beanReceptoresSPID.setRastreo(seteaRastreo(map));
    	beanReceptoresSPID.setFchPagos(seteaFchPago(map));
    	beanReceptoresSPID.setOpciones(new BeanSPIDOpciones());
    	beanReceptoresSPID.setBloqueo(beanSPIDBloqueo);
    	
    	beanSPIDBloqueo.setUsuario(utilerias.getString(map.get("CVE_USUARIO")));
    	if("RECEP_OPSPID".equals(utilerias.getString(map.get("FUNCIONALIDAD")))){
    		beanSPIDBloqueo.setBloqueado(true);	
    	}
    	return beanReceptoresSPID;
    }
    
    /**
     * Metodo que setea el folio paquete y pago de devolucion
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEnvDev Bean de respuesta 
     */
    private BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
    	beanSPIDEnvDev.setMotivoDev(utilerias.getString(map.get("MOTIVO_DEVOL")));
    	beanSPIDEnvDev.setFolioPaqDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")));
    	beanSPIDEnvDev.setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")));
    	
    	return beanSPIDEnvDev;
    }
    
    /**
     * Metodo que setea los campos llave
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDLlave Bean de respuesta 
     */
    private BeanSPIDLlave seteaLlave(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
    	beanSPIDLlave.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
    	beanSPIDLlave.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
    	beanSPIDLlave.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
    	beanSPIDLlave.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
    	beanSPIDLlave.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
    	return beanSPIDLlave;
    }
    
    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDDatGenerales Bean de respuesta de datos generales
     */
    private BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
    	BigDecimal monto = utilerias.getBigDecimal(map.get("MONTO"));
    	beanSPIDDatGenerales.setTopologia(utilerias.getString(map.get("TOPOLOGIA")));
    	beanSPIDDatGenerales.setPrioridad(utilerias.getString(map.get("PRIORIDAD")));
    	beanSPIDDatGenerales.setTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
    	beanSPIDDatGenerales.setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")));
    	beanSPIDDatGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
    	beanSPIDDatGenerales.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
    	beanSPIDDatGenerales.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
    	beanSPIDDatGenerales.setCde(utilerias.getString(map.get("CDE")));
    	beanSPIDDatGenerales.setCda(utilerias.getString(map.get("CDA")));
    	beanSPIDDatGenerales.setEnviar(utilerias.getString(map.get("ENVIAR")));
    	beanSPIDDatGenerales.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
    	
    	beanSPIDDatGenerales.setMonto(monto);
    	return beanSPIDDatGenerales;
    }
    
    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEstatus Bean de respuesta de estatus
     */
    private BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
    	beanSPIDEstatus.setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")));
    	beanSPIDEstatus.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
    	beanSPIDEstatus.setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));
    	return beanSPIDEstatus;
    }
    /**
     * Metodo que setea los campos del banco ordenante
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
    	beanSPIDBancoOrd.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
    	beanSPIDBancoOrd.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
    	beanSPIDBancoOrd.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
    	beanSPIDBancoOrd.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
    	beanSPIDBancoOrd.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
    	beanSPIDBancoOrd.setDomicilioOrd(utilerias.getString(map.get("DOMICILIO_ORD")));
    	beanSPIDBancoOrd.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
    	beanSPIDBancoOrd.setCodPostalOrd(utilerias.getString(map.get("COD_POSTAL_ORD")));
    	beanSPIDBancoOrd.setFchConstitOrd(utilerias.getString(map.get("FCH_CONSTIT_ORD")));
    	
    	return beanSPIDBancoOrd;
    }
    
    /**
     * Metodo que setea los campos de rastreo
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
    	beanSPIDRastreo.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
    	beanSPIDRastreo.setCveRastreoOri(utilerias.getString(map.get("CVE_RASTREO_ORI")));
    	beanSPIDRastreo.setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")));
    	beanSPIDRastreo.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
    	return beanSPIDRastreo;
    }
    
    /**
     * Metodo que setea los campos del Banco Receptor
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
    	beanSPIDBancoRec.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
    	beanSPIDBancoRec.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaTran(utilerias.getString(map.get("NUM_CUENTA_TRAN")));
    	beanSPIDBancoRec.setRfcRec(utilerias.getString(map.get("RFC_REC")));
    	
    	return beanSPIDBancoRec;
    }
    
    /**
     * Metodo que setea los campos de las fechas de pago
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
     */
    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
    	beanSPIDFchPagos.setFchAceptPago(utilerias.getString(map.get("FCH_ACEPT_PAGO")));
    	beanSPIDFchPagos.setHoraAceptPago(utilerias.getString(map.get("HORA_ACEPT_PAGO")));
    	beanSPIDFchPagos.setFchInstrucPago(utilerias.getString(map.get("FCH_INSTRUC_PAGO")));
    	beanSPIDFchPagos.setHoraInstrucPago(utilerias.getString(map.get("HORA_INSTRUC_PAGO")));
    	return beanSPIDFchPagos;
    }

	/**
	 * Metodo para generar el formato del query
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @param tipoOrden Objeto del tipo String
	 * @return consultaRecepciones Objeto del tipo StringBuilder
	 */
	private StringBuilder generarFormatoQuery(BeanReqConsultaRecepciones beanReqConsultaRecepciones, String tipoOrden) {
		StringBuilder consultaRecepciones = 
				new StringBuilder();
		consultaRecepciones.append("SELECT T.* FROM (SELECT T.*, ROWNUM R FROM (SELECT TOPOLOGIA, ")
		.append("TIPO_PAGO,ESTATUS_BANXICO, ESTATUS_TRANSFER, ")
		.append("TO_CHAR(FCH_OPERACION,'DD/MM/YYYY') FCH_OPERACION,TO_CHAR(FCH_CAPTURA,'HH24:MI') FCH_CAPTURA,CVE_INST_ORD, ")
		.append("CVE_INTERME_ORD,FOLIO_PAQUETE,FOLIO_PAGO, NUM_CUENTA_REC, ")
		.append("NUM_CUENTA_TRAN,MONTO, CODIGO_ERROR, MOTIVO_DEVOL,C.CONT ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT FROM TRAN_SPID_REC ")
		.append(" WHERE  ");
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos por Aplicar");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Aplicados");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Rechazados");
		}
		
		consultaRecepciones.append(" AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ) C ")
		.append(" WHERE  ");
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
			
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
		}
		consultaRecepciones.append("AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ");
		
		String sortField = getSortField(beanReqConsultaRecepciones);
		
		if (sortField != null && !"".equals(sortField)){
			consultaRecepciones.append(" ORDER BY ").append(sortField).append(" ").append(beanReqConsultaRecepciones.getSortType());
		}	
		
		consultaRecepciones.append(") T) T WHERE R BETWEEN ? AND ?  ");	
		return consultaRecepciones;
	}

	/**
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @return String
	 */
	private String getSortField(BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		String sortField = "";
		
		if ("topologia".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "TOPOLOGIA";
		} else if ("tipoPago".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "TIPO_PAGO";
		} else if ("estatusBanxico".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "ESTATUS_BANXICO";
		} else if ("estatusTransfer".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "ESTATUS_TRANSFER";
		} else if ("fechOperacion".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FCH_OPERACION";
		} else if ("fechCaptura".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FCH_CAPTURA";
		} else if ("cveInsOrd".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "CVE_INST_ORD";
		} else if ("cveIntermeOrd".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "CVE_INTERME_ORD";
		} else if ("folioPaquete".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FOLIO_PAQUETE";
		} else if ("folioPago".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FOLIO_PAGO";
		} else if ("numCuentaRec".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "NUM_CUENTA_REC";
		} else if ("monto".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "MONTO";
		} else if ("motivoDevol".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "MOTIVO_DEVOL";
		} else if ("codigoError".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "CODIGO_ERROR";
		} else if ("numCuentaTran".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "NUM_CUENTA_TRAN";
		}
		return sortField;
	}

	/**
	 * Metodo para llenar la consulta de Recepciones
	 * 
	 * @param beanResConsultaRecepcionesDAO Objeto del tipo BeanResConsultaRecepcionesDAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @return listaBeanConsultaRecepciones Objeto del tipo List<BeanConsultaRecepciones>
	 */
	private List<BeanConsultaRecepciones> llenarConsultaRecepciones(
			final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO, List<HashMap<String, Object>> list,
			Utilerias utilerias) {
		BeanConsultaRecepciones beanConsultaRecepciones;
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones;
					listaBeanConsultaRecepciones = new ArrayList<BeanConsultaRecepciones>();
					for (Map<String, Object> listMap : list) {
						Map<String, Object> mapResult = listMap;
						beanConsultaRecepciones = new BeanConsultaRecepciones();
						beanConsultaRecepciones.setBeanConsultaRecepcionesDos(new BeanConsultaRecepcionesDos());
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setTopologia(utilerias.getString(mapResult.get("TOPOLOGIA")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setTipoPago(utilerias.getInteger(mapResult.get("TIPO_PAGO")));
						beanConsultaRecepciones
						.getBeanConsultaRecepcionesDos().setEstatusBanxico(utilerias.getString(mapResult.get("ESTATUS_BANXICO")));
						beanConsultaRecepciones
						.getBeanConsultaRecepcionesDos().setEstatusTransfer(utilerias.getString(mapResult.get("ESTATUS_TRANSFER")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFechOperacion(utilerias.getString(mapResult.get("FCH_OPERACION")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFechCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setCveInsOrd(utilerias.getString(mapResult.get("CVE_INST_ORD")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setCveIntermeOrd(utilerias.getString(mapResult.get("CVE_INTERME_ORD")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFolioPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setNumCuentaTran(utilerias.getString(mapResult.get("NUM_CUENTA_TRAN")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setMonto(utilerias.getString(mapResult.get("MONTO")));
						beanConsultaRecepciones.setCodigoError(utilerias.getString(mapResult.get("CODIGO_ERROR")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setMotivoDevol(utilerias.getString(mapResult.get("MOTIVO_DEVOL")));
						beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setNumCuentaRec(utilerias.getString(mapResult.get("NUM_CUENTA_REC")));

						listaBeanConsultaRecepciones.add(beanConsultaRecepciones);
						beanResConsultaRecepcionesDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));

					}
					return listaBeanConsultaRecepciones;
				}

	/**
	 * Metodo que sirve para exportar todo de las Recepciones
	 * 
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param cveInst
	 *            cadena con la clave institucion
	 * @param fch
	 *            cadena con la fecha de recepcion
	 * @return beanResConsultaRecepcionesDAO Objeto del tipo
	 *         BeanResConsultaRecepcionesDAO
	 */
	@Override
	public BeanResConsultaRecepcionesDAO exportarTodoConsultaRecepciones(
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, ArchitechSessionBean architechSessionBean,
			String cveInst, String fch) {

		StringBuilder builder = new StringBuilder();
		final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO = new BeanResConsultaRecepcionesDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		final Date fecha = new Date();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			builder.append(TOTAL_CONSULTA_RECEPCIONES).append(" WHERE ");
			if("PA".equalsIgnoreCase(beanReqConsultaRecepciones.getTipoOrden())){
				builder.append(" (ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
				builder.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL ) ");
			}else if("TR".equalsIgnoreCase(beanReqConsultaRecepciones.getTipoOrden())){
				builder.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
				
			}else if("RE".equalsIgnoreCase(beanReqConsultaRecepciones.getTipoOrden())){
				builder.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
			}
			builder.append("AND  CVE_MI_INSTITUC='" + cveInst + "' AND FCH_OPERACION=TO_DATE('" + fch + "', 'DD-MM-YYYY') ");
			
			final String fechaActual = utilerias.formateaFecha(fecha,
					Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
			final String nombreArchivo = "SPID_" + obtenerNombre(beanReqConsultaRecepciones.getTipoOrden()) + fechaActual + ".csv";
			beanResConsultaRecepcionesDAO.setNombreArchivo(nombreArchivo);

			responseDTO = HelperDAO.insertar(INSERT_TODOS_CONSULTA_RECEPCIONES.toString(),
					Arrays.asList(new Object[] { architechSessionBean.getUsuario(), architechSessionBean.getIdSesion(),
							"SPID", "CONSULTA_RECEPCIONES", nombreArchivo, beanReqConsultaRecepciones.getTotalReg(),
							"PE", builder.toString(), "TOPO, TIP, EST, FCH OPER., HORA, INST ORD., INTER. ORD., FOLIO PAQUETE, "
							+"FOLIOS PAGO, CUENTA REC., IMPORTE, MOT. DEVOL., COD. ERR., NUM. CUENTA TRAN."  }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			beanResConsultaRecepcionesDAO.setCodError(responseDTO.getCodeError());
			beanResConsultaRecepcionesDAO.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsultaRecepcionesDAO.setCodError(Errores.EC00011B);
		}

		return beanResConsultaRecepcionesDAO;
	}
	
	/**
	 * @param tipo Objeto del tipo String
	 * @return String
	 */
	private String obtenerNombre(String tipo)
	{
		String nombre ="";
		
		if ("PA".equalsIgnoreCase(tipo)){
			nombre = "REC_PA_";
		} else if ("TR".equalsIgnoreCase(tipo)){
			nombre = "REC_TR_";
		} else if ("RE".equalsIgnoreCase(tipo)){
			nombre = "REC_RE_";
		}
		return nombre;
	}
	
	/**Metodo que sirve para el monto total
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	@Override
	public BigDecimal obtenerImporteTotal( ArchitechSessionBean architechSessionBeans, String tipoOrden, String cveInst, String fch) {
	  	BigDecimal totalImporte = BigDecimal.ZERO;
	  	List<Object> parametros = new ArrayList<Object>();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String,Object>> list = null;
		parametros.add(cveInst);
		parametros.add(fch);
		parametros.add(cveInst);
		parametros.add(fch);

	  	try {              	
	  			  		responseDTO = HelperDAO.consultar(generarQueryMonto(tipoOrden).toString(), parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	if(responseDTO.getResultQuery()!= null){
	      		 list = responseDTO.getResultQuery();
	      		 if(!list.isEmpty()){
	      			for(Map<String,Object> mapResult : list){
	      				totalImporte = totalImporte.add(new BigDecimal(Utilerias.getUtilerias().getString(mapResult.get("MONTO_TOTAL"))));
	      			 }
	      		 }
	      	}
	      	
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}

	/**
	 * Metodo para generar el formato del query
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @param tipoOrden Objeto del tipo String
	 * @return consultaRecepciones Objeto del tipo StringBuilder
	 */
	private StringBuilder generarQueryMonto( String tipoOrden) {
		StringBuilder consultaRecepciones = 
				new StringBuilder();
		consultaRecepciones.append("SELECT ")
		.append("sum(MONTO) MONTO_TOTAL ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT FROM TRAN_SPID_REC ")
		.append(" WHERE  ");
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL ) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
		}
		
		consultaRecepciones.append(" AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ) C ")
		.append(" WHERE  ");
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL ) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
			
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
		}
		consultaRecepciones.append("AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ");
				
		return consultaRecepciones;
	}
	
}
