/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOBitAdministrativaImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.comun;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitAdministrativaImpl extends Architech implements DAOBitAdministrativa{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3095582372644426098L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_BIT_ADMON
	 */
	private static final String QUERY_INSERT_BIT_ADMON = "INSERT INTO TR_NAL_BIT_ADMINISTRATIVA ( "+
		"FOLIO_OPER, FCH_OPER, USER_OPER, TIPO_OPER, ID_TOKEN, " +
		"FCH_ACT, HORA_ACT, VAL_ANT, VAL_NVO, COMENTARIOS, " +
		"DATO_FIJO, SERV_TRAN, DIR_IP, CAN_APLI, INST_WEB, "+
		"HOST_WEB, DATO_MODI, TABLA_AFEC, ID_SESION, COD_OPER ) " +
		"VALUES ( " +
		"SEQ_TR_NAL_BIT_ADMON_FOL_OPER.NEXTVAL, TO_DATE(?,'DD-MM-YYYY'), ?, ?, ?, "+
	    "TO_DATE(?,'DD-MM-YYYY'), TO_DATE(?,'DD-MM-YYYY: HH24:mi:ss'), ?, ?, ?, "+	    
	    "?, ?, ?, ?, ?, "+
	    "?, ?, ?, ?, ? )";
	    
	/**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitacora Objeto del tipo @see BeanReqInsertBitacora
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitAdmin beanReqInsertBitacora, ArchitechSessionBean architechSessionBean){
        final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.insertar(QUERY_INSERT_BIT_ADMON, 
        		   Arrays.asList(new Object[]{
        				   beanReqInsertBitacora.getFchOper(),
        				   beanReqInsertBitacora.getUserOper(),
        				   beanReqInsertBitacora.getTipoOper(),
        				   beanReqInsertBitacora.getIdToken(),
        				   beanReqInsertBitacora.getFchAct(),
        				   beanReqInsertBitacora.getHoraAct(),
        				   beanReqInsertBitacora.getValAnt(),
        				   beanReqInsertBitacora.getValNvo(),
        				   beanReqInsertBitacora.getComentarios(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getDatoFijo(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getServTran(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getDirIp(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getCanApli(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getInstWeb(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getHostWeb(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getDatoMobi(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getTablaAfec(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getIdSession(),
        				   beanReqInsertBitacora.getBeanReqInsertBitAdminDos().getCodOper()
        				   }), 
           HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
           beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaBitacoraDAO;
     }
	    
	    
		
		
		
	
	  

}
