/**
DAONotificador * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAONotificadorImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/07/2019 12:27:11 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificaciones;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesCombo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasAfectacionBloques;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasNotificador;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasNotificadorAux;

/**
 * Class DAONotificadorImpl.
 *
 * Clase que contiene los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAONotificadorImpl extends Architech implements DAONotificador{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -9208458354244541740L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante CONSULTA_TODOS. */
	private static final String CONSULTA_TODO = "SELECT TIPO_NOTIF,CVE_TRANSFER,MEDIO_ENTREGA,ESTATUS_TRANSFER,TOTAL"
			.concat(" FROM")
			.concat(" (")
			.concat(" SELECT DATOS.TIPO_NOTIF,DATOS.CVE_TRANSFER,DATOS.MEDIO_ENTREGA,DATOS.ESTATUS_TRANSFER,DATOS.TOTAL, ROWNUM AS RN FROM (")
			.concat(" SELECT ")
			.concat(" TRIM(CN.TIPO_NOTIF) ||' - '|| TRIM(TP.DESCRIPCION) TIPO_NOTIF, ")
			.concat(" TRIM(CN.CVE_TRANSFER) ||' - '|| TRIM(TT.DESCRIPCION) CVE_TRANSFER, ")
			.concat(" TRIM(ME.CVE_MEDIO_ENT) ||' - '|| TRIM(ME.DESCRIPCION) MEDIO_ENTREGA, ")
			.concat(" TRIM(CN.ESTATUS_TRANSFER) ||' - '|| TRIM(TP1.DESCRIPCION) ESTATUS_TRANSFER ,")
			.concat(" (SELECT COUNT(1) AS CONT FROM TRAN_MX_CAT_NOTIF")
			.concat(" [FILTRO_WH]")
			.concat(" ) TOTAL")
			.concat(" FROM TRAN_MX_CAT_NOTIF CN, COMU_MEDIOS_ENT ME,TRAN_PARAMETROS TP, TRAN_PARAMETROS TP1, TRAN_TRANSFERENC TT")
			.concat(" WHERE TP.CVE_PARAMETRO LIKE '%NOTIFICADOR%'")
			.concat(" AND TRIM(TP1.CVE_PARAMETRO) LIKE '%ESTATUSTRANS%'")
			.concat(" AND TRIM(CN.MEDIO_ENTREGA) = TRIM(ME.CVE_MEDIO_ENT) ")
			.concat(" AND TRIM(TP.CVE_PARAM_FALTA) = TRIM(CN.TIPO_NOTIF)")
			.concat(" AND TRIM(TP1.CVE_PARAM_FALTA) = TRIM(CN.ESTATUS_TRANSFER)")
			.concat(" AND TRIM(TT.CVE_TRANSFE) = TRIM(CN.CVE_TRANSFER)")
			.concat(" [FILTRO_AND]")
			.concat(" ) DATOS")
			.concat(" ) CAT ");
			
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
	
	
	private static final String CONSULTA_WHERE = "WHERE TRIM(TIPO_NOTIF) = TO_NUMBER(TRIM(?)) AND "
			.concat("TRIM(CVE_TRANSFER) = UPPER(TRIM(?)) AND ")
			.concat("TRIM(MEDIO_ENTREGA) = UPPER(TRIM(?)) AND ")
			.concat("TRIM(ESTATUS_TRANSFER) = UPPER(TRIM(?))");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_MX_CAT_NOTIF "
			.concat("(TIPO_NOTIF,CVE_TRANSFER,MEDIO_ENTREGA,ESTATUS_TRANSFER) ")
			.concat("VALUES (TRIM(?),UPPER(TRIM(?)),UPPER(TRIM(?)),UPPER(TRIM(?)))");
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_MX_CAT_NOTIF "
			.concat(CONSULTA_WHERE);
	
	/** La constante CONSULTA_ELIMINAR. */
	private static final String CONSULTA_ELIMINAR = "DELETE FROM TRAN_MX_CAT_NOTIF "
			.concat(CONSULTA_WHERE);
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR = "UPDATE TRAN_MX_CAT_NOTIF SET "
			.concat("TIPO_NOTIF = TRIM(?), ")
			.concat("CVE_TRANSFER = UPPER(TRIM(?)), ")
			.concat("MEDIO_ENTREGA = UPPER(TRIM(?)), ")
			.concat("ESTATUS_TRANSFER = UPPER(TRIM(?)) ")
			.concat(CONSULTA_WHERE);
	
	
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasNotificador utilerias = UtileriasNotificador.getInstancia();
	
	private static UtileriasNotificadorAux utileriasListas = UtileriasNotificadorAux.getInstancia();
	
	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanNotificacion --> objeto que almacena las notificaciones
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador --> bean que permite paginar
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	@Override
	public BeanNotificaciones consultar(BeanNotificacion beanNotificacion, BeanPaginador beanPaginador,
			ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanNotificaciones response = new BeanNotificaciones();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanNotificacion> notificaciones = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utilerias.getFiltro(beanNotificacion, "AND");
		String filtroWhere = utilerias.getFiltro(beanNotificacion, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				notificaciones = new ArrayList<BeanNotificacion>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanNotificacion notificacion = new BeanNotificacion();
					/** Seteo de datos **/
					notificacion.setTipoNotif(utilerias.getString(map.get("TIPO_NOTIF")));
					notificacion.setCveTransfer(utilerias.getString(map.get("CVE_TRANSFER")));
					notificacion.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
					notificacion.setMedioEntrega(utilerias.getString(map.get("MEDIO_ENTREGA")));
					/** Se añade el objeto a la lista**/
					notificaciones.add(notificacion);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setNotificaciones(notificaciones);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * consultar listados de notificaciones
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param tipo --> variable de tipo int que trae el tipo de listado
	 * @param filter --> objeto de tipo BeanNotificacion que trae el filtro
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return listas --> regresa la lista de los registros
	 */
	@Override
	public BeanNotificacionesCombo consultarListas(int tipo, BeanNotificacion filter, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanNotificacionesCombo listas = new BeanNotificacionesCombo();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanCatalogo> list = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		try {
			for (int i = 0; i < 4; i++) {
				responseDTO = HelperDAO.consultar(utileriasListas.evaluaConsulta(tipo, i, filter), Collections.emptyList(), HelperDAO.CANAL_GFI_DS,this.getClass().getName());
				if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
					list = new ArrayList<BeanCatalogo>();
					/** Se obtiene rel resultado de la consulta **/
					queryResponse = responseDTO.getResultQuery();
					list = getLista(queryResponse, list);
				}
				listas = utilerias.setLista(i, listas, list);
			}
		} catch (Exception e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		return listas;
	}

	/**
	 * Obtener el objeto: lista.
	 *
	 * Recorre el resultado de la consulta y setea los
	 * resultados a una lista
	 * 
	 * @param queryResponse --> objeto de tipo lista que trae la respuesta del query
	 * @param list --> objeto de tipo lista que trae el bean catalogo
	 * @return list --> retorna la lista de registros
	 */
	private List<BeanCatalogo> getLista(List<HashMap<String, Object>> queryResponse, List<BeanCatalogo> list) {
		BeanCatalogo catalogo = null;
		/** Se recorre la respuesta  **/
		for (HashMap<String, Object> map : queryResponse) {
			catalogo = new  BeanCatalogo();
			/** Seteo de datos **/
			catalogo.setCve(utilerias.getString(map.get("CVE")));
			catalogo.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
			list.add(catalogo);
		}
		return list;
	}
	
	/**
	 * agregar notificacion
	 * 
	 * Metodo publico utilizado para agregar notificaciones
	 *
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion --> bean que obtiene la notificacion
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	@Override
	public BeanResBase agregar(BeanNotificacion beanNotificacion, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanNotificacion, UtileriasNotificador.ALTA);
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(beanNotificacion);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * editar notificacion
	 * 
	 * Metodo publico utilizado para editar notificaciones
	 *
	 * @param beanNotificacion --> objeto que trae notificaciones 	
	 * @param anterior --> objeto que trae los registros anteriores
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	@Override
	public BeanResBase editar(BeanNotificacion beanNotificacion, BeanNotificacion anterior, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			int existencia = 0;
			existencia = buscarRegistro(beanNotificacion);
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utilerias.agregarParametros(beanNotificacion, UtileriasAfectacionBloques.MODIFICACION);
				parametros = utilerias.agregarParametrosOld(anterior, parametros);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(CONSULTA_MODIFICAR, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion --> bean que obtiene notificaciones
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	@Override
	public BeanResBase eliminar(BeanNotificacion beanNotificacion, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanNotificacion, UtileriasNotificador.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(CONSULTA_ELIMINAR, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 * 
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param beanNotificacion --> bean que obtiene notificaciones
	 * @return contador --> regresa el contador
	 */
	private int buscarRegistro(BeanNotificacion beanNotificacion) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utilerias.agregarParametros(beanNotificacion, UtileriasAfectacionBloques.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
}
