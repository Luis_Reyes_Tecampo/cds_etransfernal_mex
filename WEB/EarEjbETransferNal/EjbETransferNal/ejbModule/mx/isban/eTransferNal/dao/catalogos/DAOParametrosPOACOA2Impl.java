/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOParametrosPOACOAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanParamCifradoDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanProcesosPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanResObtenFechaCtrlDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Implementa la interface DAOParametrosPOACOA.
 *
 * @author FSW-Vector
 * @since 15/01/2019
 */
@Stateless
public class DAOParametrosPOACOA2Impl extends Architech implements
		DAOParametrosPOACOA2 {
	
	/** Version */
	private static final long serialVersionUID = 1L;
	
	
	/** Constenate uno. */
	private static final String UNO = "1";
	
	
	/** Query para obtener la institucion */
	private static final String QUERY_CONS_MI_INSTITUCION = 
        " SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
              " WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL ";
	
	/** Query para obtener la fecha de operacion spei */
	private static final String QUERY_CONS_FCH_SPEI = "SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL WHERE CVE_MI_INSTITUC = ?";
	
	/** Query para obtener cifrado */
	private static final String QUERY_CONS_FIRMA = "SELECT CIFRADO FROM TRAN_POA_COA_PARAM WHERE CVE_MI_INSTITUC = ?";
	
	/** Query los datos del proceso POACOA */
	private static final String QUERY_CONS_GRAL = "SELECT GENERAR, FASE, ACTIVACION, NO_HAY_RETORNO FROM TRAN_POA_COA_CTRL " +
			"WHERE FCH_OPERACION = TO_DATE(?,'DD/MM/YYYY') AND CVE_MI_INSTITUC = ?";

   
	/**
	 * Obtiene la institucion .
	 *
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResConsMiInstDAO con la respuesta de la institucion
	 */
	@Override
	public BeanResConsMiInstDAO consultaMiInstitucion(String modulo, ArchitechSessionBean sessionBean) {
		  /** Se declara el objeto de respuesta **/
		  BeanResConsMiInstDAO beanResConsMiInstDAO =  new BeanResConsMiInstDAO();
	      List<HashMap<String,Object>> list = null;
	      /** Se crea una instancia a la clase Utilerias **/
	      Utilerias utilerias = Utilerias.getUtilerias();
	      ResponseMessageDataBaseDTO responseDTO = null;
	      /** Se crea una lista para setear los parametros SQL **/
	      List<Object> parametros = new ArrayList<Object>();
	      String query = null;
	      try {      
	    	  /** Se declara una variable para evaluar la consulta **/
	            query = QUERY_CONS_MI_INSTITUCION;
	            /** Se valida el modulo **/
	            if ("2".equals(modulo)) {
	            	/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
	            	query = query.replaceAll("ENTIDAD_SPEI", "ENTIDAD_SPID");
	            }
	            responseDTO = HelperDAO.consultar(query, parametros,
	                          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	            /**  Se valida el resultado de la operacion **/
	            if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	                   list = responseDTO.getResultQuery();
	                   Map<String,Object> mapResult = list.get(0);
	                        beanResConsMiInstDAO.setMiInstitucion(utilerias.getString(mapResult.get("CV_VALOR")));
	                        /**  Se setea el codigo de error **/
	                        beanResConsMiInstDAO.setCodError(Errores.OK00000V);
	            }else{
	            	beanResConsMiInstDAO.setCodError(Errores.ED00011V);
	            }
	      } catch (ExceptionDataAccess e) {
	    	  /**  Manejo de expceciones **/
	            showException(e);
	            beanResConsMiInstDAO.setCodError(Errores.EC00011B);
	            beanResConsMiInstDAO.setMsgError(Errores.DESC_EC00011B);
	      }
	      /**  Retorno del metodo **/
	        return beanResConsMiInstDAO;     
	    }

	/**
	 * Obtiene la fecha de operacion de la tabla TRAN_CTRL_FECHAS .
	 *
	 * @param esLiqFinal true si es el campo de Liquidacion final
	 * @param sessionBean Datos de sesion
	 * @return BeanResObtenFechaCtrlDAO bean con la fecha de control
	 */
	@Override
	public BeanResObtenFechaCtrlDAO obtenFechaCTRLFECHAS(boolean esLiqFinal,
			ArchitechSessionBean sessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResObtenFechaCtrlDAO beanResObtenFechaCtrl = new BeanResObtenFechaCtrlDAO();
		String query = "";
		String fecha = null;
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se arma la consulta **/
		if (esLiqFinal) {
			query = "SELECT FCH_LIQ_FINAL FCH_OP FROM TRAN_CTRL_FECHAS WHERE FCH_SIST_PK = SYSDATE";
		} else {
			query = "SELECT FCH_CONTA FCH_OP FROM TRAN_CTRL_FECHAS WHERE FCH_SIST_PK = SYSDATE + 1";
		}
		try {
			 /**  Se ejecuta la operacion **/
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se valida el resultado de la operacion **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				/**  Se crea una lista para guardar el resultado de la operacion**/
				list = responseDTO.getResultQuery();
				/**  Se setean los valores obtenidos**/
				Map<String, Object> mapResult = list.get(0);
				fecha = utilerias.formateaFecha(mapResult.get("FCH_OP"),
						Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
				beanResObtenFechaCtrl.setFecha(fecha);
				/**  Se setea el codigo de error **/
				beanResObtenFechaCtrl.setCodError(Errores.OK00000V);
			}else{
				beanResObtenFechaCtrl.setCodError(Errores.ED00011V);
				beanResObtenFechaCtrl.setFecha("");
			}
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			  showException(e);
			  beanResObtenFechaCtrl.setCodError(Errores.EC00011B);
			  beanResObtenFechaCtrl.setMsgError(Errores.DESC_EC00011B);
		}
		 /**  Retorno del metodo **/
		return beanResObtenFechaCtrl;
	}

	/**
	 * Obtiene la fecha de operacion de la tabla TRAN_SPEI_CTRL .
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return fecha de operacion tipo String
	 */
	@Override
	public BeanResObtenFechaCtrlDAO obtenFechaSPEI(String cveInstitucion, String modulo,
			ArchitechSessionBean sessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResObtenFechaCtrlDAO beanResObtenFechaCtrlDAO = new BeanResObtenFechaCtrlDAO();
		String fecha = null;
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se asignan los valores a la lista **/
		parametros.add(cveInstitucion);
		/** Se declara una variable para evaluar la consulta **/
		String consultaEstatus = QUERY_CONS_FCH_SPEI;
		 /** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus = consultaEstatus.replaceAll("TRAN_SPEI_CTRL", "TRAN_SPID_CTRL");
		}
		try {
			/**  Se ejecuta la operacion **/
			responseDTO = HelperDAO.consultar(consultaEstatus, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se valida el resultado de la operacion **/
			if (responseDTO.getResultQuery() != null) {
				/**  Se crea una lista para guardar el resultado de la operacion**/
				list = responseDTO.getResultQuery();
				/**  Se setean los valores obtenidos**/
				Map<String, Object> mapResult = list.get(0);
				fecha = utilerias.formateaFecha(mapResult.get("FCH_OPERACION"),
						Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
				beanResObtenFechaCtrlDAO.setFecha(fecha);
				/**  Se setea el codigo de error **/
				beanResObtenFechaCtrlDAO.setCodError(Errores.OK00000V);
			}else{
				beanResObtenFechaCtrlDAO.setCodError(Errores.ED00011V);
			}
			
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResObtenFechaCtrlDAO.setCodError(Errores.EC00011B);
			beanResObtenFechaCtrlDAO.setMsgError(Errores.DESC_EC00011B);
		}
		 /**  Retorno del metodo **/
		return beanResObtenFechaCtrlDAO;
	}

	/**
	 * Obtiene el parametro de cifrado.
	 * @param cveInstitucion Clave de la institucion 
	 * @param sessionBean Datos de sesion
	 * @return BeanParamCifradoDAO Bean de respuesta para el parametro de cifrado
	 */
	@Override
	public BeanParamCifradoDAO obtenParametroCifrado(String cveInstitucion,
			ArchitechSessionBean sessionBean){
		/** Se declara el objeto de respuesta **/
		BeanParamCifradoDAO paramCifrado = new BeanParamCifradoDAO();
		String cifrado = null;
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se asignan los valores a la lista **/
		parametros.add(cveInstitucion);
		try {
			/**  Se ejecuta la operacion **/
			responseDTO = HelperDAO.consultar(QUERY_CONS_FIRMA, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se valida el resultado de la operacion **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				/**  Se setean los valores obtenidos**/
				Map<String, Object> mapResult = list.get(0);
				cifrado = mapResult.get("CIFRADO").toString();
				paramCifrado.setCifrado(cifrado);
				/**  Se setea el codigo de error **/
				paramCifrado.setCodError(Errores.OK00000V);
			}else{
				paramCifrado.setCodError(Errores.ED00011V);
			}
			
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			paramCifrado.setCodError(Errores.EC00011B);
		}		
		 /**  Retorno del metodo **/
		return paramCifrado;
	}

	/**
	 * Obten params POACOA.
	 *
	 * @param beanProcesosPOACOA Datos para la consulta.
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResPOACOA bean con los datos del resultado de la consulta
	 */
	@Override
	public BeanResPOACOA obtenParamsPOACOA(
			BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean sessionBean){
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se declara el objeto de respuesta **/
		BeanResPOACOA beanResPOACOA = new BeanResPOACOA();
		/** Se asignan los valores a la lista **/
		parametros.add(beanProcesosPOACOA.getFchOperacion());
		parametros.add(beanProcesosPOACOA.getCveInstitucion());
		/** Se declara una variable para evaluar la consulta **/
		String query = QUERY_CONS_GRAL;
		 /** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
        	query = query.replaceAll("TRAN_POA_COA_CTRL", "TRAN_POA_COA_SPID_CTRL");
        }
		try {
			/**  Se ejecuta la operacion **/
			responseDTO = HelperDAO.consultar(query, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se valida el resultado de la operacion **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				Map<String, Object> mapResult = list.get(0);
				beanResPOACOA.setFase(utilerias.getString(mapResult.get("FASE")));
				beanResPOACOA.setActivacion(utilerias.getString(mapResult.get("ACTIVACION")));
				beanResPOACOA.setNoHayRetorno(utilerias.getString(mapResult.get("NO_HAY_RETORNO")));
				beanResPOACOA.setGenerar(utilerias.getString(mapResult.get("GENERAR")));
				/**  Se setea el codigo de error **/
				beanResPOACOA.setCodError(Errores.OK00000V);
			}else{
				beanResPOACOA.setCodError(Errores.ED00011V);
			}
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResPOACOA.setCodError(Errores.EC00011B);
			beanResPOACOA.setMsgError(Errores.DESC_EC00011B);
		}
		 /**  Retorno del metodo **/
		return beanResPOACOA;
	}

	
	/**
	 * Actualiza generar.
	 *
	 * @param beanProcesosPOACOA Datos para la consulta.
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta del tipo BeanResBase
	 */
	@Override
	public BeanResBase actualizaGenerar(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se arma la consulta **/
		StringBuilder query = new StringBuilder(250);
		query.append("UPDATE TRAN_POA_COA_CTRL SET ");
		query.append("GENERAR = ? ");
		listaParametros.add(beanProcesosPOACOA.getGenerar());

			query.append(",FCH_GENERAR = SYSDATE ");
			query.append(",CVE_USUARIO_GEN = ? ");
			listaParametros.add(architectSessionBean.getUsuario());
		query.append(" WHERE FASE = ?  AND FCH_OPERACION = TO_DATE(?,'DD/MM/YYYY') AND ")
		.append("CVE_MI_INSTITUC = ? AND (ACTIVACION = ? OR ACTIVACION = ?)");
		/** Se asignan los valores a la lista **/
		listaParametros.add(beanProcesosPOACOA.getFase());
		listaParametros.add(beanProcesosPOACOA.getFchOperacion());
		listaParametros.add(beanProcesosPOACOA.getCveInstitucion());
		listaParametros.add(UNO);
		listaParametros.add("2");
		/** Se declara una variable para evaluar la consulta **/
		String queryEstatus = query.toString();
		 /** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			queryEstatus = queryEstatus.replaceAll("TRAN_POA_COA_CTRL", "TRAN_POA_COA_SPID_CTRL");
        }
		
		try {
			/**  Se ejecuta la operacion **/
			HelperDAO.actualizar(queryEstatus, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**  Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/**  Manejo de expceciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		 /**  Retorno del metodo **/
		return beanResBase;
	}

}
