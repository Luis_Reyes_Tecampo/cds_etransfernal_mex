/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConfiguracionCanalesImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanMedEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanalDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntregaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaCanales;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * @author ooamador
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConfiguracionCanalesImpl extends Architech implements DAOConfiguracionCanales {
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4064127308061283757L;

	/** ACTIVADO */
	private static final String ACTIVADO = "1";
	
	/** DESACTIVADO */
	private static final String DESACTIVADO = "0";
	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * los medios de entrega
	 */
	private static final String QUERY_CONSULTA_MEDIOS_ENTREGA = 
		"SELECT cve_medio_ent CVE, descripcion DES FROM comu_medios_ent";		
	
	/***
	 * Constante del tipo String del query para insertar el canal
	 */
	private static final String QUERY_INSERTAR_CANAL = 
		"INSERT INTO TRAN_CONTIG_CANALES (cve_medio_ent, activo) VALUES (?,?)";
	
	/***
	 * Constante del tipo String del query para eliminar el canal
	 */
	private static final String QUERY_ELIMINAR_CANAL = 
		"DELETE FROM TRAN_CONTIG_CANALES WHERE trim(cve_medio_ent) = ?";
	
	/***
	 * Constante del tipo String del query para actualizar el canal
	 */
	private static final String QUERY_ACTUALIZAR_CANAL = 
		"UPDATE TRAN_CONTIG_CANALES SET activo = ? WHERE trim(cve_medio_ent) = ?";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTAR_CANALES
	 */
	private static final String QUERY_CONSULTAR_CANALES = 
		" SELECT tcc.cve_medio_ent CVE, tcc.activo ACT, cme.descripcion DES "+
	    " FROM TRAN_CONTIG_CANALES  tcc, COMU_MEDIOS_ENT cme "+
	    " WHERE  TCC.CVE_MEDIO_ENT = CME.CVE_MEDIO_ENT ";

	
	  @Override
	   public BeanResMedEntregaDAO consultaMediosEntrega(ArchitechSessionBean architechSessionBean){
	      Utilerias utilerias = Utilerias.getUtilerias();
	      final BeanResMedEntregaDAO beanResMedEntregaDAO = new BeanResMedEntregaDAO();
	      List<BeanMedEntrega> listBeanMedEntrega = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	         responseDTO = HelperDAO.consultar(QUERY_CONSULTA_MEDIOS_ENTREGA, Collections.emptyList()
	         ,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	        	 listBeanMedEntrega = new ArrayList<BeanMedEntrega>();
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	BeanMedEntrega beanMedEntrega = new BeanMedEntrega();
					beanMedEntrega.setCveMedioEntrega(utilerias.getString(map.get("CVE")));
					beanMedEntrega.setNombreMedioEntrega(utilerias.getString(map.get("DES")));
					listBeanMedEntrega.add(beanMedEntrega);
	            }
	            beanResMedEntregaDAO.setBeanMedioEntregaList(listBeanMedEntrega);
		        beanResMedEntregaDAO.setCodError(Errores.OK00000V);
	         }else{
	        	 beanResMedEntregaDAO.setCodError(Errores.ED00011V);
	         }
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResMedEntregaDAO.setCodError(Errores.EC00011B);
	      }
	      return beanResMedEntregaDAO;
	   }

	@Override
	public BeanResCanalDAO consultarCanales(ArchitechSessionBean architectSessionBean){
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanResCanalDAO beanResCanalDAO = new BeanResCanalDAO();
		List<Object> listaParametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		List<BeanCanal> listBean = new ArrayList<BeanCanal>();
		try {
			responseDTO = HelperDAO.consultar(QUERY_CONSULTAR_CANALES,
					listaParametros, HelperDAO.CANAL_GFI_DS, this.getClass()
							.getName());
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> resultMap : list) {
					BeanCanal beanCanal = new BeanCanal();
					beanCanal.setIdCanal(utilerias.getString(resultMap.get("CVE")));
					beanCanal.setNombreCanal(utilerias.getString(resultMap.get("DES")));
					if(ACTIVADO.equals(utilerias.getString(resultMap.get("ACT")))){
						beanCanal.setEstatusCanal(true);
					} else {
						beanCanal.setEstatusCanal(false);
					}
					listBean.add(beanCanal);
				}
				beanResCanalDAO.setBeanResCanalList(listBean);
				beanResCanalDAO.setCodError(Errores.OK00000V);
			}else{
				beanResCanalDAO.setCodError(Errores.ED00011V);
	         }
		} catch (ExceptionDataAccess e) {
			 showException(e);
			 beanResCanalDAO.setCodError(Errores.EC00011B);
		}
		return beanResCanalDAO;
	}

	@Override
	public BeanResBase actualizarEstatusCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean){
		BeanResBase beanResBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();		
		if (reqCanal.isActivoCanal()) {
			listaParametros.add(ACTIVADO);
		} else {
			listaParametros.add(DESACTIVADO);
		}
		listaParametros.add(reqCanal.getIdCanal());
		try {
			HelperDAO.actualizar(QUERY_ACTUALIZAR_CANAL, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase; 
	}

	@Override
	public BeanResBase eliminarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase beanResBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();			
		listaParametros.add(reqCanal.getIdCanal());
		try {
			HelperDAO.eliminar(QUERY_ELIMINAR_CANAL, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase;
	}

	@Override
	public BeanResBase insertarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean){
		BeanResBase beanResBase = new BeanResBase();
		List<Object> listaParametros = new ArrayList<Object>();
		listaParametros.add(reqCanal.getIdCanal());
		if (reqCanal.isActivoCanal()) {
			listaParametros.add(ACTIVADO);
		} else {
			listaParametros.add(DESACTIVADO);
		}
		try {
			HelperDAO.insertar(QUERY_INSERTAR_CANAL, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
		}
		return beanResBase;
	}

	@Override
	public BeanResConsultaCanales consultarCanalesPag(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean){
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		List<Object> listaParametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		List<BeanCanal> listBean = new ArrayList<BeanCanal>();
		StringBuffer query = new StringBuffer(250);
		StringBuffer queryTotCanal = new StringBuffer(250);
		BeanResConsultaCanales resultCanales = new BeanResConsultaCanales();

		queryTotCanal.append("SELECT COUNT(*) CONT FROM TRAN_CONTIG_CANALES");
		
		query.append("SELECT ROWNUM RNUM, cve_medio_ent, activo, descripcion  FROM (")
		.append("SELECT ROWNUM RNUM, cve_medio_ent, activo, descripcion  FROM (")
		.append("SELECT tcc.cve_medio_ent , tcc.activo , cme.descripcion ")
		.append("FROM TRAN_CONTIG_CANALES  tcc, COMU_MEDIOS_ENT cme ")
		.append("WHERE  TCC.CVE_MEDIO_ENT = CME.CVE_MEDIO_ENT ");
		
		if (reqCanal.getIdCanal() != null && !reqCanal.getIdCanal().isEmpty()) {
			listaParametros.add(reqCanal.getIdCanal());
			query.append(" AND trim(TCC.CVE_MEDIO_ENT) = ?");
			queryTotCanal.append(" WHERE trim(CVE_MEDIO_ENT) = ?");
		}
		
		try {
			responseDTO = HelperDAO.consultar(queryTotCanal.toString(), listaParametros,
					  HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			if (responseDTO.getResultQuery() != null) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					resultCanales.setTotalReg(utilerias.getInteger(map
							.get("CONT")));
				}
			}
			
			if (resultCanales.getTotalReg() > 0) {
				query.append(" ORDER BY cme.descripcion ASC) ").append(
						"WHERE ROWNUM <= ?) WHERE RNUM >= ?");
				listaParametros.add(reqCanal.getPaginador().getRegFin());
				listaParametros.add(reqCanal.getPaginador().getRegIni());
				responseDTO = HelperDAO.consultar(query.toString(),
						listaParametros, HelperDAO.CANAL_GFI_DS, this.getClass()
								.getName());
				if (responseDTO.getResultQuery() != null
						&& !responseDTO.getResultQuery().isEmpty()) {
					list = responseDTO.getResultQuery();
					for (HashMap<String, Object> resultMap : list) {
						BeanCanal beanCanal = new BeanCanal();
						beanCanal.setIdCanal(resultMap.get("CVE_MEDIO_ENT")
								.toString().trim());
						beanCanal.setNombreCanal(resultMap.get("DESCRIPCION")
								.toString().trim());
						if (resultMap.get("ACTIVO").toString().trim()
								.equals(ACTIVADO)) {
							beanCanal.setEstatusCanal(true);
						} else {
							beanCanal.setEstatusCanal(false);
						}
						listBean.add(beanCanal);
					}
					resultCanales.setCodError(Errores.OK00000V);
					resultCanales.setListaBeanResCanal(listBean);
				}
				
			}else{
				resultCanales.setCodError(Errores.ED00011V);
				resultCanales.setListaBeanResCanal(listBean);
			}
			
		} catch (ExceptionDataAccess e) {
			showException(e);
			resultCanales.setCodError(Errores.EC00011B);
		}
		
		

		return resultCanales;
	}

}
