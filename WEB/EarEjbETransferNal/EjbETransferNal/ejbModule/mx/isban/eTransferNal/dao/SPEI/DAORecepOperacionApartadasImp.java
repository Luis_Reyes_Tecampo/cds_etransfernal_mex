/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORecepOperacionApartadasImp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   17/09/2018 01:46:24 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.dao.SPEI;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;

/**
 * Class DAORecepOperacionApartadasImp que muestra las operaciones apartadas por un usario
 *
 * @author FSW-Vector
 * @since 17/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperacionApartadasImp  extends Architech implements DAORecepOperacionApartadas{


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2265791574613722646L;
	
	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas util = UtileriasRecepcionOpConsultas.getInstance();
	/**
	 * Aparta operaciones.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param pantalla El objeto: pantalla
	 * @return Objeto bean res cons tran spei rec DAO
	 */
	@Override
	public BeanResTranSpeiRec  apartaOperaciones (BeanTranSpeiRecOper bean,
	    		ArchitechSessionBean sessionBean, boolean historico, String pantalla) {
		//inicializa variables		
		BeanResTranSpeiRec beanResTranSpeiR = new BeanResTranSpeiRec();
		// se iniciliza bean de error
		beanResTranSpeiR.setBeanError(new BeanResBase());
		String query= ConstantesRecepOperacionApartada.INSERT_OP_APARTADAS;
		
		
		/**pregunta hitorico para hacer el insert en la tabla correspondiente**/
		if(historico) {
				/**tabla apartados historicos**/
				query = ConstantesRecepOperacionApartada.INSERT_OP_APARTADAS_HTO;
			
		}
		
		 try{
			//se crea elobjeto para pasar los parametros
			List<Object> parametros = new ArrayList<Object>();
			//se asigna los datos para el parametro
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion().trim());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion().trim());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd().trim());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete().trim());
			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago().trim());
			parametros.add(sessionBean.getUsuario());
				
			/** busca las operaciones apartadas**/	 
			 HelperDAO.insertar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			 
			 /**Asigna el error que exista **/
			 beanResTranSpeiR.getBeanError().setCodError(Errores.CMCO019V);
			 beanResTranSpeiR.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			
			 
		 }catch (ExceptionDataAccess e) {
	        	/**en caso de error **/
	           showException(e);
	           beanResTranSpeiR.getBeanError().setCodError(Errores.EC00011B);
	           beanResTranSpeiR.getBeanError().setMsgError(Errores.DESC_EC00011B);
	           beanResTranSpeiR.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
	     }
		return beanResTranSpeiR;
		 
	 }
	
	
	/**
	 * Existe operacion.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res base
	 */
	@Override
	public BeanResBase existeOperacion(BeanTranSpeiRecOper bean,
    		ArchitechSessionBean sessionBean, boolean historico)  {
		ResponseMessageDataBaseDTO responseDTO = null;
		
		BeanResBase response = new BeanResBase();
	
		String query ="";
		if(historico) {
			query = ConstantesRecepOperacionApartada.QUERY_EXITE_OPERA_APARTADA_HTO;
		}else {			
			query = ConstantesRecepOperacionApartada.QUERY_EXITE_OPERA_APARTADA_DIA;
		}
		
    	/**Se realiza la consulta**/
    		try {      			
    			//se crea elobjeto para pasar los parametros
    			List<Object> parametros = new ArrayList<Object>();
    			//se asigna los datos para el parametro
    			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
    			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
    			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
    			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
    			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
    			parametros.add(sessionBean.getUsuario());
    			
    			/** busca las operaciones existentes**/	
				responseDTO =  HelperDAO.consultar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    		 //se asigna el resultado
	    		 response= util.obtenerExiste(responseDTO);
    			
    			/**Si ahy algun error se cacha e el tray**/
    		} catch (ExceptionDataAccess e) {
    			showException(e);
	 		    response.setCodError(Errores.EC00011B);
	 		    response.setTipoError(Errores.TIPO_MSJ_ERROR);			
			}
	    /**Se regresa el resultado**/
	    return response;	
	}
	
	/**
	 * Aparta operaciones eliminar.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param pantalla El objeto: pantalla
	 * @return Objeto bean res cons tran spei rec
	 */
	@Override
	public BeanResTranSpeiRec  apartaOperacionesEliminar (BeanTranSpeiRecOper bean,
	    		ArchitechSessionBean sessionBean, boolean historico, String pantalla) {
		//inicializa variables		
		BeanResTranSpeiRec beanResTranSpeiRec = new BeanResTranSpeiRec();
		// se iniciliza bean de error
		beanResTranSpeiRec.setBeanError(new BeanResBase());
		String query="";
		
		
		/**pregunta hitorico para hacer el insert en la tabla correspondiente**/
		if(historico) {
			/**tabla apartados historicos**/
			query = ConstantesRecepOperacionApartada.QUERY_ELIMINAR_OPERA_APARTADA_HTO;
		}else {		
			/**tabla apartados del dia**/
			query = ConstantesRecepOperacionApartada.QUERY_ELIMINAR_OPERA_APARTADA_DIA;
		}
		
		 try{
				//se crea elobjeto para pasar los parametros
	 			List<Object> parametros = new ArrayList<Object>();
	 			//se asigna los datos para el parametro
	 			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
	 			parametros.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
	 			parametros.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
	 			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
	 			parametros.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
	 			parametros.add(bean.getUsuario());
	 			//elimina el registro
				 HelperDAO.eliminar(query,parametros,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				 //asigna codigo de exito
				 beanResTranSpeiRec.getBeanError().setCodError(Errores.CMCO019V);
				 beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			 
			 
		 }catch (ExceptionDataAccess e) {
	         /**en caso de error **/
	           showException(e);
	         //asigna codigo de error
	           beanResTranSpeiRec.getBeanError().setCodError(Errores.EC00011B);
	           beanResTranSpeiRec.getBeanError().setMsgError(Errores.DESC_EC00011B);
	           beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
	     }
		return beanResTranSpeiRec;
		 
	 }
	
	
	
	
}