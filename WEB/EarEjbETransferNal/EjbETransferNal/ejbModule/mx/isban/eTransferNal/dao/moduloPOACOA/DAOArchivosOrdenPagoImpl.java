/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOArchivosRespuestaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqArchPago;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanalesDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Class DAOArchivosOrdenPagoImpl.
 *
 * @author ooamador
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOArchivosOrdenPagoImpl extends Architech implements DAOArchivosOrdenPago {
	
	/**
	 * Serial Number
	 */
	private static final long serialVersionUID = 1L;
	
	/** Query que guarda el nombre del archivo. */
	private static final String QUERY_INSERT_NOMBRA_PROC = 
		"INSERT INTO TRAN_ARCH_CANALES (NOMBRE_ARCH, ESTATUS, CVE_USUARIO_ALTA, FCH_CAPTURA) VALUES (?,?,?,TO_DATE(?,'DD-MM-YYYY'))";
	
	/** Query que consulta la duplcidad del nombre del archivo. */
	private static final String QUERY_CONSULTA_DUPLICIDAD = 
		"SELECT * FROM TRAN_ARCH_CANALES WHERE TRIM(NOMBRE_ARCH) = ? ";	
	
	/***
	 * Constante del tipo String del query para actualizar el nombre de archivo
	 */
	private static final String QUERY_ACTUALIZAR_NOMBRE = 
		"UPDATE TRAN_ARCH_CANALES SET  ESTATUS = ?, FCH_CAPTURA = TO_DATE(?,'DD-MM-YYYY'), CVE_USUARIO_ALTA = ? " +
		"WHERE TRIM(NOMBRE_ARCH) = ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_CANAL
	 */
	private static final String QUERY_CONSULTA_CANAL = "SELECT tcc.cve_medio_ent CVE, tcc.activo ACT, cme.descripcion DES "+
	   " FROM TRAN_CONTIG_CANALES  tcc, COMU_MEDIOS_ENT cme "+
	   " WHERE  TCC.CVE_MEDIO_ENT = CME.CVE_MEDIO_ENT AND TCC.ACTIVO = 1";

	
	/**
	 * Inserta el nombre del archivo en Base de Datos.
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	@Override
	public BeanResBase registarNombreArchivo(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta  **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se declara la lista que contendra los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los parametros a la lista **/
		listaParametros.add(beanReqArchPago.getNombreArchivo());
		listaParametros.add(0);
		listaParametros.add(architectSessionBean.getUsuario());
		listaParametros.add(beanReqArchPago.getFchOperacion());
		try {
			/** Ejecucion de la operacion IDA para insertar **/
			HelperDAO.insertar(QUERY_INSERT_NOMBRA_PROC, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se asigna codigo de error al objeto de respuesta **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/** Manejo de las excepciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno del metodo **/
		return beanResBase;
	}
	
	/**
	 * Consulta nombre duplicado
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO Bean de respuesta
	 */
	@Override
	public BeanResDuplicadoArchDAO consultaDuplicidadNombre(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO = new BeanResDuplicadoArchDAO();
		/** Se inicializa el valor como falso**/
		beanResDuplicadoArchDAO.setEsDuplicado(Boolean.FALSE);
		/** Se declara el objeto para poder realizar las instancias al IDA **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se declara una lista que contendra los paramtros SQL **/
		List<Object> parametros = new ArrayList<Object>();
		/** Se asignan los aprametros a la lista **/
		parametros.add(beanReqArchPago.getNombreArchivo());
		try {	
			/** Se ejecuta la operacion de consulta **/
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_DUPLICIDAD,
					parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se valida el resultado de la operacion **/
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				/** Se asigna verdadero si se cumple esta condicion para despues validarlo en el BO **/
				beanResDuplicadoArchDAO.setEsDuplicado(Boolean.TRUE);
			}
			/** Se asigna los codigos de la operacion**/
			beanResDuplicadoArchDAO.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResDuplicadoArchDAO.setCodError(Errores.EC00011B);
			beanResDuplicadoArchDAO.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno del metodo **/
		return beanResDuplicadoArchDAO;
	}
	
	/**
	 * Actualiza el nombre del archivo en Base de Datos.
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	@Override
	public BeanResBase actualizarNombreArchivo(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se declara una lista que contendra los paramtros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los aprametros a la lista **/
		listaParametros.add(0);
		listaParametros.add(beanReqArchPago.getFchOperacion());
		listaParametros.add(architectSessionBean.getUsuario());
		listaParametros.add(beanReqArchPago.getNombreArchivo());
		try {
			/** Se ejecuta la operacion  **/
			HelperDAO.actualizar(QUERY_ACTUALIZAR_NOMBRE, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se asigna los codigos de la operacion**/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno del metodo **/
		return beanResBase;
	}

	/**
	 * Consulta en BD los medios de entrega.
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanalesDAO bean de respuesta con los canales
	 */
	@Override
	public  BeanResConsCanalesDAO consultarCanales(
			ArchitechSessionBean architectSessionBean){
		/** Se declaran los objetos de respuesta **/
		ResponseMessageDataBaseDTO responseDTO = null;		
		
		BeanResConsCanalesDAO beanResConsCanalesDAO = new BeanResConsCanalesDAO();
		/** Se declara una lista que contendra los paramtros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		List<HashMap<String, Object>> list = null;
		List<BeanCanal> listBean = new ArrayList<BeanCanal>();
		
		try {
			/** Se ejecuta la operacion de consulta **/
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CANAL,
					listaParametros, HelperDAO.CANAL_GFI_DS, this.getClass()
							.getName());
			/** Se valida el resultado de la consulta **/
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				/** Se asigna el resultado de la consulta a una variable de lista**/
				list = responseDTO.getResultQuery();
				/** Se recorre la lista**/
				for (HashMap<String, Object> resultMap : list) {
					/** Se declara un bean para guardar el registro actual**/
					BeanCanal beanResCanal = new BeanCanal();
					/** Se obtienen los valores **/
					beanResCanal.setIdCanal(resultMap.get("CVE")
							.toString().trim());
					beanResCanal.setNombreCanal(resultMap.get("DES")
							.toString().trim());		
					/** Se asigna el bean actual a la lista **/
					listBean.add(beanResCanal);
				}
				/** Se asigna la lista al bean de respuesta **/
				beanResConsCanalesDAO.setBeanResCanalList(listBean);
				/** Se seta el codigo de error **/
				beanResConsCanalesDAO.setCodError(Errores.OK00000V);	
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResConsCanalesDAO.setCodError(Errores.EC00011B);
			beanResConsCanalesDAO.setMsgError(Errores.DESC_EC00011B);
		}

		/** Retorno del metodo **/
		return beanResConsCanalesDAO;
	}


}
