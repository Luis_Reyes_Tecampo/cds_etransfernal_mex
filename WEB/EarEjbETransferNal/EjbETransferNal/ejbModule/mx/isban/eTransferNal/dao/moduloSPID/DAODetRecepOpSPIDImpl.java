/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAODetRecepOpSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAODetRecepOpSPIDImpl extends Architech implements DAODetRecepOpSPID{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -580824552308046906L;
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_UPDATE_SPID_REC
	 */
	private static final String QUERY_UPDATE_SPID_REC =  
	" UPDATE TRAN_SPID_REC SET  "+
    " topologia = ?,enviar = ?, cda = ?, "+
    " estatus_banxico = ?, estatus_transfer = ?,  motivo_devol = ?, "+
    " tipo_cuenta_ord = ?, tipo_cuenta_rec = ?, clasif_operacion = ?, "+
    " tipo_operacion = ?, cve_interme_ord = ?, cod_postal_ord = ?, "+
    " codigo_error = ?, fch_constit_ord = ?, fch_instruc_pago = ?, "+
    " hora_instruc_pago = ?, fch_acept_pago = ?, hora_acept_pago = ?, "+
    " rfc_ord = ?, rfc_rec = ?, refe_numerica = ?, "+
    " num_cuenta_ord = ?, num_cuenta_rec = ?, num_cuenta_tran = ?, "+
    " tipo_pago = ?, prioridad = ?, long_rastreo = ?, "+
    " folio_paq_dev = ?, folio_pago_dev = ?, refe_transfer = ?, "+
    " monto = ?, fch_captura = to_date(?,'dd-mm-yyyy HH24:mi:ss'), cve_rastreo = ?, "+
    " cve_rastreo_ori = ?, direccion_ip = ?, cde = ?, "+
    " nombre_ord = ?, nombre_rec = ?, domicilio_ord = ?,  "+
    " concepto_pago = ? "+     
    " WHERE fch_operacion = to_date(?,'dd-mm-yyyy') AND "+ 
    " 	cve_mi_instituc = ? AND "+ 
    " 	cve_inst_ord = ? AND "+ 
    "	folio_paquete = ? AND "+ 
    "	folio_pago = ? ";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_CAMPOS
	 */
	private static final String QUERY_CONSULTA_CAMPOS =
		" SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "+
		" R.folio_paquete,R.folio_pago,R.topologia, "+
		" R.enviar,R.cda,R.estatus_banxico, "+
		" R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "+
		" R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "+
		" R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "+
		" R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "+
		" R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, "+
		" R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "+
		" R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, "+
		" R.prioridad,R.long_rastreo,R.folio_paq_dev, "+
		" R.folio_pago_dev,R.refe_transfer,R.monto, "+
		" TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "+
		" R.direccion_ip,R.cde,R.nombre_ord, "+        
		" R.nombre_rec,R.domicilio_ord,R.concepto_pago, "+     
       " ROWNUM INDICE "+
       " FROM TRAN_SPID_REC R WHERE R.CVE_MI_INSTITUC = ? "+
       " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
       " AND R.cve_inst_ord = ? "+ 
       " AND R.folio_paquete = ? "+ 
       " AND R.folio_pago = ? ";
	
	
	  @Override
	  public BeanResActTranSpeiRecDAO actualizaDetTransSpeiRec(BeanReceptoresSPID beanReceptoresSPID,
	   ArchitechSessionBean architechSessionBean){
	    	BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = new BeanResActTranSpeiRecDAO();
	    	Utilerias util = Utilerias.getUtilerias();
	       try{
	          HelperDAO.actualizar(QUERY_UPDATE_SPID_REC, 
			   Arrays.asList(new Object[]{
					   util.getString(beanReceptoresSPID.getDatGenerales().getTopologia()),
					   util.getString(beanReceptoresSPID.getDatGenerales().getEnviar()),
					   util.getString(beanReceptoresSPID.getDatGenerales().getCda()),
					   util.getString(beanReceptoresSPID.getEstatus().getEstatusBanxico()),
					   util.getString(beanReceptoresSPID.getEstatus().getEstatusTransfer()),
					   util.getString(beanReceptoresSPID.getEnvDev().getMotivoDev()),
					   util.getString(beanReceptoresSPID.getBancoOrd().getTipoCuentaOrd()),
					   util.getString(beanReceptoresSPID.getBancoRec().getTipoCuentaRec()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getClasifOperacion()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getTipoOperacion()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getCveIntermeOrd()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getCodPostalOrd()),
	                   util.getString(beanReceptoresSPID.getEstatus().getCodigoError()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getFchConstitOrd()),
	                   util.getString(beanReceptoresSPID.getFchPagos().getFchInstrucPago()),
	                   util.getString(beanReceptoresSPID.getFchPagos().getHoraInstrucPago()),
	                   util.getString(beanReceptoresSPID.getFchPagos().getFchAceptPago()),
	                   util.getString(beanReceptoresSPID.getFchPagos().getHoraAceptPago()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getRfcOrd()),
	                   util.getString(beanReceptoresSPID.getBancoRec().getRfcRec()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getRefeNumerica()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getNumCuentaOrd()),
	                   util.getString(beanReceptoresSPID.getBancoRec().getNumCuentaRec()),
	                   util.getString(beanReceptoresSPID.getBancoRec().getNumCuentaTran()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getTipoPago()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getPrioridad()),
	                   util.getString(beanReceptoresSPID.getRastreo().getLongRastreo()),
	                   util.getString(beanReceptoresSPID.getEnvDev().getFolioPaqDev()),
	                   util.getString(beanReceptoresSPID.getEnvDev().getFolioPagoDev()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getRefeTransfer()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getMonto()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getFchCaptura()),
	                   util.getString(beanReceptoresSPID.getRastreo().getCveRastreo()),
	                   util.getString(beanReceptoresSPID.getRastreo().getCveRastreoOri()),
	                   util.getString(beanReceptoresSPID.getRastreo().getDireccionIp()),
	                   util.getString(beanReceptoresSPID.getDatGenerales().getCde()),
	                   util.getString(beanReceptoresSPID.getBancoOrd().getNombreOrd()),
					   util.getString(beanReceptoresSPID.getBancoRec().getNombreRec()),
					   util.getString(beanReceptoresSPID.getBancoOrd().getDomicilioOrd()),
					   util.getString(beanReceptoresSPID.getDatGenerales().getConceptoPago()),
					   util.getString(beanReceptoresSPID.getLlave().getFchOperacion()),
					   util.getString(beanReceptoresSPID.getLlave().getCveMiInstituc()),
					   util.getString(beanReceptoresSPID.getLlave().getCveInstOrd()),
					   util.getString(beanReceptoresSPID.getLlave().getFolioPaquete()),
					   util.getString(beanReceptoresSPID.getLlave().getFolioPago())
			   }), 
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	          beanResActTranSpeiRecDAO.setCodError(Errores.OK00000V);
	       } catch (ExceptionDataAccess e) {
	          showException(e);
	          beanResActTranSpeiRecDAO.setCodError(Errores.EC00011B);
	       }
	       return beanResActTranSpeiRecDAO;
	    }
	  
		@Override
	    public BeanResReceptoresSPIDDAO consultaTransferencia(List<Object> parametros, ArchitechSessionBean sessionBean) {
	    	BeanReceptoresSPID beanReceptoresSPID = null;
	    	List<HashMap<String,Object>> list = null;
	    	ResponseMessageDataBaseDTO responseDTO = null;
	    	BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO = new BeanResReceptoresSPIDDAO();
		 	try {
				responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CAMPOS,parametros, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	  list = responseDTO.getResultQuery();
	      	  
	      	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	      		  for(HashMap<String,Object> map:list){
	      			 beanReceptoresSPID =  descomponeConsulta(map); 	
	      			 beanResReceptoresSPIDDAO.setBeanReceptoresSPID(beanReceptoresSPID);
	      		  }  
	      	  }
	      	beanResReceptoresSPIDDAO.setCodError(Errores.OK00000V);
	      	beanResReceptoresSPIDDAO.setMsgError(responseDTO.getMessageError());
			} catch (ExceptionDataAccess e) {
				showException(e);
		  		beanResReceptoresSPIDDAO.setCodError(Errores.EC00011B);
			}
			return beanResReceptoresSPIDDAO;
	    }
	    
	    /**
	     * Metodo que genera un objeto del tipo BeanReceptoresSPID a partir de la respuesta de la consulta
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros 
	     * @return BeanReceptoresSPID Objeto del tipo BeanReceptoresSPID
	     */
	    private BeanReceptoresSPID descomponeConsulta(Map<String,Object> map){
	    	BeanReceptoresSPID beanReceptoresSPID = new BeanReceptoresSPID();
	    	beanReceptoresSPID.setLlave(seteaLlave(map));
	    	beanReceptoresSPID.setDatGenerales(seteaDatosGenerales(map));
	    	beanReceptoresSPID.setEstatus(seteaEstatus(map));
	    	beanReceptoresSPID.setBancoOrd(seteaBancoOrd(map));
	    	beanReceptoresSPID.setBancoRec(seteaBancoRec(map));
	    	beanReceptoresSPID.setEnvDev(seteaEnvDev(map));
	    	beanReceptoresSPID.setRastreo(seteaRastreo(map));
	    	beanReceptoresSPID.setFchPagos(seteaFchPago(map));
	    	
	    	return beanReceptoresSPID;
	    }
	    
	    /**
	     * Metodo que setea el folio paquete y pago de devolucion
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDEnvDev Bean de respuesta 
	     */
	    private BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
	    	beanSPIDEnvDev.setMotivoDev(utilerias.getString(map.get("MOTIVO_DEVOL")));
	    	beanSPIDEnvDev.setFolioPaqDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")));
	    	beanSPIDEnvDev.setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")));
	    	
	    	return beanSPIDEnvDev;
	    }
	    
	    /**
	     * Metodo que setea los campos llave
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDLlave Bean de respuesta 
	     */
	    private BeanSPIDLlave seteaLlave(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
	    	beanSPIDLlave.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
	    	beanSPIDLlave.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
	    	beanSPIDLlave.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
	    	beanSPIDLlave.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
	    	beanSPIDLlave.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
	    	return beanSPIDLlave;
	    }
	    
	    /**
	     * Metodo que setea los campos generales
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDDatGenerales Bean de respuesta de datos generales
	     */
	    private BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
	    	BigDecimal monto = utilerias.getBigDecimal(map.get("MONTO"));
	    	beanSPIDDatGenerales.setTopologia(utilerias.getString(map.get("TOPOLOGIA")));
	    	beanSPIDDatGenerales.setPrioridad(utilerias.getString(map.get("PRIORIDAD")));
	    	beanSPIDDatGenerales.setTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
	    	beanSPIDDatGenerales.setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")));
	    	beanSPIDDatGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
	    	beanSPIDDatGenerales.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
	    	beanSPIDDatGenerales.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
	    	beanSPIDDatGenerales.setCde(utilerias.getString(map.get("CDE")));
	    	beanSPIDDatGenerales.setCda(utilerias.getString(map.get("CDA")));
	    	beanSPIDDatGenerales.setEnviar(utilerias.getString(map.get("ENVIAR")));
	    	beanSPIDDatGenerales.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
	    	
	    	beanSPIDDatGenerales.setMonto(monto);
	    	return beanSPIDDatGenerales;
	    }
	    
	    /**
	     * Metodo que setea los campos generales
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDEstatus Bean de respuesta de estatus
	     */
	    private BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
	    	beanSPIDEstatus.setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")));
	    	beanSPIDEstatus.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
	    	beanSPIDEstatus.setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));
	    	return beanSPIDEstatus;
	    }
	    /**
	     * Metodo que setea los campos del banco ordenante
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante
	     */
	    private BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
	    	beanSPIDBancoOrd.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
	    	beanSPIDBancoOrd.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
	    	beanSPIDBancoOrd.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
	    	beanSPIDBancoOrd.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
	    	beanSPIDBancoOrd.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
	    	beanSPIDBancoOrd.setDomicilioOrd(utilerias.getString(map.get("DOMICILIO_ORD")));
	    	beanSPIDBancoOrd.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
	    	beanSPIDBancoOrd.setCodPostalOrd(utilerias.getString(map.get("COD_POSTAL_ORD")));
	    	beanSPIDBancoOrd.setFchConstitOrd(utilerias.getString(map.get("FCH_CONSTIT_ORD")));
	    	
	    	return beanSPIDBancoOrd;
	    }
	    
	    /**
	     * Metodo que setea los campos de rastreo
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
	     */
	    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
	    	beanSPIDRastreo.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
	    	beanSPIDRastreo.setCveRastreoOri(utilerias.getString(map.get("CVE_RASTREO_ORI")));
	    	beanSPIDRastreo.setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")));
	    	beanSPIDRastreo.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
	    	return beanSPIDRastreo;
	    }
	    
	    /**
	     * Metodo que setea los campos del Banco Receptor
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
	     */
	    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
	    	beanSPIDBancoRec.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
	    	beanSPIDBancoRec.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
	    	beanSPIDBancoRec.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
	    	beanSPIDBancoRec.setNumCuentaTran(utilerias.getString(map.get("NUM_CUENTA_TRAN")));
	    	beanSPIDBancoRec.setRfcRec(utilerias.getString(map.get("RFC_REC")));
	    	
	    	return beanSPIDBancoRec;
	    }
	    
	    /**
	     * Metodo que setea los campos de las fechas de pago
	     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
	     * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
	     */
	    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
	    	beanSPIDFchPagos.setFchAceptPago(utilerias.getString(map.get("FCH_ACEPT_PAGO")));
	    	beanSPIDFchPagos.setHoraAceptPago(utilerias.getString(map.get("HORA_ACEPT_PAGO")));
	    	beanSPIDFchPagos.setFchInstrucPago(utilerias.getString(map.get("FCH_INSTRUC_PAGO")));
	    	beanSPIDFchPagos.setHoraInstrucPago(utilerias.getString(map.get("HORA_INSTRUC_PAGO")));
	    	return beanSPIDFchPagos;
	    }
}
