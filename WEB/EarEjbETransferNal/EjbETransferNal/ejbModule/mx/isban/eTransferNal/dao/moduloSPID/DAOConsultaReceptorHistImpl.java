/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaReceptorHistImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaReceptorHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHistDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsReceptHistSum;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Session Bean implementation class DAOConsultaReceptorHistImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaReceptorHistImpl extends Architech implements DAOConsultaReceptorHist{

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 1030122471388179487L;

	private static final String AND_FECHA_OPERACION = "AND T.FCH_OPERACION = TO_DATE(?, 'DD-MM-YYYY')";

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros todas
	 */
	public static final StringBuilder CONSULTA_RECEPTOS_HIST =
			new StringBuilder().append("SELECT T.* FROM (SELECT T.TIPO_PAGO,")
			   .append("T.TOPOLOGIA, T.ESTATUS_BANXICO, T.ESTATUS_TRANSFER, T.CVE_MI_INSTITUC,")
			   .append("TO_CHAR(T.FCH_OPERACION,'DD/MM/YYYY') FCH_OPERACION, TO_CHAR(T.FCH_CAPTURA,'HH24:MI') HORA, T.CVE_INST_ORD,")
		   	   .append("T.CVE_INTERME_ORD, T.FOLIO_PAQUETE, T.FOLIO_PAGO,")
		   	   .append("T.MONTO,T.CODIGO_ERROR,T.MOTIVO_DEVOL, D.DESCRIPCION AS MOT_DEV_DESC, B.CVE_USUARIO, C.CONT, ROWNUM R ");

	/**
	 *
	 */
	private static final String  LEFT_JOIN_COMU_DETALLE_CD= " LEFT OUTER JOIN COMU_DETALLE_CD D ON D.CODIGO_GLOBAL = T.MOTIVO_DEVOL AND D.TIPO_GLOBAL = 'TR_DEVSPID' ";

	/**
	 *
	 */
	private static final String FROM_TRAN_SPID_REC_HIS = "FROM TRAN_SPID_REC_HIS T LEFT JOIN TRAN_SPID_BLOQUEO B ON " +
			" T.FCH_OPERACION = B.FCH_OPERACION "+
	        " AND T.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
	        " AND T.CVE_INST_ORD = B.CVE_INST_ORD "+
	        " AND T.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
	        " AND T.FOLIO_PAGO = B.FOLIO_PAGO ";

	/**
	 *
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_TODAS =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
					   	   	   .append(FROM_TRAN_SPID_REC_HIS)
					   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
					   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE ")
					   	   	   .append("T.FCH_OPERACION = TO_DATE(?, 'DD-MM-YYYY')")
					   	   	   .append(" ) C WHERE ")
					   	   	   .append("T.FCH_OPERACION = TO_DATE(?, 'DD-MM-YYYY')").append(" ) T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros por liq
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_PORLIQ =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='PL' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='PL' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros devolucion
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_DEVOL =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE ESTATUS_TRANSFER='DV' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE ESTATUS_TRANSFER='DV' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros v liq
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_VLIQ =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE TOPOLOGIA='V' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE TOPOLOGIA='V' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros rechazos
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_RECH =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE ESTATUS_TRANSFER='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE ESTATUS_TRANSFER='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros rech cuenta
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_RECHCTA =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE ESTATUS_TRANSFER='RE' AND LENGTH(MOTIVO_DEVOL)>0 ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE ESTATUS_TRANSFER='RE' AND LENGTH(MOTIVO_DEVOL)>0 ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros t liq
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_TLIQ =
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString())
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");
	
	
	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros apl his
	 */
	public static final String  LEFT_JOIN_TRAN_SPID_HORAS_HIS = " LEFT JOIN TRAN_SPID_HORAS_HIS H ON (T.FCH_OPERACION = H.FCH_OPERACION AND "+
		    													" T.CVE_MI_INSTITUC =  H.CVE_INST_BENEF AND T.CVE_INST_ORD  =  H.CVE_INST_ORD AND " +
		    													" T.FOLIO_PAQUETE   =  H.FOLIO_PAQUETE  AND T.FOLIO_PAGO    =   H.FOLIO_PAGO" +
		    													" AND H.TIPO = 'R' )";

	/***
	 * Constante del tipo String que almacena la consulta para obtener
	 * registros apl
	 */
	public static final StringBuilder CONSULTA_RECEPTOR_HIST_APL = 
			new StringBuilder().append(CONSULTA_RECEPTOS_HIST.toString() + ",TO_CHAR(H.FCH_AVISO_ABONO,'DD/MM/YYYY HH24:MI:SS') FECHA_CAPTURA ")
	   	   	   .append(FROM_TRAN_SPID_REC_HIS)
	   	   	   .append(LEFT_JOIN_COMU_DETALLE_CD)
	   	   	   .append(LEFT_JOIN_TRAN_SPID_HORAS_HIS)
	   	   	   .append(", (SELECT COUNT(*) CONT FROM TRAN_SPID_REC_HIS T WHERE ESTATUS_TRANSFER='TR' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") C WHERE ESTATUS_TRANSFER='TR' ")
					   	   	   .append(AND_FECHA_OPERACION)
					   	   	   .append(") T WHERE R BETWEEN ? AND ?  ");

	/**Metodo que sirve para consultar los receptores historicos
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BeanConsultaReceptorHistDAO Objeto del tipo BeanConsultaReceptorHistDAO
	   */
	@Override
	public BeanConsultaReceptorHistDAO obtenerConsultaReceptorHist(BeanPaginador beanPaginador,BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBeans) {
		String query = null;
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanConsRecepHist> listaBeanReceptorHist = null;
    	final BeanConsultaReceptorHistDAO beanConsultaReceptorHistDAO = new BeanConsultaReceptorHistDAO();

    	try {
    		query = obtenerConsulta(beanReqConsReceptHist);

        	responseDTO = HelperDAO.consultar(query, Arrays.asList(
        			new Object[] { beanReqConsReceptHist.getFecha(),
        					beanReqConsReceptHist.getFecha(),
        					beanPaginador.getRegIni(),
 						   	beanPaginador.getRegFin() }),
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 listaBeanReceptorHist = llenarReceptorHistorico(list, utilerias, beanConsultaReceptorHistDAO);
        		 }
        	}
        	beanConsultaReceptorHistDAO.setListaDatos(listaBeanReceptorHist);
        	beanConsultaReceptorHistDAO.setCodError(responseDTO.getCodeError());
        	beanConsultaReceptorHistDAO.setMsgError(responseDTO.getMessageError());
    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return beanConsultaReceptorHistDAO;
	}

	/**Metodo que sirve para consultar los receptores historicos
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return beanConsultaReceptorHistDAO Objeto del tipo BeanConsultaReceptorHistDAO
	   */
	@Override
	public BeanConsReceptHistSum obtenerSumatoriaHist(BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBeans) {
		List<HashMap<String,Object>> list = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	String queryWhere = "";
    	String whereFecha= "";
    	final BeanConsReceptHistSum sumatoria = new BeanConsReceptHistSum();

    	try {
    		StringBuilder query = new StringBuilder().append("SELECT SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' THEN MONTO ELSE 0 END) MONTO_TOPO_V_LIQUIDADAS,")
				.append("SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_V_LIQUIDADAS,")
				.append("SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' AND ESTATUS_BANXICO='LQ' THEN MONTO ELSE 0 END) MONTO_TOPO_T_LIQUIDADAS,")
				.append("SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' AND ESTATUS_BANXICO='LQ' THEN 1 ELSE 0 END) VOLUMEN_TOPO_T_LIQUIDADAS,")
				.append("SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' AND ESTATUS_BANXICO='PL' THEN MONTO ELSE 0 END) MONTO_TOPO_T_POR_LIQUIDAR,")
				.append("SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE' AND ESTATUS_BANXICO='PL' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_T_POR_LIQUIDAR,")
				.append("SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN MONTO ELSE 0 END) MONTO_RECHAZOS,")
				.append("SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN 1 ELSE 0 END) VOLUMEN_RECHAZOS ")
				.append("FROM TRAN_SPID_REC_HIS ");

    		queryWhere = obtenerSumatoriaConsulta(beanReqConsReceptHist);
	  	    query.append(queryWhere);
	  		if("".equals(queryWhere)){
	  			whereFecha = " WHERE ";
	  		}else{
	  			whereFecha = " AND ";
	  		}
	  		query.append(whereFecha);
	  		query.append(" FCH_OPERACION = TO_DATE('"+beanReqConsReceptHist.getFecha() +"', 'dd/mm/yyyy')");

        	responseDTO = HelperDAO.consultar(query.toString(), Arrays.asList(
        			new Object[] { }),
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 llenarSumatoria(list, sumatoria);
        		 }
        	}

    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return sumatoria;
	}

	/**Metodo que sirve para consultar los receptores historicos
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	@Override
	public BigDecimal obtenerImporteTotalHist(BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBeans) {
		List<HashMap<String,Object>> list = null;
	  	ResponseMessageDataBaseDTO responseDTO = null;
	  	String queryWhere= "";
	  	String whereFecha= "";
	  	Utilerias utilerias = Utilerias.getUtilerias();
	  	BigDecimal totalImporte = BigDecimal.ZERO;

	  	try {
	  		StringBuilder query = new StringBuilder().append("SELECT SUM(MONTO) MONTO_TOTAL ")
	  			.append("FROM TRAN_SPID_REC_HIS ");
	  		queryWhere = obtenerSumatoriaConsulta(beanReqConsReceptHist);
	  	    query.append(queryWhere);
	  		if("".equals(queryWhere)){
	  			whereFecha = " WHERE ";
	  		}else{
	  			whereFecha = " AND ";
	  		}
	  		query.append(whereFecha);
	  		query.append(" FCH_OPERACION = TO_DATE('"+beanReqConsReceptHist.getFecha() +"', 'dd/mm/yyyy')");

	      	responseDTO = HelperDAO.consultar(query.toString(), Arrays.asList(
	      			new Object[] { }),
	        			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
	      	if(responseDTO.getResultQuery()!= null){
	      		 list = responseDTO.getResultQuery();
	      		 if(!list.isEmpty()){
	      			for(Map<String,Object> mapResult : list){
	      				totalImporte = totalImporte.add(new BigDecimal(utilerias.getString(mapResult.get("MONTO_TOTAL"))));
	      			 }
	      		 }
	      	}

	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  	}
	  	return totalImporte;
	}

	/**
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param sumatoria Objeto del tipo BeanConsReceptHistSum
	 */
	protected void llenarSumatoria(List<HashMap<String, Object>> list,
			final BeanConsReceptHistSum sumatoria) {
		Utilerias utilerias = Utilerias.getUtilerias();

		for(Map<String,Object> mapResult : list){
			 sumatoria.setTopoVLiqSum(utilerias.getString(mapResult.get("MONTO_TOPO_V_LIQUIDADAS")));
			 sumatoria.setTopoVLiqCount(utilerias.getString(mapResult.get("VOLUMEN_TOPO_V_LIQUIDADAS")));
			 sumatoria.setTopoTLiqSum(utilerias.getString(mapResult.get("MONTO_TOPO_T_LIQUIDADAS")));
			 sumatoria.setTopoTLiqCount(utilerias.getString(mapResult.get("VOLUMEN_TOPO_T_LIQUIDADAS")));
			 sumatoria.setTopoTPorLiqSum(utilerias.getString(mapResult.get("MONTO_TOPO_T_POR_LIQUIDAR")));
			 sumatoria.setTopoTPorLiqCount(utilerias.getString(mapResult.get("VOLUMEN_TOPO_T_POR_LIQUIDAR")));
			 sumatoria.setRechazadasSum(utilerias.getString(mapResult.get("MONTO_RECHAZOS")));
			 sumatoria.setRechazadasCount(utilerias.getString(mapResult.get("VOLUMEN_RECHAZOS")));
		 }
	}

	/**
	 * @param beanReqConsReceptHist Objeto del tipo BeanReqConsReceptHist
	 * @return query Objeto del tipo String
	 */
	private String obtenerConsulta(BeanReqConsReceptHist beanReqConsReceptHist ) {
		String query = null;
		if ("TODAS".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_TODAS.toString();
		} else if ("TVL".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_VLIQ.toString();
		} else if ("TTL".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_TLIQ.toString();
		} else if ("TTXL".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_PORLIQ.toString();
		} else if ("R".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_RECH.toString();
		} else if ("APT".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_APL.toString();
		} else if ("DEV".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_DEVOL.toString();
		} else if ("RMD".equals(beanReqConsReceptHist.getOpcion())) {
			query = CONSULTA_RECEPTOR_HIST_RECHCTA.toString();
		}
		return query;
	}

	/**
	 * @param beanReqConsReceptHist Objeto del tipo BeanReqConsReceptHist
	 * @return query Objeto del tipo String
	 */
	private String obtenerSumatoriaConsulta(BeanReqConsReceptHist beanReqConsReceptHist ) {
		String query = null;
		if ("TODAS".equals(beanReqConsReceptHist.getOpcion())) {
			query = "";
		} else if ("TVL".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE TOPOLOGIA='V' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE'";
		} else if ("TTL".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE'";
		} else if ("TTXL".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE TOPOLOGIA='T' AND ESTATUS_BANXICO='PL' AND ESTATUS_TRANSFER !='TR' AND ESTATUS_TRANSFER !='DV' AND ESTATUS_TRANSFER !='RE'";
		} else if ("R".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE ESTATUS_TRANSFER='RE'";
		} else if ("APT".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE ESTATUS_TRANSFER='TR'";
		} else if ("DEV".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE ESTATUS_TRANSFER='DV'";
		} else if ("RMD".equals(beanReqConsReceptHist.getOpcion())) {
			query = "WHERE ESTATUS_TRANSFER='RE' AND LENGTH(MOTIVO_DEVOL)>0";
		}
		return query;
	}

	/**
	 * Metodo para llenar los receptores historicos
	 *
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias objeto del tipo Utilerias
	 * @param beanConsultaReceptorHistDAO obhjeto del tipo BeanConsultaReceptorHistDAO
	 * @return listaBeanReceptorHist objeto del tipo List<BeanConsRecepHist>
	 */
	private List<BeanConsRecepHist> llenarReceptorHistorico(List<HashMap<String, Object>> list, Utilerias utilerias,
			final BeanConsultaReceptorHistDAO beanConsultaReceptorHistDAO) {
		List<BeanConsRecepHist> listaBeanReceptorHist = new ArrayList<BeanConsRecepHist>();
		Integer zero = Integer.valueOf(0);

		 for(Map<String,Object> listMap : list){
			 Map<String,Object> mapResult = listMap;
			 BeanConsRecepHist beanConsultaReceptorhist = new BeanConsRecepHist();
			 beanConsultaReceptorhist.setBeanConsRecepHistDos(new BeanConsRecepHistDos());
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setTipoPago(utilerias.getString(mapResult.get("TIPO_PAGO")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setTopologia(utilerias.getString(mapResult.get("TOPOLOGIA")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setEstatusBanxico(utilerias.getString(mapResult.get("ESTATUS_BANXICO")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setEstatusTransfer(utilerias.getString(mapResult.get("ESTATUS_TRANSFER")));
			 beanConsultaReceptorhist.setfechaOperacion(utilerias.getString(mapResult.get("FCH_OPERACION")));
			 beanConsultaReceptorhist.setCveMiInstituc(utilerias.getString(mapResult.get("CVE_MI_INSTITUC")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setHora(utilerias.getString(mapResult.get("HORA")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setCveInstOrd(utilerias.getString(mapResult.get("CVE_INST_ORD")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setCveIntermeOrd(utilerias.getString(mapResult.get("CVE_INTERME_ORD")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setFolioPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setMonto(utilerias.getString(mapResult.get("MONTO")));
			 beanConsultaReceptorhist.setCodError(utilerias.getString(mapResult.get("CODIGO_ERROR")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setMotivoDevol(utilerias.getString(mapResult.get("MOTIVO_DEVOL")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setMotivoDevolDesc(utilerias.getString(mapResult.get("MOT_DEV_DESC")));
			 beanConsultaReceptorhist.setUsuario(utilerias.getString(mapResult.get("CVE_USUARIO")));
			 beanConsultaReceptorhist.getBeanConsRecepHistDos().setFechaCaptura(utilerias.getString(mapResult.get("FECHA_CAPTURA")));
				
			 listaBeanReceptorHist.add(beanConsultaReceptorhist);

			 if (zero.equals(beanConsultaReceptorHistDAO.getTotalReg())){
				 beanConsultaReceptorHistDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			 }

		 }
		return listaBeanReceptorHist;
	}

	/**Metodo que sirve para consultar los receptores Ini
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanConsultaReceptorHistDAO Objeto del tipo BeanConsultaReceptorHistDAO
	   */
	@Override
	public BeanConsultaReceptorHistDAO obtenerConsultaReceptorHistIni(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean) {
		final BeanConsultaReceptorHistDAO beanConsultaReceptorHistDAO= new BeanConsultaReceptorHistDAO();
		List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanConsRecepHist> listaBeanReceptorHist = new ArrayList<BeanConsRecepHist>();

    	try {
        	responseDTO = HelperDAO.consultar(CONSULTA_RECEPTOR_HIST_TODAS.toString(), Arrays.asList(
        			new Object[] { }), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        		 list = responseDTO.getResultQuery();
        		 if(!list.isEmpty()){
        			 for(Map<String,Object> mapResult : list){
        				 BeanConsRecepHist beanConsultaReceptorhistoria = new BeanConsRecepHist();
        				 beanConsultaReceptorhistoria.setBeanConsRecepHistDos(new BeanConsRecepHistDos());
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setTipoPago(utilerias.getString(mapResult.get("TIPO_PAGO")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setTopologia(utilerias.getString(mapResult.get("TOPOLOGIA")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setEstatusBanxico(utilerias.getString(mapResult.get("ESTATUS_BANXICO")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setEstatusTransfer(utilerias.getString(mapResult.get("ESTATUS_TRANSFER")));
        				 beanConsultaReceptorhistoria.setfechaOperacion(utilerias.getString(mapResult.get("FCH_OPERACION")));
        				 beanConsultaReceptorhistoria.setCveMiInstituc(utilerias.getString(mapResult.get("CVE_MI_INSTITUC")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setHora(utilerias.getString(mapResult.get("HORA")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setCveInstOrd(utilerias.getString(mapResult.get("CVE_INST_ORD")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setCveIntermeOrd(utilerias.getString(mapResult.get("CVE_INTERME_ORD")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setFolioPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setMonto(utilerias.getString(mapResult.get("MONTO")));
        				 beanConsultaReceptorhistoria.setCodError(utilerias.getString(mapResult.get("CODIGO_ERROR")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setMotivoDevol(utilerias.getString(mapResult.get("MOTIVO_DEVOL")));
        				 beanConsultaReceptorhistoria.getBeanConsRecepHistDos().setMotivoDevolDesc(utilerias.getString(mapResult.get("MOT_DEV_DESC")));
        				 beanConsultaReceptorhistoria.setUsuario(utilerias.getString(mapResult.get("CVE_USUARIO")));
        				 listaBeanReceptorHist.add(beanConsultaReceptorhistoria);

        				 if (beanConsultaReceptorHistDAO.getTotalReg()==null){
        					 beanConsultaReceptorHistDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
        				 }

        			 }
        		 }
        	}

        	beanConsultaReceptorHistDAO.setListaDatos(listaBeanReceptorHist);
        	beanConsultaReceptorHistDAO.setCodError(responseDTO.getCodeError());
        	beanConsultaReceptorHistDAO.setMsgError(responseDTO.getMessageError());

    	} catch (ExceptionDataAccess e) {
    		showException(e);
    	}
		return beanConsultaReceptorHistDAO;
	}
}