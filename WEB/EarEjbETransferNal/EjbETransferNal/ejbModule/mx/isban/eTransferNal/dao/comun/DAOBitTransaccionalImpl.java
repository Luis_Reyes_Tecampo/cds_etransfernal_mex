package mx.isban.eTransferNal.dao.comun;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitTransaccionalImpl extends Architech implements DAOBitTransaccional{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2691645015501813910L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_BIT_ADMON
	 */
	private static final String QUERY_INSERT_BIT_ADMON = "INSERT INTO TR_NAL_BIT_TRANSACCIONAL ( "+
		" FOLIO_OPER, FCH_OPER, USER_OPER, COD_CTE, EST_OPER, "+
		" CONTRATO, ID_TOKEN, MONTO, TIPO_CAM, REFERENCIA, "+
		" NUM_TIT, FCH_PROG, FCH_APLI, FCH_ACT, HORA_ACT, "+
		" DESC_OPER, DESC_ERR, COMENTARIOS, SERV_TRAN, NOM_ARCH, "+
		" CTA_ORIG, CTA_DEST, DIR_IP, CAN_APLI, INST_WEB, "+
		" HOST_WEB, ID_SESION, ID_OPER, BCO_DEST, COD_ERR ) VALUES (" +
		" SEQ_TR_NAL_BIT_TRANS_FOL_OPER.NEXTVAL, TO_DATE(?,'DD-MM-YYYY'), ?, ?, ?,  "+
	    " ?, ?, ?, ?, ?,  "+
	    " ?, TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY: HH24:mi:ss'),  "+
	    " ?, ?, ?, ?, ?,  "+
	    " ?, ?, ?, ?, ?,  "+
	    " ?, ?, ?, ?, ?   )";
	    
	
	    
	/**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitacora Objeto del tipo @see BeanReqInsertBitacora
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitTrans beanReqInsertBitacora, ArchitechSessionBean architechSessionBean){
        final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.insertar(QUERY_INSERT_BIT_ADMON, 
        		   Arrays.asList(new Object[]{
        				   beanReqInsertBitacora.getDatosOpe().getFchOperacion(),
        				   architechSessionBean.getUsuario(),
        				   beanReqInsertBitacora.getDatosGenerales().getCodCliente(),
        				   beanReqInsertBitacora.getDatosOpe().getEstatusOperacion(),
        				   beanReqInsertBitacora.getDatosGenerales().getContrato(),
        				   Integer.parseInt(beanReqInsertBitacora.getDatosGenerales().getIdToken()),
        				   Double.parseDouble(beanReqInsertBitacora.getDatosOpe().getMonto()),
        				   Double.parseDouble(beanReqInsertBitacora.getDatosOpe().getTipoCambio()),
        				   beanReqInsertBitacora.getDatosOpe().getReferencia(),
        				   Integer.parseInt(beanReqInsertBitacora.getDatosGenerales().getNumeroTitulos()),
        				   beanReqInsertBitacora.getDatosGenerales().getFchProgramada(),
        				   beanReqInsertBitacora.getDatosGenerales().getFchAplica(),
        				   beanReqInsertBitacora.getDatosGenerales().getFchAct(),
        				   beanReqInsertBitacora.getDatosGenerales().getHoraAct(),
        				   beanReqInsertBitacora.getDatosOpe().getDescOper(),
        				   beanReqInsertBitacora.getDatosOpe().getDescErr(),
        				   beanReqInsertBitacora.getDatosOpe().getComentarios(),
        				   beanReqInsertBitacora.getDatosOpe().getServTran(),
        				   beanReqInsertBitacora.getDatosGenerales().getNombreArchivo(),
        				   beanReqInsertBitacora.getDatosOpe().getCtaOrigen(),
        				   beanReqInsertBitacora.getDatosOpe().getCtaDestino(),
        				   beanReqInsertBitacora.getDatosGenerales().getDirIp(),
        				   beanReqInsertBitacora.getDatosGenerales().getAplicacionCanal(),
        				   beanReqInsertBitacora.getDatosGenerales().getInstanciaWeb(),
        				   beanReqInsertBitacora.getDatosGenerales().getHostWeb(),
        				   beanReqInsertBitacora.getDatosGenerales().getIdSesion(),
        				   beanReqInsertBitacora.getDatosOpe().getIdOperacion(),
        				   beanReqInsertBitacora.getDatosOpe().getBancoDestino(),
        				   beanReqInsertBitacora.getDatosOpe().getCodigoErr()
        				   }), 
           HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
           beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaBitacoraDAO;
     }
	    
	    
		
		
		
	
	  

}
