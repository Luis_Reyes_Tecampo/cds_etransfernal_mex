/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOTransferenciaSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;

/**
 * @author cchong
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOTransferenciaSPIDImpl extends Architech implements DAOTransferenciaSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5214163679297452107L;

	/**
	 * @param trama
	 * 			json convertido a string
	 * @return
	 * 			trama de respuesta de 390
	 */
	@Override
	public ResBeanEjecTranDAO ejecutar(String trama){
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(
				trama, "TRANSPID", "ARQ_MENSAJERIA");
		return resBeanEjecTranDAO;
	}

}
