/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORecepOperacionSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMotDevolucion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBloqueo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDOpciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoSubTotalPagDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoTotalDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenSubTotalPagDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenTotalDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 *Clase del tipo DAO que se encarga  obtener la informacion
 *para la pantalla de recepcion de operaciones del dia
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORecepOperacionSPIDImpl  extends Architech implements DAORecepOperacionSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8167141597887988495L;

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_IMPORTE
	 */
	private static final String QUERY_IMPORTE =" AND MONTO = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_BLOQUEO
	 */
	private static final String QUERY_BLOQUEO =
        " R.FCH_OPERACION = B.FCH_OPERACION "+
        " AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
        " AND R.CVE_INST_ORD = B.CVE_INST_ORD "+
        " AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
        " AND R.FOLIO_PAGO = B.FOLIO_PAGO "+
		" AND TRIM(B.CVE_USUARIO) = ? ";

	private static final String QUERY =
        " R.FCH_OPERACION = B.FCH_OPERACION "+
        " AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
        " AND R.CVE_INST_ORD = B.CVE_INST_ORD "+
        " AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
        " AND R.FOLIO_PAGO = B.FOLIO_PAGO ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_SELECT
	 */
	private static final String QUERY_CONSULTA_SELECT =
		" SELECT T.* FROM ( ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_PAG
	 */
	private static final String QUERY_CONSULTA_PAG =
	" ) T "+
	" WHERE INDICE  BETWEEN ? AND  ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_CAMPOS
	 */
	private static final String QUERY_CONSULTA_CAMPOS =
		" SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "+
		" R.folio_paquete,R.folio_pago,R.topologia, "+
		" R.enviar,R.cda,R.estatus_banxico, "+
		" R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "+
		" R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "+
		" R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "+
		" R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "+
		" R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, "+
		" R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "+
		" R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, "+
		" R.prioridad,R.long_rastreo,R.folio_paq_dev, "+
		" R.folio_pago_dev,R.refe_transfer,R.monto, "+
		" TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "+
		" R.direccion_ip,R.cde,R.nombre_ord, "+
		" R.nombre_rec,R.domicilio_ord,R.concepto_pago, B.CVE_USUARIO, B.FUNCIONALIDAD, "+
        " TO_CHAR(H.FCH_AVISO_ABONO,'DD-MM-YYYY HH24:mi:ss') AS FECHA_CAPTURA, ROWNUM INDICE "+
       " FROM TRAN_SPID_REC R LEFT JOIN TRAN_SPID_BLOQUEO B ON( " +
        QUERY +
       " ) LEFT JOIN  TRAN_SPID_HORAS H ON  " +
        " (R.FCH_OPERACION =  H.FCH_OPERACION  AND " +
        " R.CVE_MI_INSTITUC = H.CVE_INST_BENEF  AND " + 
        " R.CVE_INST_ORD =    H.CVE_INST_ORD  AND " + 
        " R.FOLIO_PAQUETE =   H.FOLIO_PAQUETE  AND " +
        " R.FOLIO_PAGO =      H.FOLIO_PAGO AND H.TIPO = 'R')"+
       " WHERE R.CVE_MI_INSTITUC = ? "+
       " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_CAMPOS
	 */
	private static final String QUERY_CONSULTA_CAMPOS_BLOQUEO =
		" SELECT TO_CHAR(R.fch_operacion,'DD-MM-YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "+
		" R.folio_paquete,R.folio_pago,R.topologia, "+
		" R.enviar,R.cda,R.estatus_banxico, "+
		" R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "+
		" R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "+
		" R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "+
		" R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "+
		" R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, "+
		" R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "+
		" R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, "+
		" R.prioridad,R.long_rastreo,R.folio_paq_dev, "+
		" R.folio_pago_dev,R.refe_transfer,R.monto, "+
		" TO_CHAR(R.fch_captura,'DD-MM-YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "+
		" R.direccion_ip,R.cde,R.nombre_ord, "+
		" R.nombre_rec,R.domicilio_ord,R.concepto_pago, B.CVE_USUARIO, B.FUNCIONALIDAD, "+
		" TO_CHAR(H.FCH_AVISO_ABONO,'DD-MM-YYYY HH24:mi:ss') AS FECHA_CAPTURA, " +
       " ROWNUM INDICE "+
       " FROM TRAN_SPID_REC R" +
       " LEFT JOIN TRAN_SPID_BLOQUEO B ON(      R.FCH_OPERACION = B.FCH_OPERACION       "+
       "                                    AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC  "+
       "                                    AND R.CVE_INST_ORD = B.CVE_INST_ORD        "+
       "                                    AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE      "+
       "                                    AND R.FOLIO_PAGO = B.FOLIO_PAGO  )         "+
       " LEFT JOIN  TRAN_SPID_HORAS H ON  (      R.FCH_OPERACION  =  H.FCH_OPERACION   "+
       "                                    AND  R.CVE_MI_INSTITUC = H.CVE_INST_BENEF  "+
       "                                    AND  R.CVE_INST_ORD =    H.CVE_INST_ORD    "+
       "                                    AND  R.FOLIO_PAQUETE =   H.FOLIO_PAQUETE   "+
       "                                    AND  R.folio_pago =      H.FOLIO_PAGO      "+
       "                                    AND  H.TIPO = 'R') "+  
       " WHERE TRIM(B.CVE_USUARIO) = ?" + 
       " AND R.CVE_MI_INSTITUC = ? "+
       " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TODAS
	 */
	private static final String QUERY_CONSULTA_TODAS =
			  QUERY_CONSULTA_CAMPOS+
	         " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=?  ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TODAS_BLOQUEO
	 */
	private static final String QUERY_CONSULTA_TODAS_BLOQUEO =
			  QUERY_CONSULTA_CAMPOS_BLOQUEO+
	         " AND R.ESTATUS_TRANSFER !=? AND R.ESTATUS_TRANSFER !=?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TODAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TODAS =
		" SELECT COUNT(1) CONT,"+
		" SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_TRANSFER<>'RE' THEN MONTO ELSE 0 END) MONTO_TOPO_V_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_TRANSFER<>'RE' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_V_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='LQ' THEN MONTO ELSE 0 END) MONTO_TOPO_T_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='LQ' THEN 1 ELSE 0 END) VOLUMEN_TOPO_T_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='PL' THEN MONTO ELSE 0 END) MONTO_TOPO_T_POR_LIQUIDAR, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='PL' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_T_POR_LIQUIDAR, "+
		" SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN MONTO ELSE 0 END) MONTO_RECHAZOS, "+
		" SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN 1 ELSE 0 END)     VOLUMEN_RECHAZOS "+
	         " FROM TRAN_SPID_REC   WHERE CVE_MI_INSTITUC = ? "+
	         " AND  FCH_OPERACION  =  TO_DATE(?,'dd-mm-yyyy') "+
	          " AND ESTATUS_TRANSFER !=? AND  ESTATUS_TRANSFER !=?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TODAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TODAS_BLOQUEO =
		" SELECT COUNT(1) CONT,"+
		" SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_TRANSFER<>'RE' THEN MONTO ELSE 0 END) MONTO_TOPO_V_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='V' AND ESTATUS_TRANSFER<>'RE' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_V_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='LQ' THEN MONTO ELSE 0 END) MONTO_TOPO_T_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='LQ' THEN 1 ELSE 0 END) VOLUMEN_TOPO_T_LIQUIDADAS, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='PL' THEN MONTO ELSE 0 END) MONTO_TOPO_T_POR_LIQUIDAR, "+
		" SUM(CASE WHEN TOPOLOGIA='T' AND ESTATUS_TRANSFER<>'RE' AND ESTATUS_BANXICO='PL' THEN 1 ELSE 0 END)     VOLUMEN_TOPO_T_POR_LIQUIDAR, "+
		" SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN MONTO ELSE 0 END) MONTO_RECHAZOS, "+
		" SUM(CASE WHEN ESTATUS_TRANSFER ='RE' THEN 1 ELSE 0 END)     VOLUMEN_RECHAZOS "+
	         " FROM TRAN_SPID_REC R,  TRAN_SPID_BLOQUEO B WHERE " +QUERY_BLOQUEO+
	         " AND R.CVE_MI_INSTITUC = ? "+
	         " AND  R.FCH_OPERACION  =  TO_DATE(?,'dd-mm-yyyy') "+
	         " AND R.ESTATUS_TRANSFER !=? AND R.ESTATUS_TRANSFER !=?";



	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_V_LIQUIDADAS
	 */
	private static final String QUERY_CONSULTA_TOPO_V_LIQUIDADAS =
		QUERY_CONSULTA_CAMPOS+
	        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER != ? AND topologia = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_V_LIQUIDADAS_BLOQUEO
	 */
	private static final String QUERY_CONSULTA_TOPO_V_LIQUIDADAS_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	        " AND R.ESTATUS_TRANSFER !=? AND R.ESTATUS_TRANSFER !=? AND R.ESTATUS_TRANSFER != ? AND R.topologia = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS =
		 " SELECT COUNT(*) CONT,SUM(MONTO) MONTO_TOPO_V_LIQUIDADAS "+
	         "  FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER != ? AND  topologia = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS_BLOQUEO =
		 " SELECT COUNT(*) CONT,SUM(MONTO) MONTO_TOPO_V_LIQUIDADAS "+
	         "  FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B WHERE " +QUERY_BLOQUEO+
	         " AND R.CVE_MI_INSTITUC = ? AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER != ? AND  topologia = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_T_LIQUIDADAS
	 */
	private static final String QUERY_CONSULTA_TOPO_T_LIQUIDADAS =
		QUERY_CONSULTA_CAMPOS+
	        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER != ? "+
	        " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ?  AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_T_LIQUIDADAS_BLOQUEO
	 */
	private static final String QUERY_CONSULTA_TOPO_T_LIQUIDADAS_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER != ? "+
	        " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ?  AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS =
		 " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_TOPO_T_LIQUIDADAS "+
	         "  FROM TRAN_SPID_REC  WHERE CVE_MI_INSTITUC = ? "+
	         " AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=?  AND estatus_transfer != ? "+
		        " AND TOPOLOGIA = ? AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS_BLOQUEO =
		 " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_TOPO_T_LIQUIDADAS "+
	         "  FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B  WHERE " +QUERY_BLOQUEO+
	         " AND R.CVE_MI_INSTITUC = ? "+
	         " AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		        " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=?  AND ESTATUS_TRANSFER != ? "+
		        " AND TOPOLOGIA = ? AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR
	 */
	private static final String QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR =
		QUERY_CONSULTA_CAMPOS +
		 " AND ESTATUS_TRANSFER !=?  AND ESTATUS_TRANSFER !=? "+
	     " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ? AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR
	 */
	private static final String QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO +
		 " AND ESTATUS_TRANSFER !=?  AND ESTATUS_TRANSFER !=? "+
	     " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ? AND estatus_banxico = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR =
		     " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_TOPO_T_POR_LIQUIDAR "+
	         " FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B "+
	         " WHERE R.CVE_MI_INSTITUC = ?  AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		     " AND ESTATUS_TRANSFER  !=? AND ESTATUS_TRANSFER !=? "+
		     " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ? AND estatus_banxico = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR
	 */
	private static final String QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR_BLOQUEAR =
		     " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_TOPO_T_POR_LIQUIDAR "+
	         " FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B "+
	         " WHERE " +QUERY_BLOQUEO+" AND R.CVE_MI_INSTITUC = ? "+
	         " AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		     " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=?"+
		     " AND ESTATUS_TRANSFER != ? AND TOPOLOGIA = ? AND estatus_banxico = ?";


	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_RECHAZOS
	 */
	private static final String QUERY_CONSULTA_RECHAZOS =
		QUERY_CONSULTA_CAMPOS+
	      " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND estatus_transfer = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_RECHAZOS
	 */
	private static final String QUERY_CONSULTA_RECHAZOS_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	      " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND estatus_transfer = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_RECHAZOS
	 */
	private static final String QUERY_COUNT_CONSULTA_RECHAZOS  =
		     " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_RECHAZOS FROM TRAN_SPID_REC "+
	         " WHERE CVE_MI_INSTITUC = ? "+
	         " AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER = ? ";


	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_RECHAZOS
	 */
	private static final String QUERY_COUNT_CONSULTA_RECHAZOS_BLOQUEO  =
		     " SELECT COUNT(*) CONT, SUM(MONTO) MONTO_RECHAZOS FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B "+
	         " WHERE " + QUERY_BLOQUEO +" AND R.CVE_MI_INSTITUC = ? "+
	         " AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? AND estatus_transfer = ? " ;

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_APLICADAS
	 */
	private static final String QUERY_CONSULTA_APLICADAS =
		QUERY_CONSULTA_CAMPOS+
	      " AND estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_APLICADAS
	 */
	private static final String QUERY_CONSULTA_APLICADAS_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	      " AND estatus_transfer = ?  ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_APLICADAS
	 */
	private static final String QUERY_COUNT_CONSULTA_APLICADAS =
		    " SELECT COUNT(*) CONT FROM TRAN_SPID_REC "+
	     	" WHERE CVE_MI_INSTITUC = ? AND  FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_APLICADAS_BLOQUEO
	 */
	private static final String QUERY_COUNT_CONSULTA_APLICADAS_BLOQUEO =
		    " SELECT COUNT(*) CONT FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B "+
	     	" WHERE "+QUERY_BLOQUEO+ " AND R.CVE_MI_INSTITUC = ? AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_DEVOLUCIONES
	 */
	private static final String QUERY_CONSULTA_DEVOLUCIONES =
		QUERY_CONSULTA_CAMPOS+
	      " and estatus_transfer = ?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_DEVOLUCIONES
	 */
	private static final String QUERY_CONSULTA_DEVOLUCIONES_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	      " and estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_DEVOLUCIONES
	 */
	private static final String QUERY_COUNT_CONSULTA_DEVOLUCIONES =
		 " SELECT COUNT(*) CONT FROM TRAN_SPID_REC "+
			" WHERE CVE_MI_INSTITUC = ?  AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') and estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_DEVOLUCIONES_BLOQUEO
	 */
	private static final String QUERY_COUNT_CONSULTA_DEVOLUCIONES_BLOQUEO =
		 " SELECT COUNT(*) CONT FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B"+
			" WHERE " +QUERY_BLOQUEO+ " AND R.CVE_MI_INSTITUC = ?  AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') and estatus_transfer = ? ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_RECHAZO_C_MOT_DEV
	 */
	private static final String QUERY_CONSULTA_RECHAZO_C_MOT_DEV =
		QUERY_CONSULTA_CAMPOS+
	    " AND  ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? "+
	    " AND estatus_transfer = ? "+
	    " AND (MOTIVO_DEVOL IS NOT NULL or motivo_devol <> '') ";


	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO
	 */
	private static final String QUERY_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO =
		QUERY_CONSULTA_CAMPOS_BLOQUEO+
	    "  AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? "+
	    " AND estatus_transfer = ? "+
	    " AND (MOTIVO_DEVOL IS NOT NULL or motivo_devol <> '') ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV
	 */
	private static final String QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV =
		 " SELECT COUNT(*) CONT"+
		 " FROM TRAN_SPID_REC "+
		 " WHERE CVE_MI_INSTITUC = ? "+
		 " AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		 " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? "+
		 " AND estatus_transfer = ? AND (MOTIVO_DEVOL IS NOT NULL or motivo_devol <> '') ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO
	 */
	private static final String QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO =
		 " SELECT COUNT(*) CONT"+
		 " FROM TRAN_SPID_REC R, TRAN_SPID_BLOQUEO B"+
		 " WHERE  "+QUERY_BLOQUEO+ " AND R.CVE_MI_INSTITUC = ? "+
		 " AND R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "+
		 " AND ESTATUS_TRANSFER !=? AND ESTATUS_TRANSFER !=? "+
		 " AND estatus_transfer = ? AND (MOTIVO_DEVOL IS NOT NULL or motivo_devol <> '') ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_BLOQUEO
	 */
	private static final String QUERY_INSERT_BLOQUEO =
	" INSERT INTO TRAN_SPID_BLOQUEO  (FCH_OPERACION,CVE_MI_INSTITUC,CVE_INST_ORD,FOLIO_PAQUETE,FOLIO_PAGO,FUNCIONALIDAD,CVE_USUARIO,FCH_ALTA) "+
	" VALUES (TO_DATE(?,'DD-MM-YYYY'),?,?,?,?,?,?,SYSDATE) ";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_BLOQUEO
	 */
	private static final String QUERY_DELETE_BLOQUEO =
	" DELETE TRAN_SPID_BLOQUEO WHERE FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') AND " +
	" CVE_MI_INSTITUC  = ? AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO =? AND TRIM(FUNCIONALIDAD) = ? AND TRIM(CVE_USUARIO) =?";

	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DEVOLUCIONES
	 */
	private static final String QUERY_DEVOLUCIONES =
		"SELECT TRIM(CODIGO_GLOBAL) SECUENCIAL, DESCRIPCION FROM COMU_DETALLE_CD WHERE TIPO_GLOBAL='TR_DEVSPID'";

	/**
	 * Propiedad del tipo String que almacena el valor de TODAS
	 */
	private static final String TODAS ="TODAS";

	/**
	 * Propiedad del tipo String que almacena el valor de TVL
	 */
	private static final String TVL ="TVL";

	/**
	 * Propiedad del tipo String que almacena el valor de TTL
	 */
	private static final String TTL = "TTL";

	/**
	 * Propiedad del tipo String que almacena el valor de TTXL
	 */
	private static final String TTXL = "TTXL";

	/**
	 * Propiedad del tipo String que almacena el valor de R
	 */
	private static final String R = "R";

	/**
	 * Propiedad del tipo String que almacena el valor de APT
	 */
	private static final String APT = "APT";

	/**
	 * Propiedad del tipo String que almacena el valor de DEV
	 */
	private static final String DEV = "DEV";

	/**
	 * Propiedad del tipo String que almacena el valor de RMD
	 */
	private static final String RMD = "RMD";

	/**
	 * Propiedad del tipo String que almacena el valor de CONT
	 */
	private static final String CONT = "CONT";


	/**
     * Metodo que se encarga de realizar la consulta de los motivos de devoluciones
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMotDevolucionDAO Objeto del tipo @see BeanResConsMotDevolucionDAO
     */
    public BeanResConsMotDevolucionDAO consultaDevoluciones(ArchitechSessionBean sessionBean) {
    	List<BeanMotDevolucion> beanMotDevolucionList = Collections.emptyList();
    	BeanMotDevolucion beanMotDevolucion = null;
    	BeanResConsMotDevolucionDAO beanResConsMotDevolucionDAO =  new BeanResConsMotDevolucionDAO();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<Object> parametros = Collections.emptyList();
    	String query = null;
    	try {
    		query = QUERY_DEVOLUCIONES;

        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		beanMotDevolucionList = new ArrayList<BeanMotDevolucion>();
        		 list = responseDTO.getResultQuery();
    			 for(Map<String,Object> map:list){
    				 beanMotDevolucion =  new BeanMotDevolucion();
    				 beanMotDevolucion.setSecuencial(utilerias.getString(map.get("SECUENCIAL")));
    				 beanMotDevolucion.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
    				 beanMotDevolucionList.add(beanMotDevolucion);
         		 }
    			 beanResConsMotDevolucionDAO.setBeanMotDevolucionList(beanMotDevolucionList);
        	}
	  	} catch (ExceptionDataAccess e) {
	  		showException(e);
	  		beanResConsMotDevolucionDAO.setCodError(Errores.EC00011B);
	  	}
        return beanResConsMotDevolucionDAO;
    }


	/**
     * Metodo que se encarga de realizar la consulta de todas las transferencias
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param parametros listado de parametros
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsTranSpeiRecDAO Objeto del tipo @see BeanResConsTranSpeiRecDAO
     */
    public BeanResReceptoresSPIDDAO consultaTransferencia(BeanReqReceptoresSPID beanReqReceptoresSPID,List<Object> parametros,ArchitechSessionBean sessionBean) {
    	BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO =  new BeanResReceptoresSPIDDAO();
    	List<Object> parametroBloqueos = new ArrayList<Object>();
    	beanResReceptoresSPIDDAO.setSaldoSubTotalPag(new BeanSaldoSubTotalPagDAO());
    	beanResReceptoresSPIDDAO.setVolumenSubTotalPag(new BeanVolumenSubTotalPagDAO());


    	List<HashMap<String,Object>> list = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	List<BeanReceptoresSPID> beanReceptoresSPIDList = new ArrayList<BeanReceptoresSPID>();
    	String query = null;
    	try {
    		parametroBloqueos.add(sessionBean.getUsuario());
    		parametroBloqueos.addAll(parametros);
    		if(beanReqReceptoresSPID.getImporteBusqueda()!=null && !"".equals(beanReqReceptoresSPID.getImporteBusqueda().trim())){
    			parametroBloqueos.add(beanReqReceptoresSPID.getImporteBusqueda().replaceAll(",",""));
    			parametros.add(beanReqReceptoresSPID.getImporteBusqueda().replaceAll(",",""));
        	}
    		query = getQueryCountBloqueo(beanReqReceptoresSPID);
        	responseDTO = HelperDAO.consultar(query, parametroBloqueos,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		for(Map<String,Object> map:list){
        			descomponeConsultaCount(beanReqReceptoresSPID,beanResReceptoresSPIDDAO,map);
        		}
        	}
        	if(beanResReceptoresSPIDDAO.getTotalReg()>0){
        		beanResReceptoresSPIDDAO.setBloqueado(true);
        	    consultaTransferencia(
        	    		beanReqReceptoresSPID,parametroBloqueos,
        	    		beanResReceptoresSPIDDAO, true);
        	}else{
        		query = getQueryCount(beanReqReceptoresSPID);
            	responseDTO = HelperDAO.consultar(query, parametros,
              			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
            	if(responseDTO.getResultQuery()!= null){
            		 list = responseDTO.getResultQuery();
            		for(Map<String,Object> map:list){
            			descomponeConsultaCount(beanReqReceptoresSPID,beanResReceptoresSPIDDAO,map);
            		}
            	}
	        	if(beanResReceptoresSPIDDAO.getTotalReg()>0){
	        		beanResReceptoresSPIDDAO.setBloqueado(false);
	        		consultaTransferencia(
	        	    		beanReqReceptoresSPID,parametros,
	        	    		beanResReceptoresSPIDDAO, false);
	        }else{
	        	beanResReceptoresSPIDDAO.setCodError(Errores.OK00000V);
	        	beanReceptoresSPIDList = Collections.emptyList();
	        	beanResReceptoresSPIDDAO.setListReceptoresSPID(beanReceptoresSPIDList);
	        }
        }
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		beanResReceptoresSPIDDAO.setCodError(Errores.EC00011B);
  	}
        return beanResReceptoresSPIDDAO;
    }

	/**
     * Metodo que se encarga de realizar la consulta de todas las transferencias
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param parametros listado de parametros
     * @param beanResReceptoresSPIDDAO Objeto del tipo @see BeanResReceptoresSPIDDAO
     * @param isBloqueado boolean true si ejecuta el query de bloqueados false en caso contrario
     */
    private void consultaTransferencia(
    		BeanReqReceptoresSPID beanReqReceptoresSPID,List<Object> parametros,
    		BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO,  boolean isBloqueado) {
    	String query;
    	BeanReceptoresSPID beanReceptoresSPID = null;
    	List<HashMap<String,Object>> list = null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	parametros.add(beanReqReceptoresSPID.getPaginador().getRegIni());
    	parametros.add(beanReqReceptoresSPID.getPaginador().getRegFin());
    	if(isBloqueado){
    		query = getQueryConsultaBloqueo(beanReqReceptoresSPID);
    	}else{
    		query = getQueryConsulta(beanReqReceptoresSPID);
    	}


    	List<BeanReceptoresSPID> beanReceptoresSPIDList = null;
	 	try {
			responseDTO = HelperDAO.consultar(query,parametros,
				HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
      	  list = responseDTO.getResultQuery();

      	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){

      		beanReceptoresSPIDList = new ArrayList<BeanReceptoresSPID>();

      		  for(HashMap<String,Object> map:list){
      			 beanReceptoresSPID =  descomponeConsulta(beanReqReceptoresSPID,map,beanResReceptoresSPIDDAO);
      			beanReceptoresSPIDList.add(beanReceptoresSPID);
      		  }
      		beanResReceptoresSPIDDAO.setListReceptoresSPID(beanReceptoresSPIDList);
      	  }
      	beanResReceptoresSPIDDAO.setCodError(Errores.OK00000V);
      	beanResReceptoresSPIDDAO.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
	  		beanResReceptoresSPIDDAO.setCodError(Errores.EC00011B);
		}

    }
    /**
     * Metodo que genera un objeto del tipo BeanTranSpeiRec a partir de la respuesta de la consulta
     * @param beanReqReceptoresSPID Objeto del tipo BeanReqConsTranSpeiRec
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @param beanResReceptoresSPIDDAO Objeto del tipo BeanResReceptoresSPIDDAO
     * @return BeanReceptoresSPID Objeto del tipo BeanReceptoresSPID
     */
    private BeanReceptoresSPID descomponeConsulta(BeanReqReceptoresSPID beanReqReceptoresSPID,Map<String,Object> map,
    		BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBloqueo beanSPIDBloqueo = new BeanSPIDBloqueo();
    	BeanReceptoresSPID beanReceptoresSPID = new BeanReceptoresSPID();
    	beanReceptoresSPID.setLlave(seteaLlave(map));
    	beanReceptoresSPID.setDatGenerales(seteaDatosGenerales(map));
    	beanReceptoresSPID.setEstatus(seteaEstatus(map));
    	beanReceptoresSPID.setBancoOrd(seteaBancoOrd(map));
    	beanReceptoresSPID.setBancoRec(seteaBancoRec(map));
    	beanReceptoresSPID.setEnvDev(seteaEnvDev(map));
    	beanReceptoresSPID.setRastreo(seteaRastreo(map));
    	beanReceptoresSPID.setFchPagos(seteaFchPago(map));
    	beanReceptoresSPID.setOpciones(new BeanSPIDOpciones());
    	beanReceptoresSPID.setBloqueo(beanSPIDBloqueo);

    	beanSPIDBloqueo.setUsuario(utilerias.getString(map.get("CVE_USUARIO")));
    	if("RECEP_OPSPID".equals(utilerias.getString(map.get("FUNCIONALIDAD")))){
    		beanSPIDBloqueo.setBloqueado(true);
    	}


    	clasificaReg(beanReqReceptoresSPID,beanReceptoresSPID);
    	calculaSumas(beanReceptoresSPID,beanResReceptoresSPIDDAO);
    	return beanReceptoresSPID;
    }
    /**
     * Metodo que setea el folio paquete y pago de devolucion
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEnvDev Bean de respuesta
     */
    private BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
    	beanSPIDEnvDev.setMotivoDev(utilerias.getString(map.get("MOTIVO_DEVOL")));
    	beanSPIDEnvDev.setFolioPaqDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")));
    	beanSPIDEnvDev.setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")));

    	return beanSPIDEnvDev;
    }

    /**
     * Metodo que setea los campos llave
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDLlave Bean de respuesta
     */
    private BeanSPIDLlave seteaLlave(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
    	beanSPIDLlave.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
    	beanSPIDLlave.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
    	beanSPIDLlave.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
    	beanSPIDLlave.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
    	beanSPIDLlave.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
    	return beanSPIDLlave;
    }

    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDDatGenerales Bean de respuesta de datos generales
     */
    private BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
    	BigDecimal monto = utilerias.getBigDecimal(map.get("MONTO"));
    	beanSPIDDatGenerales.setTopologia(utilerias.getString(map.get("TOPOLOGIA")));
    	beanSPIDDatGenerales.setPrioridad(utilerias.getString(map.get("PRIORIDAD")));
    	beanSPIDDatGenerales.setTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
    	beanSPIDDatGenerales.setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")));
    	beanSPIDDatGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
    	beanSPIDDatGenerales.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
    	beanSPIDDatGenerales.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
    	beanSPIDDatGenerales.setCde(utilerias.getString(map.get("CDE")));
    	beanSPIDDatGenerales.setCda(utilerias.getString(map.get("CDA")));
    	beanSPIDDatGenerales.setEnviar(utilerias.getString(map.get("ENVIAR")));
    	beanSPIDDatGenerales.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
    	beanSPIDDatGenerales.setFechaCaptura(utilerias.getString(map.get("FECHA_CAPTURA")));
    	
    	beanSPIDDatGenerales.setMonto(monto);
    	return beanSPIDDatGenerales;
    }

    /**
     * Metodo que setea los campos generales
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEstatus Bean de respuesta de estatus
     */
    private BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
    	beanSPIDEstatus.setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")));
    	beanSPIDEstatus.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
    	beanSPIDEstatus.setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));
    	return beanSPIDEstatus;
    }
    /**
     * Metodo que setea los campos del banco ordenante
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
    	beanSPIDBancoOrd.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
    	beanSPIDBancoOrd.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
    	beanSPIDBancoOrd.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
    	beanSPIDBancoOrd.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
    	beanSPIDBancoOrd.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
    	beanSPIDBancoOrd.setDomicilioOrd(utilerias.getString(map.get("DOMICILIO_ORD")));
    	beanSPIDBancoOrd.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
    	beanSPIDBancoOrd.setCodPostalOrd(utilerias.getString(map.get("COD_POSTAL_ORD")));
    	beanSPIDBancoOrd.setFchConstitOrd(utilerias.getString(map.get("FCH_CONSTIT_ORD")));

    	return beanSPIDBancoOrd;
    }

    /**
     * Metodo que setea los campos de rastreo
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
    	beanSPIDRastreo.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
    	beanSPIDRastreo.setCveRastreoOri(utilerias.getString(map.get("CVE_RASTREO_ORI")));
    	beanSPIDRastreo.setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")));
    	beanSPIDRastreo.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
    	return beanSPIDRastreo;
    }

    /**
     * Metodo que setea los campos del Banco Receptor
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
    	beanSPIDBancoRec.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
    	beanSPIDBancoRec.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaTran(utilerias.getString(map.get("NUM_CUENTA_TRAN")));
    	beanSPIDBancoRec.setRfcRec(utilerias.getString(map.get("RFC_REC")));

    	return beanSPIDBancoRec;
    }

    /**
     * Metodo que setea los campos de las fechas de pago
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
     */
    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
    	beanSPIDFchPagos.setFchAceptPago(utilerias.getString(map.get("FCH_ACEPT_PAGO")));
    	beanSPIDFchPagos.setHoraAceptPago(utilerias.getString(map.get("HORA_ACEPT_PAGO")));
    	beanSPIDFchPagos.setFchInstrucPago(utilerias.getString(map.get("FCH_INSTRUC_PAGO")));
    	beanSPIDFchPagos.setHoraInstrucPago(utilerias.getString(map.get("HORA_INSTRUC_PAGO")));
    	return beanSPIDFchPagos;
    }

    /**
     * Metodo que sirve para realizar la suma de la pagina
     * @param beanReceptoresSPID bean del tipo BeanReceptoresSPID
     * @param beanResReceptoresSPIDDAO Bean del tipo BeanResReceptoresSPIDDAO
     */
    void calculaSumas(BeanReceptoresSPID beanReceptoresSPID,BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO){

    	BeanSPIDDatGenerales beanSPIDDatGenerales = beanReceptoresSPID.getDatGenerales();
    	BeanSaldoSubTotalPagDAO beanSaldoSubTotalPagDAO = beanResReceptoresSPIDDAO.getSaldoSubTotalPag();
    	BeanVolumenSubTotalPagDAO beanVolumenSubTotalPagDAO = beanResReceptoresSPIDDAO.getVolumenSubTotalPag();


    	if(TVL.equals(beanReceptoresSPID.getAgrupacion())){
    		beanSaldoSubTotalPagDAO.setTopoVLiquidadas(beanSaldoSubTotalPagDAO.getTopoVLiquidadas().add(beanSPIDDatGenerales.getMonto()));
    		beanVolumenSubTotalPagDAO.setTopoVLiquidadas(beanVolumenSubTotalPagDAO.getTopoVLiquidadas()+1);
	    }else if(TTL.equals(beanReceptoresSPID.getAgrupacion())){
	    	beanSaldoSubTotalPagDAO.setTopoTLiquidadas(beanSaldoSubTotalPagDAO.getTopoTLiquidadas().add(beanSPIDDatGenerales.getMonto()));
    		beanVolumenSubTotalPagDAO.setTopoTLiquidadas(beanVolumenSubTotalPagDAO.getTopoTLiquidadas()+1);
	    }else if(TTXL.equals(beanReceptoresSPID.getAgrupacion())){
	    	beanSaldoSubTotalPagDAO.setTopoTPorLiquidar(beanSaldoSubTotalPagDAO.getTopoTPorLiquidar().add(beanSPIDDatGenerales.getMonto()));
    		beanVolumenSubTotalPagDAO.setTopoTPorLiquidar(beanVolumenSubTotalPagDAO.getTopoTPorLiquidar()+1);
	    }else if(R.equals(beanReceptoresSPID.getAgrupacion())){
	    	beanSaldoSubTotalPagDAO.setRechazos(beanSaldoSubTotalPagDAO.getRechazos().add(beanSPIDDatGenerales.getMonto()));
    		beanVolumenSubTotalPagDAO.setRechazos(beanVolumenSubTotalPagDAO.getRechazos()+1);
	    }
    }

    /**
     * Metodo que sirve para clasificar cada uno de los registros
     * @param beanReqReceptoresSPID bean del tipo BeanReqReceptoresSPID
     * @param beanReceptoresSPID Bean del tipo BeanReceptoresSPID
     */
    private void clasificaReg(BeanReqReceptoresSPID beanReqReceptoresSPID,BeanReceptoresSPID beanReceptoresSPID){
    	BeanSPIDDatGenerales beanSPIDDatGenerales = beanReceptoresSPID.getDatGenerales();
    	BeanSPIDEstatus beanSPIDEstatus = beanReceptoresSPID.getEstatus();
    	if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
	    	if("V".equals(beanSPIDDatGenerales.getTopologia())&&(!"RE".equals(beanSPIDEstatus.getEstatusTransfer()))){//Agrupar TOPO_V_LIQUIDADAS
	    		beanReceptoresSPID.setAgrupacion(TVL);
	    	}else if("T".equals(beanSPIDDatGenerales.getTopologia())&&(!"RE".equals(beanSPIDEstatus.getEstatusTransfer()))
	    			&& "LQ".equals(beanSPIDEstatus.getEstatusBanxico())){//TOPO_T_LIQUIDADAS
	    		beanReceptoresSPID.setAgrupacion(TTL);
	    	}else if("T".equals(beanSPIDDatGenerales.getTopologia())&&(!"RE".equals(beanSPIDEstatus.getEstatusTransfer()))
	    			&& "PL".equals(beanSPIDEstatus.getEstatusBanxico())){//TOPO_T_POR_LIQUIDAR
	    		beanReceptoresSPID.setAgrupacion(TTXL);
	    	}else if("RE".equals(beanSPIDEstatus.getEstatusTransfer())){//RECHAZADAS
	    		beanReceptoresSPID.setAgrupacion(R);
	    	}
    	}else if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
    		beanReceptoresSPID.setAgrupacion(TVL);
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(TTL);
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(TTXL);
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(R);
		}else if(APT.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(APT);
		}else if(DEV.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(DEV);
		}else if(RMD.equals(beanReqReceptoresSPID.getOpcion())){
			beanReceptoresSPID.setAgrupacion(RMD);
		}
    }

    /**
     * Metodo para obtener el query
     * @param beanReqReceptoresSPID objeto del tipo beanReqReceptoresSPID
     * @return String cadena con el query
     */
    private String getQueryCount(BeanReqReceptoresSPID beanReqReceptoresSPID){
    	StringBuilder query=new StringBuilder();
		if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TODAS);
		}else if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS);
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS);
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR);
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_RECHAZOS);
		}else if(APT.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_APLICADAS);
		}else if(DEV.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_DEVOLUCIONES);
		}else if(RMD.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV);
		}
		if(beanReqReceptoresSPID.getImporteBusqueda()!=null && !"".equals(beanReqReceptoresSPID.getImporteBusqueda().trim())){
    		query.append(QUERY_IMPORTE);
    	}
		return query.toString();
    }
    /**
     * Metodo para obtener el query
     * @param beanReqReceptoresSPID objeto del tipo BeanReqReceptoresSPID
     * @return String cadena con el query
     */
    private String getQueryCountBloqueo(BeanReqReceptoresSPID beanReqReceptoresSPID){
    	StringBuilder query=new StringBuilder();
		if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TODAS_BLOQUEO);
		}else if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_V_LIQUIDADAS_BLOQUEO);
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_T_LIQUIDADAS_BLOQUEO);
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_TOPO_T_POR_LIQUIDAR_BLOQUEAR);
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_RECHAZOS_BLOQUEO);
		}else if(APT.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_APLICADAS_BLOQUEO);
		}else if(DEV.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_DEVOLUCIONES_BLOQUEO);
		}else if(RMD.equals(beanReqReceptoresSPID.getOpcion())){
			query.append(QUERY_COUNT_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO);
		}
    	if(beanReqReceptoresSPID.getImporteBusqueda()!=null && !"".equals(beanReqReceptoresSPID.getImporteBusqueda().trim())){
    		query.append(QUERY_IMPORTE);
    	}
		return query.toString();
    }

    /**
     * Metodo para obtener el query
     * @param beanReqReceptoresSPID objeto del tipo BeanReqReceptoresSPID
     * @return String cadena con el query
     */
    private String getQueryConsulta(BeanReqReceptoresSPID beanReqReceptoresSPID){
    	StringBuilder builder = new StringBuilder();
    	builder.append(QUERY_CONSULTA_SELECT);
    	if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
    		builder.append(QUERY_CONSULTA_TODAS);
		}else if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_V_LIQUIDADAS);
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_T_LIQUIDADAS);
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR);
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_RECHAZOS);
		}else if(APT.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_APLICADAS);
		}else if(DEV.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_DEVOLUCIONES);
		}else if(RMD.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_RECHAZO_C_MOT_DEV);
		}
    	if(beanReqReceptoresSPID.getImporteBusqueda()!=null && !"".equals(beanReqReceptoresSPID.getImporteBusqueda().trim())){
    		builder.append(QUERY_IMPORTE);
    	}
    	builder.append(QUERY_CONSULTA_PAG);
    	return builder.toString();
    }


    /**
     * Metodo para obtener el query
     * @param beanReqReceptoresSPID objeto del tipo BeanReqReceptoresSPID
     * @return String cadena con el query
     */
    private String getQueryConsultaBloqueo(BeanReqReceptoresSPID beanReqReceptoresSPID){
    	StringBuilder builder = new StringBuilder();
    	builder.append(QUERY_CONSULTA_SELECT);
    	if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
    		builder.append(QUERY_CONSULTA_TODAS_BLOQUEO);

		}else if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_V_LIQUIDADAS_BLOQUEO);
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_T_LIQUIDADAS_BLOQUEO);
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_TOPO_T_POR_LIQUIDAR_BLOQUEO);
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_RECHAZOS_BLOQUEO);
		}else if(APT.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_APLICADAS_BLOQUEO);
		}else if(DEV.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_DEVOLUCIONES_BLOQUEO);
		}else if(RMD.equals(beanReqReceptoresSPID.getOpcion())){
			builder.append(QUERY_CONSULTA_RECHAZO_C_MOT_DEV_BLOQUEO);
		}
    	if(beanReqReceptoresSPID.getImporteBusqueda()!=null && !"".equals(beanReqReceptoresSPID.getImporteBusqueda().trim())){
    		builder.append(QUERY_IMPORTE);
    	}
    	builder.append(QUERY_CONSULTA_PAG);
    	return builder.toString();
    }

    /**
     * @param beanReqReceptoresSPID Bean del tipo BeanReqReceptoresSPID
     * @param beanResReceptoresSPIDDAO Bean del tipo BeanResReceptoresSPIDDAO
     * @param map Objeto del tipo Map<String,Object>
     */
    private void descomponeConsultaCount(BeanReqReceptoresSPID beanReqReceptoresSPID,
    		BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO,Map<String,Object> map){

    	BeanVolumenTotalDAO volumenTotal = new BeanVolumenTotalDAO();
    	BeanSaldoTotalDAO saldoTotal = new BeanSaldoTotalDAO();

    	 beanResReceptoresSPIDDAO.setVolumenTotal(volumenTotal);
    	 beanResReceptoresSPIDDAO.setSaldoTotal(saldoTotal);
    	Utilerias utilerias = Utilerias.getUtilerias();
    	beanResReceptoresSPIDDAO.setTotalReg(utilerias.getInteger(map.get(CONT)));


		if(TODAS.equals(beanReqReceptoresSPID.getOpcion())){
			saldoTotal.setTopoVLiquidadas(utilerias.getBigDecimal(map.get("MONTO_TOPO_V_LIQUIDADAS")));
			saldoTotal.setTopoTLiquidadas(utilerias.getBigDecimal(map.get("MONTO_TOPO_T_LIQUIDADAS")));
			saldoTotal.setTopoTPorLiquidar(utilerias.getBigDecimal(map.get("MONTO_TOPO_T_POR_LIQUIDAR")));
			saldoTotal.setRechazos(utilerias.getBigDecimal(map.get("MONTO_RECHAZOS")));

			volumenTotal.setTopoVLiquidadas(utilerias.getInteger(map.get("VOLUMEN_TOPO_V_LIQUIDADAS")));
			volumenTotal.setTopoTLiquidadas(utilerias.getInteger(map.get("VOLUMEN_TOPO_T_LIQUIDADAS")));
			volumenTotal.setTopoTPorLiquidar(utilerias.getInteger(map.get("VOLUMEN_TOPO_T_POR_LIQUIDAR")));
			volumenTotal.setRechazos(utilerias.getInteger(map.get("VOLUMEN_RECHAZOS")));

		}
		if(TVL.equals(beanReqReceptoresSPID.getOpcion())){
			saldoTotal.setTopoVLiquidadas(utilerias.getBigDecimal(map.get("MONTO_TOPO_V_LIQUIDADAS")));
			volumenTotal.setTopoVLiquidadas(utilerias.getInteger(map.get(CONT)));
		}else if(TTL.equals(beanReqReceptoresSPID.getOpcion())){
			saldoTotal.setTopoTLiquidadas(utilerias.getBigDecimal(map.get("MONTO_TOPO_T_LIQUIDADAS")));
			volumenTotal.setTopoTLiquidadas(utilerias.getInteger(map.get(CONT)));
		}else if(TTXL.equals(beanReqReceptoresSPID.getOpcion())){
			saldoTotal.setTopoTPorLiquidar(utilerias.getBigDecimal(map.get("MONTO_TOPO_T_POR_LIQUIDAR")));
			volumenTotal.setTopoTPorLiquidar(utilerias.getInteger(map.get(CONT)));
		}else if(R.equals(beanReqReceptoresSPID.getOpcion())){
			saldoTotal.setRechazos(utilerias.getBigDecimal(map.get("MONTO_RECHAZOS")));
			volumenTotal.setRechazos(utilerias.getInteger(map.get(CONT)));
		}
    }

    /**Metodo que permite apartar una operacion
     * @param beanReceptoresSPID Objeto del tipo @see BeanReceptoresSPID
     * @param funcionalidad String con la funcionalidad
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO apartar(BeanReceptoresSPID beanReceptoresSPID, String funcionalidad,ArchitechSessionBean architechSessionBean){
        final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.insertar(QUERY_INSERT_BLOQUEO,
        		   Arrays.asList(new Object[]{
        				   beanReceptoresSPID.getLlave().getFchOperacion(),
        				   beanReceptoresSPID.getLlave().getCveMiInstituc(),
        				   beanReceptoresSPID.getLlave().getCveInstOrd(),
        				   beanReceptoresSPID.getLlave().getFolioPaquete(),
        				   beanReceptoresSPID.getLlave().getFolioPago(),
        				   funcionalidad,
        				   architechSessionBean.getUsuario()
        				   }),
           HelperDAO.CANAL_GFI_DS, this.getClass().getName());
           beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaBitacoraDAO;
     }


     /**Metodo que permite desapartar una operacion
      * @param beanReceptoresSPID Objeto del tipo @see BeanReceptoresSPID
      * @param funcionalidad String con la funcionalidad
      * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
      * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
      **/
      public BeanResGuardaBitacoraDAO desApartar(BeanReceptoresSPID beanReceptoresSPID, String funcionalidad,ArchitechSessionBean architechSessionBean){
         final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
         ResponseMessageDataBaseDTO responseDTO = null;
         try{
            responseDTO = HelperDAO.eliminar(QUERY_DELETE_BLOQUEO,
         		   Arrays.asList(new Object[]{
         				   beanReceptoresSPID.getLlave().getFchOperacion(),
         				   beanReceptoresSPID.getLlave().getCveMiInstituc(),
         				   beanReceptoresSPID.getLlave().getCveInstOrd(),
         				   beanReceptoresSPID.getLlave().getFolioPaquete(),
         				   beanReceptoresSPID.getLlave().getFolioPago(),
         				   funcionalidad,
         				   architechSessionBean.getUsuario()
         				   }),
            HelperDAO.CANAL_GFI_DS, this.getClass().getName());
            beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
            beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
         } catch (ExceptionDataAccess e) {
            showException(e);
            beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
         }
         return beanResGuardaBitacoraDAO;
      }


}