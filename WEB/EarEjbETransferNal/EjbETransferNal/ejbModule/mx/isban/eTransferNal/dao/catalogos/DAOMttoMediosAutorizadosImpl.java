/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMttoMediosAutorizadosImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:38:54 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;


import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasMttoMediosValidar;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;



/**
 * Class DAOMttoMediosAutorizadosImpl.
 *
 * Implementacion del flujo de acceso a los datos 
 * para el catalogo de Medios Autorizados.
 * 
 * @author FSW-Vector
 * @since 11/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMttoMediosAutorizadosImpl extends Architech implements DAOMttoMediosAutorizados{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -72676275001246961L;
	
	
	/** La constante util. */
	private static final UtileriasMttoMediosAut util = UtileriasMttoMediosAut.getInstance();
	
	/** La constante utilValidar. */
	private static final UtileriasMttoMediosValidar utilValidar = UtileriasMttoMediosValidar.getInstance();
	
	/** La variable que contiene informacion con respecto a: response DTO. */
	private ResponseMessageDataBaseDTO responseDTO;

	/** La constante ERROR_SERVICIOS. */
	private static final String ERROR_SERVICIOS = "No se pudo establecer comunicación con los servicios";
	
	/** La constante CONST_WHERE. */
	private static final String CONST_WHERE = "WHERE";
	
	/**
	 * Consulta tabla MA.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean --> El objeto session
	 * @param beanAuto El objeto: bean auto --> Objeto de entrada
	 * @return list --> Objeto de respuesta de la consulta principal
	 */
	
	@Override
	public BeanMttoMediosAutorizadosRes consultaTablaMA(BeanMttoMediosAutoBusqueda bean, ArchitechSessionBean sessionBean,BeanMttoMediosAutorizados beanAuto) {
		/**Se inicializan las variables **/
		BeanMttoMediosAutorizadosRes beanMttMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();		
		List<BeanMttoMediosAutorizadosReq> listaReq = new ArrayList<BeanMttoMediosAutorizadosReq>();
		
		try {
			
			/**variable para los parametros que se mandan en la consulta **/
			List<Object> parametros = new ArrayList<Object>();
			
			/** se trae el total de los registros**/
			int count = totalRegistros(beanAuto);
			/** se asigna el total**/
			beanMttMediosAutorizadosRes.setTotalReg(count);
			/** en caso de que no existan los datos se mandan el mensaje**/
			if(count==0) {
				beanMttMediosAutorizadosRes.setListaBeanMttoMedAuto(new ArrayList<BeanMttoMediosAutorizadosReq>());
				beanMttMediosAutorizadosRes.setCodError(Errores.ED00011V);
				beanMttMediosAutorizadosRes.setMsgError(Errores.DESC_ED00011V);
				beanMttMediosAutorizadosRes.setTipoError(Errores.TIPO_MSJ_INFO);
				return beanMttMediosAutorizadosRes;
			}

			/**Metodo para generar los filtros **/
			String filtroAnd = util.getFiltro(beanAuto, CONST_WHERE);
			
			/**En caso de que si tenga datos, se arma la consulta**/
			String consulta = ConstantesMttoMediosAut.QUERY_PRINCIPAL+ 
					  filtroAnd + ConstantesMttoMediosAut.QUERY_FIN ;
			
			/**asigna el pagianador **/
			parametros.add(bean.getBeanPaginador().getRegIni());
			parametros.add(bean.getBeanPaginador().getRegFin());
			/** consulta el total de los registros**/
			responseDTO = HelperDAO.consultar(consulta,parametros, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**funcion que asigna los datos al bean**/
			listaReq = util.recorrerLista(responseDTO);
			/** se asigna la lista del bean que se muestra en  el from**/
			beanMttMediosAutorizadosRes.setListaBeanMttoMedAuto(listaReq);
			beanMttMediosAutorizadosRes.setCodError(Errores.OK00000V);
			
		} catch (ExceptionDataAccess e) {
			/**Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios**/
        	showException(e);
        	beanMttMediosAutorizadosRes.setCodError(Errores.EC00011B);
        	beanMttMediosAutorizadosRes.setMsgError(ERROR_SERVICIOS);
		}
			
		/**objetoq ue se regresa **/
		return beanMttMediosAutorizadosRes;
	}
	
	
	/**
	 * Total registros.
	 * Metodo que obtiene el total de registros que tiene la consulta
	 *
	 * @param beanAuto El objeto: bean auto
	 * @return Objeto int --> Total de registros encontrados
	 * @throws ExceptionDataAccess La exception data access --> Lanzada cuando ocurre un error dentro de este flujo
	 */
	private int totalRegistros(BeanMttoMediosAutorizados beanAuto) throws ExceptionDataAccess {
		/** Instancia que guarda la respuesta de la consulta de BD. */
		responseDTO = null;	
		List<HashMap<String,Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		int cont =0;
		
		/**Metodo para generar los filtros **/
		String filtroAnd = util.getFiltro(beanAuto, CONST_WHERE);
				
		/**Se obtiene la consulta**/
		String consultaB =ConstantesMttoMediosAut.QUERY_TOTAL_INI+ ConstantesMttoMediosAut.QUERY+ 
						  filtroAnd + ConstantesMttoMediosAut.QUERY_TOTAL_FIN ;
		
		/**varaible para los parametros **/
		List<Object> param = new ArrayList<Object>();
		
		/**sE CONSULTA EL TOTAL DE REGISTROS PARA EL PAGINADOR**/
		responseDTO = HelperDAO.consultar(consultaB,param,HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	          list = responseDTO.getResultQuery();
	          Map<String,Object> map = list.get(0);
	          /** se asigna el total**/
	          cont = utilerias.getInteger(map.get("CONT"));
	          
    	  }
		/**Se regresa el total de registros encontrados */
		return cont;	
	}
	
	


	
	 
	/**
	 * Consulta listas.
	 *
	 * @param beanMttoRes El objeto: bean mtto medios autorizados res --> Objeto con informacion de consulta
	 * @param consulta El objeto: consulta --> Parametro de tipo de consulta para llenado de lista
	 * @param sesion La sesion --> El objeto session
	 * @return El bean mtto medios autorizados res --> Respuesta obtenida
	 */
	@Override
	public BeanMttoMediosAutorizadosRes consultaListas(BeanMttoMediosAutorizadosRes beanMttoRes, String consulta, ArchitechSessionBean sesion) {
		/** inicializan variables**/
		List<BeanCatalogo> listabean = Collections.emptyList();
		responseDTO = null;		
		Map<String, String> mapa = new HashMap<String, String>();
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes(); 
		
		try {
			/** se manda el mama de la consulta**/
			mapa = util.validaQuery(consulta);
			responseDTO = HelperDAO.consultar(mapa.get("query"), Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				/**se recorre la lista **/
				listabean = util.recorrerListaCatalogos(responseDTO,  mapa );
				/**valida los datos**/
				beanMttoMediosAutorizadosRes = utilValidar.validaLista(listabean, beanMttoRes, mapa);
			}
			/**se asigna los codigos de error **/
			beanMttoMediosAutorizadosRes.setCodError(responseDTO.getCodeError());
			beanMttoMediosAutorizadosRes.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanMttoMediosAutorizadosRes.setCodError(Errores.EC00011B);
			beanMttoMediosAutorizadosRes.setMsgError(ERROR_SERVICIOS);
		}
		/** objeto que se regresa**/
		return beanMttoMediosAutorizadosRes;
	}
	
	/**
	 * Operaciones mtto medios.
	 *
	 * @param beanMttoMediosAutorizadosReq El bean mtto medios autorizados req --> Objeto con datos para el flujo
	 * @param tipoOper Parametro tipo oper --> Tipo de operacion ejecutada
	 * @param sesion La sesion --> El objeto session
	 * @return El bean mtto medios autorizados res --> Respuesta
	 */
	@Override
	public BeanMttoMediosAutorizadosRes operacionesMttoMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			String tipoOper, ArchitechSessionBean sesion) {
		/**inicializacion de las varaibles**/
		BeanMttoMediosAutorizadosRes response = new BeanMttoMediosAutorizadosRes();
		responseDTO = null;
		List<Object> params = null;
		
		try {
			/** pregunta si la accion es insertar o acatualiar para traer los parametros**/
			if(ConstantesMttoMediosAut.TYPE_INSERT.equalsIgnoreCase(tipoOper)|| ConstantesMttoMediosAut.TYPE_UPDATE.equalsIgnoreCase(tipoOper)) {
				params = utilValidar.addParams(beanMttoMediosAutorizadosReq, tipoOper);
			}
			/** funcion que hace la accion**/
			response = continuarAccion(tipoOper, beanMttoMediosAutorizadosReq,params);

						
			
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(ERROR_SERVICIOS);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}		
		return response;
	}
	
	
	
	/**
	 * Continuar accion.
	 * 
	 * Metodo que realiza el registro del nuevo registro y la actualizacion
	 *
	 * @param tipoOper El objeto: tipo oper --> Tipo de operacion 
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados req
	 * @param params El objeto: params --> Lista de parametros
	 * @return Objeto bean mtto medios autorizados res --> Objeto de respuesta a la operacion
	 * @throws ExceptionDataAccess La exception data access --> Lanzada cuando ocurre un error dentro del flujo
	 */
	private BeanMttoMediosAutorizadosRes continuarAccion(String tipoOper, BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			List<Object> params) throws ExceptionDataAccess {
		/**inicializacion de las varaibles**/
		BeanMttoMediosAutorizadosRes response = new BeanMttoMediosAutorizadosRes();
		/**si es insertar entra **/
		if(ConstantesMttoMediosAut.TYPE_INSERT.equalsIgnoreCase(tipoOper)) {
			/** funcion que busca si existe algun registro**/
			int existe = exiteRegistros(beanMttoMediosAutorizadosReq);
			/** en caso de no existir lo inserta**/
			
			if(existe==0)	{
				/** trae la consulta para insertar**/
				String tipoQuery = ConstantesMttoMediosAut.QUERY_INSERT;
				
				responseDTO = HelperDAO.insertar(tipoQuery, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName());					
			}else {			
				/**si existe manda el mensaje **/
				response.setCodError(Errores.ED00130V);
				response.setMsgError(Errores.DESC_ED00130V);
				response.setTipoError(Errores.TIPO_MSJ_INFO);
			}
		}else /**si es actualizar entra **/ 
		if(ConstantesMttoMediosAut.TYPE_UPDATE.equalsIgnoreCase(tipoOper)) {
			/**trae la consulta para acutalizar **/
			String tipoQuery = ConstantesMttoMediosAut.QUERY_UPDATE;
			

			
			responseDTO = HelperDAO.actualizar(tipoQuery, params, HelperDAO.CANAL_GFI_DS, this.getClass().getName());						
		}else { /** en caso de ser eliminar**/
			/** recorre la lista para ver cuales son seleccionados y elinarlos**/
			for(BeanMttoMediosAutorizados bean:beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()){		
				/**funcion para eliminar **/
		      	recorrerListaEliminar(bean);			      	 
			}
			/**codigo de exito **/
			response.setCodError(Errores.OK00004V);
			response.setTipoError(Errores.TIPO_MSJ_ALERT);
			
			return response;
		}
		
		
		if(responseDTO != null && ("DAE000").equals(responseDTO.getCodeError()) && !("ED00130V").equals(response.getCodError())  ){
			BeanMttoMediosAutorizadosReq updateBean= new BeanMttoMediosAutorizadosReq();
			updateBean= consultaRegistro(beanMttoMediosAutorizadosReq, ConstantesMttoMediosAut.TYPE_INSERT);
			response.setBeanMttoMediosAutoReq(updateBean);				
			response.setCodError(Errores.OK00000V);
			response.setMsgError("Transacci\u00F3n Exitosa");
			response.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		
		return response;
	}
	
	
	/**
	 * Recorrer lista eliminar.
	 * Metodo que rrecorre la lista y los elimina segun el seleccionado
	 *
	 * @param bean El objeto: bean --> OBjeto de entrada con la informacion
	 * @throws ExceptionDataAccess La exception data access --> Lanzada cuando al elimnar ocurre un error
	 */
	private void recorrerListaEliminar(BeanMttoMediosAutorizados bean) throws ExceptionDataAccess {
		responseDTO = new ResponseMessageDataBaseDTO();
		/**si esta seleccionado entra **/
    	  if(bean.isSeleccionado()){    	
    		  /** trata los paramentros para enviarlos**/
    		List<Object> lst = new ArrayList<Object>(); 
    		lst.add(utilValidar.cortador(bean.getCveMedioEnt()));
	  		lst.add(utilValidar.cortador(bean.getCveTransfe()));
	  		lst.add(utilValidar.cortador(bean.getCveOperacion()));
	  		lst.add(utilValidar.cortador(bean.getCveMecanismo())); 
	  		/**consulta para eliminar **/
    		String tipoQuery = ConstantesMttoMediosAut.QUERY_DELETE;
    		/**realiza la eliminacion en BD **/
			responseDTO = HelperDAO.eliminar(tipoQuery, lst, HelperDAO.CANAL_GFI_DS, this.getClass().getName());	
    	  }
	}

	
	
	/**
	 * Consulta registro.
	 * Metodo que realiza la consulta de un registro para su actualización 
	 * y lo muestra en la pantalla para editar.
	 *
	 * @param beanMtto El objeto: bean mtto --> Dato de entrada
	 * @param accion El objeto: accion --> Tipo de accion
	 * @return Objeto bean mtto medios autorizados req --> Respuesta
	 * @throws ExceptionDataAccess La exception data access   --> Lanzada cuando ocurre un error dentro del flujo
	 */
	private BeanMttoMediosAutorizadosReq consultaRegistro(BeanMttoMediosAutorizadosReq beanMtto,String accion) throws ExceptionDataAccess{
		BeanMttoMediosAutorizadosReq beanMttRes = new BeanMttoMediosAutorizadosReq();
		
		responseDTO = null;
		List<HashMap<String,Object>> list = null;		
		
			List<Object> pmt = new ArrayList<Object>();
			pmt.add(utilValidar.cortador(beanMtto.getCveMedioEnt()));
			pmt.add(utilValidar.cortador(beanMtto.getCveTransfe()));
			pmt.add(utilValidar.cortador(beanMtto.getCveOperacion()));
			pmt.add(utilValidar.cortador(beanMtto.getCveMecanismo()));
			
			responseDTO = HelperDAO.consultar(ConstantesMttoMediosAut.QUERY_ID_CONSULTA,pmt, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();				
				 Map<String,Object> map = list.get(0);
				 beanMttRes = util.asignarValores(map); 				
				 beanMttRes.setAccion(accion);
				 if (beanMttRes.getLimiteImporte() != null && !beanMttRes.getLimiteImporte().isEmpty()) {
					 beanMttRes.setLimiteImporte(beanMttRes.getLimiteImporte().replaceAll(",", ""));
				 }
			}
		
		return beanMttRes;
	}
	
	
	
	
	
	/**
	 * Metodo que realiza la busqueda de un registro para saber si existe o no
	 *
	 * @param beanMtto El objeto: bean mtto --> Objeto de filtro en consulta
	 * @return Objeto int --> Contador encontrado
	 * @throws ExceptionDataAccess La exception data access  --> Lanzada cuando ocurre un error dentro del flujo
	 */
	private int exiteRegistros(BeanMttoMediosAutorizadosReq beanMtto) throws ExceptionDataAccess {
		/** Instancia que guarda la respuesta de la consulta de BD. */
		responseDTO = null;	
		List<HashMap<String,Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		int cont =0;
		List<Object> paramet = new ArrayList<Object>();
				
		paramet = utilValidar.addParams(beanMtto, "exist");
		
		
		/**sE CONSULTA EL TOTAL DE REGISTROS PARA EL PAGINADOR**/
		responseDTO = HelperDAO.consultar(ConstantesMttoMediosAut.QUERY_EXIST, paramet, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	          list = responseDTO.getResultQuery();
	          Map<String,Object> map = list.get(0);
	          cont = utilerias.getInteger(map.get("CONT"));
	          
    	  }
			
		return cont;	
	}
	
	
	
	

   
	/**
	 * Mostrar registro seleccionado.
	 *
	 * @param beanMtto the bean mtto --> Objeto con informacion del registro
	 * @param sesion the sesion --> El objeto session
	 * @param accion El objeto: accion --> Parametro de accion al mostrar el registro
	 * @return the bean mtto medios autorizados res --> Respuesta obtenida
	 */
	@Override
	public BeanMttoMediosAutorizadosRes mostrarRegistro(BeanMttoMediosAutorizadosReq beanMtto,
			ArchitechSessionBean sesion,String accion) {
		BeanMttoMediosAutorizadosRes response = new BeanMttoMediosAutorizadosRes();
		
		try {
			BeanMttoMediosAutorizadosReq selectBean= new BeanMttoMediosAutorizadosReq();
			selectBean= consultaRegistro(beanMtto,accion);
			/** Se valida la descripción de oepración */
			if(selectBean != null && ("DEFAULT").equalsIgnoreCase(selectBean.getCveOperacion())) {
					selectBean.setDescOperacion("DEFAULT");				
			}
			response.setBeanMttoMediosAutoReq(selectBean);		
			response.setCodError(Errores.OK00000V);
			response.setMsgError("Transacci\u00F3n Exitosa");
			response.setTipoError(Errores.TIPO_MSJ_ALERT);
		} catch (ExceptionDataAccess e) {
			showException(e);
			response.setCodError(Errores.EC00011B);
			response.setMsgError(ERROR_SERVICIOS);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}		
		return response;
	}

	
	/**
	 * Obtener datos.
	 *
	 * @param beanPaginador El objeto: bean paginador --> Objeto con informacion del paginado
	 * @param architechSessionBean El objeto: architech session bean
	 * @param sortField El objeto: sort field  -->Parametro de ordenamiento
	 * @param sortType El objeto: sort type  -->Parametro de tipo ordenamiento 
	 * @param beanAuto El objeto: bean auto --> Objeto de  entrada auxiliar
	 * @return Objeto bean mtto medios autorizados resOBjeto de salida con la respuesta de la operacion
	 */
	@Override
	public BeanMttoMediosAutorizadosRes obtenerDatos(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean,
			String sortField, String sortType,BeanMttoMediosAutorizados beanAuto) {
		BeanMttoMediosAutorizadosRes beanMttoRes = new BeanMttoMediosAutorizadosRes();
		responseDTO = null;
		List<BeanMttoMediosAutorizadosReq> listaMtto = null;
		
		/**Metodo para generar los filtros **/
		String filtroAnd = util.getFiltro(beanAuto, CONST_WHERE);
		
		/**En caso de que si tenga datos, se arma la consulta**/
		String consulta = ConstantesMttoMediosAut.QUERY_PRINCIPAL+ 
				  filtroAnd + ConstantesMttoMediosAut.QUERY_FIN ;
		try {
				
			List<Object> params = new ArrayList<Object>();
			params.add(beanPaginador.getRegIni());
			params.add(beanPaginador.getRegFin());
				responseDTO = HelperDAO.consultar(consulta,params, 
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			
			listaMtto = utilValidar.recorrerListaBuscarDto(responseDTO);
			beanMttoRes.setListaBeanMttoMedAuto(listaMtto);
			beanMttoRes.setCodError(responseDTO.getCodeError());
			beanMttoRes.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return beanMttoRes;
	}



	
	
}