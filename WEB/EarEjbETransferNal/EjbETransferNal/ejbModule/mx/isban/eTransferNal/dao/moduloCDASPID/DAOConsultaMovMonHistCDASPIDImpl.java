/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDatosGenerales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaOrdenante;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonCDADetSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCdaDAOSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Session Bean implementation class DAOConsultaMovHistCDAImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaMovMonHistCDASPIDImpl extends DAOConsultaMovMonHistCDASPIDImpl2 implements DAOConsultaMovMonHistCDASPID {

    /** Constante del serial version. */
	private static final long serialVersionUID = -1581076479590719820L;

	/** Constante TIPO_PAGO. */
	private static final String TIPO_PAGO = "TIPO_PAGO";
	
	/** Constante TIPO_PAGO. */
	private static final String FCH_HORA_ABONO ="FCH_HORA_ABONO";
	
	/** * Constante del tipo String que almacena la consulta para obtener la cuenta de regisros. */
	private static final String QUERY_COUNT_MOVIMIENTOS_CDA_SPID =
		" SELECT COUNT(1) CONT FROM TRAN_SPID_CDA_HIS T1  "+
        " WHERE T1.FCH_OPERACION BETWEEN TO_DATE(?, 'dd-MM-yyyy') AND TO_DATE(?, 'dd-MM-yyyy') ";

	/** * Constante del tipo String que almacena la consulta para obtener el listado de movimientos CDA. */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID=

		" SELECT TMP.REFERENCIA, TMP.FCH_OPERACION, "+
		 " TMP.FOLIO_PAQUETE, TMP.FOLIO_PAGO, TMP.CVE_MI_INSTITUC, TMP.CVE_INST_ORD, "+
		 " TMP.NOMBRE_BCO_ORD, TMP.CVE_RASTREO, TMP.NUM_CUENTA_REC, TMP.MONTO, "+
		 " TMP.FCH_HORA_ABONO, TMP.FCH_HORA_ENVIO, TMP.HORA_ENVIO, "+
		 " CASE(TMP.ESTATUS) "+
		 "   WHEN '0' THEN 'PENDIENTE' "+
		 "   WHEN '1' THEN 'ENVIADO' "+
		 "   WHEN '2' THEN 'CONFIRMADO' "+
		 "   WHEN '3' THEN 'PENDIENTE' "+
		 "   WHEN '4' THEN 'CONTINGENCIA' "+
		 "   WHEN '5' then 'RECHAZADO' "+
		 " END CDA, TMP.COD_ERROR, TO_CHAR(TMP.FCH_CDA,'yyyymmddhh24miss') FCH_CDA, TMP.TIPO_PAGO, "+
		 "TMP.FOLIO_PAQCDA,TMP.FOLIO_CDA, TMP.MODALIDAD, TMP.HORA_ABONO, TMP.NOMBRE_ORD, TMP.TIPO_CUENTA_ORD,"+
		 "     TMP.NUM_CUENTA_ORD, TMP.RFC_ORD, TMP.NOMBRE_BCO_REC, TMP.NOMBRE_REC, TMP.TIPO_CUENTA_REC, TMP.RFC_REC,"+
		 "     TMP.NUM_SERIE_CERTIF, TMP.SELLO_DIGITAL, TMP.CLASIF_OPERACION, TMP.CONCEPTO_PAGO, "+
		 "     TMP.ALGORITMO_FIRMA, TMP.PADDING "+
		 "from "+
		 "   (select "+
		 "     ROW_NUMBER() over (order by T1.FCH_OPERACION, T1.HORA_ENVIO, T1.FCH_CDA) RN, "+
		 "     T1.REFERENCIA,T1.FCH_OPERACION, "+
		 "     T1.FOLIO_PAQUETE,T1.FOLIO_PAGO, "+
		 "     T1.CVE_MI_INSTITUC,T1.CVE_INST_ORD, "+
		 "     NVL(T1.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD ,T1.CVE_RASTREO, "+
		 "     T1.NUM_CUENTA_REC,T1.MONTO, "+
		 "     T1.FCH_HORA_ABONO,T1.FCH_HORA_ENVIO,T1.HORA_ENVIO, "+
		 "     T1.ESTATUS, T1.COD_ERROR, T1.FCH_CDA, T1.TIPO_PAGO "+
		 "   FROM TRAN_SPID_CDA_HIS T1 "+
	     "   WHERE T1.FCH_OPERACION between TO_DATE(?,'dd-MM-yyyy') and TO_DATE(?,'dd-MM-yyyy') ";

	/** * Constante del tipo String que almacena la consulta para obtener el listado de movimientos CDA. */
	private static final String QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID_FIN =
	" ) TMP where RN between ? and ? order by TMP.FCH_OPERACION, TMP.HORA_ENVIO,TMP.FCH_CDA ";

	/** Constante del tipo String que almacena la consulta para solicitar la exportacion de todos los registros realizados durante una consulta;. */
	private static final String QUERY_INSERT_CONSULTA_CDA_SPID_EXPORTAR_TODO =
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";

	/** Constante del tipo String que almacena la consulta para solicitar la exportacion de todos los registros realizados durante una consulta;. */
	private static final String COLUMNAS_CONSULTA_MOV_CDA_SPID =
		"Refere,Fecha Ope,Folio paq,Folio pago,Clave SPEI ordenante del abono, Inst Emisora,Cve rastreo,"+
		"Cta Beneficiario,Monto,Fecha Abono,Hr Abono, Hr Envio, Estatus CDA,Cod. Error,Tipo Pago, Folio Paq Cda,Folio Cda,Modalidad,Nombre Ordenante,"+
		"Tipo Cuenta Ordenante,Numero Cuenta Ordenante,Rfc Ordenante, Banco Ordenante, Nombre Receptor,Tipo Cuenta Rec,"+
		"Rfc Rec, Numero Serie Certif,Sello Digital,Clasificacion Operacion, Concepto Pago, Algoritmo Firma, Padding";

	/** Constante para la consulta de exportar todos. */
	private static final String QUERY_CONSULTA_CDA_SPID_EXPORTAR_TODO =

		"SELECT T1.REFERENCIA || ',' || to_char(T1.FCH_OPERACION,'DD/MM/YYYY') || ',' || T1.FOLIO_PAQUETE || ',' || T1.FOLIO_PAGO || ',' ||"+
		" T1.CVE_INST_ORD || ',' || NVL(T1.NOMBRE_BCO_ORD,'') || ',' || T1.CVE_RASTREO || ',' || T1.NUM_CUENTA_REC || ',' || T1.MONTO || ',' || "+
		" to_char(T1.FCH_HORA_ABONO, 'DD/MM/YYYY') || ',' || to_char(T1.FCH_HORA_ABONO, 'HH24:MI:SS') || ',' || to_char(T1.FCH_HORA_ENVIO,'HH24:MI:SS') || ',' || " +
		" CASE(T1.ESTATUS) WHEN '0' THEN 'PENDIENTE' " +
		"WHEN '1' THEN 'ENVIADO' " +
		"WHEN '2' THEN 'CONFIRMADO' " +
		"WHEN '3' THEN 'PENDIENTE' " +
		"WHEN '4' THEN 'CONTINGENCIA' " +
		"WHEN '5' THEN 'RECHAZADO' ELSE '' END" +
		"|| ',' || T1.COD_ERROR || ',' || T1.TIPO_PAGO "+
		"     TMP.FOLIO_PAQCDA || ',' || TMP.FOLIO_CDA || ',' || TMP.MODALIDAD || ',' || TMP.HORA_ABONO || ',' || TMP.NOMBRE_ORD || ',' || TMP.TIPO_CUENTA_ORD || ',' ||"+
		 "     TMP.NUM_CUENTA_ORD || ',' || TMP.RFC_ORD || ',' || TMP.NOMBRE_BCO_REC || ',' || TMP.NOMBRE_REC || ',' || TMP.TIPO_CUENTA_REC || ',' || TMP.RFC_REC || ',' ||"+
		 "     TMP.NUM_SERIE_CERTIF || ',' || TMP.SELLO_DIGITAL || ',' || TMP.CLASIF_OPERACION || ',' || TMP.CONCEPTO_PAGO || ',' || "+
		 "     TMP.ALGORITMO_FIRMA || ',' || TMP.PADDING "+
		"FROM TRAN_SPID_CDA_HIS T1 WHERE ";

	/** * Constante del tipo String que almacena la consulta para obtener el detalle de un movimiento CDA. */
	private static final String QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA_SPID =
	    " SELECT  CDA_HIS.REFERENCIA, CDA_HIS.FCH_OPERACION, CDA_HIS.CVE_INST_ORD, CDA_HIS.CVE_RASTREO, "+
	    " CDA_HIS.NUM_CUENTA_REC, CDA_HIS.MONTO, CDA_HIS.FCH_HORA_ABONO, NVL(CDA_HIS.FCH_HORA_ENVIO,'') FCH_HORA_ENVIO, NVL(CDA_HIS.NOMBRE_BCO_ORD,'') NOMBRE_BCO_ORD, "+
        " CASE(CDA_HIS.ESTATUS) WHEN '0' THEN 'PENDIENTE' "+
        "   WHEN '1' THEN 'ENVIADO'  "+
        "   WHEN '2' THEN 'CONFIRMADO' "+
        "   WHEN '3' THEN 'PENDIENTE'  "+
        "   WHEN '4' THEN 'CONTINGENCIA' "+
        "   WHEN '5' THEN 'RECHAZADO' END CDA, "+
        " CDA_HIS.NOMBRE_ORD, CDA_HIS.NUM_CUENTA_ORD, CDA_HIS.TIPO_CUENTA_ORD,CDA_HIS.RFC_ORD, NVL(CDA_HIS.NOMBRE_BCO_REC,'') NOMBRE_BCO_REC, "+
        " CDA_HIS.TIPO_CUENTA_REC, CDA_HIS.NOMBRE_REC NOMBRE_REC, CDA_HIS.CONCEPTO_PAGO, CDA_HIS.RFC_REC RFC_REC, CDA_HIS.TIPO_PAGO "+
        " FROM TRAN_SPID_CDA_HIS CDA_HIS "+
        " WHERE CDA_HIS.FCH_OPERACION = TO_DATE(?,'dd/mm/yyyy') "+
        " AND CDA_HIS.CVE_MI_INSTITUC  = ? AND CDA_HIS.CVE_INST_ORD = ? "+
        " AND CDA_HIS.FOLIO_PAQUETE = ? AND CDA_HIS.FOLIO_PAGO = ? "+
        " AND CDA_HIS.REFERENCIA = ? AND CDA_HIS.FCH_CDA = TO_DATE(?,'YYYYMMDDHH24MISS') ";

	/**
	 * Metodo que se encarga de realizar la consulta de movimientos historicos CDA.
	 *
	 * @param beanReqConsMovHistCDA  Objeto del tipo @see BeanReqConsMovMonHistCDA
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
	 */
    public BeanResConsMovHistCdaDAOSPID consultarMovHistCDASPID(BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA, ArchitechSessionBean sessionBean) {
    	BeanResConsMovHistCdaDAOSPID resConHistCdaSpidDAO =  new BeanResConsMovHistCdaDAOSPID();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<BeanMovimientoCDASPID> listBeanMovimientoCDASPID =  null;
    	ResponseMessageDataBaseDTO responseDTO = null;
    	String queryParam = null;
    	String query = null;
        try {
        	List<Object> parametros = new ArrayList<Object>();
        	//se agregan los parametros para la consulta
        	parametros.add(beanReqConsMovHistCDA.getFechaOpeInicio());
        	parametros.add(beanReqConsMovHistCDA.getFechaOpeFin());
        	queryParam = armaConsultaParametrizada(parametros,beanReqConsMovHistCDA);
        	
        	//se arma query cuenta movimientos cda spid
        	query = QUERY_COUNT_MOVIMIENTOS_CDA_SPID+queryParam;

        	responseDTO = HelperDAO.consultar(query, parametros,
          			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

        	//se obtiene la cuenta de movimientos cda spid
        	if(responseDTO.getResultQuery()!= null){
        		 list = responseDTO.getResultQuery();
        		for(HashMap<String,Object> map:list){
        			resConHistCdaSpidDAO.setTotalReg(utilerias.getInteger(map.get("CONT")));
        		}
        	}

        	//si el total de registros es mayor a 0
        	if(resConHistCdaSpidDAO.getTotalReg()>0){
        		//se obtiene los registros de inicio y fin de paginacion
	        	parametros.add(beanReqConsMovHistCDA.getPaginador().getRegIni());
	        	parametros.add(beanReqConsMovHistCDA.getPaginador().getRegFin());

	        	//se arma query de consulta movimientos cda spid
	        	query = QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID+queryParam+QUERY_CONSULTA_MOVIMIENTOS_CDA_SPID_FIN;
	      	    responseDTO = HelperDAO.consultar(query,parametros,
	      			  HelperDAO.CANAL_GFI_DS, this.getClass().getName() );

	      	    //si hay resultados de busqueda
	      	    if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	      	    	llenarListaMovimientosCDASPID(resConHistCdaSpidDAO,responseDTO);
	      	    }

	      	    //se obtiene codigoError 
	      	    resConHistCdaSpidDAO.setCodError(responseDTO.getCodeError());
	      	    resConHistCdaSpidDAO.setMsgError(responseDTO.getMessageError());
        	}else{
        		//se envio codigoError query ejecutado exitosamente
        		resConHistCdaSpidDAO.setCodError(Errores.CODE_SUCCESFULLY);
        		resConHistCdaSpidDAO.setMsgError(Errores.OK00000V);
        		listBeanMovimientoCDASPID =  Collections.emptyList();
        		resConHistCdaSpidDAO.setListBeanMovimientoCDA(listBeanMovimientoCDASPID);
        	}
  	} catch (ExceptionDataAccess e) {
  		showException(e);
  		//obtener y setear codigoError EC00011B
  		resConHistCdaSpidDAO.setCodError(Errores.EC00011B);
  		resConHistCdaSpidDAO.setMsgError(Errores.DESC_EC00011B);
  	}
        //Se regresa el bean de respuesta
  		return resConHistCdaSpidDAO;
    }

    /**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResConsMovHistCdaDAOSPID guardarConsultaMovHistExpTodoSPID(BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA, ArchitechSessionBean sessionBean) {

    	BeanResConsMovHistCdaDAOSPID beanResConsMovHistCdaDAO  = new BeanResConsMovHistCdaDAOSPID();
         ResponseMessageDataBaseDTO responseDTO = null;
         Utilerias utilerias = Utilerias.getUtilerias();
         Date fecha = new Date();
         StringBuilder builder = new StringBuilder();
         //construccion nombre de archivo a exportar
         String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
         String nombreArchivo = "CDA_CON_MOV_CDAS_HIS_".concat(fechaActual);
         nombreArchivo = nombreArchivo.concat(".csv");

         //construccion query exportacion movimientos
         builder.append(QUERY_CONSULTA_CDA_SPID_EXPORTAR_TODO);
     	 builder.append(generarConsultaExportarTodoSPID(beanReqConsMovHistCDA, sessionBean));
     	 builder.append(" ORDER BY T1.FCH_OPERACION,T1.FCH_HORA_ENVIO");

         try{
        	 //ejecucion insert peticion de exportacion
         	responseDTO = HelperDAO.insertar(QUERY_INSERT_CONSULTA_CDA_SPID_EXPORTAR_TODO,
         			//se genera lista de parametros
       			   Arrays.asList(new Object[]{
       					   sessionBean.getUsuario(),
       					   sessionBean.getIdSesion(),
       					   "CDA",
       					   "CONSULTA_MOV_HISTORICOS_CDAS",
       					   nombreArchivo,
       					   beanReqConsMovHistCDA.getTotalReg(),
       					   "PE",
       					   builder.toString(),
       					COLUMNAS_CONSULTA_MOV_CDA_SPID}),
       			   HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
         	beanResConsMovHistCdaDAO.setNombreArchivo(nombreArchivo);
         	//se obtiene codigoError Query
         	beanResConsMovHistCdaDAO.setCodError(responseDTO.getCodeError());
         	beanResConsMovHistCdaDAO.setMsgError(responseDTO.getMessageError());
         }catch (ExceptionDataAccess e) {
         	showException(e);
      		//obtener y setear codigoError EC00011B
         	beanResConsMovHistCdaDAO.setCodError(Errores.EC00011B);
         	beanResConsMovHistCdaDAO.setMsgError(Errores.DESC_EC00011B);
		}
		return beanResConsMovHistCdaDAO;
    }

    /**
     * Metodo encargado de consultar el detalle de un movimiento CDA.
     *
     * @param beanReqConsMovCDASPID the bean req cons mov cdaspid
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovDetHistCdaDAO Objeto del tipo @see BeanResConsMovDetHistCdaDAO
     */
    public BeanResConsMovDetHistCdaDAOSPID consultarMovDetHistCDASPID(BeanReqConsMovMonCDADetSPID beanReqConsMovCDASPID,
    		ArchitechSessionBean architechSessionBean) {

    	BeanResConsMovDetHistCdaDAOSPID resConsMovDetHistCdaSpidDAO = new BeanResConsMovDetHistCdaDAOSPID();
    	BeanResConsMovCdaDatosGenerales beanMovCdaSpidDatosGenerales = new BeanResConsMovCdaDatosGenerales();
    	BeanResConsMovCdaOrdenante  beanMovCdaSpidOrdenante = new BeanResConsMovCdaOrdenante();
    	BeanResConsMovCdaBeneficiario beanMovCdaSpidBeneficiario =  new BeanResConsMovCdaBeneficiario();
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
        ResponseMessageDataBaseDTO responseDTO = null;
        
        //se obtiene datos de request
        int numReferencia = Integer.parseInt(beanReqConsMovCDASPID.getReferencia());
    	int cveMiInstitucLink = Integer.parseInt(beanReqConsMovCDASPID.getCveMiInstitucLink());
    	int cveSpeiLink = Integer.parseInt(beanReqConsMovCDASPID.getCveSpeiLink());
    	int folioPaqLink = Integer.parseInt(beanReqConsMovCDASPID.getFolioPaqLink());
    	int folioPagoLink = Integer.parseInt(beanReqConsMovCDASPID.getFolioPagoLink());

    	String fechaCDA = beanReqConsMovCDASPID.getFchCDALink();

        try {
        	//se consulta detalle movimientos cda spid
	       	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_DETALLE_HIST_MOVIMIENTO_CDA_SPID,
	       			Arrays.asList(new Object[]{beanReqConsMovCDASPID.getFechaOpeLink(),cveMiInstitucLink,cveSpeiLink,
	       					folioPaqLink,folioPagoLink,numReferencia,fechaCDA}),
					 HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	       	
	       	//si la consulta devuelve resultados
	   		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	   			//se obtiene lista de movimientos del query
	   			list = responseDTO.getResultQuery();
	   			//se recorre lista de movimientos
	   			for(HashMap<String,Object> map:list){
	   				//se obtiene campos de la lista y se setean en el bean
	   				beanMovCdaSpidDatosGenerales.setReferencia(utilerias.getString(map.get("REFERENCIA")));				//campo Referencia
	   				beanMovCdaSpidDatosGenerales.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));			//campo claveRastreo
	   				beanMovCdaSpidDatosGenerales.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));	//campo fechaOperacion
	   				beanMovCdaSpidDatosGenerales.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));					//campo horaAbono
	   				beanMovCdaSpidDatosGenerales.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));					//campo horaEnvio
	   				beanMovCdaSpidDatosGenerales.setEstatusCDA(utilerias.getString(map.get("CDA")));					//campo Cda
	   				beanMovCdaSpidDatosGenerales.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));					//campo tipoPago
	   				beanMovCdaSpidDatosGenerales.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));	//campo fechaAbono
	   				
	   				beanMovCdaSpidOrdenante.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));					//campo cveInstOrd
	   				beanMovCdaSpidOrdenante.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));			//campo nombreBcoOrd
	   				beanMovCdaSpidOrdenante.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));					//campo nombreOrd
	   				beanMovCdaSpidOrdenante.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));			//campo tipoCuentaOrd
	   				beanMovCdaSpidOrdenante.setCtaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));					//campo numCuentaOrd
	   				beanMovCdaSpidOrdenante.setRfcCurpOrdenante(utilerias.getString(map.get("RFC_ORD")));				//campo RfcOrd
	
	   				beanMovCdaSpidBeneficiario.setNomInstRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));			//campo nombreBcoRec
	   				beanMovCdaSpidBeneficiario.setNombreBened(utilerias.getString(map.get("NOMBRE_REC")));				//campo nombreRec
	   				beanMovCdaSpidBeneficiario.setTipoCuentaBened(utilerias.getString(map.get("TIPO_CUENTA_REC")));		//campo tipoCuentaRec
	   				beanMovCdaSpidBeneficiario.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));				//campo numCuentaRec
	   				beanMovCdaSpidBeneficiario.setRfcCurpBeneficiario(utilerias.getString(map.get("RFC_REC")));			//campo RfcRec
	   				beanMovCdaSpidBeneficiario.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));			//campo conceptoPago
	   				beanMovCdaSpidBeneficiario.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));				//campo monto
	   				
	   				resConsMovDetHistCdaSpidDAO.setBeanMovCdaDatosGenerales(beanMovCdaSpidDatosGenerales);
	   				resConsMovDetHistCdaSpidDAO.setBeanConsMovCdaOrdenante(beanMovCdaSpidOrdenante);
	   				resConsMovDetHistCdaSpidDAO.setBeanConsMovCdaBeneficiario(beanMovCdaSpidBeneficiario);
	
	   				resConsMovDetHistCdaSpidDAO.setCodError(responseDTO.getCodeError());
	   				resConsMovDetHistCdaSpidDAO.setMsgError(responseDTO.getMessageError());
	     		  }
	   		}
       	} catch (ExceptionDataAccess e) {
       		showException(e);
      		//obtener y setear codigoError EC00011B
       		resConsMovDetHistCdaSpidDAO.setCodError(Errores.EC00011B);
       		resConsMovDetHistCdaSpidDAO.setMsgError(Errores.DESC_EC00011B);
       	}

       return resConsMovDetHistCdaSpidDAO;
    }

    /**
     * Metodo para generar la consulta de exportar todo.
     *
     * @param reqConsMovHistCDASPID the req cons mov hist cdaspid
     * @param sessionBean the session bean
     * @return String Objeto del tipo @see String
     */
    public String generarConsultaExportarTodoSPID(BeanReqConsMovMonHistCDASPID reqConsMovHistCDASPID,
    		ArchitechSessionBean sessionBean){
    	StringBuilder builder = new StringBuilder();
    	String vacio = "";
    	String estatusCDA = "C";

    	//si fechaOperacionInicio tiene un valor valido
    	if(reqConsMovHistCDASPID.getFechaOpeInicio()!= null && reqConsMovHistCDASPID.getFechaOpeFin()!= null){
	    	builder.append(" t1.FCH_OPERACION BETWEEN TO_DATE('");
	    	builder.append(reqConsMovHistCDASPID.getFechaOpeInicio());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("') AND TO_DATE('");
	    	builder.append(reqConsMovHistCDASPID.getFechaOpeFin());
	    	builder.append("','");
	    	builder.append(Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    	builder.append("')");
    	}

    	//si operacionContigencia tiene un valor valido
    	if(reqConsMovHistCDASPID.getOpeContingencia()){
        	builder.append("AND T1.MODALIDAD = '");
        	builder.append(estatusCDA);
        	builder.append("'");
    	}
    	//si cveSpeiOrdenanteAbono tiene un valor valido
    	if(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono() != null && (!vacio.equals(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono()))){
    		builder.append(" AND T1.CVE_INST_ORD = ") ;
    		builder.append(reqConsMovHistCDASPID.getCveSpeiOrdenanteAbono());
    	}

    	//si horaAbonoInicio tiene un valor valido
    	if(reqConsMovHistCDASPID.getHrAbonoIni() != null && reqConsMovHistCDASPID.getHrAbonoFin() != null &&
    			(!vacio.equals(reqConsMovHistCDASPID.getHrAbonoIni())) && (!vacio.equals(reqConsMovHistCDASPID.getHrAbonoFin())) ){
			  builder.append("AND T1.HORA_ABONO  between ");
		      builder.append(reqConsMovHistCDASPID.getHrAbonoIni().replaceAll(":", ""));
		      builder.append(" and ");
		      builder.append(reqConsMovHistCDASPID.getHrAbonoFin().replaceAll(":", ""));
    	}

	  	builder.append(generarConsultaExportarTodo2(reqConsMovHistCDASPID));
    	return builder.toString();
    }

    /**
     * Llenar lista movimientos cdaspid.
     *
     * @param resConHistCdaSpidDAO the res con hist cda spid dao
     * @param responseDTO the response dto
     */
    private void llenarListaMovimientosCDASPID(BeanResConsMovHistCdaDAOSPID resConHistCdaSpidDAO, ResponseMessageDataBaseDTO responseDTO){
    	List<BeanMovimientoCDASPID> listBeanMovimientoCDASPID = new ArrayList<BeanMovimientoCDASPID>();
    	List<HashMap<String,Object>>list = responseDTO.getResultQuery();
    	for(HashMap<String,Object> map:list){
    		listBeanMovimientoCDASPID.add(descomponeConsulta(map));
    	}
    	resConHistCdaSpidDAO.setListBeanMovimientoCDA(listBeanMovimientoCDASPID);
    }

    /**
     * Metodo que genera un objeto del tipo BeanMovimientoCDA a partir de la respuesta de la consulta.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanMovimientoCDA Objeto del tipo BeanMovimientoCDA
     */
    private BeanMovimientoCDASPID descomponeConsulta(Map<String,Object> map){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanMovimientoCDASPID beanMovimientosCDASPID = new BeanMovimientoCDASPID();
    	beanMovimientosCDASPID.setReferencia(utilerias.getString(map.get("REFERENCIA")));				//campo Referencia
		beanMovimientosCDASPID.setFechaOpe(utilerias.formateaFecha(map.get("FCH_OPERACION"),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));		//campo fechaOperacion
		beanMovimientosCDASPID.setFolioPaq(utilerias.getString(map.get("FOLIO_PAQUETE")));				//campo folioPaquete
		beanMovimientosCDASPID.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));				//campo folioPago
		beanMovimientosCDASPID.setCveSpei(utilerias.getString(map.get("CVE_INST_ORD")));				//campo claveInstitucionOrd
		beanMovimientosCDASPID.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));		//campo cveMiInstituc
		beanMovimientosCDASPID.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));		//campo nombreBCOOrd
		beanMovimientosCDASPID.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));				//campo claveRastreo
		beanMovimientosCDASPID.setCtaBenef(utilerias.getString(map.get("NUM_CUENTA_REC")));				//campo numCuentaRec
		beanMovimientosCDASPID.setMonto(utilerias.formateaDecimales(map.get("MONTO"),Utilerias.FORMATO_DECIMAL_NUMBER_LARGO));				//campo monto
		beanMovimientosCDASPID.setFechaAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));	//campo fechaAbono
		beanMovimientosCDASPID.setHrAbono(utilerias.formateaFecha(map.get(FCH_HORA_ABONO),Utilerias.FORMATO_HH_MM_SS));						//campo horaAbono
		beanMovimientosCDASPID.setHrEnvio(utilerias.formateaFecha(map.get("FCH_HORA_ENVIO"),Utilerias.FORMATO_HH_MM_SS));					//campo fechaHoraEnvio
		beanMovimientosCDASPID.setEstatusCDA(utilerias.getString(map.get("CDA")));						//campo Cda
		beanMovimientosCDASPID.setCodError(utilerias.getString(map.get("COD_ERROR")));					//campo codError
		beanMovimientosCDASPID.setFchCDA(utilerias.getString(map.get("FCH_CDA")));						//campo fechaCda
		beanMovimientosCDASPID.setTipoPago(utilerias.getString(map.get(TIPO_PAGO)));
		beanMovimientosCDASPID.setFolioPaqCda(utilerias.getString(map.get("FOLIO_PAQCDA")));
		beanMovimientosCDASPID.setFolioCda(utilerias.getString(map.get("FOLIO_CDA")));
		beanMovimientosCDASPID.setModalidad(utilerias.getString(map.get("MODALIDAD")));
		beanMovimientosCDASPID.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
		beanMovimientosCDASPID.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
		beanMovimientosCDASPID.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
		beanMovimientosCDASPID.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
		beanMovimientosCDASPID.setNombreBcoRec(utilerias.getString(map.get("NOMBRE_BCO_REC")));
		beanMovimientosCDASPID.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
		beanMovimientosCDASPID.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
		beanMovimientosCDASPID.setRfcRec(utilerias.getString(map.get("RFC_REC")));
		beanMovimientosCDASPID.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
		beanMovimientosCDASPID.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
		beanMovimientosCDASPID.setNumSerieCertif(utilerias.getString(map.get("NUM_SERIE_CERTIF")));
		beanMovimientosCDASPID.setSelloDigital(utilerias.getString(map.get("SELLO_DIGITAL")));
		beanMovimientosCDASPID.setAlgoritmoFirma(utilerias.getString(map.get("ALGORITMO_FIRMA")));
		beanMovimientosCDASPID.setPadding(utilerias.getString(map.get("PADDING")));
		//campo tipoPago

		return beanMovimientosCDASPID;
    }
}