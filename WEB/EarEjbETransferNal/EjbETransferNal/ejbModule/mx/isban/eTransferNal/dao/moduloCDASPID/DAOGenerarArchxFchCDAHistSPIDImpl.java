/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchxFchCDAHistSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Jan 04 18:24:31 CST 2017 jjbeltran  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGenerarArchxFchCDAHistSPIDImpl extends Architech implements DAOGenerarArchxFchCDAHistSPID {

	/**
	 * Constante del Serial version
	 */
	//Constante del Serial version
	private static final long serialVersionUID = -570796511152231897L;
	
	/**
	 * Constante del tipo String que almacena la consulta que calcula el id_peticion
	 */
	//Constante del tipo String que almacena la consulta que calcula el id_peticion
	private static final String QUERY_CONSULTA_CONSULTA_ID_PETICION = 
		" SELECT (NVL(MAX(TO_NUMBER(ID_PETICION)),0)+1) ID_PETICION FROM TRAN_SPID_CTG_CDA";
	
	/**
	 * Constante que almacena el query de insert en la tabla TRAN_SPEI_CTG_CDA
	 */
	//Constante que almacena el query de insert en la tabla TRAN_SPEI_CTG_CDA
	private static final String QUERY_INSERT_TRAN_SPEI_CTG_CDA = 
		" INSERT INTO TRAN_SPID_CTG_CDA (FCH_OPERACION,ID_PETICION, TIPO_PETICION, USUARIO, SESION, MODULO, NOMBRE_ARCHIVO, FCH_HORA, ESTATUS) "+
	    " VALUES (sysdate,?, ?, ?, ?, ?, ?, TO_DATE(?,'dd-MM-yyyy'), ?)";
	
	/**
	 * Constante que almacena el query de la fecha de operacion
	 */
	//Constante que almacena el query de la fecha de operacion
	private static final String QUERY_FECHA_OPERACION =
	" SELECT FCH_OPERACION FROM TRAN_SPID_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	" WHERE CV_PARAMETRO = 'ENTIDAD_SPID'),40014) CV_VALOR FROM DUAL) TMP "+
	" WHERE CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
   /**Metodo DAO para registrar el archivo a generar del CDA historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDAO
    *    */
	//Metodo DAO para registrar el archivo a generar del CDA historico
    public BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean){
    	final BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = new BeanResGenArchCDAHistDAO();
       List<HashMap<String,Object>> list = null;
       ResponseMessageDataBaseDTO responseDTO = null;
       Utilerias utilerias = Utilerias.getUtilerias();
       Integer idPeticion = -1;
       try{
    	   //Se hace la consulta a la base de datos
    	   responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CONSULTA_ID_PETICION, 
    			   Collections.emptyList(), HelperDAO.CANAL_GFI_DS, this.getClass().getName() );
    	   list = responseDTO.getResultQuery();
    	   //Se valida que la respuesta no sea nula ni vacia
      	   if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
      	      for(HashMap<String,Object> map:list){
      			idPeticion = utilerias.getInteger(map.get("ID_PETICION"));
      		  }
      	      
      	      //Se hace el insert en la base de datos
              responseDTO = HelperDAO.insertar(QUERY_INSERT_TRAN_SPEI_CTG_CDA, Arrays.asList(
            		  new Object[]{idPeticion,"D",architechSessionBean.getUsuario(),
            				  architechSessionBean.getIdSesion(),
        					  "GENARCCDAH", beanReqGenArchCDAHist.getNombreArchivo(),
        					  beanReqGenArchCDAHist.getFechaOp(),
            		  				"PE"}
              ),
	          HelperDAO.CANAL_GFI_DS, this.getClass().getName());
              
              //Se setean los datos en el bean de respuesta
              beanResGenArchCDAHistDAO.setCodError(responseDTO.getCodeError());
              beanResGenArchCDAHistDAO.setIdPeticion(idPeticion.toString());
              beanResGenArchCDAHistDAO.setMsgError(responseDTO.getMessageError());
	          
      	   }
       } catch (ExceptionDataAccess e) {
    	   //Se controla la excepcion
    	   showException(e);
    	   beanResGenArchCDAHistDAO.setCodError(Errores.EC00011B);
    	   beanResGenArchCDAHistDAO.setMsgError(Errores.DESC_EC00011B);
       }
       
       //Devuelve el bean
       return beanResGenArchCDAHistDAO;
    }

    /**Metodo DAO para obtener la fecha de operacion
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
     *    */
    //Metodo DAO para obtener la fecha de operacion
    public BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean){
    	
    	//Creacion de objetos de respuesta
    	final BeanResConsFechaOpDAO beanResConsFechaOpDAO = new BeanResConsFechaOpDAO();
    	
    	//Creacion de objetos de apoyo
    	List<HashMap<String,Object>> list = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
    	ResponseMessageDataBaseDTO responseDTO = null;
    	try {
    		//Se hace la consulta a la base de datos
    		responseDTO = HelperDAO.consultar(QUERY_FECHA_OPERACION, Collections.emptyList(), 
    				HelperDAO.CANAL_GFI_DS, this.getClass().getName());
    		
    		//Se valida que no sea nula ni vacia la respuesta
    		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
    			list = responseDTO.getResultQuery();
    			for(HashMap<String,Object> map:list){
    				beanResConsFechaOpDAO.setCodError(responseDTO.getCodeError());
    				beanResConsFechaOpDAO.setMsgError(responseDTO.getMessageError());
    				beanResConsFechaOpDAO.setFechaOperacion(
    						utilerias.formateaFecha(map.get("FCH_OPERACION"), 
    								Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
    			}
    		}
    	} catch (ExceptionDataAccess e) {
    		//Se controla la excepcion
    		showException(e);
    		beanResConsFechaOpDAO.setCodError(Errores.EC00011B);
    		beanResConsFechaOpDAO.setMsgError(Errores.DESC_EC00011B);
    	}
    	
    	//Devuelve el bean
    	return beanResConsFechaOpDAO;
    }

}
