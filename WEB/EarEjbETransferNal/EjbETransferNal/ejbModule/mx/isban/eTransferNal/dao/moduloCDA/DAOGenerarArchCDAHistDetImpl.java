/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHistDetImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 15 13:47:03 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDetDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga obtener la informacion para la
 * funcionalidad de Generar Archivo CDA Historico Detalle
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGenerarArchCDAHistDetImpl extends Architech implements
		DAOGenerarArchCDAHistDet {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = -5193975371357151416L;

	/**
	 * Constante privada que almacena el query de la funcionalidad de la
	 * generacion de archivos CDA historico detalle
	 */
	private static final String QUERY_CONS_GEN_ARCH_CDA_HIST = " SELECT TRIM(TMP.ID_PETICION) ID_PETICION, TMP.CVE_RASTREO, TMP.NUM_CUENTA_REC, TMP.MONTO, "
			+ " TMP.NOMBRE_BCO_ORD, FCH_OPERACION, CVE_MI_INSTITUC, CVE_INST_ORD, FOLIO_PAQUETE, FOLIO_PAGO, TMP.CONT "
			+ " FROM (SELECT TMP.*, ROWNUM AS RN "
			+ " FROM ( "
			+ " SELECT CDA.ID_PETICION, CDA.CVE_RASTREO, CDA.NUM_CUENTA_REC, CDA.MONTO, "
			+ " CDA.NOMBRE_BCO_ORD, TO_CHAR(FCH_OPERACION,'DDMMYYYY') FCH_OPERACION, CDA.CVE_MI_INSTITUC, CDA.CVE_INST_ORD," 
		    + " CDA.FOLIO_PAQUETE, CDA.FOLIO_PAGO, CONTADOR.CONT "
			+ " FROM ( "
			+ "       SELECT COUNT(1) CONT "
			+ " FROM TRAN_SPEI_DET_CDA CDA "
			+ " WHERE TRIM(ID_PETICION)= ? AND ELIMINADO != 'S' ) CONTADOR, "
			+ " TRAN_SPEI_DET_CDA CDA "
			+ " WHERE TRIM(CDA.ID_PETICION) = ? AND ELIMINADO != 'S'"
			+ " ORDER BY ID_PETICION, CVE_RASTREO) TMP) TMP "
			+ " WHERE RN BETWEEN ? AND ? "
			+ " ORDER BY ID_PETICION, CVE_RASTREO ";

	/**
	 * Constante privada que almacena el query que consulta el numero de
	 * registros en el archivo y numero de registros encontrados
	 */
	private static final String QUERY_CONSULTA_NO_REG_ARCH_ENCON = " SELECT TRIM(ID_PETICION) ID_PETICION,OPE_X_GEN,OPE_GEN FROM TRAN_SPEI_CTG_CDA CDA WHERE TRIM(ID_PETICION) = ? ";

	/**
	 * Constante privada que almacena el query que actualiza el numero de
	 * registros encontrados
	 */
	private static final String QUERY_UPDATE_NUM_REG_ARCHIVO = " UPDATE TRAN_SPEI_CTG_CDA SET OPE_GEN = OPE_GEN-?  WHERE TRIM(ID_PETICION) = ? ";

	/**
	 * Constante privada que almacena el query que sirve para generar archivo
	 */
	private static final String QUERY_GENERA_ARCHIVO_CDA_HIST_DET = " UPDATE TRAN_SPEI_CTG_CDA SET TIPO_PETICION ='H', FCH_HORA = SYSDATE, "
			+ "   ESTATUS='PE', OPE_GEN=0  WHERE TRIM(ID_PETICION) = TRIM(?) ";

	/**
	 * Constante privada que almacena el query que actualiza los registros
	 * marcados
	 */
	private static final String QUERY_MARCADO_ELIMINADO = " UPDATE TRAN_SPEI_DET_CDA SET ELIMINADO = 'S' " +
			" WHERE TRIM(ID_PETICION) = TRIM(?) AND TRIM(CVE_RASTREO) = TRIM(?) AND "+
	        " TO_CHAR(FCH_OPERACION,'DDMMYYYY') = ? AND CVE_MI_INSTITUC = ? AND "+
			" CVE_INST_ORD  = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ? ";

	/**
	 * Metodo DAO que sirve para consultar la informacion de la generacion de
	 * archivos CDA historico detalle
	 * 
	 * @param beanReqConsGenArchCDAHistDet
	 *            Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsGenArchHistDetDAO Objeto del tipo
	 *         BeanResConsGenArchHistDetDAO
	 */
	public BeanResConsGenArchHistDetDAO consultaGenArchHistDet(
			BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet,
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) {
		final BeanResConsGenArchHistDetDAO beanResConsGenArchHistDetDAO = new BeanResConsGenArchHistDetDAO();
		List<HashMap<String, Object>> list = null;
		List<BeanGenArchHistDet> listBeanGenArchHistDet = Collections
				.emptyList();
		ResponseMessageDataBaseDTO responseDTO = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			responseDTO = HelperDAO.consultar(QUERY_CONS_GEN_ARCH_CDA_HIST,
					Arrays.asList(new Object[] {
							beanReqConsGenArchCDAHistDet.getIdPeticion(),
							beanReqConsGenArchCDAHistDet.getIdPeticion(),
							beanPaginador.getRegIni(),
							beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				listBeanGenArchHistDet = new ArrayList<BeanGenArchHistDet>();
				for (HashMap<String, Object> map : list) {
					BeanGenArchHistDet beanGenArchHistDet = new BeanGenArchHistDet();
					beanGenArchHistDet.setIdPeticion(utilerias.getString(map.get("ID_PETICION")));
					beanGenArchHistDet.setClaveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
					beanGenArchHistDet.setNumCuentaBeneficiario(utilerias.getString(map.get("NUM_CUENTA_REC")));
					beanGenArchHistDet.setMontoPago(utilerias.formateaDecimales(map.get("MONTO"),
									Utilerias.FORMATO_DECIMAL_NUMBER));
					beanGenArchHistDet.setNomInstEmisora(utilerias.getString(map.get("NOMBRE_BCO_ORD")));
					beanGenArchHistDet.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
					beanGenArchHistDet.setCveMiInstitucion(utilerias.getString(map.get("CVE_MI_INSTITUC")));
					beanGenArchHistDet.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
					beanGenArchHistDet.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
					beanGenArchHistDet.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
					
					
					listBeanGenArchHistDet.add(beanGenArchHistDet);
					beanResConsGenArchHistDetDAO.setTotalReg(utilerias
							.getInteger(map.get("CONT")));
				}
				beanResConsGenArchHistDetDAO
						.setListBeanGenArchHistDet(listBeanGenArchHistDet);
				beanResConsGenArchHistDetDAO.setCodError(Errores.OK00000V);
			}

			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_NO_REG_ARCH_ENCON,
					Arrays.asList(new Object[] { beanReqConsGenArchCDAHistDet
							.getIdPeticion() }), HelperDAO.CANAL_GFI_DS, this
							.getClass().getName());
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					beanResConsGenArchHistDetDAO.setNumRegEncontrados(utilerias
							.formateaDecimales(map.get("OPE_GEN"),
									Utilerias.FORMATO_NUMBER));
					beanResConsGenArchHistDetDAO.setNumRegArchivo(utilerias
							.formateaDecimales(map.get("OPE_X_GEN"),
									Utilerias.FORMATO_NUMBER));
				}
			}
			beanResConsGenArchHistDetDAO
					.setListBeanGenArchHistDet(listBeanGenArchHistDet);
			beanResConsGenArchHistDetDAO
					.setCodError(responseDTO.getCodeError());
			beanResConsGenArchHistDetDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResConsGenArchHistDetDAO.setCodError(Errores.EC00011B);

		}
		return beanResConsGenArchHistDetDAO;
	}

	/**
	 * Metodo que sirve para marcar los registros como eliminados
	 * 
	 * @param beanReqConsGenArchCDAHistDet
	 *            Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResElimSelecArchCDAHistDetDAO Objeto del tipo
	 *         BeanResElimSelecArchCDAHistDetDAO
	 * */
	public BeanResElimSelecArchCDAHistDetDAO elimSelGenArchCDAHistDet(
			BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet,
			ArchitechSessionBean architechSessionBean) {
		final BeanResElimSelecArchCDAHistDetDAO beanResElimSelecArchCDAHistDetDAO = new BeanResElimSelecArchCDAHistDetDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		int cont = 0;
		try {
			for (BeanGenArchHistDet beanGenArchHistDet : beanReqConsGenArchCDAHistDet
					.getListBeanGenArchHistDet()) {
				responseDTO = HelperDAO.actualizar(QUERY_MARCADO_ELIMINADO,
						Arrays.asList(new Object[] {
								beanGenArchHistDet.getIdPeticion(),
								beanGenArchHistDet.getClaveRastreo(),
								beanGenArchHistDet.getFchOperacion(),
								beanGenArchHistDet.getCveMiInstitucion(),
								beanGenArchHistDet.getCveInstOrd(),
								beanGenArchHistDet.getFolioPaquete(),
								beanGenArchHistDet.getFolioPago()
						}),
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				beanResElimSelecArchCDAHistDetDAO.setCodError(responseDTO
						.getCodeError());
				beanResElimSelecArchCDAHistDetDAO.setMsgError(responseDTO
						.getMessageError());
				cont++;
			}

			responseDTO = HelperDAO.actualizar(QUERY_UPDATE_NUM_REG_ARCHIVO,
					Arrays.asList(new Object[] { Integer.valueOf(cont),
							beanReqConsGenArchCDAHistDet.getIdPeticion() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResElimSelecArchCDAHistDetDAO.setCodError(responseDTO
					.getCodeError());
			beanResElimSelecArchCDAHistDetDAO.setMsgError(responseDTO
					.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResElimSelecArchCDAHistDetDAO.setCodError(Errores.EC00011B);
		}
		return beanResElimSelecArchCDAHistDetDAO;
	}

	/**
	 * Metodo que sirve para mandar a generar el archivo de CDA historico
	 * detalle
	 * 
	 * @param beanReqGenArchCDAHistDet
	 *            Objeto del tipo @see BeanReqGenArchCDAHistDet
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResGenArchCDAHistDetDAO Objeto del tipo
	 *         BeanResGenArchCDAHistDetDAO
	 */
	public BeanResGenArchCDAHistDetDAO generarArchCDAHistDet(
			BeanReqGenArchCDAHistDet beanReqGenArchCDAHistDet,
			ArchitechSessionBean architechSessionBean) {
		final BeanResGenArchCDAHistDetDAO beanResGenArchCDAHistDetDAO = new BeanResGenArchCDAHistDetDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			responseDTO = HelperDAO.actualizar(
					QUERY_GENERA_ARCHIVO_CDA_HIST_DET, Arrays
							.asList(new Object[] { beanReqGenArchCDAHistDet
									.getIdPeticion() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			beanResGenArchCDAHistDetDAO.setCodError(responseDTO.getCodeError());
			beanResGenArchCDAHistDetDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResGenArchCDAHistDetDAO.setCodError(Errores.EC00011B);
		}
		return beanResGenArchCDAHistDetDAO;
	}

}
