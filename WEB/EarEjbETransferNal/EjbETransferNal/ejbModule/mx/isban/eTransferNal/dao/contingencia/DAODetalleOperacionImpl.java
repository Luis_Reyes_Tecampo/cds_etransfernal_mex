/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAODetalleOperacionImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/10/2018 05:39:24 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.dao.contingencia;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.contingencia.BeanDatosGenerales;
import mx.isban.eTransferNal.beans.contingencia.BeanDatosGeneralesExt;
import mx.isban.eTransferNal.beans.contingencia.BeanDatosOrdenante;
import mx.isban.eTransferNal.beans.contingencia.BeanDatosReceptor;
import mx.isban.eTransferNal.beans.contingencia.BeanClaves;
import mx.isban.eTransferNal.beans.contingencia.BeanComentarios;
import mx.isban.eTransferNal.beans.contingencia.BeanDatosOperacion;
import mx.isban.eTransferNal.beans.contingencia.BeanResTranCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanTranMensajeCanales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.contingencia.UtileriasDetalleOperacion;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAODetalleOperacionImpl.
 * 
 * Clase que implementa los metodos necesarios para tener el acceso a la informacion
 *
 * @author FSW-Vector
 * @since 11/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAODetalleOperacionImpl extends Architech implements DAODetalleOperacion{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -7780746050620427915L;
	
	/** La constante CONSULTA INICIAL. */
	private static final String CONSULTA_TODO = "SELECT" 
			+ " NOMBRE_ARCH, SECUENCIA, TIPO_CUENTA_ORD, TIPO_CUENTA_REC, TIPO_CUENTA_REC2, CVE_EMPRESA, CVE_TRANSFE, "
			+ " CVE_MEDIO_ENT, CVE_PTO_VTA, CVE_PTO_VTA_ORD, CVE_DIVISA, CVE_INTERME_ORD, CVE_INTERME_REC, CVE_USUARIO_CAP,"
			+ " CVE_USUARIO_SOL, CVE_OPERACION, NUM_CUENTA_ORD, NUM_CUENTA_REC, NUM_CUENTA_REC2, REFERENCIA_MED, IMPORTE,"
			+ " NOMBRE_ORD, NOMBRE_REC, NOMBRE_REC2, RFC_REC2, CONCEPTO_PAGO2, COMENTARIO1, COMENTARIO2, COMENTARIO3, COMENTARIO4,"
			+ " FCH_CAP_TRANSFER, REFE_TRANSFER, COD_ERROR, ESTATUS_INICIAL, MENSAJE, DIRECCION_IP, BUC_CLIENTE, RFC_ORD, RFC_REC,"
			+ " IMPORTE_IVA, REFE_NUMERICA, UETR_SWIFT, CAMPO_SWIFT1, CAMPO_SWIFT2, REFE_ADICIONAL1, CONT, MONTO,"
			+ " REG FROM ("  
			+ " SELECT TM.* FROM (" 
			+ " SELECT" 
			+ " NOMBRE_ARCH, SECUENCIA, TIPO_CUENTA_ORD, TIPO_CUENTA_REC, TIPO_CUENTA_REC2, CVE_EMPRESA, CVE_TRANSFE, CVE_MEDIO_ENT,"
			+ " CVE_PTO_VTA, CVE_PTO_VTA_ORD, CVE_DIVISA, CVE_INTERME_ORD, CVE_INTERME_REC, CVE_USUARIO_CAP, CVE_USUARIO_SOL, CVE_OPERACION,"
			+ " NUM_CUENTA_ORD, NUM_CUENTA_REC, NUM_CUENTA_REC2, REFERENCIA_MED, IMPORTE, NOMBRE_ORD, NOMBRE_REC, NOMBRE_REC2, RFC_REC2,"
			+ " CONCEPTO_PAGO2, COMENTARIO1, COMENTARIO2, COMENTARIO3, COMENTARIO4, FCH_CAP_TRANSFER, REFE_TRANSFER, COD_ERROR, ESTATUS_INICIAL,"
			+ " MENSAJE, DIRECCION_IP, BUC_CLIENTE, RFC_ORD, RFC_REC, IMPORTE_IVA, REFE_NUMERICA, UETR_SWIFT, CAMPO_SWIFT1, CAMPO_SWIFT2,"
			+ " REFE_ADICIONAL1, CONTADOR.CONT, CONTADOR.MONTO, ROWNUM AS REG" 
			+ " FROM ("  
			+ " SELECT" 
			+ " COUNT(1) CONT, (SUM(IMPORTE) + SUM(IMPORTE_IVA)) MONTO FROM TRAN_MENSAJE_CANALES [FILTRO_TOT] [FILTRO_AND]) CONTADOR," 
			+ " TRAN_MENSAJE_CANALES"  
			+ " [FILTRO_ARCHIVO] [FILTRO_AND]) TM) TM ";
	
	/** La constante CONSULTA_FILTRO. */
	private static final String CONSULTA_FILTRO = " WHERE REG BETWEEN ? AND ? [FILTRO_PAG]";
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO + CONSULTA_FILTRO;
	
	/** La constante WHERE. */
	private static final String WHERE = "WHERE";
	
	/** La constante NOMBRE_ARCH. */
	private static final String NOMBRE_ARCH = "NOMBRE_ARCH"; 
	
	/** La constante AND. */
	private static final String AND = "AND"; 
	
	/** La variable que contiene informacion con respecto a: utilerias detalle operacion. */
	private UtileriasDetalleOperacion utileriasDetalleOperacion = UtileriasDetalleOperacion.getInstancia();
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private Utilerias utilerias = Utilerias.getUtilerias();
	
	
	
	/**
	 * Consultar.
	 *  Metodo para realizar la consulta principal.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param beanPaginador El objeto: bean paginador
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @return Objeto bean res tran canales
	 */
	@Override
	public BeanResTranCanales consultar(ArchitechSessionBean session, String nombreArchivo, String field, String valor,
			BeanPaginador beanPaginador,String nombrePantalla) {
		/** Se declara la lista que contendra el resultado de la operacion **/
		BeanResTranCanales response = new BeanResTranCanales();
		/** Se declara la lista que contendra los registros de la bd **/
		List<BeanTranMensajeCanales> registros = Collections.emptyList();
		/** Se declara el objecto de ayuda para almacenar y extraer los datos **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Se declara el objeto para hacer la operacion de consulta **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Se convierte a mayusculas el nombre del archivo **/
		nombreArchivo = nombreArchivo.toUpperCase();
		/** Declaracion del filtro para realizar una busqueda **/
		String filtroAnd = utileriasDetalleOperacion.getFiltro(field, valor, AND);
		/** Declaracion del filtro para condicionar el nombre de arhivo elegido en la pantalla anterior**/
		String filtroArchivo = utileriasDetalleOperacion.getFiltro(NOMBRE_ARCH, nombreArchivo, WHERE);
		
		/**  Declaracion del filtro para incluir el nombre del archivo en la paginacion **/
		String filtroPag = utileriasDetalleOperacion.getFiltro(NOMBRE_ARCH, nombreArchivo, AND);
		String filtroTot = utileriasDetalleOperacion.getFiltro(NOMBRE_ARCH, nombreArchivo, WHERE);
		/**  Declaracion del filtro para incluir el nombre del archivo en la existencia **/
		/**  Se arma la consulta rempelzando los parametros descritos **/
		String consulta = CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd).replaceAll("\\[FILTRO_ARCHIVO\\]", filtroArchivo).replaceAll("\\[FILTRO_PAG\\]", filtroPag)
				.replaceAll("\\[FILTRO_TOT\\]", filtroTot);
		if(("historica").equals(nombrePantalla)) {
			consulta = consulta.replaceAll("TRAN_MENSAJE_CANALES", "TRAN_MENSAJE_CANALES_HIS");
		}
		/** Se declara una lista para alamcenar los parametros de paginacion**/
		List<Object> params = Collections.emptyList();
		try {
			/** Se invoca al metodo que llena la lista de parametros **/
			params = utileriasDetalleOperacion.agregaParametros(beanPaginador);
			/** Se ejecuta la operacion **/
			responseDTO = HelperDAO.consultar(consulta, params, 
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se valida el resultado de la operacion**/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Se inicializa la lista**/
				registros = new ArrayList<BeanTranMensajeCanales>();
				/** Se asigna el resultado de la operacion**/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre el objecto para extraer los datos **/
				for(HashMap<String, Object> map : queryResponse) {
					/** Se inicializan los Bean necesarios para alamcenar los datos**/
					/** Se declara el objeto principal que coentiene los objetos hijos**/
					BeanTranMensajeCanales reg = new BeanTranMensajeCanales();
					/** Se declaran los objetos que almacenan los datos del mapa **/
					/** bean que contiene los datos del Ordenate**/
					BeanDatosOrdenante beanDatosOrdenante = new BeanDatosOrdenante();
					/** bean que contiene los datos del receptor**/
					BeanDatosReceptor beanDatosReceptor = new BeanDatosReceptor();
					/** bean que contiene los datos de los campos Clave**/
					BeanClaves beanClaves = new BeanClaves();
					/** bean que contiene los datos de Comentarios**/
					BeanComentarios beanComentarios = new BeanComentarios();
					/** bean que contiene los datos generales**/
					BeanDatosGenerales beanDatosGenerales = new BeanDatosGenerales();
					/** bean que contiene los datos de la operacion**/
					BeanDatosOperacion beanDatosOperacion = new BeanDatosOperacion();
					/** bean complementario que contiene los datos generales**/
					BeanDatosGeneralesExt beanDatosGeneralesExt = new BeanDatosGeneralesExt();
					/** Se asignan los datos **/
					beanDatosOrdenante.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
					beanDatosOrdenante.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
					beanDatosOrdenante.setCvePtoVtaOrd(utilerias.getString(map.get("CVE_PTO_VTA_ORD")));
					beanDatosOrdenante.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
					beanDatosOrdenante.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
					beanDatosReceptor.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
					beanDatosReceptor.setTipoCuentaRec2(utilerias.getString(map.get("TIPO_CUENTA_REC2")));
					beanDatosReceptor.setCveIntermeRec(utilerias.getString(map.get("CVE_INTERME_REC")));
					beanDatosReceptor.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
					beanDatosReceptor.setNombreRec2(utilerias.getString(map.get("NOMBRE_REC2")));
					beanDatosReceptor.setRfcRec2(utilerias.getString(map.get("RFC_REC2")));
					beanDatosReceptor.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
					beanDatosReceptor.setNumCuentaRec2(utilerias.getString(map.get("NUM_CUENTA_REC2")));
					beanClaves.setCveEmpresa(utilerias.getString(map.get("CVE_EMPRESA")));
					beanClaves.setCveTransfe(utilerias.getString(map.get("CVE_TRANSFE")));
					beanClaves.setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")));
					beanClaves.setCvePtoVta(utilerias.getString(map.get("CVE_PTO_VTA")));
					beanClaves.setCveDivisa(utilerias.getString(map.get("CVE_DIVISA")));
					beanClaves.setCveUsuarioCap(utilerias.getString(map.get("CVE_USUARIO_CAP")));
					beanClaves.setCveUsuarioSol(utilerias.getString(map.get("CVE_USUARIO_SOL")));
					beanClaves.setCveOperacion(utilerias.getString(map.get("CVE_OPERACION")));
					beanComentarios.setComentario1(utilerias.getString(map.get("COMENTARIO1")));
					beanComentarios.setComentario2(utilerias.getString(map.get("COMENTARIO2")));
					beanComentarios.setComentario3(utilerias.getString(map.get("COMENTARIO3")));
					beanComentarios.setComentario4(utilerias.getString(map.get("COMENTARIO4")));
					beanDatosGenerales.setNombreArch(utilerias.getString(map.get(NOMBRE_ARCH)));
					beanDatosGenerales.setSecuencia(utilerias.getString(map.get("SECUENCIA")));
					beanDatosGenerales.setReferenciaMed(utilerias.getString(map.get("REFERENCIA_MED")));
					beanDatosGenerales.setImporte(utilerias.getString(map.get("IMPORTE")));
					beanDatosGenerales.setConceptoPago2(utilerias.getString(map.get("CONCEPTO_PAGO2")));
					beanDatosGenerales.setFchCapTransfer(utilerias.getString(map.get("FCH_CAP_TRANSFER")));       
					beanDatosGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
					beanDatosOperacion.setCodError(utilerias.getString(map.get("COD_ERROR")));
					beanDatosOperacion.setEstatusInicial(utilerias.getString(map.get("ESTATUS_INICIAL")));
					beanDatosOperacion.setMensaje(utilerias.getString(map.get("MENSAJE")));
					beanDatosGeneralesExt.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
					beanDatosGeneralesExt.setBucCliente(utilerias.getString(map.get("BUC_CLIENTE")));
					beanDatosOrdenante.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
					beanDatosReceptor.setRfcRec(utilerias.getString(map.get("RFC_REC")));
					beanDatosGeneralesExt.setImporteIva(utilerias.getString(map.get("IMPORTE_IVA")));
					beanDatosGeneralesExt.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
					beanDatosGeneralesExt.setUetrSwift(utilerias.getString(map.get("UETR_SWIFT")));
					beanDatosGeneralesExt.setCampoSwift1(utilerias.getString(map.get("CAMPO_SWIFT1")));
					beanDatosGeneralesExt.setCampoSwift2(utilerias.getString(map.get("CAMPO_SWIFT2")));
					beanDatosGeneralesExt.setRefeAdicional1(utilerias.getString(map.get("REFE_ADICIONAL1")));
					/** Se asignan los bean al objecto principal **/
					beanDatosGenerales.setBeanDatosGeneralesExt(beanDatosGeneralesExt);
					reg.setBeanClaves(beanClaves);
					reg.setBeanComentarios(beanComentarios);
					reg.setBeanDatosGenerales(beanDatosGenerales);
					reg.setBeanDatosOperacion(beanDatosOperacion);
					reg.setBeanDatosOrdenante(beanDatosOrdenante);
					reg.setBeanDatosReceptor(beanDatosReceptor);
					/** Asigamos cada fila a la lista **/
					registros.add(reg);
					/** Recuperamos el valor del contador **/
					response.setTotalReg(utilerias.getInteger(map.get("CONT")));
					/** Recuperamos el valor del monto**/
					response.setImporteTotalActual(utilerias.getBigDecimal(map.get("MONTO")));
				}
			}
			/** Se asigna la lista de los registros al objeto principal**/
			response.setRegistros(registros);
			/** Se asigna el error**/
			response.setBeanError(utileriasDetalleOperacion.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Mostramos la excepcion ocurrida**/
			showException(e);
			/** Se asigna el error**/
			response.setBeanError(utileriasDetalleOperacion.getError(null));
		}
		/** Retorno del metodo**/
		return response;
	}

}
