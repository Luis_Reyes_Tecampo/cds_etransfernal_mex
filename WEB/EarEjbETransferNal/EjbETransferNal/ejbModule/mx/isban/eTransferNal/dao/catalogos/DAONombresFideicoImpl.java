/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAONombresFideicoImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:58:29 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;
import mx.isban.eTransferNal.beans.catalogos.BeanNombresFideico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasNombresFideico;

/**
 * Class DAONombresFideicoImpl.
 *
 * Clase que implementa la interfaz DAO para aplicar
 * los metodos necesarios para realizar el acceso a la informacion
 * de la BD y poder realizar los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAONombresFideicoImpl extends Architech implements DAONombresFideico {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 913664874759367001L;

	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ? [FILTRO_AND]";
	
	/** La constante CONSULTA_ALL. */
	private static final String CONSULTA_TODO = "SELECT DISTINCT "
			.concat("NOMBRE, ")
			.concat("TOTAL, ")  
			.concat("RN ") 
			.concat("FROM (") 
			.concat("SELECT DISTINCT ") 
			.concat("NOMBRE, ") 
			.concat("(SELECT COUNT(1) AS CONT FROM TRAN_NOMBRE_FIDEICO [FILTRO_WH]) AS TOTAL, ") 
			.concat("ROWNUM AS RN ") 
			.concat("FROM TRAN_NOMBRE_FIDEICO [FILTRO_WH]) ");
	
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL = CONSULTA_TODO.concat(FILTRO_PAGINADO);
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA = "SELECT COUNT(1) CONT FROM TRAN_NOMBRE_FIDEICO "
			.concat("WHERE TRIM(NOMBRE) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR = "INSERT INTO TRAN_NOMBRE_FIDEICO(NOMBRE) "
			.concat("VALUES(TRIM(UPPER(?)))	");
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR = "UPDATE TRAN_NOMBRE_FIDEICO SET  "
			.concat("NOMBRE = TRIM(UPPER(?)) WHERE TRIM(NOMBRE) = TRIM(UPPER(?))");
	
	/** La constante CONSULTA_ELIMINAR. */
	private static final String CONSULTA_ELIMINAR = "DELETE FROM TRAN_NOMBRE_FIDEICO "
			.concat("WHERE TRIM(NOMBRE) = TRIM(UPPER(?))");
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtileriasCatalogos utilerias = UtileriasCatalogos.getInstancia();
	
	/** La variable que contiene informacion con respecto a: utilerias nombres. */
	private static UtileriasNombresFideico utileriasNombres = UtileriasNombresFideico.getInstancia();
	
	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar los nombres de fideicomiso
	 *
	 * @param beanNombre --> objeto que almacena los nombres de fideicomiso
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador --> bean que permite paginar
	 * @return Objeto bean nombres fideico
	 */
	@Override
	public BeanNombresFideico consultar(BeanNombreFideico beanNombre, BeanPaginador beanPaginador,
			ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanNombresFideico response = new BeanNombresFideico();
		/** Declaracion de una lista que almacenará los registros del catalogo **/
		List<BeanNombreFideico> nombres = Collections.emptyList();
		/** Declaracion del objeto que almacenara el resultado de la consulta **/
		List<HashMap<String, Object>> queryResponse = null;
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		/** Obtencion de los filtros**/
		String filtroAnd = utileriasNombres.getFiltro(beanNombre, "AND");
		String filtroWhere = utileriasNombres.getFiltro(beanNombre, "WHERE");
		try {
			/** Se arma la lista de parametros de paginacion **/
			Object[] pager = new Object[] { beanPaginador.getRegIni(),beanPaginador.getRegFin() };
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_PRINCIPAL.replaceAll("\\[FILTRO_AND\\]", filtroAnd)
					.replaceAll("\\[FILTRO_WH\\]", filtroWhere), Arrays.asList(pager), HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				/** Inicializacion de la lista que guardara los registros**/
				nombres = new ArrayList<BeanNombreFideico>();
				/** Se obtiene rel resultado de la consulta **/
				queryResponse = responseDTO.getResultQuery();
				/** Se recorre la respuesta  **/
				for (HashMap<String, Object> map : queryResponse) {
					/** Se inicializa un objeto para guardar los datos por cada registro **/
					BeanNombreFideico nombre = new BeanNombreFideico();
					/** Seteo de datos **/
					nombre.setNombre(utilerias.getString(map.get("NOMBRE")));
					/** Se añade el objeto a la lista**/
					nombres.add(nombre);
					/** Seteo del total **/
					response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
				}
			}
			/** Se setea la lista al objeto de salida **/
			response.setNombres(nombres);
			/** Se setea el error al objeto de salida **/
			response.setBeanError(utilerias.getError(responseDTO));
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			response.setBeanError(utilerias.getError(null));
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Agregar registros.
	 *
	 * Metodo publico utilizado para agregar registros
	 * al catalogo.
	 * 
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNombre --> bean que obtiene el nombre
	 * @return Objeto bean nombres fideico
	 */
	@Override
	public BeanResBase agregar(BeanNombreFideico beanNombre, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasNombres.agregarParametros(beanNombre, null, ConstantesCatalogos.ALTA);
			/** Invocacion a metodo auxiliar **/
			int existencia = buscarRegistro(beanNombre);
			if(existencia == 0) {
				responseDTO = HelperDAO.insertar(CONSULTA_INSERTAR, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				response.setCodError(responseDTO.getCodeError());
			}else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Editar registros.
	 *
	 * Metodo publico utilizado para editar registros
	 * del catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 *@param anterior --> objeto de tipo  BeanNombreFideico que trae los nmbres de usuarios
	 * @param beanNombre --> bean que obtiene el nombre de usuario
	 * @return Objeto bean nombres fideico
	 */
	@Override
	public BeanResBase editar(BeanNombreFideico beanNombre, BeanNombreFideico datoAnterior, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			int existencia = 0;
			existencia = buscarRegistro(beanNombre);
			if(existencia == 0) {
				/** Se agregan los parametros a la lista **/
				parametros = utileriasNombres.agregarParametros(beanNombre, datoAnterior, ConstantesCatalogos.MODIFICACION);
				/** Ejecucion de la operacion **/
				responseDTO = HelperDAO.actualizar(CONSULTA_MODIFICAR, parametros, HelperDAO.CANAL_GFI_DS,
						this.getClass().getName());
				/** Se setea el error al objeto de salida **/
				response.setCodError(responseDTO.getCodeError());
			} else {
				/** Se setea el error al objeto de salida **/
				response.setCodError(Errores.ED00130V);
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Eliminar registros.
	 *
	 * Metodo publico utilizado para eliminar registros
	 * en el catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNombre --> bean que obtiene el nombre
	 * @return Objeto bean nombres fideico
	 */ 
	@Override
	public BeanResBase eliminar(BeanNombreFideico beanNombre, ArchitechSessionBean session) {
		/** Declaracion del objeto de salida **/
		final BeanResBase response = new BeanResBase();
		/** Declaracion del objeto que alamacenara el resultado de la operacion **/
		ResponseMessageDataBaseDTO responseDTO = null;
		List<Object> parametros;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasNombres.agregarParametros(beanNombre, null, ConstantesCatalogos.BAJA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.eliminar(CONSULTA_ELIMINAR, parametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el error al objeto de salida **/
			response.setCodError(responseDTO.getCodeError());
			response.setMsgError(responseDTO.getMessageError());
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
			/** Se setea el error al objeto de salida **/
			response.setCodError(Errores.EC00011B);
		}
		/** Se retorna la respuesta **/
		return response;
	}

	/**
	 * Buscar registro.
	 * 
	 * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param beanNombreFideico --> objeto de tipo beanNombreFideico que toma como parametro el nombre de fideicomiso para realizar la busqueda de los registros
	 * @return contador: variable de tipo int que especifica el total de registros encontrados
	 */
	private int buscarRegistro(BeanNombreFideico beanNombreFideico) {
		/** Declaracion del objeto de salida **/
		ResponseMessageDataBaseDTO responseDTO;
		List<Object> parametros = null;
		List<HashMap<String, Object>> resultado = null;
		int contador = 0;
		try {
			/** Se agregan los parametros a la lista **/
			parametros = utileriasNombres.agregarParametros(beanNombreFideico, null, ConstantesCatalogos.BUSQUEDA);
			/** Ejecucion de la operacion **/
			responseDTO = HelperDAO.consultar(CONSULTA_EXISTENCIA, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Validacion de la respuesta obtenida **/
			if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				resultado = responseDTO.getResultQuery();
				Map<String, Object> map = resultado.get(0);
				contador = utilerias.getInteger(map.get("CONT"));
			}
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones**/
			showException(e);
		}
		/** Se retorna la respuesta **/
		return contador;
	}
	
}
