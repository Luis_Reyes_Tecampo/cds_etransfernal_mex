/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOInstitucionesPCCdaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinanciero;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsTiposIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimIntFinDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasIntFinancierosHis;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class DAOIntFinancierosImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOIntFinancierosImpl extends Architech implements DAOIntFinancieros{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -59340974412400776L;
	
	/** La constante CODE. */
	private static final String CODE = "Error";
	
	/** La constante MSG. */
	private static final String MSG = "Error al extraer parametros de busqueda";

	/** La constante AMBOS. */
	private static final String AMBOS = "AMBOS";
	
	/** La constante SPID. */
	private static final String SPID = "SPID";
	
	/** La constante SPEI. */
	private static final String SPEI = "SPEI";
	
	/** La constante CONDICION_1. */
	private static final String CONDICION_1 = " WHERE RN BETWEEN ? AND ? ORDER BY TMP.NOMBRE_CORTO ";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_CONS_INST. */
	private static final String QUERY_CONS_INT = " SELECT " 
			+ "COALESCE(C.CVE_INTERME, T.CVE_INTERME) as CVE_INTERME, "
			+ "C.TIPO_INTERME,C.NUM_CECOBAN, C.NOMBRE_CORTO, C.NOMBRE_LARGO, C.NUM_PERSONA, " 
			+ "TO_CHAR(C.FCH_ALTA,'DD-MM-YYYY') FCH_ALTA, TO_CHAR(C.FCH_BAJA,'DD-MM-YYYY') FCH_BAJA, " 
			+ "TO_CHAR(C.FCH_ULT_MODIF,'DD-MM-YYYY') FCH_ULT_MODIF, C.USUARIO_MODIF, C.USUARIO_REGISTRO, " 
			+ "C.STATUS, C.NUM_BANXICO, C.NUM_INDEVAL, C.ID_INT_INDEVAL, C.FOL_INT_INDEVAL, C.CVE_SWIFT_COR, " 
			+ "CASE WHEN T.CVE_INTERME = C.CVE_INTERME THEN 'AMBOS' END AMBAS, "   
			+ "CASE WHEN T.CVE_INTERME IS NULL THEN 'N' ELSE 'S' END SPID, " 
			+ "CASE WHEN C.CVE_INTERME IS NULL THEN 'N' ELSE 'S' END SPEI, "
			+ "CASE WHEN T.CVE_INTERME IS NOT  NULL THEN 'SPID' END SPIDD, "
			+ "CASE WHEN C.CVE_INTERME IS NOT  NULL THEN 'SPEI' END SPEII "
			+ "FROM COMU_INTERME_FIN C FULL OUTER JOIN TRAN_SPID_INTERME T ON (C.CVE_INTERME = T.CVE_INTERME)";
	
	/** La constante QUERY_SELECT_COUNT_INT. */
	private static final String QUERY_SELECT_COUNT_INT = 
			" SELECT COUNT (1) CONT FROM ( SELECT TMP.*, ROWNUM AS RN FROM ( " + QUERY_CONS_INT ;
	
	/** La constante QUERY_SELECT_INT_ORDER. */
	private static final String QUERY_SELECT_COUT_INT_TMP =		  
			  " ) TMP) TMP ";		
	
	/** La constante QUERY_SELECT_INT. */
	private static final String QUERY_SELECT_INT = 
	" SELECT TMP.* FROM ( SELECT TMP.*, ROWNUM AS RN FROM ( " + QUERY_CONS_INT ;
	
	/** La constante QUERY_SELECT_INT_ORDER. */
	private static final String QUERY_SELECT_INT_ORDER = " ORDER BY NOMBRE_CORTO ) TMP) TMP "+ CONDICION_1 ;
	
	/** La constante QUERY_DELETE_INT. */
	private static final String QUERY_DELETE_INT = " UPDATE COMU_INTERME_FIN SET FCH_BAJA = SYSDATE WHERE UPPER(TRIM(CVE_INTERME)) = UPPER(?) ";
	
	/** La constante QUERY_BUSCA_DUP_INSERT_INT. */
	private static final String QUERY_BUSCA_DUP_INSERT_INT =  " SELECT COUNT(1) CONT FROM COMU_INTERME_FIN T WHERE UPPER(TRIM(CVE_INTERME)) = UPPER(?) ";
	
	/** La constante QUERY_INSERT_INT. */
	private static final String QUERY_INSERT_INT = 
	" INSERT INTO COMU_INTERME_FIN (CVE_INTERME,TIPO_INTERME,NUM_CECOBAN,NOMBRE_CORTO,NOMBRE_LARGO,NUM_PERSONA,FCH_ALTA,FCH_BAJA,FCH_ULT_MODIF,USUARIO_MODIF,USUARIO_REGISTRO,STATUS,NUM_BANXICO,NUM_INDEVAL,ID_INT_INDEVAL,FOL_INT_INDEVAL,CVE_SWIFT_COR) " 
			+ "VALUES (?, ?, ?, ?, ?, ?, SYSDATE, ?, SYSDATE, NULL, ?, ?, ?, ?, ?, ?, ?)";
	
	/** La constante QUERY_UPDATE_INT. */
	private static final String QUERY_UPDATE_INT = "UPDATE COMU_INTERME_FIN SET TIPO_INTERME = ?, NUM_CECOBAN = ?, NOMBRE_CORTO = ?, NOMBRE_LARGO = ?, NUM_PERSONA = ?, FCH_ULT_MODIF = SYSDATE, USUARIO_MODIF = ?, STATUS = ?, NUM_BANXICO = ?, NUM_INDEVAL = ?, "
			+ "ID_INT_INDEVAL = ?, FOL_INT_INDEVAL = ?, CVE_SWIFT_COR = ?, FCH_BAJA = TO_DATE(?, 'DD-MM-YYYY') WHERE UPPER(TRIM(CVE_INTERME)) = UPPER(?)";
	
	/** La constante QUERY_TIPO_INTERME. */
	private static final String QUERY_TIPO_INTERME = "SELECT TRIM(CODIGO_GLOBAL) CODIGO_GLOBAL FROM COMU_DETALLE_CD "
			+"WHERE TIPO_GLOBAL = 'COTIIFCL'";
	
	/** La constante QUERY_INSERT_COMU_INTERME_FIN_HIS. */
	private static final String QUERY_INSERT_COMU_INTERME_FIN_HIS = "INSERT INTO COMU_INTERME_FIN_HIS " +
	                                       "(CVE_INTERME,FCH_MODIF,TIPO_MODIF,USUARIO,TIPO_INTERME_ORI,TIPO_INTERME_NVO,NUM_CECOBAN_ORI,NUM_CECOBAN_NVO,NOMBRE_CORTO_ORI,NOMBRE_CORTO_NVO,NOMBRE_LARGO_ORI,NOMBRE_LARGO_NVO,NUM_BANXICO_ORI,NUM_BANXICO_NVO) "+
			                               " VALUES (?,SYSDATE,?,?,?,?,?,?,?, ?,?,?,?,?)";
	 
	/** La constante CONSULTA_DATOS_ORI. */
	private static final String CONSULTA_DATOS_ORI = "SELECT TIPO_INTERME TIPO_INTERME_ORI, NUM_CECOBAN NUM_CECOBAN_ORI,NOMBRE_CORTO NOMBRE_CORTO_ORI,NOMBRE_LARGO NOMBRE_LARGO_ORI,NUM_BANXICO NUM_BANXICO_ORI " +
	                                                  "FROM  COMU_INTERME_FIN WHERE UPPER(TRIM(CVE_INTERME)) = UPPER(?) " ;
	
	/** La constante DAE000. */
	private static final String DAE000 = "DAE000";
	
	
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOIntFinancieros#buscarIntFinancieros(mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BeanResConsIntFinDAO buscarIntFinancieros(
			BeanConsReqIntFin beanConsReqIntFin,
			ArchitechSessionBean architechSessionBean) {
		  int cont =0;
		  StringBuilder build = new StringBuilder();
		  Map<String, Object> paramQuery = new HashMap<String, Object>();
		  List<Object> listParametros = new ArrayList<Object>();
		  final BeanResConsIntFinDAO beanResConsIntFinDAO = new BeanResConsIntFinDAO();
	      BeanIntFinanciero beanIntFinanciero = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      List<BeanIntFinanciero> listBeanIntFinanciero = Collections.emptyList();
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      String querySelectAUX = null;
	      try{
	    	  querySelectAUX = getSeleccionOpcionCount (beanConsReqIntFin);
	    	  paramQuery = armaConsulta(beanConsReqIntFin, listParametros);
	    	  build = (StringBuilder) paramQuery.get("QUERY");
	    	  listParametros = (List<Object>) paramQuery.get("PARAMETROS");
	    	  responseDTO = HelperDAO.consultar(QUERY_SELECT_COUNT_INT+build.toString()+querySelectAUX, Arrays.asList(
	    			  listParametros.toArray(new Object[listParametros.size()])
		      ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	  if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
		          list = responseDTO.getResultQuery();
		          Map<String,Object> map = list.get(0);
		          cont = utilerias.getInteger(map.get("CONT"));
		          beanResConsIntFinDAO.setTotalReg(cont);
		          if(cont == 0){
		        	  beanResConsIntFinDAO.setCodError(Errores.ED00011V);
		        	  beanResConsIntFinDAO.setMsgError(Errores.DESC_ED00011V);
		        	  return beanResConsIntFinDAO;
		          }
	    	  }
	    	  if (null != beanConsReqIntFin.getPaginador()) {
	    		  listParametros.add(beanConsReqIntFin.getPaginador().getRegIni());
		    	  listParametros.add(beanConsReqIntFin.getPaginador().getRegFin());
	    	  }else{
	    		  listParametros.add(1);
		    	  listParametros.add(1);  
	    	  }
	    	  querySelectAUX = getSeleccionOpcion (beanConsReqIntFin);
	    	  responseDTO = HelperDAO.consultar(QUERY_SELECT_INT+build.toString()+querySelectAUX, Arrays.asList(
	    			  listParametros.toArray(new Object[listParametros.size()])
		      ),HelperDAO.CANAL_GFI_DS, this.getClass().getName()); 
	    	  
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	        	 listBeanIntFinanciero = new ArrayList<BeanIntFinanciero>();
	            list = responseDTO.getResultQuery();
	            for(Map<String,Object> map:list){
	            	beanIntFinanciero = descomponeConsulta(map);
	            	listBeanIntFinanciero.add(beanIntFinanciero);
	            }
	         }
	         beanResConsIntFinDAO.setListBeanIntFinanciero(listBeanIntFinanciero);
	         beanResConsIntFinDAO.setCodError(responseDTO.getCodeError());
	         beanResConsIntFinDAO.setMsgError(responseDTO.getMessageError());
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResConsIntFinDAO.setCodError(Errores.EC00011B);
	      }
	      return beanResConsIntFinDAO;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOIntFinancieros#guardaIntFinanciero(mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase guardaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin, 
			ArchitechSessionBean architechSessionBean){
	  int cont =0;
	  String cadenaVacia = null;
	  Utilerias utilerias = Utilerias.getUtilerias();
	  List<HashMap<String,Object>> list = null;
	  final BeanResBase beanResGuardaInstDAO = new BeanResBase();
	  ResponseMessageDataBaseDTO responseDTO = null;
	  
	  try{
		  
     	responseDTO = HelperDAO.consultar(QUERY_BUSCA_DUP_INSERT_INT, Arrays.asList(new Object[]{beanReqInsertIntFin.getCveInterme(),}),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
     	
	    	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    		list = responseDTO.getResultQuery();
	    		Map<String,Object> map = list.get(0);
	    		cont = utilerias.getInteger(map.get("CONT"));
	    		if(cont>0){
		          	beanResGuardaInstDAO.setCodError(Errores.ED00061V);
		          	return beanResGuardaInstDAO;
	          	}	  
	    	 }
        responseDTO = HelperDAO.insertar(QUERY_INSERT_INT, 
		   Arrays.asList(new Object[]{
				   beanReqInsertIntFin.getCveInterme().toUpperCase(),
				   beanReqInsertIntFin.getTipoInterme().trim().toUpperCase(),
				   beanReqInsertIntFin.getNumCecoban(),
				   beanReqInsertIntFin.getNombreCorto().trim().toUpperCase(),
				   beanReqInsertIntFin.getNombreLargo().trim().toUpperCase(),
				   beanReqInsertIntFin.getNumPersona().trim().toUpperCase(),
				   beanReqInsertIntFin.getFchBaja(),
				   architechSessionBean.getUsuario().trim().toUpperCase(),
				   beanReqInsertIntFin.getStatus().trim().toUpperCase(),
				   beanReqInsertIntFin.getNumBanxico(),
				   beanReqInsertIntFin.getNumIndeval(),
				   beanReqInsertIntFin.getIdIntIndeval(),
				   beanReqInsertIntFin.getFolIntIndeval(),
				   beanReqInsertIntFin.getCveSwiftCor().toUpperCase().trim()
				   }), 
        HelperDAO.CANAL_GFI_DS, this.getClass().getName());
        
        //SE INSERTA EN LA TABLA INSERT_COMU_INTERME_FIN_HIS
        if(DAE000.contentEquals(responseDTO.getCodeError())){
        	responseDTO = HelperDAO.insertar(QUERY_INSERT_COMU_INTERME_FIN_HIS, 
        			   Arrays.asList(new Object[]{
        					   beanReqInsertIntFin.getCveInterme().toUpperCase(),
        					   "A",
        					   architechSessionBean.getUsuario().trim().toUpperCase(),
        					   beanReqInsertIntFin.getTipoInterme().trim().toUpperCase(),
        					   cadenaVacia,
        					   beanReqInsertIntFin.getNumCecoban(),
        					   cadenaVacia,
        					   beanReqInsertIntFin.getNombreCorto().trim().toUpperCase(),
        					   cadenaVacia,
        					   beanReqInsertIntFin.getNombreLargo().trim().toUpperCase(),
        					   cadenaVacia,
        					   beanReqInsertIntFin.getNumBanxico(),
        					   cadenaVacia
        					   }), 
        	        HelperDAO.CANAL_GFI_DS, this.getClass().getName());        	
        }
        beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
        beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());
     } catch (ExceptionDataAccess e) {
        showException(e);
        beanResGuardaInstDAO.setCodError(Errores.EC00011B);
     }
     return beanResGuardaInstDAO;
  }
	
 	/**
	  * Metodo que sirve para marcar los registros como eliminados.
	  *
	  * @param beanIntFinanciero El objeto: bean int financiero
	  * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	  * @return BeanResElimInstPOACOADAO Objeto del tipo
	  *         BeanResElimInstPOACOADAO
	  */
	@Override
	public BeanResElimIntFinDAO elimIntFinanciero(BeanIntFinanciero beanIntFinanciero,
			ArchitechSessionBean architechSessionBean) {
		final BeanResElimIntFinDAO beanResElimIntFin = new BeanResElimIntFinDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
				responseDTO = HelperDAO.actualizar(QUERY_DELETE_INT,
						Arrays.asList(new Object[] {
								beanIntFinanciero.getCveInterme()
						}),
						HelperDAO.CANAL_GFI_DS, this.getClass().getName());
				beanResElimIntFin.setCodError(responseDTO.getCodeError());
				beanResElimIntFin.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResElimIntFin.setCodError(Errores.EC00011B);
		}
		return beanResElimIntFin;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOIntFinancieros#consultaTiposIntFin(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResConsTiposIntFinDAO consultaTiposIntFin(BeanReqInsertarIntFin beanReqInsertInst,
			ArchitechSessionBean architechSessionBean) {
      BeanResConsTiposIntFinDAO beanResConsIntFinDAO = new BeanResConsTiposIntFinDAO();
      Utilerias utilerias = Utilerias.getUtilerias();
      List<String> listTiposIntFin = Collections.emptyList();
      List<HashMap<String,Object>> list = null;
      ResponseMessageDataBaseDTO responseDTO = null;
      try{
         responseDTO = HelperDAO.consultar(QUERY_TIPO_INTERME, Arrays.asList(
        		 new Object[]{}
         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
        	 listTiposIntFin = new ArrayList<String>();
            list = responseDTO.getResultQuery();
            for(HashMap<String,Object> map:list){
            	listTiposIntFin.add(utilerias.getString(map.get("CODIGO_GLOBAL")));    	
            }
         }
         beanResConsIntFinDAO.setListTiposIntFin(listTiposIntFin);
         beanResConsIntFinDAO.setCodError(responseDTO.getCodeError());
         beanResConsIntFinDAO.setMsgError(responseDTO.getMessageError());
         if (Errores.CODE_SUCCESFULLY.equals(beanResConsIntFinDAO.getCodError()) &&
        		 null != beanReqInsertInst.getCveInterme() && !StringUtils.EMPTY.equals(beanReqInsertInst.getCveInterme())) {
        	 BeanConsReqIntFin beanConsReqIntFin = new BeanConsReqIntFin();
        	 beanConsReqIntFin.setCveInterme(beanReqInsertInst.getCveInterme());
        	 beanConsReqIntFin.setBandera(beanReqInsertInst.getBandera());
        	 BeanResConsIntFinDAO beanResConsIntFin = buscarIntFinancieros(beanConsReqIntFin,architechSessionBean);
        	 beanResConsIntFinDAO = mapeaValidacionInterme(beanResConsIntFin, 
        			 beanResConsIntFinDAO);
        	 beanResConsIntFinDAO.setCodError(beanResConsIntFinDAO.getCodError());
        	 beanResConsIntFinDAO.setMsgError(beanResConsIntFinDAO.getMsgError());
		}
      } catch (ExceptionDataAccess e) {
         showException(e);
         beanResConsIntFinDAO.setCodError(Errores.EC00011B);
      }
      return beanResConsIntFinDAO;
	}
	
	/**
     * Metodo que genera un objeto del tipo BeanInstitucionPOACOA a partir de la respuesta de la consulta.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanInstitucionPOACOA Objeto del tipo BeanInstitucionPOACOA
     */
	private BeanIntFinanciero descomponeConsulta(Map<String,Object> map){
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanIntFinanciero beanIntFinanciero = new BeanIntFinanciero();
		beanIntFinanciero.setCveInterme(utilerias.getString(map.get("CVE_INTERME")));
      	beanIntFinanciero.setTipoInterme(utilerias.getString(map.get("TIPO_INTERME")));
      	beanIntFinanciero.setNumCecoban(utilerias.getString(map.get("NUM_CECOBAN")));
      	beanIntFinanciero.setNombreCorto(utilerias.getString(map.get("NOMBRE_CORTO")));
      	beanIntFinanciero.setNombreLargo(utilerias.getString(map.get("NOMBRE_LARGO")));
      	beanIntFinanciero.setNumPersona(utilerias.getString(map.get("NUM_PERSONA")));
      	beanIntFinanciero.setFchAlta(utilerias.getString(map.get("FCH_ALTA")));
      	beanIntFinanciero.setFchBaja(utilerias.getString(map.get("FCH_BAJA")));
      	beanIntFinanciero.setFchUltModif(utilerias.getString(map.get("FCH_ULT_MODIF")));
      	beanIntFinanciero.setUsuarioModif(utilerias.getString(map.get("USUARIO_MODIF")));
      	beanIntFinanciero.setUsuarioRegistro(utilerias.getString(map.get("USUARIO_REGISTRO")));
      	beanIntFinanciero.setStatus(utilerias.getString(map.get("STATUS")));
      	beanIntFinanciero.setNumBanxico(utilerias.getString(map.get("NUM_BANXICO")));
      	beanIntFinanciero.setNumIndeval(utilerias.getString(map.get("NUM_INDEVAL")));
      	beanIntFinanciero.setIdIntIndeval(utilerias.getString(map.get("ID_INT_INDEVAL")));
      	beanIntFinanciero.setFolIntIndeval(utilerias.getString(map.get("FOL_INT_INDEVAL")));
      	beanIntFinanciero.setCveSwiftCor(utilerias.getString(map.get("CVE_SWIFT_COR")));
      	beanIntFinanciero.setOperaSpei(utilerias.getString(map.get("SPEI")));
      	beanIntFinanciero.setOperaSpid(utilerias.getString(map.get("SPID")));
      	if (AMBOS.equals(utilerias.getString(map.get("AMBAS")))){
        	beanIntFinanciero.setSelecOpcion(utilerias.getString(map.get("AMBAS")));
   	  	}else if (SPID.equals(utilerias.getString(map.get("SPIDD")))){
   	  		beanIntFinanciero.setSelecOpcion(utilerias.getString(map.get("SPIDD")));
   	  	}else if (SPEI.equals(utilerias.getString(map.get("SPEII")))){
   	  		beanIntFinanciero.setSelecOpcion(utilerias.getString(map.get("SPEII")));
   	  	}
      	return beanIntFinanciero;
	  }
	
	/**
	 * Mapea validacion interme.
	 *
	 * @param resBusqueda El objeto: res busqueda
	 * @param resValidacion El objeto: res validacion
	 * @return Objeto bean res cons tipos int fin dao
	 */
	private BeanResConsTiposIntFinDAO mapeaValidacionInterme(BeanResConsIntFinDAO resBusqueda, 
			BeanResConsTiposIntFinDAO resValidacion){
		List<BeanIntFinanciero> listBeanIntFinanciero = resBusqueda.getListBeanIntFinanciero();
		if(Errores.CODE_SUCCESFULLY.equals(resBusqueda.getCodError()) && !listBeanIntFinanciero.isEmpty()){
	    	  for(BeanIntFinanciero beanIntFinanciero : listBeanIntFinanciero){
	    		  resValidacion.setCveInterme(beanIntFinanciero.getCveInterme());
	    		  resValidacion.setNumIndeval(beanIntFinanciero.getNumIndeval());
	    		  resValidacion.setTipoInterme(beanIntFinanciero.getTipoInterme());
	    		  resValidacion.setIdIntIndeval(beanIntFinanciero.getIdIntIndeval());
	    		  resValidacion.setNumCecoban(beanIntFinanciero.getNumCecoban());
	    		  resValidacion.setFolIntIndeval(beanIntFinanciero.getFolIntIndeval());
	    		  resValidacion.setNombreCorto(beanIntFinanciero.getNombreCorto());
	    		  resValidacion.setNombreLargo(beanIntFinanciero.getNombreLargo());
	    		  resValidacion.setNumPersona(beanIntFinanciero.getNumPersona());
	    		  resValidacion.setStatus(beanIntFinanciero.getStatus());
	    		  resValidacion.setNumBanxico(beanIntFinanciero.getNumBanxico());
	    		  resValidacion.setCveSwiftCor(beanIntFinanciero.getCveSwiftCor());
	    		  resValidacion.setFchBaja(beanIntFinanciero.getFchBaja());
	    	  }
	    	  resValidacion.setMsgError(resBusqueda.getMsgError());
	    	  resValidacion.setCodError(resBusqueda.getCodError());
	    	  resValidacion.setTipoError(resBusqueda.getTipoError());
	    	  resValidacion.setTotalReg(resBusqueda.getTotalReg());
		} 
		return resValidacion;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOIntFinancieros#actualizaIntFinanciero(mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase actualizaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin, 
			ArchitechSessionBean architechSessionBean){
		
		//OBJETOS Y VARIABLES 
	  final BeanResBase beanResGuardaInstDAO = new BeanResBase();
	  ResponseMessageDataBaseDTO responseDTO = null;
	  List<HashMap<String,Object>> list = null;
	  Utilerias utilerias = Utilerias.getUtilerias();
	  String tipoInterOri = "";
	  String numCecobanOri = "";
	  String nombCortoOri = "";
	  String nommLargoOri = "";
	  String numBanxicoOri = "";
	  String tipoInterOriNvo = "";
	  String nombCortoOriNvo = "";
	  String nommLargoOriNvo = "";
	  
	  try{
		  //BUSCAMOS LOS DATOS ANTERIORES ANTES DE ACTUALIZAR PARA REALIZAR EL INSERT EN LA TABLA COMU_INTERME_FIN_HIS
		  responseDTO = HelperDAO.consultar(CONSULTA_DATOS_ORI, Arrays.asList(new Object[]{beanReqInsertIntFin.getCveInterme(),}),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	     	
		  //VALIDAMOS EL RESULTADO DE LA CONSULTA
	    	if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    		list = responseDTO.getResultQuery();
	    		Map<String,Object> map = list.get(0);
	    		tipoInterOri = utilerias.getString(map.get("TIPO_INTERME_ORI"));
	    		numCecobanOri = utilerias.getString(map.get("NUM_CECOBAN_ORI"));
	    		nombCortoOri = utilerias.getString(map.get("NOMBRE_CORTO_ORI"));
	    		nommLargoOri = utilerias.getString(map.get("NOMBRE_LARGO_ORI"));
	    		numBanxicoOri = utilerias.getString(map.get("NUM_BANXICO_ORI"));	    			  
	    	 }
		  
	    	//Cuando ninguno de estos cambios sufre algun cambio, no se inserta en la tabla
	    	 tipoInterOriNvo = beanReqInsertIntFin.getTipoInterme().trim().toUpperCase();
	  	     nombCortoOriNvo = beanReqInsertIntFin.getNombreCorto().trim().toUpperCase();
	  	     nommLargoOriNvo = beanReqInsertIntFin.getNombreLargo().trim().toUpperCase();
	  	    	  	  
	    	if(tipoInterOri.equals(tipoInterOriNvo) && 
	    	   numCecobanOri.equals(beanReqInsertIntFin.getNumCecoban()) &&
	    	   nombCortoOri.equals(nombCortoOriNvo) &&
	    	   nommLargoOri.equals(nommLargoOriNvo) &&
	    	   numBanxicoOri.equals(beanReqInsertIntFin.getNumBanxico())) {
	    		 beanResGuardaInstDAO.setCodError(DAE000);
	    	} else {
	    		//SE INSERTA EN LA TABLA INSERT_COMU_INTERME_FIN_HIS
		        if(DAE000.contentEquals(responseDTO.getCodeError())){
		        	responseDTO = HelperDAO.insertar(QUERY_INSERT_COMU_INTERME_FIN_HIS, 
		        			   Arrays.asList(new Object[]{
		        					   beanReqInsertIntFin.getCveInterme().toUpperCase(),
		        					   "M",
		        					   architechSessionBean.getUsuario().trim().toUpperCase(),
		        					   tipoInterOri,
		        					   beanReqInsertIntFin.getTipoInterme().trim().toUpperCase(),
		        					   numCecobanOri,
		        					   beanReqInsertIntFin.getNumCecoban(),
		        					   nombCortoOri,
		        					   beanReqInsertIntFin.getNombreCorto().trim().toUpperCase(),
		        					   nommLargoOri,
		        					   beanReqInsertIntFin.getNombreLargo().trim().toUpperCase(),
		        					   numBanxicoOri,
		        					   beanReqInsertIntFin.getNumBanxico()
		        					   }), 
		        	        HelperDAO.CANAL_GFI_DS, this.getClass().getName());        	
		        }
	    	}
	    	
		   //se valida que la primer insert sea exitoso para seguir con el update
	        if(DAE000.contentEquals(responseDTO.getCodeError())){
	        	responseDTO = HelperDAO.actualizar(QUERY_UPDATE_INT, 
	        			   Arrays.asList(new Object[]{
	        					   beanReqInsertIntFin.getTipoInterme().toUpperCase().trim(),
	        					   beanReqInsertIntFin.getNumCecoban(),
	        					   beanReqInsertIntFin.getNombreCorto().toUpperCase(),
	        					   beanReqInsertIntFin.getNombreLargo().toUpperCase(),
	        					   beanReqInsertIntFin.getNumPersona().toUpperCase(),
	        					   architechSessionBean.getUsuario().toUpperCase().trim(),
	        					   beanReqInsertIntFin.getStatus().toUpperCase(),	   
	        					   beanReqInsertIntFin.getNumBanxico(),
	        					   beanReqInsertIntFin.getNumIndeval(),
	        					   beanReqInsertIntFin.getIdIntIndeval(),
	        					   beanReqInsertIntFin.getFolIntIndeval(),
	        					   beanReqInsertIntFin.getCveSwiftCor().toUpperCase(),
	        					   beanReqInsertIntFin.getFchBaja(),
	        					   beanReqInsertIntFin.getCveInterme()
	        					   }), 
	        	        HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	        }
		  
		  //resultado de la consulta
        beanResGuardaInstDAO.setCodError(responseDTO.getCodeError());
        beanResGuardaInstDAO.setMsgError(responseDTO.getMessageError());
     } catch (ExceptionDataAccess e) {
        showException(e);
        beanResGuardaInstDAO.setCodError(Errores.EC00011B);
     }
     return beanResGuardaInstDAO;
  }
	
	/**
	 * Arma consulta.
	 *
	 * @param beanConsReqIntFin El objeto: bean cons req int fin
	 * @param listParametros El objeto: list parametros
	 * @return Objeto map
	 */
	private Map<String, Object> armaConsulta(BeanConsReqIntFin beanConsReqIntFin, List<Object> listParametros) {
		Map<String, Object> paramQuery = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder();
		String param = null;
		query.append(" WHERE C.CVE_INTERME IS NOT NULL  ");
		query.append(UtileriasIntFinancierosHis.obtenerConsulta(beanConsReqIntFin));
		query.append(beanConsReqIntFin.getCveInterme() 	!= null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getCveInterme().trim())) 	? " AND UPPER(TRIM(C.CVE_INTERME)) = UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getTipoInterme() != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getTipoInterme().trim())) ? " AND UPPER(C.TIPO_INTERME) LIKE UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getNumCecoban() 	!= null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getNumCecoban().trim())) 	? " AND C.NUM_CECOBAN = UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getNombreCorto() != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getNombreCorto().trim())) ? " AND UPPER(C.NOMBRE_CORTO) LIKE UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getNombreLargo() != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getNombreLargo().trim())) ? " AND UPPER(C.NOMBRE_LARGO) LIKE UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getNumPersona() 	!= null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getNumPersona().trim())) 	? " AND UPPER(C.NUM_PERSONA) LIKE UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getFchAlta() 	!= null &&	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getFchAlta().trim())) 	? " AND TO_CHAR(C.FCH_ALTA, 'DD-MM-YYYY') = ? " : "");
		query.append(beanConsReqIntFin.getFchBaja() 	!= null &&	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getFchBaja().trim())) 	? " AND TO_CHAR(C.FCH_BAJA, 'DD-MM-YYYY') = ? " : "");
		query.append(beanConsReqIntFin.getFchUltModif() != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getFchUltModif().trim())) ? " AND TO_CHAR(C.FCH_ULT_MODIF, 'DD-MM-YYYY') = ? " : "");
		query.append(beanConsReqIntFin.getUsuarioModif() != null && (!StringUtils.EMPTY.equals(beanConsReqIntFin.getUsuarioModif().trim())) ? " AND UPPER(C.USUARIO_MODIF) LIKE UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getNumBanxico () != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getNumBanxico().trim())) 	? " AND C.NUM_BANXICO = UPPER(TRIM(?)) " : "");
		query.append(beanConsReqIntFin.getCveSwiftCor() != null && 	(!StringUtils.EMPTY.equals(beanConsReqIntFin.getCveSwiftCor().trim())) ? " AND UPPER(C.CVE_SWIFT_COR) LIKE UPPER(TRIM(?)) " : "");
		//se elimina dato para no agregarlo a los parametros
		beanConsReqIntFin.setBandera("");
		try {
			for (Method method : beanConsReqIntFin.getClass().getDeclaredMethods()) {
				if (method.getName().startsWith("get")
						&& method.getReturnType() == String.class
						&& method.invoke(beanConsReqIntFin, new Object[] {}) != null 
						&& !StringUtils.EMPTY.equals(method.invoke(beanConsReqIntFin, new Object[] {}))) {
					param = (String) method.invoke(beanConsReqIntFin, new Object[] {});
					warn(method.getName().substring(3) + ": [" + param + "]");
					if (method.getName().equals("getTipoInterme") || method.getName().equals("getNombreCorto")
							|| method.getName().equals("getNombreLargo") || method.getName().equals("getNumPersona")
							|| method.getName().equals("getUsuarioModif") || method.getName().equals("getCveSwiftCor")) {
						listParametros.add("%"+param.toUpperCase()+"%");
					} else {				
						//se valida el getSelecOpcion
						if(!"getSelecOpcion".equals(method.getName())){
							//en caso de ser correcto se agrega a la lista
								listParametros.add(param.toUpperCase());
						}	
					}
				}	
			}
		} catch (IllegalArgumentException e) {
			error(String.format("%s: %s", CODE, MSG));
			showException(e);
		} catch (IllegalAccessException e) {
			error(String.format("%s: %s", CODE, MSG));
			showException(e);
		} catch (InvocationTargetException e) {
			error(String.format("%s: %s", CODE, MSG));
			showException(e);
		}
		paramQuery.put("QUERY", query);
		paramQuery.put("PARAMETROS", listParametros);
		return paramQuery;
	}
	
	/**
	 * Arma where de la consulta.
	 * @param beanConsReqIntFin El objeto: bean cons req int fin
	 * @return Objeto map
	 */
	private String getSeleccionOpcion (BeanConsReqIntFin beanConsReqIntFin){
		//se crea la variable
		String cadenaRegistro;
		//se valida si la condicion es AMBOS
		  if (AMBOS.equals(beanConsReqIntFin.getSelecOpcion())){
			  //Se envia la siguiente condicion de AMBOS
			  cadenaRegistro = " ORDER BY NOMBRE_CORTO ) TMP WHERE AMBAS ='AMBOS') TMP " + CONDICION_1;
			//se valida si la condicion es SPID
    	  }else if (SPID.equals(beanConsReqIntFin.getSelecOpcion())){
    		//Se envia la siguiente condicion de SPID
    		  cadenaRegistro = " ORDER BY NOMBRE_CORTO ) TMP WHERE SPIDD = 'SPID') TMP "+ CONDICION_1;
    		//se valida si la condicion es SPEI
    	  }else if (SPEI.equals(beanConsReqIntFin.getSelecOpcion())){
    		//Se envia la siguiente condicion de SPEI
    		  cadenaRegistro = " ORDER BY NOMBRE_CORTO ) TMP WHERE SPEII = 'SPEI') TMP "+ CONDICION_1 ;
    	  }else{
    		  //en caso de no ser ninguna de las condiciones anteriores
    		  cadenaRegistro = QUERY_SELECT_INT_ORDER;
    	  }
		  //se retorna la cadena de registros
		return cadenaRegistro;
	}
	
	
	
	/**
	 * Arma where de la consulta.
	 * @param beanConsReqIntFin El objeto: bean cons req int fin
	 * @return Objeto cadenaCount
	 */
	private String getSeleccionOpcionCount (BeanConsReqIntFin beanConsReqIntFin){
		//variable de la cadena
		String cadenaCount;
		
		//se valida si la condicion es AMBOS
		if (AMBOS.equals(beanConsReqIntFin.getSelecOpcion())){
			 //Se envia la siguiente condicion de AMBOS
			cadenaCount = " ) TMP WHERE AMBAS ='AMBOS') TMP ";
			//se valida si la condicion es SPID
		}else if (SPID.equals(beanConsReqIntFin.getSelecOpcion())){
			 //Se envia la siguiente condicion de SPID
		  cadenaCount =  " ) TMP WHERE SPIDD = 'SPID') TMP ";
		//se valida si la condicion es SPEI
		}else if (SPEI.equals(beanConsReqIntFin.getSelecOpcion())){
			 //Se envia la siguiente condicion de SPEI
		  cadenaCount =  " ) TMP WHERE SPEII = 'SPEI') TMP ";
		  //en caso de no ser ninguna de las opciones
		}else{
		  cadenaCount = QUERY_SELECT_COUT_INT_TMP;
		}
		 //se retorna la cadena de los registros
		return cadenaCount;
	}
}