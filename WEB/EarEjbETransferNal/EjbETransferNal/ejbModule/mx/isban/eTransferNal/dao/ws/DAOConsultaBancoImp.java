/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaBancoImp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.ws.ResBeanConsultaBancoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaBancoImp extends Architech implements DAOConsultaBanco{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6638158182735438953L;
	  /**
	   * Constante que Consulta la tabla de bancos nacionales
	   */
	private static final String QUERY_CONSULTA_BANCO = "SELECT to_char(NUM_CECOBAN,'000') NUM_CECOBAN,CVE_INTERME,NOMBRE_LARGO FROM COMU_INTERME_FIN WHERE NUM_CECOBAN = ?";

	@Override
	public ResBeanConsultaBancoDAO consultaBanco(String prefijoCuentaClabe,
			ArchitechSessionBean architechSessionBean) {
		info("Entra al DAO de consultaBanco");
		final ResBeanConsultaBancoDAO resBeanConsultaBancoDAO = new ResBeanConsultaBancoDAO();
	      List<HashMap<String,Object>> list = null;
	      Utilerias utilerias = Utilerias.getUtilerias();
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try {
	    	responseDTO = HelperDAO.consultar(QUERY_CONSULTA_BANCO, Arrays.asList(new Object[] {
	    			prefijoCuentaClabe }), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
				list = responseDTO.getResultQuery();
				resBeanConsultaBancoDAO.setPrefijoCuentaClabe(utilerias.getString(list.get(0).get("NUM_CECOBAN")));
				resBeanConsultaBancoDAO.setCveBanco(utilerias.getString(list.get(0).get("CVE_INTERME")));
				resBeanConsultaBancoDAO.setNombreBanco(utilerias.getString(list.get(0).get("NOMBRE_LARGO")));
				resBeanConsultaBancoDAO.setCodError(Errores.OK00000V);
				resBeanConsultaBancoDAO.setMsgError(Errores.DESC_OK00000V);
			}else{
				resBeanConsultaBancoDAO.setCodError(Errores.ED00011V);
				resBeanConsultaBancoDAO.setMsgError(Errores.DESC_ED00011V);
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			resBeanConsultaBancoDAO.setCodError(Errores.EC00011B);
			resBeanConsultaBancoDAO.setMsgError(Errores.DESC_EC00011B);
		} 
	     return resBeanConsultaBancoDAO;
	}

}
