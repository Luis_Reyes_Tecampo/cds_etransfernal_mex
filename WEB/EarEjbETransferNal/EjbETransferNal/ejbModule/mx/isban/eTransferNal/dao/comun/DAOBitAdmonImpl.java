/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOBitAdmonImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.comun;

import java.util.Arrays;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitAdmonImpl extends Architech implements DAOBitAdmon{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2691645015501813910L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_INSERT_BIT_ADMON
	 */
	private static final String QUERY_INSERT_BIT_ADMON =   	" INSERT INTO TR_NAL_BIT_ADMINISTRATIVA (" +
	  " folio_oper, fch_oper,user_oper, tipo_oper, id_token, fch_act, "+
	  " hora_act, val_ant, val_nvo, comentarios, dato_fijo, serv_tran, "+
	  " dir_ip, can_apli, inst_web, host_web, dato_modi, tabla_afec, "+ 
	  " id_sesion, cod_oper )"+
	  " VALUES (SEQ_TR_NAL_BIT_ADMON_FOL_OPER.NEXTVAL, TO_DATE(?,'dd-MM-yyyy'), ?,?, ?, TO_DATE(SYSDATE,'dd-MM-yyyy'),  "+
	  " TO_DATE(SYSDATE,'dd-MM-yyyy HH24:MI:SS'), ?, ?, ?, ? , ?, "+
	  " ?, ?, ?, ?, ? , ?, "+
	  " ?, ? )";
	    
	
	    
	/**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitAdmon Objeto del tipo @see BeanReqInsertBitAdmon
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitAdmon beanReqInsertBitAdmon, ArchitechSessionBean architechSessionBean){
        final BeanResGuardaBitacoraDAO beanResGuardaBitacoraDAO = new BeanResGuardaBitacoraDAO();
        ResponseMessageDataBaseDTO responseDTO = null;
        try{
           responseDTO = HelperDAO.insertar(QUERY_INSERT_BIT_ADMON, 
        		   Arrays.asList(new Object[]{
        				   beanReqInsertBitAdmon.getFechaOperacion(),
        				   architechSessionBean.getUsuario(),
        				   beanReqInsertBitAdmon.getTipoOperacion(),
        				   "",
        				   beanReqInsertBitAdmon.getValAnterior(),
        				   beanReqInsertBitAdmon.getValNuevo(),
        				   beanReqInsertBitAdmon.getComentarios(),
        				   beanReqInsertBitAdmon.getDatoFijo(),
        				   beanReqInsertBitAdmon.getServTran(),
        				   architechSessionBean.getIPCliente(),
        				   beanReqInsertBitAdmon.getCanalAplica(),
        				   architechSessionBean.getNombreServidor(),
        				   architechSessionBean.getIPServidor(),
        				   beanReqInsertBitAdmon.getDatoMod(),
        				   beanReqInsertBitAdmon.getTablaAfectada(),
        				   architechSessionBean.getIdSesion(),
        				   beanReqInsertBitAdmon.getCodOperacion()
        				   
        				 }), 
           HelperDAO.CANAL_TRANSFER_PLUS, this.getClass().getName());
           beanResGuardaBitacoraDAO.setCodError(responseDTO.getCodeError());
           beanResGuardaBitacoraDAO.setMsgError(responseDTO.getMessageError());
        } catch (ExceptionDataAccess e) {
           showException(e);
           beanResGuardaBitacoraDAO.setCodError(Errores.EC00011B);
        }
        return beanResGuardaBitacoraDAO;
     }
	    
	    
		
		
		
	
	  

}
