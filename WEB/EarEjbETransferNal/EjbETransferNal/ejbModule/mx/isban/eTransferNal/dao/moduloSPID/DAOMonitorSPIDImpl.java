/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOperLiquidadasSaldo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOperLiquidadasVolumen;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOperNoConfirmadasSaldo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOperNoConfirmadasVolumen;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMonitorSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorOpSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldosVolumen;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMonitorSPIDImpl extends Architech implements DAOMonitorSPID{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3902392808506779894L;
	/**
	 * Propiedad del tipo String que almacena el valor de UNION
	 */
	private static final String UNION = " UNION "; 
	/**
	 * Propiedad del tipo String que almacena el valor de SALDO
	 */
	private static final String SALDO = "SALDO";
	
	/**
	 * Propiedad del tipo String que almacena el valor de VOLUMEN
	 */
	private static final String VOLUMEN = "VOLUMEN";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_MONITOR
	 */
	private static final String QUERY_MONITOR = "SELECT 0 NUM, 'Saldo Inicial' LABEL, SALDO_INICIAL SALDO,0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ? "+
	 UNION +
	" SELECT 1 NUM,'TRASPASO SIAC-SPID' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN  FROM TRAN_TRSIAC_SPID  WHERE CVE_MI_INSTITUC = ?  AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	UNION +
	" SELECT 2 NUM, 'Ord. Recibidas por Aplic.' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC  WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ((ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ?) OR (ESTATUS_TRANSFER=? AND ESTATUS_BANXICO=? AND MOTIVO_DEVOL IS NULL)) "+
	UNION +
	" SELECT 3 NUM, 'Ord. Recibidas Aplicadas.' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ? "+
	UNION +
	" SELECT 4 NUM, 'Ord. Recibidas Rechazadas' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ? AND MOTIVO_DEVOL IS NOT NULL "+
	UNION +
	" SELECT 5 NUM, 'Ord. Recibidas por Dev' LABEL,SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS IN(?,?,?,?) "+
	UNION +
	" SELECT 6 NUM, 'Traspaso SPID-SIAC' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 7 NUM, 'Ord. Enviadas Conf.' LABEL,SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 8 NUM, 'Saldo No Reservado' LABEL, SALDO, 0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ? "+
	UNION +
	" SELECT 9 NUM, 'Saldo Reservado' LABEL, SALDO_RESERVADO SALDO, 0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ?" +
	UNION +
	" SELECT 10 NUM, 'Devoluciones Envi. Conf' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS = ?"+
	UNION +
	" SELECT 11 NUM, 'Tras. SPID-SIAC en Espera' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS IN(?,?) "+
	UNION +
	" SELECT 12 NUM, 'Tras. SPID-SIAC Enviados' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 13 NUM, 'Tras. SPID-SIAC por Reparar' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ? ) AND ESTATUS = ?"+
	UNION +
	" SELECT 14 NUM, 'Ord. en Espera' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS IN(?,?) "+
	UNION +
	" SELECT 15 NUM, 'Ord. Enviadas' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?)  AND ESTATUS = ? "+
	UNION +
	" SELECT 16 NUM, 'Ord. por Reparar' LABEL, sum(importe_abono) SALDO, count(*) VOLUMEN from tran_mensaje "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) and estatus = ?";


	/**
	 * Metodo que sirve para consultar la informacion del Monitor de SPID
	 * @param beanReqMonitorSPID Objeto del tipo BeanReqMonitorSPID
	 * @param architechSessionBean Objeto de sesion de la arquitectura
	 * @return BeanResMonitorOpSPID Bean de Respuesta del tipo BeanResMonitorOpSPID
	 */
	public BeanResMonitorOpSPID consultarMonitorSPID(BeanReqMonitorSPID beanReqMonitorSPID, ArchitechSessionBean architechSessionBean){
		final BeanResMonitorOpSPID beanResMonitorOpSPID = new BeanResMonitorOpSPID();
		List<HashMap<String,Object>> list = null;
	    ResponseMessageDataBaseDTO responseDTO = null;
	    try {
	      	responseDTO = HelperDAO.consultar(QUERY_MONITOR, Arrays.asList(new Object[]{
	      		beanReqMonitorSPID.getCveMiInstitucion(),
	      		beanReqMonitorSPID.getCveMiInstitucion(), beanReqMonitorSPID.getFchOperacion(),
	      	    beanReqMonitorSPID.getCveMiInstitucion(), beanReqMonitorSPID.getFchOperacion(),"PA","LQ","RE","LQ",
	      		beanReqMonitorSPID.getCveMiInstitucion(), beanReqMonitorSPID.getFchOperacion(),"TR","LQ",
	      		beanReqMonitorSPID.getCveMiInstitucion(), beanReqMonitorSPID.getFchOperacion(),"RE","LQ",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"PV","LI","EN","PR",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"CO",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"CO",
	      		beanReqMonitorSPID.getCveMiInstitucion(),
	      		beanReqMonitorSPID.getCveMiInstitucion(),
	      		beanReqMonitorSPID.getCveMiInstitucion(),"CO",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"PV","LI",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"EN",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"PR",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"PV","LI,",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"EN",
	      		beanReqMonitorSPID.getCveMiInstitucion(),"PR"
	      	}), HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	    			list = responseDTO.getResultQuery();
	    			seteaSaldosOperLiquidadasSaldos(beanResMonitorOpSPID, list);
	    			seteaVolumenOperLiquidadas(beanResMonitorOpSPID, list);
	    			seteaOperNoConfirmadasSaldoVolumen(beanResMonitorOpSPID, list);
	    			
	    		}
	    		beanResMonitorOpSPID.setCodError(Errores.OK00000V);
	    	} catch (ExceptionDataAccess e) {
	    		showException(e);
	    		beanResMonitorOpSPID.setCodError(Errores.EC00011B);
	    	}
	    return beanResMonitorOpSPID;
	}
	
	/**
	 * Metodo que setea los valores del volumen de las operaciones liquidadas
	 * @param beanResMonitorOpSPID bean del tipo BeanResMonitorOpSPID
	 * @param list Lista del tipo List<HashMap<String,Object>>
	 */
	private void seteaVolumenOperLiquidadas(BeanResMonitorOpSPID beanResMonitorOpSPID, List<HashMap<String,Object>> list){
		Utilerias utilerias = Utilerias.getUtilerias();
		BigDecimal tmp = BigDecimal.ZERO;
		BeanOperLiquidadasVolumen beanOperLiquidadasVolumen = new BeanOperLiquidadasVolumen();
		BeanSaldosVolumen saldosVolumen = new BeanSaldosVolumen(); 
		beanResMonitorOpSPID.setSaldosVolumen(saldosVolumen);
		beanResMonitorOpSPID.setOperLiquidadasVolumen(beanOperLiquidadasVolumen);
		tmp = utilerias.getBigDecimal(list.get(0).get(VOLUMEN));
		beanOperLiquidadasVolumen.setSaldoInicial(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(1).get(VOLUMEN));
		beanOperLiquidadasVolumen.setTraspasoSIACSPID(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(2).get(VOLUMEN));
		beanOperLiquidadasVolumen.setOrdRecibidasPorAplic(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));	
		
		tmp = utilerias.getBigDecimal(list.get(3).get(VOLUMEN));
		beanOperLiquidadasVolumen.setOrdRecibidasAplic(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(4).get(VOLUMEN));
		beanOperLiquidadasVolumen.setOrdRecibidasRechazadas(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(5).get(VOLUMEN));
		beanOperLiquidadasVolumen.setOrdRecibidasPorDev(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(6).get(VOLUMEN));
		beanOperLiquidadasVolumen.setTraspasoSPIDSIAC(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(7).get(VOLUMEN));
		beanOperLiquidadasVolumen.setOrdEnviadasConf(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		
		tmp = utilerias.getBigDecimal(list.get(10).get(VOLUMEN));
		saldosVolumen.setDevolucionesEnvConf(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
	
	}

	/**
	 * Metodo que setea los saldos del volumen de las operaciones liquidadas
	 * @param beanResMonitorOpSPID bean del tipo BeanResMonitorOpSPID
	 * @param list Lista del tipo List<HashMap<String,Object>>
	 */
	private void seteaSaldosOperLiquidadasSaldos(BeanResMonitorOpSPID beanResMonitorOpSPID, List<HashMap<String,Object>> list){
		Utilerias utilerias = Utilerias.getUtilerias();
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal tmp = BigDecimal.ZERO;
		BeanOperLiquidadasSaldo beanOperLiquidadasSaldo = new BeanOperLiquidadasSaldo();
		BeanSaldos beanSaldos = new BeanSaldos();
		beanResMonitorOpSPID.setSaldos(beanSaldos);
		beanResMonitorOpSPID.setOperLiquidadasSaldo(beanOperLiquidadasSaldo);
		tmp = utilerias.getBigDecimal(list.get(0).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setSaldoInicial(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));		
		tmp = utilerias.getBigDecimal(list.get(1).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setTraspasoSIACSPID(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(2).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setOrdRecibidasPorAplic(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));	
		tmp = utilerias.getBigDecimal(list.get(3).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setOrdRecibidasAplic(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(4).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setOrdRecibidasRechazadas(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(5).get(SALDO));
		total= total.add(tmp);
		beanOperLiquidadasSaldo.setOrdRecibidasPorDev(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(6).get(SALDO));
		total= total.subtract(tmp);
		beanOperLiquidadasSaldo.setTraspasoSPIDSIAC(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(7).get(SALDO));
		total= total.subtract(tmp);
		beanOperLiquidadasSaldo.setOrdEnviadasConf(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		beanSaldos.setSaldoCalculado(utilerias.formateaDecimales(total,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(8).get(SALDO));
		total= total.subtract(tmp);
		beanSaldos.setSaldoNoReservado(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		tmp = utilerias.getBigDecimal(list.get(9).get(SALDO));
		total= total.subtract(tmp);
		beanSaldos.setSaldoReservado(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		beanSaldos.setDiferencia(utilerias.formateaDecimales(total,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(10).get(SALDO));
		beanSaldos.setDevolucionesEnvConf(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
	}
	
	/**
	 * Metodo que setea los valores del saldo y volumen de las operaciones no confirmadas
	 * @param beanResMonitorOpSPID bean del tipo BeanResMonitorOpSPID
	 * @param list Lista del tipo List<HashMap<String,Object>>
	 */
	private void seteaOperNoConfirmadasSaldoVolumen(BeanResMonitorOpSPID beanResMonitorOpSPID, List<HashMap<String,Object>> list){
		Utilerias utilerias = Utilerias.getUtilerias();
		BigDecimal tmp = BigDecimal.ZERO;
		BeanOperNoConfirmadasSaldo beanOperNoConfirmadasSaldo = new BeanOperNoConfirmadasSaldo();;
		BeanOperNoConfirmadasVolumen beanOperNoConfirmadasVolumen = new BeanOperNoConfirmadasVolumen();
		
		
		beanResMonitorOpSPID.setOperNoConfirmadasSaldo(beanOperNoConfirmadasSaldo);
		beanResMonitorOpSPID.setOperNoConfirmadasVolumen(beanOperNoConfirmadasVolumen);
		
		tmp = utilerias.getBigDecimal(list.get(11).get(SALDO));
		beanOperNoConfirmadasSaldo.setTraspasoSPIDSIACEspera(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(11).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setTraspasoSPIDSIACEspera(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(12).get(SALDO));
		beanOperNoConfirmadasSaldo.setTraspasoSPIDSIACEnv(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(12).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setTraspasoSPIDSIACEnv(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(13).get(SALDO));
		beanOperNoConfirmadasSaldo.setTraspasoSPIDSIACReparar(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(13).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setTraspasoSPIDSIACReparar(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(14).get(SALDO));
		beanOperNoConfirmadasSaldo.setOrdEspera(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(14).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setOrdEspera(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(15).get(SALDO));
		beanOperNoConfirmadasSaldo.setOrdEnviadas(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(15).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setOrdEnviadas(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(16).get(SALDO));
		beanOperNoConfirmadasSaldo.setOrdPorReparar(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmp = utilerias.getBigDecimal(list.get(16).get(VOLUMEN));
		beanOperNoConfirmadasVolumen.setOrdPorReparar(utilerias.formateaDecimales(tmp,Utilerias.FORMATO_NUMBER));
	}
}
