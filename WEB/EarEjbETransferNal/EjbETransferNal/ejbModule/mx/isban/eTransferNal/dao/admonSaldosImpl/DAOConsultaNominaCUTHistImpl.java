package mx.isban.eTransferNal.dao.admonSaldosImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.admonSaldo.BeanConsNomCUT;
import mx.isban.eTransferNal.beans.admonSaldo.BeanReqConsNominaCUT;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResConsNomCUTHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.admonSaldos.DAOConsultaNominaCUTHist;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaNominaCUTHistImpl extends Architech implements DAOConsultaNominaCUTHist{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3781290837234280302L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_DATOS_CUT
	 */
	private static final String QUERY_DATOS_CUT = 
	" SELECT TO_CHAR(FCH_OPERACION,'DD-MM-YYYY') FCH_OPERACION, CANTIDAD_REC_CUT,MONTO_REC_CUT, CANTIDAD_PREREC_CUT,MONTO_PREREC_CUT " +
	" FROM tran_spei_cut T "+
	" WHERE TRUNC(FCH_OPERACION) = TO_DATE(?,'DD-MM-YYYY') ";

	@Override
	public BeanResConsNomCUTHistDAO consultarNomina(BeanReqConsNominaCUT req,
			ArchitechSessionBean sessionBean) {
		  BigDecimal montoRecCut = BigDecimal.ZERO;
		  BigDecimal montoPreRecCut = BigDecimal.ZERO;
		  Integer cantidadPagos = null;
		  Integer cantidadPagosPre = null;
	      final BeanResConsNomCUTHistDAO beanResConsNomCUTHistDAO = new BeanResConsNomCUTHistDAO();
	      List<BeanConsNomCUT> listBeanConsNomCUT = new ArrayList<BeanConsNomCUT>(); 
	      Utilerias utilerias = Utilerias.getUtilerias();
	      BeanConsNomCUT beanConsNomCUT = null;
	      List<HashMap<String,Object>> list = null;
	      ResponseMessageDataBaseDTO responseDTO = null;
	      try{
	         responseDTO = HelperDAO.consultar(QUERY_DATOS_CUT, Arrays.asList(
	        		 new Object[]{req.getFechaOperacion()}
	         ),HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	         if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
	            list = responseDTO.getResultQuery();
	            for(HashMap<String,Object> map:list){
	            	beanConsNomCUT = new BeanConsNomCUT(); 
	            	beanConsNomCUT.setFechaOperacion(utilerias.getString(map.get("FCH_OPERACION")));
	            	montoRecCut = utilerias.getBigDecimal(map.get("MONTO_REC_CUT"));
	            	beanConsNomCUT.setMonto(utilerias.formateaDecimales(montoRecCut, Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
	            	cantidadPagos = utilerias.getInteger(map.get("CANTIDAD_REC_CUT"));
	            	beanConsNomCUT.setCantidadPagos(utilerias.formateaDecimales(cantidadPagos, Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
	            	montoPreRecCut = utilerias.getBigDecimal(map.get("MONTO_PREREC_CUT"));
	            	beanConsNomCUT.setMontoPre(utilerias.formateaDecimales(montoPreRecCut, Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
	            	cantidadPagosPre = utilerias.getInteger(map.get("CANTIDAD_PREREC_CUT"));
	            	beanConsNomCUT.setCantidadPagosPre(utilerias.formateaDecimales(cantidadPagosPre, Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
	            	listBeanConsNomCUT.add(beanConsNomCUT);
	            	beanResConsNomCUTHistDAO.setListBeanConsNomCUT(listBeanConsNomCUT);
	            }
	            beanResConsNomCUTHistDAO.setCodError(Errores.OK00000V);
	            beanResConsNomCUTHistDAO.setTipoError(Errores.TIPO_MSJ_INFO);
		        beanResConsNomCUTHistDAO.setMsgError(responseDTO.getMessageError());
	         }else{
	        	 beanResConsNomCUTHistDAO.setCodError(Errores.ED00011V);
	        	 beanResConsNomCUTHistDAO.setTipoError(Errores.TIPO_MSJ_INFO);
	        	 
	         }
	      } catch (ExceptionDataAccess e) {
	         showException(e);
	         beanResConsNomCUTHistDAO.setCodError(Errores.EC00011B);
	         beanResConsNomCUTHistDAO.setTipoError(Errores.TIPO_MSJ_ALERT);
	      }
	      return beanResConsNomCUTHistDAO;
	}

}
