/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOBitacoraEnvioImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvioDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo DAO que se encarga obtener la informacion para la bitacora de
 * envio
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitacoraEnvioImpl extends Architech implements DAOBitacoraEnvio {

	/**
	 * Constante privada del tipo StringBuilder que almacena el query para
	 * obtener los envios
	 */
	public static final StringBuilder CONSULTA_BITACORA_ENVIO = new StringBuilder()
			.append("SELECT T.* FROM (SELECT T.*, ROWNUM R FROM (SELECT TSB.FOLIO_SOLICITUD, TSB.TIPO_MENSAJE, ")
			.append("TSB.FCH_HORA_ENVIO, TSB.NUM_PAGOS, CASE WHEN TSB.SALDO_RESERVADO > 0 THEN TSB.SALDO_RESERVADO ELSE TSB.TOTAL_MONTO END AS IMPORTE,")
			.append("TSB.CVE_INST_BENEF, TSB.NUM_REPROCESOS, TSB.RECIBI_ACUSE, ")
			.append("TSB.BYTES_ACUMULADOS, TSB.OPERACION_CERT, TSB.NUM_CERTIFICADO, ")
			.append("C.CONT ")
			.append("FROM TRAN_SPID_BITENV TSB, (SELECT COUNT(*) CONT FROM TRAN_SPID_BITENV) C [ORDENAMIENTO]) T ) T ")
			.append("WHERE R BETWEEN ? AND ?");

	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos los envios
	 * exportados
	 */
	private static final StringBuilder INSERT_TODOS_BITACORA_ENVIO = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * Constante privada del tipo StringBuilder que almacena el query para
	 * obtener los envios
	 */
	public static final StringBuilder CONSULTA_BITACORA_ENVIO_SUM = new StringBuilder()
			.append("SELECT SUM(IMPORTE) MONTO_TOTAL FROM (SELECT T.*, ROWNUM R FROM (SELECT TSB.FOLIO_SOLICITUD, TSB.TIPO_MENSAJE, ")
			.append("TSB.FCH_HORA_ENVIO, TSB.NUM_PAGOS, CASE WHEN TSB.SALDO_RESERVADO > 0 THEN TSB.SALDO_RESERVADO ELSE TSB.TOTAL_MONTO END AS IMPORTE,")
			.append("TSB.CVE_INST_BENEF, TSB.NUM_REPROCESOS, TSB.RECIBI_ACUSE, ")
			.append("TSB.BYTES_ACUMULADOS, TSB.OPERACION_CERT, TSB.NUM_CERTIFICADO, ")
			.append("C.CONT ")
			.append("FROM TRAN_SPID_BITENV TSB, (SELECT COUNT(*) CONT FROM TRAN_SPID_BITENV) C [ORDENAMIENTO]) T ) T ");

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 8911452161423709858L;

	/**
	 * Metodo que sirve para consultar los envios
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param sortField
	 *            Objeto del tipo String
	 * @param sortType
	 *            Objeto del tipo String
	 * @return BeanResBitacoraEnvioDAO
	 */
	@Override
	public BeanResBitacoraEnvioDAO obtenerBitacoraEnvio(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType) {
		final BeanResBitacoraEnvioDAO beanResBitacoraEnvioDAO = new BeanResBitacoraEnvioDAO();
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<BeanBitacoraEnvio> listaBeanBitacoraEnvio = new ArrayList<BeanBitacoraEnvio>();

		try {
			String ordenamiento = "";

			if (sortField != null && !"".equals(sortField)) {
				ordenamiento = new StringBuilder(" ORDER BY ")
						.append(sortField).append(" ").append(sortType)
						.toString();
			}

			responseDTO = HelperDAO.consultar(CONSULTA_BITACORA_ENVIO
					.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento),
					Arrays.asList(new Object[] { beanPaginador.getRegIni(),
							beanPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				if (!list.isEmpty()) {
					llenarBitacoraEnvio(beanResBitacoraEnvioDAO, list,
							utilerias, listaBeanBitacoraEnvio);
				}
			}

			beanResBitacoraEnvioDAO
					.setListBeanBitacoraEnvio(listaBeanBitacoraEnvio);
			beanResBitacoraEnvioDAO.setCodError(responseDTO.getCodeError());
			beanResBitacoraEnvioDAO.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return beanResBitacoraEnvioDAO;
	}

	/**
	 * Metodo para llenar la bitacora de envio
	 * 
	 * @param beanResBitacoraEnvioDAO
	 *            Objeto del tipo BeanResBitacoraEnvioDAO
	 * @param list
	 *            Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias
	 *            Objeto del tipo Utilerias
	 * @param listaBeanBitacoraEnvio
	 *            Objeto del tipo List<BeanBitacoraEnvio>
	 */
	private void llenarBitacoraEnvio(
			final BeanResBitacoraEnvioDAO beanResBitacoraEnvioDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias,
			List<BeanBitacoraEnvio> listaBeanBitacoraEnvio) {
		BeanBitacoraEnvio beanBitacoraEnvio;
		Integer zero = Integer.valueOf(0);

		for (HashMap<String, Object> mapResult : list) {
			beanBitacoraEnvio = new BeanBitacoraEnvio();
			beanBitacoraEnvio.setFolioSolicitud(Long.parseLong(""
					+ mapResult.get("FOLIO_SOLICITUD") + ""));
			beanBitacoraEnvio.setTipoDeMensaje(utilerias.getString(mapResult
					.get("TIPO_MENSAJE")));
			beanBitacoraEnvio.setFechaHoraEnvio(utilerias.getString(mapResult
					.get("FCH_HORA_ENVIO")));
			beanBitacoraEnvio.setNumeroPagos(utilerias.getString(mapResult
					.get("NUM_PAGOS")));
			beanBitacoraEnvio.setImporteAbono(utilerias.getString(mapResult
					.get("IMPORTE")));
			beanBitacoraEnvio.setCodBanco(utilerias.getString(mapResult
					.get("CVE_INST_BENEF")));
			beanBitacoraEnvio.setNumReprocesos(utilerias.getString(mapResult
					.get("NUM_REPROCESOS")));
			beanBitacoraEnvio.setAcuseRecibo(utilerias.getString(mapResult
					.get("RECIBI_ACUSE")));
			beanBitacoraEnvio.setBytesAcumulados(utilerias.getString(mapResult
					.get("BYTES_ACUMULADOS")));
			beanBitacoraEnvio.setOperacionCert(utilerias.getString(mapResult
					.get("OPERACION_CERT")));
			beanBitacoraEnvio.setNumCertificado(utilerias.getString(mapResult
					.get("NUM_CERTIFICADO")));

			listaBeanBitacoraEnvio.add(beanBitacoraEnvio);
			if (zero.equals(beanResBitacoraEnvioDAO.getTotalReg())) {
				beanResBitacoraEnvioDAO.setTotalReg(utilerias
						.getInteger(mapResult.get("CONT")));
			}
		}
	}

	/**
	 * Metodo que sirve para exportar todos los envios
	 * 
	 * @param beanReqBitacoraEnvio
	 *            Objeto del tipo @see BeanReqBitacoraEnvio
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @return BeanResBitacoraEnvioDAO
	 */
	public BeanResBitacoraEnvioDAO exportarTodoBitacoraEnvio(
			BeanReqBitacoraEnvio beanReqBitacoraEnvio,
			ArchitechSessionBean architechSessionBean) {
		StringBuilder builder = new StringBuilder()
				.append("SELECT TSB.FOLIO_SOLICITUD,")
				.append("TSB.TIPO_MENSAJE, TSB.FCH_HORA_ENVIO, TSB.NUM_PAGOS,")
				.append("CASE WHEN TSB.SALDO_RESERVADO > 0 THEN TSB.SALDO_RESERVADO ELSE TSB.TOTAL_MONTO END,")
				.append("TSB.CVE_INST_BENEF, TSB.NUM_REPROCESOS, TSB.RECIBI_ACUSE,")
				.append("TSB.BYTES_ACUMULADOS, TSB.OPERACION_CERT, TSB.NUM_CERTIFICADO ")
				.append("FROM TRAN_SPID_BITENV TSB");
		final BeanResBitacoraEnvioDAO beanResBitacoraEnvioDAO = new BeanResBitacoraEnvioDAO();
		Utilerias utilerias = Utilerias.getUtilerias();
		final Date fecha = new Date();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			final String fechaActual = utilerias.formateaFecha(fecha,
					Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
			final String nombreArchivo = "SPID_BIT_ENV_" + fechaActual + ".csv";
			beanResBitacoraEnvioDAO.setNombreArchivo(nombreArchivo);

			responseDTO = HelperDAO.insertar(
					INSERT_TODOS_BITACORA_ENVIO.toString(),
					Arrays.asList(new Object[] {
							architechSessionBean.getUsuario(),
							architechSessionBean.getIdSesion(), "SPID",
							"BITACORA_ENVIO", nombreArchivo,
							beanReqBitacoraEnvio.getTotalReg(), "PE",
							builder.toString().replaceAll(",", " || ',' || "), 
							"FOLIO, TIPO MENSAJE, FECHA/HORA ENVIO, NUM. PAGOS, IMPORTE, BANCO, NUM. REPROCESOS, ACUSE, BYTES, OPE. CERTIFICADO, NUM. CERTIFICADO" }), 
							HelperDAO.CANAL_GFI_DS,
					this.getClass().getName());

			beanResBitacoraEnvioDAO.setCodError(responseDTO.getCodeError());
			beanResBitacoraEnvioDAO.setMsgError(responseDTO.getMessageError());

		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResBitacoraEnvioDAO.setCodError(Errores.EC00011B);
		}

		return beanResBitacoraEnvioDAO;
	}

	/**
	 * 
	 * @param architechSessionBeans
	 *            tipo ArchitechSessionBean
	 * @param sortField
	 *            tipo String
	 * @param sortType
	 *            tipo String
	 * @return BigDecimal
	 */
	public BigDecimal obtenerImporteTotalTopo(
			ArchitechSessionBean architechSessionBeans, String sortField,
			String sortType) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		ResponseMessageDataBaseDTO responseDTO = null;

		try {
			String ordenamiento = "";

			if (sortField != null && !"".equals(sortField)) {
				ordenamiento = new StringBuilder(" ORDER BY ")
						.append(sortField).append(" ").append(sortType)
						.toString();
			}

			responseDTO = HelperDAO.consultar(CONSULTA_BITACORA_ENVIO_SUM
					.toString().replaceAll("\\[ORDENAMIENTO\\]", ordenamiento),
					Arrays.asList(new Object[] {  }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				totalImporte = (BigDecimal) responseDTO.getResultQuery().get(0).get("MONTO_TOTAL");
				
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
		}
		return totalImporte;
	}
}
