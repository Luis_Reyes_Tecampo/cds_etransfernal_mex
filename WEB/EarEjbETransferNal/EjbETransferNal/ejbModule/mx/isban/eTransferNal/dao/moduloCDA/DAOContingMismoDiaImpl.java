/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOContingMismoDiaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:02:04 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDiaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDiaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo DAO que se encarga obtener la informacion para la
 * funcionalidad de Contingencia CDA Mismo Dia
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOContingMismoDiaImpl extends Architech implements
		DAOContingMismoDia {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 7810151412692301707L;

	/**
	 * Constante del tipo String que almacena la consulta pasa obtener los
	 * registros de archivos de contingencia diario
	 */
	private static final String QUERY_CONSULTA_CONTING_MISMO_DIA = " SELECT TMP.NOMBRE_ARCHIVO,TMP.NOMBRE_ARCHIVO_ORIG,TMP.FCH_HORA,TMP.USUARIO,"
			+ "            CASE(TMP.ESTATUS) " 
			+ "              WHEN 'PE' THEN 'PENDIENTE' "
			+ "              WHEN 'EP' THEN 'EN PROCESO' "
			+ "              WHEN 'OK' THEN 'FINALIZADO' "
			+ "              WHEN 'KO' THEN 'CON ERROR' END ESTATUS, "
			+ " TMP.OPE_X_GEN,TMP.OPE_GEN,TMP.CONT "
			+ " FROM ( "
			+ "     SELECT TMP.*, ROWNUM AS RN "
			+ "            FROM ( "
			+ "            SELECT CDA.NOMBRE_ARCHIVO,CDA2.NOMBRE_ARCHIVO NOMBRE_ARCHIVO_ORIG,CDA.FCH_HORA,CDA.USUARIO,CDA.ESTATUS,CDA.OPE_X_GEN,CDA.OPE_GEN, "
			+ "            CASE(CDA.ESTATUS) " 
			+ "              WHEN 'PE' THEN 3 "
			+ "              WHEN 'EP' THEN 1 "
			+ "              WHEN 'OK' THEN 2 "
			+ "              WHEN 'KO' THEN 4 END ORDEN, "
			+ "             CONTADOR.CONT "
			+ "            FROM (SELECT COUNT(1) CONT FROM TRAN_SPEI_CTG_CDA WHERE TIPO_PETICION IN ('C','H')) CONTADOR, "
		    + "      TRAN_SPEI_CTG_CDA CDA LEFT JOIN TRAN_SPEI_CTG_CDA CDA2 ON (CDA.ID_PET_PADRE = CDA2.ID_PETICION) "
			+ "            WHERE CDA.TIPO_PETICION IN ('C','H') "
			+ "            ORDER BY ORDEN ASC"
			+ "            )TMP "
			+ "      ) TMP "
			+ "     WHERE RN BETWEEN ? AND ? "
			+ " ORDER BY ORDEN ASC,FCH_HORA ";

	/**
	 * Constante del tipo String que almacena el insert para la generacion de
	 * nuevo archivo
	 */
	private static final String QUERY_INSERT_TRAN_SPEI_CTG_CDA = " INSERT INTO TRAN_SPEI_CTG_CDA (FCH_OPERACION,ID_PETICION, TIPO_PETICION, USUARIO, SESION, MODULO, FCH_HORA, ESTATUS) "
			+ " SELECT sysdate,(NVL(MAX(TO_NUMBER(ID_PETICION)),0)+1), ?,?,?,?,SYSDATE,? FROM TRAN_SPEI_CTG_CDA ";

	/***
	 * Constante que almacena la consulta de la ultima peticion
	 */
	private static final String QUERY_CONSULTA_ULT_PETICION = "SELECT ID_PETICION,OPE_X_GEN,OPE_GEN FROM TRAN_SPEI_CTG_CDA WHERE ID_PETICION IN( "
			+ " SELECT MAX(TO_NUMBER(ID_PETICION)) ID_PETICION FROM TRAN_SPEI_CTG_CDA WHERE TRIM(TIPO_PETICION) = ? "
			+ " ) ";

	/**
	 * Constante que almacena la consulta para saber si hay operaciones pendientes
	 */
	private static final String QUERY_PENDIENTES_CDA = 
	" SELECT 'PENDIENTES' TIPO, COUNT(*) CANTIDAD, NVL(SUM(MONTO),0) MONTO "+
	" FROM TRAN_SPEI_REC  "+
	" WHERE (cda = '0' OR cda = '3') AND "+
	" tipo_pago = 1 AND estatus_transfer = 'TR' ";

	/**
	 * Metodo DAO para obtener la informacion para la funcionalidad de
	 * Contingencia CDA mismo dia
	 * 
	 * @param beanReqPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDiaDAO Objeto del tipo
	 *         BeanResContingMismoDiaDAO
	 * 
	 */
	public BeanResContingMismoDiaDAO consultaContingMismoDia(
			BeanPaginador beanReqPaginador,
			ArchitechSessionBean architechSessionBean) {
		BeanContingMismoDia beanContingMismoDia = null;
		final BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = new BeanResContingMismoDiaDAO();
		List<HashMap<String, Object>> list = null;
		List<BeanContingMismoDia> listBeanContingMismoDia = Collections
				.emptyList();
		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_CONTING_MISMO_DIA,
					Arrays.asList(new Object[] { beanReqPaginador.getRegIni(),
							beanReqPaginador.getRegFin() }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			list = responseDTO.getResultQuery();
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				listBeanContingMismoDia = new ArrayList<BeanContingMismoDia>();
				for (HashMap<String, Object> map : list) {
					beanContingMismoDia = new BeanContingMismoDia();
					beanContingMismoDia.setNomArchivo(utilerias.getString(map
							.get("NOMBRE_ARCHIVO")));
					beanContingMismoDia.setNomArchivoOrig(utilerias.getString(map
							.get("NOMBRE_ARCHIVO_ORIG")));

					beanContingMismoDia.setFechaHora(utilerias.formateaFecha(
							map.get("FCH_HORA"),
							Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
					beanContingMismoDia.setUsuario(utilerias.getString(map
							.get("USUARIO")));
					beanContingMismoDia.setEstatus(utilerias.getString(map
							.get("ESTATUS")));
					beanContingMismoDia.setNumOpGenerar(utilerias
							.formateaDecimales(map.get("OPE_X_GEN"),
									Utilerias.FORMATO_NUMBER));
					beanContingMismoDia.setNumOpGeneradas(utilerias
							.formateaDecimales(map.get("OPE_GEN"),
									Utilerias.FORMATO_NUMBER));
					listBeanContingMismoDia.add(beanContingMismoDia);
					beanResContingMismoDiaDAO.setTotalReg(utilerias
							.getInteger(map.get("CONT")));
				}

			}
			beanResContingMismoDiaDAO
					.setListBeanContingMismoDia(listBeanContingMismoDia);
			beanResContingMismoDiaDAO.setCodError(responseDTO.getCodeError());
			beanResContingMismoDiaDAO
					.setMsgError(responseDTO.getMessageError());
			
			
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResContingMismoDiaDAO.setCodError(Errores.EC00011B);
		}
		return beanResContingMismoDiaDAO;
	}
	
	/**
	 * Metodo DAO para obtener la informacion para la funcionalidad de
	 * Contingencia CDA mismo dia
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDiaDAO Objeto del tipo
	 *         BeanResContingMismoDiaDAO
	 * 
	 */
	public BeanResContingMismoDiaDAO consultaPendientesMismoDia(
			ArchitechSessionBean architechSessionBean) {
		final BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = new BeanResContingMismoDiaDAO();
		List<HashMap<String, Object>> list = null;

		Utilerias utilerias = Utilerias.getUtilerias();
		ResponseMessageDataBaseDTO responseDTO = null;
		try {
			responseDTO = HelperDAO.consultar(QUERY_PENDIENTES_CDA,
					Arrays.asList(new Object[] { }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			
			list = responseDTO.getResultQuery();
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				for (HashMap<String, Object> map : list) {
					beanResContingMismoDiaDAO.setOperacionesPendientes(utilerias.getInteger(map.get("CANTIDAD")));
				}
			}
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResContingMismoDiaDAO.setCodError(Errores.EC00011B);
		}
		return beanResContingMismoDiaDAO;
	}

	/**
	 * Metodo DAO para guardar la informacion que se requiere para generar los
	 * archivos de contingencia cda mismo dia
	 * 
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResGenArchContingMismoDiaDAO Objeto del tipo
	 *         BeanResGenArchContingMismoDiaDAO
	 * */
	public BeanResGenArchContingMismoDiaDAO genArchContingMismoDia(
			ArchitechSessionBean architechSessionBean) {
		final BeanResGenArchContingMismoDiaDAO beanResGenArchContingMismoDiaDAO = new BeanResGenArchContingMismoDiaDAO();
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> list = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		try {
			responseDTO = HelperDAO.insertar(QUERY_INSERT_TRAN_SPEI_CTG_CDA,
					Arrays.asList(new Object[] { "C",
							architechSessionBean.getUsuario(),
							architechSessionBean.getIdSesion(), "CONTMISDIA",
							"PE" }), HelperDAO.CANAL_GFI_DS, this.getClass()
							.getName());

			responseDTO = HelperDAO.consultar(QUERY_CONSULTA_ULT_PETICION,
					Arrays.asList(new Object[] { "C" }),
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());

			list = responseDTO.getResultQuery();
			if (responseDTO.getResultQuery() != null
					&& !responseDTO.getResultQuery().isEmpty()) {
				for (HashMap<String, Object> map : list) {
					beanResGenArchContingMismoDiaDAO.setIdPeticion(utilerias
							.getString(map.get("ID_PETICION")));
					beanResGenArchContingMismoDiaDAO.setNumOpGenerar(utilerias
							.getString(map.get("OPE_X_GEN")));
					beanResGenArchContingMismoDiaDAO
							.setNumOpGeneradas(utilerias.formateaDecimales(map
									.get("OPE_GEN"), Utilerias.FORMATO_NUMBER));
				}
			}
			beanResGenArchContingMismoDiaDAO.setCodError(responseDTO
					.getCodeError());
			beanResGenArchContingMismoDiaDAO.setMsgError(responseDTO
					.getMessageError());
		} catch (ExceptionDataAccess e) {
			showException(e);
			beanResGenArchContingMismoDiaDAO.setCodError(Errores.EC00011B);
		}
		return beanResGenArchContingMismoDiaDAO;
	}

}
