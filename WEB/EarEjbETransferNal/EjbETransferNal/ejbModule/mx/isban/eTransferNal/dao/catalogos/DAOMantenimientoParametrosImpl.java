/**
 * Clase de tipo DAO de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametrosDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.UtileriasImplMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DAOMantenimientoParametrosImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMantenimientoParametrosImpl extends DAOMantenimientoParametrosImplPart2 implements DAOMantenimientoParametros {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 7492842595206058210L;
	
	/** The Constant QUERY_COMBO_TIPO_PARAM. */
	private static final String QUERY_COMBO_TIPO_PARAM = "SELECT DESCRIPCION, CODIGO_GLOBAL FROM COMU_DETALLE_CD WHERE TIPO_GLOBAL = 'TR_TPPARAM' ";

	/** The Constant QUERY_COMBO_TIPO_DATO. */
	private static final String QUERY_COMBO_TIPO_DATO = "SELECT DESCRIPCION, CODIGO_GLOBAL FROM COMU_DETALLE_CD WHERE TIPO_GLOBAL = 'TR_TPDATO' ";
	
	/** The Constant QUERY_COUNT_PARAMETROS. */
	private static final String QUERY_COUNT_PARAMETROS = "select count(1) TOTAL from TRAN_PARAMETROS ";
	
	/** The Constant QUERY_INICIO_BUSQUEDA_PAGINADA. */
	private static final String QUERY_INICIO_BUSQUEDA_PAGINADA = "select CVE_PARAMETRO, CVE_PARAM_FALTA, VALOR_FALTA, TIPO_PARAMETRO, DESCRIPCION , RN from (select CVE_PARAMETRO, CVE_PARAM_FALTA, VALOR_FALTA, TIPO_PARAMETRO, DESCRIPCION , rownum RN from(";
	
	/** The Constant QUERY_CONSUL_PARAM. */
	private static final String QUERY_CONSUL_PARAM = "select CVE_PARAMETRO, CVE_PARAM_FALTA, VALOR_FALTA, TIPO_PARAMETRO, DESCRIPCION from TRAN_PARAMETROS ";
	
	/** The Constant QUERY_CONSUL_DATOS_PARAM. */
	private static final String QUERY_CONSUL_DATOS_PARAM = "select CVE_PARAMETRO, DESCRIPCION, TIPO_PARAMETRO, TIPO_DATO, LONGITUD, VALOR_FALTA, DEFINE_TRAMA, DEFINE_COLA, DEFINE_MECANISMO, POSICION, CVE_PARAM_FALTA, PROGRAMA from TRAN_PARAMETROS where TRIM(CVE_PARAMETRO)=?";
	
	/** The Constant QUERY_CONSUL_DATOS_PARAM_ORD. */
	private static final String QUERY_CONSUL_DATOS_PARAM_ORD = " ORDER BY CVE_PARAMETRO ASC";
	
	/** The Constant QUERY_FIN_BUSQUEDA_PAGINADA. */
	private static final String QUERY_FIN_BUSQUEDA_PAGINADA = ") TMP ) where RN between ? and ?";

	/** The Constant QUERY_INSERT_PARAM_CAMPOS. */
	private static final String QUERY_INSERT_PARAM_CAMPOS = "INSERT INTO TRAN_PARAMETROS (CVE_PARAMETRO, VALOR_FALTA, U_VERSION";

	/** The Constant QUERY_INSERT_PARAM_VALORES. */
	private static final String QUERY_INSERT_PARAM_VALORES = ") VALUES ( ?, ?, '!'";

	/** The Constant QUERY_UPDATE_PARAM. */
	private static final String QUERY_UPDATE_PARAM = "UPDATE TRAN_PARAMETROS SET VALOR_FALTA = ?";

	/** The Constant QUERY_UPDATE_PARAM_FILTRO. */
	private static final String QUERY_UPDATE_PARAM_FILTRO = " WHERE TRIM(CVE_PARAMETRO) = ?";

	/** The Constant QUERY_DELETE_PARAM. */
	private static final String QUERY_DELETE_PARAM = "DELETE FROM TRAN_PARAMETROS WHERE TRIM(CVE_PARAMETRO)=?";

	/** The Constant QUERY_CONSULTA_EXPORTAR_TODOS. */
	private static final String QUERY_CONSULTA_EXPORTAR_TODO = "SELECT CVE_PARAMETRO || ',' || CVE_PARAM_FALTA || ',' || VALOR_FALTA || ',' || TIPO_PARAMETRO || ',' || DESCRIPCION FROM TRAN_PARAMETROS ";

	/** The Constant QUERY_INSERT_PETICION_EXPORTAR_TODOS. */
	private static final String QUERY_INSERT_PETICION_EXPORTAR_TODO ="INSERT INTO TRAN_SPEI_EXP_CDA "
			+ "(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) values "
			+ "(sysdate,?,?,?,?,?,?,?,sysdate,?,?)";
	
	/** The Constant COLUMNAS_MANTENIMIENTO_PARAMETROS. */
	private static final String COLUMNAS_MANTENIMIENTO_PARAMETROS = "Cve. Parametro,Parametro Falta,Valor Falta,Tipo Parametro,Descripcion";

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#consultaOpcionesTipoParam(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO consultaOpcionesTipoParam(ArchitechSessionBean sessionBean) {
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
	    List<HashMap<String,Object>> listResult= new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;
		try{
    		List<Object> parametros = new ArrayList<Object>();

	    	//consultar total de operaciones
	    	responseDTO = HelperDAO.consultar(QUERY_COMBO_TIPO_PARAM, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

        	//obtner lista de resultados del query
	    	listResult = responseDTO.getResultQuery();
        	
	    	//se convierte la lista obtenida por el query y se setea en response
        	response.setOpcionesTipoParam(
        			obtnerListaOpciones(listResult));
        	
			//se obtiene el codigo de error del query
        	response.setCodError(responseDTO.getCodeError());
        	response.setMsgError(responseDTO.getMessageError());
        	
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
    	}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#consultaOpcionesTipoDato(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO consultaOpcionesTipoDato(ArchitechSessionBean sessionBean) {
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
	    List<HashMap<String,Object>> listResult= new ArrayList<HashMap<String,Object>>();
	    ResponseMessageDataBaseDTO responseDTO = null;
		try{
    		List<Object> parametros = new ArrayList<Object>();

	    	//consultar total de operaciones
	    	responseDTO = HelperDAO.consultar(QUERY_COMBO_TIPO_DATO, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

        	//obtner lista de resultados del query
	    	listResult = responseDTO.getResultQuery();

	    	//se convierte la lista obtenida por el query y se setea en response
        	response.setOpcionesTipoDato(
        			obtnerListaOpciones(listResult));
        	
			//se obtiene el codigo de error del query
        	response.setCodError(responseDTO.getCodeError());
        	response.setMsgError(responseDTO.getMessageError());
        	
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
    	}
		return response;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#consultarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros, boolean)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO consultarParametros(
			ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request) {
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
	    List<HashMap<String,Object>> listResult= new ArrayList<HashMap<String,Object>>();
	    Utilerias utilerias = Utilerias.getUtilerias();
	    ResponseMessageDataBaseDTO responseDTO = null;
		String filtrosQuery="";
		String queryPendientes="";
		String queryCount="";
		try{
    		List<Object> parametros = new ArrayList<Object>();

    		//se arma parametrizacion de la consulta
	    	filtrosQuery = armarParametrizacionSelect(parametros,request);
	    	
	    	//armar query
	    	queryCount = QUERY_COUNT_PARAMETROS + filtrosQuery;

	    	//consultar total de operaciones
	    	responseDTO = HelperDAO.consultar(queryCount, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());

	    	if(responseDTO.getResultQuery()!= null){
	    		listResult = responseDTO.getResultQuery();
	    		for(HashMap<String,Object> map:listResult){
	    			//obtnener total
	    			response.setTotalReg(utilerias.getInteger(map.get("TOTAL")));
	    			response.setCodError(responseDTO.getCodeError());
	    		}
	    	}
	    	//si total es mayor a 0
	    	if(response.getTotalReg() > 0){
	    		//obtner registro de inicio y fin de pagina
	    		parametros.add(request.getPaginador().getRegIni());
	    		parametros.add(request.getPaginador().getRegFin());

		    	//armar query
	    		queryPendientes = QUERY_INICIO_BUSQUEDA_PAGINADA + QUERY_CONSUL_PARAM + filtrosQuery + QUERY_CONSUL_DATOS_PARAM_ORD + QUERY_FIN_BUSQUEDA_PAGINADA;
	    		//Consulta de Operaciones Pendientes de Autorizar con Paginacion
		        responseDTO = HelperDAO.consultar(queryPendientes, parametros,
		        		HelperDAO.CANAL_GFI_DS, this.getClass().getName());
		        
		        //si hay resultados de la consulta
		        if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
		        	//obtner lista de resultados del query
		        	listResult = responseDTO.getResultQuery();
		        	response.setListaParametros(
		        			//metodo que convierte la lista del query en arrayList
		        			obtnenerListaParametros(listResult) );
		        	
					//se obtiene el codigo de error del query
		        	response.setMsgError(responseDTO.getMessageError());
		        }
	    	}
        	response.setCodError(responseDTO.getCodeError());
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
    	}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#altaParametroNuevo(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO altaParametroNuevo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) {
		UtileriasImplMantenimientoParametros utilsImplMantParam = UtileriasImplMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
		List<Object> parametros = new ArrayList<Object>();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    String claveParametro = request.getParametro();
	    String valorFalta = request.getValorFalta();
    	String valoresCampos = "";
		String queryInsert = "";
		try{
			//se agregan los parametros default
	    	parametros.add(claveParametro);
	    	parametros.add(valorFalta);
	    	//armar query
	    	valoresCampos = utilsImplMantParam.armarParametrizacionInsert(parametros, request, QUERY_INSERT_PARAM_VALORES);
	    	queryInsert = QUERY_INSERT_PARAM_CAMPOS + valoresCampos;

	    	//Insert nuevo parametro
	    	responseDTO = HelperDAO.insertar(queryInsert, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	
	    	//se obtiene y setea el codigo de error 
	    	response.setCodError(responseDTO.getCodeError());
	    	response.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
			//si el codigo de error es 1 (registro ya existe)
    		if("1".equals(e.getCode())){
    			//se envia codError ED00124V
	    		response.setCodError(Errores.ED00124V);
    		} else {
    			showException(e);
    			//se envia codError EC00011B
	    		response.setMsgError(Errores.DESC_EC00011B);
	    		response.setCodError(Errores.EC00011B);
    		}
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#consultarDatosParametro(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO consultarDatosParametro(ArchitechSessionBean sessionBean, String claveParametro) {
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
		List<HashMap<String,Object>> listResult = new ArrayList<HashMap<String,Object>>();
		List<Object> parametros = new ArrayList<Object>();
	    ResponseMessageDataBaseDTO responseDTO = null;
		String queryParam = "";
		try{
	    	//armar query
	    	queryParam = QUERY_CONSUL_DATOS_PARAM;
	    	parametros.add(claveParametro);

	    	//consultar total de operaciones
	    	responseDTO = HelperDAO.consultar(queryParam, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	
	    	//si el resultado es diferente de null
	    	if(responseDTO.getResultQuery() != null){
		    	listResult = responseDTO.getResultQuery();
		    	//se setea la lista de datos parametro
		    	response.setDatosParametro(
		    			//metodo que convierte listResul en ArrayList
		    			obtnerDatosParametro(listResult));
	    		response.setCodError(responseDTO.getCodeError());
	    		response.setMsgError(responseDTO.getMessageError());
	    	}
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
		}
		return response;
	} 
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#editarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO editarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) {
		UtileriasImplMantenimientoParametros utilsImplMantParam = UtileriasImplMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
		List<Object> parametros = new ArrayList<Object>();
	    ResponseMessageDataBaseDTO responseDTO = null;
	    String valorFalta = request.getValorFalta();
	    String keyParam = request.getKeyCveParam();
    	String camposActualizar = "";
		String queryUpdate = "";
		try{
	    	//armado de parametrizacion
	    	parametros.add(valorFalta);
	    	camposActualizar = utilsImplMantParam.armarParametrizacionUpdate(parametros, request);
	    	parametros.add(keyParam);
	    	
	    	//armado de query update
	    	queryUpdate = QUERY_UPDATE_PARAM + camposActualizar + QUERY_UPDATE_PARAM_FILTRO;

	    	//Update parametro
	    	responseDTO = HelperDAO.actualizar(queryUpdate, parametros,
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	
	    	//se obtiene y envia codigoError
	    	response.setCodError(responseDTO.getCodeError());
	    	response.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#eliminarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	@Override
	public BeanResMantenimientoParametrosDAO eliminarParametros(ArchitechSessionBean sessionBean, String claveParametro) {
		BeanResMantenimientoParametrosDAO response=new BeanResMantenimientoParametrosDAO();
	    ResponseMessageDataBaseDTO responseDTO = null;
		try{
	    	//Insert nuevo parametro
	    	responseDTO = HelperDAO.actualizar(QUERY_DELETE_PARAM,
	    			Arrays.asList(new Object[]{claveParametro}),
	    			HelperDAO.CANAL_GFI_DS, this.getClass().getName());
	    	//se obtiene y envia codigoError
	    	response.setCodError(responseDTO.getCodeError());
	    	response.setMsgError(responseDTO.getMessageError());
		}catch(ExceptionDataAccess e){
    		//se envia codigo error EC00011B
			showException(e);
    		response.setMsgError(Errores.DESC_EC00011B);
    		response.setCodError(Errores.EC00011B);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros#exportarTodo(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros, mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros)
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request, BeanResMantenimientoParametros responseConsulParams) {
		UtileriasImplMantenimientoParametros utilsImplMantParam = UtileriasImplMantenimientoParametros.getUtilerias();
		
		//Se crean los objetos necesarios para ejecutar el query
    	BeanResBase responseDAO = new BeanResBase();
        ResponseMessageDataBaseDTO responseDTO = null;
        Utilerias utilerias = Utilerias.getUtilerias();
        
        //Se genera el nombre del archivo para exportar
        Date fecha = new Date();
        StringBuilder builder = new StringBuilder();
        String fechaActual = utilerias.formateaFecha(fecha,Utilerias.FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS);
        String nombreArchivo = "CATALOGOS_MANTENIMIENTO_PARAMETROS_".concat(fechaActual);
        nombreArchivo = nombreArchivo.concat(".csv");

        //armado query exportarTodo
        builder.append(QUERY_CONSULTA_EXPORTAR_TODO);
        builder.append(
        		utilsImplMantParam.generarParametrizacionExportarTodo(request, QUERY_CONSUL_DATOS_PARAM_ORD));

        try{
        	//Consulta a la base de datos
        	responseDTO = HelperDAO.insertar(QUERY_INSERT_PETICION_EXPORTAR_TODO,
      			   Arrays.asList(new Object[]{
      					   sessionBean.getUsuario(),			//usuario
      					   sessionBean.getIdSesion(),			//idUsuario
      					   "CATALOGOS",							//modulo Catalogos
      					   "MANTENIMIENTO_PARAMETROS",			//menu mantenimiento parametros
      					   nombreArchivo,						//nombre de archivo a exportar
      					   responseConsulParams.getTotalReg(), 	//total de registros
      					   "PE",								//estado de exportacion
      					   builder.toString(),					//query exportarTodo
      					 COLUMNAS_MANTENIMIENTO_PARAMETROS}),
      			   HelperDAO.CANAL_GFI_DS, this.getClass().getName());
        	responseConsulParams.setNombreArchivo(nombreArchivo);
	    	//se obtiene y envia codigoError
        	responseDAO.setCodError(responseDTO.getCodeError());
        	responseDAO.setMsgError(responseDTO.getMessageError());
        }catch (ExceptionDataAccess e) {
        	//Cuando surga una excepcion se pone el codigo EC00011B = No se ha podido comunicar con los servicios
			showException(e);
        	responseDAO.setCodError(Errores.EC00011B);
	        responseDAO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return responseDAO;
	}
}