/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOParametrosPOACOAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.eTransferNal.beans.catalogos.BeanProcesosPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;

/**Implementa la interface DAOParametrosPOACOA */
@Stateless
public class DAOParametrosPOACOAImpl extends Architech implements
		DAOParametrosPOACOA {
	
	/** Version */
	private static final long serialVersionUID = 1L;
	
	/** Constenate cero. */
	private static final String CERO = "0";
	
	/** Constenate uno. */
	private static final String UNO = "1";
	
	/** Query que rigistra la activacion del proceso. */
	private static final String QUERY_INSERT_ACTIVACION = 
		"INSERT INTO TRAN_POA_COA_CTRL(CVE_MI_INSTITUC, NO_HAY_RETORNO, " +
		"FCH_OPERACION, ACTIVACION, FASE, CVE_USUARIO_ACTIV, FCH_ACTIV, GENERAR) " +
		"VALUES (?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,SYSDATE,'0')";
	
	/** Query que guarda TRAN_POA_COA_PARAM*/
	private static final String QUERY_INSERT_FIRMAS = 
		"INSERT INTO TRAN_POA_COA_PARAM(CVE_MI_INSTITUC, CIFRADO) VALUES (?,'1')";
	
	/** Query para actualizar registros de la tabla TRAN_POA_COA_PARAM */
	private static final String QUERY_ACTUALIZAR_FIRMAS = 
		"UPDATE TRAN_POA_COA_PARAM SET CVE_MI_INSTITUC = ?, CIFRADO = ? WHERE CVE_MI_INSTITUC = ?";
	
	/** Query que desactiva el proceso. */
	private static final String QUERY_ACTUALIZA_DESACTIVACION =
		"UPDATE TRAN_POA_COA_CTRL SET ACTIVACION = ?, FASE = ? " +
		"WHERE NO_HAY_RETORNO = ? AND ACTIVACION = ? " +
		"AND FCH_OPERACION = TO_DATE(?,'DD/MM/YYYY') AND CVE_MI_INSTITUC = ?";
	
	/** Actualiza cambio de fase en POACOA. */
	private static final String QUERY_ACTUALIZA_CAMBIO_POACOA =
		"UPDATE TRAN_POA_COA_CTRL SET ACTIVACION = ?, FASE = ?,  NO_HAY_RETORNO = ? " +
		"WHERE ACTIVACION = ? AND FCH_OPERACION = TO_DATE(?,'DD/MM/YYYY') AND CVE_MI_INSTITUC = ?";

	/** La constante CONS_TB_ACTUAL. */
	private static final String CONS_TB_ACTUAL = "TRAN_POA_COA_CTRL";
	
	/** La constante CONS_TB_SPID. */
	private static final String CONS_TB_SPID = "TRAN_POA_COA_SPID_CTRL";
	
	/**
	 * Inserta el registro para activar el proceso de COA o POA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para insertar BD
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase insertaActivacionPOACOA(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		 /** Se asignan los valores a la lista **/
		listaParametros.add(beanProcesosPOACOA.getCveInstitucion());
		listaParametros.add(CERO);
		listaParametros.add(beanProcesosPOACOA.getFchOperacion());
		listaParametros.add(beanProcesosPOACOA.getCveProceso());
		listaParametros.add(beanProcesosPOACOA.getFase());
		listaParametros.add(architectSessionBean.getUsuario());
		/** Se declara una variable para evaluar la consulta **/
		String query = QUERY_INSERT_ACTIVACION;
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
      	  query = query.replaceAll(CONS_TB_ACTUAL, CONS_TB_SPID);
        }
		try {
			 /**  Se ejecuta la operacion de insertar **/
			HelperDAO.insertar(query, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
		} catch (ExceptionDataAccess e) {
			/** Manejo de excepciones **/
			showException(e);
			beanResBase.setCodError(Errores.EC00011B);
			beanResBase.setMsgError(Errores.DESC_EC00011B);
		}
		/**  Retorno del metodo **/
		return beanResBase;
	}

	/**
	 * Actualiza el registro para desactivar o activar el proceso de COA o POA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para actualizar BD
	 * @param modulo El objeto: modulo
	 * @param esDesactivacion boolean que indica si se desactiva el POACOA
	 * @param nuevaCveOper Clave de operacion
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
    @Override
    public BeanResBase actualizaProcesoPOACOA(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
                boolean esDesactivacion, String nuevaCveOper,
                ArchitechSessionBean architectSessionBean){
    	 /** Se crea el bean de respuesta **/
    	BeanResBase beanResBase = new BeanResBase(); 
    	/** Se crea una lista para setear los parametros SQL **/
    	List<Object> listaParametros = new ArrayList<Object>();
          String query = null;
          if(esDesactivacion) {
        	  /** Se asignan los valores a la lista **/
                listaParametros.add(CERO);
                listaParametros.add(CERO);
                listaParametros.add(CERO);
                listaParametros.add(beanProcesosPOACOA.getCveProceso());
                listaParametros.add(beanProcesosPOACOA.getFchOperacion());
                listaParametros.add(beanProcesosPOACOA.getCveInstitucion());
                /** Se inicia la variable para evaluar la consulta **/
                query = QUERY_ACTUALIZA_DESACTIVACION;
          } else {
        	  /** Se asignan los valores a la lista **/
                listaParametros.add(nuevaCveOper);
                listaParametros.add(CERO);
                listaParametros.add(CERO);
                listaParametros.add(beanProcesosPOACOA.getCveProceso());
                listaParametros.add(beanProcesosPOACOA.getFchOperacion());
                listaParametros.add(beanProcesosPOACOA.getCveInstitucion());
                /** Se inicia la variable para evaluar la consulta **/
                query = QUERY_ACTUALIZA_CAMBIO_POACOA;
          }
          /** Se valida el modulo **/
          if ("2".equals(modulo)) {
        	  /** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
        	  query = query.replaceAll(CONS_TB_ACTUAL, CONS_TB_SPID);
          }
          try {
        	  /** Se ejecuta la operacion de actualizar **/
			HelperDAO.actualizar(query, listaParametros,
			              HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
	  		} catch (ExceptionDataAccess e) {
	  			/** Manejo de excepciones **/
	  			showException(e);
	  			beanResBase.setCodError(Errores.EC00011B);
	  			beanResBase.setMsgError(Errores.DESC_EC00011B);
	  		}
         /**  Retorno del metodo **/
		return beanResBase;

    }


    /**
	 * Actualiza del proceso COAPOA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para actualizar BD
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase actualizaFase(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResBase beanResBase = new BeanResBase();
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		String siguienteFase = String.valueOf(Integer.parseInt(beanProcesosPOACOA.getFase()));
		StringBuilder query = new StringBuilder(250);
		query.append("UPDATE TRAN_POA_COA_CTRL SET ");
		query.append("FASE = ?, LIQ_FINAL = ?");
		listaParametros.add(siguienteFase);
		listaParametros.add(beanProcesosPOACOA.getLiqFinal());
		/** Se valida la fase **/
		if("4".equals(siguienteFase)) {
			siguienteFase = CERO;
			query.append(",FCH_LIQ_FINAL = SYSDATE ");
			query.append(",CVE_USUARIO_LIQF = ? ");
			listaParametros.add(architectSessionBean.getUsuario());
		}
		query.append(" WHERE FASE = ?  AND FCH_OPERACION = TO_DATE(?,'DD/MM/YYYY') AND ")
		.append("CVE_MI_INSTITUC = ? AND (ACTIVACION = ? OR ACTIVACION = ?)");
		String siguienteAnt = String.valueOf(Integer.parseInt(beanProcesosPOACOA.getFase())-1);
		/** Se asignan los valores a la lista **/
		listaParametros.add(siguienteAnt);
		listaParametros.add(beanProcesosPOACOA.getFchOperacion());
		listaParametros.add(beanProcesosPOACOA.getCveInstitucion());
		listaParametros.add(UNO);
		listaParametros.add("2");
		/** Se declara una variable para evaluar la consulta **/
		String consultaEstatus = query.toString();
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			/** Si se cumple la condicion entonces se remplaza la consulta con los valores de las tablas del nuevo modulo **/
			consultaEstatus = consultaEstatus.replaceAll(CONS_TB_ACTUAL, CONS_TB_SPID);
		}
		try {
			/** Se ejecuta la operacion de actualizar **/
			HelperDAO.actualizar(consultaEstatus, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
  		} catch (ExceptionDataAccess e) {
  			/** Manejo de excepciones **/
  			showException(e);
  			beanResBase.setCodError(Errores.EC00011B);
  			beanResBase.setMsgError(Errores.DESC_EC00011B);
  		}
		/**  Retorno del metodo **/
  		return beanResBase;
		

	}

	/**
	 * Inserta parametro de cifrado.
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param architectSessionBean  Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase insertaParametroFirma(String cveInstitucion,
			ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResBase beanResBase = new BeanResBase(); 
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los valores a la lista **/
		listaParametros.add(cveInstitucion);
		try {
			/** Se ejecuta la operacion de insertar **/
			HelperDAO.insertar(QUERY_INSERT_FIRMAS, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
  		} catch (ExceptionDataAccess e) {
  			/** Manejo de excepciones **/
  			showException(e);
  			beanResBase.setCodError(Errores.EC00011B);
  			beanResBase.setMsgError(Errores.DESC_EC00011B);
  		}
		/**  Retorno del metodo **/
  		return beanResBase;
		

	}

	/**
	 * Actualiza parametro de cifrado.
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param cifrado 0 no firmado 1 firmado
	 * @param architectSessionBean  Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	@Override
	public BeanResBase actualizaParametroFirma(String cveInstitucion, String cifrado,
			ArchitechSessionBean architectSessionBean){
		 /** Se crea el bean de respuesta **/
		BeanResBase beanResBase = new BeanResBase(); 
		/** Se crea una lista para setear los parametros SQL **/
		List<Object> listaParametros = new ArrayList<Object>();
		/** Se asignan los valores a la lista **/
		listaParametros.add(cveInstitucion);
		listaParametros.add(cifrado);
		listaParametros.add(cveInstitucion);
		try {
			/** Se ejecuta la operacion de actualizar **/
			HelperDAO.actualizar(QUERY_ACTUALIZAR_FIRMAS, listaParametros,
					HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** Se setea el codigo de error **/
			beanResBase.setCodError(Errores.OK00000V);
  		} catch (ExceptionDataAccess e) {
  			/** Manejo de excepciones **/
  			showException(e);
  			beanResBase.setCodError(Errores.EC00011B);
  			beanResBase.setMsgError(Errores.DESC_EC00011B);
  		}
		/**  Retorno del metodo **/
  		return beanResBase;

	}
	

}
