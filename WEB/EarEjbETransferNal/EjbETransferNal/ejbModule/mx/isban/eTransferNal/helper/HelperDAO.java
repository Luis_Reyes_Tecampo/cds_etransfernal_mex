package mx.isban.eTransferNal.helper;

import java.util.List;

import mx.isban.agave.commons.beans.LoggingBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;


/** 
*   Isban Mexico
*   Clase: HelperDAO
*   Descripcion: .
*
*   Control de Cambios:
*   1.0 01/10/2013 13:12:35 Carlos Alberto Chong Antonio
*/
public final class HelperDAO {
	/**Contante que almacena el numero de registros por pagina*/
	public static final Integer REGISTROS_X_PAGINA=20;
	/**Contante que almacena el canal GFI*/
	public static final String CANAL_GFI_DS = "CANAL_GFI_DS";
	
	/**Contante que almacena el canal TRANSFER_PLU*/
	public static final String CANAL_TRANSFER_PLUS = "CANAL_TRANSFER_PLUS_DS";

	/**
	 * Constructor privado.
	 */
	private HelperDAO(){
		super();
	}
	
	/**
	 * Consultar.
	 *
	 * @param sql the sql
	 * @param parametros the parametros
	 * @param idCanal the id canal
	 * @param className el nombre de la clase que manda a llamar esta utilidad
	 * @return the response message data base dto
	 * @throws BusinessException the business exception
	 * @throws ExceptionDataAccess the exception data access
	 */
	public static ResponseMessageDataBaseDTO consultar(final String sql,final List<Object> parametros, 
			final String idCanal, final String className) 
	throws ExceptionDataAccess{
		ResponseMessageDataBaseDTO responseDTO = null;
		
   
		RequestMessageDataBaseDTO reqMessage = new RequestMessageDataBaseDTO();
		reqMessage.setQuery(sql);
		reqMessage.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_QUERY_PARAMS);
		reqMessage.setParamsToSql(parametros);

		
		// Ejecutando operacion
		DataAccess ida = DataAccess.getInstance(reqMessage, new LoggingBean());
		responseDTO = (ResponseMessageDataBaseDTO) ida.execute(idCanal);
   
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			throw new ExceptionDataAccess(className, responseDTO.getCodeError(), responseDTO.getMessageError());
		}
	
		return responseDTO;		
	}
	
	/**
	 * Insertar.
	 *
	 * @param sql the sql
	 * @param parametros the parametros
	 * @param idCanal the id canal
	 * @param className el nombre de la clase que manda a llamar esta utilidad
	 * @return the response message data base dto
	 * @throws BusinessException the business exception
	 * @throws ExceptionDataAccess the exception data access
	 */
	public static ResponseMessageDataBaseDTO insertar(final String sql,final List<Object> parametros, 
			final String idCanal, final String className) 
	throws ExceptionDataAccess{
		ResponseMessageDataBaseDTO responseDTO = null;
		RequestMessageDataBaseDTO reqMessage = new RequestMessageDataBaseDTO();
		reqMessage.setQuery(sql);
		reqMessage.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT_PARAMS);
		reqMessage.setParamsToSql(parametros);		
		// Ejecutando operacion
		DataAccess ida = DataAccess.getInstance(reqMessage, new LoggingBean());
		responseDTO = (ResponseMessageDataBaseDTO) ida.execute(idCanal);
   
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			throw new ExceptionDataAccess(className, responseDTO.getCodeError(), responseDTO.getMessageError());
		}
	
		return responseDTO;		
	}
	
	/**
	 * Update.
	 *
	 * @param sql the sql
	 * @param parametros the parametros
	 * @param idCanal the id canal
	 * @param className el nombre de la clase que manda a llamar esta utilidad
	 * @return the response message data base dto
	 * @throws BusinessException the business exception
	 * @throws ExceptionDataAccess the exception data access
	 */
	public static ResponseMessageDataBaseDTO actualizar(final String sql,final List<Object> parametros, 
			final String idCanal, final String className) 
	throws ExceptionDataAccess{
		ResponseMessageDataBaseDTO responseDTO = null;
		RequestMessageDataBaseDTO reqMessage = new RequestMessageDataBaseDTO();
		reqMessage.setQuery(sql);
		reqMessage.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_UPDATE_PARAMS);
		reqMessage.setParamsToSql(parametros);		
		// Ejecutando operacion
		DataAccess ida = DataAccess.getInstance(reqMessage, new LoggingBean());
		responseDTO = (ResponseMessageDataBaseDTO) ida.execute(idCanal);
   
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			throw new ExceptionDataAccess(className, responseDTO.getCodeError(), responseDTO.getMessageError());
		}
	
		return responseDTO;		
	}
	
	/**
	 * Metodo que permite realizar la eliminacion
	 * @param sql Objeto del tipo String con una consulta SQL
	 * @param parametros Objeto del tipo List<Object>
	 * @param idCanal String con el canal de comunicacion
	 * @param className String con el class name 
	 * @return ResponseMessageDataBaseDTO Bean del tipo ResponseMessageDataBaseDTO que almacena la respuesta
	 * @throws ExceptionDataAccess Exception del IsbanDataAccess
	 */
	public static ResponseMessageDataBaseDTO eliminar(final String sql,final List<Object> parametros, 
			final String idCanal, final String className) 
	throws ExceptionDataAccess{
		ResponseMessageDataBaseDTO responseDTO = null;
		RequestMessageDataBaseDTO reqMessage = new RequestMessageDataBaseDTO();
		reqMessage.setQuery(sql);
		reqMessage.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_DELETE_PARAMS);
		reqMessage.setParamsToSql(parametros);		
		// Ejecutando operacion
		DataAccess ida = DataAccess.getInstance(reqMessage, new LoggingBean());
		responseDTO = (ResponseMessageDataBaseDTO) ida.execute(idCanal);
   
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			throw new ExceptionDataAccess(className, responseDTO.getCodeError(), responseDTO.getMessageError());
		}
	
		return responseDTO;		
	}

}

	