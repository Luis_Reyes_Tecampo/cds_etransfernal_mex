/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * AppContextListener.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;


/**
 * @author Administrador
 *
 */
public class AppContextListener implements ServletContextListener {
	/**
	 * Constante referencia al LOG
	 */
	private static final Logger LOG = Logger.getLogger(AppContextListener.class);
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String isbanProvider = null;
		Properties rutaProporties = new Properties();
		Properties proporties = new Properties();
		InputStream inputPropoties = null;
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("ruta.properties");
		
		try {
			if(input!=null){
				rutaProporties.load(input);
				String ruta = (String)rutaProporties.get("ruta");
				input.close();
				input = null;
				File file = new File(ruta);
				if(file.exists()){
					inputPropoties = new FileInputStream(file);
					proporties.load(inputPropoties);
					isbanProvider = (String)proporties.get("isbprovider");
					inputPropoties.close();
					inputPropoties = null;
					System.setProperty("jndi.def.environmet.isbprovider", isbanProvider);
				}
			}
		} catch (IOException e) {
			LOG.error("Error de IO",e);
		}finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					LOG.error("Error de IO",e);
				}
			}
			if(inputPropoties!=null){
				try {
					inputPropoties.close();
				} catch (IOException e) {
					LOG.error("Error de IO",e);
				}
			}
		}
		
		
		
		 
		
	}

}
