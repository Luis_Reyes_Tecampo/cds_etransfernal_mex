/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BODetalleOperacionImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/10/2018 05:42:23 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.servicio.contingencia;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.contingencia.BeanResTranCanales;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.contingencia.DAODetalleOperacion;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.contingencia.UtileriasDetalleOperacion;


/**
 * Class BODetalleOperacionImpl.
 * 
 * Clase que implementa los metodos necesarios para poder realizar el tratado de la informacion en la capa de negocio
 * @author FSW-Vector
 * @since 11/10/2018
 */
@Remote(BODetalleOperacion.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BODetalleOperacionImpl extends Architech implements BODetalleOperacion{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8207464416849884466L;
	
	/** La variable que contiene informacion con respecto a: dao detalle. */
	@EJB
	private DAODetalleOperacion daoDetalle;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_MENSAJE_CANALES";
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "CANAL";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";

	/**
	 * Consultar.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param beanFilter El objeto: bean filter
	 * @param beanPaginador El objeto: bean paginador
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @return Objeto bean res tran canales
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranCanales consultar(ArchitechSessionBean session, String nombreArchivo, BeanFilter beanFilter,
			BeanPaginador beanPaginador,String nombrePantalla) throws BusinessException {
		/**Se inicia el paginador **/
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			/**se inicializa si viene null**/
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/**consulta los registros del archivo seleccionado **/
		BeanResTranCanales response = daoDetalle.consultar(session, nombreArchivo, beanFilter.getField(), beanFilter.getFieldValue(), beanPaginador, nombrePantalla);
		response.setBeanFilter(beanFilter);
		/**Cuando no se encuentran resultados**/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getRegistros().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		/**Cuando se ecuentran resultados**/
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		/**Cuando se genera un error**/
		} else {
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/**Retorna la Respuesta**/
		return response;
	}

	/**
	 * Exportar todos los registros.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param beanFilter El objeto: bean filter
	 * @param totalRegistros El objeto: total registros
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, String nombreArchivo, BeanFilter beanFilter, int totalRegistros, String nomPantalla)
			throws BusinessException {
		/**inicializa las varibles**/
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		/**genera los filtros**/
		String filtroWhere = UtileriasDetalleOperacion.getInstancia().getFiltro(beanFilter.getField(), beanFilter.getFieldValue(), "WHERE");
		String filtroArchivo = UtileriasDetalleOperacion.getInstancia().getFiltro("NOMBRE_ARCH", nombreArchivo, 
				UtileriasDetalleOperacion.getInstancia().validaClave(beanFilter.getField())).replace("UPPER", "");
		
		
		
		/**obtiene la consulta **/
		String query = UtileriasDetalleOperacion.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere).replaceAll("\\[FILTRO_ARCHIVO\\]", filtroArchivo)
				+ UtileriasDetalleOperacion.ORDEN;
		
		/**sE ASGINA HIS al inicio si la pantalla es historica **/
		if(("historica").equals(nomPantalla)) {
			query = query.replace(STR_NOMTABLA,"TRAN_MENSAJE_CANALES_HIS");
		}
		/**asigna los datos al objeto**/
		exportarRequest.setColumnasReporte(UtileriasDetalleOperacion.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CONTINGENC");
		exportarRequest.setSubModulo("DETALLE DE OPERACION");
		String nomRpt= "CONTINGENCIA_DETALLE_OPERACION_";
		/**sE ASGINA HIS al inicio si la pantalla es historica **/
		if(("historica").equals(nomPantalla)) {
			nomRpt = "HIS_"+nomRpt;
		}
		exportarRequest.setNombreRpt(nomRpt);				
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/**llama al metodo de exportar**/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/**genera los cofigo de error**/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodoDetalleOperacion.do", operacion, session, "NOMBRE_ARCH");
		
		return response;
	}

	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param operacion            El objeto: operacion
	 * @param urlController            El objeto: url controller
	 * @param resOp            El objeto: res op
	 * @param sesion            El objeto: sesion
	 * @param datoModificado El objeto: dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion, String datoModificado) {
		/** Objetos para las Pistas auditoras**/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/**Enviamos los datos de la pista auditora**/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(datoModificado);
		/**Cuando la operacion es correcta**/
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		/**Enviamos la peticion para el llenado de datos de la pista**/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
}
