/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonCDADetSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCdaDAOSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOConsultaMovMonHistCDASPID;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Session Bean implementation class BOConsultaMovHistCDAImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaMovMonHistCDASPIDImpl extends Architech implements BOConsultaMovMonHistCDASPID {
       
    /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -6609397847914445121L;

	/**
	 * Referencia privada al dao de consulta movimiento Historico CDA
	 */
	@EJB
	private transient DAOConsultaMovMonHistCDASPID daoConsultaMovHistCDASPID;	
	
	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovHistCDA Objeto del tipo @see BeanResConsMovHistCDA
	 * @throws BusinessException Exception de negocio
	 */
    public BeanResConsMovHistCDASPID consultarMovHistCDASPID(BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA,ArchitechSessionBean architechSessionBean) 
    throws BusinessException {
    	    
    	 	BeanPaginador paginador = null;
    	    BeanResConsMovHistCDASPID beanResConsMovHistCDA = new BeanResConsMovHistCDASPID();
    	    BeanResConsMovHistCdaDAOSPID resMovHistCdaSPIDDetDAO = null;
    	    List<BeanMovimientoCDASPID> listMovimientoDetCDASPID = null;
    	    paginador = beanReqConsMovHistCDA.getPaginador();
    	    String accion = "ACT";
			String vacio = "";
    	 
			//se inicializa beanPaginador
    	    if(paginador == null){
    	    	paginador = new BeanPaginador();
    	    	beanReqConsMovHistCDA.setPaginador(paginador);
    	    } else if(accion.equals(paginador.getAccion()) && vacio.equals(paginador.getPagina())){
    	    	paginador.setPagina("1");
    	    }
    	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
    	    //se consulta movimientos cda spid
		    resMovHistCdaSPIDDetDAO = daoConsultaMovHistCDASPID.consultarMovHistCDASPID(beanReqConsMovHistCDA,architechSessionBean);
		    listMovimientoDetCDASPID = resMovHistCdaSPIDDetDAO.getListBeanMovimientoCDA();
		    
		    //si consulta exitosa y listaMovimientos es vacia
		    if(Errores.CODE_SUCCESFULLY.equals(resMovHistCdaSPIDDetDAO.getCodError()) && listMovimientoDetCDASPID.isEmpty()){
		    	throw new BusinessException(Errores.ED00011V, Errores.DESC_ED00011V);
		    //si consulta Exitosa
		    }else if(Errores.CODE_SUCCESFULLY.equals(resMovHistCdaSPIDDetDAO.getCodError())){
			    beanResConsMovHistCDA.setTotalReg(resMovHistCdaSPIDDetDAO.getTotalReg());
			    beanResConsMovHistCDA.setListBeanMovimientoCDA(resMovHistCdaSPIDDetDAO.getListBeanMovimientoCDA());
			    paginador.calculaPaginas(beanResConsMovHistCDA.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			    beanResConsMovHistCDA.setBeanPaginador(paginador);		    
			    beanResConsMovHistCDA.setCodError(Errores.OK00000V);
		    }else {
		    	//se setea codigo de error EC00011B
		    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		     }
		    
		    return beanResConsMovHistCDA;
    }
	
    /**
	 * Metodo que se encarga de solicitar la insercion de la peticion de
	 * Exportar Todos de los Movimientos Historicos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConMovHistCDA  Objeto del tipo @see BeanResConsMovHistCDA 
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovHistCDASPID consultaMovHistCDAExpTodosSPID(BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	
    	BeanResConsMovHistCDASPID beanResConMovHistCDA = new BeanResConsMovHistCDASPID();
    	BeanResConsMovHistCdaDAOSPID beanResConsMovHistCdaDAO = null;
    	beanResConMovHistCDA = consultarMovHistCDASPID(beanReqConsMovHistCDA,architechSessionBean);	
    	beanReqConsMovHistCDA.setTotalReg(beanResConMovHistCDA.getTotalReg());
    	beanResConsMovHistCdaDAO = daoConsultaMovHistCDASPID.guardarConsultaMovHistExpTodoSPID(beanReqConsMovHistCDA, architechSessionBean);
    	
    	//si query ejecutado exitosamente
    	 if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovHistCdaDAO.getCodError())){
    		 beanResConMovHistCDA.setCodError(Errores.OK00001V);
    		 beanResConMovHistCDA.setTipoError(Errores.TIPO_MSJ_INFO);
    		 beanResConMovHistCDA.setNombreArchivo(beanResConsMovHistCdaDAO.getNombreArchivo());
 		}else{
	    	//se setea codigo de error EC00011B
	    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
		return beanResConMovHistCDA;
    }

    /**
	 * Metodo que sirve para solicitar el detalle de un movimiento Historico CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetHistCDA Objeto del tipo @see BeanResConsMovDetHistCDA
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovDetHistCDASPID consMovHistDetCDASPID(BeanReqConsMovMonCDADetSPID beanReqConsMovCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	BeanResConsMovDetHistCDASPID beanResConsMovDetHistCDA = new BeanResConsMovDetHistCDASPID();
		BeanResConsMovDetHistCdaDAOSPID beanResConsMovDetHistCdaDAO = null;
		beanResConsMovDetHistCdaDAO =  daoConsultaMovHistCDASPID.consultarMovDetHistCDASPID(beanReqConsMovCDA,architechSessionBean);
		
    	//si query ejecutado exitosamente
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovDetHistCdaDAO.getCodError())){
			beanResConsMovDetHistCDA.setCodError(Errores.OK00000V);
			beanResConsMovDetHistCDA.setBeanMovCdaDatosGenerales(beanResConsMovDetHistCdaDAO.getBeanMovCdaDatosGenerales());
			beanResConsMovDetHistCDA.setBeanConsMovCdaOrdenante(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaOrdenante());
			beanResConsMovDetHistCDA.setBeanConsMovCdaBeneficiario(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaBeneficiario());
		}else{
	    	//se setea codigo de error EC00011B
	    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
		return beanResConsMovDetHistCDA;
    }

}
