/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOAvisoTraspasosImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOAvisoTraspasos;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio del catalogo 
 *de aviso traspaso
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAvisoTraspasosImpl implements BOAvisoTraspasos {

	/**
	 * Referencia privada al dao de consulta
	 */
@EJB
private DAOAvisoTraspasos dAOAvisoTraspasos;

	/**
	 * Metodo que sirve para consultar los aviso traspaso 
	 * @param paginador Objeto del tipo @see BeanPaginador 
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param sortField Objeto del tipo String
	 * @param sortType Objeto del tipo String
	 * @param cveInst Objeto del tipo String
	 * @param fch Objeto del tipo String
	 * @return beanResAvisoTraspasos Objeto del tipo BeanResAvisoTraspasos
	 */
	@Override
	public BeanResAvisoTraspasos obtenerAviso(BeanPaginador paginador, ArchitechSessionBean architechSessionBean,
			String sortField, String sortType, String cveInst, String fch) {

		BeanResAvisoTraspasosDAO beanResAvisoTraspasosDAO = null;
		final BeanResAvisoTraspasos beanResAvisoTraspasos = new BeanResAvisoTraspasos();

		BeanPaginador pag = paginador;
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		String sortF = getSortField(sortField);
		
		if (sortType == null){
			sortType = "ASC";
		}

		beanResAvisoTraspasosDAO = dAOAvisoTraspasos.obtenerAviso(paginador, architechSessionBean, sortF, sortType,
				cveInst, fch);

		if (beanResAvisoTraspasosDAO.getListaConAviso() == null) {
			beanResAvisoTraspasosDAO.setListaConAviso(new ArrayList<BeanAvisoTraspasos>());
		}

		if (Errores.CODE_SUCCESFULLY.equals(beanResAvisoTraspasosDAO.getCodError())
				&& beanResAvisoTraspasosDAO.getListaConAviso().isEmpty()) {
			beanResAvisoTraspasos.setCodError(Errores.ED00011V);
			beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResAvisoTraspasos.setMsgError(Errores.DESC_ED00011V);
			beanResAvisoTraspasos.setListaConAviso(beanResAvisoTraspasosDAO.getListaConAviso());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResAvisoTraspasosDAO.getCodError())) {
			beanResAvisoTraspasos.setListaConAviso(beanResAvisoTraspasosDAO.getListaConAviso());
			beanResAvisoTraspasos.setTotalReg(beanResAvisoTraspasosDAO.getTotalReg());
			beanResAvisoTraspasos.setImporteTotal(dAOAvisoTraspasos.obtenerImporteTotal(architechSessionBean, sortF, sortType, cveInst, fch).toString());
			paginador.calculaPaginas(beanResAvisoTraspasosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResAvisoTraspasos.setBeanPaginador(paginador);
			beanResAvisoTraspasos.setCodError(Errores.OK00000V);
		} else {
			beanResAvisoTraspasos.setCodError(Errores.EC00011B);
			beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResAvisoTraspasos.setMsgError(Errores.DESC_EC00011B);
		}

		return beanResAvisoTraspasos;
	}

/**
 * @param sortField Objeto del tipo String
 * @return String
 */
private String getSortField(String sortField) {
	String sortF = "";
	
	if ("hora".equals(sortField)){
		sortF = "HORA_RECEPCION";
	} else if ("folio".equals(sortField)){
		sortF = "FOLIO_SERVIDOR";
	} else if ("typTrasp".equals(sortField)){
		sortF = "DSC";
	} else if ("importe".equals(sortField)){
		sortF = "MONTO";
	} else if ("SIAC".equals(sortField)){
		sortF = "REFERENCIA_SIAC";
	} else if ("SIDV".equals(sortField)){
		sortF = "REFERENCIA_SIDV";
	} else {
		sortF = "FOLIO_SERVIDOR";
	}
	return sortF;
}

/**
 * Metodo get para obtener el valor de DAOAvisoTraspasos
 * @return el dAOAvisoTraspasos objeto de tipo @see DAOAvisoTraspasos
 */
public DAOAvisoTraspasos getDAOAvisoTraspasos() {
	return dAOAvisoTraspasos;
}

/**
 * Metodo para establecer el valor del DAOAvisoTraspasos
 * @param dAOAvisoTraspasos objeto de tipo @see DAOAvisoTraspasos
 */
public void setDAOAvisoTraspasos(DAOAvisoTraspasos dAOAvisoTraspasos) {
	this.dAOAvisoTraspasos = dAOAvisoTraspasos;
}

/**Metodo que sirve para exportar todos los avisos traspaso
 * * @param beanReqAvisoTraspasos Objeto del tipo @see BeanReqAvisoTraspasos
   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
   * @param cveInst Objeto del tipo String
   * @param fch Objeto del tipo String
   * @throws BusinessException Exception
   * @return beanResAvisoTraspasos Objeto del tipo BeanResAvisoTraspasos
   */
@Override
public BeanResAvisoTraspasos exportarTodoAvisoTraspasos(
		BeanReqAvisoTraspasos beanReqAvisoTraspasos, ArchitechSessionBean architechSessionBean, String cveInst, String fch) throws BusinessException {
	BeanResAvisoTraspasosDAO beanResAvisoTraspasosDAO = null;
	BeanResAvisoTraspasos beanResAvisoTraspasos = null;
	
	BeanPaginador paginador = beanReqAvisoTraspasos.getPaginador();
	
	if (paginador == null){
		  paginador = new BeanPaginador();
	}
	paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
	
	beanResAvisoTraspasos = obtenerAviso(paginador, architechSessionBean, "", "", cveInst, fch);
	beanResAvisoTraspasos.setTotalReg(beanReqAvisoTraspasos.getTotalReg());
	beanResAvisoTraspasosDAO = dAOAvisoTraspasos.exportarTodoAvisoTraspasos(beanReqAvisoTraspasos, architechSessionBean,cveInst,fch);
	
	if (beanResAvisoTraspasosDAO.getListaConAviso()==null) {
		beanResAvisoTraspasosDAO.setListaConAviso(new ArrayList<BeanAvisoTraspasos>());
	}
	
	if (Errores.CODE_SUCCESFULLY.equals(beanResAvisoTraspasosDAO.getCodError())) {
		beanResAvisoTraspasos.setCodError(Errores.OK00002V);
		beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_INFO);
		beanResAvisoTraspasos.setNombreArchivo(beanResAvisoTraspasosDAO.getNombreArchivo());
	} else {
		beanResAvisoTraspasos.setCodError(Errores.EC00011B);
		beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_ERROR);
	}
	
	return beanResAvisoTraspasos;
}

}
