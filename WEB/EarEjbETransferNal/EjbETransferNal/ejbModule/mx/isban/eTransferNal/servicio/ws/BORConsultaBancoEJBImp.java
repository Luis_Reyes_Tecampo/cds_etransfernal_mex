/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORConsultaBancoEJBImp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicio.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.ws.ResBeanConsultaBancoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOConsultaBanco;
import mx.isban.eTransferNal.servicios.ws.BORConsultaBancoEJB;
import mx.isban.eTransferNal.ws.ResBeanConsultaBanco;

/**
 * Implementacion de negocio que tiene la funcionalidad de consulta
 * @author cchong
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORConsultaBancoEJBImp extends Architech implements BORConsultaBancoEJB{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3212380737726941953L;
	
	/**Referencia del servicio DAOConsultaBanco*/
	@EJB
	private DAOConsultaBanco daoConsultaBanco;

	@Override
	public ResBeanConsultaBanco consultaBanco(String prefijoCuentaClabe,ArchitechSessionBean architechSessionBean) {
		info("Entro a la funcionalidad de consultaBanco parametro:"+prefijoCuentaClabe);
		ResBeanConsultaBanco resBeanConsultaBanco = new ResBeanConsultaBanco();
		if(validaParametro(prefijoCuentaClabe)){
			ResBeanConsultaBancoDAO resBeanConsultaBancoDAO = daoConsultaBanco.consultaBanco(prefijoCuentaClabe, architechSessionBean);
			if(Errores.OK00000V.equals(resBeanConsultaBancoDAO.getCodError())){
				resBeanConsultaBanco.setCveBanco(resBeanConsultaBancoDAO.getCveBanco());
				resBeanConsultaBanco.setNombreBanco(resBeanConsultaBancoDAO.getNombreBanco());
				resBeanConsultaBanco.setPrefijoCuentaClabe(resBeanConsultaBancoDAO.getPrefijoCuentaClabe());
				resBeanConsultaBanco.setCodError(resBeanConsultaBancoDAO.getCodError());
				resBeanConsultaBanco.setMsgError(resBeanConsultaBancoDAO.getMsgError());
				
				
			}else{
				info("Hay un error de la funcionalidad de consultaBanco parametro:"+prefijoCuentaClabe+":error:"+resBeanConsultaBanco.getCodError()+
						"::"+resBeanConsultaBanco.getNombreBanco());
				resBeanConsultaBanco.setCodError(resBeanConsultaBancoDAO.getCodError());
				resBeanConsultaBanco.setMsgError(resBeanConsultaBancoDAO.getMsgError());
			}
		}else{
			resBeanConsultaBanco.setCodError(Errores.ED00027V);
			resBeanConsultaBanco.setMsgError(Errores.DESC_ED00027V);
		}
		info("Salgo de la funcionalidad de consultaBanco parametro:"+prefijoCuentaClabe+":cveBanco:"+resBeanConsultaBanco.getCveBanco()+
				"::"+resBeanConsultaBanco.getNombreBanco());
		return resBeanConsultaBanco;
	}
	
	/***
	 * Metodo que permite validar el prefijo de 3 posiciones
	 * @param prefijoCuentaClabe String con el prefijo
	 * @return boolean que regresa true si es valido y false si no
	 */
	private boolean validaParametro(String prefijoCuentaClabe){
		boolean flag = false;
		try{
			flag = prefijoCuentaClabe != null && prefijoCuentaClabe.length()==3; 
			Integer.parseInt(prefijoCuentaClabe);
		}catch(NumberFormatException e){
			showException(e);
			flag = false;
		}
		return flag;
	}

	/**Metodo get que sirve para obtener una referencia del objeto del tipo DAOConsultaBanco
	 * @return DAOConsultaBanco Referencia del servicio de consulta banco
	 */
	public DAOConsultaBanco getDaoConsultaBanco() {
		return daoConsultaBanco;
	}
	/**Metodo set que sirve para cambiar una referencia del objeto del tipo DAOConsultaBanco
	 * @param daoConsultaBanco Referencia del servicio de consulta banco
	 */
	public void setDaoConsultaBanco(DAOConsultaBanco daoConsultaBanco) {
		this.daoConsultaBanco = daoConsultaBanco;
	} 
	
	

}
