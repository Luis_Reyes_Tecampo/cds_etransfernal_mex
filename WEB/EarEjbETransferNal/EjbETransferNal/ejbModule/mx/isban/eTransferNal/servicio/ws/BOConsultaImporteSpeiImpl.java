/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BOConsultaImporteSpeiImpl.java
 *
 * Control de versiones:
 * Version	Date/Hour				By							 Company	Description
 * -------	----------------------	---------------------------  ----------	----------------------
 * 1.0.0	02/10/2016 11:27:15 AM	Carlos Alberto Chong Antonio Isban		Creacion
 * 1.0.1	04/04/2018 12:00:00 AM  Juan Manuel Fuentes Ramos	 CSA		Implementacion modulo metrics
 */
package mx.isban.eTransferNal.servicio.ws;
//Imports java
import java.util.HashMap;
import java.util.Map;
//Imports javax
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
//Imports etransferNal
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.dao.ws.DAOConsultaImporteSpei;
import mx.isban.eTransferNal.servicios.ws.BOConsultaImporteSpei;
import mx.isban.eTransferNal.utilerias.comunes.ValidaCadenas;
import mx.isban.eTransferNal.utilerias.comunes.Validate;
import mx.isban.eTransferNal.utilerias.comunes.ValidateFechas;
//Imports metrics
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;
//Imports net
import net.sf.json.JSONObject;

/**
 * Class: BOConsultaImporteSpeiImpl
 * @author ING. Juan Manuel Fuentes Ramos. CSA
 * Plan Delta Sucursales. 2018 - 06
 * Se implementa monitoreo a metrics
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@SuppressWarnings("unchecked")
public class BOConsultaImporteSpeiImpl extends Architech implements BOConsultaImporteSpei{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -394150545446005436L;
	/** Propiedad del tipo String que almacena el valor de FORMATO_FECHA_HHMMSS. */
	public static final String FORMATO_FECHA_HHMMSS = "HH:mm:ss";
	/** Propiedad del tipo String que almacena el valor de REG_IMPORTE_MAXIMO. */
	public static final String REG_IMPORTE_MAXIMO_13_2 = "([0-9]{0,13})|([0-9]{0,13}[.]{1}[0-9]{1,2})";
	/**Referencia del servicio DAOConsultaImporteSpei*/
	@EJB private DAOConsultaImporteSpei dAOConsultaImporteSpei;
	/** Propiedad del tipo String que almacena el valor de PIPE. */
	private static final String PIPE ="|";
	/** Constante DESCRIPCION. */
	private static final String DESCRIPCION = "DESCRIPCION";
	/** Constante COD_ERROR. */
	private static final String COD_ERROR = "COD_ERROR";
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** EJB para bitacora metrics*/
	@EJB private BitacorizaMetrics metricsBo;
	/** Bean para monitoreo*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	// END [ADD: JMFR CSA Monitoreo Metrics]

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.isban.eTransferNal.servicios.ws.BOConsultaImporteSpei#consultaImporteSpei(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO,
	 * mx.isban.metrics.dto.DTOMetricsException)
	 */
	@Override
	public ResEntradaStringJsonDTO consultaImporteSpei(EntradaStringJsonDTO param) {
		// Se crea objeto de respuesta
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		Map<String, String> salida = new HashMap<String, String>();
		//Se obtiene cadena json
		String json = param.getCadenaJson();
		JSONObject jsonObject = JSONObject.fromString(json);
		Map<String,String> mapMovimientos =  (Map<String,String>)JSONObject.toBean(jsonObject, Map.class);
		//Se crea objeto para validar cadena de peticion
		ValidaCadenas validaCadena = new ValidaCadenas();
		Validate validate = new Validate();
		ValidateFechas validateFechas = new ValidateFechas();
		//Se validan parametros de peticion
		if(!validaCadena.esValidoCadena(mapMovimientos.get("CVE_OPERACION"), 8)){
			salida.put(COD_ERROR, "ED00112V");
			salida.put(DESCRIPCION,"El campo  cve de operacion esta vacio o tiene un formato erroneo");
			goMetricaErrorNegocio("ED00112V","El campo  cve de operacion esta vacio o tiene un formato erroneo");
		}else if(!validaCadena.esValidoCadena(mapMovimientos.get("CVE_TRANSFE"), 3)){
			salida.put(COD_ERROR, "ED00113V");
			salida.put(DESCRIPCION,"El campo  cve de transferencia esta vacio o tiene un formato erroneo");
			goMetricaErrorNegocio("ED00113V","El campo  cve de transferencia esta vacio o tiene un formato erroneo");
		}else if(!validaCadena.esValidoCadena(mapMovimientos.get("CVE_MEDIO_ENT"), 6)){
			salida.put(COD_ERROR, "ED00114V");
			salida.put(DESCRIPCION,"El campo  medio de entrega esta vacio o tiene un formato erroneo");
			goMetricaErrorNegocio("ED00114V","El campo  medio de entrega esta vacio o tiene un formato erroneo");
		}else if(!validate.esValidoNumero(mapMovimientos.get("MONTO"), REG_IMPORTE_MAXIMO_13_2)){
			salida.put(COD_ERROR,"ED00115V");
			salida.put(DESCRIPCION,"El campo  monto esta vacio o tiene un formato erroneo");
			goMetricaErrorNegocio("ED00115V","El campo  monto esta vacio o tiene un formato erroneo");
		}else if(!validaCadena.esValidoCadenaVacia(mapMovimientos.get("NUM_CTA_ORD"), 20)){
			salida.put(COD_ERROR, "ED00116V");
			salida.put(DESCRIPCION,"El campo numero cuenta ordenante tiene un formato erroneo");
			goMetricaErrorNegocio("ED00116V","El campo numero cuenta ordenante tiene un formato erroneo");
		}else if(!validateFechas.esValidoFechaVacia(mapMovimientos.get("HORA"),FORMATO_FECHA_HHMMSS)){
			salida.put(COD_ERROR, "ED00117V");
			salida.put(DESCRIPCION,"El formato del campo Hora Futura es erroneo");
			goMetricaErrorNegocio("ED00117V","El formato del campo Hora Futura es erroneo");
		}else if(!validate.esValidoNumeroVacio(mapMovimientos.get("INHABIL"),Validate.REG_NUMERICO_1)){
			salida.put(COD_ERROR, "ED00118V");
			salida.put(DESCRIPCION,"El formato del campo Es Inhabil es erroneo");
			goMetricaErrorNegocio("ED00118V","El formato del campo Es Inhabil es erroneo");
		}else{
			salida = armaTrama(mapMovimientos);//Se ejecuta peticion 
		}
		jsonObject = JSONObject.fromMap(salida);//Se le da formato a la trama de respuesta
		resEntradaStringJsonDTO.setJson(jsonObject.toString());//Se asigna trama respuesta 
		//Se regresa cadena de respuesta
		return resEntradaStringJsonDTO;
	}

	/**
	 * Metodo encargado de armar trama de peticion e iniciar ejecucion de transaccion 
	 * 
	 * @param mapMovimientos Objetoo del tipo Map<String, String> con los datos de la operacion
	 * @return Map Objeto del tipo Map<String, String>
	 */
	private Map<String, String> armaTrama(Map<String,String> mapMovimientos){
		Map<String, String> salida = new HashMap<String, String>();
		//Se crea objeto de respuesta
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		//Se crea objeto para trama de peticion
		StringBuilder trama = new StringBuilder();
		trama.append("TRANSIMP");
		trama.append(mapMovimientos.get("CVE_OPERACION"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("CVE_TRANSFE"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("NUM_CTA_ORD"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("CVE_MEDIO_ENT"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("MONTO"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("HORA"));
		trama.append(PIPE);
		trama.append(mapMovimientos.get("INHABIL"));
		info("Trama peticion: "+ trama.toString());
		//Se ejecuta peticion MQ
		resBeanEjecTranDAO = dAOConsultaImporteSpei.consultaImporteSpei(trama.toString());
		info("Trama Respuesta: "+resBeanEjecTranDAO.getTramaRespuesta());
		//Se asigna trama respuesta a objeto String
		String respuesta = resBeanEjecTranDAO.getTramaRespuesta();
		//Se separa la trama respuesta y se guarda en arreglo de tipo String
		String[] str= respuesta.split("\\|");
		//[ADD: JMFR CSA - Se valida que la primera posision no sea nula o vacia
		//Ya que esto indica que hubo falla critica en el servicio]
		if(str[0] == null || "".equals(str[0])){
			//Se asigna codigo de error provicional para validar respuesta desde 
			//la capa del servicio e indicar que el error fue critico
			salida.put(COD_ERROR, "DELTA999");
		}else{
			//Se asigna codigo de error
			salida.put(COD_ERROR, str[0]);
		}		
		salida.put(DESCRIPCION,"");
		if(str.length>1){
			salida.put(DESCRIPCION,str[1]);
		}
		//Se valida respuesta de error.
		validaResponse(resBeanEjecTranDAO);
		//Se retorna respuesta del servicio
		return salida;
	}
	
	/**
	 * Metodo para insertar error monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error 
	 * @param msgError mensaje de error
	 * @param traza traza completa del error
	 */
	private void insertaError(String codeError, String msgError, String traza){
		senderInit.initMetrics();
		metricsBo.bitacorizaError(senderInit.getDTOError(codeError,msgError,traza));
	}
	
	/**
	 * Metodo para asignar datos de error para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error
	 * @param traza traza completa del error
	 */
	private void goMetricaErrorNegocio(String codeError, String traza){
		insertaError(EnumMetricsErrors.DELTA_NEGOCIO_BO.getCodeError(), 
				EnumMetricsErrors.DELTA_NEGOCIO_BO.getMensaje()+
				BOConsultaImporteSpeiImpl.class.getCanonicalName() + " " +codeError, traza);
	}
	
	/**
	 * Metodo para asignar datos de error para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error
	 * @param traza traza completa del error
	 */
	private void goMetricaErrorCritico(String codeError, String traza){
		insertaError(EnumMetricsErrors.DELTA_INFRA_BO.getCodeError(), 
				EnumMetricsErrors.DELTA_INFRA_BO.getMensaje()+
				BOConsultaImporteSpeiImpl.class.getCanonicalName() + " " +codeError, traza);
	}
	
	/**
	 * Metodo para validar respuesta del servicio
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param resBeanEjecTranDAO respuesta del servicio
	 */
	private void validaResponse(ResBeanEjecTranDAO resBeanEjecTranDAO){
		String codeError = resBeanEjecTranDAO.getCodError();
		if(!ResBeanEjecTranDAO.COD_EXITO.equals(codeError)){
			goMetricaErrorCritico(codeError, 
					resBeanEjecTranDAO.getMsgError() + " " + resBeanEjecTranDAO.getTramaRespuesta());
		}
	}
}