/**
 * Clase de tipo BO de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametrosDAO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOMantenimientoParametros;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.UtileriasPistasMtoParametros;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * The Class BOMantenimientoParametrosImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMantenimientoParametrosImpl extends Architech implements BOMantenimientoParametros {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -7319156900465485864L;
	
	/** The Constant TBL_TRAN_PARAMETROS. */
	private static final String TBL_TRAN_PARAMETROS = "TRAN_PARAMETROS";
	
	/** The Constant GUARDA_PARAM. */
	private static final String GUARDA_PARAM = "guardarParametro.do";
	
	/** The Constant CVE_PARAM. */
	private static final String CVE_PARAM = "CVE_PARAMETRO";
	
	/** The Constant CVE_PARAM. */
	private static final String CAMPO_PARAMETRO = "CVE_PARAMETRO=";
	
	/** DAO para participantes. */
	@EJB
	private DAOMantenimientoParametros daoMantenimientoParametros;

	/**
	 * Referencia al servicio dao DAOBitacoraAdmon.
	 *
	 * @return the bean res mantenimiento parametros
	 */
	@EJB
	private DAOBitAdministrativa daoPistaAuditoria;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#consultaOpcionesCombos(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros consultaOpcionesCombos(ArchitechSessionBean sessionBean) throws BusinessException {
		List<BeanMantenimientoParametros> opcionesTipoParam=new ArrayList<BeanMantenimientoParametros>();
		List<BeanMantenimientoParametros> opcionesTipoDato=new ArrayList<BeanMantenimientoParametros>();
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		
		//se consultan las opciones del combo tipoParametros
		responseDAO=daoMantenimientoParametros.consultaOpcionesTipoParam(sessionBean);
		if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
			opcionesTipoParam=responseDAO.getOpcionesTipoParam();
			response.setOpcionesTipoParam(opcionesTipoParam);
		} else {
			//se arroja nueva BusinessException
			throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
		}
		
		//se consultan las opciones del combo tipoDatos
		responseDAO=daoMantenimientoParametros.consultaOpcionesTipoDato(sessionBean);
		if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
			opcionesTipoDato=responseDAO.getOpcionesTipoDato();
			response.setOpcionesTipoDato(opcionesTipoDato);
		}else{
			//se arroja nueva BusinessException
			throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
		}
		return response;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#consultarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros consultarParametros(
			ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request) throws BusinessException {
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros responseCombos=new BeanResMantenimientoParametros();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		List<BeanMantenimientoParametros> listaParametros=new ArrayList<BeanMantenimientoParametros>();
		
		// se crea el objeto bean paginador
	 	BeanPaginador paginador = null;
	    paginador = request.getPaginador();

	    //si paginador es null
	    if( paginador == null ){
	    	//se inicializa paginador
	    	paginador = new BeanPaginador();
	    	request.setPaginador(paginador);
	    }
	    
	    //agregar numero registros por pagina
    	paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

    	//se consultan opciones de combo tipoParametro y tipoDato
	    responseCombos = consultaOpcionesCombos(sessionBean);
    	
    	//se consultan los parametros
		responseDAO=daoMantenimientoParametros.consultarParametros(sessionBean, request);
		listaParametros=responseDAO.getListaParametros();

		//si no hay parametros
	    if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) && listaParametros.isEmpty() ){
	    	//se envia error "no se encontro informacion"
	    	response.setCodError(Errores.ED00011V);
	    	response.setTipoError(Errores.TIPO_MSJ_INFO);
	    	response.setMsgError(Errores.DESC_ED00011V);
		//si hay parametros
	    }else if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
	    	//se calculan las paginas de bean paginador
		    paginador.calculaPaginas(responseDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
	    	response.setTotalReg(responseDAO.getTotalReg());
	    	response.setCodError(Errores.OK00000V);
	    	response.setListaParametros(listaParametros);
		    response.setPaginador(paginador);
	    } else if( Errores.EC00011B.equals(responseDAO.getCodError()) ) {
	    	// se arroja codigo de error EC00011B
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
	    }
	    response.setOpcionesTipoParam(responseCombos.getOpcionesTipoParam());
	    response.setOpcionesTipoDato(responseCombos.getOpcionesTipoDato());
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#altaParametroNuevo(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros altaParametroNuevo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) throws BusinessException {
		UtileriasPistasMtoParametros utilsPistasMantParam = UtileriasPistasMtoParametros.getUtilerias();
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;

		//se realiza el alta del nuevo parametro
		responseDAO = daoMantenimientoParametros.altaParametroNuevo(sessionBean, request);
		//si el parametro fue dado de alta correctamente
		if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setCodError(Errores.OK00003V);
			
			//construccion bean pistasAuditoria
	    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("OK", TBL_TRAN_PARAMETROS, "INSERT", GUARDA_PARAM,
	    			" ", 															//valor anterio
	    			utilsPistasMantParam.generarValNuevoAltaEditar(request),		//valor nuevo
					CVE_PARAM, CVE_PARAM, "ALTA NUEVO PARAMETRO", sessionBean);
	    	// se genera pista de auditoria cuando ok
	    	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
    	} else if(Errores.EC00011B.equals(responseDAO.getCodError())) {
			//construccion bean pistasAuditoria
        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("NOK", TBL_TRAN_PARAMETROS, "INSERT", GUARDA_PARAM,
					" ", "", // campos valor anterior y valor nuevo
					CVE_PARAM, CVE_PARAM, "HA OCURRIDO UN ERROR AL INTENTAR DAR DE ALTA NUEVO PARAMETRO", sessionBean);
	    	// se genera pista de auditoria cuando nok
        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
			//se obtiene y arroja codigo de error de apartarOperacion
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());

    	} else if(Errores.ED00124V.equals(responseDAO.getCodError())) {
    		//si codError es ED00124V (si el registro ya existe)
			response.setCodError(responseDAO.getCodError());
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
    	}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#consultarDatosParametro(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	@Override
	public BeanResMantenimientoParametros consultarDatosParametro(ArchitechSessionBean sessionBean, String claveParametro) throws BusinessException {
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		List<BeanMantenimientoParametros> datosParametro=new ArrayList<BeanMantenimientoParametros>();
    	
		//se consultan opciones de combos tipoParametro y tipoDato
		response = consultaOpcionesCombos(sessionBean);
		
		//se consultan los datos del parametro seleccionado
		responseDAO=daoMantenimientoParametros.consultarDatosParametro(sessionBean, claveParametro);
		datosParametro=responseDAO.getDatosParametro();

		//si el query se ejecuto correctamente
	    if(Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError())){
	    	response.setDatosParametro(datosParametro);
		//si hubo un error al ejecutar el query
	    }else {
	    	// se arroja codigo de error EC00011B
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
	    }
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#editarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros editarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) throws BusinessException {
		UtileriasPistasMtoParametros utilsPistasMantParam = UtileriasPistasMtoParametros.getUtilerias();
		BeanResMantenimientoParametrosDAO responseDatosParam=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		String valAnterior="";
		String valNuevo="";

		//se consulta los datos del parametro antes de ser actualizado
		responseDatosParam=daoMantenimientoParametros.consultarDatosParametro(sessionBean, request.getKeyCveParam());
		//se el update del parametro
		responseDAO = daoMantenimientoParametros.editarParametros(sessionBean, request);
		
		//se ejecuto correctamente el query consultarDatosParametro
		if( Errores.CODE_SUCCESFULLY.equals(responseDatosParam.getCodError()) ){
			valAnterior=utilsPistasMantParam.generarValAnteriorEditarParametro(responseDatosParam.getDatosParametro());
			valNuevo=utilsPistasMantParam.generarValNuevoAltaEditar(request);
		} else {
			//se arroja nuevo businessException
			throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
		}
		//si update ejecutado correctamente
		if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setCodError(Errores.OK00003V);
			//construccion bean pistasAuditoria
	    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("OK", TBL_TRAN_PARAMETROS, "UPDATE", GUARDA_PARAM,
	    			valAnterior,	//valor anterio
	    			valNuevo,		//valor nuevo
					CVE_PARAM, CVE_PARAM, "EDITAR PARAMETRO", sessionBean);
	    	// se genera pista de auditoria cuando ok
	    	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
    	} else {
        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("NOK", TBL_TRAN_PARAMETROS, "UPDATE", GUARDA_PARAM,
        			valAnterior, "", //campos valorAnterior y valorNuevo
					CVE_PARAM, CVE_PARAM, "HA OCURRIDO UN ERROR AL INTENTAR EDITAR EL PARAMETRO", sessionBean);
	    	// se genera pista de auditoria cuando nok
        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
			//se obtiene y arroja codigo de error de apartarOperacion
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
    	}
		return response;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#eliminarParametros(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros eliminarParametros(ArchitechSessionBean sessionBean,
			BeanReqMantenimientoParametros request, List<BeanMantenimientoParametros> listParametrosSeleccionados) throws BusinessException {
		BeanResMantenimientoParametrosDAO responseDAO=new BeanResMantenimientoParametrosDAO();
		BeanResMantenimientoParametros response=new BeanResMantenimientoParametros();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int paramEliminado=0;

		// se recorre la lista de parametros seleccionados
		for(BeanMantenimientoParametros elemento:listParametrosSeleccionados){
			String cveParametro=elemento.getCveParametro();
			responseDAO = daoMantenimientoParametros.eliminarParametros(sessionBean, cveParametro);
			if( Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) ){
    			//se incrementa contador parametros eliminados
				paramEliminado++;
				
				//construccion bean pistasAuditoria
		    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("OK", TBL_TRAN_PARAMETROS, "DELETE", "eliminarParametros.do",
		    			CAMPO_PARAMETRO+cveParametro,		//valor anterio
						"",										//valor nuevo
						CVE_PARAM, CVE_PARAM, "ELIMINAR PARAMETROS", sessionBean);
		    	// se genera pista de auditoria cuando ok
		    	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	} else {
				//construccion bean pistasAuditoria
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("NOK", TBL_TRAN_PARAMETROS, "DELETE", "eliminarParametros.do",
	        			CAMPO_PARAMETRO+cveParametro,		//valor anterio
						"",										//valor nuevo
						CVE_PARAM, CVE_PARAM, "HA OCURRIDO UN ERROR AL INTENTAR ELIMINAR LOS PARAMETROS", sessionBean);
		    	// se genera pista de auditoria cuando nok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	}
		}
		
		//si contador paramEliminado es igual al tamaño lista listParametrosSeleccionados
		if( paramEliminado == listParametrosSeleccionados.size() ){
			//se consultan los parametros para repintado
			response = consultarParametros(sessionBean, request);
			
			//se envia mensaje Transaccion Exitosa
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setCodError(Errores.OK00004V);
		} else if(paramEliminado == 0) {
			//se obtiene y arroja codigo de error de businessException
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
		} else {
			//se envia mensaje Transaccion Exitosa
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
			response.setMsgError("no fue posible borrar todos los parametros seleccionados");
			response.setCodError(Errores.OK00000V);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros#exportarTodo(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMantenimientoParametros exportarTodo(
			ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request) throws BusinessException {
		BeanResMantenimientoParametros response=consultarParametros(sessionBean, request);
		if(Errores.OK00000V.equals(response.getCodError())){
			//Se hace la consulta con el dao para exportar las operaciones
			BeanResBase resExportarTodo = daoMantenimientoParametros.exportarTodo(sessionBean, request, response);
			if(Errores.CODE_SUCCESFULLY.equals(resExportarTodo.getCodError())){
				response.setCodError(Errores.OK00001V);
				response.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				throw new BusinessException(resExportarTodo.getCodError(), resExportarTodo.getMsgError());
			}
		}
		return response;
	}
}