/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOInitModuloSPIDImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanInitModuloSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResInitModuloSPIDDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOInitModuloSPID;

/**
 *Clase del tipo BO que se encarga  del negocio del InitModuloSPID
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOInitModuloSPIDImpl implements BOInitModuloSPID {
	
	/** Referencia privada al dao  */
	@EJB
	private DAOInitModuloSPID daoInitModuloSPID;

	
	/**
	 * Metodo para la obtencion de los header.
	 *
	 * @param tipo El objeto: tipo de modulo SPEI o SPID
	 * @return BeanInitModuloSPID Objeto del tipo obtenerInformacionHeader
	 * @throws BusinessException exception
	 */
	@Override
	public BeanInitModuloSPID obtenerInformacionHeader(String tipo) throws BusinessException {
		BeanResInitModuloSPIDDAO beanResInitModuloSPIDDAO = null;
		final BeanInitModuloSPID beanInitModuloSPID = new BeanInitModuloSPID();
			
		beanResInitModuloSPIDDAO = daoInitModuloSPID.obtenerInformacionHeaders(tipo);
		
		if(Errores.CODE_SUCCESFULLY.equals(beanResInitModuloSPIDDAO.getCodError())){
			beanInitModuloSPID.setCveInstitucion(beanResInitModuloSPIDDAO.getCveInstitucion());
			beanInitModuloSPID.setFechaOperacion(beanResInitModuloSPIDDAO.getFechaOperacion());
		}
				
		return beanInitModuloSPID;
	}
	
	/**
	 * El metodo reinicia la session en caso de no existir.
	 *
	 * @return BeanSessionSPID
	 * @throws BusinessException exception
	 */
	@Override
	public BeanSessionSPID beanCheckSessionSPID() throws BusinessException{
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		BeanInitModuloSPID beanInitModuloSPID = this.obtenerInformacionHeader("SPID");
		sessionSPID.setFechaOperacion(beanInitModuloSPID.getFechaOperacion());
		sessionSPID.setCveInstitucion(beanInitModuloSPID.getCveInstitucion());
		return sessionSPID;
	}
	
	/**
	 * Bean check session SPEI.
	 * 
	 * Metodo para validar la existencia de la sessuin SPEI
	 * en caso de no existir 
	 *
	 * @return Objeto bean session SPID
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanSessionSPID beanCheckSessionSPEI() throws BusinessException {
		BeanSessionSPID sessionSPEI = new BeanSessionSPID();
		BeanInitModuloSPID beanInitModuloSPEI = this.obtenerInformacionHeader("SPEI");
		sessionSPEI.setFechaOperacion(beanInitModuloSPEI.getFechaOperacion());
		sessionSPEI.setCveInstitucion(beanInitModuloSPEI.getCveInstitucion());
		return sessionSPEI;
	}
	/**
	 * @return the daoInitModuloSPID objeto del tipo DAOInitModuloSPID
	 */
	public DAOInitModuloSPID getDaoInitModuloSPID() {
		return daoInitModuloSPID;
	}

	/**
	 * @param daoInitModuloSPID the DAOInitModuloSPID to set
	 */
	public void setDaoInitModuloSPID(DAOInitModuloSPID daoInitModuloSPID) {
		this.daoInitModuloSPID = daoInitModuloSPID;
	}

	
	
}
