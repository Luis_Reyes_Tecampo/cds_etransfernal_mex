/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOAdministraSaldoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;


import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranfCtaAltPrincBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranfCtaAltPrincBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOAdministraSaldo;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorCDA;
import mx.isban.eTransferNal.utilerias.GeneraTramaUtils;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase del tipo BO que se encarga  del negocio del catalogo 
 * de horarios para envio de pagos por SPEI.
 *
 * @author FSW-Vector
 * @since 8/02/2017
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAdministraSaldoImpl extends Architech implements BOAdministraSaldo{

	 /** Constante del serial version. */
	private static final long serialVersionUID = 917101799381202733L;
	
	 /** Constante con la cadena descError. */
    private static final String CEROS = "0.00";
    
    /** La constante CERO. */
    private static final String CERO = "0";
    
    /** Propiedad del tipo String que almacena el valor de ENVIO. */
    private static final String ENVIO = "ENVIO"; 
    
	/**  Referencia privada al dao. */
	@EJB
	private DAOAdministraSaldo dAOAdministraSaldo;
	
	/** Propiedad del tipo DAOMonitorCDA que almacena el valor de dAOMonitorCDA. */
	@EJB
	private DAOMonitorCDA dAOMonitorCDA;
	
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	
	/**
	 * Metodo que sirve para consultar los saldos.
	 *
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	 */
	public BeanAdministraSaldoBO obtenerSaldos(ArchitechSessionBean architechSessionBean){
		BeanAdministraSaldoDAO beanAdministraSaldoDAO = null;
		BeanAdministraSaldoBO  beanAdministraSaldoBO = new BeanAdministraSaldoBO();
		Utilerias util = Utilerias.getUtilerias();
		String sPrincipal = CEROS, sAlterno = CEROS, sCalculado = CEROS, sDevoluciones = CEROS;
		String sPrincipalCount = CERO, sAlternoCount = CERO, sCalculadoCount = CERO, sDevolucionesCount = CERO;
		//Saldo principal y no reservado
		beanAdministraSaldoDAO = dAOAdministraSaldo.obtenerSaldoPrincipal(architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanAdministraSaldoDAO.getCodError())){
			beanAdministraSaldoBO.setCodError(Errores.OK00000V);
			sPrincipal= beanAdministraSaldoDAO.getSaldoPrincipal();
			sPrincipalCount = beanAdministraSaldoDAO.getSaldoPrincipalCount();
		}else{
			beanAdministraSaldoBO = setSaldoError(beanAdministraSaldoBO);
		}	
		//Saldo alterno
		beanAdministraSaldoDAO = dAOAdministraSaldo.obtenerSaldoAlterno(architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanAdministraSaldoDAO.getCodError())){
			beanAdministraSaldoBO.setCodError(Errores.OK00000V);
			sAlterno = beanAdministraSaldoDAO.getSaldoAlterno();
			sAlternoCount = beanAdministraSaldoDAO.getSaldoAlternoCount();
		}else{
			beanAdministraSaldoBO = setSaldoError(beanAdministraSaldoBO);
		}	   
		//Saldo calculado
		beanAdministraSaldoDAO = dAOAdministraSaldo.obtenerSaldoCalculado(architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanAdministraSaldoDAO.getCodError())){
			beanAdministraSaldoBO.setCodError(Errores.OK00000V);
			sCalculado = beanAdministraSaldoDAO.getSaldoCalculado();
			sCalculadoCount = beanAdministraSaldoDAO.getSaldoCalculadoCount();
		}else{
			beanAdministraSaldoBO = setSaldoError(beanAdministraSaldoBO);
		}
		//Saldo devoluciones
		beanAdministraSaldoDAO = dAOAdministraSaldo.obtenerSaldoDevoluciones(architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanAdministraSaldoDAO.getCodError())){
			beanAdministraSaldoBO.setCodError(Errores.OK00000V);
			sDevoluciones = beanAdministraSaldoDAO.getSaldoDevoluciones();
			sDevolucionesCount = beanAdministraSaldoDAO.getSaldoDevolucionesCount();
		}else{
			beanAdministraSaldoBO = setSaldoError(beanAdministraSaldoBO);
		}
		beanAdministraSaldoBO = setSaldoPrincipal(sPrincipal, sPrincipalCount, beanAdministraSaldoBO);
		beanAdministraSaldoBO.setSaldoAlterno(sAlterno);
		beanAdministraSaldoBO.setSaldoAlternoCount(util.formateaDecimales(sAlternoCount, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		beanAdministraSaldoBO.setSaldoCalculado(sCalculado);
		beanAdministraSaldoBO.setSaldoCalculadoCount(util.formateaDecimales(sCalculadoCount, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		beanAdministraSaldoBO.setSaldoDevoluciones(sDevoluciones);
		beanAdministraSaldoBO.setSaldoDevolucionesCount(util.formateaDecimales(sDevolucionesCount, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		BeanResMonitorCdaDAO  beanResMonitorCdaDAO = dAOMonitorCDA.consultarCDAAgrupadas(architechSessionBean);
		if(beanResMonitorCdaDAO.isExisteContingencia() && !beanResMonitorCdaDAO.isExisteConexion()){
			beanAdministraSaldoBO.setSemaforo("#FF0000");
		}else if(beanResMonitorCdaDAO.isExisteConexion()){
			beanAdministraSaldoBO.setSemaforo("#77933C");
		}else{
			beanAdministraSaldoBO.setSemaforo("#FF9900");
		}
		return beanAdministraSaldoBO;
	}
	
	/**
	 * Asigna el error y mensaje en caso existir.
	 *
	 * @param beanAdministraSaldoBO El objeto: bean administra saldo bo
	 * @return Objeto bean administra saldo bo
	 */
	private BeanAdministraSaldoBO setSaldoError(BeanAdministraSaldoBO  beanAdministraSaldoBO){
		beanAdministraSaldoBO.setCodError(Errores.EC00011B);
		beanAdministraSaldoBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		return beanAdministraSaldoBO;
	}
	
	/**
	 * Asigna los valores del saldo principal.
	 *
	 * @param sPrincipal El objeto: s principal
	 * @param sPrincipalCount El objeto: s principal count
	 * @param beanAdministraSaldoBO El objeto: bean administra saldo bo
	 * @return Objeto bean administra saldo bo
	 */
	private BeanAdministraSaldoBO setSaldoPrincipal(String sPrincipal, String sPrincipalCount, 
			BeanAdministraSaldoBO  beanAdministraSaldoBO){
		Utilerias util = Utilerias.getUtilerias();
		beanAdministraSaldoBO.setSaldoPrincipal(sPrincipal);
		beanAdministraSaldoBO.setSaldoPrincipalCount(util.formateaDecimales(sPrincipalCount, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		beanAdministraSaldoBO.setSaldoNoReservado(sPrincipal);
		beanAdministraSaldoBO.setSaldoNoReservadoCount(util.formateaDecimales(sPrincipalCount, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		return beanAdministraSaldoBO;
	}
	
	 /**
 	 * Metodo que sirve para enviar monto desde la cuenta alterna.
 	 *
 	 * @param beanReqTranfCtaAltPrincBO Objeto del tipo @see BeanReqTranfCtaAltPrincBO
 	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanResTranfCtaAltPrincBO Objeto del tipo BeanResTranfCtaAltPrincBO
 	 */
	public BeanResTranfCtaAltPrincBO envioAlternaPrincipal(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,ArchitechSessionBean architechSessionBean){
		info(".:.Entra a envioAlternaPrincipal.:.");
		String saldoAlt ="";
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		String tramaRespuesta ="";
		String respuestas[] = null;
		
		BeanResTranfCtaAltPrincBO beanResTranfCtaAltPrincBO = new BeanResTranfCtaAltPrincBO();
		BeanAdministraSaldoBO beanAdministraSaldoBO = obtenerSaldos(architechSessionBean);
		
		if(beanAdministraSaldoBO.getSaldoAlterno()!= null){
			saldoAlt = beanAdministraSaldoBO.getSaldoAlterno().replaceAll(",", "");
		}
		String imp = beanReqTranfCtaAltPrincBO.getImporte().replaceAll(",", "");
		
		BigDecimal saldoAlterna = new BigDecimal(saldoAlt);
		BigDecimal importe = new BigDecimal(imp);
		
		if(saldoAlterna.compareTo(importe)<0){
			setDatos(beanResTranfCtaAltPrincBO,beanAdministraSaldoBO);
			beanResTranfCtaAltPrincBO.setCodError(Errores.ED00080V);
			beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResTranfCtaAltPrincBO;
		}
		GeneraTramaUtils generaTramaUtils = new GeneraTramaUtils();
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		BeanTransEnvio beanTransEnvio = seteaTramaEnviar(beanReqTranfCtaAltPrincBO,architechSessionBean);
		String trama = generaTramaUtils.armaTramaCadena(beanTransEnvio);
		info(".:.envioAlternaPrincipal.:."+trama);
		ResBeanEjecTranDAO resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "TranTransfe", "ARQ_MENSAJERIA", architechSessionBean);
		
		if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
			tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
			respuestas = tramaRespuesta.split("\\|");
			if(respuestas.length>0 && "TRIB0000".equals(respuestas[0])){
				beanResTranfCtaAltPrincBO.setCodError(respuestas[0]);
				beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResTranfCtaAltPrincBO.setReferencia(respuestas[1]);
				beanResTranfCtaAltPrincBO.setEstatus(respuestas[2]);
				
				beanReqInsertBitacora = utilerias.creaBitacora("envioAlternaPrincipal.do", "1",
		        		"", new Date(), 
		        		"OK", "ENVALTPRIN", "NA", "NA", 
		        		respuestas[0], "TRANSACCION EXITOSA", ENVIO,trama);
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
			}else{
				beanReqInsertBitacora = utilerias.creaBitacora("envioAlternaPrincipal.do", "1",
		        		"", new Date(), 
		        		"KO", "ENVALTPRIN", "NA", "NA", 
		        		respuestas[0], "TRANSACCION ERRONEA", ENVIO,trama);
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
				beanResTranfCtaAltPrincBO.setCodError("TRIBXXXX");
//				beanResTranfCtaAltPrincBO.setCodError(respuestas[0]);
//				beanResTranfCtaAltPrincBO.setReferencia(respuestas[1]);
//				beanResTranfCtaAltPrincBO.setMsgError(respuestas[3]);
				beanResTranfCtaAltPrincBO.setMsgError(respuestas[0]);
				beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ALERT);
			}
		}else{
			beanResTranfCtaAltPrincBO.setCodError(resBeanEjecTranDAO.getCodError());
			beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		beanResTranfCtaAltPrincBO.setTramaRespuesta(resBeanEjecTranDAO.getTramaRespuesta());
		
		setDatos(beanResTranfCtaAltPrincBO,beanAdministraSaldoBO);
		
		return beanResTranfCtaAltPrincBO;
	}
	
	 /**
 	 * Metodo que sirve para enviar monto desde la cuenta principal .
 	 *
 	 * @param beanReqTranfCtaAltPrincBO Objeto del tipo @see BeanReqTranfCtaAltPrincBO
 	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanResTranfCtaAltPrincBO Objeto del tipo BeanResTranfCtaAltPrincBO
 	 */
	public BeanResTranfCtaAltPrincBO envioPrincipalAlterna(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,ArchitechSessionBean architechSessionBean){
		info(".:.Entra a envioPrincipalAlterna.:.");
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		String tramaRespuesta ="";
		String respuestas[] = null;
		String saldoAlt ="";
		BeanResTranfCtaAltPrincBO beanResTranfCtaAltPrincBO = new BeanResTranfCtaAltPrincBO();
		
		
		BeanAdministraSaldoBO beanAdministraSaldoBO = obtenerSaldos(architechSessionBean);
		
		if(beanAdministraSaldoBO.getSaldoPrincipal()!= null){
			saldoAlt = beanAdministraSaldoBO.getSaldoPrincipal().replaceAll(",", "");
		}
		String imp = beanReqTranfCtaAltPrincBO.getImporte().replaceAll(",", "");
		
		BigDecimal saldoAlterna = new BigDecimal(saldoAlt);
		BigDecimal importe = new BigDecimal(imp);
		
		if(saldoAlterna.compareTo(importe)<0){
			setDatos(beanResTranfCtaAltPrincBO,beanAdministraSaldoBO);
			beanResTranfCtaAltPrincBO.setCodError(Errores.ED00081V);
			beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResTranfCtaAltPrincBO;
		}
		
		GeneraTramaUtils generaTramaUtils = new GeneraTramaUtils();
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		BeanTransEnvio beanTransEnvio = seteaTramaEnviar(beanReqTranfCtaAltPrincBO,architechSessionBean);
		String trama = generaTramaUtils.armaTramaCadena(beanTransEnvio);
		ResBeanEjecTranDAO resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "TranTransfe", "ARQ_MENSAJERIA", architechSessionBean);
		info(".:.Entra a envioPrincipalAlterna.:."+trama);
		
		if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
			tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
			respuestas = tramaRespuesta.split("\\|");
			if(respuestas.length>0 && "TRIB0000".equals(respuestas[0])){
				beanResTranfCtaAltPrincBO.setCodError(respuestas[0]);
				beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResTranfCtaAltPrincBO.setReferencia(respuestas[1]);
				beanResTranfCtaAltPrincBO.setEstatus(respuestas[2]);
				
				beanReqInsertBitacora = utilerias.creaBitacora("envioPrincipalAlterna.do", "1",
		        		"", new Date(), 
		        		"OK", "ENVPRINALT", "NA", "NA", 
		        		respuestas[0], "TRANSACCION EXITOSA", ENVIO,trama);
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
				
			}else{
				beanReqInsertBitacora = utilerias.creaBitacora("envioPrincipalAlterna.do", "1",
		        		"", new Date(), 
		        		"KO", "ENVPRINALT", "NA", "NA", 
		        		respuestas[0], "", ENVIO,trama);
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
				beanResTranfCtaAltPrincBO.setCodError("TRIBXXXX");
//				beanResTranfCtaAltPrincBO.setCodError(respuestas[0]);
//				beanResTranfCtaAltPrincBO.setReferencia(respuestas[1]);
//				beanResTranfCtaAltPrincBO.setMsgError(respuestas[3]);
				beanResTranfCtaAltPrincBO.setMsgError(respuestas[0]);
				beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ALERT);
			}
		}else{
			beanResTranfCtaAltPrincBO.setCodError(resBeanEjecTranDAO.getCodError());
			beanResTranfCtaAltPrincBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		beanResTranfCtaAltPrincBO.setTramaRespuesta(resBeanEjecTranDAO.getTramaRespuesta());
		setDatos(beanResTranfCtaAltPrincBO,beanAdministraSaldoBO);
		
		return beanResTranfCtaAltPrincBO;
	}
	
	/**
	 * Metodo privado que setea los parametros de saldo al bean de respuesta del bo.
	 *
	 * @param beanResTranfCtaAltPrincBO Bean del tipo BeanResTranfCtaAltPrincBO
	 * @param beanAdministraSaldoBO Bean del tipo BeanAdministraSaldoBO
	 */
	private void setDatos(BeanResTranfCtaAltPrincBO beanResTranfCtaAltPrincBO,BeanAdministraSaldoBO beanAdministraSaldoBO){
		beanResTranfCtaAltPrincBO.setSaldoAlterno(beanAdministraSaldoBO.getSaldoAlterno());
		beanResTranfCtaAltPrincBO.setSaldoAlternoCount(beanAdministraSaldoBO.getSaldoAlternoCount());
		beanResTranfCtaAltPrincBO.setSaldoCalculado(beanAdministraSaldoBO.getSaldoCalculado());
		beanResTranfCtaAltPrincBO.setSaldoCalculadoCount(beanAdministraSaldoBO.getSaldoCalculadoCount());
		beanResTranfCtaAltPrincBO.setSaldoNoReservado(beanAdministraSaldoBO.getSaldoNoReservado());
		beanResTranfCtaAltPrincBO.setSaldoNoReservadoCount(beanAdministraSaldoBO.getSaldoNoReservadoCount());
		beanResTranfCtaAltPrincBO.setSaldoPrincipal(beanAdministraSaldoBO.getSaldoPrincipal());
		beanResTranfCtaAltPrincBO.setSaldoPrincipalCount(beanAdministraSaldoBO.getSaldoPrincipalCount());
		beanResTranfCtaAltPrincBO.setSaldoDevoluciones(beanAdministraSaldoBO.getSaldoDevoluciones());
		beanResTranfCtaAltPrincBO.setSaldoDevolucionesCount(beanAdministraSaldoBO.getSaldoDevolucionesCount());
		beanResTranfCtaAltPrincBO.setSemaforo(beanAdministraSaldoBO.getSemaforo());
	}

	/**
	 * Obtener el objeto: DAO administra saldo.
	 *
	 * @return el dAOAdministraSaldo
	 */
	public DAOAdministraSaldo getDAOAdministraSaldo() {
		return dAOAdministraSaldo;
	}

	/**
	 * Definir el objeto: DAO administra saldo.
	 *
	 * @param administraSaldo el dAOAdministraSaldo a establecer
	 */
	public void setDAOAdministraSaldo(DAOAdministraSaldo administraSaldo) {
		dAOAdministraSaldo = administraSaldo;
	}
	
	
	/**
	 * Metodo privado de ayuda que permite setear los datos para generar la trama a enviar.
	 *
	 * @param beanReqTranfCtaAltPrincBO Objeto del tipo BeanReqTranfCtaAltPrincBO
	 * @param sessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanTransEnvio bean de respuesta con los datos
	 */
	private BeanTransEnvio seteaTramaEnviar(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,final ArchitechSessionBean sessionBean){
		BeanTransEnvio beanTransEnvio =new BeanTransEnvio();
		beanTransEnvio.setServicio("D");
		if("TRANS-ALTERNA-PRINC".equals(beanReqTranfCtaAltPrincBO.getOpcion())){
			beanTransEnvio.setCveIntermeOrd("BANM2");
			beanTransEnvio.setNombreOrd("BANCO SANTANDER CUENTA ALTERNA");
			beanTransEnvio.setCveIntermeRec("BANME");
			beanTransEnvio.setNombreRec("BANCO SANTANDER");
			beanTransEnvio.setMedioEnt("TRIB");
			beanTransEnvio.setCveTransfe("007");
			beanTransEnvio.setCveOperacion("OTROS");
			beanTransEnvio.setFormaLiq("N");
			beanTransEnvio.setComentario1("PAGO PROGRAMADO");
		}else if("TRANS-PRINC-ALTERNA".equals(beanReqTranfCtaAltPrincBO.getOpcion())){
			beanTransEnvio.setCveIntermeOrd("BANME");
			beanTransEnvio.setNombreOrd("BANCO SANTANDER");
			beanTransEnvio.setCveIntermeRec("BANME");
			beanTransEnvio.setNombreRec("BANCO SANTANDER CUENTA ALTERNA");
			beanTransEnvio.setMedioEnt("TRSP");
			beanTransEnvio.setCveTransfe("002");
			beanTransEnvio.setCveOperacion("SIAC");
			beanTransEnvio.setFormaLiq("C");
			beanTransEnvio.setComentario1(beanReqTranfCtaAltPrincBO.getConcepto());
		}
		beanTransEnvio.setCveEmpresa("BME");
		beanTransEnvio.setPtoVta("0901");
		
		beanTransEnvio.setUsuarioCap(sessionBean.getUsuario());
		beanTransEnvio.setUsuarioSol(sessionBean.getUsuario());
		
		
		
		
		beanTransEnvio.setCuentaOrd("");
		beanTransEnvio.setPtoVtaOrd("");
		beanTransEnvio.setDivisaOrd("MN");

		beanTransEnvio.setCuentaRec("");
		beanTransEnvio.setPlazaBanxico("01001");
		beanTransEnvio.setPtoVtaRec("");
		beanTransEnvio.setDivisaRec("MN");
		beanTransEnvio.setImporteCargo(beanReqTranfCtaAltPrincBO.getImporte());
		beanTransEnvio.setImporteAbono(beanReqTranfCtaAltPrincBO.getImporte());
		beanTransEnvio.setImporteDolares("0.00");
		beanTransEnvio.setNombreBcoRec("");
		beanTransEnvio.setSucBcoRe("");  
		beanTransEnvio.setPaisBcoRec("MEXI");
		beanTransEnvio.setCiudadBcoRe("");
		beanTransEnvio.setCveABA("");
		
		beanTransEnvio.setComentario2("");
		beanTransEnvio.setComentario3("");
		return beanTransEnvio;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOMonitorCDA.
	 *
	 * @return DAOMonitorCDA Objeto de tipo @see DAOMonitorCDA
	 */
	public DAOMonitorCDA getDAOMonitorCDA() {
		return dAOMonitorCDA;
	}

	/**
	 * Metodo que modifica el valor de la propiedad dAOMonitorCDA.
	 *
	 * @param monitorCDA Objeto de tipo @see DAOMonitorCDA
	 */
	public void setDAOMonitorCDA(DAOMonitorCDA monitorCDA) {
		dAOMonitorCDA = monitorCDA;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad daoBitacora.
	 *
	 * @return DAOBitacoraAdmon Objeto de tipo @see DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Metodo que modifica el valor de la propiedad daoBitacora.
	 *
	 * @param daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}



	
	
	
}
