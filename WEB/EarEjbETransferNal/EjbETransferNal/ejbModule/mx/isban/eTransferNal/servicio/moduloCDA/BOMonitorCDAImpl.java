/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResEstadoConexionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaMontos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaVolumen;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorCDA;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo BO que se encarga  del negocio del monitor CDA
**/
@Remote(BOMonitorCDA.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorCDAImpl extends Architech implements BOMonitorCDA {

	/**
	 * Constante del serial Version
	 */
	private static final long serialVersionUID = 6306432654741694144L;

	/**
	 * Referencia al servicio de Monitor CDA DAO
	 */
	@EJB
    private DAOMonitorCDA daoMonitorCDA;
	
	/**
	 * Referencia al servicio de bitacoraAdmon
	 */
	@EJB
    private DAOBitacoraAdmon daoBitacoraAdmon;
	
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @exception BusinessException Excepcion de negocio
	 */
	@Override
	public BeanResMonitorCdaBO obtenerDatosMonitorCDA(
			ArchitechSessionBean architectSessionBean) throws BusinessException {
		BeanResMonitorCdaBO beanResMonitorCdaBO =  new BeanResMonitorCdaBO();
		BeanResMonitorCdaMontos beanResMonitorCdaMontos = new BeanResMonitorCdaMontos();
		BeanResMonitorCdaVolumen beanResMonitorCdaVolumen = new BeanResMonitorCdaVolumen();
		
		BeanResMonitorCdaDAO beanResMonitorCdaDAO = null;		
		beanResMonitorCdaDAO = daoMonitorCDA.consultarCDAAgrupadas(architectSessionBean);
		
		if(!Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			initMontoVolumen(beanResMonitorCdaMontos,beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setConectado(false);
			beanResMonitorCdaBO.setCodError(beanResMonitorCdaDAO.getCodError());
			beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			beanResMonitorCdaMontos.setMontoCDAConfirmadas(beanResMonitorCdaDAO.getMontoCDAConfirmadas());
			beanResMonitorCdaMontos.setMontoEnvContingencia(beanResMonitorCdaDAO.getMontoCDAEnvContingencia());
			beanResMonitorCdaMontos.setMontoConfContingencia(beanResMonitorCdaDAO.getMontoCDAConfContingencia());
			beanResMonitorCdaMontos.setMontoCDAEnviadas(beanResMonitorCdaDAO.getMontoCDAEnviadas());
			beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(beanResMonitorCdaDAO.getMontoCDAOrdRecAplicadas());
			beanResMonitorCdaMontos.setMontoCDAPendEnviar(beanResMonitorCdaDAO.getMontoCDAPendEnviar());
			beanResMonitorCdaMontos.setMontoCDARechazadas(beanResMonitorCdaDAO.getMontoCDARechazadas());
			beanResMonitorCdaMontos.setMontoCDAContingencia(beanResMonitorCdaDAO.getMontoCDAContingencia());
			beanResMonitorCdaMontos.setMontoProcesadas(beanResMonitorCdaDAO.getMontoCDAProcesadas());
			
			beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(beanResMonitorCdaDAO.getVolumenCDAConfirmadas());
			beanResMonitorCdaVolumen.setVolumenEnvContingencia(beanResMonitorCdaDAO.getVolumenCDAEnvContingencia());
			beanResMonitorCdaVolumen.setVolumenConfContingencia(beanResMonitorCdaDAO.getVolumenCDAConfContingencia());
			beanResMonitorCdaVolumen.setVolumenCDAEnviadas(beanResMonitorCdaDAO.getVolumenCDAEnviadas());
			beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(beanResMonitorCdaDAO.getVolumenCDAOrdRecAplicadas());
			beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(beanResMonitorCdaDAO.getVolumenCDAPendEnviar());
			beanResMonitorCdaVolumen.setVolumenCDARechazadas(beanResMonitorCdaDAO.getVolumenCDARechazadas());
			beanResMonitorCdaVolumen.setVolumenCDAContingencia(beanResMonitorCdaDAO.getVolumenCDAContingencia());
			beanResMonitorCdaVolumen.setVolumenProcesadas(beanResMonitorCdaDAO.getVolumenCDAProcesadas());
			
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setConectado(beanResMonitorCdaDAO.isExisteConexion());
			beanResMonitorCdaBO.setContingencia(beanResMonitorCdaDAO.isExisteContingencia());
			if(beanResMonitorCdaDAO.isExisteContingencia() && !beanResMonitorCdaDAO.isExisteConexion()){
				beanResMonitorCdaBO.setColorSemaforo("#FF0000");
			}else if(beanResMonitorCdaDAO.isExisteConexion()){
				beanResMonitorCdaBO.setColorSemaforo("#77933C");
			}else{
				beanResMonitorCdaBO.setColorSemaforo("#FF9900");
			}
			
			beanResMonitorCdaBO.setCodError(Errores.OK00000V);
		}

		return beanResMonitorCdaBO;
	}
	
	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @exception BusinessException Excepcion de negocio
	 */
	@Override
	public BeanResMonitorCdaBO detenerServicioCDA(
			ArchitechSessionBean architectSessionBean) throws BusinessException {
		BeanResMonitorCdaBO beanResMonitorCdaBO =  new BeanResMonitorCdaBO();	 
		BeanResMonitorCdaBO beanResMonitorCdaBOTemp =  new BeanResMonitorCdaBO();
		BeanResEstadoConexionDAO beanResEstadoConexionDAO = null;
		beanResEstadoConexionDAO = daoMonitorCDA.consultaEstadoConexion(architectSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResEstadoConexionDAO.getCodError())){
			if(beanResEstadoConexionDAO.getBytesRecibidos()==-1){
				if(beanResEstadoConexionDAO.getContingencia()==1){
					detenerServicio(beanResMonitorCdaBO,architectSessionBean);
				}else{
					guardaBitacora(Errores.ED00024V, "El sist. esta en contingencia",architectSessionBean);
					beanResMonitorCdaBO.setConectado(false);
					beanResMonitorCdaBO.setCodError(Errores.ED00024V);
					beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_INFO);
				}
			}else{
				detenerServicio(beanResMonitorCdaBO,architectSessionBean);	
			}
			beanResMonitorCdaBOTemp = obtenerDatosMonitorCDA(architectSessionBean);
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaBOTemp.getBeanResMonitorCdaMontos());
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaBOTemp.getBeanResMonitorCdaVolumen());
			beanResMonitorCdaBO.setColorSemaforo(beanResMonitorCdaBOTemp.getColorSemaforo());
		}else{
			beanResMonitorCdaBO.setCodError(Errores.EC00011B);
			beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_ERROR);
			guardaBitacora(Errores.EC00011B, "No hay comunicaci\u00F3n serv.",architectSessionBean);
		}
		return beanResMonitorCdaBO;
	}
	
	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param beanResMonitorCdaMontos Objeto del tipo @see BeanResMonitorCdaMontos
	 * @param beanResMonitorCdaVolumen Objeto del tipo @see BeanResMonitorCdaVolumen
	 * @exception BusinessException Excepcion de negocio
	 */
	private void initMontoVolumen(BeanResMonitorCdaMontos beanResMonitorCdaMontos,
			BeanResMonitorCdaVolumen beanResMonitorCdaVolumen) throws BusinessException{
		final String CERO_PUNTO_CERO = "0.0";
		final String CERO = "0";
		beanResMonitorCdaMontos.setMontoCDAConfirmadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAContingencia(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoConfContingencia(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoEnvContingencia(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAEnviadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAPendEnviar(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDARechazadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoProcesadas(CERO_PUNTO_CERO);
		
		beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAContingencia(CERO);
		beanResMonitorCdaVolumen.setVolumenConfContingencia(CERO);
		beanResMonitorCdaVolumen.setVolumenEnvContingencia(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAEnviadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(CERO);
		beanResMonitorCdaVolumen.setVolumenCDARechazadas(CERO);
		beanResMonitorCdaVolumen.setVolumenProcesadas(CERO);
	}

	
	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param codError Objeto del tipo @see String que almacena el codigo de error
	 * @param mensajeError Objeto del tipo @see String que almacena el mensaje de error
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @exception BusinessException Excepcion de negocio
	 */
	private void guardaBitacora(String codError,String mensajeError, ArchitechSessionBean architectSessionBean) throws BusinessException{
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		beanReqInsertBitacora = utilerias.creaBitacora("detenerServicioMonCDA.do", "","", new Date(), 
				"PENDIENTE", "DETSERVCDA", "TRAN_CONTIG_UNIX", "CV_PARAMETRO", codError, mensajeError, "UPDATE","");
		daoBitacoraAdmon.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
	}

	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param beanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @exception BusinessException Excepcion de negocio
	 */
	private void detenerServicio(BeanResMonitorCdaBO beanResMonitorCdaBO,ArchitechSessionBean architectSessionBean) throws BusinessException{
		BeanResMonitorCdaDAO beanResMonitorCdaDAO = null;
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		Utilerias utilerias = Utilerias.getUtilerias();
		beanResMonitorCdaDAO = daoMonitorCDA.detenerServicioCDA(architectSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			beanReqInsertBitacora = utilerias.creaBitacora("detenerServicioMonCDA.do", "","", new Date(), 
					"PENDIENTE", "DETSERVCDA", "TRAN_CONTIG_UNIX", "CV_PARAMETRO", Errores.OK00000V, "TRANSACCION EXITOSA", "UPDATE","");
			daoBitacoraAdmon.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
			beanResMonitorCdaBO.setCodError(Errores.OK00000V);
			beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_INFO);
		}			
	}

	/**
	 * Metodo get que obtiene el servicio de la bitacora
	 * @return daoBitacoraAdmon Objeto del tipo @DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacoraAdmon() {
		return daoBitacoraAdmon;
	}

	/**
	 * Metodo set que cambia la referencia al servicio de la bitacora
	 * @param daoBitacoraAdmon Objeto del tipo @DAOBitacoraAdmon
	 */
	public void setDaoBitacoraAdmon(DAOBitacoraAdmon daoBitacoraAdmon) {
		this.daoBitacoraAdmon = daoBitacoraAdmon;
	}

	/**
	 * Metodo get que obtiene el del MonitorCDA
	 * @return daoMonitorCDA Objeto del tipo @DAOMonitorCDA
	 */
	public DAOMonitorCDA getDaoMonitorCDA() {
		return daoMonitorCDA;
	}
	/**
	 * Metodo set que cambia la referencia al monitorCDA
	 * @param daoMonitorCDA Objeto del tipo @DAOMonitorCDA
	 */
	public void setDaoMonitorCDA(DAOMonitorCDA daoMonitorCDA) {
		this.daoMonitorCDA = daoMonitorCDA;
	}
   
	
}
