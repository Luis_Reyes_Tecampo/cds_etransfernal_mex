/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOAfectacionBloquesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     22/07/2019 11:56:53 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.catalogos.BeanBloques;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOAfectacionBloques;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasAfectacionBloques;


/**
 * Class BOAfectacionBloquesImpl.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de afectacion de bloques.
 * 
 * @author FSW-Vector
 * @since 22/07/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAfectacionBloquesImpl extends Architech implements BOAfectacionBloques{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1039204159209313154L;

	/** La variable que contiene informacion con respecto a: dao afectacion bloques. */
	@EJB
	private DAOAfectacionBloques daoAfectacionBloques;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante TYPE_INSERT. */
	private static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	private static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_DELETE. */
	private static final String TYPE_DELETE = "DELETE";
	
	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "NUM_BLOQUE";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_AFECTA_BLOQUES";
	
	/** La constante UPDATE. */
	private static final String UPDATE = "VALOR, DESCRIPCION";
	/** La constante INSERT. */
	private static final String INSERT = "NUM_BLOQUE, VALOR, DESCRIPCION";
	
	/** La constante SELECT. */
	private static final String SELECT = "NUM_BLOQUE, VALOR";
	
	
	
	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanBloque: objeto que bloques
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador: bean que permite paginar
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanBloques consultar(BeanBloque beanBloque, BeanPaginador beanPaginador, ArchitechSessionBean session)
			throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanBloques response = daoAfectacionBloques.consultar(beanBloque, beanPaginador, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getBloques().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * agregar bloque
	 * 
	 * Metodo publico utilizado para agregar bloques
	 *
	 *@param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloqe: bean que obtiene los bloques
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase agregar(BeanBloque beanBloque, ArchitechSessionBean session) throws BusinessException {
		boolean operacion = false;
		/** Ejecucion de la peticion al DAO **/
		BeanResBase response = daoAfectacionBloques.agregar(beanBloque, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(beanBloque, TYPE_INSERT);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_INSERT, "agregarBloque.do", operacion, session,dtoNuevo,"",INSERT);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * editar bloque
	 * 
	 * Metodo publico utilizado para editar bloques
	 *
	 * @param beanBloque: objeto que trae bloques	
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase editar(BeanBloque beanBloque, ArchitechSessionBean session) throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		String[] dataRecover = beanBloque.getValor().split(Pattern.quote("|"));
		BeanBloque beanFilter = new BeanBloque();
		beanFilter.setNumBloque(beanBloque.getNumBloque());
		beanFilter.setValor(dataRecover[1]);
		/** Ejecucion de la peticion al DAO **/
		BeanBloques bean = daoAfectacionBloques.consultar(beanFilter, beanPaginador, session);
		String dtoAnt = obtenerDtos(bean.getBloques().get(0), TYPE_UPDATE);
		/** Ejecucion de la peticion al DAO **/
		response = daoAfectacionBloques.editar(beanBloque, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(beanBloque, TYPE_UPDATE);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_UPDATE, "editarBloque.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloques: bean que obtiene bloques
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanBloques beanBloques, ArchitechSessionBean session) throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanBloque bloque : beanBloques.getBloques()){
			if (bloque.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoAfectacionBloques.eliminar(bloque, session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(bloque.getNumBloque().concat("|".concat(bloque.getValor()).concat("|".concat(bloque.getDescripcion()))));
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(bloque.getNumBloque().concat("|".concat(bloque.getValor()).concat("|".concat(bloque.getDescripcion()))));
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDtos(bloque, TYPE_DELETE);
	        	generaPistaAuditoras(TYPE_DELETE, "eliminarBloque.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Exportar todos.
	 *
	 * @param beanBloque: trae los bloques
	 * @param totalRegistros: variable de tipo int 	y que obtiene el numero de registros
	 * @param session: objeto de tipo ArchitechSessionBean
	 * @return response: obtiene el mensaje de respuesta de la base de datos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase exportarTodo(BeanBloque beanBloque, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String filtroWhere = UtileriasAfectacionBloques.getInstancia().getFiltro(beanBloque, "WHERE");
		String query = UtileriasAfectacionBloques.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasAfectacionBloques.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasAfectacionBloques.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("AFECTACION_BLOQUES");
		exportarRequest.setNombreRpt("CATALOGOS_AFECTACION_BLOQUES_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodosAfectacionBloques.do", operacion, session,"","", DATO_FIJO);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras
	 * en la BD.
	 *
	 * @param operacion: objeto de tipo operacion: alta, modificacion, eliminacion, exportar
	 * @param urlController: objeto de tipo controller que es mandado llamar dependiendo de la operacion seleccionada
	 * @param resOp: variable de tipo boleano respuesta de la operacion
	 * @param session: objeto de tipo ArchitechSessionBean
	 * @param dtoNuevo: cadena que obtiene el nuevo dato.
	 * @param dtoAnterior: variable de tipo string que trae los registros anteriores
	 * @param dtoModificado: variable de tipo string que trae el dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
	/**
	 * Obtener dtos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 * @param ant: objeto de tipo beanBloque que trae la info del bloque
	 * @return accion: regresa la informacion de la notificacion
	 */
	private String obtenerDtos (BeanBloque ant, String accion) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es UDPDATE **/
		if(ConstantesMttoMediosAut.TYPE_UPDATE.equals(accion)) {
			dtoAnt ="VALOR=".concat(ant.getValor()).concat(", DESCRIPCION=".concat(ant.getDescripcion()));
		}
		/** Obtencion de cadena cuando es INSERT **/
		if(ConstantesMttoMediosAut.TYPE_INSERT.equals(accion)) {
			dtoAnt ="NUM_BLOQUE=".concat(ant.getNumBloque()).concat(", VALOR=".concat(ant.getValor()).concat(", DESCRIPCION=".concat(ant.getDescripcion())));
		}
		/** Obtencion de cadena cuando es DELETE **/
		if(ConstantesMttoMediosAut.TYPE_DELETE.equals(accion) || ConstantesMttoMediosAut.TYPE_SELECT.equals(accion) ) {
			dtoAnt ="NUM_BLOQUE=".concat(ant.getNumBloque()).concat(", VALOR=".concat(ant.getValor()));
		}
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
