/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepOpApartadaImp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/09/2018 06:09:30 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.SPEI;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperacionApartadas;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;

/**
 * Class BORecepOpApartadaImp.
 * clase de tipo BO que muestra las operaciones apartadas por un usario
 *
 * @author FSW-Vector
 * @since 20/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class  BORecepOpApartadaImp implements BORecepOpApartada {
	
	/** La variable que contiene informacion con respecto a: dao apartadas. */
	@EJB
	private transient DAORecepOperacionApartadas daoApartadas;
	
	/** La variable que contiene informacion con respecto a: b O recep operacion. */
	@EJB
	private transient BORecepOperacion bORecepOperacion;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas utilApartadas = UtileriasRecepcionOpConsultas.getInstance();
	
	
	/**
	 * Aparta operaciones.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec  apartaOperaciones (BeanReqTranSpeiRec beanReqTranSpeiRec,
	    		ArchitechSessionBean sessionBean, boolean historico) throws BusinessException {
		//inicializa variables
		BeanResTranSpeiRec beanResTranSpeiRec = new BeanResTranSpeiRec();
		//inicializa bean error
		beanResTranSpeiRec.setBeanError(new BeanResBase());
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion(); 
		/**Busca los datos seleccionados**/
		listBeanTranSpeiRec = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,HelperRecepcionOperacion.SELECCIONADO);
    		
		/**Mensaje que se envia si no existen datos**/
		if(listBeanTranSpeiRec.isEmpty()) {
			beanResTranSpeiRec.getBeanError().setCodError(Errores.ED00029V);
			beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);	
			return beanResTranSpeiRec;
		}
		/**Se llama la funcion para continuar con el apartado **/
		beanResTranSpeiRec = continuaApartado(listBeanTranSpeiRec,sessionBean,historico,beanReqTranSpeiRec ); 
		
		return beanResTranSpeiRec;
	}
	
	
	/**
	 * Continua apartado.
	 *
	 * @param listBeanTranSpeiRec El objeto: list bean tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	private BeanResTranSpeiRec continuaApartado(List<BeanTranSpeiRecOper> listBeanTranSpeiRec,ArchitechSessionBean sessionBean,
			boolean historico,BeanReqTranSpeiRec beanReqTranSpeiRec ) throws BusinessException {
		//inicializa variables
		boolean ok = true;
		int total=0;
		int inserto =0;
		// inicializa los objetos
		BeanResTranSpeiRec beanResTranSpeiRec = new BeanResTranSpeiRec();
		BeanResTranSpeiRec resTranSpeiRec = new BeanResTranSpeiRec();
		/**recorre la lista para apartar las operaciones**/
		for(BeanTranSpeiRecOper bean:listBeanTranSpeiRec){
			//si el usuario existe
			if(bean.getUsuario().equals(sessionBean.getUsuario())) {
				resTranSpeiRec.setBeanError(new BeanResBase());
				resTranSpeiRec.getBeanError().setCodError(Errores.ED00131V);
				resTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
				break;
			}
			
			 if(bean.getUsuario()!= null && bean.getUsuario().length()>0) {
				 total = total +1;
				 
			 }else {			 
				 beanResTranSpeiRec = daoApartadas.apartaOperaciones(bean, sessionBean, historico, beanReqTranSpeiRec.getBeanComun().getNombrePantalla());
				 inserto = inserto+1;
				 String datosInsert = bean.getBeanDetalle().getDetalleGral().getFchOperacion()+","+bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
						 +","+bean.getBeanDetalle().getDetalleGral().getCveInstOrd()+","+bean.getBeanDetalle().getDetalleGral().getFolioPaquete()
						 +","+bean.getBeanDetalle().getDetalleGral().getFolioPago()+","+sessionBean.getUsuario();
				 String datosNuevo =" ch_operacion = "+bean.getBeanDetalle().getDetalleGral().getFchOperacion()
						 +",cve_mi_instituc ="+bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
						 +",folio_paquete ="+bean.getBeanDetalle().getDetalleGral().getFolioPaquete()
						 +",folio_pago ="+bean.getBeanDetalle().getDetalleGral().getFolioPago()
						 +",cve_inst_ord="+bean.getBeanDetalle().getDetalleGral().getCveInstOrd();
				//se asigna el codigo de error
				 String codigo =beanResTranSpeiRec.getBeanError().getCodError();
				 
				 String dato = ConstantesRecepOperacionApartada.TYPE_INSERT+"/recepOperaApartadas.do/"+datosInsert+"/"+codigo+"/"+datosNuevo;
				 if(Errores.EC00011B.equals(beanResTranSpeiRec.getBeanError().getCodError())) {
						ok=false;	
						/**genera la pista de auditoria**/
						BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
						/**inserta en bitacora **/
						boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
						break;
				}
				 /**genera la pista de auditoria**/
					BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
					/**inserta en bitacora **/
					boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
			 }
			 //metod que regresa el objeto procesado
			 resTranSpeiRec = continuaApar(beanResTranSpeiRec,total,inserto,listBeanTranSpeiRec, ok);
		 }
			
		return resTranSpeiRec;
	}
	
	
	/**
	 * Continua apar.
	 *
	 * @param beanResTranSpeiRec El objeto: bean res tran spei rec
	 * @param total El objeto: total
	 * @param inserto El objeto: inserto
	 * @param listBeanTranSpeiRec El objeto: list bean tran spei rec
	 * @param ok El objeto: ok
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	private BeanResTranSpeiRec continuaApar(BeanResTranSpeiRec beanResTranSpeiRec,int total,int inserto, 
			List<BeanTranSpeiRecOper> listBeanTranSpeiRec, boolean ok) throws BusinessException {
		
		/**si hay algun error se manda el codigo**/
		 if(beanResTranSpeiRec.getBeanError().getCodError()!=null 
				 &&( beanResTranSpeiRec.getBeanError().getCodError().equals(Errores.EC00011B) || !ok)) {
			 throw new BusinessException(beanResTranSpeiRec.getBeanError().getCodError(), beanResTranSpeiRec.getBeanError().getMsgError());
		 }
		 // es correcto		 
		if(ok) { 
			//si es correcto continua
			beanResTranSpeiRec = continuarApartados(beanResTranSpeiRec,total,inserto, listBeanTranSpeiRec );
		}
		
		return beanResTranSpeiRec;
	}
	
	/**
	 * Continuar apartados.
	 *
	 * @param beanResTranSpei El objeto: bean res tran spei
	 * @param total El objeto: total
	 * @param inserto El objeto: inserto
	 * @param listBeanTranSpeiRec El objeto: list bean tran spei rec
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	private BeanResTranSpeiRec continuarApartados(BeanResTranSpeiRec beanResTranSpei, int total, int inserto, 
			List<BeanTranSpeiRecOper> listBeanTranSpeiRec )  throws BusinessException  {
		BeanResTranSpeiRec beanResTranSpeiRec = new BeanResTranSpeiRec();
		beanResTranSpeiRec = beanResTranSpei;
		 
		 /**En caso de que todas las operaciones se insertaran correctamente manda mensaje de exito**/
		 if(total==0 && inserto == listBeanTranSpeiRec.size() ) {
			 beanResTranSpeiRec.getBeanError().setCodError(Errores.CMCO019V);
			 beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		 }
		 //sicuemple asigan mensaje
		 if(total > 0 && inserto>= 0){
			 /**En caso de que existan ya apartadas y se inserten otras manda un mensaje de alerta**/
			 beanResTranSpeiRec.getBeanError().setCodError(Errores.ED00131V);
			 beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		 }
		 
		 return beanResTranSpeiRec;
	}
	
	/**
	 * Aparta operaciones eliminar.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	
	@Override
	public BeanResTranSpeiRec  apartaOperacionesEliminar (BeanReqTranSpeiRec beanReqTranSpeiRec,
	    		ArchitechSessionBean sessionBean, boolean historico) throws BusinessException {
		BeanResTranSpeiRec beanResTranSpeiRec = new BeanResTranSpeiRec();
		//inicializa bean error
		beanResTranSpeiRec.setBeanError(new BeanResBase());
		/**se llama la funcion para obtener los registros para liberar**/
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = obtenerLista(beanReqTranSpeiRec,sessionBean);
		
		int total=0;
		boolean ok = true;
		
		
		/**Mensaje que se envia si no existen datos**/
		if(listBeanTranSpeiRec.isEmpty() ) {	
			if(("TODO").equals(beanReqTranSpeiRec.getBeanComun().getEliminarTodo())) {
				beanResTranSpeiRec.getBeanError().setCodError(Errores.ED00030V);
			}else {
				beanResTranSpeiRec.getBeanError().setCodError(Errores.ED00029V);
			}
			
			beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);	
			return beanResTranSpeiRec;
		}
		
		/**recorre la lista para liberar operaciones **/
		 for(BeanTranSpeiRecOper bean:listBeanTranSpeiRec){
			 beanResTranSpeiRec = daoApartadas.apartaOperacionesEliminar(bean, sessionBean, historico, beanReqTranSpeiRec.getBeanComun().getNombrePantalla());
			 total = total+1;
			 String datosEliminar = bean.getBeanDetalle().getDetalleGral().getFchOperacion()+","+bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
					 +","+bean.getBeanDetalle().getDetalleGral().getCveInstOrd()+","+bean.getBeanDetalle().getDetalleGral().getFolioPaquete()
					 +","+bean.getBeanDetalle().getDetalleGral().getFolioPago()+","+sessionBean.getUsuario();
			 String datosNvo =" ch_operacion = "+bean.getBeanDetalle().getDetalleGral().getFchOperacion()
					 +",cve_mi_instituc ="+bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
					 +",folio_paquete ="+bean.getBeanDetalle().getDetalleGral().getFolioPaquete()
					 +",folio_pago ="+bean.getBeanDetalle().getDetalleGral().getFolioPago()
					 +",cve_inst_ord="+bean.getBeanDetalle().getDetalleGral().getCveInstOrd();
			//se asigna el codigo de error
			String codigo = beanResTranSpeiRec.getBeanError().getCodError();
			String dato = ConstantesRecepOperacionApartada.TYPE_DELETE+"/liberarOperApartadas.do/"+datosEliminar+"/"+codigo+"/"+datosNvo;
			if(Errores.EC00011B.equals(beanResTranSpeiRec.getBeanError().getCodError())) {
				ok=false;	
				/**genera la pista de auditoria**/
				BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
				/**inserta en bitacora **/
				boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
				break;
			}
			
			/**genera la pista de auditoria**/
			BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
			
		 }
		 
		 
		 /**si hay algun error se manda el codigo**/
		 if(!ok || beanResTranSpeiRec.getBeanError().getCodError().equals(Errores.EC00011B)) {
			 throw new BusinessException(beanResTranSpeiRec.getBeanError().getCodError(), beanResTranSpeiRec.getBeanError().getMsgError());
		 }else if( total == listBeanTranSpeiRec.size() ) { 
			 /**En caso de que todas las operaciones se insertaran correctamente manda mensaje de exito**/		 
			 beanResTranSpeiRec.getBeanError().setCodError(Errores.CMCO018V);
			 beanResTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		 }
		
		return beanResTranSpeiRec;
	}
	
	
	/**
	 * Obtener lista.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @return Objeto list
	 */
	private List<BeanTranSpeiRecOper> obtenerLista(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean) {
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion(); 
		/** **/
		if(("TODO").equals(beanReqTranSpeiRec.getBeanComun().getEliminarTodo())) {
			for(BeanTranSpeiRecOper bean : beanReqTranSpeiRec.getListBeanTranSpeiRec()) {
				if(sessionBean.getUsuario().equals(bean.getUsuario())) {
					listBeanTranSpeiRec.add(bean);
				}
			}
		}else {
			/**Busca los datos seleccionados**/
			listBeanTranSpeiRec = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,HelperRecepcionOperacion.SELECCIONADO);
		}
		
		return listBeanTranSpeiRec;
	}
	
		
}
