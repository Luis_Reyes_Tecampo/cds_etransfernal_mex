/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaSaldoBancoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBancoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOConsultaSaldoBanco;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio de
 *la consulta de saldos por banco
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaSaldoBancoImpl implements BOConsultaSaldoBanco {
	
	/** Referencia privada al dao  */
	@EJB
	private DAOConsultaSaldoBanco daoConsultaSaldoBanco;

	

	/**Metodo que sirve para consultar los saldos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return BeanResConsultaSaldoBanco
	   * @throws BusinessException Exception
	   */
	@Override
	public BeanResConsultaSaldoBanco obtenerConsultaSaldoBanco(
			BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException{
		BeanResConsultaSaldoBancoDAO beanResConsultaSaldoBancoDAO = null;
		final BeanResConsultaSaldoBanco beanResConsultaSaldoBanco = new BeanResConsultaSaldoBanco();
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}		
		String sF = getSortField(sortField);
		
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());		
		beanResConsultaSaldoBancoDAO = daoConsultaSaldoBanco.obtenerConsultaSaldoBanco(paginador, architechSessionBean, sF, sortType);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaSaldoBancoDAO.getCodError()) 
				&& beanResConsultaSaldoBancoDAO.getListaBeanConsultaSaldoBanco().isEmpty()) {
			beanResConsultaSaldoBanco.setCodError(Errores.ED00011V);
			beanResConsultaSaldoBanco.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsultaSaldoBanco.setListaConsultaSaldoBanco(beanResConsultaSaldoBancoDAO.getListaBeanConsultaSaldoBanco());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResConsultaSaldoBancoDAO.getCodError())){
			beanResConsultaSaldoBanco.setListaConsultaSaldoBanco(beanResConsultaSaldoBancoDAO.getListaBeanConsultaSaldoBanco());          
			beanResConsultaSaldoBanco.setTotalReg(beanResConsultaSaldoBancoDAO.getTotalReg());
			paginador.calculaPaginas(beanResConsultaSaldoBancoDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsultaSaldoBanco.setBeanPaginador(paginador);
			beanResConsultaSaldoBanco.setCodError(Errores.OK00000V);
		}else{
			beanResConsultaSaldoBanco.setCodError(Errores.EC00011B);
			beanResConsultaSaldoBanco.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResConsultaSaldoBanco;
	}
	
	/**Metodo que sirve para consultar los saldos Montos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return BeanResConsultaSaldoBanco
	   * @throws BusinessException Exception
	   */
	public BigDecimal obtenerConsultaSaldoBancoMontos(
			 ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		String sF = getSortField(sortField);
		totalImporte = daoConsultaSaldoBanco.obtenerConsultaSaldoBancoMontos(architechSessionBean, sF, sortType);	
		return totalImporte;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("claveBanco".equals(sortField)){
			sF = "INTERME";
		} else if ("nombreBanco".equals(sortField)){
			sF = "NOMBRE_CORTO";
		} else if ("volumenOperacionesEnviadas".equals(sortField)){
			sF = "SUM(VOLUMEN_ENV)";
		} else if ("importeOperacionesEnviadas".equals(sortField)){
			sF = "SUM(IMPORTE_ENV)";
		} else if ("volumenOperacionesRecibidas".equals(sortField)){
			sF = "SUM(VOLUMEN_REC)";
		} else if ("importeOperacionesRecibidas".equals(sortField)){
			sF = "SUM(IMPORTE_REC)";
		} else if ("saldo".equals(sortField)){
			sF = "SALDO";
		}
		return sF;
	}
	
	/**Metodo que sirve para exportar todos los saldos
	   * @param beanReqConsultaSaldoBanco Objeto del tipo @see BeanReqConsultaSaldoBanco
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsultaSaldoBanco
	   * @throws BusinessException Exception
	   */
	@Override
	public BeanResConsultaSaldoBanco exportarTodoConsultaSaldoBanco(
			BeanReqConsultaSaldoBanco beanReqConsultaSaldoBanco, ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResConsultaSaldoBancoDAO beanResConsultaSaldoBancoDAO = null;
		BeanResConsultaSaldoBanco beanResConsultaSaldoBanco = null;
		
		BeanPaginador paginador = beanReqConsultaSaldoBanco.getPaginador();
		
		if (paginador == null){
			  paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		beanResConsultaSaldoBanco = obtenerConsultaSaldoBanco(paginador, architechSessionBean, "", "");
		beanReqConsultaSaldoBanco.setTotalReg(beanResConsultaSaldoBanco.getTotalReg());
		beanResConsultaSaldoBancoDAO = daoConsultaSaldoBanco.exportarTodoConsultaSaldoBanco(beanReqConsultaSaldoBanco, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaSaldoBancoDAO.getCodError())) {
			beanResConsultaSaldoBanco.setCodError(Errores.OK00002V);
			beanResConsultaSaldoBanco.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsultaSaldoBanco.setNombreArchivo(beanResConsultaSaldoBancoDAO.getNombreArchivo());
		} else {
			beanResConsultaSaldoBanco.setCodError(Errores.EC00011B);
			beanResConsultaSaldoBanco.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return beanResConsultaSaldoBanco;
	}
	
	/**
	 * @return the daoConsultaSaldoBanco objeto del tipo DAOConsultaSaldoBanco
	 */
	public DAOConsultaSaldoBanco getDaoConsultaSaldoBanco() {
		return daoConsultaSaldoBanco;
	}

	/**
	 * @param daoConsultaSaldoBanco the DAOConsultaSaldoBanco to set
	 */
	public void setDaoConsultaSaldoBanco(DAOConsultaSaldoBanco daoConsultaSaldoBanco) {
		this.daoConsultaSaldoBanco = daoConsultaSaldoBanco;
	}
	
}
