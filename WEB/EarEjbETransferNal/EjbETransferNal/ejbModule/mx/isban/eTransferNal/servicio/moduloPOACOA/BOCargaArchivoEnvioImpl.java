/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOCargaArchivoRespuestaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqArchPago;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanales;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanalesDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchxFchCDAHist;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOArchivosOrdenPago;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/** Clase que implementa BOCargaArchivoRespuesta */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCargaArchivoEnvioImpl implements BOCargaArchivoEnvio {
	
	/** DAO para las transacciones a base de datos*/
	@EJB
	private DAOArchivosOrdenPago daoArchivosOrdenPago;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	
	/**
	 * Propiedad del tipo DAOParametrosPOACOAImpl que almacena el valor de dAOParametrosPOACOAImpl
	 */
	@EJB
	private DAOGenerarArchxFchCDAHist dAOGenerarArchxFchCDAHist;

	/**
	 * Agrega nombre del archivo para pagos
	 * @param beanReqArchPago con los datos del archivo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	@Override
	public BeanResConsCanales agregarNombreArchProcesar(
			BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean) {
		/** Se realizar la peticion al DAO **/
		BeanResConsCanalesDAO beanResConsCanalesDAO = daoArchivosOrdenPago.consultarCanales(architectSessionBean);
		/** Se realizar la peticion al DAO **/
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOGenerarArchxFchCDAHist.consultaFechaOperacion(null, architectSessionBean);
		beanReqArchPago.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		/** Creacion de los objetos de respuesta **/
		BeanResConsCanales beanResConsCanales = new BeanResConsCanales();
		beanResConsCanales.setBeanResCanalList(beanResConsCanalesDAO.getBeanResCanalList());
		BeanResBase res = null;
		/** Se valida la duplicidad del archivo **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO	= validaNombreDuplicado(beanReqArchPago, architectSessionBean);
		/** Se valida la respuesta **/
		if(Errores.OK00000V.equals(beanResDuplicadoArchDAO.getCodError())){
			if(!beanResDuplicadoArchDAO.isEsDuplicado()){
				/** Se realizar la peticion al DAO **/
				res = daoArchivosOrdenPago.registarNombreArchivo(beanReqArchPago,
						architectSessionBean);
				Utilerias utilerias = Utilerias.getUtilerias();
				/** Se crea el bean con los valores que se enviaran a la bitacora **/
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacora("cargarArchProc.do", "1", "", new Date(),
								"PENDIENTE", "ALTAARCHPROC", "TRAN_ARCH_CANALES",
								"CVE_MEDIO_ENT", res.getCodError(), "TRANSACCION EXITOSA",
								"INSERT", beanReqArchPago.getCveMedioEntrega());
				/** Se realizar la peticion al DAO **/
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
				/** Se setean los codigos de error**/
				beanResConsCanales.setCodError(Errores.OK00000V);
				beanResConsCanales.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				beanResConsCanales.setCodError(Errores.CD00166V);
				beanResConsCanales.setTipoError(Errores.TIPO_MSJ_CONFIRM);
				
			}
		}
		/** Retorno del metodo **/
		return beanResConsCanales;
	}
	
	/**
	 * Actualiza nombre del archivo para pagos
	 * @param beanReqArchPago con los datos del archivo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	@Override
	public BeanResConsCanales actualizarNombreArchProcesar(
			BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean) {
		/** Creacion de los objetos de respuesta **/
		BeanResBase res = null;
		BeanResConsCanales beanResConsCanales = new BeanResConsCanales();
		
		/** Se realizar la peticion al DAO **/
		/** Al metodo se le envia un NULL debido a que este parametro no es necesario en este flujo **/
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOGenerarArchxFchCDAHist.consultaFechaOperacion(null, architectSessionBean);
		beanReqArchPago.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		/** Se realizar la peticion al DAO **/
		res = daoArchivosOrdenPago.actualizarNombreArchivo(beanReqArchPago,
					architectSessionBean);
		if(Errores.OK00000V.equals(res.getCodError())){
			Utilerias utilerias = Utilerias.getUtilerias();
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("actualizarArchProc.do", "1", "", new Date(),
							"PENDIENTE", "ACTARCHPROC", "TRAN_ARCH_CANALES",
							"CVE_MEDIO_ENT", res.getCodError(), "TRANSACCION EXITOSA",
							"UPDATE", beanReqArchPago.getNombreArchivo());
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
			beanResConsCanales.setCodError(Errores.OK00000V);
			beanResConsCanales.setTipoError(Errores.TIPO_MSJ_INFO);
		}else{
			beanResConsCanales.setCodError(Errores.ED00074V);
			beanResConsCanales.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Se realizar la peticion al DAO **/
		BeanResConsCanalesDAO beanResConsCanalesDAO = daoArchivosOrdenPago.consultarCanales(architectSessionBean);
		beanResConsCanales.setBeanResCanalList(beanResConsCanalesDAO.getBeanResCanalList());
		/** Retorno del metodo **/
		return beanResConsCanales;
	}
	
	
	
	/**
	 * Valida si el nombre del archivo se encuentra ya en BD.
	 * @param beanReqArchPago Bean con los datos 
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO Bean de respuesta de archivo duplicado
	 */
	private BeanResDuplicadoArchDAO validaNombreDuplicado(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean){
		/** Creacion de los objetos de respuesta **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO = null;
		/** Se realizar la peticion al DAO **/
		beanResDuplicadoArchDAO = daoArchivosOrdenPago.consultaDuplicidadNombre(beanReqArchPago, architectSessionBean);
		/** Retorno del metodo **/
		return beanResDuplicadoArchDAO;
	}
	

	/**
	 * Obtiene los medios de entrega para el combo de nombre de archivos de pagos.
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	@Override
	public BeanResConsCanales obtenerMediosEntrega(
			ArchitechSessionBean architectSessionBean){
		/** Creacion de los objetos de respuesta **/
		/** Se realizar la peticion al DAO **/
		BeanResConsCanalesDAO beanResConsCanalesDAO = daoArchivosOrdenPago.consultarCanales(architectSessionBean);
		BeanResConsCanales beanResConsCanales = new BeanResConsCanales();
		beanResConsCanales.setBeanResCanalList(beanResConsCanalesDAO.getBeanResCanalList());
		/** Retorno del metodo **/
		return beanResConsCanales;
	}

	/**
	 * Devuelve el valor de daoArchivosOrdenPago
	 * @return the daoArchivosOrdenPago
	 */
	public DAOArchivosOrdenPago getDaoArchivosOrdenPago() {
		return daoArchivosOrdenPago;
	}

	/**
	 * Modifica el valor de daoArchivosOrdenPago
	 * @param daoArchivosOrdenPago the daoArchivosOrdenPago to set
	 */
	public void setDaoArchivosOrdenPago(DAOArchivosOrdenPago daoArchivosOrdenPago) {
		this.daoArchivosOrdenPago = daoArchivosOrdenPago;
	}

	/**
	 * Devuelve el valor de daoBitacora
	 * @return the daoBitacora
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Modifica el valor de daoBitacora
	 * @param daoBitacora the daoBitacora to set
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOGenerarArchxFchCDAHist
	 * @return DAOGenerarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public DAOGenerarArchxFchCDAHist getDAOGenerarArchxFchCDAHist() {
		return dAOGenerarArchxFchCDAHist;
	}

	/**
	 * Metodo que modifica el valor de la propiedad dAOGenerarArchxFchCDAHist
	 * @param generarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public void setDAOGenerarArchxFchCDAHist(
			DAOGenerarArchxFchCDAHist generarArchxFchCDAHist) {
		dAOGenerarArchxFchCDAHist = generarArchxFchCDAHist;
	}

}
