/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BODetRecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015     INDRA 		ISBAN      Creacion
 *   1.1   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAODetRecepOperacion;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpHTO;

/**
 * Clase del tipo BO que se encarga  del detalle
 * del detalle de recepcion de operaciones.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BODetRecepOperacionImpl extends Architech  implements  BODetRecepOperacion{
	
	/**  serialVersionUID Propiedad del tipo long con el valor de serialVersionUID. */
	private static final long serialVersionUID = 3067192882048863805L;
	
	
	/**  Referencia privada al dao. */
	@EJB
	private DAODetRecepOperacion dAODetRecepOperacion;
	
	
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/**
	 * variable de utilerias
	 */
	/** La constante util. */
	public static final UtileriasRecepcionOpHTO util = UtileriasRecepcionOpHTO.getInstance();
	
	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas utilApartadas = UtileriasRecepcionOpConsultas.getInstance();
	
	/**
	 * Update cta receptora.
	 *
	 *  @param bean El objeto: bean
 	 * @param architechSessionBean El objeto: architech session bean
 	 * @param historico El objeto: historico
 	 * @return Objeto bean det recep operacion DAO
 	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanDetRecepOperacionDAO updateCtaReceptora(ArchitechSessionBean architechSessionBean, BeanDetRecepOperacionDAO bean, boolean historico,String ctaRecepAnt) throws BusinessException{
		BeanDetRecepOperacionDAO responseUpdate =  new BeanDetRecepOperacionDAO();
		
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec());
		
		
		/** se concatenan los filtro**/			
		parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion());
		parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion());
		parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
		parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
		parametros.add(bean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd());
		
		/**se actualiza la cuenta receptora si asi se desea**/
		responseUpdate = dAODetRecepOperacion.updateCtaReceptora(architechSessionBean, bean, parametros, bean.getBeanDetRecepOperacion().getNombrePantalla());
		
		
		//se crean los datos para la bitacora
		//se asigna el codigo de error
		String codigo = bean.getBeanDetRecepOperacion().getCodigoError();
		//dato nuevo
		 String datosUpdate =" ch_operacion = "+bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion()
				 +",cve_mi_instituc ="+bean.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion()
				 +",folio_paquete ="+bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete()
				 +",folio_pago ="+bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago()
				 +",cve_inst_ord="+bean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd();
		 String datosNo = "num_cuenta_rec= "+bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec();
		 //dato anterior
		 String datosAnt = "num_cuenta_rec= "+ctaRecepAnt;
		 //se concatenan los datos
		 String dato = ConstantesRecepOperacionApartada.TYPE_UPDATE+"/updateCtaReptora.do/"+datosUpdate+"/"+codigo+"/"+datosNo+"/"+datosAnt;
		 
		if(Errores.CODE_SUCCESFULLY.equals(responseUpdate.getBeanDetRecepOperacion().getError().getCodError())){
			responseUpdate.getBeanDetRecepOperacion().getError().setCodError(Errores.OK00000V);
			responseUpdate.getBeanDetRecepOperacion().getError().setTipoError(Errores.TIPO_MSJ_INFO);
			
			
			 /**genera la pista de auditoria**/
			BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, true, architechSessionBean, historico);
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditora(pistas, architechSessionBean);
			
	    }else{
	    	
	    	 /**genera la pista de auditoria**/
			BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, false, architechSessionBean, historico);
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditora(pistas, architechSessionBean);
			
	    	throw new BusinessException(responseUpdate.getBeanDetRecepOperacion().getError().getCodError(), responseUpdate.getBeanDetRecepOperacion().getError().getMsgError());
	    	
	    }
		/**se inicializa la variable de pistad de auditoria**/
		
	    	
		return responseUpdate;
	}
	
	
	

	
}