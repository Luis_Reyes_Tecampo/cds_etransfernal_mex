/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOExcepNomBenefImpl.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      19/09/2019 11:10:46 PM     Uriel Alfredo Botello R.    VSF        Creacion
 */

package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepNomBenef;
import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOExcepNomBenef;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasExcepNomBenef;

/**
 * Class BOExcepNomBenefImpl.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOExcepNomBenefImpl extends Architech implements BOExcepNomBenef {

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5738069296370132709L;
	
	/** La constante CAMPO. */
	private static final String CAMPO = "NOMBRE";
	
	/** La constante ERR_NO_EXISTE. */
	private static final String ERR_NO_EXISTE = "El Nombre de Beneficiario no existe.";
	
	/** La constante STR_VACIO. */
	private static final String STR_VACIO = "";
	
	/** La constante TABLA. */
	private static final String TABLA = "TRAN_NOMBRE_FIDEICO_EXENTOS";
	
	/** La constante NINGUNO. */
	private static final String NINGUNO = "NA";
	
	/** La variable que contiene informacion con respecto a: dao excep nom benef. */
	@EJB
	private DAOExcepNomBenef daoExcepNomBenef;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/**
	 * Consulta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Objeto de filtrado
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepNomBenef consultaNombres(ArchitechSessionBean sessionBean, BeanExcepNomBenef beanExcepNomBenef) 
			throws BusinessException {
		/** Se inicializan variables */
		BeanExcepNomBenef response = null;
		int totalReg = 0;
		/** valida si el bean de entrada es nulo */
		if(beanExcepNomBenef == null) {
			/** si es nulo, lo inicializa */
			beanExcepNomBenef = new BeanExcepNomBenef();
		}
		/** valida si la cuenta de filtro es nula o vacia */
		if(beanExcepNomBenef.getBeanFilter() == null || beanExcepNomBenef.getBeanFilter().getNombre() == null
				|| beanExcepNomBenef.getBeanFilter().getNombre().isEmpty()) {
			/** si es nula o vacia consulta el total de todos los registros */
			totalReg = daoExcepNomBenef.totalRegistros(STR_VACIO);
		} else {
			/** si no, consulta el total de registros por filtrado */
			totalReg = daoExcepNomBenef.totalRegistros(beanExcepNomBenef.getBeanFilter().getNombre());
		}
		/** asigna valores de paginador */
		beanExcepNomBenef.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		/** Consulta todas las cuentas */
		response = daoExcepNomBenef.consultaNombres(sessionBean,beanExcepNomBenef);
		/** calcula paginas y regresa valores */
		beanExcepNomBenef.getBeanPaginador().calculaPaginas(totalReg, HelperDAO.REGISTROS_X_PAGINA.toString());
		response.setBeanPaginador(beanExcepNomBenef.getBeanPaginador());
		response.getBeanFilter().setTotalReg(totalReg);
		/** Retorna resultado */
		return response;
	}

	/**
	 * Alta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param nombre El objeto: nombre --> Valor del registro nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepNomBenef altaNombres(ArchitechSessionBean sessionBean, String nombre)
			throws BusinessException {
		/** Se inicializan variables */
		BeanExcepNomBenef response = null;
		int consultaExisteNom = -1;
		BeanExcepNomBenef beanNomBenef = new BeanExcepNomBenef();
		Boolean estatusOp = true;
		beanNomBenef.getBeanFilter().setNombre(nombre);
		beanNomBenef.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Consulta la cuenta entrante */
		consultaExisteNom = daoExcepNomBenef.buscarRegistro(beanNomBenef.getBeanFilter().getNombre(), sessionBean);
		/** Valida si ya existe */
		if(consultaExisteNom == 0) {
			/** Si no existe la da de alta */
			response = daoExcepNomBenef.altaNombre(sessionBean, nombre);
		} else {
			/** Si ya existe regresa los siguientes datos */
			response = new BeanExcepNomBenef();
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError("Ya existe un registro con ese nombre.");
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			estatusOp = false;
		}
		/** genera pista de auditoria */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_INSERT, "altaNomBeneficiarios.do", estatusOp, sessionBean, nombre, STR_VACIO, CAMPO);
		
		return response;
	}

	/**
	 * Baja nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param listaNombres El objeto: lista nombres --> Lista de registros a eliminar
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepNomBenef bajaNombres(ArchitechSessionBean sessionBean, List<BeanNomBeneficiario> listaNombres)
			throws BusinessException {
		/** Inicializa variables */
		BeanExcepNomBenef response = new BeanExcepNomBenef();
		int consultaExisteNom = -1;
		Boolean estatusOp = true;
		BeanExcepNomBenef beanNomBenef = new BeanExcepNomBenef();
		beanNomBenef.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		int cuentasEliminar = listaNombres.size();
		int contNomsEliminados = 0;
		for(int i = 0; i < cuentasEliminar; i++) {
			beanNomBenef.getBeanFilter().setNombre(listaNombres.get(i).getNombre());
			/** Consulta la cuenta entrante */
			consultaExisteNom = daoExcepNomBenef.buscarRegistro(beanNomBenef.getBeanFilter().getNombre(), sessionBean);
			/** Valida si existe la cuenta */
			if(consultaExisteNom > 0) {
				/** si existe, elimina la cuenta */
				response = daoExcepNomBenef.bajaNombre(sessionBean, listaNombres.get(i).getNombre());
				contNomsEliminados++;
			} else {
				/** Si no existe regresa los siguientes datos */
				response = new BeanExcepNomBenef();
				response.getBeanError().setCodError(Errores.ED00130V);
				response.getBeanError().setMsgError(ERR_NO_EXISTE);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
				estatusOp = false;
				break;
			}
			/** genera pista de auditoria */
			generaPistaAuditoras(ConstantesCatalogos.TYPE_DELETE, "bajaNomBeneficiarios.do", estatusOp, sessionBean, STR_VACIO, listaNombres.get(i).getNombre(), CAMPO);
		}
		/** valida si el total de cuentas eliminadas fue el mismo de las que entraron  */
		if(contNomsEliminados != cuentasEliminar) {
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError("No se pudieron eliminar algunos de los registros seleccionados.");
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			estatusOp = false;
		}
		/** Retorna la respuesta */
		return response;
	}

	/**
	 * Modificacion nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param nomOld El objeto: nom old --> Valor del registro anterior
	 * @param nomNew El objeto: nom new --> Valor del registro nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepNomBenef modificacionNombres(ArchitechSessionBean sessionBean, String nomOld, String nomNew)
			throws BusinessException {
		/** Se inicializan variables */
		boolean estatusOp = true;
		BeanExcepNomBenef response = null;
		int consultaExisteNom = -1;
		BeanExcepNomBenef beanNomBenef = new BeanExcepNomBenef();
		beanNomBenef.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanNomBenef.getBeanFilter().setNombre(nomOld);
		/** Consulta la cuenta entrante */
		consultaExisteNom = daoExcepNomBenef.buscarRegistro(beanNomBenef.getBeanFilter().getNombre(), sessionBean);
		/** Valida si encontro la cuenta */
		if(consultaExisteNom > 0) {
			/** Si la encontro la modifica */
			response = daoExcepNomBenef.modificacionNombres(sessionBean, nomOld, nomNew);
			if(!response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				estatusOp = false;
			}
		} else {
			/** Si no encontro la cuenta regresa los siguientes datos */
			response = new BeanExcepNomBenef();
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError(ERR_NO_EXISTE);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			estatusOp = false;
		}
		/** genera pista de auditoria */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_UPDATE, "cambioNomBeneficiarios.do", estatusOp, sessionBean, nomNew, nomOld, CAMPO);
		/** Retorna el resultado */
		return response;
	}

	
	
	/**
	 * Exportar todos los nombres.
	 *
	 * @param session El objeto: session --> Objeto de session
	 * @return Objeto bean res base --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean sessionBean) throws BusinessException {
		boolean estatusOp = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String query = "SELECT NOMBRE FROM "+TABLA+" ORDER BY NOMBRE";
		int totalRegistros = daoExcepNomBenef.totalRegistros(STR_VACIO);
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasExcepNomBenef.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("NOMBRE_BENEFICIARIO");
		exportarRequest.setNombreRpt("CATALOGOS_EXCEPCION_NOMBRES_BENEFICIARIO_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, sessionBean);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			estatusOp = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** genera pista de auditoria  */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_EXPORT, "exporTodoNomBeneficiarios.do", estatusOp, sessionBean, STR_VACIO, STR_VACIO, NINGUNO);
		/** Retorno de la respuesta del BO**/
		return response;
	}
	
	
	/**
	 * Genera pista auditoras.
	 *
	 * @param operacion El objeto: operacion --> Tipo de operacion realizada
	 * @param urlController El objeto: url controller --> .do donde se dispara la accion
	 * @param resOp El objeto: res op --> Respuesta de la operacion
	 * @param sesion El objeto: sesion --> El objeto sessiom
	 * @param dtoNuevo El objeto: dto nuevo --> Valor nuevo
	 * @param dtoAnterior El objeto: dto anterior --> Valor anterior
	 * @param dtoModificado El objeto: dto modificado --> Valor del dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(TABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(NINGUNO);
		beanPista.setDatoModi(dtoModificado);
		/** valida el tipo de operacion */
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		/** valida la respuesta de la operacion */
		if (resOp) {
			beanPista.setEstatus(ConstantesCatalogos.OK);
		} else {
			beanPista.setEstatus(ConstantesCatalogos.NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

}
