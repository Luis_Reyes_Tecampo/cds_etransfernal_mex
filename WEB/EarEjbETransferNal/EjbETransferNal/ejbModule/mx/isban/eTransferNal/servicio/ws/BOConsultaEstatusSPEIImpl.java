/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOConsultaEstatusSPEIImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:38:09 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.ws;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.dao.ws.DAOConsultaEstatusSPEI;
import mx.isban.eTransferNal.servicios.ws.BOConsultaEstatusSPEI;
import net.sf.json.JSONObject;

/**
 * Class BOConsultaEstatusSPEIImpl.
 * Se encarga de la Logica de Negocio e integracion con la Capa DAO
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Remote(BOConsultaEstatusSPEI.class)
public class BOConsultaEstatusSPEIImpl extends Architech implements BOConsultaEstatusSPEI {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5342480990625340978L;

	/** La variable que contiene informacion con respecto a: dao consulta estatus SPEI. */
	@EJB
	private DAOConsultaEstatusSPEI daoConsultaEstatusSPEI;

	/**
	 * Consulta estatus SPEI.
	 *
	 * @param session El objeto: session
	 * @return Objeto res entrada string json DTO
	 * @throws BusinessException La business exception
	 * @see mx.isban.eTransferNal.servicios.ws.BOConsultaEstatusSPEI#consultaEstatusSPEI(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public ResEntradaStringJsonDTO consultaEstatusSPEI() {
		info("Entrando a la funcionalidad de consultaBancoSPID");
		EntradaStringJsonDTO responseDAO = daoConsultaEstatusSPEI.consultaEstatusSPEI();
		JSONObject jsonObject = JSONObject.fromString(responseDAO.getCadenaJson());
		if (jsonObject.isNullObject() || jsonObject.isEmpty()) {
			info("No se obtuvo Respuesta");
		} else if ("OK".equals(jsonObject.get("COD_ERROR"))) {
			info("Se realizo la consulta de forma correcta.");
		}
		ResEntradaStringJsonDTO response = new ResEntradaStringJsonDTO();
		response.setJson(responseDAO.getCadenaJson());
		return response;
	}

}
