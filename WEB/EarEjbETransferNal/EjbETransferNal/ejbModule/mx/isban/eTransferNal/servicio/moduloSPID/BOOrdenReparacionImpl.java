/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOOrdenReparacionImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloSPID.DAOOrdenesReparacion;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;

/**
 *Clase del tipo BO que se encarga  del negocio de las ordenes por reparar
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOOrdenReparacionImpl implements BOOrdenReparacion {
	
	/**
	 * Propiedad del tipo @see DAOBitTransaccional que contendra el valor de EJB
	 */
	@EJB
	private DAOBitTransaccional daoBitTransaccional;

	/**
	 * Propiedad del tipo @see DAOOrdenesReparacion que contendra el valor de EJB
	 */
	@EJB
	private DAOOrdenesReparacion daoOrdenesReparacion;

	@Override
	public BeanResOrdenReparacion obtenerOrdenesReparacion(
			BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException{
		BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = null;
		final BeanResOrdenReparacion beanResOrdenReparacion = new BeanResOrdenReparacion();
		
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		
		String sF = getSortField(sortField);
		
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResOrdenReparacionDAO = daoOrdenesReparacion.obtenerOrdenesReparacion(pag, architechSessionBean, sF, sortType);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparacionDAO.getCodError()) 
				&& beanResOrdenReparacionDAO.getListBeanOrdenReparacion().isEmpty()) {
			beanResOrdenReparacion.setCodError(Errores.ED00011V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparacionDAO.getCodError())){
			beanResOrdenReparacion.setListaOrdenesReparacion(beanResOrdenReparacionDAO.getListBeanOrdenReparacion());          
			beanResOrdenReparacion.setTotalReg(beanResOrdenReparacionDAO.getTotalReg());
			pag.calculaPaginas(beanResOrdenReparacionDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResOrdenReparacion.setBeanPaginador(pag);
			beanResOrdenReparacion.setCodError(Errores.OK00000V);
		}else{
			beanResOrdenReparacion.setCodError(Errores.EC00011B);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResOrdenReparacion;
	}
	
	@Override
	public BigDecimal obtenerOrdenesReparacionMontos(
			 ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException{
		BigDecimal totalImporte = BigDecimal.ZERO;
		
		String sF = getSortField(sortField);
		totalImporte = daoOrdenesReparacion.obtenerOrdenesReparacionMontos(architechSessionBean, sF, sortType);		
		return totalImporte;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("referencia".equals(sortField)){
			sF = "REFERENCIA";
		} else if ("cveCola".equals(sortField)){
			sF = "CVE_COLA";
		} else if ("cveTran".equals(sortField)){
			sF = "CVE_TRANSFE";
		} else if ("cveMecanismo".equals(sortField)){
			sF = "CVE_MECANISMO";
		} else if ("cveMedioEnt".equals(sortField)){
			sF = "CVE_MEDIO_ENT";
		} else if ("fechaCaptura".equals(sortField)){
			sF = "FCH_CAPTURA";
		} else if ("cveIntermeOrd".equals(sortField)){
			sF = "CVE_INTERME_ORD";
		} else if ("cveIntermeRec".equals(sortField)){
			sF = "CVE_INTERME_REC";
		} else if ("importeAbono".equals(sortField)){
			sF = "IMPORTE_ABONO";
		} else if ("estatus".equals(sortField)){
			sF = "ESTATUS";
		} else if ("folioPaquete".equals(sortField)){
			sF = "FOLIO_PAQUETE";
		} else if ("foloPago".equals(sortField)){
			sF = "FOLIO_PAGO";
		} else if ("mensajeError".equals(sortField)){
			sF = "COMENTARIO3";
		}
		return sF;
	}
	
	@Override
	public BeanResOrdenReparacion exportarTodoOrdenesReparacion(
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = null;
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		
		BeanPaginador paginador = beanReqOrdeReparacion.getPaginador();
		
		if (paginador == null){
			  paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		beanResOrdenReparacion = obtenerOrdenesReparacion(paginador, architechSessionBean, "", "");
		beanReqOrdeReparacion.setTotalReg(beanResOrdenReparacion.getTotalReg());
		beanResOrdenReparacionDAO = daoOrdenesReparacion.exportarTodoOrdenReparacion(beanReqOrdeReparacion, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparacionDAO.getCodError())) {
			beanResOrdenReparacion.setCodError(Errores.OK00002V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResOrdenReparacion.setNombreArchivo(beanResOrdenReparacionDAO.getNombreArchivo());
		} else {
			beanResOrdenReparacion.setCodError(Errores.EC00011B);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return beanResOrdenReparacion;
	}

	@Override
	public BeanResOrdenReparacion enviarOrdenesReparacion(
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		List<BeanOrdenReparacion> listaBeanOrdenReparacion = 
				helperRecepcionOperacion.filtraSelecionadosOrdenReparacion(
						beanReqOrdeReparacion, HelperRecepcionOperacion.SELECCIONADO);
		if (!listaBeanOrdenReparacion.isEmpty()) {
			beanResOrdenReparacion = actualizarOrdenesReparacion(
					beanReqOrdeReparacion, listaBeanOrdenReparacion, architechSessionBean);
			beanResOrdenReparacion.setCodError(Errores.OK00000V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			BeanPaginador paginador = beanReqOrdeReparacion.getPaginador();
			
			if (paginador == null)
			{
				paginador = new BeanPaginador();
			}
			paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
			
			beanResOrdenReparacion = obtenerOrdenesReparacion(paginador, architechSessionBean, "", "");
			beanResOrdenReparacion.setCodError(Errores.ED00068V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		
		return beanResOrdenReparacion;
	}
	
	/**
	 * @param beanReqOrdeReparacion Objeto del tipo BeanReqOrdeReparacion
	 * @param listaBeanOrdenReparacion Objeto del tipo List<BeanOrdenReparacion>
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return beanResOrdenReparacion Objeto del tipo BeanResOrdenReparacion
	 * @throws BusinessException Exception
	 */
	private BeanResOrdenReparacion actualizarOrdenesReparacion(BeanReqOrdeReparacion beanReqOrdeReparacion, 
			List<BeanOrdenReparacion> listaBeanOrdenReparacion, 
			ArchitechSessionBean architechSessionBean) throws BusinessException {
    	BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = null;
    	BeanResOrdenReparacion beanResOrdenReparacion = new BeanResOrdenReparacion();
    	
    	Date fechaActual = new Date();
    	
    	for(BeanOrdenReparacion beanOrdenReparacion : listaBeanOrdenReparacion){
			beanResOrdenReparacionDAO = daoOrdenesReparacion.actualizarOdenesReparacion(
					beanOrdenReparacion ,  architechSessionBean);

			if (beanResOrdenReparacion.getCodError() == null) {
				beanResOrdenReparacion.setCodError(beanResOrdenReparacionDAO.getCodError());			
			}
			
			if (!Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparacionDAO.getCodError())) {
				SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
				setBitacoraTrans("ENVORDPORREP", beanResOrdenReparacionDAO.getCodError(), "", "", 
						"", "IntEnviarReparacion.do", "NA", formatDate.format(fechaActual), beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia().toString(),
						beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeOrd(), beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeRec(), "ERR", beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono(), architechSessionBean);				
				
				throw new BusinessException(Errores.EC00011B);
        	}
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
			setBitacoraTrans("ENVORDPORREP", beanResOrdenReparacionDAO.getCodError(), "EXITO", "", 
					"", "IntEnviarReparacion.do", "NA", formatDate.format(fechaActual), beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia().toString(),
					beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeOrd(), beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeRec(), "OK",beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono(), architechSessionBean);
					
        }
    	
    	beanResOrdenReparacion = obtenerOrdenesReparacion(beanReqOrdeReparacion.getPaginador(), architechSessionBean, "", "");
		beanResOrdenReparacion.setCodError(Errores.ED00011V);
		beanReqOrdeReparacion.setTotalReg(beanResOrdenReparacionDAO.getTotalReg());
		beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
    	
		return beanResOrdenReparacion;
	}
	
	/**
     * @param idOperacion String con el id de la operacion
     * @param codError String con el codigo de error
     * @param descErr String con la descripcion del error
     * @param descOper String con la descripcion de la operacion
     * @param comentarios String con comentarios
     * @param servTran String con servicios tran
     * @param nombreArchivo String con nombre Archivo
     * @param fchOperacion String con fecha de operacion
     * @param referencia String con Referencia 
     * @param numCuentaOrd String con numero de cuenta ord
     * @param numCuentaRec String con numero de cuenta rec
     * @param estatus String con estatus
     * @param monto String con monto
     * @param sesion Objeto de sesion de la arquitectura
     */
    private void setBitacoraTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, String fchOperacion,String referencia,
			String numCuentaOrd, String numCuentaRec,
			String estatus,String monto, ArchitechSessionBean sesion){
    	UtileriasBitTrans utileriasBitTrans = new UtileriasBitTrans();
    	BeanReqInsertBitTrans beanReqInsertBitTrans = 
    		utileriasBitTrans.createBitacoraBitTrans(idOperacion, codError, descErr, descOper, 
    				comentarios, servTran, nombreArchivo, fchOperacion,referencia,
    				numCuentaOrd, numCuentaRec,
    				estatus,monto,  sesion);
    	daoBitTransaccional.guardaBitacora(beanReqInsertBitTrans, sesion);
    }

	/**
	 * @return daoOrdenesReparacion objeto del tipo DAOOrdenesReparacion
	 */
	public DAOOrdenesReparacion getDaoOrdenesReparacion() {
		return daoOrdenesReparacion;
	}

	/**
	 * @param daoOrdenesReparacion objeto del tipo DAOOrdenesReparacion
	 */
	public void setDaoOrdenesReparacion(DAOOrdenesReparacion daoOrdenesReparacion) {
		this.daoOrdenesReparacion = daoOrdenesReparacion;
	}
	
	/**
	 * @return the daoBitTransaccional
	 */
	public DAOBitTransaccional getDaoBitTransaccional() {
		return daoBitTransaccional;
	}

	/**
	 * @param daoBitTransaccional the daoBitTransaccional to set
	 */
	public void setDaoBitTransaccional(DAOBitTransaccional daoBitTransaccional) {
		this.daoBitTransaccional = daoBitTransaccional;
	}
}