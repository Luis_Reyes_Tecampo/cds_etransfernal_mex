/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAxFchHistSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Jan 04 18:24:31 CST 2017 jjbeltran  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOGenerarArchxFchCDAHistSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGenerarArchCDAxFchHistSPIDImpl extends Architech implements BOGenerarArchCDAxFchHistSPID {

	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = -5321466890228515339L;

	/**
	 * Referencia al dao de la funcionalidad GenerarArchCDAHist
	 */
	@EJB 
	private transient DAOGenerarArchxFchCDAHistSPID dAOGenerarArchCDAHist;
	
	/**
	 * Propiedad del tipo DAOBitacoraAdmon que almacena el valor de daoBitacora
	 */
	@EJB 
	private transient DAOBitacoraAdmon daoBitacora;

  /**Metodo que sirve para consultar la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
   */
	//Metodo que sirve para consultar la fecha de operacion
   public BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean) throws BusinessException {
	   //Se crean objetos de respuesta
	   final BeanResConsFechaOp beanResConsFechaOp = new BeanResConsFechaOp();
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
	   //Se hace la consulta al dao
       beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(architechSessionBean);
       //Se evalua codigo de respuesta
       if(Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   beanResConsFechaOp.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
    	   beanResConsFechaOp.setCodError(Errores.OK00000V);
       }else{
    	   throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
       }
       //Se devuelve el bean
      return beanResConsFechaOp;
   }
   /**Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAxFchHist Objeto del tipo BeanResProcGenArchCDAxFchHist
    */
   //Metodo que sirve para indicar que se debe de procesar el archivo
   public BeanResProcGenArchCDAxFchHist procesarGenArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean) throws BusinessException {
	   //Se crean objetos de soporte
	   BeanReqInsertBitacora beanReqInsertBitacora;
	   BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = null;
	   Date fechaOpBancMex = null;
	   Date fechaOpBanc = null;
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
	   Utilerias utilerias = Utilerias.getUtilerias();
	   //Se crean objetos de respuesta
	   final BeanResProcGenArchCDAxFchHist beanResProcGenArchCDAHist = new BeanResProcGenArchCDAxFchHist();
	   //Se hace la consulta al dao
	   beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(architechSessionBean);
	   //Se valida codigos de respuesta
	   if(!Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
       }
	   //Se obtiene la cadena de fecha
       fechaOpBancMex = utilerias.cadenaToFecha(beanResConsFechaOpDAO.getFechaOperacion(),
    		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       //Se valida fecha de operacion
       if(beanReqGenArchCDAHist.getFechaOp() !=null && !"".equals(beanReqGenArchCDAHist.getFechaOp())){
           fechaOpBanc = utilerias.cadenaToFecha(beanReqGenArchCDAHist.getFechaOp(),
        		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       }else{
    	   throw new BusinessException(Errores.ED00025V,Errores.DESC_ED00025V);
       }
       //Se compara fecha
       if(fechaOpBancMex.compareTo(fechaOpBanc)>0){
    	   String nombre = "FECHAOPER";
			info("ControllerGenerarArchxFchCDAHist.procesarGenerarArchCDAHist getFechaOp:"+beanReqGenArchCDAHist.getFechaOp());	   
    	   beanReqGenArchCDAHist.setNombreArchivo(nombre);
    	   //Se hace el procesamiento para el archivo en el dao
		   beanResGenArchCDAHistDAO = dAOGenerarArchCDAHist.genArchCDAHist(beanReqGenArchCDAHist, architechSessionBean);
		   //Se valida codigo de respuesta
		   if(Errores.CODE_SUCCESFULLY.equals(beanResGenArchCDAHistDAO.getCodError())){
			   //Se setean los campos en el bean de respuesta
    		   beanResProcGenArchCDAHist.setCodError(Errores.OK00000V);
    		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_INFO);
    		   beanResProcGenArchCDAHist.setNombreArchivo(nombre);
    		   //Se genera registro en bitacora
			   beanReqInsertBitacora = utilerias.creaBitacora("procesarGenerarArchCDAxFchHist.do", "1",
		        		"", new Date(), 
		        		"OK", "AARCHCDA", "TRAN_SPEI_CTG_CDA", "FCH_OPERACION,ID_PETICION,TIPO_PETICION", 
		        		Errores.OK00000V, "TRANSACCION EXITOSA", "INSERT",beanReqGenArchCDAHist.getNombreArchivo());
		        
		        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
			   
		   }else{
			   //Si hay un error se setea el codigo EC00011B
	    	   throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
		   }
       }else{
    	   //Si hay un error se setea el codigo ED00014V
    	   throw new BusinessException(Errores.ED00014V,Errores.TIPO_MSJ_ALERT);
       }
      //Se devuelve el bean
       return beanResProcGenArchCDAHist;
   }

}
