/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 16:08:14 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchCDAHist;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico
**/
@Remote(BOGenerarArchCDAHist.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGenerarArchCDAHistImpl extends Architech implements BOGenerarArchCDAHist {

	/**
	 * Constante de la serial version 
	 */
	private static final long serialVersionUID = -5321466890228515339L;

	/**
	 * Referencia al dao de la funcionalidad GenerarArchCDAHist
	 */
	@EJB 
	private DAOGenerarArchCDAHist dAOGenerarArchCDAHist;

  /**Metodo que sirve para consultar la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean)
       throws BusinessException{
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
	   final BeanResConsFechaOp beanResConsFechaOp = new BeanResConsFechaOp();
       beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(architechSessionBean, false);
       if(Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   beanResConsFechaOp.setCodError(Errores.OK00000V);
    	   beanResConsFechaOp.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
       }else{
    	   beanResConsFechaOp.setCodError(Errores.EC00011B);
    	   beanResConsFechaOp.setTipoError(Errores.TIPO_MSJ_ERROR);
       }
      return beanResConsFechaOp;
   }
   /**Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAHist Objeto del tipo BeanResProcGenArchCDAHist
    * @exception BusinessExceptionException de negocio
    */
    public BeanResProcGenArchCDAHist procesarGenArchCDAHist(BeanReqGenArchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean)
        throws BusinessException{
    	BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = null;
       final BeanResProcGenArchCDAHist beanResProcGenArchCDAHist = new BeanResProcGenArchCDAHist();
	   Date fechaOpBancMex = null;
	   Date fechaOpBanc = null;
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
       beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(architechSessionBean, false);
       
       if(!Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
		   return beanResProcGenArchCDAHist;
       }
       Utilerias utilerias = Utilerias.getUtilerias();
       fechaOpBancMex = utilerias.cadenaToFecha(beanResConsFechaOpDAO.getFechaOperacion(),
    		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       if(beanReqGenArchCDAHist.getFechaOperacion() !=null && !"".equals(beanReqGenArchCDAHist.getFechaOperacion())){
           fechaOpBanc = utilerias.cadenaToFecha(beanReqGenArchCDAHist.getFechaOperacion(),
        		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       }else{
    	   beanResProcGenArchCDAHist.setCodError(Errores.ED00025V);
    	   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ALERT);
    	   
    	   return beanResProcGenArchCDAHist;
       }
       if(fechaOpBancMex.compareTo(fechaOpBanc)>0){    	   
    	   if(beanReqGenArchCDAHist.getNombreArchivo()!=null && 
    			   beanReqGenArchCDAHist.getNombreArchivo().matches("[a-z|A-Z|0-9]+(.)(t|T)(x|X)(t|T)")){
    		   beanResGenArchCDAHistDAO = dAOGenerarArchCDAHist.genArchCDAHist(beanReqGenArchCDAHist, architechSessionBean);
    		   if(Errores.CODE_SUCCESFULLY.equals(beanResGenArchCDAHistDAO.getCodError())){
        		   beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
        		   beanResProcGenArchCDAHist.setCodError(Errores.OK00000V);
        		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_INFO);
    		   }else{
    			   beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
        		   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
        		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
    		   }
    	   }else{
    		   beanResProcGenArchCDAHist.setCodError(Errores.ED00013V);
    		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ALERT);
    	   }
       }else{
    	   beanResProcGenArchCDAHist.setCodError(Errores.ED00014V);
    	   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ALERT);
       }
      
       return beanResProcGenArchCDAHist;
   }
    /**
     * Metodo get de la referencia del servicio DAOGenerarArchCDAHist
     * @return dAOGenerarArchCDAHist Objeto del tipo DAOGenerarArchCDAHist
     */
	public DAOGenerarArchCDAHist getDAOGenerarArchCDAHist() {
		return dAOGenerarArchCDAHist;
	}
    /**
     * Metodo set de la referencia del servicio DAOGenerarArchCDAHist
     * @param dAOGenerarArchCDAHist Objeto del tipo DAOGenerarArchCDAHist
     */	
	public void setDAOGenerarArchCDAHist(DAOGenerarArchCDAHist dAOGenerarArchCDAHist) {
		this.dAOGenerarArchCDAHist = dAOGenerarArchCDAHist;
	}

    


}
