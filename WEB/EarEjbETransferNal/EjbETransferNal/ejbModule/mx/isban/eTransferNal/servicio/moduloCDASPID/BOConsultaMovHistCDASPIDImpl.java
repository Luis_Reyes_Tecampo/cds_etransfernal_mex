/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCDA;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCdaDAOSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOConsultaMovHistCDASPID;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Session Bean implementation class BOConsultaMovHistCDAImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaMovHistCDASPIDImpl extends Architech implements BOConsultaMovHistCDASPID {
       
    /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -6609397847914445121L;

	/**
	 * Referencia privada al dao de consulta movimiento Historico CDA
	 */
	@EJB
	private DAOConsultaMovHistCDASPID daoConsultaMovHistCDA;	
	
	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovHistCDA Objeto del tipo @see BeanResConsMovHistCDA
	 * @throws BusinessException Exception de negocio
	 */
    public BeanResConsMovHistCDASPID consultarMovHistCDA(BeanReqConsMovHistCDA beanReqConsMovHistCDA,ArchitechSessionBean architechSessionBean) throws BusinessException {
    	    
    	 	BeanPaginador paginador = null;
    	    BeanResConsMovHistCDASPID beanResConsMovHistCDASPID = new BeanResConsMovHistCDASPID();
    	    BeanResConsMovHistCdaDAOSPID resConsMovHistCDASPIDDAO = null;
    	    List<BeanMovimientoCDASPID> listBeanMovimientoCDASPID = null;
    	    String accion = "ACT";
			String vacio = "";
    	    paginador = beanReqConsMovHistCDA.getPaginador();
    	 
    	    // se inicializa beanPaginador
    	    if(paginador == null){
    	    	paginador = new BeanPaginador();
    	    	beanReqConsMovHistCDA.setPaginador(paginador);
    	    } else if(accion.equals(paginador.getAccion()) && vacio.equals(paginador.getPagina())){
	    	  	paginador.setPagina("1");
	      	}
    	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
    	    resConsMovHistCDASPIDDAO =  daoConsultaMovHistCDA.consultarMovHistCDASPID(beanReqConsMovHistCDA,architechSessionBean);
		    listBeanMovimientoCDASPID = resConsMovHistCDASPIDDAO.getListBeanMovimientoCDA();
		    
		    //se analiza codigoError obtenido de consultaMovimientos
		    if(Errores.CODE_SUCCESFULLY.equals(resConsMovHistCDASPIDDAO.getCodError()) && listBeanMovimientoCDASPID.isEmpty()){
		    	beanResConsMovHistCDASPID.setCodError(Errores.ED00011V);
		    	beanResConsMovHistCDASPID.setTipoError(Errores.TIPO_MSJ_INFO);
		    }else if(Errores.CODE_SUCCESFULLY.equals(resConsMovHistCDASPIDDAO.getCodError())){
		    	//si consulta fue existosa
		    	beanResConsMovHistCDASPID.setTotalReg(resConsMovHistCDASPIDDAO.getTotalReg());
		    	beanResConsMovHistCDASPID.setListBeanMovimientoCDA(resConsMovHistCDASPIDDAO.getListBeanMovimientoCDA());
			    paginador.calculaPaginas(beanResConsMovHistCDASPID.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			    beanResConsMovHistCDASPID.setBeanPaginador(paginador);		    
			    beanResConsMovHistCDASPID.setCodError(Errores.OK00000V);
		    }else {
		    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		     }
		    //Se devuelve bean
		    return beanResConsMovHistCDASPID;
    }
	
    /**
	 * Metodo que se encarga de solicitar la insercion de la peticion de
	 * Exportar Todos de los Movimientos Historicos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConMovHistCDA  Objeto del tipo @see BeanResConsMovHistCDA 
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovHistCDASPID consultaMovHistCDAExpTodos(BeanReqConsMovHistCDA beanReqConsMovHistCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	
    	BeanResConsMovHistCDASPID beanResConMovHistCDA = new BeanResConsMovHistCDASPID();
    	BeanResConsMovHistCdaDAOSPID beanResConsMovHistCdaDAO = null;
    	beanResConMovHistCDA = consultarMovHistCDA(beanReqConsMovHistCDA,architechSessionBean);	
    	beanReqConsMovHistCDA.setTotalReg(beanResConMovHistCDA.getTotalReg());
    	beanResConsMovHistCdaDAO = daoConsultaMovHistCDA.guardarConsultaMovHistExpTodoSPID(beanReqConsMovHistCDA, architechSessionBean);

	    //se analiza codigoError obtenido
    	 if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovHistCdaDAO.getCodError())){
    		 //si consulta fue existosa
    		 beanResConMovHistCDA.setCodError(Errores.OK00001V);
    		 beanResConMovHistCDA.setTipoError(Errores.TIPO_MSJ_INFO);
    		 beanResConMovHistCDA.setNombreArchivo(beanResConsMovHistCdaDAO.getNombreArchivo());
 		}else{
	    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
    	//Se devuelve bean
		return beanResConMovHistCDA;
    }

    /**
	 * Metodo que sirve para solicitar el detalle de un movimiento Historico CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetHistCDA Objeto del tipo @see BeanResConsMovDetHistCDA
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovDetHistCDA consMovHistDetCDA(BeanReqConsMovCDADet beanReqConsMovCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	BeanResConsMovDetHistCDA beanResConsMovDetHistCDA = new BeanResConsMovDetHistCDA();
		BeanResConsMovDetHistCdaDAOSPID beanResConsMovDetHistCdaDAO = null;
		beanResConsMovDetHistCdaDAO =  daoConsultaMovHistCDA.consultarMovDetHistCDASPID(beanReqConsMovCDA,architechSessionBean);

	    //se analiza codigoError obtenido
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovDetHistCdaDAO.getCodError())){
	    	//si consulta fue existosa
			beanResConsMovDetHistCDA.setCodError(Errores.OK00000V);
			beanResConsMovDetHistCDA.setBeanMovCdaDatosGenerales(beanResConsMovDetHistCdaDAO.getBeanMovCdaDatosGenerales());
			beanResConsMovDetHistCDA.setBeanConsMovCdaOrdenante(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaOrdenante());
			beanResConsMovDetHistCDA.setBeanConsMovCdaBeneficiario(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaBeneficiario());
		}else{
	    	throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
		//Se devuelve bean
		return beanResConsMovDetHistCDA;
    }
}
