/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOMantenimientoCertificadoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResDesencripta;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEncripta;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificadoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.moduloSPID.DAOEncripta;
import mx.isban.eTransferNal.dao.moduloSPID.DAOMantenimientoCertificado;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Clase del tipo BO que se encarga  del negocio del mantenimiento de certificados.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMantenimientoCertificadoImpl implements BOMatenimientoCertificado {
	
	/** Constante con los valores a guardar en la bitacora. */
	private static final String VALUES_BIT_FORMAT = "CVE_MI_INSTITUC=%s|NUM_CERTIFICADO=%s|ESTADO_CERT=%s|SW_MANT=%s" +
			"|SW_DEFAULT_FIRMA=%s|SW_DEFAULT_MANT=%s|FACUL_FIRMA=%s|FACUL_MANT=%s|USUARIO_ALTA=%s" +
			"|USUARIO_MODIF=%s|FCH_REGISTRO=%s|FCH_MODIF=%s|PALABRA_CLAVE=%s|MENSAJE=%s";
	
	/** Constante con los valores a guardar en la bitacora. */
	private static final String DATO_FIJO_BIT = "CVE_MI_INSTITUC, NUM_CERTIFICADO";
	
	/** Constante que guarda la tabla TRAN_SPID_CERTIF. */
	private static final String TRAN_SPID_CERTIF = "TRAN_SPID_CERTIF";
	
	/** Constante que guarda la tabla TRAN_SPEI_CERTIF. */
	private static final String TRAN_SPEI_CERTIF = "TRAN_SPEI_CERTIF";
	
	/** Propiedad que almacen el parametro enviado. */
	private static final String MANTENIMIENTOCERTIFICADOSPEI ="mantenimientoCertificadoSPEI";
	
	/** La variable que contiene informacion con respecto a: tabla. */
	private static String TABLA;
	
	/** La variable que contiene informacion con respecto a: dao bit administrativa. */
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;

	/** Propiedad del tipo DAOMantenimientoCertificado. */
	@EJB
	private DAOMantenimientoCertificado daoMantenimientoCertificado;
	
	/** Referencia al DAO que realiza la encripcion. */
	@EJB
	private DAOEncripta daoEncripta;
	

	@Override
	/**
	 * Metodo que obtiene los certificados registrados
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException
	 */
	
	public BeanResMantenimientoCertificado obtenerMantenimientoCertificados(BeanPaginador beanPaginador,
			ArchitechSessionBean architechBean, String nomPantalla) throws BusinessException {
		BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = null;
		final BeanResMantenimientoCertificado beanResMantenimientoCertificado = new BeanResMantenimientoCertificado();
		
		/**
		 * valida el paginador
		 * */
			BeanPaginador pag = beanPaginador;
			if(pag==null){
				pag = new BeanPaginador();
			}
			/**muestra de 2 en 2 registros*/
			pag.paginar("2");		
			beanResMantenimientoCertificadoDAO = daoMantenimientoCertificado.obtenerTopologias(beanPaginador, architechBean, nomPantalla);
			/**
			 * trae informacion del error en caso de que haya 
			 * valida el bean para paginar
			 * */
			if (Errores.CODE_SUCCESFULLY.equals(beanResMantenimientoCertificadoDAO.getCodError()) 
					&& beanResMantenimientoCertificadoDAO.getListaMantenimientoCertificado().isEmpty()) {
				beanResMantenimientoCertificado.setCodError(Errores.ED00011V);
				beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResMantenimientoCertificado.setListaMantenimientoCertificado(Collections.<BeanMantenimientoCertificado> emptyList());
			}else if(Errores.CODE_SUCCESFULLY.equals(beanResMantenimientoCertificadoDAO.getCodError())){
				beanResMantenimientoCertificado.setListaMantenimientoCertificado(beanResMantenimientoCertificadoDAO.getListaMantenimientoCertificado());          
				beanResMantenimientoCertificado.setTotalReg(beanResMantenimientoCertificadoDAO.getTotalReg());
				pag.calculaPaginas(beanResMantenimientoCertificadoDAO.getTotalReg(), "2");
				
				for(BeanMantenimientoCertificado beanMantenimientoCertificado:beanResMantenimientoCertificadoDAO.getListaMantenimientoCertificado()){
					if(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos()!=null && !"".equals(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave())){
						BeanResDesencripta beanResDesencripta = daoEncripta.desEncripta(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(), architechBean);
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setPalabraClave(beanResDesencripta.getCadDesencriptada());
					}
				}
				/**
				 * obtiene informacion en cuanto a los errores
				 * o mensajes
				 * */
				beanResMantenimientoCertificado.setBeanPaginador(pag);
				beanResMantenimientoCertificado.setCodError(Errores.OK00000V);
				beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				beanResMantenimientoCertificado.setCodError(Errores.EC00011B);
				beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR); 
			}

			/**
	    	 * Regresa los datos obtenidos de la ejecucion del query
	    	 * */
		return beanResMantenimientoCertificado;
	}
	
	/**
	 * valida el parametro obtenido y a partir de este se diferencia
	 * el tipo de modulo del que proviene agregandose
	 *  el tipo de tabla en la cual se va a hacer el insert del registro
	 * */
	@Override
	public String asignaTablaDB(String nomPantalla) {
		if(MANTENIMIENTOCERTIFICADOSPEI.equals(nomPantalla)) {
			TABLA = TRAN_SPEI_CERTIF;
		}else {
			TABLA = TRAN_SPID_CERTIF;
		}
		return TABLA;
	}
	

	@Override
	/**
	 * Metodo que elimina un certificado
	 * @param beanReqMantenimientoCertificado Objeto del tipo @see BeanReqMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMantenimientoCertificado
	 * @param beanPaginador El objeto: bean paginador
	 * @throws BusinessException
	 */
	public BeanResMantenimientoCertificado eliminarTopologia(
			BeanReqMantenimientoCertificado beanReqMantenimientoCertificado, ArchitechSessionBean architechBean, String nomPantalla,
			 BeanPaginador beanPaginador)
					throws BusinessException {
		BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = null;
		BeanResMantenimientoCertificado beanResMantenimientoCertificado = new BeanResMantenimientoCertificado();
		final String DELETE = "DELETE";
		final String CONTROLLER = "IntEliminarMantenimiento.do";
		/**
		 * valida el BeanPaginador
		 * si es null se hace una instancia del BeanPaginador();
		 * */
		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}	
		/**
		 * variable de tipo cadena vacia
		 * el cual almacena los registros
		 * del paginado
		 * */
		String valAnt = "";
		
		try {
			/**
			 * declaracion de listado de lista listaTopologia
			 * de tipo BeanMantenimientoCertificado
			 * */
			List<BeanMantenimientoCertificado> listaTopologia = filtrarSeleccionados(beanReqMantenimientoCertificado);
			/**
			 * elimina la topologia
			 * */
			for (BeanMantenimientoCertificado beanMantenimientoCertificado : listaTopologia) {
				beanResMantenimientoCertificadoDAO = daoMantenimientoCertificado.eliminarTopologia(
						beanMantenimientoCertificado, architechBean, nomPantalla);
				valAnt = String.format(VALUES_BIT_FORMAT, beanMantenimientoCertificado.getClaveIns(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getEstado(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento(),
						beanMantenimientoCertificado.getSDefaultFirma(),
						beanMantenimientoCertificado.getSDefaultMant(),
						beanMantenimientoCertificado.getSFacultadFirma(),
						beanMantenimientoCertificado.getSFacultadMant(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuario(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuarioModifica(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaAlta(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaModificacion(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(),
						beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
				/**
				 * Muestra informacion al eliminar un registro
				 * */
				if(!Errores.CODE_SUCCESFULLY.equals(beanResMantenimientoCertificadoDAO.getCodError()) && !Errores.ED00139V.equals(beanResMantenimientoCertificadoDAO.getCodError())){					
					setBitacoraTrans("NOK", asignaTablaDB(nomPantalla), DELETE, CONTROLLER, valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(
							pag, architechBean, nomPantalla);
					beanResMantenimientoCertificado.setCodError(Errores.EC00011B);
					beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
					return beanResMantenimientoCertificado;
				}
				if(Errores.ED00139V.equals(beanResMantenimientoCertificadoDAO.getCodError()) && nomPantalla.equals(MANTENIMIENTOCERTIFICADOSPEI)){					
					setBitacoraTrans("NOK", asignaTablaDB(nomPantalla), DELETE, CONTROLLER, valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(
							pag, architechBean, nomPantalla);
					beanResMantenimientoCertificado.setCodError(Errores.ED00139V);
					beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ALERT);
					return beanResMantenimientoCertificado;
				}
			}	
			/**
			 * asigna el codigo y el tipo de error en caso de haber uno
			 * */
			setBitacoraTrans("OK", asignaTablaDB(nomPantalla), DELETE, CONTROLLER, valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
			beanResMantenimientoCertificado = obtenerMantenimientoCertificados(
					new BeanPaginador(), architechBean, nomPantalla);
			beanResMantenimientoCertificado.setCodError(Errores.OK00000V);
			beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_INFO);
	    	
			/**
			 * manejo de excepciones cuando hay errores
			 * **/
		} catch (BusinessException e) {
			beanResMantenimientoCertificado = obtenerMantenimientoCertificados(
					beanReqMantenimientoCertificado.getPaginador(), architechBean, nomPantalla);
			beanResMantenimientoCertificado.setCodError(Errores.EC00011B);
			beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
			
			throw e;
		}
		/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */
		return beanResMantenimientoCertificado;
	}
	

	@Override
	/**
	 * Metodo que guarda un ceritifcado 
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException
	 */
	public BeanResMantenimientoCertificado guardarCertificado(BeanMantenimientoCertificado beanMantenimientoCertificado,
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador, String nomPantalla) throws BusinessException {
		BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = null;
		BeanResMantenimientoCertificado beanResMantenimientoCertificado = new BeanResMantenimientoCertificado();
		
		try {
			/**
			 * valida si la topologia a insertar ya existe
			 * */
			if (!daoMantenimientoCertificado.isRepetido(beanMantenimientoCertificado,nomPantalla)) {
				if(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos()!=null && !"".equals(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave())){
					BeanResEncripta beanResEncripta = daoEncripta.encripta(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(), architechBean);
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setPalabraClave(beanResEncripta.getCadEncriptada());
				}
				beanResMantenimientoCertificadoDAO = daoMantenimientoCertificado.guardarMantenimiento(
						beanMantenimientoCertificado, architechBean,nomPantalla);
				
				String valNvo = String.format(VALUES_BIT_FORMAT, beanMantenimientoCertificado.getClaveIns(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getEstado(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento(),
					beanMantenimientoCertificado.getSDefaultFirma(),
					beanMantenimientoCertificado.getSDefaultMant(),
					beanMantenimientoCertificado.getSFacultadFirma(),
					beanMantenimientoCertificado.getSFacultadMant(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuario(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuarioModifica(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaAlta(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaModificacion(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(),
					beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
				/**
				 * mensajes mostrados al insertar una nueva topologia
				 * */
				if(Errores.CODE_SUCCESFULLY.equals(beanResMantenimientoCertificadoDAO.getCodError())){
					setBitacoraTrans("OK", asignaTablaDB(nomPantalla), "INSERT", "IntGuardarMantenimiento.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
				}else{
					setBitacoraTrans("NOK", asignaTablaDB(nomPantalla), "INSERT", "IntGuardarMantenimiento.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
					beanResMantenimientoCertificado.setCodError(Errores.ED00061V);
					beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
				}
			} else {				
				beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
				beanResMantenimientoCertificado.setCodError(Errores.ED00061V);
				beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
			/**
			 * manejo de errores
			 * */
		} catch (BusinessException e) {
			beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
			throw e;
		}
		/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */
		return beanResMantenimientoCertificado;
	}
	

	@Override
	/**
	 * Metodo que guarda la edicion de un certificado
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param numeroCertOld Objeto del tipo @see String
	 * @param cveInsOld  Objeto del tipo @see String
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException
	 */
	public BeanResMantenimientoCertificado guardarEdicionMantenimiento(
			BeanMantenimientoCertificado beanMantenimientoCertificado, ArchitechSessionBean architechBean,
			BeanPaginador beanPaginador, String numeroCertOld, String cveInsOld, String nomPantalla) throws BusinessException {
		BeanResMantenimientoCertificadoDAO beanResMantenimientoCertificadoDAO = null;
		BeanResEncripta beanResEncripta = null;
		BeanResMantenimientoCertificado beanResMantenimientoCertificado = new BeanResMantenimientoCertificado();
		if(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos()!=null && !"".equals(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave())){
			beanResEncripta = daoEncripta.encripta(beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(), architechBean);	
			beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().setPalabraClave(beanResEncripta.getCadEncriptada());
		}
		try {
			if (!daoMantenimientoCertificado.isRepetido(beanMantenimientoCertificado,nomPantalla)) {
				beanResMantenimientoCertificadoDAO = daoMantenimientoCertificado.obtenerMantenimientoCertificado(cveInsOld, numeroCertOld, architechBean,nomPantalla);
				BeanMantenimientoCertificado beanMantenimientoCertificadoAnt = beanResMantenimientoCertificadoDAO.getListaMantenimientoCertificado().get(0);
				String valAnt = String.format(VALUES_BIT_FORMAT, cveInsOld,
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getNumeroCertificado(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getEstado(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getMantenimiento(),
						beanMantenimientoCertificadoAnt.getSDefaultFirma(),
						beanMantenimientoCertificadoAnt.getSDefaultMant(),
						beanMantenimientoCertificadoAnt.getSFacultadFirma(),
						beanMantenimientoCertificadoAnt.getSFacultadMant(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getUsuario(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getUsuarioModifica(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getFechaAlta(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getFechaModificacion(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getPalabraClave(),
						beanMantenimientoCertificadoAnt.getBeanMantenimientoCertificadoDos().getMensaje());
				
				beanResMantenimientoCertificadoDAO = daoMantenimientoCertificado.guardarEdicionMantenimiento(
						beanMantenimientoCertificado, architechBean, numeroCertOld, cveInsOld,nomPantalla);
				/**
				 * manejo de errores
				 * trae el codigo y el mensaje de error 
				 * */
				if(Errores.CODE_SUCCESFULLY.equals(beanResMantenimientoCertificadoDAO.getCodError())){
					registrarPistaDeAuditoria(cveInsOld, beanMantenimientoCertificado, architechBean, valAnt, nomPantalla);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
				}else{
					setBitacoraTrans("NOK", asignaTablaDB(nomPantalla), "UPDATE", "IntGuardarEdicionMantenimiento.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
					beanResMantenimientoCertificado.setCodError(Errores.EC00011B);
					beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
				}
			} else {				
				beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
				beanResMantenimientoCertificado.setCodError(Errores.ED00061V);
				beanResMantenimientoCertificado.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
			/**
			 * manejo de errores
			 * excepcion de errores 
			 * */
		} catch (BusinessException e) {
			beanResMantenimientoCertificado = obtenerMantenimientoCertificados(beanPaginador, architechBean, nomPantalla);
			throw e;
		}	
		/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */
		return beanResMantenimientoCertificado;
	}

	/**
	 * Registrar pista de auditoria.
	 *
	 * @param cveInsOld Objeto del tipo String
	 * @param beanMantenimientoCertificado Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo ArchitechSessionBean
	 * @param valAnt Objeto del tipo String
	 * @param nomPantalla El objeto: nom pantalla
	 */
	private void registrarPistaDeAuditoria(String cveInsOld, BeanMantenimientoCertificado beanMantenimientoCertificado,
			ArchitechSessionBean architechBean, String valAnt, String nomPantalla) {
		String valNvo = String.format(VALUES_BIT_FORMAT, cveInsOld,
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getNumeroCertificado(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getEstado(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMantenimiento(),
				beanMantenimientoCertificado.getSDefaultFirma(),
				beanMantenimientoCertificado.getSDefaultMant(),
				beanMantenimientoCertificado.getSFacultadFirma(),
				beanMantenimientoCertificado.getSFacultadMant(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuario(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getUsuarioModifica(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaAlta(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getFechaModificacion(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getPalabraClave(),
				beanMantenimientoCertificado.getBeanMantenimientoCertificadoDos().getMensaje());
		setBitacoraTrans("OK", asignaTablaDB(nomPantalla), "UPDATE", "IntGuardarEdicionMantenimiento.do", valAnt, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
	}
	
	/**
	 * Metodo que filtra los registros seleccionados.
	 *
	 * @param beanReqMantenimientoCertificado Objeto del tipo BeanReqMantenimientoCertificado
	 * @return List<BeanMantenimientoCertificado>
	 */
	private List<BeanMantenimientoCertificado> filtrarSeleccionados(
			BeanReqMantenimientoCertificado beanReqMantenimientoCertificado) {
		List<BeanMantenimientoCertificado> listaSeleccionados = new ArrayList<BeanMantenimientoCertificado>();
		
		for (BeanMantenimientoCertificado beanMantenimientoCertificado : 
			beanReqMantenimientoCertificado.getListaMantenimientoCertificado()) {
			if (beanMantenimientoCertificado.isSeleccionado()){
				listaSeleccionados.add(beanMantenimientoCertificado);
			}
		}
		/**
		 * regresa los registros seleccionados
		 * */
		return listaSeleccionados;
	}
	
	/**
	 * Sets the bitacora trans.
	 *
	 * @param codOper Objeto del tipo String
	 * @param tablaAfect Objeto del tipo String
	 * @param tipoOper Objeto del tipo String
	 * @param servTran Objeto del tipo String
	 * @param valAnt Objeto del tipo String
	 * @param valNvo Objeto del tipo String
	 * @param datoModi Objeto del tipo String
	 * @param datoFijo Objeto del tipo String
	 * @param comentarios Objeto del tipo String
	 * @param sesion Objeto del tipo String
	 */
    private void setBitacoraTrans(String codOper, String tablaAfect, String tipoOper, String servTran, String valAnt,
		String valNvo, String datoModi, String datoFijo, String comentarios
		, ArchitechSessionBean sesion){
    	
    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
    		utileriasBitAdmin.createBitacoraBitAdmin(codOper, tablaAfect, tipoOper, servTran, valAnt, valNvo, 
				datoModi, datoFijo, comentarios, sesion);
    	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, sesion);
    }

	/**
	 * Metodo get que sirve para obtener la propiedad daoMantenimientoCertificado.
	 *
	 * @return daoMantenimientoCertificado Objeto del tipo @see daoMantenimientoCertificado
	 */
	public DAOMantenimientoCertificado getDaoMantenimientoCertificado() {
		return daoMantenimientoCertificado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad daoMantenimientoCertificado.
	 *
	 * @param daoMantenimientoCertificado Objeto del tipo @see DAOMantenimientoCertificado
	 */
	public void setDaoMantenimientoCertificado(DAOMantenimientoCertificado daoMantenimientoCertificado) {
		this.daoMantenimientoCertificado = daoMantenimientoCertificado;
	}
	
	/**
	 * Obtener el objeto: dao bit administrativa.
	 *
	 * @return the daoBitAdministrativa
	 */
	public DAOBitAdministrativa getDaoBitAdministrativa() {
		return daoBitAdministrativa;
	}

	/**
	 * Definir el objeto: dao bit administrativa.
	 *
	 * @param daoBitAdministrativa the daoBitAdministrativa to set
	 */
	public void setDaoBitAdministrativa(DAOBitAdministrativa daoBitAdministrativa) {
		this.daoBitAdministrativa = daoBitAdministrativa;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad daoEncripta.
	 *
	 * @return daoEncripta Objeto del tipo DAOEncripta
	 */
	public DAOEncripta getDaoEncripta() {
		return daoEncripta;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad daoEncripta.
	 *
	 * @param daoEncripta del tipo DAOEncripta
	 */
	public void setDaoEncripta(DAOEncripta daoEncripta) {
		this.daoEncripta = daoEncripta;
	}

	
}
