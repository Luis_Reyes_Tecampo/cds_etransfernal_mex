/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecComun;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAORecepOperacion;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchCDAHist;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpFunciones;

/**
 * Clase del tipo BO que se encarga de mostrar las operaciones de la cta alterna
 * y principal.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperacionImpl extends Architech implements BORecepOperacion {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1377532272492477541L;

	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private transient DAORecepOperacion daoRecepOperacion;

	/** Referencia privada al dao de generar archivos historicos. */
	@EJB
	private DAOGenerarArchCDAHist dAOGenerarArchCDAHist;

	/** variable de utilerias. */
	public static final UtileriasRecepcionOpFunciones UTIL = UtileriasRecepcionOpFunciones.getInstance();

	/**
	 * Consulta todas transf.
	 *
	 * @param beanResConsTranSpeiRec the bean res cons tran spei rec
	 * @param beanReqTranSpeiRec     El objeto: bean req tran spei rec
	 * @param sessionBean            El objeto: session bean
	 * @param historico              El objeto: historico
	 * @param beanTranSpeiRecOper    El objeto: bean tran spei rec oper
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec consultaTodasTransf(BeanResTranSpeiRec beanResConsTranSpeiRec,
			BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean, boolean historico,
			BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		/** Se inicializan las variables **/
		BeanResTranSpeiRec beanResConsTranSpeiRecNew = consultaTodasTransferencias(beanResConsTranSpeiRec,
				beanReqTranSpeiRec, sessionBean, historico, beanTranSpeiRecOper);
		if (beanResConsTranSpeiRecNew.getBeanError().getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanResConsTranSpeiRecNew.getBeanError().getCodError(),
					beanResConsTranSpeiRecNew.getBeanError().getMsgError());
		}
		/** Regresa el resultado de las operaciones **/
		return beanResConsTranSpeiRecNew;
	}

	/**
	 * Propiedad del tipo DAORecepOperacion que almacena el valor de
	 * daoRecepOperacionExt.
	 *
	 * @param beanResTranSpeiRec  the bean res tran spei rec
	 * @param beanReqTranSpeiRec  El objeto: bean req tran spei rec
	 * @param sessionBean         El objeto: session bean
	 * @param historico           El objeto: historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	public BeanResTranSpeiRec consultaTodasTransferencias(BeanResTranSpeiRec beanResTranSpeiRec,
			BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean, boolean historico,
			BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		/** Se inicializan las variables **/
		BeanResTranSpeiRec beanResConsTranSpeiRec;
		if (null == beanResTranSpeiRec) {
			beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		} else {
			beanResConsTranSpeiRec = beanResTranSpeiRec;
		}
		// se inicializ bean de error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		// se inicializa el bean de comunes
		beanResConsTranSpeiRec.setBeanComun(new BeanTranSpeiRecComun());
		/** inicializa el paginador **/
		BeanPaginador paginador = null;
		BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO = null;
		/** Se inicializan el numero del paginador **/
		paginador = beanReqTranSpeiRec.getPaginador();
		if (paginador == null) {
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanReqTranSpeiRec.setPaginador(paginador);
		/** Busca los datos, llama la funcion ejecuta consulta **/
		beanResConsTranSpeiRecDAO = ejecutaConsulta(beanReqTranSpeiRec, sessionBean, historico, beanTranSpeiRecOper);
		/** Pregunta si viene de la pantalla de historico **/
		if (historico) {
			beanResConsTranSpeiRec.getBeanComun()
					.setFechaOperacion(beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
			BeanResConsFechaOpDAO beanResConsFechaOpDAO = daoRecepOperacion.consultaFechaOperacion(sessionBean);
			BeanResConsMiInstDAO beanResConsMiInstDAO = daoRecepOperacion
					.consultaMiInstitucion(beanReqTranSpeiRec.getBeanComun().getNombrePantalla(), sessionBean);
			beanResConsTranSpeiRec.getBeanComun().setFechaActual(beanResConsFechaOpDAO.getFechaOperacion());
			beanResConsTranSpeiRec.getBeanComun().setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		}
		/** Pregunta el numero del codigo de error **/
		if (beanResConsTranSpeiRecDAO.getBeanTranSpeiRecList() != null && Errores.OK00000V.equals(beanResConsTranSpeiRecDAO.getCodError())
				&& beanResConsTranSpeiRecDAO.getBeanTranSpeiRecList().isEmpty()) {
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00011V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsTranSpeiRec.getBeanComun().setOpcion(beanReqTranSpeiRec.getBeanComun().getOpcion());
			beanResConsTranSpeiRec.setListBeanTranSpeiRec(beanResConsTranSpeiRecDAO.getBeanTranSpeiRecList());
		} else if (Errores.OK00000V.equals(beanResConsTranSpeiRecDAO.getCodError())) {
			beanResConsTranSpeiRec = UTIL.seteaRespuestaExitosa(beanReqTranSpeiRec, beanResConsTranSpeiRecDAO,
					beanResConsTranSpeiRec, paginador);
		} else {
			/** Se asigna los codigo de error **/
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.EC00011B);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResConsTranSpeiRec.setListBeanTranSpeiRec(beanResConsTranSpeiRecDAO.getBeanTranSpeiRecList());
		}
		beanResConsTranSpeiRec = consultaListas(beanResConsTranSpeiRec, beanReqTranSpeiRec, sessionBean, historico,
				beanTranSpeiRecOper);
		/** Regresa el resultado de las operaciones **/
		return beanResConsTranSpeiRec;
	}

	/**
	 * Consulta listas.
	 *
	 * @param beanResConsTranSpeiRec the bean res cons tran spei rec
	 * @param beanReqTranSpeiRec     the bean req tran spei rec
	 * @param session                the session
	 * @param historico              the historico
	 * @param beanTranSpeiRecOper    the bean tran spei rec oper
	 * @return the bean res tran spei rec
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResTranSpeiRec consultaListas(BeanResTranSpeiRec beanResConsTranSpeiRec,
			BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean session, boolean historico,
			BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		BeanResConsMotDevolucionDAO beanResConsMotDevolucionDAO = null;
		if (null != beanResConsTranSpeiRec && (beanResConsTranSpeiRec.getBeanMotDevolucionList() == null
				|| beanResConsTranSpeiRec.getBeanMotDevolucionList().isEmpty())) {
			beanResConsMotDevolucionDAO = daoRecepOperacion.consultaDevoluciones(session, 1, historico,
					beanReqTranSpeiRec);
			beanResConsTranSpeiRec.setBeanMotDevolucionList(beanResConsMotDevolucionDAO.getBeanMotDevolucionList());
		}

		if (null != beanResConsTranSpeiRec && (beanResConsTranSpeiRec.getBeanBusquedaList() == null
				|| beanResConsTranSpeiRec.getBeanBusquedaList().isEmpty())) {
			beanResConsMotDevolucionDAO = daoRecepOperacion.consultaDevoluciones(session, 2, historico,
					beanReqTranSpeiRec);
			beanResConsTranSpeiRec.setBeanBusquedaList(beanResConsMotDevolucionDAO.getBeanMotDevolucionList());
		}

		if (null != beanResConsTranSpeiRec && (beanResConsTranSpeiRec.getBeanListUsuarios() == null
				|| beanResConsTranSpeiRec.getBeanListUsuarios().isEmpty())) {
			beanResConsMotDevolucionDAO = daoRecepOperacion.consultaDevoluciones(session, 3, historico,
					beanReqTranSpeiRec);
			beanResConsTranSpeiRec.setBeanListUsuarios(beanResConsMotDevolucionDAO.getBeanMotDevolucionList());
		}

		if (null != beanResConsTranSpeiRec && (beanResConsTranSpeiRec.getBeanListDescTipoPago() == null
				|| beanResConsTranSpeiRec.getBeanListDescTipoPago().isEmpty())) {
			beanResConsMotDevolucionDAO = daoRecepOperacion.consultaDevoluciones(session, 4, historico,
					beanReqTranSpeiRec);
			beanResConsTranSpeiRec.setBeanListDescTipoPago(beanResConsMotDevolucionDAO.getBeanMotDevolucionList());
		}

		return beanResConsTranSpeiRec;
	}

	/**
	 * Metodo que se encarga en ejecutar las consultas.
	 *
	 * @param beanReqTranSpeiRec  Objeto del tipo @see BeanReqConsTranSpeiRec
	 * @param sessionBean         Objeto del tipo @see ArchitechSessionBean
	 * @param historico           the historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return BeanResConsTranSpeiRecDAO Bean del tipo BeanResConsTranSpeiRecDAO
	 * @throws BusinessException La business exception
	 */
	private BeanResConsTranSpeiRecDAO ejecutaConsulta(BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico, BeanTranSpeiRecOper beanTranSpeiRecOper)
			throws BusinessException {
		/** Se inicializan las variables **/
		List<Object> parametros = new ArrayList<Object>();
		BeanResConsMiInstDAO beanResConsMiInstDAO = null;
		BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO = null;

		/** Pregunta si viene de la pantalla de historico **/
		if (!historico) {
			/** Consulta la institucion **/
			beanResConsMiInstDAO = daoRecepOperacion
					.consultaMiInstitucion(beanReqTranSpeiRec.getBeanComun().getNombrePantalla(), sessionBean);
			beanReqTranSpeiRec.getBeanComun().setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
			parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
			parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
		} else {
			/** Busca la institucion **/
			beanResConsMiInstDAO = daoRecepOperacion
					.consultaMiInstitucion(beanReqTranSpeiRec.getBeanComun().getNombrePantalla(), sessionBean);
			beanReqTranSpeiRec.getBeanComun().setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
			/** Asigna la institucion y la fecha de operacion **/
			parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
			parametros.add(beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
//			parametros.add(beanReqTranSpeiRec.getBeanComun().getCveMiInstitucion());
//			parametros.add(beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
		}

		if (beanResConsMiInstDAO.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanResConsMiInstDAO.getCodError(), beanResConsMiInstDAO.getMsgError());
		}

		/** Si selecciono todas **/
		beanResConsTranSpeiRecDAO = daoRecepOperacion.consultaTransferencia(beanReqTranSpeiRec, parametros, sessionBean,
				historico, beanTranSpeiRecOper);

		/** Se regresa el objeto **/
		return beanResConsTranSpeiRecDAO;
	}

}