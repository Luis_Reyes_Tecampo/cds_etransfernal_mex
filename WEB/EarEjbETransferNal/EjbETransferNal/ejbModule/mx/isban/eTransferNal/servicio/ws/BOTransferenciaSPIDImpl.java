/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOTransferenciaSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicio.ws;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.constantes.modSPID.ConstantesSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOTransferenciaSPID;
import mx.isban.eTransferNal.servicios.ws.BOTransferenciaSPID;
import mx.isban.eTransferNal.utilerias.HelperSPIDTransferencia;
import mx.isban.eTransferNal.utilerias.comunes.Validate;
import mx.isban.eTransferNal.utilerias.comunes.ValidateFechas;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import net.sf.json.JSONObject;

/**
 * Implementacion de negocio que tiene la funcionalidad de consulta
 * @author cchong
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOTransferenciaSPIDImpl  extends Architech implements BOTransferenciaSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -3482163109970067685L;
	
	/**
	 * Propiedad del tipo DAOTransferenciaSPID que almacena el valor de dAOTransferenciaSPID
	 */
	@EJB
	private DAOTransferenciaSPID dAOTransferenciaSPID;
	
	/**
	 * Propiedad del tipo List<String> que almacena el valor de los campos mandatorios
	 */
	private static List<String> MANDATORIOS = Arrays.asList(new String[]{"CVE_USUARIO_CAP","CVE_USUARIO_SOL","CVE_MEDIO_ENT","CVE_TRANSFE","CVE_DIVISA_ORD","CVE_DIVISA_REC","CVE_DIVISA_ORD","IMPORTE_CARGO","IMPORTE_ABONO","CVE_INTERME_ORD"});

	@Override
	public ResEntradaStringJsonDTO transferencia(
			EntradaStringJsonDTO entradaStringJsonDTO) {
		String nomCampo ="";
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		info("Entro a la llamada transferencia "+entradaStringJsonDTO.getCadenaJson());
		boolean flag = true;
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		Map<String, String> salida = new HashMap<String, String>();
		String trama = null;
		String json = entradaStringJsonDTO.getCadenaJson();
		JSONObject jsonObject = JSONObject.fromString(json);  
		Map<String,String> mapTransferencia =  (Map<String,String>)JSONObject.toBean(jsonObject, Map.class);
		for (int index =0; index<ConstantesSPID.CAMPOS.length && flag;index++){
			flag = validaCampos(mapTransferencia, index);
			nomCampo = ConstantesSPID.CAMPOS[index];
		}
		if(flag){
			HelperSPIDTransferencia helperTransferencia = new HelperSPIDTransferencia();			
			trama = helperTransferencia.generaTrama(mapTransferencia);
			resBeanEjecTranDAO = dAOTransferenciaSPID.ejecutar(trama);
			if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
				String tramaRetorno = resBeanEjecTranDAO.getTramaRespuesta();
				if (tramaRetorno.contains(ConstantesSPID.PIPE)) {
					final String[] valores = tramaRetorno
							.split(ConstantesSPID.DIAGONALES_INVERSA
									.concat(ConstantesSPID.PIPE));
					if(isValidRetorno(valores)){
						returnSalida(valores,salida);
					}
				} else {
					returnCodigo(tramaRetorno,salida);
				}
			}else{
				returnTramaNula(salida);
			}
		}else{
			salida.put(ConstantesSPID.CAMPO_COD_ERROR, "ED00092V");
			salida.put(ConstantesSPID.CAMPO_REFERENCIA,"0" );
			salida.put(ConstantesSPID.CAMPO_ESTATUS, "ER");
			salida.put(ConstantesSPID.CAMPO_DESCRIPCION, "Error en el formato o la longitud del campo:"+nomCampo);	
		}
		jsonObject = JSONObject.fromMap(salida);
		resEntradaStringJsonDTO.setJson(jsonObject.toString());
		info("Salio de la llamada transferencias"+resEntradaStringJsonDTO.getJson());
		
		return resEntradaStringJsonDTO;
	}

	/**
	 * @param mapTransferencia Objeto del tipo Map<String,String>
	 * @param index numero entero
	 * @return boolean true si no hay errores falso en caso contrario
	 */
	public boolean validaCampos(Map<String,String> mapTransferencia, int index){
		int tamanio;
		boolean flag = true;
		String nomCampo ="";
		String tipoCampo = "";
		nomCampo = ConstantesSPID.CAMPOS[index];
		if(!validaMandatorio(mapTransferencia,nomCampo)){
			return false;
		}
		
				
		if(!mapTransferencia.containsKey(nomCampo)){//Si no existe lo inicializo
			tipoCampo = ConstantesSPID.TIPO.get(nomCampo);
			if(!(ConstantesSPID.DECIMAL.equals(tipoCampo) || ConstantesSPID.NUMERICO.equals(tipoCampo))){
				mapTransferencia.put(nomCampo, "");				
			}
		}
		tipoCampo = ConstantesSPID.TIPO.get(nomCampo);
		tamanio = ConstantesSPID.LONGITUD.get(nomCampo);
		if(ConstantesSPID.DECIMAL.equals(tipoCampo) ){
			flag = validaDecimal(mapTransferencia, nomCampo, tamanio);
		}else if(ConstantesSPID.NUMERICO.equals(tipoCampo) ){
			flag = validaEntero(mapTransferencia, nomCampo, tamanio);
		}else if(ConstantesSPID.FECHA.equals(tipoCampo) ){
			flag = validaFecha(mapTransferencia, nomCampo,ValidateFechas.FORMATO_FECHA_DDMMAAAA);	
		}else if(ConstantesSPID.FECHA.equals(tipoCampo) ){
			flag = validaFecha(mapTransferencia, nomCampo,ValidateFechas.FORMATO_FECHA_DDMMAAAA);	
		}else{
			flag = validaCadena(mapTransferencia, nomCampo, tamanio);
		}
		return flag;
	}
	
	/**
	 * Metodo que valida los campos mandatorios
	 * @param mapTransferencia Objeto del tipo Map
	 * @param campo String con el campo del mapa
	 * @return boolean true si viene el campo y trae un valor falso en caso contrario 
	 */
	private boolean validaMandatorio(Map<String,String> mapTransferencia, String campo){
		boolean flag = true;
		if(MANDATORIOS.contains(campo)){
			if(mapTransferencia.containsKey(campo)){
				if(mapTransferencia.get(campo)==null || "".equals(mapTransferencia.get(campo).trim())){
					flag = false;
				}
			}else{
				flag = false;
			}
		}
		return flag;
	}
	
	/**
	 * Metodo que valida los campos Decimal
	 * @param mapTransferencia Objeto del tipo Map
	 * @param campo String con el campo del mapa
	 * @param longitud campo entero con la longitud del campo
	 * @return boolean true si viene el campo y trae un valor falso en caso contrario 
	 */
	private boolean validaDecimal(Map<String,String> mapTransferencia, String campo, int longitud){
		Utilerias utilerias = Utilerias.getUtilerias();
		String formato = "([0-9]{0,16})|([0-9]{0,16}[.]{1}[0-9]{2})";
		Validate validate = new Validate();
		boolean flag = true;
		String value=""; 
		if(!mapTransferencia.containsKey(campo)){
			return flag;
		}
		value = mapTransferencia.get(campo);
		if(value!=null && !"".equals(value.trim())){
			if(ConstantesSPID.TIPO_CAMBIO.equals(campo)){
				
				if(validate.esValidoNumero(value, "([0-9]{0,11})|([0-9]{0,11}[.]{1}[0-9]{1,4})")){
					formato ="([0-9]{0,11})|([0-9]{0,11}[.]{1}[0-9]{4})";
					value = utilerias.formateaDecimales(value,Utilerias.FORMATO_DECIMAL_NUMBER_3);
					mapTransferencia.put(campo, value);
				}else{
					return false;
				}
			}
			value = value.replaceAll(",","");
			flag = validate.esValidoNumero(value, formato);
			if(flag){
				flag = validaLongitud(value, longitud);
			}
		}
		return flag;
	}
	
	/**
	 * Metodo que valida los campos Decimal
	 * @param mapTransferencia Objeto del tipo Map
	 * @param campo String con el campo del mapa
	 * @return boolean true si viene el campo y trae un valor falso en caso contrario 
	 */
	private boolean validaEntero(Map<String,String> mapTransferencia, String campo, int longitud){
		Validate validate = new Validate();
		boolean flag = true;
		String value=""; 
		if(!mapTransferencia.containsKey(campo)){
			return flag;
		}
		value = mapTransferencia.get(campo);
		if(value!=null && !"".equals(value.trim())){
			flag = validate.esValidoNumero(value, "([0-9]{0,})");
			if(flag){
				flag = validaLongitud(value, longitud);
			}
		}
		return flag;
	}
	
	
	/**
	 * Metodo que valida la fecha
	 * @param mapTransferencia Objeto del tipo Map
	 * @param campo String con el campo del mapa
	 * @param formato String con el formato
	 * @return boolean true si viene el campo y trae un valor falso en caso contrario 
	 */
	private boolean validaFecha(Map<String,String> mapTransferencia, String campo, String formato){
		ValidateFechas validate = new ValidateFechas();
		boolean flag = true;
		String value=""; 
		if(!mapTransferencia.containsKey(campo)){
			return flag;
		}
		value = mapTransferencia.get(campo);
		if(value!=null && !"".equals(value.trim())){
			flag = validate.esValidoFechaVacia(value,formato);
		}
		return flag;
	}
	
	/**
	 * Metodo que valida la fecha
	 * @param mapTransferencia Objeto del tipo Map
	 * @param campo String con el campo del mapa
	 * @param longitud int con ola longitud
	 * @return boolean true si viene el campo y trae un valor falso en caso contrario 
	 */
	private boolean validaCadena(Map<String,String> mapTransferencia, String campo, int longitud){
		boolean flag = true;
		String value=""; 
		if(!mapTransferencia.containsKey(campo)){
			return flag;
		}
		value = mapTransferencia.get(campo);
		if(value!=null && !"".equals(value.trim())){
			flag = validaLongitud(value, longitud);
		}
		return flag;
	}
	
	/**
	 * llenado cuando la trama es nula
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnTramaNula(Map<String, String> salida) {
		salida.put(ConstantesSPID.CAMPO_COD_ERROR,Errores.EC00011B);
		salida.put(ConstantesSPID.CAMPO_REFERENCIA,ConstantesSPID.CERO);
		salida.put(ConstantesSPID.CAMPO_ESTATUS,"ER");
		salida.put(ConstantesSPID.CAMPO_DESCRIPCION,ConstantesSPID.ERROR_IDA);
	}

	
	/**
	 * @param tramaRetorno trama de retorno de ida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnCodigo(final String tramaRetorno,Map<String, String> salida) {
		salida.put(ConstantesSPID.CAMPO_COD_ERROR, tramaRetorno.substring(0, 8));
		debug("COD_ERROR " + tramaRetorno.substring(0, 8));
		salida.put(ConstantesSPID.CAMPO_DESCRIPCION, tramaRetorno.substring(8, 78));
		debug("DESCRIPCION " + tramaRetorno.substring(8, 78));
		salida.put(ConstantesSPID.CAMPO_REFERENCIA, tramaRetorno.substring(78, 85));
		debug("REFERENCIA " + tramaRetorno.substring(78, 85));
		salida.put(ConstantesSPID.CVE_RASTREO, tramaRetorno.substring(85, 115));
		debug("RASTREO " + tramaRetorno.substring(85, 115));
		salida.put(ConstantesSPID.CAMPO_ESTATUS, tramaRetorno.substring(115, 117));
		debug("ESTATUS " + tramaRetorno.substring(115, 117));
		salida.put(ConstantesSPID.CAMPO_DESCRIPCION_ESTATUS, tramaRetorno.substring(117, 137));
		debug("DESCRIPCION ESTATUS " + tramaRetorno.substring(117, 137));

	}
	

	
	/**
	 * @param valores para validar la trama de retorno
	 * @return boolean true si es valido falso en caso contrario
	 */
	private boolean isValidRetorno(final String[] valores) {
		boolean result = true;
		if (valores.length < 6) {
			result = false;
		}
		if (!validaLongitud(valores[0], ConstantesSPID.NUM_8)) {
			result = false;
		}
		if (!validaLongitud(valores[1], ConstantesSPID.NUM_70)) {
			result = false;
		}
		if (!validaLongitud(valores[2], ConstantesSPID.NUM_7)) {
			result = false;
		}
		if (!validaLongitud(valores[3], ConstantesSPID.NUM_30)) {
			result = false;
		}
		if (!validaLongitud(valores[4], ConstantesSPID.NUM_2)) {
			result = false;
		}
		if (!validaLongitud(valores[5], ConstantesSPID.NUM_20)) {
			result = false;
		}
		return result; 
	}
	
	/**
	 * @param valores para retornar trama de salida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnSalida(final String[] valores, Map<String, String> salida) {
		salida.put(ConstantesSPID.CAMPO_COD_ERROR, valores[0]);
		debug("COD_ERROR " + valores[0]);
		salida.put(ConstantesSPID.CAMPO_REFERENCIA, valores[2]);
		debug("REFERENCIA " + valores[2]);
		salida.put(ConstantesSPID.CAMPO_ESTATUS, valores[4]);
		debug("ESTATUS " + valores[4]);
		salida.put(ConstantesSPID.CVE_RASTREO, valores[3]);
		debug("RASTREO " + valores[4]);
		salida.put(ConstantesSPID.CAMPO_DESCRIPCION_ESTATUS, valores[3]);
		debug("DESCRIPCION ESTATUS " + valores[4]);
		salida.put(ConstantesSPID.CAMPO_DESCRIPCION, valores[1]);
		debug("DESCRIPCION " + valores[1]);

	}
	
	/**
	 * @param cadena valor del campo
	 * @param numero longitud maxima
	 * @return boolean true si es correcta la longitud false en caso contrario
	 */
	private boolean validaLongitud(final String cadena, final int numero) {
		if (cadena.length() > numero) {
			return false;
		}
		return true;
	}

	
	/**
	 * Metodo get que sirve para obtener la propiedad dAOTransferenciaSPID
	 * @return dAOTransferenciaSPID Objeto del tipo DAOTransferenciaSPID
	 */
	public DAOTransferenciaSPID getDAOTransferenciaSPID() {
		return dAOTransferenciaSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOTransferenciaSPID
	 * @param transferenciaSPID del tipo DAOTransferenciaSPID
	 */
	public void setDAOTransferenciaSPID(DAOTransferenciaSPID transferenciaSPID) {
		dAOTransferenciaSPID = transferenciaSPID;
	}
	
	

}
