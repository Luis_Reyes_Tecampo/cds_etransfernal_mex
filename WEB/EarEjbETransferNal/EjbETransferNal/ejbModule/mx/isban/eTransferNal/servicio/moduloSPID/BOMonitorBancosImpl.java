/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOMonitorBancosImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMonitorBancosDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorBancos;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOMonitorBancos;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio de Monitor Bancos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)

public class BOMonitorBancosImpl extends Architech implements BOMonitorBancos{
	
	 /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -7504952116543855241L;
	
	/** Referencia privada al dao  */
	@EJB
	private DAOMonitorBancos dAOMonitorBancos;

	
	/**
	 * Metodo que sirve para consultar los registros de Bancos
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param sortField
	 *            Objeto del tipo String
	 * @param sortType
	 *            Objeto del tipo String
	 * @return beanResMonitorBancos Objeto del tipo
	 *         BeanResMonitorBancos
	 * @exception BusinessExceptionException de negocio         
	 */
	@Override
	public BeanResMonitorBancos obtenerMonitorBancos( BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException {
		BeanMonitorBancosDAO beanMonitorBancosDAO = null;
		final BeanResMonitorBancos beanResMonitorBancos = new BeanResMonitorBancos();
		
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		
		String sF = getSortField(sortField);
		
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());		
		beanMonitorBancosDAO = dAOMonitorBancos.obtenerMonitorBancos(pag, architechSessionBean, sF, sortType);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanMonitorBancosDAO.getCodError()) 
				&& beanMonitorBancosDAO.getListaMonitorBancos().isEmpty()) {
			beanResMonitorBancos.setCodError(Errores.ED00011V);
			beanResMonitorBancos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResMonitorBancos.setListaMonitorBancos(beanMonitorBancosDAO.getListaMonitorBancos());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanMonitorBancosDAO.getCodError())){
			beanResMonitorBancos.setListaMonitorBancos(beanMonitorBancosDAO.getListaMonitorBancos());          
			beanResMonitorBancos.setTotalReg(beanMonitorBancosDAO.getTotalReg());
			pag.calculaPaginas(beanMonitorBancosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResMonitorBancos.setBeanPaginador(pag);
			beanResMonitorBancos.setCodError(Errores.OK00000V);
		}else{
			beanResMonitorBancos.setCodError(Errores.EC00011B);
			beanResMonitorBancos.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResMonitorBancos;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("cveBanco".equals(sortField)){
			sF = "CVE_BANCO";
		} else if ("cveInterme".equals(sortField)){
			sF = "CVE_INTERME";
		} else if ("nombreBanco".equals(sortField)){
			sF = "NOMBRE_BANCO";
		} else if ("estadoInstit".equals(sortField)){
			sF = "ESTADO_INSTIT";
		} else if ("estadoRecep".equals(sortField)){
			sF = "ESTADO_RECEP";
		}
		return sF;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOMonitorBancos
	 * 
	 * @return dAOMonitorBancos Objeto de tipo @see DAOMonitorBancos
	 */
public DAOMonitorBancos getDAOMonitorBancos() {
		return dAOMonitorBancos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad dAOMonitorBancos
	 * 
	 * @param dAOMonitorBancos
	 *            Objeto de tipo @see DAOMonitorBancos
	 */
	public void setDAOMonitorBancos(DAOMonitorBancos dAOMonitorBancos) {
		this.dAOMonitorBancos = dAOMonitorBancos;
	}
}
