/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOEquivalenciaImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalenciaDAO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveOperacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalencia;

import mx.isban.eTransferNal.beans.moduloSPID.BeanTipoPago;

import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.moduloSPID.DAOEquivalencia;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 *Clase del tipo BO que se encarga  del negocio de
 *la configuracion de equivalencias
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOEquivalenciaImpl implements BOEquivalencia {
	
	/**
	 * 
	 */
	private static final String VALUES_BIT_FORMAT = "CVE_OPERACION=%s|TIPO_PAGO=%s|TIPO_O_CLASIF_OPER=%s" +
			"|DESCRIPCION=%s|HORARIO=%s";
	
	/**
	 * 
	 */
	private static final String DATO_FIJO_BIT = "CVE_OPERACION";
	
	/**
	 * 
	 */
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;
	
	/** Referencia privada al dao  */
	@EJB
	private DAOEquivalencia daoEquivalencia;

	@Override
	public BeanResEquivalencia listarEquivalencia(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException {
		BeanResEquivalencia beanResEquivalencia = new BeanResEquivalencia();
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = null;
		
		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String sF = getSortField(sortField);
		
		beanResEquivalenciaDAO = daoEquivalencia.listarEquivalencias(beanPaginador, architechSessionBean, sF, sortType);
		
		if(Errores.CODE_SUCCESFULLY.equals(beanResEquivalenciaDAO.getCodError()) && 
				!beanResEquivalenciaDAO.getBeanEquivalencias().isEmpty()) {
			beanResEquivalencia.setTotalReg(beanResEquivalenciaDAO.getTotalReg());
			beanResEquivalencia.setBeanEquivalencias(beanResEquivalenciaDAO.getBeanEquivalencias());
			beanPaginador.calculaPaginas(beanResEquivalenciaDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResEquivalencia.setBeanPaginador(beanPaginador);
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResEquivalenciaDAO.getCodError())) {
			beanResEquivalencia.setCodError(Errores.ED00011V);
			beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		return beanResEquivalencia;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("claveOper".equals(sortField)){
			sF = "CVE_OPERACION";
		} else if ("tipoPago".equals(sortField)){
			sF = "TIPO_PAGO";
		} else if ("clasificacion".equals(sortField)){
			sF = "TIPO_O_CLASIF_OPER";
		} else if ("descripcion".equals(sortField)){
			sF = "DESCRIPCION";
		} else if ("horario".equals(sortField)){
			sF = "HORARIO";
		}
		return sF;
	}
	
	@Override
	public BeanResEquivalencia guardarEquivalencia(BeanEquivalencia beanEquivalencia,
			ArchitechSessionBean architechSessionBean, BeanPaginador beanPaginador) throws BusinessException {
		BeanResEquivalencia beanResEquivalencia = null;
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = 
				daoEquivalencia.crearEquivalencia(beanEquivalencia, architechSessionBean);
		
		String valNvo = String.format(VALUES_BIT_FORMAT, 
				beanEquivalencia.getClaveOper(),
				beanEquivalencia.getTipoPago(),
				beanEquivalencia.getClasificacion(),
				beanEquivalencia.getDescripcion(),
				beanEquivalencia.getHorario());
		
		if(Errores.CODE_SUCCESFULLY.equals(beanResEquivalenciaDAO.getCodError())) {
			setBitacoraTrans("OK", "TRAN_SPID_EQ", "INSERT", "IntGuardarEquivalencia.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechSessionBean);
			beanResEquivalencia = listarEquivalencia(beanPaginador, architechSessionBean, "", "");
			beanResEquivalencia.setCodError(Errores.OK00000V);
			beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_INFO);
		} else{
			setBitacoraTrans("NOK", "TRAN_SPID_EQ", "INSERT", "IntGuardarEquivalencia.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechSessionBean);
			beanResEquivalencia = listarEquivalencia(beanPaginador, architechSessionBean, "", "");			
			beanResEquivalencia.setCodError(Errores.ED00061V);
			beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		return beanResEquivalencia;
	}
	
	@Override
	public BeanResEquivalencia eliminarEquivalencia(BeanReqEquivalencia beanReqEquivalencia,
			ArchitechSessionBean architechSessionBean, BeanPaginador beanPaginador) throws BusinessException {
		BeanResEquivalencia beanResEquivalencia = null;
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = null;
		
		try {
			List<BeanEquivalencia> listaEquivalencias = filtrarSeleccionados(beanReqEquivalencia);
			String valAnt = "";
			
			for(BeanEquivalencia equivalencia: listaEquivalencias) {
				valAnt = String.format(VALUES_BIT_FORMAT, 
						equivalencia.getClaveOper(),
						equivalencia.getTipoPago(),
						equivalencia.getClasificacion(),
						equivalencia.getDescripcion(),
						equivalencia.getHorario());
				
				beanResEquivalenciaDAO = 
						daoEquivalencia.eliminarEquivalencia(equivalencia, beanPaginador, architechSessionBean);
				if(!Errores.CODE_SUCCESFULLY.equals(beanResEquivalenciaDAO.getCodError())) {
					setBitacoraTrans("NOK", "TRAN_SPID_EQ", "DELETE", "IntEliminarEquivalencia.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechSessionBean);
					beanResEquivalencia = listarEquivalencia(beanPaginador, architechSessionBean, "", "");
					beanResEquivalencia.setCodError(Errores.EC00011B);
					beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_ERROR);
					return beanResEquivalencia;
				}				
			}
			setBitacoraTrans("OK", "TRAN_SPID_EQ", "DELETE", "IntEliminarEquivalencia.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechSessionBean);
			beanResEquivalencia = listarEquivalencia(beanPaginador, architechSessionBean, "", "");
			beanResEquivalencia.setCodError(Errores.OK00000V);
			beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_INFO);
		} catch(BusinessException e) {
			beanResEquivalencia = listarEquivalencia(beanPaginador, architechSessionBean, "", "");
			beanResEquivalencia.setCodError(Errores.EC00011B);
			beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_ERROR);
			
			throw e;
		}
		
		return beanResEquivalencia;
	}

	@Override
	public BeanResEquivalencia guardarEdicionEquivalencia(BeanEquivalencia beanEquivalencia, ArchitechSessionBean architechBean,
			BeanPaginador beanPaginador, String cveOperacion, String tipoPago, String clasificacion) throws BusinessException {
		BeanResEquivalenciaDAO beanResEquivalenciaDAO = null;
		BeanResEquivalencia beanResEquivalencia = new BeanResEquivalencia();
		
		try {
			beanResEquivalenciaDAO = daoEquivalencia.obtenerEquivalencia(cveOperacion, architechBean);
			BeanEquivalencia beanEquivalenciaAnt = beanResEquivalenciaDAO.getBeanEquivalencias().get(0);
			String valAnt = String.format(VALUES_BIT_FORMAT, 
					beanEquivalenciaAnt.getClaveOper(),
					beanEquivalenciaAnt.getTipoPago(),
					beanEquivalenciaAnt.getClasificacion(),
					beanEquivalenciaAnt.getDescripcion(),
					beanEquivalenciaAnt.getHorario());
			
			beanResEquivalenciaDAO = daoEquivalencia.
					editarEquivalencia(beanEquivalencia, beanPaginador, architechBean, cveOperacion, tipoPago, clasificacion);
				
				if(Errores.CODE_SUCCESFULLY.equals(beanResEquivalenciaDAO.getCodError())){
					String valNvo = String.format(VALUES_BIT_FORMAT, 
							beanEquivalencia.getClaveOper(),
							beanEquivalencia.getTipoPago(),
							beanEquivalencia.getClasificacion(),
							beanEquivalencia.getDescripcion(),
							beanEquivalencia.getHorario());
					
					setBitacoraTrans("OK", "TRAN_SPID_EQ", "UPDATE", "IntGuardarEdicionEquivalencia.do", valAnt, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResEquivalencia = listarEquivalencia(beanPaginador, architechBean, "", "");
					beanResEquivalencia.setCodError(Errores.OK00000V);
					beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_INFO);
				}else{
					setBitacoraTrans("NOK", "TRAN_SPID_EQ", "UPDATE", "IntGuardarEdicionEquivalencia.do", valAnt, "", "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResEquivalencia = listarEquivalencia(beanPaginador, architechBean, "", "");
					beanResEquivalencia.setCodError(Errores.ED00061V);
					beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_ERROR);
				}
		
		} catch (BusinessException e) {
			beanResEquivalencia = listarEquivalencia(beanPaginador, architechBean, "", "");
			throw e;
		}
		
		return beanResEquivalencia;
	}
	
	@Override
	public BeanResEquivalencia generarCombosEquivalencia() {
		BeanResEquivalencia beanResEquivalencia = new BeanResEquivalencia();
		List<BeanClaveOperacion> listaClaveOperacion = null;
		List<BeanTipoPago> listaTipoPago = null;
		
		listaClaveOperacion = daoEquivalencia.obtenerListaClaveOperacion();
		beanResEquivalencia.setListaClaveOperacion(listaClaveOperacion);

		listaTipoPago = crarListaTipoPago();
		beanResEquivalencia.setListaTipoPago(listaTipoPago);
		beanResEquivalencia.setCodError(Errores.OK00000V);
		beanResEquivalencia.setTipoError(Errores.TIPO_MSJ_INFO);
		return beanResEquivalencia;
	}
	
	/**
	 * Metodo get que obtiene el daoEquivalencia
	 * 
	 * @return DAOEquivalencia objeto con daoEquivalencia
	 */
	public DAOEquivalencia getDaoEquivalencia() {
		return daoEquivalencia;
	}

	/**
	 * Metodo set que sirve para setear el daoEquivalencia
	 * 
	 * @param daoEquivalencia
	 *            el DAOEquivalencia
	 */
	public void setDaoEquivalencia(DAOEquivalencia daoEquivalencia) {
		this.daoEquivalencia = daoEquivalencia;
	}
	
	/**M�todo que crea la lista de pagos
	 * @return listaTipoPago 
	 */
	public List<BeanTipoPago> crarListaTipoPago() {
		List<BeanTipoPago> listaTipoPago = new ArrayList<BeanTipoPago>();
		
		listaTipoPago.add(new BeanTipoPago(1,"Devoluci\u00f3n"));
		listaTipoPago.add(new BeanTipoPago(2, "Tercero a Tercero"));
		listaTipoPago.add(new BeanTipoPago(3, "Tercero a Ventanilla"));
		listaTipoPago.add(new BeanTipoPago(4, "Tercero a Tercero Vostro"));
		listaTipoPago.add(new BeanTipoPago(5, "Tercero a Banco"));
		listaTipoPago.add(new BeanTipoPago(6, "Banco a Tercero"));
		listaTipoPago.add(new BeanTipoPago(7, "Banco a Tercero Vostro"));
		listaTipoPago.add(new BeanTipoPago(8, "Banco a Banco"));
		listaTipoPago.add(new BeanTipoPago(9, "Tercero a Tercero FSW"));
		listaTipoPago.add(new BeanTipoPago(10, "Tercero a Tercero Vostro FSW"));
		listaTipoPago.add(new BeanTipoPago(11, "Banco a Tercero FSW"));
		listaTipoPago.add(new BeanTipoPago(12, "Tercero a N\u00f3mina"));
		
		return listaTipoPago;
	}
	
	/**
	 * @param beanReqEquivalencia Objeto del tipo BeanReqEquivalencia
	 * @return listaSeleccionados Objeto del tipo List<BeanEquivalencia>
	 */
	private List<BeanEquivalencia> filtrarSeleccionados(BeanReqEquivalencia beanReqEquivalencia) {
		List<BeanEquivalencia> listaSeleccionados = new ArrayList<BeanEquivalencia>();
		
		for (BeanEquivalencia beanEquivalencia : beanReqEquivalencia.getListaEquivalencias()) {
			if (beanEquivalencia.isSeleccionado()){
				listaSeleccionados.add(beanEquivalencia);
			}
		}
		
		return listaSeleccionados;
	}
	
	/**
     * @param codOper Objeto del tipo String
     * @param tablaAfect Objeto del tipo String
     * @param tipoOper Objeto del tipo String
     * @param servTran Objeto del tipo String
     * @param valAnt Objeto del tipo String
     * @param valNvo Objeto del tipo String
     * @param datoModi Objeto del tipo String
     * @param datoFijo Objeto del tipo String
     * @param comentarios Objeto del tipo String
     * @param sesion Objeto del tipo String
     */
    private void setBitacoraTrans(String codOper, String tablaAfect, String tipoOper, String servTran, String valAnt,
		String valNvo, String datoModi, String datoFijo, String comentarios
		, ArchitechSessionBean sesion){
    	
    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
    		utileriasBitAdmin.createBitacoraBitAdmin(codOper, tablaAfect, tipoOper, servTran, valAnt, valNvo, 
				datoModi, datoFijo, comentarios, sesion);
    	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, sesion);
    }
	
	/**
	 * @return the daoBitAdministrativa
	 */
	public DAOBitAdministrativa getDaoBitAdministrativa() {
		return daoBitAdministrativa;
	}

	/**
	 * @param daoBitAdministrativa the daoBitAdministrativa to set
	 */
	public void setDaoBitAdministrativa(DAOBitAdministrativa daoBitAdministrativa) {
		this.daoBitAdministrativa = daoBitAdministrativa;
	}
}
