/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOConsultaOperContingentesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   25/10/2018 05:37:07 PM Ana Gloria Martinez Rivera . VectorFSW Creacion
 */

package mx.isban.eTransferNal.servicio.contingencia;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.contingencia.BeanArchivoDuplicado;
import mx.isban.eTransferNal.beans.contingencia.BeanConsultaOperContingentes;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConFecha;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanalesD;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.contingencia.DAOConsultaOperContingentes;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;


// clase para las consultas de contingencias.
/**
 * Class BOConsultaOperContingentesImpl.
 *
 * @author FSW-Vector
 * @since 25/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaOperContingentesImpl implements BOConsultaOperContingentes {
	/**Instancia del Dao para consulta Operaciones Contingentes.*/
	@EJB
	private DAOConsultaOperContingentes daoConsultaOper;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante OK. */
	public static final String OK = "OK";

	/** La constante NOK. */
	public static final String NOK = "NOK";
	
	/** La constante TYPE_INSERT. */
	public static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	public static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_DELETE. */
	public static final String TYPE_SELECTE = "SELECT";
	
	/** La constante DATO_FIJO. */
	public static final String DATO_FIJO = "IDFOLIO";
	
	/**
	 * Consultar oper contingentes.
	 *
	 * @param architectSessionBean El objeto: architect session bean
	 * @param beanPaginador El objeto: bean paginador
	 * @param bean El objeto: bean
	 * @return Objeto bean res consulta oper contingente
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResConsultaOperContingente consultarOperContingentes(ArchitechSessionBean architectSessionBean,BeanPaginador beanPaginador,BeanReqConsultaOperContingente bean)throws BusinessException{
		/**Instancia del objeto de respuesta de la consulta de operaciones contingentes.*/
		BeanResConsultaOperContingente beanreturn = new BeanResConsultaOperContingente();
		/**Inicia llamdo a la consulta de operaciones contingentes.*/
		beanreturn = daoConsultaOper.consultaOperContingentes(architectSessionBean,beanPaginador,  bean);
		String datos="todos";
		String controller ="muestraOperacionesContingentes.do";
		if(bean.isHistorico()) {
			 datos="todos los de la fecha: "+bean.getFechaOperacion();
			 controller ="buscarOperacionContinHto.do";
		}
		
		String datosAct =datos+ ","+architectSessionBean.getUsuario();
		String dato = TYPE_SELECTE+"/"+controller+"/"+datosAct;
		
		BeanPistaAuditora pistas = null;
		if(beanreturn.getBeanbase().getCodError().equals(Errores.EC00011B)) {
			pistas = generaPistaAuditoras(dato, NOK, architectSessionBean, bean.getNomPantalla());
			throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}else {
			pistas = generaPistaAuditoras(dato, OK, architectSessionBean, bean.getNomPantalla());
		}
		/**inserta en bitacora **/
		boPistaAuditora.llenaPistaAuditora(pistas, architectSessionBean);
		
		return beanreturn;
	}
	
	/**
	 * Carga archivo.
	 *
	 * @param beanResCanales the bean res canales
	 * @param session the session
	 * @return the bean consulta oper contingentes
	 */
	@Override
	public BeanResConsCanales cargaArchivo(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session,
			BeanReqConsultaOperContingente bean) throws BusinessException {
		/**Inicia llamado a la consulta de canales.*/
		BeanResConsCanalesD beanCanalesD =daoConsultaOper.consultarCanal(session);
		/**Inicia el llamado a consultar fecha de operacion.*/
		BeanResConFecha beanConFechas = daoConsultaOper.consultaFechaOperacion(session);
		beanArchivo.setFchOperacion(beanConFechas.getFechaOperacion());
		BeanResConsCanales beanConsCanales = new BeanResConsCanales();
		beanConsCanales.setBeanResCanalList(beanCanalesD.getBeanResCanalList());
		/**Instancia para el objeto de respuest .*/
		BeanResBase res = new BeanResBase();
		/**Instancia para el bean valida duplicado .*/
		BeanArchivoDuplicado beanDuplicado = validaNombreDuplicado(beanArchivo, session,  bean);
		
		String datosAct =beanArchivo.getNombreArchivo()+ ","+beanArchivo.getEstatus()+ ","+beanArchivo.getNumPagos()
		+ ","+beanArchivo.getTotalMonto()+ ","+beanArchivo.getTotalMontoErr()+ ","+beanArchivo.getCveUsuarioAlta()
		+ ","+beanArchivo.getFchCaptura()+ ","+beanArchivo.getFchProceso()+ ","+beanArchivo.getMensaje()+ ","+beanArchivo.getCveMedioEnt()
		+ ","+beanArchivo.getDescripcion()+ ","+beanArchivo.getFchOperacion()+ ","+session.getUsuario();
		String dato = TYPE_INSERT+"/cargaArchivoOperaciones.do/"+datosAct;
		BeanPistaAuditora pistas = null;
		
		if("OK00000V".equals(beanDuplicado.getCodError())){
			if(!beanDuplicado.isEsDuplicado()){
				    res = daoConsultaOper.registraArchivo(beanArchivo,session,bean);
			        res.setCodError(Errores.OK00000V);
			        res.setTipoError(Errores.TIPO_MSJ_INFO);
			        beanConsCanales.setBeanBaseError(res);
			        pistas = generaPistaAuditoras(dato, OK, session, bean.getNomPantalla());
			        /**inserta en bitacora **/
					boPistaAuditora.llenaPistaAuditora(pistas, session);
		      }else{		    	 
			    	  /**Control de errores.*/
			    	  res.setCodError(Errores.CD00173V);
			    	  res.setTipoError(Errores.TIPO_MSJ_INFO);
			    	  beanConsCanales.setBeanBaseError(res);
			    	 
			  }
		}else {
			res.setCodError(beanDuplicado.getCodError());
			res.setTipoError(Errores.TIPO_MSJ_ERROR);
			pistas = generaPistaAuditoras(dato, NOK, session, bean.getNomPantalla());
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditora(pistas, session);
			throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
		
		
		
		/**Bean de respuesta .*/
		return beanConsCanales;
	}

	/**
	 * Obtener medio entrega.
	 *
	 * @param session the session
	 * @return the bean res cons canales
	 */
	@Override
	public BeanResConsCanales obtenerMedioEntrega(ArchitechSessionBean session)  throws BusinessException  {
		BeanResConsCanalesD beanResConsCanalesDAO = daoConsultaOper.consultarCanal(session);
		BeanResConsCanales beanResConsCanales = new BeanResConsCanales();
		beanResConsCanales.setBeanResCanalList(beanResConsCanalesDAO.getBeanResCanalList());
		return beanResConsCanales;
	}
	
	
	/**
	 * Actualiza estatus.
	 *
	 * @param beanResCanales the bean res canales
	 * @param session the session
	 * @return the bean res cons canales
	 */
	@Override 
	public BeanResConsCanales actualizaEstatus(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session,
			BeanReqConsultaOperContingente bean)  throws BusinessException {
		BeanResConsCanales beanResponse = new BeanResConsCanales();
		BeanResBase res = null;
		res = daoConsultaOper.actualizaEstatus(beanArchivo, session,bean);
		
		/**genera datos para las pistas de auditoria **/
		String datosAct =beanArchivo.getNombreArchivo()+ ","+beanArchivo.getEstatus()+","+session.getUsuario();
		String dato = TYPE_UPDATE+"/actualizaEstatus.do/"+datosAct;
		BeanPistaAuditora pistas = null;
		
		/**Validacion de errores.*/
		if(Errores.OK00000V.equals(res.getCodError())){			
			res.setTipoError(Errores.TIPO_MSJ_INFO);		
			pistas = generaPistaAuditoras(dato, OK, session, bean.getNomPantalla());
			
		}else{
			/**Control de errores.*/
			res.setTipoError(Errores.TIPO_MSJ_ERROR);			
			pistas = generaPistaAuditoras(dato, NOK, session, bean.getNomPantalla());
			throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
		}
		
		/**inserta en bitacora **/
		boPistaAuditora.llenaPistaAuditora(pistas, session);
		
		beanResponse.setBeanBaseError(res);
		return beanResponse;
		
	}
	
	/**
	 * Metodo para actualizar el nombre del archivo que ya existe .
	 *
	 * @param beanResCanales El objeto: bean res canales
	 * @param session El objeto: session
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto bean res cons canales
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResConsCanales actualizarNombreArchProcesar(BeanConsultaOperContingentes beanResCanales,
			ArchitechSessionBean session, String nomPantalla)  throws BusinessException {
		/**Instancia del bean de respuesta de operaciones contingentes .*/
		BeanConsultaOperContingentes beanRes = new BeanConsultaOperContingentes();
		/**Instancia de bean de respuesta de actualiza nombre.*/
		BeanResBase res = new BeanResBase();
		/**Instancia de bean para entrada .*/	
		BeanResConsCanales beanResConsCanales = new BeanResConsCanales();
		/**Instancia de bean de respuesta para consulta de fecha.*/
		BeanResConFecha beanConFechas = daoConsultaOper.consultaFechaOperacion(session);
		beanRes.setFchOperacion(beanConFechas.getFechaOperacion());
		beanRes.setNombreArchivo(beanResCanales.getNombreArchivo());
		/**Inicia peticion actualiza nombre.*/
		res = daoConsultaOper.actualizaNombre(beanRes,	session);
		
		String datosAct = beanResCanales.getNombreArchivo()+","+session.getUsuario();
		String dato = TYPE_UPDATE+"/actualizarNombreArchivo.do/"+datosAct;
		
		BeanPistaAuditora pistas = null;
		
		/**Validacion de errores.*/
		if(Errores.OK00000V.equals(res.getCodError())){
			res.setCodError(Errores.OK00000V);
			res.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsCanales.setBeanBaseError(res);
			pistas = generaPistaAuditoras(dato, OK, session, nomPantalla);
			
		}else{
			/**Control de errores.*/
			res.setCodError(Errores.ED00074V);
			res.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResConsCanales.setBeanBaseError(res);
			pistas = generaPistaAuditoras(dato, NOK, session, nomPantalla);
		}
		
		/**inserta en bitacora **/
		boPistaAuditora.llenaPistaAuditora(pistas, session);
		
		return beanResConsCanales;
	}

	
	/**
	 * Metodo que valida el nombre del archivo.
	 *
	 * @param beanReqArchPago El objeto: bean req arch pago
	 * @param architectSessionBean El objeto: architect session bean
	 * @param bean El objeto: bean
	 * @return Objeto bean archivo duplicado
	 * @throws BusinessException La business exception
	 */
	private BeanArchivoDuplicado validaNombreDuplicado(BeanConsultaOperContingentes beanReqArchPago,
			ArchitechSessionBean architectSessionBean,BeanReqConsultaOperContingente bean)  throws BusinessException {
		BeanArchivoDuplicado beanResDuplicadoArchDAO = null;
		/**Inicia petecion para la validaci󮠤el nombre del archivo .*/
		beanResDuplicadoArchDAO = daoConsultaOper.validaArchivoDupl(beanReqArchPago, architectSessionBean, bean);
		return beanResDuplicadoArchDAO;
	}

	/**
	 * Genera pista auditoras.
	 *
	 * @param datos El objeto: datos
	 * @param resOp El objeto: res op
	 * @param sesion El objeto: sesion
	 * @param pantalla El objeto: pantalla
	 * @return Objeto bean pista auditora
	 */
    public BeanPistaAuditora generaPistaAuditoras(String datos, String resOp, ArchitechSessionBean sesion,
    		String pantalla) {
		String[] dato= datos.split("/");
		String tabla="";
		/**pregunta hitorico para hacer el insert en la tabla correspondiente**/
		if(("normal").equals(pantalla)) {
			/**tabla apartados historicos**/
			tabla = "TRAN_ARCH_CANALES";
		}else {		
			/**tabla apartados del dia**/
			tabla = "TRAN_ARCH_CANALES_HIS";
		}
		
		/**Objetos para las Pistas auditoras**/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Enviamos los datos de la pista auditora**/
		beanPista.setNomTabla(tabla);
		beanPista.setOperacion(dato[0]);
		beanPista.setUrlController(dato[1]);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dato[2]);
		
		beanPista.setEstatus(resOp);
		
		/**regresa la pistas de auditoria**/
		return beanPista;
	}
    
}