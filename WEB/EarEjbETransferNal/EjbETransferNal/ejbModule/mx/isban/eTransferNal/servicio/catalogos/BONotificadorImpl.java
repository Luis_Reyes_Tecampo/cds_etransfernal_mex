/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BONotificadorImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/07/2019 12:33:35 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificaciones;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesCombo;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAONotificador;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasNotificador;

/**
 * Class BONotificadorImpl.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo notificador de estatus
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BONotificadorImpl extends Architech implements BONotificador{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5062718238060758791L;

	/** La variable que contiene informacion con respecto a: dao notificador. */
	@EJB
	private DAONotificador daoNotificador;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante TYPE_INSERT. */
	private static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	private static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_DELETE. */
	private static final String TYPE_DELETE = "DELETE";
	
	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "NA";
	
	/** La constante INSERT. */
	private static final String INSERT = "TIPO_NOTIF,CVE_TRANSFER,MEDIO_ENTREGA,ESTATUS_TRANSFER";
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_MX_CAT_NOTIF";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante SELECT. */
	private static final String SELECT = "TIPO_NOTIF,CVE_TRANSFER,MEDIO_ENTREGA,ESTATUS_TRANSFER";
	
	/** La constante UPDATE. */
	private static final String UPDATE = "TIPO_NOTIF,CVE_TRANSFER,MEDIO_ENTREGA,ESTATUS_TRANSFER";
	
	
	
	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanNotificacion -->  objeto que almacena las notificaciones
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador -->  bean que permite paginar
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanNotificaciones consultar(BeanNotificacion beanNotificacion, BeanPaginador beanPaginador,
			ArchitechSessionBean session) throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		/** Validacion de paginador **/
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanNotificaciones response = daoNotificador.consultar(beanNotificacion, beanPaginador, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getNotificaciones().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * consultar listados de notificaciones
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param tipo -->  variable de tipo int que trae el tipo de listado
	 * @param filter -->  objeto de tipo BeanNotificacion que trae el filtro
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return listas -->  regresa la lista de los registros
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanNotificacionesCombo consultarListas(int tipo, BeanNotificacion filter, ArchitechSessionBean session) throws BusinessException {
		/** Retorno de la respuesta directa de DAO **/
		return daoNotificador.consultarListas(tipo, filter, session);
	}
	
	/**
	 * agregar notificacion
	 * 
	 * Metodo publico utilizado para agregar notificaciones
	 *
	 *@param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion -->  bean que obtiene la notificacion
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase agregar(BeanNotificacion beanNotificacion, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = false;
		/** Ejecucion de la peticion al DAO **/
		BeanResBase response = daoNotificador.agregar(beanNotificacion, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(beanNotificacion);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_INSERT, "agregarNotificador.do", operacion, session,dtoNuevo,"",INSERT);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * editar notificacion
	 * 
	 * Metodo publico utilizado para editar notificaciones
	 *
	 * @param beanNotificacion -->  objeto que trae notificaciones 	
	 * @param anterior -->  objeto que trae los registros anteriores
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase editar(BeanNotificacion beanNotificacion, BeanNotificacion anterior, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Seteo de datos del registro **/
		BeanNotificacion beanFilter = new BeanNotificacion();
		beanFilter.setTipoNotif(String.valueOf(Integer.parseInt(anterior.getTipoNotif())));
		beanFilter.setCveTransfer(anterior.getCveTransfer());
		beanFilter.setMedioEntrega(anterior.getMedioEntrega());
		beanFilter.setEstatusTransfer(anterior.getEstatusTransfer());
		/** Ejecucion de la peticion al DAO **/
		BeanNotificaciones bean = daoNotificador.consultar(beanFilter, beanPaginador, session);
		String dtoAnt = obtenerDtos(bean.getNotificaciones().get(0));
		/** Ejecucion de la peticion al DAO **/
		response = daoNotificador.editar(beanNotificacion, anterior, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(beanNotificacion);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_UPDATE, "editarBloque.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion -->  bean que obtiene notificaciones
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanNotificaciones beanNotificaciones, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder();
		StringBuilder error = new StringBuilder();
		for(BeanNotificacion not : beanNotificaciones.getNotificaciones()){
			if (not.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoNotificador.eliminar(not, session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(not.getTipoNotif().concat("|").concat(not.getCveTransfer().concat("|").concat(not.getMedioEntrega().concat("|").concat(not.getEstatusTransfer()))));
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(not.getTipoNotif().concat("|").concat(not.getCveTransfer().concat("|").concat(not.getMedioEntrega().concat("|").concat(not.getEstatusTransfer()))));
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDtos(not);
	        	generaPistaAuditoras(TYPE_DELETE, "eliminarNotificador.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Exportar registros.
	 *
	 * @param beanNotificacion --> trae las notificaciones
	 * @param totalRegistros --> variable de tipo int 	y que obtiene el numero de registros
	 * @param session --> objeto de tipo ArchitechSessionBean
	 * @return response --> obtiene el mensaje de respuesta de la base de datos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	@Override
	public BeanResBase exportarTodo(BeanNotificacion beanNotificacion, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String filtroWhere = UtileriasNotificador.getInstancia().getFiltro(beanNotificacion, "WHERE");
		String query = UtileriasNotificador.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasNotificador.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasNotificador.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("NOTIFICADOR_ESTATUS");
		exportarRequest.setNombreRpt("CATALOGOS_NOTIFICADOR_ESTATUS_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodoNotificador.do", operacion, session,"","", DATO_FIJO);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras
	 * en la BD.
	 *
	 * @param operacion --> objeto de tipo operacion: alta, modificacion, eliminacion, exportar
	 * @param urlController --> objeto de tipo controller que es mandado llamar dependiendo de la operacion seleccionada
	 * @param resOp --> variable de tipo boleano respuesta de la operacion
	 * @param session --> objeto de tipo ArchitechSessionBean
	 * @param dtoNuevo --> cadena que obtiene el nuevo dato.
	 * @param dtoAnterior --> variable de tipo string que trae los registros anteriores
	 * @param dtoModificado --> variable de tipo string que trae el dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
	/**
	 * Obtener dtos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 * @param ant --> objeto de tipo beanNotificacion que trae la info de la notificacion
	 * @return dtoAnt: regresa la informacion de la notificacion
	 */
	private String obtenerDtos (BeanNotificacion ant) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es SELECT, UPDATE, DELETE AND INSERT **/
			dtoAnt ="TIPO_NOTIF=".concat(ant.getTipoNotif())
					.concat(", CVE_TRANSFER=".concat(ant.getCveTransfer()
					.concat(", MEDIO_ENTREGA=".concat(ant.getMedioEntrega()
					.concat(", ESTATUS_TRANSFER=".concat(ant.getEstatusTransfer()))))));
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
