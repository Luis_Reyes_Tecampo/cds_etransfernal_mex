/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOExcepCtasFideicomisoImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/09/2019 07:07:32 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepCtasFideicomiso;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOExcepCtasFideicomiso;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasExcepCtasFideico;

/**
 * Class BOExcepCtasFideicomisoImpl.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOExcepCtasFideicomisoImpl extends Architech implements BOExcepCtasFideicomiso {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2969018683101372027L;
	
	/** La constante CAMPO. */
	private static final String CAMPO = "NUM_CUENTA";
	
	/** La constante ERR_NO_EXISTE. */
	private static final String ERR_NO_EXISTE = "La cuenta no existe.";
	
	/** La constante STR_VACIO. */
	private static final String STR_VACIO = "";
	
	/** La constante TABLA. */
	private static final String TABLA = "TRAN_CTAS_FIDEICO_EXENTAS";
	
	/** La constante NINGUNO. */
	private static final String NINGUNO = "NA";
	
	/** La variable que contiene informacion con respecto a: dao excep ctas fideicomiso. */
	@EJB
	private DAOExcepCtasFideicomiso daoExcepCtasFideicomiso;

	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/**
	 * Consulta de cuentas (todas y por filtrado).
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param beanCtasFideico El objeto: bean ctas fideico --> Objeto de filtrado
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepCtasFideicomiso consultaCuentas(ArchitechSessionBean sessionBean, BeanExcepCtasFideicomiso beanCtasFideico) 
			throws BusinessException {
		/** Se inicializan variables */
		BeanExcepCtasFideicomiso response = null;
		int totalReg = 0;
		/** valida si el bean de entrada es nulo */
		if(beanCtasFideico == null) {
			/** si es nulo, lo inicializa */
			beanCtasFideico = new BeanExcepCtasFideicomiso();
		}
		/** valida si la cuenta de filtro es nula o vacia */
		if(beanCtasFideico.getBeanFilter() == null || beanCtasFideico.getBeanFilter().getCuenta() == null
				|| beanCtasFideico.getBeanFilter().getCuenta().isEmpty()) {
			/** si es nula o vacia consulta el total de todos los registros */
			totalReg = daoExcepCtasFideicomiso.totalRegistros(STR_VACIO);
		} else {
			/** si no, consulta el total de registros por filtrado */
			totalReg = daoExcepCtasFideicomiso.totalRegistros(beanCtasFideico.getBeanFilter().getCuenta());
		}
		/** asigna valores de paginador */
		beanCtasFideico.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		/** Consulta todas las cuentas */
		response = daoExcepCtasFideicomiso.consultaCuentas(sessionBean,beanCtasFideico);
		/** calcula paginas y regresa valores */
		beanCtasFideico.getBeanPaginador().calculaPaginas(totalReg, HelperDAO.REGISTROS_X_PAGINA.toString());
		response.setBeanPaginador(beanCtasFideico.getBeanPaginador());
		response.getBeanFilter().setTotalReg(totalReg);
		/** Retorna resultado */
		return response;
	}

	/**
	 * Alta de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param cuenta El objeto: cuenta --> Valor del registro nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepCtasFideicomiso altaCuentas(ArchitechSessionBean sessionBean, String cuenta)
			throws BusinessException {
		/** Se inicializan variables */
		BeanExcepCtasFideicomiso response = null;
		int consultaExisteCta = -1;
		BeanExcepCtasFideicomiso beanCtasFideico = new BeanExcepCtasFideicomiso();
		Boolean estatusOp = true;
		beanCtasFideico.getBeanFilter().setCuenta(cuenta);
		beanCtasFideico.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Consulta la cuenta entrante */
		consultaExisteCta = daoExcepCtasFideicomiso.buscarRegistro(cuenta, sessionBean);
		/** Valida si ya existe */
		if(consultaExisteCta == 0) {
			/** Si no existe la da de alta */
			response = daoExcepCtasFideicomiso.altaCuentas(sessionBean, cuenta);
		} else {
			/** Si ya existe regresa los siguientes datos */
			response = new BeanExcepCtasFideicomiso();
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError("Ya existe un registro con esa cuenta.");
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			estatusOp = false;
		}

		/** genera pista de auditoria */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_INSERT, "altaExcepCuentasFideicomiso.do", estatusOp, sessionBean, cuenta, STR_VACIO, CAMPO);
		
		return response;
	}

	/**
	 * Baja de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param listaCuentas El objeto: lista cuentas --> Lista de registros a eliminar
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepCtasFideicomiso bajaCuentas(ArchitechSessionBean sessionBean, List<BeanCtaFideicomiso> listaCuentas)
			throws BusinessException {
		/** Inicializa variables */
		BeanExcepCtasFideicomiso response = new BeanExcepCtasFideicomiso();
		int consultaExisteCta = 0;
		Boolean estatusOp = true;
		BeanExcepCtasFideicomiso beanCtasFideico = new BeanExcepCtasFideicomiso();
		beanCtasFideico.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		int cuentasEliminar = listaCuentas.size();
		int contCtasEliminadas = 0;
		for(int i = 0; i < cuentasEliminar; i++) {
			beanCtasFideico.getBeanFilter().setCuenta(listaCuentas.get(i).getCuenta());
			/** Consulta la cuenta entrante */
			consultaExisteCta = daoExcepCtasFideicomiso.buscarRegistro(beanCtasFideico.getBeanFilter().getCuenta(), sessionBean);
			/** Valida si existe la cuenta */
			if(consultaExisteCta > 0) {
				/** si existe, elimina la cuenta */
				response = daoExcepCtasFideicomiso.bajaCuentas(sessionBean, listaCuentas.get(i).getCuenta());
				contCtasEliminadas++;
			} else {
				/** Si no existe regresa los siguientes datos */
				response = new BeanExcepCtasFideicomiso();
				response.getBeanError().setCodError(Errores.ED00130V);
				response.getBeanError().setMsgError(ERR_NO_EXISTE);
				response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
				estatusOp = false;
				break;
			}
			/** genera pista de auditoria */
			generaPistaAuditoras(ConstantesCatalogos.TYPE_DELETE, "bajaExcepCuentasFideicomiso.do", estatusOp, sessionBean, STR_VACIO, listaCuentas.get(i).getCuenta(), CAMPO);
		}
		/** valida si el total de cuentas eliminadas fue el mismo de las que entraron  */
		if(contCtasEliminadas != cuentasEliminar) {
			response.getBeanError().setCodError(Errores.ED00130V);
			response.getBeanError().setMsgError("No se pudieron eliminar algunos de los registros seleccionados.");
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			estatusOp = false;
		}
		/** Retorna la respuesta */
		return response;
	}

	/**
	 * Modificacion de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param ctaOld El objeto: cta old --> Valor del registro anterior
	 * @param ctaNew El objeto: cta new --> Valor del registro nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanExcepCtasFideicomiso modificacionCuentas(ArchitechSessionBean sessionBean, String ctaOld, String ctaNew)
			throws BusinessException {
		/** Se inicializan variables */
		boolean estatusOp = true;
		BeanExcepCtasFideicomiso response = null;
		BeanExcepCtasFideicomiso beanCtasFideico = new BeanExcepCtasFideicomiso();
		beanCtasFideico.getBeanFilter().setCuenta(ctaNew);
		beanCtasFideico.getBeanPaginador().paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		response = daoExcepCtasFideicomiso.modificacionCuentas(sessionBean, ctaOld, ctaNew);
		if(!response.getBeanError().getCodError().equals(Errores.OK00000V)) {
			estatusOp = false;
		}
		/** genera pista de auditoria */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_UPDATE, "cambioExcepCuentasFideicomiso.do", estatusOp, sessionBean, ctaNew, ctaOld, CAMPO);
		/** Retorna el resultado */
		return response;
	}

	
	
	/**
	 * Exportar todos los registros.
	 *
	 * @param session El objeto: session --> Objeto de session
	 * @return Objeto bean res base --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean sessionBean) throws BusinessException {
		boolean estatusOp = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String query = "SELECT NUM_CUENTA FROM "+TABLA;
		int totalRegistros = daoExcepCtasFideicomiso.totalRegistros(STR_VACIO);
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasExcepCtasFideico.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("CUENTAS_FIDEICO");
		exportarRequest.setNombreRpt("CATALOGOS_EXCEPCION_CUENTAS_FIDEICOMISO_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, sessionBean);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			estatusOp = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** genera pista de auditoria  */
		generaPistaAuditoras(ConstantesCatalogos.TYPE_EXPORT, "exporTodoExcepCuentasFideicomiso.do", estatusOp, sessionBean, STR_VACIO, STR_VACIO, NINGUNO);
		/** Retorno de la respuesta del BO**/
		return response;
	}
	
	
	/**
	 * Genera pista auditoras.
	 *
	 * @param operacion El objeto: operacion --> Tipo de operacion realizada
	 * @param urlController El objeto: url controller --> .do donde se dispara la accion
	 * @param resOp El objeto: res op --> Respuesta de la operacion
	 * @param sesion El objeto: sesion --> El objeto sessiom
	 * @param dtoNuevo El objeto: dto nuevo --> Valor nuevo
	 * @param dtoAnterior El objeto: dto anterior --> Valor anterior
	 * @param dtoModificado El objeto: dto modificado --> Valor del dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(TABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(NINGUNO);
		beanPista.setDatoModi(dtoModificado);
		/** valida el tipo de operacion */
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		/** valida la respuesta de la operacion */
		if (resOp) {
			beanPista.setEstatus(ConstantesCatalogos.OK);
		} else {
			beanPista.setEstatus(ConstantesCatalogos.NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

}
