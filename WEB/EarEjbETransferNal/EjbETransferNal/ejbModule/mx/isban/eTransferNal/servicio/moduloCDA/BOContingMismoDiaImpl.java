/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOContingMismoDiaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:12:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDiaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResEstadoConexionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDiaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOContingMismoDia;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorCDA;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo BO que se encarga del negocio para la funcionalidad de
 * Contingencia CDA Mismo Dia
 **/
@Remote(BOContingMismoDia.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOContingMismoDiaImpl extends Architech implements
		BOContingMismoDia {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 6087179653189213838L;
	/**
	 * Referencia privada al dao de Contingencia mismo dia
	 */
	@EJB
	private DAOContingMismoDia daoContingMismoDia;

	/**
	 * Referencia privada al dao de MonitorCDA
	 */
	@EJB
	private DAOMonitorCDA daoMonitorCDA;

	/**
	 * Referencia privada al dao de la bitacora admin
	 */
	@EJB
	private DAOBitacoraAdmon daoBitacora;

	
	/**
	 * Metodo DAO para obtener la informacion para la funcionalidad de
	 * Contingencia CDA mismo dia
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDiaDAO Objeto del tipo
	 *         BeanResContingMismoDiaDAO
	 * 
	 */
	public BeanResContingMismoDia consultaPendientesMismoDia(
			ArchitechSessionBean architechSessionBean){
		BeanResContingMismoDia beanResContingMismoDia = null;
		BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = null;
		beanResContingMismoDiaDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
		beanResContingMismoDia = new BeanResContingMismoDia();
		beanResContingMismoDia.setHayOperacionespendientes(beanResContingMismoDiaDAO.getOperacionesPendientes()>0);
		return beanResContingMismoDia;
	}
	
	/**
	 * Metodo de negocio que calcula la pagina y que hace el llamado al dao para
	 * obtener la informacion para la funcionalidad de Contingencia CDA mismo
	 * dia
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanReqPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDia Objeto del tipo BeanResContingMismoDia
	 * @exception BusinessExceptionException
	 *                de negocio
	 */
	public BeanResContingMismoDia consultaContingMismoDia(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResContingMismoDiaDAO beanResContingMismoDiaPendientesDAO =null;
		List<BeanContingMismoDia> listBeanContingMismoDia = null;
		BeanResContingMismoDia beanResContingenciaMismoDia = null;
		BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = null;
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResContingMismoDiaDAO = daoContingMismoDia.consultaContingMismoDia(
				beanPaginador, architechSessionBean);
		listBeanContingMismoDia = beanResContingMismoDiaDAO
				.getListBeanContingMismoDia();
		beanResContingenciaMismoDia = new BeanResContingMismoDia();
		if (Errores.CODE_SUCCESFULLY.equals(beanResContingMismoDiaDAO
				.getCodError())
				&& listBeanContingMismoDia.isEmpty()) {
			beanResContingenciaMismoDia.setCodError(Errores.ED00011V);
			beanResContingenciaMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
			
			beanResContingMismoDiaPendientesDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
			beanResContingenciaMismoDia.setHayOperacionespendientes(
					beanResContingMismoDiaPendientesDAO.getOperacionesPendientes()>0);
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResContingMismoDiaDAO
				.getCodError())) {
			beanResContingenciaMismoDia
					.setListBeanContingMismoDia(listBeanContingMismoDia);
			beanResContingenciaMismoDia.setTotalReg(beanResContingMismoDiaDAO
					.getTotalReg());
			beanPaginador.calculaPaginas(beanResContingMismoDiaDAO
					.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResContingenciaMismoDia.setBeanPaginador(beanPaginador);
			beanResContingenciaMismoDia.setCodError(Errores.OK00000V);
			
			beanResContingMismoDiaPendientesDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
			beanResContingenciaMismoDia.setHayOperacionespendientes(
					beanResContingMismoDiaPendientesDAO.getOperacionesPendientes()>0);
			
		} else {
			beanResContingenciaMismoDia.setCodError(Errores.EC00011B);
			beanResContingenciaMismoDia.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return beanResContingenciaMismoDia;
	}

	/**
	 * Metodo que sirve para indicar que se debe de generar el archvio de
	 * contingencia mismo dia
	 * 
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResGenArchContingMismoDia Objeto del tipo
	 *         BeanResArchContingMismoDia
	 * @exception BusinessExceptionException
	 *                de negocio
	 */
	public BeanResGenArchContingMismoDia genArchContigMismoDia(
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		String numRegistros = "0";
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanResGenArchContingMismoDiaDAO beanResGenArchContingMismoDiaDAO = null;
		BeanResContingMismoDia beanResContingenciaMismoDia = null;
		BeanResEstadoConexionDAO beanResEstadoConexionDAO = null;
		final BeanResGenArchContingMismoDia beanResGenArchContingMismoDia = new BeanResGenArchContingMismoDia();
		beanResEstadoConexionDAO = daoMonitorCDA
				.consultaEstadoConexion(architechSessionBean);
		Date fechaHora = new Date();
		if (Errores.CODE_SUCCESFULLY.equals(beanResEstadoConexionDAO
				.getCodError())
				&& beanResEstadoConexionDAO.getContingencia() == -1) {
			beanResGenArchContingMismoDia.setContingencia(true);

			beanResGenArchContingMismoDiaDAO = daoContingMismoDia
					.genArchContingMismoDia(architechSessionBean);
			if (Errores.CODE_SUCCESFULLY
					.equals(beanResGenArchContingMismoDiaDAO.getCodError())) {
				
	        	if(beanResGenArchContingMismoDiaDAO.getNumOpGeneradas()!=null){
	        		numRegistros = beanResGenArchContingMismoDiaDAO.getNumOpGeneradas().replaceAll(",","");
	        	}

				
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacora("genArchContigMismoDia.do",
								numRegistros, "", fechaHora,
								"PENDIENTE", "GENARCHDIA", "TRAN_SPEI_CTG_CDA",
								"ID_PETICION", Errores.OK00000V,
								"TRANSACCION EXITOSA", "INSERT",beanResGenArchContingMismoDiaDAO.getIdPeticion());
				daoBitacora.guardaBitacora(beanReqInsertBitacora,
						architechSessionBean);
				beanResGenArchContingMismoDia.setCodError(Errores.OK00000V);
				beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
			}
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResEstadoConexionDAO
				.getCodError())) {
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("genArchContigMismoDia.do", "0", "",
							fechaHora, "PENDIENTE", "GENARCHDIA",
							"TRAN_SPEI_CTG_CDA", "ID_PETICION",
							Errores.ED00012V, "Estado Conectado, no gen arch",
							"INSERT","");
			daoBitacora.guardaBitacora(beanReqInsertBitacora,
					architechSessionBean);
			beanResGenArchContingMismoDia.setCodError(Errores.ED00012V);
			beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("genArchContigMismoDia.do", "0", "",
							fechaHora, "PENDIENTE", "GENARCHDIA",
							"TRAN_SPEI_CTG_CDA", "ID_PETICION",
							Errores.EC00011B, "No hay comunicaci\u00F3n serv.",
							"INSERT","");
			daoBitacora.guardaBitacora(beanReqInsertBitacora,
					architechSessionBean);
			beanResGenArchContingMismoDia.setCodError(Errores.EC00011B);
			beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		beanResContingenciaMismoDia = consultaContingMismoDia(
				new BeanPaginador(), architechSessionBean);
		beanResGenArchContingMismoDia
				.setListBeanContingMismoDia(beanResContingenciaMismoDia
						.getListBeanContingMismoDia());
		beanResGenArchContingMismoDia
				.setBeanPaginador(beanResContingenciaMismoDia
						.getBeanPaginador());
		beanResGenArchContingMismoDia.setTotalReg(beanResContingenciaMismoDia
				.getTotalReg());

		beanResGenArchContingMismoDia.setHayOperacionespendientes(
				beanResContingenciaMismoDia.isHayOperacionespendientes());
		return beanResGenArchContingMismoDia;
	}

	/**
	 * Metodo get de la referencia al DAO de contingencia mismo dia
	 * 
	 * @return daoContingMismoDia Objeto del tipo @see DAOContingMismoDia
	 */
	public DAOContingMismoDia getDaoContingMismoDia() {
		return daoContingMismoDia;
	}

	/**
	 * Metodo set de la referencia al DAO de contingencia mismo dia
	 * 
	 * @param daoContingMismoDia
	 *            Se setea Objeto del tipo @see DAOContingMismoDia
	 */
	public void setDaoContingMismoDia(DAOContingMismoDia daoContingMismoDia) {
		this.daoContingMismoDia = daoContingMismoDia;
	}

	/**
	 * Metodo get de la referencia al DAO del Monitor CDA
	 * 
	 * @return daoMonitorCDA Objeto del tipo @see DAOMonitorCDA
	 */
	public DAOMonitorCDA getDaoMonitorCDA() {
		return daoMonitorCDA;
	}

	/**
	 * Metodo set de la referencia al DAO del monitor CDA
	 * 
	 * @param daoMonitorCDA
	 *            Se setea Objeto del tipo @see DAOMonitorCDA
	 */
	public void setDaoMonitorCDA(DAOMonitorCDA daoMonitorCDA) {
		this.daoMonitorCDA = daoMonitorCDA;
	}

	/**
	 * Metodo get de la referencia al DAO de la bitacora
	 * 
	 * @return daoBitacora Objeto del tipo @see DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Metodo set de la referencia al DAO de la bitacora
	 * 
	 * @param daoBitacora
	 *            Se setea Objeto del tipo @see DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}

}
