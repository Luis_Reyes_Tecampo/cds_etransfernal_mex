/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOTraspasoFondosSPIDSIACImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloSPID.DAOComunesSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAOTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.utilerias.GeneraTramaUtils;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOTraspasoFondosSPIDSIACImpl extends Architech implements BOTraspasoFondosSPIDSIAC{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 6076723358752622448L;
	/**
	 * Referencia privada al dao de generar archivos historicos
	 */
	@EJB
	private DAOComunesSPID dAOComunesSPID;
	
	/**Referencia al servicio dao DAOBitTransaccional*/
	@EJB
	private DAOBitTransaccional dAOBitTransaccional;
	
	/**
	 * Propiedad del tipo DAOTraspasoFondosSPIDSIAC que almacena el valor de dAOTraspasoFondosSPIDSIAC
	 */
	@EJB
	private DAOTraspasoFondosSPIDSIAC dAOTraspasoFondosSPIDSIAC;

	@Override
	public BeanResTraspasoFondosSPIDSIAC traspasoFondosSPIDSIAC(
			BeanReqTraspasoFondosSPIDSIAC req, ArchitechSessionBean sessionBean) {
		String saldoPrincipal ="";
		BeanResTraspasoFondosSPIDSIAC beanResTraspasoFondosSPIDSIAC = new BeanResTraspasoFondosSPIDSIAC();
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
		BeanResConsMiInstDAO beanResConsMiInstDAO = null;

		beanResConsMiInstDAO = dAOComunesSPID.consultaMiInstitucion("ENTIDAD_SPID",sessionBean);
		beanResConsFechaOpDAO =dAOComunesSPID.consultaFechaOperacionSPID(sessionBean);
		beanResTraspasoFondosSPIDSIAC.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		beanResTraspasoFondosSPIDSIAC.setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		BeanAdministraSaldoDAO beanAdministraSaldoDAO = dAOTraspasoFondosSPIDSIAC.consultaSaldo(sessionBean);
		
		if(beanAdministraSaldoDAO.getSaldoPrincipal()!= null){
			saldoPrincipal = beanAdministraSaldoDAO.getSaldoPrincipal().replaceAll(",", "");
		}
		String imp = req.getImporte().replaceAll(",", "");
		
		BigDecimal saldo = new BigDecimal(saldoPrincipal);
		BigDecimal importe = new BigDecimal(imp);
		
		if(saldo.compareTo(importe)<0){
			beanResTraspasoFondosSPIDSIAC.setCodError(Errores.ED00093V);
			beanResTraspasoFondosSPIDSIAC.setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResTraspasoFondosSPIDSIAC;
		}
		enviar(req,beanResTraspasoFondosSPIDSIAC,sessionBean);
		
		
		return beanResTraspasoFondosSPIDSIAC;
	}
	
	/**
	 * @param req Objeto del tipo BeanReqTraspasoFondosSPIDSIAC
	 * @param beanResTraspasoFondosSPIDSIAC Obejto del tipo BeanResTraspasoFondosSPIDSIAC
	 * @param sessionBean Objeto de sesion de la arquitectura
	 */
	private void enviar(BeanReqTraspasoFondosSPIDSIAC req, BeanResTraspasoFondosSPIDSIAC beanResTraspasoFondosSPIDSIAC, ArchitechSessionBean sessionBean){
		String mensaje = "";
		String referencia = "";
		String estatus = "";
		String tramaRespuesta;
		String respuestas[] = null;
		GeneraTramaUtils generaTramaUtils = new GeneraTramaUtils();
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		BeanTransEnvio beanTransEnvio =seteaTramaEnviar(req.getImporte(),req.getComentario(),sessionBean);
		String trama = generaTramaUtils.armaTramaCadena(beanTransEnvio);
    	ResBeanEjecTranDAO resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "TRANSANT", "ARQ_MENSAJERIA", sessionBean);

		if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
			tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
			respuestas = tramaRespuesta.split("\\|");
			if(respuestas.length>0 && "TRIB0000".equals(respuestas[0])){
				beanResTraspasoFondosSPIDSIAC.setCodError(respuestas[0]);
				beanResTraspasoFondosSPIDSIAC.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResTraspasoFondosSPIDSIAC.setReferencia(respuestas[1]);
				beanResTraspasoFondosSPIDSIAC.setEstatus(respuestas[2]);
				
				setBitacoraTrans("TRASPSPIDSIAC", respuestas[0], "EXITO", "", 
						"", "TRASPFONDSPIDSIAC", "NA", beanResTraspasoFondosSPIDSIAC.getFchOperacion(),respuestas[1],
						"NA", "NA",
						respuestas[2],req.getImporte(), sessionBean);
			}else{
				referencia = respuestas[1];
				
				beanResTraspasoFondosSPIDSIAC.setCodError(respuestas[0]);
				beanResTraspasoFondosSPIDSIAC.setReferencia(referencia);
				if(respuestas.length>2){
					estatus = respuestas[2];
					mensaje = respuestas[3];
					beanResTraspasoFondosSPIDSIAC.setMsgError(mensaje);
				}else{
					mensaje = respuestas[1];
					referencia ="";
				}
				beanResTraspasoFondosSPIDSIAC.setTipoError(Errores.TIPO_MSJ_ALERT);
				
				setBitacoraTrans("TRASPFONDSPIDSIAC", respuestas[0], mensaje, "", 
						"", "", "NA", beanResTraspasoFondosSPIDSIAC.getFchOperacion(),referencia,
						"NA", "NA",
						estatus,req.getImporte(), sessionBean);
			}
		}else{
			beanResTraspasoFondosSPIDSIAC.setCodError(resBeanEjecTranDAO.getCodError());
			beanResTraspasoFondosSPIDSIAC.setTipoError(Errores.TIPO_MSJ_ERROR);
		}

		beanResTraspasoFondosSPIDSIAC.setTramaRespuesta(resBeanEjecTranDAO.getTramaRespuesta());
	}
	
    /**
     * @param idOperacion String con el id de la operacion
     * @param codError String con el codigo de error
     * @param descErr String con la descripcion del error
     * @param descOper String con la descripcion de la operacion
     * @param comentarios String con comentarios
     * @param servTran String con servicios tran
     * @param nombreArchivo String con nombre Archivo
     * @param fchOperacion String con fecha de operacion
     * @param referencia String con Referencia 
     * @param numCuentaOrd String con numero de cuenta ord
     * @param numCuentaRec String con numero de cuenta rec
     * @param estatus String con estatus
     * @param monto String con monto
     * @param sesion Objeto de sesion de la arquitectura
     */
    private void setBitacoraTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, String fchOperacion,String referencia,
			String numCuentaOrd, String numCuentaRec,
			String estatus,String monto, ArchitechSessionBean sesion){
    	UtileriasBitTrans utileriasBitTrans = new UtileriasBitTrans();
    	BeanReqInsertBitTrans beanReqInsertBitTrans = 
    		utileriasBitTrans.createBitacoraBitTrans(idOperacion, codError, descErr, descOper, 
    				comentarios, servTran, nombreArchivo, fchOperacion,referencia,
    				numCuentaOrd, numCuentaRec,
    				estatus,monto,  sesion);
    	dAOBitTransaccional.guardaBitacora(beanReqInsertBitTrans, sesion);
    }
	
	/**
	 * Metodo privado de ayuda que permite setear los datos para generar la trama a enviar
	 * @param monto String con el monto
	 * @param conceptoPago String con el concepto de Pago
	 * @param sessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanTransEnvio bean de respuesta con los datos
	 */
	private BeanTransEnvio seteaTramaEnviar(String monto,String conceptoPago,final ArchitechSessionBean sessionBean){
		BeanTransEnvio beanTransEnvio =new BeanTransEnvio();
		beanTransEnvio.setServicio("D");
			beanTransEnvio.setCveIntermeOrd("BANME");
			beanTransEnvio.setNombreOrd("BANCO SANTANDER");
			beanTransEnvio.setCveIntermeRec("BANME");
			beanTransEnvio.setNombreRec("BANCO SANTANDER CUENTA ALTERNA");
			beanTransEnvio.setMedioEnt("TRSP");
			beanTransEnvio.setCveTransfe("902");
			beanTransEnvio.setCveOperacion("SIAC");
			beanTransEnvio.setFormaLiq("C");
			beanTransEnvio.setComentario1(conceptoPago);
		
		beanTransEnvio.setCveEmpresa("BME");
		beanTransEnvio.setPtoVta("0901");
		
		beanTransEnvio.setUsuarioCap(sessionBean.getUsuario());
		beanTransEnvio.setUsuarioSol(sessionBean.getUsuario());
	
		beanTransEnvio.setCuentaOrd("");
		beanTransEnvio.setPtoVtaOrd("");
		beanTransEnvio.setDivisaOrd("USD");

		beanTransEnvio.setCuentaRec("");
		beanTransEnvio.setPlazaBanxico("01001");
		beanTransEnvio.setPtoVtaRec("");
		beanTransEnvio.setDivisaRec("USD");
		beanTransEnvio.setImporteCargo(monto);
		beanTransEnvio.setImporteAbono(monto);
		beanTransEnvio.setImporteDolares("0.00");
		beanTransEnvio.setNombreBcoRec("");
		beanTransEnvio.setSucBcoRe("");  
		beanTransEnvio.setPaisBcoRec("MEXI");
		beanTransEnvio.setCiudadBcoRe("");
		beanTransEnvio.setCveABA("");
		
		beanTransEnvio.setComentario2("");
		beanTransEnvio.setComentario3("");
		return beanTransEnvio;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOComunesSPID
	 * @return dAOComunesSPID Objeto del tipo DAOComunesSPID
	 */
	public DAOComunesSPID getDAOComunesSPID() {
		return dAOComunesSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOComunesSPID
	 * @param comunesSPID del tipo DAOComunesSPID
	 */
	public void setDAOComunesSPID(DAOComunesSPID comunesSPID) {
		dAOComunesSPID = comunesSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOBitTransaccional
	 * @return dAOBitTransaccional Objeto del tipo DAOBitTransaccional
	 */
	public DAOBitTransaccional getDAOBitTransaccional() {
		return dAOBitTransaccional;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOBitTransaccional
	 * @param bitTransaccional del tipo DAOBitTransaccional
	 */
	public void setDAOBitTransaccional(DAOBitTransaccional bitTransaccional) {
		dAOBitTransaccional = bitTransaccional;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOTraspasoFondosSPIDSIAC
	 * @return dAOTraspasoFondosSPIDSIAC Objeto del tipo DAOTraspasoFondosSPIDSIAC
	 */
	public DAOTraspasoFondosSPIDSIAC getDAOTraspasoFondosSPIDSIAC() {
		return dAOTraspasoFondosSPIDSIAC;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOTraspasoFondosSPIDSIAC
	 * @param traspasoFondosSPIDSIAC del tipo DAOTraspasoFondosSPIDSIAC
	 */
	public void setDAOTraspasoFondosSPIDSIAC(
			DAOTraspasoFondosSPIDSIAC traspasoFondosSPIDSIAC) {
		dAOTraspasoFondosSPIDSIAC = traspasoFondosSPIDSIAC;
	}

}
