/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConfiguracionCanalesImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanMedEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanal;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanalDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntregaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaCanales;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOConfiguracionCanales;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConfiguracionCanalesImpl implements BOConfiguracionCanales {
	
	/** Constante Pendiente para Bitacora */
	private static final String PENDIENTE_BITA = "PENDIENTE";
	
	/** Constante nombre de tabla para Bitacora */
	private static final String TABLA_BITA = "TRAN_CONTIG_CANALES";
	
	/** Constante dato fijo para Bitacora */
	private static final String FIJO_BITA = "cve_medio_ent";
	
	/** Constante descripcion de transaccion para Bitacora */
	private static final String DESCTRAN_BITA = "TRANSACCION EXITOSA";
	
	/** Constante numero uno para Bitacora */
	private static final String UNO = "1";
	
	/** Constante numero cero para Bitacora */
	private static final String CERO = "0";
	
	/** DAO para las transacciones a BD. */
	@EJB
	private DAOConfiguracionCanales daoConfiguracionCanales;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.moduloPOACOA.BOConfiguracionCanales#obtenerMediosEntrega(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResMedEntrega obtenerMediosEntrega(
			ArchitechSessionBean architectSessionBean){
		List<BeanMedEntrega> beanMedioEntregaList = Collections.emptyList();
		BeanResMedEntrega beanResMedEntrega = new BeanResMedEntrega();
		BeanResMedEntregaDAO beanResMedEntregaDAO = null;
		beanResMedEntregaDAO = daoConfiguracionCanales.consultaMediosEntrega(architectSessionBean);
		beanResMedEntrega.setCodError(beanResMedEntregaDAO.getCodError());
		if(Errores.OK00000V.equals(beanResMedEntregaDAO.getCodError())){
			beanResMedEntrega.setBeanMedioEntregaList(beanResMedEntregaDAO.getBeanMedioEntregaList());
		}else{
			beanResMedEntrega.setBeanMedioEntregaList(beanMedioEntregaList);
		}
		return beanResMedEntrega;
	}
	
	@Override
	public BeanResCanal obtenerCanales(
			ArchitechSessionBean architectSessionBean){
		List<BeanCanal> beanResCanalList = Collections.emptyList();
		BeanResCanal beanResCanal = new BeanResCanal();
		BeanResCanalDAO beanResCanalDAO = null;
		
		beanResCanalDAO = daoConfiguracionCanales.consultarCanales(architectSessionBean);
		beanResCanal.setCodError(beanResCanalDAO.getCodError());
		
		if(Errores.OK00000V.equals(beanResCanalDAO.getCodError())){
			beanResCanalList = beanResCanalDAO.getBeanResCanalList();
			beanResCanal.setBeanResCanalList(beanResCanalList);
		}else if(Errores.ED00011V.equals(beanResCanalDAO.getCodError())){
			beanResCanal.setTipoError(Errores.TIPO_MSJ_ALERT);
			beanResCanal.setBeanResCanalList(beanResCanalList);
		}else{
			beanResCanal.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResCanal.setBeanResCanalList(beanResCanalList);
		}
		return beanResCanal;
	}
	
	@Override
	public BeanResConsultaCanales obtenerCanalesPag(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean){
		BeanResConsultaCanales beanResConsultaCanales = new BeanResConsultaCanales();
		BeanPaginador beanPaginador = null;
		beanPaginador = reqCanal.getPaginador();
		 if(beanPaginador == null){
			 beanPaginador = new BeanPaginador();
			 reqCanal.setPaginador(beanPaginador);
	      } else if("".equals(beanPaginador.getPagina())){
	    	  beanPaginador.setPagina(UNO);
	      }
		 reqCanal.setIdCanal(reqCanal.getCmbCanal());
		 beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsultaCanales = daoConfiguracionCanales
					.consultarCanalesPag(reqCanal, architectSessionBean);
			if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())){
				int contador = 0;
				for (int i = 0; i < beanResConsultaCanales.getListaBeanResCanal().size(); i++) {
					if (i % 2 == 0) {
						beanResConsultaCanales.getListaBeanResCanal().get(contador)
								.setBanderaRenglon(true);
					} else {
						beanResConsultaCanales.getListaBeanResCanal().get(contador)
								.setBanderaRenglon(false);
					}
					contador++;
				}
				beanPaginador.calculaPaginas(beanResConsultaCanales.getTotalReg(),
						HelperDAO.REGISTROS_X_PAGINA.toString());
				beanResConsultaCanales.setBeanPaginador(beanPaginador);
			}else if(Errores.ED00011V.equals(beanResConsultaCanales.getCodError())){
				beanResConsultaCanales.setTipoError(Errores.TIPO_MSJ_ALERT);
			}else{
				beanResConsultaCanales.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
			BeanResCanalDAO banResCanalDAO = daoConfiguracionCanales.consultarCanales(architectSessionBean);
			beanResConsultaCanales.setBeanResCanalList(banResCanalDAO.getBeanResCanalList());
		return beanResConsultaCanales;
	}
	
	@Override
	public BeanResConsultaCanales agregarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase res = null;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.setRegIni("1");
		beanPaginador.setRegFin("2");
		reqCanal.setPaginador(beanPaginador);
		BeanResConsultaCanales beanResConsultaCanales = daoConfiguracionCanales.consultarCanalesPag(reqCanal, architectSessionBean);
		if(Errores.ED00011V.equals(beanResConsultaCanales.getCodError())&& beanResConsultaCanales.getListaBeanResCanal().isEmpty()){
			res = daoConfiguracionCanales.insertarCanal(reqCanal,
					architectSessionBean);
			Utilerias utilerias = Utilerias.getUtilerias();
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("altaCanal.do", UNO, "", new Date(),
							PENDIENTE_BITA, "ALTACANAL", TABLA_BITA,
							FIJO_BITA, res.getCodError(),
							DESCTRAN_BITA, "INSERT",
							reqCanal.getIdCanal());
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);			
			beanResConsultaCanales.setCodError(Errores.OK00000V);
			beanResConsultaCanales.setTipoError(Errores.TIPO_MSJ_INFO);
		}else{
			BeanResMedEntregaDAO beanResMedEntregaDAO = daoConfiguracionCanales.consultaMediosEntrega(architectSessionBean);
			beanResConsultaCanales = new BeanResConsultaCanales();
			beanResConsultaCanales.setCodError(Errores.ED00061V);
			beanResConsultaCanales.setTipoError(Errores.TIPO_MSJ_ALERT);
			beanResConsultaCanales.setBeanMedioEntregaList(beanResMedEntregaDAO.getBeanMedioEntregaList());
		}
		BeanResCanalDAO beanResCanalDAO = daoConfiguracionCanales.consultarCanales(architectSessionBean);
		beanResConsultaCanales.setBeanResCanalList(beanResCanalDAO.getBeanResCanalList());
		return beanResConsultaCanales;
	}
	
	@Override
	public BeanResConsultaCanales activarDesactivarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase res = null;
		BeanReqCanal reqCanalConsulta = new BeanReqCanal();
		reqCanalConsulta.setIdCanal(reqCanal.getIdCanal());
		BeanPaginador beanPaginador = null;
		beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		reqCanalConsulta.setPaginador(beanPaginador);
		BeanResConsultaCanales beanResConsultaCanales = daoConfiguracionCanales.consultarCanalesPag(reqCanalConsulta, architectSessionBean);
		if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())&& !beanResConsultaCanales.getListaBeanResCanal().isEmpty()){
			res = daoConfiguracionCanales.actualizarEstatusCanal(reqCanal,
						architectSessionBean);
				String antBit;
				String nuevoBit;
				if (reqCanal.isActivoCanal()) {
					antBit = CERO;
					nuevoBit = UNO;
				} else {
					antBit = UNO;
					nuevoBit = CERO;
				}
				Utilerias utilerias = Utilerias.getUtilerias();
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacoraMod("actualizaCanal.do", UNO, "", new Date(),
								PENDIENTE_BITA, "ACTCANAL", TABLA_BITA,
								FIJO_BITA, res.getCodError(),
								DESCTRAN_BITA, "UPDATE", nuevoBit, antBit);
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
		}else{
			beanResConsultaCanales = new BeanResConsultaCanales();
			beanResConsultaCanales.setCodError(Errores.ED00061V);
			beanResConsultaCanales.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		
		beanPaginador = reqCanal.getPaginador();
		if(beanPaginador == null){
			 beanPaginador = new BeanPaginador();
			 reqCanal.setPaginador(beanPaginador);
	     } else if("".equals(beanPaginador.getPagina())){
	    	  beanPaginador.setPagina(UNO);
	    }
		beanPaginador.setAccion("ACT");
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		reqCanal.setPaginador(beanPaginador);
		reqCanal.setIdCanal(reqCanal.getCmbCanal());
		beanResConsultaCanales = daoConfiguracionCanales.consultarCanalesPag(reqCanal, architectSessionBean);
		beanPaginador.calculaPaginas(beanResConsultaCanales.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
		BeanResCanalDAO beanResCanalDAO = daoConfiguracionCanales.consultarCanales(architectSessionBean);
		beanResConsultaCanales.setBeanPaginador(beanPaginador);
		
		beanResConsultaCanales.setBeanResCanalList(beanResCanalDAO.getBeanResCanalList());
		return beanResConsultaCanales;
	}
	
	@Override
	public BeanResConsultaCanales borrarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean) {
		BeanResConsultaCanales beanResConsultaCanales = null;
		BeanResBase res = null;
		
		BeanPaginador beanPaginador = null;
		beanPaginador = reqCanal.getPaginador();
		 if(beanPaginador == null){
			 beanPaginador = new BeanPaginador();
			 reqCanal.setPaginador(beanPaginador);
	      } else if("".equals(beanPaginador.getPagina())){
	    	  beanPaginador.setPagina(UNO);
	      }
		 beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResConsultaCanales = daoConfiguracionCanales.consultarCanalesPag(reqCanal, architectSessionBean);
		if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())&& !beanResConsultaCanales.getListaBeanResCanal().isEmpty()){
			res = daoConfiguracionCanales.eliminarCanal(reqCanal,
						architectSessionBean);
				Utilerias utilerias = Utilerias.getUtilerias();
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacora("eliminarCanales.do", UNO, "", new Date(),
								PENDIENTE_BITA, "ELIMINACANAL", TABLA_BITA,
								FIJO_BITA, res.getCodError(),
								DESCTRAN_BITA, "DELETE",
								reqCanal.getIdCanal());
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
				beanResConsultaCanales.setCodError(res.getCodError());
		}
		return beanResConsultaCanales;
	}

	/**
	 * Devuelve el valor de daoConfiguracionCanales
	 * @return the daoConfiguracionCanales
	 */
	public DAOConfiguracionCanales getDaoConfiguracionCanales() {
		return daoConfiguracionCanales;
	}

	/**
	 * Modifica el valor de daoConfiguracionCanales
	 * @param daoConfiguracionCanales the daoConfiguracionCanales to set
	 */
	public void setDaoConfiguracionCanales(
			DAOConfiguracionCanales daoConfiguracionCanales) {
		this.daoConfiguracionCanales = daoConfiguracionCanales;
	}

	/**
	 * Devuelve el valor de daoBitacora
	 * @return the daoBitacora
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Modifica el valor de daoBitacora
	 * @param daoBitacora the daoBitacora to set
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}

}
