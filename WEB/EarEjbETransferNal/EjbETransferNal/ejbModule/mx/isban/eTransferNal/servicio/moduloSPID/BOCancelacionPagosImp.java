/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOCancelacionPagosImp.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagosDAO;

import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;

import mx.isban.eTransferNal.beans.moduloSPID.BeanResCancelacionPagos;

import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
//import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloSPID.DAOCancelacionPagos;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
//import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;


/**
 *Clase del tipo BO que se encarga  del negocio de la cancelación de pagos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)

public class BOCancelacionPagosImp implements BOCancelacionPagos {
	/**
	 * Propiedad del tipo @see DAOBitTransaccional que contendra el valor de EJB
	 */
	@EJB
	private DAOBitTransaccional daoBitTransaccional;
	
	/**
	 * 
	 */
	@EJB
	private DAOCancelacionPagos daoCancelacionPagos;

	/**Metodo que sirve para consultar cancelacion de apgos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @return beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	   * @throws BusinessException Exception
	   */
	@Override
	public BeanResCancelacionPagos obtenerCancelacionPagos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)throws BusinessException{
		
		BeanCancelacionPagosDAO beanCancelacionPagosDAO = null;
		final BeanResCancelacionPagos beanResCancelacionPagos = new BeanResCancelacionPagos();
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String sF = getSortField(sortField);
		
		beanCancelacionPagosDAO = daoCancelacionPagos.obtenerCancelacionPagos(pag, architechSessionBean, sF, sortType);
		
		if (beanCancelacionPagosDAO.getBeanCancelacionPagos()==null) {
			beanCancelacionPagosDAO.setBeanCancelacionPagos(new ArrayList<BeanCancelacionPagos>());
		}

		if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError()) && beanCancelacionPagosDAO.getBeanCancelacionPagos().isEmpty() ){
			beanResCancelacionPagos.setCodError(Errores.ED00011V);
			beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResCancelacionPagos.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());			
						
		}else if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError())){
			beanResCancelacionPagos.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());
			beanResCancelacionPagos.setTotalReg(beanCancelacionPagosDAO.getTotalReg());
			pag.calculaPaginas(beanCancelacionPagosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResCancelacionPagos.setBeanPaginador(pag);
			beanResCancelacionPagos.setImporteTotal(daoCancelacionPagos.obtenerImporteTotal(architechSessionBean).toString());
			beanResCancelacionPagos.setCodError(Errores.OK00000V);
		}else{
			beanResCancelacionPagos.setCodError(Errores.EC00011B);
			beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		

		return beanResCancelacionPagos;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("referencia".equals(sortField)){
			sF = "REFERENCIA";
		} else if ("cola".equals(sortField)){
			sF = "CVE_COLA";
		} else if ("cvnTran".equals(sortField)){
			sF = "CREFERENCIA";
		} else if ("mecan".equals(sortField)){
			sF = "MECAN";
		} else if ("medioEnt".equals(sortField)){
			sF = "MEDIOENT";
		} else if ("ord".equals(sortField)){
			sF = "ORD";
		} else if ("rec".equals(sortField)){
			sF = "REC";
		} else if ("importeAbono".equals(sortField)){
			sF = "IMPORTE_ABONO";
		} else if ("paq".equals(sortField)){
			sF = "PAQUETE";
		} else if ("pago".equals(sortField)){
			sF = "PAGO";
		} else if ("est".equals(sortField)){
			sF = "EST";
		} else if ("topo".equals(sortField)){
			sF = "TOPO";
		} else if ("pri".equals(sortField)){
			sF = "PRI";
		}
		return sF;
	}

	@Override
	public BeanResCancelacionPagos obtenerCancelacionPagosTraspasos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)throws BusinessException{

		
		BeanCancelacionPagosDAO beanCancelacionPagosDAO = null;
		final BeanResCancelacionPagos beanResCancelacionPagosTras = new BeanResCancelacionPagos();
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String sF = getSortField(sortField);
		
		beanCancelacionPagosDAO = daoCancelacionPagos.obtenerCancelacionPagosTraspasos(pag, architechSessionBean, sF, sortType);
		
		if (beanCancelacionPagosDAO.getBeanCancelacionPagos()==null) {
			beanCancelacionPagosDAO.setBeanCancelacionPagos(new ArrayList<BeanCancelacionPagos>());
		}

		if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError()) && beanCancelacionPagosDAO.getBeanCancelacionPagos().isEmpty() ){
			beanResCancelacionPagosTras.setCodError(Errores.ED00011V);
			beanResCancelacionPagosTras.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResCancelacionPagosTras.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());			
						
		}else if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError())){
			beanResCancelacionPagosTras.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());
			beanResCancelacionPagosTras.setTotalReg(beanCancelacionPagosDAO.getTotalReg());
			pag.calculaPaginas(beanCancelacionPagosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResCancelacionPagosTras.setBeanPaginador(pag);
			beanResCancelacionPagosTras.setImporteTotal(daoCancelacionPagos.obtenerImporteTotalTras(architechSessionBean).toString());
			beanResCancelacionPagosTras.setCodError(Errores.OK00000V);
		}else{
			beanResCancelacionPagosTras.setCodError(Errores.EC00011B);
			beanResCancelacionPagosTras.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		

		return beanResCancelacionPagosTras;
	}

	
	

	/**Metodo que sirve para consultarla cancelacion de pagos filtrados por orden
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param sortField Objeto del tipo String
	   * @param sortType Objeto del tipo String
	   * @throws BusinessException Exception
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	@Override
	public BeanResCancelacionPagos obtenerCancelacionPagosOrden(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)throws BusinessException{

		
		BeanCancelacionPagosDAO beanCancelacionPagosDAO = null;
		final BeanResCancelacionPagos beanResCancelacionPagosOrden = new BeanResCancelacionPagos();
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String sF = getSortField(sortField);
		
		beanCancelacionPagosDAO = daoCancelacionPagos.obtenerCancelacionPagosOrden(pag, architechSessionBean, sF, sortType);
		
		if (beanCancelacionPagosDAO.getBeanCancelacionPagos()==null) {
			beanCancelacionPagosDAO.setBeanCancelacionPagos(new ArrayList<BeanCancelacionPagos>());
		}

		if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError()) && beanCancelacionPagosDAO.getBeanCancelacionPagos().isEmpty() ){
			beanResCancelacionPagosOrden.setCodError(Errores.ED00011V);
			beanResCancelacionPagosOrden.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResCancelacionPagosOrden.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());			
						
		}else if(Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError())){
			beanResCancelacionPagosOrden.setListaCancelacionPagos(beanCancelacionPagosDAO.getBeanCancelacionPagos());
			beanResCancelacionPagosOrden.setTotalReg(beanCancelacionPagosDAO.getTotalReg());
			pag.calculaPaginas(beanCancelacionPagosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResCancelacionPagosOrden.setImporteTotal(daoCancelacionPagos.obtenerImporteTotalOrd(architechSessionBean).toString());
			beanResCancelacionPagosOrden.setBeanPaginador(pag);
			
			beanResCancelacionPagosOrden.setCodError(Errores.OK00000V);
		}else{
			beanResCancelacionPagosOrden.setCodError(Errores.EC00011B);
			beanResCancelacionPagosOrden.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		

		return beanResCancelacionPagosOrden;
	}
	
	/**Metodo que sirve para insertar las cancelaciones de pagos
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException Exception
	   * @return beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	   */
	@Override
	public BeanResCancelacionPagos exportarTodoCancelacionPagos(BeanReqCancelacionPagos beanReqCancelacionPagos,
			ArchitechSessionBean architechSessionBean , String opcion) throws BusinessException {
		BeanCancelacionPagosDAO beanCancelacionPagosDAO = null;
		BeanResCancelacionPagos beanResCancelacionPagos =null;
		
		BeanPaginador paginador = beanReqCancelacionPagos.getPaginador();
		
		if (paginador == null){
			  paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		beanResCancelacionPagos =obtenerCancelacionPagos(paginador, architechSessionBean, "", "");
		
		beanReqCancelacionPagos.setTotalReg(beanResCancelacionPagos.getTotalReg());

		
		beanCancelacionPagosDAO=daoCancelacionPagos.exportarTodoCancelacionPagos(beanReqCancelacionPagos, architechSessionBean,  opcion);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError())) {
			beanResCancelacionPagos.setCodError(Errores.OK00002V);
			beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResCancelacionPagos.setNombreArchivo(beanCancelacionPagosDAO.getNombreArchivo());
		} else {
			beanResCancelacionPagos.setCodError(Errores.EC00011B);
			beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return beanResCancelacionPagos;

	}
	
	/**Metodo que sirve para actualizar una cancelacion de pago
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException Exception
	   * @return beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	   */
	@Override
	public BeanResCancelacionPagos cancelar(BeanReqCancelacionPagos beanReqCancelacionPagos,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		
		BeanResCancelacionPagos beanResCancelacionPagos = null;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		
		List<BeanCancelacionPagos> listaBeanCancelacionPagos = helperRecepcionOperacion.filtrarSelccionCancelacionPagos(beanReqCancelacionPagos,  HelperRecepcionOperacion.SELECCIONADO);
				
				
		if (!listaBeanCancelacionPagos.isEmpty()) {
			beanResCancelacionPagos = actualizarCancelacion(
					beanReqCancelacionPagos, listaBeanCancelacionPagos, architechSessionBean);
		} else {
			beanResCancelacionPagos = obtenerDatosPorFiltro(beanReqCancelacionPagos, architechSessionBean);
			beanResCancelacionPagos.setCodError(Errores.ED00068V);
			beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_ALERT);
		}		
		return beanResCancelacionPagos;
	}

	/**
	 * @param beanReqCancelacionPagos Objeto del tipo BeanReqCancelacionPagos
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResCancelacionPagos
	 * @throws BusinessException exception
	 */
	protected BeanResCancelacionPagos obtenerDatosPorFiltro(BeanReqCancelacionPagos beanReqCancelacionPagos,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResCancelacionPagos beanResCancelacionPagos;
		
		if ("1".equals(beanReqCancelacionPagos.getFiltro())){
			beanResCancelacionPagos = obtenerCancelacionPagosOrden(beanReqCancelacionPagos.getPaginador(), architechSessionBean, "", "");
		} else if ("2".equals(beanReqCancelacionPagos.getFiltro())){
			beanResCancelacionPagos = obtenerCancelacionPagosTraspasos(beanReqCancelacionPagos.getPaginador(), architechSessionBean, "", "");
		}
		else {
			beanResCancelacionPagos = obtenerCancelacionPagos(beanReqCancelacionPagos.getPaginador(), architechSessionBean, "", "");
		}
		return beanResCancelacionPagos;
	}
	/**Metodo que sirve para actualizar una cancelacion de pago
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param listaBeanCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	   * @param architechSessionBean objeto del tipo ArchitechSessionBean
	   * @throws BusinessException Exception
	   * @return beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	   */
	private BeanResCancelacionPagos actualizarCancelacion(BeanReqCancelacionPagos beanReqCancelacionPagos, 
			List<BeanCancelacionPagos> listaBeanCancelacionPagos, 
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		
		BeanCancelacionPagosDAO beanCancelacionPagosDAO = null;
		BeanResCancelacionPagos beanResCancelacionPagos = new BeanResCancelacionPagos();

		    	String fechaOperacion = beanReqCancelacionPagos.getSFechaOperacion();
    	String cveInstitucion = beanReqCancelacionPagos.getSCveInstitucion();
    	Date fechaActual = new Date();
		
		for(BeanCancelacionPagos beanCancelacionPagos : listaBeanCancelacionPagos){
			beanCancelacionPagos.setSFechaOperacion(fechaOperacion);
			beanCancelacionPagos.setSCveInstitucion(cveInstitucion);
			
    		beanCancelacionPagosDAO =daoCancelacionPagos.actualizarCancelacionPagos(beanCancelacionPagos, architechSessionBean);
    				
			if (beanResCancelacionPagos.getCodError() == null){
				beanResCancelacionPagos.setCodError(beanCancelacionPagosDAO.getCodError());
			}
		
			if (!Errores.CODE_SUCCESFULLY.equals(beanCancelacionPagosDAO.getCodError())) {
				SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
				setBitacoraTrans("CANDEPAGO", beanCancelacionPagosDAO.getCodError(), "", "", 
						"", "NacCancelar.do", "NA", formatDate.format(fechaActual), 
						beanCancelacionPagos.getBeanCancelacionPagosDos().getReferencia().toString(),
						beanCancelacionPagos.getBeanCancelacionPagosDos().getOrd(), 
						beanCancelacionPagos.getBeanCancelacionPagosDos().getRec(), "ERR", 
						beanCancelacionPagos.getBeanCancelacionPagosDos().getImporteAbono(), architechSessionBean);				
				
				beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_ERROR);
				return beanResCancelacionPagos;
        	}
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
			
			setBitacoraTrans("CANDEPAGO", beanCancelacionPagosDAO.getCodError(), "EXITO", "", 
					"", "NacCancelar.do", "NA", formatDate.format(fechaActual), 
					beanCancelacionPagos.getBeanCancelacionPagosDos().getReferencia().toString(),
					beanCancelacionPagos.getBeanCancelacionPagosDos().getOrd(), 
					beanCancelacionPagos.getBeanCancelacionPagosDos().getRec(), "OK", 
					beanCancelacionPagos.getBeanCancelacionPagosDos().getImporteAbono(), architechSessionBean);
        }
    	
		beanResCancelacionPagos = obtenerDatosPorFiltro(beanReqCancelacionPagos, architechSessionBean);		
    	
    	beanResCancelacionPagos.setCodError(Errores.OK00000V);
		beanReqCancelacionPagos.setTotalReg(beanResCancelacionPagos.getTotalReg());
		beanResCancelacionPagos.setTipoError(Errores.TIPO_MSJ_INFO);
    	
		return beanResCancelacionPagos;
	}
	
	/**
     * @param idOperacion String con el id de la operacion
     * @param codError String con el codigo de error
     * @param descErr String con la descripcion del error
     * @param descOper String con la descripcion de la operacion
     * @param comentarios String con comentarios
     * @param servTran String con servicios tran
     * @param nombreArchivo String con nombre Archivo
     * @param fchOperacion String con fecha de operacion
     * @param referencia String con Referencia 
     * @param numCuentaOrd String con numero de cuenta ord
     * @param numCuentaRec String con numero de cuenta rec
     * @param estatus String con estatus
     * @param monto String con monto
     * @param sesion Objeto de sesion de la arquitectura
     */
    private void setBitacoraTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, String fchOperacion,String referencia,
			String numCuentaOrd, String numCuentaRec,
			String estatus,String monto, ArchitechSessionBean sesion){
    	UtileriasBitTrans utileriasBitTrans = new UtileriasBitTrans();
    	BeanReqInsertBitTrans beanReqInsertBitTrans = 
    		utileriasBitTrans.createBitacoraBitTrans(idOperacion, codError, descErr, descOper, 
    				comentarios, servTran, nombreArchivo, fchOperacion,referencia,
    				numCuentaOrd, numCuentaRec,
    				estatus,monto,  sesion);
    	daoBitTransaccional.guardaBitacora(beanReqInsertBitTrans, sesion);
    }
    
    /**
	 * @return DAOCancelacionPagos
	 */
	public DAOCancelacionPagos getDaoCancelacionPagos() {
		return daoCancelacionPagos;
	}

	/**
	 * @param daoCancelacionPagos Objeto del tipo DAOCancelacionPagos
	 */
	public void setDaoCancelacionPagos(DAOCancelacionPagos daoCancelacionPagos) {
		this.daoCancelacionPagos = daoCancelacionPagos;
	}
	
	/**
	 * @return the daoBitTransaccional
	 */
	public DAOBitTransaccional getDaoBitTransaccional() {
		return daoBitTransaccional;
	}

	/**
	 * @param daoBitTransaccional the daoBitTransaccional to set
	 */
	public void setDaoBitTransaccional(DAOBitTransaccional daoBitTransaccional) {
		this.daoBitTransaccional = daoBitTransaccional;
	}
}
