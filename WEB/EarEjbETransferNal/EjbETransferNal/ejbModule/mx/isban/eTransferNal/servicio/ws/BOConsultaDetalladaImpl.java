/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BOConsultaDetalladaImpl.java
 *
 * Control de versiones:
 * Version	Date/Hour				By							Company		Description
 * -------	----------------------	--------------------------- ----------	----------------------
 * 1.0.0	 7/02/2017 11:27:15 AM	Juan Jesus Beltran R.		Isban		Creacion
 * 1.0.1	10/02/2017				Adrian Ricardo Martinez		Vector SF	Modificaciones para json de entrada/salida
 * 1.0.2	04/04/2018 12:00:00 AM  Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */
package mx.isban.eTransferNal.servicio.ws;
//Imports java
import java.util.Arrays;
//Imports javax
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
//Imports etransferNal
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.dao.ws.DAOConsulaDetallada;
import mx.isban.eTransferNal.servicios.ws.BOConsultaDetallada;
//Imports metrics
import mx.isban.metrics.dto.DTOMetricsE;
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;
//Imports net
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

/**
 * Class BOConsultaDetalladaImpl.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaDetalladaImpl extends Architech implements BOConsultaDetallada {

	/** Utileria para log*/
	private static final Logger LOGGER = Logger.getLogger(BOConsultaDetalladaImpl.class);
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -394150545446005436L;
	/** Mensaje de error cuando la trama de entrada no cumple con los requisitos MSG_ERROR_ED00113V. */
	private static final String MSG_ERROR_ED00113V = 
			"{COD_ERROR:\"ED00113V\",DESCRIPCION:\"Debe especificarse uno de los siguientes datos: %s o %s\"}";
	/** Mensaje de error cuando el dato no cumple con los valores establecidos. */
	private static final String MSG_ERR_ED00112V = 
			"{COD_ERROR:\"ED00112V\",DESCRIPCION:\"El campo %s debe tener alguno de los siguientes valores: %s\"}";
	/** Items para el armado de la trama de envio a MQ. */
	private static final String[] ITEMS_TRAMA_MQ = { "ENVIORECEPCION|1|Obligatorio|E,R",
			"FCH_OPERACION|8|Dependiente|FCH_CAPTURA", "FCH_CAPTURA|8|Dependiente|FCH_OPERACION",
			"REFE_TRANSFER|7|Dependiente|CVE_RASTREO", "CVE_RASTREO|30|Dependiente|REFE_TRANSFER",
			"CTA_ORDENANTE|20|Opcional", "TIPO_CTA_ORD|3|Opcional|40,03,10,101,04", "CTA_RECEPTORA|20|Opcional",
			"TIPO_CTA_RECEP|3|Opcional|40,03,10,101,04", "BCO_ORDENANTE|5|Opcional", "BCO_RECEPTOR|5|Opcional" };
	/** Items para el armado del JSON de salida. */
	private static final String[] ITEMS_JSON_SALIDA = { "\"COD_ERROR\"|8", "\"DESC_ERROR\"|70", "\"CVE_MECANISMO\"|8",
			"\"CVE_INST_ORD\"|5", "\"NUM_BXCO_INST_ORD\"|7", "\"NOM_LARGO_INST_ORD\"|40", "\"CVE_INST_BENEF\"|5",
			"\"NUM_BXCO_INST_BENEF\"|7", "\"NOM_LARGO_INST_BENEF\"|40", "\"ESTATUS\"|2", "\"DESC_ESTATUS\"|40",
			"\"CVE_MOTIVO_DEV\"|2", "\"DESC_MOTIVO_DEV\"|40", "\"FCH_CONST_INST_ORD\"|8", "\"FCH_APROBACION\"|18",
			"\"RFC_ORD\"|18", "\"REC_BENEF\"|18", "\"NOM_ORD\"|120", "\"NOM_BENEF\"|120", "\"REFE_PAGO\"|20",
			"\"CTA_ORDENANTE\"|20", "\"TIPO_CTA_ORD\"|3", "\"CTA_RECEPTORA\"|20", "\"TIPO_CTA_BENEF\"|3",
			"\"CVE_RASTREO\"|30", "\"REFE_TRANSFER\"|7", "\"CONCEPTO_PAGO\"|210", "\"DIR_ORD\"|120",
			"\"DIR_BENEF\"|120", "\"IMPORTE_CARGO\"|20", "\"IMPORTE_ABONO\"|20", "\"IVA\"|20", "\"TIPO_CAMBIO\"|15",
			"\"IMPORTE_DIVISA\"|20", "\"DIVISA_ORD\"|4", "\"DIVISA_BENEF\"|4", "\"LEYENDA_ORD\"|150",
			"\"LEYENDA_BENEF\"|150" };
	/** Constante SEPARADOR. */
	private static final String SEPARADOR = "\\|";
	/** Valor cero 0. */
	private static final int CERO = 0;
	/** Constante para mensaje de error TRAMA_INCORRECTA. ADD: JMFR CSA*/
	private static final String TRAMA_INCORRECTA = "Trama incorrecta";
	
	/** DAO de consulta via MQ. */
	@EJB private DAOConsulaDetallada dAOConsultaDetallada;
	
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** EJB para bitacora metrics*/
	@EJB private BitacorizaMetrics metricsBo;
	/** Bean para monitoreo*/
	private final MetricsSenderInit senderInit = new MetricsSenderInit();
	/** Bean para errores de negocio*/
	private final DTOMetricsE metricsE = new DTOMetricsE(); 
	// END [ADD: JMFR CSA Monitoreo Metrics]

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.isban.eTransferNal.servicios.ws.BOConsultaDetallada#consultaDetallada(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO,
	 * mx.isban.metrics.dto.DTOMetricsException)
	 */
	@Override
	public ResEntradaStringJsonDTO consultaDetallada(EntradaStringJsonDTO param) {
		// Se crea objeto de respuesta
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		// Se arma trama de peticion
		String trama = armarTrama(JSONObject.fromString(param.getCadenaJson()));
		debug(String.format("TRAMA SEND: [%s]", trama));
		// Se valida que la trama fue creada correctamente
		if (trama.indexOf("COD_ERROR") == -1) {
			ResBeanEjecTranDAO resBeanEjecTranDAO = dAOConsultaDetallada.consultaDetalle("TINFODET" + trama);
			String jsonSalida = armarJSONSalida(resBeanEjecTranDAO.getTramaRespuesta());
			debug(String.format("TRAMA RECEIVE: [%s]", resBeanEjecTranDAO.getTramaRespuesta()));
			debug(String.format("JSON SALIDA: [%s]", jsonSalida));
			validaResponse(resBeanEjecTranDAO);
			resEntradaStringJsonDTO.setJson(jsonSalida);
		} else {// En caso de error se informa la excepcion de negocio
			resEntradaStringJsonDTO.setJson(trama);
			// Se monitorea error de negocio
			setMetricaErrorNegocio(metricsE.getCodeError(), metricsE.getMsgError()+" "+metricsE.getTraza());
		}
		//Se retorna respuesta a la capa de servicio
		return resEntradaStringJsonDTO;
	}
	
	/**
	 * Valida si el dato es vacio o nulo.
	 *
	 * @param dato            Dato a validar
	 * @return Booleano indicando si el dato es vacio o no
	 */
	private boolean esVacio(String dato) {
		return dato == null || "".equals(dato.trim());
	}

	/**
	 * Obtiene el valor de un dato en el JSON de entrada.
	 *
	 * @param jsonKey            Dato a buscar
	 * @param jsonObject            JSON object en el cual se realiza la busqueda
	 * @return Valor del dato
	 */
	private String obtenerValorJsonKey(String jsonKey, JSONObject jsonObject) {
		String jValue;
		try {
			jValue = jsonObject.getString(jsonKey);
		} catch (JSONException e) {
			LOGGER.error(e);
			jValue = "";
		}
		return jValue;
	}

	/**
	 * Arma la trama de entrada a la TXN TINFODET.
	 *
	 * @param jsonObject            JSON Object de entrada al BO
	 * @return Trama a enviar
	 */
	private String armarTrama(JSONObject jsonObject) {
		// Se crea objeto para armado de trama
		StringBuilder sb = new StringBuilder();
		//Auxiliar para respuesta SONAR elimina returns
		String response = null;
		//Bandera de error
		boolean error = false;
		// Armando la trama con el arreglo de items
		for (String strItems : ITEMS_TRAMA_MQ) {
			String[] items = strItems.split(SEPARADOR);
			String jsonKey = items[CERO];
			String flagValidacion = items[2];
			String jsonValor = obtenerValorJsonKey(jsonKey, jsonObject);
			String flagValoresDependencia = (items.length == 4) ? items[3] : "";

			// Validar cada dato en el JSON entrante
			if (!"Opcional".equals(flagValidacion)) {
				if (("Obligatorio".equals(flagValidacion) && esVacio(jsonValor))
						|| ("Obligatorio".equals(flagValidacion)
								&& (!Arrays.asList(flagValoresDependencia.split(",")).contains(jsonValor)))) {
					setMetricaErrorNegocio("ED00112V",String.format(MSG_ERR_ED00112V, jsonKey, flagValoresDependencia));
					response = String.format(MSG_ERR_ED00112V, jsonKey, flagValoresDependencia);
					error = true;
				}
				if ("Dependiente".equals(flagValidacion)) {
					String jValueDep = obtenerValorJsonKey(flagValoresDependencia, jsonObject);
					if (esVacio(jsonValor) && esVacio(jValueDep)) {
						setMetricaErrorNegocio("ED00113V",String.format(MSG_ERROR_ED00113V, jsonKey, flagValoresDependencia));
						response = String.format(MSG_ERROR_ED00113V,jsonKey, flagValoresDependencia);
						error = true;
					}
				}
			} else if ((!esVacio(jsonValor)) && (!esVacio(flagValoresDependencia)
					&& !Arrays.asList(flagValoresDependencia.split(",")).contains(jsonValor))) {
				setMetricaErrorNegocio("ED00112V",String.format(MSG_ERR_ED00112V, jsonKey, flagValoresDependencia));
				response = String.format(MSG_ERR_ED00112V, jsonKey, flagValoresDependencia);
				error =  true;
			}
			sb.append(String.format("%1$" + items[1] + "s", jsonValor));
		}
		if(!error){
			response = sb.toString();
		}
		//Regresa la trama de peticion / en caso de error retorna la trama con la especificacion del error
		return response;
	}

	/**
	 * Arma el JSON de salida.
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * Se modifica metodo ya que cuando el servicio mq se queda en timeout
	 * la respuesta no contiene una trama de respuesta provocando la excepcion
	 * StringIndexOutOfBoundsException al intentar ejecutar (tramaRespuesta.substring(CERO, longitud))
	 *
	 * @param tramaRespuesta Trama de respuesta de MQ
	 * @return JSON de salida
	 */
	private String armarJSONSalida(String tramaRespuesta) {
		//Se crea objeto para armar trama de respuesta
		StringBuilder sb = new StringBuilder();
		//[ADD: JMFR CSA - Correccion vulnerabilidad, se valida si la trama contiene datos para evitar nullpointer]
		if(!esVacio(tramaRespuesta)){
			sb.append('{');
			for (int i = 0; i < ITEMS_JSON_SALIDA.length; i++) {
				String[] items = ITEMS_JSON_SALIDA[i].split(SEPARADOR);
				int longitud = Integer.parseInt(items[1]);
				String jsonValor = tramaRespuesta.substring(CERO, longitud);
				if (i > 0) {
					sb.append(',');
				}
				tramaRespuesta = tramaRespuesta.substring(longitud);
				sb.append(items[CERO]).append(":\"").append(jsonValor.trim()).append('"');
			}
			sb.append('}');
		}else{// en caso de error retorna codigo de error para metrics
			sb.append("DELTA999");
		}
		//Regresa trama de respuesta
		return sb.toString();
	}
	
	/**
	 * Metodo para insertar error monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error 
	 * @param msgError mensaje de error
	 * @param traza traza completa del error
	 */
	private void insertaError(String codeError, String msgError, String traza){
		senderInit.initMetrics();
		metricsBo.bitacorizaError(senderInit.getDTOError(codeError,msgError,traza));
	}
	
	/**
	 * Metodo para asignar datos de error para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error
	 * @param traza traza completa del error
	 */
	private void setMetricaErrorNegocio(String codeError, String traza){
		metricsE.setCodeError(EnumMetricsErrors.DELTA_NEGOCIO_BO.getCodeError());
		metricsE.setMsgError(EnumMetricsErrors.DELTA_NEGOCIO_BO.getMensaje()+
				BOConsultaDetalladaImpl.class.getCanonicalName() + " " + codeError);
		metricsE.setTraza(TRAMA_INCORRECTA + " " +traza);
		insertaError(metricsE.getCodeError(), metricsE.getMsgError(), metricsE.getTraza());
	}
	
	/**
	 * Metodo para validar respuesta del servicio
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param resBeanEjecTranDAO respuesta del servicio
	 */
	private void validaResponse(ResBeanEjecTranDAO resBeanEjecTranDAO){
		String codeError = resBeanEjecTranDAO.getCodError();
		info(BOConsultaDetalladaImpl.class.getCanonicalName()+ " Codigo de error a validar: " + codeError);
		if(!ResBeanEjecTranDAO.COD_EXITO.equals(codeError)){
			insertaError(EnumMetricsErrors.DELTA_INFRA_BO.getCodeError(), 
					EnumMetricsErrors.DELTA_INFRA_BO.getMensaje()+
					BOConsultaDetalladaImpl.class.getCanonicalName()+ " " + codeError,
					resBeanEjecTranDAO.getMsgError());
			info("Se monitorea error critico en capa de negocio, componente: " + BOConsultaDetalladaImpl.class.getCanonicalName());
		}
	}
}