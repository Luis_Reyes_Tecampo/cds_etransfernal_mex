/**
 * 
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * 
 * Todos los derechos reservados
 * 
 * Clase: 
 * BOMttoMediosAutorizadosImpl.java
 * 
 * Control de versiones:
 * 
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/09/2018 05:05:44 PM Alfonso Anaya. Isban Creacion
 * 
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosAutorizados;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;


/**
 * Class BOMttoMediosAutorizadosImpl.
 *
 * Implementacion del flujo de negocio 
 * para el catalogo de Medios Autorizados.
 * 
 * @author FSW-Vector
 * @since 26/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMttoMediosAutorizadosImpl extends Architech implements BOMttoMediosAutorizados {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5527316492831732004L;

	/**
	 * La variable que contiene informacion con respecto a: dao mtto medios
	 * autorizados.
	 */
	@EJB
	private DAOMttoMediosAutorizados daoMttoMediosAutorizados;

	/** La constante QUERY_MEDIOS. */
	private static final String QUERY_MEDIOS = "CVE_MEDIO_ENT";

	/** La constante QUERY_TRASFERENCIAS. */
	private static final String QUERY_TRASFERENCIAS = "CVE_TRANSFE";

	/** La constante QUERY_OEPRACIONES. */
	private static final String QUERY_OEPRACIONES = "CVE_OPERACION";

	/** La constante QUERY_MECANISMOS. */
	private static final String QUERY_MECANISMOS = "CVE_MECANISMO";

	/** La constante QUERY_HORARIOS. */
	private static final String QUERY_HORARIOS = "CVE_HORARIO";

	/** La constante QUERY_CANALES. */
	private static final String QUERY_CANALES = "CVE_CANAL";

	/** La constante QUERY_INHAB. */
	private static final String QUERY_INHAB = "CVE_INHAB"; 
	
	/** La constante UPDATE. */
	private static final String UPDATE = "CVE_CLACON,FLG_INHAB,CVE_CLACON_TEF,IMPORTE_MAX,SEGURO,SUC_OPERANTE,HORA_INICIO,HORA_CIERRE,TM.CERRADO_ABIERTO";
	/** La constante INSERT. */
	private static final String INSERT = "U_VERSION, CVE_CLACON, SUC_OPERANTE, HORA_INICIO,HORA_CIERRE, CERRADO_ABIERTO, FLG_INHAB"
			+", CVE_CLACON_TEF,SEGURO, IMPORTE_MAX,CVE_MEDIO_ENT, CVE_TRANSFE, CVE_OPERACION, CVE_MECANISMO";
	/** La constante SELECT. */
	private static final String SELECT = "CVE_MEDIO_ENT, CVE_TRANSFE, CVE_OPERACION, CVE_MECANISMO";
	

	/** La constante CONST_QUERY_CVE_MEDIO_ENT. */
	private static final String CONST_QUERY_CVE_MEDIO_ENT = ", CVE_MEDIO_ENT=";
	
	/** La constante CONST_QUERY_CVE_TRANSFE. */
	private static final String CONST_QUERY_CVE_TRANSFE = ", CVE_TRANSFE=";
	
	/** La constante CONST_QUERY_CVE_MECANISMO. */
	private static final String CONST_QUERY_CVE_MECANISMO = ", CVE_MECANISMO=";
	
	/** La constante CONST_QUERY_CVE_OPERACION. */
	private static final String CONST_QUERY_CVE_OPERACION = ", CVE_OPERACION=";
	

	/**
	 * -------------------------------------------- Datos para las Pistas Auditoras
	 * --------------------------------------------.
	 */
	/** Nombre de la Tabla */
	private static final String STR_NOMTABLA = "TRAN_MEDIO_AUT";

	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitAdministrativa daoBitacora;

	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;

	/** Referencia al dao de perfilamiento. */
	@EJB
	private DAOExportar daoExportar;

	/**
	 * Consulta tabla MA.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean the session bean --> El objeto session
	 * @param beanAuto El objeto: bean auto
	 * @return the list --> Objeto de respuesta de la consulta principal
	 * @throws BusinessException the business exception --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes consultaTablaMA(BeanMttoMediosAutoBusqueda bean,
			ArchitechSessionBean sessionBean, BeanMttoMediosAutorizados beanAuto) throws BusinessException {
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();

		BeanPaginador pag = bean.getBeanPaginador();
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanMttoMediosAutorizadosRes.setBeanPaginador(pag);

		beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaTablaMA(bean, sessionBean,beanAuto);

		if (beanMttoMediosAutorizadosRes.getCodError().equals(Errores.OK00000V)
				|| beanMttoMediosAutorizadosRes.getCodError().equals(Errores.ED00011V)) {
			/** Se calcula el regitro por pagina **/
			pag.calculaPaginas(beanMttoMediosAutorizadosRes.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanMttoMediosAutorizadosRes.setBeanPaginador(pag);

		} else {
			throw new BusinessException(beanMttoMediosAutorizadosRes.getCodError(),
					beanMttoMediosAutorizadosRes.getMsgError());
		}
		/**devuelve el resultado de la operacion**/
		return beanMttoMediosAutorizadosRes;
	}

	/**
	 * Llena listas.
	 *
	 * @param sesion the sesion  --> El objeto session
	 * @return the bean mtto medios autorizados res --> Objeto de respuesta con la informacion de listas
	 * @throws BusinessException the business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes llenaListas(ArchitechSessionBean sesion) throws BusinessException {
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = null;
		Date timeIni = null;
		long ltimeIni = 0;
		long ltimeFin = 0;
		synchronized (Constantes.LOCK) {
			boolean flag = true;
			if (Constantes.CACHE.containsKey("CONSULTA")) {
				timeIni = (Date) Constantes.CACHE.get("TIME");
				ltimeIni = timeIni.getTime();
				ltimeFin = System.currentTimeMillis();
				if ((ltimeFin - ltimeIni) < 300000) {
					flag = false;
				}
			}
			if (flag) {
				String query = null;
				beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();
				query = QUERY_MEDIOS;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_TRASFERENCIAS;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_OEPRACIONES;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_MECANISMOS;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_HORARIOS;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_CANALES;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);
				query = QUERY_INHAB;
				beanMttoMediosAutorizadosRes = daoMttoMediosAutorizados.consultaListas(beanMttoMediosAutorizadosRes,
						query, sesion);

			} else {
				beanMttoMediosAutorizadosRes = (BeanMttoMediosAutorizadosRes) Constantes.CACHE.get("CONSULTA");
				Constantes.CACHE.put("TIME", new Date());
			}
			/**devuelve el resultado de la operacion**/
			return beanMttoMediosAutorizadosRes;
		}
	}

	/**
	 * Adds the mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req --> Objeto de entrada con los datos del nuevo registro
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes guardarNuevoMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			ArchitechSessionBean sesion) throws BusinessException {
		BeanMttoMediosAutorizadosRes result = new BeanMttoMediosAutorizadosRes();

		String urlController = "modMttoMedios.do";
		
		String dtoNuevo = obtenerDtos (beanMttoMediosAutorizadosReq,ConstantesMttoMediosAut.TYPE_INSERT) ;		
		result = daoMttoMediosAutorizados.operacionesMttoMedios(beanMttoMediosAutorizadosReq,
				ConstantesMttoMediosAut.TYPE_INSERT, sesion);
		if (!result.getCodError().equals(Errores.ED00130V)) {
			if (!result.getCodError().equals(Errores.OK00000V)) {
				// Cuando ocurre un error en la transaccion
				pistasAuditoras(ConstantesMttoMediosAut.TYPE_INSERT, urlController, false, sesion,dtoNuevo,"",INSERT);
				throw new BusinessException(result.getCodError(), result.getMsgError());
			} else {
				pistasAuditoras(ConstantesMttoMediosAut.TYPE_INSERT, urlController, true, sesion,dtoNuevo,"",INSERT);
			}
		}else {
			// Cuando ocurre un error en la transaccion
			pistasAuditoras(ConstantesMttoMediosAut.TYPE_INSERT, urlController, false, sesion,dtoNuevo,"",INSERT);
		}
		
		/**devuelve el resultado de la operacion**/
		return result;
	}

	/**
	 * Del mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes delMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			ArchitechSessionBean sesion) throws BusinessException {
		BeanMttoMediosAutorizadosRes result = new BeanMttoMediosAutorizadosRes();
		String operacion = "DELETE";
		String urlController = "delMttoMedios.do";
		
		/** consulta los datos seleccionados **/		
		result = daoMttoMediosAutorizados.operacionesMttoMedios(beanMttoMediosAutorizadosReq,
				ConstantesMttoMediosAut.QUERY_DELETE, sesion);
		
		
		if (!result.getCodError().equals(Errores.OK00004V)) {
			for(BeanMttoMediosAutorizados ant:beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()){	
				if(ant.isSeleccionado()){  
					String dtoAnt ="U_VERSION="+ant.getVersion()+CONST_QUERY_CVE_MEDIO_ENT+ant.getCveMedioEnt()+CONST_QUERY_CVE_TRANSFE+ant.getCveTransfe()+
							CONST_QUERY_CVE_OPERACION+ant.getCveOperacion()+CONST_QUERY_CVE_MECANISMO+ant.getCveMecanismo();
					/**Cuando ocurre un error en la transaccion **/
					pistasAuditoras(operacion, urlController, false, sesion,"",dtoAnt,SELECT);
				}
			}
			throw new BusinessException(result.getCodError(), result.getMsgError());
		} else {
			
			for(BeanMttoMediosAutorizados ant:beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()){	
				if(ant.isSeleccionado()){  
					String dtoAnt ="U_VERSION="+ant.getVersion()+CONST_QUERY_CVE_MEDIO_ENT+ant.getCveMedioEnt()+CONST_QUERY_CVE_TRANSFE+ant.getCveTransfe()+
							CONST_QUERY_CVE_OPERACION+ant.getCveOperacion()+CONST_QUERY_CVE_MECANISMO+ant.getCveMecanismo();
					/**Cuando ocurre un error en la transaccion **/
					pistasAuditoras(operacion, urlController, true, sesion,"",dtoAnt,SELECT);
				}
			}
			
		}
		
		
		return result;
	}

	/**
	 * Mod mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req --> Objeto de entrada conla informacion del registro
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes modificarMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			ArchitechSessionBean sesion) throws BusinessException {
		BeanMttoMediosAutorizadosRes result = new BeanMttoMediosAutorizadosRes();

		String urlController = "modMttoMedios.do";
	
		BeanMttoMediosAutorizadosReq bean = new BeanMttoMediosAutorizadosReq();
		bean.setCveMecanismo(beanMttoMediosAutorizadosReq.getCveMecanismo());
		bean.setCveTransfe(beanMttoMediosAutorizadosReq.getCveTransfe());
		bean.setCveOperacion(beanMttoMediosAutorizadosReq.getCveOperacion());
		bean.setCveMedioEnt(beanMttoMediosAutorizadosReq.getCveMedioEnt());	
		BeanMttoMediosAutoBusqueda beanBus =new BeanMttoMediosAutoBusqueda();
		BeanPaginador pag = beanBus.getBeanPaginador();
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanBus.setBeanPaginador(pag);
		
		
		BeanMttoMediosAutorizadosRes res = daoMttoMediosAutorizados.consultaTablaMA(beanBus, sesion, bean);
		
		String dtoAnt = obtenerDtos(res.getListaBeanMttoMedAuto().get(0),ConstantesMttoMediosAut.TYPE_UPDATE);
		String dtoNuevo = obtenerDtos(beanMttoMediosAutorizadosReq,ConstantesMttoMediosAut.TYPE_UPDATE);
		
		result = daoMttoMediosAutorizados.operacionesMttoMedios(beanMttoMediosAutorizadosReq,
				ConstantesMttoMediosAut.TYPE_UPDATE, sesion);

		if (!result.getCodError().equals(Errores.OK00000V)) {
			/**Cuando ocurre un error en la transaccion**/
			pistasAuditoras(ConstantesMttoMediosAut.TYPE_UPDATE, urlController, false, sesion,dtoNuevo,dtoAnt,UPDATE);
			throw new BusinessException(result.getCodError(), result.getMsgError());

		} else {
			pistasAuditoras(ConstantesMttoMediosAut.TYPE_UPDATE, urlController, true, sesion,dtoNuevo,dtoAnt,UPDATE);
		}
		/**devuelve el resultado de la operacion**/
		return result;
	}
	
	/**
	 * funcion para obtener los datos ant y nuevos.
	 *
	 * @param ant --> Dato anterior
	 * @param accion --> Accion realizada
	 * @return dato tipo String
	 */
	private String obtenerDtos (BeanMttoMediosAutorizadosReq ant, String accion) {
		String dtoAnt ="";
		if(ConstantesMttoMediosAut.TYPE_UPDATE.equals(accion)) {
			dtoAnt =" CVE_CLACON="+ant.getCveCLACON()+" ,FLG_INHAB="+ant.getFlgInHab()+",CVE_CLACON_TEF="+ant.getCveCLACONTEF()+
					",IMPORTE_MAX="+ant.getLimiteImporte()+",SEGURO="+ant.getCanal()+",SUC_OPERANTE="+ant.getSucOperante()+
					",HORA_INICIO="+ant.getHoraInicio()+",HORA_CIERRE="+ant.getHoraCierre()+",TM.CERRADO_ABIERTO="+ant.getHorarioEstatus();
		}
		
		if(ConstantesMttoMediosAut.TYPE_INSERT.equals(accion)) {
			dtoAnt =" CVE_CLACON="+ant.getCveCLACON()+" ,FLG_INHAB="+ant.getFlgInHab()+",CVE_CLACON_TEF="+ant.getCveCLACONTEF()+
					",IMPORTE_MAX="+ant.getLimiteImporte()+",SEGURO="+ant.getCanal()+",SUC_OPERANTE="+ant.getSucOperante()+
					",HORA_INICIO="+ant.getHoraInicio()+",HORA_CIERRE="+ant.getHoraCierre()+",TM.CERRADO_ABIERTO="+ant.getHorarioEstatus()+
					",U_VERSION="+ant.getVersion()+CONST_QUERY_CVE_MEDIO_ENT+ant.getCveMedioEnt()+CONST_QUERY_CVE_TRANSFE+ant.getCveTransfe()+
					CONST_QUERY_CVE_OPERACION+ant.getCveOperacion()+CONST_QUERY_CVE_MECANISMO+ant.getCveMecanismo();
		}
		
		/**devuelve el resultado de la operacion**/		
		return dtoAnt;
	}
	

	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param operacion El objeto: operacion --> tipo de operacion realizada
	 * @param urlController El objeto: url controller --> .do donde se dispara la accion
	 * @param resOp El objeto: res op --> Respuesta de la operacion
	 * @param sesion El objeto: sesion --> Objeto session
	 * @param dtoNuevo El objeto: dto nuevo
	 * @param dtoAnterior El objeto: dto anterior
	 * @param dtoModificado El objeto: dto modificado
	 */
	private void pistasAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion, String dtoNuevo,String dtoAnterior, String dtoModificado) {
		// Objetos para las Pistas auditoras
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		// Enviamos los datos de la pista auditora
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(QUERY_MEDIOS);
		beanPista.setDatoModi(dtoModificado);
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		// Cuando la operacion es correcta
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

	/**
	 * mostrarRegistroEditarMttMedios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req  --> Objeto de entrada conla informacion de los registros
	 * @param sesion the sesion --> El objeto session
	 * @param accion El objeto: accion --> Parametro de tipo de operacion
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes mostrarRegistroEditarMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			ArchitechSessionBean sesion, String accion) throws BusinessException {
		BeanMttoMediosAutorizadosRes result = new BeanMttoMediosAutorizadosRes();
		String urlController = "editMttoMedios.do";
		String dtoAnt =obtenerDtos ( beanMttoMediosAutorizadosReq, ConstantesMttoMediosAut.TYPE_SELECT);	
		String dtoNuevo = "";
		
		result = daoMttoMediosAutorizados.mostrarRegistro(beanMttoMediosAutorizadosReq, sesion, accion);
		if (!result.getCodError().equals(Errores.OK00000V)) {
			// Cuando ocurre un error en la transaccion
			pistasAuditoras(ConstantesMttoMediosAut.TYPE_SELECT, urlController, false, sesion,dtoNuevo,dtoAnt,SELECT);

			throw new BusinessException(result.getCodError(), result.getMsgError());

		}

		pistasAuditoras(ConstantesMttoMediosAut.TYPE_SELECT, urlController, true, sesion,dtoNuevo,dtoAnt,SELECT);
		/**devuelve el resultado de la operacion**/
		return result;
	}

	/**
	 * Obtener datos.
	 *
	 * @param beanPaginador the bean paginador --> OBjeto con informacion del paginado
	 * @param sesion the sesion --> El objeto session
	 * @param sortField the sort field -->Parametro de ordenamiento
	 * @param sortType the sort type  -->Parametro de tipo ordenamiento 
	 * @param beanAuto El objeto: bean auto --> Objeto de informacion de entrada
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException the business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes obtenerDatos(BeanPaginador beanPaginador, ArchitechSessionBean sesion,
			String sortField, String sortType,BeanMttoMediosAutorizados beanAuto) throws BusinessException {

		BeanMttoMediosAutorizadosRes beanMttoRes = new BeanMttoMediosAutorizadosRes();
		String type = "";
		BeanPaginador pag = beanPaginador;
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		if (sortType == null) {
			type = "ASC";
		}else {
			type= sortType;
		}
		
		beanMttoRes = daoMttoMediosAutorizados.obtenerDatos(beanPaginador, sesion, sortField, type, beanAuto);

		if (beanMttoRes.getListaBeanMttoMedAuto() == null) {
			beanMttoRes.setListaBeanMttoMedAuto(new ArrayList<BeanMttoMediosAutorizadosReq>());
		}
		/**devuelve el resultado de la operacion**/
		return beanMttoRes;
	}

	/**
	 * Exportar todos.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados req
	 * @param total El objeto: total --> Parametro con el total de registros obtenidos
	 * @return Objeto bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	@Override
	public BeanMttoMediosAutorizadosRes exportarTodo(ArchitechSessionBean sessionBean,
			BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, int total) throws BusinessException {
		BeanMttoMediosAutorizadosRes beanRes = new BeanMttoMediosAutorizadosRes();
		BeanExportarTodo beanExp = new BeanExportarTodo();
		beanExp.setColumnasReporte(ConstantesMttoMediosAut.COLUMNAS_CONSULTA_CAPTURAS_MANUALES);

		String query = ConstantesMttoMediosAut.QUERY_EXPORTAR;
		beanExp.setConsultaExportar(query);
		beanExp.setModulo("catalogos");
		beanExp.setSubModulo("Mantenimiento de Medios Autorizados");
		beanExp.setNombreRpt("MttoMedioAuto_");
		beanExp.setTotalReg(String.valueOf(total));
		beanExp.setEstatus("PE");

		BeanResBase result = daoExportar.exportarTodo(beanExp, sessionBean);
		if (Errores.EC00011B.equals(result.getCodError())) {
			beanRes.setCodError(result.getCodError());
			beanRes.setMsgError(result.getMsgError());
			beanRes.setTipoError(result.getTipoError());
			throw new BusinessException(result.getCodError(), result.getMsgError());

		}
		beanRes.setCodError(result.getCodError());
		beanRes.setMsgError(result.getMsgError());
		beanRes.setTipoError(result.getTipoError());
		/**devuelve el resultado de la operacion**/
		return beanRes;
	
	}

}
