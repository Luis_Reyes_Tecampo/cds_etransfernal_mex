package mx.isban.eTransferNal.servicio.capturasManuales;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpeDAO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOAutorizarOperaciones;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * The Class BOAutorizarOperacionesImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAutorizarOperacionesImpl extends Architech implements BOAutorizarOperaciones {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 1340019404904440128L;

	/** The Constant UPDATE. */
	private static final String UPDATE = "UPDATE";
	
	/** The Constant OK. */
	private static final String OK = "OK";
	
	/** The Constant NOK. */
	private static final String NOK = "NOK";
	
	/** The Constant ID_FOLIO. */
	private static final String ID_FOLIO = "ID_FOLIO";
	
	/** The Constant TRAN_CAP_OPER. */
	private static final String TRAN_CAP_OPER = "TRAN_CAP_OPER";
	
	/** The Constant OPERACION_APARTADA. */
	private static final String OPERACION_APARTADA = "OPERACION_APARTADA";
	
	/** The Constant USR_APARTA. */
	private static final String USR_APARTA = "USR_APARTA";
	
	/** The Constant Estatus. */
	private static final String ESTATUS = "ESTATUS";
	
	/** The Constant UsrAutoriza. */
	private static final String USR_AUTORIZA = "USR_AUTORIZA";

	/** The Constant MONTO. */
	private static final String MONTO = "IMPORTE_CARGO";
	
	/** The Constant CTA_ORD. */
	private static final String CTA_ORD = "NUM_CUENTA_ORD";
	
	/** The Constant CTA_REC. */
	private static final String CTA_REC = "NUM_CUENTA_REC";
	
	/** The Dao autorizar operaciones. */
	@EJB
	private DAOAutorizarOperaciones daoAutorizarOperaciones;
	
	/** The dao pista auditoria. */
	@EJB
	private DAOBitAdministrativa daoPistaAuditoria;
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#consultarOperacionesPendientes(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Consultar operaciones pendientes.
	 *
	 * @param sessionBean the session bean
	 * @param req the req
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe consultarOperacionesPendientes(
			ArchitechSessionBean sessionBean, BeanReqAutorizarOpe req)throws BusinessException{
		BeanResAutorizarOpe response=new BeanResAutorizarOpe();
		BeanResAutorizarOpeDAO responseDAO=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpeDAO responseListOpeApartPagina=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpeDAO responseListOpeApart=new BeanResAutorizarOpeDAO();
		List<BeanAutorizarOpe> listOpePendAutorizar;
		boolean hayOperacionesApartadas = false;
		boolean hayOpeApartPorUsr = false;

		// se crea el objeto bean paginador
	 	BeanPaginador paginador = null;
	    paginador = req.getPaginador();

	    //si paginador es null
	    if( paginador == null ){
	    	//se inicializa paginador
	    	paginador = new BeanPaginador();
	    	req.setPaginador(paginador);
	    }
	    
	    //agregar numero registros por pagina
    	paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

    	//se consultan las operaciones de acuerdo a los filtros proporcionados
		responseDAO = daoAutorizarOperaciones.consultarOperacionesPendientes(sessionBean, req);
		listOpePendAutorizar = responseDAO.getListOperacionesPendAutorizar();
		//se consulta si existen operaciones apartadas en la pagina actual
		responseListOpeApartPagina = daoAutorizarOperaciones.consultaOperacionesApartadasPorPagina(
				req, new ArrayList<Object>(), sessionBean);
		//se consultan las operaciones apartadas por el usuario logado
		responseListOpeApart = daoAutorizarOperaciones.consultaOperacionesApartadas(
				req, new ArrayList<Object>(), sessionBean);
		
		//si hayOperacionesApartadas en la pagina actual
		hayOperacionesApartadas = verificarOpeApartadasPaginaActual(responseListOpeApartPagina);
	    
		//si no hay operaciones pendientes
	    if(Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError()) && listOpePendAutorizar.isEmpty()){
	    	//se envia error "no se encontro informacion"
	    	response.setCodError(Errores.ED00011V);
	    	response.setTipoError(Errores.TIPO_MSJ_ERROR);
	    	response.setMsgError(Errores.DESC_ED00011V);

		//si hay operaciones pendientes
	    }else if(Errores.CODE_SUCCESFULLY.equals(responseDAO.getCodError())){
		    paginador.calculaPaginas(responseDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
	    	response.setTotalReg(responseDAO.getTotalReg());
	    	response.setListOperacionesPendAutorizar(listOpePendAutorizar);
	    	//se setea boleano operacionesApartadas en pagina actual
	    	response.setOperacionesApartadasPorPagina( hayOperacionesApartadas );
	    	if( Errores.CODE_SUCCESFULLY.equals(responseListOpeApart.getCodError()) ){
	    		//si hay operaciones apartadas por el usuario logeado
	    		if( !responseListOpeApart.getListOperacionesApartadas().isEmpty() ){
	    			hayOpeApartPorUsr = true;
	    		} 
		    	//se setea boleano operacionesApartadas por usuario logeado
		    	response.setOperacionesApartadasUsr( hayOpeApartPorUsr );
		    	//se envia codigoError OK
			    response.setCodError(Errores.OK00000V);
			    response.setTipoError(Errores.TIPO_MSJ_INFO);
	    	} else if( Errores.EC00011B.equals(responseListOpeApart.getCodError()) ){
	    		throw new BusinessException(responseListOpeApart.getCodError(), responseListOpeApart.getMsgError());
	    	}
		    response.setPaginador(paginador);
		//si hubo un error al ejecutar el query
	    }else {
	    	// se arroja codigo de error EC00011B
	    	throw new BusinessException(responseDAO.getCodError(), responseDAO.getMsgError());
	     }
	    
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#apartarOPeraciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Apartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param listRegistrosSeleccionados the list registros seleccionados
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe apartarOPeraciones(ArchitechSessionBean sessionBean,
			List<BeanAutorizarOpe> listRegistrosSeleccionados )throws BusinessException{
		BeanResAutorizarOpeDAO responseApartarOpe=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int opeApartada=0;

		// se recorre la lista de operaciones seleccionadas
		for(BeanAutorizarOpe elemento:listRegistrosSeleccionados){
			String folio=elemento.getFolio();
			//se realiza apartado de operacion
			responseApartarOpe = daoAutorizarOperaciones.apartarOPeraciones(sessionBean, folio);
			//si operacion apartada correctamente
			if( Errores.CODE_SUCCESFULLY.equals(responseApartarOpe.getCodError()) ){
    			//se incrementa contador operaciones apartadas
				opeApartada++;
				//construccion bean pistasAuditoria
		    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, UPDATE, "apartar.do",
		    			OPERACION_APARTADA+"=0, "+USR_APARTA+"=null",					//valor anterio
						OPERACION_APARTADA+"=2, "+USR_APARTA+"="+sessionBean.getUsuario(),	//valor nuevo
						ID_FOLIO, ID_FOLIO, "APARTAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando ok
		    	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	} else {
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, UPDATE, "apartar.do",
						" ",
						" ",
						ID_FOLIO, ID_FOLIO, "APARTAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando nok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	}
		}
		
		//si contador operacionesApartadas es igual al tamaño lista operacionesApartadas
		if( opeApartada == listRegistrosSeleccionados.size() ){
			//se muestra mensaje de apartado exitoso
			responseBO.setMsgError("Las operaciones seleccionadas han sido apartadas correctamente");
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			//se obtiene y arroja codigo de error de apartarOperacion
	    	throw new BusinessException(responseApartarOpe.getCodError(), responseApartarOpe.getMsgError());
		}
		
		return responseBO;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#desapartarOPeraciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Desapartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param req the req
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe desapartarOPeraciones(ArchitechSessionBean sessionBean,
			BeanReqAutorizarOpe req)throws BusinessException{
		BeanResAutorizarOpeDAO responseListOpeApart=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpeDAO responseDesapartarOpe=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		int opeDesapartada=0;

		//se consulta operaciones apartadas por usuario logeado
		responseListOpeApart = daoAutorizarOperaciones.consultaOperacionesApartadas(req, new ArrayList<Object>(), sessionBean);
		List<BeanAutorizarOpe> operacionesApartadas = responseListOpeApart.getListOperacionesApartadas();

		//si consulta operaciones apartadas ejecutado exitosamente
		if( Errores.CODE_SUCCESFULLY.equals(responseListOpeApart.getCodError()) ){
			// se recorre la lista de operaciones seleccionadas
	    	for(BeanAutorizarOpe elemento : operacionesApartadas){
	    		String folio = elemento.getFolio();
	    		
	    		//si la pista es creada correctamente, el metodo retorna 1, en caso contrario retorna 0
	    		opeDesapartada += generarPistaAuditoriaOperacionLiberada(folio, sessionBean);
	    	}
		}

		//si contador operacionesDesapartadas es igual al tamaño lista operacionesDesapartadas
		if( opeDesapartada == operacionesApartadas.size() ){
			//se muestra mensaje de desapartado exitoso
			responseBO.setMsgError("Se han liberado correctamente todas las operaciones apartadas por su usuario");
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			//se obtiene y arroja codigo de error de desapartarOperacion
	    	throw new BusinessException(responseDesapartarOpe.getCodError(), responseDesapartarOpe.getMsgError());
		}
    	
		return responseBO;
	}


	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#liberarOperacionAutorizada(mx.isban.agave.commons.beans.ArchitechSessionBean, java.util.List)
	 */
	@Override
	public void liberarOperacionAutorizada(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listOpeSelec) throws BusinessException {
		BeanResAutorizarOpeDAO responseOperacionLiberada=new BeanResAutorizarOpeDAO();
		int opeDesapartada=0;

		// se recorre la lista de operaciones seleccionadas
    	for(BeanAutorizarOpe elemento : listOpeSelec){
    		String folio = elemento.getFolio();
    		
    		//si la pista es creada correctamente, el metodo retorna 1, en caso contrario retorna 0
    		opeDesapartada += generarPistaAuditoriaOperacionLiberada(folio, sessionBean);
    	}

		//si contador operacionesDesapartadas es igual al tamaño lista operacionesDesapartadas
		if( opeDesapartada != listOpeSelec.size() ){
			//se obtiene y arroja codigo de error de desapartarOperacion
	    	throw new BusinessException(responseOperacionLiberada.getCodError(), responseOperacionLiberada.getMsgError());
		}
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#autorizarOperaciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Autorizar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe autorizarOperaciones(ArchitechSessionBean sessionBean,
			List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException{
		BeanResAutorizarOpeDAO beanAutorizarOpe=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int opeAutorizada=0;
		String usrAutoriza="";
		String folio;

		// se recorre la lista de operaciones seleccionadas
    	for(BeanAutorizarOpe elemento : listOperacionesSeleccionadas){
    		folio=elemento.getFolio();
			//se autoriza la operacion
    		beanAutorizarOpe = daoAutorizarOperaciones.autorizarOperaciones(sessionBean, folio);
    		
    		if( Errores.CODE_SUCCESFULLY.equals(beanAutorizarOpe.getCodError()) ){
    			//se incrementa contador operaciones autorizadas
    			opeAutorizada++;
    			//se obtiene valor de usuario que Autoriza
    			if( "".equals(elemento.getUsrAutoriza()) ){
    				usrAutoriza="null";
    			} else {
    				usrAutoriza=elemento.getUsrAutoriza();
    			}
				//construccion bean pistasAuditoria
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, UPDATE, "autorizarOperacion.do",
	        			ESTATUS+"="+elemento.getEstatus()+", "+USR_AUTORIZA+"="+ usrAutoriza+", "+	//valor anterior
	        			MONTO+"= "+elemento.getMonto()+", "+CTA_ORD+"= "+elemento.getCuentaOrdenante()+", "+CTA_REC+"= "+elemento.getCuentaReceptora(),	//valor anterior
	        			ESTATUS+"=AU, "+USR_AUTORIZA+"="+sessionBean.getUsuario(),	//valor nuevo
						ID_FOLIO, ID_FOLIO, "AUTORIZAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando ok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	} else {
				//construccion bean pistasAuditoria nok
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, UPDATE, "autorizarOperacion.do",
						" ",
						" ",
						ID_FOLIO, ID_FOLIO, "AUTORIZAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando nok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	}
    	}
		//si contador operacionesAutorizadas es igual al tamaño lista operacionesAutorizadas
		if( opeAutorizada == listOperacionesSeleccionadas.size() ){
			//se muestra mensaje de autorizacion exitoso
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
			responseBO.setMsgError( "Las operaciones seleccionadas han sido autorizadas" );
		} else {
			//se obtiene y arroja codigo de error de autorizacionOperacion
	    	throw new BusinessException(beanAutorizarOpe.getCodError(), beanAutorizarOpe.getMsgError());
		}

		return responseBO;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#repararOperaciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Reparar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe repararOperaciones(ArchitechSessionBean sessionBean,
			List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException{
		BeanResAutorizarOpeDAO responseRepararOpe = new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int opeRepar=0;
		String usrAutoriza="";
		String folio;

		// se recorre la lista de operaciones seleccionadas
    	for(BeanAutorizarOpe elemento : listOperacionesSeleccionadas){
    		folio = elemento.getFolio();
			//se repara la operacion
    		responseRepararOpe = daoAutorizarOperaciones.repararOperaciones(sessionBean, folio);
    		
    		if( Errores.CODE_SUCCESFULLY.equals(responseRepararOpe.getCodError()) ){
    			//se incrementa contador operaciones reparadas
    			opeRepar++;
    			//se obtiene valor de usuario que Autoriza
    			if( "".equals(elemento.getUsrAutoriza()) ){
    				usrAutoriza="null";
    			} else {
    				usrAutoriza=elemento.getUsrAutoriza();
    			}
				//construccion bean pistasAuditoria
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, UPDATE, "repararOperacion.do",
	        			ESTATUS+"="+elemento.getEstatus()+", "+USR_AUTORIZA+"="+ usrAutoriza+", "+	//valor anterior
	        			MONTO+"= "+elemento.getMonto()+", "+CTA_ORD+"= "+elemento.getCuentaOrdenante()+", "+CTA_REC+"= "+elemento.getCuentaReceptora(),	//valor anterior
	        			ESTATUS+"=RE, "+USR_AUTORIZA+"="+sessionBean.getUsuario(),				//valor nuevo
						ID_FOLIO, ID_FOLIO, "REPARAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando ok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	} else {
				//construccion bean pistasAuditoria nok
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, UPDATE, "repararOperacion.do",
						" ",
						" ",
						ID_FOLIO, ID_FOLIO, "REPARAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando nok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
	    	}
    	}

		//si contador operacionesReparadas es igual al tamaño lista operacionesReparadas
		if( opeRepar == listOperacionesSeleccionadas.size() ){
			//se muestra mensaje de reparacion exitoso
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
			responseBO.setMsgError( "Las operaciones seleccionadas han sido marcadas para Reparar" );
		} else{
			//se obtiene y arroja codigo de error de reparacionOperacion
	    	throw new BusinessException(responseRepararOpe.getCodError(), responseRepararOpe.getMsgError());
		}

    	return responseBO;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#cancelarOperaciones(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe)
	 */
	/**
	 * Cancelar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe cancelarOperaciones(ArchitechSessionBean sessionBean,
			 List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException{
		BeanResAutorizarOpeDAO responseCancelarOpe=new BeanResAutorizarOpeDAO();
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int opeCancel=0;
		String usrAutoriza="";
		String folio;

		// se recorre la lista de operaciones seleccionadas
    	for(BeanAutorizarOpe elemento : listOperacionesSeleccionadas){
    		folio = elemento.getFolio();
			//se cancela la operacion
    		responseCancelarOpe = daoAutorizarOperaciones.cancelarOperaciones(sessionBean, folio);
    		
    		if( Errores.CODE_SUCCESFULLY.equals(responseCancelarOpe.getCodError()) ){
    			//se incrementa contador operaciones canceladas
    			opeCancel++;
    			//se obtiene valor de usuario que Autoriza
    			if( "".equals(elemento.getUsrAutoriza()) ){
    				usrAutoriza="null";
    			} else {
    				usrAutoriza=elemento.getUsrAutoriza();
    			}
				//construccion bean pistasAuditoria
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, UPDATE, "cancelarOPeracion.do",
	        			ESTATUS+"="+elemento.getEstatus()+", "+USR_AUTORIZA+"="+usrAutoriza+", "+	//valor anterior
	        			MONTO+"= "+elemento.getMonto()+", "+CTA_ORD+"= "+elemento.getCuentaOrdenante()+", "+CTA_REC+"= "+elemento.getCuentaReceptora(),	//valor anterior
	        			ESTATUS+"=CA, "+USR_AUTORIZA+"="+sessionBean.getUsuario(),				//valor nuevo
						ID_FOLIO, ID_FOLIO, "CANCELAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando ok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
        	} else {
				//construccion bean pistasAuditoria nok
	        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, UPDATE, "cancelarOPeracion.do",
						" ",
						" ",
						ID_FOLIO, ID_FOLIO, "CANCELAR OPERACIONES", sessionBean);
		    	// se genera pista de auditoria cuando nok
	        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
        	}
    	}

		//si contador operacionesCanceladas es igual al tamaño lista operacionesCanceladas
		if( opeCancel == listOperacionesSeleccionadas.size() ){
			//se muestra mensaje de cancelacion exitoso
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
			responseBO.setMsgError( "Las operaciones seleccionadas han sido Canceladas" );
		} else {
			//se obtiene y arroja codigo de error de cancelacionOperacion
	    	throw new BusinessException(responseCancelarOpe.getCodError(), responseCancelarOpe.getMsgError());
		}
		
    	return responseBO;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones#consultarUsrAdmin(java.lang.String)
	 */
	/**
	 * Consultar usr admin.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	@Override
	public BeanResAutorizarOpe consultarUsrAdmin(ArchitechSessionBean sessionBean)throws BusinessException{
		BeanResAutorizarOpe responseBO=new BeanResAutorizarOpe();
		BeanResAutorizarOpeDAO responseUsrAdmin;
		//se consulta si es usuario adminstador
		responseUsrAdmin = daoAutorizarOperaciones.consultarUsrAdmin(sessionBean);
		
		// si consulta usr administador ejecutada exitosamente
		if(Errores.CODE_SUCCESFULLY.equals(responseUsrAdmin.getCodError())){
			//obtener usuarioAdmin
			responseBO.setUsrAdmin(responseUsrAdmin.getUsrAdmin());
			//obtener codigo de error de consulta usrAdmin
			responseBO.setCodError(responseUsrAdmin.getCodError());
			responseBO.setMsgError(responseUsrAdmin.getMsgError());
			responseBO.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			//se arroja nueva excepcion de negocio
			throw new BusinessException(responseUsrAdmin.getCodError(), responseUsrAdmin.getMsgError());
		}
		return responseBO;
	}

	/**
	 * Verificar ope apartadas pagina actual.
	 *
	 * @param responseListOpeApartPagina the response list ope apart pagina
	 * @return true, if successful
	 * @throws BusinessException the business exception
	 */
	private boolean verificarOpeApartadasPaginaActual(
			BeanResAutorizarOpeDAO responseListOpeApartPagina) throws BusinessException {
		boolean hayOperacionesApartadas=false;
		//si hayOperacionesApartadas en la pagina actual
		if( Errores.CODE_SUCCESFULLY.equals(responseListOpeApartPagina.getCodError()) && 
				!responseListOpeApartPagina.getListOperacionesApartadas().isEmpty() ){
			hayOperacionesApartadas = true;
		} else if(Errores.EC00011B.equals(responseListOpeApartPagina.getCodError())){
			throw new BusinessException(responseListOpeApartPagina.getCodError(), responseListOpeApartPagina.getMsgError());
		}
		return hayOperacionesApartadas;
	}
	
	/**
	 * Generar pista auditoria operacion liberada.
	 *
	 * @param responseLiberaOperacionDAO the response libera operacion dao
	 * @param sessionBean the session bean
	 * @return the int
	 */
	private int generarPistaAuditoriaOperacionLiberada(String folio, ArchitechSessionBean sessionBean){
		BeanResAutorizarOpeDAO responseDesapartarOpe=new BeanResAutorizarOpeDAO();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		int operacionLiberada = 0;

		//se realiza desapartado de operacion
		responseDesapartarOpe = daoAutorizarOperaciones.desapartarOPeraciones(sessionBean, folio);
		
		if( Errores.CODE_SUCCESFULLY.equals(responseDesapartarOpe.getCodError()) ){
			//se incrementa contador operaciones desapartadas
			operacionLiberada = 1;
			//construccion bean pistasAuditoria
        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, UPDATE, "desapartar.do",
        			OPERACION_APARTADA+"=2, "+USR_APARTA+"="+sessionBean.getUsuario(),	//valor anterior
        			OPERACION_APARTADA+"=0, "+USR_APARTA+"=null",					//valor nuevo
					ID_FOLIO, ID_FOLIO, "DESAPARTAR OPERACIONES", sessionBean);
	    	// se genera pista de auditoria cuando ok
        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
    	} else {
        	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, UPDATE, "desapartar.do",
					" ",
					" ",
					ID_FOLIO, ID_FOLIO, "DESAPARTAR OPERACIONES", sessionBean);
	    	// se genera pista de auditoria cuando nok
        	daoPistaAuditoria.guardaBitacora(beanReqInsertBitacora, sessionBean);
    	}
		return operacionLiberada;
	}
}
