/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOInstitucionesPCCdaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqInst;
import mx.isban.eTransferNal.beans.catalogos.BeanHabDesParticipanteDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanInstitucion;
import mx.isban.eTransferNal.beans.catalogos.BeanInstitucionPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarInstPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqHabDesInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResHabDesInstParticipante;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOInstitucionesPCCda;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
@Remote(BOInstitucionesPCCda.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOInstitucionesPCCdaImpl extends Architech implements BOInstitucionesPCCda {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -3860331937644548220L;
	
	/**
	 * Constante del tipo String que almacena el valor de NUM_BANXICO
	 */
	private static final String NUM_BANXICO = "NUM_BANXICO";
	
	/**
	 * Constante del tipo String que almacena el valor de TRAN_SPEI_PARTICIP
	 */
	private static final String TRAN_SPEI_PARTICIP = "TRAN_SPEI_PARTICIP";
	/**
	 * Constante del tipo String que almacena el valor de TRANSACCION EXITOSA
	 */
	private static final String TRANSACCION_EXITOSA = "TRANSACCION EXITOSA";

	/**
	 * Propiedad del tipo DAOInstitucionesPCCda que almacena el valor de dAOInstitucionesPCCda
	 */
	@EJB
	private DAOInstitucionesPCCda dAOInstitucionesPCCda;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	
	
	
  /**Metodo que sirve para consultar la informacion de las instituciones
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResInst Objeto del tipo BeanResInst
   */
   public BeanResInst consultaInst(ArchitechSessionBean architechSessionBean){
	  List<BeanInstitucion> listBeanInstitucion = null;
	  final BeanResInst beanResInst = new BeanResInst();
	  BeanResConsInstDAO beanResConsInstDAO = null;
      beanResConsInstDAO = dAOInstitucionesPCCda.consultaInst(architechSessionBean);
      listBeanInstitucion = beanResConsInstDAO.getListBeanInstitucion();
      if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError()) && listBeanInstitucion.isEmpty()){
    	  beanResInst.setCodError(Errores.ED00011V);
    	  beanResInst.setTipoError(Errores.TIPO_MSJ_INFO);
    	  beanResInst.setListBeanInstitucion(beanResConsInstDAO.getListBeanInstitucion());
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError())){
    	  beanResInst.setListBeanInstitucion(beanResConsInstDAO.getListBeanInstitucion());
          beanResInst.setCodError(Errores.OK00000V);
      }else{
    	  beanResInst.setCodError(Errores.EC00011B);
    	  beanResInst.setTipoError(Errores.TIPO_MSJ_ERROR); 
      }
      return beanResInst;
   }
	
   @Override
   public BeanResInstPOACOA buscarInstitucionesPOACOA(BeanConsReqInst beanConsReqInst, ArchitechSessionBean architechSessionBean){
	  BeanPaginador paginador = null;
	  List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA = null;
	  final BeanResInstPOACOA beanResInstPOACOA = new BeanResInstPOACOA();
	  BeanResConsInstPOACOADAO beanResConsInstDAO = null;
      paginador = beanConsReqInst.getPaginador();
      if(paginador == null){
    	  paginador = new BeanPaginador();
    	  beanConsReqInst.setPaginador(paginador);
      }
      paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
      beanResConsInstDAO = dAOInstitucionesPCCda.buscarInstitucionesPOACOA(beanConsReqInst, architechSessionBean);
      listBeanInstitucionPOACOA = beanResConsInstDAO.getListBeanInstitucionPOACOA();
      if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError()) && listBeanInstitucionPOACOA.isEmpty()){
    	  beanResInstPOACOA.setCodError(Errores.ED00011V);
    	  beanResInstPOACOA.setTipoError(Errores.TIPO_MSJ_INFO);
    	  beanResInstPOACOA.setListBeanInstitucionPOACOA(beanResConsInstDAO.getListBeanInstitucionPOACOA());
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError())){
    	  beanResInstPOACOA.setListBeanInstitucionPOACOA(beanResConsInstDAO.getListBeanInstitucionPOACOA());          
    	  beanResInstPOACOA.setTotalReg(beanResConsInstDAO.getTotalReg());
          paginador.calculaPaginas(beanResConsInstDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
          beanResInstPOACOA.setBeanPaginador(paginador);
          beanResInstPOACOA.setCodError(Errores.OK00000V);
      }else if(Errores.ED00011V.equals(beanResConsInstDAO.getCodError())){
    	  beanResInstPOACOA.setCodError(Errores.ED00011V);
    	  beanResInstPOACOA.setTipoError(Errores.TIPO_MSJ_INFO); 
      }else{
    	  beanResInstPOACOA.setCodError(Errores.EC00011B);
    	  beanResInstPOACOA.setTipoError(Errores.TIPO_MSJ_ERROR); 
      }
      beanResInstPOACOA.setNombre(beanConsReqInst.getNombre());
      beanResInstPOACOA.setNumBanxico(beanConsReqInst.getNumBanxico());  
      return beanResInstPOACOA;
   }
	   
	@Override
	public BeanResGuardaInst guardaInstitucionPOACOA(BeanReqInsertInst beanReqInsertInst, ArchitechSessionBean architechSessionBean){
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora;
		BeanResInst beanResInst = null;
		BeanResGuardaInst beanResGuardaInst = new BeanResGuardaInst();
		BeanResGuardaInstDAO beanResGuardaInstDAO = null;
		beanResGuardaInstDAO = dAOInstitucionesPCCda.guardaInstitucionPOACOA(beanReqInsertInst, architechSessionBean);

		if(Errores.CODE_SUCCESFULLY.equals(beanResGuardaInstDAO.getCodError())){
			beanResGuardaInst.setCodError(Errores.OK00000V);
			beanResGuardaInst.setTipoError(Errores.TIPO_MSJ_INFO); 
			
	        	
	        beanReqInsertBitacora = utilerias.creaBitacora("altaCatInstFinancieras.do", "1",
	        		"", new Date(), 
	        		"OK", "AINSTPC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
	        		Errores.OK00000V, TRANSACCION_EXITOSA, "INSERT",beanReqInsertInst.getNumBanxico());
	        
	        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
	        
	    }else{
	    	
	    	beanResGuardaInst.setCodError(Errores.EC00011B);
	    	beanResGuardaInst.setTipoError(Errores.TIPO_MSJ_ERROR);
	    	beanReqInsertBitacora = utilerias.creaBitacora("altaCatInstFinancieras.do", "0", 
        			"",new Date(), 
	        		"KO", "AINSTPC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
	        		Errores.EC00011B, "No hay comunicaci\u00F3n serv.", "INSERT","");
	        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
	    	
	    	beanResInst = consultaInst(architechSessionBean);
	    	beanResGuardaInst.setListBeanInstitucion(beanResInst.getListBeanInstitucion());
	    	beanResGuardaInst.setCodError(beanResGuardaInstDAO.getCodError());
	    	beanResGuardaInst.setTipoError(Errores.TIPO_MSJ_ALERT); 
	    }
		return beanResGuardaInst;
	}
	
	/**Metodo que sirve para marcar los registros como eliminados
    * @param beanReqEliminarInstPOACOA Objeto del tipo @see BeanReqEliminarInstPOACOA
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResElimInstPOACOA Objeto del tipo BeanResElimInstPOACOA
    */
    public BeanResElimInstPOACOA  elimCatInstFinancieras(
    		BeanReqEliminarInstPOACOA beanReqEliminarInstPOACOA,
    		ArchitechSessionBean architechSessionBean){
    	StringBuilder exito = new StringBuilder();
    	StringBuilder error = new StringBuilder();
    	BeanReqInsertBitacora beanReqInsertBitacora = null;
    	Utilerias utilerias = Utilerias.getUtilerias();
      final BeanResElimInstPOACOA beanResElimInstPOACOA = new BeanResElimInstPOACOA();
      BeanResElimInstPOACOADAO beanResElimInstPOACOADAO = null;
      BeanResInstPOACOA beanResInstPOACOA = null;
      List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA = new ArrayList<BeanInstitucionPOACOA>();
      if(beanReqEliminarInstPOACOA.getListBeanInstitucionPOACOA()!=null ){
	      for(BeanInstitucionPOACOA beanInstitucionPOACOA:beanReqEliminarInstPOACOA.getListBeanInstitucionPOACOA()){
	    	  if(beanInstitucionPOACOA.isSeleccionado()){
	    		  listBeanInstitucionPOACOA.add(beanInstitucionPOACOA);
	    	  }
	      }
      }
      if(listBeanInstitucionPOACOA.isEmpty()){
    	  BeanConsReqInst beanConsReqInst = new BeanConsReqInst();
    	  beanConsReqInst.setNombre(beanReqEliminarInstPOACOA.getNombre());
    	  beanConsReqInst.setNumBanxico(beanReqEliminarInstPOACOA.getNumBanxico());
    	  beanConsReqInst.setPaginador(beanReqEliminarInstPOACOA.getPaginador());
    	  beanResInstPOACOA = buscarInstitucionesPOACOA(beanConsReqInst,architechSessionBean);
    	  beanResElimInstPOACOA.setListBeanInstitucionPOACOA(beanResInstPOACOA.getListBeanInstitucionPOACOA());
	      beanResElimInstPOACOA.setBeanPaginador(beanResInstPOACOA.getBeanPaginador());
	      beanResElimInstPOACOA.setCodError(Errores.ED00026V);
    	  beanResElimInstPOACOA.setTipoError(Errores.TIPO_MSJ_ALERT);
      }else{
    	  
    	  beanReqEliminarInstPOACOA.setListBeanInstitucionPOACOA(listBeanInstitucionPOACOA);
    	  
    	  for(BeanInstitucionPOACOA beanInstitucionPOACOA : listBeanInstitucionPOACOA){
    		  beanResElimInstPOACOADAO = dAOInstitucionesPCCda.elimCatInstFinancieras(beanInstitucionPOACOA,architechSessionBean);
		      if(Errores.CODE_SUCCESFULLY.equals(beanResElimInstPOACOADAO.getCodError())){
		    	  exito.append(beanInstitucionPOACOA.getNumBanxico());
		    	  exito.append(',');
		    	  beanResElimInstPOACOA.setCodError(Errores.OK00000V);
			      beanResElimInstPOACOA.setTipoError(Errores.TIPO_MSJ_INFO);
			      BeanConsReqInst beanConsReqInst = new BeanConsReqInst();
		    	  beanConsReqInst.setNombre(beanReqEliminarInstPOACOA.getNombre());
		    	  beanConsReqInst.setNumBanxico(beanReqEliminarInstPOACOA.getNumBanxico());
		    	  beanConsReqInst.setPaginador(beanReqEliminarInstPOACOA.getPaginador());
			      beanResInstPOACOA = buscarInstitucionesPOACOA(beanConsReqInst,architechSessionBean);
			      beanResElimInstPOACOA.setListBeanInstitucionPOACOA(beanResInstPOACOA.getListBeanInstitucionPOACOA());
			      beanResElimInstPOACOA.setBeanPaginador(beanResInstPOACOA.getBeanPaginador());
			      
			      beanReqInsertBitacora = utilerias.creaBitacora("elimCatInstFinancieras.do", "1",
			        		"", new Date(), 
			        		"OK", "DINSTPC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
			        		Errores.OK00000V, TRANSACCION_EXITOSA, "ELIMINAR",beanReqEliminarInstPOACOA.getNumBanxico());
			        
			        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
	
		      }else{
		    	  error.append(beanInstitucionPOACOA.getNumBanxico());
		    	  error.append(',');
		    	  beanReqInsertBitacora = utilerias.creaBitacora("elimCatInstFinancieras.do", "0", 
		        			"",new Date(), 
			        		"KO", "DINSTPC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
			        		Errores.EC00011B, "No hay comunicaci\u00F3n serv.", "INSERT","");
			        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
		      }
    	  }
    	  beanResElimInstPOACOA.setEliminadosCorrectos(exito.toString());
    	  beanResElimInstPOACOA.setEliminadosErroneos(error.toString());
      }
      return beanResElimInstPOACOA;
    }
	 @Override
	public BeanResHabDesInstParticipante habDesPOACOA(BeanReqHabDesInst beanReqHabDesInst, ArchitechSessionBean architechSessionBean){
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		BeanResInstPOACOA beanResInstPOACOA = null;
		BeanHabDesParticipanteDAO beanHabDesParticipanteDAO = null;
		BeanResHabDesInstParticipante beanResHabDesInstParticipante = new BeanResHabDesInstParticipante();
		beanHabDesParticipanteDAO = dAOInstitucionesPCCda.habDesParticipantePOA(beanReqHabDesInst, architechSessionBean);

		if(Errores.CODE_SUCCESFULLY.equals(beanHabDesParticipanteDAO.getCodError())){
			beanResHabDesInstParticipante.setCodError(Errores.OK00000V);
			beanResHabDesInstParticipante.setTipoError(Errores.TIPO_MSJ_INFO); 
			
			 beanReqInsertBitacora = utilerias.creaBitacoraMod("habDesPOACOA.do", "1",
		        		"", new Date(), 
		        		"OK", "MINSTPC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
		        		Errores.OK00000V, TRANSACCION_EXITOSA, "MODIFICA","1".equals(beanReqHabDesInst.getHabDesValor())?"1":"0",
		        		"1".equals(beanReqHabDesInst.getHabDesValor())?"0":"1"
			 );
		        
		        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
			
	    }else{
	    	beanResHabDesInstParticipante.setCodError(beanHabDesParticipanteDAO.getCodError());
	    	beanResHabDesInstParticipante.setTipoError(Errores.TIPO_MSJ_ALERT); 
	    }
		BeanConsReqInst beanConsReqInst = new BeanConsReqInst();
    	beanConsReqInst.setPaginador(beanReqHabDesInst.getPaginador());
    	beanConsReqInst.setNombre(beanReqHabDesInst.getNombre());
    	beanConsReqInst.setNumBanxico(beanReqHabDesInst.getNumBanxico());
    	beanResInstPOACOA = buscarInstitucionesPOACOA(beanConsReqInst, architechSessionBean);
    	beanResHabDesInstParticipante.setBeanPaginador(beanResInstPOACOA.getBeanPaginador());
    	beanResHabDesInstParticipante.setListBeanInstitucionPOACOA(beanResInstPOACOA.getListBeanInstitucionPOACOA());
		beanResHabDesInstParticipante.setNombre(beanResInstPOACOA.getNombre());
		beanResHabDesInstParticipante.setNumBanxico(beanResInstPOACOA.getNumBanxico());
		return beanResHabDesInstParticipante;
	}
	@Override
	public BeanResHabDesInstParticipante habDesValidacionCuenta(BeanReqHabDesInst beanReqHabDesInst, ArchitechSessionBean architechSessionBean){
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		BeanResInstPOACOA beanResInstPOACOA = null;
		BeanHabDesParticipanteDAO beanHabDesParticipanteDAO = null;
		BeanResHabDesInstParticipante beanResHabDesInstParticipante = new BeanResHabDesInstParticipante();
		beanHabDesParticipanteDAO = dAOInstitucionesPCCda.habDesValidacionCuenta(beanReqHabDesInst, architechSessionBean);
	
		if(Errores.CODE_SUCCESFULLY.equals(beanHabDesParticipanteDAO.getCodError())){
			beanResHabDesInstParticipante.setCodError(Errores.OK00000V);
			beanResHabDesInstParticipante.setTipoError(Errores.TIPO_MSJ_INFO);
			beanReqInsertBitacora = utilerias.creaBitacoraMod("habDesValidacionCuenta.do", "1",
	        		"", new Date(), 
	        		"OK", "MINSTVC", TRAN_SPEI_PARTICIP, NUM_BANXICO, 
	        		Errores.OK00000V, TRANSACCION_EXITOSA, "MODIFICA","1".equals(beanReqHabDesInst.getHabDesValor())?"1":"0",
	        		"1".equals(beanReqHabDesInst.getHabDesValor())?"0":"1"
		 );
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
	    }else{
	    	beanResHabDesInstParticipante.setCodError(beanHabDesParticipanteDAO.getCodError());
	    	beanResHabDesInstParticipante.setTipoError(Errores.TIPO_MSJ_ALERT); 
	    }
		BeanConsReqInst beanConsReqInst = new BeanConsReqInst();
		beanConsReqInst.setPaginador(beanReqHabDesInst.getPaginador());
		beanConsReqInst.setNombre(beanReqHabDesInst.getNombre());
		beanConsReqInst.setNumBanxico(beanReqHabDesInst.getNumBanxico());
		beanResInstPOACOA = buscarInstitucionesPOACOA(beanConsReqInst, architechSessionBean);
		beanResHabDesInstParticipante.setListBeanInstitucionPOACOA(beanResInstPOACOA.getListBeanInstitucionPOACOA());
		beanResHabDesInstParticipante.setBeanPaginador(beanResInstPOACOA.getBeanPaginador());
		beanResHabDesInstParticipante.setNombre(beanResInstPOACOA.getNombre());
		beanResHabDesInstParticipante.setNumBanxico(beanResInstPOACOA.getNumBanxico());
		return beanResHabDesInstParticipante;
	}
    
	   
	/**
	 * Metodo get que obtiene el valor de la propiedad dAOInstitucionesPCCda
	 * @return dAOInstitucionesPCCda Objeto de tipo @see DAOInstitucionesPCCda
	 */
	public DAOInstitucionesPCCda getDAOInstitucionesPCCda() {
		return dAOInstitucionesPCCda;
	}
	/**
	 * Metodo que modifica el valor de la propiedad dAOInstitucionesPCCda
	 * @param institucionesPCCda Objeto de tipo @see DAOInstitucionesPCCda
	 */
	public void setDAOInstitucionesPCCda(DAOInstitucionesPCCda institucionesPCCda) {
		dAOInstitucionesPCCda = institucionesPCCda;
	}


	/**
	 * Metodo get que obtiene el valor de la propiedad daoBitacora
	 * @return daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}


	/**
	 * Metodo que modifica el valor de la propiedad daoBitacora
	 * @param daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
	
	   
}
