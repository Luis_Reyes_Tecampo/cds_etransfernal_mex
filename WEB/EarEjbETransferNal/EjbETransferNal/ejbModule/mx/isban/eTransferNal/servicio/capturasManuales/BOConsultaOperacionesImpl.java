package mx.isban.eTransferNal.servicio.capturasManuales;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOConsultaOperaciones;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Clase encargada de la logica de negocio para Consulta Operaciones.
 */
//Clase encargada de la logica de negocio para Consulta Operaciones
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaOperacionesImpl extends Architech implements BOConsultaOperaciones{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	//Propiedad del tipo long que almacena el valor de serialVersionUID
	private static final long serialVersionUID = 6290387406154831530L;
	
	/** La constante OK. */
	//La constante OK
	private static final String OK = "OK";
	
	/** La constante NOK. */
	//La constante NOK
	private static final String NOK = "NOK";
	
	/** La constante TRAN_CAP_OPER. */
	//La constante TRAN_CAP_OPER
	private static final String TRAN_CAP_OPER = "TRAN_CAP_OPER";
	
	/** La constante ID_FOLIO. */
	//La constante ID_FOLIO
	private static final String ID_FOLIO = "ID_FOLIO";

	/** La constante NA. */
	//La constante NA
	private static final String NA = "N/A";
	
	/** La constante OPERACION_APARTADA. */
	//La constante OPERACION_APARTADA
	private static final String OPERACION_APARTADA = "OPERACION_APARTADA";
	
	/** La constante USR_APARTA. */
	//La constante USR_APARTA
	private static final String USR_APARTA = "USR_APARTA";
	
	/** Propiedad del tipo DAOConsultaOperaciones que almacena el valor de daoConsultaOperaciones. */
	//El dao propio del modulo
	@EJB
	private DAOConsultaOperaciones daoConsultaOperaciones;

	/** Referencia al DAO que realiza la bitacora. */
	//El dao necesario para generar la pista de auditoria
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;

	/**
	 * consultarOperaciones.
	 *
	 * @param session the session bean
	 * @param beanReqConsOperaciones the beanReqConsOperaciones
	 * @param nextPage the nextPage
	 * @return the BeanResConsOperaciones
	 * @throws BusinessException the business exception
	 */
	//Metodo que devuelve la consulta de las operaciones de acuerdo a los filtros seleccioandos
	public BeanResConsOperaciones consultarOperaciones(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones, boolean nextPage) throws BusinessException {
	 	BeanPaginador paginador = null;
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
	    List<BeanConsOperaciones> listBeanConsOperaciones = new ArrayList<BeanConsOperaciones>();	    
		String vacio = "";
			
		//Logica para mantener la paginacion
		paginador = beanReqConsOperaciones.getPaginador();
		if (paginador == null) {
			paginador = new BeanPaginador();
			beanReqConsOperaciones.setPaginador(paginador);
		} else if (vacio.equals(paginador.getPagina())) {
			paginador.setPagina("1");
		}
		if(!nextPage){
			paginador.setAccion("ACT");
		}
		//Se genera la paginacion
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		//Codigo que hace la consulta de las operaciones al dao
		beanResConsOperaciones = daoConsultaOperaciones.consultarOperaciones(session, beanReqConsOperaciones);
				
		listBeanConsOperaciones = beanResConsOperaciones.getListBeanConsOperaciones();
	    
		
		//Objetos de apoyo
		List<BeanConsOperaciones> operacionesApartadas = new ArrayList<BeanConsOperaciones>();		
		BeanResConsOperaciones beanResOperacionesApartadas = daoConsultaOperaciones.consultarFoliosApartados(session, beanReqConsOperaciones);
		operacionesApartadas=beanResOperacionesApartadas.getListBeanConsOperaciones();
		
		//Codigo que valida que no se reciba una lista invalida
		esListaOpeInvalida(beanResConsOperaciones, operacionesApartadas);
		/**
		if(!(operacionesApartadas != null && !operacionesApartadas.isEmpty())){
			beanResConsOperaciones.setPuedeDesapartar(false);
		}else{
			beanResConsOperaciones.setPuedeDesapartar(true);
		}
		*/
		//Validaciones para saber si hay o no registros
	    if(Errores.CODE_SUCCESFULLY.equals(beanResConsOperaciones.getCodError()) && listBeanConsOperaciones.isEmpty()){
	    	throw new BusinessException(Errores.ED00011V, beanResConsOperaciones.getMsgError());
	    }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsOperaciones.getCodError())){
		    paginador.calculaPaginas(beanResConsOperaciones.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
		    beanResConsOperaciones.setPaginador(paginador);
		    beanResConsOperaciones.setCodError(Errores.OK00000V);
		    beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
	    }else {
	    	throw new BusinessException(Errores.EC00011B, beanResConsOperaciones.getMsgError());
	    }
		
		return beanResConsOperaciones;
	}
	
	/**
	 * apartar.
	 *
	 * @param session the session bean
	 * @param beanReqConsOperaciones the beanReqConsOperaciones
	 * @return the BeanResBase
	 * @throws BusinessException the business exception
	 */
	//Metodo que sirve para apartar las operaciones seleccionadas
	public BeanResBase apartar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException {
		BeanResBase res=new BeanResBase();
		//Codigo que valida que no se reciba una lista invalida
		if(!(beanReqConsOperaciones.getListBeanConsOperaciones() != null && beanReqConsOperaciones.getListBeanConsOperaciones().size() > 0)){
	    	throw new BusinessException(Errores.CMCO002V, res.getMsgError());
		}
		List<BeanConsOperaciones> listaRequest=beanReqConsOperaciones.getListBeanConsOperaciones();
		List<BeanConsOperaciones> foliosParaApartar=new ArrayList<BeanConsOperaciones>();
		BeanResConsOperaciones beanResConsOperaciones=consultarOperaciones(session,beanReqConsOperaciones,false);
		List<BeanConsOperaciones> listaResponse=beanResConsOperaciones.getListBeanConsOperaciones();
		for(int x=0; x<listaRequest.size() && x<listaResponse.size();x++){
			BeanConsOperaciones operacionRequest=listaRequest.get(x);
			BeanConsOperaciones operacionResponse=listaResponse.get(x);
			
			//metodo que valida las operaciones selecionadas
			validarOperacionesApartadas(operacionRequest, operacionResponse, foliosParaApartar);
			/**
			if(operacionRequest.isSeleccionado() && operacionRequest.getFolio() != null && operacionRequest.getFolio().equals(operacionResponse.getFolio())){
				//Se valida que coincida la operacion seleccionada y el folio con lo que esta viendo el usuario
				if (!(operacionResponse.getStatusOperacionApartada() == null || "".equals(operacionResponse.getStatusOperacionApartada()) || "0".equals(operacionResponse.getStatusOperacionApartada()))){
					//Si se detecta que la operacion no puede ser apartada finaliza el metodo y manda el error
			    	throw new BusinessException(Errores.CMCO003V, operacionResponse.getMsgError());
				}
				//Si sale bien se agrega el folio a la lista para ser apartados
				foliosParaApartar.add(operacionRequest);
			}
			*/
		}

		//Se apartan los folios
		apartarFolios(session, foliosParaApartar,res);

		return res;
	}

	/**
	 * Apartar folios.
	 *
	 * @param session the session
	 * @param foliosParaApartar the folios para apartar
	 * @throws BusinessException the business exception
	 */
	//Metodo para apartar una lista de folios
	private void apartarFolios(ArchitechSessionBean session, List<BeanConsOperaciones> foliosParaApartar, BeanResBase res) throws BusinessException {
		//Objetos de apoyo
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		boolean operacionesFueronExitosas=true;
		BeanResBase resApartarFolio = new BeanResBase();
		//Se valida que lista no venga vacía
		if(!foliosParaApartar.isEmpty()){
			for(BeanConsOperaciones folio:foliosParaApartar){
				String estatusApartar="";
				resApartarFolio = daoConsultaOperaciones.apartarDesapartarFolio(session, folio, true);
				if(!Errores.CODE_SUCCESFULLY.equals(resApartarFolio.getCodError())){
					//Si una operacion no es exitosa se establece aqui
					operacionesFueronExitosas=false;	
					estatusApartar=NOK;
				}else{
					estatusApartar=OK;
				}
				//Se genera el registro en bitacora dependiendo si fue exitoso o no
				BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
						utileriasBitAdmin.createBitacoraBitAdmin(estatusApartar, TRAN_CAP_OPER, "UPDATE",
								"apartarOperaciones.do",
								OPERACION_APARTADA+ "=0"+","+USR_APARTA+"=NULL", 
								OPERACION_APARTADA+ "=1"+","+USR_APARTA+"=" + session.getUsuario(),
								OPERACION_APARTADA +","+ USR_APARTA, ID_FOLIO, "APARTA FOLIO", session);
				daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, session);
			}
			//Si una operacion no fue exitosa se muestra un error
			if(operacionesFueronExitosas){
				res.setCodError(Errores.CMCO019V);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				//Se setea codigo de error CMC004V
		    	throw new BusinessException(Errores.CMCO004V, resApartarFolio.getMsgError());
			}
		}else{
	    	throw new BusinessException(Errores.CMCO002V, resApartarFolio.getMsgError());
		}
	}

	/**
	 * desapartar.
	 *
	 * @param session the session bean
	 * @param beanReqConsOperaciones the beanReqConsOperaciones
	 * @return the BeanResBase
	 * @throws BusinessException the business exception
	 */
	//Metodo para desapartar las operaciones
	public BeanResBase desapartar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException {
		BeanResBase res=new BeanResBase();
		
		//Objetos de apoyo
		List<BeanConsOperaciones> operacionesApartadas = new ArrayList<BeanConsOperaciones>();		
		BeanResConsOperaciones beanResConsOperaciones = daoConsultaOperaciones.consultarFoliosApartados(session,null);
		operacionesApartadas=beanResConsOperaciones.getListBeanConsOperaciones();
		
		//Codigo que valida que no se reciba una lista invalida
		if(!(operacionesApartadas != null && !operacionesApartadas.isEmpty())){
	    	throw new BusinessException(Errores.CMCO005V, beanResConsOperaciones.getMsgError());
		}
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsOperaciones.getCodError())){
			boolean operacionesFueronExitosas=true;
			UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
			for(BeanConsOperaciones beanConsOperaciones : operacionesApartadas){
				//Se desaparta cada folio
				BeanResBase resDesapartarFolio = daoConsultaOperaciones.apartarDesapartarFolio(session,beanConsOperaciones, false);
				String estatusDesapartar="";
				if(!Errores.CODE_SUCCESFULLY.equals(resDesapartarFolio.getCodError())){
					//Si una operacion no es exitosa se establece aqui
					operacionesFueronExitosas=false;
					estatusDesapartar=NOK;
				}else{
					estatusDesapartar=OK;
				}
				//Se genera el registro en bitacora dependiendo si fue exitoso o no
				BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
						utileriasBitAdmin.createBitacoraBitAdmin(estatusDesapartar, TRAN_CAP_OPER, "UPDATE",
								"desapartarOperaciones.do",
								OPERACION_APARTADA+ "=1"+","+USR_APARTA+"=" + session.getUsuario(),
								OPERACION_APARTADA+ "=0"+","+USR_APARTA+"=NULL", 
								OPERACION_APARTADA +","+ USR_APARTA, ID_FOLIO, "DESAPARTA FOLIO", session);
				daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, session);
			}
			//Si una operacion no fue exitosa se muestra un error
			if(operacionesFueronExitosas){
				res.setCodError(Errores.CMCO018V);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
		    	throw new BusinessException(Errores.CMCO006V, beanResConsOperaciones.getMsgError());
			}

		}else{
			res.setCodError(beanResConsOperaciones.getCodError());
			res.setMsgError(beanResConsOperaciones.getMsgError());
			res.setTipoError(beanResConsOperaciones.getTipoError());
		}
		return res;
	}
	
	/**
	 * eliminar.
	 *
	 * @param session the session bean
	 * @param beanReqConsOperaciones the beanReqConsOperaciones
	 * @return the BeanResBase
	 * @throws BusinessException the business exception
	 */
	//Metodo para eliminar las operaciones seleccionadas
	public BeanResBase eliminar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException {
		BeanResBase res=new BeanResBase();
		
		//Codigo que valida que no se reciba una lista invalida
		if(!(beanReqConsOperaciones.getListBeanConsOperaciones() != null && beanReqConsOperaciones.getListBeanConsOperaciones().size()>0)){
	    	throw new BusinessException(Errores.CMCO002V, res.getMsgError());
		}

		//Objetos de apoyo
		List<BeanConsOperaciones> listaRequest=beanReqConsOperaciones.getListBeanConsOperaciones();
		List<BeanConsOperaciones> foliosAEliminar=new ArrayList<BeanConsOperaciones>();
		BeanResConsOperaciones beanResConsOperaciones=consultarOperaciones(session,beanReqConsOperaciones,false);
		List<BeanConsOperaciones> listaResponse=beanResConsOperaciones.getListBeanConsOperaciones();

		for(int x=0; x<listaRequest.size() && x<listaResponse.size();x++){
			BeanConsOperaciones operacionRequest=listaRequest.get(x);
			BeanConsOperaciones operacionResponse=listaResponse.get(x);
			
			if(operacionRequest.isSeleccionado() && operacionRequest.getFolio() != null && operacionResponse.getFolio() != null && operacionRequest.getFolio().equals(operacionResponse.getFolio())){
				agregarBeanFoliosEliminar(operacionRequest, operacionResponse, foliosAEliminar);
				/**
				//Si no esta apartado se cancela y se manda un error
				if (!operacionResponse.isApartado()){
			    	throw new BusinessException(Errores.CMCO001V, operacionResponse.getMsgError());
				}
				//Si una operacion esta cancelada o confirmada se cancela y se manda un error
				if(operacionResponse.getEstatus() == null || "CA".equals(operacionResponse.getEstatus()) || "CO".equals(operacionResponse.getEstatus())){
			    	throw new BusinessException(Errores.CMCO007V, operacionResponse.getMsgError());
				}
				//Si es una operacion valida se agrega a la lista para ser eliminado
				foliosAEliminar.add(operacionRequest);
				*/
			}
		}

		//Se valida que lista no este vacía
		eliminarFolios(foliosAEliminar, res, session);
		/**
		if(!foliosAEliminar.isEmpty()){
			eliminarFolios(foliosAEliminar, resFolioEliminar, res, session);
			for(BeanConsOperaciones folio:foliosAEliminar){
				String pistaExtraInfo = "NUM_CUENTA_ORD="+folio.getCuentaOrdenante()+", NUM_CUENTA_REC="+folio.getCuentaReceptora()+", IMPORTE_CARGO="+folio.getMonto();
				String folioStr=folio.getFolio();
				String estatusEliminado="";
				//Se elimina el folio
				resFolioEliminar = daoConsultaOperaciones.eliminarFolio(session, folio);
				if(!Errores.CODE_SUCCESFULLY.equals(resFolioEliminar.getCodError())){
					//Si una operacion no es exitosa se establece aqui
					operacionesFueronExitosas=false;
					estatusEliminado=NOK;
				}else{
					estatusEliminado=OK;
				}
				//Se genera el registro en bitacora dependiendo si fue exitoso o no
				BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
						utileriasBitAdmin.createBitacoraBitAdmin(estatusEliminado, TRAN_CAP_OPER, "DELETE", "eliminarOperaciones.do", ID_FOLIO+ "=" + folioStr+", "+pistaExtraInfo, "", 
								NA, NA, "ELIMINA FOLIO", session);
				daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, session);
			}
			//Si una operacion no fue exitosa se muestra un error
			if(operacionesFueronExitosas){
				res.setCodError(Errores.CMCO008V);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
		    	throw new BusinessException(Errores.CMCO004V, resFolioEliminar.getMsgError());
			}
		}else{
	    	throw new BusinessException(Errores.CMCO002V, resFolioEliminar.getMsgError());
		}
		*/
		return res;
	}
	
	/**
	 * eliminar.
	 *
	 * @param session the session bean
	 * @param beanReqConsOperaciones the beanReqConsOperaciones
	 * @return the BeanResConsOperaciones
	 * @throws BusinessException the business exception
	 */
	//Metodo que sirve para generar la consulta de exportar las operaciones
	public BeanResConsOperaciones exportarTodo(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException {
		BeanResConsOperaciones beanResConsOperaciones=consultarOperaciones(session,beanReqConsOperaciones,false);
		beanReqConsOperaciones.setTotalReg(beanResConsOperaciones.getTotalReg());
		if(Errores.OK00000V.equals(beanResConsOperaciones.getCodError())){
			//Se hace la consulta con el dao para exportar las operaciones
			BeanResBase resExportarTodo = daoConsultaOperaciones.exportarTodo(session, beanReqConsOperaciones, beanResConsOperaciones);
			if(Errores.CODE_SUCCESFULLY.equals(resExportarTodo.getCodError())){
				beanResConsOperaciones.setCodError(Errores.OK00001V);
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				beanResConsOperaciones.setMsgError(resExportarTodo.getMsgError());
				beanResConsOperaciones.setCodError(resExportarTodo.getCodError());
				beanResConsOperaciones.setTipoError(resExportarTodo.getTipoError());
			}
		}
		return beanResConsOperaciones;
	}

	/**
	 * Es lista ope invalida.
	 *
	 * @param beanResConsOperaciones the bean res cons operaciones
	 * @param operacionesApartadas the operaciones apartadas
	 */
	private void esListaOpeInvalida(
			BeanResConsOperaciones beanResConsOperaciones, List<BeanConsOperaciones> operacionesApartadas){
		//Codigo que valida que no se reciba una lista invalida
		if(!(operacionesApartadas != null && !operacionesApartadas.isEmpty())){
			beanResConsOperaciones.setPuedeDesapartar(false);
		}else{
			beanResConsOperaciones.setPuedeDesapartar(true);
		}
	}
	
	/**
	 * Eliminar folios.
	 *
	 * @param foliosAEliminar the folios a eliminar
	 * @param res the res
	 * @param session the session
	 * @throws BusinessException the business exception
	 */
	private void eliminarFolios(
			List<BeanConsOperaciones> foliosAEliminar, BeanResBase res, ArchitechSessionBean session) throws BusinessException{
		boolean operacionesFueronExitosas=true;
		BeanResBase resFolioEliminar = new BeanResBase();
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		if(!foliosAEliminar.isEmpty()){
			for(BeanConsOperaciones folio:foliosAEliminar){
				String pistaExtraInfo = "NUM_CUENTA_ORD="+folio.getCuentaOrdenante()+", NUM_CUENTA_REC="+folio.getCuentaReceptora()+", IMPORTE_CARGO="+folio.getMonto();
				String folioStr=folio.getFolio();
				String estatusEliminado="";
				//Se elimina el folio
				resFolioEliminar = daoConsultaOperaciones.eliminarFolio(session, folio);
				if(!Errores.CODE_SUCCESFULLY.equals(resFolioEliminar.getCodError())){
					//Si una operacion no es exitosa se establece aqui
					operacionesFueronExitosas=false;
					estatusEliminado=NOK;
				}else{
					estatusEliminado=OK;
				}
				//Se genera el registro en bitacora dependiendo si fue exitoso o no
				BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
						utileriasBitAdmin.createBitacoraBitAdmin(estatusEliminado, TRAN_CAP_OPER, "DELETE", "eliminarOperaciones.do", ID_FOLIO+ "=" + folioStr+", "+pistaExtraInfo, "", 
								NA, NA, "ELIMINA FOLIO", session);
				daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, session);
			}
			//Si una operacion no fue exitosa se muestra un error
			if(operacionesFueronExitosas){
				res.setCodError(Errores.CMCO008V);
				res.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
		    	throw new BusinessException(Errores.CMCO004V, resFolioEliminar.getMsgError());
			}
		}else{
	    	throw new BusinessException(Errores.CMCO002V, resFolioEliminar.getMsgError());
		}
	}
	
	/**
	 * Recorrer lista.
	 *
	 * @param listaRequest the lista request
	 * @param listaResponse the lista response
	 * @param foliosAEliminar the folios a eliminar
	 * @throws BusinessException the business exception
	 */
	private void agregarBeanFoliosEliminar(BeanConsOperaciones operacionRequest,
			BeanConsOperaciones operacionResponse, List<BeanConsOperaciones> foliosAEliminar) throws BusinessException {
		//Si no esta apartado se cancela y se manda un error
		if (!operacionResponse.isApartado()){
	    	throw new BusinessException(Errores.CMCO001V, operacionResponse.getMsgError());
		}
		//Si una operacion esta cancelada o confirmada se cancela y se manda un error
		if(operacionResponse.getEstatus() == null || "CA".equals(operacionResponse.getEstatus()) || "CO".equals(operacionResponse.getEstatus())){
	    	throw new BusinessException(Errores.CMCO007V, operacionResponse.getMsgError());
		}
		//Si es una operacion valida se agrega a la lista para ser eliminado
		foliosAEliminar.add(operacionRequest);
	}
	
	/**
	 * Validar operaciones apartadas.
	 *
	 * @param operacionRequest the operacion request
	 * @param operacionResponse the operacion response
	 * @param foliosParaApartar the folios para apartar
	 * @throws BusinessException the business exception
	 */
	private void validarOperacionesApartadas(BeanConsOperaciones operacionRequest,
			BeanConsOperaciones operacionResponse, List<BeanConsOperaciones> foliosParaApartar) throws BusinessException {
		if(operacionRequest.isSeleccionado() && operacionRequest.getFolio() != null && operacionRequest.getFolio().equals(operacionResponse.getFolio())){
			//Se valida que coincida la operacion seleccionada y el folio con lo que esta viendo el usuario
			if (!(operacionResponse.getStatusOperacionApartada() == null || "".equals(operacionResponse.getStatusOperacionApartada()) || "0".equals(operacionResponse.getStatusOperacionApartada()))){
				//Si se detecta que la operacion no puede ser apartada finaliza el metodo y manda el error
		    	throw new BusinessException(Errores.CMCO003V, operacionResponse.getMsgError());
			}
			//Si sale bien se agrega el folio a la lista para ser apartados
			foliosParaApartar.add(operacionRequest);
		}
	}
}