/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovimientosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicio.ws;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.constantes.modBIP.ConstantesConsultaMov;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOConsultaMovimientos;
import mx.isban.eTransferNal.servicios.ws.BOConsultaMovimientos;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import net.sf.json.JSONObject;

/**
* @author mcamarillo
*
*/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaMovimientosImpl extends Architech implements
BOConsultaMovimientos {


	
	/**
	 * Constante del tipo long que almacena el valor de serialVersionUID
	 */
	private final static long serialVersionUID = 1L;

	/**
	 * Propiedad del tipo DAOConsultaMovimientos que almacena el valor de dao
	 */
	@EJB
	private  DAOConsultaMovimientos dao;

	
	@Override
	public ResEntradaStringJsonDTO getConsultaMovimientos(
			EntradaStringJsonDTO entradaStringJsonDTO) {
		String nomCampo ="";
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		info("Entro a la llamada getConsultaMovimientos"+entradaStringJsonDTO.getCadenaJson());
		boolean flag = true;
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		Map<String, String> salida = new HashMap<String, String>();
		StringBuilder trama = new StringBuilder();
		String json = entradaStringJsonDTO.getCadenaJson();
		JSONObject jsonObject = JSONObject.fromString(json);  
		Map<String,String> mapMovimientos =  (Map<String,String>)JSONObject.toBean(jsonObject, Map.class);
		trama.append(ConstantesConsultaMov.PRIMER_CAMPO);
		for (int index =0; index<ConstantesConsultaMov.CAMPOS.length && flag;index++){
			flag = armaTrama(mapMovimientos,trama, index);
			nomCampo = ConstantesConsultaMov.CAMPOS[index];
		}
		if(flag){
			resBeanEjecTranDAO = dao.ejecutar(trama.toString());
			if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
				String tramaRetorno = resBeanEjecTranDAO.getTramaRespuesta();
				if (tramaRetorno.contains(ConstantesConsultaMov.PIPE)) {
					final String[] valores = tramaRetorno
							.split(ConstantesConsultaMov.DIAGONALES_INVERSA
									.concat(ConstantesConsultaMov.PIPE));
					if(isValidRetorno(valores)){
						returnSalida(valores,salida);
					}
				} else {
					returnCodigo(tramaRetorno,salida);
				}
			}else{
				returnTramaNula(salida);
			}
		}else{
			salida.put(ConstantesConsultaMov.CAMPO_COD_ERROR, "ED00092V");
			salida.put(ConstantesConsultaMov.CAMPO_REFERENCIA,"0" );
			salida.put(ConstantesConsultaMov.CAMPO_ESTATUS, "ER");
			salida.put(ConstantesConsultaMov.CAMPO_DESCRIPCION, "Error en el formato o la longitud del campo:"+nomCampo);	
		}
		jsonObject = JSONObject.fromMap(salida);
		resEntradaStringJsonDTO.setJson(jsonObject.toString());
		info("Salio de la llamada getConsultaMovimientos"+resEntradaStringJsonDTO.getJson());
		return resEntradaStringJsonDTO;
	}
		

	/**
	 * @param mapMovimientos Objeto del tipo Map<String,String>
	 * @param trama Objeto StringBuilder donde se va a generar la trama 
	 * @param index numero entero
	 * @return boolean true si no hay errores falso en caso contrario
	 */
	public boolean armaTrama(Map<String,String> mapMovimientos,StringBuilder trama, int index){
		int tamanio;
		boolean flag = true;
		Utilerias utilerias = Utilerias.getUtilerias();
		String nomCampo ="";
		String tipoCampo = "";
		String valorCampo = "";

		nomCampo = ConstantesConsultaMov.CAMPOS[index];
		if(!mapMovimientos.containsKey(nomCampo)){//Si no existe lo inicializo
			tipoCampo = ConstantesConsultaMov.TIPO.get(nomCampo);
			if(ConstantesConsultaMov.DECIMAL.equals(tipoCampo)){
				mapMovimientos.put(nomCampo, ConstantesConsultaMov.CERO);
			}else{
				mapMovimientos.put(nomCampo, "");
			}
		}
		tipoCampo = ConstantesConsultaMov.TIPO.get(nomCampo);
		valorCampo = mapMovimientos.get(nomCampo);
		tamanio = ConstantesConsultaMov.LONGITUD.get(nomCampo);
		if(ConstantesConsultaMov.DECIMAL.equals(tipoCampo)){
			BigDecimal valor = new BigDecimal(valorCampo);
			if(valor.compareTo(BigDecimal.ZERO)>0){
				if(isFormatoValidado(tipoCampo, tamanio, valorCampo)){
					trama.append(utilerias.formateaDecimales(valorCampo,Utilerias.FORMATO_DECIMAL_NUMBER_2)).append(ConstantesConsultaMov.PIPE);
				}else{
					flag = false;
				}
			}else{
				trama.append(ConstantesConsultaMov.CERO).append(ConstantesConsultaMov.PIPE);
			}
		}else{
			if(isFormatoValidado(tipoCampo, tamanio, valorCampo)){
				trama.append(utilerias.getString(valorCampo)).append(ConstantesConsultaMov.PIPE);
			}else{
				flag = false;
			}					
		}

		return flag;
	}
	
	/**
	 * llenado cuando la trama es nula
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnTramaNula(Map<String, String> salida) {
		salida.put(ConstantesConsultaMov.CAMPO_COD_ERROR,Errores.EC00011B);
		salida.put(ConstantesConsultaMov.CAMPO_REFERENCIA,ConstantesConsultaMov.CERO);
		salida.put(ConstantesConsultaMov.CAMPO_ESTATUS,"ER");
		salida.put(ConstantesConsultaMov.CAMPO_DESCRIPCION,ConstantesConsultaMov.ERROR_IDA);
	}

	
	/**
	 * @param tramaRetorno trama de retorno de ida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnCodigo(final String tramaRetorno,Map<String, String> salida) {
		salida.put(ConstantesConsultaMov.CAMPO_COD_ERROR, tramaRetorno);
		salida.put(ConstantesConsultaMov.CAMPO_REFERENCIA,
				ConstantesConsultaMov.CERO);
		salida.put(ConstantesConsultaMov.CAMPO_ESTATUS,
				ConstantesConsultaMov.NADA);
		salida.put(ConstantesConsultaMov.CAMPO_DESCRIPCION,
				tramaRetorno);

	}

	/**
	 * Metodo que sirve para validar el formato
	 * @param clase string con el tipo de dato
	 * @param tamanio longitud del valor
	 * @param valorDato valor del campo
	 * @return boolean true si es valido falso en caso contrario
	 */
	private boolean isFormatoValidado(final String clase,
			final int tamanio, final String valorDato) {
		if (clase.equals(ConstantesConsultaMov.STRING)
				&& (!validaLongitud(valorDato, tamanio))) {
				return false;			
		}
		if (clase.equals(ConstantesConsultaMov.DECIMAL)) {
			if (!validaLongitud(valorDato, tamanio)) {
				return false;
			}
			if (!valorDato.matches(
					ConstantesConsultaMov.EXP_REGULAR_NUMEROS)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param valores para validar la trama de retorno
	 * @return boolean true si es valido falso en caso contrario
	 */
	private boolean isValidRetorno(final String[] valores) {
		boolean result = true;
		if (valores.length < 3) {
			result = false;
		}
		if (!validaLongitud(valores[0], ConstantesConsultaMov.NUM_8)) {
			result = false;
		}
		if (!validaLongitud(valores[1], ConstantesConsultaMov.NUM_7)) {
			result = false;
		}
		if (!validaLongitud(valores[2], ConstantesConsultaMov.NUM_2)) {
			result = false;
		}
		return result; 
	}

	/**
	 * @param valores para retornar trama de salida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnSalida(final String[] valores, Map<String, String> salida) {
		salida.put(ConstantesConsultaMov.CAMPO_COD_ERROR, valores[0]);
		debug("COD_ERROR " + valores[0]);
		salida.put(ConstantesConsultaMov.CAMPO_REFERENCIA, valores[1]);
		debug("REFERENCIA " + valores[1]);
		salida.put(ConstantesConsultaMov.CAMPO_ESTATUS, valores[2]);
		debug("ESTATUS " + valores[2]);
		if(valores.length>3){
			salida.put(ConstantesConsultaMov.CAMPO_DESCRIPCION, valores[3]);
		}else{
			salida.put(ConstantesConsultaMov.CAMPO_DESCRIPCION, "");	
		}
		debug("DESCRIPCION " + valores[0]);

	}

	/**
	 * @param cadena valor del campo
	 * @param numero longitud maxima
	 * @return boolean true si es correcta la longitud false en caso contrario
	 */
	private boolean validaLongitud(final String cadena, final int numero) {
		if (cadena.length() > numero) {
			return false;
		}
		return true;
	}




	/**
	 * Metodo get que sirve para obtener la propiedad dao
	 * @return dao Objeto del tipo DAOConsultaMovimientos
	 */
	public DAOConsultaMovimientos getDao() {
		return dao;
	}




	/**
	 * Metodo set que modifica la referencia de la propiedad dao
	 * @param dao del tipo DAOConsultaMovimientos
	 */
	public void setDao(DAOConsultaMovimientos dao) {
		this.dao = dao;
	}




}
