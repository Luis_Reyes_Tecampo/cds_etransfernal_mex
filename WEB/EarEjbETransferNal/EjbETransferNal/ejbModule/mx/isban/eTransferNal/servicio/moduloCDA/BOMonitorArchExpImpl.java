/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorArchExpImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 17:39:58 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsMonitorArchExp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExpDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorArchExp;
import mx.isban.eTransferNal.helper.HelperDAO;


/**
 *Clase del tipo BO que se encarga  del negocio del Monitor de
 * archivos a exportar
**/
@Remote(BOMonitorArchExp.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorArchExpImpl extends Architech implements BOMonitorArchExp {

   /**
	 * Constante serial version
	 */
	private static final long serialVersionUID = -71297298273999570L;
	
   /**
	* Referencia privada al dao de la funcionalidad del monitor de Archivos a exportar
	*/
	@EJB 
	private DAOMonitorArchExp dAOMonitorArchExp;


  /**Metodo que permite consultar el monitor de archivos exportar
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsMonitorArchExp Objeto del tipo BeanResConsMonitorArchExp
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsMonitorArchExp consultarMonitorArchExp(BeanPaginador beanPaginador, 
		   ArchitechSessionBean architechSessionBean)
       throws BusinessException{
	   List<BeanConsMonitorArchExp> listBeanConsMonitorArchExp = null;
	   BeanResConsMonitorArchExpDAO beanResConsMonitorArchExpDAO = null;
	   final BeanResConsMonitorArchExp beanResConsMonitorArchExp = new BeanResConsMonitorArchExp();
       beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
       beanResConsMonitorArchExpDAO = dAOMonitorArchExp.consultarMonitorArchExp(beanPaginador, architechSessionBean);
       listBeanConsMonitorArchExp = beanResConsMonitorArchExpDAO.getListBeanConsMonitorArchExp();
       if(Errores.CODE_SUCCESFULLY.equals(beanResConsMonitorArchExpDAO.getCodError()) && listBeanConsMonitorArchExp.isEmpty()){
    	   beanResConsMonitorArchExp.setCodError(Errores.ED00011V);
     	 beanResConsMonitorArchExp.setTipoError(Errores.TIPO_MSJ_INFO);
       }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsMonitorArchExpDAO.getCodError())){
    	   beanResConsMonitorArchExp.setListBeanConsMonitorArchExp(beanResConsMonitorArchExpDAO.getListBeanConsMonitorArchExp());
           beanPaginador.calculaPaginas(beanResConsMonitorArchExpDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
           beanResConsMonitorArchExp.setBeanPaginador(beanPaginador);
           beanResConsMonitorArchExp.setCodError(Errores.OK00000V);
       }else{
    	   beanResConsMonitorArchExp.setCodError(Errores.EC00011B);
       	   beanResConsMonitorArchExp.setTipoError(Errores.TIPO_MSJ_ERROR);
       }
       return beanResConsMonitorArchExp;
   }

	/**
	 * Metodo get que sirve para obtener la referencia al DAO de monitor de archivos a exportar
	 * @return dAOMonitorArchExp Objeto del tipo DAOMonitorArchExp
	 */
	public DAOMonitorArchExp getDAOMonitorArchExp() {
		return dAOMonitorArchExp;
	}
	
	/**
	 * Metodo set que sirve para poner la referencia al DAO de monitor de archivos a exportar
	 * @param monitorArchExp Objeto del tipo DAOMonitorArchExp
	 */	
	public void setDAOMonitorArchExp(DAOMonitorArchExp monitorArchExp) {
		dAOMonitorArchExp = monitorArchExp;
	}

 
   


}
