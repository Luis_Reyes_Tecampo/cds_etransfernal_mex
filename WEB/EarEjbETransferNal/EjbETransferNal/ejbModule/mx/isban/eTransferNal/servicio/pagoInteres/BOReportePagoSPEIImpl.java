/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOReportePagoSPEIImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.pagoInteres;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanRangoTiempoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResEstatusDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResMedioEntDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePagoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResTipoPagoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOComunesSPEI;
import mx.isban.eTransferNal.dao.pagoInteres.DAORepPagoInteresCompSPEI;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOReportePagoSPEIImpl implements BOReportePagoSPEI{

	/**
	 * Propiedad del tipo DAORepPagoInteresCompSPEI que almacena el valor de dAORepPagoInteresCompSPEI
	 */
	@EJB
	private DAORepPagoInteresCompSPEI dAORepPagoInteresCompSPEI;
	
	/**
	 * Propiedad del tipo DAOComunesSPEI que almacena el valor de dAOComunesSPEI
	 */
	@EJB
	private DAOComunesSPEI dAOComunesSPEI;
	
	@Override
	public BeanResReportePago consultaCatalogos(ArchitechSessionBean sesion) {
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanResTipoPagoDAO beanResTipoPagoDAO = null;
		BeanResEstatusDAO beanResEstatusDAO = null;
		BeanResMedioEntDAO beanResMedioEntDAO = null;
		BeanRangoTiempoDAO beanRangoTiempoDAO = null;
		BeanResReportePago beanResReportePago = new BeanResReportePago();
		List<String> difMinutos = new ArrayList<String>();
		
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOComunesSPEI.consultaFechaOperacion(sesion);
		String fecha = utilerias.formatoToformatoFecha(beanResConsFechaOpDAO.getFechaOperacion(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY, "EEEE dd 'de' MMMM yyyy");
		
		beanResReportePago.setFechaOperacion(fecha);
		
		
		beanResTipoPagoDAO = dAORepPagoInteresCompSPEI.consultaTipoPago(sesion);
		beanResReportePago.setTipoPagoList(beanResTipoPagoDAO.getTipoPagoList());
		
		beanResEstatusDAO = dAORepPagoInteresCompSPEI.consultaEstatus(sesion);
		beanResReportePago.setEstatusList(beanResEstatusDAO.getEstatusList());
		
		beanResMedioEntDAO = dAORepPagoInteresCompSPEI.consultaMedioEnt(sesion);
		beanResReportePago.setMedioEntList(beanResMedioEntDAO.getMedioEntList());
		
		beanRangoTiempoDAO = dAORepPagoInteresCompSPEI.consultaRango(sesion);
		beanResReportePago.setListRangoTiempo(beanRangoTiempoDAO.getListRangoTiempo());
		
		difMinutos.add(">");
		difMinutos.add(">=");
		difMinutos.add("=");
		difMinutos.add("<=");
		difMinutos.add("<");
		
		beanResReportePago.setDifMinutosList(difMinutos);
		
		return beanResReportePago;
	}

	@Override
	public BeanResReportePago consultaReportePago(
			BeanReqReportePago beanReqReportePago, ArchitechSessionBean sesion) {
		BeanPaginador paginador = null;
		BeanResReportePagoDAO beanResReportePagoDAO = null;
		List<BeanReportePago> reportePagoList;
		BeanResReportePago beanResReportePago = null;
		paginador = beanReqReportePago.getPaginador();
	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());			    
	    beanReqReportePago.setPaginador(paginador);
	    beanResReportePago = consultaCatalogos(sesion);
		beanResReportePagoDAO = dAORepPagoInteresCompSPEI.consultaReportePago(beanReqReportePago,sesion);
		reportePagoList = beanResReportePagoDAO.getReportePagoList();
	    if(Errores.OK00000V.equals(beanResReportePagoDAO.getCodError()) && reportePagoList.isEmpty()){
	    	beanResReportePago.setCodError(Errores.ED00011V);
	    	beanResReportePago.setTipoError(Errores.TIPO_MSJ_INFO);
	    	beanResReportePago.setTotalReg("0");
	    }else if(Errores.OK00000V.equals(beanResReportePagoDAO.getCodError())){
	    	beanResReportePago.setReportePagoList(reportePagoList);
	    	beanResReportePago.setCodError(beanResReportePagoDAO.getCodError());
			paginador.calculaPaginas(beanResReportePagoDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResReportePago.setPaginador(paginador);	
			beanResReportePago.setTotalReg(beanResReportePagoDAO.getTotalReg());
	    }else{
	    	beanResReportePago.setCodError(Errores.EC00011B);
	    	beanResReportePago.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }	
		return beanResReportePago;
	}



	@Override
	public BeanResReportePago exportarTodoReportePago(
			BeanReqReportePago beanReqReportePago, ArchitechSessionBean sesion) {
		BeanResReportePago beanResReportePago = consultaReportePago(beanReqReportePago,sesion);
		BeanResReportePagoDAO beanResReportePagoDAO = dAORepPagoInteresCompSPEI.expTodoReportePago(beanReqReportePago, sesion);
		
		
		beanResReportePago.setNomArchivo(beanResReportePagoDAO.getNombreArchivo());
		if(Errores.OK00000V.equals(beanResReportePagoDAO.getCodError())){
			beanResReportePago.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResReportePago.setCodError(beanResReportePagoDAO.getCodError());
		}else{
			beanResReportePago.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResReportePago.setCodError(beanResReportePagoDAO.getCodError());
		}
		return beanResReportePago;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad dAORepPagoInteresCompSPEI
	 * @return dAORepPagoInteresCompSPEI Objeto del tipo DAORepPagoInteresCompSPEI
	 */
	public DAORepPagoInteresCompSPEI getDAORepPagoInteresCompSPEI() {
		return dAORepPagoInteresCompSPEI;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad dAORepPagoInteresCompSPEI
	 * @param repPagoInteresCompSPEI del tipo DAORepPagoInteresCompSPEI
	 */
	public void setDAORepPagoInteresCompSPEI(
			DAORepPagoInteresCompSPEI repPagoInteresCompSPEI) {
		dAORepPagoInteresCompSPEI = repPagoInteresCompSPEI;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOComunesSPEI
	 * @return dAOComunesSPEI Objeto del tipo DAOComunesSPEI
	 */
	public DAOComunesSPEI getDAOComunesSPEI() {
		return dAOComunesSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOComunesSPEI
	 * @param comunesSPEI del tipo DAOComunesSPEI
	 */
	public void setDAOComunesSPEI(DAOComunesSPEI comunesSPEI) {
		dAOComunesSPEI = comunesSPEI;
	}
	
}
