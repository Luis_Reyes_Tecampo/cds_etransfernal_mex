package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultasPayme;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOPayments;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasConsultaPayments;

/**
 * Class BOPaymentsImpl.
 * 
 * Clase de negocio que interpreta y sirve las 
 * peticiones del Controlador
 * 
 * Realiza peticiones al DAO para comunicarse con 
 * la Base de Datos
 *
 * @author FSW-SNG Enterprise
 * @since 07/12/2020
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOPaymentsImpl  extends Architech implements BOPayments{
	
	/** La constante serialVersionUID. 
	 * El proceso de serialización asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calculará un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 5995925672537867329L;
	
	/** La constante DATO_CTRL_DUPLICADOS. */
	private static String DATO_CTRL_DUPLICADOS = "CTRL_DUPLICADOS="; 
	
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La variable que contiene informacion con respecto a: daoConsultaPayments. */
	@EJB
	private DAOPayments daoConsultaPayments;
	
	/** La constante DATO_FIJO. */
	private static String DATO_FIJO = "CVE_MEDIO_ENT";
	
	/** La constante TYPE_EXPORT. */
	private static String TYPE_EXPORT = "EXPORT";
	
	/** La constante TYPE_UPDATE. */
	private static String TYPE_UPDATE = "UPDATE";
	
	/** La constante UPDATE. */
	private static String UPDATE = "CTRL_DUPLICADOS";
	
	
	/** La constante STR_NOMTABLA. */
	private static String STR_NOMTABLA = "COMU_MEDIOS_ENT";
	

	private static UtileriasConsultaPayments utilPayments = UtileriasConsultaPayments.getUtilerias();
	
	/**
	 * Consulta Payments 
	 * @param architectSessionBean : ArchitechSessionBean
	 * @param requestConsuPayments : BeanConsultaPayments
	 * @param beanPaginador : BeanPaginador
	 * @throws BusinessException, Exception controlada para los errores de ejecucion.
	 * @return response
	 */	
	@Override
	public BeanConsultasPayme consultasPayments(ArchitechSessionBean architectSessionBean, BeanConsultaPayme requestConsuPayments,
			BeanPaginador beanPaginador) throws BusinessException {
		BeanConsultasPayme response = new BeanConsultasPayme();
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		
		if (paginador.getAccion() != null
				&& !"SIG".equalsIgnoreCase(paginador.getAccion()) 
				&& !"ANT".equalsIgnoreCase(paginador.getAccion())
				&& !"FIN".equalsIgnoreCase(paginador.getAccion())) {
			paginador.setPagina("");
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		response = daoConsultaPayments.consultarPayments(architectSessionBean, requestConsuPayments, beanPaginador);
		if(Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError()) && response.getMedEntrega().isEmpty()){
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())){
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		}else{
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}	
		return response;
	}
	
	/**
	 * @param session : ArchitechSessionBean
	 * @param beanConsultaPayments : BeanConsultaPayments
	 * @throws BusinessException, Exception controlada para los errores de ejecucion.
	 * @return response
	 */
	@Override
	public BeanResBase editarconsultasPayments(ArchitechSessionBean session, BeanConsultaPayme beanConsultaPayments)
			throws BusinessException {
		BeanResBase response = null;
		boolean operacion = false;
		
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		BeanConsultaPayme beanFilter = new BeanConsultaPayme();
		beanFilter.setClave(beanConsultaPayments.getClave());
		
		BeanConsultasPayme beanEditar = daoConsultaPayments.consultarPayments(session, beanFilter, beanPaginador);
		String dtoAnt = obtenerDatos(beanEditar.getMedEntrega().get(0),TYPE_UPDATE);
	
		response = daoConsultaPayments.editarPayments(session, beanConsultaPayments);
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())){
			operacion = true;
			response.setCodError(Errores.OK00003V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00003V);
		}else{
			operacion = false;
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		String dtoNuevo = obtenerDatos(beanConsultaPayments, TYPE_UPDATE);
		//Genera Pista
		generaPistaAuditoras(TYPE_UPDATE, "editarConsultaAOJ.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		//Retorna la Respuesta
		return response;
	}
	
	/**
	 * @param session
	 * @param request
	 * @param totalRegistros
	 * @throws BusinessException, Exception controlada para los errores de ejecucion.
     * @return response
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, BeanConsultaPayme request, int totalRegistros)
			throws BusinessException {
		
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		
		String query = UtileriasConsultaPayments.CONSULTA_EXPORTAR_TODO;
		/*+ UtileriasConsultaPayments.ORDEN;*/
		
		exportarRequest.setColumnasReporte("Clave Medio Entrega, Descripci󮬠Activado/Desactivado");
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setNombreRpt("ACTIVAR/DESACTIVAR CANAL ID UNICO");
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("ACTIVAR/DESACTIVAR CANAL ID UNICO");
	
		exportarRequest.setTotalReg(totalRegistros + ""); 
		
		// Se hace la consulta con el dao para exportar las operaciones
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
			
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		
		//Genera Pista
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodoCodErrorDevoluciones.do", operacion, session,"","", DATO_FIJO);
		//Retorna la Respuesta
		return response;
	}

	/**
	 * Consulta Combps 
	 * @param architectSessionBean : ArchitechSessionBean
	 * @param requestConsuPayments : BeanConsultaPayments
	 * @param beanPaginador : BeanPaginador
	 * @throws BusinessException, Exception controlada para los errores de ejecucion.
	 * @return response
	 */	
	@Override
	public BeanConsultasPayme consultasCombos(ArchitechSessionBean session,
			BeanConsultaPayme beanConsultaPayments, BeanPaginador beanPaginador) throws BusinessException {
		BeanConsultasPayme response = new BeanConsultasPayme();
		
		response = daoConsultaPayments.consultarCombo(session, beanConsultaPayments, beanPaginador);
		if(Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())){
			response.getBeanError().setCodError(Errores.OK00000V);
		}else{
			// Cuando se genera un error
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return response;
	}
	

	/**
	 * obtenerDatos  
	 * @param ant : BeanConsultaPayme
	 * @param accion : valor obtenido de tipo String
	 * @return dtoAnt
	 */	
	public String  obtenerDatos(BeanConsultaPayme ant, String accion){
		String dtoAnt ="";
		
		if (BOPaymentsImpl.TYPE_UPDATE.equals(accion)){
			dtoAnt=DATO_CTRL_DUPLICADOS + ant.getTipoOperacion();
		}
		return dtoAnt;
	}
	
	
	
	/**
	 * Se insertan daros a pistas de auditoria
	 * 
	 * @param operacion
	 * @param urlController
	 * @param resOp
	 * @param sesion
	 * @param dtoNuevo
	 * @param dtoAnterior
	 * @param dtoModificado
     * @return response
	 */
	private void generaPistaAuditoras (String operacion, String urlController,boolean resOp, ArchitechSessionBean sesion, String dtoNuevo,String dtoAnterior,String dtoModificado){
		// Objetos para las Pistas auditoras
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		// Enviamos los datos de la pista auditora
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		beanPista.setDtoNuevo(dtoNuevo);
		beanPista.setDtoAnterior(dtoAnterior);
		// Cuando la operacion es correcta
		if (resOp) {
			beanPista.setEstatus(utilPayments.OK);
		} else {
			beanPista.setEstatus(utilPayments.NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
}
