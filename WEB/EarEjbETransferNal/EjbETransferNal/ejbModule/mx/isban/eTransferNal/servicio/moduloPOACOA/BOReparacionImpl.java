/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOReparacionImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:27:41 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOOrdenes;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasOrdenes;

/**
 * Class BOReparacionImpl.
 *
 * Clase BO que apoya al BOOrdenes
 * implementando los metodos de negocio 
 * correspondientes.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOReparacionImpl extends Architech implements BOReparacion {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5049376618009834336L;

	/** La variable que contiene informacion con respecto a: dao ordenes. */
	@EJB
	private DAOOrdenes daoOrdenes;
	
	/** La variable que contiene informacion con respecto a: dao bit transaccional. */
	@EJB
	private DAOBitTransaccional daoBitTransaccional;
	
	/** La constante utils. */
	private static final UtileriasOrdenes utils = UtileriasOrdenes.getUtils();
	
	/**
	 * Obtener ordenes reparacion.
	 *
	 * Consultar los registros disponibles
	 * 
	 * @param modulo        El objeto: modulo SPID o SPEI
	 * @param beanPaginador El objeto: bean paginador
	 * @param session       El objeto: session
	 * @param sortField     El objeto: sort field
	 * @param sortType      El objeto: sort type
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResOrdenReparacion obtenerOrdenesReparacion(BeanModulo modulo, BeanPaginador beanPaginador,
			ArchitechSessionBean session, String sortField, String sortType) throws BusinessException {
		BeanResOrdenReparacionDAO beanResOrdenReparanDAO = null;
		final BeanResOrdenReparacion beanResOrdenRepara = new BeanResOrdenReparacion();
		
		BeanPaginador pag = beanPaginador;
		/** Validacion del paginador **/
		if(pag==null){
			pag = new BeanPaginador();
		}
		String sF = utils.getSortField(sortField);
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResOrdenReparanDAO = daoOrdenes.obtenerOrdenesReparacion(modulo, pag, session, sF, sortType);
		/** Validacion del resultado de la oepracion **/
		if (Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparanDAO.getCodError()) 
				&& beanResOrdenReparanDAO.getListBeanOrdenReparacion().isEmpty()) {
			beanResOrdenRepara.setCodError(Errores.ED00011V);
			beanResOrdenRepara.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparanDAO.getCodError())){
			beanResOrdenRepara.setListaOrdenesReparacion(beanResOrdenReparanDAO.getListBeanOrdenReparacion());          
			beanResOrdenRepara.setTotalReg(beanResOrdenReparanDAO.getTotalReg());
			pag.calculaPaginas(beanResOrdenReparanDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResOrdenRepara.setBeanPaginador(pag);
			beanResOrdenRepara.setCodError(Errores.OK00000V);
		}else{
			beanResOrdenRepara.setCodError(Errores.EC00011B);
			beanResOrdenRepara.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
		/** Retorno del objeto de respuesta **/		
		return beanResOrdenRepara;
	}

	/**
	 * Enviar ordenes reparacion.
	 * Envia los registros a la reparacion mediante un cambio de estatus.
	 * @param modulo                Representa el objeto: modulo SPID o SPEI
	 * @param beanReqOrdeReparacion Representa el objeto: bean req orde reparacion
	 * @param architechSessionBean  Representa el objeto: architech session bean
	 * @return Objeto bean res Representa el orden reparacion
	 * @throws BusinessException Representa la business exception
	 */
	@Override
	public BeanResOrdenReparacion enviarOrdenesReparacion(BeanModulo modulo,
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechSessionBean)
			throws BusinessException {
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		List<BeanOrdenReparacion> listaBeanOrdenReparacion = 
				helperRecepcionOperacion.filtraSelecionadosOrdenReparacion(
						beanReqOrdeReparacion, HelperRecepcionOperacion.SELECCIONADO);
		if (!listaBeanOrdenReparacion.isEmpty()) {
			beanResOrdenReparacion = actualizarOrdenesReparacion(modulo,
					beanReqOrdeReparacion, listaBeanOrdenReparacion, architechSessionBean);
			beanResOrdenReparacion.setCodError(Errores.OK00000V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			BeanPaginador paginador = beanReqOrdeReparacion.getPaginador();
			
			if (paginador == null){
				paginador = new BeanPaginador();
			}
			paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
			
			beanResOrdenReparacion = obtenerOrdenesReparacion(modulo, paginador, architechSessionBean, "", "");
			beanResOrdenReparacion.setCodError(Errores.ED00068V);
			beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		/** Retorno del objeto de respuesta **/
		return beanResOrdenReparacion;
	}
	
	/**
	 * Actualizar ordenes reparacion.
	 *
	 * @param modulo El objeto: modulo
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param listaBeanOrdenReparacion El objeto: lista bean orden reparacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	private BeanResOrdenReparacion actualizarOrdenesReparacion(BeanModulo modulo, BeanReqOrdeReparacion beanReqOrdeReparacion, 
			List<BeanOrdenReparacion> listaBeanOrdenReparacion, 
			ArchitechSessionBean architechSessionBean) throws BusinessException {
   	BeanResOrdenReparacionDAO beanResOrdenReparacionDAO = null;
   	BeanResOrdenReparacion beanResOrdenReparacion = new BeanResOrdenReparacion();
   	int reg = 0;
   	Date fechaActual = new Date();
   	String [] bitacora = new String[13];
   	for(BeanOrdenReparacion beanOrdenReparacion : listaBeanOrdenReparacion){
			beanResOrdenReparacionDAO = daoOrdenes.actualizarOdenesReparacion(modulo,
					beanOrdenReparacion ,  architechSessionBean);

			
			if (beanResOrdenReparacion.getCodError() == null) {
				beanResOrdenReparacion.setCodError(beanResOrdenReparacionDAO.getCodError());			
			}
			
			bitacora[0] = "ENVORDPORREP";
			bitacora[1] = beanResOrdenReparacionDAO.getCodError();
			bitacora[3] = "";
			bitacora[4] = "";
			bitacora[5] = "IntEnviarReparacion.do";
			bitacora[6] = "NA";
			bitacora[8] = beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia().toString();
			bitacora[9] = beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeOrd();
			bitacora[10] = beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeRec();
			bitacora[12] = beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono();
			
			if (!Errores.CODE_SUCCESFULLY.equals(beanResOrdenReparacionDAO.getCodError())) {
				SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
				bitacora[2] = "";
				bitacora[7] = formatDate.format(fechaActual);
				bitacora[11] = "ERR";
				setBitacoraTrans(bitacora, architechSessionBean);				
				
				throw new BusinessException(Errores.EC00011B);
       	}
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
			bitacora[2] = "EXITO";
			bitacora[7] = formatDate.format(fechaActual);
			bitacora[11] = "OK";
			setBitacoraTrans(bitacora, architechSessionBean);		
			reg = beanResOrdenReparacionDAO.getTotalReg();
       }
   	
   	beanResOrdenReparacion = obtenerOrdenesReparacion(modulo, beanReqOrdeReparacion.getPaginador(), architechSessionBean, "", "");
		beanResOrdenReparacion.setCodError(Errores.ED00011V);
		beanReqOrdeReparacion.setTotalReg(reg);
		beanResOrdenReparacion.setTipoError(Errores.TIPO_MSJ_INFO);
   	
		/** Retono del objeto de respuesta **/
		return beanResOrdenReparacion;
	}
	
	/**
	 * Obtener ordenes reparacion montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo    El objeto: modulo SPID o SPEI
	 * @param session   El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType  El objeto: sort type
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	@Override
	public BigDecimal obtenerOrdenesReparacionMontos(BeanModulo modulo, ArchitechSessionBean session, String sortField,
			String sortType) throws BusinessException {
		BigDecimal totalImporte = BigDecimal.ZERO;
		
		String sF = utils.getSortField(sortField);
		totalImporte = daoOrdenes.obtenerOrdenesReparacionMontos(modulo, session, sF, sortType);	
		/** Retorno de importe obtenido **/
		return totalImporte;
	}

	/**
	 * Sets the bitacora trans.
	 * 
	 * Metodo para enviar a bitacora
	 * 
	 * @param data El objeto: data
	 * @param sesion El objeto: sesion
	 */
	private void setBitacoraTrans(String [] data, ArchitechSessionBean sesion){
   	UtileriasBitTrans utileriasBitTrans = new UtileriasBitTrans();
   	BeanReqInsertBitTrans beanReqInsertBitTrans = 
   		utileriasBitTrans.createBitacoraBitTrans(data[0], data[1], data[2], data[3], 
   				data[4], data[5], data[6], data[7],data[8],
   				data[9], data[10],
   				data[11],data[12],  sesion);
   	/** Envio a bitacora **/
   	daoBitTransaccional.guardaBitacora(beanReqInsertBitTrans, sesion);
   }
}
