/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOParametrosPOACOAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanParamCifradoDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanProcesosPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanResObtenFechaCtrlDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResObtenInstFechaCtrl;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOParametrosPOACOA;
import mx.isban.eTransferNal.dao.catalogos.DAOParametrosPOACOA2;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasParametros;

/**
 * Class BOParametrosPOACOAImpl.
 *
 * @author ooamador
 */
@Stateless
@Remote(BOParametrosPOACOA.class)
public class BOParametrosPOACOAImpl extends Architech implements BOParametrosPOACOA {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 805654771749650198L;


	/** Constenate uno. */
	private static final String UNO = "1";
	
	
	/** Constenate dos. */
	private static final String DOS = "2";
	
	
	/**Bitacora actualizar*/
	private static final String UPDATE_BIT = "UPDATE";
	
	/**Bitacora insertar*/
	private static final String INSERT_BIT = "INSERT";
	
	/** Constante Pendiente para Bitacora */
	private static final String PENDIENTE_BITA = "PENDIENTE";
	
	/** Constante descripcion de transaccion para Bitacora */
	private static final String DESCTRAN_BITA = "TRANSACCION EXITOSA";
	
	/** Constante nombre de tabla para Bitacora */
	private static final String TABLA_BITA = "TRAN_POA_COA_CTRL";
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;

	/** La constante OPER_UPDATE. */
	private static final String OPER_UPDATE = "UPDATE";
	
	/** La constante URL_PARAMETROS. */
	private static final String URL_PARAMETROS = "parametrosFase.do";
	
	/** DAO para las transacciones a base de datos. */
	@EJB
	private DAOParametrosPOACOA daoParametrosPOACOA;
	
	/** DAO para las transacciones a base de datos. */
	@EJB
	private DAOParametrosPOACOA2 daoParametrosPOACOA2;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	
	/** La constante utileriasPOACOA. */
	private static final UtileriasParametros utileriasPOACOA = UtileriasParametros.getInstancia();
	
	@Override
	public BeanResBase actDesPOA(boolean esActivacion, boolean esActivoCOA, String modulo,
			ArchitechSessionBean architectSessionBean) {
		BeanProcesosPOACOA beanProcesosactDesPOACOA = new BeanProcesosPOACOA();
		/** Se declara el objeto de respuesta **/
		BeanResBase resActDesPOA = new BeanResBase();
		String operacion = "";
		Boolean respuestaOperacion = false;
		
		/** Seteo de datos **/
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(modulo, architectSessionBean);
		beanProcesosactDesPOACOA.setCveInstitucion(beanResObtenInstFechaCtrl.getInstitucion());
		beanProcesosactDesPOACOA.setFchOperacion(beanResObtenInstFechaCtrl.getFechaOperacion());
		
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResPOACOA beanResPOACOA = daoParametrosPOACOA2.obtenParamsPOACOA(beanProcesosactDesPOACOA, modulo, architectSessionBean);
		
		/** Trara de desactivar pero la bandera de no retorno ya esta prendida*/
		if(utileriasPOACOA.verEstatusUno(beanResPOACOA, esActivacion)){
			/** Se setea el codigo de error **/
			resActDesPOA.setCodError(Errores.ED00079V);
			resActDesPOA.setTipoError(Errores.TIPO_MSJ_ALERT);
			return resActDesPOA;
		}
		/** Se valida el estatus del modulo **/
			if (esActivacion) {
				if (esActivoCOA) {
					beanProcesosactDesPOACOA.setCveProceso(DOS);
					/** Se realiza la peticion de la operacion al  DAO **/
					resActDesPOA = daoParametrosPOACOA.actualizaProcesoPOACOA(
							beanProcesosactDesPOACOA, modulo, false, UNO,architectSessionBean);
					operacion = UPDATE_BIT;
				} else {
					if(utileriasPOACOA.verEstatus(beanResPOACOA)) {
						beanProcesosactDesPOACOA.setCveProceso(UNO);
						beanProcesosactDesPOACOA.setFase("0");
						resActDesPOA = daoParametrosPOACOA.insertaActivacionPOACOA(
								beanProcesosactDesPOACOA, modulo, architectSessionBean);
						operacion = INSERT_BIT;
					} else {
						beanProcesosactDesPOACOA.setCveProceso(beanResPOACOA.getActivacion());
						/** Se realiza la peticion de la operacion al  DAO **/
						resActDesPOA = daoParametrosPOACOA.actualizaProcesoPOACOA(
								beanProcesosactDesPOACOA, modulo, false, UNO,architectSessionBean);
						operacion = UPDATE_BIT;
					}
				}
			} else {
				beanProcesosactDesPOACOA.setCveProceso(UNO);
				/** Se realiza la peticion de la operacion al  DAO **/
				resActDesPOA = daoParametrosPOACOA.actualizaProcesoPOACOA(beanProcesosactDesPOACOA, modulo,
						true, null,architectSessionBean);
				operacion = UPDATE_BIT;
			}
			continuaMetodo(modulo,resActDesPOA,respuestaOperacion,operacion,beanProcesosactDesPOACOA,architectSessionBean);
			
		/** Retorno del metodo **/
		return resActDesPOA;
	}

	/**
	 * Continua metodo.
	 *
	 * @param modulo the modulo
	 * @param res the res
	 * @param respuestaOperacion the respuesta operacion
	 * @param operacion the operacion
	 * @param beanProcesosPOACOA the bean procesos POACOA
	 * @param architectSessionBean the architect session bean
	 */
	private void continuaMetodo(String modulo, BeanResBase res, Boolean respuestaOperacion, String operacion,
			BeanProcesosPOACOA beanProcesosPOACOA, ArchitechSessionBean architectSessionBean) {
		if (res.getCodError().equals(Errores.OK00000V)) {
			/** En caso de que la operacion ejcutada resulte correcta entonces el registrro que se envia a la bitacora sera TRUE **/
			respuestaOperacion = true;
		}
		
		/** Se valida el modulo para guardar el bitacora **/
		if ("2".equals(modulo)) {
			/** Cuando es el modulo POA/COA SPID se usa el nuevo metodo para generar el registro **/
			utileriasPOACOA.generarPistaAuditora(boPistaAuditora,operacion, "activarDesactivarPOA.do", respuestaOperacion, architectSessionBean, "ACTIVACION", modulo);
		} else {
			/** Se crea una instancia a la clase Utilerias **/
			Utilerias utilerias = Utilerias.getUtilerias();
			/** Se crea el registro de bitacora **/
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("activarDesactivarPOA.do", "1", "", new Date(),
							PENDIENTE_BITA, "ACTDESPOA", TABLA_BITA,
							"CVE_MI_INSTITUC", res.getCodError(), DESCTRAN_BITA,
							operacion, beanProcesosPOACOA.getCveInstitucion());
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
		}
		
	}

	@Override
	public BeanResBase actDescCOA(boolean esActivacion, String modulo,
			ArchitechSessionBean architectSessionBean) {
		BeanProcesosPOACOA beanProcesosActDesCOA = new BeanProcesosPOACOA();
		/** Se declara el objeto de respuesta **/
		BeanResBase resActDesCOA = new BeanResBase();
		String operacionActDesCOA = "";
		Boolean respuestaOperacion = false;
		/** Seteo de datos **/
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(modulo, architectSessionBean);
		beanProcesosActDesCOA.setCveInstitucion(beanResObtenInstFechaCtrl.getInstitucion());
		beanProcesosActDesCOA.setFchOperacion(beanResObtenInstFechaCtrl.getFechaOperacion());
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResPOACOA beanResPOACOA = daoParametrosPOACOA2.obtenParamsPOACOA(beanProcesosActDesCOA, modulo, architectSessionBean);
		/** Trara de desactivar pero la bandera de no retorno ya esta prendida*/
		if(utileriasPOACOA.verEstatusUno(beanResPOACOA, esActivacion)){
			/** Se setea el codigo de error **/
			resActDesCOA.setCodError(Errores.ED00079V);
			resActDesCOA.setTipoError(Errores.TIPO_MSJ_ALERT);
			return resActDesCOA;
		}
		/** Se valida el estatus del modulo **/
		if(esActivacion) {
			if(utileriasPOACOA.verEstatus(beanResPOACOA)) {
				beanProcesosActDesCOA.setCveProceso(DOS);				
				beanProcesosActDesCOA.setFase("0");
				/** Se realiza la peticion de la operacion al  DAO **/
				resActDesCOA = daoParametrosPOACOA.insertaActivacionPOACOA(
						beanProcesosActDesCOA, modulo, architectSessionBean);
				operacionActDesCOA = INSERT_BIT;
			} else {
				beanProcesosActDesCOA.setCveProceso(beanResPOACOA.getActivacion());
				resActDesCOA = daoParametrosPOACOA.actualizaProcesoPOACOA(
						beanProcesosActDesCOA, modulo, false, DOS, architectSessionBean);
				operacionActDesCOA = UPDATE_BIT;
			}
		} else {				
			beanProcesosActDesCOA.setCveProceso(DOS);
			/** Se realiza la peticion de la operacion al  DAO **/
			resActDesCOA = daoParametrosPOACOA.actualizaProcesoPOACOA(beanProcesosActDesCOA, modulo,
					true, null, architectSessionBean);
			operacionActDesCOA = UPDATE_BIT;
		}
		if (resActDesCOA.getCodError().equals(Errores.OK00000V)) {
			/** En caso de que la operacion ejcutada resulte correcta entonces el registrro que se envia a la bitacora sera TRUE **/
			respuestaOperacion = true;
		}
		/** Se valida el modulo para guardar el bitacora **/
		if ("2".equals(modulo)) {
			/** Cuando es el modulo POA/COA SPID se usa el nuevo metodo para generar el registro **/
			utileriasPOACOA.generarPistaAuditora(boPistaAuditora,operacionActDesCOA, "activarDesactivarCOA.do", respuestaOperacion, architectSessionBean, "ACTIVACION", modulo);
		} else {
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		/** Se crea el registro de bitacora **/
		BeanReqInsertBitacora beanReqInsertBitacora = utilerias
				.creaBitacora("activarDesactivarCOA.do", "1", "", new Date(),
						PENDIENTE_BITA, "ACTDESCOA", TABLA_BITA,
						"CVE_MI_INSTITUC", resActDesCOA.getCodError(), DESCTRAN_BITA,
						operacionActDesCOA, beanProcesosActDesCOA.getCveInstitucion());
		daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
		}
		/** Retorno del metodo **/
		return resActDesCOA;
	}

	@Override
	public BeanResBase activaFases(String fase,String liqFinal, String modulo,
			ArchitechSessionBean architectSessionBean) {
		BeanProcesosPOACOA beanProcesosPOACOAFases = new BeanProcesosPOACOA();
		/** Se declara el objeto de respuesta **/
		BeanResBase res = new BeanResBase();
		Boolean respuestaOperacion = false;
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(modulo, architectSessionBean);
			beanProcesosPOACOAFases.setCveInstitucion(beanResObtenInstFechaCtrl.getInstitucion());
			beanProcesosPOACOAFases.setFchOperacion(beanResObtenInstFechaCtrl.getFechaOperacion());
			beanProcesosPOACOAFases.setFase(fase);
			beanProcesosPOACOAFases.setLiqFinal(liqFinal);
			/** Se realiza la peticion de la operacion al  DAO **/
			res = daoParametrosPOACOA.actualizaFase(beanProcesosPOACOAFases, modulo, architectSessionBean);
			/**  Se valida la respuesta **/
			if (res.getCodError().equals(Errores.OK00000V)) {
				/** En caso de que la operacion ejcutada resulte correcta entonces el registrro que se envia a la bitacora sera TRUE **/
				respuestaOperacion = true;
			}
			/** Se valida el modulo para guardar el bitacora **/
			if ("2".equals(modulo)) {
				/** Cuando es el modulo POA/COA SPID se usa el nuevo metodo para generar el registro **/
				utileriasPOACOA.generarPistaAuditora(boPistaAuditora,OPER_UPDATE, URL_PARAMETROS, respuestaOperacion, architectSessionBean, "FASE", modulo);
			} else {
			/** Se crea una instancia a la clase Utilerias **/
			Utilerias utilerias = Utilerias.getUtilerias();
			/** Se crea el registro de bitacora **/
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora(URL_PARAMETROS, "1", "", new Date(),
							PENDIENTE_BITA, "ACTFASE", TABLA_BITA,
							"FASE", res.getCodError(), DESCTRAN_BITA,
							UPDATE_BIT, beanProcesosPOACOAFases.getFase());
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
			}
		/** Retorno del metodo **/
		return res;
	}

	@Override
	public BeanResBase actDesFirmas(String cifrado,
			ArchitechSessionBean architectSessionBean) {
		/** Se declara el objeto de respuesta **/
		BeanResBase res = new BeanResBase();
		String operacion = "";
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(null, architectSessionBean);
		/** Se realiza la peticion de la operacion al  DAO **/	
		BeanParamCifradoDAO beanParamCifradoDAO = daoParametrosPOACOA2.obtenParametroCifrado(beanResObtenInstFechaCtrl.getInstitucion(),
					architectSessionBean);
		/**  Se valida la respuesta **/
			if (Errores.OK00000V.equals(beanParamCifradoDAO.getCodError())){
				daoParametrosPOACOA.actualizaParametroFirma(beanResObtenInstFechaCtrl.getInstitucion(),
						cifrado, architectSessionBean);
				operacion = UPDATE_BIT;
			} else {
				/** Se realiza la peticion de la operacion al  DAO **/
				daoParametrosPOACOA.insertaParametroFirma(beanResObtenInstFechaCtrl.getInstitucion(),
						architectSessionBean);
				operacion = INSERT_BIT;
			}
			/** Se crea una instancia a la clase Utilerias **/
			Utilerias utilerias = Utilerias.getUtilerias();
			/** Se crea el registro de bitacora **/
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora("activarDesactivarCifrado.do", "1", "",
							new Date(), PENDIENTE_BITA, "ACTDESFIRMA",
							"TRAN_POA_COA_PARAM", "CIFRADO", res.getCodError(), DESCTRAN_BITA, operacion,
							cifrado);
			daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
		/** Retorno del metodo **/
		return res;
	}
	
	@Override
	public BeanResPOACOA obtenParametrosPOACOA(String modulo,
			ArchitechSessionBean architectSessionBean){
		BeanProcesosPOACOA beanProcesosParamPOACOA = new BeanProcesosPOACOA();
		/** Se declara el objeto de respuesta **/
		BeanResPOACOA beanResPOACOA = new BeanResPOACOA();
		
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(modulo, architectSessionBean);
			beanProcesosParamPOACOA.setCveInstitucion(beanResObtenInstFechaCtrl.getInstitucion());
			beanProcesosParamPOACOA.setFchOperacion(beanResObtenInstFechaCtrl.getFechaOperacion());
			/** Se realiza la peticion de la operacion al  DAO **/
			beanResPOACOA = daoParametrosPOACOA2.obtenParamsPOACOA(
					beanProcesosParamPOACOA, modulo, architectSessionBean);
			/** Se realiza la peticion de la operacion al  DAO **/
			BeanParamCifradoDAO beanParamCifradoDAO = daoParametrosPOACOA2.obtenParametroCifrado(beanResObtenInstFechaCtrl.getInstitucion(), architectSessionBean);
			beanResPOACOA.setCifrado(beanParamCifradoDAO.getCifrado());
		/** Retorno del metodo **/	
		return beanResPOACOA;
	}
	

	
	@Override
	public BeanResBase generarArchivo(String tipoGenerar, String modulo, 
			ArchitechSessionBean architectSessionBean) {
		BeanProcesosPOACOA beanProcesosPOACOA = new BeanProcesosPOACOA();
		/** Se declara el objeto de respuesta **/
		BeanResBase res = new BeanResBase();
		Boolean respuestaOperacion = false;
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = obtenMiInstitucionFechaOperacion(modulo, architectSessionBean);
			beanProcesosPOACOA.setCveInstitucion(beanResObtenInstFechaCtrl.getInstitucion());
			beanProcesosPOACOA.setFchOperacion(beanResObtenInstFechaCtrl.getFechaOperacion());
			beanProcesosPOACOA.setGenerar(tipoGenerar);
			
			/** Se realiza la peticion de la operacion al  DAO **/
			BeanResPOACOA beanResPOACOA= daoParametrosPOACOA2.obtenParamsPOACOA(beanProcesosPOACOA, modulo, architectSessionBean);
			beanProcesosPOACOA.setFase(beanResPOACOA.getFase());
			if("0".equals(beanResPOACOA.getGenerar())){
				/** Se realiza la peticion de la operacion al  DAO **/
				res = daoParametrosPOACOA2.actualizaGenerar(beanProcesosPOACOA, modulo, architectSessionBean);
				/**  Se valida la respuesta **/
				if (res.getCodError().equals(Errores.OK00000V)) {
					/** En caso de que la operacion ejcutada resulte correcta entonces el registrro que se envia a la bitacora sera TRUE **/
					respuestaOperacion = true;
				}
				/** Se valida el modulo para guardar el bitacora **/
				if ("2".equals(modulo)) {
					/** Cuando es el modulo POA/COA SPID se usa el nuevo metodo para generar el registro **/
					utileriasPOACOA.generarPistaAuditora(boPistaAuditora,OPER_UPDATE, "generarArchivoPOACOA.do", respuestaOperacion, architectSessionBean, "GENERAR", modulo);
				} else {
				/** Se crea una instancia a la clase Utilerias **/
				Utilerias utilerias = Utilerias.getUtilerias();
				/** Se crea el registro de bitacora **/
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacora(URL_PARAMETROS, "1", "", new Date(),
								PENDIENTE_BITA, "GENARCH", TABLA_BITA,
								"GENERAR", res.getCodError(), DESCTRAN_BITA,
								UPDATE_BIT, beanProcesosPOACOA.getFase());
				daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
				}
			}
		/** Retorno del metodo **/
		return res;
	}

	/**
	 * Obtiene la institucion 
	 * @param architectSessionBean Datos de sesipn
	 * @return Institucion de tipo String
	 */
	private BeanResObtenInstFechaCtrl obtenMiInstitucionFechaOperacion(String modulo, ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResObtenInstFechaCtrl beanResObtenInstFechaCtrl = new BeanResObtenInstFechaCtrl();
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResConsMiInstDAO resInstitucion = daoParametrosPOACOA2.consultaMiInstitucion(modulo, architectSessionBean);
		/**  Se valida la respuesta **/
		if(!Errores.OK00000V.equals(resInstitucion.getCodError())){
			beanResObtenInstFechaCtrl.setInstitucion(resInstitucion.getMiInstitucion());
			beanResObtenInstFechaCtrl.setCodError(resInstitucion.getCodError());
			/** Retorno del metodo en caso de cumplir con la condicion **/
			return beanResObtenInstFechaCtrl;
		}
		beanResObtenInstFechaCtrl.setInstitucion(resInstitucion.getMiInstitucion());
		BeanResObtenFechaCtrlDAO beanResObtenFechaCtrlDAO = obtenFechaOperacion(resInstitucion.getMiInstitucion(), modulo, architectSessionBean);
		beanResObtenInstFechaCtrl.setFechaOperacion(beanResObtenFechaCtrlDAO.getFecha());
		beanResObtenInstFechaCtrl.setCodError(resInstitucion.getCodError());
		/** Retorno del metodo **/
		return beanResObtenInstFechaCtrl;
	}
	
	/**
	 * Obtiene la fecha de operacion
	 * @param cveInstitucion Clave de la institucion
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResObtenFechaCtrlDAO bean BeanResObtenFechaCtrlDAO
	 */
	private BeanResObtenFechaCtrlDAO obtenFechaOperacion(String cveInstitucion, String modulo,
			ArchitechSessionBean architectSessionBean) {
		//FCH_LIQ_FINAL Se obtiene la fecha TRAN_CTRL_FECHAS
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResObtenFechaCtrlDAO beanResObtenFechaCtrl = daoParametrosPOACOA2.obtenFechaCTRLFECHAS(true,
					architectSessionBean);
		/**  Se valida la respuesta **/
		if(!(Errores.OK00000V.equals(beanResObtenFechaCtrl.getCodError()) && 
				!"".equals(beanResObtenFechaCtrl.getFecha()))){
			//FCH_CONTA Se obtiene la fecha TRAN_CTRL_FECHAS
			/** Se realiza la peticion de la operacion al  DAO **/
				beanResObtenFechaCtrl = daoParametrosPOACOA2.obtenFechaCTRLFECHAS(false,
						architectSessionBean);
				/**  Se valida la respuesta **/
				if(!(Errores.OK00000V.equals(beanResObtenFechaCtrl.getCodError()) && 
						!"".equals(beanResObtenFechaCtrl.getFecha()))){
					//FCH_OPERACION TRAN_SPEI_CTRL
					/** Se realiza la peticion de la operacion al  DAO **/
					beanResObtenFechaCtrl = daoParametrosPOACOA2.obtenFechaSPEI(cveInstitucion, modulo,
							architectSessionBean);
					/**  Se valida la respuesta **/
					if(!(Errores.OK00000V.equals(beanResObtenFechaCtrl.getCodError()) && 
							!"".equals(beanResObtenFechaCtrl.getFecha()))){
						beanResObtenFechaCtrl = new BeanResObtenFechaCtrlDAO(); 
						beanResObtenFechaCtrl.setCodError("");
						beanResObtenFechaCtrl.setFecha("");
					}
				}
		}
		/** Retorno del metodo **/
		return beanResObtenFechaCtrl;
	}

}
