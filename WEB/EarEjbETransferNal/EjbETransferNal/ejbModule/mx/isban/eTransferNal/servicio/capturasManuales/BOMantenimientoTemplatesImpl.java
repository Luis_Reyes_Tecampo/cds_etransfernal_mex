/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMantenimientoTemplatesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 28 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.servicio.capturasManuales;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAltaEditTemplateDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsultaTemplatesDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOMantenimientoTemplates;
import mx.isban.eTransferNal.dao.capturasManuales.DAOMantenimientoTemplatesExt;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.UtileriasDatosMtoTemplate;
import mx.isban.eTransferNal.utilerias.UtileriasPistasMtoTemplate;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Implementacion Service BO  que actua como firma de la capa de negocios.
 */
//Implementacion Service BO  que actua como firma de la capa de negocios
@Remote(BOMantenimientoTemplates.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMantenimientoTemplatesImpl extends Architech implements BOMantenimientoTemplates{
	
	//Propiedad del tipo long que almacena el valor de serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 4011095452759110223L;
	
	//La constante TABLE_TEMPLATE
	/** La constante TABLE_TEMPLATE. */
	private static final String TABLE_TEMPLATE = "TRAN_CAP_TEMPLATE";
	
	//La constante OK
	/** La constante OK. */
	private static final String OK = "OK";
	
	//La constante NOK
	/** La constante NOK. */
	private static final String NOK = "NOK";

	
	//Componente de inyeccion del dao de mantenimineto de templates
	/** Componente de inyeccion del dao de mantenimineto de templates. */
	@EJB
	private transient DAOMantenimientoTemplates daoMantenimiento;
	
	//Componente de inyeccion del dao de mantenimineto de templates externo
	/** Componente de inyeccion del dao de mantenimineto de templates externo. */
	@EJB
	private transient DAOMantenimientoTemplatesExt daoMantenimientoExt;
	
	//Componente de inyeccion del dao de pistas de auditoria.
	/**
	 * Componente de inyeccion del dao de pistas de auditoria.
	 */
	@EJB
	private transient DAOBitAdministrativa daoBitAdministrativa;
	
	//Metodo para consultar los templates
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates#consultarTemplates(mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResPrincipalTemplate consultarTemplates(BeanReqConsultaTemplates req, ArchitechSessionBean session) throws BusinessException {
		
		//Se crean objetos de soporte
		BeanResPrincipalTemplate beanViewRespuesta = new BeanResPrincipalTemplate();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		BeanPaginador paginador = null;
		
		//Recuperar y propagar el paginador
		paginador = req.getPaginador();
		if(paginador == null){
	    	  paginador = new BeanPaginador();
	    	  req.setPaginador(paginador);
	     }
		//Se crea el paginado
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		//Inicia proceso de consulta
		BeanResConsultaTemplatesDAO daoRespuesta = daoMantenimiento.consultarTemplates(req, session);
		
		//Se valida la existencia de registros
		if((Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError()) && daoRespuesta.getListaTemplates().isEmpty()) || Errores.ED00011V.equals(daoRespuesta.getCodError())){
			utileriasDatos.setNoExistenRes(beanViewRespuesta);			
		} else if(Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError())){
			//Si es exitoso se ponen todos los datos en el bean de respuesta
			beanViewRespuesta.setTotalRegistros(daoRespuesta.getTotalRegistros());
			beanViewRespuesta.setListaTemplates(daoRespuesta.getListaTemplates());			
			paginador.calculaPaginas(daoRespuesta.getTotalRegistros(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanViewRespuesta.setPaginador(paginador);
			utileriasDatos.setExitoInfo(beanViewRespuesta);
		} else {
			//Si no pasa ninguna validacion se arroja error de conexion
			throw new BusinessException(daoRespuesta.getCodError(), daoRespuesta.getMsgError());
		}
		
		return beanViewRespuesta;
	}
	
	/**
	 * Metodo encapsulado para obtener los templates considerando el argumento de filtro.
	 *
	 * @param filtro utilizado para buscar los templates
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @param paginador the paginador
	 * @return Objeto con la lista de templates
	 * @throws BusinessException the business exception
	 */
	//Metodo encapsulado para obtener los templates considerando el argumento de filtro
	private BeanResPrincipalTemplate consultarTemplates(String filtro, ArchitechSessionBean session, BeanPaginador paginador) throws BusinessException {
		BeanResPrincipalTemplate beanViewRes;		
		BeanReqConsultaTemplates reqConsulta = new BeanReqConsultaTemplates();
		reqConsulta.setFiltroEstado(filtro);
		if(paginador != null){
			paginador.setAccion("ACT");
		}
		reqConsulta.setPaginador(paginador);
		beanViewRes= consultarTemplates(reqConsulta, session);
		if(paginador != null && beanViewRes.getPaginador() != null){
			beanViewRes.getPaginador().setAccion("");
		}
		return beanViewRes;
	}
	
	//Metodo para guardar el template
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates#guardarTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResPrincipalTemplate guardarTemplate(BeanReqAltaEditTemplate req,
			ArchitechSessionBean session) throws BusinessException {
		
		//Se crean objetos de soporte
		BeanResPrincipalTemplate beanViewRes= new BeanResPrincipalTemplate();
		BeanResBase beanResGuardarDAO = null;
		
		//Auditoria
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		UtileriasPistasMtoTemplate utileriaTemplate = new UtileriasPistasMtoTemplate();
		String statusProceso=OK;
		String pista="";
		
		//Se guarda el template con el dao
		beanResGuardarDAO= daoMantenimiento.guardarTemplate(req, session);
		
		if(Errores.EC00011B.equals(beanResGuardarDAO.getCodError())){
			throw new BusinessException(beanResGuardarDAO.getCodError(), beanResGuardarDAO.getMsgError());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResGuardarDAO.getCodError())){
			//Se busca solo si en la pagina de templates se habia buscado
			if(req.getForward()){			
				beanViewRes= consultarTemplates(req.getFiltroEstado(), session, null);
			}
			statusProceso=OK;
			utileriasDatos.setExitoAlert(beanViewRes);
		}else{
			//Si termino con error, deben recuperarse los datos del formulario req.
			statusProceso=NOK;
			utileriasDatos.copiarDatosReqToRes(req, beanViewRes);
			utileriasDatos.setError(beanResGuardarDAO.getCodError(), beanViewRes);
			
			//Registro duplicado sin pista
			if(Errores.ED00061V.equals(beanResGuardarDAO.getCodError())){
				return beanViewRes;
			}
		}
		
		//Se genera el string de pista
		pista="Temp=>{" + utileriaTemplate.obtenerDatosTemplate(req.getTemplate()) + "} Aut=>{"+
				utileriaTemplate.obtenerDatosUsuariosTemplate(req.getTemplate().getUsuariosAutorizan()) + "} Cam=>{" +
				utileriaTemplate.obtenerDatosCamposTemplate(req.getTemplate().getCampos()) + "}";
		
		//Se genera el bean de la pista
		BeanReqInsertBitAdmin beanTemp = 
    			utileriasBitAdmin.createBitacoraBitAdmin(statusProceso,TABLE_TEMPLATE, "INSERT",
    					"guardarTemplate.do","", pista,
    					" "," ","GUARDAR TEMPLATE", session);
		
		//Se guarda la pista en bitacora
		daoBitAdministrativa.guardaBitacora(beanTemp, session);
		return beanViewRes;
	}
	
	//Metodo para eliminar templates
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates#eliminarTemplates(mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResPrincipalTemplate eliminarTemplates(
			BeanReqConsultaTemplates req, ArchitechSessionBean session) throws BusinessException {
		
		//Se crean objetos de soporte
		BeanResPrincipalTemplate beanViewResFinal= new BeanResPrincipalTemplate();
		List<BeanTemplate> listaEliminar = new ArrayList<BeanTemplate>();
		BeanResBase beanResDAO = null;
		
		// Auditoria
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		String statusProceso = OK, codErrorProceso="";
		Boolean erroresProceso = false;
		
		//Obtener la lista de templates que fueron seleccionados
		for(BeanTemplate template: req.getListaTemplates()){
			if(template.getSeleccionado()){
				listaEliminar.add(template);
			}
		}
		
		//Verificar si los componentes se pueden eliminar
		for(BeanTemplate template: listaEliminar){
			beanResDAO= daoMantenimientoExt.verificarOperaciones(template, session);
			
			//Error en operaciones. Copiar lista templates, paginador, totalRegistros
			if(Errores.CMMT003V.equals(beanResDAO.getCodError())){
				beanViewResFinal.setListaTemplates(req.getListaTemplates());
				beanViewResFinal.setPaginador(req.getPaginador());
				beanViewResFinal.setTotalRegistros(req.getTotalRegistros());
				
				utileriasDatos.setError(beanResDAO.getCodError(), beanViewResFinal);
				return beanViewResFinal;
			} else if(Errores.EC00011B.equals(beanResDAO.getCodError())){
				throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
			}
		}
		
		//CONT. Eliminar componente por componente
		for(BeanTemplate template: listaEliminar){
			beanResDAO= daoMantenimiento.eliminarTemplate(template, session);
			List<Object> list=UtileriasDatosMtoTemplate.getUtilerias().evaluarCodErrorEliminar(beanResDAO, beanViewResFinal);
			statusProceso=list.get(0).toString();
			erroresProceso=(Boolean) list.get(1);
			codErrorProceso=list.get(2).toString();
			
			//Se genera pista en bitacora de eliminado
			BeanReqInsertBitAdmin beanAudTemplate = 
			  			utileriasBitAdmin.createBitacoraBitAdmin(statusProceso, TABLE_TEMPLATE, "DELETE",
			  					"eliminarTemplates.do", template.getIdTemplate() ,"",
			  					" "," ","ELIMINAR TEMPLATE", session);
			
			//Se guarda pista en bitacora
			daoBitAdministrativa.guardaBitacora(beanAudTemplate, session);			
		}
		
		// Buscar nuevamente los templates para actualizar la vista.
		beanViewResFinal = consultarTemplates(req.getFiltroEstado(), session, req.getPaginador());
		
		//Verificar si la eliminacion multiple genero algun error.
		if(!erroresProceso){
			//Si no hay error es exitoso el borrado
			utileriasDatos.setExitoAlert(beanViewResFinal);
		}else{
			//Si hay un error se setea
			utileriasDatos.setError(codErrorProceso,beanViewResFinal);
		}
		
		return beanViewResFinal;
	}

	//Metodo para obtener el template
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates#obtenerTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResPrincipalTemplate obtenerTemplate(
			BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException{
		
		//Se crea objetos de soporte
		BeanResPrincipalTemplate beanViewRes= new BeanResPrincipalTemplate();
		BeanResAltaEditTemplateDAO daoRespuesta = daoMantenimientoExt.obtenerTemplate(req, session);
		
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();
		
		//Si es exitoso se setea en el bean de respuesta los dats
		if (Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError())) {
			beanViewRes.setTemplate(daoRespuesta.getTemplate());
			beanViewRes.setListaLayouts(daoRespuesta.getListaLayouts());
			beanViewRes.setListaUsuarios(daoRespuesta.getListaUsuarios());
			beanViewRes.setListaCamposMaster(daoRespuesta.getListaCamposMaster());	
			
			utileriasDatos.setExitoInfo(beanViewRes);
		} else if (Errores.ED00011V.equals(daoRespuesta.getCodError())) {
			//Si hay error se setea que no existen registros
			utileriasDatos.setNoExistenRes(beanViewRes);
		} else if(Errores.EC00011B.equals(daoRespuesta.getCodError())){
			throw new BusinessException(daoRespuesta.getCodError(), daoRespuesta.getMsgError());
		} else {
			//Si falla se pone el error que trae el dao
			utileriasDatos.setError(daoRespuesta.getCodError(),beanViewRes);
		}

		return beanViewRes;
	}

	//Metodo para actualizar el template
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates#actualizarTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResPrincipalTemplate actualizarTemplate(
			BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session) throws BusinessException {
		//Se crean objetos de soporte
		BeanResPrincipalTemplate beanViewRes= new BeanResPrincipalTemplate();
		List<BeanUsuarioAutorizaTemplate> usuariosAnexar = new ArrayList<BeanUsuarioAutorizaTemplate>();
		List<BeanUsuarioAutorizaTemplate> usuariosEliminar = new ArrayList<BeanUsuarioAutorizaTemplate>();
		List<Object> listaEvaluacionCodErr = new ArrayList<Object>();
		BeanResBase beanResDAO = null;
		Boolean nuevoEstadoOperaciones = false, erroresProceso=false;
		String  codErrorProceso="";
		
		// Auditoria
		UtileriasDatosMtoTemplate utileriasDatos= UtileriasDatosMtoTemplate.getUtilerias();		
		BeanResPrincipalTemplate originalTemplate = obtenerTemplate(beanReq, session);		
		
		//Se hace la verificacion de las operaciones
		beanResDAO = daoMantenimientoExt.verificarOperaciones(beanReq.getTemplate(), session);
		nuevoEstadoOperaciones = Errores.CMMT003V.equals(beanResDAO.getCodError());
		
		//CONCURRENCIA. Ocurrio una concurrencia de operaciones asociadas, devolver la consulta original
		if(!beanReq.getTemplate().getOperacionesAsociadas()  && nuevoEstadoOperaciones){
			utileriasDatos.setError(Errores.CMMT005V, originalTemplate);
			return originalTemplate;
		}
		
		//Se determinan los usuarios que se eliminaran y los que se agregaran
		usuariosEliminar = obtenerUsuariosEliminar(beanReq.getListaUsuarios(), beanReq.getTemplate().getIdTemplate(), session);
		usuariosAnexar = obtenerUsuariosNuevos(beanReq.getTemplate().getUsuariosAutorizan(), beanReq.getTemplate().getIdTemplate(),  session);
		
		//Verificar que alguno de los usuarios a eliminar no tenga operaciones aprobadas en ese template 
		if(verificarUsuariosOperaciones(usuariosEliminar, session)){
			utileriasDatos.copiarDatosReqToRes(beanReq, beanViewRes);				
			utileriasDatos.setError(Errores.CMMT006V, beanViewRes);
			return beanViewRes;
		}
		
		//Copiar los usuarios a eliminar y a insertar
		utileriasDatos.establecerUsuariosEliminarAgregar(beanReq, usuariosAnexar, usuariosEliminar);			
		
		//Proceso 1. Eliminar y crear usuarios
		if(!usuariosEliminar.isEmpty() || !usuariosAnexar.isEmpty()){
			beanResDAO = daoMantenimiento.actualizarUsuarios(beanReq, session);
			
			//se llama funcion para evaluar codigoErr obtenido por el DAO
			listaEvaluacionCodErr = utileriasDatos.validaCodigoErrorActualizaTmpl(beanViewRes, beanResDAO, false);
			erroresProceso = (Boolean) listaEvaluacionCodErr.get(0);
			codErrorProceso = listaEvaluacionCodErr.get(1).toString();
		}
		
		//Proceso 2.  Actualizar los campos del template solo si no tiene operaciones
		if( utileriasDatos.validarCamposSeleccionados(beanReq, nuevoEstadoOperaciones) ){
			beanResDAO= daoMantenimientoExt.actualizarCampos(beanReq, session);

			//se llama funcion para evaluar codigoErr obtenido por el DAO
			listaEvaluacionCodErr = utileriasDatos.validaCodigoErrorActualizaTmpl(beanViewRes, beanResDAO, false);
			erroresProceso = (Boolean) listaEvaluacionCodErr.get(0);
			codErrorProceso = listaEvaluacionCodErr.get(1).toString();
		}
		
		//Actualizar el estado y la fecha del template
		beanResDAO = daoMantenimiento.actualizarFechaTemplate(beanReq, session);

		//se llama funcion para evaluar codigoErr obtenido por el DAO
		listaEvaluacionCodErr = utileriasDatos.validaCodigoErrorActualizaTmpl(beanViewRes, beanResDAO, true);
		erroresProceso = (Boolean) listaEvaluacionCodErr.get(0);
		codErrorProceso = listaEvaluacionCodErr.get(1).toString();
		
		//Si se envio una peticion de consulta antes de invocar el alta
		if(beanReq.getForward()){			
			beanViewRes= consultarTemplates(beanReq.getFiltroEstado(), session, beanReq.getPaginador());
			utileriasDatos.setExitoAlert(beanViewRes);
		}
		
		//Verificar si se generaron errores
		utileriasDatos.existenErroresProceso(beanViewRes, codErrorProceso, erroresProceso);
		
		crearPistaAuditoriaActualizar(beanReq, session, usuariosAnexar,
				usuariosEliminar, erroresProceso, originalTemplate);
		
		return beanViewRes;
	}

	/**
	 * Metodo refactorizado que nos permite crear la pista de auditoria de la accion de actualizar template.
	 *
	 * @param beanReq Objeto con los valores de la peticion
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @param usuariosAnexar Lista de usuarios que se anexaron
	 * @param usuariosEliminar Lista de usuarios eliminados
	 * @param erroresProceso Estado en el que termino el proceso
	 * @param originalTemplate Objeto original antes de actualizar
	 */
	private void crearPistaAuditoriaActualizar(BeanReqAltaEditTemplate beanReq,
			ArchitechSessionBean session,
			List<BeanUsuarioAutorizaTemplate> usuariosAnexar,
			List<BeanUsuarioAutorizaTemplate> usuariosEliminar,
			Boolean erroresProceso,
			BeanResPrincipalTemplate originalTemplate) {
		
		//Se crean objetos de soporte
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		UtileriasPistasMtoTemplate utileriaTemplate = new UtileriasPistasMtoTemplate();
		
		Map<String, Object> cambiosCampos;
		String pista="", pista0 ="";
		String statusProceso = OK;
		
		cambiosCampos = utileriaTemplate.compararCamposObtenerDiferentes(originalTemplate.getTemplate().getCampos(), beanReq.getTemplate().getCampos());
		
		//Verificar si se generaron cambios en los campos o en los usuarios
		if((Boolean) cambiosCampos.get("cambios") || 
				!usuariosAnexar.isEmpty() || !usuariosEliminar.isEmpty()){
			
			pista0= "{" + cambiosCampos.get("inicial").toString() +"}";
			
			//Pista: Auditoria Template. unica para templates
			pista="Temp=>{" + utileriaTemplate.obtenerDatosTemplate(beanReq.getTemplate()) + "} UsrNew=>{"+
					utileriaTemplate.obtenerDatosUsuariosTemplate(usuariosAnexar)+"} UsrEli=>{"+
					utileriaTemplate.obtenerDatosUsuariosTemplate(usuariosEliminar)+"} Cam=>{" +
					cambiosCampos.get("final").toString()+ "}";
			
			if(erroresProceso){
				statusProceso=NOK;
			}
			
			//Pista de Template
			BeanReqInsertBitAdmin beanAudTemplate = 
	    			utileriasBitAdmin.createBitacoraBitAdmin(statusProceso, TABLE_TEMPLATE, "UPDATE", 
	    					"actualizarTemplate.do", pista0, pista ,
	    					" "," ","ACTUALIZAR TEMPLATE", session);
			
			//Se guarda la pista en bitacora
			daoBitAdministrativa.guardaBitacora(beanAudTemplate, session);
		}
	}

	/**
	 * Metodo que permite verificar si de los usuarios eliminar alguno tiene operaciones asociadas.
	 *
	 * @param usuariosEliminar lista de usuarios a verificar
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return Tru|False dependiendo de la situacion
	 * @throws BusinessException the business exception
	 */
	//Metodo que permite verificar si de los usuarios eliminar alguno tiene operaciones asociadas
	private Boolean verificarUsuariosOperaciones(List<BeanUsuarioAutorizaTemplate> usuariosEliminar, ArchitechSessionBean session) throws BusinessException {
		BeanResBase beanResDAO=null;
		
		for(BeanUsuarioAutorizaTemplate temp: usuariosEliminar){			
			beanResDAO = daoMantenimientoExt.verificarExistenciaOperacionesUsuarioTemplate(temp, session);
			
			//El usuario esta asignado a aprobar el template. error, recuperar los datos
			if(Errores.CMMT006V.equals(beanResDAO.getCodError())){				
				return true;
			} else if(Errores.EC00011B.equals(beanResDAO.getCodError())){
				throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
			}
		}
		
		return false;
	}
		
	/**
	 * Metodo a traves del cual se obtiene la lista de usuarios que deben eliminarse en el proceso de edicion.
	 *
	 * @param usuarios lista de usuarios candidatos a eliminar
	 * @param idTemplate del template asociado
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return lista de usuarios reales a eliminar
	 */
	//Metodo a traves del cual se obtiene la lista de usuarios que deben eliminarse en el proceso de edicion
	public List<BeanUsuarioAutorizaTemplate> obtenerUsuariosEliminar(List<String> usuarios, String idTemplate, ArchitechSessionBean session) throws BusinessException {
		List<BeanUsuarioAutorizaTemplate> usuariosEliminar = new ArrayList<BeanUsuarioAutorizaTemplate>();
		BeanResBase beanResDAO = null;
		
		for(String usuario: usuarios){
			BeanUsuarioAutorizaTemplate temp = new BeanUsuarioAutorizaTemplate();			
			temp.setIdTemplate(idTemplate);
			temp.setUsrAutoriza(usuario);
			
			beanResDAO = daoMantenimiento.verificarExistenciaUsuario(temp, session);			
			//El usuario existe en la BB y debe eliminarse
			if(Errores.CMMT005V.equals(beanResDAO.getCodError())){
				usuariosEliminar.add(temp);
			} else if(Errores.EC00011B.equals(beanResDAO.getCodError())){
				throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
			}
		}
		
		return usuariosEliminar;
	}

	/**
	 * Metodo a traves del cual se obtiene la lista de usuarios nuevos del template.
	 *
	 * @param usuarios lista de usuarios nuevos sin verificar que existan en la BD
	 * @param idTemplate Identificador del template
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Lista de usuarios reales a insertar
	 */
	//Metodo a traves del cual se obtiene la lista de usuarios nuevos del template
	public List<BeanUsuarioAutorizaTemplate> obtenerUsuariosNuevos(List<BeanUsuarioAutorizaTemplate> usuarios, String idTemplate,  ArchitechSessionBean session) throws BusinessException {
		List<BeanUsuarioAutorizaTemplate> usuariosNuevos = new ArrayList<BeanUsuarioAutorizaTemplate>();
		BeanResBase beanResDAO = null;
		
		for(BeanUsuarioAutorizaTemplate usuario: usuarios){
			usuario.setIdTemplate(idTemplate);
			beanResDAO = daoMantenimiento.verificarExistenciaUsuario(usuario, session);			
			//El usuario no existe en la BD y debe crearse
			if(!Errores.CMMT005V.equals(beanResDAO.getCodError())){
				usuariosNuevos.add(usuario);
			} else if(Errores.EC00011B.equals(beanResDAO.getCodError())){
				throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
			}
		}
		return usuariosNuevos;
	}
	
}

