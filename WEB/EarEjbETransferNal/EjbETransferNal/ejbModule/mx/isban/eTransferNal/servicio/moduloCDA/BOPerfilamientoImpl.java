package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfilDAO;
import mx.isban.eTransferNal.dao.moduloCDA.DAOPerfilamiento;

@Remote(BOPerfilamiento.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOPerfilamientoImpl  extends Architech implements BOPerfilamiento {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = 6370215697925651237L;
	
	/**Referencia al dao de perfilamiento*/
	@EJB
	private DAOPerfilamiento daoPerfilamiento;
	
	/**
	 * Metodo que permite consultar los servicios y tareas que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfil Objeto del tipo BeanResConsPerfil
	 * @exception BusinessException exception de negocio
	 */
	public BeanResConsPerfil consultaServicio(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean) throws BusinessException{
		debug("++++++++++++++++++++++++++++++consultaServicio++++++++++++++++++++++++++++++");
		BeanResConsPerfilDAO beanResConsPerfilDAO = null;
		final BeanResConsPerfil beanResConsPerfil = new BeanResConsPerfil();
		beanResConsPerfilDAO = daoPerfilamiento.consultaServicio(beanReqConsPerfil, architechSessionBean);
		beanResConsPerfil.setListBeanServiciosTareas(beanResConsPerfilDAO.getListBeanServiciosTareas());
		return beanResConsPerfil;
	}
	
	/**
	 * Metodo que permite consultar las tareas que contiene un perfil en un servicio
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfil Objeto del tipo BeanResConsPerfil
	 * @exception BusinessException exception de negocio
	 */
	public BeanResConsPerfil consultaServicioTarea(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean) throws BusinessException{
		BeanResConsPerfilDAO beanResConsPerfilDAO = null;
		final BeanResConsPerfil beanResConsPerfil = new BeanResConsPerfil();
		beanResConsPerfilDAO = daoPerfilamiento.consultaServicioTareas(beanReqConsPerfil, architechSessionBean);
		beanResConsPerfil.setListBeanServiciosTareas(beanResConsPerfilDAO.getListBeanServiciosTareas());
		return beanResConsPerfil;
	}

	/**
	 * Metodo get del dao de perfilamiento
	 * @return daoPerfilamiento Objeto del tipo DAOPerfilamiento
	 */
	public DAOPerfilamiento getDaoPerfilamiento() {
		return daoPerfilamiento;
	}
	/**
	 * Metodo set del dao de perfilamiento
	 * @param daoPerfilamiento Objeto del tipo DAOPerfilamiento
	 */
	public void setDaoPerfilamiento(DAOPerfilamiento daoPerfilamiento) {
		this.daoPerfilamiento = daoPerfilamiento;
	}

	
}
