/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperActualizar;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpFunciones;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpHTO;

/**
 * Clase del tipo BO que se encarga recuperar las recepciones de operaciones.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperaRecuperaMotivoImpl extends Architech implements BORecepOperaRecuperaMotivo{
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1377532272492477541L;
	
	/** Propiedad del tipo DAORecepOperacion que almacena el valor de daoRecepOperacion. */
	@EJB
	private transient DAORecepOperActualizar daoRecepOperacion2;
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	/** La constante util. */
	private UtileriasRecepcionOpConsultas utilApartadas = UtileriasRecepcionOpConsultas.getInstance();
		
		
	/** variable de utilerias. */
	public static final UtileriasRecepcionOpFunciones UTIL = UtileriasRecepcionOpFunciones.getInstance();
	
	/** variable de utilerias. */
	public static final UtileriasRecepcionOpHTO UTIL_HTO = UtileriasRecepcionOpHTO.getInstance();
			
	    
	/**
	 * Recuperar transf spei rec.
	 *
	 * @param beanReqTranSpeiRec the bean req tran spei rec
	 * @param sessionBean the session bean
	 * @param historico the historico
	 * @return the bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec recuperarTransfSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean, boolean historico) throws BusinessException{
		
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		//se inicializ bean de error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		/**Se incializan las varaibles**/
		StringBuilder strBuilder = null;
		String estatus = "OK";
		String codError = Errores.OK00000V;
		String tipoError =Errores.TIPO_MSJ_INFO;
		boolean ok = true;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		BeanTranSpeiRecOper beanReqActTranSpeiRec = null;	
	    
		BeanResBase beanResBase = null;
    	List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
    	/**Se filtranlos datos que se seleccionador**/
    	listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec, HelperRecepcionOperacion.RECUPERA);
    	
    	/**Condicion para ver si la lista trae datos**/
		if(listBeanTranSpeiRecTemp.isEmpty()){		
			/**REGRESA EL MENSAJE PARA SELECCIONAR UN REGISTRO EN CASO DE QUE NO EXISTAN SELECCIONADOS**/
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
      		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
      		return beanResConsTranSpeiRec;
		}
		
		
		/**Recorre la lista**/
        for(BeanTranSpeiRecOper beanTranSpeiRec:listBeanTranSpeiRecTemp){
        	/**dato anterior**/
        	BeanTranSpeiRecOper beanAnt = new BeanTranSpeiRecOper();
        	
        	beanAnt=beanTranSpeiRec;
        	/**Asigna los datos**/
        	strBuilder = new StringBuilder();
        	/**asigna los valores al objeto**/
      		beanReqActTranSpeiRec = beanTranSpeiRec;
      		beanReqActTranSpeiRec.setEstatus("RE");
      		beanReqActTranSpeiRec.getBeanDetalle().setCodigoError("RECUPERA");       
      		beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusTransfer(beanReqActTranSpeiRec.getEstatus());
      		
      		/**Asigna datos al strBuilder **/
      		strBuilder.append("CODIGO_ERROR=RECUPERA|ESTATUS=RE");
      		/**Setea los datos al objeto**/
      		strBuilder.append(UTIL_HTO.seteaCampos(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd(),
      				beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec(),beanTranSpeiRec.getBeanDetalle().getOrdenante().getMonto().toString()));
      		strBuilder.append(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd());
      		/**condicion para ver si se cumple el tipo de pago**/
      		if(Integer.parseInt(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())>0){
      			beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().setMotivoDevol("18");
      			strBuilder.append("|MOTIVO_DEVOL=18");      			
      		}
      		
      		/**Actualiza la transferencia **/
      		beanResBase  = daoRecepOperacion2.actualizaTransSpeiRec(beanReqActTranSpeiRec,  sessionBean, historico);
      		
      		if(!Errores.OK00000V.equals(beanResBase.getCodError())){
      			ok= false;
      			codError = beanResBase.getCodError();
      			tipoError = Errores.TIPO_MSJ_ERROR;
      		}else if (beanResBase.getCodError().equals(Errores.EC00011B)){			
      			throw new BusinessException(beanResBase.getCodError(), beanResBase.getMsgError());      			
      		}
      		
      		/**Se asigna valores para la pistas de auditoria**/
      		BeanTranSpeiRecOper beanTranSpeiRecTemp = new BeanTranSpeiRecOper();
      		beanTranSpeiRecTemp.setBeanDetalle(new BeanDetRecepOperacion());
      		beanTranSpeiRecTemp.getBeanDetalle().setError(new BeanResBase());
      		beanTranSpeiRecTemp.getBeanDetalle().setDetEstatus(new  BeanDetRecepOp());
      		beanTranSpeiRecTemp.getBeanDetalle().getDetEstatus().setEstatusTransfer(estatus);
      		beanTranSpeiRecTemp.getBeanDetalle().getError().setCodError(codError);
      		String datosInsert = beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getError().getCodError()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()+", "+
			beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago();
      		String datosNuevo = "ESTATUS_TRANSFER= "+beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer()+", "+
      				"CODIGO_ERROR= "+beanReqActTranSpeiRec.getBeanDetalle().getError().getCodError()+", "+
      				"MOTIVO_DEVOL= "+beanReqActTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol()+", "+
      				"REFE_TRANSFER= "+beanReqActTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer()+", "+
      				"FCH_OPERACION= "+beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion()+", "+
      				"CVE_MI_INSTITUC= "+beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+", "+
      				"CVE_INST_ORD= "+beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd()+", "+
      				"FOLIO_PAQUETE= "+beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()+", "+
      				"FOLIO_PAGO= "+beanReqActTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago();
      		String datosAnte = beanAnt.getBeanDetalle().getDetEstatus().getEstatusTransfer()+", "+
      				beanAnt.getBeanDetalle().getError().getCodError()+", "+
      				beanAnt.getBeanDetalle().getBeanCambios().getMotivoDevol()+", "+
      				beanAnt.getBeanDetalle().getDetEstatus().getRefeTransfer()+", "+
      				beanAnt.getBeanDetalle().getDetalleGral().getFchOperacion()+", "+
      				beanAnt.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+", "+
      				beanAnt.getBeanDetalle().getDetalleGral().getCveInstOrd()+", "+
      				beanAnt.getBeanDetalle().getDetalleGral().getFolioPaquete()+", "+
      				beanAnt.getBeanDetalle().getDetalleGral().getFolioPago();
      		String dato = ConstantesRecepOperacionApartada.TYPE_UPDATE+"/"+ConstantesRecepOperacion.RECTRANSFERSPEIREC+"/"+datosInsert+"/"+codError+"/"+ datosNuevo+"/"+datosAnte;
      		/**genera la pista de auditoria**/
			BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
       		/**inserta en bitacora**/
      		boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
       		
        }


  		beanResConsTranSpeiRec.getBeanError().setCodError(codError);
  		beanResConsTranSpeiRec.getBeanError().setTipoError(tipoError); 		
		
  		/**Regresa el resultado de las operaciones**/		
		return beanResConsTranSpeiRec;
	}
	
	
	

	/**
	 * Act mot rechazo.
	 *
	 * @param beanReqTranSpeiRec the bean req tran spei rec
	 * @param sessionBean the session bean
	 * @param historico the historico
	 * @return the bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec actMotRechazo(BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean, boolean historico)  throws BusinessException {
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		//se inicializ bean de error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		/**Inicializa las variables**/
		StringBuilder strBuilder = new StringBuilder();
		String estatus = "OK";
		String codError = Errores.OK00000V;
		String tipoError = Errores.TIPO_MSJ_INFO;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		BeanTranSpeiRecOper beanTranSpeiRecTemp = null;
		BeanTranSpeiRecOper beanReqActTranSpeiRec = null;
		BeanResBase beanResBase = null;			
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
		/**consulta los datos seleccionados**/
		listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,HelperRecepcionOperacion.SELECCIONADO);
		/**Condicion para ver si la lista de datos seleccionados contiene informacion**/
		if(!listBeanTranSpeiRecTemp.isEmpty()){
			/**Recorre los datos**/
			for(BeanTranSpeiRecOper beanTranSpeiRec:listBeanTranSpeiRecTemp){
				if(beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol()!=null && 
						(!"".equals(beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol())
								&& !beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol().equals(beanTranSpeiRec.getMotivoDevolAnt()))){
					/**Inserta los registros a la lista**/	
					listBeanTranSpeiRec.add(beanTranSpeiRec);
		      		/**asigna la lista **/
		      		beanReqTranSpeiRec.setListBeanTranSpeiRec(listBeanTranSpeiRec);
				}else{
					beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00067V);
					beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);	
		      		return beanResConsTranSpeiRec;
	      	  	}
			}
		}else{		
			/**En caso de no tener un registro selecciona manda el mensaje  **/
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);		
       		return  beanResConsTranSpeiRec;
		}
		
		
		/**Recorre la lista**/
		for(int i=0;i<listBeanTranSpeiRec.size();i++){
			/**inicializa la variable strbuilder**/
			strBuilder = new StringBuilder();			
			/**dato anterior**/
        	BeanTranSpeiRecOper beanAnt = new BeanTranSpeiRecOper();
        	beanAnt=listBeanTranSpeiRec.get(i);
        	beanTranSpeiRecTemp = listBeanTranSpeiRec.get(i);
			beanReqActTranSpeiRec = beanTranSpeiRecTemp;
			beanResBase = daoRecepOperacion2.actualizaMotivoRec(beanReqActTranSpeiRec, sessionBean, beanReqTranSpeiRec.getBeanComun().getNombrePantalla());
			/**pregunta si el proceso tiene algun error**/
			beanResConsTranSpeiRec = UTIL.actMotRechazoCont(beanResBase,beanResConsTranSpeiRec);	
			
			strBuilder.append("MOTIVO_DEV=");
			strBuilder.append(beanTranSpeiRecTemp.getBeanDetalle().getBeanCambios().getMotivoDevol());
			/**llama la funcion para setear los datos obtenidos**/
			strBuilder.append(UTIL_HTO.seteaCampos(beanTranSpeiRecTemp.getBeanDetalle().getOrdenante().getNumCuentaOrd(),
					beanTranSpeiRecTemp.getBeanDetalle().getReceptor().getNumCuentaRec(),beanTranSpeiRecTemp.getBeanDetalle().getOrdenante().getMonto().toString()));
			
			/**Se asigna valores para la pistas de auditoria**/
      		BeanTranSpeiRecOper beanTranSpeiRecTempA = new BeanTranSpeiRecOper();
      		beanTranSpeiRecTempA = beanTranSpeiRecTemp;
      		beanTranSpeiRecTempA.getBeanDetalle().setError(new BeanResBase());
      		// se inicializan la recepcion de detalle
      		BeanTranSpeiRecOper beanTranSpeiRecA = new BeanTranSpeiRecOper();
      		beanTranSpeiRecA.setBeanDetalle(new BeanDetRecepOperacion());
      		beanTranSpeiRecA.getBeanDetalle().setError(new BeanResBase());
      		beanTranSpeiRecA.getBeanDetalle().setDetEstatus(new BeanDetRecepOp());
      		beanTranSpeiRecA.getBeanDetalle().getDetEstatus().setEstatusTransfer(estatus);
      		beanTranSpeiRecA.getBeanDetalle().getError().setCodError(codError);
      			
      		beanTranSpeiRecTempA.getBeanDetalle().getError().setMsgError(strBuilder.toString());
      		/**inserta en bitacora**/
      		insertBitacora(beanTranSpeiRecA,beanTranSpeiRecTempA,sessionBean, ConstantesRecepOperacion.ACTMOTREZCHAZ,beanAnt);
		}
		/**vuelve a consultar los datos**/
		beanResConsTranSpeiRec.getBeanError().setCodError(codError);
		beanResConsTranSpeiRec.getBeanError().setTipoError(tipoError);
		
		
		/**Regresa el resultado de las operaciones**/	
		return beanResConsTranSpeiRec;
	}
	
		
   
    /**
     * Insert bitacora.
     *
     * @param beanTranSpeiRecTemp El objeto: bean tran spei rec temp
     * @param beanTranSpeiRec El objeto: bean tran spei rec
     * @param sessionBean El objeto: session bean
     * @param funcion El objeto: funcion
     * @param beanAnt El objeto: bean ant
     * @throws BusinessException La business exception
     */
	public void insertBitacora(BeanTranSpeiRecOper beanTranSpeiRecTemp, BeanTranSpeiRecOper beanTranSpeiRec,ArchitechSessionBean sessionBean,
    		String funcion,BeanTranSpeiRecOper beanAnt)throws BusinessException {
  		/**inicializa variables**/
		String puntoDo=" ";
		String estat =" ";
		String msj =" ";
		String envioOpe =" ";
		String operacion=" ";
		String opcion="";
		/**condiciones para asignar valores para insertar las pistas de auditoria **/
		if(funcion.equals(ConstantesRecepOperacion.RECTRANSFERSPEIREC)) {		
			/**entra si se cumple con la transferencia SPEI **/
			envioOpe = "RECTRAN";
			operacion = ConstantesRecepOperacion.UPDATE;
			opcion=ConstantesRecepOperacion.TRAN_SPEI_REC;
			/**asigan el punto do**/
			puntoDo = "recuperarTransfSpeiRec.do";
			estat = beanTranSpeiRecTemp.getBeanDetalle().getDetEstatus().getEstatusTransfer();
			msj = ConstantesRecepOperacion.EXITO;
		}else if(funcion.equals(ConstantesRecepOperacion.ACTMOTREZCHAZ)) {	
			/** si hace una actualizancion **/
			envioOpe = "ACTMOTRE";
			operacion = ConstantesRecepOperacion.UPDATE;
			opcion=ConstantesRecepOperacion.TRAN_SPEI_REC;
			/**asigan el punto do**/
			puntoDo = "actMotRechazo.do";
			estat = beanTranSpeiRecTemp.getBeanDetalle().getDetEstatus().getEstatusTransfer();
			msj = ConstantesRecepOperacion.EXITO;
		}
		// se asigna los datos anteriores a una variables
		String datoAnterio = "ESTATUS_TRANSFER="+beanAnt.getBeanDetalle().getDetEstatus().getEstatusTransfer()+",CODIGO_ERROR="+ beanAnt.getBeanDetalle().getCodigoError()
			+",FCH_OPERACION="+ beanAnt.getBeanDetalle().getDetalleGral().getFchOperacion()+",CVE_MI_INSTITUC="+ beanAnt.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
			+",CVE_INST_ORD="+ beanAnt.getBeanDetalle().getDetalleGral().getCveInstOrd()+",FOLIO_PAGO="+ beanAnt.getBeanDetalle().getDetalleGral().getFolioPago()
			+",FOLIO_PAQUETE="+ beanAnt.getBeanDetalle().getDetalleGral().getFolioPaquete()+",MOTIVO_DEVOL="+ beanAnt.getBeanDetalle().getBeanCambios().getMotivoDevol();
		//se asigna los datos nuevos a una variables		
		String datoNuevo= "ESTATUS_TRANSFER="+beanTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer()+",CODIGO_ERROR="+ beanTranSpeiRec.getBeanDetalle().getCodigoError()
				+",FCH_OPERACION="+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion()+",CVE_MI_INSTITUC="+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
				+",CVE_INST_ORD="+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd()+",FOLIO_PAGO="+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago()
				+",FOLIO_PAQUETE="+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()+",MOTIVO_DEVOL="+ beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol();		
		
		/**Se genera el bean de bitacora **/
		BeanReqInsertBitacora beanReqInsertBitacora = new BeanReqInsertBitacora();
		beanReqInsertBitacora.setServicio(puntoDo);
		beanReqInsertBitacora.setFechaHora(new Date());
		//estatus de la operacion si fue exitosa
		beanReqInsertBitacora.setEstatusOperacion(estat);
		beanReqInsertBitacora.setCodOperacion(envioOpe);
		beanReqInsertBitacora.setTablaAfectada(opcion);
		//codigo de error del registro
		beanReqInsertBitacora.setCodError(beanTranSpeiRecTemp.getBeanDetalle().getError().getCodError());
		beanReqInsertBitacora.setDescCodError(msj);
		// tipo de operacion update,insert,delete
		beanReqInsertBitacora.setDescOperacion(operacion);
		beanReqInsertBitacora.setDatoFijo(UTIL_HTO.seteaKey(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion(), 
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion(), 
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd(), 
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete(),
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago()));
		// sea signa el dato antarior
		beanReqInsertBitacora.setValNuevo(datoNuevo);
		//se asigna el dato nuevo
		beanReqInsertBitacora.setValAnt(datoAnterio);
		
		/**Se realiza el insert **/
		BeanResGuardaBitacoraDAO bean = daoBitacora.guardaBitacora(beanReqInsertBitacora, sessionBean);
		/**Si esixte un error regresa el BusinessException**/
		if (bean.getCodError().equals(Errores.EC00011B)){			
  			throw new BusinessException(bean.getCodError(), bean.getMsgError());      			
  		}
		
    }
    

	
}