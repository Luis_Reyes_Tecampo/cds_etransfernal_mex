/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaReceptorHistImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsReceptHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOConsultaReceptorHist;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaReceptorHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo BO que se encarga  del negocio del catalogo 
 *de consulta receptor historico
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaReceptorHistImpl implements BOConsultaReceptorHist{

	/**
	 * Referencia privada al dao de consulta
	 */
	@EJB
	private DAOConsultaReceptorHist daoConsultaReceptorHist;

	/**Metodo que sirve para consultar los receptores historicos
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsReceptHist Objeto del tipo BeanResConsReceptHist
	   */
	@Override
	public BeanResConsReceptHist obtenerDatosReceptHist(BeanPaginador beanPaginador,BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBeans) {
		final BeanResConsReceptHist beanResConReceptHist = new BeanResConsReceptHist();
		BeanConsultaReceptorHistDAO beanConsultaReceptorHistDAO = null;		

		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		if (!validarFechaOperacion(beanReqConsReceptHist, beanResConReceptHist))
			return beanResConReceptHist;
		
		beanConsultaReceptorHistDAO = daoConsultaReceptorHist.obtenerConsultaReceptorHist(beanPaginador, beanReqConsReceptHist, architechSessionBeans);
		
		if (beanConsultaReceptorHistDAO.getListaDatos()==null) {
			beanConsultaReceptorHistDAO.setListaDatos(new ArrayList<BeanConsRecepHist>());
		}
		
		beanResConReceptHist.setOpcion(beanReqConsReceptHist.getOpcion());
		beanResConReceptHist.setFecha(beanReqConsReceptHist.getFecha());
		if (Errores.CODE_SUCCESFULLY.equals(beanConsultaReceptorHistDAO.getCodError()) && beanConsultaReceptorHistDAO.getListaDatos().isEmpty()){
			beanResConReceptHist.setCodError(Errores.ED00011V);
			beanResConReceptHist.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConReceptHist.setMsgError(Errores.DESC_ED00011V);
			beanResConReceptHist.setListaDatos(beanConsultaReceptorHistDAO.getListaDatos());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanConsultaReceptorHistDAO.getCodError())){
			beanResConReceptHist.setListaDatos(beanConsultaReceptorHistDAO.getListaDatos());
			beanResConReceptHist.setTotalReg(beanConsultaReceptorHistDAO.getTotalReg());
			beanPaginador.calculaPaginas(beanConsultaReceptorHistDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConReceptHist.setBeanConsReceptHistSum(daoConsultaReceptorHist.obtenerSumatoriaHist(beanReqConsReceptHist,architechSessionBeans));
			beanResConReceptHist.setImporteTotal(daoConsultaReceptorHist.obtenerImporteTotalHist(beanReqConsReceptHist,architechSessionBeans).toString());
			beanResConReceptHist.setBeanPaginador(beanPaginador);
			beanResConReceptHist.setCodError(Errores.OK00000V);
		} else {
			beanResConReceptHist.setCodError(Errores.EC00011B);
			beanResConReceptHist.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResConReceptHist.setMsgError(Errores.DESC_EC00011B);
		}
		
		return beanResConReceptHist;
	}
	
	/**
	 * @param beanReqConsReceptHist Objeto del tipo BeanReqConsReceptHist
	 * @param beanResConReceptHist Objeto del tipo BeanResConReceptHist
	 * @return Boolean
	 */
	private Boolean validarFechaOperacion(BeanReqConsReceptHist beanReqConsReceptHist, BeanResConsReceptHist beanResConReceptHist){
		if (beanReqConsReceptHist.getFecha() == null || "".equals(beanReqConsReceptHist.getFecha())){
			beanResConReceptHist.setCodError(Errores.ED00025V);
			beanResConReceptHist.setTipoError(Errores.TIPO_MSJ_ALERT);
			beanResConReceptHist.setMsgError(Errores.DESC_ED00025V);
			return false;
		} else if(beanReqConsReceptHist.getFecha().matches("\\d{2}/\\d{2}/\\d{4}")){
			
			SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
			
			try {
				formatDate.parse(beanReqConsReceptHist.getFecha());
			} catch (ParseException e) {
				beanResConReceptHist.setCodError(Errores.ED00020V);
				beanResConReceptHist.setTipoError(Errores.TIPO_MSJ_ALERT);
				beanResConReceptHist.setMsgError(Errores.DESC_ED00020V);
				return false;
			}
		}
		else{
			beanResConReceptHist.setCodError(Errores.ED00020V);
			beanResConReceptHist.setTipoError(Errores.TIPO_MSJ_ALERT);
			beanResConReceptHist.setMsgError(Errores.DESC_ED00020V);
			return false;
		}
		return true;
	}
	
	/**
	 * Metodo get para obtener el valor de DAOConsultaReceptorHist
	 * @return el daoConsultaReceptorHist objeto de tipo @see DAOConsultaReceptorHist
	 */
	public DAOConsultaReceptorHist getDaoConsultaReceptorHist() {
		return daoConsultaReceptorHist;
	}

	/**
	 * Metodo para establecer el valor del daoConsultaReceptorHist
	 * @param daoConsultaReceptorHist objeto de tipo @see DAOConsultaReceptorHist
	 */
	public void setDaoConsultaReceptorHist(DAOConsultaReceptorHist daoConsultaReceptorHist) {
		this.daoConsultaReceptorHist = daoConsultaReceptorHist;
	}

	/**Metodo que sirve para consultar los receptores Ini
	 * * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsReceptHist Objeto del tipo BeanResConsReceptHist
	   * @exception BusinessExceptionException de negocio
	   */
	@Override
	public BeanResConsReceptHist obtenerDatosReceptIni(BeanPaginador paginador, ArchitechSessionBean architechSessionBean) throws BusinessException{
		BeanConsultaReceptorHistDAO beanConsultaReceptorHistDAO = null;
		final BeanResConsReceptHist beanResConsReceptHist = new BeanResConsReceptHist();
		
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());		
		beanConsultaReceptorHistDAO = daoConsultaReceptorHist.obtenerConsultaReceptorHistIni(paginador, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanConsultaReceptorHistDAO.getCodError()) 
				&& beanConsultaReceptorHistDAO.getListaDatos().isEmpty()) {
			beanResConsReceptHist.setCodError(Errores.ED00011V);
			beanResConsReceptHist.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsReceptHist.setListaDatos(beanConsultaReceptorHistDAO.getListaDatos());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanConsultaReceptorHistDAO.getCodError())){
			beanResConsReceptHist.setListaDatos(beanConsultaReceptorHistDAO.getListaDatos());          
			beanResConsReceptHist.setTotalReg(beanConsultaReceptorHistDAO.getTotalReg());
			paginador.calculaPaginas(beanConsultaReceptorHistDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsReceptHist.setBeanPaginador(paginador);
			beanResConsReceptHist.setCodError(Errores.OK00000V);
		}else{
			beanResConsReceptHist.setCodError(Errores.EC00011B);
			beanResConsReceptHist.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResConsReceptHist;
	}

}
