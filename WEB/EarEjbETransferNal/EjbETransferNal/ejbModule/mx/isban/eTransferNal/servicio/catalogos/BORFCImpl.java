/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORFCImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 12:05:03 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;
import mx.isban.eTransferNal.beans.catalogos.BeanResRFC;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAORFC;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasRFC;


/**
 * Class BORFCImpl.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de excepcion de RFC.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORFCImpl extends Architech implements BORFC {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8523550939269863004L;
	
	/** La variable que contiene informacion con respecto a: dao RFC. */
	@EJB
	private DAORFC daoRFC;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;

	/** La constante UPDATE. */
	private static final String UPDATE = "RFC";
	
	/** La constante INSERT. */
	private static final String INSERT = "RFC";
	
	/** La constante SELECT. */
	private static final String SELECT = "RFC";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "NA";
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_RFC_SPID_EXENTOS";
	
	/** La constante utilerias. */
	private static final UtileriasRFC utilerias = UtileriasRFC.getInstancia();
	
	
	
	/**
	 * Consultar.
	 *
	 * @param beanRFC the beanRFC --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador the beanPaginador  --> Objeto para realizar el paginado
	 * @param session El objeto: session
	 * @return the beanResRFC --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResRFC consultar(BeanRFC beanRFC, BeanPaginador beanPaginador, ArchitechSessionBean session)
			throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanResRFC response = daoRFC.consultar(beanRFC, beanPaginador, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getRfcs().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Agregar.
	 *
	 * @param beanRFC the beanRFC --> Objeto con los datos del nuevo registro
	 * @param session El objeto: session
	 * @return the beanResBase --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase agregar(BeanRFC beanRFC, ArchitechSessionBean session) throws BusinessException {
		boolean operacion = false;
		/** Ejecucion de la peticion al DAO **/
		BeanResBase response = daoRFC.agregar(beanRFC, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanRFC);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_INSERT, "agregarNombreFideicomiso.do", operacion, session,dtoNuevo,"",INSERT);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Editar.
	 *
	 * @param beanRFC the bean RFC --> Objeto con los datos del nuevo registro
	 * @param datoAnterior the datoAnterior --> Objeto con los datos del registro a editar
	 * @param session El objeto: session
	 * @return the beanResBase --> Resultado de la operacion
	 * @throws BusinessException the business exception--> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase editar(BeanRFC beanRFC, BeanRFC datoAnterior, ArchitechSessionBean session) throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		String dtoAnt = obtenerDatos(datoAnterior);
		/** Ejecucion de la peticion al DAO **/
		response = daoRFC.editar(beanRFC, datoAnterior, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanRFC);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_UPDATE, "editarNombreFideicomiso.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Eliminar.
	 *
	 * @param beanResRFC the bean res RFC --> Registros a eliminar
	 * @param session El objeto: session
	 * @return the beanEliminarResponse --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanResRFC beanResRFC, ArchitechSessionBean session) throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanRFC rfc : beanResRFC.getRfcs()){
			if (rfc.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoRFC.eliminar(rfc, session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(rfc.getRfc());
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(rfc.getRfc());
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDatos(rfc);
	        	generaPistaAuditoras(ConstantesCatalogos.TYPE_DELETE, "eliminarNombreFideicomiso.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Exportar todos.
	 *
	 * @param beanRFC the beanRFCa --> Objeto de filtrado de registros
	 * @param totalRegistros the totalRegistros --> Parametro con el total de registros actual
	 * @param session El objeto: session
	 * @return the beanResBase --> Objeto de respuesta de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase exportarTodo(BeanRFC beanRFC, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		/** Declaracion del objeto de salida **/
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String filtroWhere = utilerias.getFiltro(beanRFC, "WHERE");
		String query = UtileriasRFC.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasRFC.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasRFC.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("EXCEPCION_RFC");
		exportarRequest.setNombreRpt("CATALOGOS_EXCEPCION_RFC_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_EXPORT, "exportarTodosNombreFideicomiso.do", operacion, session,"","", DATO_FIJO);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * generaPistaAuditoras
	 * 
	 * 
	 * Procedimiento para el almacenamiento de las Pistas Auditoras
	 * en la BD.
	 *
	 *
	 * @param operacion            El objeto: operacion --> Parametro del tipo de operacion que se realiza
	 * @param urlController            El objeto: url controller --> Parametro con el .do de la accion
	 * @param resOp            El objeto: res op --> Parametro de respuesta de la operacion
	 * @param sesion            El objeto: sesion--> Parametro de session
	 * @param dtoNuevo El objeto: dto nuevo --> Parametro de valor registro nuevo
	 * @param dtoAnterior El objeto: dto anterior --> Parametro del valor registro anterior
	 * @param dtoModificado El objeto: dto modificado --> Parametro del dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		/** Valida el tipo de operacion **/
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus(ConstantesCatalogos.OK);
		} else {
			beanPista.setEstatus(ConstantesCatalogos.NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

	/**
	 * Obtener datos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 *
	 * @param ant El objeto: ant --> Objeto anterior
	 * @return Objeto string --> Respuesta devuelta
	 */
	private String obtenerDatos(BeanRFC ant) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es UDPDATE **/
			dtoAnt ="RFC=".concat(ant.getRfc());
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
