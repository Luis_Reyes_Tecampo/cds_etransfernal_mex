/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

import java.util.ArrayList;
/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAODetRecepOperacion;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperacionApartadas;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;

/**
 * Clase del tipo BO que se encarga de de realiza la devolucion extemporanea
 * de las cuentas alternar y principales.
 *
 * @author FSW-Vector
 * @since 5/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BODevExtemporaneaImpl extends Architech implements BODevExtemporanea{
	
	/** variable para serial. */
	private static final long serialVersionUID = -6638506038114133627L;
	
	/**  Referencia privada al dao. */
	@EJB
	private DAODetRecepOperacion dAODetRecepOperacion;
	
	/**  variable para el dao de recepcion *. */
	@EJB
	private transient DAODetRecepOperacion detOperacion;
		
	/** La variable que contiene informacion con respecto a: dao apartadas. */
	@EJB
	private transient DAORecepOperacionApartadas daoApartadas;
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constante util. */
	private UtileriasRecepcionOpConsultas utilApartadas = UtileriasRecepcionOpConsultas.getInstance();
	
	/** La variable que contiene informacion con respecto a: dao bit transaccional. */
	@EJB
	private DAOBitTransaccional dAOBitTransaccional;
	
	/** La variable que contiene informacion con respecto a: NOAPLICABLE. */
	private static final String NOAPLICABLE = "No aplica";
	
	 /**
 	 * Devolver operacion.
 	 *
 	 * @param beanReqTranSpeiRec El objeto con infromacion de la operacion
 	 * @param sessionBean the session bean
 	 * @param historico ¿es historico?
 	 * @return el resultado de la devolucion
 	 * @throws BusinessException La business exception
 	 */
	@Override
    public BeanResTranSpeiRec devolucionExtemporanea(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException{
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();  
		/**Inicializa variables **/
		BeanResTranSpeiRec beanResConsTranSpeiRecTemp = new BeanResTranSpeiRec();
		//se inicailiza la lista
		beanResConsTranSpeiRecTemp.setListBeanTranSpeiRecOK(new ArrayList<BeanTranSpeiRecOper>());
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
    	/** obtiene la lista de los datos seleccionados**/
    	listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec, HelperRecepcionOperacion.DEVOLUCION);
    	
    	if(listBeanTranSpeiRecTemp.isEmpty()){
    		/**seta los codigos de error **/
			beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRecTemp;
		
    	}
    	
    	BeanTranSpeiRecOper beanTranSpeiRec = new BeanTranSpeiRecOper();
		/**rrecorre la lista **/
		for(BeanTranSpeiRecOper beanTranSpei: listBeanTranSpeiRecTemp){
			beanTranSpeiRec = beanTranSpei;
			beanTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusTransfer("RE");
			
			if(historico) {
				beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPago("-"+beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPago());
				beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPaquete("-"+beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPaquete());
			}		
			/**pregunta si tiene el mocito de la devolucion **/
			if(beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol()==null || beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol().length()<1) { 				
					/**manda el error en caso de que no tenga comentario **/
					beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.ED00129V);
					beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);   
					return beanResConsTranSpeiRecTemp;
			}
								
			
			/**Se asigna a la fecha de captura la fecha anterior de operacion para el nuevo registro**/
			beanTranSpeiRec.getBeanDetalle().getBeanCambios().setFchCaptura(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion());
			/**se asigna la fecha de operacion de la BD**/
			beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFchOperacion(beanTranSpei.getBeanDetalle().getDetalleGral().getFchOperacion());
			beanTranSpeiRec = validarOpera(beanTranSpeiRec, historico, sessionBean, beanReqTranSpeiRec);
			
			
		}
		
		if(beanTranSpeiRec.getBeanDetalle().getError().getCodError().equals(Errores.OK00021V)) {
			beanResConsTranSpeiRecTemp.getListBeanTranSpeiRecOK().add(beanTranSpeiRec);
		}		
		/**seta los codigos de error **/
		beanResConsTranSpeiRecTemp.getBeanError().setCodError(beanTranSpeiRec.getBeanDetalle().getError().getCodError());
		beanResConsTranSpeiRecTemp.getBeanError().setTipoError(beanTranSpeiRec.getBeanDetalle().getError().getTipoError());
		
		/**regresa el objeto **/
  		return beanResConsTranSpeiRecTemp;
    }
	
	
	
	
	/**
	 * Obtener detalle.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param beanTranSpei El objeto: con la informacion de la operacion
	 * @return Objeto string
	 */
	private BeanDetRecepOperacionDAO obtenerDetalle( ArchitechSessionBean sessionBean,BeanTranSpeiRecOper beanTranSpei, BeanReqTranSpeiRec beanReqTranSpeiRec)  {
		//se inicializa el bean
		BeanDetRecepOperacionDAO bean = new BeanDetRecepOperacionDAO();
		//se inicializa los datos del detalle general de recepcion de operaciones
		bean.getBeanDetRecepOperacion().getDetalleGral().setFolioPago(beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPago());
		bean.getBeanDetRecepOperacion().getDetalleGral().setFolioPaquete(beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPaquete());
		bean.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(beanTranSpei.getBeanDetalle().getDetalleGral().getFchOperacion());
		bean.getBeanDetRecepOperacion().getDetalleGral().setCveMiInstitucion(beanTranSpei.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		bean.getBeanDetRecepOperacion().getDetalleGral().setCveInstOrd(beanTranSpei.getBeanDetalle().getDetalleGral().getCveInstOrd());
		BeanDetRecepOperacionDAO beanRel = new BeanDetRecepOperacionDAO();
		beanRel = dAODetRecepOperacion.consultaDetRecepcionOp(bean, sessionBean);
		return beanRel;
	}
	
	
	/**
	 * Validar operacion.
	 *
	 * @param beanTranSpei El objeto: con la informacion de la operacion
	 * @param historico El objeto: bandera que identifica si es historico
	 * @param sessionBean El objeto: session bean
	 * @param beanReqTranSpeiRec El objeto: con la informacion de la peticion
	 * @return Objeto bean BeanReqTranSpeiRec validado
	 * @throws BusinessException La business exception
	 */
	private BeanTranSpeiRecOper validarOpera(BeanTranSpeiRecOper beanTranSpei, boolean historico,ArchitechSessionBean sessionBean,
			BeanReqTranSpeiRec beanReqTranSpeiRec) throws BusinessException {
		BeanTranSpeiRecOper bean = new BeanTranSpeiRecOper();
		BeanDetRecepOperacionDAO registroDia = new BeanDetRecepOperacionDAO();
		bean = beanTranSpei;
		BeanResBase existe = new BeanResBase();
		//llama la funcion para saber si hay un registro existente
		existe = dAODetRecepOperacion.existeOperacion(sessionBean, beanTranSpei);
		/**Se pregunta si existe la operacion **/
		if(existe.getCodError().equals(Errores.ED00133V)) {
			bean.getBeanDetalle().getError().setCodError(existe.getCodError());
			bean.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ALERT); 
			
		}else {
			registroDia = nuevoRegistroDevo(historico,sessionBean,beanReqTranSpeiRec.getBeanComun().getNombrePantalla(),bean, "devolucionExtemporanea.do",beanReqTranSpeiRec);
			bean.setBeanDetalle(registroDia.getBeanDetRecepOperacion());
			/**se pregunta si viene de la pantalla historicas para asignar la fecha de operacion de la BD**/
			if(registroDia.getBeanDetRecepOperacion().getError().getCodError().equals(Errores.EC00011B) ){
				bean.getBeanDetalle().getError().setCodError(Errores.ED00134V);
				bean.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ERROR); 				
			}else {
				bean.getBeanDetalle().getError().setCodError(Errores.OK00021V);
				bean.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_INFO); 
			}
		}
		
		return bean;
	}
	
	

	
	/**
	 * Nuevo registro devo.
	 *
	 * @param historico El objeto: ¿es historico?
	 * @param sessionBean El objeto: session bean
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @param beanTranSpei El objeto: con la infromacion de la operacion
	 * @param pundoDo El objeto: informacion del .do
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanDetRecepOperacionDAO nuevoRegistroDevo(boolean historico, ArchitechSessionBean sessionBean,String nombrePantalla,
			BeanTranSpeiRecOper beanTranSpei, String pundoDo, BeanReqTranSpeiRec beanReqTranSpeiRec)  throws BusinessException  {
		BeanDetRecepOperacionDAO beanNuevo = new BeanDetRecepOperacionDAO();
		BeanDetRecepOperacionDAO beanAnterior = new BeanDetRecepOperacionDAO();
		BeanDetRecepOperacionDAO registroDia = new BeanDetRecepOperacionDAO();
		// se inicializa bean con estatus
		beanNuevo.getBeanDetRecepOperacion().setDetEstatus(new BeanDetRecepOp());		
		beanNuevo.getBeanDetRecepOperacion().getDetalleGral().setCveMiInstitucion(beanTranSpei.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		beanNuevo.getBeanDetRecepOperacion().getDetalleGral().setCveInstOrd(beanTranSpei.getBeanDetalle().getDetalleGral().getCveInstOrd());
		beanNuevo.getBeanDetRecepOperacion().getDetalleGral().setFolioPago(beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPago());
		beanNuevo.getBeanDetRecepOperacion().getDetalleGral().setFolioPaquete(beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPaquete());
		beanNuevo.getBeanDetRecepOperacion().getBeanCambios().setFchCaptura(beanTranSpei.getBeanDetalle().getBeanCambios().getFchCaptura());
		beanNuevo.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(beanTranSpei.getBeanDetalle().getDetalleGral().getFchOperacion());
		beanNuevo.getBeanDetRecepOperacion().getDetEstatus().setEstatusTransfer(beanTranSpei.getBeanDetalle().getDetEstatus().getEstatusTransfer());
		/**se pregunta si viene de la pantalla historicas para asignar la fecha de operacion de la BD**/
		boolean tipoHis = historico(beanNuevo,beanTranSpei,historico, nombrePantalla );
		
		/**validar si existe**/
		beanAnterior = detOperacion.generarAutorizacionHTO( beanNuevo, sessionBean, tipoHis, beanReqTranSpeiRec);		
		
		
		BeanResTranSpeiRec beanApartada = new BeanResTranSpeiRec();
		// pregunta si la autorizacion
		if(!beanAnterior.getBeanDetRecepOperacion().getError().getCodError().equals(Errores.EC00011B)) {
			// llama la funcion para apartar
			beanApartada= daoApartadas.apartaOperaciones(beanTranSpei, sessionBean, false, nombrePantalla);
			// variable para la bitacora
			 String datosInsert = beanTranSpei.getBeanDetalle().getDetalleGral().getFchOperacion()+","
					 +beanTranSpei.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+","+beanTranSpei.getBeanDetalle().getOrdenante().getCveIntermeOrd()+","+
					 beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPaquete()+","+beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPago()+","+sessionBean.getUsuario();
			 String datosNuevo =ConstantesRecepOperacion.FCH_OPERACION+"="+beanTranSpei.getBeanDetalle().getDetalleGral().getFchOperacion()+","+
					 ConstantesRecepOperacion.CVE_MI_INSTITUC+"="+beanTranSpei.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+","+
					 ConstantesRecepOperacion.CVE_INST_ORD+"="+beanTranSpei.getBeanDetalle().getOrdenante().getCveIntermeOrd()
					 +","+ConstantesRecepOperacion.FOLIO_PAQUETE+"="+beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPaquete()+","+
					 ConstantesRecepOperacion.FOLIO_PAGO+"="+beanTranSpei.getBeanDetalle().getDetalleGral().getFolioPago()+","+
					 ConstantesRecepOperacion.USUARIO_REGISTRA+"="+sessionBean.getUsuario();
			 String datosAnte = ConstantesRecepOperacion.FCH_OPERACION+"="+beanAnterior.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion()+","+
					 ConstantesRecepOperacion.CVE_MI_INSTITUC+"="+beanAnterior.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion()+","+
					 ConstantesRecepOperacion.CVE_INST_ORD+"="+beanAnterior.getBeanDetRecepOperacion().getOrdenante().getCveIntermeOrd()
					 +","+ConstantesRecepOperacion.FOLIO_PAQUETE+"="+beanAnterior.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete()+","+
					 ConstantesRecepOperacion.FOLIO_PAGO+"="+beanAnterior.getBeanDetRecepOperacion().getDetalleGral().getFolioPago()+","+
					 ConstantesRecepOperacion.USUARIO_REGISTRA+"="+sessionBean.getUsuario();
			//se asigna el codigo de error
				String codigo = beanAnterior.getBeanDetRecepOperacion().getCodigoError();
			 String dato = ConstantesRecepOperacionApartada.TYPE_INSERT+"/"+pundoDo+"/"+datosInsert+"/"+codigo+"/"+ datosNuevo+"/"+datosAnte;
			 boolean ok = true;
			 if(Errores.EC00011B.equals(beanApartada.getBeanError().getCodError())) {					
					ok= false;
					/** Se inserta en bitacora **/
					agregaPistasBitacora(dato, ok, historico, sessionBean,beanTranSpei);
			} else {
				/** Se inserta en bitacora **/
				agregaPistasBitacora(dato, ok, historico, sessionBean,beanTranSpei);
			}			
		}
		try {
			Thread.sleep(5000);
			registroDia = obtenerDetalle(sessionBean, beanTranSpei,beanReqTranSpeiRec);
		} catch (InterruptedException e) {
			showException(e);
		}
		
		// se asigna el valor
		registroDia.getBeanDetRecepOperacion().setError(beanAnterior.getBeanDetRecepOperacion().getError());
		return registroDia;
	}

	
	/**
	 * Valida si es operacion historica
	 *
	 * @param beanNuevo El objeto: bean nuevo
	 * @param beanTranSpei El objeto: bean con la informacion de la operacion
	 * @param historico El objeto: bandera que identifica si es historico
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @return true, si exitoso
	 */
	private boolean historico(BeanDetRecepOperacionDAO beanNuevo,BeanTranSpeiRecOper beanTranSpei,boolean historico,String nombrePantalla ) {
		boolean tipoHis= true;
		/**se pregunta si viene de la pantalla historicas para asignar la fecha de operacion de la BD**/
		if(("RE").equals(beanTranSpei.getBeanDetalle().getDetEstatus().getEstatusTransfer()) && historico) {			
			beanNuevo.getBeanDetRecepOperacion().getDetalleGral().getFolioPago().replaceAll("-", "");
			beanNuevo.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete().replaceAll("-", "");
			//ssigna el valor
			tipoHis = historico;
		}else {
			if(nombrePantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA) 
					||nombrePantalla.equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_DIA)) {
				//ssigna el valor
				tipoHis = false;
			}else {
				//ssigna el valor
				tipoHis = historico;
			}
				
		}
		//regresa el resultado
		return tipoHis;
	}
	
	
	/**
	 * Enviar HTO.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean con la informaciond e la peticion a enviar
	 * @param sessionBean El objeto: bean de session
	 * @param historico El objeto: ¿es historico?
	 * @return Objeto bean de respuesta
	 * @throws BusinessException La business exception
	 */
	@Override
    public BeanResTranSpeiRec enviarHTO(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException{
		boolean contrario = true;
		if(historico) {
			contrario=false;
		}
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();  
		/**Inicializa variables **/
		BeanResTranSpeiRec beanResConsTranSpeiRecTemp = new BeanResTranSpeiRec();
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
    	/** obtiene la lista de los datos seleccionados**/
    	listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec, HelperRecepcionOperacion.SELECCIONADO);
    	//lista para realizar las operaciones
    	if(listBeanTranSpeiRecTemp.isEmpty()){
    		/**seta los codigos de error **/
			beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRecTemp;
		
    	}
    	// se inicializan las variables
    	int totalExito=0;
    	int totalSinExito=0;
    	
		/**rrecorre la lista **/
		for(BeanTranSpeiRecOper beanAnterior: listBeanTranSpeiRecTemp){
			BeanTranSpeiRecOper existe = new BeanTranSpeiRecOper();
			
			BeanTranSpeiRecOper inserto = new BeanTranSpeiRecOper();
			// variable para la bitacora
			String datosInsert = beanAnterior.getBeanDetalle().getDetalleGral().getFchOperacion()+","
					 +beanAnterior.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+","+beanAnterior.getBeanDetalle().getOrdenante().getCveIntermeOrd()+","+
					 beanAnterior.getBeanDetalle().getDetalleGral().getFolioPaquete()+","+beanAnterior.getBeanDetalle().getDetalleGral().getFolioPago()+","+sessionBean.getUsuario();
			//dato para insertar en la bitacora
			String datosNuevo =ConstantesRecepOperacion.FCH_OPERACION+"="+beanAnterior.getBeanDetalle().getDetalleGral().getFchOperacion()+","+
					 ConstantesRecepOperacion.CVE_MI_INSTITUC+"="+beanAnterior.getBeanDetalle().getDetalleGral().getCveMiInstitucion()+","+
					 ConstantesRecepOperacion.CVE_INST_ORD+"="+beanAnterior.getBeanDetalle().getOrdenante().getCveIntermeOrd()
					 +","+ConstantesRecepOperacion.FOLIO_PAQUETE+"="+beanAnterior.getBeanDetalle().getDetalleGral().getFolioPaquete()+","+
					 ConstantesRecepOperacion.FOLIO_PAGO+"="+beanAnterior.getBeanDetalle().getDetalleGral().getFolioPago()+","+
					 ConstantesRecepOperacion.USUARIO_REGISTRA+"="+sessionBean.getUsuario();
			//se asigna el codigo de error
			String codigo = beanAnterior.getBeanDetalle().getCodigoError();
			String datoElimina = ConstantesRecepOperacionApartada.TYPE_DELETE+"/enviarHto.do/"+datosInsert+"/"+codigo+"/- /"+datosNuevo;
									
			boolean ok=true;
			//llama la funcion para saber si hay un registro existente
			existe = dAODetRecepOperacion.existeBeanOper(beanAnterior, contrario);
			// en caso de existir
			if(existe.getBeanDetalle().getError().getCodError().equals(Errores.OK00000V)) {
				//funcion para insertar
				String datoInserta = ConstantesRecepOperacionApartada.TYPE_INSERT+"/enviarHto.do/"+datosInsert+"/"+codigo+"/"+ datosNuevo+"/- ";
				inserto =  nuevoApartado(sessionBean,contrario,beanAnterior,datoInserta,datoElimina);
				
				if(!inserto.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
					totalExito++;
					//inicializa el obejto
					BeanTranSpeiRecOper beanEliminar= new BeanTranSpeiRecOper();
					//elimina el registro de la tabla historica
					beanEliminar = dAODetRecepOperacion.eliminarRegistro(sessionBean, beanAnterior, contrario);
					ok = validaCodError(beanEliminar,ok);
				}
				/**genera la pista de auditoria**/
				BeanPistaAuditora pElimina = utilApartadas.generaPistaAuditoras(datoElimina, ok, sessionBean, contrario);
				/**inserta en bitacora **/
				boPistaAuditora.llenaPistaAuditora(pElimina, sessionBean);
			}
		
			beanResConsTranSpeiRecTemp.setBeanError(existe.getBeanDetalle().getError());
			//si hay error asigna el codigo de error
			if(existe.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
				totalSinExito++;
				break;
			} else if(existe.getBeanDetalle().getError().getCodError().equals(Errores.ED00137V)) {
				beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
				return beanResConsTranSpeiRecTemp;
			}
			
			
		}
		// si cumple con el registro de todas las operacioens en la historica manda el siguiente mensaje
		if(listBeanTranSpeiRecTemp.size()== totalExito) {
			beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.OK00023V);
			beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			// si alguna operacion no se pudo insertar en la historica  manda mensaje de alerta
			beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.OK00022V);
			beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		// si alguna operacion no se pudo pasar a la historica  manda mensaje de alerta
		if(totalSinExito>0) {
			beanResConsTranSpeiRecTemp.getBeanError().setCodError(Errores.OK00022V);
			beanResConsTranSpeiRecTemp.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		
		
		/**regresa el objeto **/
  		return beanResConsTranSpeiRecTemp;
    }
	
	/**
	 * Existe operacion.
	 *
	 * @param sessionBean El objeto: bean session
	 * @param historico El objeto: ¿es historico?
	 * @param beanAnterior El objeto: bean anterior
	 * @param dato El objeto: dato 
	 * @return Objeto bean de respuesta
	 */
	private BeanTranSpeiRecOper existeOperacion(ArchitechSessionBean sessionBean, boolean historico,BeanTranSpeiRecOper beanAnterior,String dato) {
		BeanTranSpeiRecOper existe = new BeanTranSpeiRecOper();
		boolean ok=true;
		//llama la funcion para saber si hay un registro existente
		existe = dAODetRecepOperacion.existeBeanOper(beanAnterior, historico);
		// en caso de existir
		if(existe.getBeanDetalle().getError().getCodError().equals(Errores.OK00000V)) {
			//inicializa el obejto
			BeanTranSpeiRecOper beanEliminar= new BeanTranSpeiRecOper();
			//elimina el registro de la tabla historica
			beanEliminar = dAODetRecepOperacion.eliminarRegistro(sessionBean, beanAnterior, historico);
			if(beanEliminar.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
				ok = false;
			}
			/**genera la pista de auditoria**/
			BeanPistaAuditora pElimina = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditora(pElimina, sessionBean);
		}
		return existe;
		
	}
	
	
	/**
	 * Nuevo apartado.
	 *
	 * @param sessionBean El objeto: bean session 
	 * @param historico El objeto: ¿es historico?
	 * @param beanAnterior El objeto: bean anterior
	 * @param dato El objeto: dato nuevo
	 * @param datoElimina El objeto: dato elimina
	 * @return Objeto bean respuesta
	 */
	private BeanTranSpeiRecOper nuevoApartado(ArchitechSessionBean sessionBean, boolean historico,BeanTranSpeiRecOper beanAnterior,String dato,String datoElimina) {
		
		BeanTranSpeiRecOper inserto = new BeanTranSpeiRecOper();
		
		boolean ok=true;
		// inserta el nuevo registro en la tabla historica y el la de apardos
		inserto = dAODetRecepOperacion.nuevoRegistroHTO(beanAnterior,sessionBean, historico);
		// asigna valor para la pista de auditoria
		if(inserto.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
			ok = false;				
		}
		/**genera la pista de auditoria**/
		BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
		/**inserta en bitacora **/
		boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
		//inserta en apartados
		daoApartadas.apartaOperaciones(beanAnterior, sessionBean, historico, beanAnterior.getBeanDetalle().getNombrePantalla());
		/**genera la pista de auditoria**/
		pistas = utilApartadas.generaPistaAuditoras(dato.replace("enviarHto", "recepOperaApartadas"), ok, sessionBean, historico);
		/**inserta en bitacora **/
		boPistaAuditora.llenaPistaAuditora(pistas, sessionBean);
		
		boolean contrario = true;
		if(historico) {
			contrario=false;
		}
		
		if(ok == true) {
			// eliminar registro de la tabla del dia
			existeOperacion( sessionBean, contrario,beanAnterior, datoElimina);
		}
		return inserto;
		
	}
	
	/**
	 * Agrega pistas bitacora.
	 *
	 * @param datos the datos
	 * @param envio the envio
	 * @param historico the historico
	 * @param sessionBean the session bean
	 * @param beanTranSpeiRecOper the bean tran spei rec oper
	 * @throws BusinessException the business exception
	 */
	public void agregaPistasBitacora(String datos,Boolean envio, boolean historico,ArchitechSessionBean sessionBean, BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		BeanResGuardaBitacoraDAO beanBitacora= new BeanResGuardaBitacoraDAO();
		String cero = "0";
		String[] dato= datos.split("/");
		
		/**Objetos para las Pistas auditoras**/
		BeanReqInsertBitTrans bean = new BeanReqInsertBitTrans();
		/** Enviamos los datos de la pista auditora**/
		bean.getDatosGenerales().setAplicacionCanal(sessionBean.getNombreAplicacion());
		bean.getDatosGenerales().setCodCliente(NOAPLICABLE);
		bean.getDatosGenerales().setContrato(NOAPLICABLE);
		bean.getDatosGenerales().setDirIp(sessionBean.getIPCliente());
		bean.getDatosGenerales().setFchAct(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchAplica(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchProgramada(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setHoraAct(sessionBean.getUltimoAcceso());
		bean.getDatosGenerales().setHostWeb(sessionBean.getIPServidor());
		bean.getDatosGenerales().setIdSesion(sessionBean.getIdSesion());
		bean.getDatosGenerales().setIdToken(cero);
		bean.getDatosGenerales().setInstanciaWeb(sessionBean.getNombreServidor());
		bean.getDatosGenerales().setNombreArchivo("NA");
		bean.getDatosGenerales().setNumeroTitulos(cero);
		
		bean.getDatosOpe().setBancoDestino(NOAPLICABLE);
		bean.getDatosOpe().setCodigoErr(beanTranSpeiRecOper.getBeanDetalle().getError().getCodError());
		bean.getDatosOpe().setComentarios(beanTranSpeiRecOper.getBeanDetalle().getError().getMsgError());
		bean.getDatosOpe().setCtaDestino(beanTranSpeiRecOper.getBeanDetalle().getReceptor().getNumCuentaRec());
		bean.getDatosOpe().setCtaOrigen(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		bean.getDatosOpe().setDescErr("EXITO");
		if (envio) {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.OK);
			bean.getDatosOpe().setDescOper("Operacion exitosa");
		} else {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.NOK);
			bean.getDatosOpe().setDescOper("Operacion rechazada");
		}
		bean.getDatosOpe().setFchOperacion(beanTranSpeiRecOper.getBeanDetalle().getDetalleGral().getFchOperacion());
		bean.getDatosOpe().setIdOperacion("01DEVOLRECOP");
		bean.getDatosOpe().setMonto(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getMonto());
		bean.getDatosOpe().setReferencia("REFERENCIA = "+beanTranSpeiRecOper.getBeanDetalle().getDetEstatus().getRefeTransfer());
		bean.getDatosOpe().setServTran(dato[1]);
		bean.getDatosOpe().setTipoCambio(cero);
		
		/**inserta en bitacora **/
		beanBitacora = dAOBitTransaccional.guardaBitacora(bean, sessionBean);
		
		if(beanBitacora.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanBitacora.getCodError(), beanBitacora.getMsgError());
		}
	
		
	}
	
	/**
	 * Valida cod error.
	 *
	 * @param beanEliminar bean a eliminar
	 * @param bandera que identifica si es correcto 
	 * @return true, si existoso
	 */
	public boolean validaCodError(BeanTranSpeiRecOper beanEliminar, boolean ok) {
		if(beanEliminar.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
			ok = false;
		}
		return ok;
	}

		
}