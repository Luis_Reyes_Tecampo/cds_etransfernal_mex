/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOBitacoraEnvioImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvioDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOBitacoraEnvio;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Clase del tipo BO que se encarga del negocio de la bitacora de envio
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOBitacoraEnvioImpl implements BOBitacoraEnvio {

	/** Referencia privada al dao */
	@EJB
	private DAOBitacoraEnvio daoBitacoraEnvio;

	/**
	 * Metodo que sirve para consultar los envios
	 * 
	 * @param paginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param sortField
	 *            Objeto del tipo String
	 * @param sortType
	 *            Objeto del tipo String
	 * @return BeanResBitacoraEnvio
	 * @throws BusinessException
	 *             Exception
	 */
	@Override
	public BeanResBitacoraEnvio obtenerBitacoraEnvio(BeanPaginador paginador,
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType) throws BusinessException {
		BeanResBitacoraEnvioDAO beanResBitacoraEnvioDAO = null;
		final BeanResBitacoraEnvio beanResBitacoraEnvio = new BeanResBitacoraEnvio();
		BeanPaginador pag = paginador;
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		String sF = getSortField(sortField);

		beanResBitacoraEnvioDAO = daoBitacoraEnvio.obtenerBitacoraEnvio(
				paginador, architechSessionBean, sF, sortType);

		if (Errores.CODE_SUCCESFULLY.equals(beanResBitacoraEnvioDAO
				.getCodError())
				&& beanResBitacoraEnvioDAO.getListBeanBitacoraEnvio().isEmpty()) {
			beanResBitacoraEnvio.setCodError(Errores.ED00011V);
			beanResBitacoraEnvio.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResBitacoraEnvio.setListaBitacoraEnvio(beanResBitacoraEnvioDAO
					.getListBeanBitacoraEnvio());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResBitacoraEnvioDAO
				.getCodError())) {
			beanResBitacoraEnvio.setListaBitacoraEnvio(beanResBitacoraEnvioDAO
					.getListBeanBitacoraEnvio());
			beanResBitacoraEnvio.setTotalReg(beanResBitacoraEnvioDAO
					.getTotalReg());
			beanResBitacoraEnvio
					.setImporteTotal(daoBitacoraEnvio.obtenerImporteTotalTopo(
							architechSessionBean, sF, sortType).toString());
			paginador.calculaPaginas(beanResBitacoraEnvioDAO.getTotalReg(),
					HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResBitacoraEnvio.setBeanPaginador(paginador);
			beanResBitacoraEnvio.setCodError(Errores.OK00000V);
		} else {
			beanResBitacoraEnvio.setCodError(Errores.EC00011B);
			beanResBitacoraEnvio.setTipoError(Errores.TIPO_MSJ_ERROR);
		}

		return beanResBitacoraEnvio;
	}

	/**
	 * @param sortField
	 *            Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";

		if ("folioSolicitud".equals(sortField)) {
			sF = "FOLIO_SOLICITUD";
		} else if ("tipoDeMensaje".equals(sortField)) {
			sF = "TIPO_MENSAJE";
		} else if ("fechaHoraEnvio".equals(sortField)) {
			sF = "FCH_HORA_ENVIO";
		} else if ("numeroPagos".equals(sortField)) {
			sF = "NUM_PAGOS";
		} else if ("importeAbono".equals(sortField)) {
			sF = "IMPORTE";
		} else if ("codBanco".equals(sortField)) {
			sF = "CVE_INST_BENEF";
		} else if ("numReprocesos".equals(sortField)) {
			sF = "NUM_REPROCESOS";
		} else if ("acuseRecibo".equals(sortField)) {
			sF = "RECIBI_ACUSE";
		} else if ("bytesAcumulados".equals(sortField)) {
			sF = "BYTES_ACUMULADOS";
		} else if ("operacionCert".equals(sortField)) {
			sF = "OPERACION_CERT";
		} else if ("numCertificado".equals(sortField)) {
			sF = "NUM_CERTIFICADO";
		}
		return sF;
	}

	/**
	 * Metodo que sirve para exportar todos los envios
	 * 
	 * @param beanReqBitacoraEnvio
	 *            Objeto del tipo @see BeanReqBitacoraEnvio
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @return BeanResBitacoraEnvio
	 * @throws BusinessException
	 *             Exception
	 */
	@Override
	public BeanResBitacoraEnvio exportarTodoBitacoraEnvio(
			BeanReqBitacoraEnvio beanReqBitacoraEnvio,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResBitacoraEnvioDAO beanResBitacoraEnvioDAO = null;
		BeanResBitacoraEnvio beanResBitacoraEnvio = null;

		BeanPaginador paginador = beanReqBitacoraEnvio.getPaginador();

		if (paginador == null) {
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		beanResBitacoraEnvio = obtenerBitacoraEnvio(paginador,
				architechSessionBean, "", "");
		beanResBitacoraEnvio.setTotalReg(beanResBitacoraEnvio.getTotalReg());
		beanResBitacoraEnvioDAO = daoBitacoraEnvio.exportarTodoBitacoraEnvio(
				beanReqBitacoraEnvio, architechSessionBean);

		if (Errores.CODE_SUCCESFULLY.equals(beanResBitacoraEnvioDAO
				.getCodError())) {
			beanResBitacoraEnvio.setCodError(Errores.OK00002V);
			beanResBitacoraEnvio.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResBitacoraEnvio.setNombreArchivo(beanResBitacoraEnvioDAO
					.getNombreArchivo());
		} else {
			beanResBitacoraEnvio.setCodError(Errores.EC00011B);
			beanResBitacoraEnvio.setTipoError(Errores.TIPO_MSJ_ERROR);
		}

		return beanResBitacoraEnvio;
	}

	/**
	 * @return the daoBitacoraEnvio objeto del tipo DAOBitacoraEnvio
	 */
	public DAOBitacoraEnvio getDaoBitacoraEnvio() {
		return daoBitacoraEnvio;
	}

	/**
	 * @param daoBitacoraEnvio
	 *            the DAOBitacoraEnvio to set
	 */
	public void setDaoBitacoraEnvio(DAOBitacoraEnvio daoBitacoraEnvio) {
		this.daoBitacoraEnvio = daoBitacoraEnvio;
	}

}
