/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCdaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOConsultaMovCDA;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchCDAHist;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Consulta de Movimiento CDA
 * 
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaMovCDAImpl extends Architech implements BOConsultaMovCDA {
       
    /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -3741048660205973483L;

	/**
	 * Referencia privada al dao de consulta movimiento CDA
	 */
	@EJB
	private DAOConsultaMovCDA daoConsultaMovCDA;
	
	/**
	 * Referencia privada al dao de generar archivos historicos
	 */
	@EJB
	private DAOGenerarArchCDAHist dAOGenerarArchCDAHist;

	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConsMovCDA Objeto del tipo @see BeanResConsMovCDA
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovCDA consultarMovCDA(
			BeanReqConsMovCDA beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
			BeanResConsMovCDA beanResConsMovCDA = new BeanResConsMovCDA();
		    BeanPaginador paginador = null;
			BeanResConsMovCdaDAO beanResConsMovCdaDAO = null;
			List<BeanMovimientoCDA> listBeanMovCDA = null;
			
			paginador = beanReqConsMovCDA.getPaginador();
		    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());			    
		    beanResConsMovCdaDAO =  daoConsultaMovCDA.consultarMovCDA(beanReqConsMovCDA,architechSessionBean);
		    listBeanMovCDA = beanResConsMovCdaDAO.getListBeanMovimientoCDA();
		    
		    if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovCdaDAO.getCodError()) && listBeanMovCDA.isEmpty()){
		    	beanResConsMovCDA.setCodError(Errores.ED00011V);
		    	beanResConsMovCDA.setTipoError(Errores.TIPO_MSJ_INFO);		      
		    }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovCdaDAO.getCodError())){
		    	 beanResConsMovCDA.setTotalReg(beanResConsMovCdaDAO.getTotalReg());
				    beanResConsMovCDA.setListBeanMovimientoCDA(beanResConsMovCdaDAO.getListBeanMovimientoCDA());
				    paginador.calculaPaginas(beanResConsMovCDA.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
				    beanResConsMovCDA.setBeanPaginador(paginador);	
				    beanResConsMovCDA.setCodError(Errores.OK00000V);
		    }else{
		    	beanResConsMovCDA.setCodError(Errores.EC00011B);
		    	beanResConsMovCDA.setTipoError(Errores.TIPO_MSJ_ERROR); 
		      }		    		    
		   	    
		    return beanResConsMovCDA;
	}

	/**
	 * Metodo que se encarga de solicitar la insercion de la peticion de
	 * Exportar Todos de los Movimientos CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConMovCDA  Objeto del tipo @see BeanResConsMovCDA 
	 * @throws BusinessException Excepcion de negocio
	 */
	
	public BeanResConsMovCDA consultaMovCDAExpTodos(
			BeanReqConsMovCDA beanReqConsMovCDA,			
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		    
			BeanResConsMovCdaDAO beanResConsMovCdaDAO = null;
		    BeanResConsMovCDA beanResConMovCDA = consultarMovCDA(beanReqConsMovCDA, architechSessionBean);
		    
		    beanReqConsMovCDA.setTotalReg(beanResConMovCDA.getTotalReg());
		    beanResConsMovCdaDAO = daoConsultaMovCDA.guardarConsultaMovExpTodo(beanReqConsMovCDA, architechSessionBean);
		    
		    if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovCdaDAO.getCodError())){
		    	beanResConMovCDA.setCodError(Errores.OK00001V);
		    	beanResConMovCDA.setTipoError(Errores.TIPO_MSJ_INFO);
		    	beanResConMovCDA.setNombreArchivo(beanResConsMovCdaDAO.getNombreArchivo());
		    }else{
		    	beanResConMovCDA.setCodError(Errores.EC00011B);
		    	beanResConMovCDA.setTipoError(Errores.TIPO_MSJ_ERROR); 
		      }	
		return beanResConMovCDA;
	}
   
	/**
	 * Metodo que sirve para solicitar el detalle de un movimiento CDA
	 * @param beanReqConsMovCDADet Objeto del tipo @see BeanReqConsMovCDADet
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetCDA Objeto del tipo @see BeanResConsMovDetCDA
	 * @throws BusinessException Excepcion de negocio
	 */
	
	public BeanResConsMovDetCDA consMovDetCDA(
			BeanReqConsMovCDADet beanReqConsMovCDADet,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResConsMovDetCDA beanResConsMovDetCDA = new BeanResConsMovDetCDA();
		BeanResConsMovDetCdaDAO beanResConsMovDetCdaDAO = null;
		beanResConsMovDetCdaDAO =  daoConsultaMovCDA.consultarMovDetCDA(beanReqConsMovCDADet, architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovDetCdaDAO.getCodError())){
			beanResConsMovDetCDA.setBeanMovCdaDatosGenerales(beanResConsMovDetCdaDAO.getBeanMovCdaDatosGenerales());
			beanResConsMovDetCDA.setBeanConsMovCdaOrdenante(beanResConsMovDetCdaDAO.getBeanConsMovCdaOrdenante());
			beanResConsMovDetCDA.setBeanConsMovCdaBeneficiario(beanResConsMovDetCdaDAO.getBeanConsMovCdaBeneficiario());			

			beanResConsMovDetCDA.setCodError(Errores.OK00000V);
	    }else{
	    	beanResConsMovDetCDA.setCodError(Errores.EC00011B);
	    	beanResConsMovDetCDA.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }	
		
		
		return beanResConsMovDetCDA;
	}
	
	/**
	 * Metodo para consultar la fecha de operacion de Banco de Mexico
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param esSPID Indica si es SPID
	 * @return BeanResConsMovCDA Objeto del tipo @see BeanResConsFechaOp
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovCDA consultarFechaOperacion(ArchitechSessionBean sessionBean, boolean esSPID) throws BusinessException{
		BeanResConsMovCDA beanResConsMovCDA = new BeanResConsMovCDA();
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
		beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(sessionBean, esSPID);
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
			beanResConsMovCDA.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		}else{
			beanResConsMovCDA.setCodError(Errores.EC00011B);
			beanResConsMovCDA.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return beanResConsMovCDA;
	}

	/**
	 * Metodo get para obtener el valor de daoConsultaMovCDA
	 * @return  daoConsultaMovCDA objeto de tipo @see  DAOConsultaMovCDA
	 */
	public DAOConsultaMovCDA getDaoConsultaMovCDA() {
		return daoConsultaMovCDA;
	}

	/**
	 * Metodo para establecer el valor del daoConsultaMovCDA
	 * @param daoConsultaMovCDA objeto de tipo @see  DAOConsultaMovCDA
	 */
	public void setDaoConsultaMovCDA(DAOConsultaMovCDA daoConsultaMovCDA) {
		this.daoConsultaMovCDA = daoConsultaMovCDA;
	}

	/**
	 * Metodo get para obtener el valor de dAOGenerarArchCDAHist
	 * @return dAOGenerarArchCDAHist objeto de tipo @see DAOGenerarArchCDAHist
	 */
	public DAOGenerarArchCDAHist getDAOGenerarArchCDAHist() {
		return dAOGenerarArchCDAHist;
	}

	/**
	 * Metodo para establecer el valor del dAOGenerarArchCDAHist
	 * @param generarArchCDAHist objeto de tipo @see DAOGenerarArchCDAHist
	 */
	public void setDAOGenerarArchCDAHist(DAOGenerarArchCDAHist generarArchCDAHist) {
		dAOGenerarArchCDAHist = generarArchCDAHist;
	}
	
	
}
