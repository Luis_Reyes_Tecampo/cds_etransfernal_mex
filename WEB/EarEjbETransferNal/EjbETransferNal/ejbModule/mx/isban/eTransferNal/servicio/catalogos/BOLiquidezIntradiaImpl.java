package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidacionesCombo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaReq;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOLiquidezIntradia;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasLiquidezIntradia;

/**
 * Clase BOLiquidezIntradiaImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOLiquidezIntradiaImpl implements BOLiquidezIntradia {
	
	/** DAO liquidez. */
	@EJB
	private DAOLiquidezIntradia daoLiquidez;
	
	/** DAO exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/**BO pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La constanteTABLA. */
	private static final String TABLA = "tabla";
	
	
	/**
	 * Consulta
	 * Metodo que realiza la consulta a la BD
	 *
	 * @param beanFilter El objeto --> filter
	 * @param canal variable de tipo string
	 * @param beanPaginador El objeto --> bean paginador
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */
	@Override
	public BeanLiquidezIntradiaReq consultar(BeanLiquidezIntradia beanFilter, String canal,BeanPaginador beanPaginador,
			ArchitechSessionBean session) throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		/** Validacion de paginador **/
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanLiquidezIntradiaReq response = daoLiquidez.consultar(beanFilter,canal, beanPaginador, session);
				
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getListaBeanLiquidezIntradia().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Consulta listas
	 * Metodo que realiza la consulta a la BD
	 *
	 * @param beanFilter El objeto --> filter
	 * @param canal variable de tipo string
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */
	@Override
	public BeanLiquidacionesCombo consultarListas(int tipo, BeanLiquidezIntradia beanFilter,ArchitechSessionBean session, String canal) throws BusinessException {

		/** Retorno de la respuesta directa de DAO **/
		return daoLiquidez.consultarListas(tipo, beanFilter,session,canal);
	}

	/**
	 * Agregar
	 * Metodo que agrega registros a la base de datos
	 *
	 * @param liquidacion El objeto --> liquidacion
	 * @param canal variable de tipo string
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */

	@Override
	public BeanResBase agregar(BeanLiquidezIntradia liquidacion,String canal, ArchitechSessionBean session) throws BusinessException {
		/** Validacion de la respuesta del DAO **/
		
		BeanResBase response = daoLiquidez.agregar(liquidacion,canal, session);
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
		
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		 
		String dtoNuevo = obtenerDtos(liquidacion);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TABLA,"INSERT", "agregarNotificacionLiquidez.do", false, session,dtoNuevo,"","INSERT");
		/** Retorno de la respuesta del BO**/
		return response;
	 
	}

	/**
	 * editar
	 * Metodo que edita registros de la base de datos
	 *
	 * @param liquidacion El objeto --> liquidacion
	 * @param canal variable de tipo string
	 * @param anterior El objeto anterior
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */
	@Override
	public BeanResBase editar(BeanLiquidezIntradia liquidacion,String canal, BeanLiquidezIntradia anterior, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Seteo de datos del registro **/
		BeanLiquidezIntradia beanFilter = new BeanLiquidezIntradia();
		beanFilter.setCveTransFe(anterior.getCveTransFe());
		beanFilter.setCveMecanismo(anterior.getCveMecanismo());
		beanFilter.setEstatus(anterior.getEstatus());
		/** Ejecucion de la peticion al DAO **/
		BeanLiquidezIntradiaReq bean = daoLiquidez.consultar(beanFilter,canal, beanPaginador, session);
		String dtoAnt = obtenerDtos(bean.getListaBeanLiquidezIntradia().get(0));
		/** Ejecucion de la peticion al DAO **/
		response = daoLiquidez.editar(liquidacion, anterior,canal, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(liquidacion);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(TABLA,"UPDATE", "editarBloque.do", operacion, session,dtoNuevo,dtoAnt,"UPDATE");
		/** Retorno de la respuesta del BO**/
		return response;

	}

	/**
	 * eliminar
	 * Metodo que elimina registros de la base de datos
	 *
	 * @param liquidez El objeto --> liquidez
	 * @param canal variable de tipo string
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanLiquidezIntradiaReq  liquidez,String canal, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder();
		StringBuilder error = new StringBuilder();
		for(BeanLiquidezIntradia not : liquidez.getListaBeanLiquidezIntradia()){
			if (not.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoLiquidez.eliminar(not, canal,session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(not.getCveTransFe().concat("|").concat(not.getCveMecanismo().concat("|").concat(not.getEstatus())));
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(not.getCveTransFe().concat("|").concat(not.getCveMecanismo().concat("|").concat(not.getEstatus())));
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDtos(not);
	        	generaPistaAuditoras(TABLA,"DELETE", "eliminarLiquidez.do", operacion, session,"",dtoAnt,"SELECT");
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * exportar los registros
	 * Metodo que inserta registros a la base de datos
	 *
	 * @param beanNotificacion El objeto --> notificacion
	 * @param canal variable de tipo string
	 * @param totalRegistros tipo int
	 * @param session El objeto --> session
	 * @return Objeto y vista
	 */
	@Override
	public BeanResBase exportarTodo(BeanLiquidezIntradia beanNotificacion,String canal, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String queryValid ="";
		if("NA".equals(canal)){
			queryValid= UtileriasLiquidezIntradia.CONSULTA_EXPORTAR_TODO_GFI;
		}else{
			queryValid= UtileriasLiquidezIntradia.CONSULTA_EXPORTAR_TODO_TRFPLUS;
		}
		String filtroWhere = UtileriasLiquidezIntradia.getInstancia().getFiltro(beanNotificacion, "WHERE",canal);
		String query = queryValid.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasLiquidezIntradia.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasLiquidezIntradia.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("NOTIFICACION_LIQUIDEZ_INTRADIA");
		exportarRequest.setNombreRpt("NOTIFICACION_LIQUIDEZ_INTRADIA_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras("","EXPORT", "exportarTodoLiquidez.do", operacion, session,"","", "NA");
		/** Retorno de la respuesta del BO**/
		return response;
	}

	
	/**
	 * Genera pista auditoras.
	 *
	 * @param tabla objeto  tabla
	 * @param operacion objeto operacion
	 * @param urlController objeto url controller
	 * @param resOp objeto res op
	 * @param sesion objeto sesion
	 * @param dtoNuevo objeto dto nuevo
	 * @param dtoAnterior objeto dto anterior
	 * @param dtoModificado objeto dto modificado
	 */
	private void generaPistaAuditoras(String tabla ,String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(tabla);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		if("".equals(dtoNuevo)) {
			beanPista.setDatoFijo("-");
		}else {
			beanPista.setDatoFijo(dtoNuevo);
		}
		beanPista.setDatoModi(dtoModificado);
		/**Valida estatus*/
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {	
			if("".equals(dtoAnterior)) {
				beanPista.setDtoAnterior("-");
			}else {
				beanPista.setDtoAnterior(dtoAnterior);
			}
			if("".equals(dtoNuevo)) {
				beanPista.setDtoNuevo("-");
			}else {
				beanPista.setDtoNuevo(dtoNuevo);
			}
 		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus("OK");
		} else {
			beanPista.setEstatus("NOK");
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
	/**
	 * Obtener dtos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 * @param ant El objeto --> ant
	 * @return Objeto string 
	 */
	private String obtenerDtos (BeanLiquidezIntradia ant) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es SELECT, UPDATE, DELETE AND INSERT **/
			dtoAnt ="CVE_TRANSFER=".concat(ant.getCveTransFe()
					.concat(", MECANISMO=".concat(ant.getCveMecanismo()
					.concat(", ESTATUS_TRANSFER=".concat(ant.getEstatus())))));
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
