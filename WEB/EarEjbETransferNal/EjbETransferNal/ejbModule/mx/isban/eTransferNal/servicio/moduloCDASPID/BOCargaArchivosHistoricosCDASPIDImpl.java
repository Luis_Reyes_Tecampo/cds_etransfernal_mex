/**
 * Clase: BOImpl que implementa a BOCargaArchivosHistricosCADSPID de la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 *  @author Vector
 *  @version 1.0
 *  @since Diciembre 2016
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOCargaArchivosHistoricosCDASPID;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * The Class BOCargaArchivosHistoricosCDASPIDImpl.
 */
//The Class BOCargaArchivosHistoricosCDASPIDImpl.
@Remote(BOCargaArchivosHistoricosCDASPID.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCargaArchivosHistoricosCDASPIDImpl extends Architech 
		implements BOCargaArchivosHistoricosCDASPID{

	/**
	 * La variable de serializacion
	 */
	//La variable de serializacion
	private static final long serialVersionUID = 8421604202515919776L;
	
	/**
	 * La variable de instancia daoCargaArchivoHistoricoCADSPID
	 */
	//La variable de instancia daoCargaArchivoHistoricoCADSPID
	@EJB
	private DAOCargaArchivosHistoricosCDASPID daoCargaArchivosHistoricosCDASPID;

	@Override
	public BeanResMonitorCargaArchHistCADSPID obtenerEstatusArchivo(
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean)  throws BusinessException {
		debug("Inicializando el servicio para la pantalla " +
			"Monitor Carga Archivos Historicos CADSPID");
		//Creacion de objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta = 
				new BeanResMonitorCargaArchHistCADSPID();
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		//Consulta al dao
		respuesta = daoCargaArchivosHistoricosCDASPID.
				obtenerEstatusArchivo(paginador,
						getArchitechBean());
		
		//Validacion de errores
		if("EC00011B".equals(respuesta.getCodigoError())) {
			respuesta.setCodigoError(respuesta.getCodigoError());
			respuesta.setMensajeError(respuesta.getMensajeError());
		} else {
			//Validacion de que existan datos
			if(respuesta.getListaResultado()==null ||
					respuesta.getListaResultado().isEmpty()) {
				info("No hay datos que mostrar");
				throw new BusinessException(Errores.ED00011V,Errores.DESC_ED00011V);
			} else{
				//Si pasa todas las validaciones se devuelve un error exitoso y la lista
				respuesta.setCodigoError(Errores.OK00000V);
				respuesta.setMensajeError(Errores.TIPO_MSJ_INFO);
				respuesta.setListaResultado(respuesta.getListaResultado());
				paginador.calculaPaginas(respuesta.getRegistrosTotales(), HelperDAO.REGISTROS_X_PAGINA.toString());
				respuesta.setPaginador(paginador);
			}
		}
		//Se devuleve el objeto respuesta
		return respuesta;
	}

	/**
	 * Metodo para establecer la variable de instancia al DAO
	 * 
	 * @return La variable de instantacia 
	 * daoCargaArchivosHistoricosCDASPID
	 */
	//Metodo para establecer la variable de instancia al DAO
	public DAOCargaArchivosHistoricosCDASPID getDaoCargaArchivosHistoricosCDASPID() {
		return daoCargaArchivosHistoricosCDASPID;
	}

	/**
	 * Metodo para obtener la variable de instancia al DAO
	 * 
	 * @param daoCargaArchivosHistoricosCDASPID La variable de instancia 
	 * daoCargaArchivosHistoricosCDASPID
	 */
	//Metodo para obtener la variable de instancia al DAO
	public void setDaoCargaArchivosHistoricosCDASPID(
			DAOCargaArchivosHistoricosCDASPID daoCargaArchivosHistoricosCDASPID) {
		this.daoCargaArchivosHistoricosCDASPID = daoCargaArchivosHistoricosCDASPID;
	}


}
