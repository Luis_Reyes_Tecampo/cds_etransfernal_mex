/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHistDetImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 17:25:06 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDetDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchCDAHistDet;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico Detalle
**/
@Remote(BOGenerarArchCDAHistDet.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGenerarArchCDAHistDetImpl extends Architech implements BOGenerarArchCDAHistDet {

   /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 2201050124303624799L;
	
	/**Referencia al servicio dao DAOGenerarArchCDAHistDet*/
	@EJB
	private DAOGenerarArchCDAHistDet dAOGenerarArchCDAHistDet;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;

	
	/**Metodo que sirve para consultar la informacion de la generacion
	* de archivos CDA historico detalle
	* @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	* @param beanPaginador Objeto del tipo @see BeanPaginador
	* @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	* @return BeanResConsGenArchHistDet Objeto del tipo BeanResConsGenArchHistDet
	* @exception BusinessExceptionException de negocio
	*/
	public BeanResConsGenArchHistDet consultaGenArchHistDet(
		   BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet, 
		   BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean)
	       throws BusinessException{
		List<BeanGenArchHistDet> listBeanGenArchHistDet = null;
	   BeanResConsGenArchHistDetDAO beanResConsGenArchHistDetDAO = null;
	   final BeanResConsGenArchHistDet beanResConsGenArchHistDet = new BeanResConsGenArchHistDet();
       beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
       beanResConsGenArchHistDetDAO = dAOGenerarArchCDAHistDet.consultaGenArchHistDet(
    		   beanReqConsGenArchCDAHistDet, beanPaginador, architechSessionBean);
       
       listBeanGenArchHistDet = beanResConsGenArchHistDetDAO.getListBeanGenArchHistDet();
       if(Errores.CODE_SUCCESFULLY.equals(beanResConsGenArchHistDetDAO.getCodError()) && listBeanGenArchHistDet.isEmpty()){
    	   beanResConsGenArchHistDet.setListBeanGenArchHistDet(beanResConsGenArchHistDetDAO.getListBeanGenArchHistDet());
    	   beanResConsGenArchHistDet.setNumRegArchivo(beanResConsGenArchHistDetDAO.getNumRegArchivo());
    	   beanResConsGenArchHistDet.setNumRegEncontrados(beanResConsGenArchHistDetDAO.getNumRegEncontrados());
    	   beanResConsGenArchHistDet.setCodError(Errores.ED00011V);
     	 beanResConsGenArchHistDet.setTipoError(Errores.TIPO_MSJ_INFO);
       }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsGenArchHistDetDAO.getCodError())){
    	   beanResConsGenArchHistDet.setCodError(Errores.OK00000V);
    	   beanResConsGenArchHistDet.setListBeanGenArchHistDet(beanResConsGenArchHistDetDAO.getListBeanGenArchHistDet());
           beanResConsGenArchHistDet.setNumRegArchivo(beanResConsGenArchHistDetDAO.getNumRegArchivo());
           beanResConsGenArchHistDet.setNumRegEncontrados(beanResConsGenArchHistDetDAO.getNumRegEncontrados());
           beanPaginador.calculaPaginas(beanResConsGenArchHistDetDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
           beanResConsGenArchHistDet.setPaginador(beanPaginador);
       }else{
    	   beanResConsGenArchHistDet.setCodError(Errores.EC00011B);
    	   beanResConsGenArchHistDet.setTipoError(Errores.TIPO_MSJ_ERROR);
       }
       return beanResConsGenArchHistDet;
    }
    /**Metodo que sirve para marcar los registros como eliminados
    * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResElimSelecArchCDAHistDet Objeto del tipo BeanResElimSelecArchCDAHistDet
    * @exception BusinessExceptionException de negocio
    */
    public BeanResElimSelecArchCDAHistDet elimSelGenArchCDAHistDet(
    		BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet,
    		ArchitechSessionBean architechSessionBean)
        throws BusinessException{
    	BeanResElimSelecArchCDAHistDetDAO beanResElimSelecArchCDAHistDetDAO = null;
      final BeanResElimSelecArchCDAHistDet beanResElimSelecArchCDAHistDet = new BeanResElimSelecArchCDAHistDet();
      BeanResConsGenArchHistDet beanResConsGenArchHistDet = null;
      List<BeanGenArchHistDet> listBeanGenArchHistDet = new ArrayList<BeanGenArchHistDet>();
      for(BeanGenArchHistDet beanGenArchHistDet:beanReqConsGenArchCDAHistDet.getListBeanGenArchHistDet()){
    	  if(beanGenArchHistDet.getSeleccionado()){
    		  listBeanGenArchHistDet.add(beanGenArchHistDet);
    	  }
      }
      if(listBeanGenArchHistDet.isEmpty()){
    	  beanResConsGenArchHistDet = consultaGenArchHistDet(beanReqConsGenArchCDAHistDet,new BeanPaginador(), architechSessionBean);
	      beanResElimSelecArchCDAHistDet.setListBeanGenArchHistDet(beanResConsGenArchHistDet.getListBeanGenArchHistDet());
	      beanResElimSelecArchCDAHistDet.setPaginador(beanResConsGenArchHistDet.getPaginador());
	      beanResElimSelecArchCDAHistDet.setNumRegArchivo(beanResConsGenArchHistDet.getNumRegArchivo());
	      beanResElimSelecArchCDAHistDet.setNumRegEncontrados(beanResConsGenArchHistDet.getNumRegEncontrados());
    	  beanResElimSelecArchCDAHistDet.setCodError(Errores.ED00026V);
    	  beanResElimSelecArchCDAHistDet.setTipoError(Errores.TIPO_MSJ_ERROR);
      }else{
	      beanReqConsGenArchCDAHistDet.setListBeanGenArchHistDet(listBeanGenArchHistDet);
	      beanResElimSelecArchCDAHistDetDAO = dAOGenerarArchCDAHistDet.elimSelGenArchCDAHistDet(beanReqConsGenArchCDAHistDet,architechSessionBean);
	      beanResElimSelecArchCDAHistDet.setCodError(Errores.OK00000V);
	      beanResElimSelecArchCDAHistDet.setTipoError(Errores.TIPO_MSJ_INFO);
	      if(Errores.CODE_SUCCESFULLY.equals(beanResElimSelecArchCDAHistDetDAO.getCodError())){
	    	  beanResConsGenArchHistDet = consultaGenArchHistDet(beanReqConsGenArchCDAHistDet,new BeanPaginador(), architechSessionBean);
		      beanResElimSelecArchCDAHistDet.setListBeanGenArchHistDet(beanResConsGenArchHistDet.getListBeanGenArchHistDet());
		      beanResElimSelecArchCDAHistDet.setPaginador(beanResConsGenArchHistDet.getPaginador());
		      beanResElimSelecArchCDAHistDet.setNumRegArchivo(beanResConsGenArchHistDet.getNumRegArchivo());
		      beanResElimSelecArchCDAHistDet.setNumRegEncontrados(beanResConsGenArchHistDet.getNumRegEncontrados());
	      }
      }
      return beanResElimSelecArchCDAHistDet;
    }
    
    /**Metodo que permite generar el archivo CDA Historico detalle
     * @param beanReqGenArchCDAHistDet Objeto del tipo @see BeanReqGenArchCDAHistDet
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGenArchCDAHistDet Objeto del tipo BeanResGenArchCDAHistDet
     * @exception BusinessExceptionException de negocio
     */
     public BeanResGenArchCDAHistDet generarArchCDAHistDet(BeanReqGenArchCDAHistDet beanReqGenArchCDAHistDet, 
    		 ArchitechSessionBean architechSessionBean)
         throws BusinessException{
    	 String numRegistros = "0";
        Utilerias utilerias = Utilerias.getUtilerias();
        BeanReqInsertBitacora beanReqInsertBitacora = null;
        final BeanResGenArchCDAHistDet beanResGenArchCDAHistDet = new BeanResGenArchCDAHistDet();
        BeanResGenArchCDAHistDetDAO beanResGenArchCDAHistDetDAO = null;
        beanResGenArchCDAHistDetDAO = dAOGenerarArchCDAHistDet.generarArchCDAHistDet(beanReqGenArchCDAHistDet, architechSessionBean);
        if(Errores.CODE_SUCCESFULLY.equals(beanResGenArchCDAHistDetDAO.getCodError())){
        	beanResGenArchCDAHistDet.setCodError(Errores.OK00000V);
        	beanResGenArchCDAHistDet.setTipoError(Errores.TIPO_MSJ_INFO);
        	if(beanReqGenArchCDAHistDet.getNumRegArchivo()!=null){
        		numRegistros = beanReqGenArchCDAHistDet.getNumRegArchivo().replaceAll(",","");
        	}
        	
	        beanReqInsertBitacora = utilerias.creaBitacora("generarArchCDAHistDet.do", numRegistros,
	        		"", new Date(), 
	        		"PENDIENTE", "GENARCHHIS", "TRAN_SPEI_CTG_CDA", "ID_PETICION", 
	        		Errores.OK00000V, "TRANSACCION EXITOSA", "UPDATE",beanReqGenArchCDAHistDet.getIdPeticion());
	        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
        }else{
        	beanResGenArchCDAHistDet.setCodError(Errores.EC00011B);
        	beanResGenArchCDAHistDet.setTipoError(Errores.TIPO_MSJ_ERROR);
        	beanReqInsertBitacora = utilerias.creaBitacora("generarArchCDAHistDet.do", "0", 
        			"",new Date(), 
	        		"PENDIENTE", "GENARCHHIS", "TRAN_SPEI_CTG_CDA", "ID_PETICION", 
	        		Errores.EC00011B, "No hay comunicaci\u00F3n serv.", "UPDATE","");
	        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
        }
        return beanResGenArchCDAHistDet;
     }
     
    /**
     * Metodo get que sirve para obtener la referencia al servicio DAOGenerarArchCDAHistDet
     * @return dAOGenerarArchCDAHistDet objeto del tipo DAOGenerarArchCDAHistDet
     */
	public DAOGenerarArchCDAHistDet getDAOGenerarArchCDAHistDet() {
		return dAOGenerarArchCDAHistDet;
	}
	/**
	 * Metodo set que sirve para modificar la referencia al servicio DAOGenerarArchCDAHistDet
	 * @param dAOGenerarArchCDAHistDet Objeto del tipo DAOGenerarArchCDAHistDet
	 */
	public void setDAOGenerarArchCDAHistDet(
			DAOGenerarArchCDAHistDet dAOGenerarArchCDAHistDet) {
		this.dAOGenerarArchCDAHistDet = dAOGenerarArchCDAHistDet;
	}
    /**
     * Metodo get que sirve para obtener la referencia al servicio DAOBitacoraAdmon
     * @return daoBitacora objeto del tipo DAOBitacoraAdmon
     */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}
	/**
	 * Metodo set que sirve para modificar la referencia al servicio DAOBitacoraAdmon
	 * @param daoBitacora Objeto del tipo DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
     
     



}
