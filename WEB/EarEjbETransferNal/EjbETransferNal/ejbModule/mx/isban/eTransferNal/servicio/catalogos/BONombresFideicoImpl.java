/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BONombresFideicoImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 12:06:46 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;
import mx.isban.eTransferNal.beans.catalogos.BeanNombresFideico;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAONombresFideico;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasNombresFideico;


/**
 * Class BONombresFideicoImpl.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de identificacion de nombres de fideicomiso.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BONombresFideicoImpl extends Architech implements BONombresFideico {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1862262465310973407L;

	/** La variable que contiene informacion con respecto a: dao nombres fideico. */
	@EJB
	private DAONombresFideico daoNombresFideico;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private UtileriasNombresFideico utilerias = UtileriasNombresFideico.getInstancia();
	
	/** La constante UPDATE. */
	private static final String UPDATE = "NOMBRE";
	/** La constante INSERT. */
	private static final String INSERT = "NOMBRE";
	
	/** La constante SELECT. */
	private static final String SELECT = "NOMBRE";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "NA";
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_NOMBRE_FIDEICO";
	
	
	
	/**
	 * Consultar.
	 *
	 * @param beanNombreFideico the beanNombre --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador the beanPaginador --> Objeto para realizar el paginado
	 * @param architechSessionBean the session
	 * @return the beanNombresFideico --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanNombresFideico consultar(BeanNombreFideico beanNombre, BeanPaginador beanPaginador,
			ArchitechSessionBean session) throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanNombresFideico response = daoNombresFideico.consultar(beanNombre, beanPaginador, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getNombres().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Agregar.
	 *
	 * @param beanNombreFideico the beanNombre --> Objeto con los datos del nuevo registro
	 * @param architechSessionBean the session
	 * @return the beanResBase --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase agregar(BeanNombreFideico beanNombre, ArchitechSessionBean session) throws BusinessException {
		boolean operacion = false;
		/** Ejecucion de la peticion al DAO **/
		BeanResBase response = daoNombresFideico.agregar(beanNombre, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanNombre);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_INSERT, "agregarNombreFideicomiso.do", operacion, session,dtoNuevo,"",INSERT);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Editar.
	 *
	 * @param BeanNombreFideico the beanNombre --> Objeto con los datos del nuevo registro
	 * @param BeanNombreFideico the datoAnterior --> Objeto con los datos del registro a editar
	 * @param architechSessionBean the session --> Resultado de la operacion
	 * @return the beanResBase
	 * @throws BusinessException the business exception--> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase editar(BeanNombreFideico beanNombre, BeanNombreFideico datoAnterior, ArchitechSessionBean session) throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		String dtoAnt = obtenerDatos(datoAnterior);
		/** Ejecucion de la peticion al DAO **/
		response = daoNombresFideico.editar(beanNombre, datoAnterior, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanNombre);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_UPDATE, "editarNombreFideicomiso.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Eliminar.
	 *
	 * @param beanNombresFideico the beanNombres --> Registros a eliminar
	 * @param architechSessionBean the session
	 * @return the beanEliminarResponse --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanNombresFideico beanNombres, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanNombreFideico nombre : beanNombres.getNombres()){
			if (nombre.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoNombresFideico.eliminar(nombre, session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(nombre.getNombre());
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(nombre.getNombre());
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDatos(nombre);
	        	generaPistaAuditoras(ConstantesCatalogos.TYPE_DELETE, "eliminarNombreFideicomiso.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Exportar todos.
	 *
	 * @param beanNombreFideico the beanNombre --> Objeto de filtrado de registros
	 * @param totalRegistros--> Parametro con el total de registros actual
	 * @param architechSessionBean the session
	 * @return the beanResBase --> Objeto de respuesta de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase exportarTodo(BeanNombreFideico beanNombre, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String filtroWhere = utilerias.getFiltro(beanNombre, "WHERE");
		String query = UtileriasNombresFideico.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasNombresFideico.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasNombresFideico.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("NOMBRES_FIDEICO");
		exportarRequest.setNombreRpt("CATALOGOS_NOMBRES_FIDEICOMISO_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_EXPORT, "exportarTodosNombreFideicomiso.do", operacion, session,"","", DATO_FIJO);
		/** Retorno de la respuesta del BO**/
		return response;
	}
	
	/**
	 * generaPistaAuditoras
	 * 
	 * 
	 * Procedimiento para el almacenamiento de las Pistas Auditoras
	 * en la BD.
	 *
	 *
	 * @param operacion            El objeto: operacion --> Parametro del tipo de operacion que se realiza
	 * @param urlController            El objeto: url controller --> Parametro con el .do de la accion
	 * @param resOp            El objeto: res op --> Parametro de respuesta de la operacion
	 * @param sesion            El objeto: sesion--> Parametro de session
	 * @param dtoNuevo El objeto: dto nuevo --> Parametro de valor registro nuevo
	 * @param dtoAnterior El objeto: dto anterior --> Parametro del valor registro anterior
	 * @param dtoModificado El objeto: dto modificado --> Parametro del dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		/** Valdiador de operacion **/
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus(ConstantesCatalogos.OK);
		} else {
			beanPista.setEstatus(ConstantesCatalogos.NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

	/**
	 * Obtener datos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 *
	 * @param ant El objeto: ant --> Objeto anterior
	 * @return Objeto string --> Respuesta devuelta
	 */
	private String obtenerDatos(BeanNombreFideico ant) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es UDPDATE **/
			dtoAnt ="NOMBRE=".concat(ant.getNombre());
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
