/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpFunciones;

/**
 * Clase del tipo BO que se encarga de realiza la exportacion de las recepciones de operaciones
 * de la cuenta principal y alterna
 *
 * @author FSW-Vector
 * @since 13/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperacionExportarTodoImpl  extends Architech  implements BORecepOperacionExportarTodo{
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 6269871327373936655L;

	/** La constante util. */
	public static final UtileriasRecepcionOpFunciones utilExportar = UtileriasRecepcionOpFunciones.getInstance();
	
	/** Referencia al dao de perfilamiento. */
	@EJB
	private DAOExportar daoExportar;

	/**
	 * Exportar total de registros.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param recepOperaAdmonSaldos El objeto: recep opera admon saldos
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param historico El objeto: historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec exportarTodo(ArchitechSessionBean sessionBean,BeanResTranSpeiRec recepOperaAdmonSaldos,
			BeanReqTranSpeiRec beanReqTranSpei,boolean historico,BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		BeanResTranSpeiRec beanRes = new BeanResTranSpeiRec();
		//inicializa bean de error
		beanRes.setBeanError(new BeanResBase());
		BeanExportarTodo beanExp = new BeanExportarTodo();
		beanExp.setColumnasReporte(ConstantesRecepOperacionApartada.COLUMNAS_EXPORTAR_TODO);
		
		String query = utilExportar.getQueryConsultaExportarTodo(historico,beanReqTranSpei, beanTranSpeiRecOper);
		
		beanExp.setConsultaExportar(query);
		beanExp.setModulo(buscarModulo(beanReqTranSpei.getBeanComun().getNombrePantalla()));
		beanExp.setSubModulo(subModulo(beanReqTranSpei.getBeanComun().getNombrePantalla()) );
		beanExp.setNombreRpt(nombreReporte(beanReqTranSpei.getBeanComun().getNombrePantalla()));
		beanExp.setTotalReg(String.valueOf(recepOperaAdmonSaldos.getBeanComun().getTotalReg()));
		beanExp.setEstatus("PE");

		BeanResBase result = daoExportar.exportarTodos(beanExp, sessionBean,beanTranSpeiRecOper);
		if (Errores.EC00011B.equals(result.getCodError())) {
			beanRes.getBeanError().setCodError(result.getCodError());
			beanRes.getBeanError().setMsgError(result.getMsgError());
			
			throw new BusinessException(result.getCodError(), result.getMsgError());

		}
	
		
		beanRes.getBeanError().setCodError(result.getCodError());
		beanRes.getBeanError().setMsgError(result.getMsgError());
	

		return beanRes;
	}
	
	/**
	 * Buscar modulo.
	 *
	 * @param menuOpcion El objeto: menu opcion
	 * @return Objeto string
	 */
	private String buscarModulo(String menuOpcion) {
		String modulo ="AdmonSdos";
		/**funcion para mostrar la pantalla segun de donde venga la peticion**/
		if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_DIA.equals(menuOpcion) ||
				ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_HTO.equals(menuOpcion)) {
			modulo ="ModuloSPEI";
		}
		return modulo;
	}
	
	/**
	 * Sub modulo.
	 *
	 * @param menuOpcion El objeto: menu opcion
	 * @return Objeto string
	 */
	private String subModulo(String menuOpcion) {
		String modulo ="";
		/**funcion para mostrar la pantalla segun de donde venga la peticion**/
		if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA.equals(menuOpcion)|| ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_DIA.equals(menuOpcion)) {
			modulo = "Recepci\u00f3n de Operaciones";	
		}else {
			modulo = "Recepci\u00f3n de Operaciones Hist\u00f3ricas";	
		}
		
		return modulo;
	}
	
	/**
	 * Nombre reporte.
	 *
	 * @param menuOpcion El objeto: menu opcion
	 * @return Objeto string
	 */
	private String nombreReporte(String menuOpcion) {
		String modulo ="";
		/**funcion para mostrar la pantalla segun de donde venga la peticion**/
		if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA.equals(menuOpcion)){
			modulo = "RecepcionOperacionesCtaAlterna";	
		}else if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO.equals(menuOpcion)){
			modulo = "RecepcionOperacionesHtoCtaAlterna";	
		}else if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_DIA.equals(menuOpcion)){
			modulo = "RecepcionOperacionesCtaPrincipal";	
		}else if(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_HTO.equals(menuOpcion)){
			modulo = "RecepcionOperacionesHtoCtaPrincipal";	
		}
		return modulo;
	}

	
	
}