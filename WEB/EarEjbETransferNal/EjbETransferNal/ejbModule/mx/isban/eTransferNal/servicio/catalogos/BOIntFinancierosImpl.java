/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOIntFinancierosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dici 20 09:55:49 CST 2016 jjbeltran	Vector 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinanciero;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsTiposIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancieros;
import mx.isban.eTransferNal.beans.catalogos.BeanResTiposIntFinancieros;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimIntFin;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOIntFinancieros;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Class BOIntFinancierosImpl.
 */
@Remote(BOIntFinancieros.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOIntFinancierosImpl extends Architech implements BOIntFinancieros {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -3860331937644548220L;

	/** La constante CVE_INTERME. */
	private static final String CVE_INTERME = "CVE_INTERME";

	/** La constante TRAN_INTERME. */
	private static final String TRAN_INTERME = "COMU_INTERME_FIN";
	
	/** La constante OK. */
	private static final String OK = "OK";
	
	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "COD_ERROR";
	
	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante COMU_INTERME_FIN. */
	private static final String COMU_INTERME_FIN = "COMU_INTERME_FIN";
	
	/** La constante CONSULTA_EXP_CAT_FIN. */
	private static final String CONSULTA_EXP_TODO_CAT_FIN = " SELECT " 
	+"CVE_INTERME  || ',' || TIPO_INTERME || ',' || NUM_CECOBAN || ',' || NOMBRE_CORTO || ',' || NOMBRE_LARGO || ',' || NUM_PERSONA || ',' || "  
	+"FCH_ALTA || ',' ||  FCH_BAJA || ',' || FCH_ULT_MODIF || ',' || USUARIO_MODIF|| ',' || USUARIO_REGISTRO || ',' || STATUS || ',' || NUM_BANXICO || ',' || NUM_INDEVAL || ',' || "
	+"ID_INT_INDEVAL || ',' ||FOL_INT_INDEVAL || ',' ||CVE_SWIFT_COR || ',' || AMBOS || ',' || SPID || ',' || SPEI "
	+"FROM ( SELECT TMP.*, ROWNUM AS RN FROM ( SELECT COALESCE(C.CVE_INTERME, T.CVE_INTERME) AS CVE_INTERME, "
	+"CASE WHEN T.CVE_INTERME = C.CVE_INTERME THEN 'AMBAS' END AMBOS, CASE WHEN T.CVE_INTERME IS NULL THEN 'N' ELSE 'S' END SPID, "
	+"CASE WHEN C.CVE_INTERME IS NULL THEN 'N' ELSE 'S' END SPEI, C.TIPO_INTERME,C.NUM_CECOBAN, C.NOMBRE_CORTO, C.NOMBRE_LARGO, C.NUM_PERSONA, " 
	+"TO_CHAR(C.FCH_ALTA,'DD-MM-YYYY') FCH_ALTA, TO_CHAR(C.FCH_BAJA,'DD-MM-YYYY') FCH_BAJA, TO_CHAR(C.FCH_ULT_MODIF,'DD-MM-YYYY') FCH_ULT_MODIF, C.USUARIO_MODIF, C.USUARIO_REGISTRO, "
	+"C.STATUS, C.NUM_BANXICO, C.NUM_INDEVAL, C.ID_INT_INDEVAL, C.FOL_INT_INDEVAL, C.CVE_SWIFT_COR "
	+" FROM COMU_INTERME_FIN C  FULL OUTER JOIN  Tran_SPID_interme T ON C.CVE_INTERME = T.CVE_INTERME  )TMP) TMP";
	
	/** La constante TITULO_EXP_CAT_FIN. */
	private static final String TITULO_EXP_TODO_CAT_FIN = "Cve Intermediario, Tipo Interme, Num Cecoban, Nombre Corto, Nombre Largo, Num Persona, Fecha Alta, Fecha Baja, Fecha Ultima Modif," +
                                                          "Usuario Modif, Usuario Registro, Status, Num. Banxico, Num Indeval, Id Int Indeval, Fol Int Ideval, Cve Swif Cory,Opera AMBAS, Opera SPEI, Opera SPID ";

	
	/** La variable que contiene informacion con respecto a: dao int financieros. */
	@EJB
	private DAOIntFinancieros daoIntFinancieros;
	
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitAdministrativa daoBitacora;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;		
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;		
	
		
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros#consultaTiposInterviniente(mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResTiposIntFinancieros consultaTiposInterviniente(BeanResIntFinancieros beanReqInsertInstFin,
			ArchitechSessionBean architechSessionBean) {
		  List<String> listTposInterviniente = null;
		  BeanResTiposIntFinancieros beanResTiposIntFin = new BeanResTiposIntFinancieros();
		  BeanResConsTiposIntFinDAO beanResConsTiposIntFinDAO = null;
		  BeanReqInsertarIntFin beanReqInsert = new BeanReqInsertarIntFin();
		  beanReqInsert.setCveInterme(beanReqInsertInstFin.getCveInterme());
		  beanReqInsert.setBandera(beanReqInsertInstFin.getBandera());
		  if (null != beanReqInsertInstFin.getListBeanIntFinanciero() && !beanReqInsertInstFin.getListBeanIntFinanciero().isEmpty()) {
			  for (BeanIntFinanciero beanIntFinanciero : beanReqInsertInstFin.getListBeanIntFinanciero()) {
					if (beanIntFinanciero.isSeleccionado()) {
						beanReqInsert.setCveInterme(beanIntFinanciero.getCveInterme());
						break;
				  }
			  }
		  }
		  beanResConsTiposIntFinDAO = daoIntFinancieros.consultaTiposIntFin(beanReqInsert, architechSessionBean);
		  listTposInterviniente = beanResConsTiposIntFinDAO.getListTiposIntFin();
	      if(Errores.CODE_SUCCESFULLY.equals(beanResConsTiposIntFinDAO.getCodError()) && listTposInterviniente.isEmpty()){
	    	  beanResTiposIntFin.setCodError(Errores.ED00011V);
	    	  beanResTiposIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
	    	  beanResTiposIntFin.setListTiposInterviniente(beanResConsTiposIntFinDAO.getListTiposIntFin());
	      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsTiposIntFinDAO.getCodError())){
	    	  beanResTiposIntFin.setListTiposInterviniente(beanResConsTiposIntFinDAO.getListTiposIntFin());
	    	  if (null != beanResConsTiposIntFinDAO.getCveInterme() && !StringUtils.EMPTY.equals(beanResConsTiposIntFinDAO.getCveInterme())) {
	    		  beanResTiposIntFin = (BeanResTiposIntFinancieros) mapeaBean(beanResConsTiposIntFinDAO, beanResTiposIntFin);
		    	  beanResTiposIntFin.setCodError(Errores.OK00000V);
	    	  } else if (beanResTiposIntFin.getTotalReg() == 0) {
	    		  beanResTiposIntFin = (BeanResTiposIntFinancieros) mapeaBean(beanReqInsertInstFin, beanResTiposIntFin);
	    		  beanResTiposIntFin.setCodError(Errores.ED00011V);
		    	  beanResTiposIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
	    	  }
	      }else if(Errores.ED00011V.equals(beanResConsTiposIntFinDAO.getCodError())){
	    	  beanResTiposIntFin.setCodError(Errores.ED00011V);
	    	  beanResTiposIntFin.setTipoError(Errores.TIPO_MSJ_INFO); 
	      }else{
	    	  beanResTiposIntFin.setCodError(Errores.EC00011B);
	    	  beanResTiposIntFin.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }
	      return beanResTiposIntFin;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros#buscarIntFinancierosFiltro(mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResTiposIntFinancieros buscarIntFinancierosFiltro(BeanConsReqIntFin beanConsReqIntFin,
			ArchitechSessionBean architechSessionBean) {
		  BeanPaginador paginador = null;
		  List<BeanIntFinanciero> listBeanIntFinanciero = null;
		  BeanResTiposIntFinancieros beanResIntFin = new BeanResTiposIntFinancieros();
		  BeanResConsIntFinDAO beanResConsInstDAO = null;
	      paginador = beanConsReqIntFin.getPaginador();
	      if(paginador == null){
	    	  paginador = new BeanPaginador();
	    	  beanConsReqIntFin.setPaginador(paginador);
	      }
	      BeanResConsTiposIntFinDAO beanResConsTiposIntFinDAO = daoIntFinancieros.
	    		  consultaTiposIntFin(new BeanReqInsertarIntFin(), architechSessionBean);
	      if(Errores.CODE_SUCCESFULLY.equals(beanResConsTiposIntFinDAO.getCodError()) &&
	    		  !beanResConsTiposIntFinDAO.getListTiposIntFin().isEmpty()){
	    	  beanResIntFin.setListTiposInterviniente(beanResConsTiposIntFinDAO.getListTiposIntFin());
	      }
	      paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
	      beanResConsInstDAO = daoIntFinancieros.buscarIntFinancieros(beanConsReqIntFin, architechSessionBean);
	      listBeanIntFinanciero = beanResConsInstDAO.getListBeanIntFinanciero();
	      beanResIntFin = (BeanResTiposIntFinancieros) mapeaBean(beanConsReqIntFin, beanResIntFin);
	      beanResIntFin.setCodError(Errores.ED00011V);
	      beanResIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
	      if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError()) && listBeanIntFinanciero.isEmpty()){
	    	  beanResIntFin.setCodError(Errores.ED00011V);
	    	  beanResIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
	    	  beanResIntFin.setListBeanIntFinanciero(beanResConsInstDAO.getListBeanIntFinanciero());
	      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsInstDAO.getCodError())){
	    	  beanResIntFin.setListBeanIntFinanciero(beanResConsInstDAO.getListBeanIntFinanciero());
	    	  beanResIntFin.setTotalReg(beanResConsInstDAO.getTotalReg());
	          paginador.calculaPaginas(beanResConsInstDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
	          beanResIntFin.setBeanPaginador(paginador);
	          beanResIntFin.setCodError(Errores.OK00000V);
	      }else if(Errores.ED00011V.equals(beanResConsInstDAO.getCodError())){
	    	  beanResIntFin.setCodError(Errores.ED00011V);
	    	  beanResIntFin.setTipoError(Errores.TIPO_MSJ_INFO); 
	      }else{
	    	  beanResIntFin.setCodError(Errores.EC00011B);
	    	  beanResIntFin.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }
	      return beanResIntFin;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros#elimCatIntFinancieros(mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResElimIntFin elimCatIntFinancieros(BeanReqEliminarIntFin beanReqEliminarIntFin,
			ArchitechSessionBean architechSessionBean) {
    StringBuilder exito = new StringBuilder(), error = new StringBuilder();
    BeanResElimIntFin beanResElimIntFin = new BeanResElimIntFin();
    BeanResElimIntFinDAO beanResElimIntFinDAO = null;
    BeanResIntFinancieros beanResIntFinancieros = null;
    List<BeanIntFinanciero> listBeanIntFinanciero = new ArrayList<BeanIntFinanciero>();
    if(beanReqEliminarIntFin.getListBeanIntFinanciero() != null ){
	    for(BeanIntFinanciero beanIntFinanciero : beanReqEliminarIntFin.getListBeanIntFinanciero()){
	  	  if(beanIntFinanciero.isSeleccionado()){
	  		listBeanIntFinanciero.add(beanIntFinanciero);
	   	  }
	   }
    }
    if(listBeanIntFinanciero.isEmpty()){
    	BeanConsReqIntFin beanConsReqIntFin = new BeanConsReqIntFin();
    	beanConsReqIntFin = (BeanConsReqIntFin) mapeaBean(beanReqEliminarIntFin, beanConsReqIntFin);
    	beanResIntFinancieros = buscarIntFinancierosFiltro(beanConsReqIntFin, architechSessionBean);
    	beanResElimIntFin.setListBeanIntFinanciero(beanResIntFinancieros.getListBeanIntFinanciero());
    	beanResElimIntFin.setBeanPaginador(beanResIntFinancieros.getBeanPaginador());
    	beanResElimIntFin.setCodError(Errores.ED00026V);
    	beanResElimIntFin.setTipoError(Errores.TIPO_MSJ_ALERT);
    } else {
    	beanReqEliminarIntFin.setListBeanIntFinanciero(listBeanIntFinanciero);
        for(BeanIntFinanciero beanIntFinanciero : listBeanIntFinanciero){
        	beanResElimIntFinDAO = daoIntFinancieros.elimIntFinanciero(beanIntFinanciero, architechSessionBean);
        	if(Errores.CODE_SUCCESFULLY.equals(beanResElimIntFinDAO.getCodError())){
        		exito.append(beanIntFinanciero.getCveInterme());
        		exito.append(',');
        		beanResElimIntFin.setCodError(Errores.OK00000V);
        		beanResElimIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
        		BeanConsReqIntFin beanConsReqIntFin = new BeanConsReqIntFin();
        		beanConsReqIntFin = (BeanConsReqIntFin) mapeaBean(beanReqEliminarIntFin, beanConsReqIntFin);
        		beanResIntFinancieros = buscarIntFinancierosFiltro(beanConsReqIntFin, architechSessionBean);
        		beanResElimIntFin = (BeanResElimIntFin) mapeaBean(beanResIntFinancieros, beanResElimIntFin);
        		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
            	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
            			utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_INTERME, "DELETE", "elimCatIntFinancieros.do", "", new Date().toString(), 
            					"FCH_BAJA", CVE_INTERME, "", architechSessionBean);
            	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
        	} else {
        		error.append(beanIntFinanciero.getCveInterme());
        		error.append(',');
        		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
            	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
            			utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_INTERME, "DELETE", "elimCatIntFinancieros.do", "", new Date().toString(), 
            					"FCH_BAJA", CVE_INTERME, "", architechSessionBean);
            	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
        	}
        }
        beanResElimIntFin.setEliminadosCorrectos(exito.toString());
        beanResElimIntFin.setEliminadosErroneos(error.toString());
     	}
    return beanResElimIntFin;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros#guardaIntFin(mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResTiposIntFinancieros guardaIntFin(BeanReqInsertarIntFin beanReqInsertIntFin,
			ArchitechSessionBean architechSessionBean) {
		BeanResIntFinancieros beanResIntFinancieros = null;
		BeanResTiposIntFinancieros beanResGuardaIntFin = new BeanResTiposIntFinancieros();
		BeanResBase beanResGuardaIntFinDAO = null;
		BeanConsReqIntFin beanConsReqIntFin = null;
		beanResGuardaIntFinDAO = daoIntFinancieros.guardaIntFinanciero(beanReqInsertIntFin, 
				architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResGuardaIntFinDAO.getCodError())){
			beanResGuardaIntFin.setCodError(Errores.OK00000V);
			beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
			UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
        	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_INTERME, "INSERT", "altaCatIntFinancieros.do", "", "", 
        					CVE_INTERME, CVE_INTERME, "", architechSessionBean);
        	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
        	beanConsReqIntFin = new BeanConsReqIntFin();
            beanConsReqIntFin.setCveInterme(beanReqInsertIntFin.getCveInterme());
            beanResIntFinancieros = buscarIntFinancierosFiltro(beanConsReqIntFin, architechSessionBean);
            beanResGuardaIntFin.setListBeanIntFinanciero(beanResIntFinancieros.getListBeanIntFinanciero());
            beanResGuardaIntFin = (BeanResTiposIntFinancieros) mapeaBean(beanResIntFinancieros, beanResGuardaIntFin);
            beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_ALERT); 
            return beanResGuardaIntFin;
	    }else{
	    	beanResGuardaIntFin.setCodError(Errores.EC00011B);
	    	beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_ERROR);
	    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
        	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_INTERME, "INSERT", "altaCatIntFinancieros.do", "", "", 
        					CVE_INTERME, CVE_INTERME, "", architechSessionBean);
        	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
        	
        	BeanResConsTiposIntFinDAO beanResConsTiposIntFinDAO = daoIntFinancieros.consultaTiposIntFin(new BeanReqInsertarIntFin(), architechSessionBean);
        	beanResGuardaIntFin.setListTiposInterviniente(beanResConsTiposIntFinDAO.getListTiposIntFin());
            beanResGuardaIntFin.setCodError(beanResGuardaIntFinDAO.getCodError());
            beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_ALERT);
            
    		return beanResGuardaIntFin;
	    }
	}	

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros#actualizaIntFinanciero(mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResIntFinancieros actualizaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin,
			ArchitechSessionBean architechSessionBean) {
		BeanResIntFinancieros beanResIntFinancieros = null;
		BeanResGuardaIntFin beanResGuardaIntFin = new BeanResGuardaIntFin();
		BeanResBase beanResGuardaIntFinDAO = null;
		beanResGuardaIntFinDAO = daoIntFinancieros.actualizaIntFinanciero(beanReqInsertIntFin, 
				architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResGuardaIntFinDAO.getCodError())){
			beanResGuardaIntFin.setCodError(Errores.OK00000V);
			beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_INFO);
			UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
        	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_INTERME, "UPDATE", "editarIntFinancieros.do", "", "", 
        					CVE_INTERME, CVE_INTERME, "", architechSessionBean);
        	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
	    }else{
	    	beanResGuardaIntFin.setCodError(Errores.EC00011B);
	    	beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_ERROR);
	    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
        	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_INTERME, "UPDATE", "editarIntFinancieros.do", "", "", 
        					CVE_INTERME, CVE_INTERME, "", architechSessionBean);
        	daoBitacora.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
	    }
		BeanConsReqIntFin beanConsReqIntFin = new BeanConsReqIntFin();
        beanConsReqIntFin.setCveInterme(beanReqInsertIntFin.getCveInterme()); 
        beanResIntFinancieros = buscarIntFinancierosFiltro(beanConsReqIntFin, architechSessionBean);
        beanResGuardaIntFin.setListBeanIntFinanciero(beanResIntFinancieros.getListBeanIntFinanciero());
        beanResGuardaIntFin.setCodError(beanResGuardaIntFinDAO.getCodError());
        beanResGuardaIntFin.setTipoError(Errores.TIPO_MSJ_ALERT);
        
		return beanResIntFinancieros;
	}
	
	/**
	 * Mapea bean.
	 *
	 * @param origen El objeto: origen
	 * @param destino El objeto: destino
	 * @return Objeto object
	 */
	private Object mapeaBean(Object origen, Object destino){
		 try {
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.copyProperties(destino, origen);
		} catch (IllegalAccessException e) {
			showException(e);
		} catch (InvocationTargetException e) {
			showException(e);
		}
		return destino;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad daoBitacora.
	 *
	 * @return daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public DAOBitAdministrativa getDaoBitacora() {
		return daoBitacora;
	}


	/**
	 * Metodo que modifica el valor de la propiedad daoBitacora.
	 *
	 * @param daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public void DAOBitAdministrativa(DAOBitAdministrativa daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
   
	/**
	 * @param session origen El objeto: session
	 * @param beanConsReqIntFin destino El objeto: beanConsReqIntFin
	 * @param totalRegistros El objeto: totalRegistros
	 * @return Objeto object
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, BeanConsReqIntFin beanConsReqIntFin, int totalRegistros) 
			throws BusinessException {
		//variables de operacion
		boolean operacion = true;
		BeanResBase resp = new BeanResBase();
		//objeto a utilizar para la respuesta
		BeanExportarTodo  expRequest = new BeanExportarTodo();
	    //seteo de la informacion
		expRequest.setConsultaExportar(CONSULTA_EXP_TODO_CAT_FIN);	
		expRequest.setColumnasReporte(TITULO_EXP_TODO_CAT_FIN);
		expRequest.setNombreRpt("Catalogo de Intermediarios");
		expRequest.setEstatus("PE");
		expRequest.setModulo("CATALOGOS");
		expRequest.setSubModulo("Cat. Intermediarios");
		expRequest.setTotalReg(totalRegistros + "");
		BeanResBase resExportarTodo = daoExportar.exportarTodo(expRequest, session);
		
		//se realiza la validacion del codigo
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())){
			operacion = false;
		}
		//seteo de la informacion de respuesta
		resp.setCodError(resExportarTodo.getCodError());
		resp.setMsgError(resExportarTodo.getMsgError());
		resp.setTipoError(resExportarTodo.getTipoError());
		//Se gnera Pista
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodoIntFinancieros.do", operacion, session,"","", DATO_FIJO);
		
		//retorno de la respuesta
		return resp;
	}
   
		
	/**
	 * Generar pistas de auditoria
	 * @param operacion El objeto: operacion
	 * @param urlController El objeto: urlController
	 * @param resOp respuesta
	 * @param sesion sesion que se obtiene
	 * @param dtoNuevo dato nuevo
	 * @param dtoAnterior dato anterior
	 * @param dtoModificado El objeto: dtoModificado
	 */
	private void generaPistaAuditoras (String operacion, String urlController,boolean resOp, ArchitechSessionBean sesion, String dtoNuevo,String dtoAnterior,String dtoModificado){
		//objeto de la para informar datos a la tabla de pistas de auditoria
		BeanPistaAuditora pistaAuditoria = new BeanPistaAuditora();
		
		//seteo de la informacion a informar
		pistaAuditoria.setNomTabla(COMU_INTERME_FIN);
		pistaAuditoria.setOperacion(operacion);
		pistaAuditoria.setUrlController(urlController);
		pistaAuditoria.setDatoFijo(DATO_FIJO);
		pistaAuditoria.setDatoModi(dtoModificado);
		pistaAuditoria.setDtoNuevo(dtoNuevo);
		pistaAuditoria.setDtoAnterior(dtoAnterior);
		
		//validar respuesta
		if (resOp){
			pistaAuditoria.setEstatus(OK);
		}else{
			pistaAuditoria.setEstatus(NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(pistaAuditoria, sesion);
	}
}
