/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMantenimientoTemplatesExtImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Aprl 8 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.servicio.capturasManuales;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAltaEditTemplateDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOMantenimientoTemplatesExt;

/**
 * 
 * Implementacion Service BO  que actua como firma de la capa de negocios de servicios externos
 *
 */
//Implementacion Service BO  que actua como firma de la capa de negocios de servicios externos
@Remote(BOMantenimientoTemplatesExt.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMantenimientoTemplatesExtImpl extends Architech implements BOMantenimientoTemplatesExt {
	
	//Propiedad del tipo long que almacena el valor de serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 2165298116937517142L;
	
	//Componente de inyeccion del dao de mantenimineto de templates
	/**
	 * Componente de inyeccion del dao de mantenimineto de templates
	 */
	@EJB
	private transient DAOMantenimientoTemplatesExt daoMantenimientoExt;
	
	//Metodo para obtener lista de layouts
	@Override
	public BeanResPrincipalTemplate obtenerListaLayouts(
			BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException {
		
		//Creacion de objetos para consulta
		BeanResPrincipalTemplate beanViewRespuesta = new BeanResPrincipalTemplate();
		//se obtienen los layout
		BeanResAltaEditTemplateDAO daoRespuesta = daoMantenimientoExt.obtenerLayouts(req, session);
		
		//Inicializar tipo de mensaje
		beanViewRespuesta.setCodError(Errores.TIPO_MSJ_INFO);
		
		//Validacion de mensajes de error del dao
		if ((Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError()) && daoRespuesta.getListaLayouts().isEmpty()) || Errores.ED00011V.equals(daoRespuesta.getCodError())) {
			beanViewRespuesta.setCodError(Errores.ED00011V);
		} else if (Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError())) {
			beanViewRespuesta.setListaLayouts(daoRespuesta.getListaLayouts());
			//Se devuelve codigo de error exitoso
			beanViewRespuesta.setCodError(Errores.OK00000V);
		} else {
			throw new BusinessException(daoRespuesta.getCodError(), daoRespuesta.getMsgError());
			//Se devuelve codigo de error EC00011B
		}

		return beanViewRespuesta;
	}

	//Metodo para obtener la lista de usuarios
	@Override
	public BeanResPrincipalTemplate obtenerListaUsuarios(
			BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException {
		
		//Creacion de objetos para consulta
		BeanResPrincipalTemplate beanViewRespuesta = new BeanResPrincipalTemplate();
		BeanResAltaEditTemplateDAO daoRespuesta = daoMantenimientoExt.obtenerUsuarios(req, session);
		
		//Inicializar tipo de mensaje
		beanViewRespuesta.setCodError(Errores.TIPO_MSJ_INFO);
		
		//Validacion de mensajes de error del dao
		if (Errores.ED00011V.equals(daoRespuesta.getCodError()) || (Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError()) && daoRespuesta.getListaUsuarios().isEmpty())) {
			beanViewRespuesta.setCodError(Errores.ED00011V);
		} else if (Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError())) {
			beanViewRespuesta.setListaUsuarios(daoRespuesta.getListaUsuarios());
			beanViewRespuesta.setCodError(Errores.OK00000V);
			//Se devuelve codigo de error exitoso
		} else {
			throw new BusinessException(daoRespuesta.getCodError(), daoRespuesta.getMsgError());
		}

		return beanViewRespuesta;
	}

	//Metodo para obtener los campos del layout
	@Override
	public BeanResPrincipalTemplate obtenerCamposLayout(
			BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException {
		
		//Creacion de objetos para consulta
		BeanResPrincipalTemplate beanViewRespuesta = new BeanResPrincipalTemplate();
		BeanResAltaEditTemplateDAO daoRespuesta = daoMantenimientoExt.obtenerCamposLayout(req, session);

		//Inicializar tipo de mensaje
		beanViewRespuesta.setCodError(Errores.TIPO_MSJ_INFO);
		
		//Validacion de mensajes de error del dao
		if ((Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError()) && daoRespuesta.getListaCamposMaster().isEmpty()) || Errores.ED00011V.equals(daoRespuesta.getCodError())) {
			beanViewRespuesta.setCodError(Errores.ED00011V);
		} else if (Errores.CODE_SUCCESFULLY.equals(daoRespuesta.getCodError())) {
			beanViewRespuesta.setListaCamposMaster(daoRespuesta.getListaCamposMaster());
			beanViewRespuesta.setCodError(Errores.OK00000V);
			//Se devuelve codigo de error exitoso
		} else {
			throw new BusinessException(daoRespuesta.getCodError(), daoRespuesta.getMsgError());
			//Se devuelve codigo de error EC00011B	
		}

		return beanViewRespuesta;
	}


}
