package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancierosHist;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOIntFinancierosHis;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasIntFinancierosHis;

/**
 * Class BOIntFinancierosImpl.
 * 
 * Clase de negocio que interpreta y sirve las
 * peticionbes del Controller al que corresponde
 * 
 * Realiza las peticiones del DAO a la BD
 * 
 */
/**
 * @author SNGEnterprise
 * Fecha 29/30/2020
 * Hora 00:00:00
 */
@Remote(BOIntFinancierosHis.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOIntFinancierosHisImpl extends Architech implements BOIntFinancierosHis{

	/** Propiedad del tipo long que almacena el 
	 * valor de serialVersionUID. 
	 */
	private static final long serialVersionUID = 8889877435757381640L;

	/** La variable que contiene informacion con respecto a: dao IntFinancierosHis. */
	@EJB
	private DAOIntFinancierosHis daoIntFinancierosHis;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;	

	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;	

	/** La constante OK. */
	private static final String OK = "OK";
	
	/** La constante NOK. */
	private static final String NOK = "NOK";

	/** La constante COMU_INTERME_FIN. */
	private static final String COMU_INTERME_FIN = "COMU_INTERME_FIN";

	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "CVE_INTERME";
	
	/**
	 * BeanResIntFinancierosHist : consultaIntFinaHist
	 * 
	 * @param ArchitechSessionBean : session
	 * @param BeanPaginador : beanPaginador
	 * @param BeanIntFinancieroHistorico : beanIntFinancieroHistorico
	 * @throws BusinessException : Excepcion controlada para los errores de Ejecucion
	 */
	
	@Override
	public BeanResIntFinancierosHist consultaIntFinaHist(ArchitechSessionBean session,
			BeanIntFinancieroHistorico beanIntFinancieroHistorico, BeanPaginador beanPaginador) throws BusinessException {	
		BeanPaginador paginador = beanPaginador;
		if (paginador==null){
			paginador = new BeanPaginador();
		}
		
		if (paginador.getAccion() != null
				&& !"SIG".equalsIgnoreCase(paginador.getAccion()) 
				&& !"ANT".equalsIgnoreCase(paginador.getAccion())
				&& !"FIN".equalsIgnoreCase(paginador.getAccion())) {
			paginador.setPagina("");
		}
		
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		BeanResIntFinancierosHist response = daoIntFinancierosHis.consultar(session, beanIntFinancieroHistorico, beanPaginador);
		
		//Cuando no se encuentran resultados
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError()) && response.getIntFinancieroHistoricos().isEmpty()){
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		//Cuando se ecuentran resultados
		}else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(),HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		//Cuando se genera un error
		}else{
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setMsgError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		return response;
	}
		
		
	/**
	 * generaPistaAuditoras
	 * @param operacion El objeto: operacion
	 * @param urlController El objeto: urlController
	 * @param resOp informacion respuesta
	 * @param sesion sesion
	 * @param dtoNuevo datos nuevo
	 * @param dtoAnterior dato anterior
	 * @param dtoModificado El objeto: dtoModificado
	 */
	private void generaPistaAuditoras (String operacion, String urlController,boolean resOp, ArchitechSessionBean sesion, String dtoNuevo,String dtoAnterior,String dtoModificado){
		BeanPistaAuditora beanPistaAuditora = new BeanPistaAuditora();
		
		//seteamos los valores a utilizar
		beanPistaAuditora.setNomTabla(COMU_INTERME_FIN);
		beanPistaAuditora.setOperacion(operacion);
		beanPistaAuditora.setUrlController(urlController);
		beanPistaAuditora.setDatoFijo(DATO_FIJO);
		beanPistaAuditora.setDatoModi(dtoModificado);
		beanPistaAuditora.setDtoNuevo(dtoNuevo);
		beanPistaAuditora.setDtoAnterior(dtoAnterior);
		
		//validammos las respuestas
		if (resOp){
			beanPistaAuditora.setEstatus(OK);
		}else{
			beanPistaAuditora.setEstatus(NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(beanPistaAuditora, sesion);
	}
	
	/**
	 * BeanResBase : exportarTodo
	 * 
	 * @param ArchitechSessionBean : session
	 * @param BeanPaginador : beanPaginador
	 * @param BeanIntFinancieroHistorico : beanIntFinancieroHistorico
	 * @throws BusinessException : Excepcion controlada para los errores de Ejecucion
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, BeanIntFinancieroHistorico beanIntFinancieroHisto, int totalRegistros) 
					throws BusinessException {	
		//se declara las variables y objetos a utilizar
		boolean operacion = true;
		BeanResBase respuestas = new BeanResBase();
		BeanExportarTodo  exptRequest = new BeanExportarTodo();
		
		//se generan los query a utilizar
		String query = UtileriasIntFinancierosHis.CONSULTA_EXP_TODO_CAT_FIN;
		String titulos = UtileriasIntFinancierosHis.TITULO_EXP_TODO_CAT_FIN;
		
		//seteo de campos
		exptRequest.setConsultaExportar(query);	
		exptRequest.setColumnasReporte(titulos);
		exptRequest.setNombreRpt("Historico_Intermediario");
		exptRequest.setEstatus("PE");
		exptRequest.setModulo("CATALOGOS");
		exptRequest.setSubModulo("Cat. Intermediarios");
		exptRequest.setTotalReg(totalRegistros + "");
		
		//se realiza la consulta al exportar
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exptRequest, session);
		
		//validamos el codigo
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())){
			operacion = false;
		}
		//obtenemos la respuesta
		respuestas.setCodError(resExportarTodo.getCodError());
		respuestas.setMsgError(resExportarTodo.getMsgError());
		respuestas.setTipoError(resExportarTodo.getTipoError());
		
		//Genera Pista
		generaPistaAuditoras(UtileriasIntFinancierosHis.TYPE_EXPORT, "exportarTodoIntFinancierosHis.do", operacion, session,"","", DATO_FIJO);
		return respuestas;
	}
}