/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqGuardarSPIDDet;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdmon;
import mx.isban.eTransferNal.dao.moduloSPID.DAODetRecepOpSPID;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmon;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BODetRecepOpSPIDImpl extends Architech implements BODetRecepOpSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 1391939102480279976L;
	
	/**
	 * Propiedad del tipo DAODetRecepOpSPID que almacena el valor de dAODetRecepOpSPID
	 */
	@EJB
	private DAODetRecepOpSPID dAODetRecepOpSPID;
	/**
	 * Propiedad del tipo DAOBitacoraAdmon que almacena el valor de dAOBitacoraAdmon
	 */
	@EJB
	private DAOBitAdmon dAOBitAdmon;
	
	


	@Override
	public BeanResReceptoresSPID guardarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet, ArchitechSessionBean sessionBean) {
		BeanReqInsertBitAdmon beanReqInsertBitAdmon = null;
		UtileriasBitAdmon  util =UtileriasBitAdmon.getUtileriasBitAdmon();
		BeanReceptoresSPID nuevo = beanReqGuardarSPIDDet.getBeanReq();
		BeanReceptoresSPID ant = beanReqGuardarSPIDDet.getBeanReqAnt();
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = null;
		beanResActTranSpeiRecDAO = dAODetRecepOpSPID.actualizaDetTransSpeiRec(beanReqGuardarSPIDDet.getBeanReq(), sessionBean);
		beanResReceptoresSPID = consultarDetRecepOp(beanReqGuardarSPIDDet,sessionBean);
		if(Errores.OK00000V.equals(beanResActTranSpeiRecDAO.getCodError())){
			beanReqInsertBitAdmon = util.createBitacoraBitTrans(nuevo.getLlave().getFchOperacion(),
					"GUARDAR",util.transformJavaToJson(ant), util.transformJavaToJson(nuevo), "ACTUALIZAN DATOS", 
					util.createDatoFijo(nuevo.getLlave().getFchOperacion(), nuevo.getLlave().getCveMiInstituc(), 
							nuevo.getLlave().getCveInstOrd(), nuevo.getLlave().getFolioPaquete(), nuevo.getLlave().getFolioPago()), 
							"guardarDetRecepOpSPID.do", "NA", "TRAN_SPID_REC", "ACTSPIDREC", "TRANSFER");
			dAOBitAdmon.guardaBitacora(beanReqInsertBitAdmon, sessionBean);
			beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResReceptoresSPID.setCodError(Errores.OK00000V);
		}else{
			beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResReceptoresSPID.setCodError(beanResActTranSpeiRecDAO.getCodError());
		}
		return beanResReceptoresSPID;
	}
	
	@Override
	public BeanResReceptoresSPID consultarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet, ArchitechSessionBean sessionBean) {
		BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO = null;
		BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID(); 
		List<Object> parametros = Arrays.asList(new Object[]{ 
				beanReqGuardarSPIDDet.getBeanReq().getLlave().getCveMiInstituc(),
				beanReqGuardarSPIDDet.getBeanReq().getLlave().getFchOperacion(),
				beanReqGuardarSPIDDet.getBeanReq().getLlave().getCveInstOrd(),
				beanReqGuardarSPIDDet.getBeanReq().getLlave().getFolioPaquete(),
				beanReqGuardarSPIDDet.getBeanReq().getLlave().getFolioPago()
		});
				
		beanResReceptoresSPIDDAO = dAODetRecepOpSPID.consultaTransferencia(parametros, sessionBean);
		
		beanResReceptoresSPIDDAO.getBeanReceptoresSPID().setOpciones(beanReqGuardarSPIDDet.getBeanReq().getOpciones());
		
		beanResReceptoresSPID.setBeanReceptoresSPID(beanResReceptoresSPIDDAO.getBeanReceptoresSPID());
		return beanResReceptoresSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAODetRecepOpSPID
	 * @return dAODetRecepOpSPID Objeto del tipo DAODetRecepOpSPID
	 */
	public DAODetRecepOpSPID getDAODetRecepOpSPID() {
		return dAODetRecepOpSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAODetRecepOpSPID
	 * @param detRecepOpSPID del tipo DAODetRecepOpSPID
	 */
	public void setDAODetRecepOpSPID(DAODetRecepOpSPID detRecepOpSPID) {
		dAODetRecepOpSPID = detRecepOpSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOBitAdmon
	 * @return dAOBitAdmon Objeto del tipo DAOBitAdmon
	 */
	public DAOBitAdmon getDAOBitAdmon() {
		return dAOBitAdmon;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOBitAdmon
	 * @param bitAdmon del tipo DAOBitAdmon
	 */
	public void setDAOBitAdmon(DAOBitAdmon bitAdmon) {
		dAOBitAdmon = bitAdmon;
	}



}
