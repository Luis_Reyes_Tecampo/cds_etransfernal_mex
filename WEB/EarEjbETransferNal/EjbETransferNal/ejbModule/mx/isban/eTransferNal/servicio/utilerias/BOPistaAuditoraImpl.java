/**
 * 
 */
package mx.isban.eTransferNal.servicio.utilerias;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * @author mtrejo
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOPistaAuditoraImpl implements BOPistaAuditora {
	
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitAdministrativa daoBitacora;
	
	
	@EJB
	private DAOBitacoraAdmon daoBitacoraAdmon;
	
	/**
	 * Procedimiento para el llenado de datos de la Pista Audiora Catalogos.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 */
	@Override
	public BeanResGuardaBitacoraDAO llenaPistaAuditora(BeanPistaAuditora beanPista, ArchitechSessionBean sesion) {
		return enviarPista(beanPista, sesion);
	}
	
	/**
	 * Procedimiento para el llenado de datos de la Pista Audiora Catalogos.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 */
	@Override
	public BeanResGuardaBitacoraDAO llenaPistaAuditoraAdmon(BeanPistaAuditora beanPista, ArchitechSessionBean sesion) throws BusinessException {
		return enviarPistaAdmon(beanPista, sesion);
	}
	
	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 */
	private BeanResGuardaBitacoraDAO enviarPista(BeanPistaAuditora beanPista, ArchitechSessionBean sesion) {
		// Objetos para las Pistas auditoras
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = new BeanReqInsertBitAdmin();
		// Iniciamos el proceso de llenado de las pistas auditoras
		beanReqInsertBitAdmin = utileriasBitAdmin.createBitacoraBitAdmin(
    		beanPista.getEstatus(), 
    		beanPista.getNomTabla(),
    		beanPista.getOperacion(),
    		beanPista.getUrlController(),
    		beanPista.getDtoAnterior(),
    		beanPista.getDtoNuevo(),     		
    		beanPista.getDatoModi(), 
    		beanPista.getDatoFijo(), 
    		beanPista.getComentarios(), 
    		sesion 
    	);
		// Procedemmos al almacenamiento de la pista auditora
		return daoBitacora.guardaBitacora(beanReqInsertBitAdmin, sesion);
	}
	
	
	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 * @throws BusinessException 
	 */
	private BeanResGuardaBitacoraDAO enviarPistaAdmon(BeanPistaAuditora beanPista, ArchitechSessionBean sesion) throws BusinessException {
		/**Se genera el bean de bitacora **/
		BeanReqInsertBitacora beanReqInsertBitacora = new BeanReqInsertBitacora();
		beanReqInsertBitacora.setServicio(beanPista.getUrlController());
		beanReqInsertBitacora.setFechaHora(new Date());
		//estatus de la operacion si fue exitosa
		beanReqInsertBitacora.setEstatusOperacion(beanPista.getEstatus());
		beanReqInsertBitacora.setCodOperacion(beanPista.getOperacion());
		beanReqInsertBitacora.setTablaAfectada(beanPista.getNomTabla());
		// tipo de operacion update,insert,delete
		beanReqInsertBitacora.setDescOperacion(beanPista.getOperacion());
		beanReqInsertBitacora.setCodError(beanPista.getCodigoError());
		beanReqInsertBitacora.setDescCodError(beanPista.getCodigoError());
		beanReqInsertBitacora.setDatoFijo(beanPista.getDatoFijo());
		// sea signa el dato antarior
		beanReqInsertBitacora.setValNuevo(beanPista.getDtoNuevo());
		//se asigna el dato nuevo
		beanReqInsertBitacora.setValAnt(beanPista.getDtoAnterior());
		
		/**Se realiza el insert **/
		BeanResGuardaBitacoraDAO bean = daoBitacoraAdmon.guardaBitacora(beanReqInsertBitacora, sesion);
		/**Si esixte un error regresa el BusinessException**/
		if (bean.getCodError().equals(Errores.EC00011B)){			
  			throw new BusinessException(bean.getCodError(), bean.getMsgError());      			
  		}
		
		return bean;
	}
}
