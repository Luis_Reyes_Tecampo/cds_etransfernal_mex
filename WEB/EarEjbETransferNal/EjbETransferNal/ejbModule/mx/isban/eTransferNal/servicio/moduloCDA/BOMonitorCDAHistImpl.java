/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqMonCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistMontos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistVolumen;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorCDAHist;

/**
 *Clase del tipo BO que se encarga  del negocio del monitor CDA
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorCDAHistImpl extends Architech implements BOMonitorCDAHist {

	/**
	 * Constante del serial Version
	 */
	private static final long serialVersionUID = 6306432654741694144L;

	/**
	 * Referencia al servicio de Monitor CDA DAO
	 */
	@EJB
    private DAOMonitorCDAHist daoMonitorCDAHist;
	
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param beanReqMonCDAHist Objeto del tipo @see BeanReqMonCDAHist
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @exception BusinessException Excepcion de negocio
	 */
	@Override
	public BeanResMonitorCdaHistBO obtenerDatosMonitorCDA(BeanReqMonCDAHist beanReqMonCDAHist,
			ArchitechSessionBean architectSessionBean) throws BusinessException {
		BeanResMonitorCdaHistBO beanResMonitorCdaBO =  new BeanResMonitorCdaHistBO();
		BeanResMonitorCdaHistMontos beanResMonitorCdaMontos = new BeanResMonitorCdaHistMontos();
		BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen = new BeanResMonitorCdaHistVolumen();
		
		BeanResMonitorCdaHistDAO beanResMonitorCdaDAO = null;		
		beanResMonitorCdaDAO = daoMonitorCDAHist.consultarCDAAgrupadas(beanReqMonCDAHist,architectSessionBean);
		
		
		if(!Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			initMontoVolumen(beanResMonitorCdaMontos,beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setCodError(beanResMonitorCdaDAO.getCodError());
			beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			beanResMonitorCdaMontos.setMontoCDAConfirmadas(beanResMonitorCdaDAO.getMontoCDAConfirmadas());
			beanResMonitorCdaMontos.setMontoCDAContingencia(beanResMonitorCdaDAO.getMontoCDAContingencia());
			beanResMonitorCdaMontos.setMontoCDAEnviadas(beanResMonitorCdaDAO.getMontoCDAEnviadas());
			beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(beanResMonitorCdaDAO.getMontoCDAOrdRecAplicadas());
			beanResMonitorCdaMontos.setMontoCDAPendEnviar(beanResMonitorCdaDAO.getMontoCDAPendEnviar());
			beanResMonitorCdaMontos.setMontoCDARechazadas(beanResMonitorCdaDAO.getMontoCDARechazadas());
			
			
			beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(beanResMonitorCdaDAO.getVolumenCDAConfirmadas());
			beanResMonitorCdaVolumen.setVolumenCDAContingencia(beanResMonitorCdaDAO.getVolumenCDAContingencia());
			beanResMonitorCdaVolumen.setVolumenCDAEnviadas(beanResMonitorCdaDAO.getVolumenCDAEnviadas());
			beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(beanResMonitorCdaDAO.getVolumenCDAOrdRecAplicadas());
			beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(beanResMonitorCdaDAO.getVolumenCDAPendEnviar());
			beanResMonitorCdaVolumen.setVolumenCDARechazadas(beanResMonitorCdaDAO.getVolumenCDARechazadas());
			
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);			
			beanResMonitorCdaBO.setCodError(Errores.OK00000V);
		}
		beanResMonitorCdaBO.setFechaOperacion(beanReqMonCDAHist.getFechaOperacion());
		return beanResMonitorCdaBO;
	}
	

	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param beanResMonitorCdaMontos Objeto del tipo @see BeanResMonitorCdaHistMontos
	 * @param beanResMonitorCdaVolumen Objeto del tipo @see BeanResMonitorCdaHistVolumen
	 * @exception BusinessException Excepcion de negocio
	 */
	private void initMontoVolumen(BeanResMonitorCdaHistMontos beanResMonitorCdaMontos,
			BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen) throws BusinessException{
		final String CERO_PUNTO_CERO = "0.0";
		final String CERO = "0";
		beanResMonitorCdaMontos.setMontoCDAConfirmadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAContingencia(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAEnviadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDAPendEnviar(CERO_PUNTO_CERO);
		beanResMonitorCdaMontos.setMontoCDARechazadas(CERO_PUNTO_CERO);
		
		beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAContingencia(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAEnviadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(CERO);
		beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(CERO);
		beanResMonitorCdaVolumen.setVolumenCDARechazadas(CERO);
	}


	/**
	 * Metodo get que obtiene el valor de la propiedad daoMonitorCDAHist
	 * @return daoMonitorCDAHist Objeto de tipo @see DAOMonitorCDAHist
	 */
	public DAOMonitorCDAHist getDaoMonitorCDAHist() {
		return daoMonitorCDAHist;
	}


	/**
	 * Metodo que modifica el valor de la propiedad daoMonitorCDAHist
	 * @param daoMonitorCDAHist Objeto de tipo @see DAOMonitorCDAHist
	 */
	public void setDaoMonitorCDAHist(DAOMonitorCDAHist daoMonitorCDAHist) {
		this.daoMonitorCDAHist = daoMonitorCDAHist;
	}




}
