/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCapturaCentralImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   21/03/2017 04:38:19 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.servicio.capturasManuales;


import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOCapturaCentral;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Class BOCapturaCentralImpl.
 *
 * @author FSW-Vector
 * @since 21/03/2017
 */
@Remote(BOCapturaCentral.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCapturaCentralImpl extends Architech implements BOCapturaCentral {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -3860331937644548220L;
	
	/** La constante ERMCC001.  No existe*/
	private static final String ERMCC001 = "ERMCC001";
	
	/** La constante ERMCC002. No esta disponible */
	private static final String ERMCC002 = "ERMCC002";
	
	/** La constante OKMCC000. Alta de operacion OK */
	private static final String OKMCC000 = "OKMCC000";
	
	/** La constante OK. */
	private static final String OK = "OK";
	
	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante TRAN_CAP_OPER. */
	private static final String TRAN_CAP_OPER = "TRAN_CAP_OPER";
	
	/** La constante ID_FOLIO. */
	private static final String ID_FOLIO = "ID_FOLIO";
	
	/** La variable que contiene informacion con respecto a: dao int financieros. */
	@EJB
	private DAOCapturaCentral daoCapturaCentral;
	
	/** Referencia al DAO que realiza la bitacora. */
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral#consultaTodosTemplates(mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanCapturaCentralResponse consultaTodosTemplates(
			BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Nuevo objeto de respuesta del servicio
		BeanCapturaCentralResponse response = new BeanCapturaCentralResponse();
		//Ejecuta el DAO
		response = daoCapturaCentral.consultaTodosTemplates(request, architechSessionBean);
		//Valida respuesta del DAO
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
				response.setCodError(Errores.OK00000V);
				response.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		//Regresa la respueta
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral#consultaTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanCapturaCentralResponse consultaTemplate(BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Nuevo objeto de respuesta del servicio
		BeanCapturaCentralResponse response = new BeanCapturaCentralResponse();
		//Ejecuta el DAO
		response = daoCapturaCentral.consultaTemplate(request, architechSessionBean);
		//Valida respuesta del DAO
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError()) 
				&& layoutNoEsVacio(response)) {
				response.setCodError(Errores.OK00000V);
		//Validaciones de no hay datos
		} else if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			throw new BusinessException(ERMCC001,response.getMsgError());
		//Errores generales
		} else {
			throw new BusinessException(Errores.EC00011B,response.getMsgError());
	    }
		//Regresa la respueta
		return response;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral#altaCapturaManualSPID(mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase altaCapturaManualSPID(BeanOperacion operacion,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Nuevo objeto de Utilerias
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		//Nuevo objeto de Respuesta del servicio
		BeanResBase response = new BeanResBase();
		//Se ejecuta el metodo del DAO
		response = daoCapturaCentral.altaCapturaManualSPID(operacion, architechSessionBean);
		//Validaciones de respuesta
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			response.setCodError(OKMCC000);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(OK, TRAN_CAP_OPER, "INSERT", "altaCapturaManualSPID.do", StringUtils.EMPTY, ReflectionToStringBuilder.toString(operacion, ToStringStyle.SHORT_PREFIX_STYLE), 
        					ID_FOLIO, ID_FOLIO, "INSERTA FOLIO", architechSessionBean);
        	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
		} else {
			BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
        			utileriasBitAdmin.createBitacoraBitAdmin(NOK, TRAN_CAP_OPER, "INSERT", "altaCapturaManualSPID.do", StringUtils.EMPTY, ReflectionToStringBuilder.toString(operacion, ToStringStyle.SHORT_PREFIX_STYLE), 
        					ID_FOLIO, ID_FOLIO, "INSERTA FOLIO", architechSessionBean);
        	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
        	throw new BusinessException(Errores.EC00011B,response.getMsgError());
	    }
		//Regresa el response
		return response;
	}

	private boolean layoutNoEsVacio(BeanCapturaCentralResponse response){
		boolean noEsVacio = false;
		if( null != response.getIdLayout() && StringUtils.EMPTY != response.getIdLayout() ){
			return true;
		} 
		return noEsVacio;
	}
}
