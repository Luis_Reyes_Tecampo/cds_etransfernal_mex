/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGraficoSPEIImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.pagoInteres;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGraficoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResTipoPagoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanTotalesGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanTotalesGraficoDAO;
import mx.isban.eTransferNal.dao.pagoInteres.DAOGraficoSPEI;
import mx.isban.eTransferNal.dao.pagoInteres.DAORepPagoInteresCompSPEI;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGraficoSPEIImpl implements BOGraficoSPEI{
	


	/**
	 * Propiedad del tipo DAOGraficoSPEI que almacena el valor de dAOGraficoSPEI
	 */
	@EJB private DAOGraficoSPEI dAOGraficoSPEI;
	/**
	 * Propiedad del tipo DAORepPagoInteresCompSPEI que almacena el valor de dAORepPagoInteresCompSPEI
	 */
	@EJB private DAORepPagoInteresCompSPEI dAORepPagoInteresCompSPEI;

	@Override
	public BeanResGrafico consultaDatosSPEI(BeanReqGrafico req,ArchitechSessionBean sessionBean) {
		BeanResGrafico beanResGrafico = new BeanResGrafico();
		BeanResGraficoDAO beanResGraficoDAO  = dAOGraficoSPEI.consultaDatosSPEI(req,sessionBean);
		BeanTotalesGrafico totales = seteaTotales(beanResGraficoDAO);
		Object[] datos;
		if(beanResGraficoDAO.getDatos()==null || beanResGraficoDAO.getDatos().isEmpty()){
			datos = new Object[]{new Object[]{}};
		}else{
			datos = beanResGraficoDAO.getDatos().toArray();
		}		
		if("EN".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Env\u00EDos");
			beanResGrafico.setLeyendaTotal("Total Operaciones Env\u00EDos");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DEM".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Env Mov");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Env Mov");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DE".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Env");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Env");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("RE".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Recepciones");
			beanResGrafico.setLeyendaTotal("Total Operaciones Recepciones");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("RM".equals(req.getTipoPago())){			
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Recepciones Mov");
			beanResGrafico.setLeyendaTotal("Total Operaciones Recepciones Mov");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DR".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Recibidas");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Recibidas");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else{
			seteaLeyendas(req,beanResGrafico);
		}
		
		beanResGrafico.setDatos(datos);
		beanResGrafico.setTotales(totales);
		beanResGrafico.setLeyenda(String.format("Transacciones de: %s:%s A %s:%s",req.getHoraInicio(),req.getMinutoInicio(), req.getHoraFin(),req.getMinutoFin()));
		return beanResGrafico;
	}
	
	/**
	 * @param req Bean con los parametros de entrada
	 * @param beanResGrafico Bean con los datos de respuesta
	 */
	private void seteaLeyendas(BeanReqGrafico req,BeanResGrafico beanResGrafico){
		if("DRM".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Recibidas Mov");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Recibidas Mov");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DRX".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Recibidas Ext");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Recibidas Ext");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DRXM".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Recibidas Ext Mov");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Recibidas Ext Mov");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DEX".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Env Ext");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Env Ext");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}else if("DEXM".equals(req.getTipoPago())){
			beanResGrafico.setLeyendaMonto("Monto Total Pagado Dev Env Ext Mov");
			beanResGrafico.setLeyendaTotal("Total Operaciones Dev Env Ext Mov");
			beanResGrafico.setLeyendaTipoTransfe(req.getTipoPago());
		}
	}
	/**
	 * Metodo para setear valores
	 * @param beanResGraficoDAO Bean con el resultado de la consulta
	 * @return BeanTotalesGrafico Bean con los totales
	 */
	private BeanTotalesGrafico seteaTotales(BeanResGraficoDAO beanResGraficoDAO){
		Utilerias util = Utilerias.getUtilerias();
		BeanTotalesGraficoDAO beanTotalesDAO = beanResGraficoDAO.getBeanTotalesGraficoDAO();
		BeanTotalesGrafico totales = new BeanTotalesGrafico(); 
		totales.setMontoTotal(util.formateaDecimales(beanTotalesDAO.getMontoTotal(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
		totales.setTotalOp(util.formateaDecimales(beanTotalesDAO.getTotalOp(), Utilerias.FORMATO_NUMBER));
		return totales;
	}
	
	@Override
	public BeanResGrafico consultaCatalogos(ArchitechSessionBean sesion) {
		BeanResGrafico beanResGrafico = new BeanResGrafico();
		BeanResTipoPagoDAO beanResTipoPagoDAO = dAORepPagoInteresCompSPEI.consultaTipoPago(sesion);
		beanResGrafico.setTipoPagoList(beanResTipoPagoDAO.getTipoPagoList());
		BeanResGraficoDAO beanResGraficoDAO = dAOGraficoSPEI.consultaRango(sesion);
		beanResGrafico.setRango(beanResGraficoDAO.getRango());
		return beanResGrafico;
	}
	
	@Override
	public BeanResGrafico consultaRango(ArchitechSessionBean sesion) {
		BeanResGrafico beanResGrafico = new BeanResGrafico();
		BeanResGraficoDAO beanResGraficoDAO = dAOGraficoSPEI.consultaRango(sesion);
		beanResGrafico.setRango(beanResGraficoDAO.getRango());
		return beanResGrafico;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOGraficoSPEI
	 * @return dAOGraficoSPEI Objeto del tipo DAOGraficoSPEI
	 */
	public DAOGraficoSPEI getDAOGraficoSPEI() {
		return dAOGraficoSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOGraficoSPEI
	 * @param graficoSPEI del tipo DAOGraficoSPEI
	 */
	public void setDAOGraficoSPEI(DAOGraficoSPEI graficoSPEI) {
		dAOGraficoSPEI = graficoSPEI;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAORepPagoInteresCompSPEI
	 * @return dAORepPagoInteresCompSPEI Objeto del tipo DAORepPagoInteresCompSPEI
	 */
	public DAORepPagoInteresCompSPEI getDAORepPagoInteresCompSPEI() {
		return dAORepPagoInteresCompSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAORepPagoInteresCompSPEI
	 * @param repPagoInteresCompSPEI del tipo DAORepPagoInteresCompSPEI
	 */
	public void setDAORepPagoInteresCompSPEI(
			DAORepPagoInteresCompSPEI repPagoInteresCompSPEI) {
		dAORepPagoInteresCompSPEI = repPagoInteresCompSPEI;
	}






	



	
}
