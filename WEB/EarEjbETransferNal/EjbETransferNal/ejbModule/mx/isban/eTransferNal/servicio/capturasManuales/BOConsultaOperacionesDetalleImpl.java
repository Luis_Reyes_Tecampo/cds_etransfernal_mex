package mx.isban.eTransferNal.servicio.capturasManuales;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.capturasManuales.DAOConsultaOperacionesDetalle;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Clase encargada de la logica de negocio para Consulta Operaciones Detalle
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaOperacionesDetalleImpl extends Architech implements BOConsultaOperacionesDetalle{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6656631113303263271L;
	
	//Constante OK
	/** La constante OK. */
	private static final String OK = "OK";
	
	//Constante NOK
	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	//Constante TRAN_CAP_OPER
	/** La constante TRAN_CAP_OPER. */
	private static final String TRAN_CAP_OPER = "TRAN_CAP_OPER";
	
	/**
	 * Propiedad del tipo DAOConsultaOperaciones que almacena el valor de daoConsultaOperaciones
	 */
	//DAO propio del modulo
	@EJB
	private transient DAOConsultaOperacionesDetalle daoConsultaOperacionesDetalle;

	/**
	 * Referencia al DAO que realiza la bitacora 
	 */
	//DAO para guardar las pistas
	@EJB
	private transient DAOBitAdministrativa daoBitAdministrativa;
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperacionesDetalle#obtenerTemplate(mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	//Metodo para obtener template
	@Override
	public BeanResConsOperaciones obtenerTemplate(BeanReqConsOperaciones beanReqConsOperaciones, ArchitechSessionBean architechSessionBean) throws BusinessException {
		
		BeanResConsOperaciones beanRes = daoConsultaOperacionesDetalle.obtenerTemplate(beanReqConsOperaciones, architechSessionBean);
		if(beanRes.getTemplate() != null && !StringUtils.EMPTY.equals(beanRes.getTemplate())){
			//Si no viene nulo o vacio se setea en la respuesta el template, ya que es la funcion de este metodo
			beanRes.setCodError(Errores.CODE_SUCCESFULLY);
			beanRes.setTipoError(Errores.TIPO_MSJ_INFO);
		}else{
			//Se setea codigo de error de los servicios
	    	throw new BusinessException(Errores.EC00011B, beanRes.getMsgError());
		}
		
		return  beanRes;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperacionesDetalle#consultaPermisoApartar(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	//Metodo para consultar permiso de apartado
	@Override
	public BeanResBase consultaPermisoApartar(ArchitechSessionBean architechSessionBean, String folio) throws BusinessException {

		BeanResBase beanRes = daoConsultaOperacionesDetalle.permisosApartar(architechSessionBean, folio);
		if(!Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError())){
			//Se setea codigo de error de los servicios
	    	throw new BusinessException(Errores.EC00011B, beanRes.getMsgError());
		}
		return beanRes;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperacionesDetalle#consultaPermisoModificar(mx.isban.agave.commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	//Metodo para consultar permiso de modificar
	@Override
	public BeanResBase consultaPermisoModificar(ArchitechSessionBean architechSessionBean, String folio) throws BusinessException {
		
		BeanResBase beanRes = daoConsultaOperacionesDetalle.permisosModificar(architechSessionBean, folio);
		if(!Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError())){
			//Se setea codigo de error de los servicios
	    	throw new BusinessException(Errores.EC00011B, beanRes.getMsgError());
		}
		
		return  beanRes;
	}
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperacionesDetalle#guardarOperacion(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion, java.util.List)
	 */
	@Override
	//Metodo encargado de guardar una operacion
	public BeanResBase guardarOperacion(ArchitechSessionBean architechSessionBean, BeanOperacion operacion, List<Map<String, String>> camposPistas, BeanResConsOperaciones folio){
		
		String pistaExtraInfo = ", Cuenta_origen="+folio.getCuentaOrdenante()+", Cuenta_destino="+folio.getCuentaReceptora()+", Monto="+folio.getMonto();

		//Se crean objetos necesarios para ejecutar el query
		BeanResBase response = new BeanResBase();
		response = daoConsultaOperacionesDetalle.guardarOperacion(architechSessionBean, operacion);
		String estatusGuardado="";
 		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			estatusGuardado=OK;
		} else {
			response.setCodError(Errores.EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
			estatusGuardado=NOK;
	    }
		
		//Codigo para revisar cambios en los campos y guardar en bitacora
		String valoresAnteriores=crearStringDeListaDeCampos(camposPistas.get(0));
		String valoresNuevos=crearStringDeListaDeCampos(camposPistas.get(1));
		
		//Se genera el registro en bitacora
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
    			utileriasBitAdmin.createBitacoraBitAdmin(estatusGuardado, TRAN_CAP_OPER, "UPDATE",
    					"consultaOperacionesDetalleGuardar.do",
    					valoresAnteriores+pistaExtraInfo, 
    					valoresNuevos,
    					" ",
    					" ",
    					"MODIFICA FOLIO", architechSessionBean);
    	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, architechSessionBean);
    	
		return response;
	}
	
	/**
	 * Crear string de lista de campos.
	 *
	 * @param map the map
	 * @return the string
	 */
	//Toma un diccionario y lo pasa a un string separado por comas
	private String crearStringDeListaDeCampos(Map<String,String>map){
		StringBuilder buffer = new StringBuilder();
		int count = 0;
		for ( Map.Entry<String, String> entry : map.entrySet() ) {
			if(count > 0){
				buffer.append(',');
			}
		    String key = entry.getKey();
		    String value = entry.getValue();
		    String appendingString=key + "=" + value;
		    buffer.append(appendingString);
		    count += 1;
		}
		//Se valida que vaya algo en la respuesta ya que en la tabla puede generar un error al ir vacio
		String res=buffer.toString();
		if("".equals(res)){
			res=" ";
		}
		return res;
	}
}