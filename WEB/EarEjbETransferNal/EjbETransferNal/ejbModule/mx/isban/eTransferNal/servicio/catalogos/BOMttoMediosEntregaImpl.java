/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMttoMediosEntregaImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/09/2018 04:41:38 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOMttoMediosEntrega;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasMttoMediosEntrega;

/**
 * Class BOMttoMediosEntregaImpl.
 * 
 * Clase de negocio que interpreta y sirve las 
 * peticiones del Controlador
 * 
 * Realiza peticiones al DAO para comunicarse con 
 * la Base de Datos
 *
 * @author FSW-Vector
 * @since 28/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMttoMediosEntregaImpl extends Architech implements BOMttoMediosEntrega {

	/** La constante serialVersionUID.*/
	private static final long serialVersionUID = -6711315703743350412L;	
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "COMU_MEDIOS_ENT";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "CVE_MEDIO_ENT";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante TYPE_INSERT. */
	private static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	private static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_DELETE. */
	private static final String TYPE_DELETE = "DELETE";

	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante UPDATE. */
	private static final String UPDATE = "CANAL,DESCRIPCION,COM_ANUAL_PF,COM_ANUAL_PM,CKK_SEGURIDAD";
	/** La constante INSERT. */
	private static final String INSERT = "CVE_MEDIO_ENT, U_VERSION, DESCRIPCION, COM_ANUAL_PF, COM_ANUAL_PM, CKK_SEGURIDAD, CANAL";
	/** La constante SELECT. */
	private static final String SELECT = "CVE_MEDIO_ENT";
	
	/** La variable que contiene informacion con respecto a: dao mantenimiento entrega. */
	@EJB
	private DAOMttoMediosEntrega daoMttoMedios;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega#consultar(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.comunes.BeanFilter, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 */
	@Override
	public BeanMediosEntrega consultar(ArchitechSessionBean session, BeanMedioEntrega beanFiltro,
			BeanPaginador beanPaginador) throws BusinessException {
		//Paginacion
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		//Objetos de respuesta
		BeanMediosEntrega response = daoMttoMedios.consultar(session, beanFiltro, beanPaginador);
		//Cuando no se encuentran resultados
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getMediosEntrega().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		//Cuando se ecuentran resultados
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		//Cuando se genera un error
		} else {
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Retorna la Respuesta
		return response;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega#agregarMedioEntrega(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega)
	 */
	@Override
	public BeanResBase agregarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio) 
			throws BusinessException {
		boolean operacion = false;
		
		String dtoNuevo = obtenerDtos(beanMedio, TYPE_INSERT);
		String dtoAnt = "";
		
		
		BeanResBase response = daoMttoMedios.agregarMedioEntrega(session, beanMedio);
		//Valida resultado exitoso
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			response.setCodError(Errores.OK00003V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		//Control de Erorres
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			response.setCodError(Errores.ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			response.setCodError(Errores.EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Genera Pista
		generaPistaAuditoras(TYPE_INSERT, "agregarMedioEntrega.do", operacion, session,dtoNuevo,dtoAnt,INSERT);
		//Retorna la Respuesta
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega#eliminarMedios(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega)
	 */
	@Override
	public BeanEliminarResponse eliminarMedios(ArchitechSessionBean session, BeanMediosEntrega beanMedios)
			throws BusinessException {
	
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanMedioEntrega beanMedio : beanMedios.getMediosEntrega()){
			
			String dtoAnt = obtenerDtos(beanMedio, TYPE_DELETE);
			
			if (beanMedio.isSeleccionado()) {
				BeanResBase responseEliminar = daoMttoMedios.eliminarMediosEntrega(session, beanMedio);
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(beanMedio.getCveMedioEntrega());
	        		exito.append(',');
	        	} else {
	        		operacion = false;
	        		error.append(beanMedio.getCveMedioEntrega());
	        		error.append(',');
	        	}
	        	generaPistaAuditoras(TYPE_DELETE, "eliminarMediosEntrega.do", operacion, session, "",dtoAnt,SELECT);
			}
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		//Retorna la Respuesta
		return response;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega#editarMedio(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega)
	 */
	@Override
	public BeanResBase editarMedio(ArchitechSessionBean session, BeanMedioEntrega beanMedio) throws BusinessException {
		BeanResBase response = null;
		boolean operacion;
		
		/**se crea el bean para el filtro**/
		BeanMedioEntrega beanFiltro = new BeanMedioEntrega();
		beanFiltro.setCveMedioEntrega(beanMedio.getCveMedioEntrega());
		
		/**se crea el paginador **/
		//Paginacion
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
				
		//Objetos de respuesta
		BeanMediosEntrega beanResul = daoMttoMedios.consultar(session, beanFiltro, beanPaginador);
		
		String dtoNuevo = obtenerDtos(beanMedio, TYPE_UPDATE);
		String dtoAnt = obtenerDtos(beanResul.getMediosEntrega().get(0), TYPE_UPDATE);
		
		response = daoMttoMedios.editarMedioEntrega(session, beanMedio);
		//Valida Resultado Exitoso
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			response.setCodError(Errores.OK00003V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			//Control de Erorres
			operacion = false;
			response.setCodError(Errores.EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Genera Pista
		generaPistaAuditoras(TYPE_UPDATE, "editarMedioEntrega.do", operacion, session, dtoNuevo,dtoAnt,UPDATE);
		//Retorna la Respuesta
		return response;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega#exportarTodo(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.comunes.BeanFilter, int)
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, BeanMedioEntrega request, int totalRegistros)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		// Consulta a Exportar
		String filtroWhere = UtileriasMttoMediosEntrega.getInstancia().getFiltro(request, "WHERE");
		String query = UtileriasMttoMediosEntrega.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasMttoMediosEntrega.ORDEN;
		// datos del reporte
		exportarRequest.setColumnasReporte(UtileriasMttoMediosEntrega.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("MEDIOS_ENTREGA");
		exportarRequest.setNombreRpt("CATALOGOS_MEDIOS_ENTREGA_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		// Se hace la consulta con el dao para exportar las operaciones
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		//Control de Erorres
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		//Genera Pista
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodosCertificados.do", operacion, session,"","", DATO_FIJO);
		//Retorna la Respuesta
		return response;
	}
	
	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param operacion            El objeto: operacion
	 * @param urlController            El objeto: url controller
	 * @param resOp            El objeto: res op
	 * @param sesion            El objeto: sesion
	 * @param datoModificado El objeto: dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion, String dtoNuevo,String dtoAnterior, String dtoModificado) {
		// Objetos para las Pistas auditoras
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		// Enviamos los datos de la pista auditora
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		// Cuando la operacion es correcta
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

	
	/**
	 * funcion para obtener los datos ant y nuevos
	 * @param beanMedio
	 * @param accion
	 * @return
	 */
	private String obtenerDtos (BeanMedioEntrega beanMedio, String accion) {
		String dtoAnt ="";
		if(ConstantesMttoMediosAut.TYPE_UPDATE.equals(accion)) {
			dtoAnt="COM_ANUAL_PF="+beanMedio.getComAnualPF()+", COM_ANUAL_PM="+beanMedio.getComAnualPM()+", CKK_SEGURIDAD="+beanMedio.getSeguridad()+
					" CANAL="+beanMedio.getCanal()+", DESCRIPCION="+beanMedio.getDesc();
		}
		
		if(ConstantesMttoMediosAut.TYPE_INSERT.equals(accion)) {
			dtoAnt ="CVE_MEDIO_ENT="+beanMedio.getCveMedioEntrega()+", U_VERSION="+beanMedio.getVersion()+", DESCRIPCION="+beanMedio.getDesc()+
					", COM_ANUAL_PF="+beanMedio.getComAnualPF()+", COM_ANUAL_PM="+beanMedio.getComAnualPM()+", CKK_SEGURIDAD="+beanMedio.getSeguridad()+
					" CANAL="+beanMedio.getCanal();
		}
		
		if(ConstantesMttoMediosAut.TYPE_DELETE.equals(accion) || ConstantesMttoMediosAut.TYPE_SELECT.equals(accion) ) {
			dtoAnt="CVE_MEDIO_ENT="+beanMedio.getCveMedioEntrega();
		}
		
		return dtoAnt;
	}
	
	
	
}
