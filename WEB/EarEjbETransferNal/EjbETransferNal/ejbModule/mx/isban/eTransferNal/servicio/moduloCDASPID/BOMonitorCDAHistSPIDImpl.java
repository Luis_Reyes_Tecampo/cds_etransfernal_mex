/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistMontos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistVolumen;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonCDAHistSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistDAOSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOMonitorCDAHistSPID;

/**
 *Clase del tipo BO que se encarga  del negocio del monitor CDA
**/
//Clase del tipo BO que se encarga  del negocio del monitor CDA
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorCDAHistSPIDImpl extends Architech implements BOMonitorCDAHistSPID {

	/**
	 * Constante del serial Version
	 */
	private static final long serialVersionUID = 6306432654741694144L;
	
	/**
	 * Referencia al servicio de Monitor CDA DAO
	 */
	//Referencia al servicio de Monitor CDA DAO
	@EJB
    private transient DAOMonitorCDAHistSPID daoMonitorCDAHist;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.moduloCDASPID.BOMonitorCDAHistSPID#obtenerDatosMonitorCDASPID(mx.isban.eTransferNal.servicio.moduloCDASPID.BeanReqMonCDAHistSPID, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	//Metodo para obtener los datos de monitor cda spid
	@Override
	public BeanResMonitorCdaHistBO obtenerDatosMonitorCDASPID(BeanReqMonCDAHistSPID beanReqMonCDAHist,
			ArchitechSessionBean architectSessionBean) throws BusinessException {
		//Se crean objetos de respuesta
		BeanResMonitorCdaHistBO beanResMonitorCdaBO =  new BeanResMonitorCdaHistBO();
		BeanResMonitorCdaHistMontos beanResMonitorCdaMontos = new BeanResMonitorCdaHistMontos();
		BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen = new BeanResMonitorCdaHistVolumen();
		
		BeanResMonitorCdaHistDAOSPID beanResMonitorCdaDAO = null;
		//Se hace la consulta al dao
		beanResMonitorCdaDAO = daoMonitorCDAHist.consultarCDAAgrupadasSPID(beanReqMonCDAHist,architectSessionBean);
		//Se valida codigo no exitoso
		if(!Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			//Se inicializa el monto del volumen
			initMontoVolumen(beanResMonitorCdaMontos,beanResMonitorCdaVolumen);
			//Se setean variables de bean beanResMonitorCdaBO
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
			beanResMonitorCdaBO.setCodError(beanResMonitorCdaDAO.getCodError());
			beanResMonitorCdaBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResMonitorCdaDAO.getCodError())){
			//Se setean variables de bean beanResMonitorCdaMontos
			beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(beanResMonitorCdaDAO.getMontoCDAOrdRecAplicadas());
			beanResMonitorCdaMontos.setMontoCDAPendEnviar(beanResMonitorCdaDAO.getMontoCDAPendEnviar());
			beanResMonitorCdaMontos.setMontoCDARechazadas(beanResMonitorCdaDAO.getMontoCDARechazadas());
			beanResMonitorCdaMontos.setMontoCDAConfirmadas(beanResMonitorCdaDAO.getMontoCDAConfirmadas());
			beanResMonitorCdaMontos.setMontoCDAContingencia(beanResMonitorCdaDAO.getMontoCDAContingencia());
			beanResMonitorCdaMontos.setMontoCDAEnviadas(beanResMonitorCdaDAO.getMontoCDAEnviadas());
			//Se setean variables de bean beanResMonitorCdaVolumen
			beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(beanResMonitorCdaDAO.getVolumenCDAOrdRecAplicadas());
			beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(beanResMonitorCdaDAO.getVolumenCDAPendEnviar());
			beanResMonitorCdaVolumen.setVolumenCDARechazadas(beanResMonitorCdaDAO.getVolumenCDARechazadas());
			beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(beanResMonitorCdaDAO.getVolumenCDAConfirmadas());
			beanResMonitorCdaVolumen.setVolumenCDAContingencia(beanResMonitorCdaDAO.getVolumenCDAContingencia());
			beanResMonitorCdaVolumen.setVolumenCDAEnviadas(beanResMonitorCdaDAO.getVolumenCDAEnviadas());
			//Se setean variables de bean 
			beanResMonitorCdaBO.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
			beanResMonitorCdaBO.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);			
			beanResMonitorCdaBO.setCodError(Errores.OK00000V);
		}
		//Se setea fecha de operacion
		beanResMonitorCdaBO.setFechaOperacion(beanReqMonCDAHist.getFechaOperacion());
		return beanResMonitorCdaBO;
	}

	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param beanResMonitorCdaMontos Objeto del tipo @see BeanResMonitorCdaHistMontos
	 * @param beanResMonitorCdaVolumen Objeto del tipo @see BeanResMonitorCdaHistVolumen
	 * @exception BusinessException Excepcion de negocio
	 */
	//Metodo que sirve para solicitar la detencion del servicio CDA
	private void initMontoVolumen(BeanResMonitorCdaHistMontos beanResMonitorCdaMontos,
			BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen) throws BusinessException{
		//Se crean variables de apoyo
		final String ceroPuntoCero = "0.0";
		final String cero = "0";
		//Se setean variables de bean beanResMonitorCdaVolumen
		beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(cero);
		beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(cero);
		beanResMonitorCdaVolumen.setVolumenCDARechazadas(cero);
		beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(cero);
		beanResMonitorCdaVolumen.setVolumenCDAContingencia(cero);
		beanResMonitorCdaVolumen.setVolumenCDAEnviadas(cero);
		//Se setean variables de bean beanResMonitorCdaMontos
		beanResMonitorCdaMontos.setMontoCDAConfirmadas(ceroPuntoCero);
		beanResMonitorCdaMontos.setMontoCDAContingencia(ceroPuntoCero);
		beanResMonitorCdaMontos.setMontoCDAEnviadas(ceroPuntoCero);
		beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(ceroPuntoCero);
		beanResMonitorCdaMontos.setMontoCDAPendEnviar(ceroPuntoCero);
		beanResMonitorCdaMontos.setMontoCDARechazadas(ceroPuntoCero);
	}
}
