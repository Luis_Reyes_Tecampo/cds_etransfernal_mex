/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOHistorialMsjImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 01:06:08 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOHistorialMsj;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Class BOHistorialMsjImpl.
 *
 * Clase tipo BO que implementa el metodo 
 * de negocio para realizar la consulta del
 * historial de mensajes.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOHistorialMsjImpl extends Architech implements BOHistorialMsj {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2704536781612809069L;

	/** La variable que contiene informacion con respecto a: historial. */
	@EJB
	private DAOHistorialMsj historial;
	
	/**
	 * Obtener historial.
	 *
	 * Consultar el historial de los  pagos.
	 * 
	 * @param modulo                   El objeto: modulo
	 * @param paginador                El objeto: paginador
	 * @param architechSessionBean     El objeto: architech session bean
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @return Objeto bean res historial mensajes
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResHistorialMensajes obtenerHistorial(BeanModulo modulo, BeanPaginador paginador,
			ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes)
			throws BusinessException {
		BeanHistorialMensajesDAO beanHistorialMsj=null;
		final BeanResHistorialMensajes beanResHistorialMsj= new BeanResHistorialMensajes();
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanHistorialMsj = historial.obtenerHistorial(modulo, paginador,architechSessionBean, beanReqHistorialMensajes);
		if(beanHistorialMsj.getBeanHistorialMensajes()==null  || beanHistorialMsj.getBeanHistorialMensajes().isEmpty() ) {
			beanHistorialMsj.setBeanHistorialMensajes(new ArrayList<BeanHistorialMensajes>());
		}
		/** validacion de la respuesta **/
		if(Errores.CODE_SUCCESFULLY.equals(beanHistorialMsj.getCodError()) && beanHistorialMsj.getBeanHistorialMensajes().isEmpty() ){
			beanResHistorialMsj.setCodError(Errores.ED00011V);
			beanResHistorialMsj.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResHistorialMsj.setListaHisMsj(beanHistorialMsj.getBeanHistorialMensajes());			
		}else if(Errores.CODE_SUCCESFULLY.equals(beanHistorialMsj.getCodError())){
			beanResHistorialMsj.setListaHisMsj(beanHistorialMsj.getBeanHistorialMensajes());
			beanResHistorialMsj.setTotalReg(beanHistorialMsj.getTotalReg());
			paginador.calculaPaginas(beanHistorialMsj.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResHistorialMsj.setBeanPaginador(paginador);
			beanResHistorialMsj.setCodError(Errores.OK00000V);
		}else{
			beanResHistorialMsj.setCodError(Errores.EC00011B);
			beanResHistorialMsj.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Respuesta del metodo **/
		return beanResHistorialMsj;
	}

}
