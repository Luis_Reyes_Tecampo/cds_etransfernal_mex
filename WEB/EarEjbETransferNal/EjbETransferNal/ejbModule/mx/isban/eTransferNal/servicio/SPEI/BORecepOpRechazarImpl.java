/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperActualizar;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpConsultas;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpFunciones;

/**
 * Clase del tipo BO que se encarga de realiza el rechazo de las operaciones
 *
 * @author FSW-Vector
 * @since 25/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOpRechazarImpl extends Architech implements BORecepOpRechazar{
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1377532272492477541L;
	
		
	/** Propiedad del tipo DAORecepOperacion que almacena el valor de daoRecepOperacion. */
	@EJB
	private transient DAORecepOperActualizar daoRecepOperacion2;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	
	/** La constante util. */
	public static final UtileriasRecepcionOpConsultas utilApartadas = UtileriasRecepcionOpConsultas.getInstance();
	
	/**
	 * variable de utilerias
	 */
	public static final UtileriasRecepcionOpFunciones UTIL = UtileriasRecepcionOpFunciones.getInstance();
	/**
	 * Rechazar transf spei rec.
	 *
	 * @param beanReqTranSpeiRec the bean req tran spei rec
	 * @param sessionBean the session bean
	 * @param historico the historico
	 * @return the bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec rechazarTransfSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean, boolean historico)  throws BusinessException{
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		//se inicializ bean de error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		/**inicializa funcion para helper **/
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();				
    	
    	List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
    	/**Busca los datos seleccionados**/
    	listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,HelperRecepcionOperacion.SELECCIONADO);
    	/**Condicion para ver si exite informacion**/
    	if(listBeanTranSpeiRecTemp.isEmpty()){        	
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
      		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRec;
       	}else /**condición para saber si tiene más de una opcion**/ 
       		if(listBeanTranSpeiRecTemp.size()> 1) { 
       		beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00029V);
      		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRec;
       	}
    	
    	
		/** recorre la lista de los datos seleccionados**/
		for (BeanTranSpeiRecOper beanTranSpeiRec : listBeanTranSpeiRecTemp) {
			BeanTranSpeiRecOper resul = new BeanTranSpeiRecOper();
			boolean ok = true;
			resul = continuarRecharOp( beanTranSpeiRec, historico, sessionBean);
			
			if(resul.getBeanDetalle().getError().getCodError().equals(Errores.ED00069V) || 
					resul.getBeanDetalle().getError().getCodError().equals(Errores.ED00067V)) {
				beanResConsTranSpeiRec.getBeanError().setCodError(resul.getBeanDetalle().getError().getCodError());
				beanResConsTranSpeiRec.getBeanError().setTipoError(resul.getBeanDetalle().getError().getTipoError());
				break;
			}
			
			//se asigna los datos para la bitacora
			String datosInsert = "ESTATUS_TRANSFER ="+resul.getBeanDetalle().getDetEstatus().getEstatusTransfer()+","+resul.getBeanDetalle().getBeanCambios().getMotivoDevol() 
					+","+sessionBean.getUsuario();
			//se asigna el codigo de error
			String codigo = resul.getBeanDetalle().getCodigoError();
			String dato = ConstantesRecepOperacionApartada.TYPE_UPDATE+"/rechazarTransfSpeiRec.do/"+datosInsert+"/"+codigo+"/"+datosInsert;
			// se pregunta si la consulta fue exitosa
			if(resul.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
				ok = false;
				/**genera la pista de auditoria**/
				BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
				/**inserta en bitacora **/
				boPistaAuditora.llenaPistaAuditoraAdmon(pistas, sessionBean);				
				throw new BusinessException(resul.getBeanDetalle().getError().getCodError(), resul.getBeanDetalle().getError().getMsgError());
			}else if(!resul.getBeanDetalle().getError().getCodError().equals(Errores.OK00000V)) {
				ok=false;
			}
			
			/**genera la pista de auditoria**/
			BeanPistaAuditora pistas = utilApartadas.generaPistaAuditoras(dato, ok, sessionBean, historico);
			/**inserta en bitacora **/
			boPistaAuditora.llenaPistaAuditoraAdmon(pistas, sessionBean);
			/**asigna los codigos de error**/
			beanResConsTranSpeiRec.getBeanError().setCodError(resul.getBeanDetalle().getError().getCodError());
			beanResConsTranSpeiRec.getBeanError().setTipoError(resul.getBeanDetalle().getError().getTipoError());			
		}
		
		return beanResConsTranSpeiRec;
	}
	
	
	/**
	 * Continuar rechar op.
	 *
	 * @param bean El objeto: bean
	 * @param historico El objeto: historico
	 * @param sessionBean El objeto: session bean
	 * @return Objeto bean tran spei rec
	 * @throws BusinessException La business exception
	 */
	private  BeanTranSpeiRecOper continuarRecharOp(BeanTranSpeiRecOper bean,boolean historico, ArchitechSessionBean sessionBean) throws BusinessException {
		
		BeanTranSpeiRecOper beanTranSpeiRecTemp = new BeanTranSpeiRecOper();
		// se inicializa el bean de error
		beanTranSpeiRecTemp.setBeanDetalle(new BeanDetRecepOperacion());
		beanTranSpeiRecTemp.getBeanDetalle().setError(new BeanResBase());
		beanTranSpeiRecTemp.getBeanDetalle().setDetEstatus(new BeanDetRecepOp());
		/** si el estatus de banxico es igual a LQ entra **/
		if (!"LQ".equals(bean.getBeanDetalle().getDetEstatus().getEstatusBanxico())) {
			/**mensaje para pedir que sea de tipo liquidado**/
			beanTranSpeiRecTemp.getBeanDetalle().getError().setCodError(Errores.ED00069V);
			beanTranSpeiRecTemp.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ALERT);			
		} else
			/** pregunta si el motivo de devolucion esite **/
			if (!"".equals(bean.getBeanDetalle().getBeanCambios().getMotivoDevol())) {
				/** asgina datos a la lista **/
				
				/**Vuelve a buscar los datos para regresar**/ 			
				BeanTranSpeiRecOper beanReqActTranSpeiRec = null;
				BeanResBase beanResBase = new BeanResBase();

				/**asigna valores**/
				beanTranSpeiRecTemp = bean;
				/**seta los valores para obtener el objeto**/
				beanReqActTranSpeiRec = bean;
				beanReqActTranSpeiRec.setEstatus("RE");
				
				/**llama la funcion para rechazar el registro**/
				beanResBase = daoRecepOperacion2.rechazar(beanReqActTranSpeiRec, sessionBean,historico );
				/**Asigna el codigo que halla obtenido en la funcion anterior**/
				beanTranSpeiRecTemp.getBeanDetalle().getDetEstatus().setEstatusTransfer(beanReqActTranSpeiRec.getEstatus());
				beanTranSpeiRecTemp.getBeanDetalle().setError(beanResBase);
			} else {
				/**Manda el codigo el motivo de devolucion **/
				beanTranSpeiRecTemp.getBeanDetalle().getError().setCodError(Errores.ED00067V);
				beanTranSpeiRecTemp.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ALERT);			
			}
		
   		return beanTranSpeiRecTemp;
		
	}
	
	
	
	
	
	
	
	
}