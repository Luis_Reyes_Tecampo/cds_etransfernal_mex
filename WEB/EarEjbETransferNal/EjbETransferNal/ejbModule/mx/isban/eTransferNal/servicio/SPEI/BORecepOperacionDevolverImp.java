/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperActualizar;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpHTO;

/**
 * Clase del tipo BO que se encarga de realiza la devolucion de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperacionDevolverImp extends Architech implements BORecepOperacionDevolver {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1377532272492477541L;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	/**  variable para el dao de recepcion *. */
	@EJB
	private transient DAORecepOperActualizar daoRecepOpActu;
	/**  variable para el dao bit transaccional *. */
	@EJB
	private DAOBitTransaccional dAOBitTransaccional;
	
	/** variable de utilerias. */
	public static final UtileriasRecepcionOpHTO UTIL_HTO = UtileriasRecepcionOpHTO.getInstance();
	
	
	/**
	 * Devolver transfer spei rec.
	 *
	 * @param beanReqTranSpeiRec            the bean req tran spei rec
	 * @param sessionBean            the session bean
	 * @param historico            the historico
	 * @return the bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResTranSpeiRec devolverTransferSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico) throws BusinessException {
		BeanTranSpeiRecOper beanTranSpeiRec = new BeanTranSpeiRecOper();
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();		
		
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion(); 
		
		/** Se buscan los datos seleccionados **/
		listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,
				HelperRecepcionOperacion.DEVOLUCION);
		/** condicion para ver si existen datos **/
		if (listBeanTranSpeiRecTemp.isEmpty()) {
			/** Si no hay datos se asigna los errores **/
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRec;
		}
		
		/** Se reccore la lista **/
		for (int i = 0; i < listBeanTranSpeiRecTemp.size(); i++) {
			beanTranSpeiRec = listBeanTranSpeiRecTemp.get(i);
			String[] datos = new String[2];
			datos[0]= "DV";
			datos[1]= beanReqTranSpeiRec.getBeanComun().getNombrePantalla();
			
			beanTranSpeiRec = envia(true, datos, sessionBean, historico,beanReqTranSpeiRec.getBeanComun().getNombrePantalla(),beanTranSpeiRec );
			
			String datosInsert = beanTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer();
			 String dato = ConstantesRecepOperacionApartada.TYPE_UPDATE+"/devolverTransfSpeiRec.do/"+datosInsert;
		
			if (ConstantesRecepOperacion.TRI_B0000.equals(beanTranSpeiRec.getBeanDetalle().getError().getCodError())) {
				/** Se inserta en bitacora **/
				
				agregaPistasBitacora(dato, ConstantesRecepOperacion.ENVIOESTATUS, historico, sessionBean,beanTranSpeiRec);
			} else {
				/** Se inserta en bitacora **/
				agregaPistasBitacora(dato, ConstantesRecepOperacion.NOENVIOESTATUS, historico, sessionBean,beanTranSpeiRec);
				
			}
		}
		/** Se asignan los datos del codigo de error **/
		beanResConsTranSpeiRec.getBeanError().setCodError(beanTranSpeiRec.getBeanDetalle().getError().getCodError());
		beanResConsTranSpeiRec.getBeanError().setTipoError(beanTranSpeiRec.getBeanDetalle().getError().getTipoError());
		beanResConsTranSpeiRec.getBeanComun().setReferenciaTransfer(beanTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer());
		if(beanTranSpeiRec.getBeanDetalle().getError().getMsgError().length()>0 && !beanTranSpeiRec.getBeanDetalle().getError().getCodError().equals(Errores.ETRAMA001)) {
			beanResConsTranSpeiRec.getBeanComun().setEstatus(beanTranSpeiRec.getBeanDetalle().getError().getMsgError());
		}
		beanResConsTranSpeiRec.getBeanError().setMsgError(beanTranSpeiRec.getBeanDetalle().getError().getMsgError());
		/** Regresa el resultado de las operaciones **/
		return beanResConsTranSpeiRec;
	}
	
	/**
	 * Insertar pistas bitacora.
	 *
	 * @param datos El objeto: datos
	 * @param envio El objeto: envio
	 * @param historico El objeto: historico
	 * @param sessionBean El objeto: session bean
	 * @throws BusinessException La business exception
	 */
	@Override
	public void insertarPistasBitacora(String datos,Boolean envio, boolean historico,ArchitechSessionBean sessionBean) throws BusinessException {
		BeanResGuardaBitacoraDAO beanBitacora= new BeanResGuardaBitacoraDAO();
		
		String[] dato= datos.split("/");
		String tabl="";
		/**pregunta hitorico para hacer el insert en la tabla correspondiente**/
		if(historico) {
			/**tabla apartados historicos**/
			tabl = "TRAN_SPEI_REC_HIS_OPER";
		}else {		
			/**tabla apartados del dia**/
			tabl = "TRAN_SPEI_REC_OPER";
		}
		
		/**Objetos para las Pistas auditoras**/
		BeanPistaAuditora bean = new BeanPistaAuditora();
		/** Enviamos los datos de la pista auditora**/
		bean.setNomTabla(tabl);
		bean.setOperacion(dato[0]);
		bean.setUrlController(dato[1]);
		bean.setDatoFijo(ConstantesRecepOperacionApartada.DATO_FIJO);
		bean.setDatoModi(dato[2]);
		/**Cuando la operacion es correcta**/
		if (envio) {
			bean.setEstatus(ConstantesRecepOperacionApartada.OK);
		} else {
			bean.setEstatus(ConstantesRecepOperacionApartada.NOK);
		}
		
		/**inserta en bitacora **/
		beanBitacora = boPistaAuditora.llenaPistaAuditora(bean, sessionBean);
		
		if(beanBitacora.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanBitacora.getCodError(), beanBitacora.getMsgError());
		}
	
		
	}

	/**
	 * Envia.
	 *
	 * @param esDevolucion            boolean booleano que indica si es devolucion
	 * @param datos El objeto: datos
	 * @param sessionBean            ArchitechSessionBean Objeto de la arquitectura
	 * @param historico El objeto: historico
	 * @param nombrePant El objeto: nombre pant
	 * @param beanTranSpei El objeto: bean tran spei
	 * @return BeanResConsTranSpeiRec Objeto de respuesta
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanTranSpeiRecOper envia(boolean esDevolucion, String[] datos,	ArchitechSessionBean sessionBean, boolean historico,
			String nombrePant,BeanTranSpeiRecOper beanTranSpei) throws BusinessException {
		BeanTranSpeiRecOper beanTranSpeiRec  = new BeanTranSpeiRecOper();
		beanTranSpeiRec = beanTranSpei;
		beanTranSpeiRec.getBeanDetalle().setNombrePantalla(nombrePant);
		/** Se inicializan las variables **/		
		BeanTranSpeiRecOper beanTranSpeiRecA = new BeanTranSpeiRecOper();
		
		String trama = "";
		UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
		String tramaRespuesta = "";
		String[] respuestas = null;
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion(); 
		trama = helperRecepcionOperacion.armarTrama(beanTranSpeiRec, sessionBean, esDevolucion);
		beanTranSpeiRec.getBeanDetalle().getError().setMsgError(trama);
		resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "TranTransfe", "ARQ_MENSAJERIA", sessionBean);
				
		if (ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())) {
			tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
			respuestas = tramaRespuesta.split("\\|");
			
			if (!"0".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())) {
				beanTranSpeiRec.getBeanDetalle().getBeanCambios().setMotivoDevol(helperRecepcionOperacion.getMotivoRechazo(respuestas[0]));
			}
			/**Se obtiene el mensaje de error y se actualia el registro**/
			beanTranSpeiRecA =  obtenerRespuesta(respuestas,beanTranSpei,datos, sessionBean, historico, esDevolucion, nombrePant );
			if(beanTranSpeiRecA.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
				throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
			}
		} else {
			beanTranSpeiRecA = UTIL_HTO.codigoError(beanTranSpeiRec);
		}
		/** Se regresa el objeto **/
		return beanTranSpeiRecA;
	}
	
	/**
	 * Obtener respuesta.
	 *
	 * @param respuestas El objeto: respuestas
	 * @param beanTranSpei El objeto: bean tran spei
	 * @param datos El objeto: datos
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param nombrePant El objeto: nombre pant
	 * @param esDevolucion El objeto: es devolucion
	 * @return Objeto bean tran spei rec
	 * @throws BusinessException La business exception
	 */
	private BeanTranSpeiRecOper obtenerRespuesta(String[] respuestas,BeanTranSpeiRecOper beanTranSpei, String[] datos,ArchitechSessionBean sessionBean,
			boolean historico, boolean esDevolucion, String nombrePant ) throws BusinessException {
		BeanTranSpeiRecOper beanTranSpeiRecA = new BeanTranSpeiRecOper();
		BeanTranSpeiRecOper beanTranSpeiRec = new BeanTranSpeiRecOper();
		beanTranSpeiRec = beanTranSpei;
		/**Se asgian el codigo de error**/
		if(respuestas.length>0 && ConstantesRecepOperacion.TRI_B0000.equals(respuestas[0])){
			// se llama a la funcion
			beanTranSpeiRec = actualizaTransSpeiRec(sessionBean, beanTranSpeiRec,  historico,respuestas,datos);
		}else{
			/**Se asignan valires **/
			
			//se asigna codigos de error
			beanTranSpeiRec.getBeanDetalle().setError(daoRecepOpActu.consultaError(sessionBean, respuestas[0]));
			if(!beanTranSpeiRec.getBeanDetalle().getError().getCodError().equals(Errores.EC00011B)) {
				//se asigna codigos de error
				beanTranSpeiRec.getBeanDetalle().getError().setCodError(Errores.ETRAMA001);
				beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ALERT);
			}
		}
		
		beanTranSpeiRecA = beanTranSpeiRec;
		
		return beanTranSpeiRecA;
	}
	
	
	/**
	 * Actualiza trans spei rec.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param historico El objeto: historico
	 * @param respuestas El objeto: respuestas
	 * @param datos El objeto: datos
	 * @return Objeto bean tran spei rec oper
	 */
	private BeanTranSpeiRecOper actualizaTransSpeiRec(ArchitechSessionBean sessionBean,BeanTranSpeiRecOper beanTranSpeiRec, boolean historico,
			String[] respuestas,String[] datos) {
		/** se obtienen las horas **/
		if(!daoRecepOpActu.guardaTranSpeiHorasMan(beanTranSpeiRec, sessionBean).getCodError().equals(Errores.EC00011B)) {
						
			
			/**Se asignan valires **/
			beanTranSpeiRec.getBeanDetalle().getError().setCodError(respuestas[0]);
			beanTranSpeiRec.getBeanDetalle().getDetEstatus().setRefeTransfer(respuestas[1]);
			beanTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusTransfer(datos[0]);
			
			daoRecepOpActu.actualizaTransSpeiRec(beanTranSpeiRec, sessionBean, historico);	
			beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_INFO);
			beanTranSpeiRec.getBeanDetalle().getError().setMsgError(respuestas[2]);
		}else {
			//se asigna codigos de error
			beanTranSpeiRec.getBeanDetalle().getError().setCodError(Errores.EC00011B);	
			beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}	
		
		return beanTranSpeiRec;
	}
	
	/**
	 * Agrega pistas bitacora.
	 *
	 * @param datos the datos
	 * @param envio the envio
	 * @param historico the historico
	 * @param sessionBean the session bean
	 * @param beanTranSpeiRecOper the bean tran spei rec oper
	 * @throws BusinessException the business exception
	 */
	public void agregaPistasBitacora(String datos,Boolean envio, boolean historico,ArchitechSessionBean sessionBean, BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		BeanResGuardaBitacoraDAO beanBitacora= new BeanResGuardaBitacoraDAO();
		String noAplicable = "No aplica";
		String cero = "0";
		String[] dato= datos.split("/");
		
		/**Objetos para las Pistas auditoras**/
		BeanReqInsertBitTrans bean = new BeanReqInsertBitTrans();
		/** Enviamos los datos de la pista auditora**/
		bean.getDatosGenerales().setAplicacionCanal(sessionBean.getNombreAplicacion());
		bean.getDatosGenerales().setCodCliente(noAplicable);
		bean.getDatosGenerales().setContrato(noAplicable);
		bean.getDatosGenerales().setDirIp(sessionBean.getIPCliente());
		bean.getDatosGenerales().setFchAct(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchAplica(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchProgramada(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setHoraAct(sessionBean.getUltimoAcceso());
		bean.getDatosGenerales().setHostWeb(sessionBean.getIPServidor());
		bean.getDatosGenerales().setIdSesion(sessionBean.getIdSesion());
		bean.getDatosGenerales().setIdToken(cero);
		bean.getDatosGenerales().setInstanciaWeb(sessionBean.getNombreServidor());
		bean.getDatosGenerales().setNombreArchivo("NA");
		bean.getDatosGenerales().setNumeroTitulos(cero);
		
		bean.getDatosOpe().setBancoDestino(noAplicable);
		bean.getDatosOpe().setCodigoErr(beanTranSpeiRecOper.getBeanDetalle().getError().getCodError());
		bean.getDatosOpe().setComentarios(beanTranSpeiRecOper.getBeanDetalle().getError().getMsgError());
		bean.getDatosOpe().setCtaDestino(beanTranSpeiRecOper.getBeanDetalle().getReceptor().getNumCuentaRec());
		bean.getDatosOpe().setCtaOrigen(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		bean.getDatosOpe().setDescErr("EXITO");
		if (envio) {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.OK);
			bean.getDatosOpe().setDescOper("Operacion exitosa");
		} else {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.NOK);
			bean.getDatosOpe().setDescOper("Operacion rechazada");
		}
		bean.getDatosOpe().setFchOperacion(beanTranSpeiRecOper.getBeanDetalle().getDetalleGral().getFchOperacion());
		bean.getDatosOpe().setIdOperacion("01DEVOLRECOP");
		bean.getDatosOpe().setMonto(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getMonto());
		bean.getDatosOpe().setReferencia("REFERENCIA = "+beanTranSpeiRecOper.getBeanDetalle().getDetEstatus().getRefeTransfer());
		bean.getDatosOpe().setServTran(dato[1]);
		bean.getDatosOpe().setTipoCambio(cero);
		
		/**inserta en bitacora **/
		beanBitacora = dAOBitTransaccional.guardaBitacora(bean, sessionBean);
		
		if(beanBitacora.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanBitacora.getCodError(), beanBitacora.getMsgError());
		}
	
		
	}
	
}