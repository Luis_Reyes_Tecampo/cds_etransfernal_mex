/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOTopologiaImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveMedio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveOperacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologiaDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTopologia;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.moduloSPID.DAOTopologia;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 *Clase del tipo BO que se encarga  del negocio de la configuraci�n de topologias
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOTopologiaImpl implements BOTopologia {
	
	/**
	 * 
	 */
	private static final String VALUES_BIT_FORMAT = "CVE_MEDIO_ENT=%s|CVE_OPERACION=%s|TOPO_PRI=%s|IMPORTE_MINIMO=%s";
	
	/**
	 * 
	 */
	private static final String DATO_FIJO_BIT = "CVE_MEDIO_ENT,CVE_OPERACION,TOPO_PRI";
	
	/**
	 * 
	 */
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;

	/**
	 * 
	 */
	@EJB
	private DAOTopologia daoTopologia;
	
	@Override
	public BeanResTopologia obtenerTopologias(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType){
		BeanResTopologiaDAO beanResTopologiaDAO = null;
		final BeanResTopologia beanResTopologia = new BeanResTopologia();
		
		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		String sF = getSortField(sortField);
		
		beanResTopologiaDAO = daoTopologia.obtenerTopologias(beanPaginador, architechSessionBean, sF, sortType);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError()) 
				&& beanResTopologiaDAO.getListaTopologias().isEmpty()) {
			beanResTopologia.setCodError(Errores.ED00011V);
			beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResTopologia.setListaTopologias(Collections.<BeanTopologia> emptyList());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError())){
			beanResTopologia.setListaTopologias(beanResTopologiaDAO.getListaTopologias());
			beanResTopologia.setTotalReg(beanResTopologiaDAO.getTotalReg());
			beanPaginador.calculaPaginas(beanResTopologiaDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResTopologia.setImporteTotal(daoTopologia.obtenerImporteTotalTopo(architechSessionBean).toString());
			beanResTopologia.setBeanPaginador(beanPaginador);
			beanResTopologia.setCodError(Errores.OK00000V);
			//beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
		}else{
			beanResTopologia.setCodError(Errores.EC00011B);
			beanResTopologia.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
		return beanResTopologia;
	}

	/**
	 * @param sortField Objeto del tipo String
	 * @return String
	 */
	private String getSortField(String sortField) {
		String sF = "";
		
		if ("claveMedio".equals(sortField)){
			sF = "CVE_MEDIO_ENT";
		} else if ("claveOperacion".equals(sortField)){
			sF = "CVE_OPERACION";
		} else if ("topologia".equals(sortField)){
			sF = "SUBSTR(TOPO_PRI, 1, 1)";
		} else if ("prioridad".equals(sortField)){
			sF = "SUBSTR(TOPO_PRI, 2, 1)";
		} else if ("importeMinimo".equals(sortField)){
			sF = "IMPORTE_MINIMO";
		}
		return sF;
	}
	
	@Override
	public BeanResTopologia obtenerCombosTopologia(ArchitechSessionBean architechBean){
		BeanResTopologiaDAO beanResTopologiaDAO = null;
		final BeanResTopologia beanResTopologia = new BeanResTopologia();
		
			beanResTopologiaDAO = daoTopologia.obtenerCombosTopologia(architechBean);
			if (Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError()) 
					&& beanResTopologiaDAO.getListaCveMedio().isEmpty() && beanResTopologiaDAO.getListaCveOperacion().isEmpty()) {
				beanResTopologia.setCodError(Errores.ED00011V);
				beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResTopologia.setListaCveMedio(Collections.<BeanClaveMedio> emptyList());
				beanResTopologia.setListaCveOperacion(Collections.<BeanClaveOperacion> emptyList());
			}else if(Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError())){
				beanResTopologia.setListaCveMedio(beanResTopologiaDAO.getListaCveMedio());
				beanResTopologia.setListaCveOperacion(beanResTopologiaDAO.getListaCveOperacion());
				beanResTopologia.setCodError(Errores.OK00000V);
				beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
			}else{
				beanResTopologia.setCodError(Errores.EC00011B);
				beanResTopologia.setTipoError(Errores.TIPO_MSJ_ERROR); 
			}
		return beanResTopologia;
	}
	
	@Override
	public BeanResTopologia guardarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador) {
		BeanResTopologiaDAO beanResTopologiaDAO = null;
		BeanResTopologia beanResTopologia = new BeanResTopologia();
		
			if (!daoTopologia.isRepetido(beanTopologia)) {
				beanResTopologiaDAO = daoTopologia.guardarTopologia(beanTopologia, architechBean);
				String valNvo = String.format(VALUES_BIT_FORMAT, beanTopologia.getClaveMedio(), beanTopologia.getClaveOperacion(), beanTopologia.getTopologia() + beanTopologia.getPrioridad(), beanTopologia.getImporteMinimo());
				
				if(Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError())){
					setBitacoraTrans("OK", "TRAN_SPID_TOPOL", "INSERT", "IntGuardarTopologia.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResTopologia = obtenerTopologias(beanPaginador, architechBean, "", "");
					beanResTopologia.setCodError(Errores.OK00000V);
					beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
				}else{
					setBitacoraTrans("NOK", "TRAN_SPID_TOPOL", "INSERT", "IntGuardarTopologia.do", null, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResTopologia = obtenerTopologias(beanPaginador, architechBean, "", "");
					beanResTopologia.setCodError(Errores.ED00061V);
					beanResTopologia.setTipoError(Errores.TIPO_MSJ_ALERT);
				}
			} else {				
				beanResTopologia = obtenerTopologias(beanPaginador, architechBean, "", "");
				beanResTopologia.setCodError(Errores.ED00061V);
				beanResTopologia.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		
		return beanResTopologia;
	}
	
	@Override
	public BeanResTopologia eliminarTopologia(BeanReqTopologia beanReqTopologia, 
			ArchitechSessionBean architechBean) {
		BeanResTopologiaDAO beanResTopologiaDAO = null;
		BeanResTopologia beanResTopologia = new BeanResTopologia();
		
		BeanPaginador pag = beanReqTopologia.getPaginador();
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		List<BeanTopologia> listaTopologia = filtrarSeleccionados(beanReqTopologia);
		String valAnt = "";
		
		for (BeanTopologia beanTopologia : listaTopologia) {
			beanResTopologiaDAO = daoTopologia.eliminarTopologia(beanTopologia, architechBean);
			valAnt = String.format(VALUES_BIT_FORMAT, beanTopologia.getClaveMedio(), beanTopologia.getClaveOperacion(), beanTopologia.getTopologia() + beanTopologia.getPrioridad(), beanTopologia.getImporteMinimo());
		if(!Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAO.getCodError())){
				setBitacoraTrans("NOK", "TRAN_SPID_TOPOL", "DELETE", "IntEliminarTopologia.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
				beanResTopologia = obtenerTopologias(pag, architechBean, "", "");
				beanResTopologia.setCodError(Errores.EC00011B);
				beanResTopologia.setTipoError(Errores.TIPO_MSJ_ERROR);
				return beanResTopologia;
			}
		}
		
		setBitacoraTrans("OK", "TRAN_SPID_TOPOL", "DELETE", "IntEliminarTopologia.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
		beanResTopologia = obtenerTopologias(pag, architechBean, "", "");
		beanResTopologia.setCodError(Errores.OK00000V);
		beanResTopologia.setTipoError(Errores.TIPO_MSJ_INFO);
		return beanResTopologia;
	}
	
	@Override
	public BeanResTopologia guardarEdicionTopologia(BeanTopologia beanTopologia, ArchitechSessionBean architechBean,
			BeanPaginador beanPaginador, String cveMedio, String cveOperacion, String toPri) {
		BeanResTopologiaDAO beanResTopologiaDAOSave = null;
		BeanResTopologia beanResTopologiaSave = new BeanResTopologia();
		
			if (!daoTopologia.isRepetido(beanTopologia)) {
				beanResTopologiaDAOSave = daoTopologia.obtenerTopologia(cveMedio, cveOperacion, toPri, architechBean);
				BeanTopologia beanTopologiaAnt = beanResTopologiaDAOSave.getListaTopologias().get(0);
				String valAnt = String.format(VALUES_BIT_FORMAT, 
					beanTopologiaAnt.getClaveMedio(), 
					beanTopologiaAnt.getClaveOperacion(), 
					beanTopologiaAnt.getTopologia() + beanTopologiaAnt.getPrioridad(), 
					beanTopologiaAnt.getImporteMinimo());
				
				beanResTopologiaDAOSave = daoTopologia.guardarEdicionTopologia(
						beanTopologia, architechBean, cveMedio, cveOperacion, toPri);
				
				if(Errores.CODE_SUCCESFULLY.equals(beanResTopologiaDAOSave.getCodError())){
					beanResTopologiaSave = obtenerTopologias(beanPaginador, architechBean, "", "");
					String valNvo = String.format(VALUES_BIT_FORMAT, 
						beanTopologia.getClaveMedio(), 
						beanTopologia.getClaveOperacion(), 
						beanTopologia.getTopologia() + beanTopologia.getPrioridad(), 
						beanTopologia.getImporteMinimo());
					setBitacoraTrans("OK", "TRAN_SPID_TOPOL", "UPDATE", "IntGuardarEdicionTopologia.do", valAnt, valNvo, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResTopologiaSave.setCodError(Errores.OK00000V);
					beanResTopologiaSave.setTipoError(Errores.TIPO_MSJ_INFO);

				}else{
					setBitacoraTrans("NOK", "TRAN_SPID_TOPOL", "UPDATE", "IntGuardarEdicionTopologia.do", valAnt, null, "N/A", DATO_FIJO_BIT, "", architechBean);
					beanResTopologiaSave = obtenerTopologias(beanPaginador, architechBean, "", "");
					beanResTopologiaSave.setCodError(Errores.ED00061V);
					beanResTopologiaSave.setTipoError(Errores.TIPO_MSJ_ALERT);
				}
			} else {				
			beanResTopologiaSave = obtenerTopologias(beanPaginador, architechBean, "", "");
			beanResTopologiaSave.setCodError(Errores.EC00011B);
			beanResTopologiaSave.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		return beanResTopologiaSave;
	}
	
	/**
	 * @param beanReqTopologia objeto del tipo BeanReqTopologia
	 * @return listaSeleccionados objeto del tipo List<BeanTopologia>
	 */
	private List<BeanTopologia> filtrarSeleccionados(BeanReqTopologia beanReqTopologia) {
		List<BeanTopologia> listaSeleccionados = new ArrayList<BeanTopologia>();
		
		for (BeanTopologia beanTopologia : beanReqTopologia.getListaTopologias()) {
			if (beanTopologia.isSeleccionado()) {
				listaSeleccionados.add(beanTopologia);
			}
		}		
		return listaSeleccionados;
	}
	
    
    /**
     * @param codOper Objeto del tipo String
     * @param tablaAfect Objeto del tipo String
     * @param tipoOper Objeto del tipo String
     * @param servTran Objeto del tipo String
     * @param valAnt Objeto del tipo String
     * @param valNvo Objeto del tipo String
     * @param datoModi Objeto del tipo String
     * @param datoFijo Objeto del tipo String
     * @param comentarios Objeto del tipo String
     * @param sesion Objeto del tipo String
     */
    private void setBitacoraTrans(String codOper, String tablaAfect, String tipoOper, String servTran, String valAnt,
		String valNvo, String datoModi, String datoFijo, String comentarios
		, ArchitechSessionBean sesion){
    	
    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
    		utileriasBitAdmin.createBitacoraBitAdmin(codOper, tablaAfect, tipoOper, servTran, valAnt, valNvo, 
				datoModi, datoFijo, comentarios, sesion);
    	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, sesion);
    }

	/**
	 * @return daoTopologia objeto del tipo DAOTopologia
	 */
	public DAOTopologia getDaoTopologia() {
		return daoTopologia;
	}

	/**
	 * @param daoTopologia objeto del tipo DAOTopologia
	 */
	public void setDaoTopologia(DAOTopologia daoTopologia) {
		this.daoTopologia = daoTopologia;
	}
	
	/**
	 * @return the daoBitAdministrativo
	 */
	public DAOBitAdministrativa getDaoBitAdministrativa() {
		return daoBitAdministrativa;
	}

	/**
	 * @param daoBitAdministrativa the DAOBitAdministrativa to set
	 */
	public void setDaoBitAdministrativa(DAOBitAdministrativa daoBitAdministrativa) {
		this.daoBitAdministrativa = daoBitAdministrativa;
	}
}
