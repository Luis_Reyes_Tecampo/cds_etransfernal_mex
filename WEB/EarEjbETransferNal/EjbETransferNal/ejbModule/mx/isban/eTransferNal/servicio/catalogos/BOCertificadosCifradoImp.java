/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCertificadosCifradoImp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:25:42 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOCertificadosCifrado;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCertificadoCifrado;

/**
 * Class BOCertificadosCifradoImp.
 * 
 * Clase de negocio que interpreta y sirve las 
 * peticiones del Controlador
 * 
 * Realiza peticiones al DAO para comunicarse con 
 * la Base de Datos
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCertificadosCifradoImp extends Architech implements BOCertificadosCifrado {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2529303677635061114L;

	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_CERTIF_KS";

	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "CANAL";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante TYPE_INSERT. */
	private static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	private static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_DELETE. */
	private static final String TYPE_DELETE = "DELETE";

	/** La constante TYPE_EXPORT. */
	private static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante UPDATE. */
	private static final String UPDATE = "ALIAS_KS,NUM_CERTIF,FCH_VENCIMIENTO,ALG_DIGEST,ALG_SIMETRICO,TEC_ORIGEN";
	/** La constante INSERT. */
	private static final String INSERT = "CANAL, ALIAS_KS, NUM_CERTIF, FCH_VENCIMIENTO, ALG_DIGEST, ALG_SIMETRICO, TEC_ORIGEN";
	/** La constante SELECT. */
	private static final String SELECT = "CANAL";

	/** La variable que contiene informacion con respecto a: dao certificados cifrado. */
	@EJB
	private DAOCertificadosCifrado daoCertificadosCifrado;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#consultar(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.comunes.BeanFilter, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 */
	@Override
	public BeanCertificadosCifrado consultar(ArchitechSessionBean session, BeanCertificadoCifrado beanFilter,
			BeanPaginador beanPaginador) throws BusinessException {
		//Paginacion
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		//Objetos de respuesta
		BeanCertificadosCifrado response = daoCertificadosCifrado.consultar(session, beanFilter, beanPaginador);
		//Cuando no se encuentran resultados
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getCertificados().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		//Cuando se ecuentran resultados
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		//Cuando se genera un error
		} else {
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Retorna la Respuesta
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#consultarCombos(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanCombosCertificadoCifrado consultarCombos(ArchitechSessionBean session) throws BusinessException {
		//Objeto de respuesta
		BeanCombosCertificadoCifrado response = new BeanCombosCertificadoCifrado();
		response = daoCertificadosCifrado.consultarCombo(session, response, UtileriasCertificadoCifrado.LISTA_CANALES);
		response = daoCertificadosCifrado.consultarCombo(session, response, UtileriasCertificadoCifrado.LISTA_FIRMAS);
		response = daoCertificadosCifrado.consultarCombo(session, response, UtileriasCertificadoCifrado.LISTA_CIFRADOS);
		response = daoCertificadosCifrado.consultarCombo(session, response, UtileriasCertificadoCifrado.LISTA_TECNOLOGIAS);
		//Cuando se ecuentran resultados
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			// Cuando se genera un error
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Retorna la Respuesta 
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#agregarCertificado(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado)
	 */
	@Override
	public BeanResBase agregarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado)
			throws BusinessException {
		boolean operacion = false;
		BeanResBase response = daoCertificadosCifrado.agregarCertificado(session, beanCertificado);
		//Valida resultado exitoso
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		//Control de Erorres
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDtos(beanCertificado, TYPE_INSERT);
		
		//Genera Pista
		generaPistaAuditoras(TYPE_INSERT, "agregarCertificado.do", operacion, session,dtoNuevo,"",INSERT);
		//Retorna la Respuesta
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#eliminarCertificados(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado)
	 */
	@Override
	public BeanEliminarResponse eliminarCertificados(ArchitechSessionBean session,
			BeanCertificadosCifrado beanCertificados) throws BusinessException {
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanCertificadoCifrado beanCertificado : beanCertificados.getCertificados()){
			if (beanCertificado.isSeleccionado()) {
				BeanResBase responseEliminar = daoCertificadosCifrado.eliminarCertificados(session, beanCertificado);
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(beanCertificado.getCanal());
	        		exito.append(',');
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		error.append(beanCertificado.getCanal());
	        		error.append(',');
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDtos(beanCertificado, TYPE_DELETE);
	        	generaPistaAuditoras(TYPE_DELETE, "eliminarCertificadosCifrado.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		//Retorna la Respuesta
		return response;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#editarCertificado(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado)
	 */
	@Override
	public BeanResBase editarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado)
			throws BusinessException {
		BeanResBase response = null;
		boolean operacion;
		//Paginacion
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		BeanCertificadoCifrado beanFilter = new BeanCertificadoCifrado();
		beanFilter.setCanal(beanCertificado.getCanal());
				
		//Objetos de respuesta
		BeanCertificadosCifrado bean = daoCertificadosCifrado.consultar(session, beanFilter, beanPaginador);
		String dtoAnt = obtenerDtos(bean.getCertificados().get(0), TYPE_UPDATE);
				
		response = daoCertificadosCifrado.editarCertificado(session, beanCertificado);
		//Valida Resultado Exitoso
		if(Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		}else {
			//Control de Erorres
			operacion = false;
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
				
		String dtoNuevo = obtenerDtos(beanCertificado, TYPE_UPDATE);
		
		
		//Genera Pista
		generaPistaAuditoras(TYPE_UPDATE, "editarCertificado.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		//Retorna la Respuesta
		return response;
	}

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado#exportarTodo(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.comunes.BeanFilter, int)
	 */
	@Override
	public BeanResBase exportarTodo(ArchitechSessionBean session, BeanCertificadoCifrado request, int totalRegistros)
			throws BusinessException {
		boolean operacion = true;
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		// Consulta a Exportar
		String filtroWhere = UtileriasCertificadoCifrado.getInstancia().getFiltro(request, "WHERE");
		String query = UtileriasCertificadoCifrado.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasCertificadoCifrado.ORDEN;
		// datos del reporte
		exportarRequest.setColumnasReporte(UtileriasCertificadoCifrado.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("CERTIFICADOS_CIFRADO");
		exportarRequest.setNombreRpt("CATALOGOS_CERTIFICADOS_CIFRADO_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		// Se hace la consulta con el dao para exportar las operaciones
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		//Control de Erorres
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		//Genera Pista
		generaPistaAuditoras(TYPE_EXPORT, "exportarTodosCertificados.do", operacion, session,"","", DATO_FIJO);
		//Retorna la Respuesta
		return response;
	}
	
	/**
	 * Procedimiento para el almacenamiento de las Pistas Auditoras.
	 *
	 * @param operacion            El objeto: operacion
	 * @param urlController            El objeto: url controller
	 * @param resOp            El objeto: res op
	 * @param sesion            El objeto: sesion
	 * @param datoModificado El objeto: dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		// Objetos para las Pistas auditoras
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		// Enviamos los datos de la pista auditora
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		
		// Cuando la operacion es correcta
		if (resOp) {
			beanPista.setEstatus(OK);
		} else {
			beanPista.setEstatus(NOK);
		}
		// Enviamos la peticion para el llenado de datos de la pista
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
	/**
	 * funcion para obtener los datos ant y nuevos
	 * @param ant
	 * @param accion
	 * @return
	 */
	private String obtenerDtos (BeanCertificadoCifrado ant, String accion) {
		String dtoAnt ="";
		if(ConstantesMttoMediosAut.TYPE_UPDATE.equals(accion)) {
			dtoAnt ="ALIAS_KS="+ant.getAlias()+", NUM_CERTIF="+ant.getNumCertificado()+
					", FCH_VENCIMIENTO="+ant.getFchVencimiento()+", ALG_DIGEST="+ant.getAlgDigest()+
					", ALG_SIMETRICO="+ant.getAlgSimetrico()+", TEC_ORIGEN="+ant.getTecOrigen();
		}
		
		if(ConstantesMttoMediosAut.TYPE_INSERT.equals(accion)) {
			dtoAnt ="CANAL="+ant.getCanal()+", ALIAS_KS="+ant.getAlias()+", NUM_CERTIF="+ant.getNumCertificado()+
					", FCH_VENCIMIENTO="+ant.getFchVencimiento()+", ALG_DIGEST="+ant.getAlgDigest()+
					", ALG_SIMETRICO="+ant.getAlgSimetrico()+", TEC_ORIGEN="+ant.getTecOrigen();
		}
		
		if(ConstantesMttoMediosAut.TYPE_DELETE.equals(accion) || ConstantesMttoMediosAut.TYPE_SELECT.equals(accion) ) {
			dtoAnt ="CANAL="+ant.getCanal();
		}
		
		return dtoAnt;
	}
	
	
}
