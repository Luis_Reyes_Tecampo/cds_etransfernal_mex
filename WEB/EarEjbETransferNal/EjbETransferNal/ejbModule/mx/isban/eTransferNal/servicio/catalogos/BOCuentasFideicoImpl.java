/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCuentasFideicoImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 01:03:11 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOCuentasFideico;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesCatalogos;
import mx.isban.eTransferNal.utilerias.catalogos.ConstantesMttoMediosAut;
import mx.isban.eTransferNal.utilerias.catalogos.UtileriasCuentasFideico;


/**
 * Class BOCuentasFideicoImpl.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de identificacion de cuentas de fideicomiso en SPID.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCuentasFideicoImpl extends Architech implements BOCuentasFideico {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1013696484847675721L;

	/** La variable que contiene informacion con respecto a: dao cuentas. */
	@EJB
	private DAOCuentasFideico daoCuentas;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;
	
	/** La variable que contiene informacion con respecto a: utilerias. */
	private UtileriasCuentasFideico utilerias = UtileriasCuentasFideico.getInstancia();
	
	/** La constante UPDATE. */
	private static final String UPDATE = "NUM_CUENTA";
	/** La constante INSERT. */
	private static final String INSERT = "NUM_CUENTA";
	
	/** La constante SELECT. */
	private static final String SELECT = "NUM_CUENTA";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "NA";
	
	/** La constante STR_NOMTABLA. */
	private static final String STR_NOMTABLA = "TRAN_CTAS_FIDEICO";
	
	
	
	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador El objeto: beanPaginador --> Objeto para realizar el paginado
	 * @param session objeto: bean sesion --> El objeto session
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador, ArchitechSessionBean session)
			throws BusinessException {
		BeanPaginador paginador = beanPaginador;
		if(paginador==null){
			paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		/** Ejecucion de la peticion al DAO **/
		BeanCuentas response = daoCuentas.consultar(beanCuenta, beanPaginador, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())
				&& response.getCuentas().isEmpty()) {
			response.getBeanError().setCodError(Errores.ED00011V);
			response.getBeanError().setMsgError(Errores.DESC_ED00011V);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
		} else if (Errores.CODE_SUCCESFULLY.equals(response.getBeanError().getCodError())) {
			paginador.calculaPaginas(response.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			response.setBeanPaginador(paginador);
			response.getBeanError().setCodError(Errores.OK00000V);
		} else {
			/** Seteo de errores **/
			response.getBeanError().setCodError(Errores.EC00011B);
			response.getBeanError().setCodError(Errores.DESC_EC00011B);
			response.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Agregar registro.
	 * 
	 * Metodo publico utilizado para agregar un registro
	 * en el catalogo.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto con los datos del nuevo registro
	 * @param session objeto: bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase agregar(BeanCuenta beanCuenta, ArchitechSessionBean session) throws BusinessException {
		boolean operacion = false;
		/** Ejecucion de la peticion al DAO **/
		BeanResBase response = daoCuentas.agregar(beanCuenta, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setMsgError(Errores.DESC_OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		}else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setMsgError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanCuenta);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_INSERT, "agregarCuentaFideicomiso.do", operacion, session,dtoNuevo,"",INSERT);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Editar registro.
	 * 
	 * Metodo publico utilizado para editar un registro
	 * del catalogo.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto con los datos del nuevo registro
	 * @param anterior El objeto: anterior --> Objeto con los datos del registro a editar
	 * @param session objeto: bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase editar(BeanCuenta beanCuenta, BeanCuenta datoAnterior, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		BeanResBase response = null;
		boolean operacion = false;
		BeanPaginador beanPaginador = new BeanPaginador();
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		String dtoAnt = obtenerDatos(datoAnterior);
		/** Ejecucion de la peticion al DAO **/
		response = daoCuentas.editar(beanCuenta, datoAnterior, session);
		/** Validacion de la respuesta del DAO **/
		if (Errores.CODE_SUCCESFULLY.equals(response.getCodError())) {
			operacion = true;
			/** Seteo de errores **/
			response.setCodError(Errores.OK00000V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
			response.setMsgError(Errores.DESC_OK00000V);
		} else if(Errores.ED00130V.equals(response.getCodError())) {
			/** Seteo de errores **/
			response.setCodError(Errores.ED00130V);
			response.setMsgError(Errores.DESC_ED00130V);
			response.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			/** Seteo de errores **/
			response.setCodError(Errores.EC00011B);
			response.setCodError(Errores.DESC_EC00011B);
			response.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		String dtoNuevo = obtenerDatos(beanCuenta);
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_UPDATE, "editarCuentaFideicomiso.do", operacion, session,dtoNuevo,dtoAnt,UPDATE);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Eliminar registro.
	 * 
	 * Metodo publico utilizado para eliminar un registro
	 * del catalogo.
	 *
	 * @param beanCuentas El objeto: bean cuentas --> Registros a eliminar
	 * @param session objeto: bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanEliminarResponse eliminar(BeanCuentas beanCuentas, ArchitechSessionBean session)
			throws BusinessException {
		/** Declaracion del objeto de salida **/
		final BeanEliminarResponse response = new BeanEliminarResponse();
		boolean operacion;
		StringBuilder exito = new StringBuilder(), error = new StringBuilder();
		for(BeanCuenta cuenta : beanCuentas.getCuentas()){
			if (cuenta.isSeleccionado()) {
				/** Ejecucion de la peticion al DAO **/
				BeanResBase responseEliminar = daoCuentas.eliminar(cuenta, session);
				/** Validacion de la respuesta del DAO **/
	        	if(Errores.CODE_SUCCESFULLY.equals(responseEliminar.getCodError())){
	        		operacion = true;
	        		exito.append(cuenta.getCuenta());
	        		exito.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.OK00000V);
	        		response.setMsgError(Errores.DESC_OK00000V);
	    			response.setTipoError(Errores.TIPO_MSJ_INFO);
	        	} else {
	        		operacion = false;
	        		exito.append(cuenta.getCuenta());
	        		error.append(',');
	        		/** Seteo de errores **/
	        		response.setCodError(Errores.EC00011B);
	        		response.setCodError(Errores.DESC_EC00011B);
	        		response.setTipoError(Errores.TIPO_MSJ_ERROR);
	        	}
	        	String dtoAnt = obtenerDatos(cuenta);
	        	generaPistaAuditoras(ConstantesCatalogos.TYPE_DELETE, "eliminarCuentaFideicomiso.do", operacion, session,"",dtoAnt,SELECT);
			}
			response.setEliminadosCorrectos(exito.toString());
			response.setEliminadosErroneos(error.toString());
		}
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * Exportar todos.
	 * 
	 * Realiza la inserccion para el proceso batch de bitacora.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto de filtrado de registros
	 * @param totalRegistros El objeto: total registros --> Parametro con el total de registros actual
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto bean res base --> Objeto de respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	@Override
	public BeanResBase exportarTodo(BeanCuenta beanCuenta, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException {
		boolean operacion = true;
		/** Declaracion del objeto de salida **/
		BeanResBase response = new BeanResBase();
		BeanExportarTodo exportarRequest = new BeanExportarTodo();
		String filtroWhere = utilerias.getFiltro(beanCuenta, "WHERE");
		String query = UtileriasCuentasFideico.CONSULTA_EXPORTAR_TODO.replaceAll("\\[FILTRO_WH\\]", filtroWhere)
				+ UtileriasCuentasFideico.ORDEN;
		/** LLenado de datos del objeto exportar  **/
		exportarRequest.setColumnasReporte(UtileriasCuentasFideico.COLUMNAS_REPORTE);
		exportarRequest.setConsultaExportar(query);
		exportarRequest.setEstatus("PE");
		exportarRequest.setModulo("CATALOGOS");
		exportarRequest.setSubModulo("CUENTAS_FIDEICO");
		exportarRequest.setNombreRpt("CATALOGOS_CUENTAS_FIDEICOMISO_");
		exportarRequest.setTotalReg(totalRegistros + StringUtils.EMPTY);
		/** Ejecucion de la peticion al DAO **/
		BeanResBase resExportarTodo = daoExportar.exportarTodo(exportarRequest, session);
		/** Valida error **/
		if (Errores.EC00011B.equals(resExportarTodo.getCodError())) {
			operacion = false;
		}
		/** Seteo de errores **/
		response.setCodError(resExportarTodo.getCodError());
		response.setMsgError(resExportarTodo.getMsgError());
		response.setTipoError(resExportarTodo.getTipoError());
		/** Invocacion al metodo que envia la pista **/
		generaPistaAuditoras(ConstantesCatalogos.TYPE_EXPORT, "exportarTodoCuentasFideicomiso.do", operacion, session,"","", DATO_FIJO);
		/** Retorno de la respuesta del BO**/
		return response;
	}

	/**
	 * generaPistaAuditoras
	 * 
	 * 
	 * Procedimiento para el almacenamiento de las Pistas Auditoras
	 * en la BD.
	 *
	 *
	 * @param operacion            El objeto: operacion --> Parametro del tipo de operacion que se realiza
	 * @param urlController            El objeto: url controller --> Parametro con el .do de la accion
	 * @param resOp            El objeto: res op --> Parametro de respuesta de la operacion
	 * @param sesion            El objeto: sesion--> Parametro de session
	 * @param dtoNuevo El objeto: dto nuevo --> Parametro de valor registro nuevo
	 * @param dtoAnterior El objeto: dto anterior --> Parametro del valor registro anterior
	 * @param dtoModificado El objeto: dto modificado --> Parametro del dato modificado
	 */
	private void generaPistaAuditoras(String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion,  String dtoNuevo,String dtoAnterior, String dtoModificado) {
		/** Delcaracion del objeto de Pista  **/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Seteo de datos **/
		beanPista.setNomTabla(STR_NOMTABLA);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(dtoModificado);
		/** Valida el tipo de operacion **/
		if(!operacion.equals(ConstantesMttoMediosAut.TYPE_SELECT)) {			
			beanPista.setDtoAnterior(dtoAnterior);
			beanPista.setDtoNuevo(dtoNuevo);
		}else {
			beanPista.setDtoNuevo(new Date().toString());
		}
		if (resOp) {
			beanPista.setEstatus(ConstantesCatalogos.OK);
		} else {
			beanPista.setEstatus(ConstantesCatalogos.NOK);
		}
		/** Invocacion al metodo que envia la pista **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}

	/**
	 * Obtener datos.
	 * 
	 * Obtiene los datos del bean a String
	 *
	 *
	 * @param ant El objeto: ant --> Objeto anterior
	 * @return Objeto string --> Respuesta devuelta
	 */
	private String obtenerDatos(BeanCuenta ant) {
		String dtoAnt ="";
		/** Obtencion de cadena cuando es UDPDATE **/
			dtoAnt ="NUM_CUENTA=".concat(ant.getCuenta());
		/** Retorno de cadena **/
		return dtoAnt;
	}
}
