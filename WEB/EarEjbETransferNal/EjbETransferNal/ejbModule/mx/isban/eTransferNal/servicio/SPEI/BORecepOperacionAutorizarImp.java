/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018     SMEZA 	   VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.servicio.SPEI;

import java.util.ArrayList;
/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Modulo SPEI - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAODetRecepOperacion;
import mx.isban.eTransferNal.dao.SPEI.DAORecepOperacionApartadas;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;
import mx.isban.eTransferNal.utilerias.SPEI.UtileriasRecepcionOpFunciones;

/**
 * Clase del tipo BO que se encarga de realizar la autorizacion de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperacionAutorizarImp extends Architech implements BORecepOperacionAutorizar {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8041256489091907412L;

	/**  variable para el dao de detrecepcion *. */
	@EJB
	private transient DAODetRecepOperacion daoDetRecepOp;
	
	/** La variable que contiene informacion con respecto a: dao apartadas. */
	@EJB
	private transient DAORecepOperacionApartadas daoApartadas;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BORecepOperacionDevolver boRecepDevol;
	/** La variable que contiene informacion con respecto a: dao bit transaccional. */
	@EJB
	private DAOBitTransaccional dAOBitTransaccional;
	
	/** La variable que contiene informacion con respecto a: util. */
	private UtileriasRecepcionOpFunciones util = UtileriasRecepcionOpFunciones.getInstance();
	
	/** La variable que contiene informacion con respecto a: NOAPLICABLE. */
	private static final String NOAPLICABLE = "No aplica";
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.SPEI.BORecepOperacionAutorizar#transferirSpeiRec(mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsTranSpeiRec, mx.isban.agave.commons.beans.ArchitechSessionBean, boolean)
	 */
	@Override
	public BeanResTranSpeiRec transferirSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico) throws BusinessException {
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		beanResConsTranSpeiRec.setListBeanTranSpeiRecNOK(new ArrayList<BeanTranSpeiRecOper>());
		beanResConsTranSpeiRec.setListBeanTranSpeiRecOK(new ArrayList<BeanTranSpeiRecOper>());
		BeanResTranSpeiRec listaRespuestas = new BeanResTranSpeiRec();
		listaRespuestas.setListBeanTranSpeiRecNOK(new ArrayList<BeanTranSpeiRecOper>());
		listaRespuestas.setListBeanTranSpeiRecOK(new ArrayList<BeanTranSpeiRecOper>());
		
		String cuentas = "";	
		/** Se inicializan las variables **/		
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = null;
		//se llama al helper de spei
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion(); 
				
		/** se buscan los datos seleccionados **/
		listBeanTranSpeiRecTemp = helperRecepcionOperacion.filtraSelecionados(beanReqTranSpeiRec,HelperRecepcionOperacion.SELECCIONADO);
		
		/**Se valida la si trae informacion la lista **/		
		if(validarLista(listBeanTranSpeiRecTemp )!= null) {
			return validarLista(listBeanTranSpeiRecTemp );
		}
		
		BeanTranSpeiRecOper beanTranSpeiRecTemp = new BeanTranSpeiRecOper();
		for (BeanTranSpeiRecOper beanTranSpeiRec : listBeanTranSpeiRecTemp) {
			
			beanResConsTranSpeiRec = existeOperBeanTranSpeiRecacion(sessionBean, beanReqTranSpeiRec, beanTranSpeiRec, beanResConsTranSpeiRec);
			if(beanResConsTranSpeiRec.getBeanError().getCodError().equals(Errores.ED00133V)){
				break;
			}
			cuentas = obtenerCuenta(beanTranSpeiRec,beanResConsTranSpeiRec,sessionBean);

			if (cuentas.length()==0) {
				listaRespuestas = obtieneResultadoApartado(beanTranSpeiRec, beanReqTranSpeiRec, beanResConsTranSpeiRec,
						 sessionBean, historico, cuentas );
			}else {				
				listaRespuestas.getListBeanTranSpeiRecNOK().add(beanTranSpeiRec);
			}
			
			beanTranSpeiRecTemp.getBeanDetalle().setError(listaRespuestas.getBeanError());
			/** se llama la funcion para los mesajes de error**/
			beanResConsTranSpeiRec = util.obtenerRespuesta(listaRespuestas.getListBeanTranSpeiRecOK(),listaRespuestas.getListBeanTranSpeiRecNOK(),
					listBeanTranSpeiRecTemp,beanResConsTranSpeiRec,cuentas,beanTranSpeiRecTemp );

			
		}
		/** Regresa el resultado de las operaciones **/
		return beanResConsTranSpeiRec;
	}
	
   
	/**
	 * Obtiene resultado apartado.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param cuentas El objeto: cuentas
	 * @return Objeto bean res tran spei rec
	 * @throws BusinessException La business exception
	 */
	private BeanResTranSpeiRec obtieneResultadoApartado(BeanTranSpeiRecOper beanTranSpeiRec, BeanReqTranSpeiRec beanReqTranSpeiRec,BeanResTranSpeiRec beanResConsTranSpeiRec,
			ArchitechSessionBean sessionBean,boolean historico,String cuentas ) throws BusinessException {
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusTransfer("TR");
		BeanTranSpeiRecOper beanTranSpeiRecTemp= 	new BeanTranSpeiRecOper();
		String[] datos = new String[2];
		datos[0]= "TR";
		datos[1]=beanReqTranSpeiRec.getBeanComun().getNombrePantalla();
		
		/** Genera la trama **/
		beanTranSpeiRecTemp = boRecepDevol.envia(false, datos, sessionBean, historico, beanReqTranSpeiRec.getBeanComun().getNombrePantalla(),beanTranSpeiRec);
		// se asigna los datos para insetar en el apartado
		 String datosInsert = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion()
				 // se asigna la institucion
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
				 // se asigna el onst orden
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd()+","+
				 // se asigna el folio del paquete
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()
				 // se asigna el folgio pago
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago()
				 // se asigna el usuario logueado
				 +","+sessionBean.getUsuario();
		 String dato = ConstantesRecepOperacionApartada.TYPE_INSERT+"/transferirSpeiRec.do/"+datosInsert;
		 
		/** se inserta en bitacora **/
		if (ConstantesRecepOperacion.TRI_B0000.equals(beanTranSpeiRecTemp.getBeanDetalle().getError().getCodError())) {
			beanResConsTranSpeiRec.getListBeanTranSpeiRecOK().add(beanTranSpeiRecTemp);
			/** Se inserta en bitacora para pistas de auditoria **/
			agregaPistasBitacora( dato, ConstantesRecepOperacion.ENVIOESTATUS,  historico, sessionBean,beanTranSpeiRec); 
			insertarApartado(false,sessionBean,beanReqTranSpeiRec, beanTranSpeiRec); 

		} else {
			/** Se asignan en bitacora las pistas de auditoria **/
			beanResConsTranSpeiRec.getListBeanTranSpeiRecNOK().add(beanTranSpeiRec);
			/**genera la pista de auditoria**/
			agregaPistasBitacora( dato, ConstantesRecepOperacion.NOENVIOESTATUS,  historico, sessionBean,beanTranSpeiRec);
			String msj = obtenerMensajeEror( beanTranSpeiRec,  cuentas, beanTranSpeiRecTemp ) ;
			beanTranSpeiRecTemp.getBeanDetalle().getError().setMsgError(msj);
			
		}
		beanResConsTranSpeiRec.setBeanError(beanTranSpeiRecTemp.getBeanDetalle().getError());
		
		return beanResConsTranSpeiRec;
	}
	
	
	/**
	 * Existe oper bean tran spei recacion.
	 *
	 * @param sessionBean El objeto: el bean de la sesion
	 * @param beanReqTranSpeiRec El objeto: el objeto con la informacion de la peticion a validar
	 * @param beanTranSpeiRec El objeto: el objeto con la informacion especifica de la operacion
	 * @param beanResConsTranSpeiRec El objeto: el objeto con la informacion de respuesta (Permanencia)
	 * @return Objeto bean El nuevo objeto de respuesta
	 */
	private BeanResTranSpeiRec existeOperBeanTranSpeiRecacion(ArchitechSessionBean sessionBean, BeanReqTranSpeiRec beanReqTranSpeiRec, BeanTranSpeiRecOper beanTranSpeiRec, 
			BeanResTranSpeiRec beanResConsTranSpeiRec){
		// inicializa variable error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		/**se pregunta si viene de la pantalla historicas para asignar la fecha de operacion de la BD**/
		if(beanReqTranSpeiRec.getBeanComun().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO) ||
				beanReqTranSpeiRec.getBeanComun().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SPEI_HTO)) {
			// se inicializa la variable para respuesta si existe registro
			BeanResBase existe = new BeanResBase();
			//llama la funcion para saber si hay un registro existente
			existe = daoDetRecepOp.existeOperacion(sessionBean, beanTranSpeiRec);
			/**Se pregunta si existe la operacion **/
			if(existe.getCodError().equals(Errores.ED00133V)) {
				beanResConsTranSpeiRec.getBeanError().setCodError(existe.getCodError());
				beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);	
				return beanResConsTranSpeiRec;
			}
			// se asigna el mensaje de error
			beanResConsTranSpeiRec.setBeanError(existe);
			
		}else{
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.OK00000V);
		}
		
		return beanResConsTranSpeiRec;
	}
	/**
	 * Validar lista.
	 *
	 * @param listBeanTranSpeiRec El objeto: lista de operaciones a validar a validar
	 * @return Objeto bean de respuesta
	 */
	private BeanResTranSpeiRec validarLista(List<BeanTranSpeiRecOper> listBeanTranSpeiRec ) {
		// s inicialzia los objetos
		BeanResTranSpeiRec beanResConsTranSpeiRec= new BeanResTranSpeiRec();
		//inicializa bean error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		/** Pregunta si vienen datos **/
		if (listBeanTranSpeiRec.isEmpty()) {
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
		}else /**condición para saber si tiene más de una opcion**/ 
       		if(listBeanTranSpeiRec.size()> 1) { 
       		beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00029V);
      		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			
       	}else {
       		beanResConsTranSpeiRec=null;
       	}
		
		return beanResConsTranSpeiRec;
	}
	
	/**
	 * Obtener mensaje eror.
	 *
	 * @param beanTranSpeiRec El objeto: del cual se valida el mensaje de error
	 * @param cuentas El objeto: el obejto con la informacion de las cuentas
	 * @param beanTranSpeiRecTemp El objeto: bean temporal a validar
	 * @return Objeto string
	 */
	private String obtenerMensajeEror(BeanTranSpeiRecOper beanTranSpeiRec, String cuentas,BeanTranSpeiRecOper beanTranSpeiRecTemp ) {
		String msj ="";
		if(cuentas.length()==0) {
			msj = beanTranSpeiRecTemp.getBeanDetalle().getError().getCodError()+": "+ beanTranSpeiRecTemp.getBeanDetalle().getError().getMsgError();
		}else {
			 msj = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion() + "|" + beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago() + "|"
					+ beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete();
		}
		 return msj;
		 
	}
	
	/**
	 * Obtener cuenta.
	 *
	 * @param beanTranSpeiRec El objeto: del qie se extrae la informacion de las cuentas
	 * @param beanResConsTranSpei El objeto: el ojeto de respuesta anterior
	 * @param sessionBean El objeto: bean de session
	 * @return Objeto string que contiene la cuenta
	 * @throws BusinessException La business exception
	 */
	private String obtenerCuenta(BeanTranSpeiRecOper beanTranSpeiRec,BeanResTranSpeiRec beanResConsTranSpei,ArchitechSessionBean sessionBean) throws BusinessException {
		String cuentas="";
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		beanResConsTranSpeiRec=beanResConsTranSpei;
		if ("03".equals(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec()) || "10".equals(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec())) {
			beanResConsTranSpeiRec = obtieneCuenta(beanTranSpeiRec, sessionBean);
			if (!Errores.OK00001V.equals(beanResConsTranSpeiRec.getBeanError().getCodError())) {
				beanTranSpeiRec.getBeanDetalle().setError(new BeanResBase());
				beanTranSpeiRec.getBeanDetalle().setError(beanResConsTranSpeiRec.getBeanError());				
				cuentas = cuentas.concat(beanResConsTranSpeiRec.getBeanComun().getNumCuentaRec());				
			}
		}
		return cuentas;
	}
	
	
	 /**
 	 * Metodo que permite obtener la cuenta.
 	 *
 	 * @param beanTranSpeiRecTemp Bean del tipo BeanTranSpeiRec que tiene la informacion de la cuenta
 	 * @param sessionBean Objeto del tipo ArchitechSessionBean
 	 * @return BeanTranSpeiRecOper Bean de respuesta
 	 * @throws BusinessException La business exception
 	 */
    public BeanResTranSpeiRec obtieneCuenta(BeanTranSpeiRecOper beanTranSpeiRecTemp, ArchitechSessionBean sessionBean) throws BusinessException {
    	/**Se inicializan las variables**/
    	String trama;
    	String [] respuestas = null;
    	String tramaRespuesta ="";
    	ResBeanEjecTranDAO resBeanEjecTranDAO = null;
    	UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
    	//se inicializa objeto
    	BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
    	//se llama al helper de spei
    	HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
    	// arma la trama    	
    	trama = helperRecepcionOperacion.armaTramaCuenta(beanTranSpeiRecTemp, sessionBean); 
    	/**Se manda llamar al mq para enviar la trama**/
    	resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "Obtienecuenta", "ARQ_MENSAJERIA", sessionBean);
    	/**Se* analiza el codigo de error*/
    	if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
    		/**Se envia la trama **/
    		tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();    		
    		/**Se separa la respuesta **/
    		respuestas = tramaRespuesta.split("\\|");
			if(respuestas.length>0 && ConstantesRecepOperacion.TRI_B0000.equals(respuestas[0])){
				beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
				beanResConsTranSpeiRec.getBeanError().setCodError(Errores.OK00001V);
				beanTranSpeiRecTemp.setNumCuentaTran(respuestas[2]);				
			}else{
        		beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00085V);
         		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
        		beanResConsTranSpeiRec.getBeanComun().setNumCuentaRec(beanTranSpeiRecTemp.getBeanDetalle().getReceptor().getNumCuentaRec());
            	return beanResConsTranSpeiRec;
			}
    	}else{
    		/**Se regresa el BusinessException **/
    		beanResConsTranSpeiRec.getBeanError().setCodError(Errores.EC00011B);
    		beanResConsTranSpeiRec.getBeanError().setMsgError(Errores.DESC_EC00011B);
    		beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ERROR);
    		throw new BusinessException(beanResConsTranSpeiRec.getBeanError().getCodError(), beanResConsTranSpeiRec.getBeanError().getMsgError());
    	}
    	 /**Se regresa el objeto **/	
    	return beanResConsTranSpeiRec;
    }
	
    /**
     * Insertar apartado.
     *
     * @param historico El objeto: historico
     * @param sessionBean El objeto: session bean
     * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
     * @param beanTranSpeiRec El objeto: bean tran spei rec
     * @throws BusinessException La business exception
     */
	private void insertarApartado(boolean historico,ArchitechSessionBean sessionBean,BeanReqTranSpeiRec beanReqTranSpeiRec,BeanTranSpeiRecOper beanTranSpeiRec) throws BusinessException {
		if(historico) {
			insertaApartado(sessionBean, historico,beanReqTranSpeiRec.getBeanComun().getNombrePantalla(),beanTranSpeiRec);
		}
	}
	
	
	
	
	/**
	 * Inserta apartado.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @throws BusinessException La business exception
	 */
	private void insertaApartado(ArchitechSessionBean sessionBean, boolean historico,String nombrePantalla, BeanTranSpeiRecOper beanTranSpeiRec ) throws BusinessException {
		BeanResTranSpeiRec beanApartada = new BeanResTranSpeiRec();
		beanApartada= daoApartadas.apartaOperaciones(beanTranSpeiRec, sessionBean, historico, nombrePantalla);
		// se asigna los datos para insetar en el apartado
		 String datosInsert = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion()
				 // se asigna la institucion
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion()
				 // se asigna el onst orden
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd()+","+
				 // se asigna el folio del paquete
				 beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()
				 // se asigna el folgio pago
				 +","+beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago()
				 // se asigna el usuario logueado
				 +","+sessionBean.getUsuario();
		 String dato = ConstantesRecepOperacionApartada.TYPE_INSERT+"/transferirSpeiRec.do/"+datosInsert;
		 boolean ok = true;
		 if(Errores.EC00011B.equals(beanApartada.getBeanError().getCodError())) {					
				ok= false;
		 }
		 /**genera la pista de auditoria**/
		 boRecepDevol.insertarPistasBitacora(dato, ok, historico,sessionBean);
		
	}
	
	/**
	 * Agrega pistas bitacora.
	 *
	 * @param datos the datos
	 * @param envio the envio
	 * @param historico the historico
	 * @param sessionBean the session bean
	 * @param beanTranSpeiRecOper the bean tran spei rec oper
	 * @throws BusinessException the business exception
	 */
	public void agregaPistasBitacora(String datos,Boolean envio, boolean historico,ArchitechSessionBean sessionBean, BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException {
		BeanResGuardaBitacoraDAO beanBitacora= new BeanResGuardaBitacoraDAO();
		String cero = "0";
		String[] dato= datos.split("/");
		
		/**Objetos para las Pistas auditoras**/
		BeanReqInsertBitTrans bean = new BeanReqInsertBitTrans();
		/** Enviamos los datos de la pista auditora**/
		bean.getDatosGenerales().setAplicacionCanal(sessionBean.getNombreAplicacion());
		bean.getDatosGenerales().setCodCliente(NOAPLICABLE);
		bean.getDatosGenerales().setContrato(NOAPLICABLE);
		bean.getDatosGenerales().setDirIp(sessionBean.getIPCliente());
		bean.getDatosGenerales().setFchAct(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchAplica(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setFchProgramada(sessionBean.getFechaAcceso());
		bean.getDatosGenerales().setHoraAct(sessionBean.getUltimoAcceso());
		bean.getDatosGenerales().setHostWeb(sessionBean.getIPServidor());
		bean.getDatosGenerales().setIdSesion(sessionBean.getIdSesion());
		bean.getDatosGenerales().setIdToken(cero);
		bean.getDatosGenerales().setInstanciaWeb(sessionBean.getNombreServidor());
		bean.getDatosGenerales().setNombreArchivo("NA");
		bean.getDatosGenerales().setNumeroTitulos(cero);
		
		bean.getDatosOpe().setBancoDestino(NOAPLICABLE);
		bean.getDatosOpe().setCodigoErr(beanTranSpeiRecOper.getBeanDetalle().getError().getCodError());
		bean.getDatosOpe().setComentarios(beanTranSpeiRecOper.getBeanDetalle().getError().getMsgError());
		bean.getDatosOpe().setCtaDestino(beanTranSpeiRecOper.getBeanDetalle().getReceptor().getNumCuentaRec());
		bean.getDatosOpe().setCtaOrigen(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		bean.getDatosOpe().setDescErr("EXITO");
		if (envio) {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.OK);
			bean.getDatosOpe().setDescOper("Operacion exitosa");
		} else {
			bean.getDatosOpe().setEstatusOperacion(ConstantesRecepOperacionApartada.NOK);
			bean.getDatosOpe().setDescOper("Operacion rechazada");
		}
		bean.getDatosOpe().setFchOperacion(beanTranSpeiRecOper.getBeanDetalle().getDetalleGral().getFchOperacion());
		bean.getDatosOpe().setIdOperacion("01DEVOLRECOP");
		bean.getDatosOpe().setMonto(beanTranSpeiRecOper.getBeanDetalle().getOrdenante().getMonto());
		bean.getDatosOpe().setReferencia("REFERENCIA = "+beanTranSpeiRecOper.getBeanDetalle().getDetEstatus().getRefeTransfer());
		bean.getDatosOpe().setServTran(dato[1]);
		bean.getDatosOpe().setTipoCambio(cero);

		
		/**inserta en bitacora **/
		beanBitacora = dAOBitTransaccional.guardaBitacora(bean, sessionBean);
		
		if(beanBitacora.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanBitacora.getCodError(), beanBitacora.getMsgError());
		}
	
		
	}
	

}