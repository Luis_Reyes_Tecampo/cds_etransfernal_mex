/**
 * Isban Mexico
 * Clase: BOCatParticipantesSPIDImpl.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 09/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanParticipantesSPID;
import mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes;
import mx.isban.eTransferNal.beans.catalogos.BeanResParticipantesSPID;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOCatParticipantesSPID;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCatParticipantesSPIDImpl extends Architech implements BOCatParticipantesSPID{

	/**  Propiedad del tipo long que almacena el valor de serialVersionUID*/
	private static final long serialVersionUID = 4809345722226477356L;
	
	/**
	 * DAO para participantes
	 */
	@EJB
	private DAOCatParticipantesSPID daoParticipantes;

	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitAdministrativa daoBitacora;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCatParticipantesSPID#consultaParticipantes(java.lang.String, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResParticipantesSPID consultaParticipantes(BeanReqParticipantes beanConsReqInst,
			ArchitechSessionBean architectSessionBean) { 
		 BeanPaginador paginador = null;
		 List<BeanParticipantesSPID> listBean = null;
		 BeanResParticipantesSPID beanRes = new BeanResParticipantesSPID();
		 paginador = beanConsReqInst.getPaginador();	    
		 
		 if(paginador == null){
			 paginador = new BeanPaginador();
			 beanConsReqInst.setPaginador(paginador);
	      } 
		 
		 paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
	     beanRes = daoParticipantes.consultaParticipantes(beanConsReqInst, architectSessionBean);
	     listBean = beanRes.getListParticipantes();
	     if(Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError()) && listBean.isEmpty()){
	    	 beanRes.setCodError(Errores.ED00011V);
	    	 beanRes.setTipoError(Errores.TIPO_MSJ_INFO);
	    	 beanRes.setListParticipantes(beanRes.getListParticipantes());
	      }else if(Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError())){
	    	  beanRes.setListParticipantes(beanRes.getListParticipantes());          
	    	  beanRes.setTotalReg(beanRes.getTotalReg());
	          paginador.calculaPaginas(beanRes.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
	          beanRes.setBeanPaginador(paginador);
	          beanRes.setCodError(Errores.OK00000V);
	      }else if(Errores.ED00011V.equals(beanRes.getCodError())){
	    	  beanRes.setCodError(Errores.ED00011V);
	    	  beanRes.setTipoError(Errores.TIPO_MSJ_INFO); 
	      }else{
	    	  beanRes.setCodError(Errores.EC00011B);
	    	  beanRes.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }
		return beanRes;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCatParticipantesSPID#guardarCatalogo(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase guardarParticipante(BeanReqParticipantes request,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase beanResAlta = new BeanResBase();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
		
		beanResAlta = daoParticipantes.guardarParticipante(request, architectSessionBean);
		
		if(Errores.OK00000V.equals(beanResAlta.getCodError())){
			beanResAlta.setCodError(Errores.OK00003V);
			beanResAlta.setTipoError(Errores.TIPO_MSJ_INFO);
	    	 
	    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("OK", "TRAN_SPID_INTERME", "ALTA", "altaCatParticipantes.do",
	    							" ", "CVE_INTER:"+request.getCveIntermediario()+" | "+
	    									"USUARIO:"+architectSessionBean.getUsuario()+" | "
	    							, " ", "CVE_INTERME", "ALTA CATALOGO", architectSessionBean);
		        
	    	daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
	    }else{
	    	beanResAlta.setCodError(Errores.EC00011B);
	    	beanResAlta.setTipoError(Errores.TIPO_MSJ_ERROR); 
	    }
			
	return beanResAlta;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCatParticipantesSPID#consultaPartiAlta(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResParticipantesSPID consultaPartiAlta(BeanReqParticipantes beanConsReqInst,
			ArchitechSessionBean architectSessionBean) {
		BeanPaginador paginador = null;
		 List<BeanParticipantesSPID> listBean = null;
		 BeanResParticipantesSPID beanRes = new BeanResParticipantesSPID();
		 paginador = beanConsReqInst.getPaginador();
	     
		 if(paginador == null){
	    	  paginador = new BeanPaginador();
	    	  beanConsReqInst.setPaginador(paginador);
		 }
		 
		 paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
	     beanRes = daoParticipantes.consultaPartiAlta(beanConsReqInst, architectSessionBean);
	     listBean = beanRes.getListParticipantes();
	     if(Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError()) && listBean.isEmpty()){
	    	 beanRes.setCodError(Errores.ED00011V);
	    	 beanRes.setTipoError(Errores.TIPO_MSJ_INFO);
	    	 beanRes.setListParticipantes(beanRes.getListParticipantes());
	      }else if(Errores.CODE_SUCCESFULLY.equals(beanRes.getCodError())){
	    	  beanRes.setListParticipantes(beanRes.getListParticipantes());          
	    	  beanRes.setTotalReg(beanRes.getTotalReg());
	          paginador.calculaPaginas(beanRes.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
	          beanRes.setBeanPaginador(paginador);
	          beanRes.setCodError(Errores.OK00000V);
	      }else if(Errores.ED00011V.equals(beanRes.getCodError())){
	    	  beanRes.setCodError(Errores.ED00011V);
	    	  beanRes.setTipoError(Errores.TIPO_MSJ_INFO); 
	      }else{
	    	  beanRes.setCodError(Errores.EC00011B);
	    	  beanRes.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }
		return beanRes;
	}

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.catalogos.BOCatParticipantesSPID#eliminaParticipante(mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes, mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResBase eliminaParticipante(BeanReqParticipantes request, ArchitechSessionBean architectSessionBean) {
		BeanResBase beanResElimina = new BeanResBase();
		BeanReqInsertBitAdmin beanReqInsertBitacora;
		UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();

		beanResElimina = daoParticipantes.eliminaParticipante(request, architectSessionBean);		
		if(Errores.OK00000V.equals(beanResElimina.getCodError())){
			beanResElimina.setCodError(Errores.OK00003V);
			beanResElimina.setTipoError(Errores.TIPO_MSJ_INFO);
	    	 
	    	beanReqInsertBitacora = utileriasBitAdmin.createBitacoraBitAdmin("OK", "TRAN_SPID_INTERME", "ELIMINA", "catalogoParticipantesSPID.do",
	    							"Clave Intermediario: "+request.getCveIntermediario(), " ", " ", "CVE_INTERME", "ELIMINA CATALOGO", architectSessionBean);
		        
	    	daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
	    }else{
	    	beanResElimina.setCodError(Errores.EC00011B);
	    	beanResElimina.setTipoError(Errores.TIPO_MSJ_ERROR); 
	    }
		return beanResElimina;
	}

}
