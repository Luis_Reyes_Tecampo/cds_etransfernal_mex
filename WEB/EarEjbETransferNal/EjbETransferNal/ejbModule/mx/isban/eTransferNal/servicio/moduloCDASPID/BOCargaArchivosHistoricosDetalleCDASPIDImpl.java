/**
 * Clase: BOImpl que implementa a BOCargaArchivosHistricosDetalleCADSPID de la pantalla
 * Monitor Carga Archivos Historicos Detalle CADSPID
 * 
 *  @author Vector
 *  @version 1.0
 *  @since Enero 2016
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanDetalleArchivoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOCargaArchivosHistoricosDetalleCDASPID;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class BOCargaArchivosHistoricosDetalleCDASPIDImpl.
 */
//The Class BOCargaArchivosHistoricosDetalleCDASPIDImpl.
@Remote(BOCargaArchivosHistoricosDetalleCDASPID.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCargaArchivosHistoricosDetalleCDASPIDImpl extends Architech 
		implements BOCargaArchivosHistoricosDetalleCDASPID{

	/**
	 * La variable de serializacion
	 */
	//La variable de serializacion
	private static final long serialVersionUID = 8421604202515919776L;
	
	/**
	 * La variable de instancia daoCargaArchivosHistoricosDetalleCDSPID
	 */
	//La variable de instancia daoCargaArchivosHistoricosDetalleCDSPID
	@EJB
	private DAOCargaArchivosHistoricosDetalleCDASPID 
	daoCargaArchivosHistoricosDetalleCDASPID;
	
	/**
	 * La variable de instancia daoBitacoraAdmon
	 */
	//La variable de instancia daoBitacoraAdmon
	@EJB
	private DAOBitacoraAdmon daoBitacoraAdmon;

	//Metodo para consultar detalles de un archivo
	@Override
	public BeanResMonitorCargaArchHistCADSPID consutarDetallesArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			BeanPaginador paginador, ArchitechSessionBean sessionBean) throws BusinessException {
		debug("<<<Iniciando el servicio para la consulta de pantalla: "
				+"Carga Archivos Historicos Detalle CDASPID>>>");
		//Se crea objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta = 
				new BeanResMonitorCargaArchHistCADSPID();
		 paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		 
		 //Se hace la consulta al dao
		respuesta = daoCargaArchivosHistoricosDetalleCDASPID.
				consutarDetallesArchivo(datosEntranda, paginador, getArchitechBean());
		
		//Se valida codigo de error
		if(Errores.EC00011B.equals(respuesta.getCodigoError())) {
			throw new BusinessException(respuesta.getCodigoError(),respuesta.getMensajeError());
		} else {
			//Se valida que la lista tenga datos
			if(respuesta.getListaDetalle()==null ||
					respuesta.getListaDetalle().isEmpty()) {
				info("No hay datos que mostrar");
				throw new BusinessException(Errores.ED00026V,Errores.TIPO_MSJ_ERROR);
			} else{
				//Si pasa todas las validaciones se devuelve un codigo de error exitoso y la lista de objetos
				respuesta.setCodigoError(Errores.CODE_SUCCESFULLY);
				respuesta.setMensajeError(Errores.TIPO_MSJ_INFO);
				//Se setea el paginado y la lista de resultados
				respuesta.setListaResultado(respuesta.getListaResultado());
				paginador.calculaPaginas(respuesta.getRegistrosTotales(), HelperDAO.REGISTROS_X_PAGINA.toString());
				respuesta.setPaginador(paginador);
			}
		}
		//Se devuelve objeto respuesta
		return respuesta;
	}

	//Metodo para generar archivo historico
	@Override
	public BeanResMonitorCargaArchHistCADSPID generarArchivoHistorico(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean) throws BusinessException {
		
		//Se crean objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		BeanReqInsertBitacora bitacora = new BeanReqInsertBitacora();
		
		//Se crean objetos de apoyo
		Utilerias utilerias = Utilerias.getUtilerias();
		String regitros = "0";
		
		//Se hace la consulta al dao
		respuesta = daoCargaArchivosHistoricosDetalleCDASPID.
				generarArchivoHistorico(datosEntranda, getArchitechBean());
		//Se valida que codigo de error sea exitoso
		if(Errores.CODE_SUCCESFULLY.equals(respuesta.getCodigoError())) {
			//Si se obtiene una respuesta se eliminan caracteres innecesarios
			if(respuesta.getRegistrosArchivo()!=null) {
				regitros = respuesta.getRegistrosEncontrados().replace(",", ",");
			}
			//Se genera registro exitoso en bitacora
			bitacora = utilerias.creaBitacora("generarArchivoHistorico.do", 
					regitros, "", new Date(), "PENDIENTE", "GENARCHHIS", "TRAN_SPID_CTG_CDA", "ID_PETICION", 
	        		Errores.OK00000V, "TRANSACCION EXITOSA", "UPDATE", datosEntranda.getIdArchivo());
			daoBitacoraAdmon.guardaBitacora(bitacora, sessionBean);
		} else {
			//Se genera registro no exitoso en bitacora
			bitacora = utilerias.creaBitacora("generarArchivoHistorico.do", "0", 
        			"",new Date(), "PENDIENTE", "GENARCHHIS", "TRAN_SPID_CTG_CDA", "ID_PETICION", 
	        		Errores.EC00011B, "No hay comunicaci\u00F3n serv.", "UPDATE","");			
			daoBitacoraAdmon.guardaBitacora(bitacora, sessionBean);
			//Se setea un codigo de error EC00011B
			throw new BusinessException(Errores.EC00011B, respuesta.getMensajeError());
		}
		//Se devuelve objeto respuesta
		return respuesta;
	}

	//Metodo para eliminar el archivo seleccionado
	@Override
	public BeanResMonitorCargaArchHistCADSPID eliminarSelecionArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean) throws BusinessException {
		
		//Se crean objetos de respuesta
		BeanResMonitorCargaArchHistCADSPID respuesta = 
				new BeanResMonitorCargaArchHistCADSPID();
		BeanResMonitorCargaArchHistCADSPID resObtenida = null;
		List<BeanDetalleArchivoCDASPID> listaEliminar = 
				new ArrayList<BeanDetalleArchivoCDASPID>();
		
		//Se obtiene la lista de objetos seleccionados
		for (BeanDetalleArchivoCDASPID detalle:datosEntranda.getListaDetalle()) {
			if(detalle.isSeleccionado()) {
				listaEliminar.add(detalle);
			}
		}
		
		//Se valida que haya seleccionado algo el usuario
		if(listaEliminar.isEmpty()) {
			//Se vuelve a consultar el detalle del archivo y se devuelve un error 
			resObtenida = consutarDetallesArchivo(datosEntranda, 
					new BeanPaginador(), getArchitechBean());
			respuesta.setListaDetalle(resObtenida.getListaDetalle());
			respuesta.setPaginador(resObtenida.getPaginador());
			respuesta.setRegistrosArchivo(resObtenida.getRegistrosArchivo());
			respuesta.setRegistrosEncontrados(resObtenida.getRegistrosEncontrados());
			//Se setea un codigo de error
			throw new BusinessException(Errores.ED00026V, Errores.ED00026V);
		} else {
			//Se setea la lista de archivos a eliminar
			datosEntranda.setListaDetalle(listaEliminar);
			//Se ejecuta el eliminado en el dao
			BeanResMonitorCargaArchHistCADSPID confirmacionEliminacion =
					daoCargaArchivosHistoricosDetalleCDASPID.eliminarSelecionArchivo(
							datosEntranda, getArchitechBean());
			respuesta.setCodigoError(Errores.OK00000V);
			respuesta.setMensajeError(Errores.TIPO_MSJ_INFO);
			//Si se confirma la eliminacion se setean los valores en el objeto respuesta
			if(Errores.CODE_SUCCESFULLY.equals(confirmacionEliminacion.getCodigoError())) {
				//Se vuelve a hacer la consulta de los detalles
				resObtenida = consutarDetallesArchivo(
						datosEntranda, new BeanPaginador(), getArchitechBean());
				respuesta.setListaDetalle(resObtenida.getListaDetalle());
				respuesta.setRegistrosArchivo(resObtenida.getRegistrosArchivo());
				respuesta.setRegistrosEncontrados(resObtenida.getRegistrosEncontrados());
				respuesta.setPaginador(resObtenida.getPaginador());
			}
		}
		//Se devuelve la respuesta
		return respuesta; 
	}
	/**
	 * Metodo para obtener la variable de instancia al DAO
	 * 
	 * @return La variable de instacia 
	 * daoCargaArchivosHistoricosDetalleCDASPID
	 */
	//Metodo para obtener la variable de instancia al DAO
	public DAOCargaArchivosHistoricosDetalleCDASPID 
			getDaoCargaArchivosHistoricosDetalleCDASPID() {
		return daoCargaArchivosHistoricosDetalleCDASPID;
	}

	/**
	 * Metodo para establecer la variable de instancia al DAO
	 * 
	 * @param daoCargaArchivosHistoricosDetalleCDASPID
	 * La variable de instancia daoCargaArchivosHistoricosDetalleCDASPID
	 */
	//Metodo para establecer la variable de instancia al DAO
	public void setDaoCargaArchivosHistoricosDetalleCDASPID(
			DAOCargaArchivosHistoricosDetalleCDASPID 
			daoCargaArchivosHistoricosDetalleCDASPID) {
		this.daoCargaArchivosHistoricosDetalleCDASPID = 
				daoCargaArchivosHistoricosDetalleCDASPID;
	}


	/**
	 * Metodo para acceder a la variable de instancia
	 * 
	 * @return La variable de instancia daoBitacoraAdmon
	 */
	//Metodo para acceder a la variable de instancia
	public DAOBitacoraAdmon getDaoBitacoraAdmon() {
		return daoBitacoraAdmon;
	}


	/**
	 * Metodo para establecer a la variable de instancia
	 * 
	 * @param daoBitacoraAdmon La variable daoBitacoraAdmon
	 */
	//Metodo para establecer a la variable de instancia
	public void setDaoBitacoraAdmon(DAOBitacoraAdmon daoBitacoraAdmon) {
		this.daoBitacoraAdmon = daoBitacoraAdmon;
	}



}
