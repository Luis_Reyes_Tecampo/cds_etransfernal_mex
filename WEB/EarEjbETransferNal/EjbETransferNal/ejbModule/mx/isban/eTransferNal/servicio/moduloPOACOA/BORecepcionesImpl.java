/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepcionesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:24:12 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAORecepciones;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportar;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class BORecepcionesImpl.
 *
 * Clase tipo BO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepcionesImpl extends Architech implements BORecepciones{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2336702220287934520L;

	/** La variable que contiene informacion con respecto a: dao recepciones. */
	@EJB
	private DAORecepciones daoRecepciones;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La constante COLUMNAS_REPORTE. */
	private static final String COLUMNAS_REPORTE = "Topologia,Tipo de pago, Estatus Banxico, Estatus Trasferencia, Fecha de Operacion,"
			.concat("Fecha de Captura, Institucion Ordenante, Intermediario Ordenante, Numero de cuenta Receptora,")
			.concat("Monto, Folio Paquete, Folio Pago, Motivo de Deovolucion")
			.concat("Codigo de error, Numero de cuenta Tran");
	
	/** La constante CONSULTA_RECEPCIONES. */
	private static final StringBuilder CONSULTA_RECEPCIONES = new StringBuilder().append("SELECT ")
			.append(" TOPOLOGIA || ',' || TIPO_PAGO || ',' || ESTATUS_BANXICO || '|' || ESTATUS_TRANSFER || ',' || FCH_OPERACION || ',' ||")
			.append(" FCH_CAPTURA || ',' || CVE_INST_ORD || ',' || CVE_INTERME_ORD || ',' || FOLIO_PAQUETE || ',' || FOLIO_PAGO || ',' || ")
			.append(" NUM_CUENTA_REC || ',' || MONTO || ',' ||")
			.append(" MOTIVO_DEVOL || ',' || CODIGO_ERROR || ',' || NUM_CUENTA_TRAN")
			.append(" FROM TRAN_SPID_REC");
	
	/** La constante utilsExportar. */
	private static final UtileriasExportar utilsExportar = UtileriasExportar.getUtils();
	
	/**
	 * Obtener consulta recepciones.
	 *
	 * Consultar los registros disponibles 
	 * 
	 * @param beanPaginador              El objeto: bean paginador
	 * @param modulo                     El objeto: modulo SPID o SPEI
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param cveInst                    El objeto: cve inst
	 * @param fch                        El objeto: fch
	 * @param architechSessionBean       El objeto: architech session bean
	 * @return Objeto bean res consulta recepciones
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResConsultaRecepciones obtenerConsultaRecepciones(BeanPaginador beanPaginador, BeanModulo modulo,
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch,
			ArchitechSessionBean session) throws BusinessException {
		BeanResConsultaRecepcionesDAO beanResConsultaRecepDAO = null;
		final BeanResConsultaRecepciones beanResConsultaRecep = new BeanResConsultaRecepciones();
		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}

		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResConsultaRecepDAO = daoRecepciones.obtenerConsultaRecepciones(pag, modulo,
				beanReqConsultaRecepciones, cveInst, fch, session);

		if (beanResConsultaRecepDAO.getListaConsultaRecepciones() == null) {
			beanResConsultaRecepDAO.setListaConsultaRecepciones(new ArrayList<BeanConsultaRecepciones>());
		}
		/** validacion del resultado de la operacion **/
		if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaRecepDAO.getCodError())
				&& beanResConsultaRecepDAO.getListaConsultaRecepciones().isEmpty()) {
			beanResConsultaRecep.setCodError(Errores.ED00011V);
			beanResConsultaRecep.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsultaRecep
					.setListaConsultaRecepciones(beanResConsultaRecep.getListaConsultaRecepciones());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaRecepDAO.getCodError())) {
			beanResConsultaRecep
					.setListaConsultaRecepciones(beanResConsultaRecepDAO.getListaConsultaRecepciones());
			beanResConsultaRecep.setTotalReg(beanResConsultaRecepDAO.getTotalReg());
			pag.calculaPaginas(beanResConsultaRecepDAO.getTotalReg(),
					HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsultaRecep.setBeanPaginador(pag);
			beanResConsultaRecep.setImporteTotal(
					daoRecepciones.obtenerImporteTotal(
							modulo,beanReqConsultaRecepciones.getTipoOrden(), cveInst, fch, session).toString());
			beanResConsultaRecep.setCodError(Errores.OK00000V);
		} else {
			beanResConsultaRecep.setCodError(Errores.EC00011B);
			beanResConsultaRecep.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		/** Rertono del objeto de  respuesta **/
		return beanResConsultaRecep;
	}

	/**
	 * Exportar todos.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 *  
	 * @param beanResConsultaRecepciones El objeto: bean res consulta recepciones
	 * @param modulo                     El objeto: modulo SPID o SPEI
	 * @param reqSession                 El objeto: req session
	 * @param nombre                     El objeto: nombre
	 * @param session                    El objeto: session
	 * @return Objeto bean res consulta recepciones
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResConsultaRecepciones exportarTodo(BeanResConsultaRecepciones beanResConsultaRecepciones,
			BeanModulo modulo, BeanSessionSPID reqSession, String nombre, ArchitechSessionBean session) throws BusinessException {
		BeanResBase responseDAO = null;
		BeanResConsultaRecepciones response = new BeanResConsultaRecepciones();
			BeanExportarTodo exportarRequest = new BeanExportarTodo();
			exportarRequest.setColumnasReporte(COLUMNAS_REPORTE);
			exportarRequest.setConsultaExportar(ValidadorMonitor.cambiaQuery(modulo, completaQuery(modulo, beanResConsultaRecepciones, reqSession).toString()));
			exportarRequest.setEstatus("PE");
			exportarRequest.setModulo(utilsExportar.getModulo(modulo));
			exportarRequest.setSubModulo(utilsExportar.getMonitor(modulo).concat("_".concat(nombre)));
			exportarRequest.setNombreRpt(nombre.concat(modulo.getTipo().toUpperCase().concat("_".concat(modulo.getModulo().toUpperCase().concat("_")))));
			exportarRequest.setTotalReg(beanResConsultaRecepciones.getTotalReg() + StringUtils.EMPTY);
			responseDAO = daoExportar.exportarTodo(exportarRequest, session);
			if (!Errores.EC00011B.equals(responseDAO.getCodError())) {
				response.setNombreArchivo(responseDAO.getMsgError());
				response.setCodError(responseDAO.getCodError());
				response.setMsgError(responseDAO.getMsgError());
				response.setTipoError(responseDAO.getTipoError());
			}
		/** Retorno de la respuesta **/
		return response;
	}

	/**
	 * Completa query.
	 *
	 * @param modulo El objeto: modulo
	 * @param beanResConsultaRecepciones El objeto: bean res consulta recepciones
	 * @param reqSession El objeto: req session
	 * @return Objeto string builder
	 */
	private StringBuilder completaQuery(BeanModulo modulo,BeanResConsultaRecepciones beanResConsultaRecepciones,BeanSessionSPID reqSession) {
		StringBuilder builder = new StringBuilder();
		builder.append(CONSULTA_RECEPCIONES).append(" WHERE ");
		if("PA".equalsIgnoreCase(beanResConsultaRecepciones.getTipoOrden())){
			builder.append(" ((ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ");
			builder.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ");
		}else if("TR".equalsIgnoreCase(beanResConsultaRecepciones.getTipoOrden())){
			builder.append(" (ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ') ");
			
		}else if("RE".equalsIgnoreCase(beanResConsultaRecepciones.getTipoOrden())){
			builder.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL) ");
		}
		builder.append("AND  CVE_MI_INSTITUC='")
		.append(reqSession.getCveInstitucion()).append("' AND FCH_OPERACION=TO_DATE('").append(reqSession.getFechaOperacion())
				.append("', 'DD-MM-YYYY') ");
		String origen = ValidadorMonitor.getOrigen(modulo);
		origen = origen.replace("TMS.", "");
		builder.append(origen);
		/** Retorno de cadena creada **/
		return builder;
		
	}

	/**
	 * Obtener detalle recepcion.
	 *
	 * Consultar del detalle de la
	 * recpecion seleccionada.
	 * 
	 * @param beanLlave            El objeto: bean llave
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res recepcion SPID
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResRecepcionSPID obtenerDetalleRecepcion(BeanSPIDLlave beanLlave, BeanModulo modulo,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResRecepcionDAO beanResRecepcionDet = null;
		final BeanResRecepcionSPID beanResRecepcionSPIDDet = new BeanResRecepcionSPID();
		
		beanResRecepcionDet = daoRecepciones.obtenerDetRecepcion(beanLlave, modulo, architechSessionBean);
		if (Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDet.getCodError()) 
				&& beanResRecepcionDet.getBeanReceptorSPID() == null) {
			beanResRecepcionSPIDDet.setCodError(Errores.ED00011V);
			beanResRecepcionSPIDDet.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDet.getCodError())){
			beanResRecepcionSPIDDet.setBeanReceptorSPID(beanResRecepcionDet.getBeanReceptorSPID());   
			beanResRecepcionSPIDDet.setCodError(Errores.OK00000V);
		}else{
			beanResRecepcionSPIDDet.setCodError(Errores.EC00011B);
			beanResRecepcionSPIDDet.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}			
		/** Retorno del metodo **/
		return beanResRecepcionSPIDDet;
	}
	
}
