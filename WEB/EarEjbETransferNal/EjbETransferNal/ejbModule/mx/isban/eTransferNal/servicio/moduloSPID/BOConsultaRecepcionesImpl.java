/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaRecepcionesImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.dao.moduloSPID.DAOConsultaRecepciones;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperRecepcionOperacion;

/**
 * Clase del tipo BO que se encarga del negocio del catalogo de Consulta
 * Recepciones
 **/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)

public class BOConsultaRecepcionesImpl extends Architech implements BOConsultaRecepciones {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -7582325210276440795L;

	/** Referencia privada al dao */
	@EJB
	private DAOConsultaRecepciones daoConsultaRecepciones;

	

	

	/**
	 * Metodo que sirve para consultar las recepciones
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 * @param cveInst
	 *            cadena con la clave de institucion
	 * @param fch
	 *            cadena con la fecha de operacion
	 * @return beanResConsultaRecepciones objeto del tipo BeanResConsultaRecepciones
	 * @exception BusinessExceptionException de negocio        
	 */
	@Override
	public BeanResConsultaRecepciones obtenerConsultaRecepciones(BeanPaginador paginador,
			ArchitechSessionBean architechSessionBean, BeanReqConsultaRecepciones beanReqConsultaRecepciones,
			String cveInst, String fch) throws BusinessException {
		BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO = null;
		final BeanResConsultaRecepciones beanResConsultaRecepciones = new BeanResConsultaRecepciones();
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}

		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResConsultaRecepcionesDAO = daoConsultaRecepciones.obtenerConsultaRecepciones(pag,
				architechSessionBean, beanReqConsultaRecepciones, cveInst, fch);

		if (beanResConsultaRecepcionesDAO.getListaConsultaRecepciones() == null) {
			beanResConsultaRecepcionesDAO.setListaConsultaRecepciones(new ArrayList<BeanConsultaRecepciones>());
		}

		if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaRecepcionesDAO.getCodError())
				&& beanResConsultaRecepcionesDAO.getListaConsultaRecepciones().isEmpty()) {
			beanResConsultaRecepciones.setCodError(Errores.ED00011V);
			beanResConsultaRecepciones.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsultaRecepciones
					.setListaConsultaRecepciones(beanResConsultaRecepciones.getListaConsultaRecepciones());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaRecepcionesDAO.getCodError())) {
			beanResConsultaRecepciones
					.setListaConsultaRecepciones(beanResConsultaRecepcionesDAO.getListaConsultaRecepciones());
			beanResConsultaRecepciones.setTotalReg(beanResConsultaRecepcionesDAO.getTotalReg());
			pag.calculaPaginas(beanResConsultaRecepcionesDAO.getTotalReg(),
					HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsultaRecepciones.setBeanPaginador(pag);
			beanResConsultaRecepciones.setImporteTotal(
					daoConsultaRecepciones.obtenerImporteTotal(
							architechSessionBean,beanReqConsultaRecepciones.getTipoOrden(), cveInst, fch).toString());
			beanResConsultaRecepciones.setCodError(Errores.OK00000V);
		} else {
			beanResConsultaRecepciones.setCodError(Errores.EC00011B);
			beanResConsultaRecepciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return beanResConsultaRecepciones;
	}

	/**
	 * Metodo que sirve para exportar todo de las recepciones
	 * 
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param cveInst
	 *            cadena con la clave de institucion
	 * @param fch
	 *            cadena con la fecha de operacion
	 * @return beanResConsultaRecepciones Objeto del tipo
	 *         BeanResConsultaRecepciones
	 * @exception BusinessExceptionException de negocio        
	 */
	@Override
	public BeanResConsultaRecepciones exportarTodoConsultaRecepciones(
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, ArchitechSessionBean architechSessionBean,
			String cveInst, String fch) throws BusinessException {
		BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO = null;
		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		
		BeanPaginador paginador = beanReqConsultaRecepciones.getPaginador();
		
		if (paginador == null){
			  paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		beanResConsultaRecepciones = obtenerConsultaRecepciones(paginador,
				architechSessionBean, beanReqConsultaRecepciones, cveInst, fch);
		beanReqConsultaRecepciones.setTotalReg(beanResConsultaRecepciones.getTotalReg());
		beanResConsultaRecepcionesDAO = daoConsultaRecepciones
				.exportarTodoConsultaRecepciones(beanReqConsultaRecepciones, architechSessionBean, cveInst, fch);

		if (Errores.CODE_SUCCESFULLY.equals(beanResConsultaRecepcionesDAO.getCodError())) {
			beanResConsultaRecepciones.setCodError(Errores.OK00002V);
			beanResConsultaRecepciones.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResConsultaRecepciones.setNombreArchivo(beanResConsultaRecepcionesDAO.getNombreArchivo());
		} else {
			beanResConsultaRecepciones.setCodError(Errores.EC00011B);
			beanResConsultaRecepciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}

		return beanResConsultaRecepciones;
	}

	/**
	 * Metodo que sirve para enviar las recepciones
	 * 
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 * @param architechSessionBean
	 *            Objeto del tipo ArchitechSessionBean
	 * @param cveInst
	 *            cadena con la clave de institucion
	 * @param fch
	 *            cadena con la fecha de operacion
	 * @return beanResConsultaRecepciones Objeto del tipo
	 *         BeanResConsultaRecepciones
	 * @exception BusinessExceptionException de negocio       
	 */
	@Override
	public BeanResConsultaRecepciones enviarConsultaRecepciones(BeanReqConsultaRecepciones beanReqConsultaRecepciones,
			ArchitechSessionBean architechSessionBean, String cveInst, String fch) throws BusinessException {
		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		HelperRecepcionOperacion helperRecepcionOperacion = new HelperRecepcionOperacion();
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones = helperRecepcionOperacion
				.filtraSelecionadosConsultaRecepciones(beanReqConsultaRecepciones,
						HelperRecepcionOperacion.SELECCIONADO);
		if (listaBeanConsultaRecepciones.isEmpty()) {
			beanResConsultaRecepciones = obtenerConsultaRecepciones(beanReqConsultaRecepciones.getPaginador(),
					architechSessionBean, beanReqConsultaRecepciones, cveInst, fch);
			beanResConsultaRecepciones.setCodError(Errores.ED00068V);
			beanResConsultaRecepciones.setTipoError(Errores.TIPO_MSJ_ALERT);
		}

		return beanResConsultaRecepciones;
	}

	/**
	 * @return the daoConsultaRecepciones objeto del tipo DAOConsultaRecepciones
	 */
	public DAOConsultaRecepciones getDaoConsultaRecepciones() {
		return daoConsultaRecepciones;
	}

	/**
	 * @param daoConsultaRecepciones the DAOConsultaRecepciones to set
	 */
	public void setDaoConsultaRecepciones(DAOConsultaRecepciones daoConsultaRecepciones) {
		this.daoConsultaRecepciones = daoConsultaRecepciones;
	}

	
	@Override
	public BeanResRecepcionSPID obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean) throws BusinessException{
		BeanResRecepcionDAO beanResRecepcionDAO = null;
		final BeanResRecepcionSPID beanResRecepcionSPID = new BeanResRecepcionSPID();
		
		beanResRecepcionDAO = daoConsultaRecepciones.obtenerDetRecepcion(beanLlave, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDAO.getCodError()) 
				&& beanResRecepcionDAO.getBeanReceptorSPID() == null) {
			beanResRecepcionSPID.setCodError(Errores.ED00011V);
			beanResRecepcionSPID.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDAO.getCodError())){
			beanResRecepcionSPID.setBeanReceptorSPID(beanResRecepcionDAO.getBeanReceptorSPID());   
			beanResRecepcionSPID.setCodError(Errores.OK00000V);
		}else{
			beanResRecepcionSPID.setCodError(Errores.EC00011B);
			beanResRecepcionSPID.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}				
		return beanResRecepcionSPID;
	}
}
