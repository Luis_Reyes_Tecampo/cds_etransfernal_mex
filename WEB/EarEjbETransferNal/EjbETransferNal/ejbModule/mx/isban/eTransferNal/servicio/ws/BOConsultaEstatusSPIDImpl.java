/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaEstatusSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicio.ws;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONObject;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.constantes.modSPID.ConstantesConsultaEstatusSPID;
import mx.isban.eTransferNal.constantes.modSPID.ConstantesSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOConsulaEstatusSPID;
import mx.isban.eTransferNal.servicios.ws.BOConsultaEstatusSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Implementacion de negocio que tiene la funcionalidad de consultar el estatus de operaciones SPID
 * @author cchong
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaEstatusSPIDImpl  extends Architech implements BOConsultaEstatusSPID{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -3482163109970067685L;

	/**
	 * Propiedad del tipo DAOConsulaEstatusSPID que almacena el valor de dAOConsulaEstatusSPID
	 */
	@EJB
	private DAOConsulaEstatusSPID dAOConsulaEstatusSPID;
	
	@Override
	public ResEntradaStringJsonDTO consultaEstatusDetSPID(
			EntradaStringJsonDTO reqConsultaEstatusDet) {
		Utilerias util = Utilerias.getUtilerias();
		String nomCampo ="";
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		info("Entro a la llamada consultaEstatusDetSPID"+reqConsultaEstatusDet.getCadenaJson());
		boolean flag = true;
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		Map<String, String> salida = new HashMap<String, String>();
		StringBuilder trama = new StringBuilder();
		trama.append("ESTATDET");
		String json = reqConsultaEstatusDet.getCadenaJson();
		JSONObject jsonObject = JSONObject.fromString(json);  
		Map<String,String> mapConsultaEstatus =  (Map<String,String>)JSONObject.toBean(jsonObject, Map.class);
		for (int index =0; index<ConstantesConsultaEstatusSPID.CAMPOS.length && flag;index++){
			flag = armaTrama(mapConsultaEstatus,trama, index);
			nomCampo = ConstantesConsultaEstatusSPID.CAMPOS[index];
		}
		if(flag){
			resBeanEjecTranDAO = dAOConsulaEstatusSPID.ejecutar(trama.toString());
			if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
				String tramaRetorno = resBeanEjecTranDAO.getTramaRespuesta();
				if (tramaRetorno.contains(ConstantesSPID.PIPE)) {
					final String[] valores = util.split(tramaRetorno, '|');
					if(isValidRetorno(valores)){
						returnSalida(valores,salida);
					}
				} else {
					returnCodigo(tramaRetorno,salida);
				}
			}else{
				returnTramaNula(salida);
			}
		}else{
			salida.put(ConstantesSPID.CAMPO_COD_ERROR, "ED00092V");
			salida.put(ConstantesSPID.CAMPO_REFERENCIA,"0" );
			salida.put(ConstantesSPID.CAMPO_ESTATUS, "ER");
			salida.put(ConstantesSPID.CAMPO_DESCRIPCION, "Error en el formato o la longitud del campo:"+nomCampo);	
		}
		jsonObject = JSONObject.fromMap(salida);
		resEntradaStringJsonDTO.setJson(jsonObject.toString());
		info("Salio de la llamada consultaEstatusDetSPID"+resEntradaStringJsonDTO.getJson());
		return resEntradaStringJsonDTO;
	}
	
	/**
	 * @param mapConsultaEstatus Objeto del tipo Map<String,String>
	 * @param trama String con la trama de salida
	 * @param index numero entero
	 * @return boolean true si no hay errores falso en caso contrario
	 */
	public boolean armaTrama(Map<String,String> mapConsultaEstatus, StringBuilder trama, int index){
		int tamanio;
		boolean flag = true;
		Utilerias utilerias = Utilerias.getUtilerias();
		String nomCampo ="";
		String tipoCampo = "";
		String valorCampo = "";

		nomCampo = ConstantesConsultaEstatusSPID.CAMPOS[index];
		if(!mapConsultaEstatus.containsKey(nomCampo)){//Si no existe lo inicializo
			tipoCampo = ConstantesConsultaEstatusSPID.TIPO.get(nomCampo);
			if(ConstantesConsultaEstatusSPID.DECIMAL.equals(tipoCampo)){
				mapConsultaEstatus.put(nomCampo, ConstantesConsultaEstatusSPID.CERO);
			}else{
				mapConsultaEstatus.put(nomCampo, "");
			}
		}
		tipoCampo = ConstantesConsultaEstatusSPID.TIPO.get(nomCampo);
		valorCampo = mapConsultaEstatus.get(nomCampo);
		tamanio = ConstantesConsultaEstatusSPID.LONGITUD.get(nomCampo);
		if(ConstantesConsultaEstatusSPID.DECIMAL.equals(tipoCampo)){
			BigDecimal valor = new BigDecimal(valorCampo);
			if(valor.compareTo(BigDecimal.ZERO)>0){
				if(isFormatoValidado(tipoCampo, tamanio, valorCampo)){
					trama.append(utilerias.formateaDecimales(valorCampo,Utilerias.FORMATO_DECIMAL_NUMBER_2)).append(ConstantesConsultaEstatusSPID.PIPE);
				}else{
					flag = false;
				}
			}else{
				trama.append(ConstantesConsultaEstatusSPID.CERO).append(ConstantesConsultaEstatusSPID.PIPE);
			}
		}else{
			if(isFormatoValidado(tipoCampo, tamanio, valorCampo)){
				trama.append(utilerias.getString(valorCampo)).append(ConstantesConsultaEstatusSPID.PIPE);
			}else{
				flag = false;
			}					
		}

		return flag;
	}
	
	/**
	 * @param valores para validar la trama de retorno
	 * @return boolean true si es valido falso en caso contrario
	 */
	private boolean isValidRetorno(final String[] valores) {
		boolean result = true;
		
		if (!validaLongitud(valores[0], ConstantesConsultaEstatusSPID.NUM_10)) {
			result = false;
		}
		if (!validaLongitud(valores[1], ConstantesConsultaEstatusSPID.NUM_60)) {
			result = false;
		}
		if(valores.length > 3){
			if (!validaLongitud(valores[2], ConstantesConsultaEstatusSPID.NUM_2)) {
				result = false;
			}
			if (!validaLongitud(valores[3], ConstantesConsultaEstatusSPID.NUM_40)) {
				result = false;
			}
			if (!validaLongitud(valores[4], ConstantesConsultaEstatusSPID.NUM_120)) {
				result = false;
			}
			if(result && !"CA".equals(valores[2])){
				
				if (!validaLongitud(valores[5], ConstantesConsultaEstatusSPID.NUM_19)) {
					result = false;
				}
				if (!validaLongitud(valores[6], ConstantesConsultaEstatusSPID.NUM_30)) {
					result = false;
				}
				if (!validaLongitud(valores[7], ConstantesConsultaEstatusSPID.NUM_12)) {
					result = false;
				}
				if (!validaLongitud(valores[8], ConstantesConsultaEstatusSPID.NUM_50)) {
					result = false;
				}
				if (!validaLongitud(valores[9], ConstantesConsultaEstatusSPID.NUM_50)) {
					result = false;
				}
				if (!validaLongitud(valores[11], ConstantesConsultaEstatusSPID.NUM_8)) {
					result = false;
				}
				if (!validaLongitud(valores[12], ConstantesConsultaEstatusSPID.NUM_8)) {
					result = false;
				}
				if (!validaLongitud(valores[13], ConstantesConsultaEstatusSPID.NUM_40)) {
					result = false;
				}
				if (!validaLongitud(valores[14], ConstantesConsultaEstatusSPID.NUM_120)) {
					result = false;
				}
				
			}
		}
		
		return result; 
	}
	
	/**
	 * @param valores para retornar trama de salida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnSalida(final String[] valores, Map<String, String> salida) {
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_COD_ERROR, valores[0]);
		debug("COD_ERROR " + valores[0]);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_DESC_ERROR, valores[1]);
		debug("DESC_ERROR " + valores[1]);
		if (valores.length > 3) {	
			salida.put(ConstantesConsultaEstatusSPID.CAMPO_ESTATUS, valores[2]);
			debug("ESTATUS " + valores[2]);
			salida.put(ConstantesConsultaEstatusSPID.CAMPO_DESCRIPCION, valores[3]);
			debug("DESCRIPCION " + valores[3]);
			salida.put(ConstantesConsultaEstatusSPID.CAMPO_DESC_DEVOLUCION, valores[4]);
			debug("DESC_DEVOLUCION " + valores[4]);
		
			if(!"CA".equals(valores[2]) && valores.length > 5){
				
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_FCH_APLICA, valores[5]);
				debug("FCH_APLICA " + valores[5]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_CVE_RASTREO, valores[6]);
				debug("CVE_RASTREO " + valores[6]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_REFERENCIA_TXT, valores[7]);
				debug("REFERENCIA_TXT " + valores[7]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_NOM_ORD, valores[8]);
				debug("NOM_ORD " + valores[8]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_NOM_REC, valores[9]);
				debug("NOM_REC " + valores[9]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_MONTO, valores[10]);
				debug("MONTO " + valores[10]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_INTERME_REC, valores[11]);
				debug("INTERME_REC " + valores[11]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_INTERME_ORD, valores[12]);
				debug("INTERME_ORD " + valores[12]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_NOMBRE_CORTO, valores[13]);
				debug("NOMBRE_CORTO " + valores[13]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_RFC_ORDENANTE, valores[14]);
				debug("ORDENANTE " + valores[14]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_CONCEPTO, valores[15]);
				debug("CONCEPTO " + valores[15]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_FCH_APROBACION, valores[16]);
				debug("FCH_APROBACION " + valores[16]);
				salida.put(ConstantesConsultaEstatusSPID.CAMPO_REFERENCIA_NUM, valores[17]);
				debug("REFERENCIA_NUM " + valores[17]);
				if(valores.length>=19){
					salida.put(ConstantesConsultaEstatusSPID.CAMPO_NUM_CUENTA_ORD, valores[18]);				
					debug("NUM_CUENA_ORD " + valores[18]);
				}else{
					salida.put(ConstantesConsultaEstatusSPID.CAMPO_NUM_CUENTA_ORD, "");				
					debug("NUM_CUENA_ORD ");
				}
				if(valores.length>=20){
					debug("NUM_CUENA_REC " + valores[19]);
					salida.put(ConstantesConsultaEstatusSPID.CAMPO_NUM_CUENTA_REC, valores[19]);
				}else{
					salida.put(ConstantesConsultaEstatusSPID.CAMPO_NUM_CUENTA_REC,"");
					debug("NUM_CUENTA_REC ");
				}
			}
		}
	}
	
	/**
	 * llenado cuando la trama es nula
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnTramaNula(Map<String, String> salida) {
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_COD_ERROR,Errores.EC00011B);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_REFERENCIA_NUM,ConstantesConsultaEstatusSPID.CERO);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_ESTATUS,"ER");
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_DESCRIPCION,ConstantesConsultaEstatusSPID.ERROR_IDA);
	}
	
	/**
	 * @param tramaRetorno trama de retorno de ida
	 * @param salida Objeto del tipo Map<String, String>
	 */
	private void returnCodigo(final String tramaRetorno,Map<String, String> salida) {
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_COD_ERROR, tramaRetorno);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_REFERENCIA_NUM,
				ConstantesConsultaEstatusSPID.CERO);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_ESTATUS,
				ConstantesConsultaEstatusSPID.NADA);
		salida.put(ConstantesConsultaEstatusSPID.CAMPO_DESCRIPCION,
				tramaRetorno);

	}
	
	/**
	 * Metodo que sirve para validar el formato
	 * @param clase string con el tipo de dato
	 * @param tamanio longitud del valor
	 * @param valorDato valor del campo
	 * @return boolean true si es valido falso en caso contrario
	 */
	private boolean isFormatoValidado(final String clase,
			final int tamanio, final String valorDato) {
		if (clase.equals(ConstantesConsultaEstatusSPID.STRING)
				&& (!validaLongitud(valorDato, tamanio))) {
				return false;			
		}
		if (clase.equals(ConstantesConsultaEstatusSPID.DECIMAL)) {
			if (!validaLongitud(valorDato, tamanio)) {
				return false;
			}
			if (!valorDato.matches(
					ConstantesConsultaEstatusSPID.EXP_REGULAR_NUMEROS)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @param cadena valor del campo
	 * @param numero longitud maxima
	 * @return boolean true si es correcta la longitud false en caso contrario
	 */
	private boolean validaLongitud(final String cadena, final int numero) {
		if (cadena.length() > numero) {
			return false;
		}
		return true;
	}
	

	/**
	 * Metodo get que sirve para obtener la propiedad dAOConsulaEstatusSPID
	 * @return dAOConsulaEstatusSPID Objeto del tipo DAOConsulaEstatusSPID
	 */
	public DAOConsulaEstatusSPID getDAOConsulaEstatusSPID() {
		return dAOConsulaEstatusSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOConsulaEstatusSPID
	 * @param consulaEstatusSPID del tipo DAOConsulaEstatusSPID
	 */
	public void setDAOConsulaEstatusSPID(DAOConsulaEstatusSPID consulaEstatusSPID) {
		dAOConsulaEstatusSPID = consulaEstatusSPID;
	}


	
	public static void main(String args[]){
		String str ="|s||||||";
		String array[] = str.split("\\|");
		System.out.println(array.length);
	}
	
}
