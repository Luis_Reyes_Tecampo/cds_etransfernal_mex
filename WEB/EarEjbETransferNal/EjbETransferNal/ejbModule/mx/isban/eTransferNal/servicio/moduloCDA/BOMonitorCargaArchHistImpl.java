/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorCargaArchHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:29:00 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMonCargaArchHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHistDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOMonitorCargaArchHist;
import mx.isban.eTransferNal.helper.HelperDAO;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * del Monitor de carga de archivos historico
**/
@Remote(BOMonitorCargaArchHist.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorCargaArchHistImpl extends Architech implements BOMonitorCargaArchHist {

   /**
	 * Constante del serial version 
	 */
	private static final long serialVersionUID = -6458979875850950417L;
	/**
	 * Referencia al servicio DAOMonitorCargaArchHist
	 */
	@EJB
	private DAOMonitorCargaArchHist dao;

/**Metodo que sirve para consultar la informacion del monitor de
   * carga de archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchHist Objeto del tipo BeanResMonCargaArchHist
   * @exception BusinessExceptionException de negocio
   */
   public BeanResMonCargaArchHist consultarMonitorCargaArchHist(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean)
       throws BusinessException{
	  List<BeanMonCargaArchHist> listBeanMonCargaArchHist = null;
	  final BeanResMonCargaArchHist beanResMonCargaArchHist = new BeanResMonCargaArchHist();
      BeanResMonCargaArchHistDAO beanResMonCargaArchHistDAO = null;
      beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
      beanResMonCargaArchHistDAO = dao.consultarMonitorCargaArchHist(beanPaginador, architechSessionBean);
      listBeanMonCargaArchHist = beanResMonCargaArchHistDAO.getListBeanMonCargaArchHist();
      if(Errores.CODE_SUCCESFULLY.equals(beanResMonCargaArchHistDAO.getCodError()) && listBeanMonCargaArchHist.isEmpty()){
    	  beanResMonCargaArchHist.setCodError(Errores.ED00011V);
    	  beanResMonCargaArchHist.setTipoError(Errores.TIPO_MSJ_INFO);
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResMonCargaArchHistDAO.getCodError())){
    	  beanResMonCargaArchHist.setCodError(Errores.OK00000V);
    	  beanResMonCargaArchHist.setListBeanMonCargaArchHist(beanResMonCargaArchHistDAO.getListBeanMonCargaArchHist());
          beanPaginador.calculaPaginas(beanResMonCargaArchHistDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
          beanResMonCargaArchHist.setPaginador(beanPaginador);
      }else{
    	  beanResMonCargaArchHist.setCodError(Errores.EC00011B);
    	  beanResMonCargaArchHist.setTipoError(Errores.TIPO_MSJ_ERROR);
      }


      
      return beanResMonCargaArchHist;
   }

   /**
    * Metodo get que regresa la referencia al servicio DAOMonitorCargaArchHist 
    * @return DAOMonitorCargaArchHist  Objeto del tipo DAOMonitorCargaArchHist 
    */
	public DAOMonitorCargaArchHist getDao() {
		return dao;
	}
	
   /**
    * Metodo set que pone la referencia al servicio DAOMonitorCargaArchHist 
    * @param dao Objeto del tipo DAOMonitorCargaArchHist 
    */
	public void setDao(DAOMonitorCargaArchHist dao) {
		this.dao = dao;
	}

   


}
