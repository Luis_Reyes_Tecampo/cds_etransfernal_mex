/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMonitoresImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:39:29 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.springframework.dao.DataAccessException;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOMonitores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOComunesSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class BOMonitoresImpl.
 *
 * Clase tipo BO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 6/08/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitoresImpl extends Architech implements BOMonitores{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3921053975952399135L;

	/** La variable que contiene informacion con respecto a: dao monitores. */
	@EJB
	private DAOMonitores daoMonitores;
	
	/** La variable que contiene informacion con respecto a: dao comunes. */
	@EJB
	private DAOComunesSPID daoComunes;
	

	/**
	 * Obtener monitor.
	 *
	 * Obtener la informacion actual del monitor
	 * 
	 * @param modulo  El objeto: modulo SPEI o SPID
	 * @param session El objeto: session
	 * @return Objeto bean monitor
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanMonitor obtenerMonitor(BeanModulo modulo, ArchitechSessionBean session) throws BusinessException {
		BeanMonitor responseMonitor = null;
		BeanSessionSPID sessionMonitor = null;
		try {
			final String entidad = ValidadorMonitor.getEntidad(modulo);
			final BeanResConsMiInstDAO cveInstitucion = daoComunes.consultaMiInstitucion(entidad, session);
			final BeanResConsFechaOpDAO  fechaOperacion = daoComunes.consultaFechaOperacionSPID(session);
			sessionMonitor = new BeanSessionSPID();
			sessionMonitor.setCveInstitucion(cveInstitucion.getMiInstitucion());
			sessionMonitor.setFechaOperacion(fechaOperacion.getFechaOperacion());
			responseMonitor = daoMonitores.obtenerMonitor(modulo, sessionMonitor, session);
			responseMonitor.setSession(sessionMonitor);
		} catch (DataAccessException e) {
			showException(e);
		}
		return responseMonitor;
	}

}
