/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORMonitorOpSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMonitorSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorOpSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAOComunesSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAOMonitorSPID;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORMonitorOpSPIDImpl extends Architech implements BORMonitorOpSPID{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6191282667824530672L;
	/**
	 * Propiedad del tipo DAOComunesSPID que almacena el valor de comunesSPID
	 */
	@EJB
	private DAOComunesSPID comunesSPID;
	/**
	 * Propiedad del tipo DAOMonitorSPID que almacena el valor de monitorSPID
	 */
	@EJB
	private DAOMonitorSPID monitorSPID;
	/**
	 * Metodo que sirve para consultar la informacion del monitor operativo SPID
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorOpSPID Objeto del tipo @see BeanResMonitorOpSPID
	 */
	@Override
	public BeanResMonitorOpSPID obtenerDatosMonitorSPID(
			ArchitechSessionBean architectSessionBean) {
		BeanReqMonitorSPID beanReqMonitorSPID = new BeanReqMonitorSPID();
		BeanResMonitorOpSPID beanResMonitorOpSPID = null;
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = comunesSPID.consultaFechaOperacionSPID(architectSessionBean);
		BeanResConsMiInstDAO beanResConsMiInstDAO = comunesSPID.consultaMiInstitucion("ENTIDAD_SPID", architectSessionBean);
		beanReqMonitorSPID.setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		beanReqMonitorSPID.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		beanResMonitorOpSPID = monitorSPID.consultarMonitorSPID(beanReqMonitorSPID, architectSessionBean);
		beanResMonitorOpSPID.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		beanResMonitorOpSPID.setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		return beanResMonitorOpSPID;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad comunesSPID
	 * @return comunesSPID Objeto del tipo DAOComunesSPID
	 */
	public DAOComunesSPID getComunesSPID() {
		return comunesSPID;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad comunesSPID
	 * @param comunesSPID del tipo DAOComunesSPID
	 */
	public void setComunesSPID(DAOComunesSPID comunesSPID) {
		this.comunesSPID = comunesSPID;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad monitorSPID
	 * @return monitorSPID Objeto del tipo DAOMonitorSPID
	 */
	public DAOMonitorSPID getMonitorSPID() {
		return monitorSPID;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad monitorSPID
	 * @param monitorSPID del tipo DAOMonitorSPID
	 */
	public void setMonitorSPID(DAOMonitorSPID monitorSPID) {
		this.monitorSPID = monitorSPID;
	}

}
