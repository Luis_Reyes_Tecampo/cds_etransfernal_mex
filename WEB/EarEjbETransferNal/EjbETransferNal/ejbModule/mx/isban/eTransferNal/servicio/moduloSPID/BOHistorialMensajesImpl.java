/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOHistorialMensajesImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOHistorialMensajes;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio de la historia del mensaje
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)

public class BOHistorialMensajesImpl implements BOHistorialMensajes,  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7318224828917915073L;
	
	/** Referencia privada al dao  */
	@EJB
	private DAOHistorialMensajes daoHistorialMensajes;	

	/**
	 * Metodo get que obtiene el daoHistorialMensajes
	 * 
	 * @return DAOHistorialMensajes objeto con daoHistorialMensajes
	 */
	public DAOHistorialMensajes getDaoHistorialMensajes() {
		return daoHistorialMensajes;
	}

	/**
	 * Metodo set que sirve para setear el daoHistorialMensajes
	 * 
	 * @param daoHistorialMensajes
	 *            el DAOHistorialMensajes
	 */
	public void setDaoHistorialMensajes(DAOHistorialMensajes daoHistorialMensajes) {
		this.daoHistorialMensajes = daoHistorialMensajes;
	}

	/**Metodo que sirve para consultar el historial de mensajes
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @param beanReqHistorialMensajes Objeto del tipo BeanReqHistorialMensajes
	   * @return beanResHistorialMensajes Objeto del tipo BeanResHistorialMensajes
	   */
	@Override
	public BeanResHistorialMensajes obtenerHistorial(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes  ) {
		BeanHistorialMensajesDAO beanHistorialMensajesDAO=null;
		final BeanResHistorialMensajes beanResHistorialMensajes = new BeanResHistorialMensajes();

		

		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		
		beanHistorialMensajesDAO=daoHistorialMensajes.obtenerHistorial(paginador,architechSessionBean, beanReqHistorialMensajes);
		
		
		
		if(beanHistorialMensajesDAO.getBeanHistorialMensajes()==null  || beanHistorialMensajesDAO.getBeanHistorialMensajes().isEmpty() ) {
			beanHistorialMensajesDAO.setBeanHistorialMensajes(new ArrayList<BeanHistorialMensajes>());
			
		}
		
		if(Errores.CODE_SUCCESFULLY.equals(beanHistorialMensajesDAO.getCodError()) && beanHistorialMensajesDAO.getBeanHistorialMensajes().isEmpty() ){
			beanResHistorialMensajes.setCodError(Errores.ED00011V);
			beanResHistorialMensajes.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResHistorialMensajes.setListaHisMsj(beanHistorialMensajesDAO.getBeanHistorialMensajes());			
		}else if(Errores.CODE_SUCCESFULLY.equals(beanHistorialMensajesDAO.getCodError())){
			

			beanResHistorialMensajes.setListaHisMsj(beanHistorialMensajesDAO.getBeanHistorialMensajes());
			
			
			beanResHistorialMensajes.setTotalReg(beanHistorialMensajesDAO.getTotalReg());
			paginador.calculaPaginas(beanHistorialMensajesDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResHistorialMensajes.setBeanPaginador(paginador);
			
			
			beanResHistorialMensajes.setCodError(Errores.OK00000V);
		}else{
			beanResHistorialMensajes.setCodError(Errores.EC00011B);
			beanResHistorialMensajes.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		
		
		return beanResHistorialMensajes;

	}	
}
