/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonCanCargaArchContingImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:29:00 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonCargaArchCanConting;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContig;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContingDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchxFchCDAHist;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOMonCargaArchCanConting;
import mx.isban.eTransferNal.helper.HelperDAO;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * del Monitor de carga de archivos historico
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonCanCargaArchContingImpl extends Architech implements BOMonCanCargaArchConting {

   /**
	 * Constante del serial version 
	 */
	private static final long serialVersionUID = -6458979875850950417L;
	/**
	 * Referencia al servicio DAOMonitorCargaArchHist
	 */
	@EJB
	private DAOMonCargaArchCanConting dao;
	
	/**
	 * Propiedad del tipo DAOParametrosPOACOAImpl que almacena el valor de dAOParametrosPOACOAImpl
	 */
	@EJB
	private DAOGenerarArchxFchCDAHist dAOGenerarArchxFchCDAHist;

  /**Metodo que sirve para consultar la informacion del monitor de
   * carga de archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchHist Objeto del tipo BeanResMonCargaArchHist
   */
   public BeanResMonCargaArchCanContig consultarMonCanCargaArchConting(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean){
	   List<BeanMonCargaArchCanConting> beanMonCargaArchCanContingList = null;
	   /** Invocacion al DAO para consultar la fecha de operacion **/
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOGenerarArchxFchCDAHist.consultaFechaOperacion(null, architechSessionBean);
	   
	  final BeanResMonCargaArchCanContig beanResMonCargaArchCanContig = new BeanResMonCargaArchCanContig();
	  BeanResMonCargaArchCanContingDAO beanResMonCargaArchCanContingDAO = null;
      beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
      beanResMonCargaArchCanContingDAO = dao.consultarMonCargaArchCanConting(beanPaginador, beanResConsFechaOpDAO.getFechaOperacion(), architechSessionBean);
      beanMonCargaArchCanContingList = beanResMonCargaArchCanContingDAO.getListBeanMonCargaArchCanContig();
      /** Se valdia la respuesta de la operacion **/
      if(Errores.CODE_SUCCESFULLY.equals(beanResMonCargaArchCanContingDAO.getCodError()) && beanMonCargaArchCanContingList.isEmpty()){
    	  beanResMonCargaArchCanContig.setCodError(Errores.ED00011V);
    	  beanResMonCargaArchCanContig.setTipoError(Errores.TIPO_MSJ_INFO);
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResMonCargaArchCanContingDAO.getCodError())){
    	  beanResMonCargaArchCanContig.setCodError(Errores.OK00000V);
    	  beanResMonCargaArchCanContig.setListBeanMonCargaArchCanConting(beanMonCargaArchCanContingList);
          beanPaginador.calculaPaginas(beanResMonCargaArchCanContingDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
          beanResMonCargaArchCanContig.setPaginador(beanPaginador);
      }else{
    	  beanResMonCargaArchCanContig.setCodError(Errores.EC00011B);
    	  beanResMonCargaArchCanContig.setTipoError(Errores.TIPO_MSJ_ERROR);
      }
      /** Retorno del metodo **/
      return beanResMonCargaArchCanContig;
   }

	/**
	 * Metodo get que obtiene el valor de la propiedad dao
	 * @return DAOMonCargaArchCanConting Objeto de tipo @see DAOMonCargaArchCanConting
	 */
	public DAOMonCargaArchCanConting getDao() {
		return dao;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad dao
	 * @param dao Objeto de tipo @see DAOMonCargaArchCanConting
	 */
	public void setDao(DAOMonCargaArchCanConting dao) {
		this.dao = dao;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOGenerarArchxFchCDAHist
	 * @return DAOGenerarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public DAOGenerarArchxFchCDAHist getDAOGenerarArchxFchCDAHist() {
		return dAOGenerarArchxFchCDAHist;
	}

	/**
	 * Metodo que modifica el valor de la propiedad dAOGenerarArchxFchCDAHist
	 * @param generarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public void setDAOGenerarArchxFchCDAHist(
			DAOGenerarArchxFchCDAHist generarArchxFchCDAHist) {
		dAOGenerarArchxFchCDAHist = generarArchxFchCDAHist;
	}





   



   


}
