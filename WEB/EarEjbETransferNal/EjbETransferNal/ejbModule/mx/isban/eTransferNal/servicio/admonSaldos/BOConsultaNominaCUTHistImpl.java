package mx.isban.eTransferNal.servicio.admonSaldos;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldo.BeanReqConsNominaCUT;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTHist;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResConsNomCUTHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.dao.admonSaldos.DAOConsultaNominaCUTHist;
import mx.isban.eTransferNal.dao.comun.DAOComunesSPEI;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaNominaCUTHistImpl extends Architech implements BOConsultaNominaCUTHist{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3229179003779852308L;
	
	/**
	 * Propiedad del tipo DAOComunesSPEIImpl que almacena el valor de dAOComunesSPEI
	 */
	@EJB
	private DAOComunesSPEI dAOComunesSPEI;
	
	/**
	 * Propiedad del tipo DAOConsultaNominaCUTHist que almacena el valor de dAOConsultaNominaCUTHist
	 */
	@EJB
	private DAOConsultaNominaCUTHist dAOConsultaNominaCUTHist; 


	@Override
	public BeanResConsNomCUTHist consultar(ArchitechSessionBean sessionBean) {
		BeanResConsNomCUTHist beanResConsNomCUTHist = new BeanResConsNomCUTHist(); 
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOComunesSPEI.consultaFechaOperacion(sessionBean);
		BeanResConsMiInstDAO beanResConsMiInstDAO = dAOComunesSPEI.consultaMiInstitucion(sessionBean);
		beanResConsNomCUTHist.setCveInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		beanResConsNomCUTHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		return beanResConsNomCUTHist;
	}


	@Override
	public BeanResConsNomCUTHist consultarNomina(BeanReqConsNominaCUT req,
			ArchitechSessionBean sessionBean) {
		BeanResConsNomCUTHist beanResConsNomCUTHist = consultar(sessionBean);
		BeanResConsNomCUTHistDAO beanResConsNomCUTHistDAO = dAOConsultaNominaCUTHist.consultarNomina(req,sessionBean);
		beanResConsNomCUTHist.setListBeanConsNomCUT(beanResConsNomCUTHistDAO.getListBeanConsNomCUT());
		beanResConsNomCUTHist.setCodError(beanResConsNomCUTHistDAO.getCodError());
		beanResConsNomCUTHist.setTipoError(beanResConsNomCUTHistDAO.getTipoError());
		return beanResConsNomCUTHist;
	}


	/**
	 * Metodo get que sirve para obtener la propiedad dAOComunesSPEI
	 * @return dAOComunesSPEI Objeto del tipo DAOComunesSPEI
	 */
	public DAOComunesSPEI getDAOComunesSPEI() {
		return dAOComunesSPEI;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad dAOComunesSPEI
	 * @param comunesSPEI del tipo DAOComunesSPEI
	 */
	public void setDAOComunesSPEI(DAOComunesSPEI comunesSPEI) {
		dAOComunesSPEI = comunesSPEI;
	}


	/**
	 * Metodo get que sirve para obtener la propiedad dAOConsultaNominaCUTHist
	 * @return dAOConsultaNominaCUTHist Objeto del tipo DAOConsultaNominaCUTHist
	 */
	public DAOConsultaNominaCUTHist getDAOConsultaNominaCUTHist() {
		return dAOConsultaNominaCUTHist;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad dAOConsultaNominaCUTHist
	 * @param consultaNominaCUTHist del tipo DAOConsultaNominaCUTHist
	 */
	public void setDAOConsultaNominaCUTHist(
			DAOConsultaNominaCUTHist consultaNominaCUTHist) {
		dAOConsultaNominaCUTHist = consultaNominaCUTHist;
	}
	
	
}
