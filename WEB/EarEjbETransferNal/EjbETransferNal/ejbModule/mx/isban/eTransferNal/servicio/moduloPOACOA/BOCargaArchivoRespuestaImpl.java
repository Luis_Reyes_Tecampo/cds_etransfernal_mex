/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOCargaArchivoRespuestaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOArchivosRespuesta;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasPOACOA;

/**
 *  Clase que implementa BOCargaArchivoRespuesta.
 *
 * @author FSW-Vector
 * @since 14/01/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCargaArchivoRespuestaImpl extends Architech implements BOCargaArchivoRespuesta {
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 5834633589818153421L;

	
	/**DAO para las transacciones a base de datos. */
	@EJB
	private DAOArchivosRespuesta daoArchivosRespuesta;
	
	/** Referencia al servicio dao DAOBitacoraAdmon. */
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	
	/** La variable que contiene informacion con respecto a: bo pista auditora. */
	@EJB
	private BOPistaAuditora boPistaAuditora;

	/** La constante NOMBRE_TABLA. */
	private static final String NOMBRE_TABLA = "TRAN_POA_COA_REC";
	
	/** La constante OPER_UPDATE. */
	private static final String OPER_UPDATE = "UPDATE";
	
	/** La constante OPER_INSERT. */
	private static final String OPER_INSERT = "INSERT";
	
	/** La constante utileriasPOACOA. */
	private static final UtileriasPOACOA utileriasPOACOA = UtileriasPOACOA.getInstancia();
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "FCH_OPERACION, CVE_MI_INSTITUC";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** La constante CONS_MODULO. */
	private static final String CONS_MODULO = "modulo";
	
	/** La constante CONS_ARQ. */
	private static final String CONS_ARQ = "arquitectura";
	
	/**
	 * Metodo que valida si esta iniciado POACOA.
	 *
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Objeto de la Arquitectura
	 * @return BeanResValInicioPOACOA Bean de Respuesta
	 */
	@Override
	public BeanResValInicioPOACOA validaInicioPOACOA(String modulo, ArchitechSessionBean architectSessionBean){
		boolean iniciadoPOACOA = false;
		/** Se declara el objeto de respuesta **/
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = null; 
		/** Se realiza la peticion de la operacion al  DAO **/
		beanResConsFechaOpDAO = daoArchivosRespuesta.consultaFechaOperacion(modulo, architectSessionBean);
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResConsMiInstDAO resInstitucion = daoArchivosRespuesta.consultaMiInstitucion(modulo, architectSessionBean);
		/** Se realiza la peticion de la operacion al  DAO **/
		BeanResValInicioPOACOA beanResValInicioPOACOA = 
			daoArchivosRespuesta.validaInicioPOACOA(resInstitucion.getMiInstitucion(), 
					beanResConsFechaOpDAO.getFechaOperacion(), modulo, architectSessionBean);
		/**  Se valida la respuesta **/
		if(Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
			beanResValInicioPOACOA.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
			beanResValInicioPOACOA.setCveMiInstitucion(resInstitucion.getMiInstitucion());
			iniciadoPOACOA = utileriasPOACOA.isIniciadoPOACOA(beanResValInicioPOACOA);
			beanResValInicioPOACOA.setActivoPOACOA(iniciadoPOACOA);
			if(!iniciadoPOACOA){	
				if ("2".equals(modulo)) {
					beanResValInicioPOACOA.setCodError(Errores.ED00145V);
				} else {
					beanResValInicioPOACOA.setCodError(Errores.ED00071V);
				}
			}
		}
		/** Retorno del metodo **/
		return beanResValInicioPOACOA;
	}

	/**
	 * Agrega nombre del archivo para su posterior proceso.
	 *
	 * @param nombreArchivo Nombre del archivo
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Objeto bean de respuesta
	 */
	@Override
	public BeanResValInicioPOACOA agregarNombreArchResp(String nombreArchivo, String modulo,
			ArchitechSessionBean architectSessionBean) {
		String codError;
		/** Se declara el objeto de respuesta **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO = null;
		BeanResValInicioPOACOA beanResValInicioPOACOA = null;
			beanResValInicioPOACOA = validaInicioPOACOA(modulo,architectSessionBean);
			if(utileriasPOACOA.isIniciadoPOACOA(beanResValInicioPOACOA)){	
				/** Se setea el codigo de error **/
				codError = utileriasPOACOA.validaFormatoArchivo(nombreArchivo, beanResValInicioPOACOA.getCveMiInstitucion(), beanResValInicioPOACOA.getFchOperacion());
				beanResValInicioPOACOA.setCodError(codError);
				/**  Se valida la respuesta **/
				if (Errores.OK00000V.equals(codError)) {
					beanResDuplicadoArchDAO = validaNombreDuplicado(nombreArchivo, modulo, beanResValInicioPOACOA.getCveMiInstitucion(),
								beanResValInicioPOACOA.getFchOperacion(),architectSessionBean);
					if(Errores.OK00000V.equals(beanResDuplicadoArchDAO.getCodError())){
						Map<String, Object> parametrosMap = new HashMap<String, Object>();
						parametrosMap.put("nombreArchivo", nombreArchivo);
						parametrosMap.put(CONS_ARQ, architectSessionBean);
						parametrosMap.put(CONS_MODULO, modulo);
						beanResValInicioPOACOA = continuaAgregando(parametrosMap, beanResDuplicadoArchDAO, beanResValInicioPOACOA);
					}
				} else {
					beanResValInicioPOACOA.setTipoError(Errores.TIPO_MSJ_ALERT);
				}
			}else{
				beanResValInicioPOACOA.setCodError(Errores.ED00071V);
			}
			/** Retorno del metodo **/
		return beanResValInicioPOACOA;
	}
	
	/**
	 * Continua agregando.
	 *
	 * @param parametrosMap El objeto: parametros map
	 * @param beanResDuplicadoArchDAO El objeto: bean res duplicado arch DAO
	 * @param beanResValInicioPOACOA El objeto: bean res val inicio POACOA
	 * @return Objeto bean res val inicio POACOA
	 */
	private BeanResValInicioPOACOA continuaAgregando(Map<String, Object> parametrosMap, BeanResDuplicadoArchDAO beanResDuplicadoArchDAO,
			BeanResValInicioPOACOA beanResValInicioPOACOA) {
		BeanResBase beanResBase = null;
		if(!beanResDuplicadoArchDAO.isEsDuplicado()){
			/** Se realiza la peticion de la operacion al  DAO **/
			beanResBase = daoArchivosRespuesta.registarNombreArchivo(parametrosMap.get("nombreArchivo").toString(),beanResValInicioPOACOA.getFchOperacion(),
					beanResValInicioPOACOA.getCveMiInstitucion(), parametrosMap.get(CONS_MODULO).toString(), (ArchitechSessionBean)parametrosMap.get(CONS_ARQ));
			if(Errores.OK00000V.equals(beanResBase.getCodError())){
				beanResValInicioPOACOA.setCodError(Errores.OK00000V);
					insertaBitacora(beanResValInicioPOACOA, parametrosMap.get(CONS_MODULO).toString(), OPER_INSERT, true, (ArchitechSessionBean)parametrosMap.get(CONS_ARQ));
			}else{
				beanResValInicioPOACOA.setCodError(Errores.ED00070V);
				beanResValInicioPOACOA.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		}else{
			beanResValInicioPOACOA.setCodError(Errores.CD00166V);
			beanResValInicioPOACOA.setTipoError(Errores.TIPO_MSJ_CONFIRM);
			
		}
		/** Retorno del metodo **/
		return beanResValInicioPOACOA;
	}
	
	/**
	 * Actualiza nombre del archivo para su posterior proceso.
	 *
	 * @param nombreArchivo Nombre del archivo
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Objeto bean de respuesta
	 */
	@Override
	public BeanResValInicioPOACOA actualizarNombreArchResp(String nombreArchivo, String modulo,
			ArchitechSessionBean architectSessionBean) {
		BeanResBase beanResBase = null;
		/** Se declara el objeto de respuesta **/
		BeanResValInicioPOACOA beanResValInicioPOACOA = validaInicioPOACOA(modulo,architectSessionBean);
			if(utileriasPOACOA.isIniciadoPOACOA(beanResValInicioPOACOA)){
				/**  Se valida la respuesta **/
				if (Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
					/** Se realiza la peticion de la operacion al  DAO **/
					beanResBase = daoArchivosRespuesta
							.actualizarNombreArchivo(nombreArchivo,
									beanResValInicioPOACOA.getCveMiInstitucion(),
									beanResValInicioPOACOA.getFchOperacion(),
									modulo,
									architectSessionBean);
					/**  Se valida la respuesta **/
					if(Errores.OK00000V.equals(beanResBase.getCodError())){
					
						beanResValInicioPOACOA.setCodError(Errores.OK00000V);
						insertaBitacora(beanResValInicioPOACOA, modulo, OPER_UPDATE, true, architectSessionBean);
					}else{
						beanResValInicioPOACOA.setCodError(Errores.ED00074V);
					}
				}
			}else{
				beanResValInicioPOACOA.setCodError(Errores.ED00071V);
			}
			/** Retorno del metodo **/
		return beanResValInicioPOACOA;
	}
	
	/**
	 * Valida si el nombre del archivo se encuentra ya en BD.
	 *
	 * @param nombreArchivo Nombre del archivo
	 * @param modulo El objeto: modulo
	 * @param institucion Clave de la isnstitucion
	 * @param fchOperacion String con la fecha de operacion
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO Bean de respuesta de archivo duplicado
	 */
	private BeanResDuplicadoArchDAO validaNombreDuplicado(String nombreArchivo, String modulo, String institucion, String fchOperacion,
			ArchitechSessionBean architectSessionBean){
		/** Se declara el objeto de respuesta **/
		BeanResDuplicadoArchDAO beanResDuplicadoArchDAO = null;
		/** Se realiza la peticion de la operacion al  DAO **/
		beanResDuplicadoArchDAO = daoArchivosRespuesta.consultaDuplicidadNombre(nombreArchivo,institucion, fchOperacion, modulo, architectSessionBean);
		/** Retorno del metodo **/
		return beanResDuplicadoArchDAO;
	}
	
	/**
	 * Devuelve el valor de daoArchivosRespuesta.
	 *
	 * @return the daoArchivosRespuesta
	 */
	public DAOArchivosRespuesta getDaoArchivosRespuesta() {
		return daoArchivosRespuesta;
	}

	/**
	 * Modifica el valor de daoArchivosRespuesta.
	 *
	 * @param daoArchivosRespuesta the daoArchivosRespuesta to set
	 */
	public void setDaoArchivosRespuesta(DAOArchivosRespuesta daoArchivosRespuesta) {
		this.daoArchivosRespuesta = daoArchivosRespuesta;
	}

	/**
	 * Devuelve el valor de daoBitacora.
	 *
	 * @return the daoBitacora
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Modifica el valor de daoBitacora.
	 *
	 * @param daoBitacora the daoBitacora to set
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
	
	/**
	 * Inserta bitacora.
	 *
	 * @param beanResValInicioPOACOA El objeto: bean res val inicio POACOA
	 * @param modulo El objeto: modulo
	 * @param operacion El objeto: operacion
	 * @param resOp El objeto: res op
	 * @param architectSessionBean El objeto: architect session bean
	 */
	private void insertaBitacora(BeanResValInicioPOACOA beanResValInicioPOACOA, String modulo, String operacion, Boolean resOp, ArchitechSessionBean architectSessionBean)  {
		if ("2".equals(modulo)) {
			BeanPistaAuditora beanPista = new BeanPistaAuditora();
			String tablaActual = NOMBRE_TABLA;
			Boolean banderaOperacion = resOp;
			if ("2".equals(modulo)) {
				tablaActual = tablaActual.replaceAll(NOMBRE_TABLA, "TRAN_POA_COA_SPID_REC");
			}
			beanPista.setNomTabla(tablaActual);
			beanPista.setOperacion(operacion);
			beanPista.setUrlController("cargarArchResp.do");
			beanPista.setDatoFijo(DATO_FIJO);
			beanPista.setDatoModi("CVE_MI_INSTITUC,FCH_OPERACION,NOMBRE_ARCH, ESTATUS, CIFRADO, FCH_CAPTURA");
			if (banderaOperacion) {
				beanPista.setEstatus(OK);
			} else {
				beanPista.setEstatus(NOK);
			}
			boPistaAuditora.llenaPistaAuditora(beanPista, architectSessionBean);
		} else {
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora = utilerias
				.creaBitacora("cargarArchResp.do", "1", "", new Date(),
						"PENDIENTE", "ALTAARCHRES", NOMBRE_TABLA,
						"CVE_MI_INSTITUC", beanResValInicioPOACOA.getCodError(), "TRANSACCION EXITOSA",
						OPER_INSERT, beanResValInicioPOACOA.getCveMiInstitucion());
		daoBitacora.guardaBitacora(beanReqInsertBitacora, architectSessionBean);
		}
	}
}
