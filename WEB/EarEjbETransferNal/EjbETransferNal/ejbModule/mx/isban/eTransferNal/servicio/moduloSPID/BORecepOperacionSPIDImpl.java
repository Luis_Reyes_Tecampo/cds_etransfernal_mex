/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdmon;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloSPID.DAOComunesSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAORecepOperacionSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAORecepOperacionSPID2;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.HelperSPIDRecepcionOperacion;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmon;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;





@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepOperacionSPIDImpl extends Architech implements BORecepOperacionSPID{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1377532272492477541L;
	
	/**
	 * Propiedad del tipo DAORecepOperacion que almacena el valor de daoRecepOperacion
	 */
	@EJB
	private DAORecepOperacionSPID daoRecepOperacion;
	
	/**
	 * Propiedad del tipo DAORecepOperacion que almacena el valor de daoRecepOperacion
	 */
	@EJB
	private DAORecepOperacionSPID2 daoRecepOperacion2;
	/**
	 * Referencia privada al dao de generar archivos historicos
	 */
	@EJB
	private DAOComunesSPID dAOComunesSPID;
	
	/**Referencia al servicio dao DAOBitTransaccional*/
	@EJB
	private DAOBitTransaccional dAOBitTransaccional;
	
	/**
	 * Propiedad del tipo DAOBitacoraAdmon que almacena el valor de dAOBitacoraAdmon
	 */
	@EJB
	private DAOBitAdmon dAOBitAdmon;
	
    @Override
	public BeanResReceptoresSPID consultaCveMiInstFchOp(ArchitechSessionBean sessionBean){
		BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
		BeanResConsMiInstDAO beanResConsMiInstDAO = null;
    	beanResConsMiInstDAO = dAOComunesSPID.consultaMiInstitucion("ENTIDAD_SPID",sessionBean);
    	
		beanResConsFechaOpDAO =dAOComunesSPID.consultaFechaOperacionSPID(sessionBean);
	    beanResReceptoresSPID.setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
	    beanResReceptoresSPID.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
	    return beanResReceptoresSPID;
    }
	
	@Override
    public BeanResReceptoresSPID consultaTodasTransf(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
		HelperSPIDRecepcionOperacion helper = new HelperSPIDRecepcionOperacion();
		BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
	    BeanPaginador paginador = null;
	    BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO = null;
		List<BeanReceptoresSPID> listBeanReceptoresSPID = null;
		paginador = beanReqReceptoresSPID.getPaginador();
	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());			    
	    beanReqReceptoresSPID.setPaginador(paginador);
	    beanResReceptoresSPIDDAO = helper.ejecutaConsulta(dAOComunesSPID,daoRecepOperacion,beanReqReceptoresSPID,sessionBean);
	    listBeanReceptoresSPID = beanResReceptoresSPIDDAO.getListReceptoresSPID();
	    if(Errores.OK00000V.equals(beanResReceptoresSPIDDAO.getCodError()) && listBeanReceptoresSPID.isEmpty()){
	    	beanResReceptoresSPID.setCodError(Errores.ED00011V);
	    	beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_INFO);
	    	beanResReceptoresSPID.setOpcion(beanReqReceptoresSPID.getOpcion());
	    	beanResReceptoresSPID.setBloqueado(beanResReceptoresSPIDDAO.isBloqueado());
	    }else if(Errores.OK00000V.equals(beanResReceptoresSPIDDAO.getCodError())){
	    	helper.seteaRespuestaExitosa(beanReqReceptoresSPID,beanResReceptoresSPIDDAO,beanResReceptoresSPID,paginador );
	    	BeanResConsMotDevolucionDAO beanResConsMotDevolucionDAO = null;
	    	beanResConsMotDevolucionDAO = daoRecepOperacion.consultaDevoluciones(sessionBean);
	    	beanResReceptoresSPID.setBeanMotDevolucionList(beanResConsMotDevolucionDAO.getBeanMotDevolucionList());
	    	beanResReceptoresSPID.setBloqueado(beanResReceptoresSPIDDAO.isBloqueado());
	    }else{
	    	beanResReceptoresSPID.setCodError(Errores.EC00011B);
	    	beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }	
	    BeanResReceptoresSPID beanResReceptoresSPIDTemp =consultaCveMiInstFchOp(sessionBean);
	    beanResReceptoresSPID.setCveMiInstitucion(beanResReceptoresSPIDTemp.getCveMiInstitucion());
	    beanResReceptoresSPID.setFchOperacion(beanResReceptoresSPIDTemp.getFchOperacion());
	    beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
	    return beanResReceptoresSPID;
    }
    

    

    

    @Override
    public BeanResReceptoresSPID transferirSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
    	HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
    	List<BeanReceptoresSPID> listBeanReceptoresSPIDTemp = null;
    	List<BeanReceptoresSPID> listBeanTranSpeiRecOK = new ArrayList<BeanReceptoresSPID>();
    	List<BeanReceptoresSPID> listBeanTranSpeiRecNOK = new ArrayList<BeanReceptoresSPID>();
    	BeanReceptoresSPID beanReceptoresSPID = null;
    	BeanReceptoresSPID beanReceptoresSPIDTemp = null;
    	BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
    	BeanResConsTranSpeiRec beanResConsTranSpeiRecTemp = new BeanResConsTranSpeiRec();
    	listBeanReceptoresSPIDTemp = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID,HelperSPIDRecepcionOperacion.SELECCIONADO);
    	if(!listBeanReceptoresSPIDTemp.isEmpty()){
    		beanResConsTranSpeiRecTemp.setCodError(Errores.OK00001V);
    		for(int i=0;i<listBeanReceptoresSPIDTemp.size();i++){
    			beanReceptoresSPID = listBeanReceptoresSPIDTemp.get(i);	
    			
    			if(beanReceptoresSPID.getEstatus().getEstatusTransfer().equals("TR")){
    				beanReceptoresSPID.setCodError(Errores.ED00104V);
    				listBeanTranSpeiRecNOK.add(beanReceptoresSPID);
    			}else if("1".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())){
	    			if("03".equals(beanReceptoresSPID.getBancoRec().getTipoCuentaRec())||"10".equals(beanReceptoresSPID.getBancoRec().getTipoCuentaRec())){
	    				beanResReceptoresSPID = helperRecepcionOperacion.obtieneCuenta(beanReceptoresSPID, sessionBean);
		    			if(!Errores.OK00001V.equals(beanResReceptoresSPID.getCodError())){
		    				beanReceptoresSPID.setCodError(beanResReceptoresSPID.getCodError());
		    				listBeanTranSpeiRecNOK.add(beanReceptoresSPID);
		    			}
	    			}else if("".equals(beanReceptoresSPID.getBancoRec().getTipoCuentaRec())){
	    				beanReceptoresSPID.setCodError(Errores.ED00102V);
	    				listBeanTranSpeiRecNOK.add(beanReceptoresSPID);
	    			}
	    			if("40".equals(beanReceptoresSPID.getBancoRec().getTipoCuentaRec()) || Errores.OK00001V.equals(beanResReceptoresSPID.getCodError())){
	    				beanReceptoresSPIDTemp = envia(beanReceptoresSPID,false,"TR", sessionBean);
	    				if("TRIB0000".equals(beanReceptoresSPIDTemp.getCodError())){
	    					daoRecepOperacion.desApartar(beanReceptoresSPIDTemp, "RECEP_OP_SPID", sessionBean);
	    					listBeanTranSpeiRecOK.add(beanReceptoresSPIDTemp);
	    					helperRecepcionOperacion.setBitacoraTrans("01TRANSFRECOP", beanReceptoresSPIDTemp.getCodError(), "EXITO", "Operación Exitosa", 
	    							"Operación Exitosa", "TRANSFRECEPOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
	    				}else{
	    					listBeanTranSpeiRecNOK.add(beanReceptoresSPID);
	    					helperRecepcionOperacion.setBitacoraTrans("01TRANSFRECOP", beanReceptoresSPIDTemp.getCodError(), "", "", 
	    							"", "TRANSFRECEPOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
	    				}
	    			}
    			}else{
    				beanReceptoresSPIDTemp = envia(beanReceptoresSPID,true,"TR", sessionBean);
    				if("TRIB0000".equals(beanReceptoresSPIDTemp.getCodError())){
    					daoRecepOperacion.desApartar(beanReceptoresSPIDTemp, "RECEP_OP_SPID", sessionBean);
    					listBeanTranSpeiRecOK.add(beanReceptoresSPIDTemp);
    					helperRecepcionOperacion.setBitacoraTrans("01TRANSFRECOP", beanReceptoresSPIDTemp.getCodError(), "EXITO", "Operación Exitosa", 
    							"Operación Exitosa", "TRANSFRECEPOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
    				}else{
    					listBeanTranSpeiRecNOK.add(beanReceptoresSPID);
    					helperRecepcionOperacion.setBitacoraTrans("01TRANSFRECOP", beanReceptoresSPIDTemp.getCodError(), "", "", 
    							"", "TRANSFRECEPOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
    				}
    				
    			}
    		}
    	}else{
    		beanResConsTranSpeiRecTemp.setCodError(Errores.ED00068V);
    		beanResConsTranSpeiRecTemp.setTipoError(Errores.TIPO_MSJ_ALERT);
    	}
    	beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    	beanResReceptoresSPID.setCodError(beanResConsTranSpeiRecTemp.getCodError());
    	beanResReceptoresSPID.setTipoError(beanResConsTranSpeiRecTemp.getTipoError());
    	//beanResReceptoresSPID.setNumCuentaRec(beanResConsTranSpeiRecTemp.getNumCuentaRec());
    	beanResReceptoresSPID.setListReceptoresSPIDNOK(listBeanTranSpeiRecNOK);
    	beanResReceptoresSPID.setListReceptoresSPIDOK(listBeanTranSpeiRecOK);
    	beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
    	return beanResReceptoresSPID;   
    }
    

    
    @Override
    public BeanResReceptoresSPID devolverTransferSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
    	HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
    	BeanReceptoresSPID beanReceptoresSPID = null;
    	BeanResReceptoresSPID beanResReceptoresSPID = null;
    	
    	BeanResReceptoresSPID beanResReceptoresSPIDTemp = new BeanResReceptoresSPID();
    	List<BeanReceptoresSPID> listBeanReceptoresSPIDTemp = null;
    	listBeanReceptoresSPIDTemp = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID, HelperSPIDRecepcionOperacion.DEVOLUCION);
    	if(!listBeanReceptoresSPIDTemp.isEmpty()){
    		for(int i=0;i<listBeanReceptoresSPIDTemp.size();i++){
    			beanReceptoresSPID = listBeanReceptoresSPIDTemp.get(i);
    			if(!"0".equals(beanReceptoresSPID.getDatGenerales().getTipoPago()) && !"5".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())){
    				debug("devolverTransferSpeiRec beanReceptoresSPID.getEnvDev().getMotivoDev()"+beanReceptoresSPID.getEnvDev().getMotivoDev());
    				info("devolverTransferSpeiRec beanReceptoresSPID.getEnvDev().getMotivoDev()"+beanReceptoresSPID.getEnvDev().getMotivoDev());
    				if(beanReceptoresSPID.getEnvDev() !=null && beanReceptoresSPID.getEnvDev().getMotivoDev() !=null 
    						&& !"99".equals(beanReceptoresSPID.getEnvDev().getMotivoDev()) && !"".equals(beanReceptoresSPID.getEnvDev().getMotivoDev())){
						beanReceptoresSPID = envia(beanReceptoresSPID,true, "DV",sessionBean);
						if("TRIB0000".equals(beanReceptoresSPID.getCodError())){
							daoRecepOperacion.desApartar(beanReceptoresSPID, "RECEP_OP_SPID", sessionBean);
							helperRecepcionOperacion.setBitacoraTrans("01DEVOLRECOP", beanReceptoresSPID.getCodError(), "EXITO", "Operación Exitosa", 
									"Operación Exitosa", "DEVOLVERECOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
						}else{
							
							helperRecepcionOperacion.setBitacoraTrans("01DEVOLRECOP", beanReceptoresSPID.getCodError(), "", "", 
									"", "DEVOLVERECOP", "", beanReceptoresSPID, sessionBean,dAOBitTransaccional);
						}
    				}else{
    					beanReceptoresSPID.setCodError(Errores.ED00067V);
        				beanReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
    				}
    			}else{
    				beanReceptoresSPID.setCodError(Errores.ED00103V);
    				beanReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
    			}    			
    		}
    		beanResReceptoresSPIDTemp.setCodError(beanReceptoresSPID.getCodError());
    		beanResReceptoresSPIDTemp.setTipoError(beanReceptoresSPID.getTipoError());
    		beanResReceptoresSPIDTemp.setBeanReceptoresSPID(beanReceptoresSPID);
		}else{
			beanResReceptoresSPIDTemp.setCodError(Errores.ED00068V);
			beanResReceptoresSPIDTemp.setTipoError(Errores.TIPO_MSJ_ALERT);
			
		}
    	beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    	beanResReceptoresSPID.setCodError(beanResReceptoresSPIDTemp.getCodError());
    	beanResReceptoresSPID.setTipoError(beanResReceptoresSPIDTemp.getTipoError());
    	beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
    	beanResReceptoresSPID.setBeanReceptoresSPID(beanResReceptoresSPIDTemp.getBeanReceptoresSPID());    	
  		return beanResReceptoresSPID;
    }
    
    /**
     * @param beanReceptoresSPID BeanReceptoresSPID  
     * @param esDevolucion boolean booleano que indica si es devolucion
     * @param estatus String con el estatus
     * @param sessionBean ArchitechSessionBean Objeto de la arquitectura
     * @return BeanResConsTranSpeiRec Objeto de respuesta
     */
    private BeanReceptoresSPID envia(
    		BeanReceptoresSPID beanReceptoresSPID,boolean esDevolucion,String estatus, ArchitechSessionBean sessionBean){
    	String tipoOperacion ="";
    	UtileriasBitTrans utileriasBitTrans  = UtileriasBitTrans.getUtileriasBitTrans();
    	HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
    	String cveTranfe = "";
    	String trama ="";
    	UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
    	String tramaRespuesta ="";
    	BeanReqActTranSpeiRec beanReqActTranSpeiRec = null;
    	ResBeanEjecTranDAO resBeanEjecTranDAO = null;
    	if(!esDevolucion){
    		trama = helperRecepcionOperacion.armarTramaTercerorTercero(beanReceptoresSPID, sessionBean);
    		cveTranfe = "944";
    		tipoOperacion = "R";
    	}else{
    		trama = helperRecepcionOperacion.armarTramaDevoluciones(beanReceptoresSPID, sessionBean);
    		tipoOperacion = "D";
    	}
    	
    	beanReceptoresSPID.setMsgError(trama);
    	resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(trama, "TRANSPID", "ARQ_MENSAJERIA", sessionBean);
    	if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
    		tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
    		if(tramaRespuesta.length()>0 && "TRIB0000".equals(tramaRespuesta.substring(0,8))){
    			beanReqActTranSpeiRec = utileriasBitTrans.getBeanReqActTranSpeiRec( beanReceptoresSPID, estatus);
    		}else{
    			if (!"0".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())){
    				beanReceptoresSPID.getEnvDev().setMotivoDev(helperRecepcionOperacion.getMotivoRechazo(tramaRespuesta.substring(0,8)));
    			}
    			beanReqActTranSpeiRec = utileriasBitTrans.getBeanReqActTranSpeiRec(beanReceptoresSPID,"RE");
    		}
			beanReqActTranSpeiRec.setCodigoError(tramaRespuesta.substring(0,8));
			beanReqActTranSpeiRec.setReferenciaTransfer(tramaRespuesta.substring(78,85));
			daoRecepOperacion2.actualizaTransSpeiRec(beanReqActTranSpeiRec,  sessionBean);
			beanReceptoresSPID.setCodError(beanReqActTranSpeiRec.getCodigoError());
			beanReceptoresSPID.getDatGenerales().setRefeTransfer(tramaRespuesta.substring(78,85));
			beanReceptoresSPID.getEstatus().setEstatusTransfer(tramaRespuesta.substring(115,117));
			if(tramaRespuesta.length()>0 && "TRIB0000".equals(tramaRespuesta.substring(0,8))){
				//Solo se almacena cuando es un TRIB0000 se reviso por telefono con alex galindo
				daoRecepOperacion2.guardaTranSpeiHorasMan(cveTranfe, beanReceptoresSPID,beanReqActTranSpeiRec,tipoOperacion, sessionBean);
				beanReceptoresSPID.setTipoError(Errores.TIPO_MSJ_INFO);
				beanReceptoresSPID.setCodError(tramaRespuesta.substring(0,8));
			}else{
				beanReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
				beanReceptoresSPID.setCodError(tramaRespuesta.substring(0,8));
			}
    	}else{
    		beanReceptoresSPID.setCodError("ED00090V");
    		beanReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ERROR);
    	}
    	return beanReceptoresSPID;
    }
    

 




	
	@Override
	public BeanResReceptoresSPID recuperarTransfSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
		BeanReqInsertBitAdmon beanReqInsertBitAdmon = null;
		String jsonAnt ="";
		UtileriasBitTrans utileriasBitTrans  = UtileriasBitTrans.getUtileriasBitTrans();
		UtileriasBitAdmon  util =UtileriasBitAdmon.getUtileriasBitAdmon();
		String codError = "OK00000V";
		String tipoError =Errores.TIPO_MSJ_INFO;
  			HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
		BeanReqActTranSpeiRec beanReqActTranSpeiRec = null;
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = null;
    	List<BeanReceptoresSPID> listBeanReceptoresSPIDTemp = null;
    	listBeanReceptoresSPIDTemp = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID, HelperSPIDRecepcionOperacion.RECUPERA);
		if(!listBeanReceptoresSPIDTemp.isEmpty()){
			for(BeanReceptoresSPID beanReceptoresSPID:listBeanReceptoresSPIDTemp){
	      		beanReqActTranSpeiRec = utileriasBitTrans.getBeanReqActTranSpeiRec(beanReceptoresSPID,"RE");
	      		beanReqActTranSpeiRec.setCodigoError("RECUPERA");
	      		BeanSPIDLlave beanSPIDLlave = beanReceptoresSPID.getLlave();
	      		beanReqActTranSpeiRec.setFechaOperacion(beanSPIDLlave.getFchOperacion());
	    		beanReqActTranSpeiRec.setCveInstOrd(beanSPIDLlave.getCveInstOrd());
	    		beanReqActTranSpeiRec.setCveMiInstituc(beanSPIDLlave.getCveMiInstituc());
	    		beanReqActTranSpeiRec.setFolioPago(beanSPIDLlave.getFolioPago());
	    		beanReqActTranSpeiRec.setFolioPaquete(beanSPIDLlave.getFolioPaquete());
	      		if(Integer.parseInt(beanReceptoresSPID.getDatGenerales().getTipoPago())>0){
	      			beanReqActTranSpeiRec.setMotivoDevol("99"); 			
	      		}
	      		beanResActTranSpeiRecDAO = daoRecepOperacion2.actualizaTransSpeiRec(beanReqActTranSpeiRec,  sessionBean);
	      		if(!Errores.OK00000V.equals(beanResActTranSpeiRecDAO.getCodError())){
	      			codError = beanResActTranSpeiRecDAO.getCodError();
	      			tipoError = Errores.TIPO_MSJ_ERROR;
	      		}else{
	      			jsonAnt = util.transformJavaToJson(beanReceptoresSPID);
	      			beanReceptoresSPID.getEstatus().setCodigoError("RECUPERA");
	      			beanReceptoresSPID.getEstatus().setEstatusTransfer("RE");
	      			if(Integer.parseInt(beanReceptoresSPID.getDatGenerales().getTipoPago())>0){
	      				beanReceptoresSPID.getEnvDev().setMotivoDev("99");
		      		}
	      			beanReqInsertBitAdmon = util.createBitacoraBitTrans( beanReceptoresSPID.getLlave().getFchOperacion(),
	    					"GUARDAR",jsonAnt, util.transformJavaToJson(beanReceptoresSPID), "ACTUALIZAN DATOS", 
	    					util.createDatoFijo(beanReceptoresSPID.getLlave().getFchOperacion(), beanReceptoresSPID.getLlave().getCveMiInstituc(), 
	    							beanReceptoresSPID.getLlave().getCveInstOrd(), beanReceptoresSPID.getLlave().getFolioPaquete(), beanReceptoresSPID.getLlave().getFolioPago()), 
	    							"recuperarSPIDRecOp.do", "NA", "TRAN_SPID_REC", "RECSPIDREC", "TRANSFER");
	    			dAOBitAdmon.guardaBitacora(beanReqInsertBitAdmon, sessionBean);
	      		}				
	        }
		}else{
			codError = Errores.ED00068V;
  			tipoError = Errores.TIPO_MSJ_ALERT;
		}        
        beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
        beanResReceptoresSPID.setCodError(codError);
        beanResReceptoresSPID.setTipoError(tipoError);
        beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
		return beanResReceptoresSPID;
	}

	


	@Override
	public BeanResReceptoresSPID actMotRechazo(
			BeanReqReceptoresSPID beanReqReceptoresSPID,
			ArchitechSessionBean sessionBean) {
		BeanReqInsertBitAdmon beanReqInsertBitAdmon = null;
		UtileriasBitAdmon  util =UtileriasBitAdmon.getUtileriasBitAdmon();
		UtileriasBitTrans utileriasBitTrans  = UtileriasBitTrans.getUtileriasBitTrans();
		StringBuilder strBuilder = new StringBuilder();
		String estatus = "OK";
		String codError = "OK00000V";
		String msgEstatus ="Exitoso";
		String tipoError =Errores.TIPO_MSJ_INFO;
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora = null;
		HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
		BeanReceptoresSPID beanReceptoresSPIDTemp = null;
		BeanReqActTranSpeiRec beanReqActTranSpeiRec = null;
		BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = null;
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		List<BeanReceptoresSPID> listBeanReceptoresSPID = new ArrayList<BeanReceptoresSPID>();
		List<BeanReceptoresSPID> listBeanReceptoresSPIDTemp = null;
		listBeanReceptoresSPIDTemp = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID,HelperSPIDRecepcionOperacion.SELECCIONADO);
		
		if(!listBeanReceptoresSPIDTemp.isEmpty()){
			for(BeanReceptoresSPID beanReceptoresSPID:listBeanReceptoresSPIDTemp){
				if(beanReceptoresSPID.getEnvDev() !=null && beanReceptoresSPID.getEnvDev().getMotivoDev() !=null && 
						!"".equals(beanReceptoresSPID.getEnvDev().getMotivoDev())&& !beanReceptoresSPID.getEnvDev().getMotivoDev().equals(
						beanReceptoresSPID.getEnvDev().getMotivoDevAnt())){
					listBeanReceptoresSPID.add(beanReceptoresSPID);
		      		beanReqReceptoresSPID.setListBeanReceptoresSPID(listBeanReceptoresSPID);

				}else{
		      		
					beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
					beanResReceptoresSPID.setCodError(Errores.ED00067V);
					beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
		      		return beanResReceptoresSPID;
		      	  }
			}
		}else{
			beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
			beanResReceptoresSPID.setCodError(Errores.ED00068V);
			beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
      		return beanResReceptoresSPID;
		}
		
		for(int i=0;i<listBeanReceptoresSPID.size();i++){
			beanReceptoresSPIDTemp = listBeanReceptoresSPID.get(i);
			beanReqActTranSpeiRec = utileriasBitTrans.getBeanReqActTranSpeiRec( beanReceptoresSPIDTemp, "");
			beanResActTranSpeiRecDAO = daoRecepOperacion2.actualizaMotivoRec(beanReqActTranSpeiRec, sessionBean);
			
			if(!Errores.OK00000V.equals(beanResActTranSpeiRecDAO.getCodError())){
				codError = Errores.ED00064V;
				tipoError = Errores.TIPO_MSJ_ERROR;
			}else{
				String jsonAnt = util.transformJavaToJson(beanReceptoresSPIDTemp);
				
				beanReqInsertBitAdmon = util.createBitacoraBitTrans( beanReceptoresSPIDTemp.getLlave().getFchOperacion(),
    					"GUARDAR",jsonAnt, util.transformJavaToJson(beanReceptoresSPIDTemp), "ACTUALIZAN DATOS", 
    					util.createDatoFijo(beanReceptoresSPIDTemp.getLlave().getFchOperacion(), beanReceptoresSPIDTemp.getLlave().getCveMiInstituc(), 
    							beanReceptoresSPIDTemp.getLlave().getCveInstOrd(), beanReceptoresSPIDTemp.getLlave().getFolioPaquete(), beanReceptoresSPIDTemp.getLlave().getFolioPago()), 
    							"actMotRechazoSPIDRecOp.do", "NA", "TRAN_SPID_REC", "ACTMOTREC", "TRANSFER");
    			dAOBitAdmon.guardaBitacora(beanReqInsertBitAdmon, sessionBean);
			}
		}
		beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
		beanResReceptoresSPID.setCodError(codError);
		beanResReceptoresSPID.setTipoError(tipoError);
		beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
		return beanResReceptoresSPID;
	}
	
	@Override
	public BeanResReceptoresSPID rechazarTransfSpeiRec(
			BeanReqReceptoresSPID beanReqReceptoresSPID,
			ArchitechSessionBean sessionBean){
		UtileriasBitTrans utileriasBitTrans  = UtileriasBitTrans.getUtileriasBitTrans();
		StringBuilder strBuilder = new StringBuilder();
		String err[] = new String[]{"OK","OK00000V","Exitoso",Errores.TIPO_MSJ_INFO};
		HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		BeanReceptoresSPID beanReceptoresSPIDTemp = null;
		BeanReqActTranSpeiRec beanReqActTranSpeiRec = null;
		BeanResActTranSpeiRecDAO beanResActTranSpeiRecDAO = null;
    	List<BeanReceptoresSPID> listBeanReceptoresSPID = new ArrayList<BeanReceptoresSPID>();
    	List<BeanReceptoresSPID> listBeanReceptoresSPIDTemp = null;
    	listBeanReceptoresSPIDTemp = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID,HelperSPIDRecepcionOperacion.SELECCIONADO);
    	if(!listBeanReceptoresSPIDTemp.isEmpty()){
    		for(BeanReceptoresSPID beanReceptoresSPID:listBeanReceptoresSPIDTemp){
    			if(!"LQ".equals(beanReceptoresSPID.getEstatus().getEstatusBanxico())){
    				beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    				beanResReceptoresSPID.setCodError(Errores.ED00069V);
    				beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
		      		return beanResReceptoresSPID;
    			}
    		}
    		
    		for(BeanReceptoresSPID beanReceptoresSPID:listBeanReceptoresSPIDTemp){
	        	if(!"".equals(beanReceptoresSPID.getEnvDev().getMotivoDev())){
	        		listBeanReceptoresSPID.add(beanReceptoresSPID);
		      		beanReqReceptoresSPID.setListBeanReceptoresSPID(listBeanReceptoresSPID);	
		      	  }else{
		      		beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
		      		beanResReceptoresSPID.setCodError(Errores.ED00067V);
		      		beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
		      		return beanResReceptoresSPID;
		      	  }
	        }
    	}else{
    		beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    		beanResReceptoresSPID.setCodError(Errores.ED00068V);
    		beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
      		return beanResReceptoresSPID;
		}
    	for(int i=0;i<listBeanReceptoresSPID.size();i++){
    		beanReceptoresSPIDTemp = listBeanReceptoresSPID.get(i);
			beanReqActTranSpeiRec = utileriasBitTrans.getBeanReqActTranSpeiRec( beanReceptoresSPIDTemp, "RE");
			beanResActTranSpeiRecDAO = daoRecepOperacion2.rechazar(beanReqActTranSpeiRec, sessionBean);
			if(!Errores.OK00000V.equals(beanResActTranSpeiRecDAO.getCodError())){
				beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
				err = new String[]{"ER",Errores.ED00064V,"ERROR",Errores.TIPO_MSJ_ERROR};
			}
			strBuilder.append("MOTIVO_DEV=");
			strBuilder.append(beanReceptoresSPIDTemp.getEnvDev().getMotivoDev());
			strBuilder.append(utileriasBitTrans.seteaCampos(beanReceptoresSPIDTemp.getBancoOrd().getNumCuentaOrd(),beanReceptoresSPIDTemp.getBancoRec().getNumCuentaRec(),
					beanReceptoresSPIDTemp.getDatGenerales().getMonto().toString()));

			helperRecepcionOperacion.setBitacoraTrans("03RECHAZAR", err[1], err[2], "RECHAZAR LA OPERACIÓN", 
					strBuilder.toString(), "RECHAZAR", "", beanReceptoresSPIDTemp, sessionBean,dAOBitTransaccional);
		}
    	beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    	beanResReceptoresSPID.setOpcion(beanReqReceptoresSPID.getOpcion());
    	beanResReceptoresSPID.setTipoError(err[3]);
    	beanResReceptoresSPID.setCodError(err[1]);
    	beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
		return beanResReceptoresSPID;
	}


    @Override
    public BeanResReceptoresSPID apartar(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
    	HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
    	List<BeanReceptoresSPID> listBeanReceptoresSPID = null;
    	BeanReceptoresSPID beanReceptoresSPID = null;
    	BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
    	BeanResConsTranSpeiRec beanResConsTranSpeiRecTemp = new BeanResConsTranSpeiRec();
    	listBeanReceptoresSPID = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID,HelperSPIDRecepcionOperacion.SELECCIONADO);
    	if(!listBeanReceptoresSPID.isEmpty()){
    		beanResConsTranSpeiRecTemp.setCodError(Errores.OK00001V);
    		for(int i=0;i<listBeanReceptoresSPID.size();i++){
    			beanReceptoresSPID = listBeanReceptoresSPID.get(i);	
    			daoRecepOperacion.apartar(beanReceptoresSPID, "RECEP_OPSPID", sessionBean);
    		}    		
    	}else{
    		beanResConsTranSpeiRecTemp.setCodError(Errores.ED00068V);
    		beanResConsTranSpeiRecTemp.setTipoError(Errores.TIPO_MSJ_ALERT);
    	}
    	beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    	beanResReceptoresSPID.setCodError(beanResConsTranSpeiRecTemp.getCodError());
    	beanResReceptoresSPID.setTipoError(beanResConsTranSpeiRecTemp.getTipoError());
    	beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
    	return beanResReceptoresSPID;   
    }

    @Override
    public BeanResReceptoresSPID desApartar(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
    	HelperSPIDRecepcionOperacion helperRecepcionOperacion = new HelperSPIDRecepcionOperacion();
    	List<BeanReceptoresSPID> listBeanReceptoresSPID = null;
    	BeanReceptoresSPID beanReceptoresSPID = null;
    	BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
    	BeanResConsTranSpeiRec beanResConsTranSpeiRecTemp = new BeanResConsTranSpeiRec();
    	listBeanReceptoresSPID = helperRecepcionOperacion.filtraSelecionados(beanReqReceptoresSPID,HelperSPIDRecepcionOperacion.SELECCIONADO);
    	if(!listBeanReceptoresSPID.isEmpty()){
    		beanResConsTranSpeiRecTemp.setCodError(Errores.OK00001V);
    		for(int i=0;i<listBeanReceptoresSPID.size();i++){
    			beanReceptoresSPID = listBeanReceptoresSPID.get(i);	
    			daoRecepOperacion.desApartar(beanReceptoresSPID, "RECEP_OPSPID", sessionBean);
    		}    		
    	}else{
    		beanResConsTranSpeiRecTemp.setCodError(Errores.ED00068V);
    		beanResConsTranSpeiRecTemp.setTipoError(Errores.TIPO_MSJ_ALERT);
    	}
    	beanResReceptoresSPID = consultaTodasTransf(beanReqReceptoresSPID, sessionBean);
    	beanResReceptoresSPID.setCodError(beanResConsTranSpeiRecTemp.getCodError());
    	beanResReceptoresSPID.setTipoError(beanResConsTranSpeiRecTemp.getTipoError());	
    	beanResReceptoresSPID.setImporteBusqueda(beanReqReceptoresSPID.getImporteBusqueda());
    	return beanResReceptoresSPID;   
    }

	/**
	 * Metodo get que sirve para obtener la propiedad daoRecepOperacion
	 * @return daoRecepOperacion Objeto del tipo DAORecepOperacionSPID
	 */
	public DAORecepOperacionSPID getDaoRecepOperacion() {
		return daoRecepOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad daoRecepOperacion
	 * @param daoRecepOperacion del tipo DAORecepOperacionSPID
	 */
	public void setDaoRecepOperacion(DAORecepOperacionSPID daoRecepOperacion) {
		this.daoRecepOperacion = daoRecepOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad daoRecepOperacion2
	 * @return daoRecepOperacion2 Objeto del tipo DAORecepOperacionSPID2
	 */
	public DAORecepOperacionSPID2 getDaoRecepOperacion2() {
		return daoRecepOperacion2;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad daoRecepOperacion2
	 * @param daoRecepOperacion2 del tipo DAORecepOperacionSPID2
	 */
	public void setDaoRecepOperacion2(DAORecepOperacionSPID2 daoRecepOperacion2) {
		this.daoRecepOperacion2 = daoRecepOperacion2;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOComunesSPID
	 * @return dAOComunesSPID Objeto del tipo DAOComunesSPID
	 */
	public DAOComunesSPID getDAOComunesSPID() {
		return dAOComunesSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOComunesSPID
	 * @param comunesSPID del tipo DAOComunesSPID
	 */
	public void setDAOComunesSPID(DAOComunesSPID comunesSPID) {
		dAOComunesSPID = comunesSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOBitTransaccional
	 * @return dAOBitTransaccional Objeto del tipo DAOBitTransaccional
	 */
	public DAOBitTransaccional getDAOBitTransaccional() {
		return dAOBitTransaccional;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOBitTransaccional
	 * @param bitTransaccional del tipo DAOBitTransaccional
	 */
	public void setDAOBitTransaccional(DAOBitTransaccional bitTransaccional) {
		dAOBitTransaccional = bitTransaccional;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOBitAdmon
	 * @return dAOBitAdmon Objeto del tipo DAOBitAdmon
	 */
	public DAOBitAdmon getDAOBitAdmon() {
		return dAOBitAdmon;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOBitAdmon
	 * @param bitAdmon del tipo DAOBitAdmon
	 */
	public void setDAOBitAdmon(DAOBitAdmon bitAdmon) {
		dAOBitAdmon = bitAdmon;
	}








    


}
