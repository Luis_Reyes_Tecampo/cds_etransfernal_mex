/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaCertificadoImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	By 	      					Company 	Description
 * -------  ----------- --------------------------	---------- 	----------------------
 * 	 1.0	05/07/2018	Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */

package mx.isban.eTransferNal.servicio.ws;
//Imports java
import java.util.List;
//Imports javax
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
//Imports etransferNal
import mx.isban.eTransferNal.beans.ws.BeanDatosTitular;
import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificadoDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOConsultaCertificado;
import mx.isban.eTransferNal.servicios.ws.BOConsultaCertificado;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
//Imports metrics
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;

/**
 * Class BOConsultaCertificadoImpl
 * 
 * @author ING. Juan Manuel Fuentes Ramos. CSA
 * Plan Delta Sucursales. 2018 - 06
 * Implementacion modulo metrics
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaCertificadoImpl extends Architech implements BOConsultaCertificado{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 8494241431286179914L;
	/** Propiedad del tipo DAOConsultaCertificado que almacena el valor de dAOConsultaCertificado. */
	@EJB private DAOConsultaCertificado dAOConsultaCertificado;

	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** EJB para bitacora metrics*/
	@EJB private BitacorizaMetrics metricsBo;
	/** Bean para monitoreo*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	// END [ADD: JMFR CSA Monitoreo Metrics]
		
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.isban.eTransferNal.servicios.ws.BOConsultaCertificado#consultaCertificado(mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado)
	 */
	@Override
	public BeanResConsultaCertificado consultaCertificado(final BeanReqConsultaCertificado reqBeanConsultaCertificado) {
		//Se genera una instancia de la clase utilerias para dar formato a las fechas
		Utilerias utilerias = Utilerias.getUtilerias();
		//Se crea lista para respuesta del servicio
		List<String> resString = null;
		BeanDatosTitular beanDatosTitular = new BeanDatosTitular();
		BeanResConsultaCertificado res = new BeanResConsultaCertificado();
		res.setDatosTitular(beanDatosTitular);
		BeanResConsultaCertificadoDAO resDAO = new BeanResConsultaCertificadoDAO();
		//Se validan datos de peticion
		if(reqBeanConsultaCertificado.getNumCertificado()==null|| "".equals(reqBeanConsultaCertificado.getNumCertificado().trim())){
			res.setDatosTitular(beanDatosTitular);
			res.setCodRespuesta("ED00091V");
			res.setMensaje("El número de serie del certificado es un parámetro mandatorio");
			goMetricaErrorNegocio("ED00091V", "El número de serie del certificado es un parámetro mandatorio");
			return res;
		}
		//Se ejecuta peticion a WS externo
		resDAO = dAOConsultaCertificado.consultaCertificado(reqBeanConsultaCertificado);
		//Se valida respuesta WS externo
		if(Errores.OK00000V.equals(resDAO.getCodError())){//Respuesta OK
			resString = resDAO.getCertResponse();
			info("NumCertificado:"+reqBeanConsultaCertificado.getNumCertificado() +"COD_ERROR"+resDAO.getCodError()+"codError Banxico:"+resDAO.getCertResponse().get(0));
			if(!resString.isEmpty()&& "0".equals(resString.get(0))){//Respuesta exitosa
				res.setNumSerie(resString.get(2));
				String fchEmision = resString.get(3);
				fchEmision = utilerias.formatoToformatoFecha(fchEmision, Utilerias.FORMATO_DD_GUION_MM_GUION_YY, Utilerias.FORMATO_YYYY_GUION_MM_GUION_DD);
				res.setFchEmision(fchEmision);
				String fchExpiracion = resString.get(4);
				fchExpiracion = utilerias.formatoToformatoFecha(fchExpiracion, Utilerias.FORMATO_DD_GUION_MM_GUION_YY, Utilerias.FORMATO_YYYY_GUION_MM_GUION_DD);
				res.setFchExpiracion(fchExpiracion);
				res.setEstadoCertificado(resString.get(5));
				descomponeCadena(res,resString);
			}else{//Valida error del WS externo 
				validaCodigoError(resString.get(0));
			}
			res.setCodRespuesta(resString.get(0));
			res.setMensaje(resString.get(1));
		}else{//Error al ejecutar servicio externo
			validaCodigoError(res, resDAO);
		}
		//Se retorna respuesta del servicio
		return res;
	}

	/**
	 * Valida el codigo de error en la respuesta del WS externo
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param res respuesta hacia la capa de servicio 
	 * @param resDAO respuesta del servicio
	 */
	private void validaCodigoError(BeanResConsultaCertificado res, BeanResConsultaCertificadoDAO resDAO) {
		res.setCodRespuesta(resDAO.getCodError());
		if(Errores.ED00096V.equals(resDAO.getCodError())){
			res.setMensaje("El Archivo de Configuracion no se encuentra en la ruta cfg.properties ");
			goMetricaErrorCritico(Errores.ED00096V, res.getMensaje());
		}else if(Errores.ED00097V.equals(resDAO.getCodError())){
			res.setMensaje("El Archivo de Configuracion cfg.properties no se encuentra ");
			goMetricaErrorCritico(Errores.ED00097V, res.getMensaje());
		}else if(Errores.ED00098V.equals(resDAO.getCodError())){
			res.setMensaje("TimeOut a los servicios de BANXICO");
			goMetricaErrorCritico(Errores.ED00098V, res.getMensaje());
		}else if(Errores.ED00099V.equals(resDAO.getCodError())){
			res.setMensaje("Host Desconocido");
			goMetricaErrorCritico(Errores.ED00099V, res.getMensaje());
		}else if(Errores.ED00100V.equals(resDAO.getCodError())){
			res.setMensaje("Error desconocido de IO");
			goMetricaErrorCritico(Errores.ED00100V, res.getMensaje());
		}else if(Errores.ED00101V.equals(resDAO.getCodError())){
			res.setMensaje("Error desconocido datos vacios");
			goMetricaErrorCritico(Errores.ED00101V, res.getMensaje());
		}
	}

	/**
	 * Metodo que descompone la cadena
	 * @param beanResConsultaCertificado Bean con el certificado
	 * @param resString Objeto del tipo List<String>
	 */
	private void descomponeCadena(BeanResConsultaCertificado beanResConsultaCertificado,List<String> resString){
		String cad ="";
		String[] args = null;
		for(int i=0;i<resString.size();i++){
			cad = resString.get(i);
			args = cad.split("=");
			if(args.length>1){
				descomponeCadenaAux(beanResConsultaCertificado, args);
			}
		}
	}

	/**
	 * Metodo que descompone la cadena
	 * @param beanResConsultaCertificado Bean con el certificado
	 * @param args arreglo con los datos de la cadena
	 */
	private void descomponeCadenaAux(
			BeanResConsultaCertificado beanResConsultaCertificado, String[] args) {
		if(args[0].indexOf("Pa")==0){
			beanResConsultaCertificado.getDatosTitular().setPais(args[1]);
		}else if("Estado".equals(args[0])){
			beanResConsultaCertificado.getDatosTitular().setEstado(args[1]);
		}else if("Localidad".equals(args[0])){
			beanResConsultaCertificado.getDatosTitular().setLocalidad(args[1]);
		}else if(args[0].indexOf("Nombre de la organizaci")>=0){
			beanResConsultaCertificado.getDatosTitular().setNombreOrganizacion(args[1]);
		}else if("Nombre del titular".equals(args[0])){
			beanResConsultaCertificado.getDatosTitular().setNombreTitular(args[1]);
		}else if("RFC".equals(args[0])){
			beanResConsultaCertificado.getDatosTitular().setRfc(args[1]);
		}else if(args[0].indexOf("digo postal")>=0){
			beanResConsultaCertificado.getDatosTitular().setCodPostal(args[1]);
		}else if(args[0].indexOf("Direcci")>=0){
			beanResConsultaCertificado.getDatosTitular().setDireccion(args[1]);
		}else if("CURP".equals(args[0])){
			beanResConsultaCertificado.getDatosTitular().setCurp(args[1]);
		}else if(args[0].indexOf("pasaporte")>=0){
			beanResConsultaCertificado.getDatosTitular().setNumPasaporteIfe(args[1]);
		}else if(args[0].indexOf("Correo electr")>=0){
			beanResConsultaCertificado.getDatosTitular().setEmail(args[1]);
		}
	}
	
	/**
	 * Metodo para insertar error monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error 
	 * @param msgError mensaje de error
	 * @param traza traza completa del error
	 */
	private void insertaError(String codeError, String msgError, String traza){
		senderInit.initMetrics();
		metricsBo.bitacorizaError(senderInit.getDTOError(codeError,msgError,traza));
	}
	
	/**
	 * Metodo para asignar datos de error para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error
	 * @param traza traza completa del error
	 */
	private void goMetricaErrorNegocio(String codeError, String traza){
		insertaError(EnumMetricsErrors.DELTA_NEGOCIO_BO.getCodeError(), 
				EnumMetricsErrors.DELTA_NEGOCIO_BO.getMensaje()+
				BOConsultaCertificadoImpl.class.getCanonicalName() + " " +codeError, traza);
	}
	
	/**
	 * Metodo para asignar datos de error para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codeError codigo de error
	 * @param traza traza completa del error
	 */
	private void goMetricaErrorCritico(String codeError, String traza){
		insertaError(EnumMetricsErrors.DELTA_INFRA_BO.getCodeError(), 
				EnumMetricsErrors.DELTA_INFRA_BO.getMensaje()+
				BOConsultaCertificadoImpl.class.getCanonicalName() + " " +codeError, traza);
	}
	
	/**
	 * Metodo para validar codigo de error en el llamado al Ws externo
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @param codigoError codigo de error ws externo
	 */
	private void validaCodigoError(String codigoError) {
		if("-1".equals(codigoError)){
			goMetricaErrorNegocio("WSE001", 
					"La Autoridad Certificadora no se encuentra conectada, favor de intentarlo mas tarde.");
		}else if("-2".equals(codigoError)){
			goMetricaErrorNegocio("WSE002", 
					"El certificado consultado no esta registrado en la IES.");
		}else if("-3".equals(codigoError)){
			goMetricaErrorNegocio("WSE003", 
					"Error de conexion con la IES, si el problema persiste favor de ponerse en contacto con: ies@banxico.org.mx ");
		}else if("-4".equals(codigoError)){
			goMetricaErrorNegocio("WSE004", 
					"El parametro de entrada no cumple con el formato de un numero de serie de un certificado digital.");
		}else if("-5".equals(codigoError)){
			goMetricaErrorNegocio("WSE005", 
				"El certificado no pudo ser obtenido. Si el problema persiste favor de ponerse en contacto con: ies@banxico.org.mx " +
		"Ocurrio un problema al intentar obtener el certificado digital y el tiempo de respuesta fue excedido por lo que se presenta un timeout. ");
		}
	}
}