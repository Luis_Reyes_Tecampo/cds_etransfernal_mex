/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultarArchivosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResArchRespuesta;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaArchRespuesta;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchxFchCDAHist;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOConsultarArchivos;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Class BOConsultarArchivosImpl.
 *
 * @author ooamador
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultarArchivosImpl implements BOConsultarArchivos {
	
	/** DAO para las transacciones a BD. */
	@EJB
	private DAOConsultarArchivos daoConsultarArchivos;
	
	/** Propiedad del tipo DAOParametrosPOACOAImpl que almacena el valor de dAOParametrosPOACOAImpl. */
	@EJB
	private DAOGenerarArchxFchCDAHist dAOGenerarArchxFchCDAHist;

  /**
   * Metodo que sirve para consultar la informacion del monitor de
   * carga de archivos historicos.
   *
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param modulo El objeto: modulo
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchHist Objeto del tipo BeanResMonCargaArchHist
   */
   public BeanResConsultaArchRespuesta consultarArchivosProc(BeanPaginador beanPaginador, String modulo, ArchitechSessionBean architechSessionBean){
	   List<BeanResArchRespuesta> beanResArchRespuestaList = null;
	   /** Invocacion al DAO para consultar la fecha de operacion **/
	  BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOGenerarArchxFchCDAHist.consultaFechaOperacion(modulo, architechSessionBean); 
	  BeanResConsultaArchRespuesta beanResConsultaArchRespuesta = null;
	  if (beanPaginador == null) {
		  beanPaginador = new BeanPaginador();
	  }
      beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
      /** Se realiza la consulta invocando al DAO **/
      beanResConsultaArchRespuesta = daoConsultarArchivos.consultarArchivosProc(
    		  beanPaginador, beanResConsFechaOpDAO.getFechaOperacion(), modulo, architechSessionBean);
      beanResArchRespuestaList = beanResConsultaArchRespuesta.getListaBeanResArchProc();
      /** Se valida la respuesta de la operacion **/
      if(Errores.CODE_SUCCESFULLY.equals(beanResConsultaArchRespuesta.getCodError()) && beanResArchRespuestaList.isEmpty()){
    	  beanResConsultaArchRespuesta.setCodError(Errores.ED00011V);
    	  beanResConsultaArchRespuesta.setTipoError(Errores.TIPO_MSJ_INFO);
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsultaArchRespuesta.getCodError())){
    	  beanResConsultaArchRespuesta.setCodError(Errores.OK00000V);
    	  beanResConsultaArchRespuesta.setListaBeanResArchProc(beanResArchRespuestaList);
          beanPaginador.calculaPaginas(beanResConsultaArchRespuesta.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
          beanResConsultaArchRespuesta.setBeanPaginador(beanPaginador);
      }else{
    	  beanResConsultaArchRespuesta.setCodError(Errores.ED00137V);
    	  beanResConsultaArchRespuesta.setTipoError(Errores.TIPO_MSJ_ERROR);
      }
      /** Retorno del metodo **/
      return beanResConsultaArchRespuesta;
   }

	/**
	 * Devuelve el valor de daoConsultarArchivos.
	 *
	 * @return the daoConsultarArchivos
	 */
	public DAOConsultarArchivos getDaoConsultarArchivos() {
		return daoConsultarArchivos;
	}

	/**
	 * Modifica el valor de daoConsultarArchivos.
	 *
	 * @param daoConsultarArchivos the daoConsultarArchivos to set
	 */
	public void setDaoConsultarArchivos(DAOConsultarArchivos daoConsultarArchivos) {
		this.daoConsultarArchivos = daoConsultarArchivos;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOGenerarArchxFchCDAHist.
	 *
	 * @return DAOGenerarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public DAOGenerarArchxFchCDAHist getDAOGenerarArchxFchCDAHist() {
		return dAOGenerarArchxFchCDAHist;
	}

	/**
	 * Metodo que modifica el valor de la propiedad dAOGenerarArchxFchCDAHist.
	 *
	 * @param generarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public void setDAOGenerarArchxFchCDAHist(
			DAOGenerarArchxFchCDAHist generarArchxFchCDAHist) {
		dAOGenerarArchxFchCDAHist = generarArchxFchCDAHist;
	}

}
