/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOContingMismoDiaImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:12:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDiaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResEstadoConexionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDiaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOContingMismoDiaSPID;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOMonitorCDASPID;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 *Clase del tipo BO que se encarga del negocio para la funcionalidad de
 * Contingencia CDA Mismo Dia
 **/
//Clase del tipo BO que se encarga del negocio para la funcionalidad de
@Remote(BOContingMismoDiaSPID.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOContingMismoDiaSPIDImpl extends Architech implements
		BOContingMismoDiaSPID {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 6087179653189213838L;

	/**
	 * Constante privada del genArchContigMismoDia
	 */
	private static final String GEN_ARCH_CONTIG_MISMO_DIA_DO = "genArchContigMismoDia.do";
	
	/**
	 * Constante privada del PENDIENTE
	 */
	private static final String PENDIENTE = "PENDIENTE";
	
	/**
	 * Constante privada del GENARCHDIA
	 */
	private static final String GENARCHDIA = "GENARCHDIA";
	
	/**
	 * Constante privada del TRAN_SPEI_CTG_CDA
	 */
	private static final String TRAN_SPEI_CTG_CDA = "TRAN_SPEI_CTG_CDA";
	
	/**
	 * Constante privada del ID_PETICION
	 */
	private static final String ID_PETICION = "ID_PETICION";
	
	/**
	 * Constante privada del INSERT
	 */
	private static final String INSERT = "INSERT";
		
	/**
	 * Referencia privada al dao de Contingencia mismo dia
	 */
	@EJB
	private DAOContingMismoDiaSPID daoContingMismoDia;

	/**
	 * Referencia privada al dao de MonitorCDA
	 */
	@EJB
	private DAOMonitorCDASPID daoMonitorCDA;

	/**
	 * Referencia privada al dao de la bitacora admin
	 */
	@EJB
	private DAOBitacoraAdmon daoBitacora;

	
	/**
	 * Metodo DAO para obtener la informacion para la funcionalidad de
	 * Contingencia CDA mismo dia
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDiaDAO Objeto del tipo
	 *         BeanResContingMismoDiaDAO
	 * 
	 */
	//Metodo DAO para obtener la informacion para la funcionalidad de Contingencia CDA mismo dia
	public BeanResContingMismoDia consultaPendientesMismoDia(
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Se crean objetos de respuesta
		BeanResContingMismoDia beanResContingMismoDia = null;
		BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = null;
		//Se hace la consulta de pendientes del dia
		beanResContingMismoDiaDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
		beanResContingMismoDia = new BeanResContingMismoDia();
		//Se valida codigo de error
		if(Errores.CODE_SUCCESFULLY.equals(beanResContingMismoDiaDAO.getCodError())){
			beanResContingMismoDia.setHayOperacionespendientes(beanResContingMismoDiaDAO.getOperacionesPendientes()>0);
		}else{
			throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
		}
		//Se devuelve bean de respuesta
		return beanResContingMismoDia;
	}
	
	/**
	 * Metodo de negocio que calcula la pagina y que hace el llamado al dao para
	 * obtener la informacion para la funcionalidad de Contingencia CDA mismo
	 * dia
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanReqPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDia Objeto del tipo BeanResContingMismoDia
	 *                de negocio
	 */
	//Metodo de negocio que calcula la pagina y que hace el llamado al dao para obtener la informacion para la funcionalidad de Contingencia CDA mismo dia
	public BeanResContingMismoDia consultaContingMismoDia(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Se crean objetos de respeusta
		BeanResContingMismoDiaDAO beanResContingMismoDiaPendientesDAO =null;
		BeanResContingMismoDia beanResContingenciaMismoDia = null;
		BeanResContingMismoDiaDAO beanResContingMismoDiaDAO = null;
		List<BeanContingMismoDia> listBeanContingMismoDia = null;
		beanPaginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		//Se hace la consulta al dao
		beanResContingMismoDiaDAO = daoContingMismoDia.consultaContingMismoDia(
				beanPaginador, architechSessionBean);
		//Se crean objetos de respeusta
		beanResContingenciaMismoDia = new BeanResContingMismoDia();
		listBeanContingMismoDia = beanResContingMismoDiaDAO
				.getListBeanContingMismoDia();
		//Se valida codigo de error del bean de respuesta de la consulta
		if (Errores.CODE_SUCCESFULLY.equals(beanResContingMismoDiaDAO
				.getCodError())
				&& listBeanContingMismoDia.isEmpty()) {
			beanResContingenciaMismoDia.setCodError(Errores.ED00011V);
			beanResContingenciaMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
			//Se hace la consulta de los pendientes del mismo dia
			beanResContingMismoDiaPendientesDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
			//Se valida codigo de respuesta
			if(Errores.CODE_SUCCESFULLY.equals(
					beanResContingMismoDiaPendientesDAO.getCodError())){
				beanResContingenciaMismoDia.setHayOperacionespendientes(
						beanResContingMismoDiaPendientesDAO.getOperacionesPendientes()>0);
			}
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResContingMismoDiaDAO
				.getCodError())) {
			//Si codigo de respuesta es exitoso entonces se setea la lista de beans
			beanResContingenciaMismoDia
					.setListBeanContingMismoDia(listBeanContingMismoDia);
			//Se setea el total de registros
			beanResContingenciaMismoDia.setTotalReg(beanResContingMismoDiaDAO
					.getTotalReg());
			//Se hace el calculo de paginacion
			beanPaginador.calculaPaginas(beanResContingMismoDiaDAO
					.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResContingenciaMismoDia.setBeanPaginador(beanPaginador);
			beanResContingenciaMismoDia.setCodError(Errores.OK00000V);
			//Se hace la consulta de pendientes del dia
			beanResContingMismoDiaPendientesDAO = daoContingMismoDia.consultaPendientesMismoDia(architechSessionBean);
			//Se valida codigo de respuesta
			if(Errores.CODE_SUCCESFULLY.equals(
					beanResContingMismoDiaPendientesDAO.getCodError())){
				//Se establece si hay operaciones pendientes
				beanResContingenciaMismoDia.setHayOperacionespendientes(
						beanResContingMismoDiaPendientesDAO.getOperacionesPendientes()>0);
			}
		} else {
			//Se setea el codigo de error EC00011B
			throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
		}
		return beanResContingenciaMismoDia;
	}

	/**
	 * Metodo que sirve para indicar que se debe de generar el archvio de
	 * contingencia mismo dia
	 * 
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResGenArchContingMismoDia Objeto del tipo
	 *         BeanResArchContingMismoDia
	 */
	//Metodo que sirve para indicar que se debe de generar el archvio de contingencia mismo dia
	public BeanResGenArchContingMismoDia genArchContigMismoDia(
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Se crean objetos de apoyo
		String numRegistros = "0";
		Utilerias utilerias = Utilerias.getUtilerias();
		Date fechaHora = new Date();
		//Se crean objetos de respuesta
		final BeanResGenArchContingMismoDia beanResGenArchContingMismoDia = new BeanResGenArchContingMismoDia();
		BeanResContingMismoDia beanResContingenciaMismoDia = null;
		BeanResGenArchContingMismoDiaDAO beanResGenArchContingMismoDiaDAO = null;
		BeanResEstadoConexionDAO beanResEstadoConexionDAO = null;
		//Se hace la consulta del estado de conexion
		beanResEstadoConexionDAO = daoMonitorCDA
				.consultaEstadoConexion(architechSessionBean);
		//Se valida codigo de respuesta y contingencia
		if (Errores.CODE_SUCCESFULLY.equals(beanResEstadoConexionDAO
				.getCodError())
				&& beanResEstadoConexionDAO.getContingencia() == -1) {
			beanResGenArchContingMismoDia.setContingencia(true);
			//Se hace la llamada al dao para generar archivo de contingencia
			beanResGenArchContingMismoDiaDAO = daoContingMismoDia
					.genArchContingMismoDia(architechSessionBean);
			//Se valida codigo de respuesta
			if (Errores.CODE_SUCCESFULLY
					.equals(beanResGenArchContingMismoDiaDAO.getCodError())) {
	        	if(beanResGenArchContingMismoDiaDAO.getNumOpGeneradas()!=null){
	        		numRegistros = beanResGenArchContingMismoDiaDAO.getNumOpGeneradas().replaceAll(",","");
	        	}
	        	//Se genera codigo de bitacora
				BeanReqInsertBitacora beanReqInsertBitacora = utilerias
						.creaBitacora(GEN_ARCH_CONTIG_MISMO_DIA_DO,
								numRegistros, "", fechaHora,
								PENDIENTE, GENARCHDIA, TRAN_SPEI_CTG_CDA,
								ID_PETICION, Errores.OK00000V,
								"TRANSACCION EXITOSA", INSERT,beanResGenArchContingMismoDiaDAO.getIdPeticion());
				daoBitacora.guardaBitacora(beanReqInsertBitacora,
						architechSessionBean);
				//Se setea un codigo de error exitoso
				beanResGenArchContingMismoDia.setCodError(Errores.OK00000V);
				beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
			}
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResEstadoConexionDAO
				.getCodError())) {
			//Se genera e inserta registro en bitacora
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora(GEN_ARCH_CONTIG_MISMO_DIA_DO, "0", "",
							fechaHora, PENDIENTE, GENARCHDIA,
							TRAN_SPEI_CTG_CDA, ID_PETICION,
							Errores.ED00012V, "Estado Conectado, no gen arch",
							INSERT,"");
			daoBitacora.guardaBitacora(beanReqInsertBitacora,
					architechSessionBean);
			beanResGenArchContingMismoDia.setCodError(Errores.ED00012V);
			beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_INFO);
		} else {
			//Si hay error se registra en bitacora
			BeanReqInsertBitacora beanReqInsertBitacora = utilerias
					.creaBitacora(GEN_ARCH_CONTIG_MISMO_DIA_DO, "0", "",
							fechaHora, PENDIENTE, GENARCHDIA,
							TRAN_SPEI_CTG_CDA, ID_PETICION,
							Errores.EC00011B, "No hay comunicaci\u00F3n serv.",
							INSERT,"");
			daoBitacora.guardaBitacora(beanReqInsertBitacora,
					architechSessionBean);
			beanResGenArchContingMismoDia.setCodError(Errores.EC00011B);
			beanResGenArchContingMismoDia.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//Se hace la consulta de contingencia del mismo dia
		beanResContingenciaMismoDia = consultaContingMismoDia(
				new BeanPaginador(), architechSessionBean);
		beanResGenArchContingMismoDia
				.setListBeanContingMismoDia(beanResContingenciaMismoDia
						.getListBeanContingMismoDia());
		beanResGenArchContingMismoDia
				.setBeanPaginador(beanResContingenciaMismoDia
						.getBeanPaginador());
		beanResGenArchContingMismoDia.setTotalReg(beanResContingenciaMismoDia
				.getTotalReg());

		beanResGenArchContingMismoDia.setHayOperacionespendientes(
				beanResContingenciaMismoDia.isHayOperacionespendientes());
		return beanResGenArchContingMismoDia;
	}

}
