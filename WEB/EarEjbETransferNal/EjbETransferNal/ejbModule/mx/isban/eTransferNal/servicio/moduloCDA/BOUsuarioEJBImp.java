/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BORUsuarioEJBImpl.java
*
* Control de versiones:
*
* Version Date/Hour           By       Company Description
* ------- ------------------- -------- ------- --------------
* 1.0     29/06/2011 10:35:26 O Acosta ISBAN   Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloCDA;
 
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsCatPerfilDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioPerfilesDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanUsuario;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOPerfilamiento;
import mx.isban.eTransferNal.dao.moduloCDA.DAOUsuario;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Implementacion del objeto de negocio de los usuarios con
 * acceso a Transfer.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOUsuarioEJBImp extends Architech
        implements BOUsuario{


    /**
	 * 
	 */
	private static final long serialVersionUID = -6797512565200688967L;

	/**
     * El caracter con el que se separan los perfiles.
     **/
    private static final String SEPARADOR_PERFILES = ",";
    
    /**
     * El servicio que registrara los cambios efectuados.
     **/
    private static final String SERVICIO = "CONTROLLERPRINCIPAL.JAVA";
    /**
     * El servicio que registrara los cambios efectuados.
     **/
    private static final String CADENA_VACIA = "";

    /**
     * Objeto de Acceso a Datos para los usuarios con acceso a
     * Transfer.
     **/
    @EJB
    private DAOUsuario daoUsuario;

    /**
     * El Objeto de Acceso a Datos para los perfiles de Transfer.
     **/
    @EJB
    private DAOPerfilamiento perfilDao;


    /** {@inheritDoc} */
    public void guardarUsuario(ArchitechSessionBean sessionBean)
            throws BusinessException {
    	BeanResUsuarioDAO beanResUsuarioDAO = null;
    	BeanResUsuarioPerfilesDAO beanResUsuarioPerfilesDAO = null;
        BeanUsuario usuarioDTO = null;
        String hoy = null;
        List<BeanPerfil> perfilesSam = null;
        List<BeanPerfil> perfilesEliminar = null;
        List<BeanPerfil> perfilesInsertar = null;
        debug("Actualizando los Datos del usuario en Transfer.");
        if ("".equals(sessionBean.getUsuario())) {
            debug("No se pudo obtener informacion del usuario");
            return;
        }
        Utilerias utilerias = Utilerias.getUtilerias();
        hoy = utilerias.formateaFecha(new Date(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
        beanResUsuarioDAO = daoUsuario.consultaUsuarioPorClave(
                sessionBean.getUsuario().trim(), sessionBean);
        if(Errores.CODE_SUCCESFULLY.equals(beanResUsuarioDAO.getCodError())){
        	perfilesSam = getPerfilesUsuario(sessionBean.getPerfil());
        	usuarioDTO = beanResUsuarioDAO.getUsuario();
            if (usuarioDTO != null) {
            	beanResUsuarioPerfilesDAO = daoUsuario.consultaUsuarioPerfiles(sessionBean.getUsuario().trim(),sessionBean);
            	usuarioDTO.setListPerfiles(beanResUsuarioPerfilesDAO.getListPerfiles());
            	inicializaUsuario(usuarioDTO,hoy,sessionBean);
                perfilesEliminar = new ArrayList<BeanPerfil>();
                perfilesInsertar = new ArrayList<BeanPerfil>();
                if (hayPerfilesPorModificar(usuarioDTO.getListPerfiles(),
                        perfilesSam,perfilesInsertar, perfilesEliminar)) {
                	modificarPerfiles(usuarioDTO,perfilesEliminar, 
                    		perfilesInsertar,sessionBean);	
                }
                daoUsuario.actualizaUsuario(usuarioDTO, sessionBean);
            }else{
    	        usuarioDTO = new BeanUsuario();    	        
    	        inicializaUsuario(usuarioDTO,hoy,sessionBean);
    	        daoUsuario.insertaUsuario(usuarioDTO, sessionBean);
    	        daoUsuario.insertaUsuarioPerfiles(usuarioDTO,
    	                perfilesSam,sessionBean);
            }
        }
    }
    
    
    /**
     * Metodo privad que permite modificar los perfiles
     * @param usuarioDTO Objeto del tipo BeanUsuario
     * @param perfilesEliminar Objeto del tipo List<BeanPerfil>
     * @param perfilesInsertar Objeto del tipo List<BeanPerfil>
     * @param sessionBean Objeto de la arquitectura
     * @throws BusinessException Exception de negocio
     */
    private void modificarPerfiles(BeanUsuario usuarioDTO,List<BeanPerfil> perfilesEliminar, 
    		List<BeanPerfil> perfilesInsertar,ArchitechSessionBean sessionBean) throws BusinessException{
    	String servicioTmp = usuarioDTO.getServicio();
        usuarioDTO.setServicio(SERVICIO);
        if (!perfilesInsertar.isEmpty()) {
        	daoUsuario.insertaUsuarioPerfiles(usuarioDTO,
                    perfilesInsertar,sessionBean);
        }
        if (!perfilesEliminar.isEmpty()) {
        	daoUsuario.eliminarUsuarioPerfiles(usuarioDTO,
                    perfilesEliminar,sessionBean);
        }
        usuarioDTO.setServicio(servicioTmp);
    }
    
    /**
     * Metodo privado que permite inicializar un objeto del tipo BeanUsuario
     * @param usuarioDTO Objeto del tipo BeanUsuario
     * @param hoy Objeto del tipo String que almacena la fecha del dia
     * @param sessionBean Objeto de la arquitectura
     */
    private void inicializaUsuario(BeanUsuario usuarioDTO,String hoy,ArchitechSessionBean sessionBean){
    	usuarioDTO.setCveUsuario(sessionBean.getUsuario());
        usuarioDTO.setEstatusUsuario("1");
        usuarioDTO.setFechaActualizacion(hoy);
        usuarioDTO.setServicio(SERVICIO);
        usuarioDTO.setCveUsuarioUltimaOperacion(
                sessionBean.getUsuario());
        usuarioDTO.setFlagBloqueoSam("0");
        usuarioDTO.setFlagEliminaSam("0");
        usuarioDTO.setFechaUltimoAcceso(hoy);

    }

    /**
     * Indica si hay perfiles por modificar y regresa en los campos
     * <code>perfilesInsertar</code> y <code>perfilesEliminar</code>
     * aquellos perfiles que han de ser insertados o eliminados.
     * @param perfilesOriginales los perfiles originales del usuario.
     * @param perfilesNuevos los perfiles nuevos del usuario.
     * @param perfilesInsertar los perfiles que se habran de insertar
     *  al usuario.
     * @param perfilesEliminar los perfiles que se habran de eliminar
     *  del usuario.
     * @return <code>true</code> si hay perfiles por modificar,
     *  <code>false</code> si no existen perfiles por modificar.
     **/
    private boolean hayPerfilesPorModificar(
            List<BeanPerfil> perfilesOriginales,
            List<BeanPerfil> perfilesNuevos,
            List<BeanPerfil> perfilesInsertar,
            List<BeanPerfil> perfilesEliminar) {

        //Se verifican los perfiles vacios.
        if ((perfilesOriginales == null)
                || perfilesOriginales.isEmpty()) {
            if ((perfilesNuevos == null)
                    || perfilesNuevos.isEmpty()) {
                return false;
            }
            perfilesInsertar.addAll(perfilesNuevos);
            return true;
        }

        //Se verifican elementos totalmente distintos.
        if (Collections.disjoint(perfilesOriginales,
                perfilesNuevos)) {
            perfilesEliminar.addAll(perfilesOriginales);
            perfilesInsertar.addAll(perfilesNuevos);
            return true;
        }

        //Se obtienen los elementos a eliminar.
        for (BeanPerfil perfilOriginal : perfilesOriginales) {
            if (!perfilesNuevos.contains(perfilOriginal)) {
                perfilesEliminar.add(perfilOriginal);
            }
        }
        //Se obtienen los elementos a insertar.
        for (BeanPerfil perfilNuevo : perfilesNuevos) {
            if (!perfilesOriginales.contains(perfilNuevo)) {
                perfilesInsertar.add(perfilNuevo);
            }
        }

        //Se identifica si hay cambios.
        return !(perfilesInsertar.isEmpty()
                && perfilesEliminar.isEmpty());
    }

    /**
     * Obtiene el perfil de usuario valido para Transfer.
     * @param perfiles los perfiles del usuario obtenidos de la
     *  sesion.
     * @return el perfil de usuario valido para Transfer.
     * @throws BusinessException si ocurre algun error al obtener
     *  los perfiles de transfer.
     **/
    private List<BeanPerfil> getPerfilesUsuario(String perfiles)
            throws BusinessException {
    	BeanResConsCatPerfilDAO beanResConsCatPerfilDAO = null;
        List<String> perfilesEnSesion = null;
        List<BeanPerfil> perfilesTransfer = null;
        if ("".equals(perfiles)) {
            error("El perfil esta vacio.");
            return Collections.emptyList();
        }
        beanResConsCatPerfilDAO = perfilDao.consultaPerfiles(getArchitechBean());
        if(Errores.CODE_SUCCESFULLY.equals(beanResConsCatPerfilDAO
				.getCodError())){
        	perfilesTransfer = new ArrayList<BeanPerfil>();
            if (perfiles.contains(SEPARADOR_PERFILES)) {
                perfilesEnSesion = Arrays.asList(perfiles
                        .split(SEPARADOR_PERFILES));
            } else {
                perfilesEnSesion = Arrays.asList(perfiles.trim());
            }
            for (String perfilSesion : perfilesEnSesion) {
                if (!CADENA_VACIA.equals(perfilSesion)) {
		            for (BeanPerfil perfilTransfer: beanResConsCatPerfilDAO.getPerfilList()) {
		                if (perfilSesion!=null && perfilSesion.trim().equalsIgnoreCase(perfilTransfer.getTxtPerfilSam())) {
		                    perfilesTransfer.add(perfilTransfer);
		                }
		            }
            	}
            }
        }
        return perfilesTransfer;
    }

    /**
     * Obtiene el Objeto de Acceso a Datos para los perfiles de
     * Transfer.
     * @return el Objeto de Acceso a Datos para los perfiles de
     *  Transfer.
     */
    public DAOPerfilamiento getPerfilDao() {
        return this.perfilDao;
    }

    /**
     * Asigna el Objeto de Acceso a Datos para los perfiles de
     * Transfer.
     * @param perfilDao el Objeto de Acceso a Datos para los perfiles
     *  de Transfer.
     */
    public void setPerfilDao(DAOPerfilamiento perfilDao) {
        this.perfilDao = perfilDao;
    }

    /**
     * Metodo get que sirve para obtener el servicio DAOUsuario
     * @return daoUsuario Objeto del tipo DAOUsuario
     */
	public DAOUsuario getDaoUsuario() {
		return daoUsuario;
	}

    /**
     * Metodo set que sirve para modificar el servicio DAOUsuario
     * @param daoUsuario Objeto del tipo DAOUsuario
     */
	public void setDaoUsuario(DAOUsuario daoUsuario) {
		this.daoUsuario = daoUsuario;
	}

        
    

}