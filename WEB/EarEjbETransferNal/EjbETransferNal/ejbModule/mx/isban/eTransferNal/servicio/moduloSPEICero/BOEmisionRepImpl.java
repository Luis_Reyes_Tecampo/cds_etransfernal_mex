/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOEmisionRemImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	        Company 	 Description
 * ------- -----------   -----------   ---------- ----------------------
 * 1.0   18/04/2018 12:06:39 PM Moises Trejo Hernandez. Isban Creacion
 * 2.0   25/04/2018     Salvador Meza    VECTOR       Creacion
 */
package mx.isban.eTransferNal.servicio.moduloSPEICero;

/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanConsMiInstitucion;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.dao.moduloSPEICero.DAOEmisionReporte;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.servicio.SPEICero.BOEmisionRep;
import mx.isban.eTransferNal.utilerias.SPEICero.UtileriasModuloSPEICero;
import mx.isban.eTransferNal.utilerias.SPEICero.UtileriasSPEICero;

/**
 * Class BOEmisionRemImpl.
 *
 * @author mtrejo
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOEmisionRepImpl extends Architech implements BOEmisionRep {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -921581854412999600L;
	
	/**Referencia al dao de perfilamiento*/
	@EJB
	private DAOEmisionReporte daoEmisionReporte;
	/**Referencia al dao de perfilamiento*/
	@EJB
	private DAOExportar daoExportar;
	
	/**
	 * Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ
	 */
	private static UtileriasSPEICero util = new UtileriasSPEICero();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasSPEICero getInstance(){
		return util;
	}
	

	/**
	 * Procedimiento para obtener lo siguiente:
	 *  - Estatus de Operacion 
	 *  - Hora Inicio SPEI CERO
	 *  - Horario cambio fecha operacion .
	 *
	 * @param architechSessionBean the architech session bean
	 * @param indicador the indicador
	 * @param historico
	 * @return the res emision reporte
	 */
	@Override
	public BeanResSpeiCero getResEmisionReporte(ArchitechSessionBean architechSessionBean, int indicador,boolean historico) throws BusinessException {
		BeanResSpeiCero beanResSpeiCero = new BeanResSpeiCero();
		
		BeanConsMiInstitucion resMiInstitucion = new BeanConsMiInstitucion();
		/** Se busca la institucion**/	
		resMiInstitucion = daoEmisionReporte.consMiInstitucion("ENTIDAD_SPEI", architechSessionBean);
		/**Se traen los datos de inicio **/	
		beanResSpeiCero = daoEmisionReporte.incializa(architechSessionBean, resMiInstitucion,historico);
		beanResSpeiCero.setmInstitucion(resMiInstitucion.getMiInstitucion());
		
		if(beanResSpeiCero.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanResSpeiCero.getCodError(), beanResSpeiCero.getMsgError());
		}
		
		return beanResSpeiCero;
	}
	
	/**
	 * funcion que realiza la busqueda
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	@Override
	public BeanResSpeiCero buscarDatos(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) throws BusinessException {
		
		BeanResSpeiCero beanResSpeiCero = new BeanResSpeiCero();
		
		BeanPaginador paginador = null;
		/**Se inicializan el numero del paginador**/
		paginador = beanReqSpeiCero.getPaginador();
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
  	  	beanReqSpeiCero.setPaginador(paginador);
  	  
		/**se llama la funcion que busca los datos**/	
		beanResSpeiCero = daoEmisionReporte.buscarDatos(sessionBean, beanReqSpeiCero);
		
		if(beanResSpeiCero.getCodError().equals(Errores.OK00000V)) {
			/**Se calcula el regitro por pagina **/
			 paginador.calculaPaginas(beanResSpeiCero.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			 beanResSpeiCero.setBeanPaginador(paginador);			
		}else if(beanResSpeiCero.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanResSpeiCero.getCodError(), beanResSpeiCero.getMsgError());
		}
		
		
		/**regresa el objeto**/	
		return beanResSpeiCero;
	}
	
	/**
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	@Override
	public BeanResSpeiCero exportarTodo(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) throws BusinessException {
		BeanResSpeiCero bean = new BeanResSpeiCero();
		
		/** se asignan los valores de la busqueda**/
		bean = util.setearValroes(beanReqSpeiCero);
		/**llama la funcion para los estatus **/
		bean.setListaEstatusOpera(util.buscaEstatus(sessionBean));
		String query = daoEmisionReporte.consultaRptExportar(beanReqSpeiCero, sessionBean);
		
		BeanExportarTodo beanExp = new BeanExportarTodo();
		beanExp.setColumnasReporte(UtileriasModuloSPEICero.COLUMNAS_CONSULTA_CAPTURAS_MANUALES);
		beanExp.setConsultaExportar(query);
		beanExp.setModulo("SPEICero");
		if(("0").equals(beanReqSpeiCero.getHistorico())) {
			beanExp.setSubModulo("Emision Reporte Dia");		
			beanExp.setNombreRpt("SPEI_cero_dia");
		}else {
			beanExp.setSubModulo("Emision Reporte Historico");		
			String fecha= beanReqSpeiCero.getFchOperacion().substring(0,2)+
					beanReqSpeiCero.getFchOperacion().substring(3,5)+
					beanReqSpeiCero.getFchOperacion().substring(6,10);			
			beanExp.setNombreRpt("SPEI_cero_his_"+fecha+"_");
		}
		beanExp.setTotalReg(String.valueOf(beanReqSpeiCero.getTotalReg()));
		beanExp.setEstatus("PE");
	
		
		BeanResBase result = daoExportar.exportarTodo(beanExp, sessionBean);
		
		if (Errores.EC00011B.equals(bean.getCodError())) {
			bean.setCodError(result.getCodError());
			bean.setMsgError(result.getMsgError());
			bean.setTipoError(result.getTipoError());			
			throw new BusinessException(result.getCodError(), result.getMsgError());
			
		}else {
			/**Se inicializa la variable de esportar en 0 para que muestre los datos otra vez**/
			beanReqSpeiCero.setExportar("0");
			/**Se manda la consulta principal**/		
			bean = buscarDatos(sessionBean, beanReqSpeiCero);
			
			
			
			bean.setCodError(result.getCodError());
			bean.setMsgError(result.getMsgError());
			bean.setTipoError(result.getTipoError());
		
			
		}
		return bean;
	}
		
	/**
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	@Override
	public BeanResSpeiCero generarEdoCta(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero)throws BusinessException{
		BeanResSpeiCero beanResSpeiCero = new BeanResSpeiCero();
		
		/** se asignan los valores de la busqueda**/
		beanResSpeiCero = util.setearValroes(beanReqSpeiCero);
		/**llama la funcion para los estatus **/
		beanResSpeiCero.setListaEstatusOpera(util.buscaEstatus(sessionBean));
				
		BeanResBase result = daoEmisionReporte.generarEdoCta(beanReqSpeiCero,sessionBean);	
		/**en casod e que exita error manda el msj  **/		
		if(result.getCodError().equals(Errores.EC00011B)) {
			beanResSpeiCero.setCodError(result.getCodError());
			beanResSpeiCero.setMsgError(result.getMsgError());
			beanResSpeiCero.setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResSpeiCero;
		}
		
		/**Se inicializa la variable de esportar en 0 para que muestre los datos otra vez**/
		beanReqSpeiCero.setExportar("0");
		/**Se manda la consulta principal**/		
		beanResSpeiCero = buscarDatos(sessionBean, beanReqSpeiCero);
		
		if(beanResSpeiCero.getCodError().equals(Errores.EC00011B)) {
			throw new BusinessException(beanResSpeiCero.getCodError(), beanResSpeiCero.getMsgError());
		}else {
			beanResSpeiCero.setCodError(result.getCodError());
			beanResSpeiCero.setMsgError(result.getMsgError());
			beanResSpeiCero.setTipoError(Errores.TIPO_MSJ_ALERT);			
		}
		
		return beanResSpeiCero;
	}
	
	
	
	
	
}
