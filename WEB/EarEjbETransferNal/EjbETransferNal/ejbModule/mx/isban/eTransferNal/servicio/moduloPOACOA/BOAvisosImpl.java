/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOAvisosImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:18:17 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOAvisos;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasAvisos;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportar;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class BOAvisosImpl.
 *
 * Clase tipo BO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAvisosImpl extends Architech implements BOAvisos{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3163609810573829092L;

	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La variable que contiene informacion con respecto a: dao avisos. */
	@EJB
	private DAOAvisos daoAvisos;
	
	
	/** La constante utilsExportar. */
	private static final UtileriasExportar utilsExportar = UtileriasExportar.getUtils();
	
	/** La constante COLUMNAS_REPORTE. */
	private static final String COLUMNAS_REPORTE = "Hora, Folio Servidor, Tipo de Traspaso, Importe, Referencia SIAC";
	
	/** La constante QUERY_EXPORTAR_TODOS. */
	private static final StringBuilder QUERY_EXPORTAR_TODO = new StringBuilder()
			.append("SELECT T.HORA_RECEPCION || ',' || T.FOLIO_SERVIDOR || ',' || T.TIPO_TRASPASO")
			.append(" || ',' || DECODE(T.TIPO_TRASPASO,3,'SIAC-SPEI',4,'SIDV-SIAC-SPEI','') || ',' || ")
			.append("T.MONTO || ',' || T.REFERENCIA_SIAC ")
			.append("FROM TRAN_TRSIAC_SPID T");
	

	/** La constante utils. */
	private static final UtileriasAvisos utils = UtileriasAvisos.getUtils();
	
	/**
	 * Exportar todos.
	 *
	 * Exportar todos los registros para el proceso batch.
	 * 
	 * @param beanAvisos El objeto: bean avisos
	 * @param modulo     El objeto: modulo
	 * @param reqSession El objeto: req session
	 * @param session    El objeto: session
	 * @return Objeto bean res aviso traspasos
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResAvisoTraspasos exportarTodo(BeanResAvisoTraspasos beanAvisos, BeanModulo modulo,
			BeanSessionSPID reqSession,
			ArchitechSessionBean session) throws BusinessException {
		BeanResBase responseDAO = null;
		BeanResAvisoTraspasos response = new BeanResAvisoTraspasos();
			BeanExportarTodo exportarRequest = new BeanExportarTodo();
			exportarRequest.setColumnasReporte(COLUMNAS_REPORTE);
			exportarRequest.setConsultaExportar(ValidadorMonitor.cambiaQuery(modulo, QUERY_EXPORTAR_TODO.toString()));
			exportarRequest.setEstatus("PE");
			exportarRequest.setModulo(utilsExportar.getModulo(modulo));
			exportarRequest.setSubModulo(utilsExportar.getMonitor(modulo).concat("_AVISOS_TRASPASOS"));
			exportarRequest.setNombreRpt("AVISOS_TRASPASOS_".concat(modulo.getTipo().toUpperCase().concat("_".concat(modulo.getModulo().toUpperCase().concat("_")))));
			exportarRequest.setTotalReg(beanAvisos.getTotalReg() + StringUtils.EMPTY);
			responseDAO = daoExportar.exportarTodo(exportarRequest, session);
			if (!Errores.EC00011B.equals(responseDAO.getCodError())) {
				response.setNombreArchivo(responseDAO.getMsgError());
				response.setCodError(responseDAO.getCodError());
				response.setMsgError(responseDAO.getMsgError());
				response.setTipoError(responseDAO.getTipoError());
			}
		/** Retorno de la respuesta **/
		return response;
	}
	
	/**
	 * Obtener avisos.
	 *
	 * Consultar la lista de los avisos disponibles.
	 * 
	 * @param paginador            El objeto: paginador
	 * @param modulo               El objeto: modulo
	 * @param sortField            El objeto: sort field
	 * @param sortType             El objeto: sort type
	 * @param cveInst              El objeto: cve inst
	 * @param fch                  El objeto: fch
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res aviso traspasos
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResAvisoTraspasos obtenerAvisos(BeanPaginador paginador, BeanModulo modulo, String sortField,
			String sortType, String cveInst, String fch, ArchitechSessionBean session)
			throws BusinessException {
		BeanResAvisoTraspasosDAO beanResAvisoTras = null;
		final BeanResAvisoTraspasos beanResAvisoTraspasos = new BeanResAvisoTraspasos();

		BeanPaginador pag = paginador;
		/** Valida paginador **/
		if (pag == null) {
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());

		String sortF = utils.getSortField(sortField);
		/** Valida sort type **/
		if (sortType == null){
			sortType = "ASC";
		}
		beanResAvisoTras = daoAvisos.obtenerAviso(paginador, modulo, sortF, sortType,
				cveInst, fch, session);
		/** Valida creacion de la lista**/
		if (beanResAvisoTras.getListaConAviso() == null) {
			beanResAvisoTras.setListaConAviso(new ArrayList<BeanAvisoTraspasos>());
		}
		if (Errores.CODE_SUCCESFULLY.equals(beanResAvisoTras.getCodError())
				&& beanResAvisoTras.getListaConAviso().isEmpty()) {
			beanResAvisoTraspasos.setCodError(Errores.ED00011V);
			beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResAvisoTraspasos.setMsgError(Errores.DESC_ED00011V);
			beanResAvisoTraspasos.setListaConAviso(beanResAvisoTras.getListaConAviso());
		} else if (Errores.CODE_SUCCESFULLY.equals(beanResAvisoTras.getCodError())) {
			beanResAvisoTraspasos.setListaConAviso(beanResAvisoTras.getListaConAviso());
			beanResAvisoTraspasos.setTotalReg(beanResAvisoTras.getTotalReg());
			beanResAvisoTraspasos.setImporteTotal(daoAvisos.obtenerImporteTotal(session, modulo, sortF, sortType, cveInst, fch).toString());
			pag.calculaPaginas(beanResAvisoTras.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResAvisoTraspasos.setBeanPaginador(pag);
			beanResAvisoTraspasos.setCodError(Errores.OK00000V);
		} else {
			beanResAvisoTraspasos.setCodError(Errores.EC00011B);
			beanResAvisoTraspasos.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResAvisoTraspasos.setMsgError(Errores.DESC_EC00011B);
		}
		/** Retorno de la respuesta **/
		return beanResAvisoTraspasos;
	}

	
}
