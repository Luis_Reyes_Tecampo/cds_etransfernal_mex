/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BORecepcionImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAORecepcion;

/**
 *Clase del tipo BO que se encarga  del negocio del listado de pagos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORecepcionImpl implements BORecepcion {
	
	/**
	 * 
	 */
	@EJB
	private DAORecepcion daoRecepcion;

	/**
	 * @return the daoRecepcion
	 */
	public DAORecepcion getDaoRecepcion() {
		return daoRecepcion;
	}

	/**
	 * @param daoRecepcion the daoRecepcion to set
	 */
	public void setDaoRecepcion(DAORecepcion daoRecepcion) {
		this.daoRecepcion = daoRecepcion;
	}

	@Override
	public BeanResRecepcionSPID obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean) throws BusinessException{
		BeanResRecepcionDAO beanResRecepcionDAO = null;
		final BeanResRecepcionSPID beanResRecepcionSPID = new BeanResRecepcionSPID();
		
		beanResRecepcionDAO = daoRecepcion.obtenerDetalleRecepcion(beanLlave, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDAO.getCodError()) 
				&& beanResRecepcionDAO.getBeanReceptorSPID() == null) {
			beanResRecepcionSPID.setCodError(Errores.ED00011V);
			beanResRecepcionSPID.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResRecepcionDAO.getCodError())){
			beanResRecepcionSPID.setBeanReceptorSPID(beanResRecepcionDAO.getBeanReceptorSPID());   
			beanResRecepcionSPID.setCodError(Errores.OK00000V);
		}else{
			beanResRecepcionSPID.setCodError(Errores.EC00011B);
			beanResRecepcionSPID.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}				
		return beanResRecepcionSPID;
	}
}









