/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOProgTraspasos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanHorarioTransferencia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOProgTraspasos;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio del catalogo 
 *de horarios para envio de pagos por SPEI
**/
@Remote(BOProgTraspasos.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOProgTraspasosImpl extends Architech implements BOProgTraspasos{
	
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 5063727252178372320L;
	
	/**
	 * Referencia privada al dao  
	 */
	@EJB
	private DAOProgTraspasos dAOProgTraspasos;

	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios  de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	 */
	public BeanProgTraspasosBO consultaHorariosTransferencias(BeanProgTraspasosBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
		BeanProgTraspasosBO BeanProgTraspasosBO = new BeanProgTraspasosBO();
	    BeanPaginador paginador = null;
	    BeanProgTraspasosDAO beanProgTraspasosDAO = null;
		List<BeanHorarioTransferencia> listBeanHoraiosTR = null;
		
		paginador = bean.getPaginador();
		if(paginador == null){
	    	  paginador = new BeanPaginador();
	    	  bean.setPaginador(paginador);
	     }
	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());			    
	    beanProgTraspasosDAO =  dAOProgTraspasos.consultaHorariosTransferencias(bean,architechSessionBean);
	    listBeanHoraiosTR = beanProgTraspasosDAO.getListHorariosTr();
	    
	    if(Errores.CODE_SUCCESFULLY.equals(beanProgTraspasosDAO.getCodError()) && listBeanHoraiosTR.isEmpty()){
	    	BeanProgTraspasosBO.setCodError(Errores.ED00011V);
	    	BeanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_INFO);		      
	    }else if(Errores.CODE_SUCCESFULLY.equals(beanProgTraspasosDAO.getCodError())){
	    	BeanProgTraspasosBO.setTotalReg(beanProgTraspasosDAO.getTotalReg());
	    	BeanProgTraspasosBO.setListHorariosTr(beanProgTraspasosDAO.getListHorariosTr());
			    paginador.calculaPaginas(BeanProgTraspasosBO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			    BeanProgTraspasosBO.setPaginador(paginador);	
			    BeanProgTraspasosBO.setCodError(Errores.OK00000V);
	    }else{
	    	BeanProgTraspasosBO.setCodError(Errores.EC00011B);
	    	BeanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }		    		    
	   	    
	    return BeanProgTraspasosBO;
	}
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	 */
	public BeanProgTraspasosBO agregaHorarioTrasnferencias(BeanProgTraspasosBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
			
			BeanProgTraspasosDAO beanProgTraspasosDAO = null;
		    BeanProgTraspasosBO  beanProgTraspasosBO = new BeanProgTraspasosBO();
		    
		    beanProgTraspasosDAO = dAOProgTraspasos.agregaHorarioTrasnferencias(bean, architechSessionBean);
		    
		    if(Errores.CODE_SUCCESFULLY.equals(beanProgTraspasosDAO.getCodError())){
		    	beanProgTraspasosBO.setCodError(Errores.OK00003V);
		    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_INFO);
		    }else if(Errores.ED00061V.equals(beanProgTraspasosDAO.getCodError())){
		    	beanProgTraspasosBO.setCodError(Errores.ED00061V);
		    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_ALERT);
		    }else{
		    	beanProgTraspasosBO.setCodError(Errores.EC00011B);
		    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
		      }	
		return beanProgTraspasosBO;
	}

	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios de transferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	 */
	public BeanProgTraspasosBO eliminaHorarioTrasnferencias(BeanProgTraspasosBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
			BeanProgTraspasosDAO beanProgTraspasosDAO = null;
		    BeanProgTraspasosBO  beanProgTraspasosBO = new BeanProgTraspasosBO();
		    
		    List<BeanHorarioTransferencia> listBeanHorariosTR = new ArrayList<BeanHorarioTransferencia>();
		    for(BeanHorarioTransferencia beanHorarioTR:bean.getListHorariosTr()){
		    	  if(beanHorarioTR.isSeleccionado()){
		    		  listBeanHorariosTR.add(beanHorarioTR);
		    	  }
		      }
		    if(listBeanHorariosTR.isEmpty()){
		    	beanProgTraspasosBO.setCodError(Errores.ED00026V);
		    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_ERROR);
		    }else{
		    			beanProgTraspasosBO.setListHorariosTr(listBeanHorariosTR);
					    beanProgTraspasosDAO = dAOProgTraspasos.eliminaHorarioTrasnferencias(beanProgTraspasosBO, architechSessionBean);
					    if(Errores.CODE_SUCCESFULLY.equals(beanProgTraspasosDAO.getCodError())){
					    	beanProgTraspasosBO.setCodError(Errores.OK00004V);
					    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_INFO);
					    }else{
					    	beanProgTraspasosBO.setCodError(Errores.EC00011B);
					    	beanProgTraspasosBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
					      }	
		    }
		return beanProgTraspasosBO;
	}

	/**
	 * @return el dAOProgTraspasos
	 */
	public DAOProgTraspasos getDAOProgTraspasos() {
		return dAOProgTraspasos;
	}

	/**
	 * @param progTraspasos el dAOProgTraspasos a establecer
	 */
	public void setDAOProgTraspasos(DAOProgTraspasos progTraspasos) {
		dAOProgTraspasos = progTraspasos;
	}
	
	
	
}
