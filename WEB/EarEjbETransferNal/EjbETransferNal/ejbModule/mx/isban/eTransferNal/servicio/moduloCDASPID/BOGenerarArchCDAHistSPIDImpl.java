package mx.isban.eTransferNal.servicio.moduloCDASPID;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDASPID.DAOGenerarArchCDAHistSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class BOGenerarArchCDAHistSPIDImpl.
 */
@Remote(BOGenerarArchCDAHistSPID.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGenerarArchCDAHistSPIDImpl extends Architech implements BOGenerarArchCDAHistSPID {

	/**
	 * Variable de serializacion
	 */
	private static final long serialVersionUID = -6183239801590198458L;
	
	/**
	 * Referencia al dao de la funcionalidad GenerarArchCDAHistSPID
	 */
	@EJB
	private transient DAOGenerarArchCDAHistSPID daoGenerarArchCDAHistSPID;
	
	/**Metodo que sirve para consultar la fecha de operacion de SPID
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
	   */
	//Metodo que sirve para consultar la fecha de operacion de SPID
	public BeanResConsFechaOp consultaFechaOperacionSPID(
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Se crean objetos de respuesta
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
		final BeanResConsFechaOp beanResConsFechaOp = new BeanResConsFechaOp();
		
		//Se hace la consulta al dao
		beanResConsFechaOpDAO = daoGenerarArchCDAHistSPID.consultaFechaOperacionSPID(architechSessionBean);
		//Se valida codigo de respuesta
	    if(Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
	    	beanResConsFechaOp.setCodError(Errores.OK00000V);
	    	beanResConsFechaOp.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
	    }else{
	    	//Si hay un error se setea cod de error EC00011B
	    	throw new BusinessException(Errores.EC00011B,Errores.DESC_EC00011B);
	    }
	    return beanResConsFechaOp;
	}

	/**Metodo que sirve para indicar que se debe de procesar el archivo
	    * de CDA Historico de SPID
	    * @param beanReqGenArchCDAHistSPID Objeto del tipo @see BeanReqGenArchCDAHist
	    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	    * @return BeanResProcGenArchCDAHist Objeto del tipo BeanResProcGenArchCDAHist
	    */
	//Metodo que sirve para indicar que se debe de procesar el archivo
	public BeanResProcGenArchCDAHist procesarGenArchHisSPID(BeanReqGenArchCDAHist beanReqGenArchCDAHistSPID,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		//Se crean objetos de respuesta
        final BeanResProcGenArchCDAHist beanResProcGenArchCDAHist = new BeanResProcGenArchCDAHist();
        //Se crean objetos de soporte
 	   Date fechaOpBancMex = null;
 	   Date fechaOpBanc = null;
 	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
 	   
 	   //Se hce la consulta al dao
        beanResConsFechaOpDAO = daoGenerarArchCDAHistSPID.consultaFechaOperacionSPID(architechSessionBean);
        //Se valida codigo de respeusta
        if(!Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
        	//Si no es exitoso se setea codigo de error EC00011B
     	   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
 		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
 		   return beanResProcGenArchCDAHist;
        }
        Utilerias utilerias = Utilerias.getUtilerias();
        //Se obtiene fecha de operacion
        fechaOpBancMex = utilerias.cadenaToFecha(beanResConsFechaOpDAO.getFechaOperacion(),
     		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
        //Se valida fecha de operacion
        if(beanReqGenArchCDAHistSPID.getFechaOperacion() !=null && !"".equals(beanReqGenArchCDAHistSPID.getFechaOperacion())){
            fechaOpBanc = utilerias.cadenaToFecha(beanReqGenArchCDAHistSPID.getFechaOperacion(),
         		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
        }else{
   		  throw new BusinessException(Errores.ED00025V,Errores.TIPO_MSJ_ALERT);
        }

        //Se hace la validacion de las fechas
        validacionFechas(fechaOpBanc, fechaOpBancMex,
    			beanReqGenArchCDAHistSPID, beanResProcGenArchCDAHist,
    			beanResConsFechaOpDAO, architechSessionBean);
        
        
        return beanResProcGenArchCDAHist;
	}
	
	private void validacionFechas(Date fechaOpBanc, Date fechaOpBancMex,
			BeanReqGenArchCDAHist beanReqGenArchCDAHistSPID, BeanResProcGenArchCDAHist beanResProcGenArchCDAHist,
			BeanResConsFechaOpDAO beanResConsFechaOpDAO, ArchitechSessionBean architechSessionBean) throws BusinessException {
        //Se valida la diferencia entre fechas
        if(fechaOpBancMex.compareTo(fechaOpBanc)>0){
        	//Se valida nombre de archivo
     	   if(beanReqGenArchCDAHistSPID.getNombreArchivo()!=null && 
     			   beanReqGenArchCDAHistSPID.getNombreArchivo().matches("[a-z|A-Z|0-9]+(.)(t|T)(x|X)(t|T)")){
     		  BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = daoGenerarArchCDAHistSPID.generarArchHistCDASPID(beanReqGenArchCDAHistSPID, architechSessionBean);
     		   //Se valida codigos de error
     		   if(Errores.CODE_SUCCESFULLY.equals(beanResGenArchCDAHistDAO.getCodError())){
         		   beanResProcGenArchCDAHist.setCodError(Errores.OK00000V);
         		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_INFO);
         		   beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
     		   }else{
     	        	//Si no es exitoso se setea codigo de error EC00011B
         		   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
         		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
         		  beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
     		   }
     	   }else{
           	//Si no es exitoso se setea codigo de error ED00013V
     		  throw new BusinessException(Errores.ED00013V,Errores.TIPO_MSJ_ALERT);
     	   }
        }else{
        	//Si no es exitoso se setea codigo de error ED00014V
        	throw new BusinessException(Errores.ED00014V,Errores.TIPO_MSJ_ALERT);
        }
	}

}
