/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHistImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 16:08:14 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.dao.moduloCDA.DAOGenerarArchxFchCDAHist;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 * Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico.
 *
 * @author FSW-Vector
 * @since 14/01/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGenerarArchCDAxFchHistImpl extends Architech implements BOGenerarArchCDAxFchHist {

	/** Constante de la serial version. */
	private static final long serialVersionUID = -5321466890228515339L;

	/** Referencia al dao de la funcionalidad GenerarArchCDAHist. */
	@EJB 
	private DAOGenerarArchxFchCDAHist dAOGenerarArchCDAHist;
	
	/** Propiedad del tipo DAOBitacoraAdmon que almacena el valor de daoBitacora. */
	@EJB 
	private DAOBitacoraAdmon daoBitacora;

  /**
   * Metodo que sirve para consultar la fecha de operacion.
   *
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
   * @throws BusinessException La business exception
   */
   public BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean)
       throws BusinessException{
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
	   final BeanResConsFechaOp beanResConsFechaOp = new BeanResConsFechaOp();
	   /** Se realiza la peticion al DAO **/
       beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(null, architechSessionBean);
       /** Se valida la respuesta obtenida y se setean los codigos de la operacion **/
       if(Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   beanResConsFechaOp.setCodError(Errores.OK00000V);
    	   beanResConsFechaOp.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
       }else{
    	   beanResConsFechaOp.setCodError(Errores.EC00011B);
    	   beanResConsFechaOp.setTipoError(Errores.TIPO_MSJ_ERROR);
       }
      /** Retorno del metodo **/
      return beanResConsFechaOp;
   }
   
   /**
    * Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico.
    *
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAxFchHist Objeto del tipo BeanResProcGenArchCDAxFchHist
    * @throws BusinessException La business exception
    */
    public BeanResProcGenArchCDAxFchHist procesarGenArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, ArchitechSessionBean architechSessionBean)
        throws BusinessException{
		BeanReqInsertBitacora beanReqInsertBitacora;
    	BeanResGenArchCDAHistDAO beanResGenArchCDAHistDAO = null;
       final BeanResProcGenArchCDAxFchHist beanResProcGenArchCDAHist = new BeanResProcGenArchCDAxFchHist();
	   Date fechaOpBancMex = null;
	   Date fechaOpBanc = null;
	   BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
	   /** Se realiza la peticion al DAO **/
       beanResConsFechaOpDAO = dAOGenerarArchCDAHist.consultaFechaOperacion(null, architechSessionBean);
       /** Se valida la respuesta obtenida y se setean los codigos de la operacion **/
       if(!Errores.CODE_SUCCESFULLY.equals(beanResConsFechaOpDAO.getCodError())){
    	   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
		   return beanResProcGenArchCDAHist;
       }
       /** Instancia a clase de utilerias para formatear la fecha**/
       Utilerias utilerias = Utilerias.getUtilerias();
       fechaOpBancMex = utilerias.cadenaToFecha(beanResConsFechaOpDAO.getFechaOperacion(),
    		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       if(beanReqGenArchCDAHist.getFechaOp() !=null && !"".equals(beanReqGenArchCDAHist.getFechaOp())){
           fechaOpBanc = utilerias.cadenaToFecha(beanReqGenArchCDAHist.getFechaOp(),
        		   Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
       }else{
    	   beanResProcGenArchCDAHist.setCodError(Errores.ED00025V);
    	   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ALERT);
    	   /** Retorno del metodo en caso de no cumplir las valdiaciones **/
    	   return beanResProcGenArchCDAHist;
       }
       
       if(fechaOpBancMex.compareTo(fechaOpBanc)>0){
			info("ControllerGenerarArchxFchCDAHist.procesarGenerarArchCDAHist getFechaOp:"+beanReqGenArchCDAHist.getFechaOp());	   
    	   String nombre = "FECHAOPER";
    	   beanReqGenArchCDAHist.setNombreArchivo(nombre);
    	   /** Se realiza la peticion al DAO **/
		   beanResGenArchCDAHistDAO = dAOGenerarArchCDAHist.genArchCDAHist(beanReqGenArchCDAHist, architechSessionBean);
		   /** Se valida la respuesta obtenida y se setean los codigos de la operacion **/
		   if(Errores.CODE_SUCCESFULLY.equals(beanResGenArchCDAHistDAO.getCodError())){
			   
			   beanReqInsertBitacora = utilerias.creaBitacora("procesarGenerarArchCDAxFchHist.do", "1",
		        		"", new Date(), 
		        		"OK", "AARCHCDA", "TRAN_SPEI_CTG_CDA", "FCH_OPERACION,ID_PETICION,TIPO_PETICION", 
		        		Errores.OK00000V, "TRANSACCION EXITOSA", "INSERT",beanReqGenArchCDAHist.getNombreArchivo());
		        /** Se gairda el registro de la operacion en la bitacora **/
		        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
			   
    		   beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
    		   beanResProcGenArchCDAHist.setCodError(Errores.OK00000V);
    		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_INFO);
    		   beanResProcGenArchCDAHist.setNombreArchivo(nombre);
		   }else{
			   beanResProcGenArchCDAHist.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
    		   beanResProcGenArchCDAHist.setCodError(Errores.EC00011B);
    		   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ERROR);
		   }

       }else{
    	   beanResProcGenArchCDAHist.setCodError(Errores.ED00014V);
    	   beanResProcGenArchCDAHist.setTipoError(Errores.TIPO_MSJ_ALERT);
       }
       /** Retorno del metodo **/
       return beanResProcGenArchCDAHist;
   }

	/**
	 * Metodo get que obtiene el valor de la propiedad dAOGenerarArchCDAHist.
	 *
	 * @return DAOGenerarArchxFchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public DAOGenerarArchxFchCDAHist getDAOGenerarArchCDAHist() {
		return dAOGenerarArchCDAHist;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad dAOGenerarArchCDAHist.
	 *
	 * @param generarArchCDAHist Objeto de tipo @see DAOGenerarArchxFchCDAHist
	 */
	public void setDAOGenerarArchCDAHist(
			DAOGenerarArchxFchCDAHist generarArchCDAHist) {
		dAOGenerarArchCDAHist = generarArchCDAHist;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad daoBitacora.
	 *
	 * @return daoBitacora Objeto del tipo DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad daoBitacora.
	 *
	 * @param daoBitacora del tipo DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
}
