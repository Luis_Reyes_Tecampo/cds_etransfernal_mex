/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: 
 * BOConsultaNominaCUTDiaImpl.java
 * 
 * Clase de Negocio del Flujo de OCnuslta de NominaCUT
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   19/03/2020 11:56:08 AM JJBR 	Isban 		Modificacion 
 * 													1. Se Agrega la linea 117 y se comenta modificacion
 * 													2. Se cambia StringBuffer por StringBuilder Linea 169
 */
package mx.isban.eTransferNal.servicio.admonSaldos;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTDia;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResUsrPassDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.ws.BeanReqCutSpei;
import mx.isban.eTransferNal.beans.ws.BeanResCutSpeiDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.admonSaldos.DAOConsultaNominaCUTDia;
import mx.isban.eTransferNal.dao.comun.DAOComunesSPEI;
import mx.isban.eTransferNal.dao.moduloSPID.DAOEncripta;
import mx.isban.eTransferNal.dao.ws.DAOConsultaCutSpeiWSService;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class BOConsultaNominaCUTDiaImpl.
 * 
 * Metodo encargado de la logica de negocio. Tratado de los parametros de accesos al WS de Banxico.
 *
 * @author FSW-Vector
 * @since 19/03/2020
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaNominaCUTDiaImpl extends Architech implements BOConsultaNominaCUTDia{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 4261372405722659245L;
	
	/** Propiedad del tipo DAOComunesSPEIImpl que almacena el valor de dAOComunesSPEI. */
	@EJB
	private DAOComunesSPEI dAOComunesSPEI;
	
	/** Propiedad del tipo DAOConsultaCutSpeiWSService que almacena el valor de dAOConsultaCutSpeiWSService. */
	@EJB
	private DAOConsultaCutSpeiWSService dAOConsultaCutSpeiWSService;
	
	/** La variable que contiene informacion con respecto a: d AO consulta nomina CUT dia. */
	@EJB
	private DAOConsultaNominaCUTDia dAOConsultaNominaCUTDia;
	
	/** Referencia al DAO que realiza la encripcion. */
	@EJB
	private DAOEncripta daoEncripta;

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.admonSaldos.BOConsultaNominaCUTDia#consultar(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResConsNomCUTDia consultar(ArchitechSessionBean sessionBean) {
		BeanResConsNomCUTDia beanResConsNomCUTDia = new BeanResConsNomCUTDia(); 
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOComunesSPEI.consultaFechaOperacion(sessionBean);
		BeanResConsMiInstDAO beanResConsMiInstDAO = dAOComunesSPEI.consultaMiInstitucion(sessionBean);
		beanResConsNomCUTDia.setCveInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		beanResConsNomCUTDia.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
		return beanResConsNomCUTDia;
	}
	
	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.servicio.admonSaldos.BOConsultaNominaCUTDia#consultarNomina(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResConsNomCUTDia consultarNomina(ArchitechSessionBean sessionBean) {
		Utilerias util = Utilerias.getUtilerias();
		BeanResCutSpeiDAO beanResCutSpeiDAO = null;
		BeanResConsNomCUTDia beanResConsNomCUTDia = new BeanResConsNomCUTDia(); 
		BeanReqCutSpei beanReqCutSpei = new BeanReqCutSpei();
		BeanResConsFechaOpDAO beanResConsFechaOpDAO = dAOComunesSPEI.consultaFechaOperacion(sessionBean);
		BeanResConsMiInstDAO beanResConsMiInstDAO = dAOComunesSPEI.consultaMiInstitucion(sessionBean);
		try {
			beanResConsNomCUTDia.setCveInstitucion(beanResConsMiInstDAO.getMiInstitucion());
			beanResConsNomCUTDia.setFechaOperacion(beanResConsFechaOpDAO.getFechaOperacion());
			
			beanResCutSpeiDAO = dAOConsultaNominaCUTDia.consultaNominaCUT(sessionBean, beanResConsFechaOpDAO);
			if(! (Errores.OK00000V.equals(beanResCutSpeiDAO.getCodError())||Errores.ED00011V.equals(beanResCutSpeiDAO.getCodError()))){
				beanResConsNomCUTDia.setCodError(beanResCutSpeiDAO.getCodError());
				beanResConsNomCUTDia.setTipoError(beanResCutSpeiDAO.getTipoError());
				return beanResConsNomCUTDia;
			}
			beanResConsNomCUTDia.setCantidadPagos(util.formateaDecimales(beanResCutSpeiDAO.getCantidad(), Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
			beanResConsNomCUTDia.setMonto(util.formateaDecimales(beanResCutSpeiDAO.getMonto(), Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
			BeanResUsrPassDAO beanResUsrPassDAO = null;
			beanResUsrPassDAO = dAOConsultaNominaCUTDia.consultaUsrPass(sessionBean);
			if(! (Errores.OK00000V.equals(beanResCutSpeiDAO.getCodError())||Errores.ED00011V.equals(beanResCutSpeiDAO.getCodError()))){
				beanResConsNomCUTDia.setCodError(beanResUsrPassDAO.getCodError());
				beanResConsNomCUTDia.setTipoError(beanResUsrPassDAO.getTipoError());
				return beanResConsNomCUTDia;
			}
			//JJBR: Se aplica ajuste para desencriptar Password - INICIO
			beanResUsrPassDAO.setContrasena(daoEncripta.desEncripta(beanResUsrPassDAO.getContrasena(), sessionBean).getCadDesencriptada());
			//JJBR: Se aplica ajuste para desencriptar Password - FIN
			beanReqCutSpei.setUsuario(beanResUsrPassDAO.getUsuario());
			beanResCutSpeiDAO = dAOConsultaCutSpeiWSService.obtSalt(beanReqCutSpei, sessionBean);
			if(! (Errores.OK00000V.equals(beanResCutSpeiDAO.getCodError())||Errores.ED00011V.equals(beanResCutSpeiDAO.getCodError()))){
				beanResConsNomCUTDia.setCodError(beanResCutSpeiDAO.getCodError());
				beanResConsNomCUTDia.setTipoError(beanResCutSpeiDAO.getTipoError());
				return beanResConsNomCUTDia;
			}
			String str = null; 
			
			str = aplicaMD5(beanResCutSpeiDAO.getSalt(),beanResUsrPassDAO.getContrasena());
			beanReqCutSpei.setContrasena(beanResCutSpeiDAO.getSalt()+str);
	        String fch = util.formatoToformatoFecha(beanResConsFechaOpDAO.getFechaOperacion(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY, Utilerias.FORMATO_YYYYMMDD);
	        beanReqCutSpei.setFchConsultar(fch);
	        beanResCutSpeiDAO = dAOConsultaCutSpeiWSService.obtMontoPagosNominaTesofe(beanReqCutSpei, sessionBean);
			if(!Errores.OK00000V.equals(beanResCutSpeiDAO.getCodError())){
				beanResConsNomCUTDia.setCodError(beanResCutSpeiDAO.getCodError());
				beanResConsNomCUTDia.setTipoError(beanResCutSpeiDAO.getTipoError());
				return beanResConsNomCUTDia;
			}
			beanResConsNomCUTDia.setCantidadPagosPre(util.formateaDecimales(beanResCutSpeiDAO.getCantidad(), Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
			beanResConsNomCUTDia.setMontoPre(util.formateaDecimales(beanResCutSpeiDAO.getMonto(), Utilerias.FORMATO_16_ENT_2_DECIMAL_COMAS));
		} catch (NoSuchAlgorithmException e) {
			showException(e);
			beanResConsNomCUTDia.setCodError("ED00123V");
			beanResConsNomCUTDia.setTipoError(Errores.TIPO_MSJ_ERROR);
			return beanResConsNomCUTDia;
		}
		beanResConsNomCUTDia.setCodError(Errores.OK00000V);
		beanResConsNomCUTDia.setTipoError(Errores.TIPO_MSJ_INFO);
		return beanResConsNomCUTDia;
	}

	/**
	 * Aplica MD5.
	 * Aplica Algoritmo MD5 a salt para
	 * Banxico
	 *
	 * @param salt String con el valor de la salt
	 * @param pass String con el valor del password
	 * @return String con el MD5 de la concatenacion de los valores
	 * @throws NoSuchAlgorithmException La no such algorithm exception
	 */
	private String aplicaMD5(String salt,String pass) throws NoSuchAlgorithmException{
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-256");
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(salt).append(pass);
		md.update(strBuilder.toString().getBytes(StandardCharsets.UTF_8));
		byte[] byteData = md.digest();
		// Convierte los bytes a formato hexadecimal
		StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOComunesSPEI.
	 *
	 * @return dAOComunesSPEI Objeto del tipo DAOComunesSPEI
	 */
	public DAOComunesSPEI getDAOComunesSPEI() {
		return dAOComunesSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOComunesSPEI.
	 *
	 * @param comunesSPEI del tipo DAOComunesSPEI
	 */
	public void setDAOComunesSPEI(DAOComunesSPEI comunesSPEI) {
		dAOComunesSPEI = comunesSPEI;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOConsultaNominaCUTDia.
	 *
	 * @return dAOConsultaNominaCUTDia Objeto del tipo DAOConsultaNominaCUTDia
	 */
	public DAOConsultaNominaCUTDia getDAOConsultaNominaCUTDia() {
		return dAOConsultaNominaCUTDia;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOConsultaNominaCUTDia.
	 *
	 * @param consultaNominaCUTDia del tipo DAOConsultaNominaCUTDia
	 */
	public void setDAOConsultaNominaCUTDia(
			DAOConsultaNominaCUTDia consultaNominaCUTDia) {
		dAOConsultaNominaCUTDia = consultaNominaCUTDia;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad dAOConsultaCutSpeiWSService.
	 *
	 * @return dAOConsultaCutSpeiWSService Objeto del tipo DAOConsultaCutSpeiWSService
	 */
	public DAOConsultaCutSpeiWSService getDAOConsultaCutSpeiWSService() {
		return dAOConsultaCutSpeiWSService;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad dAOConsultaCutSpeiWSService.
	 *
	 * @param consultaCutSpeiWSService del tipo DAOConsultaCutSpeiWSService
	 */
	public void setDAOConsultaCutSpeiWSService(
			DAOConsultaCutSpeiWSService consultaCutSpeiWSService) {
		dAOConsultaCutSpeiWSService = consultaCutSpeiWSService;
	}
}
