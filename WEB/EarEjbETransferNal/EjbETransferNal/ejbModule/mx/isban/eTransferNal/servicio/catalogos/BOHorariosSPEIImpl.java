package mx.isban.eTransferNal.servicio.catalogos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanHorariosSPEI;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIBO;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.catalogos.DAOHorariosSPEI;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;


/**
 *Clase del tipo BO que se encarga  del negocio del catalogo 
 *de horarios para envio de pagos por SPEI
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOHorariosSPEIImpl extends Architech implements BOHorariosSPEI{
	
	  /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 5959243721938192864L;
	/**
	 * Referencia privada al dao  
	 */
	@EJB
	private DAOHorariosSPEI dAOHorariosSPEI;
	
	/**Referencia al servicio dao DAOBitacoraAdmon*/
	@EJB
	private DAOBitacoraAdmon daoBitacora;
	 
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO consultaHorariosSPEI(BeanResHorariosSPEIBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
		BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
	    BeanPaginador paginador = null;
	    BeanResHorariosSPEIDAO beanResHorariosSPEIDAO = null;
		List<BeanHorariosSPEI> listBeanHoraiosSPEI = null;
		
		paginador = bean.getPaginador();
		if(paginador == null){
	    	  paginador = new BeanPaginador();
	    	  bean.setPaginador(paginador);
	     }
	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());			    
	    beanResHorariosSPEIDAO =  dAOHorariosSPEI.consultaHorariosSPEI(bean,architechSessionBean);
	    listBeanHoraiosSPEI = beanResHorariosSPEIDAO.getListHorariosSPEI();
	    
	    if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError()) && listBeanHoraiosSPEI.isEmpty()){
	    	beanResHorariosSPEIBO.setCodError(Errores.ED00011V);
	    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_INFO);		      
	    }else if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError())){
	    	beanResHorariosSPEIBO.setTotalReg(beanResHorariosSPEIDAO.getTotalReg());
	    	beanResHorariosSPEIBO.setListHorariosSPEI(beanResHorariosSPEIDAO.getListHorariosSPEI());
			    paginador.calculaPaginas(beanResHorariosSPEIBO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			    beanResHorariosSPEIBO.setPaginador(paginador);	
			    beanResHorariosSPEIBO.setCodError(Errores.OK00000V);
	    }else{
	    	beanResHorariosSPEIBO.setCodError(Errores.EC00011B);
	    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
	      }		    		    
	   	    
	    return beanResHorariosSPEIBO;
	}
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO agregaHorarioSPEI(BeanResHorariosSPEIBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
			Utilerias utilerias = Utilerias.getUtilerias();
			BeanReqInsertBitacora beanReqInsertBitacora;
			BeanResHorariosSPEIDAO beanResHorariosSPEIDAO = null;
		    BeanResHorariosSPEIBO  beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		    
		    beanResHorariosSPEIDAO = dAOHorariosSPEI.consultaHorarioSPEI(bean.getHorarioSPEI(), architechSessionBean);
		    if(Errores.ED00011V.equals(beanResHorariosSPEIDAO.getCodError())){
		    	bean.setTotalReg(beanResHorariosSPEIBO.getTotalReg());
			    beanResHorariosSPEIDAO = dAOHorariosSPEI.agregaHorarioSPEI(bean, architechSessionBean);
			    
			    if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError())){
			    	beanResHorariosSPEIBO.setCodError(Errores.OK00003V);
			    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_INFO);
			    	 
			    	beanReqInsertBitacora = utilerias.creaBitacora("altaHorarioSPEI.do", "1",
				        		"", new Date(), 
				        		"OK", "AHSPEI", "TRAN_SPEI_CAT_HENV", "CVE_MEDIO_ENT,CVE_TRANSFE,CVE_OPERACION", 
				        		Errores.OK00000V, "TRANSACCION EXITOSA", "INSERT",bean.getHorarioSPEI().getHoraIncio());
				        
				        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
			    }else{
			    	beanResHorariosSPEIBO.setCodError(Errores.EC00011B);
			    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
			      }	
		    }else{
		    	beanResHorariosSPEIBO.setCodError(Errores.ED00061V);
		    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ALERT);
		    }
		    
		    
		    
		return beanResHorariosSPEIBO;
	}
	
	/**Metodo que sirve para modificar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO modificaHorarioSPEI(BeanResHorariosSPEIBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
		Utilerias utilerias = Utilerias.getUtilerias();
		BeanReqInsertBitacora beanReqInsertBitacora;
			BeanResHorariosSPEIDAO beanResHorariosSPEIDAO = null;
		    BeanResHorariosSPEIBO  beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		    
		    bean.setTotalReg(beanResHorariosSPEIBO.getTotalReg());
		    beanResHorariosSPEIDAO = dAOHorariosSPEI.modificaHorarioSPEI(bean, architechSessionBean);
		    
		    if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError())){
		    	beanResHorariosSPEIBO.setCodError(Errores.OK00003V);
		    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_INFO);
		    	 beanReqInsertBitacora = utilerias.creaBitacoraMod("modificaHorarioSPEI.do", "1",
			        		"", new Date(), 
			        		"OK", "MHSPEI", "TRAN_SPEI_CAT_HENV", "CVE_MEDIO_ENT,CVE_TRANSFE,CVE_OPERACION", 
			        		Errores.OK00000V, "TRANSACCION EXITOSA", "MODIFICA",bean.getHorarioSPEI().getHoraIncio(),
			        		""
				 );
			        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
		    }else{
		    	beanResHorariosSPEIBO.setCodError(Errores.EC00011B);
		    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
		      }	
		return beanResHorariosSPEIBO;
	}
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO eliminaHorarioSPEI(BeanResHorariosSPEIBO bean,
			ArchitechSessionBean architechSessionBean)throws BusinessException{
			Utilerias utilerias = Utilerias.getUtilerias();
			BeanReqInsertBitacora beanReqInsertBitacora;
			BeanResHorariosSPEIDAO beanResHorariosSPEIDAO = null;
		    BeanResHorariosSPEIBO  beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		    
		    List<BeanHorariosSPEI> listBeanHorariosSPEI = new ArrayList<BeanHorariosSPEI>();
		    for(BeanHorariosSPEI beanHorarioSPEI:bean.getListHorariosSPEI()){
		    	  if(beanHorarioSPEI.getSeleccionado()){
		    		  listBeanHorariosSPEI.add(beanHorarioSPEI);
		    	  }
		      }
		    if(listBeanHorariosSPEI.isEmpty()){
		    	beanResHorariosSPEIBO.setCodError(Errores.ED00026V);
		    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ALERT);
		    }else{
		    			beanResHorariosSPEIBO.setListHorariosSPEI(listBeanHorariosSPEI);
					    beanResHorariosSPEIDAO = dAOHorariosSPEI.eliminaHorarioSPEI(beanResHorariosSPEIBO, architechSessionBean);
					    if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError())){
					    	beanResHorariosSPEIBO.setCodError(Errores.OK00004V);
					    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_INFO);
					    	beanReqInsertBitacora = utilerias.creaBitacora("eliminaHorarioSPEI.do",String.valueOf(listBeanHorariosSPEI.size()) ,
					        		"", new Date(), 
					        		"OK", "DHSPEI", "TRAN_SPEI_CAT_HENV", "CVE_MEDIO_ENT,CVE_TRANSFE,CVE_OPERACION", 
					        		Errores.OK00000V, "TRANSACCION EXITOSA", "ELIMINAR","");
					        
					        daoBitacora.guardaBitacora(beanReqInsertBitacora, architechSessionBean);
					    	
					    }else{
					    	beanResHorariosSPEIBO.setCodError(Errores.EC00011B);
					    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
					      }	
		    }
		return beanResHorariosSPEIBO;
	}
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO consultaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException{
		BeanHorariosSPEI beanHorariosSPEI = null;
			BeanResHorariosSPEIDAO beanResHorariosSPEIDAO = null;
			BeanResHorariosSPEIBO  beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
			List<BeanHorariosSPEI> listBeanHorariosSPEI = new ArrayList<BeanHorariosSPEI>();
			BeanPaginador paginador = null;
			if(bean.getListHorariosSPEI()==null){
				beanResHorariosSPEIBO.setCodError(Errores.ED00029V);
		    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ALERT); 
			}else{
				    for(BeanHorariosSPEI beanHorarioSPEI:bean.getListHorariosSPEI()){
			    	  if(beanHorarioSPEI.getSeleccionado()){
			    		  beanResHorariosSPEIBO.setCveMedioEntrega(beanHorarioSPEI.getCveMedioEnt());
			    		  beanResHorariosSPEIBO.setCveTransferencia(beanHorarioSPEI.getCveTransfe());
			    		  beanResHorariosSPEIBO.setCveOperacion(beanHorarioSPEI.getCveOperacion());
			    		  listBeanHorariosSPEI.add(beanHorarioSPEI);
			    	  }
			      }
			      if(listBeanHorariosSPEI.isEmpty() || listBeanHorariosSPEI.size()>1){
			    	 beanResHorariosSPEIBO.setCodError(Errores.ED00029V);
			    	 beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ALERT);
			      }else{
					    	paginador = bean.getPaginador();
							if(paginador == null){
						    	  paginador = new BeanPaginador();
						    	  beanResHorariosSPEIBO.setPaginador(paginador);
						     }
						    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());		
			    			beanResHorariosSPEIBO.setListHorariosSPEI(listBeanHorariosSPEI);
			    			beanHorariosSPEI = new BeanHorariosSPEI();
			    			beanHorariosSPEI.setCveMedioEnt(beanResHorariosSPEIBO.getCveMedioEntrega());
			    			beanHorariosSPEI.setCveOperacion(beanResHorariosSPEIBO.getCveOperacion());
			    			beanHorariosSPEI.setCveTransfe(beanResHorariosSPEIBO.getCveTransferencia());
						    beanResHorariosSPEIDAO = dAOHorariosSPEI.consultaHorarioSPEI(beanHorariosSPEI, architechSessionBean);
						    if(Errores.CODE_SUCCESFULLY.equals(beanResHorariosSPEIDAO.getCodError())){
						    	beanResHorariosSPEIBO.setCodError(Errores.OK00000V);
						    	beanResHorariosSPEIBO.setHorarioSPEI(beanResHorariosSPEIDAO.getHorarioSPEI());
						    }else{
						    	beanResHorariosSPEIBO.setCodError(Errores.EC00011B);
						    	beanResHorariosSPEIBO.setTipoError(Errores.TIPO_MSJ_ERROR); 
						      }	
			           }
			     }
			return beanResHorariosSPEIBO;
			
	}
	
	/**Metodo que sirve para consultar la informacion los catalogos para la pantalla
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO llenaListas(ArchitechSessionBean architechSessionBean)
		throws BusinessException{
		BeanResHorariosSPEIBO beanResHorariosSPEIBO = null;
	 Date timeIni =null;
	  long ltimeIni =0;
	  long ltimeFin = 0;
  	  synchronized(Constantes.LOCK){
		  boolean flag = true;
		  if(Constantes.CACHE.containsKey("CONSULTA")){
    		  timeIni = (Date)Constantes.CACHE.get("TIME");
    		  ltimeIni =timeIni.getTime();
    		  ltimeFin = System.currentTimeMillis();
    		  if((ltimeFin-ltimeIni)<300000){
    			  flag = false;
    		  }
    	  }
		  if(flag){
			beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
			BeanResHorariosSPEIBO beanlistas = new BeanResHorariosSPEIBO();
			BeanResHorariosSPEIBO bean = new BeanResHorariosSPEIBO();
			bean.setConLista("LST_MED");
			beanlistas =  dAOHorariosSPEI.consultaListas(bean,architechSessionBean);
			beanResHorariosSPEIBO.setListMedioEntrega(beanlistas.getListMedioEntrega());
			bean.setConLista("LST_TRA");
			beanlistas =  dAOHorariosSPEI.consultaListas(bean,architechSessionBean);
			beanResHorariosSPEIBO.setListTrasnfer(beanlistas.getListTrasnfer());
			bean.setConLista("LST_OPER");
			beanlistas =  dAOHorariosSPEI.consultaListas(bean,architechSessionBean);
			beanResHorariosSPEIBO.setListOperacion(beanlistas.getListOperacion());
			Constantes.CACHE.put("CONSULTA", beanResHorariosSPEIBO);
			Constantes.CACHE.put("TIME", new Date());
		  }else{
			  beanResHorariosSPEIBO = (BeanResHorariosSPEIBO)Constantes.CACHE.get("CONSULTA");
			  Constantes.CACHE.put("TIME", new Date());
		  }
  	  }
		  return beanResHorariosSPEIBO;
	}

	/**
	 * @return el dAOHorariosSPEI
	 */
	public DAOHorariosSPEI getDAOHorariosSPEI() {
		return dAOHorariosSPEI;
	}

	/**
	 * @param horariosSPEI el dAOHorariosSPEI a establecer
	 */
	public void setDAOHorariosSPEI(DAOHorariosSPEI horariosSPEI) {
		dAOHorariosSPEI = horariosSPEI;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad daoBitacora
	 * @return DAOBitacoraAdmon Objeto de tipo @see DAOBitacoraAdmon
	 */
	public DAOBitacoraAdmon getDaoBitacora() {
		return daoBitacora;
	}

	/**
	 * Metodo que modifica el valor de la propiedad daoBitacora
	 * @param daoBitacora Objeto de tipo @see DAOBitacoraAdmon
	 */
	public void setDaoBitacora(DAOBitacoraAdmon daoBitacora) {
		this.daoBitacora = daoBitacora;
	}
	
	
}
