/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BOConsultaBancoSPIDImpl.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 11:49:45 AM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.servicio.ws;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaBancoSPIDDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.ws.DAOConsultaBancoSPID;
import mx.isban.eTransferNal.servicios.ws.BOConsultaBancoSPID;
import mx.isban.eTransferNal.ws.BeanListBancosSPID;
import mx.isban.eTransferNal.ws.BeanResponseConsultaBancoSPID;

/**
 * The Class BOConsultaBancoSPIDImpl.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Remote(BOConsultaBancoSPID.class)
public class BOConsultaBancoSPIDImpl extends Architech implements BOConsultaBancoSPID {

	/** Variable de Serielizacion. */
	private static final long serialVersionUID = 5342480990625340978L;

	/** Referencia del servicio daoConsultaBancoSPID. */
	@EJB
	private DAOConsultaBancoSPID daoConsultaBancoSPID;

	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicios.ws.BOConsultaBancoSPID#consultaBancosSPID(mx.isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public BeanResponseConsultaBancoSPID consultaBancosSPID() {
		info("Entrando a la funcionalidad de consultaBancoSPID");
		BeanResponseConsultaBancoSPID resBeanConsultaBanco = new BeanResponseConsultaBancoSPID();
		BeanListBancosSPID list=new BeanListBancosSPID();
		BeanResBase resultado=new BeanResBase();
		//se consulta los bancos SPID
		BeanResConsultaBancoSPIDDAO resDAOConsultaBancoSPID = daoConsultaBancoSPID.consultaBancoSPID();
		//si ejecucion DAO fue exitosa
		if(Errores.OK00000V.equals(resDAOConsultaBancoSPID.getCodError())){
			//se obtiene la lista de bancos SPID
			list.setBancoSPID(resDAOConsultaBancoSPID.getListaBancosSPID());
			//se envia bean de lista bancos
			resBeanConsultaBanco.setListBancosSPID(list);
		}else{
			//envio mesaje a log error en ejecuion
			info("Ha ocurrido un error en la consulta de consultaBancoSPID:error:"+resDAOConsultaBancoSPID.getCodError()+":mensaje:"+resDAOConsultaBancoSPID.getMsgError());
		}
		//se obtiene codigo y mensaje de error de ejecucion
		resultado.setCodError(resDAOConsultaBancoSPID.getCodError());
		resultado.setMsgError(resDAOConsultaBancoSPID.getMsgError());
		//se envia codigo de error de la ejecucuion por response
		resBeanConsultaBanco.setResultOPeracion(resultado);
		info("Saliendo de la funcionalidad de consultaBancoSPID ");
		return resBeanConsultaBanco;
	}

	/**
	 * Gets the dao consulta banco spid.
	 *
	 * @return the dao consulta banco spid
	 */
	public DAOConsultaBancoSPID getDaoConsultaBancoSPID() {
		return daoConsultaBancoSPID;
	}

	/**
	 * Sets the dao consulta banco spid.
	 *
	 * @param daoConsultaBancoSPID the new dao consulta banco spid
	 */
	public void setDaoConsultaBancoSPID(DAOConsultaBancoSPID daoConsultaBancoSPID) {
		this.daoConsultaBancoSPID = daoConsultaBancoSPID;
	}

}