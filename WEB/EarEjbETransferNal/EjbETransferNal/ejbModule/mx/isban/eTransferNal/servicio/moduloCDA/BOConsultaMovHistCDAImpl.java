/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovHistCDAImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovHistCdaDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOConsultaMovHistCDA;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * Session Bean implementation class BOConsultaMovHistCDAImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaMovHistCDAImpl extends Architech implements BOConsultaMovHistCDA {
       
    /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -6609397847914445121L;

	/**
	 * Referencia privada al dao de consulta movimiento Historico CDA
	 */
	@EJB
	private DAOConsultaMovHistCDA daoConsultaMovHistCDA;	
	
	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovHistCDA Objeto del tipo @see BeanResConsMovHistCDA
	 * @throws BusinessException Exception de negocio
	 */
    public BeanResConsMovHistCDA consultarMovHistCDA(BeanReqConsMovHistCDA beanReqConsMovHistCDA,ArchitechSessionBean architechSessionBean) throws BusinessException {
    	    
    	 	BeanPaginador paginador = null;
    	    BeanResConsMovHistCDA beanResConsMovHistCDA = new BeanResConsMovHistCDA();;
    	    BeanResConsMovHistCdaDAO beanResConsMovHistCdaDAO = null;
    	    List<BeanMovimientoCDA> listBeanMovimientoCDA = null;
    	    String accion = "ACT";
			String vacio = "";
    	    paginador = beanReqConsMovHistCDA.getPaginador();
    	 
    	    if(paginador == null){
    	    	  paginador = new BeanPaginador();
    	    	  beanReqConsMovHistCDA.setPaginador(paginador);
    	      } else if(accion.equals(paginador.getAccion()) && vacio.equals(paginador.getPagina())){
		    	  paginador.setPagina("1");
		      }		  	    
    	    paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		    beanResConsMovHistCdaDAO =  daoConsultaMovHistCDA.consultarMovHistCDA(beanReqConsMovHistCDA,architechSessionBean);
		    listBeanMovimientoCDA = beanResConsMovHistCdaDAO.getListBeanMovimientoCDA();
		    
		    if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovHistCdaDAO.getCodError()) && listBeanMovimientoCDA.isEmpty()){
		    	beanResConsMovHistCDA.setCodError(Errores.ED00011V);
		    	beanResConsMovHistCDA.setTipoError(Errores.TIPO_MSJ_INFO);
		    }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovHistCdaDAO.getCodError())){
			    beanResConsMovHistCDA.setTotalReg(beanResConsMovHistCdaDAO.getTotalReg());
			    beanResConsMovHistCDA.setListBeanMovimientoCDA(beanResConsMovHistCdaDAO.getListBeanMovimientoCDA());
			    paginador.calculaPaginas(beanResConsMovHistCDA.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			    beanResConsMovHistCDA.setBeanPaginador(paginador);		    
			    beanResConsMovHistCDA.setCodError(Errores.OK00000V);
		    }else {
		    	 beanResConsMovHistCDA.setCodError(Errores.EC00011B);
			     beanResConsMovHistCDA.setTipoError(Errores.TIPO_MSJ_ERROR);
		     }
		    
		    return beanResConsMovHistCDA;
    }
	
    /**
	 * Metodo que se encarga de solicitar la insercion de la peticion de
	 * Exportar Todos de los Movimientos Historicos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConMovHistCDA  Objeto del tipo @see BeanResConsMovHistCDA 
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovHistCDA consultaMovHistCDAExpTodos(BeanReqConsMovHistCDA beanReqConsMovHistCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	
    	BeanResConsMovHistCDA beanResConMovHistCDA = new BeanResConsMovHistCDA();
    	BeanResConsMovHistCdaDAO beanResConsMovHistCdaDAO = null;
    	beanResConMovHistCDA = consultarMovHistCDA(beanReqConsMovHistCDA,architechSessionBean);	
    	beanReqConsMovHistCDA.setTotalReg(beanResConMovHistCDA.getTotalReg());
    	beanResConsMovHistCdaDAO = daoConsultaMovHistCDA.guardarConsultaMovHistExpTodo(beanReqConsMovHistCDA, architechSessionBean);
    	
    	 if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovHistCdaDAO.getCodError())){
    		 beanResConMovHistCDA.setCodError(Errores.OK00001V);
    		 beanResConMovHistCDA.setTipoError(Errores.TIPO_MSJ_INFO);
    		 beanResConMovHistCDA.setNombreArchivo(beanResConsMovHistCdaDAO.getNombreArchivo());
		    }
		return beanResConMovHistCDA;
    }

    /**
	 * Metodo que sirve para solicitar el detalle de un movimiento Historico CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetHistCDA Objeto del tipo @see BeanResConsMovDetHistCDA
	 * @throws BusinessExceptionException de negocio
	 */
    public BeanResConsMovDetHistCDA consMovHistDetCDA(BeanReqConsMovCDADet beanReqConsMovCDA, ArchitechSessionBean architechSessionBean)throws BusinessException {
    	BeanResConsMovDetHistCDA beanResConsMovDetHistCDA = new BeanResConsMovDetHistCDA();
		BeanResConsMovDetHistCdaDAO beanResConsMovDetHistCdaDAO = null;
		beanResConsMovDetHistCdaDAO =  daoConsultaMovHistCDA.consultarMovDetHistCDA(beanReqConsMovCDA,architechSessionBean);
		if(Errores.CODE_SUCCESFULLY.equals(beanResConsMovDetHistCdaDAO.getCodError())){
			beanResConsMovDetHistCDA.setCodError(Errores.OK00000V);
			beanResConsMovDetHistCDA.setBeanMovCdaDatosGenerales(beanResConsMovDetHistCdaDAO.getBeanMovCdaDatosGenerales());
			beanResConsMovDetHistCDA.setBeanConsMovCdaOrdenante(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaOrdenante());
			beanResConsMovDetHistCDA.setBeanConsMovCdaBeneficiario(beanResConsMovDetHistCdaDAO.getBeanConsMovCdaBeneficiario());

		}else{
			beanResConsMovDetHistCDA.setCodError(Errores.EC00011B);
			beanResConsMovDetHistCDA.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return beanResConsMovDetHistCDA;
    }

	/**
	 * Metodo get para obtener el valor de daoConsultaMovHistCDA
	 * @return el daoConsultaMovHistCDA objeto de tipo @see DAOConsultaMovHistCDA
	 */
	public DAOConsultaMovHistCDA getDaoConsultaMovHistCDA() {
		return daoConsultaMovHistCDA;
	}

	/**
	 * Metodo para establecer el valor del daoConsultaMovHistCDA
	 * @param daoConsultaMovHistCDA objeto de tipo @see DAOConsultaMovHistCDA
	 */
	public void setDaoConsultaMovHistCDA(DAOConsultaMovHistCDA daoConsultaMovHistCDA) {
		this.daoConsultaMovHistCDA = daoConsultaMovHistCDA;
	}
}
