/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOBitacoraAdmonImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:55:49 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsReqBitacoraAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmonDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResExpBitAdmonDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloCDA.DAOBitacoraAdmon;
import mx.isban.eTransferNal.helper.HelperDAO;


/**
 *Clase del tipo BO que se encarga  del negocio de la bitacora
 * administrativa
**/
@Remote(BOBitacoraAdmon.class)
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOBitacoraAdmonImpl extends Architech implements BOBitacoraAdmon {

   /**
	 * Constante del Serial version 
	 */
	private static final long serialVersionUID = -4916246327079265066L;
	/**
	 * Referencia al servicio de Bitacora administrativa dao
	 */
	@EJB
	private DAOBitacoraAdmon dAOBitacoraAdmon;

  /**Metodo que sirve para consultar la informacion de la bitacora
   * administrativa
   * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsBitAdmon Objeto del tipo BeanResConsBitAdmon
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsBitAdmon consultaBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean)
       throws BusinessException{
	  BeanPaginador paginador = null;
	  List<BeanConsBitAdmon> listBeanConsBitAdmon = null;
	  final BeanResConsBitAdmon beanResConsBitAdmon = new BeanResConsBitAdmon();
      BeanResConsBitAdmonDAO beanResConsBitAdmonDAO = null;
      paginador = beanConsReqBitacoraAdmon.getPaginador();
      if(paginador == null){
    	  paginador = new BeanPaginador();
    	  beanConsReqBitacoraAdmon.setPaginador(paginador);
      }
      paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
      beanResConsBitAdmonDAO = dAOBitacoraAdmon.consultaBitacoraAdmon(beanConsReqBitacoraAdmon, architechSessionBean);
      listBeanConsBitAdmon = beanResConsBitAdmonDAO.getListBeanConsBitAdmon();
      if(Errores.CODE_SUCCESFULLY.equals(beanResConsBitAdmonDAO.getCodError()) && listBeanConsBitAdmon.isEmpty()){
    	  beanResConsBitAdmon.setCodError(Errores.ED00011V);
    	  beanResConsBitAdmon.setTipoError(Errores.TIPO_MSJ_INFO);
    	  beanResConsBitAdmon.setListBeanConsBitAdmon(beanResConsBitAdmonDAO.getListBeanConsBitAdmon());
      }else if(Errores.CODE_SUCCESFULLY.equals(beanResConsBitAdmonDAO.getCodError())){
    	  beanResConsBitAdmon.setListBeanConsBitAdmon(beanResConsBitAdmonDAO.getListBeanConsBitAdmon());          
          beanResConsBitAdmon.setTotalReg(beanResConsBitAdmonDAO.getTotalReg());
          paginador.calculaPaginas(beanResConsBitAdmonDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
          beanResConsBitAdmon.setPaginador(paginador);
          beanResConsBitAdmon.setCodError(Errores.OK00000V);
      }else{
    	  beanResConsBitAdmon.setCodError(Errores.EC00011B);
    	  beanResConsBitAdmon.setTipoError(Errores.TIPO_MSJ_ERROR); 
      }
      
           
      return beanResConsBitAdmon;
   }
   
   /**Metodo que sirve para realizar el exportar todo
    * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResConsBitAdmon Objeto del tipo BeanResConsBitAdmon
    * @exception BusinessExceptionException de negocio
    */
    public BeanResConsBitAdmon expTodosBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean)
        throws BusinessException{
    	BeanResExpBitAdmonDAO beanResExpBitAdmonDAO = null;
    	BeanResConsBitAdmon beanResConsBitAdmon = null;
    	beanResConsBitAdmon = consultaBitacoraAdmon(beanConsReqBitacoraAdmon,architechSessionBean);
    	beanConsReqBitacoraAdmon.setTotalReg(beanResConsBitAdmon.getTotalReg());
    	beanResExpBitAdmonDAO = dAOBitacoraAdmon.expTodosBitacoraAdmon(beanConsReqBitacoraAdmon, architechSessionBean);
    	beanResConsBitAdmon.setCodError(Errores.OK00002V);
    	beanResConsBitAdmon.setTipoError(Errores.TIPO_MSJ_INFO);
    	beanResConsBitAdmon.setNomArchivo(beanResExpBitAdmonDAO.getNomArchivo());
        return beanResConsBitAdmon;
    }

    /***
     * Metodo get del Servicio de bitacora administrativa
     * @return dAOBitacoraAdmon referencia del tipo DAOBitacoraAdmon
     */
	public DAOBitacoraAdmon getDAOBitacoraAdmon() {
		return dAOBitacoraAdmon;
	}
    /***
     * Metodo set del Servicio de bitacora administrativa
     * @param bitacoraAdmon referencia del tipo DAOBitacoraAdmon
     */	
	public void setDAOBitacoraAdmon(DAOBitacoraAdmon bitacoraAdmon) {
		dAOBitacoraAdmon = bitacoraAdmon;
	}

    


}
