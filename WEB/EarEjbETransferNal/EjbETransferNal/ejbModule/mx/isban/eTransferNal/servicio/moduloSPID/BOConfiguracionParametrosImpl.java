/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConfiguracionParametrosImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametrosDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResDesencripta;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEncripta;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.dao.moduloSPID.DAOConfiguracionParametros;
import mx.isban.eTransferNal.dao.moduloSPID.DAOEncripta;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitAdmin;

/**
 * Clase del tipo BO que se encarga  del negocio de la configuración de parámetros.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConfiguracionParametrosImpl implements BOConfiguracionParametros {
	
	/** Constante con los valores a guardar en la bitacora. */
	private static final String VALUES_BIT_FORMAT = "CVE_MI_INSTITUC=%s|CVE_MECA_SPID=%s|CVE_MECA_DEVOL=%s" +
		"|CVE_MECA_TRASP=%s|DESTINAT_SPID=%s|DESTINAT_ARA=%s|PUERTO_FEC_INI=%s|PUERTO_FEC_FIN=%s|PUERTO_ARA_INI=%s" +
		"|PUERTO_ARA_FIN=%s|DIREC_IP_FEC=%s|DIREC_IP_ARA=%s|LOGIN=%s|PASSWORD_=%s|NOMBRE_INSTITUC=%s|CERTIFICADO_ARA=%s";
	
	/** Constante con los valores a guardar en la bitacora. */
	private static final String DATO_FIJO_BIT = "CVE_MI_INSTITUC";
	
	/** Referencia al DAO que realiza la bitacora. */
	@EJB
	private DAOBitAdministrativa daoBitAdministrativa;
	
	/** Referencia al DAO que realiza la encripcion. */
	@EJB
	private DAOEncripta daoEncripta;
	
	/** La variable que contiene informacion con respecto a: dao configuracion parametros. */
	@EJB
	private DAOConfiguracionParametros daoConfiguracionParametros;

	/**
	 * Metodo para obtener configuracion de parametros.
	 *
	 * @param cveInstitucion Objeto del tipo Long
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	@Override
	public BeanResConfiguracionParametros obtenerConfiguracionParametros(
		Long cveInstitucion, ArchitechSessionBean architechSessionBean, String nomPantalla) throws BusinessException{
		BeanResConfiguracionParametrosDAO beanResConfiguracionParametrosDAO = null;
		BeanResConfiguracionParametros beanResConfiguracionParametros = new BeanResConfiguracionParametros();
		
		
		beanResConfiguracionParametrosDAO = daoConfiguracionParametros.obtenerConfiguracionParametros(cveInstitucion, architechSessionBean, nomPantalla);
		/**
		 * validacion de los campos
		 * */
		if(beanResConfiguracionParametrosDAO.getConfiguracionParametros().getFec()!=null && 
				!"".equals(beanResConfiguracionParametrosDAO.getConfiguracionParametros().getFec().getFecPassword())){
			BeanResDesencripta beanResDesencripta =daoEncripta.desEncripta(beanResConfiguracionParametrosDAO.getConfiguracionParametros().getFec().getFecPassword(), architechSessionBean);
			beanResConfiguracionParametrosDAO.getConfiguracionParametros().getFec().setFecPassword(beanResDesencripta.getCadDesencriptada());
		}
		/**
		 * manejo de errores
		 * */
		if (Errores.CODE_SUCCESFULLY.equals(beanResConfiguracionParametrosDAO.getCodError()) 
				&& beanResConfiguracionParametrosDAO.getConfiguracionParametros() == null) {
			beanResConfiguracionParametros.setCodError(Errores.ED00011V);
			beanResConfiguracionParametros.setTipoError(Errores.TIPO_MSJ_INFO);
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResConfiguracionParametrosDAO.getCodError())){
			beanResConfiguracionParametros.setConfiguracionParametros(beanResConfiguracionParametrosDAO.getConfiguracionParametros());          
			beanResConfiguracionParametros.setCodError(Errores.OK00003V);
		}else{
			beanResConfiguracionParametros.setCodError(Errores.EC00011B);
			beanResConfiguracionParametros.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
		/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */		
		return beanResConfiguracionParametros;
	}
	
	/**
	 * Metodo para guardar configuracion de parametros.
	 *
	 * @param configuracionParametros Objeto del tipo BeanConfiguracionParametros
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	@Override
	public BeanResConfiguracionParametros guardarConfiguracionParametros(
			BeanConfiguracionParametros configuracionParametros, ArchitechSessionBean architechSessionBean, String nomPantalla) throws BusinessException {
		BeanResConfiguracionParametros beanResConfiguracionParametros = new BeanResConfiguracionParametros();
		BeanResConfiguracionParametrosDAO beanResConfiguracionParametrosDAO = null;		
		/**
		 * validacion de la informacion de los campos
		 * */
		if (configuracionParametros != null) {
			String fecPassword = configuracionParametros.getFec().getFecPassword();
			
			beanResConfiguracionParametrosDAO = daoConfiguracionParametros.obtenerConfiguracionParametros(configuracionParametros.getCveCESIF(), architechSessionBean, nomPantalla);
			BeanConfiguracionParametros configuracionParametrosAnt = beanResConfiguracionParametrosDAO.getConfiguracionParametros();
			
			if(configuracionParametros.getFec()!=null && !"".equals(configuracionParametros.getFec().getFecPassword())){
				BeanResEncripta beanResEncripta = daoEncripta.encripta(configuracionParametros.getFec().getFecPassword(), architechSessionBean);
				configuracionParametros.getFec().setFecPassword(beanResEncripta.getCadEncriptada());
			}
			
			/**
			 * string que trae todos los campos
			 * */
			String valAnt = String.format(VALUES_BIT_FORMAT, 
					configuracionParametrosAnt.getCveCESIF(),
					configuracionParametrosAnt.getOrdPago(),
					configuracionParametrosAnt.getDevoluciones(),
					configuracionParametrosAnt.getTrasSPIDSIAC(),
					configuracionParametrosAnt.getFec().getFecDestinatario(),
					configuracionParametrosAnt.getAra().getAraDestinatario(),
					configuracionParametrosAnt.getFec().getFecInicial(),
					configuracionParametrosAnt.getFec().getFecFinal(),
					configuracionParametrosAnt.getAra().getAraInicial(),
					configuracionParametrosAnt.getAra().getAraFinal(),
					configuracionParametrosAnt.getFec().getFecDireccionIP(),
					configuracionParametrosAnt.getAra().getAraDireccionIP(),
					configuracionParametrosAnt.getFec().getFecLogin(),
					configuracionParametrosAnt.getFec().getFecPassword(),
					configuracionParametrosAnt.getNomInstit(),
					configuracionParametrosAnt.getAra().getAraCertificado());
			
			
			
			/**
			 * guarda los campos modificados
			 * */
			beanResConfiguracionParametrosDAO = daoConfiguracionParametros.guardarConfiguracionParametros(
					configuracionParametros, architechSessionBean, nomPantalla);
			/**
			 * manejo de errores
			 * **/
			if(Errores.CODE_SUCCESFULLY.equals(beanResConfiguracionParametrosDAO.getCodError())){
				String valNvo = String.format(VALUES_BIT_FORMAT, 
						configuracionParametros.getCveCESIF(),
						configuracionParametros.getOrdPago(),
						configuracionParametros.getDevoluciones(),
						configuracionParametros.getTrasSPIDSIAC(),
						configuracionParametros.getFec().getFecDestinatario(),
						configuracionParametros.getAra().getAraDestinatario(),
						configuracionParametros.getFec().getFecInicial(),
						configuracionParametros.getFec().getFecFinal(),
						configuracionParametros.getAra().getAraInicial(),
						configuracionParametros.getAra().getAraFinal(),
						configuracionParametros.getFec().getFecDireccionIP(),
						configuracionParametros.getAra().getAraDireccionIP(),
						configuracionParametros.getFec().getFecLogin(),
						configuracionParametros.getFec().getFecPassword(),
						configuracionParametros.getNomInstit(),
						configuracionParametros.getAra().getAraCertificado());
				setBitacoraTrans("OK", "TRAN_SPID_PARAM", "UPDATE", "IntGuardarConfiguracionParametros.do", valAnt, valNvo, "N/A", DATO_FIJO_BIT, "", architechSessionBean);
				
				/**
				 * validacion de campos
				 * **/
				if(configuracionParametros.getFec()!=null && !"".equals(configuracionParametros.getFec().getFecPassword())){
					beanResConfiguracionParametrosDAO.getConfiguracionParametros().getFec().setFecPassword(fecPassword);
				}
				
				beanResConfiguracionParametros.setConfiguracionParametros(beanResConfiguracionParametrosDAO.getConfiguracionParametros());
				beanResConfiguracionParametros.setCodError(Errores.OK00003V);
				beanResConfiguracionParametros.setTipoError(Errores.TIPO_MSJ_INFO);
			} else {
				setBitacoraTrans("NOK", "TRAN_SPID_PARAM", "UPDATE", "IntGuardarConfiguracionParametros.do", valAnt, "", "N/A", DATO_FIJO_BIT, "", architechSessionBean);
				beanResConfiguracionParametros.setCodError(Errores.EC00011B);
				beanResConfiguracionParametros.setTipoError(Errores.TIPO_MSJ_ALERT);
			}
		} else {
			beanResConfiguracionParametros.setCodError(Errores.ED00026V);
			beanResConfiguracionParametros.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		/**
    	 * Regresa los datos obtenidos de la ejecucion del query
    	 * */
		return beanResConfiguracionParametros;
	}
	
	/**
	 * Sets the bitacora trans.
	 *
	 * @param codOper Objeto del tipo String
	 * @param tablaAfect Objeto del tipo String
	 * @param tipoOper Objeto del tipo String
	 * @param servTran Objeto del tipo String
	 * @param valAnt Objeto del tipo String
	 * @param valNvo Objeto del tipo String
	 * @param datoModi Objeto del tipo String
	 * @param datoFijo Objeto del tipo String
	 * @param comentarios Objeto del tipo String
	 * @param sesion Objeto del tipo String
	 */
    private void setBitacoraTrans(String codOper, String tablaAfect, String tipoOper, String servTran, String valAnt,
		String valNvo, String datoModi, String datoFijo, String comentarios
		, ArchitechSessionBean sesion){
    	/**
    	 * instancia del objeto UtileriasBitAdmin
    	 * */
    	UtileriasBitAdmin utileriasBitAdmin = new UtileriasBitAdmin();
    	BeanReqInsertBitAdmin beanReqInsertBitAdmin = 
    		utileriasBitAdmin.createBitacoraBitAdmin(codOper, tablaAfect, tipoOper, servTran, valAnt, valNvo, 
				datoModi, datoFijo, comentarios, sesion);
    	daoBitAdministrativa.guardaBitacora(beanReqInsertBitAdmin, sesion);
    }

	/**
	 * Obtener el objeto: dao configuracion parametros.
	 *
	 * @return daoConfiguracionParametros objeto del tipo DAOConfiguracionParametros
	 */
	public DAOConfiguracionParametros getDaoConfiguracionParametros() {
		return daoConfiguracionParametros;
	}

	/**
	 * Definir el objeto: dao configuracion parametros.
	 *
	 * @param daoConfiguracionParametros objeto del tipo DAOConfiguracionParametros
	 */
	public void setDaoConfiguracionParametros(DAOConfiguracionParametros daoConfiguracionParametros) {
		this.daoConfiguracionParametros = daoConfiguracionParametros;
	}
	
	/**
	 * Obtener el objeto: dao bit administrativa.
	 *
	 * @return the daoBitAdministrativa
	 */
	public DAOBitAdministrativa getDaoBitAdministrativa() {
		return daoBitAdministrativa;
	}

	/**
	 * Definir el objeto: dao bit administrativa.
	 *
	 * @param daoBitAdministrativa the daoBitAdministrativa to set
	 */
	public void setDaoBitAdministrativa(DAOBitAdministrativa daoBitAdministrativa) {
		this.daoBitAdministrativa = daoBitAdministrativa;
	}


	/**
	 * Metodo get que sirve para obtener la propiedad daoEncripta.
	 *
	 * @return daoEncripta Objeto del tipo DAOEncripta
	 */
	public DAOEncripta getDaoEncripta() {
		return daoEncripta;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad daoEncripta.
	 *
	 * @param daoEncripta del tipo DAOEncripta
	 */
	public void setDaoEncripta(DAOEncripta daoEncripta) {
		this.daoEncripta = daoEncripta;
	}
	
	
}









