/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOConsultaMovimientosSeguroImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-06-2018 04:51:28 PM Juan Jesus Beltran R. VECTOR SWF Creacion
 */
package mx.isban.eTransferNal.servicio.ws;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicios.ws.BOTransferCifrado;
import mx.isban.eTransferNal.utilerias.UtileriasMQ;
import mx.isban.eTransferNal.utilerias.wscifrado.ConstantesWSCifrado;
import mx.isban.eTransferNal.utilerias.wscifrado.UtileriasWSTransferCifrado;
import net.sf.json.JSONObject;

/**
 * Class BOConsultaMovimientosSeguroImpl.
 *
 * @author FSW-Vector
 * @since 08-06-2018
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOTransferCifradoImpl extends Architech implements BOTransferCifrado {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5315273341281991236L;
	
	/** La constante utils. */
	private static final UtileriasWSTransferCifrado utils = UtileriasWSTransferCifrado.getInstance();

	/* (sin Javadoc)
	 * 
	 * @see mx.isban.eTransferNal.servicios.ws.BOConsultaMovimientosSeguro#getConsultaMovimientos(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ResEntradaStringJsonDTO getResponseService(EntradaStringJsonDTO entradaStringJsonDTO, String mqService) {
		String nomCampo = "";
		ResBeanEjecTranDAO resBeanEjecTranDAO = null;
		info(String.format("Entro a la llamada getResponseService[%s] Seguro: [%s]", mqService, entradaStringJsonDTO.getCadenaJson()));
		boolean flag = true;
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		Map<String, String> salida = new HashMap<String, String>();
		JSONObject jsonObject = JSONObject.fromString(entradaStringJsonDTO.getCadenaJson());
		Map<String, String> mapMovimientos = (Map<String, String>) JSONObject.toBean(jsonObject, Map.class);
		String canal = StringUtils.EMPTY;
		if (mapMovimientos.containsKey(ConstantesWSCifrado.CVE_MEDIO_ENT)) {
			canal = mapMovimientos.get(ConstantesWSCifrado.CVE_MEDIO_ENT).trim();
			mapMovimientos.put(ConstantesWSCifrado.CVE_MEDIO_ENT, canal.trim());
			if (canal.length() < 4) {
				flag = false;
				nomCampo = UtileriasWSTransferCifrado.CAMPOS.get(0);
			}
		}
		for (int index = 0; index < UtileriasWSTransferCifrado.CAMPOS.size() && flag; index++){
			flag = utils.validaCampos(mapMovimientos, index);
			nomCampo = UtileriasWSTransferCifrado.CAMPOS.get(index);
			if (flag && ConstantesWSCifrado.CVE_MEDIO_ENT.equals(nomCampo)) {
				canal = mapMovimientos.get(nomCampo);
			}
		}
		if (!flag) {
			return getResponseError(salida, nomCampo, resEntradaStringJsonDTO);
		}
		StringBuilder trama = utils.getTrama(mqService, jsonObject,
				mqService.equals(ConstantesWSCifrado.STRANSPID) ? ConstantesWSCifrado.PIPE : StringUtils.EMPTY);
		resBeanEjecTranDAO = UtileriasMQ.getInstance().ejecutaTransaccion(trama.toString(),
				mqService, ConstantesWSCifrado.CANAL);
		if (ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())) {
			String tramaRetorno = resBeanEjecTranDAO.getTramaRespuesta();
			if (tramaRetorno.contains(ConstantesWSCifrado.PIPE)) {
				final String[] valores = tramaRetorno
						.split(ConstantesWSCifrado.DIAGONALES_INVERSA.concat(ConstantesWSCifrado.PIPE), -1);
				if (utils.isValidRetorno(valores)) {
					returnSalida(valores, salida);
				}
			} else {
				returnCodigo(tramaRetorno, salida);
			}
		} else {
			returnTramaNula(salida);
		}
		
		jsonObject = JSONObject.fromMap(salida);
		resEntradaStringJsonDTO.setJson(jsonObject.toString());
		info("Salio de la llamada getResponseService Seguro: " + resEntradaStringJsonDTO.getJson());
		return resEntradaStringJsonDTO;
	}
	
	/**
	 * Return trama nula.
	 *  
	 * Arma el Mapa de respuesta 
	 * Este metodo se utiliza cuando la respuesta de MQ 
	 * es invalida 
	 *
	 * @param salida
	 *            El objeto: salida
	 */
	private void returnTramaNula(Map<String, String> salida) {
		salida.put(ConstantesWSCifrado.CAMPO_COD_ERROR, Errores.EC00011B);
		salida.put(ConstantesWSCifrado.CAMPO_REFERENCIA, ConstantesWSCifrado.CERO);
		salida.put(ConstantesWSCifrado.CVE_RASTREO, StringUtils.EMPTY);
		salida.put(ConstantesWSCifrado.CAMPO_ESTATUS, "ER");
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION, ConstantesWSCifrado.ERROR_IDA);
	}
	
	/**
	 * Return codigo.
	 * 
	 * Arma el Mapa de respuesta de acuerdo a la trama 
	 * devuelta por MQ
	 * 
	 * Este metodo se utuliza si la respuesta es
	 * devuelta sin PIPES
	 *
	 * @param tramaRetorno
	 *            El objeto: trama retorno
	 * @param salida
	 *            El objeto: salida
	 */
	private void returnCodigo(final String tramaRetorno, Map<String, String> salida) {
		salida.put(ConstantesWSCifrado.CAMPO_COD_ERROR, tramaRetorno.substring(0, 8));
		debug("COD_ERROR " + tramaRetorno.substring(0, 8));
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION, tramaRetorno.substring(8, 78));
		debug("DESCRIPCION " + tramaRetorno.substring(8, 78));
		salida.put(ConstantesWSCifrado.CAMPO_REFERENCIA, tramaRetorno.substring(78, 85));
		debug("REFERENCIA " + tramaRetorno.substring(78, 85));
		salida.put(ConstantesWSCifrado.CVE_RASTREO, tramaRetorno.substring(85, 115));
		debug("RASTREO " + tramaRetorno.substring(85, 115));
		salida.put(ConstantesWSCifrado.CAMPO_ESTATUS, tramaRetorno.substring(115, 117));
		debug("ESTATUS " + tramaRetorno.substring(115, 117));
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION_ESTATUS, tramaRetorno.substring(117, 137));
		debug("DESCRIPCION ESTATUS " + tramaRetorno.substring(117, 137));

	}
	
	/**
	 * Return salida.
	 * 
	 * Arma el Mapa de respuesta de acuerdo a la trama 
	 * devuelta por MQ 
	 * 
	 * Este metodo se utuliza si la respuesta es
	 * devuelta con PIPES
	 *
	 * @param valores
	 *            El objeto: valores
	 * @param salida
	 *            El objeto: salida
	 */
	private void returnSalida(final String[] valores, Map<String, String> salida) {
		salida.put(ConstantesWSCifrado.CAMPO_COD_ERROR, valores[0]);
		debug("COD_ERROR " + valores[0]);
		salida.put(ConstantesWSCifrado.CAMPO_REFERENCIA, valores[1]);
		debug("REFERENCIA " + valores[1]);
		salida.put(ConstantesWSCifrado.CAMPO_ESTATUS, valores[2]);
		debug("ESTATUS " + valores[2]);
		salida.put(ConstantesWSCifrado.CVE_RASTREO, valores[3]);
		debug("RASTREO " + valores[3]);
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION_ESTATUS, valores[4]);
		debug("DESCRIPCION ESTATUS " + valores[4]);
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION, valores[5]);
		debug("DESCRIPCION " + valores[5]);

	}
	
	/**
	 * Obtener el objeto: response error.
	 * 
	 * Arma el Json de respuesta Generico para los 
	 * casos de error 
	 *
	 * @param salida El objeto: salida
	 * @param nomCampo El objeto: nom campo
	 * @param jsonObject El objeto: json object
	 * @param resEntradaStringJsonDTO El objeto: res entrada string json DTO
	 * @return El objeto: response error
	 */
	private ResEntradaStringJsonDTO getResponseError(Map<String, String> salida, String nomCampo, ResEntradaStringJsonDTO resEntradaStringJsonDTO) {
		salida.put(ConstantesWSCifrado.CAMPO_COD_ERROR, "ED00092V");
		salida.put(ConstantesWSCifrado.CAMPO_REFERENCIA, "0");
		salida.put(ConstantesWSCifrado.CAMPO_ESTATUS, "ER");
		salida.put(ConstantesWSCifrado.CAMPO_DESCRIPCION,
				"Error en el formato o la longitud del campo:" + nomCampo);
		resEntradaStringJsonDTO.setJson(JSONObject.fromMap(salida).toString());
		info("Salio de la llamada getConsultaMovimientos Seguro con Error: " + resEntradaStringJsonDTO.getJson());
		return resEntradaStringJsonDTO;
		
	}
	
}
