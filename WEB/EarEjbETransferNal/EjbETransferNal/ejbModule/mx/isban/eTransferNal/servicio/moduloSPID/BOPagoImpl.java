/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOPagoImpl.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPID.DAOPago;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 *Clase del tipo BO que se encarga  del negocio del listado de pagos
**/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOPagoImpl implements BOPago {
	
	/**
	 * 
	 */
	@EJB
	private DAOPago daoPago;

	@Override
	public BeanResListadoPagos obtenerListadoPagos(
			String tipoOrden, BeanPaginador paginador, ArchitechSessionBean architechSessionBean) throws BusinessException{
		BeanResListadoPagosDAO beanResListadoPagosDAO = null;
		final BeanResListadoPagos beanResListadoPagos = new BeanResListadoPagos();
		
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		Object[] filtro = getTipoOrden(tipoOrden);
		
		beanResListadoPagosDAO = daoPago.obtenerListadoPagos(filtro[0].toString(), filtro[1].toString(),  paginador, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAO.getCodError()) 
				&& beanResListadoPagosDAO.getListBeanPago().isEmpty()) {
			beanResListadoPagos.setCodError(Errores.ED00011V);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAO.getCodError())){
			beanResListadoPagos.setListadoPagos(beanResListadoPagosDAO.getListBeanPago());   
			beanResListadoPagos.setTotalReg(beanResListadoPagosDAO.getTotalReg());
			paginador.calculaPaginas(beanResListadoPagosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResListadoPagos.setBeanPaginador(paginador);
			beanResListadoPagos.setCodError(Errores.OK00000V);
		}else{
			beanResListadoPagos.setCodError(Errores.EC00011B);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResListadoPagos;
	}
	
	@Override
	public BigDecimal obtenerListadoPagosMontos(
			String tipoOrden, ArchitechSessionBean architechSessionBean) throws BusinessException{
		BigDecimal totalImporte = BigDecimal.ZERO;
		Object[] filtro = getTipoOrden(tipoOrden);
		
		totalImporte = daoPago.obtenerListadoPagosMontos(filtro[0].toString(), filtro[1].toString(),  architechSessionBean);
		
	
				
		return totalImporte;
	}


	/**
	 * @param tipoOrden Objeto del tipo String
	 * @return Object[]: Object[0] = cveMecanismo, Object[1] = estatus
	 */
	private Object[] getTipoOrden(String tipoOrden) {
		String cveMecanismo = "";
		String estatus = "";
		String traspasoCode = "TRSPISIA";
		
		if ("OPD".equals(tipoOrden)){
				cveMecanismo = "DEVSPID";
				estatus = "('PV','LI','EN','PR')";
		} else if ("TSS".equals(tipoOrden)){
				cveMecanismo = traspasoCode;
				estatus = "('CO')";
		} else if ("OEC".equals(tipoOrden)){
				cveMecanismo = "SPID";
				estatus = "('CO')";
		} else if ("DEC".equals(tipoOrden)){
				cveMecanismo = "DEVSPID";
				estatus = "('CO')";
		} else if ("TES".equals(tipoOrden)){
				cveMecanismo = traspasoCode;
				estatus = "('PV','LI')";
		} else if ("TEN".equals(tipoOrden)){
				cveMecanismo = traspasoCode;
				estatus = "('EN')";
		} else if ("TPR".equals(tipoOrden)){
				cveMecanismo = traspasoCode;
				estatus = "('PR')";
		} else if ("OES".equals(tipoOrden)){
				cveMecanismo = "SPID";
				estatus = "('PV','LI')";
		} else if ("OEN".equals(tipoOrden)){
				cveMecanismo = "SPID";
				estatus = "('EN')";
		} 
		return new Object[] { cveMecanismo, estatus };
	}
	
	@Override
	public BeanResListadoPagos buscarPagos(
			String field, String valor, String tipoOrden, BeanPaginador paginador, ArchitechSessionBean architechSessionBean) {
		BeanResListadoPagosDAO beanResListadoPagosDAOBuscar = null;
		final BeanResListadoPagos beanResListadoPagos = new BeanResListadoPagos();
		
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		Object[] filtro = getTipoOrden(tipoOrden);
		
		beanResListadoPagosDAOBuscar = daoPago.buscarPagos(field, valor, filtro[0].toString(), filtro[1].toString(),  paginador, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAOBuscar.getCodError()) 
				&& beanResListadoPagosDAOBuscar.getListBeanPago().isEmpty()) {
			beanResListadoPagos.setCodError(Errores.ED00011V);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAOBuscar.getCodError())){
			beanResListadoPagos.setListadoPagos(beanResListadoPagosDAOBuscar.getListBeanPago());          
			beanResListadoPagos.setTotalReg(beanResListadoPagosDAOBuscar.getTotalReg());
			paginador.calculaPaginas(beanResListadoPagosDAOBuscar.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResListadoPagos.setBeanPaginador(paginador);
			beanResListadoPagos.setCodError(Errores.OK00000V);
		}else{
			beanResListadoPagos.setCodError(Errores.EC00011B);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResListadoPagos;
	}
	
	
	@Override
	public BigDecimal buscarPagosMonto(
			String field, String valor, String tipoOrden,  ArchitechSessionBean architechSessionBean) {
		BigDecimal totalImporte = BigDecimal.ZERO;
		Object[] filtro = getTipoOrden(tipoOrden);
		totalImporte = daoPago.buscarPagosMontos(field, valor, filtro[0].toString(), filtro[1].toString(),architechSessionBean);
		return totalImporte;
	}
	
	@Override
	public BeanResListadoPagos exportarTodoListadoPagos(
			String tipoOrden, BeanReqListadoPagos beanReqListadoPagos, ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResListadoPagosDAO beanResListadoPagosDAOExpAll = null;
		BeanResListadoPagos beanResListadoPagos = null;
		
		BeanPaginador paginador = beanReqListadoPagos.getPaginador();
		
		if (paginador == null){
			  paginador = new BeanPaginador();
		}
		paginador.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		Object[] filtro = getTipoOrden(tipoOrden);
		
		beanResListadoPagos = obtenerListadoPagos(tipoOrden, paginador, architechSessionBean);
		beanReqListadoPagos.setTotalReg(beanResListadoPagos.getTotalReg());
		beanResListadoPagosDAOExpAll = daoPago.exportarTodoListadoPagos(tipoOrden, filtro[0].toString(), filtro[1].toString(), beanReqListadoPagos, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAOExpAll.getCodError())) {
			beanResListadoPagos.setCodError(Errores.OK00002V);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_INFO);
			beanResListadoPagos.setNombreArchivo(beanResListadoPagosDAOExpAll.getNombreArchivo());
		} else {
			beanResListadoPagos.setCodError(Errores.EC00011B);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		return beanResListadoPagos;
	}
	
	@Override
	public BeanDetallePago consultaDetallePago(String referencia, ArchitechSessionBean architechSessionBean) 
			throws BusinessException {
		BeanDetallePago beanDetallePago = null;
		beanDetallePago = daoPago.consultarDetallePago(referencia);
		
		return beanDetallePago;
	}

	/**
	 * @return daoPago objeto del tipo DAOPago
	 */
	public DAOPago getDaoPago() {
		return daoPago;
	}

	/**
	 * @param daoPago objeto del tipo DAOPago
	 */
	public void setDaoPago(DAOPago daoPago) {
		this.daoPago = daoPago;
	}

}









