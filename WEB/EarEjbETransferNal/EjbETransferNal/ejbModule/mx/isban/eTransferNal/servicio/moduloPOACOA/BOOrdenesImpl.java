/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOOrdenesImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:22:13 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOOrdenes;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportar;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class BOOrdenesImpl.
 *
 * Clase tipo BO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOOrdenesImpl extends Architech implements BOOrdenes{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1377692986995595287L;
	
	/** La variable que contiene informacion con respecto a: dao ordenes. */
	@EJB
	private DAOOrdenes daoOrdenes;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La constante COLUMNAS_REPORTE. */
	private static final String COLUMNAS_REPORTE = "Referencia, Clave Cola, Clave Transferencia, Clave Mecanismo, Medio de Entrega,"
			.concat("Fecha de Captura, Ordenante, Receptor, Importe, Estatus, Folio paquete, Folio Pago, Mensaje Error");
	
	/** La constante CONSULTA_PAGOS. */
	private static final StringBuilder CONSULTA_ORDENES = 
			new StringBuilder().append("SELECT TM.REFERENCIA || ',' ||")
			   .append("TM.CVE_COLA || ',' || TM.CVE_TRANSFE || ',' || TM.CVE_MECANISMO || ',' || ") 
			   .append("TM.CVE_MEDIO_ENT || ',' || TM.FCH_CAPTURA || ',' || TM.CVE_INTERME_ORD || ',' || ")
		   	   .append("TM.CVE_INTERME_REC || ',' || TM.IMPORTE_ABONO || ',' || TM.ESTATUS || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END || ',' || TM.COMENTARIO3 ") 
	   	   	   .append("FROM TRAN_MENSAJE TM, [TB_EXTRA] TMS ") 
	   	   	   .append("WHERE TM.REFERENCIA = TMS.REFERENCIA AND TM.ESTATUS = 'PR' AND TM.CVE_MECANISMO='SPID' [PARAM_ORIGEN]");
	
	/** La constante utilsExportar. */
	private static final UtileriasExportar utilsExportar = UtileriasExportar.getUtils();
	
	/**
	 * Exportar todos ordenes reparacion.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 * 
	 * @param modulo                El objeto: modulo
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param architechBean         El objeto: architech bean
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResOrdenReparacion exportarTodoOrdenesReparacion(BeanModulo modulo,
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechBean) throws BusinessException {
		BeanResBase responseDAO = null;
		BeanResOrdenReparacion response = new BeanResOrdenReparacion();
			BeanExportarTodo exportarRequest = new BeanExportarTodo();
			exportarRequest.setColumnasReporte(COLUMNAS_REPORTE);
			String query = CONSULTA_ORDENES.toString();
			/** Validacion del modulo **/
			if (ValidadorMonitor.isSPEI(modulo)) {
    			query = query.replaceAll("\\[TB_EXTRA\\]", "TRAN_SPEI_ENV");
    		} else {
    			query = query.replaceAll("\\[TB_EXTRA\\]", "TRAN_MENSAJE_SPID");
    		}
			/** Invocacion del metodo para validar el query **/
    		query = ValidadorMonitor.cambiaQuery(modulo, query);
    		/** Validacion del origen **/
    		String origen = ValidadorMonitor.getOrigen(modulo);
    		query = query.replaceAll("\\[PARAM_ORIGEN\\]", origen);
			exportarRequest.setConsultaExportar(query);
			exportarRequest.setEstatus("PE");
			exportarRequest.setModulo(utilsExportar.getModulo(modulo));
			exportarRequest.setSubModulo(utilsExportar.getMonitor(modulo).concat("_".concat("ORDENES_REPARAR")));
			exportarRequest.setNombreRpt("ORDENES_REPARAR_".concat(modulo.getTipo().toUpperCase().concat("_".concat(modulo.getModulo().toUpperCase().concat("_")))));
			exportarRequest.setTotalReg(beanReqOrdeReparacion.getTotalReg() + "");
			responseDAO = daoExportar.exportarTodo(exportarRequest, architechBean);
			/** Validacion de la respuesta y seteo de datos **/
			if (!Errores.EC00011B.equals(responseDAO.getCodError())) {
				response.setNombreArchivo(responseDAO.getMsgError());
				response.setCodError(responseDAO.getCodError());
				response.setMsgError(responseDAO.getMsgError());
				response.setTipoError(responseDAO.getTipoError());
			}
		/** Retorno de la respuesta **/
		return response;
	}

}
