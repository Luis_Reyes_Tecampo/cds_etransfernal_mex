/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorBancosSPEIImpl.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 11:19:46	    1396920: Moises Navarro                 TCS         Creacion de BOMonitorBancosSPEIImpl.java
 */
package mx.isban.eTransferNal.servicio.moduloSPEI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.global.BeanNuevoPaginador;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanResponseMonBancSPEIBO;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanResponseMonBancSPEIDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.moduloSPEI.DAOMonitorBancosSPEI;

/**
 * Descripcion:Clase que define el comportamiento de los metodos de BO para el monitor de bancos SPEI
 * BOMonitorBancosSPEIImpl.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BOMonitorBancosSPEIImpl.java
*/
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMonitorBancosSPEIImpl extends Architech implements BOMonitorBancosSPEI{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = 6589234979030700934L;

	/** Atributo que representa la variable daoMonitorBancosSpei del tipo DAOMonitorBancosSPEI */
	@EJB
	private DAOMonitorBancosSPEI daoMonitorBancosSpei;
	
	
	/* (non-Javadoc)
	 * @see mx.isban.eTransferNal.servicio.moduloSPEI.BOMonitorBancosSPEI#consultaMonitorBancos(mx.isban.agave.commons.beans.ArchitechSessionBean, mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador)
	 */
	@Override
	public BeanResponseMonBancSPEIBO consultaMonitorBancos(ArchitechSessionBean session, BeanNuevoPaginador paginador) throws BusinessException {
		/**Se generan objetos necesarios*/
		BeanResponseMonBancSPEIBO respuestaBO= new BeanResponseMonBancSPEIBO();
		BeanResponseMonBancSPEIDAO respuestaDAO=null;
		/**Se evalua nulidad del paginador*/
		if(paginador==null){
			paginador = new BeanNuevoPaginador();
		}
		/**Se calcula paginacion de enttrada*/
		paginador.paginar();
		/**Se ejecuta el DAO*/
		respuestaDAO= daoMonitorBancosSpei.consultaMonitorBancosSPEI(session, paginador);
		/**Se evalua codigo de error*/
		if( Errores.CODE_SUCCESFULLY.equals(respuestaDAO.getCodError()) ){
			respuestaBO.setListadoBancos(respuestaDAO.getListaBancos());
			/**Se ejecuta el DAO*/
			respuestaDAO= daoMonitorBancosSpei.resumenMonitorBancosSPEI(session);
			/**Se evalua codigo de error*/
			if( Errores.CODE_SUCCESFULLY.equals(respuestaDAO.getCodError()) ){
				respuestaBO.setConsolidado(respuestaDAO.getConsolidadoEstatus());
				paginador.calculaPaginas(respuestaDAO.getConsolidadoEstatus().getTotal());
				respuestaBO.setBeanPaginador(paginador);
			} else {
				/**se arroja nueva BusinessException*/
				throw new BusinessException(respuestaDAO.getCodError(), respuestaDAO.getMsgError());
			}
		} else {
			/**se arroja nueva BusinessException*/
			throw new BusinessException(respuestaDAO.getCodError(), respuestaDAO.getMsgError());
		}
		/**Se retorna la respuesta*/
		return respuestaBO;
	}

}
