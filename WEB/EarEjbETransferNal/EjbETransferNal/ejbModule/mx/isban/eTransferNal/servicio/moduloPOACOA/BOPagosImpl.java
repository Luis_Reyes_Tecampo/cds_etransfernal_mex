/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOPagosImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     18/09/2019 04:01:16 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import org.apache.commons.lang.StringUtils;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.exportar.DAOExportar;
import mx.isban.eTransferNal.dao.moduloPOACOA.DAOPagos;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportar;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ValidadorMonitor;

/**
 * Class BOPagosImpl.
 *
 * Clase tipo BO que implementa su interfaz
 * para llevar a cabo la logica de los flujos
 * del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOPagosImpl extends Architech implements BOPagos{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8628047620318664542L;

	/** La variable que contiene informacion con respecto a: dao pagos. */
	@EJB
	private DAOPagos daoPagos;
	
	/** La variable que contiene informacion con respecto a: dao exportar. */
	@EJB
	private DAOExportar daoExportar;
	
	/** La constante utilsExportar. */
	private static final UtileriasExportar utilsExportar = UtileriasExportar.getUtils();
	
	/** La constante CONS_CO. */
	private static final String CONS_CO = "('CO')";
	
	/** La constante DEVSPID. */
	private static final String DEVSPID = "DEVSPID";

	/** La constante COLUMNAS_REPORTE. */
	private static final String COLUMNAS_REPORTE = "Referencia, Clave Cola, Clave Transferencia, Clave Mecanismo, Medio de Entrega,"
			.concat("Fecha de Captura, Ordenante, Receptor, Importe, Estatus");
	
	/** La constante CONSULTA_PAGOS. */
	private static final StringBuilder CONSULTA_PAGOS = new StringBuilder().append("SELECT TM.REFERENCIA,")
			   .append("TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO,") 
			   .append("TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, TM.CVE_INTERME_ORD,")
		   	   .append("TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, TM.ESTATUS ");
	

	/**
	 * Obtener listado pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param tipoOrden            El objeto: tipo orden
	 * @param beanPaginador        El objeto: bean paginador
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResListadoPagos obtenerListadoPagos(BeanModulo modulo, String tipoOrden, BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResListadoPagosDAO beanResListadoPagosDAO = null;
		final BeanResListadoPagos beanResListadoPagos = new BeanResListadoPagos();
		
		BeanPaginador pag = beanPaginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		Object[] filtro = getTipoOrden(tipoOrden,modulo);
		
		beanResListadoPagosDAO = daoPagos.obtenerListadoPagos(modulo, filtro[0].toString(), filtro[1].toString(),  beanPaginador, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAO.getCodError()) 
				&& beanResListadoPagosDAO.getListBeanPago().isEmpty()) {
			beanResListadoPagos.setCodError(Errores.ED00011V);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAO.getCodError())){
			beanResListadoPagos.setListadoPagos(beanResListadoPagosDAO.getListBeanPago());   
			beanResListadoPagos.setTotalReg(beanResListadoPagosDAO.getTotalReg());
			pag.calculaPaginas(beanResListadoPagosDAO.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResListadoPagos.setBeanPaginador(pag);
			beanResListadoPagos.setCodError(Errores.OK00000V);
		}else{
			beanResListadoPagos.setCodError(Errores.EC00011B);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResListadoPagos;
	}


	/**
	 * Buscar pagos.
	 *
	 * Buscar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param field                El objeto: field
	 * @param valor                El objeto: valor
	 * @param tipoOrden            El objeto: tipo orden
	 * @param paginador            El objeto: paginador
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResListadoPagos buscarPagos(BeanModulo modulo, String field, String valor, String tipoOrden,
			BeanPaginador paginador, ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanResListadoPagosDAO beanResListadoPagosDAOBuscar = null;
		final BeanResListadoPagos beanResListadoPagos = new BeanResListadoPagos();
		
		BeanPaginador pag = paginador;
		if(pag==null){
			pag = new BeanPaginador();
		}
		pag.paginar(HelperDAO.REGISTROS_X_PAGINA.toString());
		
		Object[] filtro = getTipoOrden(tipoOrden,modulo);
		
		beanResListadoPagosDAOBuscar = daoPagos.buscarPagos(modulo, field, valor, filtro[0].toString(), filtro[1].toString(),  paginador, architechSessionBean);
		
		if (Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAOBuscar.getCodError()) 
				&& beanResListadoPagosDAOBuscar.getListBeanPago().isEmpty()) {
			beanResListadoPagos.setCodError(Errores.ED00011V);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_INFO);

		}else if(Errores.CODE_SUCCESFULLY.equals(beanResListadoPagosDAOBuscar.getCodError())){
			beanResListadoPagos.setListadoPagos(beanResListadoPagosDAOBuscar.getListBeanPago());          
			beanResListadoPagos.setTotalReg(beanResListadoPagosDAOBuscar.getTotalReg());
			pag.calculaPaginas(beanResListadoPagosDAOBuscar.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResListadoPagos.setBeanPaginador(pag);
			beanResListadoPagos.setCodError(Errores.OK00000V);
		}else{
			beanResListadoPagos.setCodError(Errores.EC00011B);
			beanResListadoPagos.setTipoError(Errores.TIPO_MSJ_ERROR); 
		}
				
		return beanResListadoPagos;
	}

	/**
	 * Obtener el objeto: tipo orden.
	 *
	 * @param tipoOrden El objeto: tipo orden
	 * @param modulo El objeto: modulo
	 * @return El objeto: tipo orden
	 */
	private Object[] getTipoOrden(String tipoOrdPag, BeanModulo modulo) {
		String cveMecanismo = "";
		String estatus = "";
		String traspasoCode = "TRSPISIA";
		
		if ("OPD".equals(tipoOrdPag)){
				cveMecanismo = DEVSPID;
				estatus = "('PV','LI','EN','PR')";
		} else if ("TSS".equals(tipoOrdPag)){
				cveMecanismo = traspasoCode;
				estatus = CONS_CO;
		} else if ("OEC".equals(tipoOrdPag)){
				cveMecanismo = "SPID";
				estatus = CONS_CO;
		} else if ("DEC".equals(tipoOrdPag)){
				cveMecanismo = DEVSPID;
				estatus = CONS_CO;
		} else if ("TES".equals(tipoOrdPag)){
				cveMecanismo = traspasoCode;
				estatus = "('PV','LI')";
		} else if ("TEN".equals(tipoOrdPag)){
				cveMecanismo = traspasoCode;
				estatus = "('EN')";
		} else if ("TPR".equals(tipoOrdPag)){
				cveMecanismo = traspasoCode;
				estatus = "('PR')";
		} else if ("OES".equals(tipoOrdPag)){
				cveMecanismo = "SPID";
				estatus = "('PV','LI')";
		} else if ("OEN".equals(tipoOrdPag)){
				cveMecanismo = "SPID";
				estatus = "('EN')";
		} 
		cveMecanismo = validadorMecanismoSPEI(cveMecanismo,modulo);
		return new Object[] { cveMecanismo, estatus };
	}


	/**
	 * Validador mecanismo SPEI.
	 * Valida los mecanismos y los cambia a spei
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	private String validadorMecanismoSPEI(String cveMecanismo, BeanModulo modulo) {
		String mecanismoSPEI = cveMecanismo;
		if (ValidadorMonitor.isSPEI(modulo)) {
			if ("TRSPISIA".equalsIgnoreCase(mecanismoSPEI)) {
				mecanismoSPEI = "TRSPESIA";
			}
			if (ConstantesPOACOA.PAN_SPID.equalsIgnoreCase(mecanismoSPEI)) {
				mecanismoSPEI = ConstantesPOACOA.PAN_SPEI;
			}
			if (DEVSPID.equalsIgnoreCase(mecanismoSPEI)) {
				mecanismoSPEI = "DEVSPEI";
			}
		}
		return mecanismoSPEI;
	}


	/**
	 * Obtener listado pagos montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param tipoOrden            El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	@Override
	public BigDecimal obtenerListadoPagosMontos(BeanModulo modulo, String tipoOrden,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BigDecimal totalImporte = BigDecimal.ZERO;
		Object[] filtro = getTipoOrden(tipoOrden,modulo);
		totalImporte = daoPagos.obtenerListadoPagosMontos(modulo, filtro[0].toString(), filtro[1].toString(),  architechSessionBean);
		return totalImporte;
	}


	/**
	 * Buscar pagos monto.
	 *
	 * Buscar los importes
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param field                El objeto: field
	 * @param valor                El objeto: valor
	 * @param tipoOrden            El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	@Override
	public BigDecimal buscarPagosMonto(BeanModulo modulo, String field, String valor, String tipoOrden,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BigDecimal totalImporte = BigDecimal.ZERO;
		Object[] filtro = getTipoOrden(tipoOrden,modulo);
		totalImporte = daoPagos.buscarPagosMontos(modulo, field, valor, filtro[0].toString(), filtro[1].toString(),architechSessionBean);
		return totalImporte;
	}

	/**
	 * Exportar todos listado pagos.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 * 
	 * @param modulo             El objeto: modulo SPID o SPEI
	 * @param nombre             El objeto: nombre
	 * @param tipoOrden          El objeto: tipo orden
	 * @param beanReqListadoPago El objeto: bean req listado pago
	 * @param architechBean      El objeto: architech bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanResListadoPagos exportarTodoListadoPagos(BeanModulo modulo,String nombre, String tipoOrden,
			BeanReqListadoPagos beanReqListadoPago, ArchitechSessionBean architechBean) throws BusinessException {
		BeanResBase responseDAO = null;
		BeanResListadoPagos response = new BeanResListadoPagos();
			BeanExportarTodo exportarRequest = new BeanExportarTodo();
			exportarRequest.setColumnasReporte(COLUMNAS_REPORTE);
			Object[] filtro = getTipoOrden(tipoOrden,modulo);
			exportarRequest.setConsultaExportar(completaQuery(modulo, filtro[0].toString(), filtro[1].toString()));
			exportarRequest.setEstatus("PE");
			exportarRequest.setModulo(utilsExportar.getModulo(modulo));
			exportarRequest.setSubModulo(utilsExportar.getMonitor(modulo).concat("_".concat(nombre)));
			exportarRequest.setNombreRpt(nombre.concat(modulo.getTipo().toUpperCase().concat("_".concat(modulo.getModulo().toUpperCase().concat("_")))));
			exportarRequest.setTotalReg(beanReqListadoPago.getTotalReg() + StringUtils.EMPTY);
			responseDAO = daoExportar.exportarTodo(exportarRequest, architechBean);
			if (!Errores.EC00011B.equals(responseDAO.getCodError())) {
				response.setNombreArchivo(responseDAO.getMsgError());
				response.setCodError(responseDAO.getCodError());
				response.setMsgError(responseDAO.getMsgError());
				response.setTipoError(responseDAO.getTipoError());
			}
		return response;
	}
	
	/**
	 * Completa query.
	 *
	 * @param modulo El objeto: modulo
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @return Objeto string
	 */
	private String completaQuery(BeanModulo modulo, String cveMecanismo, String estatus) {
		StringBuilder query = new StringBuilder();
		query.append(CONSULTA_PAGOS.toString().replaceAll("\\,", " || ',' || "))
		 .append("FROM TRAN_MENSAJE TM ");
		if (ValidadorMonitor.isSPEI(modulo)) {
			query.append(", TRAN_SPEI_ENV TMS ");
		} else {
			query.append(", TRAN_MENSAJE_SPID TMS ");
		}
		String origen = ValidadorMonitor.getOrigen(modulo);
		query.append("WHERE TM.ESTATUS IN [").append("TM.ESTATUS").append("] AND TM.CVE_MECANISMO = '[CVE_MECANISMO]'")
		.append(" AND TM.REFERENCIA = TMS.REFERENCIA ")
		.append(origen);
		return query.toString().replaceAll("\\[CVE_MECANISMO\\]", cveMecanismo).replaceAll("\\[TM.ESTATUS\\]", estatus);
	}


	/**
	 * Consulta detalle pago.
	 *
	 * Consulta el detalle de pago
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param referencia           El objeto: referencia
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean detalle pago
	 * @throws BusinessException La business exception
	 */
	@Override
	public BeanDetallePago consultaDetallePago(BeanModulo modulo, String referencia,
			ArchitechSessionBean architechSessionBean) throws BusinessException {
		BeanDetallePago beanDetallePago = null;
		beanDetallePago = daoPagos.consultarDetallePago(referencia, modulo, architechSessionBean);
		return beanDetallePago;
	}

}
