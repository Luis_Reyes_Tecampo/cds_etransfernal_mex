/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExcepCtasFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     18/12/2019 11:43:26 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;

/**
 * Class UtileriasExcepCtasFideico.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
public final class UtileriasExcepCtasFideico implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3709960265697667178L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasExcepCtasFideico instancia;
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT ''''||TO_CHAR(NUM_CUENTA)||'''' FROM TRAN_CTAS_FIDEICO_EXENTAS [FILTRO_WH]";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "N\u00FAmero de Cuenta";
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return El objeto: instancia unica para la clase
	 */
	public static UtileriasExcepCtasFideico getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasExcepCtasFideico();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro --> Parametro con los datos de filtrado
	 * @param key El objeto: key --> Campo de BD
	 * @return El objeto: filtro retornado en String
	 */
	public String getFiltro(BeanCtaFideicomiso beanFiltro, String key) {
		filtro = new StringBuilder();
		if (beanFiltro.getCuenta() != null && !beanFiltro.getCuenta().isEmpty()) {
			filtro.append(" "+key+" UPPER(TRIM(NUM_CUENTA)) LIKE UPPER('%"+beanFiltro.getCuenta().trim()+"%')");
		}
		return filtro.toString();
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para agregar los parametros a la consulta
	 * que se este ejecutando.
	 *
	 * @param beanCuenta El objeto: bean cuenta con los datos
	 * @param anterior El objeto: anterior --> Dato anterior
	 * @param operacion El objeto: operacion --> Operacion realizada
	 * @return Objeto list --> lista con los parametros seteados
	 */
	public List<Object> agregarParametros(BeanCtaFideicomiso beanCuenta, BeanCtaFideicomiso anterior, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.MODIFICACION)) {
			parametros.add(beanCuenta.getCuenta());
			parametros.add(anterior.getCuenta());
		}
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.ALTA) || 
				operacion.equalsIgnoreCase(ConstantesCatalogos.BAJA) ||
				operacion.equalsIgnoreCase(ConstantesCatalogos.BUSQUEDA)) {
			parametros.add(beanCuenta.getCuenta());
		}
		return parametros;
	}
}
