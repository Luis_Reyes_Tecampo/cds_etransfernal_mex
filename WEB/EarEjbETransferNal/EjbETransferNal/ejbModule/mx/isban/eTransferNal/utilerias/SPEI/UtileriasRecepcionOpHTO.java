/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpHTO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:47:39 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * en esta clase de utilerias se las funciones para realizar una devolucion extemporanea.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
public class UtileriasRecepcionOpHTO  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -8242875650770861907L;
	
		
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionOpHTO util = new UtileriasRecepcionOpHTO();
	
	
	/**
	 * Obten la instancia de UtileriasRecepcionOpHTO.
	 *
	 * @return instancia de UtileriasRecepcionOpHTO
	 */
	public static UtileriasRecepcionOpHTO getInstance(){
		return util;
	}
			 
	
	/**
	 * Setea key.
	 *
	 * @param fchOperacion El objeto: fch operacion
	 * @param cveMiInstituc El objeto: cve mi instituc
	 * @param cveInstOrd El objeto: cve inst ord
	 * @param folioPaquete El objeto: folio paquete
	 * @param folioPago El objeto: folio pago
	 * @return Objeto string
	 */
	public  String seteaKey(String fchOperacion,String cveMiInstituc, String cveInstOrd, String folioPaquete, String folioPago){
			StringBuilder strBuild = new StringBuilder();
			String pipe ="|";
			strBuild.append("FCH_OPERACION=");
			strBuild.append(fchOperacion);
			strBuild.append(pipe);
			strBuild.append("CVE_MI_INSTITUC=");
			strBuild.append(cveMiInstituc);
			strBuild.append(pipe);
			strBuild.append("CVE_INST_ORD=");
			strBuild.append(cveInstOrd);
			strBuild.append(pipe);
			strBuild.append("FOLIO_PAQUETE=");
			strBuild.append(folioPaquete);
			strBuild.append(pipe);
			strBuild.append("FOLIO_PAGO=");
			strBuild.append(folioPago);
			
		return strBuild.toString();
	}
	
	/**
	 * Metodo que permite generar la llave.
	 *
	 * @param numCuentaOrd String con el numero de cuenta ordenante
	 * @param numCuentaRec String cve mi institucion
	 * @param importe String con el importe
	 * @return String con la trama de campos
	 */
	public  String seteaCampos(String numCuentaOrd,String numCuentaRec,String importe){
			StringBuilder strBuild = new StringBuilder();
			String pipe ="|";
			strBuild.append(pipe);
			strBuild.append("NUM_CUENTA_ORD=");
			strBuild.append(numCuentaOrd);
			strBuild.append(pipe);
			strBuild.append("NUM_CUENTA_REC=");
			strBuild.append(numCuentaRec);
			strBuild.append(pipe);
			strBuild.append("IMPORTE=");
			strBuild.append(importe);
			strBuild.append(pipe);
			
		return strBuild.toString();
	}
	
	/**
	 * Setea trama enviar.
	 *
	 * @param bean El objeto: bean
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean trans envio
	 */
	public  BeanTransEnvio seteaTramaEnviar(BeanDetRecepOperacionDAO bean, ArchitechSessionBean architechSessionBean) {
		/**Se inicializa la trama**/
		BeanTransEnvio tEnvio = new BeanTransEnvio();
		/**Se asigna los datos principales**/
		tEnvio.setCveEmpresa("TRANSANTBME");
		tEnvio.setPtoVta("0901");
		tEnvio.setMedioEnt("SPEI");
		tEnvio.setCveIntermeOrd("BANME");
		tEnvio.setUsuarioCap(architechSessionBean.getUsuario());
		tEnvio.setUsuarioSol(architechSessionBean.getUsuario());
		tEnvio.setCveOperacion("DEVEXTEM");
		tEnvio.setFormaLiq("H");
		tEnvio.setDivisaOrd("MN");
		tEnvio.setPlazaBanxico("01001");
		tEnvio.setDivisaRec("MN");
		tEnvio.setImporteDolares("0.0");
		tEnvio.setImporteCargo("0.0");
		tEnvio.setImporteAbono("0.0");
			
		tEnvio=  seteaTramaEnviarCont( bean, tEnvio );
		
		return tEnvio;
	}
	
	/**
	 *  Se genera la trama que se envia a mq
	 *
	 * @param bean El objeto: bean
	 * @param tEnvio El objeto: t envio
	 * @return Objeto bean trans envio
	 */
	public  BeanTransEnvio seteaTramaEnviarCont(BeanDetRecepOperacionDAO bean,BeanTransEnvio tEnvio ) {
		/**Se inicializa la trama**/
		BeanTransEnvio tEnvioB = new BeanTransEnvio();
		
		double folioPaq = 0;
		double folioPgo = 0;
		/**ongtiene el folio paquete**/
		if (bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete() != null) {
			folioPaq = Double.parseDouble(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
		}
		/**asigna el folio de pago**/
		if (bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago() != null) {
			folioPgo = Double.parseDouble(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
		}
		/** calcula la referencia**/
		tEnvio.setReferenciaMed(String.valueOf(folioPaq * 10000 + folioPgo));
		/** Si el tipo de pago  cumple con 1 o 5 se asigna la cvetransferencia**/
		if ("1".equals(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago()) || "5".equals(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago())
				|| "12".equals(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago())) {
			tEnvio.setCveTransfe("074");
		} else {
			tEnvio.setCveTransfe("077");
		}
		
		tEnvioB=tEnvio;	
		/**Se llama la funcion**/
		tEnvioB= seteaTramaEnviarContA( bean, tEnvio );
		
		/**Se regresa la funcion**/
		return tEnvioB;
	}
	
	/**
	 * Setea trama enviar cont A.
	 *
	 * @param bean El objeto: bean
	 * @param tEnvio El objeto: t envio
	 * @return Objeto bean trans envio
	 */
	public  BeanTransEnvio seteaTramaEnviarContA(BeanDetRecepOperacionDAO bean,BeanTransEnvio tEnvio ) {
		/**Se inicializa la trama**/
		BeanTransEnvio tEnvioB = new BeanTransEnvio();
		/** asigna la cuenta orden**/
		if (bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd() != null) {
			tEnvio.setCuentaOrd(bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd());
		}
		/** asigna los diferentes valores**/
		if (bean.getBeanDetRecepOperacion().getOrdenante().getNombreOrd() != null) {
			tEnvio.setNombreOrd(bean.getBeanDetRecepOperacion().getOrdenante().getNombreOrd());
		}
		if (bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd() != null) {
			tEnvio.setCuentaRec(bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd());
		}
		if (bean.getBeanDetRecepOperacion().getReceptor().getNombreRec() != null) {
			tEnvio.setNombreRec(bean.getBeanDetRecepOperacion().getReceptor().getNombreRec());
		}
		if (bean.getBeanDetRecepOperacion().getReceptor().getBancoRec() != null) {
			tEnvio.setSucBcoRe(bean.getBeanDetRecepOperacion().getReceptor().getBancoRec()); 
		}
		if (bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo() != null) {
			tEnvio.setCiudadBcoRe(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo());
		}
		if (bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo() != null) {
			tEnvio.setComentario3(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo());
		}
		tEnvioB =tEnvio;	
		return tEnvioB;
	}
	
	
	/**
	 * Calcular.
	 *
	 * @param bean El objeto: bean
	 * @param valores El objeto: valores
	 * @param val El objeto: val
	 * @return Objeto double
	 */
	public  double calcular(BeanDetRecepOperacionDAO bean, double[] valores,String[] val ) {
		/**Se asigna los valores **/
		double tiempoRechazo=0;
    	double sqlDif1 =valores[0];
    	double sqlDif2 =valores[1];
    	double sqlDif3 =valores[2];
    	String fNoctFinRec= val[0];
    	String fNoctFinRecNomina= val[1];
    	int tMaxDevEnvMv= Integer.parseInt(val[2]);
    	/**Se inicializa el calculo**/
		if(("1").equals(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago())) { 
			/**tercero a tercero**/
			if(bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion().equals(fNoctFinRec.subSequence(0,10))) {
				tiempoRechazo =sqlDif3 - tMaxDevEnvMv;
			}else {
				tiempoRechazo = sqlDif1;
			}   			
		}else if(("12").equals(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago())){
			if(bean.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion().equals(fNoctFinRecNomina.subSequence(0,10))) {
				tiempoRechazo= sqlDif2;
			}else {
				tiempoRechazo =sqlDif3 - tMaxDevEnvMv;
			}
		}
		return tiempoRechazo;
	}
	
	/**
	 * Respues envio.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param respuestas El objeto: respuestas
	 * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
	 * @return Objeto bean tran spei rec
	 */
	public BeanTranSpeiRecOper respuesEnvio(BeanTranSpeiRecOper beanTranSpeiRec,String[] respuestas,BeanReqActTranSpeiRec beanReqActTranSpeiRec){
		BeanTranSpeiRecOper beanTranSpeiRecA = new BeanTranSpeiRecOper();
		beanTranSpeiRec.getBeanDetalle().setError(new BeanResBase());
		/**Se asgian el codigo de error**/
		if(respuestas.length>0 && ConstantesRecepOperacion.TRI_B0000.equals(respuestas[0])){
			beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_INFO);
			beanTranSpeiRec.getBeanDetalle().getError().setCodError(respuestas[0]);
		}else{
			beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ALERT);
			beanTranSpeiRec.getBeanDetalle().getError().setCodError(respuestas[0]);
		}
		beanTranSpeiRecA = beanTranSpeiRec;
		/**regresa el objeto*/
		return beanTranSpeiRecA;
	}
	
	/**
	 * Codigo error.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto bean tran spei rec
	 */
	public   BeanTranSpeiRecOper codigoError(BeanTranSpeiRecOper beanTranSpeiRec) {
		beanTranSpeiRec.getBeanDetalle().setError(new BeanResBase());
		/**asigna el error **/
		beanTranSpeiRec.getBeanDetalle().getError().setCodError("ED00090V");
		beanTranSpeiRec.getBeanDetalle().getError().setTipoError(Errores.TIPO_MSJ_ERROR);
		beanTranSpeiRec.getBeanDetalle().getError().setMsgError("");
		/**regresa el objeto*/
		return beanTranSpeiRec;
	}
	
	
	/**
	 * Dato.
	 *
	 * @param constante El objeto: constante
	 * @return Objeto string
	 */
	public String dato(String constante) {
		String dto="";
		/**Se metodo valores**/
		if(ConstantesRecepOperacion.PI_H_NOC_FIN_REC.equals(constante)) {
			dto = ConstantesRecepOperacion.VD_PI_H_NOC_FIN_REC;
		}else if(ConstantesRecepOperacion.PI_H_NOC_FIN_D_N .equals(constante)) {
			dto = ConstantesRecepOperacion.VD_PI_H_NOC_FIN_D_N;
		}else if(ConstantesRecepOperacion.PI_H_NOC_FIN_DEV.equals(constante)) {
			dto = ConstantesRecepOperacion.VD_PI_H_NOC_FIN_DEV;
		}else if(ConstantesRecepOperacion.PI_H_NOC_FIN_R_N.equals(constante)) {
			dto = ConstantesRecepOperacion.VD_PI_H_NOC_FIN_R_N;
		}
		/**regresa el objeto**/
		return dto;
	}
	
	
	
	
}
