package mx.isban.eTransferNal.utilerias.SPEI;

import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRecExt;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRecOpcion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecComun;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * en esta clase de utilerias se pusieron las funciones que se utilizan  para que sean reutilizables como el
 * sete de la respuesta de la consulta al bean de operciones.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
public class UtileriasRecepcionOpFunciones  extends Architech {
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -7113137848795772969L;

	
	
	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionOpFunciones util = new UtileriasRecepcionOpFunciones();
	
	/** La constante util. */
	public static final UtileriasRecepcionFiltros utilFiltro =  UtileriasRecepcionFiltros.getInstance();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasRecepcionOpFunciones getInstance(){
		return util;
	}
			 
	 
	 /**
 	 * Setea respuesta exitosa.
 	 *
 	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
 	 * @param beanResConsTranSpeiRecDAO El objeto: bean res cons tran spei rec DAO
 	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
 	 * @param paginador El objeto: paginador
 	 * @return Objeto bean res tran spei rec
 	 */
 	public BeanResTranSpeiRec seteaRespuestaExitosa(BeanReqTranSpeiRec beanReqTranSpeiRec, BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO, BeanResTranSpeiRec beanResConsTranSpeiRec, BeanPaginador paginador ){
	    	/**Se declara el objeto utilerias**/
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	/**Se asigna valores**/
	    	//inicializa bean se sumas 
	    	beanResConsTranSpeiRec.setBeanResConsOpcion(new BeanResTranSpeiRecOpcion());	    	
	    	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrVolumenTopoVLiquidadas(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getVolumenTopoVLiquidadas(), Utilerias.FORMATO_NUMBER) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrMontoTopoVLiquidadas(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getMontoTopoVLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrVolumenTopoTLiquidadas(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getVolumenTopoTLiquidadas(), Utilerias.FORMATO_NUMBER) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrMontoTopoTLiquidadas(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getMontoTopoTLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrVolumenTopoTXLiquidar(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getVolumenTopoTXLiquidar(), Utilerias.FORMATO_NUMBER) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrMontoTopoTXLiquidar(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getMontoTopoTXLiquidar(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrVolumenRechazos(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getVolumenRechazos(), Utilerias.FORMATO_NUMBER) );
		   	beanResConsTranSpeiRec.getBeanResConsOpcion().setStrMontoRechazos(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getMontoRechazos(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS) );
		   	beanResConsTranSpeiRec.setListBeanTranSpeiRec(beanResConsTranSpeiRecDAO.getBeanTranSpeiRecList());
		   	//inicializa bean se sumas 
		   	beanResConsTranSpeiRec.setBeanResConsExt(new BeanResTranSpeiRecExt());
		   	beanResConsTranSpeiRec.getBeanResConsExt().setSumaTopoV(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getSumaTopoV(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS) );
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaVolV(beanResConsTranSpeiRecDAO.getSumaVolV());
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaTopoTxL(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getSumaTopoTxL(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaVolTxL(beanResConsTranSpeiRecDAO.getSumaVolTxL());
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaTopoT(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getSumaTopoT(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaVolT(beanResConsTranSpeiRecDAO.getSumaVolT());
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaTopoRech(utilerias.formateaDecimales(beanResConsTranSpeiRecDAO.getSumaTopoRech(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
	    	beanResConsTranSpeiRec.getBeanResConsExt().setSumaVolRech(beanResConsTranSpeiRecDAO.getSumaVolRech());
	    	
	    	//incializa bean comun
	    	beanResConsTranSpeiRec.setBeanComun(new BeanTranSpeiRecComun());
	    	beanResConsTranSpeiRec.getBeanComun().setTotalReg(beanResConsTranSpeiRecDAO.getTotalReg());
	    	beanResConsTranSpeiRec.getBeanComun().setOpcion(beanReqTranSpeiRec.getBeanComun().getOpcion());
	    	
	    	/**Se calcula el paginador**/
	    	paginador.calculaPaginas(beanResConsTranSpeiRec.getBeanComun().getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
			beanResConsTranSpeiRec.setBeanPaginador(paginador);	
			//inicialzia bean error
			beanResConsTranSpeiRec.setBeanError(new BeanResBase());
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.OK00000V);
			
			 /**Se regresa el objeto **/
			return beanResConsTranSpeiRec;
	    }
	    
	    
	   
	
	    /**
    	 * Act mot rechazo cont.
    	 *
    	 * @param beanResBase El objeto: bean res base
    	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
    	 * @return Objeto bean res cons tran spei rec
    	 * @throws BusinessException La business exception
    	 */
	    public BeanResTranSpeiRec actMotRechazoCont(BeanResBase beanResBase,BeanResTranSpeiRec beanResConsTranSpeiRec)  throws BusinessException {
	    	//inicialzia bean error
			beanResConsTranSpeiRec.setBeanError(new BeanResBase());
	    	/**Inicia la consulta para ver el error**/
		    if(!Errores.OK00000V.equals(beanResBase.getCodError())){
		    	beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00064V);
		    	beanResConsTranSpeiRec.getBeanError().setMsgError( Errores.TIPO_MSJ_ERROR);
		    	/**inicializa el BusinessException **/
	  			throw new BusinessException(beanResBase.getCodError(), beanResBase.getMsgError());
		    }
		    return beanResConsTranSpeiRec;
		} 
	    
	    /**
    	 *  
    	 * funcion para otener la constante.
    	 *
    	 * @param constante El objeto: constante
    	 * @return Objeto string
    	 */
	    public String paramentro(String constante) {
	    	String dto=""; 	    	
	    	/**busca la constante deacuerdo al paramentro de entrada**/
	    	if(ConstantesRecepOperacion.PI_T_MAX_DEV_ENV.equals(constante)) {
				dto = ConstantesRecepOperacion.VD_PI_T_MAX_DEV_ENV;
			}else if(ConstantesRecepOperacion.PI_T_MAX_DEV_E_M .equals(constante)) {
				dto = ConstantesRecepOperacion.VD_PI_T_MAX_DEV_E_M;
			}
	    	return dto;
	    }
	    
	    	    
	    
	    /**
    	 * Paramentro bitacora.
    	 *
    	 * @param codigoError El objeto: codigo error
    	 * @return true, si exitoso
    	 */
    	public boolean paramentroBitacora(String codigoError) {
	    	boolean dto=false; 	    	
	    	/**Confirmar que valla esta parte**/	       		
			if(ConstantesRecepOperacion.TRI_B0000.equals(codigoError)){    
				dto = ConstantesRecepOperacion.ENVIOESTATUS;
			}
	    	return dto;
	    }
	    
    	
    	/**
	     * Obtener el objeto: query consulta exportar.
	     *
	     * @param historico El objeto: historico
	     * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	     * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	     * @return El objeto: query consulta exportar
	     */
        public String getQueryConsultaExportarTodo(boolean historico,BeanReqTranSpeiRec beanReqConsTranSpei,BeanTranSpeiRecOper beanTranSpeiRecOper){
 
        	/**es la entidad segun la pantalla principal o alterna**/
        	String institucion =beanReqConsTranSpei.getListBeanTranSpeiRec().get(0).getBeanDetalle().getDetalleGral().getCveMiInstitucion();
        	String  fechaOp="";
        	String tabla =ConstantesRecepOperacionApartada.QUERY_CONSULTA_TODAS_EXP.replaceAll("\\[FILTRO_INST\\]", institucion);
        	String filtroAnd="";
        	if(beanTranSpeiRecOper.getBeanDetalle()!=null) {
    			filtroAnd = utilFiltro.getFiltro(beanTranSpeiRecOper, "AND");
    		}
        	
        	/**si es historico solo manda la fecha **/
        	if(historico) {        		
        		fechaOp= " AND FCH_OPERACION= to_date('"+beanReqConsTranSpei.getListBeanTranSpeiRec().get(0).getBeanDetalle().getDetalleGral().getFchOperacion()+"','dd-mm-yyyy' )";   
        		tabla = ConstantesRecepOperacionApartada.QUERY_CONSULTA_TODAS_EXP_HTO.replaceAll("\\[FILTRO_INST\\]", institucion);
        	}
        	
        	tabla = tabla.replaceAll("\\[FILTRO_FECHA\\]", fechaOp).replaceAll("\\[FILTRO_AND\\]", filtroAnd);
    		
        	return tabla;
        }
        
         
    	/**
	     * Obtener respuesta.
	     *
	     * @param listBeanTranSpeiRecOK El objeto: list bean tran spei rec OK
	     * @param listBeanTranSpeiRecNOK El objeto: list bean tran spei rec NOK
	     * @param listBeanTranSpeiRecTemp El objeto: list bean tran spei rec temp
	     * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
	     * @param cuentas El objeto: cuentas
	     * @param beanTranSpeiRecTemp El objeto: bean tran spei rec temp
	     * @return Objeto bean res cons tran spei rec
	     * @throws BusinessException La business exception
	     */
	    public BeanResTranSpeiRec obtenerRespuesta(List<BeanTranSpeiRecOper> listBeanTranSpeiRecOK,List<BeanTranSpeiRecOper> listBeanTranSpeiRecNOK,
    			List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp,BeanResTranSpeiRec beanResConsTranSpeiRec,String cuentas,BeanTranSpeiRecOper beanTranSpeiRecTemp )throws BusinessException {
	    	BeanResTranSpeiRec beanRes=new  BeanResTranSpeiRec();
	    	//inicialzia bean error
			beanResConsTranSpeiRec.setBeanError(new BeanResBase());
    		beanRes = beanResConsTranSpeiRec;
    		/** pregunta si trae operaciones sin asignar**/
    		if (!listBeanTranSpeiRecNOK.isEmpty()) {
    			/**en caso de que la lista sera mayor a las que se mandaron se manda el error**/
    			if (listBeanTranSpeiRecNOK.size() >= listBeanTranSpeiRecTemp.size() && cuentas.length()>0) {
    				beanRes.getBeanError().setCodError(Errores.ED00132V);
    				beanRes.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
    				beanRes.getBeanError().setMsgError(cuentas);
    			}else {
    				beanRes.getBeanError().setCodError(beanTranSpeiRecTemp.getBeanDetalle().getError().getCodError());
    				beanRes.getBeanError().setMsgError(beanTranSpeiRecTemp.getBeanDetalle().getError().getMsgError());
    				beanRes.getBeanError().setTipoError(beanTranSpeiRecTemp.getBeanDetalle().getError().getTipoError());
    				
    			}
    		} else /**si la lista correcta es igual a la lista que se envio manda mensaje de exito**/
    			if (listBeanTranSpeiRecOK.size() == listBeanTranSpeiRecTemp.size()) {
    			beanRes.getBeanError().setCodError(Errores.OK00001V);
    			beanRes.getBeanError().setTipoError(Errores.TIPO_MSJ_INFO);
    		}else {
    			/**en caso de haber alguna excepcion**/
    			throw new BusinessException(Errores.EC00011B, Errores.DESC_EC00011B);
    		}
    		
      		return beanRes;
    	}
	    
}
