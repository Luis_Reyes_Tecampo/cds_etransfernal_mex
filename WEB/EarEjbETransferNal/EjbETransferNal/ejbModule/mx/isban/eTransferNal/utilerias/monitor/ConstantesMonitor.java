/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/01/2019 05:37:08 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.monitor;

/**
 * The Class UtileriasDetRecpOp.
 *
 * @author FSW-Vector
 * @since 28/01/2019
 */
public final class ConstantesMonitor {
	
	/** La constante ENTIDAD_SPEI_CA. */
	public static final String ENTIDAD_SPEI_CA ="ENTIDAD_SPEI_CA";
	
	/** La constante ENTIDAD_SPEI. */
	public static final String ENTIDAD_SPEI ="ENTIDAD_SPEI";
	
	/** La constante ENTIDAD_SPID. */
	public static final String ENTIDAD_SPID ="ENTIDAD_SPID";
	
	/** La constante SELECT_A. */
	private static final String SELECT_A = "SELECT T.*, ROWNUM R FROM (SELECT T.HORA_RECEPCION,T.FOLIO_SERVIDOR,T.TIPO_TRASPASO"
			+"||' '||DECODE(T.TIPO_TRASPASO,3,'SIAC-SPID',4,'SIDV-SIAC-SPID','') DSC,"
			+"T.MONTO,T.REFERENCIA_SIAC, C.CONT ";
	
	
	/**  * Constante del tipo String que almacena la consulta para obtener. */
	public static final StringBuilder CONSULTA_AVISO = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append(SELECT_A).append("T.MONTO,T.REFERENCIA_SIAC, C.CONT ")
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");

	/** * Constante del tipo String que almacena la consulta para obtener registros con fecha y cveinst. */
	public static final StringBuilder CONSULTA_AVISO_PAR = new StringBuilder()
			.append("SELECT T.* FROM (")
			.append(SELECT_A)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ").append("WHERE R BETWEEN ? AND ?  ");

	/** Consulta para buscar todos los traspasos. */
	public static final StringBuilder TOTAL_CONSULTA_AVISO_TRAPASOS = new StringBuilder()
			.append("SELECT T.HORA_RECEPCION || ',' || T.FOLIO_SERVIDOR || ',' || T.TIPO_TRASPASO")
			.append(" || ' ' || DECODE(T.TIPO_TRASPASO,3,'SIAC-SPEI',4,'SIDV-SIAC-SPEI','') || ',' || ")
			.append("T.MONTO || ',' || T.REFERENCIA_SIAC ")
			.append("FROM TRAN_TRSIAC_SPID T");

	/**  Consulta de insercion en tabla con nombre de archivo de exportar. */
	public static final StringBuilder INSERT_EXPORTAR_TODO = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA ")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2) ")
            .append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	
	/** Consulta de insercion en tabla con nombre de archivo de todos las recepciones exportadas. */
	public static final StringBuilder INSERT_TODOS_CONSULTA_RECEPCIONES = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	
	
	/** Consulta de sumatoria de montos. */
	public static final StringBuilder CONSULTA_AVISO_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) AS MONTO_TOTAL FROM (")
			.append(SELECT_A)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID) C [ORDENAMIENTO]) T ")
			.append(") T ");

	/** * Constante del tipo String que almacena la consulta para obtener registros con fecha y cveinst. */
	public static final StringBuilder CONSULTA_AVISO_PAR_SUM = new StringBuilder()
			.append("SELECT SUM(MONTO) MONTO_TOTAL FROM (")
			.append(SELECT_A)
			.append("FROM TRAN_TRSIAC_SPID T, (SELECT COUNT(*) CONT FROM TRAN_TRSIAC_SPID WHERE CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy')) C WHERE  CVE_MI_INSTITUC=? AND TRUNC(FCH_OPERACION) = TO_DATE(?,'dd/mm/yyyy') [ORDENAMIENTO]) T ")
			.append(") T ");
	

	/** Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP. */
	public static final String SPID = "SPID";
	
	/** Propiedad del tipo String que almacena el valor de PAGE_SPEI. */
	public static final String SPEI = "SPEI";
	
	/** La constante SPEICIA. */
	public static final String SPEICA = "SPEICA";	
	
	/** Propiedad del tipo String que almacena el valor de PAGE_ADMON. */
	public static final String ADMON = "ADMON";
	
	/** Constante privada del tipo String que almacena el query para la consulta de recepciones. */
	public static final StringBuilder TOTAL_CONSULTA_RECEPCIONES = new StringBuilder().append("SELECT ")
			.append(" TOPOLOGIA || ',' || TIPO_PAGO || ',' || ESTATUS_BANXICO || '|' || ESTATUS_TRANSFER || ',' || FCH_OPERACION || ',' ||")
			.append(" FCH_CAPTURA || ',' || CVE_INST_ORD || ',' || CVE_INTERME_ORD || ',' || FOLIO_PAQUETE || ',' || FOLIO_PAGO || ',' || ")
			.append(" NUM_CUENTA_REC || ',' || MONTO || ',' ||")
			.append(" MOTIVO_DEVOL || ',' || CODIGO_ERROR || ',' || NUM_CUENTA_TRAN").append(" FROM TRAN_SPID_REC");
	

	/** JOIN BLOQUEO. */
	public static final String QUERY =
	        " R.FCH_OPERACION = B.FCH_OPERACION "+
	        " AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
	        " AND R.CVE_INST_ORD = B.CVE_INST_ORD "+
	        " AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
	        " AND R.FOLIO_PAGO = B.FOLIO_PAGO ";
	
	/** CONSULTA DETALLE. */
	public static final String QUERY_DETALLE =	
	" SELECT TO_CHAR(R.fch_operacion,'DD/MM/YYYY') fch_operacion, R.cve_mi_instituc,R.cve_inst_ord, "+
	" R.folio_paquete,R.folio_pago,R.topologia, "+
	" R.enviar,R.cda,R.estatus_banxico, "+
	" R.estatus_transfer,R.motivo_devol,R.tipo_cuenta_ord, "+
	" R.tipo_cuenta_rec,R.clasif_operacion,R.tipo_operacion, "+
	" R.cve_interme_ord,R.cod_postal_ord,R.codigo_error, "+
	" R.fch_constit_ord,R.fch_instruc_pago,R.hora_instruc_pago, "+
	" R.fch_acept_pago,R.hora_acept_pago,R.rfc_ord, "+
	" R.rfc_rec,R.refe_numerica,R.num_cuenta_ord, "+
	" R.num_cuenta_rec,R.num_cuenta_tran,R.tipo_pago, "+
	" R.prioridad,R.long_rastreo,R.folio_paq_dev, "+
	" R.folio_pago_dev,R.refe_transfer,R.monto, "+
	" TO_CHAR(R.fch_captura,'DD/MM/YYYY HH24:mi:ss') fch_captura,R.cve_rastreo,R.cve_rastreo_ori, "+
	" R.direccion_ip,R.cde,R.nombre_ord, "+        
	" R.nombre_rec,R.domicilio_ord,R.concepto_pago, B.CVE_USUARIO, B.FUNCIONALIDAD, "+     
   " ROWNUM INDICE "+
   " FROM TRAN_SPID_REC R LEFT JOIN TRAN_SPID_BLOQUEO B ON( " +
    QUERY +
   " ) "+
   " WHERE R.CVE_MI_INSTITUC = ? "+
   " AND  R.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') " +
   " AND  R.CVE_INST_ORD = ? " +
   " AND  R.FOLIO_PAQUETE = ? " +
   " AND  R.FOLIO_PAGO = ? ";
	
	
	/** ****************************************** ordenes por reparar***********************************************. */
	
	/**
	 * Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPID
	 */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION =
			new StringBuilder()
			.append("SELECT TM.* FROM ( ")
				.append("SELECT TM.*, ROWNUM R FROM ( ")
					.append("SELECT DISTINCT(TM.REFERENCIA), TM.CVE_COLA, TM.CVE_TRANSFE, ")
					.append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TO_CHAR(TM.FCH_CAPTURA, 'DD/MM/YYYY HH24:MI:SS') FCH_CAPTURA,TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")			
					.append("TM.ESTATUS, TM.COMENTARIO3, TMS.FOLIO_PAQUETE, TMS.FOLIO_PAGO, C.CONT ")
					.append("FROM TRAN_MENSAJE TM JOIN TRAN_MENSAJE_SPID TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR' [ORDENAMIENTO] ) TM ) TM WHERE TM.R BETWEEN ? AND ?");
	
	
	/** Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPEI. */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION_SPEI =
			new StringBuilder()
			.append("SELECT TM.* ")
			.append("FROM ( SELECT TM.*, ROWNUM R ")
				.append("FROM ( SELECT DISTINCT(TM.REFERENCIA), TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO ")
					.append(", TM.CVE_MEDIO_ENT, TO_CHAR(TM.FCH_CAPTURA, 'DD/MM/YYYY HH24:MI:SS') FCH_CAPTURA,TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")			
					.append("TM.ESTATUS, TM.COMENTARIO3,  C.CONT ")
					.append("FROM TRAN_MENSAJE TM , ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPEI' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPEI' ")
					.append("AND ESTATUS='PR' [ORDENAMIENTO]")
				.append(") TM ) TM  WHERE TM.R BETWEEN ? AND ?");
	
	/** Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPEI. */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION_SPEI_OPT =
			new StringBuilder()
			.append("SELECT ")
			.append("referencia,cve_cola,cve_transfe,cve_mecanismo,")
			.append("cve_medio_ent,FCH_CAPTURA,cve_interme_ord,cve_interme_rec,")
			.append("importe_abono,estatus,")			
			.append("comentario3, CONT, rows_no ")
			.append("FROM (")
			.append("SELECT tm.referencia,tm.cve_cola,tm.cve_transfe,tm.cve_mecanismo,")
				.append("tm.cve_medio_ent,TO_CHAR(TM.FCH_CAPTURA, 'DD/MM/YYYY HH24:MI:SS') FCH_CAPTURA,tm.cve_interme_ord,tm.cve_interme_rec,")
					.append("tm.importe_abono,tm.estatus,")			
					.append("tm.comentario3, (SELECT COUNT(*) FROM TRAN_MENSAJE WHERE CVE_MECANISMO = 'SPEI' AND ESTATUS = 'PR') AS CONT")
					.append(",rownum rows_no")
					.append(" FROM TRAN_MENSAJE tm ")
					.append("WHERE CVE_MECANISMO = 'SPEI'")
					.append("AND ESTATUS = 'PR'")
					.append(" [ORDENAMIENTO]")
				.append(") WHERE rows_no > ? AND rows_no <= ?");
	
	/** Consulta para buscar las ordenes de reparacion con estatus PR y modulo CA. */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION_CA =
			new StringBuilder()
			.append("SELECT TM.* ")
			.append("FROM ( SELECT TM.*, ROWNUM R ")
				.append("FROM ( SELECT DISTINCT(TM.REFERENCIA), TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO ")
					.append(", TM.CVE_MEDIO_ENT, TO_CHAR(TM.FCH_CAPTURA, 'DD/MM/YYYY HH24:MI:SS') FCH_CAPTURA,TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")			
					.append("TM.ESTATUS, TM.COMENTARIO3,  C.CONT ")
					.append("FROM TRAN_MENSAJE TM , ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPEICA' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPEICA' ")
					.append("AND ESTATUS='PR' [ORDENAMIENTO]")
				.append(") TM ) TM  WHERE TM.R BETWEEN ? AND ?");
	
	
	
	/** Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPID. */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION_MONTOS =
			new StringBuilder()
			.append(" SELECT SUM(TM.IMPORTE_ABONO) as MONTO_TOTAL FROM TRAN_MENSAJE TM ")
					.append("JOIN TRAN_MENSAJE_SPID TMS ON TMS.REFERENCIA = TM.REFERENCIA, ")
					.append("(SELECT COUNT(*) CONT FROM TRAN_MENSAJE WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR') C ")
					.append("WHERE CVE_MECANISMO='SPID' AND ESTATUS='PR' [ORDENAMIENTO]");

	/** Consulta para buscar las ordenes de reparacion con estatus PR y modulo SPEI. */
	public static final StringBuilder CONSULTA_ORDENES_REPARACION_MONTOS_OPT =
			new StringBuilder()
			.append(" SELECT SUM(tm.importe_abono) AS monto_total FROM tran_mensaje tm ")
					.append("WHERE cve_mecanismo = 'SPEI' AND estatus = 'PR'");
	
	/** Consulta para actualizar las ordenes de reparacion a estatus LI del modulo SPID. */
	public static final StringBuilder UPDATE_ORDENES_REPARACION = 
			new StringBuilder().append("UPDATE TRAN_MENSAJE SET ESTATUS='LI' WHERE CVE_MECANISMO = 'SPID' AND REFERENCIA = ? ");
	
	/** Consulta para actualizar las ordenes de reparacion a estatus LI del modulo SPEI. */
	public static final StringBuilder UPDATE_ORDENES_REPARACION_SPEI = 
			new StringBuilder().append("UPDATE TRAN_MENSAJE SET ESTATUS='LI' WHERE CVE_MECANISMO = 'SPEI' AND REFERENCIA = ? ");
	
	/** Consulta para actualizar las ordenes de reparacion a estatus LI del modulo CA. */
	public static final StringBuilder UPDATE_ORDENES_REPARACION_CA = 
			new StringBuilder().append("UPDATE TRAN_MENSAJE SET ESTATUS='LI' WHERE CVE_MECANISMO = 'SPEICA' AND REFERENCIA = ? ");
	
	/** Consulta para buscar todas las ordenes de reparacion, se usa para exportar  a archivo. */
	public static final StringBuilder TOTAL_CONSULTA_ORDENES_REPARACION = 
			new StringBuilder().append("SELECT TM.REFERENCIA || ',' ||")
							   .append("TM.CVE_COLA || ',' || TM.CVE_TRANSFE || ',' || TM.CVE_MECANISMO || ',' || ") 
							   .append("TM.CVE_MEDIO_ENT || ',' || TM.FCH_CAPTURA || ',' || TM.CVE_INTERME_ORD || ',' || ")
						   	   .append("TM.CVE_INTERME_REC || ',' || TM.IMPORTE_ABONO || ',' || TM.ESTATUS || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END || ',' || TM.COMENTARIO3 ") 
					   	   	   .append("FROM TRAN_MENSAJE TM, TRAN_MENSAJE_SPID TMS ") 
					   	   	   .append("WHERE TM.REFERENCIA = TMS.REFERENCIA AND TM.ESTATUS = 'PR' AND TM.CVE_MECANISMO='SPID'");
	
	/** Consulta para buscar todas las ordenes de reparacion, se usa para exportar  a archivo. */
	public static final StringBuilder TOTAL_CONSULTA_ORDENES_REPARACION_SPEI = 
			new StringBuilder().append("SELECT TM.REFERENCIA || ',' ||")
							   .append("TM.CVE_COLA || ',' || TM.CVE_TRANSFE || ',' || TM.CVE_MECANISMO || ',' || ") 
							   .append("TM.CVE_MEDIO_ENT || ',' || TM.FCH_CAPTURA || ',' || TM.CVE_INTERME_ORD || ',' || ")
						   	   .append("TM.CVE_INTERME_REC || ',' || TM.IMPORTE_ABONO || ',' || TM.ESTATUS || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN ")
						   	.append("TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END || ',' || CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN ")
						   	.append("TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END || ',' || TM.COMENTARIO3 FROM TRAN_MENSAJE TM ") 
					   	   	   .append("WHERE TM.ESTATUS = 'PR' AND TM.CVE_MECANISMO='SPEI'");
	
	
	
	/** ********************************************************pagos ****************************************************************. */
	/**
	 * Consulta para buscar las ordenes y modulo SPID
	 */
	public static final String CVE_INTERME_ORD= "CVE_INTERME_ORD";
	
	/** Constante FROM de uso comun en queries. */
	public static final String FROM= "FROM (";
	
	/** CAMPO CVE_MECANISMO. */
	public static final String CVE_MECANISMO = "CVE_MECANISMO";
	
	/** CAMPO ORDENAMIENTO. */
	public static final String ORDENAMIENTO = "ORDENAMIENTO";
	
	/** CAMPO ORDER_BY. */
	public static final String ORDER_BY = " ORDER BY ";
		
	
	/** CAMPO ESTATUS. */
	public static final String ESTATUS = "ESTATUS";
	
	/** Constante WHERE para uso comun en queries. */
	public static final String WHERE = "WHERE ";
	
	/** Constante AND para uso comun en queries. */
	public static final String AND = "AND ";
	
	/** Constante IN para uso comun en queries. */
	public static final String IN = " IN [";
	
	/** Constante de llave para uso comun en queries. */
	public static final String OPEN_SP = " = '[";
	
	/** Constante de cierre de llave para uso comun en queries. */
	public static final String CLOSE_SP = "]' ";
	
	/** Querie para consulta de ordenes. */
	public static final StringBuilder CONSULTA_ORDENES = 
			new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") /** continua **/
			    .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") /** continua **/
			    .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") /** continua **/
			    .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        /** continua **/
			    .append(FROM) /** continua **/
			       .append("SELECT TM.*, ROWNUM AS RN ")/** continua **/
			        .append(FROM)/** continua **/
			            .append("SELECT REFERENCIA, CVE_COLA, CVE_TRANSFE, CVE_MECANISMO,")/** continua **/
			            .append("CVE_MEDIO_ENT, FCH_CAPTURA, CVE_INTERME_ORD, CVE_INTERME_REC,")/** continua **/
			            .append("IMPORTE_ABONO, ").append(ESTATUS).append(", COMENTARIO3, REFERENCIA_MECA, CONTADOR.CONT ") /** continua **/
			            .append(FROM)/** continua **/
			                .append("SELECT COUNT(1) CONT FROM TRAN_MENSAJE TM ")/** continua **/
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")/** continua **/
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)/** continua **/
			        .append(") CONTADOR, TRAN_MENSAJE ")/** continua **/
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")/** continua **/
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)/** continua **/
			      .append(") TM ")/** continua **/
			     .append(")TM ")/** continua **/
			    .append("WHERE RN BETWEEN ? AND ? ");/** termina **/
	
	/** Querie para consulta de ordenes para la sumatoria. */
	public static final StringBuilder CONSULTA_ORDENES_SUMATORIA = 
			new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
			            .append(FROM)
			                .append("SELECT COUNT(1) CONT ")
			            .append("FROM TRAN_MENSAJE TM ")
			            .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
		            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
			        .append(") CONTADOR, TRAN_MENSAJE ")
			        .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ")
			        .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP);
			     
	
	

	/** Consulta del detalle de pago. */
	public static final StringBuilder CONSULTA_DETALLE_PAGO =
			 new StringBuilder().append("SELECT TM.REFERENCIA, TM.ESTATUS, TM.CVE_EMPRESA, TM.CVE_MEDIO_ENT, TM.CVE_USUARIO_CAP, ")
			 					.append("TM.REFERENCIA_CON, TM.CVE_TRANSFE, TM.CVE_PTO_VTA, TM.REFERENCIA_MED, TM.REFERENCIA_MECA, ")
			 					.append("TM.FORMA_LIQ, TM.CVE_COLA, TM.CVE_OPERACION, TM.CVE_USUARIO_SOL, TM.CVE_INTERME_ORD, TM.CVE_PTO_VTA_ORD, ")
			 					.append("TM.NUM_CUENTA_ORD, TM.CVE_DIVISA_ORD, TM.IMPORTE_CARGO, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")
			 					.append("TM.CVE_PTO_VTA_REC,  TM.CVE_DIVISA_REC, TM.NUM_CUENTA_REC, TM.CVE_SWIFT_COR, TM.COMENTARIO1, TM.COMENTARIO2, ")
			 					.append("TM.COMENTARIO3, TM.SUCURSAL_BCO_REC, TM.CVE_DEVOLUCION, TMS.MOTIVO_DEVOL, TMS.FCH_INSTRUC_PAGO, TMS.HORA_INSTRUC_PAGO, TMS.REFE_NUMERICA, ")
			 					.append("CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TMS.FCH_CAPTURA, TMS.CONCEPTO_PAGO, TMS.CVE_RASTREO, TMS.DIRECCION_IP, ")
			 					.append("TMS.TIPO_CUENTA_ORD, TMS.COD_POSTAL_ORD, TMS.FCH_CONSTIT_ORD, TMS.RFC_ORD, TMS.NOMBRE_ORD, ")
			 					.append("TMS.DOMICILIO_ORD, TMS.TIPO_CUENTA_REC, TMS.RFC_REC, TMS.NOMBRE_REC, TMS.CONCEPTO_PAGO ")
			 					.append("FROM TRAN_MENSAJE TM LEFT JOIN TRAN_MENSAJE_SPID TMS ON TM.REFERENCIA = TMS.REFERENCIA ")
			 					.append("WHERE TM.REFERENCIA = ? ");
	
	/** Consulta del detalle de pago. */
	public static final StringBuilder CONSULTA_DETALLE_PAGO_SPEI =
			 new StringBuilder().append("SELECT TM.REFERENCIA, TM.ESTATUS, TM.CVE_EMPRESA, TM.CVE_MEDIO_ENT, TM.CVE_USUARIO_CAP,TM.REFERENCIA_CON, TM.CVE_TRANSFE,")
			 					.append("TM.CVE_PTO_VTA, TM.REFERENCIA_MED, TM.REFERENCIA_MECA,TM.FORMA_LIQ, TM.CVE_COLA, TM.CVE_OPERACION, TM.CVE_USUARIO_SOL,")
			 					.append("TM.CVE_INTERME_ORD, TM.CVE_PTO_VTA_ORD,TM.NUM_CUENTA_ORD, TM.CVE_DIVISA_ORD, TM.IMPORTE_CARGO, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ")
			 					.append("TM.CVE_PTO_VTA_REC,  TM.CVE_DIVISA_REC, TM.NUM_CUENTA_REC, TM.CVE_SWIFT_COR, TM.COMENTARIO1, TM.COMENTARIO2,TM.COMENTARIO3,")
			 					.append("TM.SUCURSAL_BCO_REC, TM.CVE_DEVOLUCION,TMC.REFE_NUMERICA,TO_CHAR(TM.FCH_CAPTURA, 'DD/MM/YYYY HH24:MI:SS')FCH_CAPTURA,TM.NOMBRE_ORD,TM.NOMBRE_REC,TM.CIUDAD_BCO_REC CVE_RASTREO,")
			 					.append("CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE,")
			 					.append("CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO ")
			 					.append(",TMC.BUC_CLIENTE,TMC.TIPO_ID_ORD,TMC.ID_ORD,TMC.TIPO_ID_REC,TMC.ID_REC,TMC.TIPO_ID_REC2,TMC.IMPORTE_IVA,TMC.NUM_CUENTA_CLABE,")
			 					.append("TMC.APLICATIVO,TMC.DIR_IP_GEN_TRAN,TMC.DIR_IP_FIRMA,TMC.TIMESTAMP,TMC.CANAL_FIRMA,TMC.UETR_SWIFT,TMC.CAMPO_SWIFT1,")
			 					.append("TMC.CAMPO_SWIFT2,TMC.REFE_ADICIONAL1,TO_CHAR(TM.FCH_APLICACION, 'DD/MM/YYYY HH24:MI:SS') FCH_APLICACION, NBM.BUFFER ")							
			 					.append("FROM TRAN_MENSAJE TM LEFT JOIN tran_mensaje_comp TMC ON TO_NUMBER(TM.REFERENCIA) = TO_NUMBER(TMC.REFERENCIA) LEFT JOIN NUCL_BITA_MOV NBM ON TO_NUMBER(TM.REFERENCIA) = TO_NUMBER(NBM.REFERENCIA) ")
			 					.append("WHERE TM.REFERENCIA = ? ");
	
	/** ********************************************************Monitor ****************************************************************. */
	/**
	 * Propiedad del tipo String que almacena el valor de UNION
	 */
	public static final String UNION = " UNION "; 
	
	/** Propiedad del tipo String que almacena el valor de SALDO. */
	public static final String SALDO = "SALDO";
	
	/** Propiedad del tipo String que almacena el valor de VOLUMEN. */
	public static final String VOLUMEN = "VOLUMEN";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_MONITOR. */
	public static final String QUERY_MONITOR = "SELECT 0 NUM, 'Saldo Inicial' LABEL, SALDO_INICIAL SALDO,0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ? "+
	 UNION +
	" SELECT 1 NUM,'TRASPASO SIAC-SPID' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN  FROM TRAN_TRSIAC_SPID  WHERE CVE_MI_INSTITUC = ?  AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	UNION +
	" SELECT 2 NUM, 'Ord. Recibidas por Aplic.' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC  WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ((ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ?) OR (ESTATUS_TRANSFER=? AND ESTATUS_BANXICO=? AND MOTIVO_DEVOL IS NULL)) "+
	UNION +
	" SELECT 3 NUM, 'Ord. Recibidas Aplicadas.' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ? "+
	UNION +
	" SELECT 4 NUM, 'Ord. Recibidas Rechazadas' LABEL, SUM(MONTO) SALDO, COUNT(*) VOLUMEN FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?,'DD-MM-YYYY') "+
	" AND ESTATUS_TRANSFER  = ? AND ESTATUS_BANXICO  = ? AND MOTIVO_DEVOL IS NOT NULL "+
	UNION +
	" SELECT 5 NUM, 'Ord. Recibidas por Dev' LABEL,SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS IN(?,?,?,?) "+
	UNION +
	" SELECT 6 NUM, 'Traspaso SPID-SIAC' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 7 NUM, 'Ord. Enviadas Conf.' LABEL,SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 8 NUM, 'Saldo No Reservado' LABEL, SALDO, 0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ? "+
	UNION +
	" SELECT 9 NUM, 'Saldo Reservado' LABEL, SALDO_RESERVADO SALDO, 0 VOLUMEN FROM TRAN_SPID_SALDO WHERE CVE_MI_INSTITUC = ?" +
	UNION +
	" SELECT 10 NUM, 'Devoluciones Envi. Conf' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_DEVOL FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS = ?"+
	UNION +
	" SELECT 11 NUM, 'Tras. SPID-SIAC en Espera' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) "+
	" AND ESTATUS IN(?,?) "+
	UNION +
	" SELECT 12 NUM, 'Tras. SPID-SIAC Enviados' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS = ? "+
	UNION +
	" SELECT 13 NUM, 'Tras. SPID-SIAC por Reparar' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_TRASP FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ? ) AND ESTATUS = ?"+
	UNION +
	" SELECT 14 NUM, 'Ord. en Espera' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) AND ESTATUS IN(?,?) "+
	UNION +
	" SELECT 15 NUM, 'Ord. Enviadas' LABEL, SUM(IMPORTE_ABONO) SALDO, COUNT(*) VOLUMEN FROM TRAN_MENSAJE "+ 
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?)  AND ESTATUS = ? "+
	UNION +
	" SELECT 16 NUM, 'Ord. por Reparar' LABEL, sum(importe_abono) SALDO, count(*) VOLUMEN from tran_mensaje "+
	" WHERE CVE_MECANISMO = (SELECT CVE_MECA_SPID FROM TRAN_SPID_PARAM WHERE CVE_MI_INSTITUC = ?) and estatus = ?";
	
	/** ********************************************************utilerias ****************************************************************. */
	
	public static final String STATUS_TRANS_BANX =" ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ";
	
	/** La constante STATUS_TRANS_BANXTR. */
	public static final String STATUS_TRANS_BANXTR =" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ";
	
	/** La constante STATUS_TRANS_BANXRE. */
	public static final String STATUS_TRANS_BANXRE =" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ";
	
	/** ***********************************************************BANCOS *******************************************************************. */
	/** Constante privada del tipo StringBuilder que almacena el query para 
	 * obtener los saldos*/
	public static final StringBuilder CONSULTA_SALDO_BANCO =
		new StringBuilder().append("SELECT TMPTBL.* FROM (SELECT TBL.*,ROWNUM R,C.* FROM ") /** continua **/
						   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),") /** continua **/
						   .append("SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ") /** continua **/
						   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS = 'CO'") /** continua **/
						   .append("GROUP BY CVE_INTERME_REC UNION ") /** continua **/
						   .append("SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ") /** continua **/
						   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = '[ENTIDAD]') AND ESTATUS_BANXICO='LQ' ") /** continua **/
						   .append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ") /** continua **/
						   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME, CIF.NOMBRE_CORTO [ORDENAMIENTO]) TBL, ") /** continua **/
						   .append("(SELECT COUNT(*) CONT FROM (SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),") /** continua **/
						   .append("SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ") /** continua **/
						   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS='CO' GROUP BY CVE_INTERME_REC ") /** continua **/
						   .append("UNION SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ") /** continua **/
						   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = '[ENTIDAD]') AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ") /** continua **/
						   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME,CIF.NOMBRE_CORTO) )C ) TMPTBL WHERE R BETWEEN ? AND ?"); /** termina **/
	
	/**  Constante privada del tipo StringBuilder que almacena el query para  obtener los saldos. */
	public static final StringBuilder CONSULTA_SALDO_BANCO_MONTOS =
			new StringBuilder().append("SELECT sum(saldo) as MONTO_TOTAL FROM ")  /** continua **/
			   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),") /** continua **/
			   .append("SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ") /** continua **/
			   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ") /** continua **/
			   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS = 'CO'") /** continua **/
			   .append("GROUP BY CVE_INTERME_REC UNION ") /** continua **/
			   .append("SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ") /** continua **/
			   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = '[ENTIDAD]') AND ESTATUS_BANXICO='LQ' ") /** continua **/
			   .append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ") /** continua **/
			   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME, CIF.NOMBRE_CORTO [ORDENAMIENTO]) TBL, ") /** continua **/
			   .append("(SELECT COUNT(*) CONT FROM ") /** continua **/
			   .append("(SELECT INTERME,CIF.NOMBRE_CORTO,SUM(VOLUMEN_ENV),SUM(IMPORTE_ENV),SUM(VOLUMEN_REC),SUM(IMPORTE_REC),SUM(IMPORTE_REC) - SUM(IMPORTE_ENV) SALDO ") /** continua **/
			   .append("FROM (SELECT CVE_INTERME_REC INTERME,COUNT(*) VOLUMEN_ENV,SUM(IMPORTE_ABONO) IMPORTE_ENV,0 VOLUMEN_REC,0.0 IMPORTE_REC ") /** continua **/
			   .append("FROM TRAN_MENSAJE WHERE CVE_MECANISMO IN ('SPID','DEVSPID') AND ESTATUS='CO' GROUP BY CVE_INTERME_REC ") /** continua **/
			   .append("UNION SELECT CVE_INTERME_ORD INTERME,0 VOLUMEN_ENV,0.0 IMPORTE_ENV,COUNT(*) VOLUMEN_REC,SUM(MONTO) IMPORTE_REC ") /** continua **/ 
			   .append("FROM TRAN_SPID_REC WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = '[ENTIDAD]') AND ESTATUS_BANXICO='LQ' AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') GROUP BY CVE_INTERME_ORD) TM ") /** continua **/
			   .append("JOIN COMU_INTERME_FIN CIF ON CIF.CVE_INTERME = TM.INTERME GROUP BY INTERME,CIF.NOMBRE_CORTO) )C "); /** termina **/
						   					
	/** Consulta para buscar todos los saldos, se usa para exportar todo a archivo. */	
	public static final StringBuilder TOTAL_CONSULTA_SALDO_BANCO =
			new StringBuilder().append("SELECT INTERME || ',' || CIF.NOMBRE_CORTO || ',' || SUM(VOLUMEN_ENV) || ',' || SUM(IMPORTE_ENV) || ',' || ") /** continua **/
			   .append("SUM(VOLUMEN_REC) || ',' || SUM(IMPORTE_REC) || ',' || (SUM(IMPORTE_REC) - SUM(IMPORTE_ENV)) ") /** continua **/
			   .append("FROM (SELECT CVE_INTERME_REC INTERME, COUNT(*) VOLUMEN_ENV, ") /** continua **/
			   		.append("SUM(IMPORTE_ABONO) IMPORTE_ENV, 0 VOLUMEN_REC, 0.0 IMPORTE_REC ") /** continua **/
			   		.append("FROM TRAN_MENSAJE ") /** continua **/
			   			.append("WHERE CVE_MECANISMO IN ('SPID','DEVSPID') ") /** continua **/
			   			.append("AND ESTATUS = 'CO' ") /** continua **/
			   			.append("GROUP BY CVE_INTERME_REC ") /** continua **/
			   			.append("UNION ") /** continua **/
			   				.append("SELECT CVE_INTERME_ORD INTERME, 0 VOLUMEN_ENV, ") /** continua **/
			   				.append("0.0 IMPORTE_ENV, COUNT(*) VOLUMEN_REC, SUM(MONTO) IMPORTE_REC ") /** continua **/
			   				.append("FROM TRAN_SPID_REC ") /** continua **/
			   					.append("WHERE CVE_MI_INSTITUC = (SELECT TCU.CV_VALOR FROM TRAN_CONTIG_UNIX TCU WHERE TCU.CV_PARAMETRO = '[ENTIDAD]') ") /** continua **/
			   					.append("AND ESTATUS_BANXICO = 'LQ' ") /** continua **/
			   					.append("AND ESTATUS_TRANSFER IN ('RE','PA','TR','DV') ") /** continua **/ 
			   					.append("GROUP BY CVE_INTERME_ORD ")/** continua **/
			   					.append(") TM ") /** continua **/
			   					.append("JOIN COMU_INTERME_FIN CIF on CIF.CVE_INTERME = TM.INTERME ") /** continua **/
			   					.append("GROUP BY INTERME, CIF.NOMBRE_CORTO"); /** termina **/
	
	
	
	/**
	 * Nueva instancia constantes monitor.
	 */
	private ConstantesMonitor() {
		super();
	}
	
}
