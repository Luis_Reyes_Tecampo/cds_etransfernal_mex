/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesPOACOA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     12/08/2019 01:07:48 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

/**
 * Class ConstantesPOACOA.
 *
 * Clase que contiene constantes utilizadas por los
 * monitores en sus implementaciones BO y DAO.
 * 
 * @author FSW-Vector
 * @since 12/08/2019
 */
public class ConstantesPOACOA implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1629634338654316084L;

	/** La constante POA. */
	public static final String POA = "POA";
	
	/** La constante COA. */
	public static final String COA = "COA";
	
	/** La constante PAN_SPEI. */
	public static final String PAN_SPEI = "SPEI";
	
	/** La constante PAN_SID. */
	public static final String PAN_SPID = "SPID";
	
	/** La constante MOD_SPEI. */
	public static final String MOD_SPEI = "POACOASPEI";
	
	/** La constante MOD_SPID. */
	public static final String MOD_SPID = "POACOASPID";
	
	/** La constante MONITOR. */
	public static final String MONITOR = "MONITOR_DE_CONTINGENCIA_";
}
