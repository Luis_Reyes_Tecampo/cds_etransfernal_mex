/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasParametros.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   16/01/2019 09:43:04 AM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.servicio.utilerias.BOPistaAuditora;


/**
 * Class UtileriasParametros.
 *
 * Clase utilerias para albergar los metodos 
 * llamados durante el flujo de negocio,
 * 
 * @author FSW-Vector
 * @since 16/01/2019
 */
public class UtileriasParametros implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1144419839662422654L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasParametros instancia;

	/** Constenate uno. */
	private static final String UNO = "1";
	
	/** La constante NOMBRE_TABLA. */
	private static final String NOMBRE_TABLA = "TRAN_POA_COA_CTRL";
	
	/** La constante DATO_FIJO. */
	private static final String DATO_FIJO = "FCH_OPERACION, CVE_MI_INSTITUC";
	
	/** La constante OK. */
	private static final String OK = "OK";

	/** La constante NOK. */
	private static final String NOK = "NOK";
	
	/** Constante nombre de tabla para Bitacora */
	private static final String TABLA_BITA = "TRAN_POA_COA_CTRL";
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasParametros getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasParametros();
		}
		return instancia;
	}

	/**
	 * Ver estatus.
	 *
	 * @param beanResPOACOA the bean res POACOA
	 * @return true, if successful
	 */
	public boolean verEstatus(BeanResPOACOA beanResPOACOA) {
		boolean estado = false;
		if(beanResPOACOA.getActivacion() == null || beanResPOACOA.getActivacion().isEmpty()) {
			estado = true;
		}
		return estado;
	}
	
	/**
	 * Ver estatusUno.
	 *
	 * @param beanResPOACOA the bean res POACOA
	 * @param esActivacion the es activacion
	 * @return true, if successful
	 */
	public boolean verEstatusUno(BeanResPOACOA beanResPOACOA,boolean esActivacion) {
		boolean estado = false;
		if(!esActivacion && UNO.equals(beanResPOACOA.getNoHayRetorno())) {
			estado = true;
		}
		return estado;
	}
	
	/**
	 * Generar pista auditora.
	 *
	 * @param boPistaAuditora the bo pista auditora
	 * @param operacion the operacion
	 * @param urlController the url controller
	 * @param resOp the res op
	 * @param sesion the sesion
	 * @param datoModificado the dato modificado
	 * @param modulo the modulo
	 */
	public void generarPistaAuditora(BOPistaAuditora boPistaAuditora, String operacion, String urlController, boolean resOp, ArchitechSessionBean sesion, String datoModificado, String modulo) {
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		String tablaActual = NOMBRE_TABLA;
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			tablaActual = tablaActual.replaceAll(TABLA_BITA, "TRAN_POA_COA_SPID_CTRL");
		}
		/** Se asignan los valores de entrada**/
		beanPista.setNomTabla(tablaActual);
		beanPista.setOperacion(operacion);
		beanPista.setUrlController(urlController);
		beanPista.setDatoFijo(DATO_FIJO);
		beanPista.setDatoModi(datoModificado);
		/** Se valida el resultado de la operacion**/
		beanPista.setEstatus( getEstatus(resOp) );
		
		/** Se invoca a la clase que realiza el flujo **/
		boPistaAuditora.llenaPistaAuditora(beanPista, sesion);
	}
	
	/**
	 * Gets the estatus.
	 *
	 * @param resOp the res op
	 * @return the estatus
	 */
	private String getEstatus(boolean resOp) {
		String estatus = NOK;
		if (resOp) {
			estatus = OK;
		}
		return estatus;
	}
}
