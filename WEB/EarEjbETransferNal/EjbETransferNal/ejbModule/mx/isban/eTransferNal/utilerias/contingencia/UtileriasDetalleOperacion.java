/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasDetalleOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/10/2018 01:52:15 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.contingencia;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * Class UtileriasDetalleOperacion.
 * 
 *  Clase que contiene metodos de utilerias que ayudan al negocio con el manejo de datos 
 * que pasan por dicha capa
 * @author FSW-Vector
 * @since 11/10/2018
 */
public class UtileriasDetalleOperacion extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8849870966609698168L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasDetalleOperacion instancia;
	
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT NOMBRE_ARCH || ',' || SECUENCIA || ',' || TIPO_CUENTA_ORD || ',' || TIPO_CUENTA_REC || ',' ||"
			+ " TIPO_CUENTA_REC2 || ',' || CVE_EMPRESA || ',' || CVE_TRANSFE || ',' || CVE_MEDIO_ENT || ',' || CVE_PTO_VTA || ',' || CVE_PTO_VTA_ORD || ',' ||"
			+ " CVE_DIVISA || ',' || CVE_INTERME_ORD || ',' || CVE_INTERME_REC || ',' || CVE_USUARIO_CAP || ',' || CVE_USUARIO_SOL || ',' || CVE_OPERACION ||"
			+ " ',' || NUM_CUENTA_ORD || ',' || NUM_CUENTA_REC || ',' || NUM_CUENTA_REC2 || ',' || REFERENCIA_MED || ',' || IMPORTE || ',' || NOMBRE_ORD || ',' ||"
			+ " NOMBRE_REC || ',' || NOMBRE_REC2 || ',' || RFC_REC2 || ',' || CONCEPTO_PAGO2 || ',' || COMENTARIO1 || ',' || COMENTARIO2 || ',' || COMENTARIO3 ||"
			+ " ',' || COMENTARIO4 || ',' || FCH_CAP_TRANSFER || ',' || REFE_TRANSFER || ',' || COD_ERROR || ',' || ESTATUS_INICIAL || ',' || MENSAJE || ',' ||"
			+ " DIRECCION_IP || ',' || BUC_CLIENTE || ',' || RFC_ORD || ',' || RFC_REC || ',' || IMPORTE_IVA || ',' || REFE_NUMERICA || ',' || UETR_SWIFT || ',' ||"
			+ " CAMPO_SWIFT1 || ',' || CAMPO_SWIFT2 || ',' || REFE_ADICIONAL1"
			+ " FROM TRAN_MENSAJE_CANALES [FILTRO_WH] [FILTRO_ARCHIVO]"; 
	
	public static final String ORDEN = " ORDER BY NOMBRE_ARCH ASC";
	
	public static final String COLUMNAS_REPORTE = "Nombre del archivo, Secuencia de numero de registros por archivo, Tipo cuenta ordenante, Tipo cuenta receptora,"
			+ " Tipo de cuenta receptora 2, Clave de la empresa, Clave de transferencia, Clave del medio de entrega, Clave de punto de venta , Clave de punto de venta ordenante,"
			+ " Clave divisa, Clave del intermediario ordenante, Clave del intermediario receptor, Clave de usuario capturista, Clave de usuario solicitante, Clave de operacion,"
			+ " Numero de cuenta ordenante, Numero de cuenta receptora, Numero de cuenta receptora 2, Referencia del medio de entrega, Importe, Nombre del ordenante, Nombre del receptor,"
			+ " Nombre receptor 2, RFC receptor 2, Concepto de pago 2, Comentario 1, Comentario 2, Comentario 3, Comentario 4, Fecha de captura, Referencia de transferencia,"
			+ " Codigo de error, Estatus inicial, Mensaje, Direccion IP, Buc de Cliente, RFC Ordenante, RFC Receptor, Importe Iva, Referencia Numerica, UetrSwift, Swift1, Swift2,"
			+ " Referencia Adicional";
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasDetalleOperacion getInstancia() {
		if(instancia == null) {
			instancia =  new UtileriasDetalleOperacion();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltro(String field, String valor, String key) {
		if (field != null && !field.isEmpty()) {
			return new StringBuilder(key+" UPPER("+field.toUpperCase()+") LIKE ('%").append(valor).append("%')").toString();
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * Valida clave.
	 *
	 * @param campo El objeto: campo
	 * @return Objeto string
	 */
	public String validaClave(String campo) {
		String clave = null;
		if(campo == null || "".equals(campo)) {
			clave = "WHERE";
		} else {
			clave = "AND";
		}
		return clave;
	}
	
	/**
	 * Obtener el objeto: error.
	 *
	 * @param responseDAO El objeto: response DAO
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		//Si  responseDAO es null, se envia el codigo de error por defecto
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		//Regresa el codigo y mensaje del DAO
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	
	/**
	 * Agrega parametros.
	 *
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto list
	 */
	public List<Object> agregaParametros(BeanPaginador beanPaginador) {
		/** Crea una lista y asigna los valores del objeto beanPaginador **/
		List<Object> params = new ArrayList<Object>();
		params.add(beanPaginador.getRegIni());
		params.add(beanPaginador.getRegFin());
		/** Retorno del metodo **/
		return params;
	}
	
}
