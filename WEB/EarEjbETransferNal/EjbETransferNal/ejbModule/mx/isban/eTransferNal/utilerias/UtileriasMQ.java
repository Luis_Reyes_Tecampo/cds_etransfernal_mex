/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: UtileriasMQ.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      					Company 	Description
 * ------- 	----------------------	--------------------------	-----------	----------------------
 * 1.0.0	04/04/2018 12:00:00 AM  Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */

package mx.isban.eTransferNal.utilerias;
//Imports java
import java.util.Arrays;
//Imports agave
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LoggingBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.ConfigFactory;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.genericmq.dto.RequestMessageGenericMQDTO;
import mx.isban.agave.dataaccess.channels.genericmq.dto.ResponseMessageGenericMQDTO;
import mx.isban.agave.dataaccess.factories.jmsgeneric.ConfigFactoryJMSGeneric;
//Imports etransfernal
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
//Imports modulo metrics
import mx.isban.metrics.dto.DTOMetricsE;
import mx.isban.metrics.util.EnumMetricsErrors;

/**
 * Clase utileria para ejecucion de MQ
 * 
 * @author ING. Juan Manuel Fuentes Ramos. CSA
 * Plan Delta Sucursales. 2018 - 06
 * Implementacion monitoreo metrics.
 */
public class UtileriasMQ extends Architech {
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 7397739424226835974L;
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasMQ util = new UtileriasMQ();
	
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** bean para errores*/
	private final DTOMetricsE metricsE = new DTOMetricsE();
	// END [ADD: JMFR CSA Monitoreo Metrics]
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasMQ getInstance(){
		return util;// Retorna una instancia de la clase UtileriasMQ
	}
	
    /**
	* Ejecuta la transaccion
	* 
	* @author ING. Juan Manuel Fuentes Ramos. CSA
	* Plan Delta Sucursales. 2018 - 06
	* Se modifica funcion para monitoreo metrics para errores criticos.
	* 
	* @param msg con la trama a enviar
	* @param codOperacion String con el codigo de operacion
	* @param idCanal String con el Id del Canal
    * @param sessionBean el bean de  session.
    * @return ResBeanEjecTranDAO objeto bean con la cadena de respuesta
	**/
	public ResBeanEjecTranDAO ejecutaTransaccion(final String msg,final String codOperacion,
			final String idCanal,final ArchitechSessionBean sessionBean){
		// Se crea objeto de respuesta 
		ResBeanEjecTranDAO resBeanEjecTranDAO = new ResBeanEjecTranDAO();
		// Se crea objeto para peticion via MQ
		RequestMessageGenericMQDTO requestDTO = new RequestMessageGenericMQDTO();
		// Se crea objeto para respuesta MQ
        ResponseMessageGenericMQDTO responseDTO = null;
        DataAccess ldtaDataase = null;// Objeto para ejecucion de transaccion
        
        metricsE.setError(false);// Se indica que no a sucedido ningun error
        
	    try {
	    	info("ejecutaTransaccion"+codOperacion+":"+msg);
	    	// Se asginan parametros de peticion
	    	requestDTO.setTypeOperation(ConfigFactoryJMSGeneric.OPERATION_TYPE_SEND_AND_RECEIVE_MESSAGE);
		    requestDTO.setCodeOperation(codOperacion);
			requestDTO.setMessage(msg);
			ldtaDataase = DataAccess.getInstance(requestDTO, new LoggingBean());
			// Se ejecuta transaccion
			responseDTO = (ResponseMessageGenericMQDTO)ldtaDataase.execute(idCanal);
			// Se valida si la operacion fue exitosa
			if (responseDTO.getCodeError().equals(ConfigFactory.CODE_SUCCESFULLY)) {
			   info("Respuesta ejecutaTransaccion"+codOperacion+":"+responseDTO.getResponseMessage());
			   resBeanEjecTranDAO.setTramaRespuesta(responseDTO.getResponseMessage());
			   resBeanEjecTranDAO.setCodError(ResBeanEjecTranDAO.COD_EXITO);
			}else{// En caso contrario se asignan codigos de error
			   resBeanEjecTranDAO.setCodError(ResBeanEjecTranDAO.COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS);
			   resBeanEjecTranDAO.setMsgError(ResBeanEjecTranDAO.DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS);
			   resBeanEjecTranDAO.setTipoError(Errores.TIPO_MSJ_ERROR);
			   metricsE.setError(true);//Se activa bandera de error
			   metricsE.setCodeError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError());//Se coloca codigo de error metrics
			   metricsE.setMsgError(EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje() +
					   UtileriasMQ.class.getCanonicalName()+ " " + responseDTO.getCodeError());//Se informa clase y codigo de error
			   metricsE.setTraza(ResBeanEjecTranDAO.DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS 
					   + " " + responseDTO.getMessageError());//Se coloca traza del error
			}
	    } catch (ExceptionDataAccess e) {
	    	showException(e);
	    	// Se obtienen datos de la excepcion para monitoreo metrics
	    	metricsE.setError(true);//Se activa bandera de error
	    	metricsE.setCodeError(EnumMetricsErrors.DELTA_INFRA_DAO.getCodeError());//Se coloca codigo de error metrics
	    	metricsE.setMsgError(EnumMetricsErrors.DELTA_INFRA_DAO.getMensaje() + 
					UtileriasMQ.class.getCanonicalName() + " " +
					ResBeanEjecTranDAO.COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS);//Se informa clase y codigo de error
	    	metricsE.setTraza(ResBeanEjecTranDAO.DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS + " " +
	    			e.getMessage() + " " + Arrays.toString(e.getStackTrace()));//Se coloca traza del error
	    	resBeanEjecTranDAO.setCodError(ResBeanEjecTranDAO.COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS);
			resBeanEjecTranDAO.setMsgError(ResBeanEjecTranDAO.DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS);
			resBeanEjecTranDAO.setTipoError(Errores.TIPO_MSJ_ERROR);
		} 
		// Se retorna respuesta del servicio
		return resBeanEjecTranDAO;
	}
	
    /**
	* Ejecuta la transaccion
	* @param msg con la trama a enviar
	* @param codOperacion String con el codigo de operacion
	* @param idCanal String con el Id del Canal
    * @return ResBeanEjecTranDAO objeto bean con la cadena de respuesta
	**/
	public ResBeanEjecTranDAO ejecutaTransaccion(final String msg,final String codOperacion,
			final String idCanal){
		return ejecutaTransaccion(msg,codOperacion,idCanal,null);
	}

	/**
	 * Obtiene datos del error
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
	 * Plan Delta Sucursales. 2018 - 06
	 * 
	 * @return metricsE bean con datos del error.
	 */
	public DTOMetricsE getMetricsE() {
		return metricsE;
	}
}
