/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMttoMediosValidar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-13-2018 10:17:18 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;

/**
 * Class UtileriasMttoMediosValidar.
 *
 * Utilerias de formato y 
 * obtencion de datos de los queries y 
 * transformacion en Beans
 * 
 * 
 * @author FSW-Vector
 * @since 08-13-2018
 */
public class UtileriasMttoMediosValidar extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2833321871040123691L;

	
	/** varaible para las utilerias *. */
	private static final UtileriasMttoMediosAut ut = UtileriasMttoMediosAut.getInstance();

	/** La variable que contiene informacion con respecto a: tipo. */
	private static final String TIPO = "tipo";

	
	
	/** The Constant CVE_DEFAULT. */
	private static final String CVE_DEFAULT = "DEFAULT";
	
	/** The instance UtileriasMttoMediosValidar */
	private static UtileriasMttoMediosValidar instance;

	/**
	 * Nueva instancia utilerias mtto medios validar.
	 */
	public UtileriasMttoMediosValidar() {
		super();
	}

	/**
	 * Metodo que sirve para obtener la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasMttoMediosValidar getInstance() {
		if( instance == null ) {
			instance = new UtileriasMttoMediosValidar();
		}
		return instance;
	}

	/**
	 * Valida campo integer.
	 *
	 * @param campo --> Parametro de entrada tipo String 
	 * @return the int --> Valor enetero convertido
	 * @throws BusinessException La business exception --> Lanzada en caso de que el valor de entrada sea incorrecto
	 */
	public int validaCampoInteger(String campo) throws BusinessException {
		if(campo.length()==0) {
			return 0;
		}
		return Integer.parseInt(campo);
	}

	/**
	 * Cortador.
	 *
	 * @param campo --> Parametro tipo String de entrada.
	 * @return Objeto string --> Parametro tipo String de salida convertido
	 */
	public String cortador(String campo) {
		String campoAux;		
		int tope = 0;
		
		if((tope = campo.indexOf('[')) ==-1 ) {
			return campo;
		}			
		campoAux = campo.substring(0, tope);
		return campoAux;
	}


	/**
	 * Valida campo decimal.
	 *
	 * @param campo El objeto tipo String  de entrada
	 * @return Objeto double de salida
	 */
	public double validaCampoDecimal(String campo) {
		
		if(campo.length()== 0){
			return 0.0;
		}
		return Double.parseDouble(campo);
		
	}

	/** 
	 * Valida lista. 
	 * Por cada uno de los tipos 
	 *
	 * 
	 * metodo que asigna la lista al bean en base al tipo d consulta ejecutada
	 *
	 * @param lista --> lista de tipo de campo de entrada
	 * @param beanMttoMediosAutorizadosRes --> Objeto de salida para ser usado como auxiliar
	 * @param map --> objeto que contiene informacion en la entrada
	 * @return the bean mtto medios autorizados res --> el objeto de salida
	 */
	public BeanMttoMediosAutorizadosRes validaLista(List<BeanCatalogo> lista,
			BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes, Map<String, String> map) {
		if (("1").equals(map.get(TIPO))) {
			beanMttoMediosAutorizadosRes.setListMediosEntrega(lista);
		}
		if (("2").equals(map.get(TIPO))) {
			beanMttoMediosAutorizadosRes.setListTransferencia(lista);
		}
		if (("3").equals(map.get(TIPO))) {
			lista = agregarValores(lista);
			beanMttoMediosAutorizadosRes.setListOperaciones(lista);
		}
		if (("4").equals(map.get(TIPO))) {
			lista = agregarValores(lista);
			beanMttoMediosAutorizadosRes.setListMecanismo(lista);
		}
		if (("5").equals(map.get(TIPO))) {
			beanMttoMediosAutorizadosRes.setListHorario(lista);
		}
		if (("6").equals(map.get(TIPO))) {
			beanMttoMediosAutorizadosRes.setListCanal(lista);
		}
		if (("7").equals(map.get(TIPO))) {
			beanMttoMediosAutorizadosRes.setListInHab(lista);
		}

		return beanMttoMediosAutorizadosRes;
	}

	
	
	/**
	 * Agregar los parametros.
	 * De cada consulta
	 *
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados req como parametro de entrada
	 * @param tipoOper --> parametro de tipo de operacion 
	 * @return Objeto list --> lista con los parametros que se agregaran a la consulta
	 */
	public List<Object> addParams(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, String tipoOper) {
		List<Object> lst = new ArrayList<Object>();
		if (ConstantesMttoMediosAut.TYPE_INSERT.equalsIgnoreCase(tipoOper)) {
			lst.add(beanMttoMediosAutorizadosReq.getVersion());
			lst.add(beanMttoMediosAutorizadosReq.getCveCLACON());
			lst.add(beanMttoMediosAutorizadosReq.getSucOperante());
			lst.add(beanMttoMediosAutorizadosReq.getHoraInicio());
			lst.add(beanMttoMediosAutorizadosReq.getHoraCierre());
			lst.add(beanMttoMediosAutorizadosReq.getHorarioEstatus());
			lst.add(beanMttoMediosAutorizadosReq.getFlgInHab());
			lst.add(beanMttoMediosAutorizadosReq.getCveCLACONTEF());
			lst.add(beanMttoMediosAutorizadosReq.getCanal());
			lst.add(!"".equals(beanMttoMediosAutorizadosReq.getLimiteImporte())?Double.parseDouble(beanMttoMediosAutorizadosReq.getLimiteImporte()):beanMttoMediosAutorizadosReq.getLimiteImporte());
			
		}

		if (ConstantesMttoMediosAut.TYPE_UPDATE.equalsIgnoreCase(tipoOper)) {
			lst.add(beanMttoMediosAutorizadosReq.getCveCLACON());
			lst.add(beanMttoMediosAutorizadosReq.getFlgInHab());
			lst.add(beanMttoMediosAutorizadosReq.getCveCLACONTEF());
			lst.add(!"".equals(beanMttoMediosAutorizadosReq.getLimiteImporte())?Double.parseDouble(beanMttoMediosAutorizadosReq.getLimiteImporte()):beanMttoMediosAutorizadosReq.getLimiteImporte());
			lst.add(beanMttoMediosAutorizadosReq.getCanal());
			lst.add(beanMttoMediosAutorizadosReq.getSucOperante());
			lst.add(beanMttoMediosAutorizadosReq.getHoraInicio());
			lst.add(beanMttoMediosAutorizadosReq.getHoraCierre());
			lst.add(beanMttoMediosAutorizadosReq.getHorarioEstatus());

		}

		/** llave primaria conformado por los siguiente campos **/
		lst.add(cortador(beanMttoMediosAutorizadosReq.getCveMedioEnt()));
		lst.add(cortador(beanMttoMediosAutorizadosReq.getCveTransfe()));
		lst.add(cortador(beanMttoMediosAutorizadosReq.getCveOperacion()));
		lst.add(cortador(beanMttoMediosAutorizadosReq.getCveMecanismo()));

		return lst;
	}

	/**
	 * Recorrer lista buscar dto.
	 *
	 * @param responseDTO --> Parametro como resultado de la operacion
	 * @return Objeto list --> Lista de BeanMttoMediosAutorizadosReq con los valores seteados 
	 */
	public List<BeanMttoMediosAutorizadosReq> recorrerListaBuscarDto(ResponseMessageDataBaseDTO responseDTO) {
		List<HashMap<String, Object>> list = null;
		List<BeanMttoMediosAutorizadosReq> listaMtto = new ArrayList<BeanMttoMediosAutorizadosReq>();
		if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
			list = responseDTO.getResultQuery();
			if (!list.isEmpty()) {
				listaMtto = new ArrayList<BeanMttoMediosAutorizadosReq>();
				for (Map<String, Object> map : list) {
					BeanMttoMediosAutorizadosReq beanMttReq = new BeanMttoMediosAutorizadosReq();
					beanMttReq = ut.asignarValores(map);
					listaMtto.add(beanMttReq);

				}
			}
		}
		return listaMtto;
	}
	
	
	/**
	 * Agregar valores.
	 * 
	 * Metodo para agregar las claves faltantes a las listas
	 *
	 * @param lista --> lista de BeanCatalogo de entrada
	 * @return the list --> lista de BeanCatalogo de salida con objetos por default
	 */
	public List<BeanCatalogo> agregarValores(List<BeanCatalogo> lista){
		BeanCatalogo bean = new BeanCatalogo();
		bean.setCve(CVE_DEFAULT);
		bean.setDescripcion(CVE_DEFAULT);
		lista.add(bean);
		return lista;
	}
}
