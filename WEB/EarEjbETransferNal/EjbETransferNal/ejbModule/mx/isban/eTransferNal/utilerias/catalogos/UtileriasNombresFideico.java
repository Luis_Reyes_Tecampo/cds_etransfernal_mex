/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasNombresFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 03:56:28 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;

/**
 * Class UtileriasNombresFideico.
 *
 * Clase de utilerias que contiene metodos
 * utilizados por el flujo del catalogo
 * de excepcion de nombres de fideicomiso 
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public final class UtileriasNombresFideico implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3709960265697667178L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasNombresFideico instancia;
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT NOMBRE FROM TRAN_NOMBRE_FIDEICO [FILTRO_WH]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY NOMBRE";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "Nombre de Fideicomiso";
	/**
	 * Nueva instancia utilerias nombres fideico.
	 */
	private UtileriasNombresFideico() {
	}
	
	/**
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return instanica: Regresa el objeto instancia cuando esta es nula
	 * 
	 */
	public static UtileriasNombresFideico getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasNombresFideico();
		}
		return instancia;
	}
	
	/**
	 * obtiene el filtro para la realización de la búsqueda de registros
	 *
	 * @param beanFiltro --> dato insertado en la caja de texto cuyo valor es tomado como referencia para realizar la consulta a la BD
	 * @param key --> Id de la caja de texto en la cual se insertó el dato.
	 * @return filtro --> regresa la cadena con el valor del filtro
	 */
	public String getFiltro(BeanNombreFideico beanFiltro, String key) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getNombre() != null && !beanFiltro.getNombre().isEmpty()) {
			getFiltroXParam(filtro, key, "NOMBRE", beanFiltro.getNombre().trim());
		}
		return filtro.toString();
	}
	
	/**
	 * Obtiene los filtros por cada parámetro no null
	 *
	 * @param filtro --> variable que obtiene los datos almacenados en cada caja de texto
	 * @param key --> Id de la caja de texto de la cual se obtiene el dato.
	 * @param param --> tipo de parámetro.
	 * @param value --> valor del parámetro obtenido.
	 * 
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para agregar los parametros a la consulta
	 * que se este ejecutando.
	 *
	 * @param beanNombre --> objeto de tipo beannombrefideico que trae 20 nombres y que serán mostrados en la pantalla 
	 * @param anterior --> objeto de tipo beannombrefideico que trae el listado de los 20 nombres  de fideicomiso anteriores.
	 * @param operacion --> objeto de tipo String que trae el listado de las operaciones que el usuario puede realizar. 
	 * @return parametros --> regresa y enlista los 20 nombres de fideicomiso anteriores
	 * 
	 */
	public List<Object> agregarParametros(BeanNombreFideico beanNombre, BeanNombreFideico anterior, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.MODIFICACION)) {
			parametros.add(beanNombre.getNombre());
			parametros.add(anterior.getNombre());
		}
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.ALTA) || 
				operacion.equalsIgnoreCase(ConstantesCatalogos.BAJA) ||
				operacion.equalsIgnoreCase(ConstantesCatalogos.BUSQUEDA)) {
			parametros.add(beanNombre.getNombre());
		}
		return parametros;
	}
}
