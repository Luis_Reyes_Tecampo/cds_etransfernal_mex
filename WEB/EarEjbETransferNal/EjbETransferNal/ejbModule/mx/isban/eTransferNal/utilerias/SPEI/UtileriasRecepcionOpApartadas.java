/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpConsultas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/09/2018 06:07:44 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;

/**
 * en esta clase de utilerias se arma la consulta para buscar las operaciones apartadas.
 *
 * @author FSW-Vector
 * @since 13/09/2018
 */
public class UtileriasRecepcionOpApartadas  extends Architech {
		
		
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1624455250037942756L;

	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionOpApartadas util = new UtileriasRecepcionOpApartadas();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasRecepcionOpApartadas getInstance(){
		return util;
	}
	  
           
    /**
     * Generar consulta auto.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    public String generarConsultaAuto(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";
    	
    	/**en caso de que exita clave intermedio orden**/
    	if(bean.getBeanDetRecepOperacion().getOrdenante().getCveIntermeOrd()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getCveIntermeOrd().length()>0){
    		inicio.append(",CVE_INTERME_ORD");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getOrdenante().getCveIntermeOrd()).append("'");
    	}
    	/**en caso de que exita referencia numerica**/
    	if(bean.getBeanDetRecepOperacion().getDetalleGral().getRefeNumerica()!= null && bean.getBeanDetRecepOperacion().getDetalleGral().getRefeNumerica().length()>0){
    		inicio.append(",REFE_NUMERICA");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleGral().getRefeNumerica()).append("'");
    	}
    	/**en caso de que exita tipo cuenta rec 2**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec2()!= null && bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec2().length()>0){
    		inicio.append(",TIPO_CUENTA_REC2");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getTipoCuentaRec2()).append("'");
    	}
    	/**en caso de que exita tipo operacion**/
    	if(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoOperacion()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoOperacion().length()>0){
    		inicio.append(",TIPO_OPERACION");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoOperacion()).append("'");
    	}
    	
    	query = generarConsultaAut(bean, inicio,  values);
    	
    	return query;
    }  
       
    
    /**
     * Generar consulta cont.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsultaAut(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";    
    	/**en caso de que exita codigo de error**/
    	if(bean.getBeanDetRecepOperacion().getCodigoError()!= null && bean.getBeanDetRecepOperacion().getCodigoError().length()>0){
    		inicio.append(",CODIGO_ERROR");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getCodigoError()).append("'");
    	}
    	/**en caso de que exita clave para pago**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getCvePago()!= null && bean.getBeanDetRecepOperacion().getDetEstatus().getCvePago().length()>0){
    		inicio.append(",CVE_PARA_PAGO");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getCvePago()).append("'");
    	}    	
    	/**en caso de que exita rfc rec**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getRfcRec()!= null && bean.getBeanDetRecepOperacion().getReceptor().getRfcRec().length()>0){
    		inicio.append(",RFC_REC");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getRfcRec()).append("'");
    	}
    	/**en caso de que exitarfc rec2 **/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getRfcRec2()!= null && bean.getBeanDetRecepOperacion().getReceptor().getRfcRec2().length()>0){
    		inicio.append(",RFC_REC2");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getRfcRec2()).append("'");
    	}   
    	
    	query = generarConsultaAu(bean, inicio,  values);
    	
    	return query;
    }
    
        
    /**
     * Generar consulta cont.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsultaAu(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";   
    	/**en caso de que exita numero cuenta orden**/
    	if(bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd().length()>0){
    		inicio.append(",NUM_CUENTA_ORD");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getOrdenante().getNumCuentaOrd()).append("'");
    	}
    	/**en caso de que exita numero cuenta rec**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec()!= null && bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec().length()>0){
    		inicio.append(",NUM_CUENTA_REC");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec()).append("'");
    	}
    	/**en caso de que exita numero cuenta tran**/
    	if(bean.getBeanDetRecepOperacion().getDetalleTopo().getCuentaModif()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getCuentaModif().length()>0){
    		inicio.append(",NUM_CUENTA_TRAN");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getCuentaModif()).append("'");
    	}
    	/**en caso de que exita tipo pago**/
    	if(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago().length()>0){
    		inicio.append(",TIPO_PAGO");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getTipoPago());
    	}
    	
    	query = generarConsultaA(bean, inicio,  values);
    	
    	return query;
    }    
     
    /**
     * Generar consulta aut cont.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsultaA(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";    
    	
    	/**en caso de que exita monto**/
    	if(bean.getBeanDetRecepOperacion().getOrdenante().getMonto()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getMonto().length()>0){
    		inicio.append(",MONTO");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getOrdenante().getMonto());
    	}
    	/**en caso de que exita iva**/
    	if(bean.getBeanDetRecepOperacion().getOrdenante().getIva()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getIva().length()>0){
    		inicio.append(",IVA");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getOrdenante().getIva());
    	}
    	/**en caso de que exita clave rastreo**/
    	if(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo()!= null && bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo().length()>0){
    		inicio.append(",CVE_RASTREO");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreo()).append("'");
    	}
    	/**en caso de que exita clae rastreo original**/
    	if(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreoOri()!= null && bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreoOri().length()>0){
    		inicio.append(",CVE_RASTREO_ORI");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleGral().getCveRastreoOri()).append("'");
    	}   	
    	
    	query = generarConsultaFal(bean, inicio,  values);
    	
    	return query;
    }
    
    
    /**
     * Generar consulta fal.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    public String generarConsultaFal(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";
    	/**en caso de que exita motivo de devolucion**/
    	if(bean.getBeanDetRecepOperacion().getBeanCambios().getMotivoDevol()!= null && bean.getBeanDetRecepOperacion().getBeanCambios().getMotivoDevol().length()>0) {
    		inicio.append(",MOTIVO_DEVOL");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getBeanCambios().getMotivoDevol()).append("'");
    	}
    	
    	/**en caso de que exita rfc orden**/
    	if(bean.getBeanDetRecepOperacion().getOrdenante().getRfcOrd()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getRfcOrd().length()>0){
    		inicio.append(",RFC_ORD");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getOrdenante().getRfcOrd()).append("'");
    	}
    	/**en caso de que exita numero cuenta rec2**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec2()!= null && bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec2().length()>0){
    		inicio.append(",NUM_CUENTA_REC2");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec2()).append("'");
    	}
    	
    	/**en caso de que exita priodidad**/
    	if( bean.getBeanDetRecepOperacion().getDetalleTopo().getPrioridad()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getPrioridad().length()>0){
    		inicio.append(",PRIORIDAD");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getPrioridad());
    	}
    	
    	query = generarConsulta(bean, inicio,  values);
    	return query;
    }
    
    
   
    /**
     * Generar consulta.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsulta(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";   
    	
    	/**en caso de que exita nombre rec**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getNombreRec()!= null && bean.getBeanDetRecepOperacion().getReceptor().getNombreRec().length()>0){
    		inicio.append(",NOMBRE_REC");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getNombreRec()).append("'");
    	}
    	/**en caso de que exita nombre rec2**/
    	if(bean.getBeanDetRecepOperacion().getReceptor().getNombreRec2()!= null && bean.getBeanDetRecepOperacion().getReceptor().getNombreRec2().length()>0){
    		inicio.append(",NOMBRE_REC2");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getReceptor().getNombreRec2()).append("'");
    	}
    	/**en caso de que exita concepto de pago**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago2()!= null && bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago2().length()>0){
    		inicio.append(",CONCEPTO_PAGO2");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago2()).append("'");
    	}
    	/**en caso de que exita referecina cobranza**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getRefeCobranza()!= null && bean.getBeanDetRecepOperacion().getDetEstatus().getRefeCobranza().length()>0){
    		inicio.append(",REFE_COBRANZA");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getRefeCobranza()).append("'");
    	}
    	
    	
    	query = generarConsultaF(bean, inicio,  values);
    	
    	return query;
    }
    
    
    /**
     * Generar consulta F.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    public String generarConsultaF(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";
    	
    	/**en caso de que exita nombre orden**/    	
    	if( bean.getBeanDetRecepOperacion().getOrdenante().getNombreOrd()!= null && bean.getBeanDetRecepOperacion().getOrdenante().getNombreOrd().length()>0){
    		inicio.append(",NOMBRE_ORD");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getOrdenante().getNombreOrd()).append("'");
    	}
    	
    	/**en caso de que exita concepto pago**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago1()!= null && bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago1().length()>0) {
    		inicio.append(",CONCEPTO_PAGO1");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getConceptoPago1()).append("'");
    	}
    	
    	/**en caso de que exita referencia transfer**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getRefeTransfer()!=null  && bean.getBeanDetRecepOperacion().getDetEstatus().getRefeTransfer().length()>0){
    		inicio.append(",REFE_TRANSFER");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getDetEstatus().getRefeTransfer());
    	}
    	
		inicio.append(",FCH_CAPTURA");
		values.append(",sysdate");
    	
    	query = generarConsult(bean, inicio,  values);
    	return query;
    }
    
    /**
     * Generar consul aut cont.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsult(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";    
    	
    	/**en caso de que exita version**/
    	if(bean.getBeanDetRecepOperacion().getBeanCambios().getVersion()!= null && bean.getBeanDetRecepOperacion().getBeanCambios().getVersion().length()>0){
    		inicio.append(",U_VERSION");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getBeanCambios().getVersion()).append("'");
    	}
    	/**en caso de que exita longitud de ratreo**/
    	if(bean.getBeanDetRecepOperacion().getBeanCambios().getLongRastreo()!= null && bean.getBeanDetRecepOperacion().getBeanCambios().getLongRastreo().length()>0){
    		inicio.append(",LONG_RASTREO");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getBeanCambios().getLongRastreo());
    	}
    	/**en caso de que exita folio  paquete dev**/
    	if(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaqueteDev()!= null && bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaqueteDev().length()>0){
    		inicio.append(",FOLIO_PAQ_DEV");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPaqueteDev());
    	}
    		
    	/**en caso de que exita la estatus banxico**/
    	if(bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusBanxico()!= null && bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusBanxico().length()>0){
    		inicio.append(",ESTATUS_BANXICO");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetEstatus().getEstatusBanxico()).append("'");
    	}
    	
    	query = generarConsul(bean, inicio,  values);
    	
    	return query;
    }
    
    /**
     * Generar consul aut cont.
     *
     * @param bean El objeto: bean
     * @param inicio El objeto: inicio
     * @param values El objeto: values
     * @return Objeto string
     */
    private String generarConsul(BeanDetRecepOperacionDAO bean, StringBuilder inicio, StringBuilder values) {
    	String query="";  
    	
    	/**en caso de que exita folio pago dev**/
    	if(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPagoDev()!= null && bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPagoDev().length()>0){
    		inicio.append(",FOLIO_PAGO_DEV");
    		values.append(",").append(bean.getBeanDetRecepOperacion().getDetalleGral().getFolioPagoDev());
    	}   
    	
    	    	
    	/**en caso de que exita cde**/
    	if(bean.getBeanDetRecepOperacion().getDetalleTopo().getCde()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getCde().length()>0){
    		inicio.append(",CDE");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getCde()).append("'");
    	}
    	/**en caso de que exita cda**/
    	if(bean.getBeanDetRecepOperacion().getDetalleTopo().getCda()!= null && bean.getBeanDetRecepOperacion().getDetalleTopo().getCda().length()>0){
    		inicio.append(",CDA");
    		values.append(",'").append(bean.getBeanDetRecepOperacion().getDetalleTopo().getCda()).append("'");
    	}
    	
    	inicio.append(") "); 
    	values.append(")");
    	query = inicio.toString()+  values.toString();
    	
    	return query;
    }
    
   
    
}
