/**
 * Clase de tipo Utilerias de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Junio 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.utilerias;

import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;

/**
 * The Class UtileriasImplMantenimientoParametros.
 */
public final class UtileriasImplMantenimientoParametros {
	
	/** The Constant DESCRIPCION. */
	private static final String DESCRIPCION = ", DESCRIPCION";
	
	/** The Constant TIPO_PARAMETRO. */
	private static final String TIPO_PARAMETRO = ", TIPO_PARAMETRO";

	/** The Constant TIPO_DATO. */
	private static final String TIPO_DATO = ", TIPO_DATO";
	
	/** The Constant LONGITUD. */
	private static final String LONGITUD = ", LONGITUD";
	
	/** The Constant TRAMA. */
	private static final String TRAMA = ", DEFINE_TRAMA";
	
	/** The Constant COLA. */
	private static final String COLA = ", DEFINE_COLA";
	
	/** The Constant MECANISMO. */
	private static final String MECANISMO = ", DEFINE_MECANISMO";
	
	/** The Constant POSICION. */
	private static final String POSICION = ", POSICION";
	
	/** The Constant PARAMETRO_FALTA. */
	private static final String PARAMETRO_FALTA = ", CVE_PARAM_FALTA";
	
	/** The Constant PROGRAMA. */
	private static final String PROGRAMA = ", PROGRAMA";
	
	/** Referencia del Objeto Utilerias. */
	private static final UtileriasImplMantenimientoParametros UTILS_IMPL_MANT_PARAM_INSTACE = new UtileriasImplMantenimientoParametros();

	/**
	 * Constructor privado.
	 */
	private UtileriasImplMantenimientoParametros() {super();}
	
	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasImplMantenimientoParametros getUtilerias(){
		return UTILS_IMPL_MANT_PARAM_INSTACE;
	}

	/**
	 * Armar parametrizacion insert.
	 *
	 * @param parametros the parametros
	 * @param request the request
	 * @param queryInsertParamVal the query insert param val
	 * @return the string
	 */
	public String armarParametrizacionInsert(List<Object> parametros, BeanReqMantenimientoParametros request, String queryInsertParamVal){
		StringBuilder builderCampos = new StringBuilder();
		StringBuilder builderValores = new StringBuilder();
		String parametrizacionInsert="";
		String param = ", ?";
		String vacio = "";
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getDescripParam()) ){
			builderCampos.append(DESCRIPCION);
			builderValores.append(param);
			parametros.add(request.getDescripParam());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getTipoParametro()) ){
			builderCampos.append(TIPO_PARAMETRO);
			builderValores.append(param);
			parametros.add(request.getTipoParametro());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getTipoDato()) ){
			builderCampos.append(TIPO_DATO);
			builderValores.append(param);
			parametros.add(request.getTipoDato());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getLongitudDato()) ){
			builderCampos.append(LONGITUD);
			builderValores.append(param);
			parametros.add(request.getLongitudDato());
		}

		parametrizacionInsert=
				//metodo que genera la segunda parte de laparametrizacion de insert
				armarParametrizacionInsertPart2(builderCampos, builderValores, parametros, request, queryInsertParamVal);
		
		return parametrizacionInsert;
	}

	/**
	 * Armar parametrizacion insert.
	 *
	 * @param builderCampos the builder campos
	 * @param builderValores the builder valores
	 * @param parametros the parametros
	 * @param request the request
	 * @param queryInsertParamVal the query insert param val
	 * @return the string
	 */
	private String armarParametrizacionInsertPart2(StringBuilder builderCampos, StringBuilder builderValores,
			List<Object> parametros, BeanReqMantenimientoParametros request, String queryInsertParamVal){
		String param = ", ?";
		String vacio = "";
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getTrama()) && !"Nulo".equals(request.getTrama()) ){
			builderCampos.append(TRAMA); 
			builderValores.append(param);
			parametros.add(request.getTrama());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getCola()) && !"Nulo".equals(request.getCola()) ){
			builderCampos.append(COLA); 
			builderValores.append(param);
			parametros.add(request.getCola());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getMecanismo()) && !"Nulo".equals(request.getMecanismo()) ){
			builderCampos.append(MECANISMO);
				builderValores.append(param);
				parametros.add(request.getMecanismo());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getPosicion()) ){
			builderCampos.append(POSICION);
			builderValores.append(param);
			parametros.add(request.getPosicion());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getParametroFalta()) ){
			builderCampos.append(PARAMETRO_FALTA);
			builderValores.append(param);
			parametros.add(request.getParametroFalta());
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getPrograma()) ){
			builderCampos.append(PROGRAMA);
			builderValores.append(param);
			parametros.add(request.getPrograma());
		}
		// se concatenan builde de campos y valores
		builderCampos.append( queryInsertParamVal + builderValores.toString() + ")" );
		//se retorna la parametrizacion generada
		return builderCampos.toString();
	}
	
	/**
	 * Armar parametrizacion update.
	 *
	 * @param parametros the parametros
	 * @param request the request
	 * @return the string
	 */
	public String armarParametrizacionUpdate(List<Object> parametros, BeanReqMantenimientoParametros request){
		StringBuilder builderCampos = new StringBuilder();
		String equalsNull = " = null";
		String parametrizacion = "";
		String paramEquals = " = ?";
		String vacio = "";
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getDescripParam()) ){
			builderCampos.append(DESCRIPCION + paramEquals);
			parametros.add(request.getDescripParam());
		} else {
			builderCampos.append(DESCRIPCION + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getTipoParametro()) ){
			builderCampos.append(TIPO_PARAMETRO + paramEquals);
			parametros.add(request.getTipoParametro());
		} else {
			builderCampos.append(TIPO_PARAMETRO + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getTipoDato()) ){
			builderCampos.append(TIPO_DATO + paramEquals);
			parametros.add(request.getTipoDato());
		} else {
			builderCampos.append(TIPO_DATO + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getLongitudDato()) ){
			builderCampos.append(LONGITUD + paramEquals);
			parametros.add(request.getLongitudDato());
		} else {
			builderCampos.append(LONGITUD + equalsNull);
		}
		
		builderCampos.append(
				//metodo genera la parametrizacion de los combos define trama, cola y mecanismo
				armarParametrizacionUpdateComboDefine(parametros, request));
		
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getPosicion())){
			builderCampos.append(POSICION + paramEquals);
			parametros.add(request.getPosicion());
		} else {
			builderCampos.append(POSICION + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getParametroFalta()) ){
			builderCampos.append(PARAMETRO_FALTA + paramEquals);
			parametros.add(request.getParametroFalta());
		} else {
			builderCampos.append(PARAMETRO_FALTA + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( !vacio.equals(request.getPrograma()) ){
			builderCampos.append(PROGRAMA + paramEquals);
			parametros.add(request.getPrograma());
		} else {
			builderCampos.append(PROGRAMA + equalsNull);
		}
		parametrizacion = builderCampos.toString() ;
		return parametrizacion;
	}

	/**
	 * Armar parametrizacion update.
	 *
	 * @param parametros the parametros
	 * @param builderCampos the builder campos
	 * @param request the request
	 * @return the string
	 */
	private String armarParametrizacionUpdateComboDefine(List<Object> parametros, BeanReqMantenimientoParametros request){
		StringBuilder builderCampos=new StringBuilder();
		String equalsNull = " = null";
		String paramEquals = " = ?";
		
		//se valida que el campo no sea vacio
		if( "S".equals(request.getTrama()) || "N".equals(request.getTrama()) ){
			builderCampos.append(TRAMA + paramEquals);
			parametros.add(request.getTrama());
		} else {
			builderCampos.append(TRAMA + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( "S".equals(request.getCola()) || "N".equals(request.getCola()) ){
			builderCampos.append(COLA + paramEquals);
			parametros.add(request.getCola());
		} else {
			builderCampos.append(COLA + equalsNull);
		}
		//se valida que el campo no sea vacio
		if( "S".equals(request.getMecanismo()) || "N".equals(request.getMecanismo()) ){
			builderCampos.append(MECANISMO + paramEquals);
			parametros.add(request.getMecanismo());
		} else {
			builderCampos.append(MECANISMO + equalsNull);
		}

		return builderCampos.toString();
	}
	
	 /**
 	 * Generar parametrizacion exportar todos.
 	 *
 	 * @param request the request
 	 * @param queryConsulOrder the query consul order
 	 * @return the string
 	 */
 	public String generarParametrizacionExportarTodo(BeanReqMantenimientoParametros request, String queryConsulOrder){	
 		StringBuilder builder = new StringBuilder();
		String vacio = "";
		String equal=" = ";
		boolean isFirst=true;

		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getParametro())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(CVE_PARAMETRO)");
			builder.append(equal);
			builder.append("'"+request.getParametro()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getDescripParam())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(DESCRIPCION)");
			builder.append(equal);
			builder.append("'"+request.getDescripParam()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getTipoParametro())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(TIPO_PARAMETRO)");
			builder.append(equal);
			builder.append("'"+request.getTipoParametro()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getTipoDato())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(TIPO_DATO)");
			builder.append(equal);
			builder.append("'"+request.getTipoDato()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getLongitudDato())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(LONGITUD)");
			builder.append(equal);
			builder.append("'"+request.getLongitudDato()+"'");
		}
		//Se revisa si lleva el filtro
		if(!vacio.equals(request.getValorFalta())){
			isFirst=addAnd(builder, isFirst);
			builder.append("TRIM(VALOR_FALTA)");
			builder.append(equal);
			builder.append("'"+request.getValorFalta()+"'");
		}

		builder.append(
				//metodo que genera la segunda parte de laparametrizacion de ExportarTodo
				generarParametrizacionExportarTodoPart2(request, isFirst));

		//Se agrega el orden
		builder.append(queryConsulOrder);
		
		return builder.toString();
    }

 	/**
	  * Generar parametrizacion exportar todos part2.
	  *
	  * @param builder the builder
	  * @param request the request
	  * @param isFirst the is first
	  * @return the string
	  */
	 private String generarParametrizacionExportarTodoPart2(BeanReqMantenimientoParametros request, boolean isFirst){
		 StringBuilder builder=new StringBuilder();
		 String vacio = "";
		 String equal=" = ";
		 boolean vaPrimero=isFirst;

		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getTrama())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(DEFINE_TRAMA)");
			 builder.append(equal);
			 builder.append("'"+request.getTrama()+"'");
		 }
		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getCola())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(DEFINE_COLA)");
			 builder.append(equal);
			 builder.append("'"+request.getCola()+"'");
		 }
		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getMecanismo())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(DEFINE_MECANISMO)");
			 builder.append(equal);
			 builder.append("'"+request.getMecanismo()+"'");
		 }
		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getPosicion())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(POSICION)");
			 builder.append(equal);
			 builder.append("'"+request.getPosicion()+"'");
		 }
		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getParametroFalta())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(CVE_PARAM_FALTA)");
			 builder.append(equal);
			 builder.append("'"+request.getParametroFalta()+"'");
		 }
		 //Se revisa si lleva el filtro
		 if(!vacio.equals(request.getPrograma())){
			 vaPrimero=addAnd(builder, vaPrimero);
			 builder.append("TRIM(PROGRAMA)");
			 builder.append(equal);
			 builder.append("'"+request.getPrograma()+"'");
		 }
		
		 return builder.toString();
	 }
 	
	/**
	 * Adds the and.
	 *
	 * @param builder the builder
	 * @param isFirst the is first
	 * @return true, if successful
	 */
	public boolean addAnd(StringBuilder builder, boolean isFirst){
		boolean result = isFirst;
		if(result){
			//Cuando ya no es el primer filtro se agrega where
			builder.append(" WHERE ");
			result=false;
		}else{
			//Cuando es el primer filtro se agrega and
			builder.append(" AND ");
		}
		return result;
	}
}