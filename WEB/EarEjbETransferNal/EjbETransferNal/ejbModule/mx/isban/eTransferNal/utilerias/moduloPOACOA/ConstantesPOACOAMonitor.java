/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:02:04 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

/**
 * Class ConstantesMonitor.
 *
 * Clase que contiene constantes utilizadas por los
 * monitores en sus implementaciones BO y DAO.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class ConstantesPOACOAMonitor implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2568495271797414949L;

	/** La constante ENTIDAD. */
	public static final String ENTIDAD = "ENTIDAD";
	
	/** La constante GUION_BAJO. */
	public static final String GUION_BAJO = "_";
	
	/** La constante CONS_PV. */
	public static final String CONS_PV = "PV";
	
	/** La constante CONS_LI. */
	public static final String CONS_LI = "LI";
	
	/** La constante CONS_EN. */
	public static final String CONS_EN = "EN";
	
	/** La constante CONS_PR. */
	public static final String CONS_PR = "PR";
	
	/** La constante CONS_LQ. */
	public static final String CONS_LQ = "LQ";
	
	/** La constante CONS_RE. */
	public static final String CONS_RE = "RE";
	
	/** La constante CONS_PA. */
	public static final String CONS_PA = "PA";
	
	/** La constante CONS_CO. */
	public static final String CONS_CO = "CO";
	
	/** La constante CONS_TR. */
	public static final String CONS_TR = "TR";
	
}
