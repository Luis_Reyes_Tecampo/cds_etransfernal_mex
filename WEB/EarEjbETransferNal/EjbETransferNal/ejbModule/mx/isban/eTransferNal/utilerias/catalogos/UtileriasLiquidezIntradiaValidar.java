/**
 * 
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;

/**
 * The Class UtileriasLiquidezIntradiaValidar.
 *
 * @author vector
 */
public class UtileriasLiquidezIntradiaValidar implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8593901383459147995L;

	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA_NA = "SELECT CVE_TRANSFE as CVE, DESCRIPCION FROM TRAN_TRANSFERENC ORDER BY CVE_TRANSFE";	
	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MECANISMO_NA = "SELECT CVE_MECANISMO AS CVE, DESCRIPCION FROM TRAN_MECANISMOS ORDER BY CVE_MECANISMO";
	
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS_NA = "SELECT CVE_PARAM_FALTA AS CVE, DESCRIPCION FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%ESTATUSTRANS%' ORDER BY CVE_PARAM_FALTA";
	
	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA_TR = "SELECT CVE_TRANSFE AS CVE,TXT_DESCRIPCION AS DESCRIPCION FROM TR_CORE_CFG_CVETRANSFE";
	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MECANISMO_TR = "select CVE_MECANISMO AS CVE,TXT_DESCRIPCION AS DESCRIPCION from TR_CORE_CFG_MECANISMOS";
	
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS_TR = "SELECT CVE_ESTATUS AS CVE ,TXT_DESCRIPCION  AS DESCRIPCION from TR_CORE_CAT_ESTATUS";
	
	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA_FILTRO_NA = "SELECT DISTINCT TT.CVE_TRANSFE AS CVE, TT.DESCRIPCION AS DESCRIPCION FROM TRAN_TRANSFERENC TT, TRAN_PARAMETROS TC, TRAN_MECANISMOS TM "
			.concat(" ,TRAN_LIQ_INTRADIA CL WHERE TT.CVE_TRANSFE = CL.CVE_TRANSFE")
			.concat(" AND TC.CVE_PARAMETRO      LIKE '%ESTATUSTRANS%'")
			.concat(" AND TRIM(CL.ESTATUS)  = TRIM(TC.CVE_PARAM_FALTA)")
			.concat(" AND TRIM(CL.CVE_MECANISMO) = TRIM(TM.CVE_MECANISMO) ")
			.concat(" [FILTER] ORDER BY TT.CVE_TRANSFE ");
	
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS_FILTRO_NA = "SELECT DISTINCT TP.CVE_PARAM_FALTA AS CVE, TP.DESCRIPCION AS DESCRIPCION FROM TRAN_LIQ_INTRADIA CL,TRAN_PARAMETROS TP,TRAN_MECANISMOS TM "
			.concat(" WHERE TP.CVE_PARAMETRO LIKE '%ESTATUSTRANS%' ")
			.concat(" AND TP.CVE_PARAM_FALTA = CL.ESTATUS [FILTER] ")
			.concat(" AND TRIM(CL.CVE_MECANISMO) = TRIM(TM.CVE_MECANISMO)")
			.concat(" ORDER BY TP.CVE_PARAM_FALTA") ;
	
	
	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA_FILTRO_TR = "SELECT DISTINCT TT.CVE_TRANSFE AS CVE, TT.TXT_DESCRIPCION AS DESCRIPCION FROM TR_CORE_CFG_CVETRANSFE TT,TR_LIQ_INTRADIA CL,TR_CORE_CAT_ESTATUS TC,  TR_CORE_CFG_MECANISMOS TM "
			.concat("WHERE TT.CVE_TRANSFE = CL.CVE_TRANSFE " )
			.concat("  AND TRIM(CL.CVE_ESTATUS) = TRIM(TC.CVE_ESTATUS) ")
			.concat(" AND TRIM(CL.CVE_MECANISMO) = TRIM(TM.CVE_MECANISMO)  ")
 			.concat(" [FILTER] ORDER BY TT.CVE_TRANSFE  ");
			
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS_FILTRO_TR= "SELECT DISTINCT CN.CVE_ESTATUS AS CVE, CN.TXT_DESCRIPCION AS DESCRIPCION FROM TR_CORE_CAT_ESTATUS CN ,TR_CORE_CFG_CVETRANSFE TT, TR_CORE_CFG_MECANISMOS TM "
			.concat(", TR_LIQ_INTRADIA CL WHERE  CN.CVE_ESTATUS =CL.CVE_ESTATUS ")
			.concat("AND TT.CVE_TRANSFE = CL.CVE_TRANSFE ")
			.concat(" AND TRIM(CL.CVE_MECANISMO) = TRIM(TM.CVE_MECANISMO) ")
 			.concat(" [FILTER] ORDER BY CN.CVE_ESTATUS ");
	
	
	/** Instancia de utilerias validar. */
	private static UtileriasLiquidezIntradiaValidar utileriasValidar;
	
	/** La constante util. */
	private static final UtileriasLiquidezIntradia util =  UtileriasLiquidezIntradia.getInstancia();

	/** La constante utilAux. */
	private static final UtileriasLiquidezIntradiaAux utilAux = UtileriasLiquidezIntradiaAux.getUtils();
	
	/**
	 * Obtiene el objeto utilerias.
	 *
	 * @return  El objeto: instancia del objeto utilerias
	 */
	public static UtileriasLiquidezIntradiaValidar getUtileriasValidar() {
		if(utileriasValidar==null){
			utileriasValidar = new UtileriasLiquidezIntradiaValidar();
		 }
		return utileriasValidar;
	}

	/**
	 * Setea el objeto utilerias validar.
	 *
	 * @param utileriasValidar the new utilerias validar
	 */
	public static void setUtileriasValidar(UtileriasLiquidezIntradiaValidar utileriasValidar) {
		UtileriasLiquidezIntradiaValidar.utileriasValidar = utileriasValidar;
	}
	

	/**
	 * Evalua consulta.
	 *
	 * @param tipo consulta - Variable que contiene el valor para definir el tipo de consulta
	 * @param index de la consulta - Indice con el que se verifica la consulta
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @param beanFilter - contiene los campos para realizar el filtro
	 * @return objeto string - Valor resultante para verificar el tipo de consulta
	 */
	public String evaluaConsulta(int tipo, BeanLiquidezIntradia beanFilter,int index,String canal){
		String query = "";
		if (tipo == 1) {
			if (index == 0) {
				query = validaConsultaTrans(canal);
			} 
			if (index == 1) {
				query =  validaConsultaMecanismo(canal);
			} 
			if (index == 2) {
				query =  validaConsultaEstatus(canal);
			} 
		} else {
			query = evaluaConsultaAux(index,beanFilter,canal);
		}
		return query;
	}
	
	/**
	 * Evalua consulta aux.
	 * 
	 * Obtiene la  consulta cuando se necesitan los registros de 
	 * los filtros
	 *	
	 * @param index - Indice con el que se verifica la consulta
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return Objeto string - Valor resultante para verificar el tipo de consulta
	 */
	private String evaluaConsultaAux(int index, BeanLiquidezIntradia beanFilter,String canal) {
		String query = "";
		
		if (index == 0) {
			query = validaConsultaTransFiltro(canal);
		} 
		if (index == 1) {
			query = utilAux.validaConsultaMecanismoFiltro(canal);
		} 
		if (index == 2) {
			query =  validaConsultaEstatusFiltro(canal);
		} 
		query = validaFiltroExistente(query, beanFilter,canal);
 		return query;
	}
	
	/**
	 * Valida consulta trans.
	 *
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return  Objeto string - Valor de BD a consultar
	 */
	public String validaConsultaTrans(String canal){
		String query= "";
		if("NA".equals(canal)){
			query=CONSULTA_TRASFERENCIA_NA;
			
		}else{
			query=CONSULTA_TRASFERENCIA_TR;
		}
		return query;
	}
	
	/**
	 * Valida consulta mecanismo.
	 *
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return Objeto string - Valor de variante mecanismo
	 */
	public String validaConsultaMecanismo(String canal){
		String query= "";
		if("NA".equals(canal)){
			query=CONSULTA_MECANISMO_NA;
			
		}else{
			query=CONSULTA_MECANISMO_TR;
		}
		return query;
	}
	
	/**
	 * Valida consulta estatus.
	 *
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return Objeto string - Valor de estatus dependiendo el canal
	 */
	public String validaConsultaEstatus(String canal){
		String query= "";
		if("NA".equals(canal)){
			query=CONSULTA_ESTATUS_NA;
			
		}else{
			query=CONSULTA_ESTATUS_TR;
		}
		return query;
	}
	
	/**
	 * Valida consulta estatus filtro.
	 *
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return Objeto string - valor de filro estatus, dependiendo el canal
	 */
	public String validaConsultaEstatusFiltro(String canal){
		String query= "";
		if("NA".equals(canal)){
			query=CONSULTA_ESTATUS_FILTRO_NA;
			
		}else{
			query=CONSULTA_ESTATUS_FILTRO_TR;
		}
		return query;
	}
	
	/**
	 * Valida consulta trans filtro.
	 *
	 * @param canal - Contiene el canal al que se realiza la consulta 
	 * @return Objeto string -  valor de filro estatus, dependiendo el canal
	 */
	public String validaConsultaTransFiltro(String canal){
		String query= "";
		if("NA".equals(canal)){
			query=CONSULTA_TRASFERENCIA_FILTRO_NA;
			
		}else{
			query=CONSULTA_TRASFERENCIA_FILTRO_TR;
		}
		return query;
	}
	
	
	/**
	 * Valida filtro existente.
	 *
	 * Busca los filtros actuales y los 
	 * ingresa a los querys de los combos
	 * de filtro.
	 * 
	 * @param query El objeto: query - Query a ejecutar
	 * @param filter El objeto: filter - Objeto qeu contiene los filtros de la consulta
	 * @return Objeto string - Valor del filtro en query
	 */
	private String validaFiltroExistente (String query, BeanLiquidezIntradia filter,String canal) {
		String cadenaFiltro="";
		if(filter!=null){
			cadenaFiltro = util.getFiltro(filter, "AND", canal);
		}
		
		if (cadenaFiltro != null && !cadenaFiltro.trim().isEmpty()) {
			query = query.replaceAll("\\[FILTER\\]", cadenaFiltro);
		} else {
			query = query.replaceAll("\\[FILTER\\]", "");
		}
		return query;
	}
}
