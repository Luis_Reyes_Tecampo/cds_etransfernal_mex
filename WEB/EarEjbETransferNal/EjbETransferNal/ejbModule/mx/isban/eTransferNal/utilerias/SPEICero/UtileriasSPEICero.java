package mx.isban.eTransferNal.utilerias.SPEICero;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanConsMiInstitucion;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanListas;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanFechaHorario;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanSpeiCero;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
/**
 * 
 * @author vector
 *
 */
public class UtileriasSPEICero extends Architech {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2833321871040123691L;
	/**
	 * Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ
	 */
	private static UtileriasSPEICero util = new UtileriasSPEICero();
	/**
	 * propiedad que almacena CERO
	 */	
	private static final String CERO = "0";
	/**constante de error **/
	private static final String ERROR =" Se genero el error: ";
	
	/** constructor**/
	public UtileriasSPEICero(){
		super();
	}
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasSPEICero getInstance(){
		return util;
	}
	
	/**
	 * 
	 * @param responseDTO
	 * @param list
	 * @return
	 */
	public List<BeanSpeiCero> separarConsulta(ResponseMessageDataBaseDTO responseDTO, List<HashMap<String,Object>> list ){
		List<BeanSpeiCero>  lista = new ArrayList<BeanSpeiCero>();
		Utilerias utilerias = Utilerias.getUtilerias();		
						
		for(int i = 0; i<list.size(); i++){
			 Map<String,Object> mapResult = list.get(i);						
			 BeanSpeiCero bean = new BeanSpeiCero();				
			 bean.setReferencia(utilerias.getString(mapResult.get("REFERENCIA")));
			 bean.setTipoOperacion(utilerias.getString(mapResult.get("TIPO_OPERACION")));
			 bean.setDescripcion(utilerias.getString(mapResult.get("DESCRIPCION")));
			 bean.setCveTransferencia(utilerias.getString(mapResult.get("CVE_TRANSFE")));
			 bean.setCveOperacion(utilerias.getString(mapResult.get("CVE_OPERACION")));
			 bean.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
			 bean.setNombreOrd(utilerias.getString(mapResult.get("NOMBRE_ORD")));
			 bean.setNombreRec(utilerias.getString(mapResult.get("NOMBRE_REC")));	
			 bean.setEstatusSer(utilerias.getString(mapResult.get("ESTATUS_SERVIDOR")));
			 lista.add(bean);
		}
				
				
		
		
		return lista;
	}
	
	/**
	 * funcion para setear los valores
	 * 
	 * @param bean
	 * @return
	 */
	public BeanResSpeiCero setearValroes(BeanReqSpeiCero bean) {
		/**inicializa el objeto **/
		BeanResSpeiCero dto = new BeanResSpeiCero();
		/**asignacion de datos**/
		dto.setDuranSpeiCero(bean.isDuranSpeiCero());
		dto.setInstiAlterna(bean.isInstiAlterna());
		dto.setInstiPrincipal(bean.isInstiPrincipal());
		dto.setPrevSpeiCero(bean.isPrevSpeiCero());
		dto.setEstatusOpera(bean.getEstatusOpera());	
		dto.setOpcion(bean.getOpcion());
		dto.setOpcionCero(bean.getOpcionCero());
		dto.setmInstitucion(bean.getmInstitucion());
		dto.setHoraFin(bean.getHoraFin());
		dto.setHoraInicio(bean.getHoraFin());
		dto.setRptTodo(bean.getRptTodo());
		dto.setHistorico(bean.getHistorico());
		dto.setExportar(bean.getExportar());
		dto.setTodo(bean.isTodo());
		dto.setHrInicio(bean.getHrInicio());
		dto.setHrFin(bean.getHrFin());
		dto.setMinInicio(bean.getMinInicio());
		dto.setMinFin(bean.getMinFin());
		
		if(bean.getFchOperacion()!=null) {
			dto.setFchOperacion(bean.getFchOperacion());
		}
		if(bean.getHoraFecOperacion()!=null) {
			dto.setHoraFecOperacion(bean.getHoraFecOperacion());
		}
		
		if(bean.getHoraIniSpeiCero()!=null) {
			dto.setHoraFecOperacion(bean.getHoraFecOperacion());
		}
		/**regresa el objeto **/
		return dto;
	}
	
	/**
	 * funcion para obtener la fecha de spei
	 * 
	 * @param entrada
	 * @return
	 */
	public BeanFechaHorario fechaSPEI(BeanConsMiInstitucion entrada) throws ExceptionDataAccess {
		BeanFechaHorario resFechaHorarioCambio = new BeanFechaHorario();		
		List<HashMap<String,Object>> list = null;		
		Utilerias utilerias = Utilerias.getUtilerias();		
		/** Instancia que guarda la respuesta de la consulta de BD. */
		ResponseMessageDataBaseDTO responseDTO = null;		
		/** Instancia ArrayList tipo Object que almacena los parametros a enviar a la BD . */
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(entrada.getMiInstitucion());
		parametros.add(entrada.getMiInstitucion());
		/** Obtenemos la Query a ejecutar . */
		String query = UtileriasModuloSPEICero.QUERY_HRO_SPEI_CERO;		
		try {
			/** Metodo que ejecuta la query, parametros de entrada:
			 *  - Query
			 *  - Parametros de Entrada
			 *  - ID de Canal a ejecutar 
			 *  - Class Name
			 **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**se asigna el valor de la consulta **/
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				if(!list.isEmpty()){
					Map<String,Object> mapResult = list.get(0);
					resFechaHorarioCambio.setHoraIniSpeiCero(utilerias.getString(mapResult.get("FCH_SPEI")));
				}
			}
			
		} catch (ExceptionDataAccess e) {
			error(ERROR+e.getMessage(), this.getClass());
			throw e;
		}
		/**Se regresa el Valor**/
		return resFechaHorarioCambio;
	}
	
	/**
	 *funcion para obtener la fecha de operacion
	 * @param entrada
	 * @return
	 */
	public BeanFechaHorario fechaOperacion(BeanConsMiInstitucion entrada)throws ExceptionDataAccess {
		BeanFechaHorario resFechaHorarioCambio = new BeanFechaHorario();		
		List<HashMap<String,Object>> list = null;		
		Utilerias utilerias = Utilerias.getUtilerias();		
		/** Instancia que guarda la respuesta de la consulta de BD. */
		ResponseMessageDataBaseDTO responseDTO = null;		
		/** Instancia ArrayList tipo Object que almacena los parametros a enviar a la BD . */
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(entrada.getMiInstitucion());
		
		/** Obtenemos la Query a ejecutar . */
		String query = UtileriasModuloSPEICero.QUERY_FECHA_OPERACION;		
		try {
			/** Metodo que ejecuta la query, parametros de entrada:
			 *  - Query
			 *  - Parametros de Entrada
			 *  - ID de Canal a ejecutar 
			 *  - Class Name
			 **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/**se asigna el valor de la consulta **/
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				if(!list.isEmpty()){
					Map<String,Object> mapResult = list.get(0);
					resFechaHorarioCambio.setHoraFecOperacion(utilerias.getString(mapResult.get("FCH_OPERACION")));
				}
			}
			
		} catch (ExceptionDataAccess e) {
			error(ERROR+e.getMessage(), this.getClass());
			throw e;
		}
		/**Se regresa el Valor**/
		return resFechaHorarioCambio;
	}
	
	/**
	 * funcion para obtener el query que continua
	 * 
	 * @param beanReqSpeiCero
	 * @return
	 */
	public String querySPEI(BeanReqSpeiCero beanReqSpeiCero) {
		String query ="";
		
		/**Se suman los filtro de previo**/
		if(beanReqSpeiCero.isPrevSpeiCero()) {
			query = UtileriasModuloSPEICero.PREVIO;		
		}
		/**Se suman los filtro de durante**/
		if(beanReqSpeiCero.isDuranSpeiCero()) {
			query = UtileriasModuloSPEICero.DURANTE;	
		}
		
		return query;
	}
	
	/**
	 * funcion para obtener la fecha actual en formato dd/mm/yyyy
	 * @return
	 */
	public String fechaAcutal() {
		/**Asigna la fecha actual para pasarla como paramentro **/
		SimpleDateFormat d = new SimpleDateFormat("dd/mm/yyyy");
		return d.format(new Date());
	}
	
	/**
	 * Funcion para buscar los estatus
	 * 
	 * @param sessionBean
	 * @param entrada
	 * @return
	 */
	public  List<BeanListas> buscaEstatus(ArchitechSessionBean sessionBean) {
		List<BeanListas> listaEstatusOpera = new ArrayList<BeanListas>();
		List<HashMap<String,Object>> list = null;		
		Utilerias utilerias = Utilerias.getUtilerias();
		
		/** Instancia que guarda la respuesta de la consulta de BD. */
		ResponseMessageDataBaseDTO responseDTO = null;
	
		/** Instancia ArrayList tipo Object que almacena los parametros a enviar a la BD . */
		List<Object> parametros = new ArrayList<Object>();
	
		/** Obtenemos la Query a ejecutar . */
		String query = UtileriasModuloSPEICero.QUERY_CONS_ESTATUS_OPERA;
		
		try {
			/** Metodo que ejecuta la query, parametros de entrada:
			 *  - Query
			 *  - Parametros de Entrada
			 *  - ID de Canal a ejecutar 
			 *  - Class Name
			 **/
			responseDTO = HelperDAO.consultar(query, parametros, HelperDAO.CANAL_GFI_DS, this.getClass().getName());
			/** pregunta si el resultado trae datos**/
			if(responseDTO.getResultQuery()!= null){
				list = responseDTO.getResultQuery();
				/**si hay datos en la lista**/				
				BeanListas  regOperaEstatus = null;
				/**se se recorre la lista**/
				for(int i = 0; i<list.size(); i++){
					Map<String,Object> mapResult = list.get(i);						
					regOperaEstatus = new BeanListas();		
					String valor = utilerias.getString(mapResult.get("VALOR_FALTA"));
					String clave = utilerias.getString(mapResult.get("CVE_PARAM_FALTA"));						
					regOperaEstatus.setValor(valor);
					regOperaEstatus.setClave(clave);
					listaEstatusOpera.add(regOperaEstatus);
				}
					
			}	
		} catch (ExceptionDataAccess e) {			
			error(ERROR+e.getMessage(), this.getClass());			
		}
		
		return listaEstatusOpera;
	}
	
	/**
	 * funcion para traer query inicial
	 * @param beanReqSpeiCero
	 * @return
	 */
	public String seleccionarQuery(BeanReqSpeiCero beanReqSpeiCero) {
		String query="";
		
		if(beanReqSpeiCero.getHistorico().equals(CERO)){
			if(beanReqSpeiCero.getExportar().equals(CERO)) {
				/**se arma el query **/
				query = UtileriasModuloSPEICero.QUERY_INICIAL+UtileriasModuloSPEICero.ESTATUS_NR+UtileriasModuloSPEICero.FILTROS_NORMAL + util.querySPEI(beanReqSpeiCero); 
			}else {
				/**se arma el query **/
				query = UtileriasModuloSPEICero.QUERY_CONSULTA_EXPORTAR+UtileriasModuloSPEICero.FILTROS_NORMAL + util.querySPEI(beanReqSpeiCero); 
			}
			
		}else {
			if(beanReqSpeiCero.getExportar().equals(CERO)) {
				/**se arma el query **/
				query = UtileriasModuloSPEICero.QUERY_INICIAL+UtileriasModuloSPEICero.ESTATUS_HTO +UtileriasModuloSPEICero.FILTROS_HTO + util.querySPEI(beanReqSpeiCero); 
			}else {
				/**se arma el query **/
				query = UtileriasModuloSPEICero.QUERY_CONSULTA_EXPORTAR+UtileriasModuloSPEICero.FILTROS_HTO + util.querySPEI(beanReqSpeiCero); 

			}
		}	
		
		if(beanReqSpeiCero.getExportar().equals(CERO)) {
			/**se concatena el filtro del instituto**/
			if(beanReqSpeiCero.isTodo()) {
				query = query + UtileriasModuloSPEICero.INTITUTOS;
			}else {
				query = query + UtileriasModuloSPEICero.INTITUTO;
			}
			
			/**asigna el filtro de la fecha **/ 
			query =query +UtileriasModuloSPEICero.FILTROFECHA ;
		}
		
		return query;
	}
	
	
}

