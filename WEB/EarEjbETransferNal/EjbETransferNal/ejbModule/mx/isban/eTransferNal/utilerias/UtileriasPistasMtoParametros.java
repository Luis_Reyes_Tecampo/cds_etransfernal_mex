/**
 * Clase de tipo Utilerias de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Junio 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.utilerias;

import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;

/**
 * The Class UtileriasPistasMtoParametros.
 */
public final class UtileriasPistasMtoParametros {

	/** The Constant CVE_PARAM. */
	private static final String CVE_PARAM = "CVE_PARAMETRO=";
	
	/** The Constant DESCRIPCION. */
	private static final String DESCRIP = "DESCRIPCION=";
	
	/** The Constant TIPO_PARAMETRO. */
	private static final String TIPO_PARAM = ", TIPO_PARAMETRO=";

	/** The Constant TIPO_DATO. */
	private static final String TIPO_DATO = ", TIPO_DATO=";
	
	/** The Constant LONGITUD. */
	private static final String LONGITUD = ", LONGITUD=";
	
	/** The Constant VALOR_FALTA. */
	private static final String VALOR_FALTA = ", VALOR_FALTA=";
	
	/** The Constant TRAMA. */
	private static final String DEFINE_TRAMA = ", DEFINE_TRAMA=";
	
	/** The Constant COLA. */
	private static final String DEFINE_COLA = ", DEFINE_COLA=";
	
	/** The Constant MECANISMO. */
	private static final String DEFINE_MECANISMO = ", DEFINE_MECANISMO=";
	
	/** The Constant POSICION. */
	private static final String POSICION = ", POSICION=";
	
	/** The Constant PARAMETRO_FALTA. */
	private static final String PARAM_FALTA = ", CVE_PARAM_FALTA=";
	
	/** The Constant PROGRAMA. */
	private static final String PROGRAM = ", PROGRAMA=";

	/** Referencia del Objeto Utilerias. */
	private static final UtileriasPistasMtoParametros UTILS_PISTAS_MANT_PARAM_INSTACE = new UtileriasPistasMtoParametros();

	/**
	 * Constructor privado.
	 */
	private UtileriasPistasMtoParametros() {super();}
	
	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasPistasMtoParametros getUtilerias(){
		return UTILS_PISTAS_MANT_PARAM_INSTACE;
	}
	
	/**
	 * Generar val nuevo_ alta editar.
	 *
	 * @param request the request
	 * @param esAltaParam the es alta param
	 * @return the string
	 */
	public String generarValNuevoAltaEditar(BeanReqMantenimientoParamGuardar request){
		StringBuilder builder=new StringBuilder();
		String vacio="";
		
		builder.append(
				//metodo que genera parte de la parametrizacion de valorNuevo para pista de auditoria
				generarValNuevoAltaEditarPart2(request));

		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getCola()) && !"Nulo".equals(request.getCola()) ){
			builder.append(DEFINE_COLA + request.getCola());
		}else{
			builder.append(DEFINE_COLA + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getMecanismo()) && !"Nulo".equals(request.getMecanismo()) ){
			builder.append(DEFINE_MECANISMO + request.getMecanismo());
		}else{
			builder.append(DEFINE_MECANISMO + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getPosicion()) ){
			builder.append(POSICION + request.getPosicion());
		}else{
			builder.append(POSICION + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getParametroFalta()) ){
			builder.append(PARAM_FALTA + request.getParametroFalta());
		}else{
			builder.append(PARAM_FALTA + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getPrograma()) ){
			builder.append(PROGRAM + request.getPrograma());
		}else{
			builder.append(PROGRAM + "null");
		}
		return builder.toString();
	}

	/**
	 * Generar val nuevo_ alta editar.
	 *
	 * @param builder the builder
	 * @param request the request
	 * @return the string
	 */
	public String generarValNuevoAltaEditarPart2(BeanReqMantenimientoParamGuardar request){
		StringBuilder builder=new StringBuilder();
		String vacio="";
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getParametro()) ){
			builder.append(CVE_PARAM+request.getParametro()+", ");
		}else{
			builder.append(CVE_PARAM+"null, ");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getDescripParam()) ){
			builder.append(DESCRIP + request.getDescripParam());
		}else{
			builder.append(DESCRIP + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getTipoParametro()) ){
			builder.append(TIPO_PARAM + request.getTipoParametro());
		}else{
			builder.append(TIPO_PARAM + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getTipoDato()) ){
			builder.append(TIPO_DATO + request.getTipoDato());
		}else{
			builder.append(TIPO_DATO + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getLongitudDato()) ){
			builder.append(LONGITUD + request.getLongitudDato());
		}else{
			builder.append(LONGITUD + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getValorFalta()) ){
			builder.append(VALOR_FALTA + request.getValorFalta());
		}else{
			builder.append(VALOR_FALTA + "null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(request.getTrama()) && !"Nulo".equals(request.getTrama()) ){
			builder.append(DEFINE_TRAMA + request.getTrama());
		}else{
			builder.append(DEFINE_TRAMA + "null");
		}
		return builder.toString();
	}

	/**
	 * Generar val anteriro editar parametro.
	 *
	 * @param datosParametro the datos parametro
	 * @return the string
	 */
	public String generarValAnteriorEditarParametro(List<BeanMantenimientoParametros> datosParametro){
		StringBuilder builder=new StringBuilder();
		String vacio="";
		for(BeanMantenimientoParametros bean: datosParametro){
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getCveParametro()) ){
				builder.append(CVE_PARAM+bean.getCveParametro()+", ");
			} else {
				builder.append(CVE_PARAM+"null, ");
			}
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getDescripParam()) ){
				builder.append(DESCRIP+bean.getDescripParam());
			} else {
				builder.append(DESCRIP+"null");
			}
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getTipoParametro()) ){
				builder.append(TIPO_PARAM+bean.getTipoParametro());
			} else {
				builder.append(TIPO_PARAM+"null");
			}
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getTipoDato()) ){
				builder.append(TIPO_DATO+bean.getTipoDato());
			} else {
				builder.append(TIPO_DATO+"null");
			}
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getLongitud()) ){
				builder.append(LONGITUD+bean.getLongitud());
			} else {
				builder.append(LONGITUD+"null");
			}
			//se verifica que el campo no venga vacio
			if( !vacio.equals(bean.getValorFalta()) ){
				builder.append(VALOR_FALTA+bean.getValorFalta());
			} else {
				builder.append(VALOR_FALTA+"null");
			}
			builder.append(
					generarValAnteriorEditarParametroPart2(bean));
		}
		return builder.toString();
	}
	
	/**
	 * Generar val anteriro editar parametro.
	 *
	 * @param builder the builder
	 * @param bean the bean
	 * @return the string
	 */
	public String generarValAnteriorEditarParametroPart2(BeanMantenimientoParametros bean){
		StringBuilder builder=new StringBuilder();
		String vacio="";
		
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getTrama()) ){
			builder.append(DEFINE_TRAMA+bean.getTrama());
		} else {
			builder.append(DEFINE_TRAMA+"null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getCola()) ){
			builder.append(DEFINE_COLA+bean.getCola());
		} else {
			builder.append(DEFINE_COLA+"null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getMecanismo()) ){
			builder.append(DEFINE_MECANISMO+bean.getMecanismo());
		} else {
			builder.append(DEFINE_MECANISMO+"null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getPosicion()) ){
			builder.append(POSICION+bean.getPosicion());
		} else {
			builder.append(POSICION+"null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getParametroFalta()) ){
			builder.append(PARAM_FALTA+bean.getParametroFalta());
		} else {
			builder.append(PARAM_FALTA+"null");
		}
		//se verifica que el campo no venga vacio
		if( !vacio.equals(bean.getPrograma()) ){
			builder.append(PROGRAM+bean.getPrograma());
		} else {
			builder.append(PROGRAM+"null");
		}
		return builder.toString();
	}
}
