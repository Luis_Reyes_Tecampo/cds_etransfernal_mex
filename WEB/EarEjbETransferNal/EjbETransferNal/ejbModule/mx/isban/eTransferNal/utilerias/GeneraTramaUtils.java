package mx.isban.eTransferNal.utilerias;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;

/**
 * Class GeneraTramaUtils.
 *
 * @author FSW-Vector
 * @since 15/02/2019
 */
public class GeneraTramaUtils {

	/** Propiedad del tipo String que almacena el valor de CONSTANTE_47. */
	private static final String CONSTANTE_47 = "047";
	
	private UtileriasGeneraTrama utilTrama = UtileriasGeneraTrama.getInstance();
	
	/**
	 * Inicializa.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param medioEnt El objeto: medio ent
	 * @param sessionBean El objeto: session bean
	 * @return Objeto bean trans envio
	 */
	public BeanTransEnvio inicializa(BeanTranSpeiRecOper beanTranSpeiRec, String medioEnt,
			ArchitechSessionBean sessionBean) {
		BeanTransEnvio beanTrans = new BeanTransEnvio();
		beanTrans.setUsuarioCap(sessionBean.getUsuario());
		beanTrans.setUsuarioSol(sessionBean.getUsuario());
		beanTrans.setPtoVta("0901");
		beanTrans.setFormaLiq("N");
		beanTrans.setCveEmpresa("BME");

		beanTrans.setMedioEnt(medioEnt);

		beanTrans.setDivisaOrd("MN");
		beanTrans.setDivisaRec("MN");
		beanTrans.setPtoVtaOrd("00000");
		beanTrans.setPtoVtaRec("00000");
		beanTrans.setImporteAbono(beanTranSpeiRec.getBeanDetalle().getOrdenante().getMonto().toString());
		beanTrans.setImporteCargo(beanTranSpeiRec.getBeanDetalle().getOrdenante().getMonto().toString());
		beanTrans.setImporteDolares("0");
		beanTrans.setNombreBcoRec(beanTranSpeiRec.getBeanDetalle().getReceptor().getBancoRec());
		beanTrans.setPaisBcoRec("");
		beanTrans.setCveABA("");
		beanTrans.setPlazaBanxico("01001");
		return beanTrans;
	}

	/**
	 * Setea envio.
	 *
	 * @param beanTransEnvio            Objeto del tipo BeanTransEnvio
	 * @param beanTranSpeiRec            Objeto del tipo BeanTranSpeiRec
	 * @param bancoReceptor            String con el banco receptor
	 */
	public void seteaEnvio(BeanTransEnvio beanTransEnvio, BeanTranSpeiRecOper beanTranSpeiRec, String bancoReceptor) {
		String conceptoPago1 = null;
		beanTransEnvio.setCveOperacion(obtenerCveOperacion(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago()));
		StringBuilder strBuilder = new StringBuilder();
		beanTransEnvio.setCveIntermeOrd(beanTranSpeiRec.getBeanDetalle().getOrdenante().getCveIntermeOrd());
		beanTransEnvio.setNombreOrd(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNombreOrd());
		beanTransEnvio.setCuentaOrd(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		beanTransEnvio.setNombreRec(beanTranSpeiRec.getBeanDetalle().getReceptor().getNombreRec());
		beanTransEnvio.setCuentaRec(beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec());
		conceptoPago1 = beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago1();
		if (conceptoPago1 != null && conceptoPago1.length() > 40) {
			strBuilder.append(conceptoPago1.substring(0, 40));
		} else {
			strBuilder.append(conceptoPago1);
		}
		if (beanTranSpeiRec.getBeanDetalle().getDetalleGral().getRefeNumerica().length() > 7 ) {
			strBuilder.append(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getRefeNumerica().substring(0, 7));
		} else {
			strBuilder.append(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getRefeNumerica());
		}
		if (beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd().length() > 20) {
			strBuilder.append(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd().substring(0, 20));
		} else {
			strBuilder.append(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		}
		beanTransEnvio.setComentario1(strBuilder.toString());
		beanTransEnvio.setCiudadBcoRe("");
		beanTransEnvio.setSucBcoRe("");
		if ("40014".equals(bancoReceptor)) {
			armaEnvioBanme(beanTransEnvio, beanTranSpeiRec);
		} else if ("40003".equals(bancoReceptor)) {
			// *** S E R F I N ***
			beanTransEnvio.setCveIntermeRec("SERFI");
			beanTransEnvio.setCveTransfe(utilTrama.obtenerCveTransBanco(beanTranSpeiRec));
		}
	}

	/**
	 * Metodo setea los datos para la devolucion.
	 *
	 * @param beanTransEnvio            Objeto del tipo BeanTransEnvio
	 * @param beanTranSpeiRec            Objeto del tipo BeanTranSpeiRec
	 * @param bancoReceptor            Cadena con la clave del banco receptor
	 */
	public void seteaDevolucion(BeanTransEnvio beanTransEnvio, BeanTranSpeiRecOper beanTranSpeiRec, String bancoReceptor) {
		beanTransEnvio.setCveOperacion("DEVSPEI");
		beanTransEnvio.setNombreOrd(beanTranSpeiRec.getBeanDetalle().getReceptor().getNombreRec());
		beanTransEnvio.setCuentaOrd(beanTranSpeiRec.getNumCuentaTran());
		beanTransEnvio.setCveIntermeRec(beanTranSpeiRec.getBeanDetalle().getOrdenante().getCveIntermeOrd());
		beanTransEnvio.setNombreRec(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNombreOrd());
		beanTransEnvio.setCuentaRec(beanTranSpeiRec.getBeanDetalle().getOrdenante().getNumCuentaOrd());
		beanTransEnvio.setComentario1(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getCde());
		beanTransEnvio.setCiudadBcoRe(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreo());
		beanTransEnvio.setSucBcoRe(beanTranSpeiRec.getBeanDetalle().getBeanCambios().getMotivoDevol());
		beanTransEnvio.setTipoCuentaOrd(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec());
		beanTransEnvio.setTipoCuentaRec(beanTranSpeiRec.getBeanDetalle().getOrdenante().getTipoCuentaOrd());
		if ("40014".equals(bancoReceptor)) {
			// *** S A N T A N D E R ***
			beanTransEnvio.setCveIntermeOrd("BANME");
			if ("1".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago()) || "5".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())
					|| "12".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())) {
				beanTransEnvio.setCveTransfe("074");
			} else {
				beanTransEnvio.setCveTransfe("077");
			}
		} else {
			if ("40003".equals(bancoReceptor)) {
				beanTransEnvio.setCveIntermeOrd("SERFI");
				beanTransEnvio.setCveTransfe(utilTrama.obtenerBancoRecep(beanTranSpeiRec));
			}
		}
	}

	/**
	 * Metodo setea los datos para envio a Satander.
	 *
	 * @param beanTransEnvio            Objeto del tipo BeanTransEnvio
	 * @param beanTranSpeiRec            Objeto del tipo BeanTranSpeiRec
	 */
	private void armaEnvioBanme(BeanTransEnvio beanTransEnvio, BeanTranSpeiRecOper beanTranSpeiRec) {
		boolean flagConvVostro = false;
		if ("VOSTRO".equals(beanTranSpeiRec.getBeanDetalle().getDetEstatus().getCvePago())) {
			flagConvVostro = true;
		}
		// *** S A N T A N D E R ***
		beanTransEnvio.setCveIntermeRec("BANME");
		switch (Integer.parseInt(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())) {
		case 0:
			beanTransEnvio.setCveTransfe(utilTrama.envioBanmeCve(beanTranSpeiRec));
			break;
		case 1:
			beanTransEnvio.setCveTransfe(utilTrama.obtenerCveTrans(flagConvVostro, beanTranSpeiRec));
			
			break;
		case 3:
			beanTransEnvio.setCveTransfe("046");
			break;
		case 5:
			beanTransEnvio.setCveTransfe(utilTrama.envioBanme(beanTranSpeiRec,flagConvVostro));
			
			break;
		case 8:
			beanTransEnvio.setCveTransfe("096");
			break;
		case 9:
			beanTransEnvio.setCveTransfe("046");
			break;
		case 10:
			beanTransEnvio.setCveTransfe("097");
			break;
		case 11:
			beanTransEnvio.setCveTransfe(CONSTANTE_47);
			break;
		case 12:
			beanTransEnvio.setCveTransfe("044");
			break;
		default:
			beanTransEnvio.setCveTransfe(CONSTANTE_47);
		}
		if (flagConvVostro) {
			beanTransEnvio.setCveOperacion("CTAVOS");
		}
	}

	/**
	 * Metodo setea los datos para envio a Satander.
	 *
	 * @param beanTransEnvio Objeto del tipo BeanTransEnvio
	 * @return String con la trama a enviar
	 */
    public String armaTramaCadena(BeanTransEnvio beanTransEnvio){
    	final String PIPE = "|";
    	StringBuilder strBuilder = new StringBuilder();
    	if("D".equals(beanTransEnvio.getServicio())){
    		strBuilder.append("TRANSANT");
    	}else if("2".equals(beanTransEnvio.getServicio())){
    		strBuilder.append("TRANTRF2");
    	}
    	strBuilder.append(beanTransEnvio.getCveEmpresa());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getPtoVta());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getMedioEnt());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getReferenciaMed());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getUsuarioCap());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getUsuarioSol());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getCveTransfe());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getCveOperacion());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getFormaLiq());
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getCveIntermeOrd());
    	strBuilder.append(PIPE);    	
    	strBuilder.append(beanTransEnvio.getCuentaOrd());
    	strBuilder.append(PIPE);
    	if (beanTransEnvio.getPtoVtaOrd() != null && !"".equals(beanTransEnvio.getPtoVtaOrd())){
    		strBuilder.append(beanTransEnvio.getPtoVtaOrd());    		
    	}
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getDivisaOrd());
    	strBuilder.append(PIPE);    	
    	strBuilder.append(beanTransEnvio.getNombreOrd());
    	strBuilder.append(PIPE);
    	if (beanTransEnvio.getCveIntermeRec() != null && !"".equals(beanTransEnvio.getCveIntermeRec())){
    		strBuilder.append(beanTransEnvio.getCveIntermeRec());        	
    	} 
    	strBuilder.append(PIPE);
    	strBuilder.append(beanTransEnvio.getCuentaRec());
    	strBuilder.append(PIPE);
    	// se llama a la funcion
    	strBuilder.append(utilTrama.armaTramaCadena2(beanTransEnvio));
    	return strBuilder.toString();
    }

	


	
	/**
	 * Metodo privado que sirve para obtner la cve de operacion.
	 *
	 * @param getTipoPago El objeto: get tipo pago
	 * @return String con la cve de operacion
	 */
	public String obtenerCveOperacion(String getTipoPago) {
		String cveOperacion;
		switch (Integer.parseInt(getTipoPago)) {
		case 2:
			cveOperacion = "PAGOVENT";
			break;
		case 3:
		case 6:
			cveOperacion = "CTAVOS";
			break;
		case 1:
		case 5:
			cveOperacion = "BCACOMER";
			break;
		case 9:
		case 11:
			cveOperacion = "MAPEO";
			break;
		case 12:
			cveOperacion = "NOMISPEI";
			break;
		default:
			cveOperacion = "";
			break;
		}
		return cveOperacion;
	}
}
