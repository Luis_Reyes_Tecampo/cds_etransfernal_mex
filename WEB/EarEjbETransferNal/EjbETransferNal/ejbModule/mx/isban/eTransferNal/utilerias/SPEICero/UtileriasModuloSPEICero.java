package mx.isban.eTransferNal.utilerias.SPEICero;

/**
 * The Class UtileriasModuloSPEICero.
 */
public final class UtileriasModuloSPEICero {
			
	/**
	 * variable para minutos
	 */
	public static final int MIN= 60;
	/**
	 * variable para hras
	 */
	public static final int HRS= 24;
	/**
	 * variable para la cuenta principal
	 */
	public static final String CTAPRINCIPAL ="ENTIDAD_SPEI";
	/**
	 * variable para la cuenta alterna
	 */
	public static final String CTAALTERNA ="ENTIDAD_SPEI_CA";

	
	/**variable  Previo SIGNO **/
	public static final String SIGNO=" ? ";
	
	/** The Constant QUERY_CONS_MI_INSTITUCION. */
	public static final String QUERY_CONS_MI_INSTITUCION = " SELECT CV_VALOR FROM TRAN_CONTIG_UNIX WHERE TRIM(CV_PARAMETRO) = ? ";
	
	
	/** The Constant QUERY_CONS_HORARIO_CAMBIO. */
	public static final String QUERY_HRO_SPEI_CERO = " SELECT MIN(TO_CHAR(FCH_HORA_ENVIO,'DD/MM/YYYY HH:MM:SS')) FCH_SPEI FROM TRAN_SPEI_BITENV_CERO " + 
													"WHERE FCH_HORA_ENVIO <> 'ULTIMOFOLIO' " + 
													"AND CVE_MI_INSTITUC = ? " + 
													"AND FCH_OPERACION = (SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL "+
																		 " WHERE CVE_MI_INSTITUC in( ? ) )"  ;
	
	
	/** The Constant QUERY_CONS_HORARIO_CAMBIO. */
	public static final String QUERY_FECHA_OPERACION = " SELECT CASE WHEN FCH_OPERACION > TRUNC(SYSDATE) " +
														   " THEN TO_CHAR(SYSDATE,'DD/MM/YYYY ')||NVL(HORA_RECEPCION, '00:00:00') " +
														   " ELSE TO_CHAR(FCH_OPERACION,'DD/MM/YYYY ')||NVL(HORA_RECEPCION, '00:00:00')  " +
														   " END FCH_OPERACION FROM TRAN_SPEI_CTRL " + 
														   " WHERE CVE_MI_INSTITUC = ? ";
	
	/**variable solo para la clave del estatus**/
	public static final String CAMPO_A = "SELECT TRIM(CVE_PARAM_FALTA) CVE_PARAM_FALTA ";
	/**variable solo para la el valor del estatus**/
	public static final String CAMPO_B = ", TRIM(VALOR_FALTA) VALOR_FALTA ";
	/**variable solo para la el **/
	public static final String TODO = "TODO" ;
	
	
	
	/** The Constant QUERY_CONS_ESTATUS_OPERA. */
	public static final String QUERY_SELECT_PARAMA =
												   " FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE 'ESTATUS0%' AND CVE_PARAM_FALTA";
		
	
	
	/** The Constant QUERY_CONS_ESTATUS_OPERA. */
	public static final String QUERY_CONS_ESTATUS_OPERA = CAMPO_A +CAMPO_B +QUERY_SELECT_PARAMA+"='"+TODO+"' UNION ALL " 
													    +CAMPO_A +CAMPO_B + QUERY_SELECT_PARAMA +"!='"+TODO+"'";
	
	/** The Constant QUERY_CONS_ESTATUS_OPERA. */
	public static final String QUERY_CONS_ESTATUS_OPERA_TODO = CAMPO_A + QUERY_SELECT_PARAMA +"!='"+TODO+"'";
	
	/**
	 * inicia la consulta
	 */
	public static final String QUERY_INICIAL="SELECT TM.REFERENCIA, " + 
								"CASE WHEN TT.TIPO_TRANSFE = 'E' THEN 'ENVIO' " + 
								"     WHEN TT.TIPO_TRANSFE = 'R' THEN 'RECEPCION' " + 
								"     WHEN TT.TIPO_TRANSFE = 'D' AND TM.CVE_TRANSFE LIKE '_7_'  THEN 'DEVOLUCION ENVIADA' " + 
								"     WHEN TT.TIPO_TRANSFE = 'D' AND TM.CVE_TRANSFE LIKE '_8_'  THEN 'DEVOLUCION ENVIADA' " + 
								"     ELSE '' " + 
								"END TIPO_OPERACION, ROWNUM INDICE, " + 
								"CME.DESCRIPCION, TM.CVE_TRANSFE, TM.CVE_OPERACION, " + 
								"TM.IMPORTE_ABONO, TM.NOMBRE_ORD, TM.NOMBRE_REC " ;
	/**
	 * inicia la consulta
	 */
	public static final String ESTATUS_HTO=" ,' ' ESTATUS_SERVIDOR " ;
	
	/**
	 * inicia la consulta
	 */
	public static final String ESTATUS_NR=" ,tm.estatus_servidor" ;
	
	/** tabla principal**/
	public static final String TABLA_MSJ=" FROM TRAN_MENSAJE TM ";
	/**tabla historica**/
	public static final String TABLA_MSJHTO=" FROM TRAN_MENSAJE_HIS TM ";
	/**tabla de mensajes rec **/
	public static final String TABLA_REC=" LEFT OUTER JOIN TRAN_SPEI_REC TSR ";
	/**tabla de mensajes rec his **/
	public static final String TABLA_RECHTO=" LEFT OUTER JOIN TRAN_SPEI_REC_HIS TSR ";
	/**union de tablas **/
	public static final String TABLA_UNION_REC=" ON TM.REFERENCIA = TSR.REFE_TRANSFER ";
	/**tabla de mensajes env **/
	public static final String TABLA_ENV=" LEFT OUTER JOIN TRAN_SPEI_ENV TSE ";
	/**tabla de mensajes env hto **/
	public static final String TABLA_ENVHTO=" LEFT OUTER JOIN TRAN_SPEI_ENV_HIS TSE ";
	/**union de tablas **/
	public static final String TABLA_UNION_ENV= " ON TM.REFERENCIA = TSE.REFERENCIA " ;
	/**continuacion del filtro **/
	public static final String FILTRO_A= " INNER JOIN COMU_MEDIOS_ENT CME " + 
										"    ON TM.CVE_MEDIO_ENT = CME.CVE_MEDIO_ENT " + 
										" INNER JOIN TRAN_TRANSFERENC TT " + 
										"    ON TM.CVE_TRANSFE = TT.CVE_TRANSFE " ;
	/**continuacion del filtro **/
	public static final String INTITUTO=" AND  TSR.CVE_MI_INSTITUC = "+SIGNO;
	
	/**continuacion del filtro **/
	public static final String INTITUTOS=" TSR.CVE_MI_INSTITUC IN (?,?) ";
	/**continuacion del filtro **/
	public static final String FILTRO_B=" AND TSE.CVE_MI_INSTITUC = TSR.CVE_MI_INSTITUC  WHERE ";
	/**consulta normal **/
	public static final String FILTROS_NORMAL=TABLA_MSJ+FILTRO_A+TABLA_REC+TABLA_UNION_REC+TABLA_ENV+TABLA_UNION_ENV+  FILTRO_B;
	/**consulta hisotrica **/
	public static final String FILTROS_HTO=TABLA_MSJHTO+FILTRO_A+TABLA_RECHTO+TABLA_UNION_REC+TABLA_ENVHTO+TABLA_UNION_ENV+ FILTRO_B;
	
	/**variable  filtro SPEI CERO: **/
	public static final String LIKE = " TSR.FOLIO_PAQUETE ";
	/**variable  filtro SPEI CERO: **/
	public static final String NOLIKE = " NOT LIKE '16__________'";
	/**variable  filtro SPEI CERO: **/
	public static final String SILIKE = " LIKE '16__________'";
	/**variable  Previo al SPEI CERO: **/
	public static final String 	PREVIO = LIKE + NOLIKE;									
	/**variable  Durante al SPEI CERO: **/
	public static final String DURANTE = LIKE + SILIKE;	
	
	/**filtro reporte **/
	public static final String QUERY_REPORTE ="SELECT NVL(TSR.CODIGO_ERROR,TM.COMENTARIO3) COMENTARIO, " + 
											"NVL(TSR.CONCEPTO_PAGO1,TSE.CONCEPTO_PAGO1) CONCEPTO_PAGO1," + 
											"NVL(TSR.CONCEPTO_PAGO2,TSE.CONCEPTO_PAGO2) CONCEPTO_PAGO2, " + 
											"NVL(TSR.CVE_INST_ORD,TSE.CVE_INTERME_ORD) CVE_INTERME_ORD, " + 
											"NVL(TSR.CVE_MI_INSTITUC,TSE.CVE_INTERME_REC) CVE_MI_INSTITUC, " + 
											"NVL(TSR.CVE_RASTREO,TSE.CVE_RASTREO) CVE_RASTREO, " + 
											"NVL(TSR.CVE_RASTREO_ORI,TSE.CVE_RASTREO_ORI)CVE_RASTREO_ORI, " + 
											"NVL(TSR.ESTATUS_BANXICO,'') ESTATUS_BANXICO, " + 
											"NVL(TSR.ESTATUS_TRANSFER,'') ESTATUS_TRANSFER, " + 
											"NVL(TSR.FCH_CAPTURA,TSE.FCH_CAPTURA) FCH_CAPTURA, " + 
											"NVL(TSR.FCH_OPERACION,TSE.FCH_OPERACION) FCH_OPERACION, " + 
											"NVL(TSR.FOLIO_PAGO,TSE.FOLIO_PAGO) FOLIO_PAGO, " + 
											"NVL(TSR.FOLIO_PAGO_DEV,TSE.FOLIO_PAGO_DEV) FOLIO_PAGO_DEV, " + 
											"NVL(TSR.FOLIO_PAQ_DEV,TSE.FOLIO_PAQ_DEV) FOLIO_PAQ_DEV, " + 
											"NVL(TSR.FOLIO_PAQUETE,TSE.FOLIO_PAQUETE) FOLIO_PAQUETE, " + 
											"NVL(TSR.IVA,TSE.IVA) IVA, " + 
											"NVL(TSR.MONTO,TSE.MONTO) MONTO, " + 
											"NVL(TSR.MOTIVO_DEVOL,TSE.MOTIVO_DEVOL) MOTIVO_DEVOL , " + 
											"NVL(TSR.NOMBRE_ORD,TSE.NOMBRE_ORD) NOMBRE_ORD, " + 
											"NVL(TSR.NOMBRE_REC,TSE.NOMBRE_REC) NOMBRE_REC, " + 
											"NVL(TSR.NOMBRE_REC2,TSE.NOMBRE_REC2) NOMBRE_REC2, " + 
											"NVL(TSR.NUM_CUENTA_ORD,TSE.NUM_CUENTA_ORD) NUM_CUENTA_ORD, " + 
											"NVL(TSR.NUM_CUENTA_REC,TSE.NUM_CUENTA_REC) NUM_CUENTA_REC, " + 
											"NVL(TSR.NUM_CUENTA_REC2,TSE.NUM_CUENTA_REC2) NUM_CUENTA_REC2, " + 
											"NVL(TSR.NUM_CUENTA_TRAN,TSE.NUM_CUENTA_TRAN) NUM_CUENTA_TRAN, " + 
											"NVL(TSR.PRIORIDAD,TSE.PRIORIDAD) PRIORIDAD, " + 
											"NVL(TSR.TIPO_OPERACION,TSE.TIPO_OPERACION) TIPO_OPERACION, " + 
											"NVL(TSR.REFE_NUMERICA,TSE.REFE_NUMERICA) REFE_NUMERICA, " + 
											"NVL(TSR.REFE_TRANSFER,TSE.REFERENCIA) REFE_TRANSFER, " + 
											"TM.CVE_TRANSFE, " + 
											"TM.CVE_OPERACION, " + 
											"TM.CVE_MEDIO_ENT, " + 
											"NVL(TSR.TIPO_CUENTA_ORD,TSE.TIPO_CUENTA_ORD) TIPO_CUENTA_ORD, " + 
											"NVL(TSR.TIPO_CUENTA_REC,TSE.TIPO_CUENTA_REC) TIPO_CUENTA_REC, " + 
											"NVL(TSR.TIPO_CUENTA_REC2,TSE.TIPO_CUENTA_REC2) TIPO_CUENTA_REC2, " + 
											"NVL(TSR.TIPO_PAGO,TSE.TIPO_PAGO) TIPO_PAGO, " + 
											"NVL(TSR.TOPOLOGIA,TSE.TOPOLOGIA) TOPOLOGIA, " + 
											"NVL(TSR.CDA,'') CDA " ;
	
	
	
	
	/**variable  Previo ESTATUS **/
	public static final String ESTATUS=" AND trim(TM.ESTATUS) = ";
	/**variable  Previo ESTATUS **/
	public static final String FILTROESTATUS=ESTATUS +SIGNO;
	/**variable  Previo ESTATUS **/
	public static final String FILTRO_ESTATUS_TODOS=" AND trim(TM.ESTATUS) IN("+QUERY_CONS_ESTATUS_OPERA_TODO+") ";
	
	/**variable  Previo FECHA **/
	public static final String FECHA =" AND TO_CHAR(TM.FCH_CAPTURA ,'DD/MM/YYYY HH24:MI')"; 
	
	/**variable  Previo FECHA **/
	public static final String FILTROFECHA =FECHA+">= ? "+FECHA +" < ? "; 
    
	/** VARIABLE QYE HACE EL COUNT**/
	public static final String COUNT_REGISTROS=" SELECT COUNT(1) CONT FROM ( ";
	
	/** VARIABLE QYE HACE EL COUNT**/
	public static final String COUNT_REGISTROS_FIN=" )";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_PAG
	 */
	public static final String QUERY_CONSULTA_PAG =
	" ) T "+
	" WHERE INDICE  BETWEEN ? AND  ? ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de QUERY_CONSULTA_SELECT
	 */
	public static final String QUERY_CONSULTA_SELECT = 
		" SELECT T.* FROM ( ";
	
	/**
	 * Constante para la consulta de exportar
	 */
	public static final String QUERY_CONSULTA_EXPORTAR = "select NVL(TSR.CODIGO_ERROR,TM.COMENTARIO3) || ',' || " + 
			" NVL(TSR.CONCEPTO_PAGO1,TSE.CONCEPTO_PAGO1) || ',' ||  " + 
			" NVL(TSR.CONCEPTO_PAGO2,TSE.CONCEPTO_PAGO2) || ',' || " + 
			" NVL(TSR.CVE_INST_ORD,TSE.CVE_INTERME_ORD) || ',' || " + 
			" NVL(TSR.CVE_MI_INSTITUC,TSE.CVE_INTERME_REC) || ',' || " + 
			" NVL(TSR.CVE_RASTREO,TSE.CVE_RASTREO) || ',' || " + 
			" NVL(TSR.CVE_RASTREO_ORI,TSE.CVE_RASTREO_ORI)|| ',' || " + 
			" NVL(TSR.ESTATUS_BANXICO,'') || ',' || " + 
			" NVL(TSR.ESTATUS_TRANSFER,'') || ',' || " + 
			" NVL(TSR.FCH_CAPTURA,TSE.FCH_CAPTURA) || ',' || " + 
			" NVL(TSR.FCH_OPERACION,TSE.FCH_OPERACION) || ',' || " + 
			" NVL(TSR.FOLIO_PAGO,TSE.FOLIO_PAGO) || ',' || " + 
			" NVL(TSR.FOLIO_PAGO_DEV,TSE.FOLIO_PAGO_DEV) || ',' || " + 
			" NVL(TSR.FOLIO_PAQ_DEV,TSE.FOLIO_PAQ_DEV) || ',' || " + 
			" NVL(TSR.FOLIO_PAQUETE,TSE.FOLIO_PAQUETE) || ',' || " + 
			" NVL(TSR.IVA,TSE.IVA) || ',' || " + 
			" NVL(TSR.MONTO,TSE.MONTO) || ',' || " + 
			" NVL(TSR.MOTIVO_DEVOL,TSE.MOTIVO_DEVOL)  || ',' || " + 
			" NVL(TSR.NOMBRE_ORD,TSE.NOMBRE_ORD) || ',' || " + 
			" NVL(TSR.NOMBRE_REC,TSE.NOMBRE_REC) || ',' || " + 
			" NVL(TSR.NOMBRE_REC2,TSE.NOMBRE_REC2) || ',' || " + 
			" NVL(TSR.NUM_CUENTA_ORD,TSE.NUM_CUENTA_ORD) || ',' || " + 
			" NVL(TSR.NUM_CUENTA_REC,TSE.NUM_CUENTA_REC) || ',' || " + 
			" NVL(TSR.NUM_CUENTA_REC2,TSE.NUM_CUENTA_REC2) || ',' || " + 
			" NVL(TSR.NUM_CUENTA_TRAN,TSE.NUM_CUENTA_TRAN) || ',' || " + 
			" NVL(TSR.PRIORIDAD,TSE.PRIORIDAD) || ',' || " + 
			" NVL(TSR.TIPO_OPERACION,TSE.TIPO_OPERACION) || ',' || " + 
			" NVL(TSR.REFE_NUMERICA,TSE.REFE_NUMERICA) || ',' || " + 
			" NVL(TSR.REFE_TRANSFER,TSE.REFERENCIA) || ',' || " + 
			"											TM.CVE_TRANSFE|| ',' || " + 
			"											TM.CVE_OPERACION|| ',' || " + 
			"											TM.CVE_MEDIO_ENT|| ',' || " + 
			" NVL(TSR.TIPO_CUENTA_ORD,TSE.TIPO_CUENTA_ORD) || ',' || " + 
			" NVL(TSR.TIPO_CUENTA_REC,TSE.TIPO_CUENTA_REC) || ',' || " + 
			" NVL(TSR.TIPO_CUENTA_REC2,TSE.TIPO_CUENTA_REC2) || ',' || " + 
			" NVL(TSR.TIPO_PAGO,TSE.TIPO_PAGO) || ',' || " +  
			" NVL(TSR.TOPOLOGIA,TSE.TOPOLOGIA) || ',' || " + 
			" NVL(TSR.CDA,'') ";
			
	/**
	 * Constante para la consulta de exportar
	 */
	public static final String QUERY_CONSULTA_EXPORTAR_TODO = QUERY_CONSULTA_EXPORTAR+FILTROS_NORMAL;
	
	/**
	 * Constante para la consulta de exportar
	 */
	public static final String QUERY_CONSULTA_EXPORTAR_TODOHTO = QUERY_CONSULTA_EXPORTAR+FILTROS_HTO;
	
	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	public static final String COLUMNAS_CONSULTA_CAPTURAS_MANUALES = "CODIGO_ERROR,CONCEPTO_PAGO1,CONCEPTO_PAGO2,"
			+ "CVE_INST_ORD,CVE_INTERME_ORD, CVE_INTERME_REC,"
			+"CVE_RASTREO,CVE_RASTREO_ORI,ESTATUS_BANXICO,ESTATUS_TRANSFER,FCH_CAPTURA,FCH_OPERACION,FOLIO_PAGO,FOLIO_PAGO_DEV,"
			+"FOLIO_PAQ_DEV,FOLIO_PAQUETE,IVA,MONTO,MOTIVO_DEVOL,NOMBRE_ORD,NOMBRE_REC,NOMBRE_REC2,NUM_CUENTA_ORD,NUM_CUENTA_REC,"
			+"NUM_CUENTA_REC2,NUM_CUENTA_TRAN,PRIORIDAD,TIPO_OPERACION,REFE_NUMERICA,REFE_TRANSFER,CVE_TRANSFE,CVE_OPERACION,"
			+"CVE_MEDIO_ENT,TIPO_CUENTA_ORD,TIPO_CUENTA_REC,TIPO_CUENTA_REC2,TIPO_PAGO,TOPOLOGIA,CDA";

	/**
	 * Consulta de insercion en tabla con nombre de archivo de todos los envios
	 * exportados
	 */
	public static final StringBuilder EXPORT_TODOS = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");
	
	/**
	 * Insert que genera el estado de cuenta
	 */
	public static final String GENERAR_EDO_CTA = "INSERT INTO TRAN_SPEI_EDO_USUARIO "+
													" VALUES(SYSDATE,?,?,'PG',NULL,NULL)";
	
	
	/** constructor**/
	private UtileriasModuloSPEICero(){
		super();
    }
	

	
}