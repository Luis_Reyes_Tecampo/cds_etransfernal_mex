/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExportar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:04:33 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;

/**
 * Class UtileriasExportar.
 *
 * Clase de utilerias tipo singleton que contiene metodos
 * utilizados para los flujos de exportacion 
 * en las pantalles de detalle de monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public final class UtileriasExportar implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5525788833948124764L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasExportar instancia;
	
	/** La constante GUION_BAJO. */
	private static final String GUION_BAJO = "_";
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * Obetener la instacia singleton
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasExportar getUtils() {
		if (instancia == null) {
			instancia = new UtileriasExportar();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: modulo.
	 *
	 * Metodo para obtener el tipo de modulo
	 * SPEI o SPID
	 * 
	 * @param beanModulo El objeto: bean modulo
	 * @return El objeto: modulo
	 */
	public String getModulo(BeanModulo beanModulo) {
		String nombreModulo = ""; 
		if (ConstantesPOACOA.PAN_SPEI.equalsIgnoreCase(beanModulo.getModulo())) {
			nombreModulo = ConstantesPOACOA.MOD_SPEI;
		}
		if (ConstantesPOACOA.PAN_SPID.equalsIgnoreCase(beanModulo.getModulo())) {
			nombreModulo = ConstantesPOACOA.MOD_SPID;
		}
		return nombreModulo;
	}
	
	/**
	 * Obtener el objeto: monitor.
	 *
	 * Metodo para obtener el nombre del monitor 
	 * en el que esta el usuario.
	 * 
	 * @param beanModulo El objeto: bean modulo
	 * @return El objeto: monitor
	 */
	public String getMonitor(BeanModulo beanModulo) {
		String nombreMonitor = ""; 
		if (ConstantesPOACOA.POA.equalsIgnoreCase(beanModulo.getTipo())) {
			nombreMonitor = ConstantesPOACOA.MONITOR.concat(ConstantesPOACOA.POA);
		} else {
			nombreMonitor = ConstantesPOACOA.MONITOR.concat(ConstantesPOACOA.COA);
		}
		if (ConstantesPOACOA.PAN_SPEI.equalsIgnoreCase(beanModulo.getModulo())) {
			nombreMonitor = nombreMonitor.concat(GUION_BAJO).concat(ConstantesPOACOA.PAN_SPEI);
		} else {
			nombreMonitor = nombreMonitor.concat(GUION_BAJO).concat(ConstantesPOACOA.PAN_SPID);
		}
		return nombreMonitor;
	}
	
}
