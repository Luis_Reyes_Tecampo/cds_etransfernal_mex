package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class DatosMtoTemplates2.
 */
public class DatosMtoTemplates2 {
	
	/** La constante UTILS. */
	protected static final Utilerias utilerias = Utilerias.getUtilerias();
	
	/**
	 * Metodo que permite definir la lista de usuarios y lista de usuarios a eliminar en el objeto de peticion.
	 *
	 * @param beanReq  Objeto con los valores de la peticion
	 * @param usuariosAnexar Lista de usuarios que se anexaron
	 * @param usuariosEliminar Lista de usuarios eliminados
	 */
	public void establecerUsuariosEliminarAgregar(
			BeanReqAltaEditTemplate beanReq,
			List<BeanUsuarioAutorizaTemplate> usuariosAnexar,
			List<BeanUsuarioAutorizaTemplate> usuariosEliminar) {
		
		List<String> listaUsuarios = new ArrayList<String>();
		
		for(BeanUsuarioAutorizaTemplate elemento:usuariosEliminar){
			listaUsuarios.add(elemento.getUsrAutoriza());
		}
		
		beanReq.setListaUsuarios(listaUsuarios);
		beanReq.getTemplate().setUsuariosAutorizan(usuariosAnexar);
	}
	
	/**
	 * Metodo utilizado para establecer el error deseado y el tipo de mensaje.
	 *
	 * @param codError codigo del error a establecer
	 * @param beanViewRespuesta Con el valor establecido
	 */
	public void setError(String codError, BeanResPrincipalTemplate beanViewRespuesta) {
		beanViewRespuesta.setCodError(codError);
		beanViewRespuesta.setTipoError(Errores.TIPO_MSJ_ERROR); 
	}
	
	/**
	 * Metodo utilizado para establecer un error OK con Alerta.
	 *
	 * @param beanViewRespuesta Con el valor establecido
	 */
	public void setExitoAlert(BeanResPrincipalTemplate beanViewRespuesta){
		beanViewRespuesta.setCodError(Errores.OK00000V);
		beanViewRespuesta.setTipoError(Errores.TIPO_MSJ_ALERT); 
	}
	
	/**
	 * Metodo utilizado para establecer un error OK con Info.
	 *
	 * @param beanViewRespuesta Con el valor establecido
	 */
	public void setExitoInfo(BeanResPrincipalTemplate beanViewRespuesta){
		beanViewRespuesta.setCodError(Errores.OK00000V);
		beanViewRespuesta.setTipoError(Errores.TIPO_MSJ_INFO); 
	}
	
	/**
	 * Metodo utilizado para establecer un error de conexion con Msj de error.
	 *
	 * @param beanViewRespuesta Con el valor establecido
	 */
	public void setErrorConexion(BeanResPrincipalTemplate beanViewRespuesta){
		beanViewRespuesta.setCodError(Errores.EC00011B);
		beanViewRespuesta.setTipoError(Errores.TIPO_MSJ_ERROR); 
	}
	
	/**
	 * Metodo utilizado para establecer un error de conexion con Msj de error.
	 *
	 * @param beanViewRespuesta Con el valor establecido
	 */
	public void setNoExistenRes(BeanResPrincipalTemplate beanViewRespuesta){
		beanViewRespuesta.setCodError(Errores.ED00011V);
		beanViewRespuesta.setTipoError(Errores.TIPO_MSJ_INFO); 
	}
	
	/**
	 * Eliminar.
	 *
	 * @param beanResDAO the bean res dao
	 * @param beanViewResFinal the bean view res final
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	public List<Object> evaluarCodErrorEliminar(
			BeanResBase beanResDAO, BeanResPrincipalTemplate beanViewResFinal) throws BusinessException {
		List<Object> lista=new ArrayList<Object>();
		String estatus, codError = "";
		boolean hayErrores = false;
		if(Errores.EC00011B.equals(beanResDAO.getCodError())){
			throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
		}else if(Errores.CODE_SUCCESFULLY.equals(beanResDAO.getCodError())){
			setExitoAlert(beanViewResFinal);			
			estatus="OK";
		}else{
			codError=beanResDAO.getCodError();
			hayErrores=true;
			estatus="NOK";
		}
		lista.add(estatus);
		lista.add(hayErrores);
		lista.add(codError);
		return lista;
	}

	/**
	 * Valida codigo error dao.
	 *
	 * @param beanViewRes the bean view res
	 * @param beanResDAO the bean res dao
	 * @param isActualizaFecha the is actualiza fecha
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	public List<Object> validaCodigoErrorActualizaTmpl(BeanResPrincipalTemplate beanViewRes, BeanResBase beanResDAO, boolean isActualizaFecha) throws BusinessException {
		List<Object> lista=new ArrayList<Object>();
		boolean hayErroresProceso=false;
		String codError = "";
		if(Errores.EC00011B.equals(beanResDAO.getCodError())){
			throw new BusinessException(beanResDAO.getCodError(), beanResDAO.getMsgError());
		} else if(Errores.CODE_SUCCESFULLY.equals(beanResDAO.getCodError())){
			if(isActualizaFecha){
				setExitoInfo(beanViewRes);
			} else {
				setExitoAlert(beanViewRes);
			}
		}else{
			hayErroresProceso=true;
			codError = beanResDAO.getCodError();
		}
		lista.add(hayErroresProceso);
		lista.add(codError);
		
		return lista;
	}
	
	/**
	 * Existen errores proceso.
	 *
	 * @param beanViewRes the bean view res
	 * @param codErrorProceso the cod error proceso
	 * @param erroresProceso the errores proceso
	 */
	public void existenErroresProceso(BeanResPrincipalTemplate beanViewRes, String codErrorProceso, boolean erroresProceso){
		if(erroresProceso){
			setError(codErrorProceso, beanViewRes);
		}
	}
	
	/**
	 * Validar campos seleccionados.
	 *
	 * @param beanReq the bean req
	 * @param nuevoEstadoOperaciones the nuevo estado operaciones
	 * @return true, if successful
	 */
	public boolean validarCamposSeleccionados(BeanReqAltaEditTemplate beanReq, Boolean nuevoEstadoOperaciones){
		boolean condicionValida = false;
		if(!nuevoEstadoOperaciones && !beanReq.getListaCamposMaster().isEmpty()){
			condicionValida = true;
		}
		return condicionValida;
	}
}
