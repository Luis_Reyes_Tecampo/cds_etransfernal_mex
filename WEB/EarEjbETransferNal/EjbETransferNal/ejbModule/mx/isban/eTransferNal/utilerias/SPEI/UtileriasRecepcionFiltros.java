/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpHTO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:47:39 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

/**
 * en esta clase de utilerias se las funciones para realizar una devolucion extemporanea.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
public class UtileriasRecepcionFiltros  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -839585439824097676L;

	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionFiltros util = new UtileriasRecepcionFiltros();
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/**
	 * Obten la instancia de UtileriasRecepcionOpHTO.
	 *
	 * @return instancia de UtileriasRecepcionOpHTO
	 */
	public static UtileriasRecepcionFiltros getInstance(){
		return util;
	}
			 
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltro(BeanTranSpeiRecOper beanFiltro, String key) {
		// se inicializa las variables para el filtro
		filtro = new StringBuilder();
		first = true;
		
		if (beanFiltro.getBeanDetalle().getDetEstatus() != null && beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer() != null && !beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().isEmpty()) {
			getEstatusTransfer(beanFiltro,filtro, key);
		}
		if (beanFiltro.getBeanDetalle().getDetalleTopo() != null && beanFiltro.getBeanDetalle().getDetalleTopo().getTopologia() != null 
				&& !beanFiltro.getBeanDetalle().getDetalleTopo().getTopologia().isEmpty()) {
			getFiltroXParam(filtro, key, "TOPOLOGIA", beanFiltro.getBeanDetalle().getDetalleTopo().getTopologia());
		}
		if (beanFiltro.getBeanDetalle().getDetalleTopo() != null && beanFiltro.getBeanDetalle().getDetalleTopo().getTipoPago() != null && !beanFiltro.getBeanDetalle().getDetalleTopo().getTipoPago().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(TIPO_PAGO)", beanFiltro.getBeanDetalle().getDetalleTopo().getTipoPago());
		}
		if (beanFiltro.getBeanDetalle().getDetEstatus() != null && beanFiltro.getBeanDetalle().getDetEstatus().getEstatusBanxico() != null && !beanFiltro.getBeanDetalle().getDetEstatus().getEstatusBanxico().isEmpty()) {
			getFiltroXParam(filtro, key, "ESTATUS_BANXICO", beanFiltro.getBeanDetalle().getDetEstatus().getEstatusBanxico());
		}
		if (beanFiltro.getBeanDetalle().getReceptor() != null && beanFiltro.getBeanDetalle().getReceptor().getNumCuentaRec() != null && !beanFiltro.getBeanDetalle().getReceptor().getNumCuentaRec().isEmpty()) {
			getFiltroXParam(filtro, key, "TSR.NUM_CUENTA_REC", beanFiltro.getBeanDetalle().getReceptor().getNumCuentaRec());
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral() != null && beanFiltro.getBeanDetalle().getDetalleGral().getFchOperacion() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getFchOperacion().isEmpty()) {
			if(!first) {
				key = "AND";
			}
			filtro.append(key+" FCH_OPERACION = TO_DATE('"+beanFiltro.getBeanDetalle().getDetalleGral().getFchOperacion()+"','dd-mm-yyyy')");
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral() != null && beanFiltro.getBeanDetalle().getDetalleGral().getEstatusOpe() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getEstatusOpe().isEmpty()) {
			getFiltroXParam(filtro, key, "ESTATUS", beanFiltro.getBeanDetalle().getDetalleGral().getEstatusOpe());
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral() != null && beanFiltro.getBeanDetalle().getDetalleGral().getCveTransfer() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getCveTransfer().isEmpty()) {
			getFiltroXParam(filtro, key, "CVE_TRANSFE", beanFiltro.getBeanDetalle().getDetalleGral().getCveTransfer());
		}
        if (beanFiltro.getBeanDetalle().getDetalleGral() != null && beanFiltro.getBeanDetalle().getDetalleGral().getDesCveTransf() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getDesCveTransf().isEmpty()) {
        	getFiltroXParam(filtro, key, "TT.DESCRIPCION", beanFiltro.getBeanDetalle().getDetalleGral().getDesCveTransf());
        }
		
		//se llaman la funciones
		getFiltr(beanFiltro, key);		
		getFiltros(beanFiltro,key);
		// se regresa el filtro armando
		return filtro.toString();
	}	
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	private String getFiltr(BeanTranSpeiRecOper beanFiltro, String key) {
		if (beanFiltro.getBeanDetalle().getDetalleGral().getCveMiInstitucion() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getCveMiInstitucion().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(CVE_MI_INSTITUC)", beanFiltro.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral().getCveInstOrd() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getCveInstOrd().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(CVE_INST_ORD)", beanFiltro.getBeanDetalle().getDetalleGral().getCveInstOrd());
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral().getFolioPaquete() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getFolioPaquete().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(FOLIO_PAQUETE)", beanFiltro.getBeanDetalle().getDetalleGral().getFolioPaquete());
		}		
		
		if (beanFiltro.getBeanDetalle().getBeanCambios().getMotivoDevol() != null && !beanFiltro.getBeanDetalle().getBeanCambios().getMotivoDevol().isEmpty() && 
				!ConstantesRecepOperacionApartada.TODAS.equals(beanFiltro.getBeanDetalle().getBeanCambios().getMotivoDevol())) {
				getFiltroXParam(filtro, key, "MOTIVO_DEVOL", beanFiltro.getBeanDetalle().getBeanCambios().getMotivoDevol());
			
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral().getFolioPago() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getFolioPago().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(FOLIO_PAGO)", beanFiltro.getBeanDetalle().getDetalleGral().getFolioPago());
		}
		
				
		return filtro.toString();
	}	
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltros(BeanTranSpeiRecOper beanFiltro, String key) {
		
		if (beanFiltro.getBeanDetalle().getOrdenante() != null && beanFiltro.getBeanDetalle().getOrdenante().getMonto() != null && !beanFiltro.getBeanDetalle().getOrdenante().getMonto().isEmpty()) {
			if(!first) {
				key = "AND";
			}
			filtro.append(key+" TO_CHAR(MONTO, '999999999990.00') LIKE '%"+beanFiltro.getBeanDetalle().getOrdenante().getMonto()+"%'");
		}
		if (beanFiltro.getBeanDetalle().getDetEstatus() != null && beanFiltro.getBeanDetalle().getDetEstatus().getRefeTransfer() != null && !beanFiltro.getBeanDetalle().getDetEstatus().getRefeTransfer().isEmpty()) {
			getFiltroXParam(filtro, key, "TO_CHAR(REFE_TRANSFER)", beanFiltro.getBeanDetalle().getDetEstatus().getRefeTransfer());
		}
		if (beanFiltro.getBeanDetalle().getDetalleGral() != null && beanFiltro.getBeanDetalle().getDetalleGral().getCveRastreo() != null && !beanFiltro.getBeanDetalle().getDetalleGral().getCveRastreo().isEmpty()) {
			getFiltroXParam(filtro, key, "CVE_RASTREO", beanFiltro.getBeanDetalle().getDetalleGral().getCveRastreo());
		}
		if (beanFiltro.getBeanDetalle().getCodigoError() != null && !beanFiltro.getBeanDetalle().getCodigoError().isEmpty()) {
			getFiltroXParam(filtro, key, "CODIGO_ERROR", beanFiltro.getBeanDetalle().getCodigoError());
		}
		
		if (beanFiltro.getBeanDetalle().getBeanCambios() != null && beanFiltro.getBeanDetalle().getBeanCambios().getOtros() != null && beanFiltro.getBeanDetalle().getBeanCambios().getOtros().getDescTipoPgo() != null 
				&& !beanFiltro.getBeanDetalle().getBeanCambios().getOtros().getDescTipoPgo().isEmpty() && !ConstantesRecepOperacionApartada.TODAS.equals(beanFiltro.getBeanDetalle().getBeanCambios().getOtros().getDescTipoPgo())) {
			getFiltroXParam(filtro, key, "TTP.DESCRIPCION", beanFiltro.getBeanDetalle().getBeanCambios().getOtros().getDescTipoPgo());
		}
				
		return filtro.toString();
	}	
	
	
	/**
	 * Obtener el objeto: estatus transfer.
	 *
	 * @param beanFiltro El objeto: bean filtro
	 * @param filtro El objeto: filtro
	 * @param key El objeto: key
	 * @return El objeto: estatus transfer
	 */
	private void getEstatusTransfer(BeanTranSpeiRecOper beanFiltro,StringBuilder filtro, String key) {
		String dto="";
		// filtro para las topologias V Liquidadas
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.TVL)) {
			dto = " ESTATUS_TRANSFER NOT IN('TR','DV','RE') AND  TOPOLOGIA = 'V' AND ESTATUS_BANXICO = 'LQ' ";
		}
		//filtro para las topologia T liquidadas
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.TTL)) {
			dto = " ESTATUS_TRANSFER NOT IN('TR','DV','RE') AND TOPOLOGIA ='T' AND estatus_banxico = 'LQ' ";
		}
		//filtro para la topologia por liquidar
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.TTXL)) {
			dto =  " ESTATUS_TRANSFER NOT IN('TR','DV','RE') AND TOPOLOGIA = 'T' AND estatus_banxico = 'PL' ";
		}
		//filtro para la topologian de rechazados
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.R)) {
			dto = " ESTATUS_TRANSFER = 'RE'  ";
		}
		// filtro para los enviados a transfer
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.APT)) {
			dto = " ESTATUS_TRANSFER = 'TR' ";
		}
		//filtro para las devoluciones
		if(beanFiltro.getBeanDetalle().getDetEstatus().getEstatusTransfer().equals(ConstantesRecepOperacionApartada.DEV)) {
			dto = " ESTATUS_TRANSFER ='DV' " ;
		}
				
		
		
		// concatena el valor al query
		if (first && dto.length()>0) {
			filtro.append(key + dto);
			first = false;
		} else {
			filtro.append(dto);
		}
		
	}
	
	/**
	 * Obtener el objeto: filtro X parametro.
	 *
	 * @param filtro El objeto: filtro Actual
	 * @param key El objeto: key
	 * @param param El objeto: param
	 * @param value El objeto: value
	 * @return El objeto: filtro X param
	 */
	public void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
}
