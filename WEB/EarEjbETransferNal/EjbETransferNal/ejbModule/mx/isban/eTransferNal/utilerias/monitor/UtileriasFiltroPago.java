/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasFiltroPago.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   29/01/2019 04:59:48 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.utilerias.monitor;



import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;

/**
 * The Class UtileriasFiltroPago.
 *
 * @author FSW-Vector
 * @since 29/01/2019
 */
public class UtileriasFiltroPago  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -6769482385375882746L;
	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasFiltroPago util = new UtileriasFiltroPago();
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasFiltroPago getInstance(){
		return util;
	}
			 
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: beanFiltro
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltro(BeanPago beanFiltro, String key) {
		filtro = new StringBuilder();
		Object valor = null;
		
		valor = beanFiltro.getReferencia();
		filtro.append(getFiltroXParam("REFERENCIA", valor, 1));
		
		valor = beanFiltro.getCveCola();
		filtro.append(getFiltroXParam("CVE_COLA", valor, 1));
		
		valor = beanFiltro.getCveTran();
		filtro.append(getFiltroXParam("CVE_TRANSFE", valor, 1));
		
		valor = beanFiltro.getCveMecanismo();
		filtro.append(getFiltroXParam("CVE_MECANISMO", valor, 1));
		
		valor = beanFiltro.getCveIntermeOrd();
		filtro.append(getFiltroXParam("CVE_INTERME_ORD", valor, 1));
		
		valor = beanFiltro.getCveIntermeRec();
		filtro.append(getFiltroXParam("CVE_INTERME_REC", valor, 1));
		
		valor = beanFiltro.getImporteAbono();
		filtro.append(getFiltroXParam("IMPORTE_ABONO", valor, 2));
	
		return filtro.toString();
	}
	
	/**
	 * Obtener el objeto: filtro X parametro.
	 *
	 * @param param El objeto: param
	 * @param valor El objeto: valor
	 * @param tipoDato El objeto: tipo dato. 0 = Cadena, 1 = Numerico, 2 = Decimal
	 * @return El objeto: filtro X param
	 */
	private String getFiltroXParam(String param, Object valor, int tipoDato) {
		String filtro = "";
		if (valor != null && !valor.toString().isEmpty()) {
			if (tipoDato == 2) {
				filtro = " AND TO_CHAR("+param+",'999999999999999999999999999999.99') = TO_CHAR('"+valor+"','999999999999999999999999999999.99')";
			} else if(tipoDato == 1) {
				filtro = " AND "+param+" = '"+valor+"'";
			} else {
				filtro = " AND UPPER("+param+") LIKE UPPER('%"+valor+"%')";				
			}
		}
		return filtro;
	}
	
	/**
	 * Valida mecanismo.
	 *
	 * Valida el mecanismo para el monitor SPEI y CA
	 * 
	 * @param mecanismo El objeto: mecanismo
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto string
	 */
	public String validaMecanismo(String mecanismo, String nomPantalla) {
		String nuevoMecanismo = "";
		if (nomPantalla.equals(ConstantesMonitor.SPID)) {
			nuevoMecanismo = mecanismo;
		}
		if (nomPantalla.equals(ConstantesMonitor.SPEI)) {
			nuevoMecanismo = getMecanismoSPEI(mecanismo);
		}
		if (nomPantalla.equals(ConstantesMonitor.ADMON)) {
			nuevoMecanismo = getMecanismoADMON(mecanismo);
		}
		return nuevoMecanismo;
	}

	/**
	 * Obtener el objeto: mecanismo SPEI.
	 *
	 * Obtiene el mecanismo de SPEI
	 * 
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @return El objeto: mecanismo SPEI
	 */
	private String getMecanismoSPEI(String cveMecanismo) {
		String mecanismoSPEI = cveMecanismo;
		if ("TRSPISIA".equalsIgnoreCase(mecanismoSPEI)) {
			mecanismoSPEI = "TRSPESIA";
		}
		if (ConstantesMonitor.SPID.equalsIgnoreCase(mecanismoSPEI)) {
			mecanismoSPEI = ConstantesMonitor.SPEI;
		}
		if ("DEVSPID".equalsIgnoreCase(mecanismoSPEI)) {
			mecanismoSPEI = "DEVSPEI";
		}
		return mecanismoSPEI;
	}
		
	/**
	 * Obtener el objeto: mecanismo ADMON.
	 *
	 * Obtiene el mecanismo de CA
	 * 
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @return El objeto: mecanismo ADMON
	 */
	private String getMecanismoADMON(String cveMecanismo) {
		String mecanismoAMON = cveMecanismo;
		if ("TRSPISIA".equalsIgnoreCase(mecanismoAMON)) {
			mecanismoAMON = "TRSPEICA";
		}
		if (ConstantesMonitor.SPID.equalsIgnoreCase(mecanismoAMON)) {
			mecanismoAMON = ConstantesMonitor.SPEICA;
		}
		if ("DEVSPID".equalsIgnoreCase(mecanismoAMON)) {
			mecanismoAMON = "DVSPEICA";
		}
		return mecanismoAMON;
	}
	
}
