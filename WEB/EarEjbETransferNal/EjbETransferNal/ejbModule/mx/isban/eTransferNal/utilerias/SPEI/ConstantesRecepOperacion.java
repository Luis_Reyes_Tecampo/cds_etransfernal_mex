/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesRecepOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:41:58 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

/**
 * Clase de constantes que tiene todos los parametro  y consultas que se utilizan para 
 * mostrar la informacion de las pantalla de recepcion de operaciones  de cuenta alternas y principales
 * 
 * tambien contiene actualizacione de  operaciones como la consulta para mostrar los resportes.
 *
 * @author FSW-Vector
 * @since 28/02/2019
 */

public final class ConstantesRecepOperacion {
	
	
	
	/** VALOR DE LA COSNTANTE PI_H_NOC_FIN_REC. */
	public static final String PI_H_NOC_FIN_REC = "PI_H_NOC_FIN_REC";
	
	/** VALOR DE LA COSNTANTE PPI_H_NOC_FIN_DEV. */
	public static final String PI_H_NOC_FIN_DEV = "PI_H_NOC_FIN_DEV";
	
	/** VALOR DE LA COSNTANTE PI_H_NOC_FIN_R_N. */
	public static final String PI_H_NOC_FIN_R_N = "PI_H_NOC_FIN_R_N";
	
	/** VALOR DE LA COSNTANTE PI_H_NOC_FIN_D_N. */
	public static final String PI_H_NOC_FIN_D_N = "PI_H_NOC_FIN_D_N";
	
	/** VALOR POR DEFAULT DE VD_PI_H_NOC_FIN_REC. */
	public static final String VD_PI_H_NOC_FIN_REC = "05:59:59";
	
	/** VALOR POR DEFAULT DE VD_PPI_H_NOC_FIN_DEV. */
	public static final String VD_PI_H_NOC_FIN_DEV = "06:01:00";
	
	/** VALOR POR DEFAULT DE VD_PI_H_NOC_FIN_R_N. */
	public static final String VD_PI_H_NOC_FIN_R_N = "08:30:00";
	
	/** VALOR POR DEFAULT DE VD_PI_H_NOC_FIN_D_N. */
	public static final String VD_PI_H_NOC_FIN_D_N = "08:35:00";
	
	/** variable que guarda PI_T_MAX_DEV_ENV. */
	public static final String PI_T_MAX_DEV_ENV = "PI_T_MAX_DEV_ENV";
	
	/** variable que guarda  el valor por defaul PI_T_MAX_DEV_ENV. */
	public static final String VD_PI_T_MAX_DEV_ENV = "60";	
	
	/** variable que guarda PI_T_MAX_DEV_ENV. */
	public static final String PI_T_MAX_DEV_E_M = "PI_T_MAX_DEV_E_M";
	
	/** variable que guarda  el valor por defaul PI_T_MAX_DEV_ENV. */
	public static final String VD_PI_T_MAX_DEV_E_M = "10";
	
	/** variable que guarda PI_TASA_POND_FON. */
	public static final String PI_TASA_POND_FON = "PI_TASA_POND_FON";
	
	/** variable que guarda PI_T_MAX_DEV_ENV. */
	public static final String PI_PAGO_MIN_CFCH = "PI_PAGO_MIN_CFCH";
	
	/** variable que guarda PI_T_MAX_DEV_ENV. */
	public static final String PI_DIVISOR = "PI_DIVISOR";
	
	/** TEXTO PARA EL PAGO. */
	public static final String CVEPAGO="TRANSITO";
	
	/** TEXTO PARA LA CLAVE DE LA OPERACION. */
	public static final String CVEOPERACION="DEVEXTEM";
	
	/** The Constant TRI_B0000. */
	public static final String TRI_B0000 = "TRIB0000";
	
	/** Variable para el estatus*. */
	public static final boolean ENVIOESTATUS = true;
	
	
	/** variable para el 00. */
	public static final String CEROS="00";
	
	/** VARIABLE PARA WHERE *. */
	public static final String WHERE = " WHERE ";
	
	/**  *. */
	public static final String FECHA_OPERACION=" AND FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') ";
	
	
	/** ACTUALIZACION A LA TABLA tran_spei_rec_his. */
	public static final String QUERY_UPDATE_TRANSPEINULL ="UPDATE tran_spei_rec SET cve_para_pago = 'TRANSITO' " + 
			WHERE+" estatus_transfer = 'RE' " + 
			" AND cve_mi_instituc = ? " + 
			" AND cve_inst_ord = ? " +
			" AND  fch_operacion = TO_DATE(?,'dd-mm-yyyy')  " +
			" AND codigo_error is null ";
	
	/** ACTUALIZACION A LA TABLA tran_spei_rec_his. */
	public static final String QUERY_UPDATE_TRANSPEI ="UPDATE tran_spei_rec SET cve_para_pago = 'TRANSITO' " + 
			WHERE+"  estatus_transfer = 'RE' " + 
			" AND cve_mi_instituc = ? " + 
			" AND cve_inst_ord = ? " +
			" AND  fch_operacion = TO_DATE(?,'dd-mm-yyyy')  "+ 
			" AND codigo_error=? ";
	
	/** continua filtro. */
	public static final String FILTRO_PAGO=" AND folio_pago = ? ";
	
	/** continua filtro. */
	public static final String FILTRO_PAQUETE=" AND folio_paquete = ? ";	

	/** The Constant QUERY_INSERT_INTERESES. */
	public static final String QUERY_INSERT_INTERESES ="INSERT INTO tran_spei_interes(fch_operacion,"+
			"cve_inst_ord,cve_inst_benef,folio_paquete,folio_pago,cta_movil,tipo,estatus,cve_transfe," + 
			"codigo_error,num_cuenta_ord,num_cuenta_rec,tipo_pago,referencia,referencia_interes,dif_captura_recepcion," + 
			"tiempo_retraso,monto,monto_interes,fch_captura,fch_recepcion,cve_rastreo,nombre_ord,nombre_rec) " + 
			" VALUES (TO_DATE(?,'DD-MM-YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,(SYSDATE-TO_DATE(?,'DD-MM-YYYY'))*86400," + 
			" ?,?,?,SYSDATE,TO_DATE(?,'DD-MM-YYYY'),?,?,?)";
	
	/** query para buscar la operacion. */
	public static final String QUERY_OPERACIONES="select TO_CHAR(TO_DATE(?,'dd-mm-yyyy'),'YYYYMMDD HH24:MI:SS') FECHA_RECEPCION, " + 
			"ROUND(((SYSDATE - TO_DATE(REPLACE(?,'-',''),'DDMMYYYY HH24:MI:SS'))*86400),2) SQL1, " + 
			"ROUND(((SYSDATE - TO_DATE(REPLACE(?,'-',''),'DDMMYYYY HH24:MI:SS'))*86400),2) SQL2, " + 
			"ROUND(((SYSDATE - TO_DATE(?,'dd-mm-yyyy') ) * 86400),2) SQL3 " + 
			"from tran_spei_rec_his " + 
			WHERE+"fch_operacion = TO_DATE(?,'dd-mm-yyyy')  AND cve_mi_instituc =?  AND cve_inst_ord = ?  AND estatus_transfer = ?   " + 
			"AND folio_pago = ? AND folio_paquete = ? and cve_para_pago= 'TRANSITO'";
	
	/** CONSULTA PARA OBTENER EL VALOR_FALTA DE LA TABLA TRAN_PARAMETROS. */
	public static final String QUERY_VALOR_FALTA= " SELECT valor_falta VALOR  FROM tran_parametros "+WHERE+" cve_parametro =  ?" ;
	
	/** Propiedad del tipo String que almacena el valor de QUERY_CONS_MI_INSTITUCION. */
	public static final String QUERY_CONS_MI_INSTITUCION = " SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+WHERE+" TRIM(CV_PARAMETRO)  = ? ";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_DEVOLUCIONES. */
	public static final String QUERY_DEVOLUCIONES = 
		"SELECT TRIM(CODIGO_GLOBAL) SECUENCIAL, DESCRIPCION FROM COMU_DETALLE_CD "+WHERE+" TIPO_GLOBAL='TR_DEVSPEI'";
	
	/** Propiedad del tipo String que almacena el valor de QUERY_BUSQUEDA. */
	public static final String QUERY_BUSQUEDA = 
		"SELECT CVE_PARAM_FALTA SECUENCIAL,DESCRIPCION FROM TRAN_PARAMETROS "+WHERE+"  CVE_PARAMETRO like '%ESTATRECEP%'";
		
	/** Propiedad del tipo String que almacena el valor de QUERY_UPDATE_CTA_RECEPTORA. */
	public static final String QUERY_UPDATE_CTA_RECEPTORA = "UPDATE TRAN_SPEI_REC " + 
				 " SET NUM_CUENTA_REC = ? ";
	
	/** variable para la cve de institucion*. */
	public static final String CVEINSTITUCION="CVE_MI_INSTITUC =  ? ";
	
	/** contiene el filtro de la consulta. */
	public static final String FILTROS= WHERE+"FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') "
			 + " AND "+CVEINSTITUCION 
			 + " AND FOLIO_PAQUETE = ? "
			 + " AND FOLIO_PAGO = ? ";
	
	/** consulta general *. */
	public static final String QUERY_FECHA_OPERACION_SPID =
	" SELECT FCH_OPERACION FROM TRAN_SPEI_CTRL CTL,"+
	" (SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX "+
	WHERE+" CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP "+
	WHERE +" CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";
	
	/** variables para la consulta transferirSpeiRec*. */
	public static final String TRANSFERSPEIREC ="transferirSpeiRec";
	
	/** variables para la consulta transferirSpeiRec*. */
	public static final String DELVTRANSFERSPEIREC ="devolverTransferSpeiRec";
	
	/** variables para la consulta recuperarTransfSpeiRec*. */
	public static final String RECTRANSFERSPEIREC= "recuperarTransfSpeiRec";
	
	/** variables para la consulta recuperarTransfSpeiRec*. */
	public static final String ACTMOTREZCHAZ= "actMotRechazo";
	
	/** variables para la consulta devolucionExtemporanea*. */
	public static final String DEVEXT="devolucionExtemporanea";
	
	/** Variable para el estatus*. */
	public static final boolean NOENVIOESTATUS = false;
		
	/** The Constant URL_TRANSFERIRS_SPEI. */
	public static final String URL_TRANSFERIRS_SPEI = "transferirSpeiRec.do";
	
	/** The Constant UPDATE. */
	public static final String ENVTRAN ="ENV_TRAN";
	
	/** The Constant Tran_Spei_Horas_Man. */
	public static final String TRANSPEIHORASMAN = "TRAN_SPEI_HORAS_MAN";

	/** The Constant Envio. */
	public static final String ENVIO = "ENVIO";
	
	/** The Constant ERROR. */
	public static final String ERROR = "ERROR";
	
	/** The Constant EXITO. */
	public static final String EXITO = "EXITO";
	
	/** The Constant UPDATE. */
	public static final String DEVTRAN = "DEV_TRAN";
	
	/** The Constant DEV_TRAN. */
	public static final String UPDATE = "UPDATE";
	
	/** The Constant TRAN_SPEI_REC. */
	public static final String TRAN_SPEI_REC = "TRAN_SPEI_REC";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_TR. */
	public static final String ESTATUS_TR ="TR";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_DV. */
	public static final String ESTATUS_DV ="DV";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_RE. */
	public static final String ESTATUS_RE ="RE";
	
	/** Propiedad del tipo String que almacena el valor de TOPOLOGIA_V. */
	public static final String TOPOLOGIA_V ="V";
	
	/** Propiedad del tipo String que almacena el valor de TOPOLOGIA_T. */
	public static final String TOPOLOGIA_T ="T";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_BANXICO_LQ. */
	public static final String ESTATUS_BANXICO_LQ ="LQ";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_BANXICO_PL. */
	public static final String ESTATUS_BANXICO_PL ="PL";
	
	/** Propiedad del tipo String que almacena el valor de ESTATUS_BANXICO_PL. */
	public static final String CONSULTA_ERROR ="SELECT MSG_ERROR,COD_ERROR from COMU_ERROR where COD_ERROR= ?";
	
	
	/**  Propiedad del tipo String que almacena el valor de TODAS. */
	public static final String TODAS ="TODAS";
	
	/**  Propiedad del tipo String que almacena el valor de TVL. */
	public static final String TVL ="TVL";
	
	/**  Propiedad del tipo String que almacena el valor de TTL. */
	public static final String TTL = "TTL";
	
	/**  Propiedad del tipo String que almacena el valor de TTXL. */
	public static final String TTXL = "TTXL";
	
	/**  Propiedad del tipo String que almacena el valor de R. */
	public static final String R = "R";
	
	/**  Propiedad del tipo String que almacena el valor de APT. */
	public static final String APT = "APT";
		
	/**  Propiedad del tipo String que almacena el valor de DEV. */
	public static final String DEV = "DEV";
	
	/**  Propiedad del tipo String que almacena el valor de RMD. */
	public static final String RMD = "RMD";
	
	/**  Propiedad del tipo String que almacena el valor de CONT. */
	public static final String CONT = "CONT";
	
	/** La constante FCH_OPERACION. */
	public static final String FCH_OPERACION="FCH_OPERACION";
	
	/** La constante CVE_MI_INSTITUC. */
	public static final String CVE_MI_INSTITUC="CVE_MI_INSTITUC";
	
	/** La constante FOLIO_PAQUETE. */
	public static final String FOLIO_PAQUETE="FOLIO_PAQUETE";
	
	/** La constante CVE_INST_ORD. */
	public static final String CVE_INST_ORD="CVE_INST_ORD";
	
	/** La constante FOLIO_PAGO. */
	public static final String FOLIO_PAGO="FOLIO_PAGO";
	
	/** La constante USUARIO_REGISTRA. */
	public static final String USUARIO_REGISTRA="USUARIO_REGISTRA";
	
	public static final String QUERY_CONSULTA_TIPO_PAGO = "SELECT TIPO_PAGO secuencial, DESCRIPCION FROM tran_tipo_pago ORDER BY DESCRIPCION";
	
	
	/** La constante QUERY_CONSULTA_REF. */
	public static final String QUERY_CONSULTA_REF = "SELECT REFE_TRANSFER, ESTATUS_TRANSFER, FOLIO_PAQUETE, FOLIO_PAGO FROM TRAN_SPEI_REC WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yy') AND CVE_MI_INSTITUC = ? AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ?";
	
	/** La constante TRANSFERENCIA. */
	public static final String REFERENCIA = "REFE_TRANSFER";
	
	/** La constante ESTATUS. */
	public static final String ESTATUS = "ESTATUS_TRANSFER";
	
	/** La constante PAQUETE. */
	public static final String PAQUETE = "FOLIO_PAQUETE";
	
	/** La constante PAGO. */
	public static final String PAGO = "FOLIO_PAGO";
	/**
	 *  
	 * constructor.
	 */
	 private ConstantesRecepOperacion() {
	 }
}
