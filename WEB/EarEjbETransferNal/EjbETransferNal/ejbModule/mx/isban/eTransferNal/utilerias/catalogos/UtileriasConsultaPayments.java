package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * Class UtileriasConsultaPayments.
 * 
 * Clase que contiene metodos de utilerias que ayudan al negocio con el manejo de datos 
 * que pasan por dicha capa
 *
 * @author FSW-SNG Enterprise
 * @since 12/12/2019
 */
public class UtileriasConsultaPayments extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 509418768331384466L;

	/** Referencia del Objeto Utilerias. */
	protected static final UtileriasConsultaPayments UTILERIAS = new UtileriasConsultaPayments();

	/** Referencia del Objeto instancia. */
	protected static UtileriasConsultaPayments instancia;
	
	/** La constante BAJA. */
	public static final String BAJA = "baja";

	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";
	
	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
	
	/** La constante CONSULTA. */
	public static final String CONSULTA = "consulta";
	
	/** La constante CONSULTA. */
	public static final String CONSULTA_PK = "consultaPK";
	
	/** La constante OK. */
	public static final String OK = "OK";

	/** La constante NOK. */
	public static final String NOK = "NOK";
	
	/** La constante FILTROLK. */
	public static final String FILTROLK  = "\\[FILTRO_LK\\]";
	
	/** La constante FILTROAND. */
	public static final String FILTROAND = "\\[FILTRO_AND\\]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY RN";

	/** La constante CONSULTA_EXPORTAR_TODOs. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT CVE_MEDIO_ENT||','||DESCRIPCION||','||CASE CTRL_DUPLICADOS  "
			+ "WHEN '0' THEN 'DESACTIVADO' WHEN '1' THEN 'ACTIVADO' END AS CTRL_DUPLICADOS FROM COMU_MEDIOS_ENT";

	

	/**
	 * Obtener el objeto: UTILERIAS.
	 * 
	 * @return El objeto: UTILERIAS
	 */
	public static UtileriasConsultaPayments getUtilerias() {
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: instancia.
	 * 
	 * @return El objeto: instancia
	 */
	public static UtileriasConsultaPayments getInstancia(){
		if(instancia==null){
			instancia = new UtileriasConsultaPayments();
		}
		return instancia;
	}
	
	
	/**
	 * Obtener el objeto: responseDAO.
	 * @param responseDAO de ResponseMessageDataBaseDTO
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO){
		BeanResBase error = new BeanResBase();
		if(responseDAO == null){
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	/**
	 * Agregar parametros metodo que va acomulando los campo para modificar, consulta canal id unico.
	 * @param beanConsultaPayments la variable beanConsultaPayments para obtener el campo llave
	 * @param operacion variable operacion, para determinar el ABC
	 * @return parametros Objeto list
	 */
	public List<Object> agregarParametros(BeanConsultaPayme beanConsultaPayments, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if (operacion.equalsIgnoreCase(MODIFICACION)) {
			parametros.add(beanConsultaPayments.getActDes());
			parametros.add(beanConsultaPayments.getClave());
		} else if (operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
			parametros.add(beanConsultaPayments.getClave());
		}
		return parametros;
	}
	
	/**
	 * Obtener el objeto Sql de Catalagos Obtiene el Bean SQL
	 * @param beanFiltro la variable beanFiltro, contiene los atributos a ser accesados
	 * @param sql la variable que contiene la  cadena a remplasar 
	 * @return newSql objeto newSql
	 */
	public String getSqlCatalogo(BeanConsultaPayme beanFiltro, String sql) {
		String newSql = null;
		
		if (beanFiltro.getTipoOperacion() != null && !beanFiltro.getTipoOperacion().isEmpty()) {
			newSql = sql.replaceAll(FILTROLK, "'" + beanFiltro.getTipoOperacion() + "%'");

		}else{
			newSql = sql.replaceAll(FILTROLK, "'%'");
		}
		
		return buildFiltro(beanFiltro, newSql);
	}
	
	/**
	 * Obtener el objeto Sql de Catalagos Obtiene el Bean SQL
	 * @param beanFiltro la variable beanFiltro, contiene los atributos a ser accesados
	 * @param sql la variable que contiene la  cadena a remplasar 
	 * @return newSql objeto newSql
	 */
	public String buildFiltro(BeanConsultaPayme beanFiltro, String sql) {
		String newSql = null;
		if (beanFiltro.getClave() != null && !beanFiltro.getClave().isEmpty()) {
			newSql = sql.replaceAll(FILTROAND, " AND CVE_MEDIO_ENT = '"+ beanFiltro.getClave() + "'");
		}else{
			newSql = sql.replaceAll(FILTROAND, " ");
		}
		return newSql;
	}
	
	/**
	 * Metodo para ir concatenando los filtros del paginador
     * @param beanPaginador paginador de la vista
     * @return parametros recibidos
	 */
	public List<Object> getPaginadorXParam(BeanPaginador beanPaginador) {
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(beanPaginador.getRegIni());
		parametros.add(beanPaginador.getRegFin());
		return parametros;
	}
	
	/**
	 * Obtener el objeto Sql del combo Obtiene el Bean SQL
	 * @param beanFiltro la variable beanFiltro, contiene los atributos a ser accesados
	 * @param sql la variable que contiene la  cadena a remplasar 
	 * @return newSql objeto newSql
	 */
	public String getSqlCombo(BeanConsultaPayme beanFiltro, String sql) {
		String newSql = null;
		if (beanFiltro.getTipoOperacion() != null && !beanFiltro.getTipoOperacion().isEmpty()) {
			newSql = sql.replaceAll("\\[FILTRO_CD\\]", "'" + beanFiltro.getTipoOperacion() + "%'");
		}else{
			newSql = sql.replaceAll("\\[FILTRO_CD\\]", "'%'");
		}
		

		return newSql;
	}
}
