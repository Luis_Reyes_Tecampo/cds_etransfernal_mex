/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPistasMtoTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Thu Apr 06 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.utilerias;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayoutTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAltaEditTemplateDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.dao.capturasManuales.DAOMantenimientoTemplatesExternalImpl;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Clase Utileria para uso de datos en la capa de DAO y BO.
 */
public class UtileriasDatosMtoTemplate extends DatosMtoTemplates2{

	/** Constante para imprimir el log en consola */
	private static final Log LOG = LogFactory.getLog(UtileriasDatosMtoTemplate.class);
	
	/** Referencia del Objeto Utilerias. */
	private static final UtileriasDatosMtoTemplate UTILERIAS_INSTACE = new UtileriasDatosMtoTemplate();
	
	/**  Constante para referenciar el ID_TEMPLATE. */
	private static final String ID_TEMPLATE = "ID_TEMPLATE";
	
	/**  Constante para referenciar el ID_LAYOUT. */
	private static final String ID_LAYOUT="ID_LAYOUT";
	
	/**  Constante para referenciar el formato de la fecha. */
	private static final String FORMAT_DAY="dd/MM/yyyy";
	
	/**  Constante para referenciar el formato de la fecha. */
	private static final String ALTERNATIVE_DAY="yyyy-MM-dd";

	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasDatosMtoTemplate getUtilerias(){
		return UTILERIAS_INSTACE;
	}

	/**
	 * Metodo que permite obtener la informacion de un template del responseDTO.
	 *
	 * @param responseDTO Objeto con la ejecucion de la consulta y los resultado
	 * @param listaTemplates Objeto destino donde se colocara la informaicon
	 */
	public void extraerDatosTemplates(ResponseMessageDataBaseDTO responseDTO,
			List<BeanTemplate> listaTemplates) {
		//Se crean objetos de entrada y salida
		List<HashMap<String, Object>> listMapRes = new ArrayList<HashMap<String,Object>>();
		listMapRes = responseDTO.getResultQuery();
		SimpleDateFormat inputFormato = new SimpleDateFormat(ALTERNATIVE_DAY);
		SimpleDateFormat outFormato = new SimpleDateFormat(FORMAT_DAY);
		Date ini =null;
		
		//Se recorre el mapa
		for(HashMap<String,Object> map:listMapRes){
			 BeanTemplate entidad = new BeanTemplate();
			 entidad.setIdRegistro(utilerias.getInteger(map.get("ID_REG")));					 
			 entidad.setIdTemplate(utilerias.getString(map.get(ID_TEMPLATE)));
			 
			 entidad.setIdLayout(utilerias.getString(map.get(ID_LAYOUT)));
				 
			 entidad.setEstatus(utilerias.getString(map.get("ESTATUS")));
			 entidad.setUsrAlta(utilerias.getString(map.get("USR_ALTA")));
			 
			 entidad.setFechaCaptura(utilerias.getString(map.get("FEC_CAPTURA")));
			 entidad.setFechaModifica(utilerias.getString(map.get("FEC_MODIFICA")));
			 
			//Se valida fecha de captura
			 if(entidad.getFechaCaptura()!=null  && !"".equals(entidad.getFechaCaptura()) ) {
				 try {
					ini =  new Date();
					ini = inputFormato.parse(entidad.getFechaCaptura());
					entidad.setFechaCaptura(outFormato.format(ini));
				} catch (ParseException e) {
					LOG.error("Error", e);
					LOG.error("Ha ocurrido un error al obtener fecha de captura");
					LOG.error(String.format("Mensaje de error: %s", e.getMessage()));
				}
			 }
			 
			 if(entidad.getFechaModifica()!=null  && !"".equals(entidad.getFechaModifica()) ){
				 try {
					 ini =  new Date();
					 ini = inputFormato.parse(entidad.getFechaModifica());
					 entidad.setFechaModifica(outFormato.format(ini));
				} catch (ParseException e) {
					LOG.error("Error", e);
					LOG.error("Ha ocurrido un error al obtener fecha de modificacion");
					LOG.error(String.format("Mensaje de error: %s", e.getMessage()));
				}
			 }
			 listaTemplates.add(entidad);
		 }
	}
	
	/**
	 * Metodo que permite extraer los datos de un template.
	 *
	 * @param responseDTO Objeto con la consulta
	 * @param beanResDAO Objeto donde se almacena el resultado a responder
	 */
	public void extraerDatosTemplate(ResponseMessageDataBaseDTO responseDTO,
			BeanResAltaEditTemplateDAO beanResDAO) {
		List<HashMap<String, Object>> listMapRes;
		listMapRes = responseDTO.getResultQuery();
		HashMap<String, Object> first = listMapRes.get(0);
		
		//Obtener los atributos del template
		beanResDAO.getTemplate().setIdRegistro(1);
		beanResDAO.getTemplate().setIdTemplate(utilerias.getString(first.get(ID_TEMPLATE)));
		beanResDAO.getTemplate().setIdLayout(utilerias.getString(first.get(ID_LAYOUT)));
		
		beanResDAO.getTemplate().setEstatus(utilerias.getString(first.get("ESTATUS")));
		beanResDAO.getTemplate().setUsrAlta(utilerias.getString(first.get("USR_ALTA")));
		beanResDAO.getTemplate().setUsrAutoriza(utilerias.getString(first.get("USR_AUTORIZA")));
		
		beanResDAO.getTemplate().setFechaCaptura(utilerias.getString(first.get("FEC_CAPTURA")));
		beanResDAO.getTemplate().setFechaModifica(utilerias.getString(first.get("FEC_MODIFICA")));
	}
	
	/**
	 * Metodo que permite extraer datos relacionados a los usuarios de una consulta.
	 *
	 * @param responseDTO Objeto con la consulta
	 * @param listaAutorizan  Objeto donde se almacenan los resultados
	 */
	public void extraerDatosUsuarios(ResponseMessageDataBaseDTO responseDTO,
			List<BeanUsuarioAutorizaTemplate> listaAutorizan) {
		List<HashMap<String, Object>> listMapRes;
		listMapRes = responseDTO.getResultQuery();
		
		for(HashMap<String,Object> map:listMapRes){
			 //Construir la lista de usuarios
			 BeanUsuarioAutorizaTemplate entidad = new BeanUsuarioAutorizaTemplate();					 
			 entidad.setIdTemplate(utilerias.getString(map.get(ID_TEMPLATE)));
			 entidad.setUsrCaptura(utilerias.getString(map.get("USR_CAPTURA")));
			 entidad.setUsrAutoriza(utilerias.getString(map.get("USR_AUTORIZA")));	
			 
			 listaAutorizan.add(entidad);
		 }
	}
	
	/**
	 * Metodo que permite obtener los datos de una consulta relacionada con los campos de un template.
	 *
	 * @param responseDTO  Objeto con el valor de la consulta
	 * @param listaLayoutCampos Objeto con los datos de los campos. nivel vista
	 * @param listaLayoutCamposTemplate Objeto con los datos de los campos nivel negocios
	 * @param idTemplate Id del template a referenciar
	 * @param origin the origin
	 * @param session the session
	 */
	public void extraerDatosCamposTemplate(
			ResponseMessageDataBaseDTO responseDTO,
			List<BeanCampoLayout> listaLayoutCampos,
			List<BeanCampoLayoutTemplate> listaLayoutCamposTemplate,
			String idTemplate, DAOMantenimientoTemplatesExternalImpl origin,
			ArchitechSessionBean session) {
		List<HashMap<String, Object>> listMapRes;
		listMapRes = responseDTO.getResultQuery();
		
		for(HashMap<String,Object> map:listMapRes){
			 //Construir la lista de campos master y campos del template
			BeanCampoLayoutTemplate campoTem = new BeanCampoLayoutTemplate();
			BeanCampoLayout campo = new BeanCampoLayout();
			
			//Campos Template
			campoTem.setIdTemplate(idTemplate);
			campoTem.setIdCampo(utilerias.getInteger(map.get("ID_CAMPO")));
			campoTem.setCampo(utilerias.getString(map.get("CAMPO")));
			campoTem.setObligatorio(utilerias.getString(map.get("OBLIGATORIO")));
			campoTem.setConstante(utilerias.getString(map.get("CONSTANTE")));
			//Campos Layout
			campo.setIdLayout(utilerias.getString(map.get(ID_LAYOUT)));					
			campo.setIdCampo(utilerias.getInteger(map.get("ID_CAMPO")));
			campo.setCampo(utilerias.getString(map.get("CAMPO")));
			campo.setDescripcion(utilerias.getString(map.get("DESCRIPCION")));
			campo.setObligatorio(utilerias.getString(map.get("OBLIGATORIO")));
			campo.setConstante(utilerias.getString(map.get("CONSTANTE")));
			campo.setTipo(utilerias.getString(map.get("TIPO")));
			campo.setLongitud(utilerias.getInteger(map.get("LONGITUD")));
			
			String query = utilerias.getString(map.get("QUERY"));
			if(null != query && !StringUtils.EMPTY.equals(query)){
				campo.setComboValores(origin.getComboValores(query, session));
			}
			
			listaLayoutCamposTemplate.add(campoTem);
			listaLayoutCampos.add(campo);
		 }
	}
	
	/**
	 * Metodo que permite copiar los atributos del nivel superior transporte al nivel IN template.
	 *
	 * @param beanReq Objeto que contiene los atributos a manipular
	 * @param creacion True o False para indicar si es creacion
	 * @param session Objeto general de session @see ArchitechSessionBean
	 */
	public void ajustarDatosTemplate(BeanReqAltaEditTemplate beanReq, Boolean creacion, ArchitechSessionBean session){
		String idLayout = beanReq.getSelectedLayout();
		String usrAlta = session.getUsuario().toUpperCase();
		String estatus = "PD";

		//Obtener: selectedLayout, usrAlta y estatus
		//Establecer en:  Template
		beanReq.getTemplate().setIdLayout(idLayout);
		if(creacion){
			beanReq.getTemplate().setEstatus(estatus);
		}
		beanReq.getTemplate().setUsrAlta(usrAlta);
		
		SimpleDateFormat formato = new SimpleDateFormat(FORMAT_DAY);
		beanReq.getTemplate().setFechaCaptura(formato.format(new Date()));
	}
	
	/**
	 * Metodo que permite copiar los atributos del nivel superior transporte al nivel IN template.
	 *
	 * @param beanReq Objeto que contiene los atributos a manipular
	 * @param session Objeto general de session @see ArchitechSessionBean
	 */
	public void ajustarDatosUsuariosAut(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session){
		String idTemplate = beanReq.getTemplate().getIdTemplate();		
		String usrAlta = session.getUsuario().toUpperCase();
		
		// Obtener: idTemplate, usrAlta
		//Establecer en:  template.usuariosAutorizan
		for (BeanUsuarioAutorizaTemplate usuario : beanReq.getTemplate().getUsuariosAutorizan()) {
			usuario.setIdTemplate(idTemplate);
			usuario.setUsrCaptura(usrAlta);
		}
	}

	/**
	 * Metodo que permite copiar los atributos del nivel superior transporte al nivel IN template.
	 *
	 * @param beanReq Objeto que contiene los atributos a manipular	 *
	 */
	public void ajustarDatosCampos(BeanReqAltaEditTemplate beanReq) {
		String idTemplate = beanReq.getTemplate().getIdTemplate();
		//Obtener: idTemplate,  Req. listaCamposMaster
		//Establecer:  Template.campos
		
		for(BeanCampoLayout campoMaster:beanReq.getListaCamposMaster()){
			BeanCampoLayoutTemplate temp = new BeanCampoLayoutTemplate();			
			temp.setIdCampo(campoMaster.getIdCampo());
			temp.setIdTemplate(idTemplate);
			temp.setCampo(campoMaster.getCampo());
			
			//Convertir el valor de check a bool
			temp.setObligatorio(campoMaster.getObligatorio());	
			temp.setConstante(campoMaster.getConstante());			
			beanReq.getTemplate().getCampos().add(temp);
		}
	}
	
	/**
	 * Metodo a traves del cual se elimina la redundancia en usuarios disponibles considerando la lista de autorizados.
	 *
	 * @param disponibles lista donde se eliminara la redundancia
	 * @param autorizan lista de usuarios base
	 */
	public void removerAutorizadosDesDisponibles(List<String> disponibles, List<BeanUsuarioAutorizaTemplate> autorizan){
		//DESC. Iterar los usuarios que autorizan, y eliminar la duplicidad en disponibles
		for(BeanUsuarioAutorizaTemplate entidad: autorizan){			
			if(disponibles.contains(entidad.getUsrAutoriza())){
				disponibles.remove(entidad.getUsrAutoriza());
			}
		}
	}
		
	/**
	 * Metodo a traves del cual se comparten datos del request hacia el response en la
	 * Capa de Negocios BO.
	 *
	 * @param req componente de request
	 * @param beanView componente de response
	 */
	public void copiarDatosReqToRes(BeanReqAltaEditTemplate req, BeanResPrincipalTemplate beanView){
		// id template, tipo operacion, selected layout, lista usuarios, lista autorizados
		//Lista de campos master, 				
		beanView.setListaLayouts(req.getListaLayouts());		
		beanView.setListaUsuarios(req.getListaUsuarios());		
		beanView.setListaCamposMaster(req.getListaCamposMaster());		
		beanView.setTemplate(req.getTemplate());
	}
}
