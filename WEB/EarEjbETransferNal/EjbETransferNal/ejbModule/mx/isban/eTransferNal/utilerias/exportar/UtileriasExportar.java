package mx.isban.eTransferNal.utilerias.exportar;

/**
 * The Class UtileriasModuloSPEICero.
 */
public final class UtileriasExportar {
	
	
	/**
	 * Constante del tipo String que almacena la consulta para solicitar
	 * la exportacion de todos los registros realizados durante una
	 * consulta;
	 */
	public static final String QUERY_INSERT_CONSULTA_EXPORTAR_TODO =
		"INSERT INTO TRAN_SPEI_EXP_CDA (FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,"
		+ "NOMBRE_ARCHIVO,NUMERO_REGISTROS,"
		+ "ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)"+
        "values (sysdate,?,?,?,?,?,?,?,sysdate,?,?)";
	
	/** constructor**/
	private UtileriasExportar(){
		super();
    }
	
}