/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * HelperSPIDRecepcionOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCfgTramaSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDTransEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDTransEnvioDatGen;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDTransEnvioDatGen2;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDTransEnvioOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDTransEnvioRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoSubTotalPag;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoSubTotalPagDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoTotal;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSaldoTotalDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenSubTotalPag;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenSubTotalPagDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenTotal;
import mx.isban.eTransferNal.beans.moduloSPID.BeanVolumenTotalDAO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.dao.comun.DAOBitTransaccional;
import mx.isban.eTransferNal.dao.moduloSPID.DAOComunesSPID;
import mx.isban.eTransferNal.dao.moduloSPID.DAORecepOperacionSPID;
import mx.isban.eTransferNal.helper.HelperDAO;
import mx.isban.eTransferNal.utilerias.comunes.UtileriasBitTrans;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

public class HelperSPIDRecepcionOperacion extends Architech{


	/**
	 * Propiedad del tipo String que almacena el valor de SELECCIONADO
	 */
	public static final String SELECCIONADO ="SELECCIONADO";

	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCION
	 */
	public static final String DEVOLUCION ="DEVOLUCION";
	/**
	 * Propiedad del tipo String que almacena el valor de RECUPERA
	 */
	public static final String RECUPERA ="RECUPERA";
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8421289823136778319L;
	
	/**
	 * Propiedad del tipo Map<String,String> que almacena el valor de MAPA
	 */
	private static final Map<String, String> MAPA = new HashMap<String, String>();
	
	/**
	 * Propiedad del tipo String que almacena el valor de REFE_NUMERICA
	 */
	private static final String REFE_NUMERICA = "REFE_NUMERICA";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BME
	 */
	private static final String BME = "BME";
	
	/**
	 * Propiedad del tipo String que almacena el valor de USA
	 */
	private static final String USD = "USD";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ESTATUS_TR
	 */
	private static final String ESTATUS_TR ="TR";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ESTATUS_DV
	 */
	private static final String ESTATUS_DV ="DV";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ESTATUS_RE
	 */
	private static final String ESTATUS_RE ="RE";
	
	/**
	 * Propiedad del tipo String que almacena el valor de TOPOLOGIA_V
	 */
	private static final String TOPOLOGIA_V ="V";
	
	/**
	 * Propiedad del tipo String que almacena el valor de TOPOLOGIA_T
	 */
	private static final String TOPOLOGIA_T ="T";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ESTATUS_BANXICO_LQ
	 */
	private static final String ESTATUS_BANXICO_LQ ="LQ";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ESTATUS_BANXICO_PL
	 */
	private static final String ESTATUS_BANXICO_PL ="PL";

	/**
	 * Propiedad del tipo Map<String,BeanCfgTramaSPID> que almacena el valor de MAP
	 */
	private static final Map<String,BeanCfgTramaSPID> MAP = new HashMap<String, BeanCfgTramaSPID>();

	/**
	 * Propiedad del tipo String que almacena el valor de CEROS
	 */
	private static final String CEROS = "00000";
	
	/**Inicializacion statica*/
	static {
		MAPA.put("01","TTLIQVAL");
		MAPA.put("02","TTLIQDER");
		MAPA.put("03","TTLIQCAM");
		MAPA.put("04","TTPAGSER");
		MAPA.put("05","TTPAGBIE");
		MAPA.put("06","TTOTOPRE");
		MAPA.put("07","TTPAGPRE");
		MAPA.put("08","TTOTROS");
		
		MAP.put("CVE_EMPRESA"      ,new BeanCfgTramaSPID("CVE_EMPRESA","A",3,"I"));
		MAP.put("CVE_PTO_VTA"      ,new BeanCfgTramaSPID("CVE_PTO_VTA","N",4,"I"));
		MAP.put("CVE_MEDIO_ENT"    ,new BeanCfgTramaSPID("CVE_MEDIO_ENT","A",4,"I"));
		MAP.put("REFERENCIA_MED"   ,new BeanCfgTramaSPID("REFERENCIA_MED","N",12,"D"));
		MAP.put("CVE_USUARIO_CAP"  ,new BeanCfgTramaSPID("CVE_USUARIO_CAP","A",7,"I"));
		MAP.put("CVE_USUARIO_SOL"  ,new BeanCfgTramaSPID("CVE_USUARIO_SOL","A",7,"I"));
		MAP.put("CVE_TRANSFE"      ,new BeanCfgTramaSPID("CVE_TRANSFE","N",3,"I"));
		MAP.put("CVE_OPERACION"    ,new BeanCfgTramaSPID("CVE_OPERACION","A",8,"I"));
		MAP.put(REFE_NUMERICA      ,new BeanCfgTramaSPID(REFE_NUMERICA,"N",20,"I"));
		MAP.put("DIRECCION_IP"     ,new BeanCfgTramaSPID("DIRECCION_IP","N",39,"I"));
		MAP.put("FCH_INSTRUC_PAGO" ,new BeanCfgTramaSPID("FCH_INSTRUC_PAGO","N",8,"I"));
		MAP.put("HORA_INSTRUC_PAGO",new BeanCfgTramaSPID("HORA_INSTRUC_PAGO","N",11,"I"));
		MAP.put("CVE_RASTREO"      ,new BeanCfgTramaSPID("CVE_RASTREO","N",30,"I"));
		MAP.put("CVE_DIVISA_ORD"   ,new BeanCfgTramaSPID("CVE_DIVISA_ORD","N",4,"I"));
		MAP.put("CVE_DIVISA_REC"   ,new BeanCfgTramaSPID("CVE_DIVISA_REC","N",4,"I"));
		MAP.put("IMPORTE_CARGO"    ,new BeanCfgTramaSPID("IMPORTE_CARGO","N",18,"D"));
		MAP.put("IMPORTE_ABONO"    ,new BeanCfgTramaSPID("IMPORTE_ABONO","N",18,"D"));
		MAP.put("CVE_PTO_VTA_ORD"  ,new BeanCfgTramaSPID("CVE_PTO_VTA_ORD","N",4,"I"));
		MAP.put("CVE_INTERME_ORD"  ,new BeanCfgTramaSPID("CVE_INTERME_ORD","A",5,"I"));
		MAP.put("TIPO_CUENTA_ORD"  ,new BeanCfgTramaSPID("TIPO_CUENTA_ORD","N",3,"I"));
		MAP.put("NUM_CUENTA_ORD"   ,new BeanCfgTramaSPID("NUM_CUENTA_ORD","N",20,"I"));
		MAP.put("NOMBRE_ORD"       ,new BeanCfgTramaSPID("NOMBRE_ORD","N",120,"I"));
		MAP.put("RFC_ORD"          ,new BeanCfgTramaSPID("RFC_ORD","A",18,"I"));
		MAP.put("DOMICILIO_ORD"    ,new BeanCfgTramaSPID("DOMICILIO_ORD","A",120,"I"));
		MAP.put("COD_POSTAL_ORD"   ,new BeanCfgTramaSPID("COD_POSTAL_ORD","A",5,"I"));
		MAP.put("FCH_CONSTIT_ORD"  ,new BeanCfgTramaSPID("FCH_CONSTIT_ORD","A",8,"I"));
		MAP.put("CVE_PTO_VTA_REC"  ,new BeanCfgTramaSPID("CVE_PTO_VTA_REC","A",4,"I"));
		MAP.put("CVE_INTERME_REC"  ,new BeanCfgTramaSPID("CVE_INTERME_REC","A",5,"I"));
		MAP.put("TIPO_CUENTA_REC"  ,new BeanCfgTramaSPID("TIPO_CUENTA_REC","A",2,"I"));
		MAP.put("NUM_CUENTA_REC"   ,new BeanCfgTramaSPID("NUM_CUENTA_REC","A",20,"I"));
		MAP.put("NOMBRE_REC"       ,new BeanCfgTramaSPID("NOMBRE_REC","A",120,"I"));
		MAP.put("RFC_REC"          ,new BeanCfgTramaSPID("RFC_REC","A",18,"I"));
		MAP.put("MOTIVO_DEVOL"     ,new BeanCfgTramaSPID("MOTIVO_DEVOL","A",2,"I"));
		MAP.put("FOLIO_PAQUETE"    ,new BeanCfgTramaSPID("FOLIO_PAQUETE","A",12,"D"));
		MAP.put("FOLIO_PAGO"       ,new BeanCfgTramaSPID("FOLIO_PAGO","A",12,"D"));
		MAP.put("TIPO_CAMBIO"      ,new BeanCfgTramaSPID("TIPO_CAMBIO","A",15,"D"));
		MAP.put("COMENTARIO2"      ,new BeanCfgTramaSPID("COMENTARIO2","A",30,"I"));
		MAP.put("COMENTARIO3"      ,new BeanCfgTramaSPID("COMENTARIO3","A",30,"I"));
		MAP.put("CONCEPTO_PAGO"    ,new BeanCfgTramaSPID("CONCEPTO_PAGO","A",40,"I"));
	}



	
	
	   /**Metodo privado que sirve para obtener el motivo de rechazo
     *@param respuesta String con la respuesta del servicio
     *@return String con el motivo de rechazo
     *
     */
    public String getMotivoRechazo(String respuesta){
    	String motivo = "";
    	if("TUBO0802".equals(respuesta) ||  "TUBO0818".equals(respuesta) || 
    			"TUBO0869".equals(respuesta) || validaTuboRespuesta(respuesta)){
    		motivo = "13";
    	}else if("TUBO2065".equals(respuesta) || "TUBO0716".equals(respuesta)){
    		motivo = "02";
    	}else if("TUBO0060".equals(respuesta) || "TUBO2063".equals(respuesta)){
    		motivo = "03";
    	}else if("TUBO0097".equals(respuesta) || "TUBO0038".equals(respuesta)){
    		motivo = "01";
    	}else if("TUBO9020".equals(respuesta)){
    		motivo = "21";
    	}else if("TUBO9037".equals(respuesta)){
    		motivo = "01";
    	}else if("TUBO0103".equals(respuesta)){
    		motivo = "05";
    	}else if("TUBO0184".equals(respuesta)){
    		motivo = "03";
    	}else if("TUBO0186".equals(respuesta)){
    		motivo = "21";
    	}else if("TUBO0904".equals(respuesta)){
    		motivo = "20";
    	}else{
    		motivo = "99";
    	}
    	return motivo;
    }
    
    /**
     * @param respuesta String con la trama de respuesta
     * @return boolean que indica que la respuesta es:   TUBO0870,TUBO0040,TUBO0079,TUBO0569,TUBO5122
     */
    private boolean validaTuboRespuesta(String respuesta){
    	return "TUBO0870".equals(respuesta) || "TUBO0040".equals(respuesta) 
		|| "TUBO0079".equals(respuesta) || "TUBO0569".equals(respuesta) || "TUBO5122".equals(respuesta);
    }
    

    
  
    
    /**
     * Metodo privado que sirve para armar la trama
     * @param beanReceptoresSPID Objeto bean del tipo BeanReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return String con la trama generada
     */
    public String armarTramaTercerorTercero(BeanReceptoresSPID beanReceptoresSPID,ArchitechSessionBean sessionBean){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDTransEnvio beanSPIDTransEnvio = creaBeans();

    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setReferenciaMed(
    			String.valueOf(Integer.valueOf(
    					beanReceptoresSPID.getLlave().getFolioPaquete())*10000+Integer.valueOf(beanReceptoresSPID.getLlave().getFolioPago())));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveUsuarioCap(beanReceptoresSPID.getBloqueo().getUsuario());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveUsuarioSol(beanReceptoresSPID.getBloqueo().getUsuario());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveTransfe("944");
    	String clasif = beanReceptoresSPID.getDatGenerales().getClasifOperacion();
    	String cveOperacion = MAPA.get(clasif);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveOperacion(cveOperacion);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setRefeNumerica(beanReceptoresSPID.getBancoOrd().getRefeNumerica());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setDireccionIp(beanReceptoresSPID.getRastreo().getDireccionIp());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setFchInstrucPago(beanReceptoresSPID.getFchPagos().getFchInstrucPago());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setHoraInstrucPago(beanReceptoresSPID.getFchPagos().getHoraInstrucPago());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveRastreo(beanReceptoresSPID.getRastreo().getCveRastreo());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setImporteAbono(
    			utilerias.formateaDecimales(beanReceptoresSPID.getDatGenerales().getMonto(),Utilerias.FORMATO_16_ENT_2_DECIMAL));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setImporteCargo(
    			utilerias.formateaDecimales(beanReceptoresSPID.getDatGenerales().getMonto(),Utilerias.FORMATO_16_ENT_2_DECIMAL));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCvePtoVtaOrd(CEROS);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCveIntermeOrd(beanReceptoresSPID.getBancoOrd().getCveIntermeOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setTipoCuentaOrd(beanReceptoresSPID.getBancoOrd().getTipoCuentaOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNumCuentaOrd(beanReceptoresSPID.getBancoOrd().getNumCuentaOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNombreOrd(beanReceptoresSPID.getBancoOrd().getNombreOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setRfcOrd(beanReceptoresSPID.getBancoOrd().getRfcOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setDomicilioOrd(beanReceptoresSPID.getBancoOrd().getDomicilioOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCodPostalOrd(beanReceptoresSPID.getBancoOrd().getCodPostalOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setFchConstitOrd(beanReceptoresSPID.getBancoOrd().getFchConstitOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCvePtoVtaRec(CEROS);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCveIntermeRec("BANME");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setTipoCuentaRec(beanReceptoresSPID.getBancoRec().getTipoCuentaRec());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNumCuentaRec(beanReceptoresSPID.getBancoRec().getNumCuentaRec());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNombreRec(beanReceptoresSPID.getBancoRec().getNombreRec());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setRfcRec(beanReceptoresSPID.getBancoRec().getRfcRec());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setMotivoDevol("");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setFolioPaquete(beanReceptoresSPID.getLlave().getFolioPaquete());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setFolioPago(beanReceptoresSPID.getLlave().getFolioPago());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setComentario2(beanReceptoresSPID.getRastreo().getCveRastreoOri());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setComentario3(beanReceptoresSPID.getLlave().getCveInstOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setConceptoPago(armaCampo(beanReceptoresSPID.getDatGenerales().getConceptoPago(),"CONCEPTO_PAGO"));
    	return generaTrama(beanSPIDTransEnvio);

    }
    /**
     * @param beanReceptoresSPID Bean con la informacion para la devolucion
     * @param sessionBean Objeto de sesion de la arquitectura
     * @return String con la trama generada
     */   	
    public String armarTramaDevoluciones(BeanReceptoresSPID beanReceptoresSPID,ArchitechSessionBean sessionBean){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDTransEnvio beanSPIDTransEnvio = creaBeans();
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setReferenciaMed(
    			String.valueOf(Integer.valueOf(
    					beanReceptoresSPID.getLlave().getFolioPaquete())*10000+Integer.valueOf(beanReceptoresSPID.getLlave().getFolioPago())));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveUsuarioCap(beanReceptoresSPID.getBloqueo().getUsuario());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveUsuarioSol(beanReceptoresSPID.getBloqueo().getUsuario());
    	
    	if("0".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())|| "5".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())){
    		//Devolución recivida
    		beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveTransfe("984");
    		beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setTipoCuentaOrd(beanReceptoresSPID.getBancoOrd().getTipoCuentaOrd());
    		beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNumCuentaOrd(beanReceptoresSPID.getBancoOrd().getNumCuentaOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNombreOrd(beanReceptoresSPID.getBancoOrd().getNombreOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setTipoCuentaRec(beanReceptoresSPID.getBancoRec().getTipoCuentaRec());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNumCuentaRec(beanReceptoresSPID.getBancoRec().getNumCuentaRec());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNombreRec(beanReceptoresSPID.getBancoRec().getNombreRec());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCveIntermeOrd(beanReceptoresSPID.getBancoOrd().getCveIntermeOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCveIntermeRec("BANME");
		} else{
			//Devolución enviada
			//Se rotan cuentas
			//beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveTransfe("974");
			if("1".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())||"2".equals(beanReceptoresSPID.getDatGenerales().getTipoPago())){
				beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveTransfe("974");
			}else{
				beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveTransfe("977");
			}
	          			
			beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setTipoCuentaOrd(beanReceptoresSPID.getBancoRec().getTipoCuentaRec());
			beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNumCuentaOrd(beanReceptoresSPID.getBancoRec().getNumCuentaRec());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setNombreOrd(beanReceptoresSPID.getBancoRec().getNombreRec());
	    	
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setTipoCuentaRec(beanReceptoresSPID.getBancoOrd().getTipoCuentaOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNumCuentaRec(beanReceptoresSPID.getBancoOrd().getNumCuentaOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setNombreRec(beanReceptoresSPID.getBancoOrd().getNombreOrd());
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCveIntermeOrd("BANME");
	    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCveIntermeRec(beanReceptoresSPID.getBancoOrd().getCveIntermeOrd());
		}

    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveOperacion("DEVSPID");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setRefeNumerica(beanReceptoresSPID.getBancoOrd().getRefeNumerica());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setDireccionIp(beanReceptoresSPID.getRastreo().getDireccionIp());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setFchInstrucPago("");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setHoraInstrucPago("");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveRastreo(beanReceptoresSPID.getRastreo().getCveRastreo());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setImporteAbono(
    			utilerias.formateaDecimales(beanReceptoresSPID.getDatGenerales().getMonto(),Utilerias.FORMATO_16_ENT_2_DECIMAL));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setImporteCargo(
    			utilerias.formateaDecimales(beanReceptoresSPID.getDatGenerales().getMonto(),Utilerias.FORMATO_16_ENT_2_DECIMAL));
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCvePtoVtaOrd(CEROS);
    	
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setRfcOrd(beanReceptoresSPID.getBancoRec().getRfcRec());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setDomicilioOrd(beanReceptoresSPID.getBancoOrd().getDomicilioOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCodPostalOrd(beanReceptoresSPID.getBancoOrd().getCodPostalOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setFchConstitOrd("");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCvePtoVtaRec(CEROS);
    	
    	
     	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setRfcRec(beanReceptoresSPID.getBancoOrd().getRfcOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setMotivoDevol(beanReceptoresSPID.getEnvDev().getMotivoDev());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setFolioPaquete(beanReceptoresSPID.getLlave().getFolioPaquete());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setFolioPago(beanReceptoresSPID.getLlave().getFolioPago());

    	
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setComentario2(beanReceptoresSPID.getRastreo().getCveRastreoOri());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setComentario3(beanReceptoresSPID.getLlave().getCveInstOrd());
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setConceptoPago(completa(beanReceptoresSPID.getDatGenerales().getConceptoPago(),240,"I"));
    	return generaTrama(beanSPIDTransEnvio);

    }
    
    /**
     * @return BeanSPIDTransEnvio Regresa una instancia inicializada de BeanSPIDTransEnvio
     */
    private BeanSPIDTransEnvio creaBeans(){
    	BeanSPIDTransEnvio beanSPIDTransEnvio = new BeanSPIDTransEnvio(); 
    	BeanSPIDTransEnvioDatGen beanSPIDTransEnvioDatGen = new BeanSPIDTransEnvioDatGen();
    	BeanSPIDTransEnvioDatGen2 beanSPIDTransEnvioDatGen2 = new BeanSPIDTransEnvioDatGen2();
    	BeanSPIDTransEnvioOrd beanSPIDTransEnvioOrd = new BeanSPIDTransEnvioOrd();
    	BeanSPIDTransEnvioRec beanSPIDTransEnvioRec = new BeanSPIDTransEnvioRec();
    	beanSPIDTransEnvio.setBeanSPIDTransEnvioDatGen(beanSPIDTransEnvioDatGen);
    	beanSPIDTransEnvio.setBeanSPIDTransEnvioDatGen2(beanSPIDTransEnvioDatGen2);
    	beanSPIDTransEnvio.setBeanSPIDTransEnvioOrd(beanSPIDTransEnvioOrd);
    	beanSPIDTransEnvio.setBeanSPIDTransEnvioRec(beanSPIDTransEnvioRec);
    	
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveEmpresa(BME);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCvePtoVta("0901");  
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().setCveMedioEnt("SPID");
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().setCveDivisaOrd(USD);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().setCveDivisaRec(USD);
    	beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().setTipoCambio("0.0000");
    	
    	return beanSPIDTransEnvio;
    }
    
    

    
    /**
     * @param beanSPIDTransEnvio Bean con los datoa que forman la trama a enviar
     * @return String con la trama a enviar
     */
    private String generaTrama(BeanSPIDTransEnvio beanSPIDTransEnvio){
    	StringBuilder builder = new StringBuilder();
    	builder.append("TRANSPID");
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveEmpresa(),"CVE_EMPRESA"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCvePtoVta(),"CVE_PTO_VTA"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveMedioEnt(),"CVE_MEDIO_ENT"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getReferenciaMed(),"REFERENCIA_MED"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveUsuarioCap(),"CVE_USUARIO_CAP"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveUsuarioSol(),"CVE_USUARIO_SOL"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveTransfe(),"CVE_TRANSFE"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveOperacion(),"CVE_OPERACION"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getRefeNumerica(),REFE_NUMERICA));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getDireccionIp(),"DIRECCION_IP"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getFchInstrucPago(),"FCH_INSTRUC_PAGO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getHoraInstrucPago(),"HORA_INSTRUC_PAGO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen().getCveRastreo(),"CVE_RASTREO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getCveDivisaOrd(),"CVE_DIVISA_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getCveDivisaRec(),"CVE_DIVISA_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getImporteAbono(),"IMPORTE_ABONO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getImporteCargo(),"IMPORTE_CARGO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getCvePtoVtaOrd(),"CVE_PTO_VTA_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getCveIntermeOrd(),"CVE_INTERME_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getTipoCuentaOrd(),"TIPO_CUENTA_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getNumCuentaOrd(),"NUM_CUENTA_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getNombreOrd(),"NOMBRE_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getRfcOrd(),"RFC_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getDomicilioOrd(),"DOMICILIO_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getCodPostalOrd(),"COD_POSTAL_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioOrd().getFchConstitOrd(),"FCH_CONSTIT_ORD"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getCvePtoVtaRec(),"CVE_PTO_VTA_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getCveIntermeRec(),"CVE_INTERME_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getTipoCuentaRec(),"TIPO_CUENTA_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getNumCuentaRec(),"NUM_CUENTA_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getNombreRec(),"NOMBRE_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioRec().getRfcRec(),"RFC_REC"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getMotivoDevol(),"MOTIVO_DEVOL"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getFolioPaquete(),"FOLIO_PAQUETE"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getFolioPago(),"FOLIO_PAGO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getTipoCambio(),"TIPO_CAMBIO"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getComentario2(),"COMENTARIO2"));
    	builder.append(armaCampo(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getComentario3(),"COMENTARIO3"));
    	builder.append(beanSPIDTransEnvio.getBeanSPIDTransEnvioDatGen2().getConceptoPago());
    	return builder.toString();
    }

    
    /**
     * @param valor String con el valor
     * @param campo String con el campo
     * @return String con el formateo
     */
    private String armaCampo(String valor,String campo){
    	BeanCfgTramaSPID beanCfgTramaSPID = MAP.get(campo);
    	String _valor = valor; 
    	if(_valor == null){
    		_valor = "";
    	}else{
    		_valor = _valor.trim();
    	}
    	return completa(_valor,beanCfgTramaSPID.getTamano(), beanCfgTramaSPID.getJustificado());
    }
    
    /**
     * @param valor String con el valor
     * @param longitud entero con la longitud del campo
     * @param justificacion Es la justificacion del campo I (Izquierdo) o D (Derecho)
     * @return String completa caracteres de izq o der
     */
    private String completa(String valor,int longitud, String justificacion){
    	StringBuilder builder = new StringBuilder();
    	String _valor = valor;
    	int length = valor.length();
    	int tam = longitud - length;
    	if(tam>0){
	    	if("I".equals(justificacion)){
	    		builder.append(valor);
	    		for(int i=0;i<tam;i++){
	    			builder.append(" ");
	        	}
	    	}else if("D".equals(justificacion)){
	    		for(int i=0;i<tam;i++){
	    			builder.append(" ");
	        	}
	    		builder.append(valor);
	    	}
	    	_valor = builder.toString();
    	}else{
    		_valor = _valor.substring(0,longitud);
    	}
    	return _valor;
    }
    
	
	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqReceptoresSPID Bean con el listado de objetos
	 * @param filtra String con el filtro 
	 * @return List<BeanTranSpeiRec> listado con los bean seleccionados
	 */
	public List<BeanReceptoresSPID> filtraSelecionados(BeanReqReceptoresSPID beanReqReceptoresSPID, String filtra){
		List<BeanReceptoresSPID> listBeanTranSpeiRec = new ArrayList<BeanReceptoresSPID>();
		
		if(SELECCIONADO.equals(filtra)){
			for(BeanReceptoresSPID beanReceptoresSPID:beanReqReceptoresSPID.getListBeanReceptoresSPID()){
		      	  if(beanReceptoresSPID.getOpciones().isSeleccionado()){
		      		listBeanTranSpeiRec.add(beanReceptoresSPID);
		      	  }
			}
		}else if(DEVOLUCION.equals(filtra)){
			for(BeanReceptoresSPID beanReceptoresSPID:beanReqReceptoresSPID.getListBeanReceptoresSPID()){
		      	  if(beanReceptoresSPID.getOpciones().isSelecDevolucion()){
		      		listBeanTranSpeiRec.add(beanReceptoresSPID);
		      	  }
			}
		}else if(RECUPERA.equals(filtra)){
			for(BeanReceptoresSPID beanReceptoresSPID:beanReqReceptoresSPID.getListBeanReceptoresSPID()){
		      	  if(beanReceptoresSPID.getOpciones().isSelecRecuperar()){
		      		listBeanTranSpeiRec.add(beanReceptoresSPID);
		      	  }
			}
		}
		
		return listBeanTranSpeiRec;
	}

	 /**
     * Metodo encargado de setear la respuesta al bean
     * @param beanReqReceptoresSPID Bean del tipo BeanReqReceptoresSPID
     * @param beanResReceptoresSPIDDAO Bean del tipo BeanResReceptoresSPIDDAO
     * @param beanResReceptoresSPID Bean del tipo BeanResReceptoresSPID
     * @param paginador bean del tipo BeanPaginador
     */
    public void seteaRespuestaExitosa(BeanReqReceptoresSPID beanReqReceptoresSPID,BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO,
    		BeanResReceptoresSPID beanResReceptoresSPID,
    		BeanPaginador paginador ){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	beanResReceptoresSPID.setTotalReg(beanResReceptoresSPIDDAO.getTotalReg());
    	BeanSaldoTotal beanSaldoTotal = new BeanSaldoTotal();
    	BeanSaldoSubTotalPag beanSaldoSubTotalPag = new BeanSaldoSubTotalPag();
    	BeanVolumenSubTotalPag beanVolumenSubTotalPag = new BeanVolumenSubTotalPag();
    	BeanVolumenTotal beanVolumenTotal = new BeanVolumenTotal();
   	 
    	BeanSaldoTotalDAO beanSaldoTotalDAO = beanResReceptoresSPIDDAO.getSaldoTotal();
    	BeanSaldoSubTotalPagDAO beanSaldoSubTotalPagDAO = beanResReceptoresSPIDDAO.getSaldoSubTotalPag();
    	BeanVolumenSubTotalPagDAO beanVolumenSubTotalPagDAO = beanResReceptoresSPIDDAO.getVolumenSubTotalPag();
    	BeanVolumenTotalDAO beanVolumenTotalDAO = beanResReceptoresSPIDDAO.getVolumenTotal();
    	
    	beanResReceptoresSPID.setListReceptoresSPID(beanResReceptoresSPIDDAO.getListReceptoresSPID());
    	
    	beanSaldoTotal.setTopoVLiquidadas(utilerias.formateaDecimales(beanSaldoTotalDAO.getTopoVLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoTotal.setTopoTLiquidadas(utilerias.formateaDecimales(beanSaldoTotalDAO.getTopoTLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoTotal.setTopoTPorLiquidar(utilerias.formateaDecimales(beanSaldoTotalDAO.getTopoTPorLiquidar(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoTotal.setRechazos(utilerias.formateaDecimales(beanSaldoTotalDAO.getRechazos(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	
    	beanSaldoSubTotalPag.setTopoVLiquidadas(utilerias.formateaDecimales(beanSaldoSubTotalPagDAO.getTopoVLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoSubTotalPag.setTopoTLiquidadas(utilerias.formateaDecimales(beanSaldoSubTotalPagDAO.getTopoTLiquidadas(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoSubTotalPag.setTopoTPorLiquidar(utilerias.formateaDecimales(beanSaldoSubTotalPagDAO.getTopoTPorLiquidar(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	beanSaldoSubTotalPag.setRechazos(utilerias.formateaDecimales(beanSaldoSubTotalPagDAO.getRechazos(), Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS));
    	
    	beanVolumenTotal.setTopoVLiquidadas(utilerias.formateaDecimales(beanVolumenTotalDAO.getTopoVLiquidadas(), Utilerias.FORMATO_NUMBER));
    	beanVolumenTotal.setTopoTLiquidadas(utilerias.formateaDecimales(beanVolumenTotalDAO.getTopoTLiquidadas(), Utilerias.FORMATO_NUMBER));
    	beanVolumenTotal.setTopoTPorLiquidar(utilerias.formateaDecimales(beanVolumenTotalDAO.getTopoTPorLiquidar(), Utilerias.FORMATO_NUMBER));
    	beanVolumenTotal.setRechazos(utilerias.formateaDecimales(beanVolumenTotalDAO.getRechazos(), Utilerias.FORMATO_NUMBER));
    	
    	beanVolumenSubTotalPag.setTopoVLiquidadas(utilerias.formateaDecimales(beanVolumenSubTotalPagDAO.getTopoVLiquidadas(), Utilerias.FORMATO_NUMBER));
    	beanVolumenSubTotalPag.setTopoTLiquidadas(utilerias.formateaDecimales(beanVolumenSubTotalPagDAO.getTopoTLiquidadas(), Utilerias.FORMATO_NUMBER));
    	beanVolumenSubTotalPag.setTopoTPorLiquidar(utilerias.formateaDecimales(beanVolumenSubTotalPagDAO.getTopoTPorLiquidar(), Utilerias.FORMATO_NUMBER));
    	beanVolumenSubTotalPag.setRechazos(utilerias.formateaDecimales(beanVolumenSubTotalPagDAO.getRechazos(), Utilerias.FORMATO_NUMBER));

    	
    	beanResReceptoresSPID.setSaldoTotal(beanSaldoTotal);
    	beanResReceptoresSPID.setSaldoSubTotalPag(beanSaldoSubTotalPag);
    	beanResReceptoresSPID.setVolumenSubTotalPag(beanVolumenSubTotalPag);
    	beanResReceptoresSPID.setVolumenTotal(beanVolumenTotal);
    	
		paginador.calculaPaginas(beanResReceptoresSPID.getTotalReg(), HelperDAO.REGISTROS_X_PAGINA.toString());
		beanResReceptoresSPID.setPaginador(paginador);	
		beanResReceptoresSPID.setCodError(Errores.OK00000V);
		beanResReceptoresSPID.setOpcion(beanReqReceptoresSPID.getOpcion());
    	
    }
    

    
    /**
     * Metodo que permite obtener la cuenta
     * @param beanReceptoresSPIDTemp Bean del tipo BeanReceptoresSPID que tiene la informacion de la cuenta
     * @param sessionBean Objeto del tipo ArchitechSessionBean
     * @return BeanResReceptoresSPID Bean de respuesta
     */
    public BeanResReceptoresSPID obtieneCuenta(BeanReceptoresSPID beanReceptoresSPIDTemp, ArchitechSessionBean sessionBean){
    	String respuestas[] = null;
    	String tramaRespuesta ="";
    	ResBeanEjecTranDAO resBeanEjecTranDAO = null;
    	UtileriasMQ utileriasMQ = UtileriasMQ.getInstance();
    	BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
    	
    	StringBuilder builder = new StringBuilder();
    	if("03".equals(beanReceptoresSPIDTemp.getBancoRec().getTipoCuentaRec())){
    		builder.append("TRANVDEB");
    		builder.append(beanReceptoresSPIDTemp.getBancoRec().getTipoCuentaRec());
    	}else if("10".equals(beanReceptoresSPIDTemp.getBancoRec().getTipoCuentaRec())){
    		builder.append("TRANVMOV");
    		builder.append(beanReceptoresSPIDTemp.getBancoRec().getNumCuentaRec());
    	}
    	
    	
    	resBeanEjecTranDAO = utileriasMQ.ejecutaTransaccion(builder.toString(), "Obtienecuenta", "ARQ_MENSAJERIA", sessionBean);
    	if(ResBeanEjecTranDAO.COD_EXITO.equals(resBeanEjecTranDAO.getCodError())){
    		tramaRespuesta = resBeanEjecTranDAO.getTramaRespuesta();
    		info("Respuesta trama"+tramaRespuesta);
    		respuestas = tramaRespuesta.split("\\|");
			if(respuestas.length>0 && "TRIB0000".equals(respuestas[0])){
				beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResReceptoresSPID.setCodError(Errores.OK00001V);
				info("Respuesta cuenta"+respuestas[2]);
			}else{
				beanResReceptoresSPID.setCodError(Errores.ED00085V);
        		beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ALERT);
            	return beanResReceptoresSPID;
			}
    	}else{
    		beanResReceptoresSPID.setCodError("ED00089V");
    		beanResReceptoresSPID.setTipoError(Errores.TIPO_MSJ_ERROR);
    	}
    	return beanResReceptoresSPID;
    }
    
    /**
     * Metodo que se encarga en ejecutar las consultas
     * @param dAOComunesSPID referencia al DAO DAOComunesSPID
     * @param daoRecepOperacion referencia al DAO DAORecepOperacionSPID
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPIDDAO Bean del tipo BeanResReceptoresSPIDDAO
     */
    public BeanResReceptoresSPIDDAO ejecutaConsulta(DAOComunesSPID dAOComunesSPID,DAORecepOperacionSPID daoRecepOperacion,BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean){
    	List<Object> parametros = new ArrayList<Object>();
    	BeanResConsMiInstDAO beanResConsMiInstDAO = null;
    	BeanResReceptoresSPIDDAO beanResReceptoresSPIDDAO = null;
    	BeanResConsFechaOpDAO beanResConsFechaOpDAO = null;
    	beanResConsMiInstDAO = dAOComunesSPID.consultaMiInstitucion("ENTIDAD_SPID",sessionBean);
    	beanReqReceptoresSPID.setCveMiInstitucion(beanResConsMiInstDAO.getMiInstitucion());
		parametros.add(beanReqReceptoresSPID.getCveMiInstitucion());
		beanResConsFechaOpDAO =dAOComunesSPID.consultaFechaOperacionSPID(sessionBean);
		parametros.add(beanResConsFechaOpDAO.getFechaOperacion());
		if("TODAS".equals(beanReqReceptoresSPID.getOpcion())){
			parametros.addAll(Arrays.asList(new String[]{ESTATUS_TR,ESTATUS_DV}));
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("TVL".equals(beanReqReceptoresSPID.getOpcion())){
	    	parametros.addAll(Arrays.asList(new String[]{ESTATUS_TR,ESTATUS_DV,ESTATUS_RE,TOPOLOGIA_V}));
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("TTL".equals(beanReqReceptoresSPID.getOpcion())){
	    	parametros.addAll(Arrays.asList(new String[]{ESTATUS_TR,ESTATUS_DV,ESTATUS_RE,TOPOLOGIA_T,ESTATUS_BANXICO_LQ}));
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("TTXL".equals(beanReqReceptoresSPID.getOpcion())){
	    	parametros.addAll(Arrays.asList(new String[]{ESTATUS_TR,ESTATUS_DV,ESTATUS_RE,TOPOLOGIA_T,ESTATUS_BANXICO_PL}));
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("R".equals(beanReqReceptoresSPID.getOpcion())){
    		parametros.add(ESTATUS_TR);
    		parametros.add(ESTATUS_DV);
    		parametros.add(ESTATUS_RE);
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("APT".equals(beanReqReceptoresSPID.getOpcion())){
	    	
    		parametros.add(ESTATUS_TR);
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("DEV".equals(beanReqReceptoresSPID.getOpcion())){
	    	
    		parametros.add(ESTATUS_DV);
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }else if("RMD".equals(beanReqReceptoresSPID.getOpcion())){
    		parametros.add(ESTATUS_TR);
    		parametros.add(ESTATUS_DV);
    		parametros.add(ESTATUS_RE);
    		beanResReceptoresSPIDDAO =  daoRecepOperacion.consultaTransferencia(beanReqReceptoresSPID,parametros,sessionBean);
	    }
		beanResReceptoresSPIDDAO.setCveMiInstitucion(beanReqReceptoresSPID.getCveMiInstitucion());
		beanResReceptoresSPIDDAO.setFchOperacion(beanResConsFechaOpDAO.getFechaOperacion());
	    return beanResReceptoresSPIDDAO;
    }
    /**
     * @param idOperacion String con el id de la operacion
     * @param codError String con el codigo de error 
     * @param descErr String con la descripcion del error
     * @param descOper Strig con ls descripcion de la operacion
     * @param comentarios Strig con los comentarios
     * @param servTran String ccn el servicio
     * @param nombreArchivo String con el nombre del archivo
     * @param bean del tipo BeanReceptoresSPID
     * @param sesion Objeto de session de la arquitectura
     * @param dAOBitTransaccional Referencia al DAO DAOBitTransaccional
     */
    public void setBitacoraTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, BeanReceptoresSPID bean, ArchitechSessionBean sesion, DAOBitTransaccional dAOBitTransaccional){
    	UtileriasBitTrans utileriasBitTrans = new UtileriasBitTrans();
    	BeanReqInsertBitTrans beanReqInsertBitTrans = 
    		utileriasBitTrans.createBitacoraBitTrans(idOperacion, codError, descErr, descOper, comentarios, servTran, nombreArchivo,bean,  sesion);
    	dAOBitTransaccional.guardaBitacora(beanReqInsertBitTrans, sesion);
    }
    
	/**
	 * Cambia el nombre de la tabla cuando es SPID
	 * @param sql Query
	 * @param esSPID Indicador de SPID
	 * @return Nombre de la tabla
	 */
	public static String obtenerQuery(String sql, boolean esSPID) {
		return ( ! esSPID ) ? sql : sql.replaceAll("TRAN_SPEI_", "TRAN_SPID_");
	}

}
