/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpHTO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:47:39 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;
/**
 * en esta clase de utilerias se las funciones para realizar una devolucion extemporanea.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
public class UtileriasGeneraTrama  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -8242875650770861907L;
	
	/** La constante PIPE. */
	private static final String  PIPE = "|";
	/** Propiedad del tipo String que almacena el valor de CONSTANTE_47. */
	private static final String CONSTANTE_47 = "047";
		
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasGeneraTrama util = new UtileriasGeneraTrama();
	
	
	/**
	 * Obten la instancia de UtileriasRecepcionOpHTO.
	 *
	 * @return instancia de UtileriasRecepcionOpHTO
	 */
	public static UtileriasGeneraTrama getInstance(){
		return util;
	}
		
	/**
	 * Metodo setea los datos para envio a Satander.
	 *
	 * @param beanTransEnvio            Objeto del tipo BeanTransEnvio
	 * @return Objeto string builder
	 */
	public StringBuilder armaTramaCadena2(BeanTransEnvio beanTransEnvio) {
		// se inicializa el string builder
		StringBuilder strBuilder= new StringBuilder();
		strBuilder.append(beanTransEnvio.getPlazaBanxico());
		// se asigna el pipe
		strBuilder.append(PIPE);
		if (beanTransEnvio.getPtoVtaRec() != null && !"".equals(beanTransEnvio.getPtoVtaRec())) {
			strBuilder.append(beanTransEnvio.getPtoVtaRec());
		}
		strBuilder.append(PIPE);
		//concatena datos
		strBuilder.append(beanTransEnvio.getDivisaRec());
		strBuilder.append(PIPE);
		strBuilder.append(beanTransEnvio.getNombreRec());
		strBuilder.append(PIPE);
		// llama a la funcion
		strBuilder.append(armaCadena(beanTransEnvio));
		strBuilder.append(PIPE);
		strBuilder.append(beanTransEnvio.getSucBcoRe());
		strBuilder.append(PIPE);
		if (beanTransEnvio.getPaisBcoRec() != null && !"".equals(beanTransEnvio.getPaisBcoRec())) {
			strBuilder.append(beanTransEnvio.getPaisBcoRec());
		}
		strBuilder.append(PIPE);
		strBuilder.append(beanTransEnvio.getCiudadBcoRe());
		strBuilder.append(PIPE);
		if (beanTransEnvio.getCveABA() != null && !"".equals(beanTransEnvio.getCveABA())) {
			strBuilder.append(beanTransEnvio.getCveABA());
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getComentario1() != null && !"".equals(beanTransEnvio.getComentario1())) {
			strBuilder.append(beanTransEnvio.getComentario1());
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getComentario2() != null && !"".equals(beanTransEnvio.getComentario2())) {
			strBuilder.append(beanTransEnvio.getComentario2());
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getComentario3() != null && !"".equals(beanTransEnvio.getComentario3())) {
			strBuilder.append(beanTransEnvio.getComentario3());
		}
		
		strBuilder.append(armaTramaCadena3(beanTransEnvio));
		
		return strBuilder;

	}
	
	/**
	 * Arma cadena.
	 *
	 * @param beanTransEnvio El objeto: bean trans envio
	 * @return Objeto string builder
	 */
	public StringBuilder armaCadena(BeanTransEnvio beanTransEnvio) {
		// se inicializa el string builder
		StringBuilder strBuilder= new StringBuilder();
		if (beanTransEnvio.getImporteCargo() != null && !"".equals(beanTransEnvio.getImporteCargo())) {
			strBuilder.append(beanTransEnvio.getImporteCargo().replaceAll(",", ""));
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getImporteAbono() != null && !"".equals(beanTransEnvio.getImporteAbono())) {
			strBuilder.append(beanTransEnvio.getImporteAbono().replaceAll(",", ""));
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getImporteDolares() != null && !"".equals(beanTransEnvio.getImporteDolares())) {
			strBuilder.append(beanTransEnvio.getImporteDolares().replaceAll(",", ""));
		}
		strBuilder.append(PIPE);
		if (beanTransEnvio.getNombreBcoRec() != null && !"".equals(beanTransEnvio.getNombreBcoRec())) {
			strBuilder.append(beanTransEnvio.getNombreBcoRec());
		}
		
		return strBuilder;
	}
	
	/**
	 * Metodo setea los datos para envio a Satander.
	 *
	 * @param beanTransEnvio            Objeto del tipo BeanTransEnvio
	 * @return Objeto string builder
	 */
	private StringBuilder armaTramaCadena3(BeanTransEnvio beanTransEnvio) {
		// se inicializa el string builder
		StringBuilder strBuilder= new StringBuilder();
		// concatena los valores
		if ("2".equals(beanTransEnvio.getServicio())) {
			// asigna los datos del receptos
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getNumCuentaRec2());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getNombreRec2());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getRfcRec2());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getConceptoPago2());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getTipoCuentaRec2());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getComentario4());
			strBuilder.append(PIPE);
			//asigna la cuenta el ordenante
			strBuilder.append(beanTransEnvio.getTipoCuentaOrd());
			strBuilder.append(PIPE);
			strBuilder.append(beanTransEnvio.getTipoCuentaRec());
		}
		
		return strBuilder;
	}
	
	/**
	 * Obtener cve trans.
	 *
	 * @param flagConvVostro El objeto: flag conv vostro
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String obtenerCveTrans(boolean flagConvVostro,BeanTranSpeiRecOper beanTranSpeiRec ) {
		// se crea variable
		String cve;
		// si cumple con la conicion asigna el valor
		if (flagConvVostro) {
			cve="046";
		} else {
			if (beanTranSpeiRec.getNumCuentaTran() != null && !"".equals(beanTranSpeiRec.getNumCuentaTran())) {
				cve="044";
			} else {
				cve=CONSTANTE_47;
			}
		}
		
		return cve;
	}
	
	/**
	 * Obtener cve trans banco.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String obtenerCveTransBanco(BeanTranSpeiRecOper beanTranSpeiRec) {
		String cve;
		if ("0".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())) {
			// *** D E V U E L T O ***
			if ("7".equals(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri().substring(5, 6))
					|| "6".equals(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri().substring(5, 6))) {
				cve="587";
			} else {
				cve="584";
			}
		} else {
			// *** R E C I B I D O ***
			if ("1".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago()) || "5".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())
					|| "12".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago()) && beanTranSpeiRec.getNumCuentaTran() != null
							&& !"".equals(beanTranSpeiRec.getNumCuentaTran())) {
				cve="544";
			} else {
				cve="547";
			}
		}
		return cve;
	}
	
	/**
	 * Obtener banco recep.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String obtenerBancoRecep(BeanTranSpeiRecOper beanTranSpeiRec) {
		//inicializa variable
		String cve;
		//asigna el valor
		if ("1".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago()) || "5".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())) {
			cve="574";
		} else {
			cve="577";
		}
		
		return cve;
	}
	
	
	/**
	 * Envio banme.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param flagConvVostro El objeto: flag conv vostro
	 * @return Objeto string
	 */
	public String envioBanme(BeanTranSpeiRecOper beanTranSpeiRec,boolean flagConvVostro) {
		//inciializa la variable
		String cve;
		//asigna el valor
		if (beanTranSpeiRec.getNumCuentaTran() != null && !"".equals(beanTranSpeiRec.getNumCuentaTran())
				&& !flagConvVostro) {
			cve="044";
		} else {
			cve=CONSTANTE_47;
		}
		
		return cve;
	}
	
	/**
	 * Envio banme cve.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String envioBanmeCve(BeanTranSpeiRecOper beanTranSpeiRec) {
		//inicializa la variable
		String cve;
		//asigna el valor si cumple
		if (beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri().length()>20 && ( "7".equals(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri().substring(21, 22))
				|| "6".equals(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri().substring(21, 22)))) {
			cve="087";
		} else {
			cve="084";
		}
		
		return cve;
	}
	
	
}
