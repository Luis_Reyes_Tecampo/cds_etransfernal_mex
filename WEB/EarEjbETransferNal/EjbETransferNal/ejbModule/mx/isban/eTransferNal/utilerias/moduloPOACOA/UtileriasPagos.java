/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:54:49 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPagoDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenantePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptorPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasPagos.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los flujos de la pantalla de Pagos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class UtileriasPagos implements Serializable {


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3350567338206950156L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasPagos utils;
	
	/** La constante CONST_COUNT. */
	private static final String CONST_COUNT = "SELECT COUNT(1) CONT ";
	
	/** La constante CONST_CONTADOR. */
	private static final String CONST_CONTADOR = ") CONTADOR, TRAN_MENSAJE TM, [TB_EXTRA] TMS  ";
	
	/** Constante WHERE para uso comun en queries. */
	private static final String WHERE = "WHERE ";
	
	/** Constante AND para uso comun en queries. */
	private static final String AND = "AND ";
	
	/** CAMPO CVE_MECANISMO. */
	private static final String CVE_MECANISMO = "TM.CVE_MECANISMO";
	
	/** CAMPO ESTATUS. */
	private static final String ESTATUS = "TM.ESTATUS";
	
	/** Constante FROM de uso comun en queries. */
	private static final String FROM= "FROM (";
	
	/** Constante IN para uso comun en queries. */
	private static final String IN = " IN [";
	
	/** Constante de llave para uso comun en queries. */
	private static final String OPEN_SP = " = '[";
	
	/** Constante de cierre de llave para uso comun en queries. */
	private static final String CLOSE_SP = "]' ";
	
	/** La constante CVE_INTERME_ORD. */
	private static final String CVE_INTERME_ORD= "CVE_INTERME_ORD";
	
	/** La constante CONST_ESTATUS. */
	private static final String  CONST_ESTATUS = "ESTATUS";
	
	/** Constante REFERENCIAMIN para uso comun en queries. */
	private static final String REFERENCIAMIN= "referencia";
	
	/** Constante COLAMIN para uso comun en queries. */
	private static final String COLAMIN = "cola";
	
	/** Constante CVE_TRAN para uso comun en queries. */
	private static final String CVE_TRAN = "cveTran";
	
	/** Constante CVE_MECAN para uso comun en queries. */
	private static final String CVE_MECAN = "mecan";
	
	/** Constante ORD para uso comun en queries. */
	private static final String ORD = "ord";
	
	/** Constante REC para uso comun en queries. */
	private static final String REC = "rec";
	
	/** Constante IMP para uso comun en queries. */
	private static final String IMP = "imp";
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils
	 */
	public static UtileriasPagos getUtils() {
		if (utils == null) {
			utils = new UtileriasPagos();
		}
		return utils;
	}
	
	/**
	 * Llenar listado pagos.
	 * 
	 * metodo para recorrer el resultado de la BD
	 * y setearlo en el objeto de salida.
	 * 
	 * @param beanResListadoPagosDAO El objeto: bean res listado pagos DAO
	 * @param list El objeto: list
	 * @param utilerias El objeto: utilerias
	 * @return Objeto list
	 */
	public List<BeanPago> llenarListadoPagos(final BeanResListadoPagosDAO beanResListadoPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		BeanPago pagoList;
		List<BeanPago> listaPagos;
		listaPagos = new ArrayList<BeanPago>();
	    Integer cero = 0;
        			 
        			 for(Map<String,Object> mapResult : list){
        				 pagoList = new BeanPago();
        				 pagoList.setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
        				 pagoList.setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
        				 pagoList.setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
        				 pagoList.setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
        				 pagoList.setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
        				 pagoList.setFechaCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
        				 pagoList.setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD)));
        				 pagoList.setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
        				 pagoList.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
        				 pagoList.setEstatus(utilerias.getString(mapResult.get(CONST_ESTATUS)));
        				 pagoList.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
        				 pagoList.setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
        				 pagoList.setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
        				 
        				 listaPagos.add(pagoList);
        				 
        				 if (cero.equals(beanResListadoPagosDAO.getTotalReg())){
        					 beanResListadoPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
						 }
        			 }
		return listaPagos;
	}
	
	/**
	 * Llenarhistorial mensajes.
	 *
	 * metodo para recorrer el resultado de la BD
	 * y setearlo en el objeto de salida.
	 * 
	 * @param utilerias El objeto: utilerias
	 * @param beanHistorialMensajesDAO El objeto: bean historial mensajes DAO
	 * @param listaHistorialMensajes El objeto: lista historial mensajes
	 * @param list El objeto: list
	 */
	public void llenarhistorialMensajes(Utilerias utilerias, final BeanHistorialMensajesDAO beanHistorialMensajesDAO,
			List<BeanHistorialMensajes> listaHistorialMensajes,List<HashMap<String, Object>> list) {
		for(Map<String,Object> listMap : list){
		Map<String, Object> mapResult = listMap;
		Integer zero = Integer.valueOf(0);
		BeanHistorialMensajes beanHistorialMensajes = new BeanHistorialMensajes();
		beanHistorialMensajes.setDel(utilerias.getString(mapResult.get("DEL")));
		beanHistorialMensajes.setAl(utilerias.getString(mapResult.get("AL")));
		beanHistorialMensajes.setEstatus2(utilerias.getString(mapResult.get("ESTATUS2")));
		beanHistorialMensajes.setUsuario(utilerias.getString(mapResult.get("USUARIO")));
		beanHistorialMensajes.setFecha(utilerias.getString(mapResult.get("FECHA")));
		listaHistorialMensajes.add(beanHistorialMensajes);
		if(zero.equals(beanHistorialMensajesDAO.getTotalReg())){
			beanHistorialMensajesDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			
		}
		}
	}
	
	/**
	 * Obtener el objeto: sort field.
	 *
	 * Validar filtro 
	 * 
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @return El objeto: sort field
	 */
	public String getSortField(BeanReqHistorialMensajes beanReqHistorialMensajes) {
		String sortField = "";
		
		if ("del".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "DEL";
		} else if ("estatus".equals(beanReqHistorialMensajes.getSortField())){
			sortField = CONST_ESTATUS;
		} else if ("al".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "AL";
		} else if ("estatus2".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "ESTATUS2";
		} else if ("usuario".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "USUARIO";
		} else if ("fecha".equals(beanReqHistorialMensajes.getSortField())){
			sortField = "FECHA";
		}
		return sortField;
	}
	
	/**
	 * Llenar datos generales pago.
	 *
	 * metodo para recorrer el resultado de la BD
	 * y setearlo en el objeto de salida.
	 * 
	 * @param beanDetallePago El objeto: bean detalle pago
	 * @param utilerias El objeto: utilerias
	 * @param mapaResultado El objeto: mapa resultado
	 * @param modulo El objeto: modulo
	 */
	public void llenarDatosGeneralesPago(BeanDetallePago beanDetallePago, Utilerias utilerias,
			Map<String, Object> mapaResultado, BeanModulo modulo) {
		BeanDatosGeneralesPago beanDatosGeneralesPago;
		BeanOrdenantePago ordenantePago;
		BeanReceptorPago receptorPago;
					beanDatosGeneralesPago = new BeanDatosGeneralesPago();
					ordenantePago = new BeanOrdenantePago();
					receptorPago = new BeanReceptorPago();
					
					datosGenerales(utilerias, mapaResultado, beanDatosGeneralesPago, modulo);
					
					ordenantePago.setIntermediario(utilerias.getString(mapaResultado.get(CVE_INTERME_ORD))); /** Set CVE_INTERME_ORD **/
					ordenantePago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA_ORD"))); /** Set CVE_PTO_VTA_ORD **/
					ordenantePago.setNumeroCuenta(utilerias.getString(mapaResultado.get("NUM_CUENTA_ORD"))); /** Set NUM_CUENTA_ORD **/
					ordenantePago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_ORD"))); /** Set NOMBRE_ORD **/
					ordenantePago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_ORD"))); /** Set CVE_DIVISA_ORD **/
					ordenantePago.setImporteCargo(utilerias.getString(mapaResultado.get("IMPORTE_CARGO"))); /** Set IMPORTE_CARGO **/
					ordenantePago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_ORD"))); /** Set TIPO_CUENTA_ORD **/
					receptorPago.setIntermediario(utilerias.getString(mapaResultado.get("CVE_INTERME_REC"))); /** Set CVE_INTERME_REC **/
					receptorPago.setNumeroCuentaReceptor(utilerias.getString(mapaResultado.get("NUM_CUENTA_REC"))); /** Set NUM_CUENTA_REC **/
					receptorPago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_REC"))); /** Set NOMBRE_REC **/
					receptorPago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_REC"))); /** Set CVE_DIVISA_REC **/
					receptorPago.setImporteAbono(utilerias.getBigDecimal(mapaResultado.get("IMPORTE_ABONO"))); /** Set IMPORTE_ABONO **/
					receptorPago.setClaveSwiftCorres(utilerias.getString(mapaResultado.get("CVE_SWIFT_COR"))); /** Set CVE_SWIFT_COR **/
					receptorPago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_REC"))); /** Set TIPO_CUENTA_REC **/
					receptorPago.setRfc(utilerias.getString(mapaResultado.get("RFC_REC"))); /** Set RFC_REC **/
					receptorPago.setSucursal(utilerias.getString(mapaResultado.get("SUCURSAL_BCO_REC"))); /** Set SUCURSAL_BCO_REC **/
					receptorPago.setConceptoPago(utilerias.getString(mapaResultado.get("CONCEPTO_PAGO"))); /** Set CONCEPTO_PAGO **/
					receptorPago.setComentario1(utilerias.getString(mapaResultado.get("COMENTARIO1"))); /** Set COMENTARIO1 **/
					receptorPago.setComentario2(utilerias.getString(mapaResultado.get("COMENTARIO2"))); /** Set COMENTARIO2 **/
					receptorPago.setComentario3(utilerias.getString(mapaResultado.get("COMENTARIO3"))); /** Set COMENTARIO3 **/
					
					
					if (ValidadorMonitor.isSPID(modulo)) {
						ordenantePago.setDomicilio(utilerias.getString(mapaResultado.get("DOMICILIO_ORD"))); /** Set DOMICILIO_ORD **/
						ordenantePago.setRfc(utilerias.getString(mapaResultado.get("RFC_ORD"))); /** Set RFC_ORD **/
						ordenantePago.setFechaConstit(utilerias.getString(mapaResultado.get("FCH_CONSTIT_ORD"))); /** Set FCH_CONSTIT_ORD **/
						ordenantePago.setCodigoPostal(utilerias.getString(mapaResultado.get("COD_POSTAL_ORD"))); /** Set COD_POSTAL_ORD **/
						
					}
					beanDetallePago.setBeanDatosGeneralesPago(beanDatosGeneralesPago);
					beanDetallePago.setBeanReceptorPago(receptorPago);
					beanDetallePago.setBeanOrdenantePago(ordenantePago);
	}
	
	/**
	 * Datos generales.
	 *
	 * metodo para recorrer el resultado de la BD
	 * y setearlo en el objeto de salida.
	 * 
	 * @param utilerias El objeto: utilerias
	 * @param mapaResultado El objeto: mapa resultado
	 * @param beanDatosGeneralesPago El objeto: bean datos generales pago
	 * @param modulo El objeto: modulo
	 */
	private void datosGenerales(Utilerias utilerias, Map<String, Object> mapaResultado,
			BeanDatosGeneralesPago datosGeneralesPago, BeanModulo modulo) {
		datosGeneralesPago.setBeanDatosGeneralesPagoDos(new BeanDatosGeneralesPagoDos());
		
		datosGeneralesPago.setClaveTransferencia(utilerias.getString(mapaResultado.get("CVE_TRANSFE")));
		datosGeneralesPago.setFormaLiquidacion(utilerias.getString(mapaResultado.get("FORMA_LIQ")));
		datosGeneralesPago.setReferencia(utilerias.getString(mapaResultado.get("REFERENCIA")));
		datosGeneralesPago.setCola(utilerias.getString(mapaResultado.get("CVE_COLA")));
		datosGeneralesPago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA")));
		datosGeneralesPago.setEmpresa(utilerias.getString(mapaResultado.get("CVE_EMPRESA")));
		datosGeneralesPago.setClaveOperacion(utilerias.getString(mapaResultado.get("CVE_OPERACION")));
		datosGeneralesPago.setReferenciaMedio(utilerias.getString(mapaResultado.get("REFERENCIA_MED")));
		datosGeneralesPago.setMedioEntrega(utilerias.getString(mapaResultado.get("CVE_MEDIO_ENT")));
		datosGeneralesPago.setUsuarioSolicito(utilerias.getString(mapaResultado.get("CVE_USUARIO_SOL")));
		datosGeneralesPago.setFechaCaptura(utilerias.getString(mapaResultado.get("FCH_CAPTURA")));
		datosGeneralesPago.setUsuarioCapturo(utilerias.getString(mapaResultado.get("CVE_USUARIO_CAP")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setClaveDevolucion(utilerias.getString(mapaResultado.get("CVE_DEVOLUCION")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setRefeMecanismo(utilerias.getString(mapaResultado.get("REFERENCIA_MECA")));
		datosGeneralesPago.setRefeConcent(utilerias.getString(mapaResultado.get("REFERENCIA_CON")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setMotivoDevolucion(utilerias.getString(mapaResultado.get("MOTIVO_DEVOL")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setRefeNumerica(utilerias.getString(mapaResultado.get("REFE_NUMERICA")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setFolioPaquete(utilerias.getString(mapaResultado.get("FOLIO_PAQUETE")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setFolioPago(utilerias.getString(mapaResultado.get("FOLIO_PAGO")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setClaveRastreo(utilerias.getString(mapaResultado.get("CVE_RASTREO")));
		datosGeneralesPago.getBeanDatosGeneralesPagoDos().setEstatus(utilerias.getString(mapaResultado.get(CONST_ESTATUS)));
		
		if (ValidadorMonitor.isSPEI(modulo)) {
			datosGeneralesPago.getBeanDatosGeneralesPagoDos().setDireccionIP(utilerias.getString(mapaResultado.get("DIRECCION_IP")));
			datosGeneralesPago.getBeanDatosGeneralesPagoDos().setFechaInstruccionPago(utilerias.getString(mapaResultado.get("FCH_INSTRUC_PAGO")));
			datosGeneralesPago.getBeanDatosGeneralesPagoDos().setHoraInstruccionPago(utilerias.getString(mapaResultado.get("HORA_INSTRUC_PAGO")));
		}
	}
	
	/**
	 * Obtener formato query.
	 *
	 * metodo para crear el query y retornarlo al flujo
	 * 
	 * @param queryCo El objeto: query co
	 * @return Objeto string builder
	 */
	public StringBuilder obtenerFormatoQuery(StringBuilder queryCo) {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") 
		   .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") 
		   .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") 
		   .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        
		   .append(FROM) 
				 .append("SELECT TM.*, ROWNUM AS RN ")  
			 	 .append(FROM)
			 	 	   .append("SELECT TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, TM.CVE_MECANISMO,")
			 	 	   .append("TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC,")
		 	 	   	   .append("TM.IMPORTE_ABONO, TM.ESTATUS, TM.COMENTARIO3, TM.REFERENCIA_MECA, CONTADOR.CONT ") 
		   			   .append(FROM)
						   		  .append(CONST_COUNT) 
				   				  .append("FROM TRAN_MENSAJE TM, [TB_EXTRA] TMS  ") 
				   				.append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
					            .append("AND TM.REFERENCIA = TMS.REFERENCIA [PARAM_ORIGEN]")
							  .append(queryCo.toString())
							  .append(CONST_CONTADOR)
							  .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
					            .append("AND TM.REFERENCIA = TMS.REFERENCIA [PARAM_ORIGEN]")
					  .append(queryCo.toString())
					  .append(") TM ")
			   .append(")TM ")
		   .append("WHERE RN BETWEEN ? AND ? ");
		return queryCompleto;
			}
	
	
	
	/**
	 * Condicion fields.
	 *
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param queryCond El objeto: query co
	 */
	public void condicionFields(String field, String valor, StringBuilder queryCond) {
		if (REFERENCIAMIN.equals(field)) {
			queryCond.append(" AND TM.REFERENCIA='").append(valor).append("'").toString();
		} else if (COLAMIN.equals(field)) {
			queryCond.append(" AND TM.CVE_COLA='").append(valor).append("'").toString();
		} else if (CVE_TRAN.equals(field)) {
			queryCond.append(" AND TM.CVE_TRANSFE='").append(valor).append("'").toString();
		} else if (CVE_MECAN.equals(field)) {
			queryCond.append(" AND TM.CVE_MECANISMO='").append(valor).append("'").toString();
		} else if (ORD.equals(field)) {
			queryCond.append(" AND TM.CVE_INTERME_ORD='").append(valor).append("'").toString();
		} else if (REC.equals(field)) {
			queryCond.append(" AND TM.CVE_INTERME_REC='").append(valor).append("'").toString();
		} else if (IMP.equals(field)) {
			queryCond.append(" AND TM.IMPORTE_ABONO=").append(valor).append("").toString();
		}
	}
	
	/**
	 * Obtener formato query montos.
	 * 
	 * metodo para obtener un query y retornarlo al flujo
	 * 
	 * @param queryCo El objeto: query co
	 * @return Objeto string builder
	 */
	public StringBuilder obtenerFormatoQueryMontos(StringBuilder queryCo) {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
		   			   .append(FROM)
						   		  .append(CONST_COUNT) 
				   				  .append("FROM TRAN_MENSAJE TM, [TB_EXTRA] TMS   ") 
				   				.append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
							  .append(queryCo.toString())
							  .append(CONST_CONTADOR)
							  .append(WHERE).append(ESTATUS).append(IN).append(ESTATUS).append("] ") 
					            .append(AND).append(CVE_MECANISMO).append(OPEN_SP).append(CVE_MECANISMO).append(CLOSE_SP)
					  .append(queryCo.toString()).append(" AND TM.REFERENCIA = TMS.REFERENCIA ");
		return queryCompleto;
			}
}
