/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:55:00 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepcionesDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasRecepciones.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los flujos de la pantalla de Recepciones.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class UtileriasDetRecepciones implements Serializable {


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3523715617360488504L;
	
	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasDetRecepciones utils;
	
	/** La constante QUERY_SPID. */
	private static final String QUERY_SPID =
	        " R.FCH_OPERACION = B.FCH_OPERACION "+
	        " AND R.CVE_MI_INSTITUC = B.CVE_MI_INSTITUC "+
	        " AND R.CVE_INST_ORD = B.CVE_INST_ORD "+
	        " AND R.FOLIO_PAQUETE = B.FOLIO_PAQUETE "+
	        " AND R.FOLIO_PAGO = B.FOLIO_PAGO ";
	
	/** La constante QUERY_SPEI. */
	private static final String QUERY_SPEI =
			" R.FCH_OPERACION = B.FCH_OPER_PK"+
					" AND R.CVE_MI_INSTITUC = B.COD_MI_INSTI_PK "+
					" AND R.CVE_INST_ORD = B.COD_INST_ORD_PK "+
					" AND R.FOLIO_PAQUETE = B.ID_FOL_PAQ_PK "+
					" AND R.FOLIO_PAGO = B.ID_FOL_PAGO_PK ";
	
	
	/** La constante CONST_Q1. */
	private static final String CONST_Q1 = " ((ESTATUS_TRANSFER = 'PA' AND ESTATUS_BANXICO = 'LQ' ) OR ";
	
	/** La constante CONST_Q2. */
	private static final String CONST_Q2 = " (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ";
	
	/** La constante CONST_WHERE. */
	private static final String CONST_WHERE = " WHERE  ";
	
	/** La constante TOPOLOGIA. */
	private static final String TOPOLOGIA = "TOPOLOGIA";
	
	/** La constante TIPO_PAGO. */
	private static final String TIPO_PAGO = "TIPO_PAGO";
	
	/** La constante ESTATUS_BANXICO. */
	private static final String ESTATUS_BANXICO = "ESTATUS_BANXICO";
	
	/** La constante ESTATUS_TRANSFER. */
	private static final String ESTATUS_TRANSFER = "ESTATUS_TRANSFER";
	
	/** La constante FCH_OPERACION. */
	private static final String FCH_OPERACION = "FCH_OPERACION";
	
	/** La constante FCH_CAPTURA. */
	private static final String FCH_CAPTURA = "FCH_CAPTURA";
	
	/** La constante CVE_INST_ORD. */
	private static final String CVE_INST_ORD = "CVE_INST_ORD";
	
	/** La constante CVE_INTERME_ORD. */
	private static final String CVE_INTERME_ORD = "CVE_INTERME_ORD";
	
	/** La constante FOLIO_PAQUETE. */
	private static final String FOLIO_PAQUETE = "FOLIO_PAQUETE";
	
	/** La constante FOLIO_PAGO. */
	private static final String FOLIO_PAGO = "FOLIO_PAGO";
	
	/** La constante NUM_CUENTA_REC. */
	private static final String NUM_CUENTA_REC = "NUM_CUENTA_REC";
	
	/** La constante MONTO. */
	private static final String MONTO = "MONTO";
	
	/** La constante MOTIVO_DEVOL. */
	private static final String MOTIVO_DEVOL = "MOTIVO_DEVOL";
	
	/** La constante CODIGO_ERROR. */
	private static final String CODIGO_ERROR = "CODIGO_ERROR";
	
	/** La constante NUM_CUENTA_TRAN. */
	private static final String NUM_CUENTA_TRAN = "NUM_CUENTA_TRAN";
	
	
	
	/** La constante utilsSet. */
	private static final UtileriasSetRecepciones utilsSet = UtileriasSetRecepciones.getUtils();
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils como instancia
	 * unica de la clase
	 */
	public static UtileriasDetRecepciones getUtils() {
		if (utils == null) {
			utils = new UtileriasDetRecepciones();
		}
		return utils;
	}
	
	/**
	 * Generar formato query.
	 *
	 * Metodo para formar el query que sera ejecutado
	 * 
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param tipoOrden El objeto: tipo orden
	 * @param modulo El objeto: modulo
	 * @return Objeto string builder
	 */
	public StringBuilder generarFormatoQuery(BeanReqConsultaRecepciones beanReqConsultaRecepciones, String tipoOrden, BeanModulo modulo) {
		StringBuilder consultaRecepFormat = 
				new StringBuilder();
		consultaRecepFormat.append("SELECT T.* FROM (SELECT T.*, ROWNUM R FROM (SELECT TOPOLOGIA, ")
		.append("TIPO_PAGO,ESTATUS_BANXICO, ESTATUS_TRANSFER, ")
		.append("TO_CHAR(FCH_OPERACION,'DD/MM/YYYY') FCH_OPERACION,TO_CHAR(FCH_CAPTURA,'HH24:MI') FCH_CAPTURA,CVE_INST_ORD, ")
		.append("CVE_INTERME_ORD,FOLIO_PAQUETE,FOLIO_PAGO, NUM_CUENTA_REC, ")
		.append("NUM_CUENTA_TRAN,MONTO, CODIGO_ERROR, MOTIVO_DEVOL,C.CONT ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT FROM TRAN_SPID_REC ")
		.append(CONST_WHERE);
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(CONST_Q1);
			consultaRecepFormat.append(CONST_Q2);
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos por Aplicar");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Aplicados");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Rechazados");
		}
		
		consultaRecepFormat.append(" AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') [ORIGEN]) C ")
		.append(CONST_WHERE);
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(CONST_Q1);
			consultaRecepFormat.append(CONST_Q2);
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(" ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ' ");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepFormat.append(" ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL ");
		}
		consultaRecepFormat.append("AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') [ORIGEN] ");
		
		String sortField = getSortField(beanReqConsultaRecepciones);
		
		if (!"".equals(sortField)){
			consultaRecepFormat.append(" ORDER BY ").append(sortField).append(" ").append(beanReqConsultaRecepciones.getSortType());
		}	
		
		consultaRecepFormat.append(") T) T WHERE R BETWEEN ? AND ?  ");	
		
		return consultaRecepFormat;
	}
	
	/**
	 * Obtener el objeto: sort field de la consulta recepciones.
	 * Metodo para validar el filtro que se esta llevando a cabo
	 * en base al campo.
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @return El objeto: sort field objeto de retorno
	 */
	private String getSortField(BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		String sortFieldRecep = "";
		
		if ("topologia".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = TOPOLOGIA;
		} else if ("tipoPago".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = TIPO_PAGO;
		} else if ("estatusBanxico".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = ESTATUS_BANXICO;
		} else if ("estatusTransfer".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = ESTATUS_TRANSFER;
		} else if ("fechOperacion".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = FCH_OPERACION;
		} else if ("fechCaptura".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = FCH_CAPTURA;
		} else if ("cveInsOrd".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldRecep = CVE_INST_ORD;
		} else {
			sortFieldRecep = getSortFieldPart(beanReqConsultaRecepciones);			
		}
		return sortFieldRecep;
	}
	
	/**
	 * Metodo que sirve para ordenar 
	 * @param beanReqConsultaRecepciones - bean de parametros
	 * @return string - return string
	 */
	private String getSortFieldPart(BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		
		String sortFieldPartRecep = "";
		
		if ("cveIntermeOrd".equals(beanReqConsultaRecepciones.getSortField())){
			sortFieldPartRecep = CVE_INTERME_ORD;
			} else if ("folioPaquete".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = FOLIO_PAQUETE;
			} else if ("folioPago".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = FOLIO_PAGO;
			} else if ("numCuentaRec".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = NUM_CUENTA_REC;
			} else if ("monto".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = MONTO;
			} else if ("motivoDevol".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = MOTIVO_DEVOL;
			} else if ("codigoError".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = CODIGO_ERROR;
			} else if ("numCuentaTran".equals(beanReqConsultaRecepciones.getSortField())){
				sortFieldPartRecep = NUM_CUENTA_TRAN;
			}
		return sortFieldPartRecep;
	}
	
	/**
	 * Generar query monto.
	 *
	 * Metodo para formar el query que sera ejecutado
	 * 
	 * @param tipoOrden El objeto: tipo orden
	 * @return Objeto string builder
	 */
	public StringBuilder generarQueryMonto( String tipoOrden) {
		StringBuilder consultaRecepciones = 
				new StringBuilder();
		consultaRecepciones.append("SELECT ")
		.append("sum(MONTO) MONTO_TOTAL ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT FROM TRAN_SPID_REC ")
		.append(CONST_WHERE);
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(CONST_Q1);
			consultaRecepciones.append(CONST_Q2);
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ') ");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL) ");
		}
		
		consultaRecepciones.append(" AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') [ORIGEN]) C ")
		.append(CONST_WHERE);
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(CONST_Q1);
			consultaRecepciones.append(CONST_Q2);
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'TR' AND ESTATUS_BANXICO = 'LQ') ");
			
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NOT NULL) ");
		}
		consultaRecepciones.append("AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') [ORIGEN]");
				
		return consultaRecepciones;
	}
	
	/**
	 * Completa query.
	 *
	 * Funcion para completar el query en base al 
	 * modulo indicado SPEI o SPID
	 * 
	 * @param query El objeto: query
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	public String completaQuery(String query, BeanModulo modulo) {
		String origen = ValidadorMonitor.getOrigen(modulo);
		origen = origen.replace("TMS.", "");
		query = query.concat(origen);
		if (ValidadorMonitor.isSPEI(modulo)) {
			query = query.replaceAll("\\[QUERY\\]", QUERY_SPEI);
		}
		if (ValidadorMonitor.isSPID(modulo)) {
			query = query.replaceAll("\\[QUERY\\]", QUERY_SPID);
		}
		query = ValidadorMonitor.cambiaQuery(modulo, query);
		return query;
	}
	
	/**
	 * Recorre result.
	 *
	 * Metodo para recorrer el resultado de la BD y setearlo
	 * en el objeto de salida.
	 * 
	 * @param list El objeto: list
	 * @param utilerias El objeto: utilerias
	 * @param beanResConsultaRecepcionesDAO El objeto: bean res consulta recepciones DAO
	 * @return Objeto list
	 */
	public List<BeanConsultaRecepciones> recorreResult(List<HashMap<String, Object>> list,Utilerias utilerias,final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO) {
		BeanConsultaRecepciones beanConsultaRecep = null;
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones = new ArrayList<BeanConsultaRecepciones>();
		for (Map<String, Object> listMap : list) {
			Map<String, Object> mapResult = listMap;
			beanConsultaRecep = new BeanConsultaRecepciones();
			beanConsultaRecep.setBeanConsultaRecepcionesDos(new BeanConsultaRecepcionesDos());
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setTopologia(utilerias.getString(mapResult.get(TOPOLOGIA)));/** Set TOPOLOGIA**/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setTipoPago(utilerias.getInteger(mapResult.get(TIPO_PAGO)));/** Set TIPO_PAGO**/
			beanConsultaRecep
			.getBeanConsultaRecepcionesDos().setEstatusBanxico(utilerias.getString(mapResult.get(ESTATUS_BANXICO)));/** Set ESTATUS_BANXICO**/
			beanConsultaRecep
			.getBeanConsultaRecepcionesDos().setEstatusTransfer(utilerias.getString(mapResult.get(ESTATUS_TRANSFER))); /** Set ESTATUS_TRANSFER**/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setFechOperacion(utilerias.getString(mapResult.get(FCH_OPERACION))); /** Set FCH_OPERACION **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setFechCaptura(utilerias.getString(mapResult.get(FCH_CAPTURA))); /** Set FCH_CAPTURA **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setCveInsOrd(utilerias.getString(mapResult.get(CVE_INST_ORD))); /** Set CVE_INST_ORD **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD))); /** Set CVE_INTERME_ORD **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setFolioPaquete(utilerias.getString(mapResult.get(FOLIO_PAQUETE))); /** Set FOLIO_PAQUETE **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setFolioPago(utilerias.getString(mapResult.get(FOLIO_PAGO))); /** Set FOLIO_PAGO **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setNumCuentaTran(utilerias.getString(mapResult.get(NUM_CUENTA_TRAN))); /** Set NUM_CUENTA_TRAN **/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setMonto(utilerias.getString(mapResult.get(MONTO))); /** Set MONTO **/
			beanConsultaRecep.setCodigoError(utilerias.getString(mapResult.get(CODIGO_ERROR))); /** Set CODIGO_ERROR**/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setMotivoDevol(utilerias.getString(mapResult.get(MOTIVO_DEVOL))); /** Set MOTIVO_DEVOL**/
			beanConsultaRecep.getBeanConsultaRecepcionesDos().setNumCuentaRec(utilerias.getString(mapResult.get(NUM_CUENTA_REC))); /** Set NUM_CUENTA_REC**/

			listaBeanConsultaRecepciones.add(beanConsultaRecep);
			beanResConsultaRecepcionesDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
		}
		return listaBeanConsultaRecepciones;
	}
	
	/**
	 * Llenar consulta recepciones.
	 *
	 * Funcion para llenar el objeto de salida
	 * 
	 * @param beanResConsultaRecepcionesDAO El objeto: bean res consulta recepciones
	 *                                      DAO
	 * @param list                          El objeto: list
	 * @param utilerias                     El objeto: utilerias
	 * @return Objeto list
	 */
	public List<BeanConsultaRecepciones> llenarConsultaRecepciones(
			final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO, List<HashMap<String, Object>> list,
			Utilerias utilerias) {
		return recorreResult(list, utilerias, beanResConsultaRecepcionesDAO);
	}
	
	/**
	 * Adds the importe.
	 *
	 * Metodo para agregar importe
	 * 
	 * @param totalImporte El objeto: total importe
	 * @param list El objeto: list
	 * @return Objeto big decimal
	 */
	public BigDecimal addImporte(BigDecimal totalImporte,List<HashMap<String, Object>> list) {
		for (Map<String, Object> mapResult : list) {
			totalImporte = totalImporte
					.add(new BigDecimal(Utilerias.getUtilerias().getString(mapResult.get("MONTO_TOTAL"))));
		}
		return totalImporte;
	}
	
	/**
	 * Completa metodo.
	 * 
	 * Proceso para terminar de setear los datos del metodo
	 * principal de consulta.
	 * 
	 * @param list El objeto: list
	 * @param beanResRecepcionDAO El objeto: bean res recepcion DAO
	 * @param responseDTO El objeto: response DTO
	 * @return Objeto bean res recepcion DAO
	 */
	public BeanResRecepcionDAO completaMetodo(List<HashMap<String, Object>> list,BeanResRecepcionDAO beanResRecepcionDAO,
			ResponseMessageDataBaseDTO responseDTO) {
		BeanReceptoresSPID aux = null;
		for (Map<String, Object> mapResult : list) {
			aux = utilsSet.descomponeConsulta(mapResult);
			beanResRecepcionDAO.setBeanReceptorSPID(aux);
			beanResRecepcionDAO.setCodError(responseDTO.getCodeError());
			beanResRecepcionDAO.setMsgError(responseDTO.getMessageError());
		}
		return beanResRecepcionDAO;
	}
	

}
