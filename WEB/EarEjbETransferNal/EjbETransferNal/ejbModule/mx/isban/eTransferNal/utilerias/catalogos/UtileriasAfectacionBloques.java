/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasAfectacionBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:05:55 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * Class UtileriasAfectacionBloques.
 *
 * Clase de utilerias que contiene metodos que ayudan al flujo normal
 * a realizar los procesos del negocio.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public class UtileriasAfectacionBloques implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 717891016974581046L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasAfectacionBloques instancia;
	
	/** La constante ALTA. */
	public static final String ALTA = "alta";
	
	/** La constante BAJA. */
	public static final String BAJA = "baja";
	
	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";
	
	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT NUM_BLOQUE || ',' || VALOR || ',' || DESCRIPCION FROM TRAN_AFECTA_BLOQUES [FILTRO_WH]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY NUM_BLOQUE";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "Numero de Bloque, Valor, Descripcion";
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/**
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return instanica: Regresa el objeto instancia cuando esta es nula
	 * 
	 */
	public static UtileriasAfectacionBloques getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasAfectacionBloques();
		}
		return instancia;
	}
	
	/**
	 * obtiene el filtro para la realización de la búsqueda de registros.
	 *
	 * @param beanFiltro --> dato insertado en la caja de texto cuyo valor es tomado como referencia para realizar la consulta a la BD
	 * @param key --> Id de la caja de texto en la cual se insertó el dato.
	 * @return filtro --> regresa la cadena con el valor del filtro
	 */
	public String getFiltro(BeanBloque beanFiltro, String key) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getNumBloque() != null && !beanFiltro.getNumBloque().trim().isEmpty()) {
			getFiltroXParamNumber(filtro, key, "NUM_BLOQUE", beanFiltro.getNumBloque().trim());
			
		}
		if (beanFiltro.getValor() != null && !beanFiltro.getValor().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "VALOR", beanFiltro.getValor().trim());
		}
		if (beanFiltro.getDescripcion() != null && !beanFiltro.getDescripcion().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "DESCRIPCION", beanFiltro.getDescripcion().trim());
		}
		return filtro.toString();
	}
	
	/**
	 * Obtiene los filtros por cada parámetro no null.
	 *
	 * @param filtro --> variable que obtiene los datos almacenados en cada caja de texto
	 * @param key --> Id de la caja de texto de la cual se obtiene el dato.
	 * @param param --> tipo de parámetro.
	 * @param value --> valor del parámetro obtenido.
	 * @return El objeto: filtro X param
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	/**
	 * Obtiene los filtros por cada número de parámetro .
	 *
	 * @param filtro --> variable que obtiene los datos almacenados en cada caja de texto
	 * @param key --> Id de la caja de texto de la cual se obtiene el dato.
	 * @param param --> tipo de parámetro.
	 * @param value --> valor del parámetro obtenido.
	 * @return El objeto: filtro X param number
	 */
	private void getFiltroXParamNumber(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" "+param+"="+value);
			first = false;
		} else {
			filtro.append(" AND "+param+"="+value);
		}
	}
	
	/**
	 * Obtiene un objeto string.
	 *
	 * @param obj --> objeto de tipo Object que obtiene registros
	 * @return cad --> variable de tipo cad que se le asigna el contenido del objeto obj
	 */
	public String getString(Object obj){
		String cad = "";
		if(obj!= null){
			cad = obj.toString().trim();
		}
		return cad;
	}
	
	/**
	 * Obtiene un integer.
	 *
	 * @param obj --> objeto de tipo Object que obtiene registros
	 * @return cad --> variable de tipo cad que se le asigna el contenido del objeto obj
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
	
	/**
	 * Obtiene un mensaje de bd.
	 *
	 * @param responseDAO --> trae la respuesta del dao
	 * @return error --> muestra el mensaje obtenido del dao
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	/**
	 * Agregar parametros.
	 *
	 * Funcion para asignar los parametros utilizados por las operaciones en el flujo
	 * 
	 * @param beanBloque --> objeto BeanBloque que otiene bloques
	 * @param operacion --> variable de tipo string que almacena el tipo de operacion ejecutado
	 * @return parametros --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	public List<Object> agregarParametros(BeanBloque beanBloque, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ALTA)) {
			parametros.add(beanBloque.getNumBloque());
			parametros.add(beanBloque.getValor());
			parametros.add(beanBloque.getDescripcion());
		}
		if(operacion.equalsIgnoreCase(MODIFICACION)) {
			String[] data = beanBloque.getValor().split(Pattern.quote("|"));
			parametros.add(data[0]);
			parametros.add(beanBloque.getDescripcion());
			parametros.add(beanBloque.getNumBloque());
			parametros.add(data[1]);
		}
		if(operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
			parametros.add(beanBloque.getNumBloque());
			parametros.add(beanBloque.getValor());
		}
		return parametros;
	}
}
