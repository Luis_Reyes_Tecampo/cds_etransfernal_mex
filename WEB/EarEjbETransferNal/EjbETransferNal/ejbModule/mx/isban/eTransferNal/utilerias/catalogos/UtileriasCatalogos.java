/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasCatalogos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 03:33:06 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * Class UtileriasCatalogos.
 * 
 * Clase de utilerias que contiene metodos 
 * utilizados por varios flujos de los catalogos.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public final class UtileriasCatalogos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7280179182205665638L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasCatalogos instancia;
	
	
	/**
	 * Nueva instancia utilerias catalogos.
	 */
	private UtileriasCatalogos() {
	}

	/**
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return instanica: Regresa el objeto instancia cuando esta es nula
	 * 
	 */
	public static UtileriasCatalogos getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasCatalogos();
		}
		return instancia;
	}
	
	/**
	 * Obtiene un objeto String
	 * 
	 * @param obj --> objeto de tipo string que trae los registros
	 * @return cad --> el metodo regresa la cadena cad con el valor del objeto obj sin espacios 
	 */
	public String getString(Object obj){
		String cad = "";
		if(obj!= null){
			cad = obj.toString().trim();
		}
		return cad;
	}
	
	/**
	 * Obtener un valor integer
	 * 
	 * @param obj --> objeto que almacena enteros
	 * @return cad --> regresa los elementos del objeto integer
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
	
	/**
	 * funcion para obtener y setear la respuesta
	 * obtenida.
	 * 
	 * @param responseDAO --> objeto que trae la respuesta de la base de datos.
	 * @return error --> regresa el mensaje de la base de datos.
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
}
