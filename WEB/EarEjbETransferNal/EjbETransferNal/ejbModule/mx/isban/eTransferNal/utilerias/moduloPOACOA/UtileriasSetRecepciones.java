/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasSetRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/09/2019 02:46:23 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBloqueo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDOpciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasSetRecepciones.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los datos de respuesta
 * para la pantalla de Recepciones. 
 * 
 * @author FSW-Vector
 * @since 26/09/2019
 */
public class UtileriasSetRecepciones implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -7841265464849301269L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasSetRecepciones utils;
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils de instancia unica
	 */
	public static UtileriasSetRecepciones getUtils() {
		if (utils == null) {
			utils = new UtileriasSetRecepciones();
		}
		return utils;
	}
	
	/**
 	 * Descompone consulta.
 	 *
 	 * Metodo padre que invoca metodos
 	 * para setear cada uno de los objetos 
 	 * 
 	 * @param map El objeto: map
 	 * @return Objeto bean receptores SPID
 	 */
 	public BeanReceptoresSPID descomponeConsulta(Map<String,Object> map){
	    	Utilerias utilerias = Utilerias.getUtilerias();
	    	BeanSPIDBloqueo bloqueoSPID = new BeanSPIDBloqueo();
	    	BeanReceptoresSPID receptoresSPID = new BeanReceptoresSPID();
	    	receptoresSPID.setLlave(seteaLlave(map));
	    	receptoresSPID.setDatGenerales(seteaDatosGenerales(map));
	    	receptoresSPID.setEstatus(seteaEstatus(map));
	    	receptoresSPID.setBancoOrd(seteaBancoOrd(map));
	    	receptoresSPID.setBancoRec(seteaBancoRec(map));
	    	receptoresSPID.setEnvDev(seteaEnvDev(map));
	    	receptoresSPID.setRastreo(seteaRastreo(map));
	    	receptoresSPID.setFchPagos(seteaFchPago(map));
	    	receptoresSPID.setOpciones(new BeanSPIDOpciones());
	    	receptoresSPID.setBloqueo(bloqueoSPID);
	    	
	    	bloqueoSPID.setUsuario(utilerias.getString(map.get("CVE_USUARIO")));
	    	if("RECEP_OPSPID".equals(utilerias.getString(map.get("FUNCIONALIDAD")))){
	    		bloqueoSPID.setBloqueado(true);	
	    	}
	    	return receptoresSPID;
	    }
	 
	 /**
 	 * Metodo que setea el folio paquete y pago de devolucion para los envios.
 	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
 	 * @return BeanSPIDEnvDev Bean de respuesta para retornarlo
 	 */
	    private BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
	    	Utilerias utileriasEnvDev = Utilerias.getUtilerias();
	    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
	    	beanSPIDEnvDev.setMotivoDev(utileriasEnvDev.getString(map.get("MOTIVO_DEVOL"))); /** Set MOTIVO_DEVOL **/
	    	beanSPIDEnvDev.setFolioPaqDev(utileriasEnvDev.getString(map.get("FOLIO_PAQ_DEV"))); /** Set FOLIO_PAQ_DEV **/
	    	beanSPIDEnvDev.setFolioPagoDev(utileriasEnvDev.getString(map.get("FOLIO_PAGO_DEV"))); /** Set FOLIO_PAGO_DEV **/
	    	
	    	return beanSPIDEnvDev;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de los campos llave.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDLlave Bean de respuesta para retornar
    	 */
	    private BeanSPIDLlave seteaLlave(Map<String,Object> map){
	    	Utilerias utileriasLlave = Utilerias.getUtilerias();
	    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
	    	beanSPIDLlave.setFchOperacion(utileriasLlave.getString(map.get("FCH_OPERACION"))); /** Set FCH_OPERACION **/
	    	beanSPIDLlave.setCveMiInstituc(utileriasLlave.getString(map.get("CVE_MI_INSTITUC"))); /** Set CVE_MI_INSTITUC **/
	    	beanSPIDLlave.setCveInstOrd(utileriasLlave.getString(map.get("CVE_INST_ORD"))); /** Set CVE_INST_ORD **/
	    	beanSPIDLlave.setFolioPaquete(utileriasLlave.getString(map.get("FOLIO_PAQUETE"))); /** Set FOLIO_PAQUETE **/
	    	beanSPIDLlave.setFolioPago(utileriasLlave.getString(map.get("FOLIO_PAGO"))); /** Set FOLIO_PAGO **/
	    	return beanSPIDLlave;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de los campos generales.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDDatGenerales Bean de respuesta de datos generales para retornar
    	 */
	    private BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
	    	Utilerias utileriasGen = Utilerias.getUtilerias();
	    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
	    	BigDecimal monto = utileriasGen.getBigDecimal(map.get("MONTO")); /** Set  MONTO **/
	    	beanSPIDDatGenerales.setTopologia(utileriasGen.getString(map.get("TOPOLOGIA"))); /** Set TOPOLOGIA **/
	    	beanSPIDDatGenerales.setPrioridad(utileriasGen.getString(map.get("PRIORIDAD"))); /** Set PRIORIDAD **/
	    	beanSPIDDatGenerales.setTipoPago(utileriasGen.getString(map.get("TIPO_PAGO"))); /** Set TIPO_PAGO **/
	    	beanSPIDDatGenerales.setTipoOperacion(utileriasGen.getString(map.get("TIPO_OPERACION"))); /** Set TIPO_OPERACION **/
	    	beanSPIDDatGenerales.setRefeTransfer(utileriasGen.getString(map.get("REFE_TRANSFER"))); /** Set REFE_TRANSFER **/
	    	beanSPIDDatGenerales.setConceptoPago(utileriasGen.getString(map.get("CONCEPTO_PAGO"))); /** Set CONCEPTO_PAGO **/
	    	beanSPIDDatGenerales.setFchCaptura(utileriasGen.getString(map.get("FCH_CAPTURA"))); /** Set FCH_CAPTURA **/
	    	beanSPIDDatGenerales.setCde(utileriasGen.getString(map.get("CDE"))); /** Set CDE **/
	    	beanSPIDDatGenerales.setCda(utileriasGen.getString(map.get("CDA"))); /** Set CDA **/
	    	beanSPIDDatGenerales.setEnviar(utileriasGen.getString(map.get("ENVIAR"))); /** Set ENVIAR **/
	    	beanSPIDDatGenerales.setClasifOperacion(utileriasGen.getString(map.get("CLASIF_OPERACION"))); /** Set CLASIF_OPERACION **/
	    	
	    	beanSPIDDatGenerales.setMonto(monto);
	    	return beanSPIDDatGenerales;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de los campos generales.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDEstatus Bean de respuesta de estatus para retornar
    	 */
	    private BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
	    	Utilerias utileriasEst = Utilerias.getUtilerias();
	    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
	    	beanSPIDEstatus.setEstatusBanxico(utileriasEst.getString(map.get("ESTATUS_BANXICO"))); /** Set ESTATUS_BANXICO **/
	    	beanSPIDEstatus.setEstatusTransfer(utileriasEst.getString(map.get("ESTATUS_TRANSFER"))); /** Set ESTATUS_TRANSFER **/
	    	beanSPIDEstatus.setCodigoError(utileriasEst.getString(map.get("CODIGO_ERROR"))); /** Set CODIGO_ERROR **/
	    	return beanSPIDEstatus;
	    }
	    
    	/**
    	 * Metodo que realiza el seteo de los campos del banco ordenante.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante para retornar
    	 */
	    private BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
	    	Utilerias utileriasBanOrd = Utilerias.getUtilerias();
	    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
	    	beanSPIDBancoOrd.setNumCuentaOrd(utileriasBanOrd.getString(map.get("NUM_CUENTA_ORD"))); /** Set NUM_CUENTA_ORD **/
	    	beanSPIDBancoOrd.setTipoCuentaOrd(utileriasBanOrd.getString(map.get("TIPO_CUENTA_ORD"))); /** Set TIPO_CUENTA_ORD **/
	    	beanSPIDBancoOrd.setRfcOrd(utileriasBanOrd.getString(map.get("RFC_ORD"))); /** Set RFC_ORD **/
	    	beanSPIDBancoOrd.setNombreOrd(utileriasBanOrd.getString(map.get("NOMBRE_ORD"))); /** Set NOMBRE_ORD **/
	    	beanSPIDBancoOrd.setCveIntermeOrd(utileriasBanOrd.getString(map.get("CVE_INTERME_ORD"))); /** Set CVE_INTERME_ORD **/
	    	beanSPIDBancoOrd.setDomicilioOrd(utileriasBanOrd.getString(map.get("DOMICILIO_ORD"))); /** Set DOMICILIO_ORD **/
	    	beanSPIDBancoOrd.setRefeNumerica(utileriasBanOrd.getString(map.get("REFE_NUMERICA"))); /** Set REFE_NUMERICA **/
	    	beanSPIDBancoOrd.setCodPostalOrd(utileriasBanOrd.getString(map.get("COD_POSTAL_ORD"))); /** Set COD_POSTAL_ORD **/
	    	beanSPIDBancoOrd.setFchConstitOrd(utileriasBanOrd.getString(map.get("FCH_CONSTIT_ORD"))); /** Set FCH_CONSTIT_ORD **/
	    	
	    	return beanSPIDBancoOrd;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de los campos de rastreo.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
    	 */
	    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
	    	/** Se crea la instancia de utilerias*/
	    	Utilerias utileriasRast = Utilerias.getUtilerias();
	    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
	    	beanSPIDRastreo.setCveRastreo(utileriasRast.getString(map.get("CVE_RASTREO"))); /** Set CVE_RASTREO **/
	    	beanSPIDRastreo.setCveRastreoOri(utileriasRast.getString(map.get("CVE_RASTREO_ORI"))); /** Set CVE_RASTREO_ORI **/
	    	beanSPIDRastreo.setLongRastreo(utileriasRast.getString(map.get("LONG_RASTREO"))); /** Set LONG_RASTREO **/
	    	beanSPIDRastreo.setDireccionIp(utileriasRast.getString(map.get("DIRECCION_IP")));	 /** Set DIRECCION_IP **/
	    	return beanSPIDRastreo;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de los campos del Banco Receptor.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
    	 */
	    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
	    	/** Se crea la instancia utileriasBanRec de utilerias*/
	    	Utilerias utileriasBanRec = Utilerias.getUtilerias();
	    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
	    	beanSPIDBancoRec.setNombreRec(utileriasBanRec.getString(map.get("NOMBRE_REC"))); /** Set NOMBRE_REC **/
	    	beanSPIDBancoRec.setTipoCuentaRec(utileriasBanRec.getString(map.get("TIPO_CUENTA_REC"))); /** Set TIPO_CUENTA_REC **/
	    	beanSPIDBancoRec.setNumCuentaRec(utileriasBanRec.getString(map.get("NUM_CUENTA_REC"))); /** Set NUM_CUENTA_REC **/
	    	beanSPIDBancoRec.setNumCuentaTran(utileriasBanRec.getString(map.get("NUM_CUENTA_TRAN"))); /** Set NUM_CUENTA_TRAN **/
	    	beanSPIDBancoRec.setRfcRec(utileriasBanRec.getString(map.get("RFC_REC"))); /** Set RFC_REC **/
	    	
	    	/** Se realiza el retorno de beanSPIDBancoRec */
	    	return beanSPIDBancoRec;
	    }
	    
	    /**
    	 * Metodo que realiza el seteo de  los campos de las fechas de pago.
    	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
    	 * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
    	 */
	    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
	    	/** Se crea la instancia utileriasBanRec de utilerias*/
	    	Utilerias utileriasFchPag = Utilerias.getUtilerias();
	    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
	    	beanSPIDFchPagos.setFchAceptPago(utileriasFchPag.getString(map.get("FCH_ACEPT_PAGO"))); /** Set FCH_ACEPT_PAGO **/
	    	beanSPIDFchPagos.setHoraAceptPago(utileriasFchPag.getString(map.get("HORA_ACEPT_PAGO"))); /** Set HORA_ACEPT_PAGO **/
	    	beanSPIDFchPagos.setFchInstrucPago(utileriasFchPag.getString(map.get("FCH_INSTRUC_PAGO"))); /** Set FCH_INSTRUC_PAGO **/
	    	beanSPIDFchPagos.setHoraInstrucPago(utileriasFchPag.getString(map.get("HORA_INSTRUC_PAGO"))); /** Set HORA_INSTRUC_PAGO **/
	    	/** Se realiza el retorno de beanSPIDFchPagos */
	    	return beanSPIDFchPagos;
	    }
}
