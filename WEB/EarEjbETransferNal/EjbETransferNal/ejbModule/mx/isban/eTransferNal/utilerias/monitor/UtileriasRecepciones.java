/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/01/2019 04:49:20 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.monitor;

import java.math.BigDecimal;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEnvDev;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDEstatus;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasRecepciones.
 *
 * @author FSW-Vector
 * @since 28/01/2019
 */
public class UtileriasRecepciones  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -8242875650770861907L;
	
		
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepciones util = new UtileriasRecepciones();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasRecepciones getInstance(){
		return util;
	}
			 	
	/**
	 * Obtener el objeto: sort field.
	 *
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @return String
	 */
	public String getSortField(BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		String sortField = "";
		/**se recorre para asignar el valor sorField**/
		if ("topologia".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "TOPOLOGIA";
		} else if ("tipoPago".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "TIPO_PAGO";
		} else if ("estatusBanxico".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "ESTATUS_BANXICO";
		} else if ("estatusTransfer".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "ESTATUS_TRANSFER";
		} else if ("fechOperacion".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FCH_OPERACION";
		} else if ("fechCaptura".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "FCH_CAPTURA";
		} else if ("cveInsOrd".equals(beanReqConsultaRecepciones.getSortField())){
			sortField = "CVE_INST_ORD";
		} else {
			sortField =  getSortFieldConti(beanReqConsultaRecepciones);
		}
		return sortField;
	}
	
	
	/**
	 * Obtener el objeto: sort field conti.
	 *
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @return El objeto: sort field conti
	 */
	private String getSortFieldConti(BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		String stField = "";
		/**se recorre para asignar el valor sorField**/
		if ("cveIntermeOrd".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "CVE_INTERME_ORD";
		} else if ("folioPaquete".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "FOLIO_PAQUETE";
		} else if ("folioPago".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "FOLIO_PAGO";
		} else if ("numCuentaRec".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "NUM_CUENTA_REC";
		} else if ("monto".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "MONTO";
		} else if ("motivoDevol".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "MOTIVO_DEVOL";
		} else if ("codigoError".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "CODIGO_ERROR";
		} else if ("numCuentaTran".equals(beanReqConsultaRecepciones.getSortField())){
			stField = "NUM_CUENTA_TRAN";
		}
		return stField;
	}
	
	/**
     * Metodo que setea el folio paquete y pago de devolucion.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEnvDev Bean de respuesta
     */
    public BeanSPIDEnvDev seteaEnvDev(Map<String,Object> map){
    	/**inicializa ultilerias**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEnvDev beanSPIDEnvDev = new BeanSPIDEnvDev();
    	/**asigna los valores**/
    	beanSPIDEnvDev.setMotivoDev(utilerias.getString(map.get("MOTIVO_DEVOL")));
    	beanSPIDEnvDev.setFolioPaqDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")));
    	beanSPIDEnvDev.setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")));
    	
    	return beanSPIDEnvDev;
    }
    
    /**
     * Metodo que setea los campos llave.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDLlave Bean de respuesta
     */
    public BeanSPIDLlave seteaLlave(Map<String,Object> map){
    	/**inicializa ultilerias**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDLlave beanSPIDLlave = new BeanSPIDLlave();
    	/**asigna los valores**/
    	beanSPIDLlave.setFchOperacion(utilerias.getString(map.get("FCH_OPERACION")));
    	beanSPIDLlave.setCveMiInstituc(utilerias.getString(map.get("CVE_MI_INSTITUC")));
    	beanSPIDLlave.setCveInstOrd(utilerias.getString(map.get("CVE_INST_ORD")));
    	beanSPIDLlave.setFolioPaquete(utilerias.getString(map.get("FOLIO_PAQUETE")));
    	beanSPIDLlave.setFolioPago(utilerias.getString(map.get("FOLIO_PAGO")));
    	return beanSPIDLlave;
    }
    
    /**
     * Metodo que setea los campos generales.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDDatGenerales Bean de respuesta de datos generales
     */
    public BeanSPIDDatGenerales seteaDatosGenerales(Map<String,Object> map){
    	/**inicializa ultilerias**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDDatGenerales beanSPIDDatGenerales = new BeanSPIDDatGenerales();
    	/**asigna los valores**/
    	BigDecimal monto = utilerias.getBigDecimal(map.get("MONTO"));
    	beanSPIDDatGenerales.setTopologia(utilerias.getString(map.get("TOPOLOGIA")));
    	beanSPIDDatGenerales.setPrioridad(utilerias.getString(map.get("PRIORIDAD")));
    	beanSPIDDatGenerales.setTipoPago(utilerias.getString(map.get("TIPO_PAGO")));
    	beanSPIDDatGenerales.setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")));
    	beanSPIDDatGenerales.setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")));
    	beanSPIDDatGenerales.setConceptoPago(utilerias.getString(map.get("CONCEPTO_PAGO")));
    	beanSPIDDatGenerales.setFchCaptura(utilerias.getString(map.get("FCH_CAPTURA")));
    	beanSPIDDatGenerales.setCde(utilerias.getString(map.get("CDE")));
    	beanSPIDDatGenerales.setCda(utilerias.getString(map.get("CDA")));
    	beanSPIDDatGenerales.setEnviar(utilerias.getString(map.get("ENVIAR")));
    	beanSPIDDatGenerales.setClasifOperacion(utilerias.getString(map.get("CLASIF_OPERACION")));
    	
    	beanSPIDDatGenerales.setMonto(monto);
    	return beanSPIDDatGenerales;
    }
    
    /**
     * Metodo que setea los campos generales.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDEstatus Bean de respuesta de estatus
     */
    public BeanSPIDEstatus seteaEstatus(Map<String,Object> map){
    	/**inicializa ultilerias**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDEstatus beanSPIDEstatus = new BeanSPIDEstatus();
    	/**asigna los valores**/
    	beanSPIDEstatus.setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")));
    	beanSPIDEstatus.setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")));
    	beanSPIDEstatus.setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));
    	return beanSPIDEstatus;
    }
    
    /**
     * Metodo que setea los campos del banco ordenante.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoOrd Bean de respuesta con los datos del banco ordenante
     */
    public BeanSPIDBancoOrd seteaBancoOrd(Map<String,Object> map){
    	/**inicializa ultilerias**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoOrd beanSPIDBancoOrd = new BeanSPIDBancoOrd();
    	/**asigna los valores**/
    	beanSPIDBancoOrd.setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")));
    	beanSPIDBancoOrd.setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")));
    	beanSPIDBancoOrd.setRfcOrd(utilerias.getString(map.get("RFC_ORD")));
    	beanSPIDBancoOrd.setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")));
    	beanSPIDBancoOrd.setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")));
    	beanSPIDBancoOrd.setDomicilioOrd(utilerias.getString(map.get("DOMICILIO_ORD")));
    	beanSPIDBancoOrd.setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")));
    	beanSPIDBancoOrd.setCodPostalOrd(utilerias.getString(map.get("COD_POSTAL_ORD")));
    	beanSPIDBancoOrd.setFchConstitOrd(utilerias.getString(map.get("FCH_CONSTIT_ORD")));
    	
    	return beanSPIDBancoOrd;
    }
	
}
