/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpHTO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:47:39 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
/**
 * en esta clase de utilerias se las funciones para realizar una devolucion extemporanea.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
public class UtileriasHelper  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -8242875650770861907L;
	
		
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasHelper util = new UtileriasHelper();
	
	
	/**
	 * Obten la instancia de UtileriasRecepcionOpHTO.
	 *
	 * @return instancia de UtileriasRecepcionOpHTO
	 */
	public static UtileriasHelper getInstance(){
		return util;
	}
			 
	
    /**
     * Expresion 2.
     *
     * @param beanTranSpeiRec Objeto del tipo BeanTranSpeiRec
     * @return boolean
     */
	public boolean expresion2 (BeanTranSpeiRecOper beanTranSpeiRec){
	   return "9".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())||"11".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago());
    }
	
	
	/**
	 * Buscar seleccionado.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto list
	 */
	public List<BeanTranSpeiRecOper> buscarSeleccionado(BeanReqTranSpeiRec beanReqTranSpeiRec){
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		//se recorre una lista para obtener los selccionados
		for(BeanTranSpeiRecOper beanTranSpeiRec:beanReqTranSpeiRec.getListBeanTranSpeiRec()){
	      	  if(beanTranSpeiRec.isSeleccionado()){
	      		listBeanTranSpeiRec.add(beanTranSpeiRec);
	      	  }
		}
		// se regresa la lista de seleccionados
		return listBeanTranSpeiRec;
	}
	
	/**
	 * Buscar devolucion.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto list
	 */
	public List<BeanTranSpeiRecOper> buscarDevolucion(BeanReqTranSpeiRec beanReqTranSpeiRec){
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		//se recorre una lista para obtener los de devolucion
		for(BeanTranSpeiRecOper beanTranSpeiRec:beanReqTranSpeiRec.getListBeanTranSpeiRec()){
	      	  if(beanTranSpeiRec.isSelecDevolucion()){
	      		listBeanTranSpeiRec.add(beanTranSpeiRec);
	      	  }
		}
		// se regresa la lista de seleccionados
		return listBeanTranSpeiRec;
	}
	
	/**
	 * Buscar recupera.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto list
	 */
	public List<BeanTranSpeiRecOper> buscarRecupera(BeanReqTranSpeiRec beanReqTranSpeiRec){
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		//se recorre una lista para obtener los de recuperacion
		for(BeanTranSpeiRecOper beanTranSpeiRec:beanReqTranSpeiRec.getListBeanTranSpeiRec()){
	      	  if(beanTranSpeiRec.isSelecRecuperar()){
	      		listBeanTranSpeiRec.add(beanTranSpeiRec);
	      	  }
		}
		// se regresa la lista de seleccionados
		return listBeanTranSpeiRec;
	}
	
	
	/**
	 * Obtener valor.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String obtenerValor(BeanTranSpeiRecOper beanTranSpeiRec) {
		String var20 = null;
		if("1".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getPrioridad())){
			var20 ="A";
		}else{
			var20 ="B";
		}
		return var20;
	}
	
	
	/**
	 * Obtener comentario.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @return Objeto string
	 */
	public String obtenerComentario(BeanTranSpeiRecOper beanTranSpeiRec) {
		String comentario="";
		if(beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago1()!= null && beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago1().length()>120){
			comentario=beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago1().substring(0,120);
		}else{
			comentario=beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago1();
		}
		return comentario;
	}
	
	
	 /**
 	 * Valida tubo respuesta.
 	 *
 	 * @param respuesta String con la trama de respuesta
 	 * @return boolean que indica que la respuesta es:   TUBO0870,TUBO0040,TUBO0079,TUBO0569,TUBO5122
 	 */
	public boolean validaTuboRespuesta(String respuesta){
    	return "TUBO0870".equals(respuesta) || "TUBO0040".equals(respuesta) || exp2(respuesta);
    }
    
    /**
     * @param respuesta Objeto del tipo String
     * @return boolean
     */
    private boolean exp2(String respuesta){
    	return "TUBO0079".equals(respuesta) || "TUBO0569".equals(respuesta) || "TUBO5122".equals(respuesta);
    }
}
