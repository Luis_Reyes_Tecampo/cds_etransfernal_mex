/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasConsultaRecep.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/01/2019 04:59:48 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepcionesDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBloqueo;	
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDFchPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDOpciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDRastreo;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class UtileriasDetRecpOp.
 *
 * Clase de utilerias 
 * 
 * @author FSW-Vector
 * @since 28/01/2019
 */
public class UtileriasConsultaRecep extends UtileriasFiltroPago {
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -7702375334609770881L;

	/** La constante util. */
	private static final UtileriasRecepciones utilConsultas = UtileriasRecepciones.getInstance();
	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasConsultaRecep util = new UtileriasConsultaRecep();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasConsultaRecep getInstance(){
		return util;
	}
	
	
	/**
	 * Recorrer lista.
	 *
	 * @param list El objeto: list
	 * @param responseDTO El objeto: response DTO
	 * @return Objeto bean res recepcion DAO
	 */
	public BeanResRecepcionDAO recorrerLista(List<HashMap<String,Object>> list,ResponseMessageDataBaseDTO responseDTO ) {
		/**Se inicializan pbjetos**/
		BeanResRecepcionDAO beanResRecepcionDAO = new BeanResRecepcionDAO();
		BeanReceptoresSPID beanDetReceptorSPID = new BeanReceptoresSPID();
		/** Valida que la lista no este vacia **/
		if(!list.isEmpty()){
			/**se asigna datos de la consulta**/
		 	for (Map<String, Object> mapResult : list){
		 		/** Llenado del objeto de salida**/
		 		beanDetReceptorSPID = descomponeConsulta(mapResult);
		 		beanResRecepcionDAO.setBeanReceptorSPID(beanDetReceptorSPID);
	        	beanResRecepcionDAO.setCodError(responseDTO.getCodeError());
	        	beanResRecepcionDAO.setMsgError(responseDTO.getMessageError());
	        	
		 	}
		 }
		/** Retorno del objeto de salida **/
		return beanResRecepcionDAO;
		
	}
	
	 /**
 	 * Metodo que setea los campos de rastreo.
 	 *
 	 * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
 	 * @return BeanSPIDRastreo Bean de respuesta con los datos del banco ordenante
 	 */
    private BeanSPIDRastreo seteaRastreo(Map<String,Object> map){
    	/**se inicializan los objetos**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDRastreo beanSPIDRastreo = new BeanSPIDRastreo();
    	/**se asgina datos**/
    	beanSPIDRastreo.setCveRastreo(utilerias.getString(map.get("CVE_RASTREO")));
    	beanSPIDRastreo.setCveRastreoOri(utilerias.getString(map.get("CVE_RASTREO_ORI")));
    	beanSPIDRastreo.setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")));
    	beanSPIDRastreo.setDireccionIp(utilerias.getString(map.get("DIRECCION_IP")));
    	/** Retorna el objeto de salida**/
    	return beanSPIDRastreo;
    }
    
    /**
     * Metodo que setea los campos del Banco Receptor.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDBancoRec Bean de respuesta con los datos del banco ordenante
     */
    private BeanSPIDBancoRec seteaBancoRec(Map<String,Object> map){
    	/**se inicializan los objetos**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBancoRec beanSPIDBancoRec = new BeanSPIDBancoRec();
    	/**se asigna datos de la consulta**/
    	beanSPIDBancoRec.setNombreRec(utilerias.getString(map.get("NOMBRE_REC")));
    	beanSPIDBancoRec.setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaRec(utilerias.getString(map.get("NUM_CUENTA_REC")));
    	beanSPIDBancoRec.setNumCuentaTran(utilerias.getString(map.get("NUM_CUENTA_TRAN")));
    	beanSPIDBancoRec.setRfcRec(utilerias.getString(map.get("RFC_REC")));
    	/** Retorna el objeto de salida **/
    	return beanSPIDBancoRec;
    }
    
    
    /**
     * Metodo que genera un objeto del tipo BeanTranSpeiRec a partir de la respuesta de la consulta.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanReceptoresSPID Objeto del tipo BeanReceptoresSPID
     */
    private BeanReceptoresSPID descomponeConsulta(Map<String,Object> map){
    	/**se inicializan los objetos**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDBloqueo beanSPIDBloqueo = new BeanSPIDBloqueo();
    	BeanReceptoresSPID beanReceptoresSPID = new BeanReceptoresSPID();
    	/**se asigna datos de la consulta**/
    	beanReceptoresSPID.setLlave(utilConsultas.seteaLlave(map));
    	beanReceptoresSPID.setDatGenerales(utilConsultas.seteaDatosGenerales(map));
    	beanReceptoresSPID.setEstatus(utilConsultas.seteaEstatus(map));
    	beanReceptoresSPID.setBancoOrd(utilConsultas.seteaBancoOrd(map));
    	beanReceptoresSPID.setBancoRec(seteaBancoRec(map));
    	beanReceptoresSPID.setEnvDev(utilConsultas.seteaEnvDev(map));
    	beanReceptoresSPID.setRastreo(seteaRastreo(map));
    	beanReceptoresSPID.setFchPagos(seteaFchPago(map));
    	beanReceptoresSPID.setOpciones(new BeanSPIDOpciones());
    	beanReceptoresSPID.setBloqueo(beanSPIDBloqueo);
    	
    	beanSPIDBloqueo.setUsuario(utilerias.getString(map.get("CVE_USUARIO")));
    	/** Valida campo **/
    	if("RECEP_OPSPID".equals(utilerias.getString(map.get("FUNCIONALIDAD")))){
    		beanSPIDBloqueo.setBloqueado(true);	
    	}
    	/** Retorna el objeto de salida **/
    	return beanReceptoresSPID;
    }
    
    /**
     * Metodo que setea los campos de las fechas de pago.
     *
     * @param map Objeto del tipo Map<String,Object> que almacena el conjunto de registros
     * @return BeanSPIDFchPagos Bean de respuesta con los datos de las fechas de pagos
     */
    private BeanSPIDFchPagos seteaFchPago(Map<String,Object> map){
    	/**se inicializan los objetos**/
    	Utilerias utilerias = Utilerias.getUtilerias();
    	BeanSPIDFchPagos beanSPIDFchPagos = new BeanSPIDFchPagos();
    	/**se asigna datos de la consulta**/
    	beanSPIDFchPagos.setFchAceptPago(utilerias.getString(map.get("FCH_ACEPT_PAGO")));
    	beanSPIDFchPagos.setHoraAceptPago(utilerias.getString(map.get("HORA_ACEPT_PAGO")));
    	beanSPIDFchPagos.setFchInstrucPago(utilerias.getString(map.get("FCH_INSTRUC_PAGO")));
    	beanSPIDFchPagos.setHoraInstrucPago(utilerias.getString(map.get("HORA_INSTRUC_PAGO")));
    	/** Retorna el objeto de salida **/
    	return beanSPIDFchPagos;
    }
	
    
    /**
     * Metodo para generar el formato del query.
     *
     * @param tipoOrden Objeto del tipo String
     * @return consultaRecepciones Objeto del tipo StringBuilder
     */
	public StringBuilder generarQueryMonto( String tipoOrden) {
		/**se inicializan los objetos**/
		StringBuilder consultaRecepciones = 
				new StringBuilder();
		/**se genera la consulta**/
		consultaRecepciones.append("SELECT ")
		.append("sum(MONTO) MONTO_TOTAL ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT FROM TRAN_SPID_REC ")
		.append(" WHERE  ");
		/**se concatena el valor segun el tipo de orden**/
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((").append(ConstantesMonitor.STATUS_TRANS_BANX).append(") OR ")
			.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' ")
			.append(" AND MOTIVO_DEVOL IS NULL )) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXTR);
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXRE);
		}
		
		consultaRecepciones.append(" AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ) C ")
		.append(ConstantesMonitor.WHERE);
		/**se concatena el valor segun el tipo de orden**/
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((").append(ConstantesMonitor.STATUS_TRANS_BANX).append(") OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' ")
			.append(" AND MOTIVO_DEVOL IS NULL )) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXTR);
			
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXRE);
		}
		consultaRecepciones.append("AND  CVE_MI_INSTITUC=? AND FCH_OPERACION=TO_DATE(?, 'DD-MM-YYYY') ");
		/** Retorna el objeto de salida **/		
		return consultaRecepciones;
	}
	
	/**
	 * Metodo para llenar la consulta de Recepciones.
	 *
	 * @param beanResConsultaRecepcionesDAO Objeto del tipo BeanResConsultaRecepcionesDAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @return listaBeanConsultaRecepciones Objeto del tipo List<BeanConsultaRecepciones>
	 */
	public List<BeanConsultaRecepciones> llenarConsultaRecepciones(
			final BeanResConsultaRecepcionesDAO beanResConsultaRecepcionesDAO, List<HashMap<String, Object>> list,
			Utilerias utilerias) {
		/**se inicializan los objetos**/
		BeanConsultaRecepciones beanConsultaRecepciones;
		List<BeanConsultaRecepciones> listaBeanConsultaRecepciones;
		listaBeanConsultaRecepciones = new ArrayList<BeanConsultaRecepciones>();
		/**se recorre el resultado**/
		for (Map<String, Object> listMap : list) {
			Map<String, Object> mapResult = listMap;
			beanConsultaRecepciones = new BeanConsultaRecepciones();
			beanConsultaRecepciones.setBeanConsultaRecepcionesDos(new BeanConsultaRecepcionesDos());
			/**se asogna los datos**/
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setTopologia(utilerias.getString(mapResult.get("TOPOLOGIA")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setTipoPago(utilerias.getInteger(mapResult.get("TIPO_PAGO")));
			beanConsultaRecepciones
			.getBeanConsultaRecepcionesDos().setEstatusBanxico(utilerias.getString(mapResult.get("ESTATUS_BANXICO")));
			beanConsultaRecepciones
			.getBeanConsultaRecepcionesDos().setEstatusTransfer(utilerias.getString(mapResult.get("ESTATUS_TRANSFER")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFechOperacion(utilerias.getString(mapResult.get("FCH_OPERACION")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFechCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setCveInsOrd(utilerias.getString(mapResult.get("CVE_INST_ORD")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setCveIntermeOrd(utilerias.getString(mapResult.get("CVE_INTERME_ORD")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setFolioPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setNumCuentaTran(utilerias.getString(mapResult.get("NUM_CUENTA_TRAN")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setMonto(utilerias.getString(mapResult.get("MONTO")));
			beanConsultaRecepciones.setCodigoError(utilerias.getString(mapResult.get("CODIGO_ERROR")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setMotivoDevol(utilerias.getString(mapResult.get("MOTIVO_DEVOL")));
			beanConsultaRecepciones.getBeanConsultaRecepcionesDos().setNumCuentaRec(utilerias.getString(mapResult.get("NUM_CUENTA_REC")));

			listaBeanConsultaRecepciones.add(beanConsultaRecepciones);
			beanResConsultaRecepcionesDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			beanResConsultaRecepcionesDAO.setImporteTotal(Utilerias.getUtilerias().getString(mapResult.get("TOTAL")));

		}
		/** Retorna el objeto de salida **/
		return listaBeanConsultaRecepciones;
	}
	
	
	/**
	 * Metodo para generar el formato del query.
	 *
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @param tipoOrden Objeto del tipo String
	 * @return consultaRecepciones Objeto del tipo StringBuilder
	 */
	public StringBuilder generarFormatoQuery(BeanReqConsultaRecepciones beanReqConsultaRecepciones, String tipoOrden) {
		/**se inicializan los objetos**/
		StringBuilder consultaRecepciones = 
				new StringBuilder();
		/**se crea la consulta**/
		consultaRecepciones.append("SELECT T.* FROM (SELECT T.*, ROWNUM R FROM (SELECT TOPOLOGIA, ")
		.append("TIPO_PAGO,ESTATUS_BANXICO, ESTATUS_TRANSFER, ")
		.append("TO_CHAR(FCH_OPERACION,'DD/MM/YYYY') FCH_OPERACION,TO_CHAR(FCH_CAPTURA,'HH24:MI:SS') FCH_CAPTURA,CVE_INST_ORD, ")
		.append("CVE_INTERME_ORD,FOLIO_PAQUETE,FOLIO_PAGO, NUM_CUENTA_REC, ")
		.append("NUM_CUENTA_TRAN,MONTO, CODIGO_ERROR, MOTIVO_DEVOL,C.CONT,TOTAL ")
		.append("FROM TRAN_SPID_REC")
		.append(",(SELECT COUNT(*) CONT, SUM(MONTO) TOTAL FROM TRAN_SPID_REC ")
		.append(ConstantesMonitor.WHERE);
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((").append(ConstantesMonitor.STATUS_TRANS_BANX).append(" ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ");
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos por Aplicar");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXTR);
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Aplicados");
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXRE);
			beanReqConsultaRecepciones.setTitulo("Movimientos Recibidos Rechazados");
		}
		
		consultaRecepciones.append(" AND  CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?, 'DD-MM-YYYY') [FILTRO_WH] ) C ")
		.append(" WHERE  ");
		if("PA".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(" ((").append(ConstantesMonitor.STATUS_TRANS_BANX).append(" ) OR ");
			consultaRecepciones.append(" (ESTATUS_TRANSFER = 'RE' AND ESTATUS_BANXICO = 'LQ' AND MOTIVO_DEVOL IS NULL )) ");
		}else if("TR".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXTR);
			
		}else if("RE".equalsIgnoreCase(tipoOrden)){
			consultaRecepciones.append(ConstantesMonitor.STATUS_TRANS_BANXRE);
		}
		consultaRecepciones.append("AND  CVE_MI_INSTITUC = ? AND FCH_OPERACION = TO_DATE(?, 'DD-MM-YYYY') [FILTRO_WH]");
		
		String sortField =utilConsultas.getSortField(beanReqConsultaRecepciones);
		
		if (sortField != null && !"".equals(sortField)){
			consultaRecepciones.append(" ORDER BY ").append(sortField).append(" ").append(beanReqConsultaRecepciones.getSortType());
		}	
		//JJBR || 17/09/2020 || SE AGREGAN FILTROS DE BUSQUEDA
		StringBuilder filtro = new StringBuilder();
		String valor = null;
		valor = beanReqConsultaRecepciones.getTopologia();
		filtro.append(getFiltroXParam("TOPOLOGIA", valor, 1));
		valor = beanReqConsultaRecepciones.getTipoPago();
		filtro.append(getFiltroXParam("TIPO_PAGO", valor, 1));
		valor = beanReqConsultaRecepciones.getCveInsOrd();
		filtro.append(getFiltroXParam("CVE_INST_ORD", valor, 1));
		valor = beanReqConsultaRecepciones.getCveIntermeOrd();
		filtro.append(getFiltroXParam("CVE_INTERME_ORD", valor, 1));
		valor = beanReqConsultaRecepciones.getFolioPaquetes();
		filtro.append(getFiltroXParam("FOLIO_PAQUETE", valor, 1));
		valor = beanReqConsultaRecepciones.getFolioPagos();
		filtro.append(getFiltroXParam("FOLIO_PAGO", valor, 1));
		valor = beanReqConsultaRecepciones.getNumCuentaRec();
		filtro.append(getFiltroXParam("NUM_CUENTA_REC", valor, 1));
		valor = beanReqConsultaRecepciones.getNumCuentaTran();
		filtro.append(getFiltroXParam("NUM_CUENTA_TRAN", valor, 1));
		valor = beanReqConsultaRecepciones.getMonto();
		filtro.append(getFiltroXParam("MONTO", valor, 2));
		valor = beanReqConsultaRecepciones.getCodigoError();
		filtro.append(getFiltroXParam("CODIGO_ERROR", valor, 1));
		valor = beanReqConsultaRecepciones.getMotivoDevol();
		filtro.append(getFiltroXParam("MOTIVO_DEVOL", valor, 1));
		
		consultaRecepciones = new StringBuilder(consultaRecepciones.toString().replaceAll("\\[FILTRO_WH\\]", filtro.toString()));
		consultaRecepciones.append(") T) T WHERE R BETWEEN ? AND ?  ");
		/** Retorna el String de salida **/
		return consultaRecepciones;
	}
	
	/**
	 * Obtener el objeto: filtro X parametro.
	 *
	 * @param param El objeto: param
	 * @param valor El objeto: valor
	 * @param tipoDato El objeto: tipo dato. 0 = Cadena, 1 = Numerico, 2 = Decimal
	 * @return El objeto: filtro X param
	 */
	private String getFiltroXParam(String param, String valor, int tipoDato) {
		String filtro = "";
		if (valor != null && !valor.isEmpty()) {
			if (tipoDato == 2) {
				filtro = " AND TO_CHAR("+param+",'999999999999999999999999999999.99') = TO_CHAR('"+valor+"','999999999999999999999999999999.99')";
			} else if(tipoDato == 1) {
				filtro = " AND "+param+" = '"+valor+"'";
			} else {
				filtro = " AND UPPER("+param+") LIKE UPPER('%"+valor+"%')";				
			}
		}
		return filtro;
	}
	
}