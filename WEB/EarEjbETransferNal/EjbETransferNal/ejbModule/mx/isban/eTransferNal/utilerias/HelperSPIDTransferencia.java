/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * HelperSPIDTransferencia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/03/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.utilerias;

import java.util.HashMap;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCfgTramaSPID;
import mx.isban.eTransferNal.constantes.modSPID.ConstantesSPID;

public class HelperSPIDTransferencia extends Architech{
	
	/**
	 * Id Version
	 */
	private static final long serialVersionUID = 8730149159458846257L;
	
	/**
	 * Propiedad del tipo Map<String,String> que almacena el valor de MAPA
	 */
	private static final Map<String, String> MAPA = new HashMap<String, String>();
	
	/**
	 * Propiedad del tipo String que almacena el valor de REFE_NUMERICA
	 */
	private static final String REFE_NUMERICA = "REFE_NUMERICA";
	

	/**
	 * Propiedad del tipo Map<String,BeanCfgTramaSPID> que almacena el valor de MAP
	 */
	private static final Map<String,BeanCfgTramaSPID> MAP = new HashMap<String, BeanCfgTramaSPID>();

	
	
	/**Inicializacion statica*/
	static {
		MAPA.put("01","TTLIQVAL");
		MAPA.put("02","TTLIQDER");
		MAPA.put("03","TTLIQCAM");
		MAPA.put("04","TTPAGSER");
		MAPA.put("05","TTPAGBIE");
		MAPA.put("06","TTOTOPRE");
		MAPA.put("07","TTPAGPRE");
		MAPA.put("08","TTOTROS");
		
		MAP.put(ConstantesSPID.CVE_EMPRESA      ,new BeanCfgTramaSPID(ConstantesSPID.CVE_EMPRESA,"A",3,"I"));
		MAP.put(ConstantesSPID.CVE_PTO_VTA      ,new BeanCfgTramaSPID(ConstantesSPID.CVE_PTO_VTA,"N",4,"D"));
		MAP.put(ConstantesSPID.CVE_MEDIO_ENT    ,new BeanCfgTramaSPID(ConstantesSPID.CVE_MEDIO_ENT,"A",4,"I"));
		MAP.put(ConstantesSPID.REFERENCIA_MED   ,new BeanCfgTramaSPID(ConstantesSPID.REFERENCIA_MED,"A",12,"D"));
		MAP.put(ConstantesSPID.CVE_USUARIO_CAP  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_USUARIO_CAP,"N",7,"D"));
		MAP.put(ConstantesSPID.CVE_USUARIO_SOL  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_USUARIO_SOL,"N",7,"D"));
		MAP.put(ConstantesSPID.CVE_TRANSFE      ,new BeanCfgTramaSPID(ConstantesSPID.CVE_TRANSFE,"N",3,"I"));
		MAP.put(ConstantesSPID.CVE_OPERACION    ,new BeanCfgTramaSPID(ConstantesSPID.CVE_OPERACION,"A",8,"I"));
		MAP.put(REFE_NUMERICA    ,new BeanCfgTramaSPID(REFE_NUMERICA,"A",20,"I")); 
		MAP.put(ConstantesSPID.DIRECCION_IP     ,new BeanCfgTramaSPID(ConstantesSPID.DIRECCION_IP,"A",39,"I"));
		MAP.put(ConstantesSPID.FCH_INSTRUC_PAGO ,new BeanCfgTramaSPID(ConstantesSPID.FCH_INSTRUC_PAGO,"A",8,"I"));
		MAP.put(ConstantesSPID.HORA_INSTRUC_PAGO,new BeanCfgTramaSPID(ConstantesSPID.HORA_INSTRUC_PAGO,"A",11,"I"));
		MAP.put(ConstantesSPID.CVE_RASTREO      ,new BeanCfgTramaSPID(ConstantesSPID.CVE_RASTREO,"A",30,"I"));
		MAP.put(ConstantesSPID.CVE_DIVISA_ORD   ,new BeanCfgTramaSPID(ConstantesSPID.CVE_DIVISA_ORD,"A",4,"I"));
		MAP.put(ConstantesSPID.CVE_DIVISA_REC   ,new BeanCfgTramaSPID(ConstantesSPID.CVE_DIVISA_REC,"A",4,"I"));
		MAP.put(ConstantesSPID.IMPORTE_CARGO    ,new BeanCfgTramaSPID(ConstantesSPID.IMPORTE_CARGO,"A",18,"D"));
		MAP.put(ConstantesSPID.IMPORTE_ABONO    ,new BeanCfgTramaSPID(ConstantesSPID.IMPORTE_ABONO,"A",18,"D"));
		MAP.put(ConstantesSPID.CVE_PTO_VTA_ORD  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_PTO_VTA_ORD,"A",4,"I"));
		MAP.put(ConstantesSPID.CVE_INTERME_ORD  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_INTERME_ORD,"A",5,"I"));
		MAP.put(ConstantesSPID.TIPO_CUENTA_ORD  ,new BeanCfgTramaSPID(ConstantesSPID.TIPO_CUENTA_ORD,"A",3,"I"));
		MAP.put(ConstantesSPID.NUM_CUENTA_ORD   ,new BeanCfgTramaSPID(ConstantesSPID.NUM_CUENTA_ORD,"A",20,"I"));
		MAP.put(ConstantesSPID.NOMBRE_ORD       ,new BeanCfgTramaSPID(ConstantesSPID.NOMBRE_ORD,"A",120,"I"));
		MAP.put(ConstantesSPID.RFC_ORD          ,new BeanCfgTramaSPID(ConstantesSPID.RFC_ORD,"A",18,"I"));
		MAP.put(ConstantesSPID.DOMICILIO_ORD    ,new BeanCfgTramaSPID(ConstantesSPID.DOMICILIO_ORD,"A",120,"I"));
		MAP.put(ConstantesSPID.COD_POSTAL_ORD   ,new BeanCfgTramaSPID(ConstantesSPID.COD_POSTAL_ORD,"A",5,"I"));
		MAP.put(ConstantesSPID.FCH_CONSTIT_ORD  ,new BeanCfgTramaSPID(ConstantesSPID.FCH_CONSTIT_ORD,"A",8,"I"));
		MAP.put(ConstantesSPID.CVE_PTO_VTA_REC  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_PTO_VTA_REC,"A",4,"I"));
		MAP.put(ConstantesSPID.CVE_INTERME_REC  ,new BeanCfgTramaSPID(ConstantesSPID.CVE_INTERME_REC,"A",5,"I"));
		MAP.put(ConstantesSPID.TIPO_CUENTA_REC  ,new BeanCfgTramaSPID(ConstantesSPID.TIPO_CUENTA_REC,"A",2,"I"));
		MAP.put(ConstantesSPID.NUM_CUENTA_REC   ,new BeanCfgTramaSPID(ConstantesSPID.NUM_CUENTA_REC,"A",20,"I"));
		MAP.put(ConstantesSPID.NOMBRE_REC       ,new BeanCfgTramaSPID(ConstantesSPID.NOMBRE_REC,"A",120,"I"));
		MAP.put(ConstantesSPID.RFC_REC          ,new BeanCfgTramaSPID(ConstantesSPID.RFC_REC,"A",18,"I"));
		MAP.put(ConstantesSPID.MOTIVO_DEVOL     ,new BeanCfgTramaSPID(ConstantesSPID.MOTIVO_DEVOL,"N",2,"D"));
		MAP.put(ConstantesSPID.FOLIO_PAQUETE    ,new BeanCfgTramaSPID(ConstantesSPID.FOLIO_PAQUETE,"A",12,"D"));
		MAP.put(ConstantesSPID.FOLIO_PAGO       ,new BeanCfgTramaSPID(ConstantesSPID.FOLIO_PAGO,"A",12,"D"));
		MAP.put(ConstantesSPID.TIPO_CAMBIO      ,new BeanCfgTramaSPID(ConstantesSPID.TIPO_CAMBIO,"A",15,"D"));
		MAP.put(ConstantesSPID.COMENTARIO2      ,new BeanCfgTramaSPID(ConstantesSPID.COMENTARIO2,"A",30,"I"));
		MAP.put(ConstantesSPID.COMENTARIO3      ,new BeanCfgTramaSPID(ConstantesSPID.COMENTARIO3,"A",30,"I"));
		MAP.put(ConstantesSPID.CONCEPTO_PAGO    ,new BeanCfgTramaSPID(ConstantesSPID.CONCEPTO_PAGO,"A",40,"I"));
	}
	
	/**
     * @param mapTransferencia Map con los datos que forman la trama a enviar
     * @return String con la trama a enviar
     */
    public String generaTrama(Map<String,String> mapTransferencia){
    	StringBuilder builder = new StringBuilder();
    	builder.append("TRANSPID");
    	builder.append(armaCampo(mapTransferencia.get("CVE_EMPRESA"),"CVE_EMPRESA",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_PTO_VTA"),"CVE_PTO_VTA",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_MEDIO_ENT"),"CVE_MEDIO_ENT",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("REFERENCIA_MED"),"REFERENCIA_MED",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_USUARIO_CAP"),"CVE_USUARIO_CAP",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_USUARIO_SOL"),"CVE_USUARIO_SOL",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_TRANSFE"),"CVE_TRANSFE",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_OPERACION"),"CVE_OPERACION",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("REFE_NUMERICA"),"REFE_NUMERICA",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("DIRECCION_IP"),"DIRECCION_IP",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("FCH_INSTRUC_PAGO"),"FCH_INSTRUC_PAGO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("HORA_INSTRUC_PAGO"),"HORA_INSTRUC_PAGO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_RASTREO"),"CVE_RASTREO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_DIVISA_ORD"),"CVE_DIVISA_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_DIVISA_REC"),"CVE_DIVISA_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("IMPORTE_ABONO"),"IMPORTE_ABONO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("IMPORTE_CARGO"),"IMPORTE_CARGO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_PTO_VTA_ORD"),"CVE_PTO_VTA_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_INTERME_ORD"),"CVE_INTERME_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("TIPO_CUENTA_ORD"),"TIPO_CUENTA_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("NUM_CUENTA_ORD"),"NUM_CUENTA_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("NOMBRE_ORD"),"NOMBRE_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("RFC_ORD"),"RFC_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("DOMICILIO_ORD"),"DOMICILIO_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("COD_POSTAL_ORD"),"COD_POSTAL_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("FCH_CONSTIT_ORD"),"FCH_CONSTIT_ORD",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_PTO_VTA_REC"),"CVE_PTO_VTA_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CVE_INTERME_REC"),"CVE_INTERME_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("TIPO_CUENTA_REC"),"TIPO_CUENTA_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("NUM_CUENTA_REC"),"NUM_CUENTA_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("NOMBRE_REC"),"NOMBRE_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("RFC_REC"),"RFC_REC",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("MOTIVO_DEVOL"),"MOTIVO_DEVOL",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("FOLIO_PAQUETE"),"FOLIO_PAQUETE",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("FOLIO_PAGO"),"FOLIO_PAGO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("TIPO_CAMBIO"),"TIPO_CAMBIO",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("COMENTARIO2"),"COMENTARIO2",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("COMENTARIO3"),"COMENTARIO3",mapTransferencia));
    	builder.append(armaCampo(mapTransferencia.get("CONCEPTO_PAGO"),"CONCEPTO_PAGO",mapTransferencia));
    	return builder.toString();
    }
    
    /**
     * Metodo que asigna longitud a campos especificos
     * @param mapTransferencia map de datos de entrada
     * @return int con la longitud
     */
    public int validaConcepto(Map<String,String> mapTransferencia){
    	int conceptoPago = 40; 
    	if("974".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE)) || "977".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE)) || 
    			"984".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE)) || "987".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE))){
    		conceptoPago = 210;
    	}else if("907".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE))
    			&& "".equals(mapTransferencia.get(ConstantesSPID.TIPO_CUENTA_REC))){
    			conceptoPago = 210;
    		
    	}else if("947".equals(mapTransferencia.get(ConstantesSPID.CVE_TRANSFE))&&
    			"".equals(mapTransferencia.get(ConstantesSPID.TIPO_CUENTA_ORD))){
    			conceptoPago = 210;
    	}
    	return conceptoPago;
    }
    
    /**
     * @param valor String con el valor
     * @param campo String con el campo
     * @param mapTransferencia map con los valores
     * @return String con el formateo
     */
    private String armaCampo(String valor,String campo,Map<String,String> mapTransferencia){
    	int tamanio = 0;
    	BeanCfgTramaSPID beanCfgTramaSPID = MAP.get(campo);
    	
    	//Ajuste a longitud campo concepto_pago
    	if(ConstantesSPID.CONCEPTO_PAGO.equals(campo)){
    		tamanio = validaConcepto(mapTransferencia);
    	}else{
    		tamanio = beanCfgTramaSPID.getTamano();
    	}
    	
    	String _valor = valor; 
    	if(_valor == null){
    		_valor = "";
    	}else{
    		_valor = _valor.trim();
    	}
    	return completa(_valor,tamanio, beanCfgTramaSPID.getJustificado(), beanCfgTramaSPID.getTipoCampo());
    }

    /**
     * @param valor String con el valor
     * @param longitud entero con la longitud del campo
     * @param justificacion Es la justificacion del campo I (Izquierdo) o D (Derecho)
     * @param tipo String con el tipo de variable
     * @return String completa caracteres de izq o der
     */
    private String completa(String valor,int longitud, String justificacion, String tipo){
    	StringBuilder builder = new StringBuilder();
    	String _valor = valor;
    	int length = valor.length();
    	int tam = longitud - length;
    	if(tam>0){
	    	if("I".equals(justificacion)){
	    		builder.append(valor);
	    		if("A".equals(tipo)){
	    			for(int i=0;i<tam;i++){
		    			builder.append(" ");
		        	}
	    		}else{
		    		for(int i=0;i<tam;i++){
		    			builder.append("0");
		        	}
	    		}
	    	}else if("D".equals(justificacion)){
	    		if("A".equals(tipo)){
	    			for(int i=0;i<tam;i++){
		    			builder.append(" ");
		        	}
	    		}else{
		    		for(int i=0;i<tam;i++){
		    			builder.append("0");
		        	}
	    		}
	    		
	    		builder.append(valor);
	    	}
	    	_valor = builder.toString();
    	}else{
    		_valor = _valor.substring(0,longitud);
    	}
    	return _valor;
    }

}
