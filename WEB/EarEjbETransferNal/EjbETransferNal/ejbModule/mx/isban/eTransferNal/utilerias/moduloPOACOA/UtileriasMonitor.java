/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:54:49 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanOperacionesLiquidadas;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanOperacionesNoConfirmadas;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanOrdenes;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanRecepciones;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanSaldo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanSaldos;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanTraspasoOrden;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanTraspasos;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriUtileriasMonitorasPagos.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los flujos del monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class UtileriasMonitor implements Serializable {


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2881647703492633291L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasMonitor utils;
	
	/** La constante VOLUMEN. */
	private static final String VOLUMEN = "VOLUMEN";
	
	/** La constante SALDO. */
	private static final String SALDO = "SALDO";
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils de instancia
	 */
	public static UtileriasMonitor getUtils() {
		if (utils == null) {
			utils = new UtileriasMonitor();
		}
		return utils;
	}
	
	/**
	 * Completa query.
	 *
	 * Metodo para  validar el query que se 
	 * ejecutara en base al parametro modulo SPEI o 
	 * SPID
	 * 
	 * @param query El objeto: query
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	public String completaQuery(String query, BeanModulo modulo) {
		final String paramEntidad = ValidadorMonitor.getEntidad(modulo);
		String origen = "AND TMS.ORIGEN = ";
		query = query.replaceAll("\\[PARAM_ENTIDAD\\]", "'".concat(paramEntidad).concat("'"));
		query = query.replaceAll("\\[AND_MODULO\\]", "");
		if (ValidadorMonitor.isSPEI(modulo)) {
			query = query.replaceAll("SPID", "SPEI");
			query = query.replaceAll("TRAN_MENSAJE_SPEI", "TRAN_SPEI_ENV");
		} 
		if (ValidadorMonitor.isCOA(modulo)) {
			origen = origen.concat("'C'");
		}
		if (ValidadorMonitor.isPOA(modulo)) {
			origen = origen.concat("'P'");
		}
		query = query.replaceAll("\\[AND_TIPO\\]", origen);
		
		return query;
	}
	
	/**
	 * Obtener el objeto: valores.
	 *
	 * Metodo para obtener los valores de 
	 * la cosulta principal mostrados por
	 * el monitor principal.
	 * 
	 * @param list El objeto: list
	 * @return El objeto: valores
	 */
	public BeanMonitor getValores(List<HashMap<String,Object>> list) {
		/** Declaracion de las variables a utilizar **/
		BeanMonitor valores = new BeanMonitor();
		Utilerias utilerias = Utilerias.getUtilerias();
		BigDecimal tmpSaldo = BigDecimal.ZERO;
		BigDecimal tmpVolumen = BigDecimal.ZERO;
		BigDecimal total = BigDecimal.ZERO;
		BeanSaldo saldo = null;
		
		BeanOperacionesLiquidadas operacionesLiquidadas = new BeanOperacionesLiquidadas();
		BeanTraspasoOrden beanTraspasoOrden = new BeanTraspasoOrden();
		BeanRecepciones beanRecepciones = new BeanRecepciones();
		
		tmpSaldo = utilerias.getBigDecimal(list.get(0).get(SALDO));
		total= total.add(tmpSaldo);
		/** Set Saldo **/
		operacionesLiquidadas.setSaldo(utilerias.formateaDecimales(tmpSaldo,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmpSaldo = utilerias.getBigDecimal(list.get(1).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(1).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total = total.add(tmpSaldo);
		/** Set TraspasoSIACTipo **/
		beanTraspasoOrden.setTraspasoSIACTipo(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(2).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(2).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total = total.add(tmpSaldo);
		/** Set OrdenesPorAplicar **/
		beanRecepciones.setOrdenesPorAplicar(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(3).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(3).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total = total.add(tmpSaldo);
		/** Set OrdenesAplicadas**/
		beanRecepciones.setOrdenesAplicadas(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(4).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(4).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total = total.add(tmpSaldo);
		/** Set OrdenesRechazadas**/
		beanRecepciones.setOrdenesRechazadas(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(5).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(5).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total = total.add(tmpSaldo);
		/** Set OrdenesPorDevolver**/
		beanTraspasoOrden.setOrdenesPorDevolver(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(6).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(6).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total= total.subtract(tmpSaldo);
		/** Set TraspasoTipoSIAC**/
		beanTraspasoOrden.setTraspasoTipoSIAC(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(7).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(7).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		total= total.subtract(tmpSaldo);
		/** Set OrdenesEnvidasConfirmadas **/
		beanTraspasoOrden.setOrdenesEnvidasConfirmadas(saldo);
		/** Set Recepciones **/
		operacionesLiquidadas.setRecepciones(beanRecepciones);
		/** Set TraspasoOrden **/
		operacionesLiquidadas.setTraspasoOrden(beanTraspasoOrden);
		
		BeanSaldos saldos = new BeanSaldos();
		/** Set SaldoCalculado **/
		saldos.setSaldoCalculado(utilerias.formateaDecimales(total,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		tmpSaldo = utilerias.getBigDecimal(list.get(8).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(8).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set DevolucionesEnvidadas **/
		saldos.setDevolucionesEnvidadas(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(15).get(SALDO));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set SaldoNoReservado **/
		saldos.setSaldoNoReservado(saldo.getSaldo());
		total= total.subtract(tmpSaldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(16).get(SALDO));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set SaldoReservado **/
		saldos.setSaldoReservado(saldo.getSaldo());
		total= total.subtract(tmpSaldo);
		
		/** Set Diferencia **/
		saldos.setDiferencia(utilerias.formateaDecimales(total,Utilerias.FORMATO_DECIMAL_NUMBER));
		
		
		BeanOperacionesNoConfirmadas beanOperacionesNoConfirmadas = new BeanOperacionesNoConfirmadas();
		BeanTraspasos beanTraspasos = new BeanTraspasos();
		BeanOrdenes beanOrdenes = new BeanOrdenes();
		
		tmpSaldo = utilerias.getBigDecimal(list.get(9).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(9).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set TraspasosEspera**/
		beanTraspasos .setTraspasosEspera(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(10).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(10).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set TraspasosEnviados **/
		beanTraspasos.setTraspasosEnviados(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(11).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(11).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set TraspasosPorReparar **/
		beanTraspasos.setTraspasosPorReparar(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(12).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(12).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set OrdenesEspera **/
		beanOrdenes.setOrdenesEspera(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(13).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(13).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set OrdenesEnviadas **/
		beanOrdenes.setOrdenesEnviadas(saldo);
		
		tmpSaldo = utilerias.getBigDecimal(list.get(14).get(SALDO));
		tmpVolumen = utilerias.getBigDecimal(list.get(14).get(VOLUMEN));
		/** Set Saldo **/
		saldo = setSaldo(tmpSaldo, tmpVolumen);
		/** Set OrdenesPorReparar **/
		beanOrdenes.setOrdenesPorReparar(saldo);
		
		/** Set Ordenes**/
		beanOperacionesNoConfirmadas.setOrdenes(beanOrdenes);
		/** Set Traspasos **/
		beanOperacionesNoConfirmadas.setTraspasos(beanTraspasos);
		/** Set OperacionesLiquidadas**/
		valores.setOperacionesLiquidadas(operacionesLiquidadas);
		/** Set Saldos **/
		valores.setSaldos(saldos);
		/** Set OperacionesNoConfirmadas **/
		valores.setOperacionesNoConfirmadas(beanOperacionesNoConfirmadas);
		
		/** Retorno de objeto con todos los valores seteados **/
		return valores;
	}
	
	/**
	 * Sets the saldo.
	 *
	 * Metodo que llena el beanSaldo
	 * con los parametros saldo y volumen recibidos.
	 * 
	 * @param tmpSaldo El objeto: tmp saldo
	 * @param tmpVolumen El objeto: tmp volumen
	 * @return Objeto bean saldo
	 */
	private BeanSaldo setSaldo(BigDecimal tmpSaldo, BigDecimal tmpVolumen) {
		BeanSaldo saldo = new BeanSaldo();
		Utilerias utilerias = Utilerias.getUtilerias();
		saldo.setSaldo(utilerias.formateaDecimales(tmpSaldo,Utilerias.FORMATO_DECIMAL_NUMBER));
		saldo.setVolumen(utilerias.formateaDecimales(tmpVolumen,Utilerias.FORMATO_NUMBER));
		return saldo;
	}
}
