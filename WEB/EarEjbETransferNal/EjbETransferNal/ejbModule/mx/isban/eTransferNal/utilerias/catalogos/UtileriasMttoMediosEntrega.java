/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: UtileriasMttoMediosEntrega.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/09/2018 04:42:44 PM Juan Jesus Beltran R. VectorFSW Creacion
 * 1.1   19/11/2020      Rodolfo Mendez ISBAN  Enviar como numero el importe maximo al dar de alta o actualizar
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasMttoMediosEntrega.
 *
 * Clase que contiene metodos de utilerias que ayudan al negocio con el manejo de datos
 * que pasan por dicha capa
 *
 * @author FSW-Vector
 * @since 28/09/2018
 */
public class UtileriasMttoMediosEntrega extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3753742596172765250L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasMttoMediosEntrega instancia;

	/** La constante CLAVE. */
	private static final String CLAVE = "clave";

	/** La constante DESCRIPCION. */
	private static final String DESCRIPCION = "descripcion";

	/** La constante ALTA. */
	public static final String ALTA = "alta";

	/** La constante BAJA. */
	public static final String BAJA = "baja";

	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";

	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";

	//Constantes para expotacion batch
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE = "Clave medio de Entrega,Version,Descripcion,Comision Anual PF,Comision Anual PM,Seguridad, Canal, Importe Maximo";

	/** La constante CONSULTA_EXPORTAR_TODOs. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT CVE_MEDIO_ENT || ',' || U_VERSION || ',' || DESCRIPCION || ',' || COM_ANUAL_PF || ',' || COM_ANUAL_PM || ',' || CKK_SEGURIDAD || ',' || CANAL  || ',' || IMP_MAX_SPEI_7X24 FROM COMU_MEDIOS_ENT [FILTRO_WH]";

	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY CVE_MEDIO_ENT ASC";

	/** La variable que contiene informacion con respecto a: first. */
	boolean first = true;

	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasMttoMediosEntrega getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasMttoMediosEntrega();
		}
		return instancia;
	}

	/**
	 * Recorrer lista.
	 *
	 * @param responseDTO El objeto: response DTO
	 * @param mapa El objeto: mapa
	 * @return Objeto list
	 */
	public List<BeanCatalogo> recorrerLista(ResponseMessageDataBaseDTO responseDTO, Map<String, String> mapa ){
		List<BeanCatalogo> listabean = Collections.emptyList();
		List<HashMap<String,Object>> list = null;
		Utilerias utils = Utilerias.getUtilerias();

		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			listabean = new ArrayList<BeanCatalogo>();
			list = responseDTO.getResultQuery();
			for(HashMap<String, Object> map : list) {
				BeanCatalogo beanCatalogo = new BeanCatalogo();
				beanCatalogo.setCve(utils.getString(map.get(mapa.get(CLAVE))).trim());
				beanCatalogo.setDescripcion(utils.getString(map.get(mapa.get(DESCRIPCION))).trim());
				listabean.add(beanCatalogo);
			}
		}
		return listabean;
	}

	/**
	 * Agregar parametros.
	 *
	 * @param beanCertificado El objeto: bean certificado
	 * @param operacion El objeto: operacion
	 * @return Objeto list
	 */
	public List<Object> agregarParametros(BeanMedioEntrega beanMedio, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ALTA)) {
			BigDecimal importMax = null;
			importMax = new BigDecimal(beanMedio.getImporteMax().replace(",", ""));
			parametros.add(beanMedio.getCveMedioEntrega());
			parametros.add(beanMedio.getDesc());
			parametros.add(beanMedio.getComAnualPF());
			parametros.add(beanMedio.getComAnualPM());
			parametros.add(beanMedio.getSeguridad());
			parametros.add(beanMedio.getCanal());
			parametros.add(importMax);

		}
		if(operacion.equalsIgnoreCase(MODIFICACION)) {
			BigDecimal importMax = null;
			importMax = new BigDecimal(beanMedio.getImporteMax().replace(",", ""));
			parametros.add(beanMedio.getCanal());
			parametros.add(beanMedio.getDesc());
			parametros.add(beanMedio.getComAnualPF());
			parametros.add(beanMedio.getComAnualPM());
			parametros.add(beanMedio.getSeguridad());
			parametros.add(importMax);
			parametros.add(beanMedio.getCveMedioEntrega());

		}
		if(operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
			parametros.add(beanMedio.getCveMedioEntrega());
		}
		return parametros;
	}

	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltro(BeanMedioEntrega beanFiltro, String key) {
		StringBuilder filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getCveMedioEntrega() != null && !beanFiltro.getCveMedioEntrega().isEmpty()) {
			getFiltroXParam(filtro, key, "CVE_MEDIO_ENT", beanFiltro.getCveMedioEntrega());
		}
		if (beanFiltro.getDesc() != null && !beanFiltro.getDesc().isEmpty()) {
			getFiltroXParam(filtro, key, "DESCRIPCION", beanFiltro.getDesc());
		}
		if (beanFiltro.getComAnualPF() != null && !beanFiltro.getComAnualPF().isEmpty()) {
			getFiltroXParam(filtro, key, "COM_ANUAL_PF", beanFiltro.getComAnualPF());
		}
		if (beanFiltro.getComAnualPM() != null && !beanFiltro.getComAnualPM().isEmpty()) {
			getFiltroXParam(filtro, key, "COM_ANUAL_PM", beanFiltro.getComAnualPM());
		}
		if (beanFiltro.getSeguridad() != null && !beanFiltro.getSeguridad().isEmpty()) {
			getFiltroXParam(filtro, key, "CKK_SEGURIDAD", beanFiltro.getSeguridad());
		}
		if (beanFiltro.getCanal() != null && !beanFiltro.getCanal().isEmpty()) {
			getFiltroXParam(filtro, key, "CANAL", beanFiltro.getCanal());
		}
		if (beanFiltro.getImporteMax() != null && !beanFiltro.getImporteMax().isEmpty()) {
			getFiltroXParam(filtro, key, "IMP_MAX_SPEI_7X24", beanFiltro.getImporteMax());
		}
		return filtro.toString();
	}

	/**
	 * Obtener el objeto: filtro X parametro.
	 *
	 * @param filtro El objeto: filtro Actual
	 * @param param El objeto: param
	 * @param value El objeto: value
	 * @return El objeto: filtro X param
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}

	/**
	 * Obtener el objeto: estatus.
	 *
	 * @param fchVencimiento El objeto: fch vencimiento
	 * @return El objeto: estatus
	 */
	public String getEstatus(String fchVencimiento) {
		Date fechaActual = new Date();
		if (fchVencimiento == null || fchVencimiento.isEmpty()) {
			//No se puede validar la fecha
			return "NA";
		}
		if (fechaActual.before(Utilerias.getUtilerias().cadenaToFecha(fchVencimiento, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY))) {
			//Estatus vihente
			return "VIGENTE";
		} else {
			//Estatus Vencido
			return "VENCIDO";
		}
	}

	/**
	 * Obtener el objeto: error.
	 * Obtiene el Bean de error en base al responseDTO
	 *
	 * @param responseDAO El objeto: response DAO, si es null se envia el codigo de error por defecto
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		//Si  responseDAO es null, se envia el codigo de error por defecto
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		//Regresa el codigo y mensaje del DAO
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}

}