/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPOACOA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   16/01/2019 09:43:04 AM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasPOACOA.
 *
 * Clase utilerias para albergar los metodos 
 * llamados durante el flujo de negocio,
 * 
 * @author FSW-Vector
 * @since 16/01/2019
 */
public class UtileriasPOACOA implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 248673258563677747L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasPOACOA instancia;
	
	/**
	 * Extensiones validas para archivos de respuesta.
	 */
	private static final String EXTENSIONES = "tpro,ack,apro,liq,nliq,req,rech,rliq,nak";
	
	/**  ACTIVADO. */
	private static final String ACTIVADO = "1";
	
	/** Constenate uno. */
	private static final String UNO = "1";
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasPOACOA getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasPOACOA();
		}
		return instancia;
	}

	/**
	 * Metodo que valida el formato y la extensi�n del archivo.
	 *
	 * @param nombreArchivo String con el nombre del archivo
	 * @param cveMiInstitucion String con mi institucion
	 * @param fechaOperacion String con la fechaOperacion
	 * @return Sttring con el codigo de error
	 */
	public String validaFormatoArchivo(String nombreArchivo, String cveMiInstitucion, String fechaOperacion){
		int posIniExt = nombreArchivo.lastIndexOf('.');
		String cveSpei;
		int posx = 0;
		String result = Errores.OK00000V;
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		String strFchOperacion = "";
		String extension = nombreArchivo.substring(posIniExt+1,nombreArchivo.length());
		String nomArchivo = nombreArchivo.substring(0,posIniExt);
		String sobrente = nomArchivo.substring(cveMiInstitucion.length());
		String fechaLarga;
		
		if(!validaExtensionArchivo(extension)){
			/** Retorno del metodo **/
			result = Errores.ED00072V;	
		}
		posx = cveMiInstitucion.length();
		cveSpei = nombreArchivo.substring(0,posx);
		if(!cveMiInstitucion.equals(cveSpei)){
			/** Retorno del metodo **/
			result =  Errores.ED00077V;
		}
		
		int longoitudSobrante = sobrente.length();
		if(longoitudSobrante-14>0){
			fechaLarga = sobrente.substring(longoitudSobrante-14);
			strFchOperacion = utilerias.formatoToformatoFecha(fechaOperacion, 
					Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY, 
					Utilerias.FORMATO_SIN_ESPACIOS_YYYYMMDD);
			if(!strFchOperacion.equals(fechaLarga.substring(0,8))){
				/** Retorno del metodo **/
				result =  Errores.ED00076V;
			}
		}else{
			/** Retorno del metodo **/
			result =  Errores.ED00073V;
		}
		
		/** Retorno del metodo **/
		return result;
	}
	
	/**
	 * Valida que la extension del archivo sea correcta.
	 *
	 * @param ext Extension del archivo
	 * @return boolean true si es valido o false en caso contrario
	 */
	private boolean validaExtensionArchivo(String ext){
		String[] extsValidas = EXTENSIONES.split(",");
		/** Se declara el objeto de respuesta **/
		boolean isValid = false;
		for (int i = 0; extsValidas.length > i; i++) {
			if (extsValidas[i].equalsIgnoreCase(ext)) {
				isValid = true;
			}
		}
		/** Retorno del metodo **/
		return isValid;
	}
	
	/**
	 * Verificar si iniciado POACOA.
	 *
	 * @param beanResValInicioPOACOA Objeto del tipo BeanResValInicioPOACOA
	 * @return boolean true si esta activo falso en caso contrario
	 */
	public boolean isIniciadoPOACOA(BeanResValInicioPOACOA beanResValInicioPOACOA){
		/** Retorno del metodo **/
		return beanResValInicioPOACOA.getActivacion()!=null && ("1".equals(beanResValInicioPOACOA.getActivacion()) || 
		"2".equals(beanResValInicioPOACOA.getActivacion()));
	}
	
	/**
	 * Valida estatus.
	 *
	 * @param beanCanal El objeto: bean canal
	 * @param resultMap El objeto: result map
	 * @return Objeto bean canal
	 */
	public BeanCanal validaEstatus(BeanCanal beanCanal, Map<String, Object> resultMap) {
		/** Se crea una instancia a la clase Utilerias **/
		Utilerias utilerias = Utilerias.getUtilerias();
		beanCanal.setEstatusCanal(false);
		if(ACTIVADO.equals(utilerias.getString(resultMap.get("ACT")))){
			beanCanal.setEstatusCanal(true);
		} 
		/** Retorno del metodo **/
		return beanCanal;
	}
	
	/**
	 * Almacena lista.
	 *
	 * @param list El objeto: list
	 * @return Objeto list
	 */
	public List<BeanCanal> almacenaLista(List<HashMap<String, Object>> list){
		/**  Se declara una lista para alamcemar los registros del mapList **/
		List<BeanCanal> listBean = new ArrayList<BeanCanal>();
		/**  Se recorre la lista **/
		for (HashMap<String, Object> resultMap : list) {
			/** Se declara un bean por cada registro obtenido **/
			BeanCanal beanCanal = new BeanCanal();
			beanCanal.setIdCanal(resultMap.get("CVE_MEDIO_ENT")
					.toString().trim());
			beanCanal.setNombreCanal(resultMap.get("DESCRIPCION")
					.toString().trim());
			beanCanal.setEstatusCanal(false);
			if (resultMap.get("ACTIVO").toString().trim()
					.equals(ACTIVADO)) {
				beanCanal.setEstatusCanal(true);
			}
			/** Se asigna el bean a la lista **/
			listBean.add(beanCanal);
		}
		/** Retorno del metodo **/
		return listBean;
	}
	
	/**
	 * Metodo para validar el codigo de error
	 * @param esActivacion - El parametro esActivacion
	 * @param beanResPOACOA - El parametro beanResPOACOA
	 * @return BeanResBase - return beanBase
	 */
	public BeanResBase validaCodigo(boolean esActivacion, BeanResPOACOA beanResPOACOA) {
		BeanResBase res = new BeanResBase();
		//Trata de desactivar pero la bandera de no retorno ya esta prendida
		if(!esActivacion && UNO.equals(beanResPOACOA.getNoHayRetorno())){
			/** Se setea el codigo de error **/
			res.setCodError(Errores.ED00079V);
			res.setTipoError(Errores.TIPO_MSJ_ALERT);
		}
		return res;
	}
	
	
	/**
	 * metodo que valida el codigo de error
	 * @param res - parametro BeanBase
	 * @return boolean -  resultado respuestaOperacion
	 */
	public boolean validaEjec(BeanResBase res) {
		Boolean respuestaOperacion = false;
		if (res.getCodError().equals(Errores.OK00000V)) {
			/** En caso de que la operacion ejcutada resulte correcta entonces el registrro que se envia a la bitacora sera TRUE **/
			respuestaOperacion = true;
		}
		
		return respuestaOperacion;
	}
	

	
	/**
	 * Metodo que setea la pagina al bean paginador
	 * @param beanPaginador - el parametro beanPaginador
	 * @param reqCanal - el parametro reqCanal
	 * @return BeanPaginador - return bean
	 */
	public BeanPaginador obtienePaginador(BeanPaginador beanPaginador,BeanReqCanal reqCanal) {
		BeanPaginador beanPagRes = new BeanPaginador();
		
		/** Se valida el bean de paginacion **/
		 if(beanPaginador == null){
			 /** Se crea el bean de paginacion **/
			 beanPagRes = new BeanPaginador();
			 reqCanal.setPaginador(beanPagRes);
	      } else if("".equals(beanPaginador.getPagina())){
	    	  beanPagRes.setPagina(UNO);
	      }else {
	    	  beanPagRes = reqCanal.getPaginador();
	      }
		 
		 return beanPagRes;
	}
	
}
