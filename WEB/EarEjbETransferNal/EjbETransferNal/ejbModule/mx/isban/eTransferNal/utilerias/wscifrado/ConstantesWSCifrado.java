/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesWSSeguro.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-06-2018 04:50:21 PM Juan Jesus Beltran R. VECTOR SWF Creacion
 */
package mx.isban.eTransferNal.utilerias.wscifrado;

/**
 * Class ConstantesWSSeguro.
 *
 * @author FSW-Vector
 * @since 08-06-2018
 */
public final class ConstantesWSCifrado{
	
	/** La constante CANAL. */
	public static final String CANAL = "ARQ_MENSAJERIA";
	
	/** La constante STRANSANT. */
	public static final String STRANSANT = "STRANSANT";
	
	/** La constante STRANSANT. */
	public static final String STRANTRF2 = "STRANTRF2";
	
	/** La constante STRANSPID. */
	public static final String STRANSPID = "STRANSPID";
	
	/** La constante CVE_MEDIO_ENT. */
	public static final String CVE_MEDIO_ENT 	= "CANAL";
	
	/** La constante TAMANIO. */
	public static final String TAMANIO 			= "LONGITUD_CIFRADO";
	
	/** La constante CADENA_INI. */
	public static final String CADENA_INI 		= "CADENA_Y_VECTOR_INI";
	
	/** La constante CADENA. */
	public static final String CADENA 			= "CADENA_ENCRIPTADA";
	
	/** La constante CAMPO_COD_ERROR. */
	public static final String CAMPO_COD_ERROR 	= "COD_ERROR";

	/** La constante CAMPO_REFERENCIA. */
	public static final String CAMPO_REFERENCIA = "REFERENCIA";

	/** La constante CAMPO_ESTATUS. */
	public static final String CAMPO_ESTATUS 	= "ESTATUS";
	
	/** La constante CAMPO_DESCRIPCION. */
	public static final String CAMPO_DESCRIPCION = "DESCRIPCION";
	
	/** La constante CAMPO_DESCRIPCION_ESTATUS. */
	public static final String CAMPO_DESCRIPCION_ESTATUS = "DESCRIPCION_ESTATUS";

	/** La constante STRING. */
	public static final String STRING 			= "String";
	
	/** La constante NUMERICO. */
	public static final String NUMERICO 		= "NUMERICO";
	
	/** La constante DECIMAL. */
	public static final String DECIMAL 			= "DECIMAL";
	
	/** La constante CVE_RASTREO. */
	public static final String CVE_RASTREO 		= "CVE_RASTREO";
	
	/** La constante PIPE. */
	public static final String PIPE 			= "|";
	
	/** La constante DIAGONALES_INVERSA. */
	public static final String DIAGONALES_INVERSA = "\\";
	
	/** La constante CERO. */
	public static final String CERO 			= "0";
	
	/** La constante ERROR_IDA. */
	public static final String ERROR_IDA 		= "Ocurrio un error de Isban Data Access";
	
	/** La constante EXP_REGULAR_NUMEROS. */
	public static final String EXP_REGULAR_NUMEROS = "([0-9]+(\\.[0-9]{1,2}){0,1})";
	
	/** La constante NUM_70. */
	public static final int NUM_70	= 70;
	
	/** La constante NUM_8. */
	public static final int NUM_8	= 8;
	
	/** La constante NUM_30. */
	public static final int NUM_30 	= 30;
	
	/** La constante NUM_7. */
	public static final int NUM_7 	= 7;
	
	/** La constante NUM_1. */
	public static final int NUM_1 	= 1;
	
	/** La constante NUM_2. */
	public static final int NUM_2 	= 2;
	
	/** La constante NUM_20. */
	public static final int NUM_20 	= 20;
	
	/**
	 * Nueva instancia constantes WS seguro.
	 */
	private ConstantesWSCifrado() {
	}

}
