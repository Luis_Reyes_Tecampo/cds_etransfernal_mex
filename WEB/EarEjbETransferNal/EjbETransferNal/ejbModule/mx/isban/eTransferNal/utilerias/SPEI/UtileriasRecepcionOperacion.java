/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: UtileriasRecepcionOpHTO.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:47:39 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsHorasHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecOpOrdent;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecOpRecep;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecOperaOtros;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOpera;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperaci;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepcionOperaCambios;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * en esta clase de utilerias se las funciones para realizar una devolucion
 * extemporanea.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
public class UtileriasRecepcionOperacion extends Architech {

	/** variable serial. */
	private static final long serialVersionUID = 5015844902033230347L;

	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionOperacion util = new UtileriasRecepcionOperacion();

	/** The utilerias. */
	private Utilerias utilerias = Utilerias.getUtilerias();

	/** La constante NUM_CUENTA_TRAN. */
	public static final String NUMCUENTATRAN = "NUM_CUENTA_TRAN";

	/** La constante CVE_MI_INSTITUC. */
	public static final String CVEMIINSTITUC = "CVE_MI_INSTITUC";

	/** La constante FOLIO_PAQUETE. */
	public static final String FOLIOPAQUETE = "FOLIO_PAQUETE";

	/** La constante NUM_CUENTA_REC. */
	public static final String NUMCUENTAREC = "NUM_CUENTA_REC";

	/** La constante FCH_OPERACION. */
	public static final String FCHOPERACION = "FCH_OPERACION";

	/** La constante CVE_INST_ORD. */
	public static final String CVEINSTORD = "CVE_INST_ORD";

	/** La constante FOLIO_PAGO. */
	public static final String FOLIOPAGO = "FOLIO_PAGO";

	/** La constante CVE_RASTREO. */
	public static final String CVERASTREO = "CVE_RASTREO";

	/** La constante CVE_RASTREO_ORI. */
	public static final String CVERASTREOORI = "CVE_RASTREO_ORI";

	/** La constante CVE_TRANSFE. */
	public static final String CVETRANSFE = "CVE_TRANSFE";

	/** La constante TIPO_PAGO. */
	public static final String TIPOPAGO = "TIPO_PAGO";

	/** La constante FCH_CAPTURA. */
	public static final String FCHCAPTURA = "FCH_CAPTURA";

	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasRecepcionOperacion getInstance() {
		return util;
	}

	/**
	 * Continua recorer.
	 *
	 * @param list                El objeto: list
	 * @param beanTranSpeiRecList El objeto: bean tran spei rec list
	 * @param beanReqTranSpeiRec  El objeto: bean req tran spei rec
	 * @return Objeto list
	 */
	public BeanResConsTranSpeiRecDAO continuaRecorer(List<HashMap<String, Object>> list,
			List<BeanTranSpeiRecOper> beanTranSpeiRecList, BeanReqTranSpeiRec beanReqTranSpeiRec) {
		BeanResConsTranSpeiRecDAO response = new BeanResConsTranSpeiRecDAO();
		List<BeanTranSpeiRecOper> listOper = new ArrayList<BeanTranSpeiRecOper>();
		/** recorre la lista **/
		for (HashMap<String, Object> map : list) {
			/** descompone la consulta **/
			BeanTranSpeiRecOper beanTranSpeiRec = descomponeConsulta(map);
			response.setTotalReg(utilerias.getInteger(map.get("CONT")));
			/** asigna el valor **/
			listOper.add(beanTranSpeiRec);
		}
		response.setBeanTranSpeiRecList(listOper);
		/** regresa el valor **/
		return response;
	}

	/**
	 * Metodo que genera un objeto del tipo BeanTranSpeiRec a partir de la respuesta
	 * de la consulta.
	 *
	 * @param map                Objeto del tipo Map<String,Object> que almacena el
	 *                           conjunto de registros
	 * @return BeanTranSpeiRec Objeto del tipo BeanTranSpeiRec
	 */
	private BeanTranSpeiRecOper descomponeConsulta(Map<String, Object> map) {
		/** incialiaza el objeto **/
		BeanTranSpeiRecOper beanTranSpeiRec = null;
		// se inicializa el objeto
		beanTranSpeiRec = new BeanTranSpeiRecOper();
		beanTranSpeiRec.setNumCuentaTran(utilerias.getString(map.get(NUMCUENTATRAN)));
		beanTranSpeiRec.setUsuario(utilerias.getString(map.get("COD_USR_APART")));
		beanTranSpeiRec.setBeanDetalle(new BeanDetRecepOperacion());
		beanTranSpeiRec.getBeanDetalle().setCodigoError(utilerias.getString(map.get("CODIGO_ERROR")));

		// se inilza bean generales
		beanTranSpeiRec.getBeanDetalle().setDetalleGral(new BeanDetRecepOpera());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFchOperacion(utilerias.getString(map.get(FCHOPERACION)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveMiInstitucion(utilerias.getString(map.get(CVEMIINSTITUC)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveInstOrd(utilerias.getString(map.get(CVEINSTORD)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPaquete(utilerias.getString(map.get(FOLIOPAQUETE)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPago(utilerias.getString(map.get(FOLIOPAGO)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveRastreo(utilerias.getString(map.get(CVERASTREO)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveRastreoOri(utilerias.getString(map.get(CVERASTREOORI)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setEstatusOpe(utilerias.getString(map.get("ESTATUS")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveTransfer(utilerias.getString(map.get(CVETRANSFE)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setDesCveTransf(utilerias.getString(map.get("DESCRIPCION")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPaqueteDev(utilerias.getString(map.get("FOLIO_PAQ_DEV")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFolioPagoDev(utilerias.getString(map.get("FOLIO_PAGO_DEV")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setCveOperacion(utilerias.getString(map.get("CVE_OPERACION")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleGral().setFchCaptura(utilerias.getString(map.get("FECHA_CAPTURA")).trim());
		
		// inicializa el bean para gurarda la prioridia
		beanTranSpeiRec.getBeanDetalle().setDetalleTopo(new BeanDetRecepOperaci());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setTopologia(utilerias.getString(map.get("TOPOLOGIA")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setTipoPago(utilerias.getString(map.get(TIPOPAGO)).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setTipoOperacion(utilerias.getString(map.get("TIPO_OPERACION")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setCde(utilerias.getString(map.get("CDE")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setCda(utilerias.getString(map.get("CDA")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setPrioridad(utilerias.getString(map.get("PRIORIDAD")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setDescripcionPrioridad(utilerias.getString(map.get("DESCRIP_PRI")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetalleTopo().setDescTipoOperacion(utilerias.getString(map.get("DES_TIPO_OPE")).trim());

		// iniicializa el bien para los estatus
		beanTranSpeiRec.getBeanDetalle().setDetEstatus(new BeanDetRecepOp());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusBanxico(utilerias.getString(map.get("ESTATUS_BANXICO")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setEstatusTransfer(utilerias.getString(map.get("ESTATUS_TRANSFER")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setCvePago(utilerias.getString(map.get("CVE_PARA_PAGO")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setRefeTransfer(utilerias.getString(map.get("REFE_TRANSFER")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setConceptoPago2(utilerias.getString(map.get("CONCEPTO_PAGO2")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setConceptoPago1(utilerias.getString(map.get("CONCEPTO_PAGO1")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setRefeCobranza(utilerias.getString(map.get("REFE_COBRANZA")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setDescripcionBanxico(utilerias.getString(map.get("DES_EST_BANX")).trim());
		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setDescripcionTrasnfer(utilerias.getString(map.get("DESCRIP_EST_TRAN")).trim());

		// inicilaiza el bean de cambios
		beanTranSpeiRec.getBeanDetalle().setBeanCambios(new BeanDetRecepcionOperaCambios());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setFchCaptura(utilerias.getString(map.get(FCHCAPTURA)).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setMotivoDevol(utilerias.getString(map.get("MOTIVO_DEVOL")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setRefeNumerica(utilerias.getString(map.get("REFE_NUMERICA")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setVersion(utilerias.getString(map.get("U_VERSION")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setLongRastreo(utilerias.getString(map.get("LONG_RASTREO")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setMotivoDevolDesc(utilerias.getString(map.get("MOTIVO_DEVOL_DESC")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setAplicativo(utilerias.getString(map.get("APLICATIVO")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setBucCliente(utilerias.getString(map.get("BUC_CLIENTE")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setCtaReceptoraOri(utilerias.getString(map.get(NUMCUENTATRAN)).trim());

		// descripcion del error
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().setOtros(new BeanDetRecOperaOtros());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setSwift(utilerias.getString(map.get("UETR_SWIFT")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setDesError(utilerias.getString(map.get("MSG_ERROR")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setDescTipoPgo(utilerias.getString(map.get("DES_TIPO_PAGO")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setSwiftUno(utilerias.getString(map.get("CAMPO_SWIFT1")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setSwiftDos(utilerias.getString(map.get("CAMPO_SWIFT2")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setFchOperOri(utilerias.getString(map.get("FCH_OPER_ORIG")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setIndBenefRec(utilerias.getString(map.get("IND_BENEF_RECURSOS")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setMontoPgoOri(utilerias.getString(map.get("MONTO_PAGO_ORIG")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setRefeAdi(utilerias.getString(map.get("REFE_ADICIONAL1")).trim());
		beanTranSpeiRec.getBeanDetalle().getBeanCambios().getOtros().setNumCuentClv(utilerias.getString(map.get("NUM_CUENTA_CLABE")).trim());

		// inicializa el bean del ordenante
		beanTranSpeiRec.getBeanDetalle().setOrdenante(new BeanDetRecOpOrdent());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setMonto(utilerias.getString(map.get("MONTO")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setNumCuentaOrd(utilerias.getString(map.get("NUM_CUENTA_ORD")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setNombreOrd(utilerias.getString(map.get("NOMBRE_ORD")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setTipoCuentaOrd(utilerias.getString(map.get("TIPO_CUENTA_ORD")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setCveIntermeOrd(utilerias.getString(map.get("CVE_INTERME_ORD")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setRfcOrd(utilerias.getString(map.get("RFC_ORD")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setIva(utilerias.getString(map.get("IVA")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setImporteCargo(utilerias.getString(map.get("IMPORTE_CARGO")).trim());
		beanTranSpeiRec.getBeanDetalle().getOrdenante().setFolioPaqueteOrd(utilerias.getString(map.get(FOLIOPAQUETE)).trim());

		// inicializa el bean receptor
		beanTranSpeiRec.getBeanDetalle().setReceptor(new BeanDetRecOpRecep());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setNumCuentaRec2(utilerias.getString(map.get("NUM_CUENTA_REC2")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setNombreRec2(utilerias.getString(map.get("NOMBRE_REC2")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setRfcRec(utilerias.getString(map.get("RFC_REC")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setRfcRec2(utilerias.getString(map.get("RFC_REC2")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setTipoCuentaRec2(utilerias.getString(map.get("TIPO_CUENTA_REC2")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setTipoCuentaRec(utilerias.getString(map.get("TIPO_CUENTA_REC")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setNombreRec(utilerias.getString(map.get("NOMBRE_REC")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setNumCuentaRec(utilerias.getString(map.get(NUMCUENTAREC)).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setImporteDls(utilerias.getString(map.get("IMPORTE_DLS")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setImporteAbono(utilerias.getString(map.get("IMPORTE_ABONO")).trim());
		beanTranSpeiRec.getBeanDetalle().getReceptor().setBancoRec(utilerias.getString(map.get(CVEMIINSTITUC)).trim());

		return beanTranSpeiRec;
	}

	/**
	 * Descompone consulta count.
	 *
	 * @param beanReqTranSpeiRec        Bean del tipo BeanReqConsTranSpeiRec
	 * @param beanResConsTranSpeiRecDAO Bean del tipo beanResConsTranSpeiRecDAO
	 * @param map                       Objeto del tipo Map<String,Object>
	 */
	public void descomponeConsultaCount(BeanReqTranSpeiRec beanReqTranSpeiRec,
			BeanResConsTranSpeiRecDAO beanResConsTranSpeiRecDAO, Map<String, Object> map) {
		/** convierte el cont en int **/
		beanResConsTranSpeiRecDAO.setTotalReg(utilerias.getInteger(map.get(ConstantesRecepOperacion.CONT)));
	}

	/**
	 * Asigna datos tab horas SPEI.
	 *
	 * @param beanConsHorasHist the bean cons horas hist
	 * @param map               the map
	 * @return the bean cons horas hist
	 */
	public BeanConsHorasHist asignaDatosTabHorasSPEI(BeanConsHorasHist beanConsHorasHist, Map<String, Object> map) {
		/** Asigna datos de consulta al bean */
		beanConsHorasHist.getBeanGeneralHoras().setDifAcuseAprobacion(utilerias.getString(map.get("DIF_ACUSE_APROBACION")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setDifCapturaRecepcion(utilerias.getString(map.get("DIF_CAPTURA_RECEPCION")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setDifRecepcionAcuse(utilerias.getString(map.get("DIF_RECEPCION_ACUSE")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setSwActHisCo(utilerias.getString(map.get("SW_ACT_HIS_CO")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setSwActHisEn(utilerias.getString(map.get("SW_ACT_HIS_EN")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setSwActHisLs(utilerias.getString(map.get("SW_ACT_HIS_LS")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setSwCalcInteres("N");
		beanConsHorasHist.getBeanGeneralHoras().setTipo(utilerias.getString(map.get("TIPO")).trim());
		beanConsHorasHist.getBeanGeneralHoras().setTipoPago(utilerias.getString(map.get(TIPOPAGO)).trim());
		beanConsHorasHist.getBeanGeneralHoras().setuVersion(utilerias.getString(map.get("U_VERSION")).trim());

		beanConsHorasHist.getBeanCvesHoras().setCveInstBenef(utilerias.getString(map.get("CVE_INST_BENEF")).trim());
		beanConsHorasHist.getBeanCvesHoras().setCveInstOrd(utilerias.getString(map.get(CVEINSTORD)).trim());
		beanConsHorasHist.getBeanCvesHoras().setCveMedioEnt(utilerias.getString(map.get("CVE_MEDIO_ENT")).trim());
		beanConsHorasHist.getBeanCvesHoras().setCveRastreo(utilerias.getString(map.get(CVERASTREO)).trim());
		beanConsHorasHist.getBeanCvesHoras().setCveRastreoOri(utilerias.getString(map.get(CVERASTREOORI)).trim());
		beanConsHorasHist.getBeanCvesHoras().setCveTransfe(utilerias.getString(map.get(CVETRANSFE)).trim());
		beanConsHorasHist.getBeanCvesHoras().setFolioPago(utilerias.getString(map.get(FOLIOPAGO)).trim());
		beanConsHorasHist.getBeanCvesHoras().setFolioPaquete(utilerias.getString(map.get(FOLIOPAQUETE)).trim());
		beanConsHorasHist.getBeanCvesHoras().setReferencia(utilerias.getString(map.get("REFERENCIA")).trim());

		beanConsHorasHist.getBeanFechasHoras().setFchAcuse(utilerias.getString(map.get("FCH_ACUSE")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchAprobacion(utilerias.getString(map.get("FCH_APROBACION")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchCaptura(utilerias.getString(map.get(FCHCAPTURA)).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchCapturaRec(utilerias.getString(map.get("FCH_CAPTURA_REC")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchRecepcion(utilerias.getString(map.get("FCH_RECEPCION")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchConfirmacion(utilerias.getString(map.get("FCH_CONFIRMACION")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchEnvio(utilerias.getString(map.get("FCH_ENVIO")).trim());
		beanConsHorasHist.getBeanFechasHoras().setFchOperacion(utilerias.getString(map.get(FCHOPERACION)).trim());

		return beanConsHorasHist;
	}

}