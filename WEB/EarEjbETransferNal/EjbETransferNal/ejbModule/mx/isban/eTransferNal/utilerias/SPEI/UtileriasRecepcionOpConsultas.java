/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasRecepcionOpConsultas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/09/2018 06:07:44 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * En esta clase de utilerias se hace el swicht de las consultas, para mostrar la informacion segun la peticion del menu seleccionado,
 * esto para las recepcion de operaciones historicas como las del dia.
 *
 * @author FSW-Vector
 * @since 13/09/2018
 */
public class UtileriasRecepcionOpConsultas  extends Architech {
		
		
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1624455250037942756L;

		
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasRecepcionOpConsultas util = new UtileriasRecepcionOpConsultas();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasRecepcionOpConsultas getInstance(){
		return util;
	}
	
    
    /**
     * Obtener el objeto: query count.
     *
     * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
     * @return El objeto: query count
     */
    public String getQueryCount(BeanReqTranSpeiRec beanReqTranSpeiRec){
    	String query="";
    	/**realiza la consulta **/
    	/**realiza la consulta segun el flitro seleccionado **/
		query = ConstantesRecepOperacionApartada.QUERY_COUNT_CONSULTA_TODAS;
		
		
		return query;
    }
    
    
    /**
     * Obtener el objeto: query count.
     *
     * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
     * @return El objeto: query count
     */
    public String getQueryConsult(BeanReqTranSpeiRec beanReqTranSpeiRec){
    	StringBuilder builder=new StringBuilder();
    	/**realiza la consulta **/
		builder.append(ConstantesRecepOperacionApartada.QUERY_CONSULTA_TODAS);
		
		return builder.toString();
    }
    
    /**
     * Metodo para obtener el query.
     *
     * @param beanReqTranSpeiRec objeto del tipo BeanReqConsTranSpeiRec
     * @return String cadena con el query
     */
    public String getQueryCountHTO(BeanReqTranSpeiRec beanReqTranSpeiRec){
    	String query="";
	    query = ConstantesRecepOperacionApartada.QUERY_COUNT_CONSULTA_TODAS_HTO;
		
		return query;
    }
    
    /**
     * Obtener el objeto: query consulta HTO.
     *
     * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
     * @return El objeto: query consulta HTO
     */
    public String getQueryConsultaHTO(BeanReqTranSpeiRec beanReqTranSpeiRec){
    	StringBuilder builder = new StringBuilder();
    	builder.append(ConstantesRecepOperacionApartada.QUERY_CONSULTA_TODAS_HTO);
			
    	return builder.toString();
    }
    
    
    /**
     * Genera pista auditoras.
     *
     * @param datos El objeto: datos
     * @param resOp El objeto: res op
     * @param sesion El objeto: sesion
     * @param historico El objeto: historico
     * @return Objeto bean pista auditora
     */
    public BeanPistaAuditora generaPistaAuditoras(String datos, boolean resOp, ArchitechSessionBean sesion, boolean historico) {
		String[] dato= datos.split("/");
		String tabla="TRAN_SPEI_REC";
		/**pregunta hitorico para hacer el insert en la tabla correspondiente**/
		if(historico) {
			/**tabla apartados historicos**/
			tabla = "TRAN_SPEI_REC_HIS";
		}
		/**pregunta si vienen de estas acciones para asignar la tabla**/
		if("recepOperaApartadas.do".equals(dato[0]) || "liberarOperApartadas.do".equals(dato[0])){
			/**tabla apartados historicos**/
			tabla = "TRAN_SPEI_BLOQUEO";
			if(historico) {
				/**tabla apartados historicos**/
				tabla = "TRAN_SPEI_BLOQUEO_HIS";
			}
		}
		
		/**Objetos para las Pistas auditoras**/
		BeanPistaAuditora beanPista = new BeanPistaAuditora();
		/** Enviamos los datos de la pista auditora**/
		beanPista.setNomTabla(tabla);
		beanPista.setOperacion(dato[0]);
		beanPista.setUrlController(dato[1]);
		beanPista.setDatoFijo(ConstantesRecepOperacionApartada.DATO_FIJO);
		beanPista.setDatoModi(dato[2]);		
		beanPista.setCodigoError(dato[3]);
		beanPista.setDtoNuevo(dato[4]);
		if(dato.length>5) {
			beanPista.setDtoAnterior(dato[5]);
		}
				
		
		beanPista.setEstatus(ConstantesRecepOperacionApartada.NOK);
		/**Cuando la operacion es correcta**/
		if (resOp) {
			beanPista.setEstatus(ConstantesRecepOperacionApartada.OK);
		} 
		
		/**regresa la pistas de auditoria**/
		return beanPista;
	}
    
    /**
     * Obtener existe.
     *
     * @param list El objeto: list
     * @param responseDTO El objeto: response DTO
     * @return Objeto bean res base
     */
    public BeanResBase  obtenerExiste(ResponseMessageDataBaseDTO responseDTO){
    	Utilerias utilerias = Utilerias.getUtilerias();
    	List<HashMap<String,Object>> list = null;
    	//se asigna la variable de regreso
    	BeanResBase response = new BeanResBase(); 
    	if(responseDTO.getResultQuery()!= null){
    		//se asigna el resultado
			list = responseDTO.getResultQuery();	    				
			/**inicializa el map**/
			Map<String,Object> mapResult = list.get(0);
			/**Si se encuentra el registro se manda el error**/
			if(utilerias.getInteger(mapResult.get("TOTAL")) > 0) {
				response.setCodError(Errores.ED00133V);
			}else {
				//No existe el registro. Puede continuar el flujo
				response.setCodError(responseDTO.getCodeError());
			}
		 }
		 response.setMsgError(responseDTO.getMessageError());
		return response;
    }
    
}
