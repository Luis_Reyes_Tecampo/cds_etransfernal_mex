/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasCertificadoCifrado.java
 * 
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 10:28:08 AM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasCertificadoCifrado.
 * 
 * Clase que contiene metodos de utilerias que ayudan al negocio con el manejo de datos 
 * que pasan por dicha capa
 *
 * @author FSW-Vector
 * @since 12/09/2018
 */
public class UtileriasCertificadoCifrado extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3753742596172765250L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasCertificadoCifrado instancia;
	
	//Constantes Auxiliares 
	/** La constante CONSULTA. */
	private static final String CONSULTA = "consulta";
	
	/** La constante CLAVE. */
	private static final String CLAVE = "clave";
	
	/** La constante DESCRIPCION. */
	private static final String DESCRIPCION = "descripcion";
	
	/** La constante DESCRIPCIONREGISTRO. */
	private static final String DESCRIPCIONREGISTRO = "DESCRIPCION";
	
	/** La constante REGISTRO. */
	private static final String REGISTRO = "VALOR_FALTA";
	
	/** La constante CANAL. */
	private static final String CANAL = "CANAL";
	
	//Combos
	/** La constante ORDERBY_PARAMETRO. */
	public static final String ORDERBY_PARAMETRO = " ORDER BY CVE_PARAMETRO ";
	
	/** La constante CONSULTA_LISTA_CANAL. */
	public static final String CONSULTA_LISTA_CANAL = "SELECT CANAL FROM COMU_MEDIOS_ENT WHERE"
			+ " CANAL IS NOT NULL GROUP BY(CANAL) ORDER BY CANAL";
	
	/** La constante CONSULTA_LISTA_FIRMAS. */
	public static final String CONSULTA_LISTA_FIRMAS = "SELECT VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%ALGDIGEST%'" + ORDERBY_PARAMETRO;
	
	/** La constante CONSULTA_LISTA_CIFRADOS. */
	public static final String CONSULTA_LISTA_CIFRADOS = "SELECT VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%ALGSIMETRI%'" + ORDERBY_PARAMETRO;
	
	/** La constante CONSULTA_LISTA_TECNOLOGIAS. */
	public static final String CONSULTA_LISTA_TECNOLOGIAS = "SELECT VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%TECORIGEN%'" + ORDERBY_PARAMETRO;
	
	/** La constante LISTA_CANALES. */
	public static final String LISTA_CANALES = "lst_canales";
	
	/** La constante LISTA_FIRMAS. */
	public static final String LISTA_FIRMAS = "lst_firmas";
	
	/** La constante LISTA_CIFRADOS. */
	public static final String LISTA_CIFRADOS = "lst_cifrados";
	
	/** La constante LISTA_TECNOLOGIAS. */
	public static final String LISTA_TECNOLOGIAS = "lst_tecnologias";
	
	//Operaciones
	/** La constante ALTA. */
	public static final String ALTA = "alta";
	
	/** La constante BAJA. */
	public static final String BAJA = "baja";
	
	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";
	
	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
	
	//Constantes para expotacion batch
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE = "Canal,Alias,Numero de certificado,Fecha de Vencimiento,Algoritmo de Firma, Algoritmo de Cifrado, Tecnologia Origen";
	
	/** La constante CONSULTA_EXPORTAR_TODOs. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT CANAL || ',' || ALIAS_KS || ',' || NUM_CERTIF || ',' || FCH_VENCIMIENTO || ',' || ALG_DIGEST || ',' || ALG_SIMETRICO || ',' || TEC_ORIGEN FROM TRAN_CERTIF_KS [FILTRO_WH]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY CANAL ASC";
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	boolean first = true;

	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasCertificadoCifrado getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasCertificadoCifrado();
		}
		return instancia;
	}
	
	
	/**
	 * Valida consulta.
	 *
	 * @param consulta El objeto: consulta
	 * @return Objeto hash map
	 */
	public Map<String, String> validaConsulta(String consulta){
		Map<String, String> mapa = new HashMap<String, String>();
		if(consulta.equalsIgnoreCase(LISTA_CANALES)) {
			mapa.put(CONSULTA, CONSULTA_LISTA_CANAL);
			mapa.put(CLAVE, CANAL);
			mapa.put(DESCRIPCION, DESCRIPCIONREGISTRO);
		}
		if(consulta.equalsIgnoreCase(LISTA_FIRMAS)) {
			mapa.put(CONSULTA, CONSULTA_LISTA_FIRMAS);
			mapa.put(CLAVE, REGISTRO);
			mapa.put(DESCRIPCION, REGISTRO);
		}
		if(consulta.equalsIgnoreCase(LISTA_CIFRADOS)) {
			mapa.put(CONSULTA, CONSULTA_LISTA_CIFRADOS);
			mapa.put(CLAVE, REGISTRO);
			mapa.put(DESCRIPCION, REGISTRO);
		}
		if(consulta.equalsIgnoreCase(LISTA_TECNOLOGIAS)) {
			mapa.put(CONSULTA, CONSULTA_LISTA_TECNOLOGIAS);
			mapa.put(CLAVE, REGISTRO);
			mapa.put(DESCRIPCION, REGISTRO);
		}
		return mapa;
	}
	
	/**
	 * Recorrer lista.
	 *
	 * @param responseDTO El objeto: response DTO
	 * @param mapa El objeto: mapa
	 * @return Objeto list
	 */
	public List<BeanCatalogo> recorrerLista(ResponseMessageDataBaseDTO responseDTO, Map<String, String> mapa ){
		List<BeanCatalogo> listabean = Collections.emptyList();
		List<HashMap<String,Object>> list = null;	
		Utilerias utils = Utilerias.getUtilerias();
		
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			listabean = new ArrayList<BeanCatalogo>();
			list = responseDTO.getResultQuery();
			for(HashMap<String, Object> map : list) {
				BeanCatalogo beanCatalogo = new BeanCatalogo();
				beanCatalogo.setCve(utils.getString(map.get(mapa.get(CLAVE))).trim());
				beanCatalogo.setDescripcion(utils.getString(map.get(mapa.get(DESCRIPCION))).trim());
				listabean.add(beanCatalogo);
			}
		}
		return listabean;
	}
	
	/**
	 * Valida lista.
	 *
	 * @param beanCertificados El objeto: bean certificados
	 * @param listaActual El objeto: lista actual
	 * @param mapa El objeto: mapa
	 * @param consulta El objeto: consulta
	 * @return Objeto bean certificados
	 */
	public BeanCombosCertificadoCifrado validaLista(BeanCombosCertificadoCifrado beanCertificados, List<BeanCatalogo> listaActual, Map<String, String> mapa, String consulta) {
		if(consulta.equals(LISTA_CANALES)) {
			beanCertificados.setListaCanales(listaActual);
		}
		if(consulta.equals(LISTA_FIRMAS)) {
			beanCertificados.setListaFirmas(listaActual);
		}
		if(consulta.equals(LISTA_CIFRADOS)) {
			beanCertificados.setListaCifrados(listaActual);
		}
		if(consulta.equals(LISTA_TECNOLOGIAS)) {
			beanCertificados.setListaTecnologias(listaActual);
		}
		return beanCertificados;
	}
	
	/**
	 * Agregar parametros.
	 *
	 * @param beanCertificado El objeto: bean certificado
	 * @param operacion El objeto: operacion
	 * @return Objeto list
	 */
	public List<Object> agregarParametros(BeanCertificadoCifrado beanCertificado, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ALTA)) {
			parametros.add(beanCertificado.getCanal());
			parametros.add(beanCertificado.getAlias());
			parametros.add(beanCertificado.getNumCertificado());
			parametros.add(beanCertificado.getFchVencimiento());
			parametros.add(beanCertificado.getAlgDigest());
			parametros.add(beanCertificado.getAlgSimetrico());
			parametros.add(beanCertificado.getTecOrigen());
		}
		if(operacion.equalsIgnoreCase(MODIFICACION)) {
			parametros.add(beanCertificado.getAlias());
			parametros.add(beanCertificado.getNumCertificado());
			parametros.add(beanCertificado.getFchVencimiento());
			parametros.add(beanCertificado.getAlgDigest());
			parametros.add(beanCertificado.getAlgSimetrico());
			parametros.add(beanCertificado.getTecOrigen());
			parametros.add(beanCertificado.getCanal());
		}
		if(operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
			parametros.add(beanCertificado.getCanal());
		}
		return parametros;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param key El objeto: key
	 * @return El objeto: filtro
	 */
	public String getFiltro(BeanCertificadoCifrado beanFiltro, String key) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getCanal() != null && !beanFiltro.getCanal().isEmpty()) {
			getFiltroXParam(filtro, key, "CANAL", beanFiltro.getCanal());
		}
		if (beanFiltro.getAlias() != null && !beanFiltro.getAlias().isEmpty()) {
			getFiltroXParam(filtro, key, "ALIAS_KS", beanFiltro.getAlias());
		}
		if (beanFiltro.getNumCertificado() != null && !beanFiltro.getNumCertificado().isEmpty()) {
			getFiltroXParam(filtro, key, "NUM_CERTIF", beanFiltro.getNumCertificado());
		}
		if (beanFiltro.getFchVencimiento() != null && !beanFiltro.getFchVencimiento().isEmpty()) {
			getFiltroXParam(filtro, key, "FCH_VENCIMIENTO", beanFiltro.getFchVencimiento());
		}
		if (beanFiltro.getAlgDigest() != null && !beanFiltro.getAlgDigest().isEmpty()) {
			getFiltroXParam(filtro, key, "ALG_DIGEST", beanFiltro.getAlgDigest());
		}
		if (beanFiltro.getAlgSimetrico() != null && !beanFiltro.getAlgSimetrico().isEmpty()) {
			getFiltroXParam(filtro, key, "ALG_SIMETRICO", beanFiltro.getAlgSimetrico());
		}
		if (beanFiltro.getTecOrigen() != null && !beanFiltro.getTecOrigen().isEmpty()) {
			getFiltroXParam(filtro, key, "TEC_ORIGEN", beanFiltro.getTecOrigen());
		}
		return filtro.toString();
	}
	
	/**
	 * Obtener el objeto: filtro X parametro.
	 *
	 * @param filtro El objeto: filtro Actual
	 * @param param El objeto: param
	 * @param value El objeto: value
	 * @return El objeto: filtro X param
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	/**
	 * Obtener el objeto: estatus.
	 *
	 * @param fchVencimiento El objeto: fch vencimiento
	 * @return El objeto: estatus
	 */
	public String getEstatus(String fchVencimiento) {
		Date fechaActual = new Date();
		if (fchVencimiento == null || fchVencimiento.isEmpty()) {
			//No se puede validar la fecha
			return "NA";
		}
		if (fechaActual.before(Utilerias.getUtilerias().cadenaToFecha(fchVencimiento, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY))) {
			//Estatus vihente
			return "VIGENTE";
		} else {
			//Estatus Vencido
			return "VENCIDO";
		}
	}
	
	/**
	 * Obtener el objeto: error.
	 * Obtiene el Bean de error en base al responseDTO
	 *
	 * @param responseDAO El objeto: response DAO, si es null se envia el codigo de error por defecto
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		//Si  responseDAO es null, se envia el codigo de error por defecto
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		//Regresa el codigo y mensaje del DAO
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
}
