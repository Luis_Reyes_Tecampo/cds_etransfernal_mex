/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMttoMediosValidar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10-13-2018 10:17:18 PM Ana Gloria Martinez Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidacionesCombo;

/**
 * Clase UtileriasLiquidezIntradiaAux.
 *
 * Clase de utilerias tratado de consultas Notificacion intradia
 * 
 * @author FSW-Vector
 * @since 10-13-2018
 */
public class UtileriasLiquidezIntradiaAux implements Serializable{
	
	/** Constante serialVersionUID. */
	private static final long serialVersionUID = -6590236674465512144L;


	/** Constante utils. */
	private static UtileriasLiquidezIntradiaAux utils;
	
	/** La constante QUERY_AUX. */
	private static final String QUERY_AUX = " AND TRIM(CL.CVE_MECANISMO) = TRIM(TM.CVE_MECANISMO) ";
	
	/** La constante FILTRO_PAGINADO. */
	private static final String FILTRO_PAGINADO = " WHERE RN BETWEEN ? AND ?   ";
	
	/** La constante CONSULTA_TODOS. */
	private static final String CONSULTA_GFI = "SELECT CVE_TRASFE,ESTATUS, CVE_OPERACION, CVE_MECANISMO, TOTAL"
			.concat(" FROM")
			.concat(" (")
			.concat(" SELECT DATOS.CVE_TRASFE,DATOS.ESTATUS, DATOS.CVE_OPERACION, DATOS.CVE_MECANISMO, DATOS.TOTAL,   ROWNUM AS RN FROM (")
			.concat(" SELECT ")
			.concat(" TRIM(CL.CVE_TRANSFE) ||' - '|| TRIM(TT.DESCRIPCION) CVE_TRASFE,")
			.concat(" TRIM(CL.CVE_OPERACION) ||' - '  CVE_OPERACION, ")
			.concat(" TRIM(CL.ESTATUS) ||' - ' || TRIM(TC.DESCRIPCION) ESTATUS,")
			.concat(" TRIM(CL.CVE_MECANISMO)  ||' - ' || TRIM(TM.DESCRIPCION) CVE_MECANISMO, ")
			.concat(" (SELECT COUNT(1) AS CONT FROM TRAN_LIQ_INTRADIA CL , TRAN_TRANSFERENC TT, TRAN_PARAMETROS TC, TRAN_MECANISMOS TM")
			.concat(" WHERE TC.CVE_PARAMETRO      LIKE '%ESTATUSTRANS%'")
			.concat(" AND TRIM(CL.CVE_TRANSFE)   = TRIM(TT.CVE_TRANSFE) ")
			.concat(" AND TRIM(CL.ESTATUS)       = TRIM(TC.CVE_PARAM_FALTA) ")
			.concat(QUERY_AUX)
			.concat(" [FILTRO_WH]") 	
			.concat(" ) TOTAL")
			.concat(" FROM TRAN_LIQ_INTRADIA CL, TRAN_TRANSFERENC TT, TRAN_PARAMETROS TC, TRAN_MECANISMOS TM")
			.concat(" WHERE TC.CVE_PARAMETRO      LIKE '%ESTATUSTRANS%'")

			.concat(" AND TRIM(CL.CVE_TRANSFE)   = TRIM(TT.CVE_TRANSFE)")
			.concat(" AND TRIM(CL.ESTATUS)       = TRIM(TC.CVE_PARAM_FALTA) ")
			.concat(QUERY_AUX)
			.concat(" [FILTRO_AND]")
			.concat(" ) DATOS")
			.concat(" ) CAT ");

	/** La constante CONSULTA_TRFPLUS. */
	private static final String CONSULTA_TRFPLUS="SELECT CVE_TRASFE, ESTATUS, CVE_MECANISMO, TOTAL"
			.concat(" FROM")
			.concat(" (")
			.concat(" SELECT DATOS.CVE_TRASFE, DATOS.ESTATUS, DATOS.CVE_MECANISMO, DATOS.TOTAL, ROWNUM AS RN FROM  (")
			.concat(" SELECT ")
			.concat(" TRIM(CL.CVE_TRANSFE) ||' - ' || TRIM(TT.TXT_DESCRIPCION) CVE_TRASFE,")
			.concat(" TRIM(CL.CVE_ESTATUS) ||' - ' || TRIM(TC.TXT_DESCRIPCION) ESTATUS, ")
			.concat(" TRIM(CL.CVE_MECANISMO) ||' - ' || TRIM(TM.TXT_DESCRIPCION) CVE_MECANISMO,")
			.concat(" (SELECT COUNT(1) AS CONT FROM TR_LIQ_INTRADIA  CL	, TR_CORE_CFG_CVETRANSFE TT, TR_CORE_CAT_ESTATUS TC,  TR_CORE_CFG_MECANISMOS TM ")
			.concat(" WHERE TRIM(CL.CVE_TRANSFE) = TRIM(TT.CVE_TRANSFE) ")
			.concat(" AND TRIM(CL.CVE_ESTATUS) = TRIM(TC.CVE_ESTATUS) ")
			.concat(QUERY_AUX)
			.concat(" [FILTRO_WH]") 	
			.concat(" ) TOTAL")
			.concat(" FROM TR_LIQ_INTRADIA CL, TR_CORE_CFG_CVETRANSFE TT, TR_CORE_CAT_ESTATUS TC,  TR_CORE_CFG_MECANISMOS TM")
			.concat(" WHERE TRIM(CL.CVE_TRANSFE) = TRIM(TT.CVE_TRANSFE)")
			.concat(" AND TRIM(CL.CVE_ESTATUS) = TRIM(TC.CVE_ESTATUS)")
			.concat(QUERY_AUX)
			.concat(" [FILTRO_AND]")
			.concat(" ) DATOS")
			.concat(" ) CAT ");
	/** La constante CONSULTA_WHERE. */
	private static final String CONSULTA_WHERE_GFI = "WHERE TRIM(CVE_TRANSFE) = UPPER(TRIM(?)) AND "
			.concat("TRIM(CVE_MECANISMO) = UPPER(TRIM(?)) AND ")
			.concat("TRIM(ESTATUS) = UPPER(TRIM(?))");
	
	/** La constante CONSULTA_WHERE. */
	private static final String CONSULTA_WHERE_TRFPLUS = "WHERE TRIM(CVE_TRANSFE) = UPPER(TRIM(?)) AND "
			.concat("TRIM(CVE_MECANISMO) = UPPER(TRIM(?)) AND ")
			.concat("TRIM(CVE_ESTATUS) = UPPER(TRIM(?))");
	/** La constante CONSULTA_PRINCIPAL. */
	private static final String CONSULTA_PRINCIPAL_GFI = CONSULTA_GFI.concat(FILTRO_PAGINADO);
	
	/** La constante CONSULTA_PRINCIPAL_GFI. */
	private static final String CONSULTA_PRINCIPAL_TRFPLUS = CONSULTA_TRFPLUS.concat(FILTRO_PAGINADO);

	
	/**La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA_GFI = "SELECT COUNT(1) CONT FROM TRAN_LIQ_INTRADIA "
			.concat(CONSULTA_WHERE_GFI);
	
	/** La constante CONSULTA_EXISTENCIA. */
	private static final String CONSULTA_EXISTENCIA_TRFPLUS = "SELECT COUNT(1) CONT FROM TR_LIQ_INTRADIA "
			.concat(CONSULTA_WHERE_TRFPLUS);
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR_GFI = "INSERT INTO TRAN_LIQ_INTRADIA "
			.concat("(CVE_TRANSFE,ESTATUS,CVE_MECANISMO,CVE_OPERACION) ")
			.concat("VALUES (TRIM(?),UPPER(TRIM(?)),UPPER(TRIM(?)),UPPER(TRIM(?)))");
	
	/** La constante CONSULTA_INSERTAR. */
	private static final String CONSULTA_INSERTAR_TRFPLUS = "INSERT INTO TR_LIQ_INTRADIA "
			.concat("(CVE_TRANSFE,CVE_ESTATUS,CVE_MECANISMO) ")
			.concat("VALUES (TRIM(?),UPPER(TRIM(?)),UPPER(TRIM(?)))");
	/** La constante CONSULTA_ELIMINAR NACIONAL. */
	private static final String CONSULTA_ELIMINAR_GFI = "DELETE FROM TRAN_LIQ_INTRADIA "
			.concat(CONSULTA_WHERE_GFI);
	/** La constante CONSULTA_ELIMINAR INTERNACIONAL. */
	private static final String CONSULTA_ELIMINAR_TRFPLUS = "DELETE FROM TR_LIQ_INTRADIA "
			.concat(CONSULTA_WHERE_TRFPLUS);
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR_GFI = "UPDATE TRAN_LIQ_INTRADIA SET "
			.concat("CVE_TRANSFE = TRIM(?), ")
			.concat("ESTATUS = UPPER(TRIM(?)), ")
			.concat("CVE_MECANISMO = UPPER(TRIM(?)) ")
			.concat(CONSULTA_WHERE_GFI);
	
	/** La constante CONSULTA_MODIFICAR. */
	private static final String CONSULTA_MODIFICAR_TRFPLUS = "UPDATE TR_LIQ_INTRADIA SET "
			.concat("CVE_TRANSFE = TRIM(?), ")
			.concat("CVE_ESTATUS = UPPER(TRIM(?)), ")
			.concat("CVE_MECANISMO = UPPER(TRIM(?)) ")
			.concat(CONSULTA_WHERE_TRFPLUS);

	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MECANISMOS_FILTRO_NA = "SELECT DISTINCT ME.CVE_MECANISMO AS CVE, ME.DESCRIPCION AS DESCRIPCION FROM TRAN_MECANISMOS ME, TRAN_LIQ_INTRADIA CL WHERE ME.CVE_MECANISMO = CL.CVE_MECANISMO [FILTER] ORDER BY ME.CVE_MECANISMO";
	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MECANISMO_FILTRO_TR = "SELECT DISTINCT TR.CVE_MECANISMO AS CVE,  TR.TXT_DESCRIPCION AS DESCRIPCION  FROM TR_CORE_CFG_MECANISMOS TR ,TR_LIQ_INTRADIA CL "
					.concat("WHERE TR.CVE_MECANISMO = CL.CVE_MECANISMO  [FILTER] "
					.concat("ORDER BY TR.CVE_MECANISMO "));
	
	 /**
	 * Obtener el objeto: instancia.
 	 *
 	 * @return  utils - instancia del objeto UtileriasLiquidezIntradia
 	 */
 	public static UtileriasLiquidezIntradiaAux getUtils() {
		/**Validacion de */ 
 		if(utils==null){
			 utils = new UtileriasLiquidezIntradiaAux();
		 }
		 /**Return utils */
		return utils;
	}

	/**
	 * Setea el objeto  utils.
	 *
	 * @param nuevo utils - Objeto UtileriasLiquidezIntradiaAux
	 */
	public static void setUtils(UtileriasLiquidezIntradiaAux utils) {
		UtileriasLiquidezIntradiaAux.utils = utils;
	}

	/**
 	 * Valida consulta principal.
 	 *
 	 * @param canal - variable que contiene valor canal para tipo de consulta
 	 * @return  string - Regresa el valor de BD a consultar
 	 */
 	public String validaConsultaPrincipal(String canal){
		 String consulta ="";
		 if("NA".equals(canal)){
			 consulta=CONSULTA_PRINCIPAL_GFI;
		 }else{
			 consulta=CONSULTA_PRINCIPAL_TRFPLUS;
		 }
		 
		 return consulta;
	 }
	 
 	/**
 	 * Valida insert.
 	 *
 	 * @param canal - variable que contiene valor canal para tipo de consulta
 	 * @return string - Regresa el valor de BD a consultar
 	 */
 	public String validaInsert(String canal){
		 String insert ="";
		 if("NA".equals(canal)){
			 insert=CONSULTA_INSERTAR_GFI;
		 }else{
			 insert=CONSULTA_INSERTAR_TRFPLUS;
		 }
		 
		 return insert;
	 }
	 
 	/**
 	 * Valida delete.
 	 *
 	 * @param canal - variable que contiene valor canal para tipo de consulta 
 	 * @return the string - Regresa el valor de BD a consultar
 	 */
 	public String validaDelete(String canal){
		 String delete ="";
		 if("NA".equals(canal)){
			 delete=CONSULTA_ELIMINAR_GFI;
		 }else{
			 delete=CONSULTA_ELIMINAR_TRFPLUS;
		 }
		 
		 return delete;
	 }
	 
 	/**
 	 * Valida update.
 	 *
 	 * @param canal - variable que contiene valor canal para tipo de consulta
 	 * @return string - Regresa el valor de BD a consultar
 	 */
 	public String validaUpdate(String canal){
		 String update ="";
		 if("NA".equals(canal)){
			 update=CONSULTA_MODIFICAR_GFI;
		 }else{
			 update=CONSULTA_MODIFICAR_TRFPLUS;
		 }
		 
		 return update;
	 }

	 /**
 	 * Valida existencia.
 	 *
 	 * @param canal - variable que contiene valor canal para tipo de consulta 
 	 * @return string - Regresa el valor de BD a consultar
 	 */
 	public String validaExistencia(String canal){
		 String existencia ="";
		 if("NA".equals(canal)){
			 existencia=CONSULTA_EXISTENCIA_GFI;
		 }else{
			 existencia=CONSULTA_EXISTENCIA_TRFPLUS;
		 }
		 
		 return existencia;
	 }
	 /**
	 * Obtener el objeto: integer.
	 *
	 * Obtiene un integer
	 * 
	 * @param obj El objeto: obj - valor a obtener
	 * @return El objeto: integer - resultado de la operacion
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
		
		/**
		 * Sets the lista.
		 *
		 * Agrega al objeto la lista en su parametro
		 * que le corresponde
		 * 
		 * @param index El objeto: index - Verifica el catalogo/combo
		 * @param listas El objeto: listas - Lista que contiene informacion del combo
		 * @param catalogo El objeto: catalogo - BeanCatalogo con informacion
		 * @return Objeto bean notificaciones combo
		 */
		public BeanLiquidacionesCombo setLista(int index, BeanLiquidacionesCombo listas, List<BeanCatalogo> catalogo) {
			if (index == 0) {
				listas.setTrasferencias(catalogo);
			}
			if (index == 1) {
				listas.setMecanismos(catalogo);
			}
			if (index == 2) {
				listas.setEstatus(catalogo);
			}
			
			
			return listas;
		}

		/**
		 * Valida consulta mecanismo filtro.
		 *
		 * @param canal -  variable que contiene valor canal para tipo de consulta 
		 * @return   string - Regresa el valor de BD a consultar
		 */
		public String validaConsultaMecanismoFiltro(String canal){
			String query= "";
			if("NA".equals(canal)){
				query=CONSULTA_MECANISMOS_FILTRO_NA;
				
			}else{
				query=CONSULTA_MECANISMO_FILTRO_TR;
			}
			return query;
		}
}
