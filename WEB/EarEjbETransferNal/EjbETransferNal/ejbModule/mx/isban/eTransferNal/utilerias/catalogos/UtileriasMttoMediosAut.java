/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * 
 * Todos los derechos reservados
 * 
 * 
 * Clase: UtileriasMttoMediosAut.java
 * 
 * Control de versiones:
 * 
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-13-2018 08:36:39 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Imports 
 * de la clase
 */
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasMttoMediosAut.
 * Clase de utilierias para obtencion de datos de entrada de consulta
 * Y salida de datos
 * 
 *
 * @author FSW-Vector
 * @since 08-13-2018
 */
public class UtileriasMttoMediosAut extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2833321871040123691L;
	
		
	/** La variable que contiene informacion con respecto a: consulta. */
	private static final String CONSULTA = "query";

	/** La constante CLAVE. */
	private static final String CLAVE = "clave";

	/** La constante DESCRIPCION. */
	private static final String DESCRIPCION = "descripcion";
	
	/** La constante DESCRIPCION_UP. */
	private static final String DESCRIPCION_UP = "DESCRIPCION";

	/** La constante VALOR_FALTA. */
	private static final String VALOR_FALTA = "VALOR_FALTA";

	/** La constante CVE_PARAM_FALTA. */
	private static final String CVE_PARAM_FALTA = "CVE_PARAM_FALTA";
	
	/** La variable que contiene informacion con respecto a: tipo. */
	private static final String TIPO = "tipo";
	
	/** Instancia  UtileriasMttoMediosAut. */
	private static UtileriasMttoMediosAut instance;
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/** La constante CONS_AND. */
	private static final String CONS_AND = " AND ";
	
	/** La constante CONS_CIERRE. */
	private static final String CONS_CIERRE = ",' ','') ";
	/**
	 * Recupera la instancia de
	 * La clase.
	 *
	 * @return UtileriasMttoMediosAut --> Objeto de acceso unico a la clase.
	 */
	public static UtileriasMttoMediosAut getInstance() {
		if (instance == null) {
			instance = new UtileriasMttoMediosAut();
		}
		return instance;
	}
	
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro --> Parametro de entrada para recuperar los filtros.
	 * @param key El objeto: key --> Parametro de entrada para recuperar la palabra reservada AND y WHERE.
	 * @return El objeto: filtro --> Objeto de tipo String que retorna el filtro encontrado.
	 */
	public String getFiltro(BeanMttoMediosAutorizados beanFiltro, String key) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getCveMedioEnt() != null && !beanFiltro.getCveMedioEnt().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_MEDIO_ENT", beanFiltro.getCveMedioEnt());
		}
		if (beanFiltro.getDesCveMed() != null && !beanFiltro.getDesCveMed().isEmpty()) {
			getFiltroXParam(filtro, key, "CM.DESCRIPCION", beanFiltro.getDesCveMed());
		}
		if (beanFiltro.getCveTransfe() != null && !beanFiltro.getCveTransfe().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_TRANSFE", beanFiltro.getCveTransfe());
		}
		if (beanFiltro.getDescTrasferencia() != null && !beanFiltro.getDescTrasferencia().isEmpty()) {
			getFiltroXParam(filtro, key, "TRNS.DESCRIPCION", beanFiltro.getDescTrasferencia());
		}
		if (beanFiltro.getCveOperacion() != null && !beanFiltro.getCveOperacion().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_OPERACION", beanFiltro.getCveOperacion());
		}
		if (beanFiltro.getDescOperacion() != null && !beanFiltro.getDescOperacion().isEmpty()) {
			getFiltroXParam(filtro, key, "TS.DESCRIPCION", beanFiltro.getDescOperacion());
		}
		if (beanFiltro.getCveCLACON() != null && !beanFiltro.getCveCLACON().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_CLACON", beanFiltro.getCveCLACON());
		}
		if (beanFiltro.getSucOperante() != null && !beanFiltro.getSucOperante().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.SUC_OPERANTE", beanFiltro.getSucOperante());
		}
		if (beanFiltro.getFlgInHab() != null && !beanFiltro.getFlgInHab().isEmpty()) {
			
			if("SI".equalsIgnoreCase(beanFiltro.getFlgInHab())) {
				getFiltroXParam(filtro, key, "TO_CHAR(TM.FLG_INHAB)", "1");
			}
			
			if("NO".equalsIgnoreCase(beanFiltro.getFlgInHab())) {
				if (first) {
					filtro.append(key);	
				}else {
					filtro.append(CONS_AND);	
				}
				filtro.append(" (TM.FLG_INHAB IS NULL OR TM.FLG_INHAB =0) ");
			}else {
				getFiltroXParam(filtro, key, "TO_CHAR(TM.FLG_INHAB)", beanFiltro.getFlgInHab());
			}		
		}
		if (beanFiltro.getCveCLACONTEF() != null && !beanFiltro.getCveCLACONTEF().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_CLACON_TEF", beanFiltro.getCveCLACONTEF());
		}
		if (beanFiltro.getCveMecanismo() != null && !beanFiltro.getCveMecanismo().isEmpty()) {
			getFiltroXParam(filtro, key, "TM.CVE_MECANISMO", beanFiltro.getCveMecanismo());
		}
		if (beanFiltro.getLimiteImporte() != null && !beanFiltro.getLimiteImporte().isEmpty()) {
			if("0.0".equals(beanFiltro.getLimiteImporte())) {
				if (first) {
					filtro.append(key);	
				}else {
					filtro.append(CONS_AND);	
				}
				filtro.append("  (TM.IMPORTE_MAX IS NULL OR TM.IMPORTE_MAX = 0) ");
			}else {			
				getFiltroXParamNumber(filtro, key, "TM.IMPORTE_MAX", beanFiltro.getLimiteImporte());
			}
		}
		if (beanFiltro.getCanal() != null && !beanFiltro.getCanal().isEmpty()) {
			String canal =beanFiltro.getCanal();
			if("SEGURO".equalsIgnoreCase(beanFiltro.getCanal())) {
				canal = "S";
			}
			if("NO SEGURO".equalsIgnoreCase(beanFiltro.getCanal())) {
				canal ="N";				
			}
			
			getFiltroXParam(filtro, key, "TM.SEGURO", canal);
		}
		
		/** Retorno de la respuesta **/
		return filtro.toString();
	}
	
	/**
	 * Obtener el objeto: filtro X param.
	 * 
	 * Valida el parametro para asignarlo en la consulta 
	 *
	 * @param filtro El objeto: filtro --> Cadena con filtros actual.
	 * @param key El objeto: key --> Palabra reservada AND or WHERE.
	 * @param param El objeto: param --> Nombre de la columna donde se hara la referencia al filtro.
	 * @param value El objeto: value --> Palabra clave de entrada para realizar el filtro.
	 * @return NA
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	
	
	/**
	 * Obtener el objeto: filtro X param Number.
	 * 
	 * Valida el parametro para asignarlo en la consulta para un campo numerico
	 *
	 * @param filtro El objeto: filtro --> Cadena con filtros actual.
	 * @param key El objeto: key --> Palabra reservada AND or WHERE.
	 * @param param El objeto: param --> Nombre de la columna donde se hara la referencia al filtro.
	 * @param value El objeto: value --> Palabra clave de entrada para realizar el filtro.
	 * @return NA
	 */
	private void getFiltroXParamNumber(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" "+param+"="+value);
			first = false;
		} else {
			filtro.append(CONS_AND+param+"="+value);
		}
	}
	
	
	/**
	 * Asignar valores.
	 *
	 * @param map El objeto: map --> mapa de la operacion ejecutada
	 * @return Objeto bean mtto medios autorizados req --> Objeto de salida con los datos obtenidos
	 */
	public BeanMttoMediosAutorizadosReq asignarValores(Map<String, Object> map) {
		//La clase de utilerias
		Utilerias utils = Utilerias.getUtilerias();
		//Nuevo objeto de respuesta
		BeanMttoMediosAutorizadosReq beanReq = new BeanMttoMediosAutorizadosReq();
		//Setea los valores del Bean
		beanReq.setCveMedioEnt(utils.getString(map.get("CVE_MEDIO_ENT")));
		beanReq.setCveTransfe(utils.getString(map.get("CVE_TRANSFE")));
		beanReq.setCveOperacion(utils.getString(map.get("CVE_OPERACION")));
		beanReq.setCveCLACON(utils.getString(map.get("CVE_CLACON")));
		/**se valida la sucurcal operativa**/
		if(utils.getString(map.get("SUC_OPERANTE")).length()>0) {
			beanReq.setSucOperante(utils.getString(map.get("SUC_OPERANTE")));
		}else {
			beanReq.setSucOperante("0");
		}
		
		beanReq.setHoraInicio(utils.getString(map.get("HORA_INICIO")));
		beanReq.setHoraCierre(utils.getString(map.get("HORA_CIERRE")));
		beanReq.setHorarioEstatus(utils.getString(map.get("CERRADO_ABIERTO")));
		/**se valida el dia inhabil**/
		if(utils.getString(map.get("FLG_INHAB")).length()>0) {
			beanReq.setFlgInHab(utils.getString(map.get("FLG_INHAB")));
		}else {
			beanReq.setFlgInHab("0");
		}
		beanReq.setCveCLACONTEF(utils.getString(map.get("CVE_CLACON_TEF")));
		beanReq.setCveMecanismo(utils.getString(map.get("CVE_MECANISMO")));
		beanReq.setDesCveMed(utils.getString(map.get("DESCRIPCION_MEDIO")));
		/**se valida el monto**/
		beanReq.setLimiteImporte(utils.getString(map.get("IMPORTE_MAX")));
		beanReq.setCanal(utils.getString(map.get("SEGURO")));				
		beanReq.setDescOperacion(utils.getString(map.get("DES_OPERACION")));
		beanReq.setDescTrasferencia(utils.getString(map.get("DES_TRANSFERENCIA")));
		//Respuesta
		return beanReq;	
	}
	
	/**
	 * Recorrer lista.
	 * 
	 * Recorre la lista y llena el 
	 * Bean de respuesta 
	 * Con los registros 
	 * de la consulta
	 *
	 * @param responseDTO El objeto: response DTO de la consulta
	 * @return Objeto list --> Lista con los valores asignados
	 */
	public List<BeanMttoMediosAutorizadosReq> recorrerLista(ResponseMessageDataBaseDTO responseDTO){
		//Nueva objeto de respuesta
		List<BeanMttoMediosAutorizadosReq> listaReq = new ArrayList<BeanMttoMediosAutorizadosReq>();
		List<HashMap<String,Object>> list = null;	
		if(responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
			list = responseDTO.getResultQuery();				
			if(!list.isEmpty()) {
				for(Map<String, Object> map : list) {
					BeanMttoMediosAutorizadosReq beanReq = new BeanMttoMediosAutorizadosReq();
					beanReq= asignarValores(map);						
					listaReq.add(beanReq);
				}
			}
		}
		//Retorna la respuesta
		return listaReq;
	}
	
	/**
	 * Recorrer lista 
	 * catalogos.
	 * La Lista se setea en el front 
	 * en los combos
	 *
	 * @param responseDTO El objeto: response DTO de la consulta
	 * @param mapa El objeto: mapa --> Resultado arrojado por la operacion
	 * @return Objeto list --> lista con valores asignados en base a la operacion
	 */
	public List<BeanCatalogo> recorrerListaCatalogos(ResponseMessageDataBaseDTO responseDTO, Map<String, String> mapa ){
		List<BeanCatalogo> listabean = Collections.emptyList();
		List<HashMap<String,Object>> list = null;	
		Utilerias utils = Utilerias.getUtilerias();
		
		if(responseDTO.getResultQuery()!= null && !responseDTO.getResultQuery().isEmpty()){
			listabean = new ArrayList<BeanCatalogo>();
			list = responseDTO.getResultQuery();
			for(HashMap<String, Object> map : list) {
				BeanCatalogo beanCatalogo = new BeanCatalogo();
				beanCatalogo.setCve(utils.getString(map.get(mapa.get(CLAVE))).trim());
				beanCatalogo.setDescripcion(utils.getString(map.get(mapa.get(DESCRIPCION))).trim());
				listabean.add(beanCatalogo);
			}
		}
		//Retorna la respuesta
		return listabean;
	}	
	
	/**
	 * Valida query.
	 * 
	 * Metodo que asiga valores de acuerdo a la consulta que se quiere ejecutar
	 *
	 * @param consulta
	 *            the consulta --> Parametro de la consulta a ejecutar
	 * @return the hash map --> Mapa de llaves de acceso para la operacion
	 */
	public Map<String, String> validaQuery(String consulta) {
		String query = "";
		HashMap<String, String> map = new HashMap<String, String>();

		/** Validacion de: CVE_MEDIO_ENT**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMCVEMEDIOSENTREGA)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_CVEMEDIOSENTREGA;
			map.put(TIPO, "1");
			map.put(CONSULTA, query);
			map.put(CLAVE, "CVE_MEDIO_ENT");
			map.put(DESCRIPCION, DESCRIPCION_UP);
		}
		/** Validacion de: CVE_TRANSFE**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMCVETRANSFERENCIA)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_CVETRANSFERENCIA;
			map.put(TIPO, "2");
			map.put(CONSULTA, query);
			map.put(CLAVE, "CVE_TRANSFE");
			map.put(DESCRIPCION, DESCRIPCION_UP);
		}
		/** Validacion de: CVE_OPERACION**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMCVEOPERACIONES)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_CVEOPERACIONES;
			map.put(TIPO, "3");
			map.put(CONSULTA, query);
			map.put(CLAVE, "CVE_OPERACION");
			map.put(DESCRIPCION, DESCRIPCION_UP);
		}
		/** Validacion de: CVE_MECANISMO**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMMECANISMOS)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_MECANISMOS;
			map.put(TIPO, "4");
			map.put(CONSULTA, query);
			map.put(CLAVE, "CVE_MECANISMO");
			map.put(DESCRIPCION, DESCRIPCION_UP);
		}
		/** Validacion de: CVE_PARAM_FALTA**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMHORARIOS)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_HORARIOS;
			map.put(TIPO, "5");
			map.put(CONSULTA, query);
			map.put(CLAVE, CVE_PARAM_FALTA);
			map.put(DESCRIPCION, VALOR_FALTA);
		}
		/** Validacion de: PARAMCANALES**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMCANALES)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_CANALES;
			map.put(TIPO, "6");
			map.put(CONSULTA, query);
			map.put(CLAVE, CVE_PARAM_FALTA);
			map.put(DESCRIPCION, VALOR_FALTA);
		}
		/** Validacion de: PARAMINHAB**/
		if (consulta.equals(ConstantesMttoMediosAut.PARAMINHAB)) {
			query = ConstantesMttoMediosAut.QUERY_LIST_INHAB;
			map.put(TIPO, "7");
			map.put(CONSULTA, query);
			map.put(CLAVE, CVE_PARAM_FALTA);
			map.put(DESCRIPCION, VALOR_FALTA);
		}
		/** Retorno de la respuesta **/
		return map;
	}
	
	/**
	 * Obtener query 
	 * exportar.
	 * 
	 *
	 * @param dto El objeto: dto --> Dato de entrada
	 * @param filtro El objeto: filtro --> Filtro a validar
	 * @return Objeto string --> Objeto tipo String de salida
	 */
	public String obtenerQueryExportar( String dto,String filtro){
		 String result="";
		//Validacion del campo cveMedioEnt
         if("cveMedioEnt".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
        	  "TRIM(REPLACE(UPPER(TM.CVE_MEDIO_ENT),' ',''))= replace("+filtro+CONS_CIERRE; 
        	//Validacion del campo descMedioEnt
         } else if("descMedioEnt".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(CM.DESCRIPCION),' ',''))= replace("+filtro+CONS_CIERRE; 
        	//Validacion del campo cveTransFer
         } else if("cveTransFer".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.CVE_TRANSFE),' ',''))= replace("+filtro+CONS_CIERRE; 
        	//Validacion del campo desTransFer
         } else if("desTransFer".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TRNS.DESCRIPCION),' ',''))= replace("+filtro+CONS_CIERRE; 
        	//Validacion del campo cveOpera
         } else	if("cveOpera".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.CVE_OPERACION),' ',''))= replace("+filtro+CONS_CIERRE; 
        	//Validacion para el filtro completo
         } else if("todo".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR ;
        	//Validacion del campo nivel
         } else {        	  
        	 if(result.length()==0) {
        		 result= queryExportar(dto,filtro);
        	 }
         }      
         /** Retorno de la respuesta **/
		return result;
	}
	
	/**
	 * Query exportar.
	 * Online
	 * 
	 * 
	 * @param dto El objeto: dto --> Dato de entrada
	 * @param filtro El objeto: filtro --> Filtro a validar
	 * @return Objeto string --> Objeto tipo String de salida
	 */
	private String queryExportar(String dto,String filtro ){
		//Comineza validaciones para 
		//Querie
		String result="";
		 if("descOpera".equals(dto)) {
			 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
		        	  "TRIM(REPLACE(UPPER(TS.DESCRIPCION),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("clacon".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.CVE_CLACON),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("sucOpera".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.SUC_OPERANTE),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("inhabil".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.FLG_INHAB),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("mecanismo".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.CVE_MECANISMO),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("importe".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.IMPORTE_MAX),' ',''))= replace("+filtro+CONS_CIERRE; 
         } else
         if("nivel".equals(dto)) {
        	 result=ConstantesMttoMediosAut.QUERY_EXPORTAR +
               	  "TRIM(REPLACE(UPPER(TM.SEGURO),' ',''))= replace("+filtro+CONS_CIERRE; 
         } 
		 /** Retorno de la respuesta **/
		 return result;
	}

}