/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasCapturasManuales.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   29/10/2018 11:26:53 AM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.dao.capturasManuales.DAOBaseCapturasManualesImpl;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasCapturasManuales.
 *
 * Clase que contienne metodos utilizados en el flujo dento la capa de acesso a los datos
 * @author FSW-Vector
 * @since 29/10/2018
 */
public class UtileriasCapturasManuales extends DAOBaseCapturasManualesImpl implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2224023795045776380L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasCapturasManuales instancia;
	
	/** La constante UTILS. */
	protected static final Utilerias UTILS = Utilerias.getUtilerias();
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static UtileriasCapturasManuales getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasCapturasManuales();
		}
		return instancia;
	}
	
	/**
	 * Asigna valores.
	 *
	 * @param operacion El objeto: operacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto list
	 */
	public List<Object> asignaValores(BeanOperacion operacion, ArchitechSessionBean architechSessionBean) {
		List<Object> valores = null;
		valores = new ArrayList<Object>();
		//Setea los valores de la operacion para el alta 
		valores.add(operacion.getIdFolio()); 								//Obtener IdFolio
		valores.add(operacion.getIdTemplate()); 							//Obtener IdTemplate
		valores.add(operacion.getIdLayout()); 								//Obtener IdLayout
		valores.add(architechSessionBean.getUsuario()); 					//Obtener Usuario
		valores.add(operacion.getCodPostalOrd()); 							//Obtener CodPostalOrd
		valores.add(operacion.getComentario1());							//Obtener Comentario1
		valores.add(operacion.getComentario2()); 							//Obtener Comentario2
		valores.add(operacion.getComentario3()); 							//Obtener Comentario3
		valores.add(operacion.getComentario4()); 							//Obtener Comentario4
		valores.add(operacion.getConceptoPago());  							//Obtener ConceptoPago
		valores.add(operacion.getConceptoPago2());							//Obtener ConceptoPago2
		valores.add(operacion.getCveDivisaOrd()); 							//Obtener CveDivisaOrd
		valores.add(operacion.getCveDivisaRec()); 							//Obtener CveDivisaRec
		valores.add(operacion.getCveEmpresa()); 							//Obtener CveEmpresa
		valores.add(operacion.getCveIntermeOrd()); 							//Obtener CveIntermeOrd
		valores.add(operacion.getCveIntermeRec()); 							//Obtener CveIntermeRec
		valores.add(operacion.getCveMedioEnt()); 							//Obtener CveMedioEnt
		valores.add(operacion.getCveOperacion()); 							//Obtener CveOperacion
		valores.add(operacion.getCvePtoVta());  							//Obtener CvePtoVta
		valores.add(operacion.getCvePtoVtaOrd()); 							//Obtener CvePtoVtaOrd
		valores.add(operacion.getCvePtoVtaRec());  							//Obtener CvePtoVtaRec
		valores.add(operacion.getCveRastreo()); 							//Obtener CveRastreo
		valores.add(operacion.getCveTransfe()); 							//Obtener CveTransfe
		valores.add(operacion.getCveUsuarioCap()); 							//Obtener CveUsuarioCap
		valores.add(operacion.getCveUsuarioSol()); 							//Obtener CveUsuarioSol
		valores.add(operacion.getDireccionIp());							//Obtener DireccionIp
		valores.add(operacion.getDomicilioOrd()); 							//Obtener DomicilioOrd
		valores.add(operacion.getFchConstitOrd()); 							//Obtener FchConstitOrd
		valores.add(operacion.getFchInstrucPago()); 						//Obtener FchInstrucPago
		valores.add(operacion.getFolioPago()); 								//Obtener FolioPago
		valores.add(operacion.getFolioPaquete());							//Obtener FolioPaquete
		valores.add(operacion.getHoraInstrucPago()); 						//Obtener HoraInstrucPago
		valores.add(operacion.getImporteAbono()); 							//Obtener ImporteAbono
		valores.add(operacion.getImporteCargo()); 							//Obtener ImporteCargo
		valores.add(operacion.getMotivoDevol()); 							//Obtener MotivoDevol
		valores.add(operacion.getNombreOrd());								//Obtener NombreOrd
		valores.add(operacion.getNombreRec()); 								//Obtener NombreRec
		valores.add(operacion.getNombreRec2()); 							//Obtener NombreRec2
		valores.add(operacion.getNumCuentaOrd()); 							//Obtener NumCuentaOrd
		valores.add(operacion.getNumCuentaRec());							//Obtener NumCuentaRec
		valores.add(operacion.getReferenciaMed()); 							//Obtener ReferenciaMed
		valores.add(operacion.getRfcOrd()); 								//Obtener RfcOrd
		valores.add(operacion.getRfcRec()); 								//Obtener RfcRec
		valores.add(operacion.getRfcRec2()); 								//Obtener RfcRec2
		valores.add(operacion.getTipoCambio()); 							//Obtener TipoCambio
		valores.add(operacion.getTipoCuentaOrd());							//Obtener TipoCuentaOrd
		valores.add(operacion.getTipoCuentaRec()); 							//Obtener TipoCuentaRec
		valores.add(operacion.getTipoCuentaRec2()); 						//Obtener TipoCuentaRec2
		valores.add(operacion.getCiudadBcoRec()); 							//Obtener CiudadBcoRec
		valores.add(operacion.getCveAba()); 								//Obtener CveAba
		valores.add(operacion.getCvePaisBcoRec());							//Obtener CvePaisBcoRec
		valores.add(operacion.getFormaLiq()); 								//Obtener FormaLiq
		valores.add(operacion.getImporteDls()); 							//Obtener ImporteDls
		valores.add(operacion.getNombreBcoRec()); 							//Obtener NombreBcoRec
		valores.add(operacion.getNumCuentaRec2()); 							//Obtener NumCuentaRec2
		valores.add(operacion.getPlazaBanxico()); 							//Obtener PlazaBanxico
		valores.add(operacion.getSucursalBcoRec());							//Obtener SucursalBcoRec
		valores.add(operacion.getBeanDatosGenerales().getBucCliente()); 	//Obtener BucCliente
		valores.add(operacion.getBeanDatosGenerales().getImporteIva()); 	//Obtener ImporteIva
		valores.add(operacion.getBeanDatosIp().getDirIpGenTran());			//Obtener DirIpGenTran
		valores.add(operacion.getBeanDatosIp().getDirIpFirma()); 			//Obtener DirIpFirma
		valores.add(operacion.getBeanDatosGenerales().getUetrSwift()); 		//Obtener UetrSwift
		valores.add(operacion.getBeanDatosGenerales().getCampoSwift1());	//Obtener CampoSwift1
		valores.add(operacion.getBeanDatosGenerales().getCampoSwift2()); 	//Obtener CampoSwift2
		valores.add(operacion.getBeanDatosGenerales().getRefeAdicional1()); //Obtener RefeAdicional1
		valores.add(operacion.getRefeNumerica());							//Obtener RefeNumerica
		/** Retorno del metodo **/
		return valores;
	}
	
	/**
	 * Valida operacion.
	 *
	 * @param list El objeto: list
	 * @param listTemplates El objeto: list templates
	 * @param listLayouts El objeto: list layouts
	 */
	public void validaOperacion(List<HashMap<String,Object>> list, List<String> listTemplates, List<String> listLayouts) {
		if(list != null){
	    	for(HashMap<String,Object> map:list){
				 String template = UTILS.getString(map.get("ID_TEMPLATE"));
				 String layout = UTILS.getString(map.get("ID_LAYOUT"));
				 if(template != null){
					 listTemplates.add(template);
				 }
				 if(layout != null){
					 listLayouts.add(layout);
				 }
			 }
	    }
	}
	
	/**
	 * Continua operacion.
	 *
	 * @param campos El objeto: campos
	 * @param list El objeto: list
	 * @param layout El objeto: layout
	 * @param architechSessionBean El objeto: architech session bean
	 */
	public void continuaOperacion(List<HashMap<String,Object>> list, List<BeanLayout> layout, ArchitechSessionBean architechSessionBean) {
		for(HashMap<String,Object> map:list){
			BeanLayout campos = new BeanLayout();
			//Nuevo campo
			campos = new BeanLayout();
			campos.setCampo(UTILS.getString(map.get("CAMPO")).toLowerCase().replace("_", StringUtils.EMPTY));
			campos.setEtiqueta(UTILS.getString(map.get("DESCRIPCION")));
			campos.setObligatorio(Integer.parseInt(UTILS.getString(map.get("OBLIGATORIO"))));
			campos.setConstante(UTILS.getString(map.get("CONSTANTE")));
			campos.setQuery(UTILS.getString(map.get("QUERY")));
			campos.setValorActual(StringUtils.EMPTY);
			//Se agrega los valores en caso de existir un query para combo
			if(null != campos.getQuery() && !StringUtils.EMPTY.equals(campos.getQuery())){
				campos.setComboValores(getComboValores(campos.getQuery(), architechSessionBean));
			}
			layout.add(campos);
		}
	}
}
