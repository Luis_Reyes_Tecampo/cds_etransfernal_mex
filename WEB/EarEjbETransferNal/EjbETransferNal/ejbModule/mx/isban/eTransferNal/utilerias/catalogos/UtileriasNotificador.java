/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasAfectacionBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:05:55 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanCatalogo;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesCombo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * Class UtileriasAfectacionBloques.
 *
 * Clase de utilerias que contiene metodos que ayudan al flujo normal
 * a realizar los procesos del negocio.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public class UtileriasNotificador implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 717891016974581046L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasNotificador instancia;
	
	/** La constante ALTA. */
	public static final String ALTA = "alta";
	
	/** La constante BAJA. */
	public static final String BAJA = "baja";
	
	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";
	
	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT TIPO_NOTIF || ',' || CVE_TRANSFER || ',' || MEDIO_ENTREGA || ',' || ESTATUS_TRANSFER FROM TRAN_MX_CAT_NOTIF [FILTRO_WH]";
	
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY TIPO_NOTIF";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "Tipo Notificador, Clave Trasferencia, Medio de Entrega, Estatus";
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/**
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return instanica: Regresa el objeto instancia cuando esta es nula
	 * 
	 */
	public static UtileriasNotificador getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasNotificador();
		}
		return instancia;
	}
	
	/**
	 * obtiene el filtro para la realización de la búsqueda de registros
	 *
	 * @param beanFiltro --> dato insertado en la caja de texto cuyo valor es tomado como referencia para realizar la consulta a la BD
	 * @param key --> Id de la caja de texto en la cual se insertó el dato.
	 * @return filtro --> regresa la cadena con el valor del filtro
	 */
	public String getFiltro(BeanNotificacion beanFiltro, String key) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getTipoNotif() != null && !beanFiltro.getTipoNotif().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "TIPO_NOTIF", beanFiltro.getTipoNotif().trim());
		}
		if (beanFiltro.getTipoNotif() != null && !beanFiltro.getCveTransfer().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "CVE_TRANSFER", beanFiltro.getCveTransfer().trim());
		}
		if (beanFiltro.getTipoNotif() != null && !beanFiltro.getMedioEntrega().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "MEDIO_ENTREGA", beanFiltro.getMedioEntrega().trim());
		}
		if (beanFiltro.getTipoNotif() != null && !beanFiltro.getEstatusTransfer().trim().isEmpty()) {
			getFiltroXParam(filtro, key, "ESTATUS_TRANSFER", beanFiltro.getEstatusTransfer().trim());
		}
		return filtro.toString();
	}
	
	/**
	 * Obtiene los filtros por cada parámetro no null
	 *
	 * @param filtro --> variable que obtiene los datos almacenados en cada caja de texto
	 * @param key --> Id de la caja de texto de la cual se obtiene el dato.
	 * @param param --> tipo de parámetro.
	 * @param value --> valor del parámetro obtenido.
	 * 
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	/**
	 * Obtiene un objeto string
	 * 
	 * @param obj --> objeto de tipo Object que obtiene registros
	 * @return cad --> variable de tipo cad que se le asigna el contenido del objeto obj
	 */
	public String getString(Object obj){
		String cad = "";
		if(obj!= null){
			cad = obj.toString().trim();
		}
		return cad;
	}
	
	/**
	 * Obtiene un integer
	 * 
	 *@param obj --> objeto de tipo Object que obtiene registros
	 * @return cad --> variable de tipo cad que se le asigna el contenido del objeto obj
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
	
	/**
	 * Obtiene mensaje de la base de datos
	 *
	 * @param responseDAO --> obtiene la respuesta del dao
	 * @return error -->Regresa el mensaje obtenido de la base de datos.
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	/**
	 * Agregar parametros.
	 *
	 * Funcion para asignar los parametros utilizados por las operaciones en el flujo
	 * 
	 * @param operacion --> variable de tipo cadena que trae la operacion que el usuario ejecuta
	 * @param beanNotificacion --> bean que obtiene la notificacion
	 * @return parametros --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	public List<Object> agregarParametros(BeanNotificacion beanNotificacion, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ALTA)) {
			parametros.add(beanNotificacion.getTipoNotif());
			parametros.add(beanNotificacion.getCveTransfer());
			parametros.add(beanNotificacion.getMedioEntrega());
			parametros.add(beanNotificacion.getEstatusTransfer());
		}
		if(operacion.equalsIgnoreCase(MODIFICACION)) {
			parametros.add(beanNotificacion.getTipoNotif());
			parametros.add(beanNotificacion.getCveTransfer());
			parametros.add(beanNotificacion.getMedioEntrega());
			parametros.add(beanNotificacion.getEstatusTransfer());
		}
		if(operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
			parametros.add(beanNotificacion.getTipoNotif());
			parametros.add(beanNotificacion.getCveTransfer());
			parametros.add(beanNotificacion.getMedioEntrega());
			parametros.add(beanNotificacion.getEstatusTransfer());
		}
		return parametros;
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para asignar los parametros anteriores en el flujo de modificar
	 *
	 * @param beanNotificacion --> bean que obtiene la notificacion
	 * @param parametros El objeto: parametros --> Parametros de entrada
	 * @return parametros --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	public List<Object> agregarParametrosOld(BeanNotificacion beanNotificacion, List<Object> parametros) {
		parametros.add(beanNotificacion.getTipoNotif());
		parametros.add(beanNotificacion.getCveTransfer());
		parametros.add(beanNotificacion.getMedioEntrega());
		parametros.add(beanNotificacion.getEstatusTransfer());
		return parametros;
	}
	
	/**
	 * Sets the lista.
	 *
	 * Agrega al objeto la lista en su parametro
	 * que le corresponde
	 * 
	 * @param index --> variable de tipo int que obtiene el id del registro
	 * @param listas --> obj de tipo BeanNotificacionesCombo que obtiene las listas de notificaciones
	 * @param catalogo --> lista de tipo obj que obtiene el listado de las notificaciones agrupadas
	 * @return Objeto bean notificaciones combo 
	 */
	public BeanNotificacionesCombo setLista(int index, BeanNotificacionesCombo listas, List<BeanCatalogo> catalogo) {
		if (index == 0) {
			listas.setNotificadores(catalogo);
		}
		if (index == 1) {
			listas.setTrasferencias(catalogo);
		}
		if (index == 2) {
			listas.setMediosEntrega(catalogo);
		}
		if (index == 3) {
			listas.setEstatus(catalogo);
		}
		return listas;
	}
	
}
