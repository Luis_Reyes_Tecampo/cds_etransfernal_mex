/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ValidadorMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:08:31 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;

/**
 * Class ValidadorMonitor.
 *
 * Clase de utilerias tipo singleton que contiene metodos
 * utilizados para los flujos
 * en las pantalles de detalle de monitor.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public final class ValidadorMonitor implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1555491726142364775L;

	/**
	 * Nueva instancia validador monitor.
	 */
	private ValidadorMonitor() {
	}

	/**
	 * Obtener el objeto: entidad.
	 *
	 * Metodo para retornar el parametro entidad 
	 * SPEI o SPID
	 * 
	 * @param modulo El objeto: modulo
	 * @return El objeto: entidad
	 */
	public static  String getEntidad(BeanModulo modulo) {
		String entidad = ConstantesPOACOAMonitor.ENTIDAD.concat(ConstantesPOACOAMonitor.GUION_BAJO);
		if (modulo.getModulo().equalsIgnoreCase(ConstantesPOACOA.PAN_SPEI)) {
			entidad = entidad.concat(ConstantesPOACOA.PAN_SPEI);
		}
		if (modulo.getModulo().equalsIgnoreCase(ConstantesPOACOA.PAN_SPID)) {
			entidad = entidad.concat(ConstantesPOACOA.PAN_SPID);
		}
		return entidad;
	}
	
	/**
	 * Checks if is SPEI.
	 *
	 * Valida que el modulo sea SPEI.
	 * 
	 * @param modulo El objeto: modulo
	 * @return Objeto boolean
	 */
	public static Boolean isSPEI(BeanModulo modulo) {
		return modulo.getModulo().equalsIgnoreCase(ConstantesPOACOA.PAN_SPEI);
	}
	
	/**
	 * Checks if is SPID.
	 *
	 * Valida que el modulo sea SPID.
	 * 
	 * @param modulo El objeto: modulo
	 * @return Objeto boolean
	 */
	public static Boolean isSPID(BeanModulo modulo) {
		return modulo.getModulo().equalsIgnoreCase(ConstantesPOACOA.PAN_SPID);
	}
	
	/**
	 * Trata query.
	 *
	 * Retorna un query parametrizada 
	 * a SPEI o SPID
	 * 
	 * @param query El objeto: query
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	public static String trataQuery (String query, BeanModulo modulo) {
		if (modulo.getModulo().equalsIgnoreCase(ConstantesPOACOA.PAN_SPEI)) {
			query = query.replaceAll(ConstantesPOACOA.PAN_SPID, ConstantesPOACOA.PAN_SPEI);
		}
		return query;
	}
	
	/**
	 * Checks if is COA.
	 *
	 * Valida si el proceso es COA.
	 * 
	 * @param modulo El objeto: modulo
	 * @return Objeto boolean
	 */
	public static Boolean isCOA(BeanModulo modulo) {
		return modulo.getTipo().equalsIgnoreCase(ConstantesPOACOA.COA);
	}
	
	/**
	 * Checks if is POA.
	 *
	 * Valida si el proceso es POA.
	 * 
	 * @param modulo El objeto: modulo
	 * @return Objeto boolean
	 */
	public static Boolean isPOA(BeanModulo modulo) {
		return modulo.getTipo().equalsIgnoreCase(ConstantesPOACOA.POA);
	}
	
	/**
	 * Cambia query.
	 *
	 * Metodo para tratar el query al modulo
	 * SPEI o SPID.
	 * 
	 * @param modulo El objeto: modulo
	 * @param query El objeto: query
	 * @return Objeto string
	 */
	public static String cambiaQuery(BeanModulo modulo, String query) {
		if (ValidadorMonitor.isSPEI(modulo)) {
			query = query.replaceAll(ConstantesPOACOA.PAN_SPID, ConstantesPOACOA.PAN_SPEI);
		}
		return query;
	}
	
	/**
	 * Obtener el objeto: origen.
	 *
	 * Retorna el parametro origen 
	 * para enviarlo a parametrizar a un query.
	 * 
	 * @param modulo El objeto: modulo
	 * @return El objeto: origen
	 */
	public static String getOrigen(BeanModulo modulo) {
		String origen = "AND TMS.ORIGEN = ";
		if (ValidadorMonitor.isCOA(modulo)) {
			origen = origen.concat("'C'");
		}
		if (ValidadorMonitor.isPOA(modulo)) {
			origen = origen.concat("'P'");
		}
		return origen;
	}
}
