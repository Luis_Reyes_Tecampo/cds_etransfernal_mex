/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasOrdenes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:54:36 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacionDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasOrdenes.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los flujos de la pantalla de Ordenes. 
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class UtileriasOrdenes implements Serializable {


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1035805443746024963L;
	
	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasOrdenes utils;
	
	/** La constante REFERENCIA. */
	private static final String REFERENCIA = "REFERENCIA";
	
	/** La constante CVE_COLA. */
	private static final String CVE_COLA = "CVE_COLA";
	
	/** La constante CVE_TRANSFE. */
	private static final String CVE_TRANSFE = "CVE_TRANSFE";
	
	/** La constante CVE_MECANISMO. */
	private static final String CVE_MECANISMO = "CVE_MECANISMO";
	
	/** La constante CVE_MEDIO_ENT. */
	private static final String CVE_MEDIO_ENT = "CVE_MEDIO_ENT";
	
	/** La constante FCH_CAPTURA. */
	private static final String FCH_CAPTURA = "FCH_CAPTURA";
	
	
	/** La constante CVE_INTERME_ORD. */
	private static final String CVE_INTERME_ORD = "CVE_INTERME_ORD";
	
	/** La constante CVE_INTERME_REC. */
	private static final String CVE_INTERME_REC = "CVE_INTERME_REC";
	
	/** La constante IMPORTE_ABONO. */
	private static final String IMPORTE_ABONO = "IMPORTE_ABONO";
	
	/** La constante ESTATUS. */
	private static final String ESTATUS = "ESTATUS";
	
	/** La constante FOLIO_PAQUETE. */
	private static final String FOLIO_PAQUETE = "FOLIO_PAQUETE";
	
	/** La constante FOLIO_PAGO. */
	private static final String FOLIO_PAGO = "FOLIO_PAGO";
	
	/** La constante COMENTARIO3. */
	private static final String COMENTARIO3 = "COMENTARIO3";
	
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils como instancia de 
	 * la clase
	 */
	public static UtileriasOrdenes getUtils() {
		if (utils == null) {
			utils = new UtileriasOrdenes();
		}
		return utils;
	}
	
	/**
	 * Setea datos.
	 *
	 * Procedimiento para recorrer el result obtenido de BD y 
	 * setearlo a sus respectivos objetos, llenando y retornando 
	 * la lista.
	 * 
	 * @param beanResOrdenReparacionDAO El objeto: bean res orden reparacion DAO
	 * @param responseDTO El objeto: response DTO
	 * @return Objeto bean res orden reparacion DAO
	 */
	public BeanResOrdenReparacionDAO seteaDatos (BeanResOrdenReparacionDAO beanResOrdenReparacionDAO,ResponseMessageDataBaseDTO responseDTO) {
		Utilerias utilerias = Utilerias.getUtilerias();
		List<HashMap<String,Object>> listDat = null;
		BeanOrdenReparacion beanReparaResDat = null;
		List<BeanOrdenReparacion> listaBeanOrdenReparaDat = new ArrayList<BeanOrdenReparacion>();
		listDat = responseDTO.getResultQuery();        		 
		 if(!listDat.isEmpty()){      			 
			 for(Map<String,Object> mapResult : listDat){
				 beanReparaResDat = new BeanOrdenReparacion();
				 beanReparaResDat.setBeanOrdenReparacionDos(new BeanOrdenReparacionDos());
				 beanReparaResDat.getBeanOrdenReparacionDos().setReferencia(Long.parseLong(""+mapResult.get(REFERENCIA)+"")); /** Set REFERENCIA **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveCola(utilerias.getString(mapResult.get(CVE_COLA))); /** Set CVE_COLA **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveTran(utilerias.getString(mapResult.get(CVE_TRANSFE))); /** Set CVE_TRANSFE **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveMecanismo(utilerias.getString(mapResult.get(CVE_MECANISMO))); /** Set CVE_MECANISMO **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveMedioEnt(utilerias.getString(mapResult.get(CVE_MEDIO_ENT))); /** Set CVE_MEDIO_ENT **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setFechaCaptura(utilerias.formateaFecha(utilerias.getDate(mapResult.get(FCH_CAPTURA)),Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY)); /** Set FCH_CAPTURA **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveIntermeOrd(utilerias.getString(mapResult.get(CVE_INTERME_ORD))); /** Set CVE_INTERME_ORD **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setCveIntermeRec(utilerias.getString(mapResult.get(CVE_INTERME_REC))); /** Set CVE_INTERME_REC **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setImporteAbono(utilerias.getString(mapResult.get(IMPORTE_ABONO))); /** Set IMPORTE_ABONO **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setEstatus(utilerias.getString(mapResult.get(ESTATUS))); /** Set ESTATUS **/
				 beanReparaResDat.setMensajeError(utilerias.getString(mapResult.get(COMENTARIO3))); /** Set COMENTARIO3 **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setFolioPaquete(utilerias.getString(mapResult.get(FOLIO_PAQUETE))); /** Set FOLIO_PAQUETE **/
				 beanReparaResDat.getBeanOrdenReparacionDos().setFoloPago(utilerias.getString(mapResult.get(FOLIO_PAGO))); /** Set FOLIO_PAGO **/
				 
				 listaBeanOrdenReparaDat.add(beanReparaResDat);
				 beanResOrdenReparacionDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
			 }
			 beanResOrdenReparacionDAO.setListBeanOrdenReparacion(listaBeanOrdenReparaDat);
		 }
		 /** Retorno de la lista **/
		 return beanResOrdenReparacionDAO;
	}
	
	/**
	 * Obtener el objeto: sort field.
	 *
	 * validacion del campo a filtrar
	 * 
	 * @param sortField El objeto: sort field como campo indicado
	 * @return El objeto: sort field
	 */
	public String getSortField(String sortField) {
		String sField = "";
		
		if ("referencia".equals(sortField)){
			sField = REFERENCIA;
		} else if ("cveCola".equals(sortField)){
			sField = CVE_COLA;
		} else if ("cveTran".equals(sortField)){
			sField = CVE_TRANSFE;
		} else if ("cveMecanismo".equals(sortField)){
			sField = CVE_MECANISMO;
		} else if ("cveMedioEnt".equals(sortField)){
			sField = CVE_MEDIO_ENT;
		} else if ("fechaCaptura".equals(sortField)){
			sField = FCH_CAPTURA;
		} else {
			sField = getSortFieldPart(sortField);
		}
		/** Retorna el valor final **/
		return sField;
	}
	
	/**
	 * Metodo parte 2 para ordenar
	 * @param sortField - campo parametros field
	 * @return string - return string
	 */
	private String getSortFieldPart(String sortField) {
		String sFieldPart = "";
		if ("cveIntermeOrd".equals(sortField)){
			sFieldPart = CVE_INTERME_ORD;
		} else if ("cveIntermeRec".equals(sortField)){
			sFieldPart = CVE_INTERME_REC;
		} else if ("importeAbono".equals(sortField)){
			sFieldPart = IMPORTE_ABONO;
		} else if ("estatus".equals(sortField)){
			sFieldPart = ESTATUS;
		} else if ("folioPaquete".equals(sortField)){
			sFieldPart = FOLIO_PAQUETE;
		} else if ("foloPago".equals(sortField)){
			sFieldPart = FOLIO_PAGO;
		} else if ("mensajeError".equals(sortField)){
			sFieldPart = COMENTARIO3;
		}
		
		return sFieldPart;
	}
}
