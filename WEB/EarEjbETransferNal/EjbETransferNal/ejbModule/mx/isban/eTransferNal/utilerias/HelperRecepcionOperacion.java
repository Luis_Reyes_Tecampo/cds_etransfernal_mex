package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTransEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMonitorBancos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMonitorBancos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOperacionApartada;

/**
 *Clase que se encarga del HelperRecepcionOperacion
**/
public class HelperRecepcionOperacion {
	/**
	 * Propiedad del tipo String que almacena el valor de SELECCIONADO
	 */
	public static final String SELECCIONADO ="SELECCIONADO";
	
	/**
	 * Propiedad del tipo String que almacena el valor de DEVOLUCION
	 */
	public static final String DEVOLUCION ="DEVOLUCION";
	/**
	 * Propiedad del tipo String que almacena el valor de RECUPERA
	 */
	public static final String RECUPERA ="RECUPERA";
	
	/**
	 * variable de utilerias
	 */
	/** La constante util. */
	public static final UtileriasHelper util = UtileriasHelper.getInstance();
	
	
	   /**Metodo privado que sirve para obtener el motivo de rechazo
     *@param respuesta String con la respuesta del servicio
     *@return String con el motivo de rechazo
     *
     */
    public String getMotivoRechazo(String respuesta){
    	String motivo = "";
    	if("TUBO0802".equals(respuesta) ||  "TUBO0818".equals(respuesta) ||
    			"TUBO0869".equals(respuesta) || util.validaTuboRespuesta(respuesta)){
    		motivo = "13";
    	}else if("TUBO2065".equals(respuesta) || "TUBO0716".equals(respuesta)){
    		motivo = "02";
    	}else if("TUBO0060".equals(respuesta) || "TUBO2063".equals(respuesta)||"TUBO0184".equals(respuesta)){
    		motivo = "03";
    	}else if("TUBO0097".equals(respuesta) || "TUBO0038".equals(respuesta)||"TUBO9037".equals(respuesta)){
    		motivo = "01";
    	}else if("TUBO9020".equals(respuesta)||"TUBO0186".equals(respuesta)){
    		motivo = "21";
    	}else if("TUBO0103".equals(respuesta)){
    		motivo = "05";
    	}else if("TUBO0904".equals(respuesta)){
    		motivo = "20";
    	}else{
    		motivo = "99";
    	}
    	return motivo;
    }
    
   
    /**
     * Metodo que permite armar la trama para obtener la cuenta
     * @param beanTranSpeiRec Bean del tipo BeanTranSpeiRec
     * @param sessionBean Objeto del tipo ArchitechSessionBean
     * @return String con la trama
     */
    public String armaTramaCuenta(BeanTranSpeiRecOper beanTranSpeiRec,ArchitechSessionBean sessionBean){
    	StringBuilder builder = new StringBuilder();
    	if("03".equals(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec())){
    		builder.append("TRANVDEB");
    		builder.append(beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec());
    	}else if("10".equals(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec())){
    		builder.append("TRANVMOV");
    		builder.append(beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec());
    	}
    	return builder.toString();
    }
    
    
 
    
    /**
     * Metodo privado que sirve para armar la trama
     * @param beanTranSpeiRec Objeto bean del tipo BeanTranSpeiRec
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @param esDevolucion que indica si es una devolucion
     * @return String con la cve de operacion
     */
    public String armarTrama(BeanTranSpeiRecOper beanTranSpeiRec,ArchitechSessionBean sessionBean,final boolean esDevolucion){
    	GeneraTramaUtils generaTramaUtils = new GeneraTramaUtils();
    	BeanTransEnvio beanTransEnvio = null;
    	String medioEnt ="SPEI";
    	if(beanTranSpeiRec.getBeanDetalle().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA) 
    			||beanTranSpeiRec.getBeanDetalle().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO)) {
    		medioEnt="SPE2";
    	}
    	beanTransEnvio = generaTramaUtils.inicializa(beanTranSpeiRec,medioEnt,sessionBean);
    	String servicio = null;

    	String bancoReceptor = "40014";

    	String var20 = null;
    	boolean flagConvVostro = false;
    	if("VOSTRO".equals(beanTranSpeiRec.getBeanDetalle().getDetEstatus().getCvePago())){
    		flagConvVostro = true;
    	}
    	if("3".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())||"6".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())||flagConvVostro
    			|| util.expresion2(beanTranSpeiRec)){
    		servicio="2";
    		beanTransEnvio.setServicio(servicio);
    		beanTransEnvio.setNumCuentaRec2(beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec2());
    		beanTransEnvio.setNombreRec2(beanTranSpeiRec.getBeanDetalle().getReceptor().getNombreRec2());
    		beanTransEnvio.setRfcRec2(beanTranSpeiRec.getBeanDetalle().getReceptor().getRfcRec2());
    		beanTransEnvio.setTipoCuentaRec2(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec2());
    		beanTranSpeiRec.getBeanDetalle().getDetEstatus().setConceptoPago2(beanTranSpeiRec.getBeanDetalle().getDetEstatus().getConceptoPago2());
    		beanTransEnvio.setComentario4(util.obtenerComentario(beanTranSpeiRec));
    		beanTransEnvio.setTipoCuentaOrd(beanTranSpeiRec.getBeanDetalle().getOrdenante().getTipoCuentaOrd());
    		beanTransEnvio.setTipoCuentaRec(beanTranSpeiRec.getBeanDetalle().getReceptor().getTipoCuentaRec());
    	}else{
    		servicio="D";
    		beanTransEnvio.setServicio(servicio);
    	}
    	beanTransEnvio.setReferenciaMed(String.valueOf(Integer.valueOf(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete())*10000
    			+Integer.valueOf(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago())));

    	if(esDevolucion){
    		generaTramaUtils.seteaDevolucion(beanTransEnvio,beanTranSpeiRec,bancoReceptor);
    	}else{
    		generaTramaUtils.seteaEnvio(beanTransEnvio, beanTranSpeiRec,bancoReceptor);
    	}

    	if("0".equals(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTipoPago())){
    		beanTransEnvio.setComentario2(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreoOri());
    	}else{
    		var20 =util.obtenerValor(beanTranSpeiRec);
    		beanTransEnvio.setComentario2(beanTranSpeiRec.getBeanDetalle().getDetalleTopo().getTopologia()+var20 +" CTA."+beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec());
    	}
    	beanTransEnvio.setComentario3(beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveRastreo());
    	
    	String trama = generaTramaUtils.armaTramaCadena(beanTransEnvio);
    	
    	if(beanTranSpeiRec.getBeanDetalle().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_DIA) 
    			||beanTranSpeiRec.getBeanDetalle().getNombrePantalla().equals(ConstantesRecepOperacionApartada.RECEPCION_OPERACION_SDO_HTO)) {
    		trama=trama.replace("BANME", "BANM2");
    	}
    	return trama;
    }


	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqTranSpeiRec Bean con el listado de objetos
	 * @param filtra String con el filtro
	 * @return List<BeanTranSpeiRec> listado con los bean seleccionados
	 */
	public List<BeanTranSpeiRecOper> filtraSelecionados(BeanReqTranSpeiRec beanReqTranSpeiRec, String filtra){
		List<BeanTranSpeiRecOper> listBeanTranSpeiRec = new ArrayList<BeanTranSpeiRecOper>();
		if(SELECCIONADO.equals(filtra)){
			listBeanTranSpeiRec.addAll(util.buscarSeleccionado(beanReqTranSpeiRec));
		}else if(DEVOLUCION.equals(filtra)){
			listBeanTranSpeiRec.addAll(util.buscarDevolucion(beanReqTranSpeiRec));
		}else if(RECUPERA.equals(filtra)){
			listBeanTranSpeiRec.addAll(util.buscarRecupera(beanReqTranSpeiRec));
		}

		return listBeanTranSpeiRec;
	}
	
	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqOrdeReparacion Objeto del tipo BeanReqOrdeReparacion
	 * @param filtra Objeto del tipo String
	 * @return listBeanOrdenReparacion
	 */
	public List<BeanOrdenReparacion> filtraSelecionadosOrdenReparacion(BeanReqOrdeReparacion beanReqOrdeReparacion, String filtra){
		List<BeanOrdenReparacion> listBeanOrdenReparacion = new ArrayList<BeanOrdenReparacion>();
		if(SELECCIONADO.equals(filtra)){
			for(BeanOrdenReparacion beanOrdenReparacion:beanReqOrdeReparacion.getListaOrdenesReparacion()){
		      	  if(beanOrdenReparacion.isSeleccionado()){
		      		listBeanOrdenReparacion.add(beanOrdenReparacion);
		      	  }
			}
		}
		
		return listBeanOrdenReparacion;
	}
	
	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqOrdeReparacion Objeto del tipo BeanReqConsReceptHist
	 * @param filtra Objeto del tipo String
	 * @return listaDatosFiltrados
	 */
	public List<BeanConsRecepHist> filtraSelecionadosConsReceptHist(BeanReqConsReceptHist beanReqOrdeReparacion, String filtra){
		List<BeanConsRecepHist> listaDatosFiltrados = new ArrayList<BeanConsRecepHist>();
		if(SELECCIONADO.equals(filtra)){
			for(BeanConsRecepHist beanOrdenReparacion:beanReqOrdeReparacion.getListaDatos()){
		      	  if(beanOrdenReparacion.getSeleccionado()){
		      		listaDatosFiltrados.add(beanOrdenReparacion);
		      	  }
			}
		}
		
		return listaDatosFiltrados;
	}

	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqConsultaRecepciones Objeto del tipo BeanReqConsultaRecepciones
	 * @param filtra Objeto del tipo String
	 * @return listaDatosFiltrados
	 */
	public List<BeanConsultaRecepciones> filtraSelecionadosConsultaRecepciones(BeanReqConsultaRecepciones beanReqConsultaRecepciones, String filtra){
		List<BeanConsultaRecepciones> listaDatosFiltrados = new ArrayList<BeanConsultaRecepciones>();
		if(SELECCIONADO.equals(filtra)){
			for(BeanConsultaRecepciones beanConsultaRecepciones:beanReqConsultaRecepciones.getListaConsultaRecepciones()){
		      	  if(beanConsultaRecepciones.getSeleccionado()){
		      		listaDatosFiltrados.add(beanConsultaRecepciones);
		      	  }
			}
		}
		
		return listaDatosFiltrados;
	}
	
	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqCancelacionPagos Objeto del tipo BeanReqCancelacionPagos
	 * @param filtra Objeto del tipo String
	 * @return listBeanCancelacionPagos
	 */
	public List<BeanCancelacionPagos> filtrarSelccionCancelacionPagos(BeanReqCancelacionPagos beanReqCancelacionPagos, String filtra){
		
		List<BeanCancelacionPagos> listBeanCancelacionPagos= new ArrayList<BeanCancelacionPagos>();
		if(SELECCIONADO.equals(filtra)){
			for(BeanCancelacionPagos beanCancelacionPagos:beanReqCancelacionPagos.getListaCancelacionPagos()){
		      	  if(beanCancelacionPagos.isSeleccionado()){
		      		listBeanCancelacionPagos.add(beanCancelacionPagos);
		      	  }
			}
		}
		
		return listBeanCancelacionPagos;
	}
	
	/**
	 * Metodo que permite filtrar si esta seleccionado
	 * @param beanReqMonitorBancos Objeto del tipo BeanReqMonitorBancos
	 * @param filtra Objeto del tipo String
	 * @return listaDatosFiltrados
	 */
	public List<BeanMonitorBancos> filtraSelecionadosMonitorBancos(BeanReqMonitorBancos beanReqMonitorBancos, String filtra){
		List<BeanMonitorBancos> listaDatosFiltrados = new ArrayList<BeanMonitorBancos>();
		if(SELECCIONADO.equals(filtra)){
			for(BeanMonitorBancos beanMonitorBancos:beanReqMonitorBancos.getListaMonitorBancos()){
		      	  if(beanMonitorBancos.getSeleccionado()){
		      		listaDatosFiltrados.add(beanMonitorBancos);
		      	  }
			}
		}
		
		return listaDatosFiltrados;
	}
	
	
}
