/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: EntramadoSeguroUtils.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   07-26-2018 04:30:00 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.wscifrado;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.utilerias.comunes.Validate;
import net.sf.json.JSONObject;

/**
 * Class EntramadoSeguroUtils.
 *
 * @author FSW-Vector
 * @since 07-26-2018
 */
public class UtileriasWSTransferCifrado extends Architech {

	/** Constante con el valor serialVersionUID. */
	private static final long serialVersionUID = -3686066798866593752L;

	/** La constante STR_ESPACIO. */
	public static final char STR_ESPACIO = ' ';

	/** La constante STR_CERO. */
	public static final char STR_CERO = '0';

	/** La constante STR_DIRECION_DER. */
	public static final char STR_DIRECION_DER = 'D';

	/** La constante STR_DIRECION_IZQ. */
	public static final char STR_DIRECION_IZQ = 'I';

	/** La constante INT_CERO. */
	public static final int INT_CERO = 0;

	/** La constante INT_UNO. */
	public static final int INT_UNO = 1;

	/** La constante INT_DOS. */
	public static final int INT_DOS = 2;

	/** La constante INT_TRES. */
	public static final int INT_TRES = 3;

	/** La constante CAMPOS. */
	public static final List<String> CAMPOS;

	/** La constante LONGITUD. */
	public static final Map<String, Integer> LONGITUD;
	
	/** La constante TIPO. */
	protected static final Map<String, String> TIPO;
	
	/** Instancia que hace referencia al archivo properties. */
	private static UtileriasWSTransferCifrado instance;

	/**
	 * Recupera la instancia del properties.
	 *
	 * @return DesentramadoUtils
	 */
	public static UtileriasWSTransferCifrado getInstance() {
		if (instance == null) {
			instance = new UtileriasWSTransferCifrado();
		}
		return instance;
	}

	/**
	 * Rellenar.
	 *
	 * @param cad
	 *            La cadena que sera completada
	 * @param lon
	 *            El numero de caracteres con los que se completara la cadena
	 *            original.
	 * @param rellenaCon
	 *            El caracter con el que se completara la cadena original.
	 * @param direccion
	 *            Indica el sentido en que se completara la cadena I = Izquierda, D
	 *            = Derecha
	 * @return the string
	 */
	public String rellenar(String cad, int lon, char rellenaCon, char direccion) {
		String cadena = cad;
		StringBuilder salida = new StringBuilder();
		char direccionFin = direccion;
		if (cadena == null) {
			cadena = StringUtils.EMPTY;
		}
		if (cadena.length() > lon) {
			return cadena.substring(INT_CERO, lon);
		}
		if (direccionFin != STR_DIRECION_IZQ && direccionFin != STR_DIRECION_DER) {
			direccionFin = STR_DIRECION_IZQ;
		}
		if (direccionFin == STR_DIRECION_DER) {
			salida.append(cadena);
		}
		for (int i = INT_CERO; i < (lon - cadena.length()); i++) {
			salida.append(rellenaCon);
		}
		if (direccionFin == STR_DIRECION_IZQ) {
			salida.append(cadena);
		}
		return salida.toString();
	}

	/**
	 * Obtener el objeto: trama.
	 *
	 * @param mqService
	 *            El objeto: mq service
	 * @param jsonObject
	 *            El objeto: json object
	 * @param separador
	 *            El objeto: separador
	 * @return El objeto: trama
	 */
	public StringBuilder getTrama(String mqService, JSONObject jsonObject, String separador) {
		StringBuilder trama = new StringBuilder().append(mqService);
		//int tamanioCadena = Integer.parseInt(jsonObject.getString(ConstantesWSCifrado.TAMANIO));
		trama.append(rellenar(jsonObject.getString(ConstantesWSCifrado.CVE_MEDIO_ENT), 		6, STR_ESPACIO, STR_DIRECION_DER))
				.append(separador)
				.append(rellenar(jsonObject.getString(ConstantesWSCifrado.TAMANIO), 		6, STR_CERO, STR_DIRECION_IZQ))
				.append(separador)
				.append(jsonObject.getString(ConstantesWSCifrado.CADENA_INI))
				.append(separador)
				.append(jsonObject.getString(ConstantesWSCifrado.CADENA))
				.append(separador);
		return trama;
	}

	/**
	 * Valida campos.
	 *
	 * @param mapTransferencia
	 *            El objeto: map transferencia
	 * @param index
	 *            El objeto: index
	 * @return true, si exitoso
	 */
	public boolean validaCampos(Map<String, String> mapTransferencia, int index) {
		int tamanio;
		boolean flag = true;
		String nomCampo = StringUtils.EMPTY;
		String tipoCampo = StringUtils.EMPTY;
		nomCampo = CAMPOS.get(index);
		if (!mapTransferencia.containsKey(nomCampo)) {
			return false;
		}
		if (mapTransferencia.get(nomCampo) == null || "".equals(mapTransferencia.get(nomCampo).trim())) {
			return false;
		}
		if (!mapTransferencia.containsKey(nomCampo)) {// Si no existe lo inicializo
			tipoCampo = TIPO.get(nomCampo);
			if (!(ConstantesWSCifrado.DECIMAL.equals(tipoCampo) || ConstantesWSCifrado.NUMERICO.equals(tipoCampo))) {
				mapTransferencia.put(nomCampo, "");
			}
		}
		tipoCampo = TIPO.get(nomCampo);
		if (index == 3) {
			tamanio = Integer.parseInt(mapTransferencia.get(CAMPOS.get(1)));
		} else {
			tamanio = LONGITUD.get(nomCampo);
		}
		if (ConstantesWSCifrado.NUMERICO.equals(tipoCampo)) {
			flag = validaEntero(mapTransferencia, nomCampo, tamanio);
		} else {
			flag = validaCadena(mapTransferencia, nomCampo, tamanio);
		}
		return flag;
	}

	/**
	 * Valida entero.
	 *
	 * @param mapTransferencia
	 *            El objeto: map transferencia
	 * @param campo
	 *            El objeto: campo
	 * @param longitud
	 *            El objeto: longitud
	 * @return true, si exitoso
	 */
	public boolean validaEntero(Map<String, String> mapTransferencia, String campo, int longitud) {
		Validate validate = new Validate();
		boolean flag = true;
		String value = StringUtils.EMPTY;
		if (!mapTransferencia.containsKey(campo)) {
			return flag;
		}
		value = mapTransferencia.get(campo);
		if (value != null && !"".equals(value.trim())) {
			flag = validate.esValidoNumero(value, "([0-9]{0,})");
			if (flag) {
				flag = validaLongitud(value, longitud);
			}
		}
		return flag;
	}

	/**
	 * Valida cadena.
	 *
	 * @param mapTransferencia
	 *            El objeto: map transferencia
	 * @param campo
	 *            El objeto: campo
	 * @param longitud
	 *            El objeto: longitud
	 * @return true, si exitoso
	 */
	private boolean validaCadena(Map<String, String> mapTransferencia, String campo, int longitud) {
		boolean flag = true;
		if (longitud == 0) {
			return flag;
		}
		String value = StringUtils.EMPTY;
		if (!mapTransferencia.containsKey(campo)) {
			return flag;
		}
		value = mapTransferencia.get(campo);
		if (value != null && !StringUtils.EMPTY.equals(value.trim())) {
			flag = validaLongitud(value, longitud);
		}
		return flag;
	}

	/**
	 * Verificar si valid retorno.
	 *
	 * @param valores
	 *            El objeto: valores
	 * @return true, si valid retorno
	 */
	public boolean isValidRetorno(final String[] valores) {
		boolean resultado = true;
		if (resultado && valores.length < 6) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[0], ConstantesWSCifrado.NUM_8)) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[1], ConstantesWSCifrado.NUM_7)) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[2], ConstantesWSCifrado.NUM_2)) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[3], ConstantesWSCifrado.NUM_30)) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[4], ConstantesWSCifrado.NUM_70)) {
			resultado = false;
		}
		if (resultado && !validaLongitud(valores[5], ConstantesWSCifrado.NUM_20)) {
			resultado = false;
		}
		return resultado;
	}

	/**
	 * Valida longitud.
	 *
	 * @param cadena
	 *            El objeto: cadena
	 * @param numero
	 *            El objeto: numero
	 * @return true, si exitoso
	 */
	public boolean validaLongitud(final String cadena, final int numero) {
		if (cadena.length() > numero) {
			return false;
		}
		return true;
	}

	static {
		List<String> campos = Arrays.asList(new String[] { 
				ConstantesWSCifrado.CVE_MEDIO_ENT, 
				ConstantesWSCifrado.TAMANIO,
				ConstantesWSCifrado.CADENA_INI, 
				ConstantesWSCifrado.CADENA });
		CAMPOS = Collections.unmodifiableList(campos);

		Map<String, Integer> longitudes = new HashMap<String, Integer>();
		longitudes.put(ConstantesWSCifrado.CVE_MEDIO_ENT, 6);
		longitudes.put(ConstantesWSCifrado.TAMANIO, 6);
		longitudes.put(ConstantesWSCifrado.CADENA_INI, 0);
		longitudes.put(ConstantesWSCifrado.CADENA, 0);
		LONGITUD = Collections.unmodifiableMap(longitudes);
		
		Map<String, String> mapTipo = new HashMap<String, String>();
		mapTipo.put(ConstantesWSCifrado.CVE_MEDIO_ENT, ConstantesWSCifrado.STRING);
		mapTipo.put(ConstantesWSCifrado.TAMANIO, ConstantesWSCifrado.NUMERICO);
		mapTipo.put(ConstantesWSCifrado.CADENA_INI, ConstantesWSCifrado.STRING);
		mapTipo.put(ConstantesWSCifrado.CADENA, ConstantesWSCifrado.STRING);
		TIPO = Collections.unmodifiableMap(mapTipo);
	}


}
