/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExcepNomBenef.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     14/10/2019 02:25:47 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;


/**
 * Class UtileriasExcepNomBenef.
 *
 * @author uabotello
 */
public final class UtileriasExcepNomBenef implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3709960265697667178L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasExcepNomBenef instancia;
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO = "SELECT NOMBRE FROM TRAN_NOMBRE_FIDEICO_EXENTOS [FILTRO_WH]";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "Nombre Beneficiario";
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia unica de la clase
	 */
	public static UtileriasExcepNomBenef getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasExcepNomBenef();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @param beanFiltro El objeto: bean filtro --> Parametro con los datos de filtrado
	 * @param key El objeto: key --> Campo de BD
	 * @return El objeto: filtro retornado en String
	 */
	public String getFiltro(BeanNomBeneficiario beanFiltro, String key) {
		filtro = new StringBuilder();
		if (beanFiltro.getNombre() != null && !beanFiltro.getNombre().isEmpty()) {
			filtro.append(" "+key+" UPPER(TRIM(NOMBRE)) LIKE UPPER('%"+beanFiltro.getNombre().trim()+"%')");
		}
		return filtro.toString();
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para agregar los parametros a la consulta
	 * que se este ejecutando.
	 *
	 * @param beanNombre El objeto: bean nombre --> con los datos
	 * @param anterior El objeto: anterior --> Dato anterior
	 * @param operacion El objeto: operacion --> Operacion realizada
	 * @return Objeto list --> lista con los parametros seteados
	 */
	public List<Object> agregarParametros(BeanNomBeneficiario beanNombre, BeanNomBeneficiario anterior, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.MODIFICACION)) {
			parametros.add(beanNombre.getNombre());
			parametros.add(anterior.getNombre());
		}
		if(operacion.equalsIgnoreCase(ConstantesCatalogos.ALTA) || 
				operacion.equalsIgnoreCase(ConstantesCatalogos.BAJA) ||
				operacion.equalsIgnoreCase(ConstantesCatalogos.BUSQUEDA)) {
			parametros.add(beanNombre.getNombre());
		}
		return parametros;
	}
}
