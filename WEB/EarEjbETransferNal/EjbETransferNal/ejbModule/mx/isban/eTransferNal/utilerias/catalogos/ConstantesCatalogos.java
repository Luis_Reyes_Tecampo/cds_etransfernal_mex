/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesCatalogos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 03:08:52 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

/**
 * Class ConstantesCatalogos.
 *
 * Clase de constantes utilizadas en el flujo de los catalogos.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public class ConstantesCatalogos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8599059493367417643L;

	/** Constante usada para insertar de datos */
	public static final String TYPE_INSERT = "INSERT";

	/** Constante usada para modificar registros */
	public static final String TYPE_UPDATE = "UPDATE";

	/** Constante para eliminar registros */
	public static final String TYPE_DELETE = "DELETE";

	/** Constante usada para exportar registros */
	public static final String TYPE_EXPORT = "EXPORT";

	/** Constante usada para la carga de pantalla */
	public static final String TYPE_UPLOAD = "UPLOAD";

	/** Estatus NA */
	public static final String NA = "NA";

	/** Estatus OK. */
	public static final String OK = "OK";

	/** Estatus NOK. */
	public static final String NOK = "NOK";

	/** Accion ALTA */
	public static final String ALTA = "alta";

	/** Accion BAJA. */
	public static final String BAJA = "baja";

	/** Accion MODIFICACION. */
	public static final String MODIFICACION = "modificacion";

	/** Accion BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
}
