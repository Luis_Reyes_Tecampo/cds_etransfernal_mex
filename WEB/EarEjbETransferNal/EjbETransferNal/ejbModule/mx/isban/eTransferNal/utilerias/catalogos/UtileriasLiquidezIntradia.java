/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasLiquidezIntradia.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10-13-2018 10:17:18 PM Ana Gloria Martinez Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;


/**
 * Clase UtileriasLiquidezIntradia.
 * Clase de utilerias tratado de consultas Notificacion intradia
 * 
 * @author FSW-Vector
 * @since 10-13-2018
 */
public class UtileriasLiquidezIntradia implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 717891016974581046L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasLiquidezIntradia instancia;
	
	/** La constante ALTA. */
	public static final String ALTA = "alta";
	
	/** La constante BAJA. */
	public static final String BAJA = "baja";
	
	/** La constante MODIFICACION. */
	public static final String MODIFICACION = "modificacion";
	
	/** La constante BUSQUEDA. */
	public static final String BUSQUEDA = "busqueda";
	 
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO_GFI = "SELECT   CVE_TRANSFE || ',' || CVE_MECANISMO || ',' || ESTATUS FROM TRAN_LIQ_INTRADIA [FILTRO_WH]";
	
	/** La constante CONSULTA_EXPORTAR_TODOS. */
	public static final String CONSULTA_EXPORTAR_TODO_TRFPLUS = "SELECT   CVE_TRANSFE || ',' || CVE_MECANISMO || ',' || CVE_ESTATUS FROM TR_LIQ_INTRADIA [FILTRO_WH]";
	/** La constante ORDEN. */
	public static final String ORDEN = " ORDER BY CVE_TRANSFE";
	
	/** La constante COLUMNAS_REPORTE. */
	public static final String COLUMNAS_REPORTE  = "Clave Trasferencia, Mecanismo, Estatus";
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private StringBuilder filtro = new StringBuilder();
	/** La variable que contiene informacion con respecto a: first. */
	private boolean first = true;
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia del objeto UtileriasLiquidezIntradia
	 */
	public static UtileriasLiquidezIntradia getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasLiquidezIntradia();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 * 
	 * Obtiene el filtro ingresado en la pantalla
	 *
	 * @param beanFiltro El objeto: bean filtro - contiene los campos para realizar el filtro
	 * @param key El objeto: key - Objeto para validar el tipo de filtro
	 * @param canal El objeto: canal - Contiene el canal al que se realiza la consulta 
	 * @return El objeto: filtro - Resultado con el filtro
	 */
	public String getFiltro(BeanLiquidezIntradia beanFiltro, String key, String canal) {
		filtro = new StringBuilder();
		first = true;
		if (beanFiltro.getCveTransFe() != null && !beanFiltro.getCveTransFe().isEmpty()) {
			getFiltroXParam(filtro, key, "CL.CVE_TRANSFE", beanFiltro.getCveTransFe());
		}
		if (beanFiltro.getEstatus() != null && !beanFiltro.getEstatus().isEmpty()) {
			
			if("NA".equals(canal)) {
				getFiltroXParam(filtro, key, "CL.ESTATUS",  beanFiltro.getEstatus());
			}else {
				getFiltroXParam(filtro, key, "CL.CVE_ESTATUS", beanFiltro.getEstatus());
			}
		 }
		if (beanFiltro.getCveMecanismo() != null && !beanFiltro.getCveMecanismo().isEmpty()) {
			getFiltroXParam(filtro, key, "CL.CVE_MECANISMO", beanFiltro.getCveMecanismo());
		}
		return filtro.toString();
	}
	
	/**
	 * Obtener el objeto: filtro X param.
	 *
	 * Valida el parametro para asignarlo en la consulta 
	 * 
	 * @param filtro El objeto: filtro bean - contiene los campos para realizar el filtro
	 * @param key El objeto: key - Objeto para validar el tipo de filtro
	 * @param param El objeto: param - Valor del nombre del parametro a filtrar
	 * @param value El objeto: value - Valor del parametro a filtrar
	 * @return El objeto: filtro X param - EL resultado paea filtrar
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {
		if (first) {
			filtro.append(key+" UPPER("+param+") LIKE UPPER('%"+value+"%')");
			first = false;
		} else {
			filtro.append(" AND UPPER("+param+") LIKE UPPER('%"+value+"%')");
		}
	}
	
	/**
	 * Obtener el objeto: string.
	 *
	 * Obtiene un string
	 * 
	 * @param obj El objeto: obj - objeto a convertir
	 * @return El objeto: string - Resultado de la conversion
	 */
	public String getString(Object obj){
		String cad = "";
		if(obj!= null){
			cad = obj.toString().trim();
		}
		return cad;
	}
	
	/**
	 * Obtener el objeto: integer.
	 *
	 * Obtiene un integer
	 * 
	 * @param obj El objeto: obj - valor a validar
	 * @return El objeto: integer - valor del resultado
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
	
	/**
	 * Obtener el objeto: error.
	 *
	 * Obtiene el error
	 *
	 * @param responseDAO El objeto: response DAO
	 * @return El objeto: error - Bean que contiene el codigo y el mensaje de error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para asignar los parametros utilizados por las operaciones en el flujo
	 *
	 * @param beanLiquidez objeto beanliquidez - objeto que contiene informacion de los parametros
	 * @param operacion El objeto: operacion - Contiene la variable operacion
	 * @return Objeto list - Lista de paramatros
	 */
	public List<Object> agregarParametros(BeanLiquidezIntradia beanLiquidez, String operacion) {
		List<Object> parametros = new ArrayList<Object>();
		if(operacion.equalsIgnoreCase(ALTA)) {
 			parametros.add(beanLiquidez.getCveTransFe());
			parametros.add(beanLiquidez.getEstatus());
			parametros.add(beanLiquidez.getCveMecanismo());
			
 		}
		if(operacion.equalsIgnoreCase(MODIFICACION)) {
 			parametros.add(beanLiquidez.getCveTransFe());
 			parametros.add(beanLiquidez.getEstatus());
			parametros.add(beanLiquidez.getCveMecanismo());
 		}
		if(operacion.equalsIgnoreCase(BAJA) || operacion.equalsIgnoreCase(BUSQUEDA)) {
 			parametros.add(beanLiquidez.getCveTransFe());
			parametros.add(beanLiquidez.getCveMecanismo());
			parametros.add(beanLiquidez.getEstatus());
 		}
		return parametros;
	}
	
	/**
	 * Agregar parametros.
	 * 
	 * Funcion para asignar los parametros anteriores en el flujo de modificar
	 *
	 * @param beanLiquidez El objeto  bean liquidez - objeto que contiene informacion de los parametros
	 * @param parametros El objeto: parametros - Lista de parametros
	 * @return Objeto list - Lista de paramatros
	 */
	public List<Object> agregarParametrosOld(BeanLiquidezIntradia beanLiquidez, List<Object> parametros) {
 		parametros.add(beanLiquidez.getCveTransFe());
		parametros.add(beanLiquidez.getCveMecanismo());
		parametros.add(beanLiquidez.getEstatus());
 		return parametros;
	}
	

	
}
