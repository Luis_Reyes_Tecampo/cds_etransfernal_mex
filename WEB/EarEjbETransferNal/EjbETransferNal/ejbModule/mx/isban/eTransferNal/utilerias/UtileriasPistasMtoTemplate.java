/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPistasMtoTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Thu Apr 06 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.utilerias;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayoutTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;

/**
 * Clase Utileria para uso de pistas de auditoria.
 */
public class UtileriasPistasMtoTemplate {
	
	/**  Constante privada de: Atributo Template. */
	private static final String TEMP = "TEMP";
	
	/**  Constante privada de: Atributo Layout. */
	private static final String LAY = "LAY";
	
	/**  Constante privada de: Atributo Estatus. */
	private static final String EST = "EST";
	
	/**  Constante privada de: Atributo campo. */
	private static final String CAM = "CAM";
	
	/**  Constante privada de: Atributo Obligatorio. */
	private static final String OB = "OB";
	
	/**  Constante privada de: Atributo Constante. */
	private static final String CONS = "CONS";
	
	
	/**
	 * Metodo que permite obtener los atributos de la clase template con sus valores.
	 *
	 * @param template objeto con las propiedades
	 * @return String con la informacion del template
	 */
	// Metodo que permite obtener los atributos de la clase template con sus valores.
	public String obtenerDatosTemplate(BeanTemplate template){
		Map<String, Object> mapa = new HashMap<String, Object>();		
		mapa.put(TEMP, template.getIdTemplate());
		mapa.put(LAY, template.getIdLayout());
		mapa.put(EST, template.getEstatus());
		return convertirString(mapa);
	}
	
	/**
	 * Metodo que permite comparar la informacion de la tabla de campos, y devuelve un string de diferencias.
	 *
	 * @param listaCamposBD con los atributos de origen
	 * @param listaCamposFinal con los atributos finales
	 * @return Map<String, String>  con las diferencias finales
	 */
	// Metodo que permite comparar la informacion de la tabla de campos, y devuelve un string de diferencias.
	public Map<String, Object> compararCamposObtenerDiferentes(List<BeanCampoLayoutTemplate> listaCamposBD,
			List<BeanCampoLayoutTemplate> listaCamposFinal){
		
		Map<String, Object> temp = new HashMap<String, Object>();
		Map<String, Map<String, Object>> inicialMap = new HashMap<String, Map<String, Object>>();
		Map<String, Map<String, Object>> finalMap = new HashMap<String, Map<String, Object>>();
		
		Map<String, Object> resultado = new HashMap<String, Object>();
		Boolean cambios = false;
		StringBuilder tempString1 = new StringBuilder();
		StringBuilder tempString2 = new StringBuilder();
		Integer index=1;
		
		//inicial y final son objetos de pistas: Inicial = BD, Final = Usuario
		resultado.put("inicial", "");
		resultado.put("final", "");
		
		//A. Convertir a HashMap la primer lista
		for (BeanCampoLayoutTemplate campo : listaCamposBD) {
			temp = new HashMap<String, Object>();
			temp.put(CAM, campo.getCampo());
			
			temp.put(OB, '0');
			temp.put(CONS, "");
			
			//se obtiene el campo obligatorio
			if(campo.getObligatorio()!=null){
				temp.put(OB, campo.getObligatorio());
			}
			//se obtiene el nombre de campo
			if(campo.getConstante()!=null){
				temp.put(CONS, campo.getConstante());
			}
			
			inicialMap.put(campo.getCampo(), temp);
		}
		
		//B. Convertir a HashMap segunda lista
		for (BeanCampoLayoutTemplate campo : listaCamposFinal) {
			temp = new HashMap<String, Object>();
			temp.put(CAM, campo.getCampo());
			
			temp.put(OB, '0');
			temp.put(CONS, "");
			
			// llamada a metodo obtnerCampoObligatorio()
			obtnerCampoObligatorio(temp, campo);
			
			finalMap.put(campo.getCampo(), temp);
		}
		
		//Verificar si se generaron cambios en los campos: CONS y OB
		for(Map.Entry<String, Map<String, Object>> entry : inicialMap.entrySet()){
			String key= entry.getKey();
			Map<String, Object> filaBD = entry.getValue();
			Map<String, Object> filaView = finalMap.get(key);
			
			//Si existe la fila 
			if(filaView!=null && (!filaBD.get(CONS).equals(filaView.get(CONS)) || 
					!filaBD.get(OB).equals(filaView.get(OB))) ){
					
					cambios=true;
					//se concatena filaBD
					tempString1.append(convertirString(filaBD));
					//se concatena filaView
					tempString2.append(convertirString(filaView));
					
					//concatenar pipe como separadores
					if(index<inicialMap.size()){
						tempString1.append('|');
						tempString2.append('|');
					}
					//se agrega el resultado
					resultado.put("inicial", tempString1.toString());
					resultado.put("final", tempString2.toString());
			}
			index++;
		}
		
		resultado.put("cambios", cambios);
		return resultado;
	}
	
	
	/**
	 * Metodo que permite obtener informacion de los usuarios asociados al template.
	 *
	 * @param listaUsuarios con la informacion del template
	 * @return String con la informacion de los usuarios que autorizan el template
	 */
	// Metodo que permite obtener informacion de los usuarios asociados al template.
	public String obtenerDatosUsuariosTemplate(List<BeanUsuarioAutorizaTemplate> listaUsuarios){
		Map<String, Object> mapa = new HashMap<String, Object>();
		StringBuilder resultado=new StringBuilder();
		Integer index=1;
		
		for(BeanUsuarioAutorizaTemplate usuario: listaUsuarios){
			
			mapa.put("AUT", usuario.getUsrAutoriza());
			resultado.append(convertirString(mapa));
			if(index<listaUsuarios.size()){
				resultado.append('|'); 
			}
			index++;
		}
		return resultado.toString();
	}

	/**
	 * Metodo que permite obtener los campos de un template.
	 *
	 * @param listaCampos que contienen la informacion de los campos
	 * @return String con la informacion de los campos
	 */
	//Metodo que permite obtener los campos de un template.
	public String obtenerDatosCamposTemplate(List<BeanCampoLayoutTemplate> listaCampos){
		Map<String, Object> mapa = new HashMap<String, Object>();
		StringBuilder resultado=new StringBuilder();
		Integer index=1;
		
		for(BeanCampoLayoutTemplate campo: listaCampos){
			mapa.put(CAM, campo.getCampo());
			
			mapa.put(OB, '0');
			mapa.put(CONS, "");
			
			if(campo.getObligatorio()!=null){
				mapa.put(OB, campo.getObligatorio());
			}
			if(campo.getConstante()!=null){
				mapa.put(CONS, campo.getConstante());
			}		
			resultado.append(convertirString(mapa));
			if(index<listaCampos.size()){
				resultado.append('|'); 
			}
			index++;
		}
		return resultado.toString();
	}
	
	/**
	 * Metodo que permite convertir una estructura Hash a string con key, value.
	 *
	 * @param map con los valores a convertir
	 * @return String con los atributos y valores
	 */
	// Metodo que permite convertir una estructura Hash a string con key, value.
	public String convertirString(Map<String, Object> map){
		StringBuilder resultado=new StringBuilder();
		Integer index= 1;
		
		for(Map.Entry<String, Object> entry : map.entrySet()){
			resultado.append(entry.getKey());
			resultado.append('=');
			resultado.append(entry.getValue());
			if(index<map.size()){
				resultado.append(',');
			}
			index++;
		}
		return resultado.toString();
	}
	
	/**
	 * Obtner obligatorio constante.
	 *
	 * @param temp the temp
	 * @param campo the campo
	 */
	//metodo que obtiene el campo obligatorio y constante
	private void obtnerCampoObligatorio(Map<String, Object> temp, BeanCampoLayoutTemplate campo){
		//se determina si el campo es obligatorio
		if(campo.getObligatorio()!=null){
			temp.put(OB, campo.getObligatorio());
		}
		//se obtiene el nombre del campo
		if(campo.getConstante()!=null){
			temp.put(CONS, campo.getConstante());
		}
	}
}