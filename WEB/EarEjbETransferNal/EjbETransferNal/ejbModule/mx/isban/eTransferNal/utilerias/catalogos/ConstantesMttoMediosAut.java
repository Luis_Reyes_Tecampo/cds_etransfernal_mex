/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesMttoMediosAut.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-13-2018 08:23:04 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

/**
 * Class ConstantesMttoMediosAut.
 *
 * Clase de contasntes utilizadas por la capa de Acceso a los datos
 * del aplicativo para el catalogo de Medios Autorizados.
 * 
 * @author FSW-Vector
 * @since 08-13-2018
 */
public final class ConstantesMttoMediosAut {
	
	/** La constante TXT_ALL. */
	public static final String TXT_TODO = "todo";
	
	/** La constante TXT_INHABIL. */
	public static final String TXT_INHABIL = "inhabil";
	
	/** La constante COLUMNAS_CONSULTA_CAPTURAS_MANUALES. */
	public static final String COLUMNAS_CONSULTA_CAPTURAS_MANUALES =" CVE_MEDIO_ENT, CVE_TRANSFE, CVE_OPERACION, CVE_CLACON, SUC_OPERANTE, HORA_INICIO, HORA_CIERRE, CERRADO_ABIERTO, "
			 +"FLG_INHAB, CVE_CLACON_TEF, CVE_MECANISMO,DESCRIPCION_MEDIO, SEGURO, IMPORTE_MAX, DES_OPERACION ,DES_TRANSFERENCIA ";
	
	/** The Constant QUERY_ORDER_BY. */
	public static final String QUERY_ORDER_BY = " ORDER BY T.CVE_MEDIO_ENT";
	
	/** La constante QUERY_FILTROS. */
	public static final String QUERY_FILTROS =	"FROM TRAN_MEDIO_AUT TM  " + 
			"LEFT JOIN COMU_MEDIOS_ENT CM ON TM.CVE_MEDIO_ENT = CM.CVE_MEDIO_ENT " + 
			"LEFT JOIN TRAN_CVE_OPER TS ON TS.CVE_OPERACION=TM.CVE_OPERACION  "
			+ " LEFT JOIN TRAN_TRANSFERENC TRNS ON TRNS.CVE_TRANSFE= TM.CVE_TRANSFE ";
	
	/** La constante QUERY. */
	public static final String QUERY = "SELECT TM.CVE_MEDIO_ENT, TM.CVE_TRANSFE, TM.CVE_OPERACION, TM.CVE_CLACON, " + 
			"TM.SUC_OPERANTE, TM.HORA_INICIO, TM.HORA_CIERRE, TM.CERRADO_ABIERTO, " + 
			"TM.FLG_INHAB, TM.CVE_CLACON_TEF, TM.CVE_MECANISMO, NVL(CM.DESCRIPCION,' ') DESCRIPCION_MEDIO, " + 
			"TM.SEGURO, TO_CHAR(TM.IMPORTE_MAX, '999,999,999,999,999,990.00') AS IMPORTE_MAX, NVL(TS.DESCRIPCION,' ') DES_OPERACION ,NVL(TRNS.DESCRIPCION,' ') DES_TRANSFERENCIA " + 
			QUERY_FILTROS;
			
	/** La constante QUERY_PRINCIPAL. */
	public static final String QUERY_PRINCIPAL = "SELECT T.* FROM (SELECT T.*, ROWNUM R, COUNT(*) OVER() CONT FROM (" +
			QUERY;
	
	/** La constante QUERY_FIN. */
	public static final String QUERY_FIN = " ) T)T WHERE R BETWEEN ? AND ? " + QUERY_ORDER_BY;
	
	/** La constante QUERY_FIN_TOTAL. */
	public static final String QUERY_FIN_TOTAL = " ) T)T ";
	 
	/** La constante QUERY_FILTROS_CONSULTA. */
	public static final String QUERY_FILTROS_CONSULTA =" TRIM(TM.CVE_MEDIO_ENT) = ? "
			+" AND TRIM(TM.CVE_TRANSFE)  = ? "
			+" AND TRIM(TM.CVE_OPERACION)  = ? "
			+" AND TRIM(TM.CVE_MECANISMO) = ? " ;
	
	
	/** La constante WHERE. */
	public static final String WHERE = " WHERE ";
	
	/** La constante QUERY_ID_CONSULTA. */
	public static final String QUERY_ID_CONSULTA = QUERY + WHERE + QUERY_FILTROS_CONSULTA;
	
	/** La constante CVEMEDIO. */
	public static final String CVEMEDIO = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_MEDIO_ENT),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante DESMEDIO. */
	public static final String DESMEDIO = WHERE+" TRIM(REPLACE(UPPER(CM.DESCRIPCION),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante CVETRANF. */
	public static final String CVETRANF = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_TRANSFE),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante DESCTRANF. */
	public static final String DESCTRANF = WHERE+" TRIM(REPLACE(UPPER(TRNS.DESCRIPCION),' ','')) LIKE ? ";
	
	/** La constante CVEOPERA. */
	public static final String CVEOPERA = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_OPERACION),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante DESCOPERA. */
	public static final String DESCOPERA = WHERE+" TRIM(REPLACE(UPPER(TS.DESCRIPCION),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante CLACON. */
	public static final String CLACON = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_CLACON),' ','')) LIKE replace(?,' ','')";
	
	/** La constante SUCURSAL. */
	public static final String SUCURSAL = WHERE+" TRIM(REPLACE(UPPER(TM.SUC_OPERANTE),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante INHABIL. */
	public static final String INHABIL = WHERE+" TRIM(REPLACE(UPPER(TM.FLG_INHAB),' ','')) LIKE replace(?,' ','')";
	
	/** La constante CLACONTEF. */
	public static final String CLACONTEF = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_CLACON_TEF),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante MECANISMO. */
	public static final String MECANISMO = WHERE+" TRIM(REPLACE(UPPER(TM.CVE_MECANISMO),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante IMPORTE. */
	public static final String IMPORTE = WHERE+" TRIM(REPLACE(UPPER(TM.IMPORTE_MAX),' ','')) LIKE replace(?,' ','') ";
	
	/** La constante NIVEL. */
	public static final String NIVEL = WHERE+" TRIM(REPLACE(UPPER(TM.SEGURO),' ','')) LIKE replace(?,' ','')";
	
	/** La constante QUERY_CONS_TABLA_MA_PARAM. */
	public static final String QUERY_CONS_TABLA_MA_PARAM = QUERY_PRINCIPAL + QUERY_FIN  ;	
	
	/** La constante QUERY_FILTRO_CVEMEDIO. */
	public static final String QUERY_FILTRO_CVEMEDIO = QUERY_PRINCIPAL+CVEMEDIO+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_DESMEDIO. */
	public static final String QUERY_FILTRO_DESMEDIO = QUERY_PRINCIPAL+DESMEDIO+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_CVETRANF. */
	public static final String QUERY_FILTRO_CVETRANF = QUERY_PRINCIPAL+CVETRANF+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_DESCTRANF. */
	public static final String QUERY_FILTRO_DESCTRANF = QUERY_PRINCIPAL+DESCTRANF+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_CVEOPERA. */
	public static final String QUERY_FILTRO_CVEOPERA = QUERY_PRINCIPAL+CVEOPERA + QUERY_FIN;
	
	/** La constante QUERY_FILTRO_DESCOPERA. */
	public static final String QUERY_FILTRO_DESCOPERA = QUERY_PRINCIPAL+DESCOPERA+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_CLACON. */
	public static final String QUERY_FILTRO_CLACON = QUERY_PRINCIPAL+CLACON + QUERY_FIN;
	
	/** La constante QUERY_FILTRO_SUCURSAL. */
	public static final String QUERY_FILTRO_SUCURSAL = QUERY_PRINCIPAL+SUCURSAL+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_INHABIL. */
	public static final String QUERY_FILTRO_INHABIL = QUERY_PRINCIPAL+INHABIL+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_CLACONTEF. */
	public static final String QUERY_FILTRO_CLACONTEF = QUERY_PRINCIPAL+CLACONTEF+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_MECANISMO. */
	public static final String QUERY_FILTRO_MECANISMO = QUERY_PRINCIPAL+MECANISMO + QUERY_FIN;
	
	/** La constante QUERY_FILTRO_IMPORTE. */
	public static final String QUERY_FILTRO_IMPORTE = QUERY_PRINCIPAL+IMPORTE+ QUERY_FIN;
	
	/** La constante QUERY_FILTRO_NIVEL. */
	public static final String QUERY_FILTRO_NIVEL = QUERY_PRINCIPAL+NIVEL+ QUERY_FIN;
	
	/** La constante QUERY_TOTAL_INI. */
	public static final String QUERY_TOTAL_INI=" SELECT COUNT(1) CONT FROM( ";
	
	/** La constante QUERY_TOTAL_FIN. */
	public static final String QUERY_TOTAL_FIN=")";
			
	/** La constante QUERY_TOTAL. */
	public static final String QUERY_TOTAL=QUERY_TOTAL_INI+ QUERY_PRINCIPAL + QUERY_FIN_TOTAL  + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_CVEMEDIO. */
	public static final String QUERY_TOTAL_CVEMEDIO =QUERY_TOTAL_INI+ QUERY+CVEMEDIO + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_DESMEDIO. */
	public static final String QUERY_TOTAL_DESMEDIO=QUERY_TOTAL_INI+ QUERY+DESMEDIO + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_CVETRANF. */
	public static final String QUERY_TOTAL_CVETRANF =QUERY_TOTAL_INI+ QUERY +CVETRANF+ QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_DESCTRANF. */
	public static final String QUERY_TOTAL_DESCTRANF = QUERY_TOTAL_INI+QUERY+DESCTRANF+ QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_CVEOPERA. */
	public static final String QUERY_TOTAL_CVEOPERA =QUERY_TOTAL_INI+ QUERY +CVEOPERA+QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_DESCOPERA. */
	public static final String QUERY_TOTAL_DESCOPERA =QUERY_TOTAL_INI+ QUERY+DESCOPERA + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_CLACON. */
	public static final String QUERY_TOTAL_CLACON =QUERY_TOTAL_INI+ QUERY +CLACON + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_SUCURSAL. */
	public static final String QUERY_TOTAL_SUCURSAL =QUERY_TOTAL_INI+ QUERY +SUCURSAL+ QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_INHABIL. */
	public static final String QUERY_TOTAL_INHABIL =QUERY_TOTAL_INI+ QUERY +INHABIL+QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_CLACONTEF. */
	public static final String QUERY_TOTAL_CLACONTEF =QUERY_TOTAL_INI+ QUERY +CLACONTEF+QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_MECANISMO. */
	public static final String QUERY_TOTAL_MECANISMO =QUERY_TOTAL_INI+ QUERY +MECANISMO+ QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_IMPORTE. */
	public static final String QUERY_TOTAL_IMPORTE =QUERY_TOTAL_INI+ QUERY +IMPORTE+ QUERY_TOTAL_FIN;
	
	/** La constante QUERY_TOTAL_NIVEL. */
	public static final String QUERY_TOTAL_NIVEL =QUERY_TOTAL_INI+ QUERY+NIVEL + QUERY_TOTAL_FIN;
	
	/** La constante QUERY_LIST_CVEMEDIOSENTREGA. */
	public static final String QUERY_LIST_CVEMEDIOSENTREGA = "SELECT CVE_MEDIO_ENT,DESCRIPCION FROM COMU_MEDIOS_ENT"
															+ " ORDER BY CVE_MEDIO_ENT";
	
	/** La constante QUERY_LIST_CVETRANSFERENCIA. */
	public static final String QUERY_LIST_CVETRANSFERENCIA = "SELECT CVE_TRANSFE, DESCRIPCION FROM TRAN_TRANSFERENC"
														    + " ORDER BY CVE_TRANSFE";
	
	/** La constante QUERY_LIST_CVEOPERACIONES. */
	public static final String QUERY_LIST_CVEOPERACIONES = "SELECT CVE_OPERACION, DESCRIPCION FROM TRAN_CVE_OPER"
			                                              + " ORDER BY  CVE_OPERACION";
	
	/** La constante QUERY_LIST_MECANISMOS. */
	public static final String QUERY_LIST_MECANISMOS = "SELECT CVE_MECANISMO, DESCRIPCION FROM TRAN_MECANISMOS"
													  + " ORDER BY CVE_MECANISMO";
	
	/** La constante QUERY_LIST_HORARIOS. */
	public static final String ORDERBY_FALTA = " ORDER BY CVE_PARAM_FALTA ";
	
	/** La constante QUERY_LIST_HORARIOS. */
	public static final String QUERY_LIST_HORARIOS = "SELECT CVE_PARAM_FALTA, VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%HRMEDAUT%'"
													+ ORDERBY_FALTA;
	
	/** La constante QUERY_LIST_INHAB. */
	public static final String QUERY_LIST_INHAB = "SELECT CVE_PARAM_FALTA, VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%OPERAINHAB%'"
												 + ORDERBY_FALTA;
	
	/** La constante QUERY_LIST_CANALES. */
	public static final String QUERY_LIST_CANALES = "SELECT CVE_PARAM_FALTA, VALOR_FALTA FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%NVLSEG%'"
												   +ORDERBY_FALTA;

	/** La constante PARAMCVEMEDIOSENTREGA. */
	public static final String PARAMCVEMEDIOSENTREGA = "CVE_MEDIO_ENT";
	
	/** La constante PARAMCVETRANSFERENCIA. */
	public static final String PARAMCVETRANSFERENCIA = "CVE_TRANSFE";
	
	/** La constante PARAMCVEOPERACIONES. */
	public static final String PARAMCVEOPERACIONES = "CVE_OPERACION";
	
	/** La constante PARAMMECANISMOS. */
	public static final String PARAMMECANISMOS = "CVE_MECANISMO";
	
	/** La constante PARAMHORARIOS. */
	public static final String PARAMHORARIOS = "CVE_HORARIO";
	
	/** La constante PARAMCANALES. */
	public static final String PARAMCANALES = "CVE_CANAL";
	
	/** La constante PARAMINHAB. */
	public static final String PARAMINHAB = "CVE_INHAB";
	
	/** La constante QUERY_EXIST. */
	public static final String QUERY_EXIST = "SELECT COUNT(1) CONT FROM TRAN_MEDIO_AUT TM " + WHERE +QUERY_FILTROS_CONSULTA;
	
	/** La constante QUERY_UPDATE. */
	public static final String QUERY_UPDATE = "UPDATE TRAN_MEDIO_AUT TM SET  TM.CVE_CLACON=?, TM.FLG_INHAB = ?, TM.CVE_CLACON_TEF = ?,"
			+ " TM.IMPORTE_MAX = ?, TM.SEGURO =? , TM.SUC_OPERANTE = ?, TM.HORA_INICIO = ?, TM.HORA_CIERRE = ?, TM.CERRADO_ABIERTO = ? "
			+ WHERE + QUERY_FILTROS_CONSULTA ;
	
	/** La constante QUERY_DELETE. */
	public static final String QUERY_DELETE = "DELETE FROM TRAN_MEDIO_AUT TM "+ WHERE + QUERY_FILTROS_CONSULTA ;
	
	/** La constante TYPE_INSERT. */
	public static final String TYPE_INSERT = "INSERT";
	
	/** La constante TYPE_UPDATE. */
	public static final String TYPE_UPDATE = "UPDATE";
	
	/** La constante TYPE_SELECT. */
	public static final String TYPE_SELECT = "SELECT";
	
	/** La constante TYPE_DELETE. */
	public static final String TYPE_DELETE = "DELETE";
	
	/** La constante TYPE_EXIST. */
	public static final String TYPE_EXIST = "EXIST";
	
	/** La constante QUERY_INSERT. */
	public static final String QUERY_INSERT = "INSERT INTO TRAN_MEDIO_AUT("
			+"U_VERSION, CVE_CLACON, SUC_OPERANTE, HORA_INICIO, "
            + "HORA_CIERRE, CERRADO_ABIERTO, FLG_INHAB, CVE_CLACON_TEF,SEGURO, IMPORTE_MAX, "
            + "CVE_MEDIO_ENT, CVE_TRANSFE, CVE_OPERACION, CVE_MECANISMO) "
            + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	/** La constante QUERY_EXPORTAR. */
	public static final String QUERY_EXPORTAR = "SELECT TM.CVE_MEDIO_ENT||','||TM.CVE_TRANSFE||','||TM.CVE_OPERACION||','|| "+
			"TM.CVE_CLACON||','|| TM.SUC_OPERANTE||','||TM.HORA_INICIO||','||TM.HORA_CIERRE||','||TM.CERRADO_ABIERTO||','||" + 
			"TM.FLG_INHAB||','||TM.CVE_CLACON_TEF||','||TM.CVE_MECANISMO||','||NVL(CM.DESCRIPCION,' ')||','||TM.SEGURO||','||"+
			"TM.IMPORTE_MAX||','|| NVL(TS.DESCRIPCION,' ') ||','||NVL(TRNS.DESCRIPCION,' ')  " + 
			QUERY_FILTROS;
	
	/** Consulta de insercion en tabla con nombre de archivo de todos los envios exportados. */
	public static final StringBuilder EXPORT_TODOS = new StringBuilder()
			.append("INSERT INTO TRAN_SPEI_EXP_CDA")
			.append("(FCH_OPERACION,USUARIO,SESION,MODULO,SUBMODULO,NOMBRE_ARCHIVO,NUMERO_REGISTROS,ESTATUS,FECHA_EXPORTA,CONSULTA1,CONSULTA2)")
			.append(" VALUES (SYSDATE, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?,?  )");

	/**
	 * Nueva instancia constantes mtto medios aut.
	 */
	private ConstantesMttoMediosAut(){
		super();
    }
	

	
}