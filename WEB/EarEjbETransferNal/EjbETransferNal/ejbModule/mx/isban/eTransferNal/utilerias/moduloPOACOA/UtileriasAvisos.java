/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasAvisos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:53:49 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.Map;

import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Class UtileriasAvisos.
 *
 * Clase de utilerias que contiene metodos dedicados
 * para los flujos de la pantalla de Avisos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
public class UtileriasAvisos implements Serializable {


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -720215113653274811L;
	
	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasAvisos utils;
	
	/**
	 * Obtener el objeto: utils.
	 *
	 * @return El objeto: utils
	 */
	public static UtileriasAvisos getUtils() {
		if (utils == null) {
			utils = new UtileriasAvisos();
		}
		return utils;
	}
	
	/**
	 * Obtener el objeto: sort field.
	 *
	 * Obtiene el campo a buscar 
	 * en el query a manera de filtro.
	 * 
	 * @param sortField El objeto: sort field
	 * @return El objeto: sort field
	 */
	public String getSortField(String sortField) {
		String cadena = "";
		if ("hora".equals(sortField)){
			cadena = "HORA_RECEPCION";
		} else if ("folio".equals(sortField)){
			cadena = "FOLIO_SERVIDOR";
		} else if ("typTrasp".equals(sortField)){
			cadena = "DSC";
		} else if ("importe".equals(sortField)){
			cadena = "MONTO";
		} else if ("SIAC".equals(sortField)){
			cadena = "REFERENCIA_SIAC";
		} else if ("SIDV".equals(sortField)){
			cadena = "REFERENCIA_SIDV";
		} else {
			cadena = "FOLIO_SERVIDOR";
		}
		return cadena;
	}
	
	/**
	 * Valida cantidad.
	 *
	 * @param response El objeto: response
	 * @param mapResult El objeto: map result
	 * @return Objeto bean res aviso traspasos DAO
	 */
	public BeanResAvisoTraspasosDAO validaCantidad(BeanResAvisoTraspasosDAO response,Map<String, Object> mapResult) {
		Integer cero = 0;
		Utilerias utilerias = Utilerias.getUtilerias();
		if (cero.equals(response.getTotalReg())) {
			response.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
		}
		return response;
	}
}
