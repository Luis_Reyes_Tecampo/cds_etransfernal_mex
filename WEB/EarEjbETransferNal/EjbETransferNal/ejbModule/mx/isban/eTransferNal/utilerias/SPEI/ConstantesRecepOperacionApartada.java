/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: ConstantesRecepOperacionApartada.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   19/09/2018 05:54:10 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.SPEI;

/**
 *  * Clase de constantes que tiene todos los parametro  y consultas que se utilizan para
 * mostrar la informacion de las pantalla de recepcion de operaciones  de cuenta alternas y principales
 *
 * tambien contiene actualizacione de  operaciones como la consulta para mostrar los resportes
 *
 *  esto de historicas y operciones del dia.
 *
 * @author FSW-Vector
 * @since 20/09/2018
 */
public final class ConstantesRecepOperacionApartada {

	/** La constante OK. */
	public static final String OK = "OK";

	/** La constante NOK. */
	public static final String NOK = "NK";

	/** La constante TYPE_INSERT. */
	public static final String TYPE_INSERT = "INSERT";

	/** La constante TYPE_UPDATE. */
	public static final String TYPE_UPDATE = "UPDATE";

	/** La constante TYPE_DELETE. */
	public static final String TYPE_DELETE = "DELETE";

	/** La constante DATO_FIJO. */
	public static final String DATO_FIJO = "IDFOLIO";

	/** La constante RECEPCION_OPERACION_SDO_DIA. */
	public static final String RECEPCION_OPERACION_SDO_DIA= "recepcionOperacion";

	/** La constante RECEPCION_OPERACION_SDO_HTO. */
	public static final String RECEPCION_OPERACION_SDO_HTO = "recepcionOperacionHto";

	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	public static final String RECEPCION_OPERACION_SPEI_HTO = "recepcionOperaHistoricaSPEI";

	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	public static final String RECEPCION_OPERACION_SPEI_DIA = "recepcionOperaSPEI";

	/** Propiedad del tipo String que almacena el valor de TABLA_APARTA. */
	public static final String TABLA_APARTA = "TRAN_SPEI_BLOQUEO";

	/** Propiedad del tipo String que almacena el valor de TABLA_APARTA_HTO. */
	public static final String TABLA_APARTA_HTO = "TRAN_SPEI_BLOQUEO_HIS";

	/** La constante INSERT_OP_APARTADAS_DIA. */
	public static final String INSERT_OP_APARTADAS= "INSERT INTO "+TABLA_APARTA+"(ID_FOLIO,FCH_OPER_PK,COD_MI_INSTI_PK,COD_INST_ORD_PK,ID_FOL_PAQ_PK,"+
			"ID_FOL_PAGO_PK,COD_USR_APART,FCH_ALTA ) VALUES(TRAN_SPEI_BLOQUEO_SEQ.NEXTVAL,to_date(?,'dd-MM-yyyy'),?,?,?,?,?,SYSDATE) ";

	/** La constante INSERT_OP_APARTADAS_DIA. */
	public static final String INSERT_OP_APARTADAS_HTO= "INSERT INTO "+TABLA_APARTA_HTO+"(ID_FOLIO,FCH_OPER_PK,COD_MI_INSTI_PK,COD_INST_ORD_PK,ID_FOL_PAQ_PK," +
			"ID_FOL_PAGO_PK,COD_USR_APART,FCH_ALTA) VALUES(TRAN_SPEI_BLOQUEO_HIS_SEQ.NEXTVAL,to_date(?,'dd-MM-yyyy'),?,?,?,?,?,SYSDATE) ";

	/** La constante TABLA_DIA. */
	public static final String TABLA_TRAN_SPEI ="TRAN_SPEI_REC";

	/** La constante TABLA_HTO. */
	public static final String TABLA_TRAN_SPEI_HTO ="TRAN_SPEI_REC_HIS";

	/** La constante FROM. */
	public static final String FROM =" FROM ";

	/** La constante TSR. */
	public static final String OPER =" TSR ";

		/** La constante TABLA_DIA. */
	public static final String TABLA_DIA =FROM+TABLA_TRAN_SPEI+ OPER;

	/** La constante TABLA_HTO. */
	public static final String TABLA_HTO =FROM+TABLA_TRAN_SPEI_HTO+OPER;

	/** Propiedad del tipo String que almacena el valor de QUERY_BUSQUEDA. */
	public static final String QUERY_USUARIOS = "SELECT T1.COD_USR_APART SECUENCIAL, T1.COD_USR_APART DESCRIPCION " +
			"FROM "+TABLA_APARTA+"  T1 INNER JOIN "+TABLA_TRAN_SPEI+"  T2 ON T1.COD_INST_ORD_PK=T2.CVE_INST_ORD " +
			"AND T2.CVE_MI_INSTITUC=T1.COD_MI_INSTI_PK AND T2.FOLIO_PAGO=T1.ID_FOL_PAGO_PK " +
			"AND T2.FOLIO_PAQUETE=T1.ID_FOL_PAQ_PK AND T2.FCH_OPERACION=T1.FCH_OPER_PK " +
			"WHERE T1.COD_MI_INSTI_PK = ? [FILTRO_FECHA] " +
			"GROUP BY T1.COD_USR_APART ORDER BY T1.COD_USR_APART ";

	/** La constante TABLA_DIA. */
	public static final String TABLA_DIA_APARTA =FROM+TABLA_APARTA+OPER;

	/** La constante TABLA_HTO. */
	public static final String TABLA_HTO_APARTA =FROM+TABLA_APARTA_HTO+OPER;

	/** La constante TABLA_DIA_APART_LEFT. */
	public static final String TABLA_DIA_APART_LEFT =" LEFT JOIN  "+TABLA_APARTA+" APART ";

	/** La constante TABLA_MENSAJE_LEFT. */
	public static final String TABLA_MENSAJE_LEFT ="LEFT JOIN tran_mensaje mnsj ON  mnsj.referencia= TSR.refe_transfer ";

	/** La constante TABLA_TRANSFER_LEFT. */
	public static final String TABLA_TRANSFER_LEFT ="LEFT JOIN tran_transferenc refv ON refv.cve_transfe = mnsj.cve_transfe ";

	/** La constante TABLA_HTO_APART_LEFT. */
	public static final String TABLA_HTO_APART_LEFT =" LEFT JOIN "+TABLA_APARTA_HTO+" APART ";

	/**  Propiedad del tipo String que almacena el valor de QUERY_COUNT_CONSULTA. */
	public static final String QUERY_COUNT_CONSULTA_CAMP =" SELECT COUNT(1) CONT " ;

	/** variable que contiene la consulta. */
	public static final String QUERY_CONSULTA_CAMPOS_ALL= " SELECT TO_CHAR(TSR.FCH_OPERACION,'dd-mm-yyyy') FCH_OPERACION, TSR.CVE_MI_INSTITUC, " +
		      " TSR.CVE_INST_ORD, TSR.FOLIO_PAQUETE, TSR.FOLIO_PAGO, TSR.TOPOLOGIA, TSR.TIPO_PAGO, TSR.ESTATUS_BANXICO, " +
		       " TSR.ESTATUS_TRANSFER, TO_CHAR(TSR.FCH_CAPTURA,'dd-mm-yyyy HH24:MI:SS') FCH_CAPTURA,"+
		       " TSR.CVE_INTERME_ORD, TSR.NUM_CUENTA_TRAN, TSR.MONTO, TSR.CODIGO_ERROR, TSR.MOTIVO_DEVOL MOTIVO_DEVOL, "+
		       " TSR.CVE_PARA_PAGO, TSR.NUM_CUENTA_REC2, TSR.NOMBRE_REC2, "+
		       " TSR.RFC_REC2, TSR.TIPO_CUENTA_REC2, TSR.CONCEPTO_PAGO2, TSR.CONCEPTO_PAGO1, "+
		       " TSR.TIPO_CUENTA_ORD, TSR.TIPO_CUENTA_REC, TSR.NOMBRE_ORD, TSR.NOMBRE_REC, "+
		       " TSR.NUM_CUENTA_ORD, TSR.CDE, TSR.CVE_RASTREO, TSR.REFE_NUMERICA, TSR.CVE_RASTREO_ORI,"+
		       " TSR.PRIORIDAD, TSR.NUM_CUENTA_REC, TSR.TIPO_OPERACION, APART.COD_USR_APART,ER.MSG_ERROR, TTP.DESCRIPCION DES_TIPO_PAGO, TSR.REFE_TRANSFER,"
		       + " MNSJ.ESTATUS, MNSJ.CVE_TRANSFE, REFV.DESCRIPCION " ;

	/** variable que contiene la consulta. */
	public static final String QUERY_EXPORTAR_CAMPOS_ALL= "SELECT TSR.REFE_TRANSFER||','||TSR.CVE_INST_ORD||','||TSR.CVE_MI_INSTITUC||','||TSR.MONTO||','||"
			+ "TSR.ESTATUS_TRANSFER||','||TO_CHAR(TSR.FCH_OPERACION,'dd-mm-yyyy') ||','|| TSR.FCH_CAPTURA||','||TSR.NUM_CUENTA_REC||','||TSR.MOTIVO_DEVOL||','||"
			+"TSR.CODIGO_ERROR||','||ER.MSG_ERROR||','||TSR.CVE_RASTREO||','||APART.COD_USR_APART||','||TSR.FOLIO_PAQUETE||','||TSR.FOLIO_PAGO||','||"
			+ "TSR.TIPO_PAGO||','||TTP.DESCRIPCION||','||TSR.ESTATUS_BANXICO||','||MNSJ.ESTATUS||','||MNSJ.CVE_TRANSFE||','||REFV.DESCRIPCION";

	/** La constante COLUMNAS_EXPORTAR_TOtal. */
	public static final String COLUMNAS_EXPORTAR_TODO = "REFE_TRANSFER,CVE_INST_ORD,CVE_MI_INSTITUC,MONTO,ESTATUS_TRANSFER,FCH_OPERACION,FCH_CAPTURA,"
			+ "NUM_CUENTA_REC,TSR.MOTIVO_DEVOL,CODIGO_ERROR,MSG_ERROR,CVE_RASTREO,COD_USR_APART,FOLIO_PAQUETE,FOLIO_PAGO,TIPO_PAGO,DES_TIPO_PAGO,ESTATUS_BANXICO "
			+"";

	/** La constante QUERY_COUNT_LEFT_APAR. */
	public static final String QUERY_FILTROS_APAR = " on TSR.fch_operacion=APART.FCH_OPER_PK AND " +
			"    TSR.cve_inst_ord= APART.COD_INST_ORD_PK AND TSR.cve_mi_instituc=APART.COD_MI_INSTI_PK " +
			"    AND TSR.folio_paquete= APART.ID_FOL_PAQ_PK AND TSR.folio_pago=APART.ID_FOL_PAGO_PK "
			+ " LEFT JOIN COMU_ERROR ER ON TSR.codigo_error=ER.cod_error "
			+ " LEFT JOIN TRAN_TIPO_PAGO TTP ON TTP.TIPO_PAGO = TSR.TIPO_PAGO ";

	/** *****************************************************************FILTROS DE LA CONSULTA DE OPERACIONES **********************************************************************. */

	/** La constante QUERY_COUNT_FILTRO_USU. */
	public static final String QUERY_COUNT_FILTRO_USU = "AND APART.COD_USR_APART= TRIM(?)   ";

	/** fecha operacion*. */
	public static final String FECHA_OPERACION = " AND TSR.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') ";

	/** La constante FECHA_OPERACION_HTO. */
	public static final String FECHA_OPERACION_HTO = " AND tsrh.FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') ";

	/** La constante FECHA_OPERACION_HTO_DATE. */
	public static final String FECHA_OPERACION_HTO_DATE = " WHERE TO_DATE(FCH_OPERACION,'dd-mm-yyyy') = TO_DATE(?,'dd-mm-yyyy') ";

	/** La constante QUERY_COUNT_FILTROS. */
	public static final String QUERY_COUNT_FILTROS_TODOS_GRAL =" WHERE TSR.CVE_MI_INSTITUC = ? [FILTRO_FECHA] [FILTRO_AND]  ORDER BY TSR.FCH_OPERACION DESC ";

	/** La constante QUERY_COUNT_FILTROS. */
	public static final String QUERY_COUNT_FILTROS_EXP =" WHERE TSR.CVE_MI_INSTITUC = TRIM([FILTRO_INST]) [FILTRO_FECHA] [FILTRO_AND]  ORDER BY TSR.FCH_OPERACION DESC ";

	/** La constante QUERY_EXITE_FILTRO_GRAL. */
	public static final String QUERY_EXITE_DTO_DIA = QUERY_CONSULTA_CAMPOS_ALL + ", ROWNUM INDICE " + TABLA_DIA  +TABLA_DIA_APART_LEFT + QUERY_FILTROS_APAR
			+TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT
			+ " WHERE TSR.CVE_MI_INSTITUC = ? AND TSR.cve_inst_ord = ? "
			+"AND TSR.folio_paquete = ? AND TSR.folio_pago = ?" + FECHA_OPERACION ;

	/** La constante QUERY_EXITE_FILTRO_GRAL. */
	public static final String QUERY_EXITE_DTO_HTO = QUERY_CONSULTA_CAMPOS_ALL + ", ROWNUM INDICE " + TABLA_HTO +TABLA_HTO_APART_LEFT + QUERY_FILTROS_APAR
			+TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT
			+ " WHERE TSR.CVE_MI_INSTITUC = TRIM(?) AND TSR.cve_inst_ord = TRIM(?) "
			+"AND TSR.folio_paquete = TRIM(?) AND TSR.folio_pago = TRIM(?)" + FECHA_OPERACION ;

	/** ********************************************* CONSULTAS SI EXISTE EL DATO EN LA TABLAS DE APARTADOS*********************************. */
	/**FILTROS ELIMINAR**/
	public static final String QUERY_ELIMINAR_INICIO =" DELETE  ";

	/** FILTROS GENERALES*. */
	public static final String QUERY_EXITE_INICIO =" SELECT count(1) TOTAL ";

	/** La constante QUERY_EXITE_FILTRO_GRAL. */
	public static final String QUERY_EXITE_FILTRO_GRAL =" AND TSR.COD_INST_ORD_PK =TRIM(?) AND TSR.ID_FOL_PAQ_PK=TRIM(?) "+
							"AND TSR.ID_FOL_PAGO_PK=TRIM(?) AND TRIM(TSR.COD_USR_APART) =TRIM(?) ";

	/** La constante QUERY_COUNT_FILTROS. */
	public static final String QUERY_FILTROS_TODOS_GRAL =" WHERE TSR.COD_MI_INSTI_PK = TRIM(?) AND TO_CHAR(TSR.FCH_OPER_PK,'dd-mm-yyyy') = ? ";

	/** La constante QUERY_EXITE_OPERA_APARTADA_DIA. */
	public static final String QUERY_EXITE_OPERA_APARTADA_DIA = QUERY_EXITE_INICIO+TABLA_DIA_APARTA+ QUERY_FILTROS_TODOS_GRAL+ QUERY_EXITE_FILTRO_GRAL;

	/** La constante QUERY_EXITE_OPERA_APARTADA_HTO. */
	public static final String QUERY_EXITE_OPERA_APARTADA_HTO = QUERY_EXITE_INICIO+TABLA_HTO_APARTA + QUERY_FILTROS_TODOS_GRAL+ QUERY_EXITE_FILTRO_GRAL;

	/** La constante QUERY_EXITE_OPERA_APARTADA_DIA. */
	public static final String QUERY_ELIMINAR_OPERA_APARTADA_DIA = QUERY_ELIMINAR_INICIO+TABLA_DIA_APARTA+ QUERY_FILTROS_TODOS_GRAL+ QUERY_EXITE_FILTRO_GRAL;

	/** La constante QUERY_EXITE_OPERA_APARTADA_HTO. */
	public static final String QUERY_ELIMINAR_OPERA_APARTADA_HTO = QUERY_ELIMINAR_INICIO+TABLA_HTO_APARTA + QUERY_FILTROS_TODOS_GRAL+ QUERY_EXITE_FILTRO_GRAL;


	/** *****************************************************************CONSULTA LOS DATOS DE LAS OPERACIONES DEL DIA**********************************************************************. */
	/** La constante QUERY_COUNT_CONSULTA_TODAS. */
	public static final String QUERY_COUNT_CONSULTA_TODAS = QUERY_COUNT_CONSULTA_CAMP+TABLA_DIA + TABLA_DIA_APART_LEFT+QUERY_FILTROS_APAR + TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT + QUERY_COUNT_FILTROS_TODOS_GRAL ;


	/** La constante QUERY_CONSULTA_TODAS. */
	public static final String QUERY_CONSULTA_TODAS =
			"SELECT *\r\n" +
			"FROM\r\n" +
			"  (SELECT ROWNUM INDICE,\r\n" +
			"    CONTADOR.CONT,\r\n" +
			"    NVL(TSR.ESTATUS_BANXICO,'') ESTATUS_BANXICO,\r\n" +
			"    NVL(TSR.ESTATUS_TRANSFER,'') ESTATUS_TRANSFER,\r\n" +
			"    NVL(TSR.CODIGO_ERROR,'') CODIGO_ERROR,\r\n" +
			"    TO_CHAR(TSR.FCH_OPERACION,'dd-mm-YYYY') FCH_OPERACION,\r\n" +
			"    NVL(TSR.TOPOLOGIA,'') TOPOLOGIA,\r\n" +
			"    NVL(TSR.PRIORIDAD,'') PRIORIDAD,\r\n" +
			"    NVL(TSR.TIPO_PAGO,'') TIPO_PAGO,\r\n" +
			"    NVL(TSR.TIPO_OPERACION,'') TIPO_OPERACION,\r\n" +
			"    NVL(TSR.CVE_INST_ORD,'') CVE_INST_ORD,\r\n" +
			"    NVL(TSR.CVE_INTERME_ORD,'') CVE_INTERME_ORD,\r\n" +
			"    NVL(TSR.MONTO,'') MONTO,\r\n" +
			"    NVL(TSR.IVA,'') IVA,\r\n" +
			"    NVL(TSR.FOLIO_PAQUETE,'') FOLIO_PAQUETE,\r\n" +
			"    NVL(TSR.REFE_NUMERICA,'') REFE_NUMERICA,\r\n" +
			"    NVL(TSR.FOLIO_PAGO,'') FOLIO_PAGO,\r\n" +
			"    NVL(TSR.NUM_CUENTA_ORD,'') NUM_CUENTA_ORD,\r\n" +
			"    NVL(TSR.TIPO_CUENTA_ORD,'') TIPO_CUENTA_ORD,\r\n" +
			"    NVL(TSR.RFC_ORD,'') RFC_ORD,\r\n" +
			"    NVL(TSR.NOMBRE_ORD,'') NOMBRE_ORD,\r\n" +
			"    NVL(TSR.CVE_RASTREO,'') CVE_RASTREO,\r\n" +
			"    NVL(TSR.CVE_RASTREO_ORI,'') CVE_RASTREO_ORI,\r\n" +
			"    NVL(TSR.CVE_MI_INSTITUC,'') CVE_MI_INSTITUC,\r\n" +
			"    NVL(TSR.NUM_CUENTA_REC,'') NUM_CUENTA_REC,\r\n" +
			"    NVL(TSR.TIPO_CUENTA_REC,'') TIPO_CUENTA_REC,\r\n" +
			"    NVL(TSR.RFC_REC,'') RFC_REC,\r\n" +
			"    NVL(TSR.NOMBRE_REC,'') NOMBRE_REC,\r\n" +
			"    NVL(TSR.NUM_CUENTA_REC2,'') NUM_CUENTA_REC2,\r\n" +
			"    NVL(TSR.TIPO_CUENTA_REC2,'') TIPO_CUENTA_REC2,\r\n" +
			"    NVL(TSR.RFC_REC2,'') RFC_REC2,\r\n" +
			"    NVL(TSR.NOMBRE_REC2,'') NOMBRE_REC2,\r\n" +
			"    NVL(TSR.NUM_CUENTA_TRAN,'') NUM_CUENTA_TRAN,\r\n" +
			"    NVL(TSR.CVE_PARA_PAGO,'') CVE_PARA_PAGO,\r\n" +
			"    NVL(TSR.REFE_COBRANZA,'') REFE_COBRANZA,\r\n" +
			"    NVL(TSR.CONCEPTO_PAGO1,'') CONCEPTO_PAGO1,\r\n" +
			"    NVL(TSR.CONCEPTO_PAGO2,'') CONCEPTO_PAGO2,\r\n" +
			"    NVL(TSR.MOTIVO_DEVOL,'') MOTIVO_DEVOL,\r\n" +
			"    NVL(TSR.U_VERSION,'') U_VERSION,\r\n" +
			"    NVL(TSR.LONG_RASTREO,'') LONG_RASTREO,\r\n" +
			"    NVL(TSR.FOLIO_PAQ_DEV,'') FOLIO_PAQ_DEV,\r\n" +
			"    NVL(TSR.FOLIO_PAGO_DEV,'') FOLIO_PAGO_DEV,\r\n" +
			"    NVL(TSR.REFE_TRANSFER,'') REFE_TRANSFER,\r\n" +
			"    NVL(TO_CHAR(TSR.FCH_CAPTURA,'dd-mm-YYYY hh:mm:ss'),'') FCH_CAPTURA,\r\n" +
			"    NVL(TSR.CDE,'') CDE,\r\n" +
			"    NVL(TSR.CDA,'') CDA,\r\n" +
			"    CASE TSR.ESTATUS_BANXICO\r\n" +
			"      WHEN 'LQ'\r\n" +
			"      THEN 'LIQUIDADA'\r\n" +
			"      WHEN 'PL'\r\n" +
			"      THEN 'POR LIQUIDAR'\r\n" +
			"    END AS DES_EST_BANX,\r\n" +
			"    CASE TSR.ESTATUS_TRANSFER\r\n" +
			"      WHEN 'PL'\r\n" +
			"      THEN 'POR LIQUIDAR'\r\n" +
			"      WHEN 'PA'\r\n" +
			"      THEN 'POR APLICAR'\r\n" +
			"      WHEN 'DV'\r\n" +
			"      THEN 'DEVUELTA'\r\n" +
			"      WHEN 'RE'\r\n" +
			"      THEN 'POSIBLE RECHAZO'\r\n" +
			"      WHEN 'TR'\r\n" +
			"      THEN 'TRANSAC. EXITOSA'\r\n" +
			"    END AS DESCRIP_EST_TRAN,\r\n" +
			"    CASE TSR.PRIORIDAD\r\n" +
			"      WHEN 0\r\n" +
			"      THEN 'SIN PRIORIDAD'\r\n" +
			"      WHEN 1\r\n" +
			"      THEN 'CON PRIORIDAD'\r\n" +
			"    END AS descrip_pri,\r\n" +
			"    CASE TSR.TIPO_OPERACION\r\n" +
			"      WHEN '01'\r\n" +
			"      THEN 'INICIO DE CALL MONEY'\r\n" +
			"      WHEN '02'\r\n" +
			"      THEN 'PAGO DE ACUMULADOS'\r\n" +
			"      WHEN '03'\r\n" +
			"      THEN 'OPERACIONES CON MESA DE DINERO'\r\n" +
			"      WHEN '04'\r\n" +
			"      THEN 'OTROS CONCEPTOS'\r\n" +
			"      WHEN '05'\r\n" +
			"      THEN 'VENCIMIENTO DE CALL MONEY'\r\n" +
			"      WHEN '06'\r\n" +
			"      THEN 'COMPRA-VENTA DE MONEDA EXTRANJERA'\r\n" +
			"      WHEN '07'\r\n" +
			"      THEN 'COBERTURAS CAMBIARIAS'\r\n" +
			"      WHEN '08'\r\n" +
			"      THEN 'FUTUROS DE PESOS'\r\n" +
			"      WHEN '09'\r\n" +
			"      THEN 'TRANSFERENCIA VALOR PLAZA'\r\n" +
			"      WHEN '10'\r\n" +
			"      THEN 'ORDENES DE PAGO DEL EXTERIOR'\r\n" +
			"      WHEN '11'\r\n" +
			"      THEN 'APORT,AMORT O INT DE CREDITO SINDICADO'\r\n" +
			"      WHEN '12'\r\n" +
			"      THEN 'TRASPASO FONDOS CUENTAHABIENTES BANXICO'\r\n" +
			"      WHEN '13'\r\n" +
			"      THEN 'COMPENSACION PROSA'\r\n" +
			"      WHEN '14'\r\n" +
			"      THEN 'LIQ. COMPRA DE BILLETE Y MONEDA - PESOS'\r\n" +
			"      WHEN '15'\r\n" +
			"      THEN 'LIQUIDACION DE OAL'\r\n" +
			"    END AS DES_TIPO_OPE,\r\n" +
			"    (SELECT dev.descripcion\r\n" +
			"    FROM COMU_DETALLE_CD dev\r\n" +
			"    WHERE TIPO_GLOBAL    ='TR_DEVSPEI'\r\n" +
			"    AND TSR.motivo_devol = dev.CODIGO_GLOBAL\r\n" +
			"    ) AS motivo_devol_desc,\r\n" +
			"    TMC.APLICATIVO,\r\n" +
			"    TMC.BUC_CLIENTE,\r\n" +
			"    TSR.UETR_SWIFT,\r\n" +
			"    TSR.CAMPO_SWIFT1,\r\n" +
			"    TSR.CAMPO_SWIFT2,\r\n" +
			"    TSR.REFE_ADICIONAL1,\r\n" +
			"    TSR.IND_BENEF_RECURSOS,\r\n" +
			"    TSR.MONTO_PAGO_ORIG,\r\n" +
			"    TO_CHAR(TSR.FCH_OPER_ORIG,'dd-mm-YYYY') FCH_OPER_ORIG,\r\n" +
			"    TM.ESTATUS,\r\n" +
			"    TM.CVE_TRANSFE,\r\n" +
			"    (SELECT TT.DESCRIPCION\r\n" +
			"    FROM TRAN_TRANSFERENC TT\r\n" +
			"    WHERE TT.CVE_TRANSFE = TM.CVE_TRANSFE\r\n" +
			"    ) AS DESCRIPCION,\r\n" +
			"    TMC.NUM_CUENTA_CLABE,\r\n" +
			"    TM.CVE_OPERACION,\r\n" +
			"    TM.IMPORTE_CARGO,\r\n" +
			"    TM.IMPORTE_DLS,\r\n" +
			"    TM.IMPORTE_ABONO,\r\n" +
			"    (SELECT TSB.COD_USR_APART\r\n" +
			"    FROM TRAN_SPEI_BLOQUEO TSB\r\n" +
			"    WHERE TSR.FCH_OPERACION = TSB.FCH_OPER_PK\r\n" +
			"    AND TSR.CVE_INST_ORD    = TSB.COD_INST_ORD_PK\r\n" +
			"    AND TSR.CVE_MI_INSTITUC = TSB.COD_MI_INSTI_PK\r\n" +
			"    AND TSR.FOLIO_PAQUETE   = TSB.ID_FOL_PAQ_PK\r\n" +
			"    AND TSR.FOLIO_PAGO      = TSB.ID_FOL_PAGO_PK\r\n" +
			"    ) AS COD_USR_APART,\r\n" +
			"    (SELECT ER.MSG_ERROR\r\n" +
			"    FROM COMU_ERROR ER\r\n" +
			"    WHERE TSR.CODIGO_ERROR = ER.COD_ERROR\r\n" +
			"    ) AS MSG_ERROR,\r\n" +
			"    (SELECT ttp.descripcion\r\n" +
			"    FROM tran_tipo_pago ttp\r\n" +
			"    WHERE tsr.tipo_pago = to_number(ttp.tipo_pago)\r\n" +
			"    ) AS des_tipo_pago, \r\n" +
			"    TO_CHAR(H.FCH_RECEPCION,'DD/MM/YYYY HH24:MI:SS') FECHA_CAPTURA\r\n" +
			"  FROM   \r\n" +
			"    (SELECT COUNT(1) CONT\r\n" +
			"    FROM tran_spei_rec tsr\r\n" +
			"    LEFT JOIN tran_spei_bloqueo tsb\r\n" +
			"    ON (tsb.fch_oper_pk       = tsr.fch_operacion\r\n" +
			"    AND tsb.cod_inst_ord_pk   = tsr.cve_inst_ord\r\n" +
			"    AND tsb.cod_mi_insti_pk   = tsr.cve_mi_instituc\r\n" +
			"    AND tsb.id_fol_paq_pk     = tsr.folio_paquete\r\n" +
			"    AND tsb.id_fol_pago_pk    = tsr.folio_pago)\r\n" +
			"    WHERE tsr.cve_mi_instituc = ?\r\n" +
			"    [FILTRO_AND] \r\n" +
			"    ) CONTADOR,\r\n" +
			"    TRAN_SPEI_REC TSR\r\n" +
			"  LEFT JOIN tran_mensaje tm\r\n" +
			"  ON tm.referencia = tsr.refe_transfer\r\n" +
			"  LEFT JOIN tran_mensaje_comp tmc\r\n" +
			"  ON tmc.referencia = tsr.refe_transfer\r\n" +
			"  LEFT JOIN tran_spei_bloqueo tsb\r\n" +
			"  ON (tsb.fch_oper_pk       = tsr.fch_operacion\r\n" +
			"  AND tsb.cod_inst_ord_pk   = tsr.cve_inst_ord\r\n" +
			"  AND tsb.cod_mi_insti_pk   = tsr.cve_mi_instituc\r\n" +
			"  AND tsb.id_fol_paq_pk     = tsr.folio_paquete\r\n" +
			"  AND tsb.id_fol_pago_pk    = tsr.folio_pago)\r\n" +
			"  LEFT JOIN TRAN_SPEI_HORAS H ON (TSR.CVE_INST_ORD = H.CVE_INST_ORD AND\r\n" +
			"  TSR.FCH_OPERACION = H.FCH_OPERACION AND\r\n" +
			"  TSR.FOLIO_PAQUETE = H.FOLIO_PAQUETE AND\r\n" + 
			"  TSR.FOLIO_PAGO    = H.FOLIO_PAGO AND\r\n" + 
			"  TSR.CVE_INST_ORD  = H.CVE_INST_ORD AND H.TIPO = 'R' )\r\n" +
			"  WHERE tsr.cve_mi_instituc = ?\r\n" +
			"  [FILTRO_AND] \r\n" +
			"  ORDER BY FCH_OPERACION DESC\r\n" +
			"  )\r\n" +
			"WHERE INDICE BETWEEN ? AND ? ";

	/** La constante QUERY_CONSULTA_TODAS. */
	public static final String QUERY_CONSULTA_TODAS_EXP =  QUERY_EXPORTAR_CAMPOS_ALL+TABLA_DIA +TABLA_DIA_APART_LEFT+ QUERY_FILTROS_APAR + TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT + QUERY_COUNT_FILTROS_EXP ;

	/** *****************************************************************CONSULTA LOS DATOS DE LAS OPERACIONES DEL HISTORICAS**********************************************************************. */
	public static final String QUERY_COUNT_CONSULTA_TODAS_HTO = QUERY_COUNT_CONSULTA_CAMP+TABLA_HTO + TABLA_HTO_APART_LEFT+QUERY_FILTROS_APAR + TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT + QUERY_COUNT_FILTROS_TODOS_GRAL ;

	public static final String CONTADOR_HIST = "  SELECT COUNT(1) cont\r\n" +
			"    FROM tran_spei_rec_his tsrh\r\n" +
			"    LEFT JOIN tran_spei_bloqueo_his tsbh\r\n" +
			"    ON (tsbh.fch_oper_pk      = tsrh.fch_operacion\r\n" +
			"    AND tsbh.cod_inst_ord_pk  = tsrh.cve_inst_ord\r\n" +
			"    AND tsbh.cod_mi_insti_pk  = tsrh.cve_mi_instituc\r\n" +
			"    AND tsbh.id_fol_paq_pk    = tsrh.folio_paquete\r\n" +
			"    AND tsbh.id_fol_pago_pk   = tsrh.folio_pago)\r\n" +
			"    WHERE tsrh.cve_mi_instituc = ?\r\n" +
			"    [FILTRO_FECHA] \r\n" +
			"    [FILTRO_AND] \r\n";

	public static final String QUERY_CONSULTA_TODAS_HTO =
			"SELECT oper.*\r\n" +
			"FROM\r\n" +
			"  (SELECT ROWNUM indice, regOper.*\r\n" +
			"  FROM\r\n" +
			"  (SELECT \r\n" +
			"    NVL(tsrh.estatus_banxico,'') estatus_banxico,\r\n" +
			"    NVL(tsrh.estatus_transfer,'') estatus_transfer,\r\n" +
			"    NVL(tsrh.codigo_error,'') codigo_error,\r\n" +
			"    TO_CHAR(tsrh.fch_operacion,'dd-mm-YYYY') fch_operacion,\r\n" +
			"    NVL(tsrh.topologia,'') topologia,\r\n" +
			"    NVL(tsrh.prioridad,'') prioridad,\r\n" +
			"    NVL(tsrh.tipo_pago,'') tipo_pago,\r\n" +
			"    NVL(tsrh.tipo_operacion,'') tipo_operacion,\r\n" +
			"    NVL(tsrh.cve_inst_ord,'') cve_inst_ord,\r\n" +
			"    NVL(tsrh.cve_interme_ord,'') cve_interme_ord,\r\n" +
			"    NVL(tsrh.monto,'') monto,\r\n" +
			"    NVL(tsrh.iva,'') iva,\r\n" +
			"    NVL(tsrh.folio_paquete,'') folio_paquete,\r\n" +
			"    NVL(tsrh.refe_numerica,'') refe_numerica,\r\n" +
			"    NVL(tsrh.folio_pago,'') folio_pago,\r\n" +
			"    NVL(tsrh.num_cuenta_ord,'') num_cuenta_ord,\r\n" +
			"    NVL(tsrh.tipo_cuenta_ord,'') tipo_cuenta_ord,\r\n" +
			"    NVL(tsrh.rfc_ord,'') rfc_ord,\r\n" +
			"    NVL(tsrh.nombre_ord,'') nombre_ord,\r\n" +
			"    NVL(tsrh.cve_rastreo,'') cve_rastreo,\r\n" +
			"    NVL(tsrh.cve_rastreo_ori,'') cve_rastreo_ori,\r\n" +
			"    NVL(tsrh.cve_mi_instituc,'') cve_mi_instituc,\r\n" +
			"    NVL(tsrh.num_cuenta_rec,'') num_cuenta_rec,\r\n" +
			"    NVL(tsrh.tipo_cuenta_rec,'') tipo_cuenta_rec,\r\n" +
			"    NVL(tsrh.rfc_rec,'') rfc_rec,\r\n" +
			"    NVL(tsrh.nombre_rec,'') nombre_rec,\r\n" +
			"    NVL(tsrh.num_cuenta_rec2,'') num_cuenta_rec2,\r\n" +
			"    NVL(tsrh.tipo_cuenta_rec2,'') tipo_cuenta_rec2,\r\n" +
			"    NVL(tsrh.rfc_rec2,'') rfc_rec2,\r\n" +
			"    NVL(tsrh.nombre_rec2,'') nombre_rec2,\r\n" +
			"    NVL(tsrh.num_cuenta_tran,'') num_cuenta_tran,\r\n" +
			"    NVL(tsrh.cve_para_pago,'') cve_para_pago,\r\n" +
			"    NVL(tsrh.refe_cobranza,'') refe_cobranza,\r\n" +
			"    NVL(tsrh.concepto_pago1,'') concepto_pago1,\r\n" +
			"    NVL(tsrh.concepto_pago2,'') concepto_pago2,\r\n" +
			"    NVL(tsrh.motivo_devol,'') motivo_devol,\r\n" +
			"    NVL(tsrh.u_version,'') u_version,\r\n" +
			"    NVL(tsrh.long_rastreo,'') long_rastreo,\r\n" +
			"    NVL(tsrh.folio_paq_dev,'') folio_paq_dev,\r\n" +
			"    NVL(tsrh.folio_pago_dev,'') folio_pago_dev,\r\n" +
			"    NVL(tsrh.refe_transfer,'') refe_transfer,\r\n" +
			"    NVL(TO_CHAR(tsrh.fch_captura,'dd-mm-YYYY hh:mm:ss'),'') fch_captura,\r\n" +
			"    NVL(tsrh.cde,'') cde,\r\n" +
			"    NVL(tsrh.cda,'') cda,\r\n" +
			"    CASE tsrh.estatus_banxico\r\n" +
			"      WHEN 'LQ'\r\n" +
			"      THEN 'LIQUIDADA'\r\n" +
			"      WHEN 'PL'\r\n" +
			"      THEN 'POR LIQUIDAR'\r\n" +
			"    END AS des_est_banx,\r\n" +
			"    CASE tsrh.estatus_transfer\r\n" +
			"      WHEN 'PL'\r\n" +
			"      THEN 'POR LIQUIDAR'\r\n" +
			"      WHEN 'PA'\r\n" +
			"      THEN 'POR APLICAR'\r\n" +
			"      WHEN 'DV'\r\n" +
			"      THEN 'DEVUELTA'\r\n" +
			"      WHEN 'RE'\r\n" +
			"      THEN 'POSIBLE RECHAZO'\r\n" +
			"      WHEN 'TR'\r\n" +
			"      THEN 'TRANSAC. EXITOSA'\r\n" +
			"    END AS descrip_est_tran,\r\n" +
			"    CASE tsrh.prioridad\r\n" +
			"      WHEN 0\r\n" +
			"      THEN 'SIN PRIORIDAD'\r\n" +
			"      WHEN 1\r\n" +
			"      THEN 'CON PRIORIDAD'\r\n" +
			"    END AS descrip_pri,\r\n" +
			"    CASE tsrh.tipo_operacion\r\n" +
			"      WHEN '01'\r\n" +
			"      THEN 'INICIO DE CALL MONEY'\r\n" +
			"      WHEN '02'\r\n" +
			"      THEN 'PAGO DE ACUMULADOS'\r\n" +
			"      WHEN '03'\r\n" +
			"      THEN 'OPERACIONES CON MESA DE DINERO'\r\n" +
			"      WHEN '04'\r\n" +
			"      THEN 'OTROS CONCEPTOS'\r\n" +
			"      WHEN '05'\r\n" +
			"      THEN 'VENCIMIENTO DE CALL MONEY'\r\n" +
			"      WHEN '06'\r\n" +
			"      THEN 'COMPRA-VENTA DE MONEDA EXTRANJERA'\r\n" +
			"      WHEN '07'\r\n" +
			"      THEN 'COBERTURAS CAMBIARIAS'\r\n" +
			"      WHEN '08'\r\n" +
			"      THEN 'FUTUROS DE PESOS'\r\n" +
			"      WHEN '09'\r\n" +
			"      THEN 'TRANSFERENCIA VALOR PLAZA'\r\n" +
			"      WHEN '10'\r\n" +
			"      THEN 'ORDENES DE PAGO DEL EXTERIOR'\r\n" +
			"      WHEN '11'\r\n" +
			"      THEN 'APORT,AMORT O INT DE CREDITO SINDICADO'\r\n" +
			"      WHEN '12'\r\n" +
			"      THEN 'TRASPASO FONDOS CUENTAHABIENTES BANXICO'\r\n" +
			"      WHEN '13'\r\n" +
			"      THEN 'COMPENSACION PROSA'\r\n" +
			"      WHEN '14'\r\n" +
			"      THEN 'LIQ. COMPRA DE BILLETE Y MONEDA - PESOS'\r\n" +
			"      WHEN '15'\r\n" +
			"      THEN 'LIQUIDACION DE OAL'\r\n" +
			"    END AS des_tipo_ope,\r\n" +
			"    (SELECT dev.descripcion\r\n" +
			"    FROM COMU_DETALLE_CD dev\r\n" +
			"    WHERE TIPO_GLOBAL     ='TR_DEVSPEI'\r\n" +
			"    AND tsrh.motivo_devol = dev.CODIGO_GLOBAL\r\n" +
			"    )                AS motivo_devol_desc,\r\n" +
			"    tmch.aplicativo  AS aplicativo,\r\n" +
			"    tmch.buc_cliente AS buc_cliente,\r\n" +
			"    tsrh.uetr_swift,\r\n" +
			"    tsrh.campo_swift1,\r\n" +
			"    tsrh.campo_swift2,\r\n" +
			"    tsrh.refe_adicional1,\r\n" +
			"    tsrh.ind_benef_recursos,\r\n" +
			"    tsrh.monto_pago_orig,\r\n" +
			"    TO_CHAR(tsrh.fch_oper_orig,'dd-mm-YYYY') fch_oper_orig,\r\n" +
			"    tmh.estatus     AS estatus,\r\n" +
			"    tmh.cve_transfe AS cve_transfe,\r\n" +
			"    (SELECT tt.descripcion\r\n" +
			"    FROM tran_transferenc tt\r\n" +
			"    WHERE tt.cve_transfe = tmh.cve_transfe\r\n" +
			"    ) AS descripcion,\r\n" +
			"    tmch.num_cuenta_clabe,\r\n" +
			"    tmh.cve_operacion,\r\n" +
			"    tmh.importe_cargo,\r\n" +
			"    tmh.importe_dls,\r\n" +
			"    tmh.importe_abono,\r\n" +
			"    (SELECT tsbs.cod_usr_apart\r\n" +
			"    FROM tran_spei_bloqueo_his tsbs\r\n" +
			"    WHERE tsrh.fch_operacion = tsbs.fch_oper_pk\r\n" +
			"    AND tsrh.cve_inst_ord    = tsbs.cod_inst_ord_pk\r\n" +
			"    AND tsrh.cve_mi_instituc = tsbs.cod_mi_insti_pk\r\n" +
			"    AND tsrh.folio_paquete   = tsbs.id_fol_paq_pk\r\n" +
			"    AND tsrh.folio_pago      = tsbs.id_fol_pago_pk\r\n" +
			"    ) AS cod_usr_apart,\r\n" +
			"    (SELECT er.msg_error\r\n" +
			"    FROM comu_error er\r\n" +
			"    WHERE tsrh.codigo_error = er.cod_error\r\n" +
			"    ) AS msg_error,\r\n" +
			"    (\r\n" +
			"    SELECT ttp.descripcion\r\n" +
			"    FROM tran_tipo_pago ttp\r\n" +
			"    WHERE tsrh.tipo_pago = to_number(ttp.tipo_pago)\r\n" +
			"    ) AS des_tipo_pago,\r\n" +
			"  TO_CHAR(tshh.FCH_RECEPCION,'DD/MM/YYYY HH24:MI:SS')AS FECHA_CAPTURA\r\n" + 
			"  FROM\r\n" +
			"    tran_spei_rec_his tsrh\r\n" +
			"  LEFT JOIN tran_mensaje_his tmh\r\n" +
			"  ON (tmh.referencia  = tsrh.refe_transfer\r\n" +
			"  AND tmh.fch_captura = TRUNC(tsrh.fch_captura))\r\n" +
			"  LEFT JOIN tran_mensaje_fch_his tmfh\r\n" +
			"  ON ( tmfh.num_ref = tsrh.refe_transfer\r\n" +
			"  AND tmfh.fch_capt = TRUNC(tsrh.fch_captura) )\r\n" +
			"  LEFT JOIN tran_mensaje_comp_his tmch\r\n" +
			"  ON ( tmch.referencia = tsrh.refe_transfer\r\n" +
			"  AND tmch.fch_captura = TRUNC(tsrh.fch_captura))\r\n" +
			"  LEFT JOIN tran_spei_bloqueo_his tsbh\r\n" +
			"  ON (tsbh.fch_oper_pk       = tsrh.fch_operacion\r\n" +
			"  AND tsbh.cod_inst_ord_pk   = tsrh.cve_inst_ord\r\n" +
			"  AND tsbh.cod_mi_insti_pk   = tsrh.cve_mi_instituc\r\n" +
			"  AND tsbh.id_fol_paq_pk     = tsrh.folio_paquete\r\n" +
			"  AND tsbh.id_fol_pago_pk    = tsrh.folio_pago)\r\n" +
			"  LEFT JOIN TRAN_SPEI_HORAS_HIS tshh\r\n" +
			"  ON (tsrh.fch_operacion   = tshh.FCH_OPERACION\r\n" + 
			"  AND tsrh.cve_inst_ord    = tshh.CVE_INST_ORD\r\n" +
			"  AND tsrh.cve_mi_instituc = tshh.CVE_INST_BENEF\r\n" +
			"  AND tsrh.folio_paquete   = tshh.FOLIO_PAQUETE\r\n" +
			"  AND tsrh.folio_pago      = tshh.FOLIO_PAGO AND tshh.TIPO = 'R' )\r\n" +		
			"  WHERE tsrh.cve_mi_instituc = ?\r\n" +
			"  [FILTRO_FECHA] \r\n" +
			"  ) regOper \r\n" +
			"   [FILTRO_FECHA_DATE]\r\n" +
			"   [FILTRO_AND] \r\n" +
			"  ORDER BY FCH_OPERACION DESC\r\n"+
			" ) oper" +
			"  WHERE INDICE BETWEEN ? AND ?";

	/** La constante QUERY_CONSULTA_TODAS. */
	public static final String QUERY_CONSULTA_TODAS_EXP_HTO =  QUERY_EXPORTAR_CAMPOS_ALL+TABLA_HTO +TABLA_HTO_APART_LEFT+ QUERY_FILTROS_APAR + TABLA_MENSAJE_LEFT + TABLA_TRANSFER_LEFT + QUERY_COUNT_FILTROS_EXP ;

	/** ***********************************************************************RECUPERAR OPERACION****************************************************************************************. */
	/*** VARIABLE QUE CONTIENE EL FILTRO */
	private static final String FILTRO =" WHERE FCH_OPERACION = TO_DATE(?,'dd-mm-yyyy') AND CVE_MI_INSTITUC = ? AND CVE_INST_ORD = ? AND FOLIO_PAQUETE = ? AND FOLIO_PAGO = ? ";

	/** La constante ACTUALIZAR. */
	private static final String ACTUALIZAR ="UPDATE ";

	/** Propiedad del tipo String que almacena el valor de QUERY_UPDATE. */
	private static final String QUERY_UPDATE_FIN =" SET ESTATUS_TRANSFER=?,CODIGO_ERROR =?, MOTIVO_DEVOL=?, REFE_TRANSFER=? "+	FILTRO;

	/** Propiedad del tipo String que almacena el valor de QUERY_UPDATE. */
	public static final String QUERY_UPDATE_RECUPERAR_DIA = ACTUALIZAR + TABLA_TRAN_SPEI + QUERY_UPDATE_FIN;

	/** Propiedad del tipo String que almacena el valor de QUERY_UPDATE. */
	public static final String QUERY_UPDATE_RECUPERAR_HTO = ACTUALIZAR + TABLA_TRAN_SPEI_HTO + QUERY_UPDATE_FIN;

	/** **********************************************************************Actualiza motivo de rechazo**********************************************************************************. */
		private static final String MOTIVO =" SET MOTIVO_DEVOL=? " + FILTRO;

	/** La constante QUERY_UPDATE_MOTIVO. */
	public static final String QUERY_UPDATE_MOTIVO = ACTUALIZAR + TABLA_TRAN_SPEI + MOTIVO ;

	/** La constante MOTIVO_RECHAZO. */
	private static final String MOTIVO_RECHAZO =" SET MOTIVO_DEVOL=?, ESTATUS_TRANSFER=? " + FILTRO;

	/** La constante QUERY_UPDATE_MOTIVO_RECHAZO. */
	public static final String QUERY_UPDATE_MOTIVO_RECHAZO = ACTUALIZAR+ TABLA_TRAN_SPEI + MOTIVO_RECHAZO ;


	/** La constante QUERY_UPDATE_MOTIVO_RECHAZO_HTO. */
	public static final String QUERY_UPDATE_MOTIVO_RECHAZO_HTO =ACTUALIZAR + TABLA_TRAN_SPEI_HTO+ MOTIVO_RECHAZO ;

	/** La constante QUERY_OBTENER_DIAS. */
	public static final String QUERY_OBTENER_DIAS = "SELECT "
			+ "TRUNC(TO_DATE(FCH_OPERACION, 'DD/MM/YYYY') - TO_DATE(sysdate, 'DD/MM/YYYY')) DIAS " +
			"FROM TRAN_SPEI_CTRL CTL, " +
			"(SELECT NVL((SELECT CV_VALOR FROM TRAN_CONTIG_UNIX  " +
			"	WHERE CV_PARAMETRO = 'ENTIDAD_SPEI'),40014) CV_VALOR FROM DUAL) TMP  " +
			"	WHERE  CTL.CVE_MI_INSTITUC = TMP.CV_VALOR ";

	/**  Propiedad del tipo String que almacena el valor de TODAS. */
	public static final String TODAS ="TODAS";

	/**  Propiedad del tipo String que almacena el valor de TVL. */
	public static final String TVL ="TVL";

	/**  Propiedad del tipo String que almacena el valor de TTL. */
	public static final String TTL = "TTL";

	/**  Propiedad del tipo String que almacena el valor de TTXL. */
	public static final String TTXL = "TTXL";

	/**  Propiedad del tipo String que almacena el valor de R. */
	public static final String R = "R";

	/**  Propiedad del tipo String que almacena el valor de APT. */
	public static final String APT = "APT";

	/**  Propiedad del tipo String que almacena el valor de DEV. */
	public static final String DEV = "DEV";

	/**  Propiedad del tipo String que almacena el valor de RMD. */
	public static final String RMD = "RMD";

	/**  Propiedad del tipo String que almacena el valor de CONT. */
	public static final String CONT = "CONT";

	/** La constante EXISTE_OPERACION_AUTORIZADA. */
	public static final String EXISTE_OPERACION_AUTORIZADA="SELECT COUNT(1) TOTAL FROM  "+TABLA_TRAN_SPEI+" TSR" +
			" WHERE TSR.cve_mi_instituc = TRIM(?) AND TSR.fch_operacion = TO_DATE(?,'dd-mm-yyyy')" +
			" AND TSR.cve_inst_ord = ? AND TSR.folio_paquete = ? AND TSR.folio_pago=  ? ";

	/** La constante EXISTE_OPERACION_BLOQUEO. */
	public static final String EXISTE_OPERACION_BLOQUEO="SELECT COUNT(1) TOTAL FROM TRAN_SPEI_BLOQUEO " +
			"			 WHERE COD_MI_INSTI_PK = TRIM(?) AND FCH_OPER_PK = TO_DATE(?,'dd-mm-yyyy') AND COD_INST_ORD_PK = TRIM(?) " +
			"			 AND ID_FOL_PAQ_PK = ? AND ID_FOL_PAGO_PK= ?";

	/** La constante ELIMINAR_RECEPCION_HISTORICA. */
	public static final String ELIMINAR_RECEPCION_HISTORICA =" delete from tran_spei_rec " +
			" where cve_mi_instituc = TRIM(?) AND TO_CHAR(FCH_OPERACION,'dd-mm-yyyy')= trim(?) AND cve_inst_ord = ? "+
			" AND folio_paquete=? AND folio_pago= ? " ;

	/**  Propiedad del tipo String que almacena el valor de CONSULTA HORAS HISTORICA. */
	public static final String CONSULTA_HORAS_HISTORICA = "SELECT TO_CHAR(HHIS.FCH_OPERACION,'dd-MM-yyyy HH24:mi:ss') FCH_OPERACION,HHIS.U_VERSION,HHIS.CVE_INST_ORD,HHIS.CVE_INST_BENEF," +
			"			HHIS.FOLIO_PAQUETE,HHIS.FOLIO_PAGO,HHIS.TIPO,HHIS.REFERENCIA, TO_CHAR(HHIS.FCH_CAPTURA,'dd-MM-yyyy HH24:mi:ss') FCH_CAPTURA, TO_CHAR(HHIS.FCH_ENVIO,'dd-MM-yyyy HH24:mi:ss') FCH_ENVIO, "
			+ " 		TO_CHAR(HHIS.FCH_ACUSE,'dd-MM-yyyy HH24:mi:ss') FCH_ACUSE, TO_CHAR(HHIS.FCH_CONFIRMACION,'dd-MM-yyyy HH24:mi:ss') FCH_CONFIRMACION,TO_CHAR(HHIS.FCH_RECEPCION,'dd-MM-yyyy HH24:mi:ss') FCH_RECEPCION,"
			+ "			TO_CHAR(HHIS.FCH_APROBACION,'dd-MM-yyyy HH24:mi:ss') FCH_APROBACION," +
			"			HHIS.CVE_RASTREO,HHIS.CVE_RASTREO_ORI,HHIS.DIF_ACUSE_APROBACION,HHIS.DIF_RECEPCION_ACUSE,HHIS.DIF_CAPTURA_RECEPCION,HHIS.CVE_TRANSFE,HHIS.CVE_MEDIO_ENT," +
			"			TO_CHAR(HHIS.FCH_CAPTURA_REC,'dd-MM-yyyy HH24:mi:ss') FCH_CAPTURA_REC,HHIS.TIPO_PAGO,HHIS.SW_ACT_HIS_LS,HHIS.SW_ACT_HIS_EN,HHIS.SW_ACT_HIS_CO FROM TRAN_SPEI_HORAS_HIS HHIS "
			+ " WHERE HHIS.FCH_OPERACION =TO_DATE(?,'dd-MM-yy')  AND HHIS.CVE_INST_ORD= ? AND HHIS.CVE_INST_BENEF=? AND HHIS.FOLIO_PAQUETE=? AND "
			+ " HHIS.FOLIO_PAGO=?";

	/**  Propiedad del tipo String que almacena el valor de INSERT HORAS. */
	public static final String INSERT_HORAS = "Insert into TRAN_SPEI_HORAS (FCH_OPERACION,U_VERSION,CVE_INST_ORD,CVE_INST_BENEF,"
			+ "FOLIO_PAQUETE,FOLIO_PAGO,TIPO,REFERENCIA,FCH_CAPTURA,FCH_ENVIO,FCH_ACUSE,FCH_CONFIRMACION,FCH_RECEPCION,FCH_APROBACION,"
			+ "CVE_RASTREO,CVE_RASTREO_ORI,DIF_ACUSE_APROBACION,DIF_RECEPCION_ACUSE,DIF_CAPTURA_RECEPCION,CVE_TRANSFE,CVE_MEDIO_ENT,"
			+ "FCH_CAPTURA_REC,TIPO_PAGO,SW_ACT_HIS_LS,SW_ACT_HIS_EN,SW_ACT_HIS_CO,SW_CALC_INTERES) "
			+ "values (TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),?,?,?,?,?,?,?,NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),"
			+ "NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),?,?,?,?,?,?,?,NVL(TO_DATE(?,'dd-MM-yyyy HH24:mi:ss'),null),?,?,?,?,?)";

	 /**
 	 * Nueva instancia constantes recep operacion apartada.
 	 */
 	private ConstantesRecepOperacionApartada() {
	 }
}