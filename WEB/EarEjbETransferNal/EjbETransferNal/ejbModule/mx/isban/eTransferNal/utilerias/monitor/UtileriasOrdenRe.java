/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasOrdenRe.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/01/2019 11:52:16 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.utilerias.monitor;

import java.util.Date;


import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanDatosGenerales;
import mx.isban.eTransferNal.beans.comunes.BeanDatosOpe;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloSPID.BeanBitOrdenRecep;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class UtileriasDetRecpOp.
 *
 * Clase de utilerias 
 * 
 * @author FSW-Vector
 * @since 29/01/2019
 */
public class UtileriasOrdenRe  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -6769482385375882746L;
	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasOrdenRe util = new UtileriasOrdenRe();
	
	/** Propiedad del tipo String que almacena el valor de NO_APLICA. */
	private static final String NO_APLICA ="No aplica";
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasOrdenRe getInstance(){
		return util;
	}
			 
	/**
	 * Create bitacora bit trans.
	 *
	 * @param beanBitOrdenRecep El objeto: bean bit orden recep
	 * @param sesion El objeto: sesion
	 * @return Objeto bean req insert bit trans
	 */
	public BeanReqInsertBitTrans createBitacoraBitTrans(BeanBitOrdenRecep beanBitOrdenRecep, ArchitechSessionBean sesion){
		BeanReqInsertBitTrans beanReqInsertBitTrans = new BeanReqInsertBitTrans();
		beanReqInsertBitTrans.setDatosOpe(createDatosOpeBitTran(beanBitOrdenRecep));
		beanReqInsertBitTrans.setDatosGenerales(createDatosGenOpeBitTran(beanBitOrdenRecep, sesion));
		/** Retorna objeto de salida**/
		return beanReqInsertBitTrans;
	}
	
	
	/**
	 * Create datos ope bit tran.
	 *
	 * @param beanBitOrdenRecep El objeto: bean bit orden recep
	 * @return Objeto bean datos ope
	 */
	private BeanDatosOpe createDatosOpeBitTran(BeanBitOrdenRecep beanBitOrdenRecep){
		Utilerias utilerias  = Utilerias.getUtilerias();
		BeanDatosOpe datosOpe = new BeanDatosOpe();
		StringBuilder builder = new StringBuilder();
		
		datosOpe.setFchOperacion(beanBitOrdenRecep.getDatosTran().getFchOperacion());
		datosOpe.setEstatusOperacion(beanBitOrdenRecep.getDatosComp().getEstatus());
		datosOpe.setMonto(utilerias.getString(beanBitOrdenRecep.getDatosTran().getMonto()));
		datosOpe.setTipoCambio("");
		builder.append("REFERENCIA =");
		builder.append(beanBitOrdenRecep.getDatosTran().getReferencia());
		datosOpe.setReferencia(builder.toString());
		datosOpe.setCtaOrigen(beanBitOrdenRecep.getDatosTran().getNumCuentaOrd());
		datosOpe.setCtaDestino(beanBitOrdenRecep.getDatosTran().getNumCuentaRec());
		datosOpe.setIdOperacion(beanBitOrdenRecep.getIdOperacion());
		datosOpe.setBancoDestino(NO_APLICA);
		datosOpe.setCodigoErr(beanBitOrdenRecep.getDatosComp().getCodError());
		datosOpe.setDescErr(beanBitOrdenRecep.getDatosComp().getDescErr());
		datosOpe.setDescOper(beanBitOrdenRecep.getDatosComp().getDescOper());
		datosOpe.setComentarios(beanBitOrdenRecep.getDatosComp().getComentarios());
		datosOpe.setServTran(beanBitOrdenRecep.getServTran());
		/** Retorna objeto de salida**/
		return datosOpe;
	}
	
	
	/**
	 * Create datos gen ope bit tran.
	 *
	 * @param beanBitOrdenRecep El objeto: bean bit orden recep
	 * @param sesion El objeto: sesion
	 * @return Objeto bean datos generales
	 */
	private BeanDatosGenerales createDatosGenOpeBitTran(BeanBitOrdenRecep beanBitOrdenRecep, ArchitechSessionBean sesion){
		Utilerias utilerias  = Utilerias.getUtilerias();
		Date fecha = new Date();
		BeanDatosGenerales beanDatosGenerales = new BeanDatosGenerales();
		beanDatosGenerales.setIdToken("0");
		beanDatosGenerales.setDirIp(sesion.getIPCliente());
		beanDatosGenerales.setAplicacionCanal("Transfer Nacional");
		beanDatosGenerales.setInstanciaWeb(sesion.getNombreServidor());
		beanDatosGenerales.setHostWeb(sesion.getIPServidor());
		beanDatosGenerales.setIdSesion(sesion.getIdSesion());
		beanDatosGenerales.setCodCliente(NO_APLICA);
		beanDatosGenerales.setContrato(NO_APLICA);
		beanDatosGenerales.setNumeroTitulos("0");
		beanDatosGenerales.setFchProgramada("");
		beanDatosGenerales.setFchAplica(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanDatosGenerales.setFchAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanDatosGenerales.setHoraAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
		beanDatosGenerales.setNombreArchivo(beanBitOrdenRecep.getNombreArchivo());
		/** Retorna objeto de salida**/
		return beanDatosGenerales;
	}
}
