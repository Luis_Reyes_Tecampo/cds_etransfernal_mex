package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * class : UtileriasIntFinancierosHis
 * 
 * Almacena variables que contienen consultas a realizar a la Bd
 * segun el metodo que la requiera
 *
 * @author SNGEnterprise
 * Fecha 29/30/2020
 * Hora 00:00:00
 */
public class UtileriasIntFinancierosHis extends Architech{

	/**
	 * Propiedad del tipo long que almacena el 
	 * valor de serialVersionUID. 
	 */
	private static final long serialVersionUID = 5168485281641262700L;

	/** La variable que contiene informacion con respecto a: instancia. */
	protected static UtileriasIntFinancierosHis instancia;

	/** Referencia del Objeto Utilerias. */
	protected static final UtileriasIntFinancierosHis UTILERIAS = new UtileriasIntFinancierosHis();
	
	/** La constante TYPE_EXPORT. */
	public static final String TYPE_EXPORT = "EXPORT";
	
	/** La constante CONSULTA_EXP_TDO_CAT_FIN que contiene la consulta para exportar. */
	public static final String CONSULTA_EXP_TODO_CAT_FIN = "SELECT CVE_INTERME || ',' || TO_CHAR(FCH_MODIF,'DD-MM-YYYY') || ',' || TIPO_MODIF || ',' || USUARIO|| ',' || TIPO_INTERME_ORI || ',' ||"
			+ " TIPO_INTERME_NVO || ',' || NUM_CECOBAN_ORI || ',' || NUM_CECOBAN_NVO || ',' || NOMBRE_CORTO_ORI || ',' || NOMBRE_CORTO_NVO || ',' ||"
			+ " NOMBRE_LARGO_ORI || ',' || NOMBRE_LARGO_NVO || ',' || NUM_BANXICO_ORI || ',' || NUM_BANXICO_NVO FROM COMU_INTERME_FIN_HIS";
	
	/** La constante TITULO_EXP_TDO_CAT_FIN de los titulos del encabezado para exportar el archivo excel*/
	public static final String TITULO_EXP_TODO_CAT_FIN = "Cve Intermediario, Fecha Modif, Tipo Modif, Usuario Modif, Tipo Interme ORI, Tipo Interme NVO, Num Cecoban ORI, "
			+ "Num Cecoban NVO, Nombre Corto ORI, Nombre Corto NVO, Nombre Largo ORI, Nombre Largo NVO, Num. Banxico ORI, Num. Banxico NVO";	
	
	/** La constante WHERE. */
	protected static final String WHERE = "WHERE";
	
	/**
	 * Obtener el objeto: Utilerias.
	 * 
	 * @return El objeto: UTILERIAS
	 */
	public static UtileriasIntFinancierosHis getUtilerias() {
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: instancia.
	 * 
	 * @return El objeto: instancia
	 */
	public static UtileriasIntFinancierosHis getInstancia() {
		if (instancia == null) {
			instancia = new UtileriasIntFinancierosHis();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: error. Obtiene el Bean de error en base al responseDTO
	 * @param responseDAO El objeto: response DAO, si es null se envia el codigo de error por defecto
	 * @return error El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO) {
		BeanResBase error = new BeanResBase();
		// Si responseDAO es null, se envia el codigo de error por defecto
		if (responseDAO == null) {
			error.setCodError(Errores.EC00011B);
			return error;
		}
		// Regresa el codigo y mensaje del DAO
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	
	/**
	 * Metodo para ir concatenando los filtros del paginador
     * @param beanPaginador paginador de la vista
     * @return parametros recibidos
	 */
	public List<Object> getPaginadorXParamHis(BeanPaginador beanPaginador){
		List<Object> parametros = new ArrayList<Object>();
		parametros.add(beanPaginador.getRegIni());
		parametros.add(beanPaginador.getRegFin());
		return parametros;
	}
	
	/**
	 * getFiltro metodo en el cual regresa las condiciones where y and de la consulta.
	 * @param beanFiltro objeto donde se obtiene los atributos 
	 * @param key parametro que especifica el where o and
	 * @param filtro parametroe donde se asigna el string formado.
	 * @return buildFiltro regresa un objeto de tipo StringBuilder
	 */
	public StringBuilder getFiltro(BeanIntFinancieroHistorico beanFiltro, String key, StringBuilder filtro) {

		if (beanFiltro.getCveInterme() != null && !beanFiltro.getCveInterme().isEmpty() && !filtro.toString().contains("CVE_INTERME")) {
			getFiltroXParam(filtro, key, "CVE_INTERME", beanFiltro.getCveInterme().toUpperCase().trim());
		}

		if (beanFiltro.getFchModif() != null && !beanFiltro.getFchModif().isEmpty() && !filtro.toString().contains("FCH_MODIF")) {
			getFiltroXParam(filtro, key, "TO_DATE (FCH_MODIF)", beanFiltro.getFchModif().trim());
		}

		if (beanFiltro.getTipoModif() != null && !beanFiltro.getTipoModif().isEmpty() && !filtro.toString().contains("TIPO_MODIF")) {
			getFiltroXParam(filtro, key, "TIPO_MODIF", beanFiltro.getTipoModif().toUpperCase().trim());
		}

		return getFiltr(beanFiltro, key, filtro);
	}
	
	/**
	 * getFiltro metodo en el cual regresa las condiciones where y and de la consulta.
	 * @param beanFiltro objeto donde se obtiene los atributos 
	 * @param key parametro que especifica el where o and
	 * @param filtro parametroe donde se asigna el string formado.
	 * @return buildFiltro regresa un objeto de tipo StringBuilder
	 */
	public StringBuilder getFiltr(BeanIntFinancieroHistorico beanFiltro, String key, StringBuilder filtro) {

		if (beanFiltro.getUsuario() != null && !beanFiltro.getUsuario().isEmpty() && !filtro.toString().contains("USUARIO")) {
			getFiltroXParam(filtro, key, "USUARIO", beanFiltro.getUsuario().toUpperCase().trim());
		}

		if (beanFiltro.getTipoIntermeOri() != null && !beanFiltro.getTipoIntermeOri().isEmpty() && !filtro.toString().contains("TIPO_INTERME_ORI")) {
			getFiltroXParam(filtro, key, "TIPO_INTERME_ORI", beanFiltro.getTipoIntermeOri().toUpperCase().trim());
		}

		if (beanFiltro.getNumCecobanOri()!= null && !beanFiltro.getNumCecobanOri().isEmpty() && !filtro.toString().contains("NUM_CECOBAN_ORI")) {
			getFiltroXParam(filtro, key, "NUM_CECOBAN_ORI", beanFiltro.getNumCecobanOri().trim());
		}

		return buildFiltro(beanFiltro, key, filtro);
	}
		
	/**
	 * getFiltro metodo en el cual regresa las condiciones where y and de la consulta.
	 * @param beanFiltro objeto donde se obtiene los atributos 
	 * @param key parametro que especifica el where o and
	 * @param filtro parametroe donde se asigna el string formado.
	 * @return filtro regresa un objeto de tipo StringBuilder
	 */
	public StringBuilder buildFiltro(BeanIntFinancieroHistorico beanFiltro, String key, StringBuilder filtro) {
		
		if (beanFiltro.getNumBanxicoOri() != null && !beanFiltro.getNumBanxicoOri().isEmpty() && !filtro.toString().contains("NUM_BANXICO_ORI")) {
			getFiltroXParam(filtro, key, "NUM_BANXICO_ORI", beanFiltro.getNumBanxicoOri().trim());
		}

		if (beanFiltro.getNombreCortoOri() != null && !beanFiltro.getNombreCortoOri().isEmpty() && !filtro.toString().contains("NOMBRE_CORTO_ORI")) {
			getFiltroXParam(filtro, key, "NOMBRE_CORTO_ORI", beanFiltro.getNombreCortoOri().toUpperCase().trim());
		}

		if (beanFiltro.getNombreLargoOri() != null && !beanFiltro.getNombreLargoOri().isEmpty() && !filtro.toString().contains("NOMBRE_LARGO_ORI")) {
			getFiltroXParam(filtro, key, "NOMBRE_LARGO_ORI", beanFiltro.getNombreLargoOri().toUpperCase().trim());
		}
		return filtro;		
	}

	/**
	 * Metodo para ir concatenando los filtros del where o and.
     * @param filtro variable de asignacion 
	 * @param key varible para identificar la llave (and o where)
	 * @param param variable para identificar los parametros
	 * @param value variable que contiene el valor
	 */
	private void getFiltroXParam(StringBuilder filtro, String key, String param, String value) {

		if (!filtro.toString().contains(WHERE) && WHERE.equalsIgnoreCase(key)) {
			filtro.append(key + " " + param + "=" + "'" + value + "'");
		}
		
		if (filtro.toString().contains(WHERE) && "AND".equalsIgnoreCase(key)) {
			filtro.append(key + " " + param + "=" + "'" + value + "'");
		}

	}
	
	/**
	 * Se realiza la validacion para la
	 * busqueda de fecha baja en la pantalla de intermediario
	 * vigente
	 * @param beanConsReqIntFin llevamos el parametro 
	 * @return se retorna una cadena para
	 */
	public static String obtenerConsulta(BeanConsReqIntFin beanConsReqIntFin) {
		String resultado = "";
		
		if((beanConsReqIntFin.getFchBaja() == null || beanConsReqIntFin.getFchBaja().isEmpty()) && !"E".equals(beanConsReqIntFin.getBandera())) {
			resultado = "  AND (TO_DATE(C.FCH_BAJA) IS NULL OR TO_DATE(C.FCH_BAJA) > trunc(SYSDATE))  ";
		} 
		
		return resultado;
	}
}
