/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasNotificadorAux.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     1/08/2019 09:59:39 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;

/**
 * Class UtileriasNotificadorAux.
 *
 ** Clase de utilerias que contiene metodos que ayudan al flujo normal
 * a realizar los procesos del negocio.
 * 
 * @author FSW-Vector
 * @since 1/08/2019
 */
public class UtileriasNotificadorAux implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1113596797093963121L;
	private static UtileriasNotificadorAux instancia;
	
	/** La constante CONSULTA_NOTIFICADOR. */
	private static final String CONSULTA_NOTIFICADOR = "SELECT CVE_PARAM_FALTA AS CVE, DESCRIPCION FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%NOTIFICADOR%' ORDER BY CVE_PARAM_FALTA";
	
	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA = "SELECT CVE_TRANSFE AS CVE, DESCRIPCION FROM TRAN_TRANSFERENC ORDER BY CVE_TRANSFE";
	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MEDIOS_ENTREGA = "SELECT CVE_MEDIO_ENT AS CVE, DESCRIPCION FROM COMU_MEDIOS_ENT ORDER BY CVE_MEDIO_ENT";
	
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS = "SELECT CVE_PARAM_FALTA AS CVE, DESCRIPCION FROM TRAN_PARAMETROS WHERE CVE_PARAMETRO LIKE '%ESTATUSTRANS%' ORDER BY CVE_PARAM_FALTA";
	
	/** La constante CONSULTA_NOTIFICADOR. */
	private static final String CONSULTA_NOTIFICADOR_FILTRO = "SELECT DISTINCT TP.CVE_PARAM_FALTA AS CVE, TP.DESCRIPCION FROM TRAN_MX_CAT_NOTIF CN, "
			.concat("TRAN_PARAMETROS TP WHERE TP.CVE_PARAMETRO LIKE '%NOTIFICADOR%' AND TP.CVE_PARAM_FALTA = CN.TIPO_NOTIF [FILTER] ORDER BY TP.CVE_PARAM_FALTA");
	
	/** La constante CONSULTA_TRASFERENCIA. */
	private static final String CONSULTA_TRASFERENCIA_FILTRO = "SELECT DISTINCT TT.CVE_TRANSFE AS CVE, TT.DESCRIPCION FROM TRAN_TRANSFERENC TT, "
			.concat(" TRAN_MX_CAT_NOTIF CN WHERE TT.CVE_TRANSFE = CN.CVE_TRANSFER [FILTER] ORDER BY TT.CVE_TRANSFE");
	
	/** La constante CONSULTA_MEDIOS_ENTREGA. */
	private static final String CONSULTA_MEDIOS_ENTREGA_FILTRO = "SELECT DISTINCT CVE_MEDIO_ENT AS CVE, DESCRIPCION FROM TRAN_MX_CAT_NOTIF, COMU_MEDIOS_ENT WHERE CVE_MEDIO_ENT = MEDIO_ENTREGA [FILTER] ORDER BY CVE_MEDIO_ENT";
	
	/** La constante CONSULTA_ESTATUS. */
	private static final String CONSULTA_ESTATUS_FILTRO = "SELECT DISTINCT TP.CVE_PARAM_FALTA AS CVE, TP.DESCRIPCION FROM TRAN_MX_CAT_NOTIF"
			.concat(" CN,TRAN_PARAMETROS TP WHERE TP.CVE_PARAMETRO LIKE '%ESTATUSTRANS%' AND TP.CVE_PARAM_FALTA = CN.ESTATUS_TRANSFER [FILTER] ORDER BY TP.CVE_PARAM_FALTA");
	
	/** La constante utils. */
	private static final UtileriasNotificador utils = UtileriasNotificador.getInstancia();
	
	/**
	 *
	 * Metodo para obtener la referencia a esta clase.
	 * 
	 * @return instanica: Regresa el objeto instancia cuando esta es nula
	 * 
	 */
	public static UtileriasNotificadorAux getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasNotificadorAux();
		}
		return instancia;
	}
	
	/**
	 * Evalua consulta.
	 * 
	 * Retorna la consulta en base al tipo de lista que se espera llenar
	 *
	 * @param tipo --> variable de tipo int que almacena el tipo de consulta
	 * @param index --> variable de tipo int que trae el id del registro
	 * @param filter --> objeto BeanNotificacion que trae los filtros
	 * @return query --> variable de tipo string que trae la consulta
	 */
	public String evaluaConsulta(int tipo, int index, BeanNotificacion filter) {
		String query = "";
		if (tipo == 1) {
			if (index == 0) {
				query = CONSULTA_NOTIFICADOR;
			} 
			if (index == 1) {
				query = CONSULTA_TRASFERENCIA;
			} 
			if (index == 2) {
				query = CONSULTA_MEDIOS_ENTREGA;
			} 
			if (index == 3) {
				query = CONSULTA_ESTATUS;
			} 
		} else {
			query = evaluaConsultaAux(index, filter);
		}
		return query;
	}
	
	/**
	 * Evalua consulta aux.
	 * 
	 * Obtiene la  consulta cuando se necesitan los registros de 
	 * los filtros
	 *
	 *  @param index --> variable de tipo int que trae el id del registro
	 * @param filter --> objeto BeanNotificacion que trae los filtros
	 * @return query --> variable de tipo string que trae la consulta
	 */
	private String evaluaConsultaAux(int index, BeanNotificacion filter) {
		String query = "";
		if (index == 0) {
			query = CONSULTA_NOTIFICADOR_FILTRO;
		} 
		if (index == 1) {
			query = CONSULTA_TRASFERENCIA_FILTRO;
		} 
		if (index == 2) {
			query = CONSULTA_MEDIOS_ENTREGA_FILTRO;
		} 
		if (index == 3) {
			query = CONSULTA_ESTATUS_FILTRO;
		} 
		query = validaFiltroExistente(query, filter);
		return query;
	}
	
	/**
	 * Valida filtro existente.
	 *
	 * Busca los filtros actuales y los 
	 * ingresa a los querys de los combos
	 * de filtro.
	 * 
	 *  @param query --> variable de tipo query que trae la consulta a la bd
	 * @param filter --> objeto BeanNotificacion que trae los filtros
	 * @return query --> variable de tipo string que trae la consulta
	 */
	private String validaFiltroExistente (String query, BeanNotificacion filter) {
		String cadenaFiltro = utils.getFiltro(filter, "AND");
		if (cadenaFiltro != null && !cadenaFiltro.trim().isEmpty()) {
			query = query.replaceAll("\\[FILTER\\]", cadenaFiltro);
		} else {
			query = query.replaceAll("\\[FILTER\\]", "");
		}
		return query;
	}
}
