/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPago.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     18/03/2020 01:58:32 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDatosGeneralesPagoDos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenantePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptorPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTranMensaje;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTranMensajeComp;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class UtileriasDetRecpOp.
 *
 * Clase de utilerias
 * 
 * @author FSW-Vector
 * @since 29/01/2019
 */
public class UtileriasPago  extends Architech {
	
	
	/** variable serial. */
	private static final long serialVersionUID = -6769482385375882746L;
	
	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtileriasPago util = new UtileriasPago();
	
	/** La constante FILTRO_AND. */
	private static final String FILTRO_AND = "[FILTRO_AND]"; 
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtileriasPago getInstance(){
		return util;
	}
			 
	
	/**
	 * Metodo que sirve para llenar el listado de pagos
	 * * @param beanResListadoPagosDAO Objeto del tipo BeanResListadoPagosDAO.
	 *
	 * @param beanResListadoPagosDAO El objeto: bean res listado pagos DAO
	 * @param list Objeto del tipo List<HashMap<String, Object>>
	 * @param utilerias Objeto del tipo Utilerias
	 * @return List<BeanPago> Objeto del tipo List<BeanPago>
	 */
	public List<BeanPago> llenarListadoPagos(final BeanResListadoPagosDAO beanResListadoPagosDAO,
			List<HashMap<String, Object>> list, Utilerias utilerias) {
		BeanPago beanPago;
		List<BeanPago> listaBeanPagos;
	    listaBeanPagos = new ArrayList<BeanPago>();
	    Integer cero = 0;
        			 /** Recorre el mapa **/
        			 for(Map<String,Object> mapResult : list){
        				 beanPago = new BeanPago();
        				 beanPago.setReferencia(Long.parseLong(""+mapResult.get("REFERENCIA")+""));
        				 beanPago.setCveCola(utilerias.getString(mapResult.get("CVE_COLA")));
        				 beanPago.setCveTran(utilerias.getString(mapResult.get("CVE_TRANSFE")));
        				 beanPago.setCveMecanismo(utilerias.getString(mapResult.get("CVE_MECANISMO")));
        				 beanPago.setCveMedioEnt(utilerias.getString(mapResult.get("CVE_MEDIO_ENT")));
        				 beanPago.setFechaCaptura(utilerias.getString(mapResult.get("FCH_CAPTURA")));
        				 beanPago.setCveIntermeOrd(utilerias.getString(mapResult.get(ConstantesMonitor.CVE_INTERME_ORD)));
        				 beanPago.setCveIntermeRec(utilerias.getString(mapResult.get("CVE_INTERME_REC")));
        				 beanPago.setImporteAbono(utilerias.getString(mapResult.get("IMPORTE_ABONO")));
        				 beanPago.setEstatus(utilerias.getString(mapResult.get("ESTATUS")));
        				 beanPago.setMensajeError(utilerias.getString(mapResult.get("COMENTARIO3")));
        				 beanPago.setFolioPaquete(utilerias.getString(mapResult.get("FOLIO_PAQUETE")));
        				 beanPago.setFoloPago(utilerias.getString(mapResult.get("FOLIO_PAGO")));
        				 
        				 listaBeanPagos.add(beanPago);
        				 
        				 if (cero.equals(beanResListadoPagosDAO.getTotalReg())){
        					 beanResListadoPagosDAO.setTotalReg(utilerias.getInteger(mapResult.get("CONT")));
						 }
        			 }
		return listaBeanPagos;
	}
	

	/**
	 * Ganera la nomenclatura del archivo a exportar.
	 *
	 * @param tipoOrden objeto del tipo String
	 * @return String
	 */
	public String obtenerNombre (String tipoOrden){
		String nom = "";
		/** Valida el parametro **/
		if ("OPD".equals(tipoOrden)){
			nom = "OR_DV_";
		} else if ("TSS".equals(tipoOrden)){
			nom = "TR_SS_";
		} else if ("OEC".equals(tipoOrden)){
			nom = "OR_CO_";
		} else if ("DEC".equals(tipoOrden)){
			nom = "DV_CO_";
		} else if ("TES".equals(tipoOrden)){
			nom = "TR_ES_";
		} else {
			nom = contObtenerNombre(tipoOrden);
		}
		/** Retorna el nombre **/
		return nom;
	}
	
	/**
	 * Ganera la nomenclatura del archivo a exportar.
	 *
	 * @param tipoOrden objeto del tipo String
	 * @return String
	 */
	private String contObtenerNombre(String tipoOrden) {
		String nom = "";
		/** Valida el parametro de entrada **/
		if ("TEN".equals(tipoOrden)){
			nom = "TR_EN_";
		} else if ("TPR".equals(tipoOrden)){
			nom = "TR_PR_";
		} else if ("OES".equals(tipoOrden)){
			nom = "OR_ES_";
		} else if ("OEN".equals(tipoOrden)){
			nom = "OR_EN_";
		} 
		/** Retorna el nombre **/
		return nom;
	}
	
	
	/**
	 * Metodo para llenar los datos generales.
	 *
	 * @param beanDetallePago Objeto del tipo BeanDetallePago
	 * @param utilerias Objeto del tipo Utilerias
	 * @param mapaResultado Objeto del tipo Map<String, Object>
	 * @param nomPantalla Objeto del tipo String
	 */
	public void llenarDatosGeneralesPago(BeanDetallePago beanDetallePago, Utilerias utilerias,
			Map<String, Object> mapaResultado, String nomPantalla) {

		/** Inicializa los objetos **/
		BeanDatosGeneralesPago beanDatosGeneralesPago= new BeanDatosGeneralesPago();
		BeanDatosGeneralesPagoDos beanDatosGeneralesPagoDos = new BeanDatosGeneralesPagoDos();
		BeanOrdenantePago beanOrdenantePago= new BeanOrdenantePago();
		BeanReceptorPago beanReceptorPago= new BeanReceptorPago();
		BeanTranMensaje beanTranMsj = new BeanTranMensaje();
		
		/** Setea los valores **/
		beanDatosGeneralesPago.setClaveTransferencia(utilerias.getString(mapaResultado.get("CVE_TRANSFE")));
		beanDatosGeneralesPago.setFormaLiquidacion(utilerias.getString(mapaResultado.get("FORMA_LIQ")));
		beanDatosGeneralesPago.setReferencia(utilerias.getString(mapaResultado.get("REFERENCIA")));
		beanDatosGeneralesPago.setCola(utilerias.getString(mapaResultado.get("CVE_COLA")));
		beanDatosGeneralesPago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA")));
		beanDatosGeneralesPago.setEmpresa(utilerias.getString(mapaResultado.get("CVE_EMPRESA")));
		beanDatosGeneralesPago.setClaveOperacion(utilerias.getString(mapaResultado.get("CVE_OPERACION")));
		beanDatosGeneralesPago.setReferenciaMedio(utilerias.getString(mapaResultado.get("REFERENCIA_MED")));
		beanDatosGeneralesPago.setMedioEntrega(utilerias.getString(mapaResultado.get("CVE_MEDIO_ENT")));
		beanDatosGeneralesPago.setUsuarioSolicito(utilerias.getString(mapaResultado.get("CVE_USUARIO_SOL")));
		beanDatosGeneralesPago.setFechaCaptura(utilerias.getString(mapaResultado.get("FCH_CAPTURA")));
		beanDatosGeneralesPago.setUsuarioCapturo(utilerias.getString(mapaResultado.get("CVE_USUARIO_CAP")));
		beanDatosGeneralesPago.setRefeConcent(utilerias.getString(mapaResultado.get("REFERENCIA_CON")));
		
		//Datos generales Adicionales
		beanDatosGeneralesPagoDos.setEnvConf(utilerias.getString(mapaResultado.get("FCH_APLICACION"))); 
		beanDatosGeneralesPagoDos.setClaveDevolucion(utilerias.getString(mapaResultado.get("CVE_DEVOLUCION")));
		beanDatosGeneralesPagoDos.setRefeMecanismo(utilerias.getString(mapaResultado.get("REFERENCIA_MECA")));
		beanDatosGeneralesPagoDos.setRefeNumerica(utilerias.getString(mapaResultado.get("REFE_NUMERICA")));
		beanDatosGeneralesPagoDos.setFolioPaquete(utilerias.getString(mapaResultado.get("FOLIO_PAQUETE")));
		beanDatosGeneralesPagoDos.setFolioPago(utilerias.getString(mapaResultado.get("FOLIO_PAGO")));
		beanDatosGeneralesPagoDos.setEstatus(utilerias.getString(mapaResultado.get("ESTATUS")));
		beanDatosGeneralesPagoDos.setClaveRastreo(utilerias.getString(mapaResultado.get("CVE_RASTREO")));
		beanDatosGeneralesPago.setBeanDatosGeneralesPagoDos(beanDatosGeneralesPagoDos);
		
		//Ordenante
		beanOrdenantePago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_ORD")));
		beanOrdenantePago.setIntermediario(utilerias.getString(mapaResultado.get("CVE_INTERME_ORD")));
		beanOrdenantePago.setPuntoVenta(utilerias.getString(mapaResultado.get("CVE_PTO_VTA_ORD")));
		beanOrdenantePago.setNumeroCuenta(utilerias.getString(mapaResultado.get("NUM_CUENTA_ORD")));					
		beanOrdenantePago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_ORD")));
		beanOrdenantePago.setImporteCargo(utilerias.getString(mapaResultado.get("IMPORTE_CARGO")));
		
		//Receptor
		beanReceptorPago.setIntermediario(utilerias.getString(mapaResultado.get("CVE_INTERME_REC")));
		beanReceptorPago.setNumeroCuentaReceptor(utilerias.getString(mapaResultado.get("NUM_CUENTA_REC")));
		beanReceptorPago.setNombre(utilerias.getString(mapaResultado.get("NOMBRE_REC")));//
		beanReceptorPago.setDivisa(utilerias.getString(mapaResultado.get("CVE_DIVISA_REC")));
		beanReceptorPago.setImporteAbono(utilerias.getBigDecimal(mapaResultado.get("IMPORTE_ABONO")));
		beanReceptorPago.setClaveSwiftCorres(utilerias.getString(mapaResultado.get("CVE_SWIFT_COR")));					
		//Se cambia el calor de SUCURSAL_BCO_REC a CVE_PTO_VTA_REC
		beanReceptorPago.setSucursal(utilerias.getString(mapaResultado.get("CVE_PTO_VTA_REC")));
		beanReceptorPago.setComentario1(utilerias.getString(mapaResultado.get("COMENTARIO1")));
		beanReceptorPago.setComentario2(utilerias.getString(mapaResultado.get("COMENTARIO2")));
		beanReceptorPago.setComentario3(utilerias.getString(mapaResultado.get("COMENTARIO3")));
		
		if(nomPantalla.equals(ConstantesMonitor.SPID)) {
			datosSPID(utilerias,mapaResultado,beanDatosGeneralesPago,beanOrdenantePago, beanReceptorPago );
		}else {
			datosSPEI(utilerias,mapaResultado,beanTranMsj);
		}
		
		beanDetallePago.setBeanDatosGeneralesPago(beanDatosGeneralesPago);
		beanDetallePago.setBeanReceptorPago(beanReceptorPago);
		beanDetallePago.setBeanOrdenantePago(beanOrdenantePago);
		beanDetallePago.setBeanTranMsj(beanTranMsj);
	}
	
	
	/**
	 * Datos SPIDSPEI.
	 *
	 * @param utilerias El objeto: utilerias
	 * @param mapaResultado El objeto: mapa resultado
	 * @param beanDatosGeneralesPago El objeto: bean datos generales pago
	 * @param beanOrdenantePago El objeto: bean ordenante pago
	 * @param beanReceptorPago El objeto: bean receptor pago
	 */
	public void datosSPID(Utilerias utilerias, Map<String, Object> mapaResultado,
			BeanDatosGeneralesPago beanDatosGeneralesPago,BeanOrdenantePago beanOrdenantePago,BeanReceptorPago beanReceptorPago ) {

		/** Obtener:  MOTIVO_DEVOL**/
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setMotivoDevolucion(utilerias.getString(mapaResultado.get("MOTIVO_DEVOL")));
		/** Obtener: FCH_INSTRUC_PAGO**/
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setFechaInstruccionPago(utilerias.getString(mapaResultado.get("FCH_INSTRUC_PAGO")));
		/** Obtener: HORA_INSTRUC_PAGO**/
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setHoraInstruccionPago(utilerias.getString(mapaResultado.get("HORA_INSTRUC_PAGO")));
		/** Obtener: DIRECCION_IP**/
		beanDatosGeneralesPago.getBeanDatosGeneralesPagoDos().setDireccionIP(utilerias.getString(mapaResultado.get("DIRECCION_IP")));
		
		/** Obtener: TIPO_CUENTA_ORD**/
		beanOrdenantePago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_ORD")));
		/** Obtener: COD_POSTAL_ORD**/
		beanOrdenantePago.setCodigoPostal(utilerias.getString(mapaResultado.get("COD_POSTAL_ORD")));
		/** Obtener: FCH_CONSTIT_ORD**/
		beanOrdenantePago.setFechaConstit(utilerias.getString(mapaResultado.get("FCH_CONSTIT_ORD")));
		/** Obtener: RFC_ORD **/
		beanOrdenantePago.setRfc(utilerias.getString(mapaResultado.get("RFC_ORD")));
		/** Obtener: DOMICILIO_ORD **/
		beanOrdenantePago.setDomicilio(utilerias.getString(mapaResultado.get("DOMICILIO_ORD")));
		/** Obtener: TIPO_CUENTA_REC **/
		beanReceptorPago.setTipoCuenta(utilerias.getString(mapaResultado.get("TIPO_CUENTA_REC")));
		/** Obtener: RFC_REC **/
		beanReceptorPago.setRfc(utilerias.getString(mapaResultado.get("RFC_REC")));		
		/** Obtener: CONCEPTO_PAGO**/
		beanReceptorPago.setConceptoPago(utilerias.getString(mapaResultado.get("CONCEPTO_PAGO")));
	}
	

	/**
	 * Metodo para obtener los datos generales.
	 *
	 * @param utilerias objeto del tipo Utilerias
	 * @param mapaResultado objeto del tipo Map<String, Object>
	 * @param beanTranMsj objeto del tipo BeanTranMensaje
	 */
	public void datosSPEI(Utilerias utilerias, Map<String, Object> mapaResultado,BeanTranMensaje beanTranMsj ) {
		BeanTranMensajeComp msjComp = new BeanTranMensajeComp();
		/** Obtener: CANAL_FIRMA **/
		msjComp.setCanalFirma(utilerias.getString(mapaResultado.get("CANAL_FIRMA")));
		/** Obtener:  DIR_IP_FIRMA**/
		msjComp.setDirIpFirma(utilerias.getString(mapaResultado.get("DIR_IP_FIRMA")));
		/** Obtener: DIR_IP_GEN_TRAN **/
		msjComp.setDirIpGen(utilerias.getString(mapaResultado.get("DIR_IP_GEN_TRAN")));
		/** Obtener: TIMESTAMP **/
		msjComp.setTimeTamp(utilerias.getString(mapaResultado.get("TIMESTAMP")));
		/** Obtener: REFE_ADICIONAL1 **/
		msjComp.setRefeAdicional(utilerias.getString(mapaResultado.get("REFE_ADICIONAL1")));
		/** Obtener: **/
		msjComp.setSwift(utilerias.getString(mapaResultado.get("UETR_SWIFT")));
		/** Obtener: UETR_SWIFT **/
		msjComp.setSwiftUno(utilerias.getString(mapaResultado.get("CAMPO_SWIFT1")));
		/** Obtener: CAMPO_SWIFT2 **/
		msjComp.setSwiftDos(utilerias.getString(mapaResultado.get("CAMPO_SWIFT2"))); 
		/** Obtener: FCH_APLICACION **/
		msjComp.setFchAplicion(utilerias.getString(mapaResultado.get("FCH_APLICACION")));
		msjComp.setBuffer(utilerias.getString(mapaResultado.get("BUFFER")));
		beanTranMsj.setTransMsjComp(msjComp);		
		/** Obtener: TIPO_ID_ORD **/
		beanTranMsj.setTipoIdOrd(utilerias.getString(mapaResultado.get("TIPO_ID_ORD")));
		/** Obtener: ID_ORD **/
		beanTranMsj.setIdOrd(utilerias.getString(mapaResultado.get("ID_ORD"))); 
		/** Obtener: ID_REC **/
		beanTranMsj.setIdRec(utilerias.getString(mapaResultado.get("ID_REC"))); 
		/** Obtener: TIPO_ID_REC **/
		beanTranMsj.setTipoIdRec(utilerias.getString(mapaResultado.get("TIPO_ID_REC")));
		/** Obtener: IMPORTE_IVA **/
		beanTranMsj.setImporteIva(utilerias.getInteger(mapaResultado.get("IMPORTE_IVA"))); 
		/** Obtener: NUM_CUENTA_CLABE **/
		beanTranMsj.setNumCtaCve(utilerias.getString(mapaResultado.get("NUM_CUENTA_CLABE"))); 
		/** Obtener: TIPO_ID_REC2**/
		beanTranMsj.setTipoIdRecDos(utilerias.getString(mapaResultado.get("TIPO_ID_REC2"))); 
		/** Obtener: APLICATIVO**/
		beanTranMsj.setAplicativo(utilerias.getString(mapaResultado.get("APLICATIVO")));
		/** Obtener: BUC_CLIENTE **/
		beanTranMsj.setBucCliente(utilerias.getString(mapaResultado.get("BUC_CLIENTE")));
		
	}
	
	/**
	 * Metodo para generar el formato del query.
	 *
	 * @return queryCompleto Objeto del tipo StringBuilder
	 */
	public StringBuilder obtenerFormatoQueryMontos() {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT SUM(IMPORTE_ABONO) as MONTO_TOTAL ") 
		   			   .append(ConstantesMonitor.FROM)
						   		  .append("SELECT COUNT(1) CONT ") 
				   				  .append("FROM TRAN_MENSAJE ") 
				   				.append(ConstantesMonitor.WHERE).append(ConstantesMonitor.ESTATUS).append(ConstantesMonitor.IN).append(ConstantesMonitor.ESTATUS).append("] ") 
					            .append(ConstantesMonitor.AND).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.OPEN_SP).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.CLOSE_SP)
					            .append(FILTRO_AND) 
							  .append(") CONTADOR, TRAN_MENSAJE ")
							  .append(ConstantesMonitor.WHERE).append(ConstantesMonitor.ESTATUS).append(ConstantesMonitor.IN).append(ConstantesMonitor.ESTATUS).append("] ") 
					            .append(ConstantesMonitor.AND).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.OPEN_SP).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.CLOSE_SP)
					          .append(FILTRO_AND) ;
		return queryCompleto;
			}
	
	/**
	 * Metodo para generar el formato del query.
	 *
	 * @return queryCompleto Objeto del tipo StringBuilder
	 */
	public StringBuilder obtenerFormatoQuery() {
		StringBuilder queryCompleto;
		queryCompleto = new StringBuilder().append("SELECT  TM.REFERENCIA, TM.CVE_COLA, TM.CVE_TRANSFE, ") 
		   .append("TM.CVE_MECANISMO, TM.CVE_MEDIO_ENT, TM.FCH_CAPTURA, ") 
		   .append("TM.CVE_INTERME_ORD, TM.CVE_INTERME_REC, TM.IMPORTE_ABONO, ") 
		   .append("TM.ESTATUS, TM.COMENTARIO3, CASE WHEN length(TM.REFERENCIA_MECA) > 5 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,0,6)) ELSE 0 END FOLIO_PAQUETE, CASE WHEN length(TM.REFERENCIA_MECA) > 10 THEN TO_NUMBER(SUBSTR(TM.REFERENCIA_MECA,7,4)) ELSE 0 END FOLIO_PAGO, TM.CONT ")        
		   .append(ConstantesMonitor.FROM) 
				 .append("SELECT TM.*, ROWNUM AS RN ")  
			 	 .append(ConstantesMonitor.FROM)
			 	 	   .append("SELECT REFERENCIA, CVE_COLA, CVE_TRANSFE, CVE_MECANISMO,")
			 	 	   .append("CVE_MEDIO_ENT, TO_CHAR(FCH_CAPTURA,'DD/MM/YYYY HH24:mi:ss') FCH_CAPTURA, CVE_INTERME_ORD, CVE_INTERME_REC,")
		 	 	   	   .append("IMPORTE_ABONO, ESTATUS, COMENTARIO3, REFERENCIA_MECA, CONTADOR.CONT ") 
		   			   .append(ConstantesMonitor.FROM)
						   		  .append("SELECT COUNT(1) CONT ") 
				   				  .append("FROM TRAN_MENSAJE ") 
				   				.append(ConstantesMonitor.WHERE).append(ConstantesMonitor.ESTATUS).append(ConstantesMonitor.IN).append(ConstantesMonitor.ESTATUS).append("] ") 
					            .append(ConstantesMonitor.AND).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.OPEN_SP).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.CLOSE_SP)
							  .append(FILTRO_AND) 
							  .append(") CONTADOR, TRAN_MENSAJE ")
							  .append(ConstantesMonitor.WHERE).append(ConstantesMonitor.ESTATUS).append(ConstantesMonitor.IN).append(ConstantesMonitor.ESTATUS).append("] ") 
					            .append(ConstantesMonitor.AND).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.OPEN_SP).append(ConstantesMonitor.CVE_MECANISMO).append(ConstantesMonitor.CLOSE_SP)
					  .append(FILTRO_AND) 
					  .append(") TM ")
			   .append(")TM ")
		   .append("WHERE RN BETWEEN ? AND ? ");
		return queryCompleto;
			}

	

	
}
