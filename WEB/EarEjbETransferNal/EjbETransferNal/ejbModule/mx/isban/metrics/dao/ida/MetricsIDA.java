/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * MetricsIDA.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dao.ida;

import java.util.List;

import mx.isban.agave.commons.beans.LoggingBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;

/**
* MetricsIDA clase con la logica de conexion y ejecucion para el canal CANAL_METRICS_DS
* @author ING. JUAN MANUEL FUENTES RAMOS.
* @since 2018 -05 Plan Delta Monitores Sucurasales
*/
public final class MetricsIDA {
	
	/** Contante que almacena el canal CANAL_METRICS_DS para servicio de metricas. */
	public static final String CANAL_METRICS_DS = "CANAL_METRICS_DS";
	
	/** Constructor*/
	private MetricsIDA(){}
	
	/**
	 * Insertar.
	 *
	 * @param sql the sql
	 * @param parametros the parametros
	 * @param idCanal the id canal
	 * @param className el nombre de la clase que manda a llamar esta utilidad
	 * @return the response message data base dto
	 * @throws ExceptionDataAccess the exception data access
	 */
	public static ResponseMessageDataBaseDTO insertar(final String sql,final List<Object> parametros, 
			final String idCanal, final String className)throws ExceptionDataAccess{
		ResponseMessageDataBaseDTO responseDTO = null;
		RequestMessageDataBaseDTO reqMessage = new RequestMessageDataBaseDTO();
		reqMessage.setQuery(sql);
		reqMessage.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT_PARAMS);
		reqMessage.setParamsToSql(parametros);		
		reqMessage.setCodeOperation("DELTA01");
		
		DataAccess ida = DataAccess.getInstance(reqMessage, new LoggingBean());
		responseDTO = (ResponseMessageDataBaseDTO) ida.execute(idCanal);
   
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())){
			throw new ExceptionDataAccess(className, responseDTO.getCodeError(), responseDTO.getMessageError());
		}
		return responseDTO;		
	}
}
