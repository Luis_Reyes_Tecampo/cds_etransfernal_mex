/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * GuardaMetricaDAOImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */


package mx.isban.metrics.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.dao.ida.MetricsIDA;
import mx.isban.metrics.dao.util.UtileriasDAO;
import mx.isban.metrics.dto.DTOMetricsBitacora;
import mx.isban.metrics.service.GuardaMetricaDAO;
import mx.isban.metrics.util.MetricsSingleton;
import mx.isban.metrics.util.MetricsUtil;

/**
* DAO DAOGuardaMetrica 
* 
* Clase para insercion a Metrics.
* 
* @author ING. JUAN MANUEL FUENTES RAMOS.
* @since 2018 -05 Plan Delta Monitores Sucurasales
* [[ CSA ]]
*/
@Stateless
@Local(GuardaMetricaDAO.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class GuardaMetricaDAOImpl implements GuardaMetricaDAO{
	
	/** The constant LOG*/
	private static final Logger LOG = Logger.getLogger(GuardaMetricaDAOImpl.class);

	/** The Constant INSERTA_ERROR. Para pruebas locales quitar . al nombre de la tabla*/
	private static final String INSERTA_ERROR = 
	"INSERT INTO ?.LOG_ERROR_PH_?  "
	+ "(ER_INTIMESTAMP, SERVERID, REQUESTID, APPNAME, DESCRIPTION, RETURNCODE, TRACE) " 
	+ "VALUES (?, ?, ?, ?, ?, ?, ?)";

	/** The Constant INSERTA_REQUEST. Para pruebas locales quitar . al nombre de la tabla */
	private static final String INSERTA_REQUEST = 
	"INSERT INTO ?.LOG_REQUEST_PH_? "
	+ "(RQ_INTIMESTAMP, RQ_OUTTIMESTAMP, SERVERID, ERROR, REQUESTID, THREADID, USERID, APPNAME, SESSIONID, IPCLIENT, USERAGENT, ACCEPTLANGUAGE, METHOD, URL) "
	+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** The Constant INSERTA_WS. Para pruebas locales quitar . al nombre de la tabla */
	private static final String INSERTA_WS = 
	"INSERT INTO ?.LOG_WS_PH_? "
	+ "(WS_INTIMESTAMP, WS_OUTTIMESTAMP, WSRETURNCODE, WSERROR, REQUESTID, WSALIAS, WSURL, WSMETHOD, WSTYPE) "
	+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** The listValues */
	private List<Object> values = null;
	
	/**
	 * Metodo bitacorizaRequest.
	 * Realiza INSERT a la tabla LOG_REQUEST_PH_
	 * 
	 * Tomando como referencia el contenedor 
	 * de Beans DTOMetricsBitacora
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param dtoBitacora 
	 * 			  dto con los parametros de peticion.
	 * @param tabla 
	 * 			  1 o 2 para indicar a que tabla se realiza el insert.
	 */
	@Override
	public void insertaRequest(DTOMetricsBitacora dtoBitacora, String tabla) {
		StringBuilder queryRequest = new StringBuilder(INSERTA_REQUEST);
		if(StringUtils.isBlank(MetricsSingleton.getInstance().getCFG().getInstancia())){
			queryRequest.replace(queryRequest.indexOf("?"), queryRequest.indexOf("?") + 2, "");
		}else{
			queryRequest.replace(queryRequest.indexOf("?"), queryRequest.indexOf("?") + 1, MetricsSingleton.getInstance().getCFG().getInstancia());
		}
		queryRequest.replace(queryRequest.indexOf("?"), queryRequest.indexOf("?") + 1, tabla);
		values = new ArrayList<Object>();
		values.add(new Timestamp(Long.valueOf(dtoBitacora.getInTimestamp())));
		values.add(new Timestamp(Long.valueOf(dtoBitacora.getOutTimestamp())));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getServerId(), 30));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getError().getError(), 1));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestId(),24));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequest().getThreadId(), 24));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestWeb().getUserId(), 20));
		values.add(MetricsUtil.validaCampo(Constantes.APP_NAME, 60));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestWeb().getSessionId(), 60));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestWeb().getIpClient(), 15));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestWeb().getUserAgent() , 300));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestWeb().getAcceptLanguage(), 14));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequest().getMethod(), 6));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequest().getUrl(), 200));
		
		try {
			MetricsIDA.insertar(queryRequest.toString(), values, MetricsIDA.CANAL_METRICS_DS, this.getClass().getName());
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_OK, dtoBitacora, 1);
		} catch (ExceptionDataAccess eda1) {
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_ERR, dtoBitacora, 1);
			LOG.error(eda1);
		}
	}

	/**
	 * Metodo bitacorizaError
	 * Realiza INSERT a la tabla LOG_ERROR_PH_
	 * 
	 * Tomando como referencia el contenedor 
	 * de Beans DTOMetricsBitacora
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param dtoBitacora 
	 * 			  dto con los parametros de error.
	 * @param tabla 
	 * 			  1 o 2 para indicar a que tabla se realiza el insert.
	 */
	@Override
	public void insertaError(DTOMetricsBitacora dtoBitacora, String tabla) {
		StringBuilder queryError = new StringBuilder(INSERTA_ERROR);
		if(StringUtils.isBlank(MetricsSingleton.getInstance().getCFG().getInstancia())){
			queryError.replace(queryError.indexOf("?"), queryError.indexOf("?") + 2, "");
		}else{
			queryError.replace(queryError.indexOf("?"), queryError.indexOf("?") + 1, MetricsSingleton.getInstance().getCFG().getInstancia());
		}
		queryError.replace(queryError.indexOf("?"), queryError.indexOf("?") + 1, tabla);
		values = new ArrayList<Object>();
		values.add(new Timestamp(Long.valueOf(dtoBitacora.getInTimestamp())));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getServerId(),30));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getRequestId(),30));
		values.add(MetricsUtil.validaCampo(Constantes.APP_NAME, 60));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getError().getDescription(), 200));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getError().getReturnCode(), 20));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getError().getTrace(), 3200));
		
		try {
			MetricsIDA.insertar(queryError.toString(), values, MetricsIDA.CANAL_METRICS_DS, this.getClass().getName());
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_OK, dtoBitacora, 2);
		} catch (ExceptionDataAccess eda2) {
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_ERR, dtoBitacora, 2);
			LOG.error(eda2);
		}
	}

	/**
	 * Metodo bitacorizaService
	 * Realiza INSERT a la tabla LOG_WS_PH_
	 * 
	 * Tomando como referencia el contenedor 
	 * de Beans DTOMetricsBitacora
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param dtoBitacora 
	 * 			  dto con los parametros para llamados externos WS.
	 * @param tabla 
	 * 			  1 o 2 para indicar a que tabla se realiza el insert.
	 */
	@Override
	public void insertaServicios(DTOMetricsBitacora dtoBitacora, String tabla) {
		StringBuilder queryWS = new StringBuilder(INSERTA_WS);
		if(StringUtils.isBlank(MetricsSingleton.getInstance().getCFG().getInstancia())){
			queryWS.replace(queryWS.indexOf("?"), queryWS.indexOf("?") + 2, "");
		}else{
			queryWS.replace(queryWS.indexOf("?"), queryWS.indexOf("?") + 1, MetricsSingleton.getInstance().getCFG().getInstancia());
		}
		queryWS.replace(queryWS.indexOf("?"), queryWS.indexOf("?") + 1, tabla);
		values = new ArrayList<Object>();
		values.add(new Timestamp(Long.valueOf(dtoBitacora.getInTimestamp())));
		values.add(new Timestamp(Long.valueOf(dtoBitacora.getOutTimestamp())));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getWebService().getWsReturnCode(), 3));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getError().getError(), 1));
		values.add(MetricsUtil.validaCampo(Constantes.APP_NAME, 24));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getWebService().getWsAlias(), 050));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getWebService().getWsUrl(), 200));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getWebService().getWsMethod(), 050));
		values.add(MetricsUtil.validaCampo(dtoBitacora.getWebService().getWsType(), 5));
		
		try {
			MetricsIDA.insertar(queryWS.toString(), values, MetricsIDA.CANAL_METRICS_DS, this.getClass().getName());
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_OK, dtoBitacora, 3);
		} catch (ExceptionDataAccess eda3) {
			UtileriasDAO.printBitacora(Constantes.MSG_INSERT_ERR, dtoBitacora, 3);
			LOG.error(eda3);
		}
	}
}
