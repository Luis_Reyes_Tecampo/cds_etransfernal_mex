/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BitacorizaMetricsImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.service.impl;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.metrics.constantes.CFGConstants;
import mx.isban.metrics.dto.DTOMetricsBitacora;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.service.GuardaMetricaDAO;
import mx.isban.metrics.util.MetricsSingleton;
import mx.isban.metrics.util.MetricsUtil;

/**
 * Session Bean implementation class BitacorizaMetricsImpl
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS
 * @since 2018 - 05 PLAN DELTA SUCURSALES
 */
@Stateless
@Remote(BitacorizaMetrics.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BitacorizaMetricsImpl implements BitacorizaMetrics{
	
	/** Servicio para guardar metricas*/
	@EJB 
	private transient GuardaMetricaDAO dao;
	/** Para la respuesta de configuracion*/
	private transient String resp = null;
	/** Para nuevo estatus del modulo*/
	private transient String estatus = null;
	
	/**
	 * Metodo para actualizar modulo metrics
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param estConfig de tipo String.
	 * @return estatus del modulo
	 */
	@Override
	public String configMetrics(String estConfig) {
		estatus = estConfig;
		if(estatus != null && !"".equals(estatus)) {
			validaOperacion();
		}else {
			resp = "Debe ingresar un valor para modificar estatus del modulo metrics [[ true / false ]]]  (DELTA) = Indica el estatus actual del modulo ";
		}
		return resp;
	}
	
	/**
	 * Metodo para bitacorizar request
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param request
	 * 			  bean con los parametros a bitacorizar
	 */
	@Override
	@Asynchronous
	public void bitacorizaRequest(DTOMetricsBitacora request) {
		if(MetricsUtil.validaRequest(request) && MetricsSingleton.getInstance().getCFG().isActivarMetrics()){
			dao.insertaRequest(request,String.valueOf(MetricsUtil.getTabla()));
		}
	}
	
	/**
	 * Metodo para bitacorizar error
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param error
	 * 			  bean con los parametros a bitacorizar
	 */
	@Override
	@Asynchronous
	public void bitacorizaError(DTOMetricsBitacora error) {
		if(MetricsUtil.validaError(error) && MetricsSingleton.getInstance().getCFG().isActivarMetrics()){
			dao.insertaError(error, String.valueOf(MetricsUtil.getTabla()));
		}
	}

	/**
	 * Metodo para bitacorizar ws
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param ws
	 * 			  bean con los parametros a bitacorizar
	 */
	@Override
	@Asynchronous
	public void bitacorizaWs(DTOMetricsBitacora ws) {
		if(MetricsUtil.validaWS(ws) && MetricsSingleton.getInstance().getCFG().isActivarMetrics()){
			dao.insertaServicios(ws, String.valueOf(MetricsUtil.getTabla()));
		}
	}
	
	/**
	 * Metodo para validar la operacion a realizar para modulo metrics.
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 */
	private void validaOperacion() {
		if("DELTA".equalsIgnoreCase(estatus)){
			getEstatus();
		}else {
			resp = MetricsSingleton.getInstance().guardarConfiguracion(Boolean.valueOf(estatus));
		}
	}
	
	/**
	 * Metodo para obtener estado del modulo metrics.
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 */
	private void getEstatus() {
		if(MetricsSingleton.getInstance().getCFG().isActivarMetrics()) {
			resp = CFGConstants.CFG_OK_A_INIT;
		}else {
			resp = CFGConstants.CFG_OK_D_INIT;
		}
	}

}
