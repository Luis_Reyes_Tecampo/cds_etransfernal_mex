/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.dao.DAOEjecutaQuery.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.dao;

import java.util.List;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;

/**
 * The Interface DAOEjecutaQuery.
 */
public interface DAOEjecutaQuery {

	/**
	 * Ejecuta querys.
	 *
	 * @param asb the asb
	 * @param bitacora the bitacora
	 * @param bean the bean
	 * @return the string
	 */
	String ejecutaQuerys(ArchitechSessionBean asb, BitacoraDTO bitacora, QuerysListDTO bean);

	/**
	 * Valida inhabil.
	 *
	 * @param asb the asb
	 * @param bitacora the bitacora
	 * @param query the query
	 * @param dataBase the data base
	 * @return true, if successful
	 */
	boolean validaInhabil(ArchitechSessionBean asb, BitacoraDTO bitacora, String query, String dataBase);

	/**
	 * Gets the querys bd.
	 *
	 * @param asb the asb
	 * @param bitacora the bitacora
	 * @param query the query
	 * @param dataBase the data base
	 * @return the querys bd
	 */
	List<QuerysListDTO> getQuerysBd(ArchitechSessionBean asb, BitacoraDTO bitacora, String query, String dataBase);

	/**
	 * Consulta bd.
	 *
	 * @param asb the asb
	 * @param bitacora the bitacora
	 * @param query the query
	 * @param dataBase the data base
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String consultaBd(ArchitechSessionBean asb, BitacoraDTO bitacora, String query, String dataBase)
			throws BusinessException;

	/**
	 * Consulta paginacion.
	 *
	 * @param asb the asb
	 * @param query the query
	 * @param reqTransfer the req transfer
	 * @param bitacora the bitacora
	 * @return the int
	 */
	int consultaPaginacion(ArchitechSessionBean asb, String query, ReqConsultaDTO reqTransfer, BitacoraDTO bitacora);

	/**
	 * Ejecuta querys programados.
	 *
	 * @param asb the asb
	 * @param bitacora the bitacora
	 * @param bean the bean
	 * @return the string
	 */
	String ejecutaQuerysProgramados(ArchitechSessionBean asb, BitacoraDTO bitacora, QuerysListDTO bean);
}
