/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCoreImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		10/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ejb.impl;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;
import mx.isban.csa.valtransfer.beans.ValInitCoreDTO;
import mx.isban.csa.valtransfer.dao.DAOEjecutaQuery;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;
import mx.isban.csa.valtransfer.util.UtilCoreValidacion;

/**
 * The Class BOValInitCoreImpl.
 */
@Singleton
@Local(BOValInitCore.class)
@Startup
public class BOValInitCoreImpl extends Architech implements BOValInitCore {

	/** The Constant PIPE. */
	private static final String PIPE = "|";

	/** The Constant SALTO_LINEA. */
	private static final String SALTO_LINEA = "\n";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8983427740243112415L;

	/** The cfg. */
	private ValInitCoreDTO cfg = new ValInitCoreDTO();

	/** The ejec query. */
	@EJB
	private DAOEjecutaQuery ejecQuery;

	/** The carga querys. */
	private boolean cargaQuerys;

	/** The monitor programados. */
	private boolean monitorProgramados;
	
	private boolean refresh; 

	/** The intentos. */
	private int intentos = 0;

	/** The programados. */
	private ResValTransferDTO programados = new ResValTransferDTO();

	/** The fecha. */
	private String fecha;
	
	/** The esquema. */
	private String esquema = "STPROD";

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		// funcion que al crear el componente carga en memoria los querys de inicio
		// cierre y monitoreo
		cargaQuerys = true;
		monitorProgramados = false;
		refresh = false;
		fecha = UtilCoreValidacion.getDay();
	}

	/**
	 * Valida configuracion.
	 */
	@Schedule(minute = "*/1", hour = "*", dayOfWeek = "*", dayOfMonth = "*", persistent = false)
	public void validaConfiguracion() {

		if (!fecha.equalsIgnoreCase(UtilCoreValidacion.getDay())) {
			fecha = UtilCoreValidacion.getDay();
			programados.setValidacion("");
		}

		// valida que la bandera para la lectura de los querys en bd este activa si hubo
		// error al consultar en bd valida los 3 reintentos programados
		if (cargaQuerys && intentos < 3) {
			cargaQuerysBD();
		}

		// si se cargaron los querys valida los querys que estan programados y los va
		// ejecutando
		if (monitorProgramados) {
			monitoreaProgramados(UtilCoreValidacion.getTime());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCore#obtieneConfiguracion(mx.isban
	 * .agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public ValInitCoreDTO obtieneConfiguracion(ArchitechSessionBean asb) throws BusinessException {
		return cfg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCore#actualizaConfiguracion(mx.
	 * isban.agave.commons.beans.ArchitechSessionBean)
	 */
	@Override
	public boolean actualizaConfiguracion(ArchitechSessionBean asb) {
		refresh = false;
		cargaQuerysBD();
		return refresh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCore#setBitacora(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public void setBitacora(ArchitechSessionBean asb, BitacoraDTO bitacora, String esquema) {
		this.esquema = esquema;
		cfg.setBitacora(bitacora);
		// imprime en log el detalle obtenido
		info("TRAN_VALINI || Cargando datos del servidor");
		info("Instancia WEB \t: " + cfg.getBitacora().getInstWeb());
		info("IP Address \t\t: " + cfg.getBitacora().getIpClient());
		info("Hostname \t\t:" + cfg.getBitacora().getHostname());
		info("Codigo Operacion: " + cfg.getBitacora().getOperacion().getCodOper());
		info("Servicio \t\t: " + cfg.getBitacora().getOperacion().getServTran());
		info("Tipo Operacion \t: " + cfg.getBitacora().getOperacion().getTipoOper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCore#getProgramados(mx.isban.agave
	 * .commons.beans.ArchitechSessionBean)
	 */
	@Override
	public ResValTransferDTO getProgramados(ArchitechSessionBean asb) {
		return programados;
	}

	/**
	 * Monitorea programados.
	 *
	 * @param hrValidacion the hr validacion
	 */
	private void monitoreaProgramados(String hrValidacion) {
		cfg.setBitacora(UtilCoreValidacion.llenaBitCargaBd(cfg));
		for (QuerysListDTO bean : cfg.getListQuerys()) {
			if (hrValidacion.trim().equalsIgnoreCase(bean.getDetalle().getHrEjec().trim())) {
				programados.setValidacion(ejecProgramado(hrValidacion, bean.getQuery(), bean.getDescQuery()));
			}
		}

	}

	/**
	 * Ejec programado.
	 *
	 * @param hrValidacion the hr validacion
	 * @param producto     the producto
	 * @param grpEjec      the grp ejec
	 * @return the string
	 */
	private String ejecProgramado(String hrValidacion, String producto, String grpEjec) {
		StringBuilder prog = new StringBuilder();
		if (programados.getValidacion() != null) {
			prog.append(programados.getValidacion());
		}

		for (QuerysListDTO bean : cfg.getListQuerys()) {
			if (bean.getProducto().equalsIgnoreCase(producto)
					&& bean.getDetalle().getGrpEjec().equalsIgnoreCase(grpEjec)) {
				cfg.getBitacora().getTablaAfecatada().setValAnt(bean.getQuery());
				cfg.getBitacora().getTablaAfecatada().setValNvo("PROGRAMADO");
				cfg.getBitacora().getTablaAfecatada().setDatoFijo(hrValidacion);
				prog.append(bean.getProducto()).append("_").append(hrValidacion).append(PIPE)
						.append(bean.getDescQuery()).append(SALTO_LINEA);
				prog.append(ejecQuery.ejecutaQuerysProgramados(getArchitechBean(), cfg.getBitacora(), bean));
			}
		}
		return prog.toString();
	}

	/**
	 * Carga querys BD.
	 */
	private void cargaQuerysBD() {
		StringBuilder query = new StringBuilder(KtesGlobal.QUERY_TRANVAL);
		query.replace(query.indexOf("?"), query.indexOf("?")+1, esquema);
		
		// inicializa la bitacora para la consulta a base de datos
		cfg.setBitacora(UtilCoreValidacion.llenaBitCargaBd(cfg));
		// realiza la consulta a bd y carga el listado en memoria
		cfg.setListQuerys(
				ejecQuery.getQuerysBd(getArchitechBean(), cfg.getBitacora(), query.toString(), KtesGlobal.GFI));
		// si no trajo resultados deja activas las banderas para cargar y
		// mostrar los querys e incrementa en numero de reintentos para leer los datos
		if (cfg.getListQuerys().isEmpty()) {
			intentos++;
		} else {
			cargaQuerys = false;
			monitorProgramados = true;
			refresh = true;
		}
	}
}
