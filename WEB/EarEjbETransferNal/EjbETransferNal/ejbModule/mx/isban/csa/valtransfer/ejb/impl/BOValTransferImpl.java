/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.ejb.impl.BOValTransferImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.beans.ResConsultaDbDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;
import mx.isban.csa.valtransfer.beans.ValInitCoreDTO;
import mx.isban.csa.valtransfer.dao.DAOEjecutaQuery;
import mx.isban.csa.valtransfer.ejb.BOValTransfer;
import mx.isban.csa.valtransfer.ejb.BOValidacion;
import mx.isban.csa.valtransfer.ktes.ECatalogoError;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;
import mx.isban.csa.valtransfer.util.UtilValTransfer;
import mx.isban.csa.valtransfer.util.UtilValidador;

/**
 * The Class BOValTransferImpl.
 */
@Stateless
@Remote(BOValTransfer.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOValTransferImpl extends Architech implements BOValTransfer {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8581999358490657687L;

	/** The core. */
	@EJB
	private BOValInitCore core;

	/** The bo validacion. */
	@EJB
	private BOValidacion boValidacion;

	/** The ejecutor. */
	@EJB
	private DAOEjecutaQuery ejecutor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValTransfer#monitor(mx.isban.agave.commons.
	 * beans.ArchitechSessionBean, mx.isban.csa.valtransfer.beans.ReqValidacionDTO,
	 * java.lang.String)
	 */
	@Override
	public ResValTransferDTO monitor(ArchitechSessionBean asb, ReqValidacionDTO reqTransfer, String ipRemota)
			throws BusinessException {
		// obtine tiempo de inicio para atender la peticion
		long inTime = System.currentTimeMillis();
		// genera obj de respuesta
		ResValTransferDTO monitor = new ResValTransferDTO();
		List<QuerysListDTO> usuarios = core.obtieneConfiguracion(getArchitechBean()).getListQuerys();
		
		if (UtilValidador.validaUsuario(usuarios, reqTransfer.getUsuario())) {
			// Se inicializa la bitacora
			BitacoraDTO bitacora = core.obtieneConfiguracion(getArchitechBean()).getBitacora();
			UtilValTransfer.creaBitacora(bitacora, reqTransfer.getUsuario(), ipRemota);

			// Valida el tipo de peticion
			UtilValidador.validaModulos(reqTransfer.getValidacion(), reqTransfer.getModulo(), reqTransfer.getUsuario());
			// Se ejecuta el servicio de monitoreo
			monitor.setValidacion(UtilValTransfer.invocaPeticion(boValidacion, bitacora, reqTransfer, getArchitechBean()));
			// en caso de exito coloca codigo y msg de error
			monitor.setCodeError(ECatalogoError.VTINI000.getCodeError());
			monitor.setMsgError(ECatalogoError.VTINI000.getMsgError());
		} else {
			// se informa que el usuario no tiene permisos para uso del servicio
			monitor.setCodeError(ECatalogoError.VTVAL007.getCodeError());
			monitor.setMsgError(ECatalogoError.VTVAL007.getMsgError());
		}

		// asigna tiempo de respuesta
		monitor.setTiempoEjecucion(UtilValidador.getTiempoEjec(inTime));
		// regresa respuesta del servicio
		return monitor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValTransfer#consulta(mx.isban.agave.commons.
	 * beans.ArchitechSessionBean, mx.isban.csa.valtransfer.beans.ReqConsultaDTO,
	 * java.lang.String)
	 */
	@Override
	public ResConsultaDbDTO consulta(ArchitechSessionBean asb, ReqConsultaDTO reqTransfer, String ipRemota)
			throws BusinessException {
		// obtine tiempo de inicio para atender la peticion
		long inTime = System.currentTimeMillis();
		// genera obj de respuesta
		ResConsultaDbDTO consulta = new ResConsultaDbDTO();
		ValInitCoreDTO cfg = core.obtieneConfiguracion(getArchitechBean());

		if (UtilValidador.validaUsuario(cfg.getListQuerys(), reqTransfer.getUser())) {
			UtilValTransfer.creaBitacora(cfg.getBitacora(), reqTransfer.getUser(), ipRemota);
			ejecutaEnvio(reqTransfer, consulta, cfg);
		} else {
			// se informa que el usuario no tiene permisos para uso del servicio
			consulta.setCodeErrConsulta(ECatalogoError.VTVAL007.getCodeError());
			consulta.setMsgErrConsulta(ECatalogoError.VTVAL007.getMsgError());
		}

		// asigna tiempo de respuesta
		consulta.setTiempoEjecucion(UtilValidador.getTiempoEjec(inTime));
		// regresa respuesta del servicio
		return consulta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValTransfer#setBitacoraSingle(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public void setBitacoraSingle(ArchitechSessionBean asb, BitacoraDTO bitacora, String esquema) {
		// se utiliza para tener en memoria el host e instancia web del equipo
		core.setBitacora(getArchitechBean(), bitacora, esquema);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.isban.csa.valtransfer.ejb.BOValTransfer#cargaQuerysBd(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean, java.lang.String)
	 */
	@Override
	public String cargaQuerysBd(ArchitechSessionBean architechBean, String ipRemota)
			throws BusinessException {
		BitacoraDTO bitacora = core.obtieneConfiguracion(getArchitechBean()).getBitacora();
		String response = "Fallo la actualizacion de querys";
		if(core.actualizaConfiguracion(getArchitechBean())) {
			response = "Se actualizan querys en memoria para instancia web :: " + bitacora.getInstWeb();
		}
		// regresa respuesta del servicio
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValTransfer#consultaProgramados(mx.isban.agave
	 * .commons.beans.ArchitechSessionBean)
	 */
	@Override
	public ResValTransferDTO consultaProgramados(ArchitechSessionBean architechBean) {
		// genera obj de respuestas
		ResValTransferDTO response = core.getProgramados(getArchitechBean());
		// en caso de exito coloca codigo y msg de error
		response.setCodeError(ECatalogoError.VTINI000.getCodeError());
		response.setMsgError(ECatalogoError.VTINI000.getMsgError());
		// asigna tiempo de respuesta
		// regresa respuesta del servicio
		return response;
	}

	/**
	 * Valida paginacion.
	 *
	 * @param reqTransfer the req transfer
	 * @param bitacora    the bitacora
	 * @return the int
	 */
	private int validaPaginacion(ReqConsultaDTO reqTransfer, BitacoraDTO bitacora) {
		StringBuilder sbAux = new StringBuilder();
		int paginas;
		int operaciones = 0;

		// se obtiene query de la peticion
		sbAux.append(reqTransfer.getQuery().toUpperCase());
		// se reemplaza query para obtener el total y la paginacion
		sbAux.replace(0, sbAux.toString().indexOf(KtesGlobal.FROM), "select count(*) as TOT_REG ");
		// Se ejecuta la validacion del numero de operaciones que se obtendra de la
		// consulta
		operaciones = ejecutor.consultaPaginacion(getArchitechBean(), sbAux.toString(), reqTransfer, bitacora);
		paginas = operaciones / 80;
		return paginas;
	}

	/**
	 * Ejecuta envio.
	 *
	 * @param reqTransfer the req transfer
	 * @param consulta    the consulta
	 * @param cfg         the cfg
	 * @throws BusinessException the business exception
	 */
	private void ejecutaEnvio(ReqConsultaDTO reqTransfer, ResConsultaDbDTO consulta, ValInitCoreDTO cfg)
			throws BusinessException {
		int paginas = validaPaginacion(reqTransfer, cfg.getBitacora());
		String queryPaginacion;
		queryPaginacion = getQueryPaginacion(reqTransfer, paginas, consulta);
		consulta.setValidacion(
				ejecutor.consultaBd(getArchitechBean(), cfg.getBitacora(), queryPaginacion, reqTransfer.getDataBase()));
		// en caso de exito coloca codigo y msg de error
		consulta.setCodeErrConsulta(ECatalogoError.VTINI000.getCodeError());
		consulta.setMsgErrConsulta(ECatalogoError.VTINI000.getMsgError());
	}

	/**
	 * Gets the query paginacion.
	 *
	 * @param reqTransfer the req transfer
	 * @param paginas     the paginas
	 * @param ejecucion   the ejecucion
	 * @return the query paginacion
	 */
	private String getQueryPaginacion(ReqConsultaDTO reqTransfer, int paginas, ResConsultaDbDTO consulta) {
		StringBuilder queryAux = new StringBuilder(reqTransfer.getQuery());
		StringBuilder query = new StringBuilder();
		int pagInicio = 0;
		int pagFin = 0;
		queryAux.replace(queryAux.indexOf(KtesGlobal.FROM), queryAux.indexOf(KtesGlobal.FROM) + 4,
				",ROW_NUMBER() OVER(ORDER BY 1 ASC) LINE_NUMBER FROM ");
		query.append("SELECT P.* FROM (").append(queryAux.toString())
				.append(") P WHERE LINE_NUMBER BETWEEN ? AND ? ORDER BY LINE_NUMBER");

		// Se valida que la pagina enviada en la peticion sea menor igual a la ultima
		// pagina
		if (reqTransfer.getPaginacion() <= paginas) {
			pagInicio = reqTransfer.getPaginacion() * 80;
			pagFin = (reqTransfer.getPaginacion() * 80) + 80;
		} else {
			pagInicio = 0;
			pagFin = 80;
		}

		// se genera el query para envio a capa DAO
		query.replace(query.indexOf("?"), query.indexOf("?") + 1, String.valueOf((pagInicio)));
		query.replace(query.indexOf("?"), query.indexOf("?") + 1, String.valueOf(pagFin));
		debug(query.toString());

		consulta.setPaginas(
				String.format(ECatalogoError.VTVAL005.getMsgError(), reqTransfer.getPaginacion(), paginas));

		return query.toString();
	}
}
