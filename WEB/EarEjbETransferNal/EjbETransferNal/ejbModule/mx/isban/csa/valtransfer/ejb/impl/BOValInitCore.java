/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.ejb.impl.BOValInitCore.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		7/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */

package mx.isban.csa.valtransfer.ejb.impl;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;
import mx.isban.csa.valtransfer.beans.ValInitCoreDTO;

/**
 * The Interface BOValInitCore.
 */
public interface BOValInitCore {

	/**
	 * Obtiene configuracion.
	 *
	 * @param asb the asb
	 * @return the val init core DTO
	 * @throws BusinessException the business exception
	 */
	ValInitCoreDTO obtieneConfiguracion(ArchitechSessionBean asb) throws BusinessException;

	/**
	 * Sets the bitacora.
	 *
	 * @param asb      the asb
	 * @param bitacora the bitacora
	 */
	void setBitacora(ArchitechSessionBean asb, BitacoraDTO bitacora, String esquema);

	/**
	 * Gets the programados.
	 *
	 * @param asb the asb
	 * @return the programados
	 */
	ResValTransferDTO getProgramados(ArchitechSessionBean asb);

	/**
	 * Actualiza configuracion.
	 *
	 * @param asb the asb
	 * @return the val init core DTO
	 */
	boolean actualizaConfiguracion(ArchitechSessionBean asb);

}
