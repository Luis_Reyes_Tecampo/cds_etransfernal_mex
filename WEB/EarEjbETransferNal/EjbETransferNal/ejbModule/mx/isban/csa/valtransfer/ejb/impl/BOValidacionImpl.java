/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.ejb.impl.BOValidacionImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ejb.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.dao.DAOEjecutaQuery;
import mx.isban.csa.valtransfer.ejb.BOValidacion;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;
import mx.isban.csa.valtransfer.util.UtilCoreValidacion;

/**
 * The Class BOValidacionImpl.
 */
@Stateless
@Local(BOValidacion.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOValidacionImpl extends Architech implements BOValidacion {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1903302586963967607L;

	/** The core. */
	@EJB
	private BOValInitCore core;

	/** The ejecutor. */
	@EJB
	private DAOEjecutaQuery ejecutor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValidacion#getInicioTransfer(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.ReqValidacionDTO,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public String getInicioTransfer(ArchitechSessionBean asb, ReqValidacionDTO validacion,
			BitacoraDTO bitacora) throws BusinessException {
		String response = null;
		List<QuerysListDTO> listado = core.obtieneConfiguracion(getArchitechBean()).getListQuerys();

		// valida si se especifico el modulo
		if ("ALL".equalsIgnoreCase(validacion.getModulo())) {
			response = enviaMonitor(listado, bitacora, "1");
		} else {
			response = envMonXModulo(listado, bitacora, "1", validacion.getModulo());
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValidacion#getCierreTransfer(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.ReqValidacionDTO,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public String getCierreTransfer(ArchitechSessionBean asb, ReqValidacionDTO validacion,
			BitacoraDTO bitacora) throws BusinessException {
		String response = null;
		List<QuerysListDTO> listado = core.obtieneConfiguracion(getArchitechBean()).getListQuerys();

		// valida si se especifico el modulo
		if ("ALL".equalsIgnoreCase(validacion.getModulo())) {
			response = enviaMonitor(listado, bitacora, "3");
		} else {
			response = envMonXModulo(listado, bitacora, "3", validacion.getModulo());
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.ejb.BOValidacion#getMonitoreo(mx.isban.agave.commons
	 * .beans.ArchitechSessionBean, mx.isban.csa.valtransfer.beans.ReqValidacionDTO,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public String getMonitoreo(ArchitechSessionBean asb, ReqValidacionDTO validacion,
			BitacoraDTO bitacora) throws BusinessException {
		String response = null;
		List<QuerysListDTO> listado = core.obtieneConfiguracion(getArchitechBean()).getListQuerys();

		// valida si se especifico el modulo
		if ("ALL".equalsIgnoreCase(validacion.getModulo())) {
			response = enviaMonitor(listado, bitacora, "2");
		} else {
			response = envMonXModulo(listado, bitacora, "2", validacion.getModulo());
		}
		return response;
	}

	/**
	 * Env mon X modulo.
	 *
	 * @param listado the listado
	 * @param bitacora the bitacora
	 * @param grpEjec the grp ejec
	 * @param modulo the modulo
	 * @return the string
	 */
	private String envMonXModulo(List<QuerysListDTO> listado, BitacoraDTO bitacora, String grpEjec,
			String modulo) {
		StringBuilder response = new StringBuilder();
		boolean habil = valDiaHabil(bitacora);
		for (QuerysListDTO bean : listado) {
			if (grpEjec.equalsIgnoreCase(bean.getDetalle().getGrpEjec()) && modulo.equalsIgnoreCase(bean.getProducto())
					&& !KtesGlobal.MONPLUS.equalsIgnoreCase(bean.getProducto())) {
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getDescQuery()).append("\n");
				response.append(validaEnvio(bitacora, bean, habil)).append("\n");
			}
			if (KtesGlobal.MONPLUS.equalsIgnoreCase(bean.getProducto())
					&& KtesGlobal.MONPLUS.equalsIgnoreCase(modulo)) {
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getDescQuery()).append("\n");
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getQuery()).append("\n");
			}
		}
		return response.toString();
	}

	/**
	 * Envia monitor.
	 *
	 * @param listado the listado
	 * @param bitacora the bitacora
	 * @param grpEjec the grp ejec
	 * @return the list
	 */
	private String enviaMonitor(List<QuerysListDTO> listado, BitacoraDTO bitacora, String grpEjec) {
		StringBuilder response = new StringBuilder();
		boolean habil = ejecutor.validaInhabil(getArchitechBean(), bitacora, KtesGlobal.QUERY_INHABIL_DAY,
				KtesGlobal.GFI);
		for (QuerysListDTO bean : listado) {
			if (grpEjec.equalsIgnoreCase(bean.getDetalle().getGrpEjec())
					&& !KtesGlobal.MONPLUS.equalsIgnoreCase(bean.getProducto())) {
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getDescQuery()).append("\n");
				response.append(validaEnvio(bitacora, bean, habil)).append("\n");
			}

			if (KtesGlobal.MONPLUS.equalsIgnoreCase(bean.getProducto())) {
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getDescQuery()).append("\n");
				response.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |").append(bean.getQuery()).append("\n");
			}

		}
		return response.toString();
	}

	/**
	 * Valida envio.
	 *
	 * @param bitacora the bitacora
	 * @param bean the bean
	 * @param habil the habil
	 * @return the string
	 */
	private String validaEnvio(BitacoraDTO bitacora, QuerysListDTO bean, boolean habil) {
		String response = null;

		// Valida si es TEF y no es dia habil
		if ("TEF".equalsIgnoreCase(bean.getProducto()) || "CUT".equalsIgnoreCase(bean.getProducto())) {
			response = validaEnvioTefCut(bitacora, habil, bean);
		} else {
			response = ejecutor.ejecutaQuerys(getArchitechBean(), bitacora, bean);
		}
		return response;
	}

	/**
	 * Valida envio tef cut.
	 *
	 * @param bitacora the bitacora
	 * @param habil the habil
	 * @param bean the bean
	 * @return the string
	 */
	private String validaEnvioTefCut(BitacoraDTO bitacora, boolean habil, QuerysListDTO bean) {
		if (habil) {
			return ejecutor.ejecutaQuerys(getArchitechBean(), bitacora, bean);
		} else {
			return null;
		}
	}

	/**
	 * Val dia habil.
	 *
	 * @param bitacora the bitacora
	 * @return true, if successful
	 */
	private boolean valDiaHabil(BitacoraDTO bitacora) {
		// Se agrega bandera de validacion para dia inhabil TEF
		boolean response = true;
		// Se agrega Listado de dias
		String[] dias = { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
		// Se agregan obj para obtener fecha
		Date hoy = new Date();
		int numeroDia = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(hoy);
		// Se obtiene el numero de dia
		numeroDia = cal.get(Calendar.DAY_OF_WEEK);
		// Se valida que no sea Sabado ni Domingo
		if ("Sabado".equalsIgnoreCase(dias[numeroDia - 1]) || "Domingo".equalsIgnoreCase(dias[numeroDia - 1])) {
			// Se indica que no se debe validar TEF
			response = false;
		} else {
			// En caso de no ser sabado ni domingo se valida que el dia de la semana sea
			// habil
			response = ejecutor.validaInhabil(getArchitechBean(), bitacora, KtesGlobal.QUERY_INHABIL_DAY,
					KtesGlobal.GFI);
		}
		return response;
	}

}
