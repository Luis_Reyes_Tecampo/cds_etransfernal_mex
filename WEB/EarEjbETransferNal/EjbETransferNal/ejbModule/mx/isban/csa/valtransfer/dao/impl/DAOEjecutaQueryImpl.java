/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNal]
 * mx.isban.csa.valtransfer.dao.impl.DAOEjecutaQueryImpl.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		7/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysDetDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.dao.DAOEjecutaQuery;
import mx.isban.csa.valtransfer.ktes.ECatalogoError;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;
import mx.isban.csa.valtransfer.util.UtilBitacora;
import mx.isban.csa.valtransfer.util.UtilCoreValidacion;
import mx.isban.eTransferNal.dao.comun.DAOBitAdministrativa;
import mx.isban.eTransferNal.helper.HelperDAO;

/**
 * The Class DAOEjecutaQueryImpl.
 */
@Stateless
@Local(DAOEjecutaQuery.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOEjecutaQueryImpl extends Architech implements DAOEjecutaQuery {

	/** The Constant VALIDACION. */
	private static final String VALIDACION = "VALIDACION";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 766076064571060292L;

	/** The dao pista auditoria. */
	@EJB
	private DAOBitAdministrativa daoPistaAuditoria;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#ejecutaQuerys(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String ejecutaQuerys(ArchitechSessionBean asb, BitacoraDTO bitEjecucion, QuerysListDTO bean) {
		// Se crea obj de respuesta para BD
		ResponseMessageDataBaseDTO responseHelper1 = null;
		List<HashMap<String, Object>> listResultados;
		// Se crea obj List para respuesta
		String datasource = HelperDAO.CANAL_GFI_DS;
		// Se inicializan variables para ejecucion
		StringBuilder responseEjec = new StringBuilder();

		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(bean.getDetalle().getEjecucion())) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}

			responseHelper1 = HelperDAO.consultar(bean.getQuery(), new ArrayList<Object>(), datasource,
					this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseHelper1.getResultQuery() != null && !responseHelper1.getResultQuery().isEmpty()) {
				// Se obtiene los registros
				listResultados = responseHelper1.getResultQuery();
				for (HashMap<String, Object> map : listResultados) {
					responseEjec.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
							.append(" |").append(String.valueOf(map.get(VALIDACION))).append("\n");
				}
				// Se bitacoriza ejecucion exitosa
				bitEjecucion.getOperacion().setCodOper(ECatalogoError.VTINI000.getCodeError());
				bitEjecucion.getOperacion().setComentarios(ECatalogoError.VTINI000.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitEjecucion), getArchitechBean());
			} else {
				// se indica que no se tubo exito en la ejecucion
				responseEjec.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
						.append(" |").append(ECatalogoError.VTDAE001.getCodeError()).append(" - ")
						.append(ECatalogoError.VTDAE001.getMsgError()).append("\n");
				// Se bitacoriza error de no optener datos
				bitEjecucion.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitEjecucion.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitEjecucion), getArchitechBean());
			}
		} catch (ExceptionDataAccess eda1) {
			// Se informa en log un posible error de ejecucion en BD
			showException(eda1);
			responseEjec.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
			.append(" |").append(eda1.getCode()).append(" :: ").append(eda1.getMessage()).append("\n");
			// Se bitacoriza error
			bitEjecucion.getOperacion().setCodOper(eda1.getCode());
			bitEjecucion.getOperacion().setComentarios(eda1.getMessage());
			// se ejecuta query de bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitEjecucion), getArchitechBean());
		}
		return responseEjec.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#ejecutaQuerysProgramados(mx.
	 * isban.agave.commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO, java.lang.String,
	 * java.lang.String, java.util.List)
	 */
	@Override
	public String ejecutaQuerysProgramados(ArchitechSessionBean asb, BitacoraDTO bitProgramados, QuerysListDTO bean) {
		// Se crea obj de respuesta para BD
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> list;
		// Se crea obj List para respuesta
		String datasource = HelperDAO.CANAL_GFI_DS;
		// Se inicializan variables para ejecucion
		StringBuilder responseEjecQuery = new StringBuilder("\n");

		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(bean.getDetalle().getEjecucion())) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}

			responseDTO = HelperDAO.consultar(bean.getQuery(), new ArrayList<Object>(), datasource, this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				// Se obtiene los registros
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					responseEjecQuery.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
					.append(" |").append(String.valueOf(map.get(VALIDACION))).append("\n");
				}
				// Se bitacoriza ejecucion exitosa
				bitProgramados.getOperacion().setCodOper(ECatalogoError.VTINI000.getCodeError());
				bitProgramados.getOperacion().setComentarios(ECatalogoError.VTINI000.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitProgramados),
						getArchitechBean());
			} else {
				// se indica que no se tubo exito en la ejecucion
				responseEjecQuery.append(bean.getProducto()).append(ECatalogoError.VTDAE001.getCodeError()).append("_").append(UtilCoreValidacion.getTime())
				.append(" |")
						.append(ECatalogoError.VTDAE001.getMsgError()).append("\n");
				// Se bitacoriza error de no optener datos
				bitProgramados.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitProgramados.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitProgramados),
						getArchitechBean());
			}
		} catch (ExceptionDataAccess eda2) {
			// Se informa en log un posible error de ejecucion en BD
			showException(eda2);
			responseEjecQuery.append(bean.getProducto()).append("_").append(UtilCoreValidacion.getTime())
			.append(" |").append(eda2.getCode()).append(" :: ").append(eda2.getMessage()).append("\n");
			// Se bitacoriza error
			bitProgramados.getOperacion().setCodOper(eda2.getCode());
			bitProgramados.getOperacion().setComentarios(eda2.getMessage());
			// se ejecuta query de bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitProgramados), getArchitechBean());
		}
		return responseEjecQuery.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#validaInhabil(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean validaInhabil(ArchitechSessionBean asb, BitacoraDTO bitInhabil, String query, String dataBase) {
		debug("Validando dia inhabil. . .");
		// Se crea obj de respuesta para BD
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> list;
		String datasource = HelperDAO.CANAL_GFI_DS;
		// Se inicializan variables para ejecucion
		boolean resp = false;
		String val = "";
		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(dataBase)) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}
			responseDTO = HelperDAO.consultar(query, new ArrayList<Object>(), datasource, this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				// Se obtiene los registros
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					val = String.valueOf(map.get(VALIDACION));
				}

				if ("OK".equalsIgnoreCase(val.trim())) {
					resp = true;
				}
				// Se bitacoriza ejecucion exitosa
				bitInhabil.getOperacion().setCodOper(ECatalogoError.VTINI000.getCodeError());
				bitInhabil.getOperacion().setComentarios(ECatalogoError.VTINI000.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitInhabil), getArchitechBean());
			} else {
				// Se bitacoriza error de no optener datos
				bitInhabil.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitInhabil.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitInhabil), getArchitechBean());
			}
		} catch (ExceptionDataAccess eda3) {
			// Se informa en log un posible error de ejecucion en BD
			showException(eda3);
			// Se bitacoriza error
			bitInhabil.getOperacion().setCodOper(eda3.getCode());
			bitInhabil.getOperacion().setComentarios(eda3.getMessage());
			// se ejecuta query de bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitInhabil), getArchitechBean());
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#getQuerysBd(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<QuerysListDTO> getQuerysBd(ArchitechSessionBean asb, BitacoraDTO bitCargaQuerys, String query,
			String dataBase) {
		// Se crea obj de respuesta para BD
		ResponseMessageDataBaseDTO responseDTO = null;
		List<HashMap<String, Object>> list;
		// Se crea obj List para respuesta
		List<QuerysListDTO> respCargaQuerys = new ArrayList<QuerysListDTO>();
		QuerysListDTO queryBean = null;
		QuerysDetDTO detalle = null;
		String datasource = HelperDAO.CANAL_GFI_DS;

		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(dataBase)) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}

			responseDTO = HelperDAO.consultar(query, new ArrayList<Object>(), datasource, this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				// Se obtiene los registros
				list = responseDTO.getResultQuery();
				for (HashMap<String, Object> map : list) {
					detalle = new QuerysDetDTO();
					detalle.setEjecucion(String.valueOf(map.get("EJECUCION")));
					detalle.setGrpEjec(String.valueOf(map.get("GRUPO_EJEC")));
					detalle.setHrEjec(String.valueOf(map.get("HORA_EJEC")));
					queryBean = new QuerysListDTO();
					queryBean.setDetalle(detalle);
					queryBean.setDescQuery(String.valueOf(map.get("DESC_QUERY")));
					queryBean.setProducto(String.valueOf(map.get("PRODUCTO")));
					queryBean.setQuery(String.valueOf(map.get("QUERY_PRINC")));
					respCargaQuerys.add(queryBean);
				}
				// Se bitacoriza ejecucion exitosa
				bitCargaQuerys.getOperacion().setCodOper(ECatalogoError.VTINI000.getCodeError());
				bitCargaQuerys.getOperacion().setComentarios(ECatalogoError.VTINI000.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitCargaQuerys),
						getArchitechBean());
			} else {
				// se indica que no se tubo exito en la ejecucion
				// Se bitacoriza error de no optener datos
				bitCargaQuerys.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitCargaQuerys.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				// se ejecuta query de bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitCargaQuerys),
						getArchitechBean());
			}
		} catch (ExceptionDataAccess eda4) {
			// Se informa en log un posible error de ejecucion en BD
			showException(eda4);
			// Se bitacoriza error
			bitCargaQuerys.getOperacion().setCodOper(eda4.getCode());
			bitCargaQuerys.getOperacion().setComentarios(eda4.getMessage());
			// se ejecuta query de bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitCargaQuerys), getArchitechBean());
		}
		return respCargaQuerys;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#consultaBd(mx.isban.agave.
	 * commons.beans.ArchitechSessionBean,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String consultaBd(ArchitechSessionBean asb, BitacoraDTO bitConsulta, String query, String dataBase)
			throws BusinessException {
		// inicializa variables de respuesta BD
		ResponseMessageDataBaseDTO responseDTO = null;
		StringBuilder responseDb = new StringBuilder("\n");

		String datasource = HelperDAO.CANAL_GFI_DS;
		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(dataBase)) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}
			// Envio y respuesta de query
			responseDTO = HelperDAO.consultar(query, new ArrayList<Object>(), datasource, this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				obtieneDatos(responseDTO, responseDb, bitConsulta);
			} else {
				bitConsulta.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitConsulta.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitConsulta), getArchitechBean());
			}
		} catch (ExceptionDataAccess eda5) {
			// Muestra el error en log
			showException(eda5);
			// bitacoriza el error de la ejecucion
			bitConsulta.getOperacion().setCodOper(eda5.getCode());
			bitConsulta.getOperacion().setComentarios(eda5.getMessage());
			// Ejecuta insert a bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitConsulta), getArchitechBean());
			if (eda5.getCode().equalsIgnoreCase(ECatalogoError.VTVAL005.getCodeError())) {
				throw new BusinessException(ECatalogoError.VTVAL005.getCodeError(),
						String.format(ECatalogoError.VTVAL005.getMsgError()));
			}
		}
		return responseDb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.csa.valtransfer.dao.DAOEjecutaQuery#consultaPaginacion(mx.isban.
	 * agave.commons.beans.ArchitechSessionBean, java.lang.String,
	 * mx.isban.csa.valtransfer.beans.ReqConsultaDTO,
	 * mx.isban.csa.valtransfer.beans.BitacoraDTO)
	 */
	@Override
	public int consultaPaginacion(ArchitechSessionBean asb, String query, ReqConsultaDTO reqTransfer,
			BitacoraDTO bitacora) {
		// genera los obj de respuesta para la peticion a base de datos
		List<HashMap<String, Object>> list = null;
		ResponseMessageDataBaseDTO responseDTO = null;
		int response = 50;

		String datasource = HelperDAO.CANAL_GFI_DS;
		// Ejecucion
		try {
			// Envio y respuesta de query
			if (KtesGlobal.TRPLUS.equalsIgnoreCase(reqTransfer.getDataBase())) {
				datasource = HelperDAO.CANAL_TRANSFER_PLUS;
			}
			// Envio y respuesta de query
			responseDTO = HelperDAO.consultar(query, new ArrayList<Object>(), datasource, this.getClass().getName());
			// Se valida que la respuesta contenga datos
			if (responseDTO.getResultQuery() != null && !responseDTO.getResultQuery().isEmpty()) {
				list = responseDTO.getResultQuery();
				// Se recorreo lista de registros
				for (HashMap<String, Object> map : list) {
					response = Integer.parseInt(String.valueOf(map.get("TOT_REG")));
				}
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitacora), getArchitechBean());
			} else {
				// Muestra el error en log
				bitacora.getOperacion().setCodOper(ECatalogoError.VTDAE001.getCodeError());
				bitacora.getOperacion().setComentarios(ECatalogoError.VTDAE001.getMsgError());
				// Ejecuta insert a bitacora
				daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitacora), getArchitechBean());
			}
		} catch (ExceptionDataAccess eda6) {
			// Muestra el error en log
			showException(eda6);
			// bitacoriza el error de la ejecucion
			bitacora.getOperacion().setCodOper(eda6.getCode());
			bitacora.getOperacion().setComentarios(eda6.getMessage());
			// Ejecuta insert a bitacora
			daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitacora), getArchitechBean());
		}
		return response;
	}

	/**
	 * Obtiene datos.
	 *
	 * @param responseDTO the response DTO
	 * @param response    the response
	 * @param linea       the linea
	 * @param bitacora    the bitacora
	 * @throws BusinessException the business exception
	 */
	private void obtieneDatos(ResponseMessageDataBaseDTO responseDTO, StringBuilder linea, BitacoraDTO bitacora) {
		List<HashMap<String, Object>> list;
		list = responseDTO.getResultQuery();

		// Se recorreo lista de registros
		for (HashMap<String, Object> map : list) {
			recorreCampos(linea, map);
		}
		daoPistaAuditoria.guardaBitacora(UtilBitacora.createBitacoraBitAdmin(bitacora), getArchitechBean());
	}

	/**
	 * Recorre campos.
	 *
	 * @param response the response
	 * @param linea    the linea
	 * @param map      the map
	 */
	private void recorreCampos(StringBuilder linea, HashMap<String, Object> map) {
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			linea.append(entry.getKey()).append("|").append(String.valueOf(entry.getValue())).append("|");
		}
		linea.append("\n");
	}

}
