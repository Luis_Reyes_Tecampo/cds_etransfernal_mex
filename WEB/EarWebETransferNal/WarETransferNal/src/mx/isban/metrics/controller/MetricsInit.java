/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * MetricsInit.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.controller;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import mx.isban.metrics.util.MetricsSingleton;


/**
 * Class: MetricsInit
 * Servlet para inicio de configuraciones del modulo metrics.
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales
 * [[ CSA ]]
 */
public class MetricsInit extends HttpServlet {
	
	/** Serial Version*/
	private static final long serialVersionUID = 1L;
       
	/**
	 * Controller de inicio para inicializar cmp Singleton.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param config de tipo ServletConfig
	 * @throws ServletException en caso de alguna excepcion.
	 */
	public void init(ServletConfig config) throws ServletException {
		MetricsSingleton.getInstance();
	}
}
