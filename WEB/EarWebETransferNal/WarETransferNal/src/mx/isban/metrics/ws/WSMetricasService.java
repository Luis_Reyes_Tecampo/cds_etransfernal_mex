/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * WSMetricasService.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0		2018 - 05				"JMFR"				Creacion del componente
 */

package mx.isban.metrics.ws;
//Imports javax
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
//Imports etransferNal
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
//Import metrics
import mx.isban.metrics.constantes.CFGConstants;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.MetricsSingleton;

/**
 * Servicio para activar modulo de metricas
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS
 * @since 2018 - 05 PLAN DELTA SUCURSALES
 */
@WebService (targetNamespace="http://ws.metrics.isban.com/", serviceName="MetricsService", portName="MetricsPort")
@SOAPBinding(style = javax.jws.soap.SOAPBinding.Style.RPC)
public class WSMetricasService {
	
	/** EJB Remoto*/
	private BitacorizaMetrics boMetrics = null;
	/** Para el cambio de estatus del modulo metrics*/
	private StringBuilder resp = null;
	
	/**
	 * Metodo encargado de encender y apagar modulo metrics.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 05 - 2018 Plan Delta Monitoreo Sucursales.
	 * 
	 * @param estatus : true / false 
	 * @return res estatus del modulo metrics.
	 */
	@WebMethod(operationName="configuraMetrics")
	@WebResult(name="configuraMetrics")
	public String configuraMetrics(@WebParam(name="estatus") String estatus){
		initEJB();//Inicializa ejb remoto
		resp = new StringBuilder();//Inicializa objeto de respuesta
		if(estatus != null && !"".equals(estatus)) {//Valida que la peticion contenga datos
			validaOperacion(estatus);//Valida tipo de operacion
		}else {//Error en la peticion
			resp.append("Debe ingresar un valor para modificar estatus del modulo metrics [[ true / false ]]]");
			resp.append("(DELTA) = Indica el estatus actual del modulo ");
		}
		//Regresa respuesta del servicio
		return resp.toString();
	}
	
	/**
	 * Inicializa ejb remoto
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 */
	private void initEJB(){
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		boMetrics = (BitacorizaMetrics)globalJNDIName.withAppName("EarEjbETransferNal")
		.withModuleName("EjbTransferNal").withBeanName("BitacorizaMetrics")
		.withBusinessInterface(BitacorizaMetrics.class).locate();
	}
	
	/**
	 * Metodo para validar la operacion a realizar para modulo metrics.
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 */
	private void validaOperacion(String estatus) {
		//Valida si solo se desea obtener el estatus del servicio
		if("DELTA".equalsIgnoreCase(estatus)){
			getEstatus();
		}else {//Realiza una actualizacion al objeto singleton en la parte WEB y EJB
			resp.append("@EAR-WEB [");
			resp.append(MetricsSingleton.getInstance().guardarConfiguracion(Boolean.valueOf(estatus)));
			resp.append("] @EAR-EJB [");
			resp.append(boMetrics.configMetrics(estatus));
			resp.append("]");
		}
	}
	
	/**
	 * Metodo para obtener estado del modulo metrics.
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 */
	private void getEstatus() {
		//Obtiene el estado actual del modulo metrics
		if(MetricsSingleton.getInstance().getCFG().isActivarMetrics()) {
			resp.append(CFGConstants.CFG_OK_A_INIT);//Modulo encendido
		}else {
			resp.append(CFGConstants.CFG_OK_D_INIT);//Modulo apagado
		}
	}
}
