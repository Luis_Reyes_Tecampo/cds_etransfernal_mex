package mx.isban.eTransferNal.filter.moduloCDA;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LookAndFeel;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanServiciosTareas;
import mx.isban.eTransferNal.servicio.moduloCDA.BOPerfilamiento;


public class FilterSeguridad extends Architech implements Filter{

	/**
	 * Constante privada del serial version
	 */
	private static final long serialVersionUID = -1435626515160910226L;
	
	/**
	 * Referencia privada al servicio de perfilamiento
	 */
	private BOPerfilamiento boPerfilamiento;
	
	/**
	 * Metodo destroy
	 */	
	@Override
	public void destroy() {}

	/**
	 * Metodo doFilter
	 * @param request Objeto del tipo ServletRequest
	 * @param response Objeto del tipo ServletResponse
	 * @param chain Objeto del tipo FilterChain
	 * @exception IOException Exception de IO
	 * @exception ServletException Exceptio de Servlet
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = null;
		String []urlPart = null;
		String servicioUrl="";
		httpRequest= (HttpServletRequest)request;
		debug("++++++++++++++++++++++++++++++Filter seguridad++++++++++++++++++++++++++++++");
		HttpSession lobjSession            = ((HttpServletRequest)request).getSession(false);
		ArchitechSessionBean lobjArchitechBean      = lobjSession.getAttribute("ArchitechSession") == null ? new ArchitechSessionBean() : (ArchitechSessionBean)lobjSession.getAttribute("ArchitechSession");
		LookAndFeel lobjLyFBean = lobjSession.getAttribute("LyFBean")==null?new LookAndFeel():(LookAndFeel)lobjSession.getAttribute("LyFBean");
		if("".equals(lobjArchitechBean.getUsuario())){
			String linkSAM = lobjLyFBean.getLinkSalirSAM();
			((HttpServletResponse)response).sendRedirect(linkSAM);
			return;
		}

		String requestURI = httpRequest.getRequestURI();
		if(requestURI!= null && !"".equals(requestURI.trim())){
			urlPart = requestURI.split("/");
		}

		if(urlPart!= null && urlPart.length>3) {
			servicioUrl = urlPart[3];
		}
		String perfiles = ((HttpServletRequest)request).getHeader("iv-groups");
		debug("++++++++++++++++++++++++++++++1.-Perfiles++++++++++++++++++++++++++++++"+perfiles);
		procesaPerfiles(httpRequest,perfiles,lobjArchitechBean,servicioUrl);
		chain.doFilter(request, response);
		
	}
	
	/**
	 * Metodo que sirve para procesar los perfiles
	 * @param request Objeto del tipo HttpServletRequest
	 * @param _perfiles Objeto del tipo String que almacena los _perfiles
	 * @param lobjArchitechBean Objeto del tipo ArchitechSessionBean
	 * @param servicioUrl es el servicio que se valida si se tiene accese o no
	 * @return boolean indica un true si se tienes acceso o un false en caso contrario
	 */
	public boolean procesaPerfiles(HttpServletRequest request, String _perfiles,ArchitechSessionBean lobjArchitechBean,String servicioUrl){
		boolean puedeVerPagina = false;
		List<BeanServiciosTareas> listBeanServiciosTareas = null;
		StringBuilder builder = new StringBuilder();
		StringBuilder tareasBuilder = new StringBuilder();
		Set<String> serviciosSet = new HashSet<String>();

		BeanReqConsPerfil beanReqConsPerfil = null;
		BeanResConsPerfil beanResConsPerfil = null;
		String perfiles = _perfiles;
		String arrPerfiles[] = perfiles.split(",");
		
		try {
			for(String tmp:arrPerfiles){
				tmp = tmp.replaceAll("\"", "" );
				debug("++++++++++++++++++++++++++++++2.-Perfiles++++++++++++++++++++++++++++++:"+tmp);
				beanReqConsPerfil = new BeanReqConsPerfil();
				beanReqConsPerfil.setPerfil(tmp.trim());
				
				beanResConsPerfil = boPerfilamiento.consultaServicio(beanReqConsPerfil, lobjArchitechBean);
				listBeanServiciosTareas = beanResConsPerfil.getListBeanServiciosTareas();
				for(BeanServiciosTareas beanServiciosTareas:listBeanServiciosTareas){
					serviciosSet.add(beanServiciosTareas.getServicio());
					builder.append(beanServiciosTareas.getServicio());
					builder.append(",");
				}
				
				if(serviciosSet.contains(servicioUrl)){
					puedeVerPagina = true;
					beanReqConsPerfil.setServicio(servicioUrl);
					beanResConsPerfil = boPerfilamiento.consultaServicioTarea(beanReqConsPerfil, lobjArchitechBean);
					listBeanServiciosTareas = beanResConsPerfil.getListBeanServiciosTareas();
					for(BeanServiciosTareas beanServiciosTareas:listBeanServiciosTareas){
						tareasBuilder.append(beanServiciosTareas.getTarea());
						tareasBuilder.append(",");
					}
				}	
			}
		} catch (BusinessException e) {
			showException(e);
		}
		if("moduloPOACOAInit.do".equals(servicioUrl) || "catalogosInit.do".equals(servicioUrl) || "moduloPagoInteresInit.do".equals(servicioUrl)||
				"moduloCDAInit.do".equals(servicioUrl)|| "inicioAplicacion.do".equals(servicioUrl)||"admonSaldoInit.do".equals(servicioUrl)||"contingenciaInit.do".equals(servicioUrl)){
			puedeVerPagina = true;
		}
		if(puedeVerPagina){
			request.setAttribute("seguServicios", builder.toString());
			request.setAttribute("seguTareas", tareasBuilder.toString());
			
		}
		return puedeVerPagina;
	}
	
	/***
	 * Metodo init
	 * @param filterConfig Objeto del tipo FilterConfig
	 * @exception ServletException Excepcion de la firma
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	/**
	 * Metodo get del servicio BoPerfilamiento
	 * @return boPerfilamiento Objeto del tipo boPerfilamiento
	 */
	public BOPerfilamiento getBoPerfilamiento() {
		return boPerfilamiento;
	}

	/**
	 * Metodo set del servicio BoPerfilamiento
	 * @param boPerfilamiento Objeto del tipo boPerfilamiento
	 */
	public void setBoPerfilamiento(BOPerfilamiento boPerfilamiento) {
		this.boPerfilamiento = boPerfilamiento;
	}
	
	

}
