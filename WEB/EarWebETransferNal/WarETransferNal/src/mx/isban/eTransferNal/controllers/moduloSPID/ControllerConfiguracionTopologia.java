/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerConfiguracionTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTopologia;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOTopologia;

/**
 * Controlador para la configuraci�n de topologias
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerConfiguracionTopologia extends Architech {
	
	/**Propiedad de tipo long que mantiene el estado del objeto*/
	private static final long serialVersionUID = -3822328788633549254L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private BeanSessionSPID sessionSPID;
	
	/**
	 * 
	 */
	private static final String NBEANRESTOPOLOGIA = "beanResTopologia";
	
	/**
	 * 
	 */
	private static final String NCODERROR = "codError.";
	
	/**
	 * 
	 */
	private static final String NBEANTOPOLOGIA = "beanTopologia";
	
	/**
	 * 
	 */
	private static final String NCONFIGURACIONTOPOLOGIA = "configuracionTopologia";
	
	/**Propiedad del tipo BOTopologia que contiene el valor de bOTopologia*/
	private BOTopologia bOTopologia;

	/**
	 * Metodo que obtiene las topologias registradas
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return model Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/configuracionTopologias.do")
	public ModelAndView obtenerTopologias(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResTopologia = bOTopologia.obtenerTopologias(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			model = new ModelAndView(NCONFIGURACIONTOPOLOGIA, NBEANRESTOPOLOGIA, beanResTopologia);

			model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("sortField", beanOrdenamiento.getSortField());
			model.addObject("sortType", beanOrdenamiento.getSortType());
			generarContadoresPaginacion(beanResTopologia, model);
			
		} catch (BusinessException be) {
			showException(be);
		}
		
		return model;
	}
	
	/**
	 * Metodo que muestra la  pagina de inicio de alta de topologias
	 * @param beanReqTopologia objeto del tipo BeanReqTopologia
	 * @return model Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/agregarTopologia.do")
	public ModelAndView mostrarAltaTopologia(BeanReqTopologia beanReqTopologia) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResTopologia = bOTopologia.obtenerCombosTopologia(getArchitechBean());
			model = new ModelAndView("nuevaTopologia", NBEANRESTOPOLOGIA, beanResTopologia);
			
			model.addObject(NBEANTOPOLOGIA, new BeanTopologia());
			
			if (Errores.OK00000V.equals(beanResTopologia.getCodError())){
				beanResTopologia.setCodError(null);
			}
			else{
				model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
				model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
				model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));				
			}

			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException be) {
			showException(be);
		}
		
		return model;
	}
	
	/**
	 * Metodo que guarda la topologia
	 * @param beanTopologia objeto del tipo BeanTopologia
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @return model Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/guardarTopologia.do")
	public ModelAndView guardarTopologia(@ModelAttribute(value=NBEANTOPOLOGIA) BeanTopologia beanTopologia,BeanPaginador beanPaginador) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {			
			beanResTopologia = bOTopologia.guardarTopologia(beanTopologia, getArchitechBean(), beanPaginador);
			
			if (Errores.OK00000V.equals(beanResTopologia.getCodError())){
				model = new ModelAndView(NCONFIGURACIONTOPOLOGIA, NBEANRESTOPOLOGIA, beanResTopologia);
			}
			else{
				BeanResTopologia beanResTopologia2 = bOTopologia.obtenerCombosTopologia(getArchitechBean());
				model = new ModelAndView("nuevaTopologia", NBEANRESTOPOLOGIA, beanResTopologia2);
				model.addObject(NBEANTOPOLOGIA, new BeanTopologia());
			}
			
			model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException be) {
			showException(be);
		}

		return model;
	}
	
	/**
	 * Metodo que muestra la pagina de edicion de topologia
	 * @param beanReqTopologia Objeto del tipo BeanReqTopologia
	 * @return model Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/editarTopologia.do")
	public ModelAndView editarTopologia(BeanReqTopologia beanReqTopologia) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		BeanTopologia beanTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResTopologia = bOTopologia.obtenerCombosTopologia(getArchitechBean());			
			
			for(BeanTopologia bTopologia : beanReqTopologia.getListaTopologias()) {
				if (bTopologia.isSeleccionado()){
					beanTopologia = bTopologia;
			}
			}
			model = new ModelAndView("nuevaTopologia", NBEANRESTOPOLOGIA, beanResTopologia);
			model.addObject("isEditar", "editar");
			model.addObject(NBEANTOPOLOGIA, beanTopologia);
			model.addObject("beanOldTopologia", beanTopologia);
			
			if (Errores.OK00000V.equals(beanResTopologia.getCodError())){
				beanResTopologia.setCodError(null);
			}
			else{
				model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
				model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
				model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));				
			}
			
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException be) {
			showException(be);
		}
		
		return model;
	}
	
	/**
	 * Metodo que guarda la edicion de la topologia
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param cveMedio Objeto del tipo @see String
	 * @param cveOperacion Objeto del tipo @see String
	 * @param toPri Objeto del tipo @see String
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @return model Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/guardarEdicionTopologia.do")
	public ModelAndView guardarEdicionTopologia(@ModelAttribute(value=NBEANTOPOLOGIA) BeanTopologia beanTopologia,
			@RequestParam(value="cveMedio", required=false) String cveMedio,
			@RequestParam(value="cveOperacion", required=false) String cveOperacion,
			@RequestParam(value="toPri", required=false) String toPri, BeanPaginador beanPaginador) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {	
			beanResTopologia = bOTopologia.guardarEdicionTopologia(beanTopologia, getArchitechBean(), 
					beanPaginador, cveMedio, cveOperacion, toPri);
			
			if (Errores.OK00000V.equals(beanResTopologia.getCodError())){
				model = new ModelAndView(NCONFIGURACIONTOPOLOGIA, NBEANRESTOPOLOGIA, beanResTopologia);
			}
			else{
				BeanResTopologia beanResTopologia2 = bOTopologia.obtenerCombosTopologia(getArchitechBean());
				model = new ModelAndView("nuevaTopologia", NBEANRESTOPOLOGIA, beanResTopologia2);
				model.addObject(NBEANTOPOLOGIA, beanTopologia);
			}			
			
			model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException be) {
			showException(be);
		}
		return model;
	}
	
	/**
	 * Metodo que elimina las topologias
	 * @param beanReqTopologia objeto del tipo @see BeanReqTopologia
	 * @return model Objeto del tipo @see ModelAndView
	 */
	@RequestMapping(value="/eliminarTopologia.do")
	public ModelAndView eliminarTopologia(BeanReqTopologia beanReqTopologia) {
		ModelAndView model = null;
		BeanResTopologia beanResTopologia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResTopologia = bOTopologia.eliminarTopologia(beanReqTopologia, getArchitechBean());
			model = new ModelAndView(NCONFIGURACIONTOPOLOGIA, NBEANRESTOPOLOGIA, beanResTopologia);
			
			model.addObject(Constantes.COD_ERROR, beanResTopologia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResTopologia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResTopologia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException e) {
			model = new ModelAndView(NCONFIGURACIONTOPOLOGIA, NBEANRESTOPOLOGIA, beanResTopologia);
			showException(e);
		}
		
		return model;
	}
	
	/**
	 * 
	 * @param beanResTop tipo BeanResTopologia
	 * @param model tipo ModelAndView 
	 */
	protected void generarContadoresPaginacion(BeanResTopologia beanResTop, ModelAndView model) {
		if (beanResTop.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResTop.getTotalReg();
			
			if (!"1".equals(beanResTop.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResTop.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResTop.getTotalReg() >= 20 * Integer.parseInt(beanResTop.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResTop.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanTopologia beanConsTop : beanResTop.getListaTopologias()){
				totalImporte = totalImporte.add(new BigDecimal(beanConsTop.getImporteMinimo()));
			}
			model.addObject("importePagina", totalImporte.toString());
			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
			
		}
	}

	/***
	 * Metodo que obtiene la referencia de bOTopologia
	 * @return BOTopologia
	 */
	public BOTopologia getBOTopologia() {
		return bOTopologia;
	}

	/**
	 * Metodo que modifica la referencia de bOTopologia
	 * @param bOTopologia Objeto del tipo @see bOTopologia
	 */
	public void setBOTopologia(BOTopologia bOTopologia) {
		this.bOTopologia = bOTopologia;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
