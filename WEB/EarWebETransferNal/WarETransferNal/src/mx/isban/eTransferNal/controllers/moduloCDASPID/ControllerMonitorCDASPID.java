/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 16:54:08 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDASPID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaBO;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOMonitorCDASPID;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la consulta de Movimientos CDAs
 *
 */
@Controller
public class ControllerMonitorCDASPID extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -8396037496907638074L;
	
	/**
	 * Constante del VISTA
	 */
	private static final String VISTA = "monitorCDASPID";
	
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOMonitorCDASPID boMonitorCDASPID;

	 /**
 	 * Metodo que se utiliza para mostrar la pagina de monitorCDA
 	 * monitor CDA.
 	 *
 	 * @param req Objeto del tipo HttpServletRequest
 	 * @param res Objeto del tipo HttpServletResponse
 	 * @return ModelAndView Objeto del tipo ModelAndView
 	 */
	 //Metodo que se utiliza para mostrar la pagina de monitorCDA
	 @RequestMapping(value = "/muestraMonitorCDASPID.do")
	 public ModelAndView muestraMonitorCDA(HttpServletRequest req, HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = consInfMonCDA(req,res);
		 return modelAndView;
	 }
	 
	/**
	 * Metodo que solicita la informacion para el monitor CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	 //Metodo que solicita la informacion para el monitor CDA
	@RequestMapping(value="/consInfMonCDASPID.do")
	public ModelAndView consInfMonCDA(HttpServletRequest req, HttpServletResponse res){
		ModelAndView modelAndView = null;  
		RequestContext ctx = new RequestContext(req);
	    BeanResMonitorCdaBO beanResMonitorCDA = new BeanResMonitorCdaBO();

	    //Se hace la consulta al BO
	    try{
		beanResMonitorCDA = boMonitorCDASPID
				.obtenerDatosMonitorCDA(getArchitechBean());
		debug("consInfMonCDA boMonitorCDA.obtenerDatosMonitorCDA Codigo de error:"
				+ beanResMonitorCDA.getCodError());
		modelAndView = new ModelAndView(VISTA, "beanResMonitorCDA",
				beanResMonitorCDA);
		}catch(BusinessException e){
			showException(e);
			beanResMonitorCDA.setCodError(e.getCode());
			beanResMonitorCDA.setTipoError(Errores.TIPO_MSJ_ERROR);
			modelAndView = new ModelAndView(VISTA);
		}

	    //Se obtiene el mensaje de properties
		String mensaje = ctx.getMessage("codError."
				+ beanResMonitorCDA.getCodError());
		modelAndView.addObject("codError", beanResMonitorCDA.getCodError());
		modelAndView.addObject("tipoError", beanResMonitorCDA.getTipoError());
		modelAndView.addObject("descError", mensaje);

		return modelAndView;
	 }
	
	/**
	 * Metodo que manda la solicitud para detener servicio CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	//Metodo que manda la solicitud para detener servicio CDA
	@RequestMapping(value="/detenerServicioMonCDASPID.do")
	public ModelAndView detenerServicioMonCDA(HttpServletRequest req, HttpServletResponse res){
		ModelAndView modelAndView = null;      
		RequestContext ctx = new RequestContext(req);
		BeanResMonitorCdaBO beanResMonitorCDA = new BeanResMonitorCdaBO();
		
		//Se hace la consulta al BO
		try{
			beanResMonitorCDA = boMonitorCDASPID
					.detenerServicioCDA(getArchitechBean());
			modelAndView = new ModelAndView(VISTA, "beanResMonitorCDA",
					beanResMonitorCDA);
		}catch(BusinessException e){
			showException(e);
			modelAndView = new ModelAndView(VISTA);
			beanResMonitorCDA.setCodError(e.getCode());
			beanResMonitorCDA.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		
		//Se obtiene el mensaje de properties
		String mensaje = ctx.getMessage("codError."
				+ beanResMonitorCDA.getCodError());
		modelAndView.addObject("codError", beanResMonitorCDA.getCodError());
		modelAndView.addObject("tipoError", beanResMonitorCDA.getTipoError());
		modelAndView.addObject("descError", mensaje);
		return modelAndView;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad boMonitorCDASPID
	 * @param boMonitorCDASPID del tipo BOMonitorCDASPID
	 */
	public void setBoMonitorCDASPID(BOMonitorCDASPID boMonitorCDASPID) {
		this.boMonitorCDASPID = boMonitorCDASPID;
	}

	

}
