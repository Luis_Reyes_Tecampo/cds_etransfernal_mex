package mx.isban.eTransferNal.controllers.pagoInteres;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGrafico;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.pagoInteres.BOGraficoSPEI;
import mx.isban.eTransferNal.servicio.pagoInteres.BOGraficoSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;
import net.sf.json.JSONObject;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ControllerGraficoJson extends Architech{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -175831997742251319L;
	/**
	 * Propiedad del tipo BOGraficoSPID que almacena el valor de bOGraficoSPID
	 */
	private BOGraficoSPID bOGraficoSPID;
	/**
	 * Propiedad del tipo BOGraficoSPEI que almacena el valor de bOGraficoSPEI
	 */
	private BOGraficoSPEI bOGraficoSPEI;
	
	/**
	 * Propiedad del tipo ResourceBundleMessageSource que almacena el valor de messageSource
	 */
	private ResourceBundleMessageSource messageSource;
	
	 /**
	 * Metodo que se utiliza para obtener los datos para graficar SPID
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @param reqGrafico BeanReqGrafico bean con los datos de busqueda
	 */
	 @RequestMapping(value = "/muestraGraficoSPID.json")
	 public void muestraGraficoSPID(HttpServletRequest req, HttpServletResponse res,BeanReqGrafico reqGrafico) {
		 BeanResGrafico beanResGrafico = new BeanResGrafico();
		 
		 Date fechaInicio = null;
		 Date fechaFin = null;
		 Utilerias utilerias = Utilerias.getUtilerias();
		 String strFechaIni = reqGrafico.getHoraInicio()+":"+reqGrafico.getMinutoInicio();
		 String strFechaFin = reqGrafico.getHoraFin()+":"+reqGrafico.getMinutoFin();
		 
		 if(reqGrafico.getTipoPago()== null || "".equals(reqGrafico.getTipoPago())){
			 beanResGrafico.setCodError("ED00114V");
			 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00114V", null, null));
			 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
		 }else{
			 fechaInicio = utilerias.cadenaToFecha(strFechaIni, Utilerias.FORMATO_HH_MM);
			 fechaFin = utilerias.cadenaToFecha(strFechaFin, Utilerias.FORMATO_HH_MM);
			 
			 if(fechaInicio.getTime()>=fechaFin.getTime()){
				 beanResGrafico.setCodError("ED00107V");
				 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00107V", null, null));
				 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
				 
			 }else{
				 BeanResGrafico rango = bOGraficoSPID.consultaRango(getArchitechBean());
			 
				 if(fechaFin.getTime()-fechaInicio.getTime()>rango.getRango()*60*1000){
			 
					 beanResGrafico.setCodError("ED00108V");
					 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00108V", new Object[]{rango.getRango()}, null));
					 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
					 
				 }else{
					 beanResGrafico = bOGraficoSPID.consultaDatosSPID(reqGrafico,getArchitechBean());
				 }
			 }
		}
		 salidaReturn(res,beanResGrafico);
	 }
	 
	 /**
	  * Metodo que se utiliza para obtener los datos para graficar SPEI
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @param reqGrafico BeanReqGrafico bean con los datos de busqueda
	 */
	@RequestMapping(value = "/muestraGraficoSPEI.json")
	 public void muestraGraficoSPEI(HttpServletRequest req, HttpServletResponse res,BeanReqGrafico reqGrafico) {
		 BeanResGrafico beanResGrafico = new BeanResGrafico();
		 Date fechaInicio = null;
		 Date fechaFin = null;
		 Utilerias utilerias = Utilerias.getUtilerias();
		 String strFechaIni = reqGrafico.getHoraInicio()+":"+reqGrafico.getMinutoInicio();
		 String strFechaFin = reqGrafico.getHoraFin()+":"+reqGrafico.getMinutoFin();
		 if(reqGrafico.getTipoPago()== null || "".equals(reqGrafico.getTipoPago())){
			 beanResGrafico.setCodError("ED00114V");
			 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00114V", null, null));
			 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
		 }else{
			 fechaInicio = utilerias.cadenaToFecha(strFechaIni, Utilerias.FORMATO_HH_MM);
			 fechaFin = utilerias.cadenaToFecha(strFechaFin, Utilerias.FORMATO_HH_MM);
			 
			 if(fechaInicio.getTime()>=fechaFin.getTime()){
				 beanResGrafico.setCodError("ED00107V");
				 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00107V", null, null));
				 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
			 }else{
				 BeanResGrafico rango = bOGraficoSPID.consultaRango(getArchitechBean());
				 if(fechaFin.getTime()-fechaInicio.getTime()>rango.getRango()*60*1000){
			 
					 beanResGrafico.setCodError("ED00108V");
					 beanResGrafico.setMsgError(messageSource.getMessage("codError.ED00108V", new Object[]{rango.getRango()}, null));
					 beanResGrafico.setTipoError(Errores.TIPO_MSJ_ALERT);
				 }else{
					 beanResGrafico = bOGraficoSPEI.consultaDatosSPEI(reqGrafico,getArchitechBean());
				 }
			 }
		 }
		salidaReturn(res,beanResGrafico);
	 }

	/**
	 * @param res Objeto del tipo HttpServletResponse
	* @param beanResGrafico BeanResGrafico bean con los datos de busqueda
	 */
	private void salidaReturn(HttpServletResponse res,BeanResGrafico beanResGrafico){
		 try {
				JSONObject jsonObject = JSONObject.fromObject(beanResGrafico);
				debug("json:"+jsonObject.toString());
				res.setContentType("application/json; ; charset=utf-8");
				OutputStream out = res.getOutputStream();
				out.write(jsonObject.toString().getBytes("UTF-8"));
				out.flush();
				out.close();
			} catch (IOException e) {
				showException(e);
			}
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad bOGraficoSPID
	 * @return bOGraficoSPID Objeto del tipo BOGraficoSPID
	 */
	public BOGraficoSPID getBOGraficoSPID() {
		return bOGraficoSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOGraficoSPID
	 * @param graficoSPID del tipo BOGraficoSPID
	 */
	public void setBOGraficoSPID(BOGraficoSPID graficoSPID) {
		bOGraficoSPID = graficoSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOGraficoSPEI
	 * @return bOGraficoSPEI Objeto del tipo BOGraficoSPEI
	 */
	public BOGraficoSPEI getBOGraficoSPEI() {
		return bOGraficoSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOGraficoSPEI
	 * @param graficoSPEI del tipo BOGraficoSPEI
	 */
	public void setBOGraficoSPEI(BOGraficoSPEI graficoSPEI) {
		bOGraficoSPEI = graficoSPEI;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo ResourceBundleMessageSource
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo ResourceBundleMessageSource
	 */
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	 
	 
}
