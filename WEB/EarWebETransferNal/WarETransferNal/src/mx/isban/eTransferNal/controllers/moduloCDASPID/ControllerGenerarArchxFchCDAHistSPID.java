/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerGenerarArchxFchCDAHistSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Jan 04 18:24:31 CST 2017 jjbeltran  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDASPID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOGenerarArchCDAxFchHistSPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Generar Archivo CDA Historico
**/
//Clase del tipo Controller que se encarga de recibir y procesar
@Controller
public class ControllerGenerarArchxFchCDAHistSPID extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;

	/**
	 * La constante COD_ERROR
	 */
	private static final String COD_ERROR = "codError";

	/**
	 * La constante COD_ERROR
	 */
	private static final String COD_ERROR_PUNTO = "codError.";

	/**
	 * La constante DESC_ERROR
	 */
	private static final String DESC_ERROR = "descError";

	/**
	 * La constante TIPO_ERROR
	 */
	private static final String TIPO_ERROR = "tipoError";
	
	/**
	 * La constante TIPO_ERROR
	 */
	private static final String VISTA = "generarArchivoxFchCDAHistSPID";

	/**Referencia privada del servicio boGenerarArchCDAxFchHist*/
	private BOGenerarArchCDAxFchHistSPID boGenerarArchCDAxFchHistSPID;
	
   /**
    * Metodo que muestra la generacion de Archivo CDA historico.
    *
    * @param req Objeto del tipo HttpServletRequests
    * @param res the res
    * @return ModelAndView Objeto del tipo ModelAndView
    */
	//Metodo que muestra la generacion de Archivo CDA historico
   @RequestMapping(value = "/muestraGenerarArchCDAxFchHistSPID.do")
   public ModelAndView muestraGenerarArchCDAHistSPID(HttpServletRequest req, HttpServletResponse res) {
      ModelAndView modelAndView = null;
      RequestContext ctx = new RequestContext(req);
      BeanResConsFechaOp beanResConsFechaOp = null;
      try{
    	  beanResConsFechaOp = boGenerarArchCDAxFchHistSPID.consultaFechaOperacion(getArchitechBean());
    	  modelAndView = new ModelAndView(VISTA,"generarArchCDAHist",beanResConsFechaOp);
      }catch(BusinessException e){
    	  //Se regresa el cod, tipo y desc del error
    	  showException(e);
    	  modelAndView = new ModelAndView(VISTA);
    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+e.getCode());
    	  modelAndView.addObject(COD_ERROR, e.getCode());
    	  modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
    	  modelAndView.addObject(DESC_ERROR, mensaje);
      }
      return modelAndView;
   }
   
   /**
    * Procesar generar arch cda hist spid.
    *
    * @param req Objeto del tipo HttpServletRequests
    * @param res the res
    * @return ModelAndView Objeto del tipo ModelAndView
    */
   //Metodo para procesar y generar el archivo historico por fecha
   @RequestMapping(value = "/procesarGenerarArchCDAxFchHistSPID.do")
   public ModelAndView procesarGenerarArchCDAHistSPID(HttpServletRequest req, HttpServletResponse res) {
      ModelAndView modelAndView = null;
      BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist = new BeanReqGenArchXFchCDAHist();
      UtilsMapeaRequest.getMapper().mapearObject(beanReqGenArchCDAHist, req.getParameterMap());
      
	  info("ControllerGenerarArchxFchCDAHistSPID.procesarGenerarArchCDAHistSPID: getFechaOp"+beanReqGenArchCDAHist.getFechaOp());
      BeanResProcGenArchCDAxFchHist beanResProcGenArchCDAxFchHist = new BeanResProcGenArchCDAxFchHist();
      RequestContext ctx = new RequestContext(req);
      //Se hace el procesamiento en el BO
      try{
    	  beanResProcGenArchCDAxFchHist = boGenerarArchCDAxFchHistSPID.procesarGenArchCDAHist(beanReqGenArchCDAHist,getArchitechBean());
    	  modelAndView = new ModelAndView(VISTA,"generarArchCDAHist",beanResProcGenArchCDAxFchHist);
      }catch(BusinessException e){
    	  showException(e);
    	  modelAndView = new ModelAndView(VISTA);
    	  beanResProcGenArchCDAxFchHist.setCodError(e.getCode());
    	  beanResProcGenArchCDAxFchHist.setTipoError(Errores.TIPO_MSJ_ERROR);
      }
      //Se valida codigo de respuesta exitoso
      if(Errores.OK00000V.equals(beanResProcGenArchCDAxFchHist.getCodError())){
    	  modelAndView.addObject(COD_ERROR, "OK00016V");
    	  modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_INFO);
    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+"OK00016V",
    			  new Object[]{beanResProcGenArchCDAxFchHist.getNombreArchivo()});
    	  modelAndView.addObject(DESC_ERROR, mensaje);
      }else{
    	  //Se regresa el cod, tipo y desc del error
    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResProcGenArchCDAxFchHist.getCodError());
    	  modelAndView.addObject(DESC_ERROR, mensaje);
    	  modelAndView.addObject(COD_ERROR, beanResProcGenArchCDAxFchHist.getCodError());
    	  modelAndView.addObject(TIPO_ERROR, beanResProcGenArchCDAxFchHist.getTipoError());
      }
      //Se setea la fecha de consulta
      beanResProcGenArchCDAxFchHist.setFechaOperacion(beanReqGenArchCDAHist.getFechaOp());
      return modelAndView;
   }
   
	/**
	 * Metodo que modifica el valor de la propiedad boGenerarArchCDAxFchHist
	 * @param boGenerarArchCDAxFchHistSPID Objeto de tipo @see BOGenerarArchCDAxFchHistSPID
	 */
	public void setBoGenerarArchCDAxFchHistSPID(
			BOGenerarArchCDAxFchHistSPID boGenerarArchCDAxFchHistSPID) {
		this.boGenerarArchCDAxFchHistSPID = boGenerarArchCDAxFchHistSPID;
	}
}
