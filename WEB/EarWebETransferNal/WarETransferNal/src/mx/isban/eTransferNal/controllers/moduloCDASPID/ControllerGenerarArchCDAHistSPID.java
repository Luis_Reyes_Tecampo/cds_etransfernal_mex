/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorArchExpSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Dec 28 12:20:00 CST 2016 Alan Garcia Villagran  Vector  Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloCDASPID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOGenerarArchCDAHistSPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar las peticiones
 * para la funcionalidad de Generar Archivo CDA Historico.
 */
@Controller
public class ControllerGenerarArchCDAHistSPID extends Architech {

	/** Variable de serializacion. */
	private static final long serialVersionUID = 1220515029523855775L;

	/** Variable de VIEW. */
	private static final String VIEW = "generarArchivoCDAHistSPID";
	
	/**  Referencia privada del servicio bOGenerarArchCDAHistSPID. */
	private BOGenerarArchCDAHistSPID boGenerarArchCDAHistSPID;

	/**
	 * Metodo que muestra la generacion de Archivo CDA historico.
	 *
	 * @param req            Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	//Metodo que muestra la generacion de Archivo CDA historico.
	@RequestMapping(value = "/generarArchHisCDASPID.do")
	public ModelAndView controllerPagina(HttpServletRequest req, HttpServletResponse res) {
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		BeanResConsFechaOp beanResConsFechaOp = null;

		//Se hace la consulta al BO
		try{
			beanResConsFechaOp = boGenerarArchCDAHistSPID.consultaFechaOperacionSPID(getArchitechBean());
			modelAndView = new ModelAndView(VIEW,"generarArchCDAHistSPID", beanResConsFechaOp);
		}catch(BusinessException e){
			showException(e);
			//Se devuelven los codigos de error y mensaje
			modelAndView = new ModelAndView(VIEW);
			String mensaje = ctx.getMessage("codError."+ e.getCode());
			modelAndView.addObject("codError", e.getCode());
			modelAndView.addObject("tipoError",Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject("descError", mensaje);
		}
		return modelAndView;
	}

	/**
	 * Procesa generacion arch his cdaspid.
	 *
	 * @param req            Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	//Procesa generacion arch his cdaspid.
	@RequestMapping(value = "/procesaGeneracionArchHisCDASPID.do")
	public ModelAndView procesaGeneracionArchHisCDASPID(HttpServletRequest req, HttpServletResponse res) {
	      ModelAndView modelAndView = new ModelAndView(VIEW);
	      //Se genera objeto de entrada
	      BeanReqGenArchCDAHist beanReqGenArchCDAHist = new BeanReqGenArchCDAHist();
	      UtilsMapeaRequest.getMapper().mapearObject(beanReqGenArchCDAHist, req.getParameterMap());
	      
	      BeanResProcGenArchCDAHist beanResProcGenArchCDAHist = null;
	      RequestContext ctx = new RequestContext(req);

	      //Se hace la consulta al BO
	      try{
	    	  beanResProcGenArchCDAHist = boGenerarArchCDAHistSPID.procesarGenArchHisSPID(beanReqGenArchCDAHist, getArchitechBean());
	    	  modelAndView = new ModelAndView(VIEW,"generarArchCDAHistSPID",beanResProcGenArchCDAHist);
	      }catch(BusinessException e){
	    	  showException(e);
	    	  //Se devuelven los codigos de error y mensaje
	    	  modelAndView = new ModelAndView(VIEW);
	    	  String mensaje = ctx.getMessage("codError."+ e.getCode());
	    	  modelAndView.addObject("codError", e.getCode());
	    	  modelAndView.addObject("tipoError",Errores.TIPO_MSJ_ERROR);
	    	  modelAndView.addObject("descError", mensaje);
	      }

	      return modelAndView;
	}

	/**
	 * Sets the b o generar arch cda hist spid.
	 *
	 * @param boGenerarArchCDAHistSPID Objeto del tipo @see BOGenerarArchCDAHistSPID
	 */
	public void setBoGenerarArchCDAHistSPID(
			BOGenerarArchCDAHistSPID boGenerarArchCDAHistSPID) {
		this.boGenerarArchCDAHistSPID = boGenerarArchCDAHistSPID;
	}
	
	
}