package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarInstPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqHabDesInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResHabDesInstParticipante;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOInstitucionesPCCda;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del catalogo de instituciones financieras
**/
@Controller
public class ControllerInstitucionesPCCda extends Architech{

	/**
	 *
	 */
	private static final long serialVersionUID = -4199156317910952128L;

	/**
	 * Constante con el nombre de la pagina de consulta
	 */
	private static final String PAG_CAT_INST_FINANCIERAS="catInstFinancieras";
	/**
	 * Constante con el nombre de la pagina de alta
	 */
	private static final String PAG_ALTA_CAT_INST_FINANCIERAS="altaCatInstFinancieras";

    /**
     * Constante del tipo String que almacena el valor de BEAN_RES
     */
    private static final String BEAN_RES = "beanResInstPOACOA";

	/**
	 * Propiedad del tipo BOInstitucionesPCCda que almacena el valor de bOInstitucionesPCCda
	 */
	private BOInstitucionesPCCda bOInstitucionesPCCda;

	/**
	 * Metodo que muestra la pantalla de parametros del POA/COA
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="muestraCatInstFinancieras.do")
	public ModelAndView muestraCatInstFinancieras(){
			return new ModelAndView(PAG_CAT_INST_FINANCIERAS);
	}

   /**Metodo que se utiliza para buscar registros en la funcionalidad
    * de instituciones participantes
    * @param beanConsReqInst Objeto del tipo @see BeanConsReqInst
    * @param req Objeto del tipo HttpServletRequest
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/buscarInstitucionesPOACOA.do")
    public ModelAndView buscarInstitucionesPOACOA(BeanConsReqInst beanConsReqInst, HttpServletRequest req){
    	BeanResInstPOACOA beanResInstPOACOA = null;
        ModelAndView modelAndView = null;
        RequestContext ctx = new RequestContext(req);
        	beanResInstPOACOA = bOInstitucionesPCCda.buscarInstitucionesPOACOA(beanConsReqInst, getArchitechBean());
    	    modelAndView = new ModelAndView(PAG_CAT_INST_FINANCIERAS,BEAN_RES,beanResInstPOACOA);
      	    String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResInstPOACOA.getCodError());
     	    modelAndView.addObject(Constantes.COD_ERROR, beanResInstPOACOA.getCodError());
     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResInstPOACOA.getTipoError());
     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
       return modelAndView;
    }

    /**Metodo que se utiliza para habilitar o deshabilitar instituciones participantes
     * @param beanReqHabDesInst Objeto del tipo @see BeanReqHabDesInst
     * @param req Objeto del tipo HttpServletRequest
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/habDesPOACOA.do")
     public ModelAndView habilitarDeshabilitarPOACOA(BeanReqHabDesInst beanReqHabDesInst, HttpServletRequest req){
    	 String accion = "deshabilitada";
    	 BeanResHabDesInstParticipante beanResHabDesInstParticipante = null;
         ModelAndView modelAndView = null;
         RequestContext ctx = new RequestContext(req);
        	beanResHabDesInstParticipante = bOInstitucionesPCCda.habDesPOACOA(beanReqHabDesInst, getArchitechBean());
     	    modelAndView = new ModelAndView(PAG_CAT_INST_FINANCIERAS,BEAN_RES,beanResHabDesInstParticipante);
     	   if(Errores.OK00000V.equals(beanResHabDesInstParticipante.getCodError())){
     	    	if("1".equals(beanReqHabDesInst.getHabDesValor())){
     	    		accion = "activa";
     	    	}
     	        String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.OK00014V,
     	        		new String[]{beanReqHabDesInst.getHabDesNombre(),accion});
        	    modelAndView.addObject(Constantes.COD_ERROR, beanResHabDesInstParticipante.getCodError());
        	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResHabDesInstParticipante.getTipoError());
        	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
     	    }else{
     	    	String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResHabDesInstParticipante.getCodError());
          	    modelAndView.addObject(Constantes.COD_ERROR, beanResHabDesInstParticipante.getCodError());
          	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResHabDesInstParticipante.getTipoError());
          	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
     	    }

        return modelAndView;
     }

     /**Metodo que se utiliza para habilitar o deshabilitar la validacion de cuenta
      * @param beanReqHabDesInst Objeto del tipo @see BeanReqHabDesInst
      * @param req Objeto del tipo HttpServletRequest
      * @return ModelAndView Objeto del tipo ModelAndView
      */
      @RequestMapping(value = "/habDesValidacionCuenta.do")
      public ModelAndView habDesValidacionCuenta(BeanReqHabDesInst beanReqHabDesInst, HttpServletRequest req){
    	  String accion = "deshabilitada";
     	 BeanResHabDesInstParticipante beanResHabDesInstParticipante = null;
          ModelAndView modelAndView = null;
          RequestContext ctx = new RequestContext(req);
         	beanResHabDesInstParticipante = bOInstitucionesPCCda.habDesValidacionCuenta(beanReqHabDesInst, getArchitechBean());
      	    modelAndView = new ModelAndView(PAG_CAT_INST_FINANCIERAS,BEAN_RES,beanResHabDesInstParticipante);
      	    if(Errores.OK00000V.equals(beanResHabDesInstParticipante.getCodError())){
      	    	if("1".equals(beanReqHabDesInst.getHabDesValor())){
      	    		accion = "activa";
      	    	}
      	        String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.OK00015V,
      	        		new String[]{beanReqHabDesInst.getHabDesNombre(),accion});
         	    modelAndView.addObject(Constantes.COD_ERROR, beanResHabDesInstParticipante.getCodError());
         	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResHabDesInstParticipante.getTipoError());
         	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
      	    }else{
      	    	String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResHabDesInstParticipante.getCodError());
           	    modelAndView.addObject(Constantes.COD_ERROR, beanResHabDesInstParticipante.getCodError());
           	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResHabDesInstParticipante.getTipoError());
           	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
      	    }
         return modelAndView;
      }

	/**
	 * Metodo que muestra la pantalla de parametros del POA/COA
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="muestraAltaCatInstFinancieras.do")
	public ModelAndView muestraAltaCatInstFinancieras(HttpServletRequest req){
		ModelAndView modelAndView = null;
		BeanResInst beanResInst = null;
        RequestContext ctx = new RequestContext(req);
			beanResInst = bOInstitucionesPCCda.consultaInst(getArchitechBean());
    	    modelAndView = new ModelAndView(PAG_ALTA_CAT_INST_FINANCIERAS,"beanResInst",beanResInst);
      	    String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResInst.getCodError());
     	    modelAndView.addObject(Constantes.COD_ERROR, beanResInst.getCodError());
     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResInst.getTipoError());
     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	    return modelAndView;
	}

	/**
	 * Metodo que sirve para dar de alta una institucion participante
	 * @param beanReqInsertInst Objeto del tipo @see BeanReqInsertInst
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="altaCatInstFinancieras.do")
	public ModelAndView altaCatInstFinancieras(BeanReqInsertInst beanReqInsertInst,HttpServletRequest req){
		ModelAndView modelAndView = null;
		BeanResGuardaInst beanResGuardaInst = null;
		ArchitechSessionBean architechSessionBean = getArchitechBean();
        RequestContext ctx = new RequestContext(req);
			beanReqInsertInst.setCveUsuario(architechSessionBean.getUsuario());
			beanResGuardaInst = bOInstitucionesPCCda.guardaInstitucionPOACOA(beanReqInsertInst, getArchitechBean());
			if(Errores.OK00000V.equals(beanResGuardaInst.getCodError())){
				modelAndView = new ModelAndView(PAG_CAT_INST_FINANCIERAS);
				String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.OK00013V);
	     	    modelAndView.addObject(Constantes.COD_ERROR, Errores.OK00013V);
	     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResGuardaInst.getTipoError());
	     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}else{
				modelAndView = new ModelAndView(PAG_ALTA_CAT_INST_FINANCIERAS,"beanResInst",beanResGuardaInst);
	      	    String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResGuardaInst.getCodError());
	     	    modelAndView.addObject(Constantes.COD_ERROR, beanResGuardaInst.getCodError());
	     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResGuardaInst.getTipoError());
	     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
	    return modelAndView;
	}

   /**Metodo que permite eliminar una institucion participante
	* @param beanReqEliminarInst Objeto del tipo @see BeanReqEliminarInstPOACOA
	* @param req Objeto del tipo HttpServletRequests
	* @return ModelAndView Objeto del tipo ModelAndView
	*/
   @RequestMapping(value = "/elimCatInstFinancieras.do")
   public ModelAndView elimCatInstFinancieras(BeanReqEliminarInstPOACOA beanReqEliminarInst,HttpServletRequest req){
	   BeanResElimInstPOACOA beanResElimInstPOACOA = null;
	   StringBuilder strBuilder = new StringBuilder();
	   String correctos = "";
	   String erroneos = "";
	   String mensaje = "";
	   ModelAndView modelAndView = null;
	   RequestContext ctx = new RequestContext(req);

	    	beanResElimInstPOACOA = bOInstitucionesPCCda.elimCatInstFinancieras(
	    			beanReqEliminarInst, getArchitechBean());
	    	modelAndView = new ModelAndView(PAG_CAT_INST_FINANCIERAS,BEAN_RES,beanResElimInstPOACOA);
	   if(Errores.OK00000V.equals(beanResElimInstPOACOA.getCodError())){
			modelAndView.addObject("paginador", beanReqEliminarInst.getPaginador());
			correctos = beanResElimInstPOACOA.getEliminadosCorrectos();
			erroneos = beanResElimInstPOACOA.getEliminadosErroneos();

			if(!"".equals(correctos)){
				correctos = correctos.substring(0,correctos.length()-1);
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00017V",new String[]{correctos});
			}
			if(!"".equals(erroneos)){
				erroneos = erroneos.substring(0,erroneos.length()-1);
				strBuilder.append(mensaje);
				strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO+"CD00165V",new String[]{erroneos}));
				mensaje = strBuilder.toString();
			}
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResElimInstPOACOA.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResElimInstPOACOA.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	   }else{
		   mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResElimInstPOACOA.getCodError());
		   modelAndView.addObject(Constantes.COD_ERROR, beanResElimInstPOACOA.getCodError());
		   modelAndView.addObject(Constantes.TIPO_ERROR, beanResElimInstPOACOA.getTipoError());
	       modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	   }


	    return modelAndView;
   }

	/**
	 * Metodo get que obtiene el valor de la propiedad bOInstitucionesPCCda
	 * @return bOInstitucionesPCCda Objeto de tipo @see BOInstitucionesPCCda
	 */
	public BOInstitucionesPCCda getBOInstitucionesPCCda() {
		return bOInstitucionesPCCda;
	}

	/**
	 * Metodo que modifica el valor de la propiedad bOInstitucionesPCCda
	 * @param institucionesPCCda Objeto de tipo @see BOInstitucionesPCCda
	 */
	public void setBOInstitucionesPCCda(BOInstitucionesPCCda institucionesPCCda) {
		bOInstitucionesPCCda = institucionesPCCda;
	}


}