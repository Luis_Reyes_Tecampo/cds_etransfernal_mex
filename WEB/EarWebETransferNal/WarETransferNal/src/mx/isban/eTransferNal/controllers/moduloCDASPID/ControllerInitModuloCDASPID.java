/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerInitModuloCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:11:45 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDASPID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del monitor
**/
@Controller
public class ControllerInitModuloCDASPID extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	
	/**
	 * Metodo inicial del modulo CDA.
	 *
	 * @param req the req
	 * @param res the res
	 * @return ModelAndView Objetode ModelAndView de Spring
	 */
	@RequestMapping(value="moduloCDASPIDInit.do")
	public ModelAndView inicio(HttpServletRequest req, HttpServletResponse res) {
			return new ModelAndView("inicio");
	}

	
}
