/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerInitModuloCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mar 21 10:12:45 CST 2017 Omar Zúñiga  VECTOR 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.capturasManuales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del monitor
**/
@Controller
public class ControllerInitCapturasManuales extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	
	/**
	 * Metodo inicial del modulo CDA.
	 *
	 * @param request the request
	 * @param response the response
	 * @return ModelAndView Objetode ModelAndView de Spring
	 */
	@RequestMapping(value="capturasManualesInit.do")
	public ModelAndView inicio(HttpServletRequest request, HttpServletResponse response){
			return new ModelAndView("inicio");
	}

	
}
