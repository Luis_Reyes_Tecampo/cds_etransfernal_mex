/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerReporteDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	     By 	          Company 	 Description
 * ------- -----------   -----------        ---------- ----------------------
 * 	 1.0   18/04/2018       MTREJO            VECTOR 		Creacion
 *   2.0   25/04/2018       SMEZA             VECTOR 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPEICero;


/**
 * Anexo de Imports para la funcionalidad de la pantalla de SPEI Cero - Emision Reporte del Dia
 * 
 * @author FSW-Vector
 * @sice 18 abril 2018
 *
 */

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.servicio.SPEICero.BOEmisionRep;
import mx.isban.eTransferNal.utilerias.MetodosRepoDiaSpeiCero;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de SPEI Cero - Emision Reporte del Dia.
 *
 * @author FSW-Vector
 * @sice 18 abril 2018
 */
@Controller
public class ControllerReporteDia extends Architech {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8542182737311779841L;
	
	/**  Llamada a BO desde el Controller. */
	private BOEmisionRep boEmisionRep;
	/**constante de error **/
	private static final String ERROR =" Se genero el error: ";
	/**
	 * variable de utilerias
	 */
	public static final MetodosRepoDiaSpeiCero util = new MetodosRepoDiaSpeiCero();
	/**
	 * @return the util
	 */
	public static MetodosRepoDiaSpeiCero getUtil() {
		return util;
	}
	
	/**
	 * Frm initial.
	 *
	 * @return the model and view
	 */
	@RequestMapping(value = "/moduloSPEICero.do")
	public ModelAndView frmInitial() {
		info("<----- Procedimiento para SPEI Cero.....");
		
		return new ModelAndView("inicio");
	}
	
	/**
	 * Procedimiento inicial del metodo.
	 * @param req
	 * @param res
	 * @return the model and view
	 */
	@RequestMapping(value = "/moduloReporteDia.do")
	public ModelAndView initialRepDia(HttpServletRequest req, HttpServletResponse res) {
		
		return inicializa(req, false);
	}
	
	/**
	 * funcion que inicia la pantalla del historico
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping( value = "moduloReporteHis.do")
	public ModelAndView initialRepHistorico(HttpServletRequest req, HttpServletResponse res) {
		return inicializa(req,true);
	}
	
	/**
	 * funcion que inicializa la pantalla principal
	 * @param req
	 * @param historico
	 * @return
	 */
	
	private ModelAndView inicializa(HttpServletRequest req, boolean historico) {
		ModelAndView mdlVista = null;
		RequestContext ctx = new RequestContext(req);
		BeanResSpeiCero beanResSpeiCero = new BeanResSpeiCero();
		try {
			
			info("<----- Procedimiento para Reportes por Dia.....");
				/** Instancia de Bean que almacena la siguiente informacion:
			 *   - Estatus Operacion
			 *   - Hora Inicio SPEI CERO 
			 *   - Horario cambio fecha operacion
			 **/
					
			beanResSpeiCero = boEmisionRep.getResEmisionReporte(getArchitechBean(), 1, historico);
			mdlVista = util.continuaInicializa(beanResSpeiCero,historico, ctx, true);
			
		}catch (BusinessException e) {
			mdlVista = util.continuaInicializa(beanResSpeiCero,historico, ctx,false);
			error(ERROR+e.getMessage(), this.getClass());
			showException(e);
		}
		/** Retornamos el valor al metodo que lo llamo**/
		return mdlVista;
	}

	/**
	 * @return the boEmisionRep
	 */
	public BOEmisionRep getBoEmisionRep() {
		return boEmisionRep;
	}

	/**
	 * @param boEmisionRep the boEmisionRep to set
	 */
	public void setBoEmisionRep(BOEmisionRep boEmisionRep) {
		this.boEmisionRep = boEmisionRep;
	}

	
	

	
	
	
}