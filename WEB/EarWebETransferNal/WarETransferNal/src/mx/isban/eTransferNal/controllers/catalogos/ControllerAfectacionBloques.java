/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerAfectacionBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:16:01 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.catalogos.BeanBloques;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOAfectacionBloques;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarAfectacionBloques;

/**
 * Class ControllerAfectacionBloques.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * de afectacion de bloques
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
@Controller
public class ControllerAfectacionBloques extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5688715144196873567L;
	
	
	/** La variable que contiene informacion con respecto a: bo afectacion bloques. */
	private transient BOAfectacionBloques boAfectacionBloques;
	
	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaAfectacionBloques";
	
	/** La constante BEAN_BLOQUE. */
	private static final String BEAN_BLOQUE = "beanBloque";
	
	/** La constante BEAN_BLOQUES. */
	private static final String BEAN_BLOQUES = "beanBloques";

	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaAfectacionBloques";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	

/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante utilerias. */
	private static final UtilsExportarAfectacionBloques utilerias = UtilsExportarAfectacionBloques.getInstancia();
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/consultaAfectacionBloques.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanBloque beanFilter, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		ModelAndView model = null;
		BeanBloques response = null;
		try {
			/** Validacion del parametro accion **/
			if (req.getParameter(ACCION) != null && !req.getParameter(ACCION).equals(StringUtils.EMPTY)) {
				/** Se valida los filtros guardados**/
				beanFilter.setNumBloque(req.getParameter("numBloqueF"));
				beanFilter.setValor(req.getParameter("valorF"));
				beanFilter.setDescripcion(req.getParameter("descripcionF"));
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			/** Ejecuta peticion al BO **/
			response = boAfectacionBloques.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_BLOQUES, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
				
			}
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}

	
	/**
	 * Agregar bloque.
	 * 
	 * Mapeo del metodo para iniciar el flujo de agregar 
	 * un nuevo registro al catalogo.
	 *
	 * @param bloque --> objeto de tipo BeanBloque que agrega bloques
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/agregarBloque.do")
	public ModelAndView agregarBloque(@ModelAttribute(BEAN_BLOQUE) BeanBloque bloque, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de alta **/
		ModelAndView model = null;
		BeanBloques bloques = null;
		BeanResBase response = null;
		try {
			response = boAfectacionBloques.agregar(bloque, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				bloques = boAfectacionBloques.consultar(new BeanBloque(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_BLOQUE, new BeanBloque());
				model.addObject(BEAN_BLOQUES, bloques);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(BEAN_BLOQUE, bloque);
				model.addObject(ACCION, ACCION_ALTA);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Editar bloque.
	 * 
	 * Mapeo del metodo para iniciar el flujo de 
	 * modificar un registro del catalogo.
	 *
	 * @param bloque --> objeto de tipo BeanBloque que edita bloques
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/editarBloque.do")
	public ModelAndView editarBloque(@ModelAttribute(BEAN_BLOQUE) BeanBloque bloque, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de edicion  **/
		ModelAndView model = null;
		BeanBloques bloques = null;
		String valorOld = null;
		try {
			valorOld = req.getParameter("valorOld");
			bloque.setValor(bloque.getValor().concat("|".concat(valorOld)));
			BeanResBase response = boAfectacionBloques.editar(bloque, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				bloques = boAfectacionBloques.consultar(new BeanBloque(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_BLOQUES, bloques);
				model.addObject(BEAN_BLOQUE, new BeanBloque());
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				model.addObject(BEAN_BLOQUE, bloque);
				bloque.setValor(valorOld);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Mantenimiento bloques.
	 * 
	 * Mapeo del flujo para mostrar el fomurlario de 
	 * matenimiento de registros del catalogo.
	 *
	 * @param beanBloques --> objeto de tipo BeanBloques que muestra bloques
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/mantenimientoBloques.do")
	public ModelAndView mantenimientoBloques(@ModelAttribute(BEAN_BLOQUES) BeanBloques beanBloques,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanBloque beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Creacion de la vista de mantenimiento  **/
		ModelAndView model = null;
		BeanBloque bloque = new BeanBloque();
		if (beanBloques != null && beanBloques.getBloques() != null) {
			for(BeanBloque bl : beanBloques.getBloques()) {
				if(bl.isSeleccionado()) {
					bloque = bl;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		if (bloque.getNumBloque() != null) {
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		model.addObject(BEAN_BLOQUE, bloque);
		model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		model.addObject(UtilsConstantes.BEAN_PAGINADOR, beanPaginador);
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Elim bloques.
	 * 
	 * Mapeo del flujo para iniciar la eliminacion 
	 * de registros del catalogo.
	 *
	 * @param bloques --> objeto de tipo BeanBloque que elimina bloques
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/eliminarBloque.do")
    public ModelAndView elimBloques(@ModelAttribute(BEAN_BLOQUES) BeanBloques bloques, BindingResult bindingResult){
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de eliminacion **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			response = boAfectacionBloques.eliminar(bloques, getArchitechBean());
			BeanBloques responseConsulta = boAfectacionBloques.consultar(new BeanBloque(), new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_BLOQUES, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
					
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return modelAndView;
    }
	
	/**
	 * Exportar bloques.
	 * 
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 * @param bloques --> objeto de tipo BeanBloque que exporta
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/exportarAfectacionBloques.do")
	public ModelAndView exportarBloques(@ModelAttribute(BEAN_BLOQUES) BeanBloques bloques, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBody(bloques.getBloques()));
		modelAndView.addObject("NAME_SHEET", "Afectacion de Bloques");
		/** Retorno del modelo **/
		return modelAndView;
	}
	
	/**
	 * Exportar todos afectacion bloques.
	 * 
	 * Mapeo del metodo para iniciar el flujo de exportar los bloques de la pantalla
	 * al proceso de inserccion en BD
	 *
	 * @param beanPaginador --> objeto de tipo beanPaginador que permite paginar
	 * @param beanBloque --> objeto de tipo BeanBloque que edita bloques
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value="/exportarTodosAfectacionBloques.do")
	public ModelAndView exportarTodosAfectacionBloques(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanBloque beanBloque, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanBloques responseConsulta = new BeanBloques();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boAfectacionBloques.consultar(beanBloque, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_BLOQUES, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boAfectacionBloques.exportarTodo(beanBloque, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_BLOQUES, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Metodo GET que obtiene los bloques
	 *
	 * @return boAfectacionBloques --> devuelve el objeto de bloques obtenido
	 */
	public BOAfectacionBloques getBoAfectacionBloques() {
		return boAfectacionBloques;
	}

	/**
	 *Metodo SET que asigna los bloques
	 *
	 * @return boAfectacionBloques --> devuelve el objeto de bloques asignado
	 */
	public void setBoAfectacionBloques(BOAfectacionBloques boAfectacionBloques) {
		this.boAfectacionBloques = boAfectacionBloques;
	}
	
}
