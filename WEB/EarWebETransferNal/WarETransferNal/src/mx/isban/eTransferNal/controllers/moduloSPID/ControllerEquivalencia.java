/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalencia;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOEquivalencia;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador para el catalogo de equivalencias
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerEquivalencia extends Architech{

	/**
	 * variable del serial version
	 */
	private static final long serialVersionUID = -3602789526841076690L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private static final String NLISTADOEQUIVALENCIAS = "listadoEquivalencias";
	
	/**
	 * 
	 */
	private static final String NBEANRESEQUIVALENCIA = "beanResEquivalencia";
	
	/**
	 * 
	 */
	private static final String NBEANEQUIVALENCIA = "beanEquivalencia";
	
	/**
	 * propiedad privada del BO
	 */
	private BOEquivalencia bOEquivalencia;
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/listarEquivalencias.do")
	public ModelAndView cargaListadoEquivalencias(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento) {
		ModelAndView model = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {			
			BeanResEquivalencia beanResEquivalencia = bOEquivalencia.listarEquivalencia(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			model = new ModelAndView(NLISTADOEQUIVALENCIAS, NBEANRESEQUIVALENCIA, beanResEquivalencia);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("sortField", beanOrdenamiento.getSortField());
			model.addObject("sortType", beanOrdenamiento.getSortType());
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			generarContadoresPaginacion(beanResEquivalencia, model);
			
			String mensaje = "";
			
			if (beanResEquivalencia.getCodError() != null) {
				mensaje = messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null);
			}
			
			model.addObject(Constantes.DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/agregarEquivalencia.do")
	public ModelAndView agregarEquivalencia() {
		ModelAndView model = null;
		BeanResEquivalencia beanResEquivalencia = new BeanResEquivalencia();
		beanResEquivalencia = bOEquivalencia.generarCombosEquivalencia();
		model = new ModelAndView("agregarEquivalencia", NBEANRESEQUIVALENCIA, beanResEquivalencia);
		model.addObject(NBEANEQUIVALENCIA, new BeanEquivalencia());
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		model.addObject("sessionSPID", sessionSPID);
		
		if (Errores.OK00000V.equals(beanResEquivalencia.getCodError())){
			beanResEquivalencia.setCodError(null);
		}
		else
		{
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, 
				messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null));
		}
		
		return model;
	}
	
	/**
	 * @param beanEquivalencia Objeto del tipo BeanEquivalencia
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/guardarEquivalencia.do")
	public ModelAndView guardarEquivalencia(@ModelAttribute(NBEANEQUIVALENCIA) BeanEquivalencia beanEquivalencia,
			BeanPaginador beanPaginador) {
		ModelAndView model = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			BeanResEquivalencia beanResEquivalencia =
					bOEquivalencia.guardarEquivalencia(beanEquivalencia, getArchitechBean(), beanPaginador);
			
			if (Errores.OK00000V.equals(beanResEquivalencia.getCodError())){
				model = new ModelAndView(NLISTADOEQUIVALENCIAS, NBEANRESEQUIVALENCIA, beanResEquivalencia);
			}
			else{
				BeanResEquivalencia beanResEquivalencia2 = bOEquivalencia.generarCombosEquivalencia();
				model = new ModelAndView("agregarEquivalencia", NBEANRESEQUIVALENCIA, beanResEquivalencia2);
				model.addObject(NBEANEQUIVALENCIA, new BeanEquivalencia());
			}
			
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch(BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * @param beanReqEquivalencia Objeto del tipo BeanReqEquivalencia
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/eliminarEquivalencia.do")
	public ModelAndView eliminarEquivalencia(@ModelAttribute(NBEANEQUIVALENCIA) BeanReqEquivalencia beanReqEquivalencia,
			BeanPaginador beanPaginador) {
		ModelAndView model = null;
		BeanResEquivalencia beanResEquivalencia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResEquivalencia =
					bOEquivalencia.eliminarEquivalencia(beanReqEquivalencia, getArchitechBean(), beanPaginador);
			model = new ModelAndView(NLISTADOEQUIVALENCIAS, NBEANRESEQUIVALENCIA, beanResEquivalencia);
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, 
					messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch(BusinessException e) {
			showException(e);
			model = new ModelAndView(NLISTADOEQUIVALENCIAS, NBEANRESEQUIVALENCIA, beanResEquivalencia);
		}
		return model;
	}
	
	/**
	 * @param beanReqEquivalencia Objeto del tipo BeanReqEquivalencia
	 * @return ModelAndView
	 */
	@RequestMapping(value="/editarEquivalencia.do")
	public ModelAndView editarEquivalencia(BeanReqEquivalencia beanReqEquivalencia) {
		ModelAndView model = null;
		BeanResEquivalencia beanResEquivalencia = null;
		BeanEquivalencia beanEquivalencia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		beanResEquivalencia = bOEquivalencia.generarCombosEquivalencia();			
		
		for(BeanEquivalencia bEquivalencia : beanReqEquivalencia.getListaEquivalencias()) {
			if (bEquivalencia.isSeleccionado()) {
				beanEquivalencia = bEquivalencia;
		}
		}
		model = new ModelAndView("agregarEquivalencia", NBEANRESEQUIVALENCIA, beanResEquivalencia);
		model.addObject("isEditar", "editar");
		model.addObject(NBEANEQUIVALENCIA, beanEquivalencia);
		model.addObject("beanOldEquivalencia", beanEquivalencia);
		model.addObject("sessionSPID", sessionSPID);
		
		if (Errores.OK00000V.equals(beanResEquivalencia.getCodError())){
			beanResEquivalencia.setCodError(null);
		}
		else
		{
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, 
				messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null));
		}
		return model;
	}
	
	/**
	 * @param beanEquivalencia Objeto del tipo BeanEquivalencia
	 * @param tipoPago Objeto del tipo String
	 * @param cveOperacion Objeto del tipo String
	 * @param clasificacion Objeto del tipo String
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/guardarEdicionEquivalencia.do")
	public ModelAndView guardarEdicionEquivalencia(@ModelAttribute(value="beanTopologia") BeanEquivalencia beanEquivalencia,
			@RequestParam(value="tipoPagoOld", required=false) String tipoPago,
			@RequestParam(value="cveOperacionOld", required=false) String cveOperacion,
			@RequestParam(value="clasificacionOld", required=false) String clasificacion, BeanPaginador beanPaginador) {
		ModelAndView model = null;
		BeanResEquivalencia beanResEquivalencia = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {	
			beanResEquivalencia = bOEquivalencia.guardarEdicionEquivalencia(beanEquivalencia, getArchitechBean(), 
					beanPaginador, cveOperacion, tipoPago, clasificacion);
			
			if (Errores.OK00000V.equals(beanResEquivalencia.getCodError())){
				model = new ModelAndView(NLISTADOEQUIVALENCIAS, NBEANRESEQUIVALENCIA, beanResEquivalencia);
			}
			else{
				BeanResEquivalencia beanResEquivalencia2 = bOEquivalencia.generarCombosEquivalencia();
				model = new ModelAndView("agregarEquivalencia", NBEANRESEQUIVALENCIA, beanResEquivalencia2);
				model.addObject(NBEANEQUIVALENCIA, beanEquivalencia);
			}
			
			model.addObject(Constantes.COD_ERROR, beanResEquivalencia.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResEquivalencia.getTipoError());
			model.addObject(Constantes.DESC_ERROR, 
					messageSource.getMessage("codError."+beanResEquivalencia.getCodError(), null, null));
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException be) {
			showException(be);
		}

		return model;
	}

	/**
	 * 
	 * @param beanResEquiv  tipo BeanResEquivalencia
	 * @param model tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(BeanResEquivalencia beanResEquiv, ModelAndView model) {
		if (beanResEquiv.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResEquiv.getTotalReg();
			
			if (!"1".equals(beanResEquiv.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResEquiv.getBeanPaginador().getPagina()) - 1)+1;
			}
			if (beanResEquiv.getTotalReg() >= 20 * Integer.parseInt(beanResEquiv.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResEquiv.getBeanPaginador().getPagina());
			}
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
			
		}
	}
	/**
	 * @return bOEquivalencia objeto del tipo BOEquivalencia
	 */
	public BOEquivalencia getBOEquivalencia() {
		return bOEquivalencia;
	}

	
	/**
	 * @param bOEquivalencia Objeto del tipo BOEquivalencia
	 */
	public void setBOEquivalencia(BOEquivalencia bOEquivalencia) {
		this.bOEquivalencia = bOEquivalencia;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
