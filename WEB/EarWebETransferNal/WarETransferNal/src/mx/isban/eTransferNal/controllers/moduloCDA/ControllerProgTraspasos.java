/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerProgTraspaso.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015 17:23:00 INDRA		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosBO;
import mx.isban.eTransferNal.servicio.moduloCDA.BOProgTraspasos;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la pantalla de programcion de horarios
 *
 */
@Controller
public class ControllerProgTraspasos extends Architech{

	/**Constante del Serial Version */
	private static final long serialVersionUID = 1L;
	/**Constante con la cadena pagina de monitor*/
	private static final String PAG_PROG_TRASPASOS = "programaTraspasos";
	/**Constante con la cadena pagina de monitor*/
	private static final String VAL_HORA = "selHora";
	/**Constante con la cadena importe*/
	private static final String IMPORTE = "importeTR";
	/**Constante con la cadena pagina de monitor*/
	private static final String DATOS_HTR = "datosHTR";
	/**Constante con la cadena codError.*/
    private static final String COD_ERROR_PUNTO = "codError.";
    /**Constante con la cadena codError*/
    private static final String COD_ERROR = "codError";
    /**Constante con la cadena tipoError*/
    private static final String TIPO_ERROR = "tipoError";
    /**Constante con la cadena descError*/
    private static final String DESC_ERROR = "descError";
	
	/**  Referencia al servicio del catalogo*/
	private BOProgTraspasos boProgTraspasos;
	
	 /**
	  * Metodo que se utiliza para mostrar la pagina de
	  * Progracion de Traspasos
	  * @param paginador BeanPaginador del tipo HttpServletRequest
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraProgTraspasos.do")
	 public ModelAndView muestraProgTraspasos(BeanPaginador paginador,HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_PROG_TRASPASOS);
		 BeanProgTraspasosBO beanAdministraSaldoBO= new BeanProgTraspasosBO();
		 try{
			 beanAdministraSaldoBO.setPaginador(paginador);
			 beanAdministraSaldoBO = boProgTraspasos.consultaHorariosTransferencias(beanAdministraSaldoBO,getArchitechBean());
			 beanAdministraSaldoBO.setImporteProgTran(req.getParameter(IMPORTE));
			 modelAndView.addObject(DATOS_HTR, beanAdministraSaldoBO);
		 }catch(BusinessException e){
			 showException(e); 
		 }
		 return modelAndView;
	 }
	 

	 /**
	  * Metodo que se utiliza para insertar un horario
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/altaHorarioTR.do")
	 public ModelAndView altaHorarioTR(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_PROG_TRASPASOS);
		 BeanProgTraspasosBO bean = new BeanProgTraspasosBO();
		 BeanProgTraspasosBO beanProgTraspasosBO = new BeanProgTraspasosBO();
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //datos de pantalla
			 bean.setHora(req.getParameter(VAL_HORA));
			 bean.setDescripcion(req.getParameter("descripTR"));
			 //realiza el insert
			 beanProgTraspasosBO=boProgTraspasos.agregaHorarioTrasnferencias(bean, getArchitechBean());
			 modelAndView.addObject(COD_ERROR ,beanProgTraspasosBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanProgTraspasosBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanProgTraspasosBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
		     //lista de horarios
		     beanProgTraspasosBO = boProgTraspasos.consultaHorariosTransferencias(beanProgTraspasosBO,getArchitechBean());
		     beanProgTraspasosBO.setImporteProgTran(req.getParameter(IMPORTE));
		     modelAndView.addObject(DATOS_HTR, beanProgTraspasosBO);
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para eliminar un horario
	  * @param bean Objeto del tipo BeanProgTraspasosBO
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/eliminaHorarioTR.do")
	 public ModelAndView eliminaHorarioTR(BeanProgTraspasosBO bean, HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_PROG_TRASPASOS);
		 BeanProgTraspasosBO beanProgTraspasosBO = new BeanProgTraspasosBO();
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //realiza la eliminacion
			 beanProgTraspasosBO=boProgTraspasos.eliminaHorarioTrasnferencias(bean, getArchitechBean());
			 modelAndView.addObject(COD_ERROR ,beanProgTraspasosBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanProgTraspasosBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanProgTraspasosBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
		     //lista de horarios
			 beanProgTraspasosBO = boProgTraspasos.consultaHorariosTransferencias(beanProgTraspasosBO,getArchitechBean());
			 beanProgTraspasosBO.setImporteProgTran(req.getParameter(IMPORTE));
			 modelAndView.addObject(DATOS_HTR, beanProgTraspasosBO);
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }


	/**
	 * @return el boProgTraspasos
	 */
	public BOProgTraspasos getBoProgTraspasos() {
		return boProgTraspasos;
	}


	/**
	 * @param boProgTraspasos el boProgTraspasos a establecer
	 */
	public void setBoProgTraspasos(BOProgTraspasos boProgTraspasos) {
		this.boProgTraspasos = boProgTraspasos;
	}


}
