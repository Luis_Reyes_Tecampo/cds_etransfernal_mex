/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerCatCuentasFiduciario.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     17/09/2019 01:51:45 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOCuentasFiduciario;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarCuentasFideico;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsGeneral;

/**
 * Class ControllerCatCuentasFiduciario.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * de cuentas fiduciaro
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Controller
public class ControllerCatCuentasFiduciario extends Architech implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1698873774968582074L;

	/** La variable que contiene informacion con respecto a: bo cuentas fiduciario. */
	private BOCuentasFiduciario boCuentasFiduciario;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaCuentasFiduciario";
	
	/** La constante BEAN_CUENTAS. */
	private static final String BEAN_CUENTAS = "beanCuentas";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";

	/** La constante utilerias. */
	private static final UtilsExportarCuentasFideico utilerias = UtilsExportarCuentasFideico.getInstancia();
	
	/** La constante utilsGeneral. */
	private static final UtilsGeneral utilsGeneral = UtilsGeneral.getUtils();
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/consultaCuentasFiduciario.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanCuenta beanFilter, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanCuentas response = null;
		try {
			/** Ejecuta peticion al BO **/
			response = boCuentasFiduciario.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
				
			}
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, response);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 * @param beanCuentas --> objeto de tipo cuenta enviado para realizar el alta a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/exportarCuentasFiduciario.do")
	public ModelAndView exportarCuentas(@ModelAttribute(BEAN_CUENTAS) BeanCuentas beanCuentas, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcelSPID(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBodySPID(beanCuentas.getCuentas()));
		modelAndView.addObject("NAME_SHEET", "Cuentas Fiduciario");
		/** Retorno del modelo a la vista **/
		return modelAndView;
	}
	
	/**
	 * Mapeo del metodo para iniciar el flujo de exportar los numeros de cuenta
	 * al proceso de inserccion en BD.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanCuenta --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value="/exportarTodoCuentasFiduciario.do")
	public ModelAndView exportarTodosCuentas(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanCuenta beanCuenta, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanCuentas responseConsulta = new BeanCuentas();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boCuentasFiduciario.consultar(beanCuenta, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boCuentasFiduciario.exportarTodo(beanCuenta, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}

	/**
	 * Metodo Get para obtener el numero de cuenta
	 *
	 * @return boCuentasFiduciario --> variable de tipo  clase que regresa el numero de cuenta
	 */
	public BOCuentasFiduciario getBoCuentasFiduciario() {
		return boCuentasFiduciario;
	}

	/**
	 * Metodo Set para obtener el numero de cuenta
	 *
	 * @param boCuentasFiduciario --> variable de tipo  clase que asigna el numero de cuenta
	 */
	public void setBoCuentasFiduciario(BOCuentasFiduciario boCuentasFiduciario) {
		this.boCuentasFiduciario = boCuentasFiduciario;
	}

}
