/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerParametrosPOACOA.java
 * 
 * La clase ha sido modificada
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 06:49:13 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOParametrosPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.MapeaParametrosPOACOA;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion  de los parametros.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
@Controller
public class ControllerParametrosPOACOA extends Architech{

	 /** Constante del Serial Version. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Constante con el nombre de la pagina. */
	private static final String PAG_PARAMETROS_POA_COA ="parametrosPOACOA";
	
	/** Constante ERROR_PANT. */
	private static final String MSJ_PANT = "msjPantalla";
	
	/** Constante ERROR_TITULO. */
	private static final String MSJ_TITULO = "msjTitulo";
	
	
	/** Titulo pantalla. */
	private static final String PARAMETROS = "Par\u00E1metros POA/COA";
	
	/** Constante cero. */
	private static final String CERO = "0";
	
	/** Constante uno. */
	private static final String UNO = "1";
	
	/**  BO que contiene las operaciones de los parametros POA COA. */
	private BOParametrosPOACOA boParametrosPOACOA;
	
	
	/** La constante UTILS_POACOA. */
	private static final MapeaParametrosPOACOA UTILS_POACOA	= MapeaParametrosPOACOA.getInstancia();
	
	/**
	 * Metodo que muestra la pantalla de parametros del POA/COA.
	 *
	 * @param req El objeto: req
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="parametrosPOACOA.do")
	public ModelAndView muestraParamPOACOA(HttpServletRequest req) {
		ModelAndView modelAndView = null;
		String modulo = ConstantesWebPOACOA.CONS_ACTUAL;
		String banderaModulo = "1";
		String moduloParams = req.getParameter(ConstantesWebPOACOA.CONS_SPID);
		/** Se valida el paramtro que se ingresa por el url **/
		if (moduloParams == null || "2".equals(moduloParams)) {
			if ("2".equals(moduloParams)) {
				modulo = ConstantesWebPOACOA.CONS_SPID;
				banderaModulo = "2";
			}
		} else {
			/** Si se ingresa un parametro erroneo entonces se devuelve la vista inicial **/
			modelAndView = new ModelAndView("inicio");
			modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_SPID);
			return modelAndView;
		}
		Map<String, Object> model = new HashMap<String, Object>();
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(banderaModulo));

		modelAndView = new ModelAndView(PAG_PARAMETROS_POA_COA, model);
		/** Se setea el modulo para hacer las validaciones en el JSP **/
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, modulo);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Activar o desactivar POA.
	 *
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="activarDesactivarPOA.do")
	public ModelAndView activarDesactivarPOA(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = null;
		String poa = request.getParameter("procesoPOA");
		String coa = request.getParameter("procesoCOAParaPOA");
		/** Obtencion de la varible que se envia desde el JSP para validar el modulo **/
		String moduloPOA = request.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloView = ConstantesWebPOACOA.CONS_ACTUAL;
		boolean esActivacion = false;
		boolean esActivoCOA = false;
		String view = PAG_PARAMETROS_POA_COA;
		String msjExito = "Se desactiv\u00f3 correctamente el proceso POA";
		if (poa.equals(UNO)) {
			esActivacion = true;
			msjExito = "Se activ\u00f3 correctamente el proceso POA";
		}
		if (coa.equals(UNO)) {
			esActivoCOA = true;
		}
		/** Se hace la peticion al BO **/
		BeanResBase respuestaBase = boParametrosPOACOA.actDesPOA(esActivacion,
				esActivoCOA, moduloPOA, getArchitechBean());
		Map<String, Object> model = new HashMap<String, Object>();
		/** Validacion de la respuesta del BO **/
		if(Errores.OK00000V.equals(respuestaBase.getCodError())){
			model.put(Constantes.COD_ERROR, respuestaBase.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, msjExito);
		}else{
			RequestContext ctxReq = new RequestContext(request);
			String mensaje = ctxReq.getMessage(Constantes.COD_ERROR_PUNTO+respuestaBase.getCodError());
			model.put(Constantes.COD_ERROR, respuestaBase.getCodError());
			model.put(Constantes.TIPO_ERROR, respuestaBase.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		/** Se valida el parametro del modulo actual **/
		if ("2".equals(moduloPOA)) {
			moduloView = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(moduloPOA));
		modelAndView = new ModelAndView(view, model);
		/** Se setea el modulo para hacer las validaciones en el JSP **/
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloView);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Activar o desactivar los parametros para COA.
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="activarDesactivarCOA.do")
	public ModelAndView activarDesactivarCOA(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = null;
		Map<String, Object> model = new HashMap<String, Object>();
		String coa = request.getParameter("procesoCOA");
		/** Obtencion de la varible que se envia desde el JSP para validar el modulo **/
		String moduloCOA = request.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloViewCOA = ConstantesWebPOACOA.CONS_ACTUAL;
		boolean esActivacion = false;
		String view = PAG_PARAMETROS_POA_COA;
		String msjExito = "Se desactiv\u00f3 correctamente el proceso COA";
		if (coa.equals(UNO)) {
			esActivacion = true;
			msjExito = "Se activ\u00f3 correctamente el proceso COA";
		}
		/** Se hace la peticion al BO **/
		BeanResBase respuestaBaseCoa = boParametrosPOACOA.actDescCOA(esActivacion, moduloCOA,
				getArchitechBean());
		/** Validacion de la respuesta del BO **/
		if(Errores.OK00000V.equals(respuestaBaseCoa.getCodError())){
			model.put(Constantes.COD_ERROR, respuestaBaseCoa.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, msjExito);
		}else{
			RequestContext ctx = new RequestContext(request);
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+respuestaBaseCoa.getCodError());
			model.put(Constantes.COD_ERROR, respuestaBaseCoa.getCodError());
			model.put(Constantes.TIPO_ERROR, respuestaBaseCoa.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		/** Se valida el parametro del modulo actual **/
		if ("2".equals(moduloCOA)) {
			moduloViewCOA = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(moduloCOA));
		modelAndView = new ModelAndView(view, model);
		/** Se setea el modulo para hacer las validaciones en el JSP **/
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloViewCOA);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Activa alguna de las fases para los parametros.
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="parametrosFase.do")
	public ModelAndView parametrosFase(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = null;
		Map<String, Object> model = new HashMap<String, Object>();
		String numeroFase = request.getParameter("numeroFase");
		String liqFinal = request.getParameter("liqFinal");
		/** Obtencion de la varible que se envia desde el JSP para validar el modulo **/
		String modulo = request.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloViewFase = ConstantesWebPOACOA.CONS_ACTUAL;
		/** Se hace la peticion al BO **/
		BeanResBase respuestaBaseFase = boParametrosPOACOA.activaFases(numeroFase,liqFinal, modulo,
				getArchitechBean());
		model.put(MSJ_PANT, true);
		model.put(MSJ_TITULO, PARAMETROS);
		/** Validacion de la respuesta del BO **/
		if (Errores.OK00000V.equals(respuestaBaseFase.getCodError())) {
			model.put(Constantes.COD_ERROR, respuestaBaseFase.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, "Se activ\u00f3 la fase " + numeroFase
					+ " correctamente");
		} else {
			RequestContext ctxFase = new RequestContext(request);
			String mensaje = ctxFase.getMessage(Constantes.COD_ERROR_PUNTO+respuestaBaseFase.getCodError());
			model.put(Constantes.COD_ERROR, respuestaBaseFase.getCodError());
			model.put(Constantes.TIPO_ERROR, respuestaBaseFase.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		/** Se valida el parametro del modulo actual **/
		if ("2".equals(modulo)) {
			moduloViewFase = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(modulo));
		modelAndView = new ModelAndView(PAG_PARAMETROS_POA_COA, model);
		/** Se setea el modulo para hacer las validaciones en el JSP **/
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloViewFase);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Activa alguna de las fases para los parametros.
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="generarArchivoPOACOA.do")
	public ModelAndView generarArchivoPOACOA(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = null;
		Map<String, Object> model = new HashMap<String, Object>();
		String tipoArchivoGenerar = request.getParameter("tipoArchivoGenerar");
		/** Obtencion de la varible que se envia desde el JSP para validar el modulo **/
		String modulo = request.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloViewArch = ConstantesWebPOACOA.CONS_ACTUAL;
		/** Se hace la peticion al BO **/
		BeanResBase beanResBaseArch = boParametrosPOACOA.generarArchivo(tipoArchivoGenerar, modulo,
				getArchitechBean());
		model.put(MSJ_PANT, true);
		model.put(MSJ_TITULO, PARAMETROS);
		/** Validacion de la respuesta del BO **/
		if (Errores.OK00000V.equals(beanResBaseArch.getCodError())) {
			model.put(Constantes.COD_ERROR, beanResBaseArch.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, "Se va a generar el tipo de archivo: " + tipoArchivoGenerar
					+ " correctamente");
		} else {
			RequestContext ctxArch = new RequestContext(request);
			String mensaje = ctxArch.getMessage(Constantes.COD_ERROR_PUNTO+beanResBaseArch.getCodError());
			model.put(Constantes.COD_ERROR, beanResBaseArch.getCodError());
			model.put(Constantes.TIPO_ERROR, beanResBaseArch.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		/** Se valida el parametro del modulo actual **/
		if ("2".equals(modulo)) {
			moduloViewArch = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(modulo));
		modelAndView = new ModelAndView(PAG_PARAMETROS_POA_COA, model);
		/** Se setea el modulo para hacer las validaciones en el JSP **/
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloViewArch);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Activa alguna de las fases para el Cifrado de los parametros.
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="activarDesactivarCifrado.do")
	public ModelAndView activarDesactivarCifrado(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> model = new HashMap<String, Object>();
		String parametroCifrado = request.getParameter("parametroCifrado");
		String msjExito = "Se activ\u00f3 la firma de archivo";
		/** Obtencion de la varible que se envia desde el JSP para validar el modulo **/
		String modulo = request.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		if(parametroCifrado.equals(CERO)) {
			msjExito = "Se desactiv\u00f3 la firma de archivo";
		}
		/** Se hace la peticion al BO **/
		BeanResBase respuestaBaseCifra = boParametrosPOACOA.actDesFirmas(parametroCifrado, getArchitechBean());
		model.put(MSJ_PANT, true);
		model.put(MSJ_TITULO, PARAMETROS);
		/** Validacion de la respuesta del BO **/
		if (Errores.OK00000V.equals(respuestaBaseCifra.getCodError())) {
			model.put(Constantes.COD_ERROR, respuestaBaseCifra.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, msjExito);
		} else {
			RequestContext ctx = new RequestContext(request);
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+respuestaBaseCifra.getCodError());
			model.put(Constantes.COD_ERROR, respuestaBaseCifra.getCodError());
			model.put(Constantes.TIPO_ERROR, respuestaBaseCifra.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		/** Se invoca al metodo que setea los parametros al modelo **/
		model.putAll(obtenParametros(modulo));
		/** Retorno del metodo **/
		return new ModelAndView(PAG_PARAMETROS_POA_COA, model);
	}
	
	/**
	 * Obtiene mapa de los parametros.
	 *
	 * @param modulo El objeto: modulo
	 * @return Map<String, Object> mapa con los valores de parametros
	 */
	public Map<String, Object> obtenParametros(String modulo)  {
		Map<String, Object> model = null;
		/** Se hace la peticion al BO **/
		BeanResPOACOA beanResPOACOA = boParametrosPOACOA.obtenParametrosPOACOA(modulo, getArchitechBean());
		/** Validacion de la respuesta del BO **/
		if (beanResPOACOA.getActivacion() == null
				|| beanResPOACOA.getActivacion().equals(CERO)) {
			model = UTILS_POACOA.seteaValores(true,true,false,false,false,false,false,false,false,false);
		}else{
			model = UTILS_POACOA.mapeaFases(beanResPOACOA);
		}
		/** Retorno del metodo **/
		return model;
	}
	

	/**
	 * Devuelve el valor de boParametrosPOACOA.
	 *
	 * @return the boParametrosPOACOA
	 */
	public BOParametrosPOACOA getBoParametrosPOACOA() {
		return boParametrosPOACOA;
	}

	/**
	 * Modifica el valor de boParametrosPOACOA.
	 *
	 * @param boParametrosPOACOA the boParametrosPOACOA to set
	 */
	public void setBoParametrosPOACOA(BOParametrosPOACOA boParametrosPOACOA) {
		this.boParametrosPOACOA = boParametrosPOACOA;
	}
	
}
