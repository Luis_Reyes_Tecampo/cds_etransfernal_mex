/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerMantenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificado;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOMatenimientoCertificado;

/**
 * Controlador para mantenimiento de certificados.
 *
 * @author IDS
 */
@Controller
public class ControllerMantenimientoCertificado extends Architech {
	
	/** Propiedad del tipo long que mantiene el estado del objeto. */
	private static final long serialVersionUID = -5554454613812951527L;
	
	/** Propiedad del tipo long que trae el tipo de sesi�n. */
	private static final String SESSION_SPID = "sessionSPID";
	
	/** Propiedad del tipo long que trae el nombre del m�dulo seleccionado por el usuario. */
	private static final String NOMPANTALLA = "nomPantalla";
	
	/** Propiedad. */
	private static final String NUEVOMANTENIMIENTO = "nuevoMantenimientoCeritificado";
	
	/** Propiedad. */
	private static final String BEANMANTENIMIENTO = "beanMantenimientoCertificado";
	
	/** Propiedad que almacen el parametro enviado. */
	private static final String mantenimientoCertificadoSPEI ="mantenimientoCertificadoSPEI";
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La variable que contiene informacion con respecto a: session. */
	@Autowired
	private BeanSessionSPID session;
	
	/** Propiedad del tipo MessageSource que almacena el valor de messageSource. */
	private MessageSource messageSource;
	
	/** Propiedad del tipo BoMantenimientoCertificado. */
	private BOMatenimientoCertificado bOMatenimientoCertificado;
	
	/** La constante NMANTENIMIENTOCERTIFICADO. */
	private static final String NMANTENIMIENTOCERTIFICADO = "mantenimientoCertificado";
	
	/** La constante NBEANRESMANTENIMIENTOCERTIFICADO. */
	private static final String NBEANRESMANTENIMIENTOCERTIFICADO = "beanResMatenimientoCertificado";
	
	/** La constante NCODERROR. */
	private static final String NCODERROR = "codError.";
	
	/**
	 * Metodo que obtiene los ceritificados dados de alta.
	 *
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return ModelAndView
	 */
	@RequestMapping(value="/mantenimientoCertificados.do")
	public ModelAndView mantenimientoCertificados(BeanPaginador beanPaginador, @RequestParam(required = false) String nomPantalla) {
		ModelAndView model = null;
		BeanResMantenimientoCertificado beanResMatenimientoCertificado = null;
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
		
		try {			
			beanResMatenimientoCertificado = bOMatenimientoCertificado.obtenerMantenimientoCertificados(
					beanPaginador, getArchitechBean(), nomPantalla);
			model = new ModelAndView(NMANTENIMIENTOCERTIFICADO, 
					NBEANRESMANTENIMIENTOCERTIFICADO, beanResMatenimientoCertificado);
			model.addObject(SESSION_SPID, sessionSPID);
			model.addObject(NOMPANTALLA, nomPantalla);
		} catch (BusinessException be) {
			showException(be);
		}
		
		return model;
	}
	
	/**
	 * Metodo que muestra la pantalla de nuevo certificado.
	 *
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return ModelAndView
	 */
	@RequestMapping(value="/agregarMantenimiento.do")
	public ModelAndView agregarMantenimiento(@RequestParam(required = false) String nomPantalla) {
		ModelAndView model = new ModelAndView(NUEVOMANTENIMIENTO);
		
		model.addObject(BEANMANTENIMIENTO, new BeanMantenimientoCertificado());
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
		model.addObject(NOMPANTALLA, nomPantalla);
		model.addObject(SESSION_SPID, sessionSPID);
		return model;
	}
	
	/**
	 * Metodo que guarda un nuevo certificado .
	 *
	 * @param beanMantenimientoCertificado Objeto del tipo BeanMantenimientoCertificado
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return ModelAndView
	 */
	@RequestMapping(value="/guardarMantenimiento.do")
	public ModelAndView guardarTopologia(@ModelAttribute(value=BEANMANTENIMIENTO) BeanMantenimientoCertificado beanMantenimientoCertificado, 
			BeanPaginador beanPaginador, @RequestParam(required = false) String nomPantalla) {
		ModelAndView model = null;
		BeanResMantenimientoCertificado beanResMatenimientoCertificado = null;
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
	
		try {	
			beanMantenimientoCertificado.setClaveIns(sessionSPID.getCveInstitucion());
			beanResMatenimientoCertificado = bOMatenimientoCertificado.guardarCertificado(beanMantenimientoCertificado, getArchitechBean(), beanPaginador, nomPantalla);
			
			if (Errores.OK00000V.equals(beanResMatenimientoCertificado.getCodError())){
				model = new ModelAndView(NMANTENIMIENTOCERTIFICADO, 
						NBEANRESMANTENIMIENTOCERTIFICADO, beanResMatenimientoCertificado);
			}else{
				model = new ModelAndView(NUEVOMANTENIMIENTO, NBEANRESMANTENIMIENTOCERTIFICADO, beanResMatenimientoCertificado);
				model.addObject(BEANMANTENIMIENTO, new BeanMantenimientoCertificado());
			}
			
			model.addObject(Constantes.COD_ERROR, beanResMatenimientoCertificado.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResMatenimientoCertificado.getTipoError());
			model.addObject(Constantes.DESC_ERROR, toUpper(messageSource.getMessage(
					NCODERROR+beanResMatenimientoCertificado.getCodError(), null, null)));
			model.addObject(SESSION_SPID, sessionSPID);
			model.addObject(NOMPANTALLA, nomPantalla);
		} catch (BusinessException be) {
			showException(be);
		}

		return model;
	}
	
	/**
	 * Metodo que muestra la pantalla de edicion de certificados.
	 *
	 * @param beanReqMantenimientoCertificado Objeto del tipo BeanReqMantenimientoCertificado
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return ModelAndView
	 */
	@RequestMapping(value="/editarMantenimiento.do")
	public ModelAndView editarMantenimiento(BeanReqMantenimientoCertificado beanReqMantenimientoCertificado, @RequestParam(required = false) String nomPantalla) {
		ModelAndView model = null;
		BeanResMantenimientoCertificado beanResMantenimientoCertificado = null;
		BeanMantenimientoCertificado beanMantenimientoCertificado = null;
		
		for(BeanMantenimientoCertificado beanCertitficado : 
			beanReqMantenimientoCertificado.getListaMantenimientoCertificado()) {
			if (beanCertitficado.isSeleccionado()) {
				beanMantenimientoCertificado = beanCertitficado;
		}
		}
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
		
		model = new ModelAndView(NUEVOMANTENIMIENTO, 
				NBEANRESMANTENIMIENTOCERTIFICADO, beanResMantenimientoCertificado);
		model.addObject("isEditar", "editar");
		model.addObject(BEANMANTENIMIENTO, beanMantenimientoCertificado);
		model.addObject("beanMantenimientoCertificadoOld", beanMantenimientoCertificado);
		model.addObject(SESSION_SPID, sessionSPID);
		model.addObject(NOMPANTALLA, nomPantalla);
		return model;
	}
	
	/**
	 * Metodo que guarda la edicion den un certificado .
	 *
	 * @param beanMantenimientoCertificado Objeto del tipo BeanMantenimientoCertificadoo
	 * @param numeroCertOld Objeto del tipo String
	 * @param cveInsOld Objeto del tipo String
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/guardarEdicionMantenimiento.do")
	public ModelAndView guardarEdicionMantenimiento(@ModelAttribute(value="beanTopologia") BeanMantenimientoCertificado beanMantenimientoCertificado,
			@RequestParam(value="numCertOld", required=false) String numeroCertOld,
			@RequestParam(value="cveInsOld", required=false) String cveInsOld, BeanPaginador beanPaginador,
			@RequestParam(required = false) String nomPantalla) {
		ModelAndView model = null;
		BeanResMantenimientoCertificado beanResMantenimientoCertificado = null;
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
		
		try {	
			beanResMantenimientoCertificado = bOMatenimientoCertificado.guardarEdicionMantenimiento(
					beanMantenimientoCertificado, getArchitechBean(), beanPaginador, numeroCertOld, cveInsOld, nomPantalla);

			if (Errores.OK00000V.equals(beanResMantenimientoCertificado.getCodError())){
				model = new ModelAndView(NMANTENIMIENTOCERTIFICADO, 
						NBEANRESMANTENIMIENTOCERTIFICADO, beanResMantenimientoCertificado);
			}else{
				model = new ModelAndView(NUEVOMANTENIMIENTO, NBEANRESMANTENIMIENTOCERTIFICADO, beanResMantenimientoCertificado);
				model.addObject(BEANMANTENIMIENTO, new BeanMantenimientoCertificado());
			}			
			
			model.addObject(Constantes.COD_ERROR, beanResMantenimientoCertificado.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResMantenimientoCertificado.getTipoError());
			model.addObject(Constantes.DESC_ERROR,  toUpper(messageSource.getMessage(
					NCODERROR+beanResMantenimientoCertificado.getCodError(), null, null)));
			model.addObject(SESSION_SPID, sessionSPID);
			model.addObject(NOMPANTALLA, nomPantalla);
		} catch (BusinessException be) {
			showException(be);
		}

		return model;
	}

	/**
	 * Metodo que elimina un certificado .
	 *
	 * @param beanReqMantenimientoCertificado Objeto del tipo BeanReqMantenimientoCertificado
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @param beanPaginador El objeto: bean paginador
	 * @return ModelAndView
	 */
	@RequestMapping(value="/eliminarMantenimiento.do")
	public ModelAndView eliminarTopologia(BeanReqMantenimientoCertificado beanReqMantenimientoCertificado, @RequestParam(required = false) String nomPantalla, BeanPaginador beanPaginador) {
		ModelAndView model = null;
		BeanResMantenimientoCertificado beanResMatenimientoCertificado = null;
		
		BeanSessionSPID sessionSPID = getSession(nomPantalla);
		
		try {
			beanResMatenimientoCertificado = bOMatenimientoCertificado.eliminarTopologia(
					beanReqMantenimientoCertificado, getArchitechBean(), nomPantalla,beanPaginador);
			model = new ModelAndView(NMANTENIMIENTOCERTIFICADO, 
					NBEANRESMANTENIMIENTOCERTIFICADO, beanResMatenimientoCertificado);
			model.addObject(Constantes.COD_ERROR, beanResMatenimientoCertificado.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResMatenimientoCertificado.getTipoError());
			if (beanResMatenimientoCertificado.getCodError().equals(Errores.ED00139V)) {
				model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResMatenimientoCertificado.getCodError(), null, null));
			} else {
				model.addObject(Constantes.DESC_ERROR, toUpper(messageSource.getMessage(NCODERROR+beanResMatenimientoCertificado.getCodError(), null, null)));
			}
			model.addObject(SESSION_SPID, sessionSPID);
			model.addObject(NOMPANTALLA, nomPantalla);
			
		} catch (BusinessException be) {
			showException(be);
		}
		
		return model;
	}

	/**
	 * To upper.
	 *
	 * Metodo que sirve para convertir a mayuscula el mensaje
	 * retornado del messageSoruce 
	 * 
	 * @param mensaje El objeto: mensaje original 
	 * @return Objeto string con el mensaje nuevo
	 */
	private String toUpper(String mensaje) {
		String msj = mensaje;
		if (msj != null && !msj.isEmpty()) {
			msj = msj.toUpperCase();
		}
		return msj;
	}
	
	/**
	 * Obtener el objeto: session.
	 *
	 * Metodo para obtener la session SPEI o SPID en
	 * base al nombre de la pantalla que se recibe.
	 * 
	 * @param nomPantalla El objeto: nom pantalla
	 * @return El objeto: session
	 */
	private BeanSessionSPID getSession(String nomPantalla) {
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			if(mantenimientoCertificadoSPEI.equals(nomPantalla)) {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPEI();
			}else {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			}
		} catch (BusinessException e) {
			showException(e);
		}
		return sessionSPID;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad bOMatenimientoCertificado.
	 *
	 * @return bOMatenimientoCertificado Objeto del tipo BOMatenimientoCertificado
	 */
	public BOMatenimientoCertificado getBOMatenimientoCertificado() {
		return bOMatenimientoCertificado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOMatenimientoCertificado.
	 *
	 * @param bOMatenimientoCertificado Objeto del tipo BOMatenimientoCertificado
	 */
	public void setBOMatenimientoCertificado(BOMatenimientoCertificado bOMatenimientoCertificado) {
		this.bOMatenimientoCertificado = bOMatenimientoCertificado;
	}
	
	/**
	 * Obtener el objeto: session SPID.
	 *
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return session;
	}
	
	/**
	 * Definir el objeto: session SPID.
	 *
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.session = sessionSPID;
	}
	
	/**
	 * Obtener el objeto: message source.
	 *
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
