package mx.isban.eTransferNal.controllers.catalogos;

import javax.validation.Valid;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancierosHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOIntFinancierosHis;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.UtileriasHistorico;

/**
 * version 1.0
 * @controller ControllerIntFinancierosHis
 * Se encarga de recibir y procesar la peticion de Cat. Intermediarios / Historico.
 */
@Controller
public class ControllerIntFinancierosHis extends Architech {

	/**
	 * @author SNGEnterprise
	 * Fecha 29/30/2020
	 * Hora 00:00:00
	 */
	
	/**
	 * serialVersionUID  se crea por defecto segun los aspectos de la clase. 
	 */
	private static final long serialVersionUID = 4347260861554015670L;
	
	/** La constante VALIDACION_JSR_303. */
	private static final String VALIDACION_JSR_303 = "Validaciones JSR-303, valores de entrada incorrectos.";

	/** Constante con el nombre de la pagina de consulta. */
	private static final String PAG_CAT_INT_FINANCIEROS="catIntFinancierosHis";
	
	/** La variable que contiene informacion con respecto a: bo int financieros. */
	private BOIntFinancierosHis boIntFinancierosHis;

	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";

	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";

	/** La constante PAGINA_CONSULTA_CATALOGOS_DEV. */
	private static final String PAGINA_CONSULTA_CAT_FINAN_HIS = "catIntFinancierosHis";

	/**
	 * @category Object Type String
	 * Constante del tipo String que almacena el valor de BEAN_CATALOGOS
	*/
	private static final String BEAN_CAT_FINAN_HIS = "beanResIntFinancierosHist";
	
	/** 
	 * @category Object Type String
	 * La constante UTILERIAS
	*/
	private static final UtileriasHistorico UTILERIAS = UtileriasHistorico.getUtilerias();

	/**
	 * @category Object Type String La constante NAME_COLIBRI Inicializada con
	 *           el valor Colibri
	 */
	private static final String NAME_COLIBRI = "Colibri";

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private ResourceBundleMessageSource messageSource;
		
	/**
	 * Getter Obtiene el Objeto: MessageSource
	 * @return Objeto: messageSource
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	  * Definir el objeto: bo int financieros.
	  * @param messageSource El nuevo objeto: messageSource
	*/
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}
			
	/**
	 * Metodo muestraCatIntFinancierosHis Funcionalidad: Inicializa la vista consultaCodErrorDevoluciones.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/muestraCatIntFinancierosHis.do")
	public ModelAndView muestraCatIntFinancierosHis() {
		return new ModelAndView(PAG_CAT_INT_FINANCIEROS);
	}	
		
	/**
	 * Muestra cat inst financieras.
	 * 
	 * @param beanPaginador El objeto de BeanPaginador 
	 * @param beanFilter objeto de   BeanIntFinancieroHistorico
	 * @param bindingResult objeto validador de datos del bean
	 * @throws  BusinessException Exception controlada para los errores de la ejecucion  
	 * @return modelAndView objeto que contiene los deatos del modelo y el nombre de la vista
	 */
	@RequestMapping(value="/consultaCatIntFinancierosHis.do")
	public ModelAndView consultaCatIntFinancierosHis(@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) @Valid BeanIntFinancieroHistorico beanFilter, BindingResult bindingResult){
		
		validarBindingResult(bindingResult);
		ModelAndView modelAndView = new ModelAndView();
		BeanResIntFinancierosHist response = new BeanResIntFinancierosHist();
		try {
		response = boIntFinancierosHis.consultaIntFinaHist(getArchitechBean(), beanFilter,beanPaginador);
		modelAndView = new ModelAndView(PAGINA_CONSULTA_CAT_FINAN_HIS,BEAN_CAT_FINAN_HIS,response);
		if (!Errores.OK00000V.equals(response.getBeanError().getCodError())){
			modelAndView.addObject(Constantes.COD_ERROR,response.getBeanError().getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getBeanError().getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
		}
		}catch (BusinessException e){
 			showException(e);
		}
				
		return modelAndView;
		               
	}
		
	/**
	 * Metodo publico que sirve para obtener todos lo datos del Cat. Intermediarios historico y seran
	 * exportados a un archivos de excel el cual se deposita en un servidor.
	 * @param beanPaginador El objeto: BeanPaginador
	 * @param beanFilter El objeto: que contiene todos los datos del cat. intemediarios historico
	 * @param bindingResult objeto validador de datos del bean
	 * @return modelIntFinancieros Objeto ModelAndView
	 */
	@RequestMapping(value="exportarTodoIntFinancierosHis.do")
	public ModelAndView exportarTodoFinancieroHis(@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
					@ModelAttribute(BEAN_FILTER) @Valid BeanIntFinancieroHistorico beanFilter, BindingResult bindingResult) {
					
		validarBindingResult(bindingResult);
		ModelAndView modelHist = new ModelAndView();
		BeanResBase responseHis;
		BeanResIntFinancierosHist responseHistorico = new BeanResIntFinancierosHist();

		try {
			responseHistorico = boIntFinancierosHis.consultaIntFinaHist(getArchitechBean(), beanFilter, beanPaginador);
			if (Errores.ED00011V.equals(responseHistorico.getBeanError().getCodError())
					|| Errores.EC00011B.equals(responseHistorico.getBeanError().getCodError())) {
				modelHist = new ModelAndView(PAGINA_CONSULTA_CAT_FINAN_HIS,BEAN_CAT_FINAN_HIS, responseHistorico);
				modelHist.addObject(Constantes.COD_ERROR, responseHistorico.getBeanError().getCodError());
				modelHist.addObject(Constantes.DESC_ERROR, responseHistorico.getBeanError().getMsgError());
				modelHist.addObject(Constantes.TIPO_ERROR, responseHistorico.getBeanError().getTipoError());
				return modelHist;
			}
			responseHis = boIntFinancierosHis.exportarTodo(getArchitechBean(), beanFilter, responseHistorico.getTotalReg());
			modelHist = new ModelAndView(PAGINA_CONSULTA_CAT_FINAN_HIS,BEAN_CAT_FINAN_HIS, responseHistorico);
			if (Errores.OK00002V.equals(responseHis.getCodError())) {
				String mensajeHist = messageSource.getMessage(
						Constantes.COD_ERROR_PUNTO + responseHis.getCodError(),
						new Object[] { responseHis.getMsgError() }, LocaleContextHolder.getLocale());
				modelHist.addObject(Constantes.DESC_ERROR, mensajeHist);
			} else {
				modelHist.addObject(Constantes.DESC_ERROR, responseHis.getMsgError());
			}
			modelHist.addObject(Constantes.COD_ERROR, responseHis.getCodError());
			modelHist.addObject(Constantes.TIPO_ERROR, responseHis.getTipoError());

		} catch (BusinessException e) {
			showException(e);
		}
		return modelHist;
	}
	 
	/**
	 * Metodo  Funcionalidad: Exporta una coleccion de datos recibidos dentro del bean en un archivo "Cat. Financieros Historico.xls".
	 * @param  beanHistorico con los valores ingresados desde BeanResIntFinancierosHist.
	 * @param  bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	*/
	@RequestMapping(value = "/exportarHistorico.do")
	public ModelAndView exportarHistorico(@ModelAttribute(BEAN_CAT_FINAN_HIS) @Valid BeanResIntFinancierosHist beanHistorico,
				BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		formatCellBody = new FormatCell();
		formatCellBody.setFontName(NAME_COLIBRI);
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);
		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName(NAME_COLIBRI);
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);
		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", UTILERIAS.getHeaderExcel(messageSource));
		modelAndView.addObject("BODY_VALUE", UTILERIAS.getBody(beanHistorico.getIntFinancieroHistoricos()));
		modelAndView.addObject("NAME_SHEET", "Cat. Financieros Historico");
		return modelAndView;
	}
	    
	/**
	 * Metodo validarBindingResult, Funcionalidad: Validar si existen excepciones del Bean y enviarlas.
	 * @param bindingResult Objeto validador de datos del bean.
	 */
	public void validarBindingResult(BindingResult bindingResult) {
		if (bindingResult.hasFieldErrors()) {
			error(VALIDACION_JSR_303);
			error(bindingResult.getAllErrors().get(0).getDefaultMessage());
		}
	}
	    
	/**
	 * Obtener el objeto: bo int financieros.
	 *
	 * @return El objeto: bo int financieros
	 */
	public BOIntFinancierosHis getBoIntFinancierosHis() {
		return boIntFinancierosHis;
	}

	/**
	 * Definir el objeto: bo int financieros.
	 *
	 * @param boIntFinancieros El nuevo objeto: bo int financieros
	 */
	public void setBoIntFinancierosHis(BOIntFinancierosHis boIntFinancierosHis) {
		this.boIntFinancierosHis = boIntFinancierosHis;
	}
}
