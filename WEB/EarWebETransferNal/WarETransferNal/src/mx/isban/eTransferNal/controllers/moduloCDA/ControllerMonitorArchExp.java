/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorArchExp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:47:09 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExp;
import mx.isban.eTransferNal.servicio.moduloCDA.BOMonitorArchExp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones del Monitor de archivos a exportar
**/
@Controller
public class ControllerMonitorArchExp extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	/**
	 * Referencia al servicio de monitor de archivos exportar
	 */
	private BOMonitorArchExp bOMonitorArchExp;

   /**Metodo que se utiliza para mostrar la pagina de Monitor archivos
   * exportar vacio
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraMonitorArchExp.do")
   public ModelAndView muestraMonitorArchExp(){
      return new ModelAndView("monitorArchExp");
   }
   /**Metodo que se utiliza para mostrar la pagina de Monitor archivos
   * exportar con registros
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param req Objeto del tipo HttpServletRequest
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/consultarMonitorArchExp.do")
   public ModelAndView consultarMonitorArchExp(BeanPaginador beanPaginador, HttpServletRequest req){
      ModelAndView modelAndView = null;
      BeanResConsMonitorArchExp beanResConsMonitorArchExp = null;
      RequestContext ctx = new RequestContext(req);
      try {
		beanResConsMonitorArchExp = bOMonitorArchExp.consultarMonitorArchExp(beanPaginador, getArchitechBean());
		modelAndView = new ModelAndView("monitorArchExp","resMon",beanResConsMonitorArchExp);
		String mensaje = ctx.getMessage("codError."+beanResConsMonitorArchExp.getCodError());
	  	modelAndView.addObject("codError", beanResConsMonitorArchExp.getCodError());
	  	modelAndView.addObject("tipoError", beanResConsMonitorArchExp.getTipoError());
	  	modelAndView.addObject("descError", mensaje);
	} catch (BusinessException e) {
		showException(e);
	}
    
    return modelAndView;
   }
   
   /**
    * Metodo get del servicio del monitor de archivos exportar
    * @return bOMonitorArchExp Objeto del Servicio del monitor carga archivos historicos
    */   
   public BOMonitorArchExp getBOMonitorArchExp() {
	  return bOMonitorArchExp;
   }
	/**
    * Metodo set del servicio del monitor de archivos exportar
    * @param bOMonitorArchExp Se setea objeto del monitor de archivos exportar
    */
   public void setBOMonitorArchExp(BOMonitorArchExp bOMonitorArchExp) {
	  this.bOMonitorArchExp = bOMonitorArchExp;
   }



}
