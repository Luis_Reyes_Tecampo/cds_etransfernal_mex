/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResCancelacionPagos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOCancelacionPagos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Controllador para la cancelacion de pagos
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerCancelacionPagos  extends Architech {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1381565592264637980L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * BO para la pantalla de cancelacion de pagos
	 */
	private BOCancelacionPagos bOCancelacionPagos;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private static final String NCANCELACIONPAGOS = "cancelacionPagos";
	
	/**
	 * 
	 */
	private static final String NBEANRESCANCELACIONPAGOS = "beanResCancelacionPagos";
	
	/**
	 * 
	 */
	private static final String NCODERROR = "codError.";
	
	/**
	 * 
	 */
	private static final String SORT_FIELD = "sortField";
	
	/**
	 * 
	 */
	private static final String SORT_TYPE = "sortType";
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/cancelacionPagos.do")
	public ModelAndView viewHistorial(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento){
		ModelAndView modelAndView = null;
		
		BeanResCancelacionPagos beanResCancelacionPagos = null;
		 modelAndView = new ModelAndView(NCANCELACIONPAGOS);
		 
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		 try {
			 beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagos(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			 
			 modelAndView.addObject(NBEANRESCANCELACIONPAGOS,beanResCancelacionPagos);
			 String mensaje = messageSource.getMessage("codError."+beanResCancelacionPagos.getCodError(), null, null);
			
			 Integer tipoFiltro = 3;
			 
			 modelAndView.addObject("tipoFiltro", tipoFiltro);
			 
			 
			 modelAndView.addObject(Constantes.COD_ERROR, beanResCancelacionPagos.getCodError());
			 modelAndView.addObject(Constantes.TIPO_ERROR, beanResCancelacionPagos.getTipoError());
			 modelAndView.addObject(Constantes.DESC_ERROR,mensaje);
			 modelAndView.addObject("sessionSPID", sessionSPID);
			 modelAndView.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			 modelAndView.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			 modelAndView.addObject("url", "cancelacionPagos.do");
			 generarContadoresPag(modelAndView, beanResCancelacionPagos);
			 modelAndView.addObject("tipoRadio", 3);
		 } catch (BusinessException e) {
				showException(e);
			}
			  
		return modelAndView;		
	}

	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResCancelacionPagos beanResCancelacionPagos) {
		if (beanResCancelacionPagos.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResCancelacionPagos.getTotalReg();
			
			if (!"1".equals(beanResCancelacionPagos.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResCancelacionPagos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResCancelacionPagos.getTotalReg() >= 20 * Integer.parseInt(beanResCancelacionPagos.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResCancelacionPagos.getBeanPaginador().getPagina());
			}		
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanCancelacionPagos beanCons : beanResCancelacionPagos.getListaCancelacionPagos()){
				totalImporte = totalImporte.add(new BigDecimal(beanCons.getBeanCancelacionPagosDos().getImporteAbono()));
			}
			modelAndView.addObject("importePagina", totalImporte.toString());
		}
	}

	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/cancelacionPagosTraspasos.do")
	public ModelAndView viewHistorialTraspasos(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, @RequestParam(value="tipo", required=false) String tipo){
		ModelAndView modelAndView = null;
		
		BeanResCancelacionPagos beanResCancelacionPagos = null;
		 modelAndView = new ModelAndView(NCANCELACIONPAGOS);
		 
		 BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
		
		 try {			 
			 beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagosTraspasos(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			 
			 modelAndView.addObject(NBEANRESCANCELACIONPAGOS,beanResCancelacionPagos);
			 String mensaje = messageSource.getMessage("codError."+beanResCancelacionPagos.getCodError(), null, null);
			
			 Integer tipoFiltro = 2;
			 
			 modelAndView.addObject("tipoFiltro", tipoFiltro);
			 modelAndView.addObject("tipo", tipo);
			 
			 modelAndView.addObject(Constantes.COD_ERROR, beanResCancelacionPagos.getCodError());
			 modelAndView.addObject(Constantes.TIPO_ERROR, beanResCancelacionPagos.getTipoError());
			 modelAndView.addObject(Constantes.DESC_ERROR,mensaje);
			 modelAndView.addObject("sessionSPID", sessionSPID);
			 modelAndView.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			 modelAndView.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			 modelAndView.addObject("url", "cancelacionPagosTraspasos.do");
			 modelAndView.addObject("tipoRadio", 2);
			 generarContadoresPag(modelAndView, beanResCancelacionPagos);
		 } catch (BusinessException e) {
				showException(e);
			}
			  
		return modelAndView;
		
	}
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/cancelacionPagosOrden.do")
	public ModelAndView viewHistorialOrden(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, @RequestParam(value="tipo", required=false) String tipo){
		ModelAndView modelAndView = null;
		
		BeanResCancelacionPagos beanResCancelacionPagos = null;
		 modelAndView = new ModelAndView(NCANCELACIONPAGOS);
		 
		 BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
		 
		 try {
			 beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagosOrden(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			 
			 modelAndView.addObject(NBEANRESCANCELACIONPAGOS,beanResCancelacionPagos);
			 String mensaje = messageSource.getMessage("codError."+beanResCancelacionPagos.getCodError(), null, null);
			 
			 Integer tipoFiltro = 1;
			 
			 modelAndView.addObject("tipoFiltro", tipoFiltro);
			 modelAndView.addObject("tipo", tipo);
			
			 modelAndView.addObject(Constantes.COD_ERROR, beanResCancelacionPagos.getCodError());
			 modelAndView.addObject(Constantes.TIPO_ERROR, beanResCancelacionPagos.getTipoError());
			 modelAndView.addObject(Constantes.DESC_ERROR,mensaje);
			 modelAndView.addObject("sessionSPID", sessionSPID);
			 modelAndView.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			 modelAndView.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			 modelAndView.addObject("url", "cancelacionPagosOrden.do");
			 modelAndView.addObject("tipoRadio", 1);
			 generarContadoresPag(modelAndView, beanResCancelacionPagos);
		 } catch (BusinessException e) {
				showException(e);
			}
			  
		return modelAndView;
		
	}
	
	 /**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @param opcion Objeto del tipo RequestParam(value="opcion", required=false) String
	 * @param tipo Objeto del tipo RequestParam(value="tipo", required=false) String
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarCancelacionPagos.do")
	    public ModelAndView expCancelPagos(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, @RequestParam(value="opcion", required=false) String opcion, @RequestParam(value="tipo", required=false) String tipo, HttpServletRequest req){
		 
		 FormatCell formatCellHeaderCP = null;
			FormatCell formatCellBodyCP = null;
			ModelAndView model = null;
	  	 	RequestContext ctx = new RequestContext(req);
	  	 	
	  	 	BeanResCancelacionPagos beanResCancelacionPagos = null;
	  	 	 
	  	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
			 
				try {					
					if ("1".equals(opcion)){
						beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagosOrden(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
					} else if ("2".equals(opcion)){
						beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagosTraspasos(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
					} else if ("3".equals(opcion)){
						beanResCancelacionPagos=bOCancelacionPagos.obtenerCancelacionPagos(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
					}
					 
		  	 		if(Errores.OK00000V.equals(beanResCancelacionPagos.getCodError())||
		 				Errores.ED00011V.equals(beanResCancelacionPagos.getCodError())){
		  	 			formatCellBodyCP = new FormatCell();
		  	 			formatCellBodyCP.setFontName("Calibri");
		  	 			formatCellBodyCP.setFontSize((short)11);
		  	 			formatCellBodyCP.setBold(false);
		    	 	    
		  	 			formatCellHeaderCP = new FormatCell();
		  	 			formatCellHeaderCP.setFontName("Calibri");
		  	 			formatCellHeaderCP.setFontSize((short)11);
		  	 			formatCellHeaderCP.setBold(true);

		  	 			model = new ModelAndView(new ViewExcel());
		  	 			model.addObject("FORMAT_HEADER", formatCellHeaderCP);
		  	 			model.addObject("FORMAT_CELL", formatCellBodyCP);
		  	 			model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
		  	 			model.addObject("BODY_VALUE", getBody(beanResCancelacionPagos.getListaCancelacionPagos()));
		  	 			model.addObject("NAME_SHEET", "Cancelacion");
		  	 			model.addObject("tipo", tipo);
		  	 			model.addObject("tipoFiltro", opcion);
		  	 			model.addObject("tipoRadio", opcion);
		  	 		}else{
		  	 			model = viewHistorial(beanPaginador, beanOrdenamiento);
		  	 			String mensaje = messageSource.getMessage(NCODERROR+beanResCancelacionPagos.getCodError(), null, null);
		  	 			model.addObject(NCODERROR, beanResCancelacionPagos.getCodError());
		  	 			model.addObject("tipoError", beanResCancelacionPagos.getTipoError());
		  	 			model.addObject("descError", mensaje);
		  	 			model.addObject("sessionSPID", sessionSPID);
		  	 			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
		  	 			model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
		  	 			model.addObject("tipoFiltro", 3);
		  	 			model.addObject("tipo", tipo);
		  	 			model.addObject("tipoRadio", 3);
		  	 			generarContadoresPag(model, beanResCancelacionPagos);
		  	 		}
		 		} catch (BusinessException e) {
		 			showException(e);
			    }
		 
		 return model;
	 }	 
		
	    /**
	     * @param beanReqCancelacionPagos Objeto del tipo BeanReqCancelacionPagos
	     * @param opcion Objeto del tipo RequestParam
	     * @return ModelAndView
	     */
	    @RequestMapping(value = "/exportarTodoCancelacionPagos.do")
	    public ModelAndView expTodosCancelPagos(BeanReqCancelacionPagos beanReqCancelacionPagos, @RequestParam(value="opcion", required=false) String opcion ){
	   	 	ModelAndView model = null; 
	   	 	BeanResCancelacionPagos beanResCancelacionPagos = null;
	   	 	
	   	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}

			try { 			
				beanResCancelacionPagos = bOCancelacionPagos.exportarTodoCancelacionPagos(beanReqCancelacionPagos, getArchitechBean(), opcion);
				String mensaje = messageSource.getMessage("codError."+beanResCancelacionPagos.getCodError(),new Object[]{beanResCancelacionPagos.getNombreArchivo()}, null);
				
	   	 		model = new ModelAndView(NCANCELACIONPAGOS);
	 			model.addObject(NBEANRESCANCELACIONPAGOS,beanResCancelacionPagos);

	 			model.addObject(Constantes.COD_ERROR, beanResCancelacionPagos.getCodError());
	 			model.addObject(Constantes.TIPO_ERROR, beanResCancelacionPagos.getTipoError());
				model.addObject(Constantes.DESC_ERROR, mensaje );
				model.addObject("sessionSPID", sessionSPID);
				model.addObject("tipoFiltro", Integer.parseInt(opcion));
				generarContadoresPag(model, beanResCancelacionPagos);
	 	   } catch (BusinessException e) {
	 		 showException(e);
	 	   }
	        return model;
	    }
	    
	    
		/**
		 * @param beanReqCancelacionPagos Objeto del tipo BeanReqCancelacionPagos
		 * @return ModelAndView
		 */
		@RequestMapping(value="/NacCancelar.do")
		public ModelAndView enviarOrdenesReparacion(BeanReqCancelacionPagos beanReqCancelacionPagos, @RequestParam(value = "opcion", required = false) String opcion, @RequestParam(value = "tipo", required = false) String tipo) {
			BeanResCancelacionPagos beanResCancelacionPagos = null;
			ModelAndView model = new ModelAndView(NCANCELACIONPAGOS);
			
			BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}

			try {
				beanReqCancelacionPagos.setSFechaOperacion(sessionSPID.getFechaOperacion());
				beanReqCancelacionPagos.setSCveInstitucion(sessionSPID.getCveInstitucion());
		
				beanReqCancelacionPagos.setFiltro(opcion);
				beanResCancelacionPagos = bOCancelacionPagos.cancelar(beanReqCancelacionPagos,  getArchitechBean());
				 
				String servicio = "cancelacionPagosOrden.do";
				
				if ("2".equals(opcion)){
					servicio = "cancelacionPagosTraspasos.do";
				} else if ("3".equals(opcion)){
					servicio = "cancelacionPagos.do";
				}
					
				model.addObject(NBEANRESCANCELACIONPAGOS,beanResCancelacionPagos);
				
		     	model.addObject(Constantes.COD_ERROR, beanResCancelacionPagos.getCodError());
		     	model.addObject(Constantes.TIPO_ERROR, beanResCancelacionPagos.getTipoError());
		     	model.addObject(Constantes.DESC_ERROR, messageSource.getMessage(NCODERROR+beanResCancelacionPagos.getCodError(), null, null));
		     	model.addObject("sessionSPID", sessionSPID);
		     	model.addObject("url", servicio);
		     	model.addObject("tipoFiltro", opcion);
				model.addObject("tipo", tipo);
		     	generarContadoresPag(model, beanResCancelacionPagos);
			} catch (BusinessException e) {
				showException(e);
			}
			
			return model;
		}
		
	    
	/**
	 * @return BOCancelacionPagos objeto del tipo BOCancelacionPagos
	 */
	public BOCancelacionPagos getBOCancelacionPagos(){
		return bOCancelacionPagos;
	}
	
	/**
	 * @param bOCancelacionPagos Objeto del tipo BOCancelacionPagos
	 */
	public void setBOCancelacionPagos(BOCancelacionPagos bOCancelacionPagos){
		this.bOCancelacionPagos=bOCancelacionPagos;
	}

	/**
	 * @param listaCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	 * @return List<List<Object>>
	 */
	private List<List<Object>> getBody(List<BeanCancelacionPagos> listaCancelacionPagos){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	if(listaCancelacionPagos!= null && 
		   !listaCancelacionPagos.isEmpty()){
  		   objList = new ArrayList<List<Object>>();
  	
  		   for(BeanCancelacionPagos beanCancelacionPagos : listaCancelacionPagos){ 
  	 		  objListParam = new ArrayList<Object>();
  	 		  
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getReferencia());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getCola());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getCvnTran());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getMecan());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getMedioEnt());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getOrd());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getRec());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getImporteAbono());
  	 		objListParam.add(beanCancelacionPagos.getBeanCancelacionPagosDos().getEst());
		    objList.add(objListParam);
  	 	   }
  	   } 
   	return objList;
    }
	
    /**
     * @param ctx Objeto del tipo RequestContext
     * @return List<String>
     */
    private List<String> getHeaderExcel(RequestContext ctx){
      	 String []header = new String [] { 

       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.referencia"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.cola"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.tran"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.mecan"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.medioEnt"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.ord"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.rec"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.abono"),
       			 ctx.getMessage("moduloSPID.cancelacionPagos.text.est"),
		 }; 
  	 	 return Arrays.asList(header);
    }
    
    /**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}


