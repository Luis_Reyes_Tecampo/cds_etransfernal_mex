/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerHorariosSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/09/2015 12:42:00 INDRA		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanHorariosSPEI;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.servicio.catalogos.BOHorariosSPEI;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones del catalogo de horarios para el envio de pagos por SPEI
**/
@Controller
public class ControllerHorariosSPEI extends Architech {

	/*** Constante del Serial Version*/
	private static final long serialVersionUID = 7151706715788487408L;
	/*** Constante para establecer nombre del bean*/
	private static final String PAGINA_INS_UPD = "altaModifHorariosSPEI";
	/*** Constante para el parametro de lista de medios de entrega*/
	private static final String PARAM_MEDIO = "selMedEntrega";
	/** * Constante para el parametro de lista de transferencias*/
	private static final String PARAM_TRANSFER = "selTransfer";
	/*** Constante para el parametro de listas de claves de operacion*/
	private static final String PARAM_OPERA = "selOperacion";
	/** * Constante para el valor de topologia */
	private static final String VAL_TOPO = "selTopo";
	/** * Constante para el valor de hora de inicio */
	private static final String HORA_INICIO = "horaInicio";
	/** * Constante para el valor de hora de cierre */
	private static final String HORA_CIERRE = "horaCierre";
	/** * Constante para el valor de diahabil */
	private static final String DIA_HABIL = "banderaDiaHabil";
	/**Constante con la cadena codError.*/
    private static final String COD_ERROR_PUNTO = "codError.";
    /**Constante con la cadena codError*/
    private static final String COD_ERROR = "codError";
    /**Constante con la cadena tipoError*/
    private static final String TIPO_ERROR = "tipoError";
    /**Constante con la cadena descError*/
    private static final String DESC_ERROR = "descError";
    /*** Constante para el parametro de lista de medios de entrega*/
	private static final String LST_MEDIO = "lstMedEntrega";
	/** * Constante para el parametro de lista de transferencias*/
	private static final String LST_TRANSFER = "lstTransfer";
	/*** Constante para el parametro de listas de claves de operacion*/
	private static final String LST_OPERA = "lstOperacion";
	/*** Constante de pantalla*/
	private static final String PAG_CAT_HORARIOS_SPEI = "consultaHorariosSPEI";
	/**
	 *  Referencia al servicio del catalogo
	 * */
	private BOHorariosSPEI bOHorariosSPEI;

	 /**
	  * Metodo que se utiliza para mostrar la pagina de Horarios SPEI
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraCatHorariosSPEI.do")
	 public ModelAndView muestraCatHorariosSPEI(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_CAT_HORARIOS_SPEI);
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 try{
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
		     modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 
	 /**
	  * Metodo que se utiliza para consultar los horarios SPEI
	  * @param paginador objeto del tipo BeanPaginador
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/consultarHorarioSPEI.do")
	 public ModelAndView consultarHorarioSPEI(BeanPaginador paginador,HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_CAT_HORARIOS_SPEI);
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 BeanResHorariosSPEIBO bean = new BeanResHorariosSPEIBO();
		 
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //consulta datos 
			 bean.setCveMedioEntrega(req.getParameter(PARAM_MEDIO));
			 bean.setCveTransferencia(req.getParameter(PARAM_TRANSFER));
			 bean.setCveOperacion(req.getParameter(PARAM_OPERA));
			 bean.setPaginador(paginador);
			 beanResHorariosSPEIBO = bOHorariosSPEI.consultaHorariosSPEI(bean, getArchitechBean());
			 modelAndView.addObject("horariosSPEI",beanResHorariosSPEIBO);
			 modelAndView.addObject(COD_ERROR ,beanResHorariosSPEIBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResHorariosSPEIBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanResHorariosSPEIBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
			 //consulta listas
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(PARAM_MEDIO, bean.getCveMedioEntrega());
			 modelAndView.addObject(PARAM_TRANSFER, bean.getCveTransferencia());
		     modelAndView.addObject(PARAM_OPERA, bean.getCveOperacion());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 
	 /**
	  * Metodo que se utiliza para mostrar la pagina de captura de datos
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraAltaHorarioSPEI.do")
	 public ModelAndView muestraCatHorariosSPEIdet(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAGINA_INS_UPD);
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 
		 try{
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para mostrar la pagina de captura de datos
	  * @param bean Objeto del tipo BeanResHorariosSPEIBO
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraModificaHorarioSPEI.do")
	 public ModelAndView muestraModificaHorarioSPEI(BeanResHorariosSPEIBO bean,HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAGINA_INS_UPD);
		 RequestContext ctx = new RequestContext(req);
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 
		 try{
			 //consulta datos a modificar
			 beanResHorariosSPEIBO = bOHorariosSPEI.consultaHorarioSPEI(bean, getArchitechBean());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResHorariosSPEIBO.getCodError());
		     	//Si se obtiene error se direcciona a la pantalla de busqueda
		     if("ED00029V".equals(beanResHorariosSPEIBO.getCodError()))
		     {
		    	 modelAndView = new ModelAndView(PAG_CAT_HORARIOS_SPEI);
		    	 modelAndView.addObject(TIPO_ERROR, beanResHorariosSPEIBO.getTipoError());
			     modelAndView.addObject(DESC_ERROR, mensaje);
		    	 return modelAndView;
		     }
			 modelAndView.addObject("horarioSPEI", beanResHorariosSPEIBO.getHorarioSPEI());
			 modelAndView.addObject("altaMod","UPDATE");
			 //consulta listas
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		     
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 
	 /**
	  * Metodo que se utiliza para insertar un horario
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/altaHorarioSPEI.do")
	 public ModelAndView altaHorarioSPEI(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAGINA_INS_UPD);
		 BeanResHorariosSPEIBO bean = new BeanResHorariosSPEIBO();
		 BeanHorariosSPEI datos = new BeanHorariosSPEI();
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //datos de pantalla
			 datos.setCveMedioEnt(req.getParameter(PARAM_MEDIO));
			 datos.setCveTransfe(req.getParameter(PARAM_TRANSFER));
			 datos.setCveOperacion(req.getParameter(PARAM_OPERA));
			 datos.setCveTopoPri(req.getParameter(VAL_TOPO));
			 datos.setHoraIncio(req.getParameter(HORA_INICIO));
			 datos.setHoraCierre(req.getParameter(HORA_CIERRE));
			 datos.setFlgInhabil(req.getParameter(DIA_HABIL));
			 if(req.getParameter(DIA_HABIL) == null|| "".equals(req.getParameter(DIA_HABIL))){
				 datos.setFlgInhabil("0");
			 }
			 
			 bean.setHorarioSPEI(datos);
			 //realiza el insert
			 beanResHorariosSPEIBO=bOHorariosSPEI.agregaHorarioSPEI(bean, getArchitechBean());
			 modelAndView.addObject(COD_ERROR ,beanResHorariosSPEIBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResHorariosSPEIBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanResHorariosSPEIBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
			 //llenar las listas para la pantalla
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para modificar un horario
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/modificaHorarioSPEI.do")
	 public ModelAndView modificaHorarioSPEI(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_CAT_HORARIOS_SPEI);
		 BeanResHorariosSPEIBO bean = new BeanResHorariosSPEIBO();
		 BeanHorariosSPEI datos = new BeanHorariosSPEI();
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //datos de pantalla
			 datos.setCveMedioEnt(req.getParameter("auxCveMedios"));
			 datos.setCveTransfe(req.getParameter("auxCveTransfer"));
			 datos.setCveOperacion(req.getParameter("auxCveOper"));
			 datos.setCveTopoPri(req.getParameter(VAL_TOPO));
			 datos.setHoraIncio(req.getParameter(HORA_INICIO));
			 datos.setHoraCierre(req.getParameter(HORA_CIERRE));
			 datos.setFlgInhabil(req.getParameter(DIA_HABIL));
			 if(req.getParameter(DIA_HABIL) == null|| "".equals(req.getParameter(DIA_HABIL))){
				 datos.setFlgInhabil("0");
			 }
			 bean.setHorarioSPEI(datos);
			 //realiza la modificacion
			 beanResHorariosSPEIBO=bOHorariosSPEI.modificaHorarioSPEI(bean, getArchitechBean());
			 modelAndView.addObject(COD_ERROR ,beanResHorariosSPEIBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResHorariosSPEIBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanResHorariosSPEIBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
			 //llenar las listas para la pantalla
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject("altaMod", "UPDATE");
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para eliminar un horario
	  * @param bean Objeto del tipo BeanResHorariosSPEIBO
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/eliminaHorarioSPEI.do")
	 public ModelAndView eliminaHorarioSPEI(BeanResHorariosSPEIBO bean, HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_CAT_HORARIOS_SPEI);
		
		 BeanResHorariosSPEIBO beanResHorariosSPEIBO = new BeanResHorariosSPEIBO();
		 RequestContext ctx = new RequestContext(req);
		 try{
			 //realiza la eliminacion
			 beanResHorariosSPEIBO=bOHorariosSPEI.eliminaHorarioSPEI(bean, getArchitechBean());
			 modelAndView.addObject(COD_ERROR ,beanResHorariosSPEIBO.getCodError());
			 String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResHorariosSPEIBO.getCodError());
		     	modelAndView.addObject(TIPO_ERROR, beanResHorariosSPEIBO.getTipoError());
		     	modelAndView.addObject(DESC_ERROR, mensaje);
			 //llenar las listas para la pantalla
			 beanResHorariosSPEIBO=bOHorariosSPEI.llenaListas(getArchitechBean());
			 modelAndView.addObject(LST_MEDIO, beanResHorariosSPEIBO.getListMedioEntrega());
			 modelAndView.addObject(LST_TRANSFER, beanResHorariosSPEIBO.getListTrasnfer());
		     modelAndView.addObject(LST_OPERA, beanResHorariosSPEIBO.getListOperacion());
		 }catch(BusinessException e){
			 showException(e); 
		 }
	     return modelAndView;
	 }

	 /**
	    * Metodo get del servicio del catalogo
	    * @return bOBitacoraAdmon Objeto del servicio de la bitacora administrativa
	    */
	    public BOHorariosSPEI getBOHorariosSPEI() {
			return bOHorariosSPEI;
		}
	    /**
	    * Metodo set del servicio del catalogo
	    * @param horarioSPEI Se setea objeto del servicio de la bitacora administrativa
	    */
		public void setBOHorariosSPEI(BOHorariosSPEI horarioSPEI) {
			bOHorariosSPEI = horarioSPEI;
		}

}
