/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerRecepOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018      SMEZA		VECTOR      Creacion
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOperacionDevolver;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de Admon Saldos - Recepcion de Operaciones.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepOperaDevolver extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;
	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de BORecepOperacionExt. */
	private BORecepOperacionDevolver bORecepOperacionDevolver;
			
	/** Propiedad del tipo BORecepOpRechazar que almacena el valor de BORecepOperacion. **/
	private  BORecepOperacion bORecepOperacion;
	
	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modelVista;
	
	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();
	

	
	
	/**
	 * Metodo que permite devolver las operaciones.
	 *
	 * @param beanReqConsTranSpeiRec Objeto con los datos de la peticion
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/devolverTransfSpeiRec.do")
	public ModelAndView devolverTransfSpeiRec(BeanReqTranSpeiRec beanReqConsTranSpeiRec,HttpServletRequest req,
			HttpServletResponse res,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper){
		/**inicializan datos **/
		modelVista = null;
		BeanResTranSpeiRec beanResTranSpeiRec = null;
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpeiRec);
		RequestContext ctx = new RequestContext(req);
		beanReqConsTranSpeiRec.getPaginador().setAccion("ACT");
		try {
			String mensaje = null;
			
			BeanResTranSpeiRec beanResConsTranSpeiRec = null;
			/** llama la funcion de devolcer transferencia**/
			beanResConsTranSpeiRec = bORecepOperacionDevolver.devolverTransferSpeiRec(beanReqConsTranSpeiRec, getArchitechBean(), false);
			/**Se hace la busqueda de los datos**/			
			beanResTranSpeiRec = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpeiRec, getArchitechBean(), historico,beanTranSpeiRecOper);
			
			if(beanResConsTranSpeiRec.getBeanError().getCodError().contains("TRIB0000")){
			    mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00019V",new Object[]{beanResConsTranSpeiRec.getBeanComun().getReferenciaTransfer(),
			    		beanResConsTranSpeiRec.getBeanComun().getEstatus()});
			}else if(beanResConsTranSpeiRec.getBeanError().getCodError().contains(ConstantesRecepOpePantalla.TRIB)||beanResConsTranSpeiRec.getBeanError().getCodError().contains("TUBO")){
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00019V",new Object[]{beanResConsTranSpeiRec.getBeanComun().getReferenciaTransfer(),
						beanResConsTranSpeiRec.getBeanError().getCodError()});
			}else if("ED00090V".equals(beanResConsTranSpeiRec.getBeanError().getCodError())){
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"ED00091V");
			}else if((Errores.ETRAMA001).equals(beanResConsTranSpeiRec.getBeanError().getCodError())){
				mensaje = beanResConsTranSpeiRec.getBeanError().getMsgError();
			}else{
			    mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsTranSpeiRec.getBeanError().getCodError());
			}


			/**asigna la vista **/
			/**Se llama a la vista a mostrar o pantalla**/
			modelVista =  util.vistaMostrar(beanReqConsTranSpeiRec,beanResTranSpeiRec,historico);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modelVista.addObject("sessionBean", sessionBean); 
			modelVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			modelVista.addObject(Constantes.COD_ERROR, beanResConsTranSpeiRec.getBeanError().getCodError());
			modelVista.addObject(Constantes.TIPO_ERROR, beanResConsTranSpeiRec.getBeanError().getTipoError());
			modelVista.addObject(Constantes.DESC_ERROR, mensaje);
		}catch (BusinessException e) {
			showException(e);
			modelVista = util.errorVista(historico, beanResTranSpeiRec,beanReqConsTranSpeiRec,ctx);	
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage(), this.getClass());
		}
	    return modelVista;
	 }
	
	


	
	/**
	 * getbORecepOperacionDevolver de tipo BORecepOperacionDevolver.
	 * @author FSW-Vector
	 * @return bORecepOperacionDevolver de tipo BORecepOperacionDevolver
	 */
	public BORecepOperacionDevolver getbORecepOperacionDevolver() {
		return bORecepOperacionDevolver;
	}





	/**
	 * setbORecepOperacionDevolver para asignar valor a bORecepOperacionDevolver.
	 * @author FSW-Vector
	 * @param bORecepOperacionDevolver de tipo BORecepOperacionDevolver
	 */
	public void setbORecepOperacionDevolver(BORecepOperacionDevolver bORecepOperacionDevolver) {
		this.bORecepOperacionDevolver = bORecepOperacionDevolver;
	}





	/**
	 * Obtener el objeto: b O recep operacion.
	 *
	 * @return El objeto: b O recep operacion
	 */
	public BORecepOperacion getbORecepOperacion() {
		return bORecepOperacion;
	}



	/**
	 * Definir el objeto: b O recep operacion.
	 *
	 * @param bORecepOperacion El nuevo objeto: b O recep operacion
	 */
	public void setbORecepOperacion(BORecepOperacion bORecepOperacion) {
		this.bORecepOperacion = bORecepOperacion;
	}

	
	

	
}