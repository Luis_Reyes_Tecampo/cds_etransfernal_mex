/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloSPID.BOHistorialMensajes;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;

/**
 * Controlador para el historial de mensajes
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerHistorialMensajes extends Architech {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3462464581279850807L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * BO para la pantalla de Historial mensajes
	 */
	private BOHistorialMensajes bOHistorialMensajes;
	
	/**
	 * Metodo que muestra la pantalla incial de historial mensaje
	 * @param paginador Objeto del tipo BeanPaginador
	 * @param tipoOrden Objeto del tipo String
	 * @param referencia Objeto del tipo String
	 * @param beanReqHistorialMensajes Objeto del tipo BeanReqHistorialMensajes
	 * @return ModelAndView
	 */
	@RequestMapping(value="/historial.do")
	public ModelAndView viewHistorialMsj( BeanPaginador paginador, @RequestParam("tipo") String tipoOrden, @RequestParam("ref") String referencia, BeanReqHistorialMensajes beanReqHistorialMensajes ){
		beanReqHistorialMensajes.setRef(referencia);
		ModelAndView modelAndView = null;
		 
		 BeanResHistorialMensajes beanResHistorialMensajes = null;
		 
		 beanResHistorialMensajes = bOHistorialMensajes.obtenerHistorial(paginador,getArchitechBean(), beanReqHistorialMensajes);
		 beanResHistorialMensajes.setRef(referencia);
	
		 modelAndView = new ModelAndView("historialMensajes");
		 
		 modelAndView.addObject("beanResHistorialMensajes",beanResHistorialMensajes);
		
		 String mensaje = messageSource.getMessage("codError."+beanResHistorialMensajes.getCodError(), null, null);
		 
		 BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		 
		 modelAndView.addObject("tipoOrden", tipoOrden);
		 modelAndView.addObject("refe", referencia);
		 modelAndView.addObject(Constantes.COD_ERROR, beanResHistorialMensajes.getCodError());
		 modelAndView.addObject(Constantes.TIPO_ERROR, beanResHistorialMensajes.getTipoError());
		 modelAndView.addObject(Constantes.DESC_ERROR,mensaje);
		 modelAndView.addObject("sessionSPID", sessionSPID);
		 modelAndView.addObject("sortField", beanReqHistorialMensajes.getSortField());
		 modelAndView.addObject("sortType", beanReqHistorialMensajes.getSortType());
		 generarContadoresPaginacion(beanResHistorialMensajes, modelAndView);
		return modelAndView;
	}
	
	/**
	 * @param beanResHistorialMensajes Objeto del tipo BeanResHistorialMensajes
	 * @param model Objeto del tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(BeanResHistorialMensajes beanResHistorialMensajes, ModelAndView model) {
		if (beanResHistorialMensajes.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResHistorialMensajes.getTotalReg();
			
			if (!"1".equals(beanResHistorialMensajes.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResHistorialMensajes.getTotalReg() >= 20 * Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina());
			}			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}
	
	/**
	 * Devuelve el valor de bOHistorialMensajes
	 * @return bOHistorialMensajes objeto del tipo BOHistorialMensajes
	 */
	public BOHistorialMensajes getBOHistorialMensajes(){
		return bOHistorialMensajes;
	}
	
	
	/**
	 * Modifica el valor de bOHistorialMensajes
	 * @param bOHistorialMensajes Objeto del tipo BOHistorialMensajes
	 */
	public void setBOHistorialMensajes(BOHistorialMensajes bOHistorialMensajes){
		this.bOHistorialMensajes=bOHistorialMensajes;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
