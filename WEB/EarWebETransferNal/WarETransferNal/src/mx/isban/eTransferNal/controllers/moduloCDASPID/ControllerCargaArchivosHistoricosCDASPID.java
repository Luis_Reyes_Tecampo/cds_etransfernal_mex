/**
 * Clase de tipo controller para procesar la pantalla
 * de Monitor de Carga Archivos Historicos
 * 
 * @author: Vector
 * @since: Diciembre 2016
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOCargaArchivosHistoricosCDASPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class ControllerCargaArchivosHistoricosCDASPID.
 */
@Controller
public class ControllerCargaArchivosHistoricosCDASPID 
		extends Architech {

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = 2281708842871324188L;
	
	/**
	 * La variable de instancia para el servicio
	 */
	private BOCargaArchivosHistoricosCDASPID boCargaArchivosHistoricosCDASPID;
	
	/**
	 * Metodo para inicializar la pantalla 
	 * Monitor Carga Archivos Historicos CADSPID
	 *  
	 * @param request Los parametros requeridos
	 * @param response Los parametros de salida
	 * @param map El paramtro de salida
	 * @param paginador La vriable para la paginacion
	 * @param sessionBean La variable de session
	 * @return  El resultado del proceso
	 */
	//Metodo para inicializar la pantalla
	@RequestMapping(value="/monitorCargArchHistCDASPID.do")
	public ModelAndView iniciarMonitorCargaArchivosHistoricos(HttpServletRequest request, HttpServletResponse response){
		
		debug("<<<Iniciando la pantalla de Monitor Carga de Archivos Historicos CADSPID>>>");
		ModelAndView modelAndView = new ModelAndView("cargaArchivosHistoricosCDASPID");
		BeanResMonitorCargaArchHistCADSPID respuesta = 
				new BeanResMonitorCargaArchHistCADSPID();
		BeanPaginador paginador = UtilsMapeaRequest.getMapper().getPaginadorFromRequest(request.getParameterMap());
		
		Map<String, Object> map = modelAndView.getModel();
	
		//Se hace la consulta al BO
		try{
		respuesta = boCargaArchivosHistoricosCDASPID.
				obtenerEstatusArchivo(paginador,
						getArchitechBean());
		}catch(BusinessException e){
			showException(e);
			map.put("codigoError", e.getCode());
			map.put("mensajeError", e.getMessage());			
		}
		//Se agrega el paginador
		map.put("paginador", paginador);
		
		//Se valida respuesta
		if(respuesta.getCodigoError()!=null ||
				!"".equals(respuesta.getCodigoError())) {
			info("Codigo de error: " + respuesta.getCodigoError());
			map.put("codigoError", respuesta.getCodigoError());
			map.put("mensajeError", respuesta.getMensajeError());
		}
		//Se agrega la respuesta
		map.put("respuesta", respuesta);
		return modelAndView;
	}
	
	/**
	 * Metodo para para establecer la variable de instancia
	 * 
	 * @param boCargaArchivosHistoricosCDASPID La variable de instancia boCargaArchivosHistoricosCDASPID
	 */
	public void setBoCargaArchivosHistoricosCDASPID(
			BOCargaArchivosHistoricosCDASPID boCargaArchivosHistoricosCDASPID) {
		this.boCargaArchivosHistoricosCDASPID = boCargaArchivosHistoricosCDASPID;
	}

}
