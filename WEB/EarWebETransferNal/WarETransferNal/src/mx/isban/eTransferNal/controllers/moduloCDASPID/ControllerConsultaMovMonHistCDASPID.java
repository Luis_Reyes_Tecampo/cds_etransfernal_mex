/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 17:18:25 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonCDADetSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCDASPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOConsultaMovMonHistCDASPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * Clase que se encarga de que se encarga de recibir y procesar las peticiones
 * para la consulta de Movimientos CDAs.
 */
@Controller
public class ControllerConsultaMovMonHistCDASPID  extends Architech {
	 
	 /** Constante del Serial Version. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Constante para establecer la pagina de servicio. */
	private static final String PAGINA_SERVICIO = "consultaMovMonHistCDASPID";
	
	/** Constante para el parametro de OpeContingencia. */
	private static final String OPE_CONTINGENCIA = "paramOpeContingencia";

	/** Constante para el parametro de fechaHoy. */
	private static final String FECHA_HOY = "fechaHoy";
	
	/** Constante para establecer nombre del bean. */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsMovHistCDA";
	
	/** The Constant CODIGO_ERROR. */
	private static final String CODIGO_ERROR = "codError.";
	
	/** The Constant PARAM_COD_ERROR. */
	private static final String PARAM_COD_ERROR = "codError";
	
	/** The Constant PARAM_TIPO_ERROR. */
	private static final String PARAM_TIPO_ERROR = "tipoError";
	
	/** The Constant PARAM_DESC_ERROR. */
	private static final String PARAM_DESC_ERROR = "descError";
	
	/** The Constant PARAM_FECH_OPE_INI. */
	private static final String PARAM_FECH_OPE_INI = "paramFechaOpeInicio";
	
	/** The Constant PARAM_FECH_OPE_FIN. */
	private static final String PARAM_FECH_OPE_FIN = "paramFechaOpeFin";
	
	/** The Constant PARAM_HR_ABON_INI. */
	private static final String PARAM_HR_ABON_INI = "paramHrAbonoIni";
	
	/** The Constant PARAM_HR_ABON_FIN. */
	private static final String PARAM_HR_ABON_FIN = "paramHrAbonoFin";
	
	/** The Constant PARAM_HR_ENVIO_INI. */
	private static final String PARAM_HR_ENVIO_INI = "paramHrEnvioIni";
	
	/** The Constant PARAM_HR_ENVIO_FIN. */
	private static final String PARAM_HR_ENVIO_FIN = "paramHrEnvioFin";
	
	/** The Constant PARAM_CVE_SPEI_ORD_ABON. */
	private static final String PARAM_CVE_SPEI_ORD_ABON = "paramCveSpeiOrdenanteAbono";
	
	/** The Constant PARAM_NOMB_INST_EMISORA. */ 
	private static final String PARAM_NOMB_INST_EMISORA = "paramNombreInstEmisora";
	
	/** The Constant PARAM_CVE_RASTREO. */
	private static final String PARAM_CVE_RASTREO = "paramCveRastreo";
	
	/** The Constant PARAM_MONTO_BENEF. */
	private static final String PARAM_MONTO_BENEF = "paramCtaBeneficiario";
	
	/** The Constant PARAM_MONTO_PAGO. */
	private static final String PARAM_MONTO_PAGO = "paramMontoPago";
	
	/** The Constant PARAM_TIPO_PAGO. */
	private static final String PARAM_TIPO_PAGO = "paramTipoPago";
	
	/** The Constant PARAM_ESTATUS_CDA. */
	private static final String PARAM_ESTATUS_CDA = "paramEstatusCDA";
	
	/** Referencia al servicio de consulta Mov CDA. */
	 private transient BOConsultaMovMonHistCDASPID boConsultaMovMonHistCDASPID;
	
	
	/**
	 * Metodo que se utiliza para realizar la consulta
	 * de movimientos Historicos CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovMonHistCDASPID.do")
	public ModelAndView consMovHistCDASPID(HttpServletRequest req, HttpServletResponse res) {
		
		ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA = requestToBeanReqConsMovMonHistCDASPID(req, modelAndView);
	    BeanResConsMovHistCDASPID beanResConsMovHistCDA = null;
	    RequestContext ctx = new RequestContext(req);
	   	    

	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    try {	    	
	    	beanResConsMovHistCDA = boConsultaMovMonHistCDASPID.consultarMovHistCDASPID(beanReqConsMovHistCDA,getArchitechBean());
	    	String mensaje = ctx.getMessage(CODIGO_ERROR+beanResConsMovHistCDA.getCodError());
	    	modelAndView.addObject(BEAN_RESULTADO_CONSULTA,beanResConsMovHistCDA);
	    	modelAndView.addObject(PARAM_COD_ERROR, beanResConsMovHistCDA.getCodError());
	     	modelAndView.addObject(PARAM_TIPO_ERROR, beanResConsMovHistCDA.getTipoError());
	     	modelAndView.addObject(PARAM_DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			//se obtiene y setea codigoError BusinessException
			showException(e);
	     	modelAndView.addObject(PARAM_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(PARAM_DESC_ERROR, e.getMessage());
	    	modelAndView.addObject(PARAM_COD_ERROR, e.getCode());
		}		
		modelAndView.addObject(PARAM_FECH_OPE_INI,beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject(PARAM_FECH_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject(PARAM_HR_ABON_INI,beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject(PARAM_HR_ABON_FIN,beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject(PARAM_HR_ENVIO_INI,beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject(PARAM_HR_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject(PARAM_CVE_SPEI_ORD_ABON,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject(PARAM_NOMB_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject(PARAM_MONTO_BENEF,beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject(PARAM_MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject(PARAM_ESTATUS_CDA,beanReqConsMovHistCDA.getEstatusCDA());
		setearOperacionMonContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);			
	    return modelAndView;
	}

	/**
	 * Metodo utilizado para solicitar la exportacion de
	 * los movimientos historicos CDA obtenidos en la consulta.
	 *
	 * @param req the req
	 * @param res objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expConsMovMonHistCDASPID.do")
	public ModelAndView expConsMovHistCDASPID(HttpServletRequest req, HttpServletResponse res) {
		FormatCell formatCellHead = null;
		FormatCell formatCellBody = null;
		RequestContext ctx = new RequestContext(req);
		ModelAndView modelAndView = new ModelAndView(new ViewExcel());
        BeanResConsMovHistCDASPID resConMovCDASPID = null;
		BeanReqConsMovMonHistCDASPID beanReqConsMovCDA = requestToBeanReqConsMovMonHistCDASPID(req, modelAndView);
        beanReqConsMovCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));

        try {
        	resConMovCDASPID = boConsultaMovMonHistCDASPID.consultarMovHistCDASPID(beanReqConsMovCDA,getArchitechBean());
      	   if(Errores.OK00000V.equals(resConMovCDASPID.getCodError())||
     			   Errores.ED00011V.equals(resConMovCDASPID.getCodError())){
      		 formatCellHead = new FormatCell();
      		 formatCellHead.setFontName("Calibri");
      		 formatCellHead.setFontSize((short)11);
      		 formatCellHead.setBold(true);
      	     formatCellBody = new FormatCell();
      	     formatCellBody.setFontName("Calibri");
    	     formatCellBody.setFontSize((short)11);
    	     formatCellBody.setBold(false);
      	 	 
       	 	 modelAndView.addObject("FORMAT_HEADER", formatCellHead);
  	 	     modelAndView.addObject("FORMAT_CELL", formatCellBody);
      	 	 modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
      	 	 modelAndView.addObject("BODY_VALUE", getBody(resConMovCDASPID.getListBeanMovimientoCDA()));
      	 	 modelAndView.addObject("NAME_SHEET", "expConsMovHistCDA");
      	   }else{
      		 modelAndView = consMovHistCDASPID(req,res);
      		 String mensaje = ctx.getMessage(CODIGO_ERROR+resConMovCDASPID.getCodError());
      	      modelAndView.addObject(PARAM_COD_ERROR, resConMovCDASPID.getCodError());
      	      modelAndView.addObject(PARAM_TIPO_ERROR, resConMovCDASPID.getTipoError());
      	      modelAndView.addObject(PARAM_DESC_ERROR, mensaje);
      	   }
 	    } catch (BusinessException e) {
			//se obtiene y setea codigoError BusinessException
 	    	showException(e);
	     	modelAndView.addObject(PARAM_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(PARAM_DESC_ERROR, e.getMessage());
	    	modelAndView.addObject(PARAM_COD_ERROR, e.getCode());
 	    }
 	    return modelAndView;		
	}
	
    /**
     * Metodo privado que sirve para obtener los datos para el excel.
     *
     * @param listBeanMovimientoCDA Objeto (List<BeanMovimientoCDA>) con el listado de objetos del tipo BeanMovimientoCDA
     * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
     */
    private List<List<Object>> getBody(List<BeanMovimientoCDASPID> listBeanMovimientoCDA){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	
  	 	//si lista de movimientos no esta vacia
  	 	if(listBeanMovimientoCDA!= null && 
  	 			!listBeanMovimientoCDA.isEmpty()){
  	 		objList = new ArrayList<List<Object>>();
  	 		for(BeanMovimientoCDASPID movCDASPID : listBeanMovimientoCDA){ 
  	 			objListParam = new ArrayList<Object>();
  	 			objListParam.add(movCDASPID.getReferencia());			//campo referencia
  	 			objListParam.add(movCDASPID.getFechaOpe());				//campo fechaOperacion
		 		objListParam.add(movCDASPID.getFolioPaq());				//campo folioPa
		 		objListParam.add(movCDASPID.getFolioPago());			//campo folioPago
		 		objListParam.add(movCDASPID.getCveSpei());				//campo cveSpei
		 		objListParam.add(movCDASPID.getNomInstEmisora());		//campo nombreInstitucionEmisora
		 		objListParam.add(movCDASPID.getCveRastreo());			//campo claveRastreo
		 		objListParam.add(movCDASPID.getCtaBenef());				//cuentaBeneficiario
		 		objListParam.add(movCDASPID.getMonto());				//campo monto
		 		objListParam.add(movCDASPID.getFechaAbono());			//campo fechaAbono
		 		objListParam.add(movCDASPID.getHrAbono());				//campo horaAbono
		 		objListParam.add(movCDASPID.getHrEnvio());				//campo horaEnvio
		 		objListParam.add(movCDASPID.getEstatusCDA());			//campo estatusCDA
		 		objListParam.add(movCDASPID.getCodError());				//campo codError
		 		objListParam.add(movCDASPID.getTipoPago());				//campo tipoPago
		 		objListParam.add(movCDASPID.getFolioPaqCda());
	 			objListParam.add(movCDASPID.getFolioCda());
	 			objListParam.add(movCDASPID.getModalidad());
	 			objListParam.add(movCDASPID.getNombreOrd());
	 			objListParam.add(movCDASPID.getTipoCuentaOrd());
	 			objListParam.add(movCDASPID.getNumCuentaOrd());
	 			objListParam.add(movCDASPID.getRfcOrd());
	 			objListParam.add(movCDASPID.getNombreBcoRec());
	 			objListParam.add(movCDASPID.getNombreRec());
	 			objListParam.add(movCDASPID.getTipoCuentaRec());
	 			objListParam.add(movCDASPID.getRfcRec());
	 			objListParam.add(movCDASPID.getConceptoPago());
	 			objListParam.add(movCDASPID.getClasifOperacion());
	 			objListParam.add(movCDASPID.getNumSerieCertif());
	 			objListParam.add(movCDASPID.getSelloDigital());
	 			objListParam.add(movCDASPID.getAlgoritmoFirma());
	 			objListParam.add(movCDASPID.getPadding());
		 		objList.add(objListParam);
  	 		}
  	 	}
  	 	return objList;
    }
    
    /**
     * Metodo privado que sirve para obtener los encabezados del Excel.
     *
     * @param ctx Objeto del tipo RequestContext
     * @return List<String> Objeto con el listado de String
     */
    private List<String> getHeaderExcel(RequestContext ctx){
    	String []header = new String [] {
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.refere"),				//campo referencia
  		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.fechaOp"),				//campo fechaOperacion
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaq"),			//campo folioPaq
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPag"),			//campo folioPag
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono"),	//campo claveSpeiOrdAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.instEmisora"),			//campo	instEmisora
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.cveRastreo"),			//campo claveRastreo
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.ctaBenef"),			//campo cuentaBeneficiario
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.monto"),				//campo monto
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.fechaAbono"),			//campo fechaAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrAbono"),				//campo horaAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrEnvio"),				//campo horaEnvio
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.estatusCDA"),			//campo estatusCDA
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.codError"),			//campo codError
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoPago"),				//campo tipoPago
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaqCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.modalidad"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreBcoRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.conceptoPago"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.clasifOperarcion"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.algoritmoFirma"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.padding")
  	 	   };
   	 return Arrays.asList(header);
    }

	/**
	 * Metodo utilizado para realizar solicitar la exportacion de todos
	 * los movimientos historicos CDA obtenidos en la consulta.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expTodosConsMovMonHistCDASPID.do")
	public ModelAndView expTodosConsMovHistCDASPID(HttpServletRequest req, HttpServletResponse res) {
		
		ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA = requestToBeanReqConsMovMonHistCDASPID(req, modelAndView);
	    BeanResConsMovHistCDASPID resConsMovHistCDASPID = null;
	    StringBuilder  builder = new StringBuilder();
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    RequestContext ctx = new RequestContext(req);
	    
	    try {
	    	resConsMovHistCDASPID = boConsultaMovMonHistCDASPID.consultaMovHistCDAExpTodosSPID(beanReqConsMovHistCDA,getArchitechBean());
	    	modelAndView.addObject(BEAN_RESULTADO_CONSULTA,resConsMovHistCDASPID);
	    	modelAndView.addObject(PARAM_COD_ERROR, resConsMovHistCDASPID.getCodError());
	     	modelAndView.addObject(PARAM_TIPO_ERROR, resConsMovHistCDASPID.getTipoError());
	     	String mensaje = ctx.getMessage(CODIGO_ERROR+resConsMovHistCDASPID.getCodError());
	     	builder.append(mensaje);
	     	builder.append(resConsMovHistCDASPID.getNombreArchivo());
	     	mensaje = builder.toString();
	     	modelAndView.addObject(PARAM_DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			//se obtiene y setea codigoError BusinessException
			showException(e);
	     	modelAndView.addObject(PARAM_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(PARAM_DESC_ERROR, e.getMessage());
	    	modelAndView.addObject(PARAM_COD_ERROR, e.getCode());
		}
		modelAndView.addObject(PARAM_FECH_OPE_INI,beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject(PARAM_FECH_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject(PARAM_HR_ENVIO_INI,beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject(PARAM_HR_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject(PARAM_HR_ABON_INI,beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject(PARAM_HR_ABON_FIN,beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject(PARAM_MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject(PARAM_MONTO_BENEF,beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject(PARAM_NOMB_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject(PARAM_CVE_SPEI_ORD_ABON,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		setearOperacionMonContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);		
		
		return modelAndView;			
	}

	/**
	 * Metodo que consulta detalle de un Movimiento Historico CDA Seleccionado.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovMonHistDetCDASPID.do")
	public ModelAndView consMovHistDetCDASPID(HttpServletRequest req, HttpServletResponse res) {
		ModelAndView modelAndView = new ModelAndView("consultaDetHistMovMonCDASPID");
		BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA = requestToBeanReqConsMovMonHistCDASPID(req, modelAndView);
		BeanReqConsMovMonCDADetSPID beanReqConsMovCDADet = requestToBeanReqConsMovMonCDADetSPID(req, modelAndView);
		
	    BeanResConsMovDetHistCDASPID beanResConsMovDetHistCDASPID = null;
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    
	    try {
	    	beanResConsMovDetHistCDASPID = boConsultaMovMonHistCDASPID.consMovHistDetCDASPID(beanReqConsMovCDADet,getArchitechBean());
		} catch (BusinessException e) {
			//se obtiene y setea codigoError BusinessException
			showException(e);
	     	modelAndView.addObject(PARAM_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(PARAM_DESC_ERROR, e.getMessage());
	    	modelAndView.addObject(PARAM_COD_ERROR, e.getCode());
		}
		
		modelAndView.addObject("beanResConsMovDetHistCDA",beanResConsMovDetHistCDASPID);
		modelAndView.addObject(PARAM_FECH_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject(PARAM_FECH_OPE_INI,beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject(PARAM_HR_ABON_FIN,beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject(PARAM_HR_ABON_INI,beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject(PARAM_HR_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject(PARAM_HR_ENVIO_INI,beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject(PARAM_CVE_SPEI_ORD_ABON,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject(PARAM_NOMB_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject(PARAM_MONTO_BENEF,beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject(PARAM_ESTATUS_CDA,beanReqConsMovHistCDA.getEstatusCDA());
		modelAndView.addObject(PARAM_MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject("beanPaginador",beanReqConsMovHistCDA.getPaginador());
		setearOperacionMonContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);	
		return modelAndView;
	}

	/**
	 * Metodo que modifica el valor de la propiedad boConsultaMovMonHistCDA.
	 *
	 * @param boConsultaMovMonHistCDA Objeto de tipo @see BOConsultaMovMonHistCDA
	 */
	public void setBoConsultaMovMonHistCDASPID(
			BOConsultaMovMonHistCDASPID boConsultaMovMonHistCDA) {
		this.boConsultaMovMonHistCDASPID = boConsultaMovMonHistCDA;
	}

	/**
	 * Setear operacion mon contingencia.
	 *
	 * @param modelAndView the model and view
	 * @param reqConsMovHistCDA the req cons mov hist cda
	 */
	private void setearOperacionMonContingencia(ModelAndView modelAndView, BeanReqConsMovMonHistCDASPID reqConsMovHistCDA){
		if( reqConsMovHistCDA.getOpeContingencia() ){
			modelAndView.addObject(OPE_CONTINGENCIA, reqConsMovHistCDA.getOpeContingencia() );
		} else {
			modelAndView.addObject(OPE_CONTINGENCIA, "");
		}
	}
	
	/**
	 * Request to bean req cons mov mon hist cdaspid.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req cons mov mon hist cdaspid
	 */
	public BeanReqConsMovMonHistCDASPID requestToBeanReqConsMovMonHistCDASPID(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA = new BeanReqConsMovMonHistCDASPID();
		UtilsMapeaRequest.getMapper().mapearObject(beanReqConsMovHistCDA, req.getParameterMap());
		return beanReqConsMovHistCDA;
	}
	
	/**
	 * Request to bean req cons mov mon cda det spid.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req cons mov mon cda det spid
	 */
	public BeanReqConsMovMonCDADetSPID requestToBeanReqConsMovMonCDADetSPID(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqConsMovMonCDADetSPID beanReqConsMovCDADet = new BeanReqConsMovMonCDADetSPID();
		UtilsMapeaRequest.getMapper().mapearObject(beanReqConsMovCDADet, req.getParameterMap());
		return beanReqConsMovCDADet;
	}
}


