package mx.isban.eTransferNal.controllers.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinanciero;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancieros;
import mx.isban.eTransferNal.beans.catalogos.BeanResTiposIntFinancieros;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimIntFin;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOIntFinancieros;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del catalogo de intermediarios financieros.
 * 
 */
@Controller
public class ControllerIntFinancieros extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4199156317910952128L;

	/** Constante con el nombre de la pagina de consulta. */
	private static final String PAG_CAT_INT_FINANCIEROS="catIntFinancieros";
	
	/** Constante con el nombre de la pagina de alta. */
	private static final String PAG_ALTA_CAT_INT_FINANCIEROS="altaCatIntFinancieros";

    /** Constante del tipo String que almacena el valor de BEAN_RES. */
	private static final String BEAN_RES = "beanResIntFinancieros";

	/** La variable que contiene informacion con respecto a: bo int financieros. */
	private BOIntFinancieros boIntFinancieros;

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private ResourceBundleMessageSource messageSource;

	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";

	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";
	

	/** La constante VALIDACION_JSR_303. */
	private static final String VALIDACION_JSR_303 = "Validaciones JSR-303, valores de entrada incorrectos.";
	
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * Muestra cat inst financieras.
	 *
	 * @return Objeto model and view
	 */
	@RequestMapping(value="muestraCatIntFinancieros.do")
	public ModelAndView muestraCatInstFinancieras(){
		ModelAndView modelAndView = null;
        BeanResTiposIntFinancieros beanResIntFinancieros = boIntFinancieros.
        		consultaTiposInterviniente(new BeanResIntFinancieros(), getArchitechBean());
    	modelAndView = new ModelAndView(PAG_CAT_INT_FINANCIEROS,BEAN_RES,beanResIntFinancieros);

     	return modelAndView;	
	}
		
	/**
	 * Muestra alta cat inst financieras.
	 * @param beanReqInsertarIntFin El objeto: bean req insert inst
	 * @param req El objeto: req
	 * @param bandera parametro que sirve para ver la accion que se realiza
	 * @return Objeto model and view
	 */
	@RequestMapping(value="muestraAltaCatIntFinancieros.do")
	public ModelAndView muestraAltaCatInstFinancieras(BeanResIntFinancieros beanReqInsertarIntFin,
			HttpServletRequest req,@RequestParam("bandera") String bandera){
		ModelAndView modelAndView = null;
		BeanResTiposIntFinancieros beanResTiposIntFin = null;
		RequestContext ctx = new RequestContext(req);
		int checkSeleccionados = Integer.parseInt((String)req.getParameter("checkSeleccionados"));
		if (checkSeleccionados == 0) {
			beanReqInsertarIntFin = new BeanResIntFinancieros();
		}
		beanReqInsertarIntFin.setBandera(bandera);
		beanResTiposIntFin = boIntFinancieros.
				consultaTiposInterviniente(beanReqInsertarIntFin, getArchitechBean());
		modelAndView = new ModelAndView(PAG_ALTA_CAT_INT_FINANCIEROS,"beanResTiposIntFin",beanResTiposIntFin);
		if (checkSeleccionados == 0) {
			return modelAndView;
		}else if(!Errores.OK00000V.equals(beanResTiposIntFin.getCodError())){
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResTiposIntFin.getCodError());
	    	modelAndView.addObject(Constantes.COD_ERROR, beanResTiposIntFin.getCodError());
	    	modelAndView.addObject(Constantes.TIPO_ERROR, beanResTiposIntFin.getTipoError());
	    	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}
   	    return modelAndView;
	}

   /**
    * Metodo que se utiliza para buscar registros en la funcionalidad
    * de Intervinientes financieros.
    *
    * @param beanConsReqIntFin El objeto: bean cons req int fin
    * @param req El objeto: req
    * @return Objeto model and view
    */
    @RequestMapping(value = "/buscarIntFinancieros.do")
    public ModelAndView buscarCatIntFinancieros(BeanConsReqIntFin beanConsReqIntFin, HttpServletRequest req){
        ModelAndView modelAndView = null;
        RequestContext ctx = new RequestContext(req);
        BeanResTiposIntFinancieros beanResIntFinancieros = boIntFinancieros.
        		buscarIntFinancierosFiltro(beanConsReqIntFin, getArchitechBean());
    	modelAndView = new ModelAndView(PAG_CAT_INT_FINANCIEROS,BEAN_RES,beanResIntFinancieros);
    	if(!Errores.OK00000V.equals(beanResIntFinancieros.getCodError())){
    		String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResIntFinancieros.getCodError());
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResIntFinancieros.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResIntFinancieros.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
    	}
     	return modelAndView;
    }
    
    /**
     * Editar int financieros.
     *
     * @param beanConsReqIntFin El objeto: bean cons req int fin
     * @param req El objeto: req
     * @return Objeto model and view
     */
    @RequestMapping(value = "/editarIntFinancieros.do")
    public ModelAndView editarIntFinancieros(BeanReqInsertarIntFin beanConsReqIntFin, HttpServletRequest req){
    	BeanResIntFinancieros beanResIntFinancieros = null;
        ModelAndView modelAndView = null;
        RequestContext ctx = new RequestContext(req);
        beanResIntFinancieros = boIntFinancieros.
        		actualizaIntFinanciero(beanConsReqIntFin, getArchitechBean());
        modelAndView = new ModelAndView(PAG_CAT_INT_FINANCIEROS,BEAN_RES,beanResIntFinancieros);
        if(Errores.OK00000V.equals(beanResIntFinancieros.getCodError())){
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.OK00000V);
     	    modelAndView.addObject(Constantes.COD_ERROR, Errores.OK00000V);
     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResIntFinancieros.getTipoError());
     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}else{
			modelAndView = new ModelAndView(PAG_ALTA_CAT_INT_FINANCIEROS,"beanResIntFin",beanResIntFinancieros);
      	    String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResIntFinancieros.getCodError());
     	    modelAndView.addObject(Constantes.COD_ERROR, beanResIntFinancieros.getCodError());
     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResIntFinancieros.getTipoError());
     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}
     	
     	return modelAndView;
    }
    
    /**
     * Metodo que permite eliminar una institucion participante.
     *
     * @param beanReqEliminarIntFin El objeto: bean req eliminar int fin
     * @param req El objeto: req
     * @return Objeto model and view
     */
    @RequestMapping(value = "/elimCatIntFinancieros.do")
    public ModelAndView elimCatIntFinancieros(BeanReqEliminarIntFin beanReqEliminarIntFin, 
    		HttpServletRequest req){
    	BeanResElimIntFin beanResElimIntFin = null;
  	   	StringBuilder strBuilder = new StringBuilder();
  	   	String correctos = "", erroneos = "", mensaje = "";
  	   	ModelAndView modelAndView = null;
  	   	RequestContext ctx = new RequestContext(req);
  	   	beanResElimIntFin = boIntFinancieros.elimCatIntFinancieros(beanReqEliminarIntFin, 
  			  getArchitechBean());
  	    modelAndView = new ModelAndView(PAG_CAT_INT_FINANCIEROS,BEAN_RES,beanResElimIntFin);
  	    if(Errores.OK00000V.equals(beanResElimIntFin.getCodError())){
  			modelAndView.addObject("paginador", beanReqEliminarIntFin.getBeanPaginador());
  			correctos = beanResElimIntFin.getEliminadosCorrectos();
  			erroneos = beanResElimIntFin.getEliminadosErroneos();
  			if(!"".equals(correctos)){
  				correctos = correctos.substring(0,correctos.length()-1);
  				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00020V",new String[]{correctos});
  			}
  			if(!"".equals(erroneos)){
  				erroneos = erroneos.substring(0,erroneos.length()-1);
  				strBuilder.append(mensaje);
  				strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO+"CD00171V",new String[]{erroneos}));
  				mensaje = strBuilder.toString();
  			}
  	     	modelAndView.addObject(Constantes.COD_ERROR, beanResElimIntFin.getCodError());
  	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResElimIntFin.getTipoError());
  	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
  	   } else{
  		   String  dtosCorrectos = beanResElimIntFin.getEliminadosCorrectos();
			if(!"".equals(dtosCorrectos)){
				dtosCorrectos = dtosCorrectos.substring(0,dtosCorrectos.length()-1);
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00020V",new String[]{dtosCorrectos});
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResElimIntFin.getCodError());
			}		
  		   
  		   modelAndView.addObject(Constantes.COD_ERROR, beanResElimIntFin.getCodError());
  		   modelAndView.addObject(Constantes.TIPO_ERROR, beanResElimIntFin.getTipoError());
  	       modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
  	   }
  	   return modelAndView;
    }
      
    /**
     * Metodo que sirve para dar de alta un Interviniente Financiero.
     *
     * @param beanReqInsertInst El objeto: bean req insert inst
     * @param req El objeto: req
     * @return Objeto model and view
     */
	@RequestMapping(value="altaCatIntFinancieros.do")
	public ModelAndView altaCatInstFinancieras(BeanReqInsertarIntFin beanReqInsertInst,
			HttpServletRequest req){
		ModelAndView modelAndView = null;
		BeanResTiposIntFinancieros beanResGuardaIntFin = null;
		ArchitechSessionBean architechSessionBean = getArchitechBean();
		RequestContext ctx = new RequestContext(req);
			beanReqInsertInst.setCveUsuario(architechSessionBean.getUsuario());
			beanResGuardaIntFin = boIntFinancieros.guardaIntFin(beanReqInsertInst, 
					getArchitechBean());
			
			if(Errores.OK00000V.equals(beanResGuardaIntFin.getCodError())){
				modelAndView = new ModelAndView(PAG_CAT_INT_FINANCIEROS,BEAN_RES,beanResGuardaIntFin);
				String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.OK00000V);
	     	    modelAndView.addObject(Constantes.COD_ERROR, Errores.OK00000V);
	     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResGuardaIntFin.getTipoError());
	     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}else{
				modelAndView = new ModelAndView(PAG_ALTA_CAT_INT_FINANCIEROS,"beanResTiposIntFin",beanResGuardaIntFin);
	      	    String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResGuardaIntFin.getCodError());
	     	    modelAndView.addObject(Constantes.COD_ERROR, beanResGuardaIntFin.getCodError());
	     	    modelAndView.addObject(Constantes.TIPO_ERROR, beanResGuardaIntFin.getTipoError());
	     	    modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
	    return modelAndView;
	}
		
    /**
     * Metodo que se utiliza para exportar en la funcionalidad de Intermediarios
     *
     * @param beanConsReqIntFin El objeto: bean cons req int fin
     * @param req Objeto del tipo HttpServletRequest
     * @param servletResponse Objeto del tipo HttpServletResponse
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/expIntFin.do")
     public ModelAndView expIntFin(BeanConsReqIntFin beanConsReqIntFin, HttpServletRequest req,HttpServletResponse servletResponse){
    	FormatCell formatCellHeader = null;
    	FormatCell formatCellBody = null;
    	ModelAndView modelAndView = null;
   	 	RequestContext ctx = new RequestContext(req);
   	 	BeanResIntFinancieros beanResIntFinancieros = null;
        beanResIntFinancieros = boIntFinancieros.buscarIntFinancierosFiltro(beanConsReqIntFin, 
        	   getArchitechBean());
     	if(Errores.OK00000V.equals(beanResIntFinancieros.getCodError())||
     		   Errores.ED00011V.equals(beanResIntFinancieros.getCodError())){
     	 	formatCellBody = new FormatCell();
     	 	formatCellBody.setFontName("Calibri");
     	 	formatCellBody.setFontSize((short)11);
     	 	formatCellBody.setBold(false);
     	 	    
     	 	formatCellHeader = new FormatCell();
     	 	formatCellHeader.setFontName("Calibri");
     	 	formatCellHeader.setFontSize((short)11);
     	 	formatCellHeader.setBold(true);

     	 	modelAndView = new ModelAndView(new ViewExcel());
     	    modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
     	 	modelAndView.addObject("FORMAT_CELL", formatCellBody);
     	 	modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
     	 	modelAndView.addObject("BODY_VALUE", getBody(beanResIntFinancieros.getListBeanIntFinanciero()));
     	 	modelAndView.addObject("NAME_SHEET", "Intermediarios");
     	}else{
     		modelAndView = buscarCatIntFinancieros(beanConsReqIntFin, req);
     		String mensaje = ctx.getMessage("codError."+beanResIntFinancieros.getCodError());
       	    modelAndView.addObject("codError", beanResIntFinancieros.getCodError());
       	    modelAndView.addObject("tipoError", beanResIntFinancieros.getTipoError());
       	    modelAndView.addObject("descError", mensaje);
     	}
 	    return modelAndView;
     }
     
     /**
      * Metodo privado que sirve para obtener los encabezados del Excel.
      *
      * @param ctx Objeto del tipo RequestContext
      * @return List<String> Objeto con el listado de String
      */
     private List<String> getHeaderExcel(RequestContext ctx){
    	 String []header = new String [] {
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.cveInterme"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoInterme"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numCecoban"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreCorto"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreLargo"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numPersona"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.fchAlta"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.fchBaja"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.fchUltModif"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.usuarioModif"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.usuarioRegistro"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.status"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numBanxico"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numIndeval"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.idIntIndeval"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.folIntIndeval"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.cveSwiftCor"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.operaSpei"),
    			 ctx.getMessage("catalogos.consultaCatIntFinancieros.text.operaSpid")
    	 	   }; 
    	 return Arrays.asList(header);
     }
     
     /**
      * Metodo privado que sirve para obtener los datos para el excel.
      *
      * @param listBeanIntFinanciero El objeto: list bean int financiero
      * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
      */
     private List<List<Object>> getBody(List<BeanIntFinanciero> listBeanIntFinanciero){
	 	List<Object> objListParam = null;
   	 	List<List<Object>> objList = null;
    	 if(listBeanIntFinanciero!= null && 
   			   !listBeanIntFinanciero.isEmpty()){
   		   objList = new ArrayList<List<Object>>();
   	 	   for(BeanIntFinanciero beanIntFinanciero : listBeanIntFinanciero){ 
   	 		   objListParam = new ArrayList<Object>();
   	 		   objListParam.add(beanIntFinanciero.getCveInterme());
   	 		   objListParam.add(beanIntFinanciero.getTipoInterme());
   	 		   objListParam.add(beanIntFinanciero.getNumCecoban());
   	 		   objListParam.add(beanIntFinanciero.getNombreCorto());
   	 		   objListParam.add(beanIntFinanciero.getNombreLargo());
   	 		   objListParam.add(beanIntFinanciero.getNumPersona());
   	 		   objListParam.add(beanIntFinanciero.getFchAlta());
   	 		   objListParam.add(beanIntFinanciero.getFchBaja());
   	 		   objListParam.add(beanIntFinanciero.getFchUltModif());
   	 		   objListParam.add(beanIntFinanciero.getUsuarioModif());
   	 		   objListParam.add(beanIntFinanciero.getUsuarioRegistro());
   	 		   objListParam.add(beanIntFinanciero.getStatus());
   	 		   objListParam.add(beanIntFinanciero.getNumBanxico());
   	 		   objListParam.add(beanIntFinanciero.getNumIndeval());
   	 		   objListParam.add(beanIntFinanciero.getIdIntIndeval());
   	 		   objListParam.add(beanIntFinanciero.getFolIntIndeval());
   	 		   objListParam.add(beanIntFinanciero.getCveSwiftCor());
   	 		   objListParam.add(beanIntFinanciero.getOperaSpei());
   	 		   objListParam.add(beanIntFinanciero.getOperaSpid());
   	 		 objList.add(objListParam);
   	 	   }
   	   } 
    	return objList;
     }
         
		/**
		 * Metodo publico que sirve para obtener todos lo datos del Cat. Intermediarios
		 * y seran exportados a un archivos de excel el cual se deposita en un servidor.
		 * 
		 * @param beanPaginador     El objeto: BeanPaginador
		 * @param beanConsReqIntFin El objeto: que contiene todos los datos del cat.
		 *                          intemediarios
		 * @param bindingResult     El objeto: que contiene todos los datos del cat.
		 *                          intemediarios
		 * @return modelIntFinancieros Objeto ModelAndView
		 */
		@RequestMapping(value = "exportarTodoIntFinancieros.do")
		public ModelAndView exportarTodoFinancieros(@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
				@ModelAttribute(BEAN_FILTER) @Valid BeanConsReqIntFin beanConsReqIntFin, BindingResult bindingResult) {
			// validar los resultados
			validarBindingResult(bindingResult);
			ModelAndView modelIntFinancieros = new ModelAndView();
			BeanResBase responseIntFinancieros;
			// El objeto a informar
			BeanResTiposIntFinancieros responseFinancieros = new BeanResTiposIntFinancieros();

			try {
				// se realiza la consulta
				responseFinancieros = boIntFinancieros.buscarIntFinancierosFiltro(beanConsReqIntFin,
						getArchitechBean());

				// se valida el resultado
				if (Errores.ED00011V.equals(responseFinancieros.getCodError())
						|| Errores.EC00011B.equals(responseFinancieros.getCodError())) {
					modelIntFinancieros = new ModelAndView(PAG_CAT_INT_FINANCIEROS, BEAN_RES, responseFinancieros);
					modelIntFinancieros.addObject(Constantes.COD_ERROR, responseFinancieros.getCodError());
					modelIntFinancieros.addObject(Constantes.DESC_ERROR, responseFinancieros.getMsgError());
					modelIntFinancieros.addObject(Constantes.TIPO_ERROR, responseFinancieros.getTipoError());
					return modelIntFinancieros;
				}
				// se realiza la operacion para exportar
				responseIntFinancieros = boIntFinancieros.exportarTodo(getArchitechBean(), beanConsReqIntFin,
						responseFinancieros.getTotalReg());
				modelIntFinancieros = new ModelAndView(PAG_CAT_INT_FINANCIEROS, BEAN_RES, responseFinancieros);
				// se valida el resultado
				if (Errores.OK00002V.equals(responseIntFinancieros.getCodError())) {
					String mensajeIntFinancieros = messageSource.getMessage(
							Constantes.COD_ERROR_PUNTO + responseIntFinancieros.getCodError(),
							new Object[] { responseIntFinancieros.getMsgError() }, LocaleContextHolder.getLocale());
					modelIntFinancieros.addObject(Constantes.DESC_ERROR, mensajeIntFinancieros);
				} else {
					// en caso de no cumplir la validacion
					modelIntFinancieros.addObject(Constantes.DESC_ERROR, responseIntFinancieros.getMsgError());
				}
				// se envia la informacion
				modelIntFinancieros.addObject(Constantes.COD_ERROR, responseIntFinancieros.getCodError());
				modelIntFinancieros.addObject(Constantes.TIPO_ERROR, responseIntFinancieros.getTipoError());
				// en caso de error
			} catch (BusinessException e) {
				showException(e);
			}
			// lo que se recupera
			return modelIntFinancieros;
		}
     
		/**
		 * Metodo validarBindingResult, Funcionalidad: Validar si existen excepciones
		 * del Bean y enviarlas.
		 * 
		 * @param bindingResult Objeto validador de datos del bean.
		 */
		public void validarBindingResult(BindingResult bindingResult) {
			// se valida si existen excepciones del Bean y enviarlas
			if (bindingResult.hasFieldErrors()) {
				// se VALIDACION_JSR_303
				error(VALIDACION_JSR_303);
				error(bindingResult.getAllErrors().get(0).getDefaultMessage());
			}
		}

		/**
		 * Obtener el objeto: bo int financieros.
		 *
		 * @return El objeto: bo int financieros
		 */
		public BOIntFinancieros getBoIntFinancieros() {
			return boIntFinancieros;
		}

		/**
		 * Definir el objeto: bo int financieros.
		 *
		 * @param boIntFinancieros El nuevo objeto: bo int financieros
		 */
		public void setBoIntFinancieros(BOIntFinancieros boIntFinancieros) {
			this.boIntFinancieros = boIntFinancieros;
		}
}