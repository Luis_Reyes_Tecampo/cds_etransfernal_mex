/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:21 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BORecepciones;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

/**
 * Class ControllerRecepciones.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * recepciones y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerRecepciones extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2528505424478686009L;
	
	/** La variable que contiene informacion con respecto a: bo recepciones. */
	private BORecepciones boRecepciones;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "consultaRecepcionesPC";
	
	/** La constante BEAN_RECEPCIONES. */
	private static final String BEAN_RECEPCIONES = "beanResConsultaRecepciones";
 	/** La constante CONST_REF. */
 	private static final String CONST_REF = "refModel";
 	
 	/** La constante PANTALLA_DETALLE. */
	 private static final String PANTALLA_DETALLE = "detalleRecepcion";
 	
 	/** La constante BEAN_DETALLE. */
	 private static final String BEAN_DETALLE = "beanRes";
	 
	/**
	 * Consulta avisos traspasasos.
	 * 
	 * Muestra la pantalla con la informacion
	 * a la que hace referencia esta funcionbilidad.
	 *
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanModulo El objeto: bean modulo
	 * @param tipoOrden El objeto: tipo orden
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaRecepcionesConting.do")
	public ModelAndView consultaAvisosTraspasasos(BeanPaginador beanPaginador, @Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			@RequestParam("ref") String tipoOrden,
			@ModelAttribute(BEAN_RECEPCIONES) BeanReqConsultaRecepciones beanReqConsultaRecepciones, BindingResult bindingResult) {
		
		ModelAndView model = null;
		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		/** Validacion de los datos de entrada **/
  	    if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		String tipo = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			tipo = tipoOrden.replace(",", "");
			beanReqConsultaRecepciones.setTipoOrden(tipo);
			beanReqConsultaRecepciones.setSortField("folioPago");
			beanReqConsultaRecepciones.setSortType("ASC");
			/** Ejecucion de la peticion al BO **/
			beanResConsultaRecepciones = boRecepciones.obtenerConsultaRecepciones(beanPaginador, beanModulo, 
					beanReqConsultaRecepciones, sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion(), getArchitechBean());
			
			if ("PA".equalsIgnoreCase(tipoOrden)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes  Recibidas por Aplicar");
			} else if ("TR".equalsIgnoreCase(tipoOrden)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas Aplicadas");
			} else if ("RE".equalsIgnoreCase(tipoOrden)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas por Rechazar");
			}
			beanResConsultaRecepciones.setTipoOrden(tipoOrden);
			
			String mensaje = messageSource.getMessage("codError." + beanResConsultaRecepciones.getCodError(), null, null);
			model = new ModelAndView(PAGINA_PRINCIPAL);
			/** Seteo de datos al modelo **/
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(BEAN_RECEPCIONES, beanResConsultaRecepciones);
			model.addObject(Constantes.COD_ERROR, beanResConsultaRecepciones.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResConsultaRecepciones.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject(CONST_REF, tipoOrden);
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			generarContadoresPag(model, beanResConsultaRecepciones);
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Consutla detalle recepcion.
	 *
	 * @param beanModulo El objeto: bean modulo
	 * @param tipoOrden El objeto: tipo orden
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanLlave El objeto: bean llave
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "detalleRecepcion.do")
	public ModelAndView consutlaDetalleRecepcion(@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			@RequestParam("ref") String tipoOrden,
			BeanPaginador beanPaginador, 
			BeanSPIDLlave  beanLlave, BindingResult bindingResult) {
		ModelAndView modelRecep = null;
		BeanReceptoresSPID beanReceptoresSPIDTemp = null;
		BeanResRecepcionSPID beanResRecepcionSPID;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
  	    if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
			try {
				/** Ejecucion de la peticion al BO **/
				beanResRecepcionSPID = boRecepciones.obtenerDetalleRecepcion(beanLlave, beanModulo, getArchitechBean());
				beanReceptoresSPIDTemp = beanResRecepcionSPID.getBeanReceptorSPID();
			} catch (BusinessException e) {
				showException(e);
			}
		modelRecep = new ModelAndView(PANTALLA_DETALLE);
		/** Seteo de datos al modelo **/
		modelRecep.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
		modelRecep.addObject(BEAN_DETALLE, beanReceptoresSPIDTemp);
		modelRecep.addObject("titulo", "Detalle de Recepci&oacute;n de Operaciones");
		modelRecep.addObject("opcion", tipoOrden);
		modelRecep.addObject("ref", tipoOrden);
		modelRecep.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
		modelRecep.addObject("servicio", "consultaRecepcionesConting.do");
		/** Retorno del modelo **/
		return modelRecep;
	}
	
	/**
	 * Generar contadores pag para las recepciones
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResConsultaRecepciones El objeto: bean res consulta recepciones
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResConsultaRecepciones beanResConsultaRecepciones) {
		if (beanResConsultaRecepciones.getTotalReg() > 0){
			 Integer regIniConRecep = 1;
			 Integer regFinConRecep = beanResConsultaRecepciones.getTotalReg();
			
			if (!"1".equals(beanResConsultaRecepciones.getBeanPaginador().getPagina())) {
				regIniConRecep = 20 * (Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResConsultaRecepciones.getTotalReg() >= 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina())){
				regFinConRecep = 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina());
			}		
			modelAndView.addObject("regIni", regIniConRecep);
			modelAndView.addObject("regFin", regFinConRecep);
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanConsultaRecepciones beanCons : beanResConsultaRecepciones.getListaConsultaRecepciones()){
				totalImporte = totalImporte.add(new BigDecimal(beanCons.getBeanConsultaRecepcionesDos().getMonto()));
			}
			modelAndView.addObject("importePagina", totalImporte.toString());
		}
	}
	
	/**
	 * Obtener el objeto: bo recepciones.
	 *
	 * @return El objeto: bo recepciones
	 */
	public BORecepciones getBoRecepciones() {
		return boRecepciones;
	}

	/**
	 * Definir el objeto: bo recepciones.
	 *
	 * @param boRecepciones El nuevo objeto: bo recepciones
	 */
	public void setBoRecepciones(BORecepciones boRecepciones) {
		this.boRecepciones = boRecepciones;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}

	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
