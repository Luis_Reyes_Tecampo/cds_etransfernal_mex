/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerCertificadosCifrado.java
 * 
 * -Contiene los metodos y atributos para las fucniones del 
 * -Catalogo de Mantenimiento de certificados Cifrados
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/09/2018 07:21:58 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOCertificadosCifrado;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtilsExportacionCertificados;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Class ControllerCertificadosCifrado.
 * 
 * -Contiene los metodos y atributos para las fucniones del 
 * -Catalogo de Mantenimiento de certificados Cifrados
 * 
 * Proyecto: Firma y Cifrado de Pagos
 *
 * @author FSW-Vector
 * @since 10/09/2018
 */
@Controller
public class ControllerCertificadosCifrado extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2569938308224124278L;
	
	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaCertificadosCifrado";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaCertificadoCifrado";
	
	/** La constante BEAN_CERTIFICADO. */
	private static final String BEAN_CERTIFICADO = "beanCertificadoCifrado";
	
	/** La constante BEAN_CERTIFICADOS. */
	private static final String BEAN_CERTIFICADOS = "beanCertificados";
	
	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";
	
	/** La constante LISTAS. */
	private static final String LISTAS = "listas";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	private static final UtilsExportacionCertificados UTILERIAS = UtilsExportacionCertificados.getUtilerias();
	
	/** La variable que contiene informacion con respecto a: bo certificados cifrado. */
	private BOCertificadosCifrado boCertificadosCifrado;

	/**
	 * Muestra certificados cifrado.
	 * 
	 * Metodo que carga la vista principal del catalogo con 
	 * todas sus opciones de gestion 
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @return Objeto model and view
	 */
	@RequestMapping(value="/consultaCertificadosCifrado.do")
	public ModelAndView muestraCertificadosCifrado(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) BeanCertificadoCifrado beanFilter) {
		//Se crea el model&view a retornar
		ModelAndView model = new ModelAndView();
		BeanCertificadosCifrado response = null;
		try {
			//Se realiza la consulta
			response = boCertificadosCifrado.consultar(getArchitechBean(), beanFilter, beanPaginador);
			// Seteamos los datos al Model para pasarlos a la Vista
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CERTIFICADOS, response);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * Agregar certificado.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/agregarCertificado.do")
	public ModelAndView agregarCertificado(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_CERTIFICADO) BeanCertificadoCifrado beanCertificado) {
		//Se crea el model&view a retornar
		ModelAndView model = null;
		BeanCertificadosCifrado certificados = null;
		BeanResBase response = null;
		try {
			beanCertificado.setCanal(beanCertificado.getCanal());
			//Se conusmen el servicio de alta de certificado
			response = boCertificadosCifrado.agregarCertificado(getArchitechBean(), beanCertificado);
			if (Errores.OK00000V.equals(response.getCodError())) {
				certificados = boCertificadosCifrado.consultar(getArchitechBean(), new BeanCertificadoCifrado(), new BeanPaginador());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_CERTIFICADOS, certificados);
				model.addObject(BEAN_CERTIFICADO, new BeanCertificadoCifrado());
			} else {
				//Se consultan los combos
				model = new ModelAndView(PAGINA_ALTA);
				BeanCombosCertificadoCifrado combos = boCertificadosCifrado.consultarCombos(getArchitechBean());
				model.addObject(LISTAS, combos);
				model.addObject(ACCION, ACCION_ALTA);
				model.addObject(BEAN_CERTIFICADO, beanCertificado);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	/**
	 * Editar certificado.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/editarCertificado.do")
	public ModelAndView editarCertificado(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_CERTIFICADO) BeanCertificadoCifrado beanCertificado) {
		//Se crea el model&view a retornar
		ModelAndView model = null;
		BeanCertificadosCifrado certificados = null;
		try {
			//Se conusmen el servicio de edicion de certificado
			BeanResBase response = boCertificadosCifrado.editarCertificado(getArchitechBean(), beanCertificado);
			if (Errores.OK00000V.equals(response.getCodError())) {
				certificados = boCertificadosCifrado.consultar(getArchitechBean(), new BeanCertificadoCifrado(), new BeanPaginador());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_CERTIFICADOS, certificados);
				model.addObject(BEAN_CERTIFICADO, new BeanCertificadoCifrado());
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				BeanCombosCertificadoCifrado combos = boCertificadosCifrado.consultarCombos(getArchitechBean());
				model.addObject(LISTAS, combos);
				model.addObject(BEAN_CERTIFICADO, beanCertificado);
				model.addObject(ACCION, ACCION_MODIFICAR);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	/**
	 * Mantenimiento de Certificado.
	 * 
	 * Muestar la pantalla de Agregar o Modificar Certificado. 
	 * Si la lista contiene una seleccion se cargan los datos en el Bean 
	 * y se retorna el model&view
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanCertificados El objeto: bean certificados
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/mantenimientoCertificado.do")
	public ModelAndView mantenimientoCertificado(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_CERTIFICADOS) BeanCertificadosCifrado beanCertificados) {
		//Se crea el model&view a retornar
		BeanCombosCertificadoCifrado combos = null;
		ModelAndView model = null;
		BeanCertificadoCifrado beanReq = new BeanCertificadoCifrado();
		if (beanCertificados != null && beanCertificados.getCertificados() != null) {
			for(BeanCertificadoCifrado certificado : beanCertificados.getCertificados()) {
				//se valida que haya una seleccion a dar mantenimiento
				if(certificado.isSeleccionado()) {
					beanReq = certificado;
				}
			}			
		}
		try {
			//Se consultan los combos
			combos = boCertificadosCifrado.consultarCombos(getArchitechBean());
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(LISTAS, combos);
			model.addObject(ACCION, beanReq.getCanal() != null ? ACCION_MODIFICAR : ACCION_ALTA);
			model.addObject(BEAN_CERTIFICADO, beanReq);
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}

    /**
     * Elim cat int financieros.
     *
     * @param req El objeto: req
     * @param servletResponse El objeto: servlet response
     * @param beanCertificados El objeto: bean certificados
     * @return Objeto model and view
     */
    @RequestMapping(value = "/eliminarCertificadosCifrado.do")
    public ModelAndView elimCatIntFinancieros(HttpServletRequest req, HttpServletResponse servletResponse,
    		@ModelAttribute(BEAN_CERTIFICADOS) BeanCertificadosCifrado beanCertificados){
    	//Se crea el model&view a retornar
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "", erroneos = "", mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			//Se ejecuta la eliminacion de los registros seleccionados
			response = boCertificadosCifrado.eliminarCertificados(getArchitechBean(), beanCertificados);
			//Se ejecuta la consulta inicial
			BeanCertificadosCifrado responseConsulta = boCertificadosCifrado.consultar(getArchitechBean(),
					new BeanCertificadoCifrado(), new BeanPaginador());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_CERTIFICADOS, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
				}
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje.replaceAll("intermediarios", "Certificados"));
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return modelAndView;
    }
    
    /**
     * Metodo que se utiliza para exportar en la pantalla de Certificados.
     *
     * @param req Objeto del tipo HttpServletRequest
     * @param servletResponse Objeto del tipo HttpServletResponse
     * @param beanPaginador El objeto: bean paginador
     * @param beanFilter El objeto: bean filter
     * @return ModelAndView Objeto del tipo ModelAndView
     */
	@RequestMapping(value = "/exportarCertificados.do")
	public ModelAndView exportarCertificados(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_CERTIFICADOS) BeanCertificadosCifrado beanCertificado) {
		//Se crea el model&view a retornar
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", UTILERIAS.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", UTILERIAS.getBody(beanCertificado.getCertificados()));
		modelAndView.addObject("NAME_SHEET", "Certificados de Cifrado");
			
		// Seteamos los datos al Model para pasarlos a la Vista
		return modelAndView;
	}
	
	/**
	 * Exportar todos certificados.
	 *
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param req El objeto: req
	 * @return Objeto model and view
	 */
	@RequestMapping(value="/exportarTodosCertificados.do")
	public ModelAndView exportarTodosCertificados(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) BeanCertificadoCifrado beanFilter) {
		//Se crea el model&view a retornar
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanCertificadosCifrado responseConsulta = new BeanCertificadosCifrado();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boCertificadosCifrado.consultar(getArchitechBean(), beanFilter, beanPaginador);
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_CERTIFICADOS, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boCertificadosCifrado.exportarTodo(getArchitechBean(), beanFilter, responseConsulta.getTotalReg());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CERTIFICADOS, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	/**
	 * Obtener el objeto: bo certificados cifrado.
	 *
	 * @return El objeto: bo certificados cifrado
	 */
	public BOCertificadosCifrado getBoCertificadosCifrado() {
		return boCertificadosCifrado;
	}

	/**
	 * Definir el objeto: bo certificados cifrado.
	 *
	 * @param boCertificadosCifrado El nuevo objeto: bo certificados cifrado
	 */
	public void setBoCertificadosCifrado(BOCertificadosCifrado boCertificadosCifrado) {
		this.boCertificadosCifrado = boCertificadosCifrado;
	}

}
