/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerDetalleRecepcion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BORecepcion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.exception.BusinessException;

/**
 * Controlador para la consulta de detalle de pago
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerDetalleRecepcion extends Architech{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5512026133096833306L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private BORecepcion boRecepcion;
	
	/**
	 * @return the boRecepcion
	 */
	public BORecepcion getBoRecepcion() {
		return boRecepcion;
	}

	/**
	 * @param boRecepcion the boRecepcion to set
	 */
	public void setBoRecepcion(BORecepcion boRecepcion) {
		this.boRecepcion = boRecepcion;
	}
	
	/**
	 * @param beanLlave Objeto del tipo BeanSPIDLlave
	 * @param opcion Objeto del tipo String
	 * @return ModelAndView
	 */
	@RequestMapping(value="/muestraDetalleRecepcion.do")
	public ModelAndView mostrarDetalleRecepcion(BeanSPIDLlave beanLlave, @RequestParam("fecha") String fecha, @RequestParam("opcion") String opcion) {
		ModelAndView model = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			BeanResRecepcionSPID beanResReceptorSPID = boRecepcion.obtenerDetalleRecepcion(beanLlave, getArchitechBean());
			
			model = new ModelAndView("detRecep", "beanRes", beanResReceptorSPID.getBeanReceptorSPID());
			model.addObject(Constantes.COD_ERROR, beanResReceptorSPID.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResReceptorSPID.getTipoError());
			model.addObject(Constantes.DESC_ERROR, beanResReceptorSPID.getCodError());
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("opcion", opcion);
			model.addObject("fecha", fecha);
			model.addObject("titulo", "Detalle de Recepci&oacute;n de Operaciones Hist&oacute;ricas");
			model.addObject("servicio", "realizaConsultaReceptorHist.do");
		}
		catch(BusinessException ex){
			showException(ex);
		}
		return model;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
