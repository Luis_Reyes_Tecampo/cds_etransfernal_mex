package mx.isban.eTransferNal.controllers.contingencia;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Class ControllerInitContingencia.
 *
 * @author FSW-Vector
 * @since 26/10/2018
 */
@Controller
public class ControllerInitContingencia extends Architech{
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**
	 * Inicio.
	 *
	 * @return Objeto model and view
	 */
	@RequestMapping(value="contingenciaInit.do")
	public ModelAndView inicio(){
		/**Se instancia la vista **/
		return new ModelAndView("inicio");
	}

	
}
