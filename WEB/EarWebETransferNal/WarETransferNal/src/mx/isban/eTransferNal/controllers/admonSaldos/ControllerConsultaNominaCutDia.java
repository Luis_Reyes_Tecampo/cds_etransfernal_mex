/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaNominaCutDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/11/2016 12:42:00 IDS		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.admonSaldos;

import java.util.HashMap;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTDia;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.admonSaldos.BOConsultaNominaCUTDia;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerConsultaNominaCutDia extends Architech {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1215371993784593044L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de PAGINA
	 */
	private static final String PAGINA = "consultaNominaCUTDia";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN
	 */
	private static final String BEAN = "beanRes";
	
	/**
	 * Propiedad del tipo BOConsultaNominaCUTDia que almacena el valor de bOConsultaNominaCUTDia
	 */
	private BOConsultaNominaCUTDia bOConsultaNominaCUTDia;
	
	/**
	 * Propiedad del tipo ResourceBundleMessageSource que almacena el valor de messageSource
	 */
	private ResourceBundleMessageSource messageSource;
	/**
	 * @return ModelAndView Objeto de Spring MVC 
	 */
	@RequestMapping(value = "/muestraConsultaNominaCutDia.do")
	ModelAndView muestraConsultaNominaCutDia(){
		Map<String,Object> map = new HashMap<String,Object>();
		ModelAndView modelAndView = null;
		BeanResConsNomCUTDia beanResConsNomCUTDia = null;
		modelAndView = new ModelAndView(PAGINA);
		beanResConsNomCUTDia = bOConsultaNominaCUTDia.consultar(getArchitechBean());
		map = modelAndView.getModel();
		map.put(BEAN, beanResConsNomCUTDia);
		return modelAndView;
	}

	/**
	 * @return ModelAndView Objeto de Spring MVC 
	 */
	@RequestMapping(value = "/consultaNominaCutDia.do")
	ModelAndView consultaNominaCutDia(){
		Map<String,Object> map = null;
		ModelAndView modelAndView = null;
		BeanResConsNomCUTDia beanResConsNomCUTDia = null;
		modelAndView = new ModelAndView(PAGINA);
		beanResConsNomCUTDia = bOConsultaNominaCUTDia.consultarNomina(getArchitechBean());
		if(!Errores.OK00000V.equals(beanResConsNomCUTDia.getCodError())){
			map = modelAndView.getModel();
			map.put(Constantes.TIPO_ERROR, beanResConsNomCUTDia.getTipoError());
	  	    map.put(Constantes.COD_ERROR, beanResConsNomCUTDia.getCodError());
			map.put(Constantes.DESC_ERROR, messageSource.getMessage("codError."+beanResConsNomCUTDia.getCodError(), null, null));
		}
		modelAndView.addObject(BEAN, beanResConsNomCUTDia);
		return modelAndView;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOConsultaNominaCUTDia
	 * @return bOConsultaNominaCUTDia Objeto del tipo BOConsultaNominaCUTDia
	 */
	public BOConsultaNominaCUTDia getBOConsultaNominaCUTDia() {
		return bOConsultaNominaCUTDia;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOConsultaNominaCUTDia
	 * @param consultaNominaCUTDia del tipo BOConsultaNominaCUTDia
	 */
	public void setBOConsultaNominaCUTDia(
			BOConsultaNominaCUTDia consultaNominaCUTDia) {
		bOConsultaNominaCUTDia = consultaNominaCUTDia;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo ResourceBundleMessageSource
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo ResourceBundleMessageSource
	 */
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}



}
