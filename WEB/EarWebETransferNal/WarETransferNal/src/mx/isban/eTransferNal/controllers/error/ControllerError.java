package mx.isban.eTransferNal.controllers.error;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerError extends Architech{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 3514922619969306831L;

  /**Metodo que se utiliza para recibir la peticion de inicio.do
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "errorAgave.do")
   public ModelAndView errorAgave(){
      return new ModelAndView("excepcionInesperadaArq");
   }
   
   /**Metodo que se utiliza para recibir la peticion de inicio.do
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "errorGrl.do")
    public ModelAndView errorGrl(){
       return new ModelAndView("excepcionInesperadaGrl");
    }

}
