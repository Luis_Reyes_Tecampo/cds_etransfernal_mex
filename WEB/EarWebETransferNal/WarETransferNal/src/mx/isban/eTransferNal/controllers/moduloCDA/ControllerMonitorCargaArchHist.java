/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorCargaArchHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHist;
import mx.isban.eTransferNal.servicio.moduloCDA.BOMonitorCargaArchHist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad del Monitor de carga de archivos
 * historico
**/
@Controller
public class ControllerMonitorCargaArchHist extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;

	/**
	 * Referencia al servicio del Monitor de carga de Archivos historicos
	 */
	private BOMonitorCargaArchHist bOMonitorCargaArchHist = null;
	
   /**Metodo que muestra la funcionalidad del monitor carga archivos
   * CDA historicos vacia
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraMonitorCargaArchHist.do")
   public ModelAndView muestraMonitorCargaArchHist(){
      return new ModelAndView("monitorCargaArchHist");
   }
   /**Metodo que muestra la funcionalidad del monitor carga archivos
   * CDA historicos con datos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/consultarMonitorCargaArchHist.do")
   public ModelAndView consultarMonitorCargaArchHist(BeanPaginador beanPaginador,HttpServletRequest req){
      ModelAndView modelAndView = null;
      BeanResMonCargaArchHist beanResMonCargaArchHist = null;
      RequestContext ctx = new RequestContext(req);
      try {
    	  beanResMonCargaArchHist = bOMonitorCargaArchHist.consultarMonitorCargaArchHist(
    			  beanPaginador, getArchitechBean());
    	  modelAndView = new ModelAndView("monitorCargaArchHist","resMon",beanResMonCargaArchHist);
     	  String mensaje = ctx.getMessage("codError."+beanResMonCargaArchHist.getCodError());
     	  modelAndView.addObject("codError", beanResMonCargaArchHist.getCodError());
     	  modelAndView.addObject("tipoError", beanResMonCargaArchHist.getTipoError());
     	  modelAndView.addObject("descError", mensaje);

	} catch (BusinessException e) {
		showException(e);
	}
      
      return modelAndView;
   }
   /**
    * Metodo get del servicio del monitor carga archivos historicos
    * @return BOMonitorCargaArchHist Objeto del Servicio del monitor carga archivos historicos
    */   
   public BOMonitorCargaArchHist getBOMonitorCargaArchHist() {
	  return bOMonitorCargaArchHist;
   }
	/**
    * Metodo set del servicio del monitor carga archivos historicos
    * @param bOMonitorCargaArchHist Se setea objeto del Servicio del monitor carga archivos historicos
    */
   public void setBOMonitorCargaArchHist(BOMonitorCargaArchHist bOMonitorCargaArchHist) {
	  this.bOMonitorCargaArchHist = bOMonitorCargaArchHist;
   }

}
