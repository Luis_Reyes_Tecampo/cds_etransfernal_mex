/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMantenimientoTemplates.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */

package mx.isban.eTransferNal.controllers.capturasManuales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplates;
import mx.isban.eTransferNal.servicio.capturasManuales.BOMantenimientoTemplatesExt;
import mx.isban.eTransferNal.utilerias.MetodosMantenimientoTemplates;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
/**
 * 
 * Clase Bean tipo Controller encargada de recibir y procesar las peticiones de la vista
 *
 */
@Controller
public class ControllerMantenimientoTemplates extends Architech{

	/** Constante privada del Serial Version */
	private static final long serialVersionUID = 8815748878189993702L;
	
	/** Constante privada de: Pagina principal del mantenimiento de templates */
	private static final String PAGINA_MTO_TEMPLATES = "mantenimientoTemplates";
	
	/** Constante privada de: Pagina de alta de templates */
	private static final String PAGINA_ALTA_TEMPLATES = "altaTemplates";
	
	/** Constante privada de: respuesta bean identificador vista */
	private static final String BEAN_VIEW_RES = "beanViewRespuesta";
	
	/** Variable de inyeccion a la capa de negocios */
	private BOMantenimientoTemplates boMantenimientoTemplates;
	
	/** Variable de inyeccion a la capa de negocios externa */
	private BOMantenimientoTemplatesExt boTemplatesExt;
	
	
	/**
	 * Metodo inicial GET de mantenimiento de templates.
	 *
	 * @param req Objeto con la solicitud request
	 * @param res Objeto con la peticion response
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/consultarMtoTemplates.do")
	public ModelAndView consultarMtoTemplates(HttpServletRequest req, HttpServletResponse res){
		return new ModelAndView(PAGINA_MTO_TEMPLATES, BEAN_VIEW_RES, new BeanResPrincipalTemplate());
	}
	
	
	/**
	 * Metodo que obtiene los templates basados en una peticion de filtro.
	 *
	 * @param req Objeto con la peticion Request
	 * @param res the res
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/consultarTemplatesBuscar.do")
	public ModelAndView buscarMtoTemplates(HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqConsultaTemplates beanReq = utilsMantenimientoTmps.requestToBeanReqConsultaTemplates(req,modelAndView);
		//Componentes iniciales
		RequestContext contexto = new RequestContext(req);
		BeanResPrincipalTemplate beanResultado = new BeanResPrincipalTemplate();
		
		//Acceso via URL. filtro = null
		if(beanReq.getFiltroEstado() == null){
			return new ModelAndView(PAGINA_MTO_TEMPLATES, BEAN_VIEW_RES, beanResultado);
		}
		
		try{
			//Buscar los templates que coincidan con el filtro
			 beanResultado = boMantenimientoTemplates.consultarTemplates(beanReq, getArchitechBean());
			 
			 //Re-propagar el filtro de busqueda
			 beanResultado.setFiltroEstado(beanReq.getFiltroEstado());
			 beanResultado.setForward(true);
			 
			 //Crear modelView para responder		
			 modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES, BEAN_VIEW_RES, beanResultado);
			
			//Ocurrio un error en la consulta. NO esta documentado en el cu
			if(!Errores.OK00000V.equals(beanResultado.getCodError())){
				String mensaje = contexto.getMessage(Constantes.COD_ERROR_PUNTO+beanResultado.getCodError());
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, beanResultado.getCodError(), beanResultado.getTipoError(), mensaje);			
	    	}
		} catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
     	return modelAndView;
	}
	
	
	/**
	 * Metodo que procesa la peticion de altas y devuelve los objetos necesarios para el formulario.
	 *
	 * @param req Objeto de peticion request
	 * @param res the res
	 * @return  ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/muestraAltaTemplates.do")
	public ModelAndView altaMtoTemplates(HttpServletRequest req, HttpServletResponse res){	
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqAltaEditTemplate beanReq = utilsMantenimientoTmps.requestToBeanReqAltaEditTemplate(req,modelAndView);
		//Componentes iniciales
		BeanResPrincipalTemplate beanResultados = new BeanResPrincipalTemplate();	
		BeanResPrincipalTemplate temp = new BeanResPrincipalTemplate();

		//Acceso via URL. tipo solicitud = null
		if(beanReq.getTipoSolicitud() == null){
			beanReq.setTipoSolicitud(0);
		}
		
		try{
			//Primera ocasion que se accede a la pagina
			if(beanReq.getTipoSolicitud() <=0){
				//Solicitar la lista de layouts
				beanResultados = boTemplatesExt.obtenerListaLayouts(beanReq,getArchitechBean());
				
				//Obtener la lista de usuarios
				temp = boTemplatesExt.obtenerListaUsuarios(beanReq, getArchitechBean());
				beanResultados.setListaUsuarios(temp.getListaUsuarios());
				
				//La lista de usuarios puede estar vacia y no es un error
				
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward		
				beanResultados.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultados.setForward(beanReq.getForward());
				
			} else{
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward		
				beanResultados.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultados.setForward(beanReq.getForward());
				
				//Propagar en respuesta. nombre del layout, selected layout, lista layouts
				beanResultados.getTemplate().setIdTemplate(beanReq.getTemplate().getIdTemplate());
				beanResultados.setSelectedLayout(beanReq.getSelectedLayout());
				beanResultados.setListaLayouts(beanReq.getListaLayouts());
				
				//Propagar en respuesta. Tabla de usuarios, Tabla de usuarios autorizados
				beanResultados.setListaUsuarios(beanReq.getListaUsuarios());
				beanResultados.getTemplate().setUsuariosAutorizan(beanReq.getTemplate().getUsuariosAutorizan());
				
				//Cargar tabla de campos.
				temp = boTemplatesExt.obtenerCamposLayout(beanReq, getArchitechBean());
				beanResultados.setListaCamposMaster(temp.getListaCamposMaster());
				
				//C1. La lista de campos del layout puede venir vacia y no genera un error.
			}
			beanResultados.setPaginador(beanReq.getPaginador());
			modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES, BEAN_VIEW_RES, beanResultados);
		}catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
		return modelAndView;
	}
	
	/**
	 * Metodo encargado de guardar  el objeto de template.
	 *
	 * @param req Objeto de peticion request
	 * @param res the res
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/guardarTemplate.do")
	public ModelAndView guardarMtoTemplate(HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqAltaEditTemplate beanReq = utilsMantenimientoTmps.requestToBeanReqAltaEditTemplate(req,modelAndView);
		//Componentes iniciales
		BeanResPrincipalTemplate beanResultadoView = new BeanResPrincipalTemplate();
		RequestContext ctx = new RequestContext(req);
		
		try{
			//Invocar el servicio
			beanResultadoView = boMantenimientoTemplates.guardarTemplate(beanReq, getArchitechBean());
			
			//El proceso termino sin errores: Regresar a Principal
			if(Errores.OK00000V.equals(beanResultadoView.getCodError())){
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward	
				beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultadoView.setForward(beanReq.getForward());
				
				modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES,BEAN_VIEW_RES,beanResultadoView); 	    
			}else{
				//Recuperar los parametros adicionales
				beanResultadoView.setSelectedLayout(beanReq.getSelectedLayout());			
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward		
				beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultadoView.setForward(beanReq.getForward());
				
				//Error en el proceso de guardado. 1) Duplicidad
				beanResultadoView.setPaginador(beanReq.getPaginador());
				modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES, BEAN_VIEW_RES, beanResultadoView);
				String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResultadoView.getCodError());			
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, beanResultadoView.getCodError(), beanResultadoView.getTipoError(), mensaje); 
			}
		}catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
		return modelAndView;
	}
	
	
		
	/**
	 * Metodo encargado de procesar la eliminacion de un conjunto de templates.
	 *
	 * @param req Objeto de peticion request
	 * @param res the res
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/eliminarTemplates.do")
	public ModelAndView eliminarMtoTemplates(final HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqConsultaTemplates beanReq = utilsMantenimientoTmps.requestToBeanReqConsultaTemplates(req,modelAndView);
		//Componentes iniciales
		BeanResPrincipalTemplate beanResultadoView = new BeanResPrincipalTemplate();
		RequestContext ctx = new RequestContext(req);
		String mensaje="";

		try{
			//Invocar el servicio
			beanResultadoView = boMantenimientoTemplates.eliminarTemplates(beanReq, getArchitechBean());
			
			// Propagar en respuesta: Filtro seleccionado y si se realizo una
			// peticion de forward
			beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
			beanResultadoView.setForward(true);
			beanResultadoView.setPaginador(beanReq.getPaginador());
			modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES,BEAN_VIEW_RES,beanResultadoView);
			
			//El proceso termino sin errores: Regresar a Principal. 1) Los registros se eliminaron OK.
			if(Errores.OK00000V.equals(beanResultadoView.getCodError())){			
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.CMMT004V);
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, Errores.CMMT004V, beanResultadoView.getTipoError(), mensaje);
				    	    
			}else{
				//El proceso termino con errores quedarse en la misma pagina. 1) Los registros tiene operaciones
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResultadoView.getCodError());
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView,  beanResultadoView.getCodError(), beanResultadoView.getTipoError(), mensaje);
			}
		}catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
		return modelAndView;
	}
	
	
	/**
	 * Metodo que obtiene los datos de un template para presentarlo en edicion.
	 *
	 * @param req Objeto de peticion request
	 * @param res the res
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/muestraEdicionTemplate.do")
	public ModelAndView edicionMtoTemplates(HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqAltaEditTemplate beanReq = utilsMantenimientoTmps.requestToBeanReqAltaEditTemplate(req,modelAndView);
		//Componentes iniciales
		BeanResPrincipalTemplate beanResultadoView = new BeanResPrincipalTemplate();
		RequestContext contexto = new RequestContext(req);
		
		//Acceso via URL. filtro = null =>(ir a) Mto Templates
		if(beanReq.getTemplate().getIdTemplate() == null){
			return new ModelAndView(PAGINA_MTO_TEMPLATES, BEAN_VIEW_RES, beanResultadoView);
		}
		
		try{
			//Cargar los datos basicos del template:
			beanResultadoView = boMantenimientoTemplates.obtenerTemplate(beanReq, getArchitechBean());
			
			//Verificar los resultados de la consulta. El template no se puede obtener.
			if(!Errores.OK00000V.equals(beanResultadoView.getCodError())){
				String mensaje = contexto.getMessage(Constantes.COD_ERROR_PUNTO + beanResultadoView.getCodError());			
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, beanResultadoView.getCodError(), beanResultadoView.getTipoError(), mensaje);			
			}
			
			//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward y modificacion
			beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
			beanResultadoView.setForward(beanReq.getForward());
			beanResultadoView.setModoEdicion(true);
			
			
			//Propagar en respuesta. selected layout, lista layouts
			beanResultadoView.setSelectedLayout(beanResultadoView.getTemplate().getIdLayout());		
			beanResultadoView.setPaginador(beanReq.getPaginador());
			modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES, BEAN_VIEW_RES, beanResultadoView);
		}catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
		return modelAndView;
	}

	/**
	 * Metodo encargado de guardar/actualizar el template modificado.
	 *
	 * @param req Objeto de peticion request
	 * @param res the res
	 * @return ModelAndView Objeto de ModelAndView de Spring
	 */
	@RequestMapping(value="/actualizarTemplate.do")
	public ModelAndView actualizarMtoTemplate(HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES);
		BeanReqAltaEditTemplate beanReq = utilsMantenimientoTmps.requestToBeanReqAltaEditTemplate(req,modelAndView);
		//Componentes iniciales
		BeanResPrincipalTemplate beanResultadoView = new BeanResPrincipalTemplate();
		RequestContext contexto = new RequestContext(req);
		
		try{
			//Invocar el servicio de actualizar
			beanResultadoView = boMantenimientoTemplates.actualizarTemplate(beanReq, getArchitechBean());
			
			//El proceso termino sin errores: Regresar a Principal
			if(Errores.OK00000V.equals(beanResultadoView.getCodError())){
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward	
				beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultadoView.setForward(beanReq.getForward());
				
				modelAndView = new ModelAndView(PAGINA_MTO_TEMPLATES,BEAN_VIEW_RES,beanResultadoView);  	    
			}else{
				//Recuperar los parametros adicionales
				beanResultadoView.setSelectedLayout(beanReq.getSelectedLayout());			
				//Propagar en respuesta: Filtro seleccionado y si se realizo una peticion de forward		
				beanResultadoView.setFiltroEstado(beanReq.getFiltroEstado());
				beanResultadoView.setForward(beanReq.getForward());
				//Recuperar el modo edicion
				beanResultadoView.setModoEdicion(beanReq.getModoEdicion());
						
				//Error en el proceso de guardado. 1) Los usuarios tienen operaciones asociadas
				beanResultadoView.setPaginador(beanReq.getPaginador());
				modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES, BEAN_VIEW_RES, beanResultadoView);
				String mensaje = contexto.getMessage(Constantes.COD_ERROR_PUNTO+beanResultadoView.getCodError());			
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, beanResultadoView.getCodError(), beanResultadoView.getTipoError(), mensaje); 
			}
		} catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			modelAndView = new ModelAndView(PAGINA_ALTA_TEMPLATES);
			utilsMantenimientoTmps.asignarErroresProceso(
					modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
		}
		return modelAndView;
	}
	
	/**
	 * Clonado template.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value="/clonadoTemplate.do")
	public ModelAndView clonadoTemplate(HttpServletRequest req, HttpServletResponse res){
		MetodosMantenimientoTemplates utilsMantenimientoTmps=new MetodosMantenimientoTemplates();
		ModelAndView modelAndView = edicionMtoTemplates(req, res);
		Object bean = modelAndView.getModel().get(BEAN_VIEW_RES);
		if(bean != null && bean instanceof BeanResPrincipalTemplate){
			BeanResPrincipalTemplate beanResultadoView = (BeanResPrincipalTemplate)bean;
			beanResultadoView.getTemplate().setOperacionesAsociadas(false);
			beanResultadoView.getTemplate().setIdTemplate("");
			beanResultadoView.setModoEdicion(false);
			
			//Solicitar la lista de layouts
			BeanReqAltaEditTemplate beanReq = new BeanReqAltaEditTemplate();
			try {
				BeanResPrincipalTemplate beanResultados = boTemplatesExt.obtenerListaLayouts(beanReq,getArchitechBean());
				beanResultadoView.setListaLayouts(beanResultados.getListaLayouts());
			} catch (BusinessException e) {
				//Se controla BusinessException
				showException(e);
				utilsMantenimientoTmps.asignarErroresProceso(
						modelAndView, e.getCode(), Errores.TIPO_MSJ_ERROR, e.getMessage());
			}
			
		}
		return modelAndView;
	}

	/**
	 * Metodo que permite asignar el objeto de negocios
	 * @param boMantenimientoTemplates the boMantenimientoTemplates to set
	 */
	public void setBoMantenimientoTemplates(
			BOMantenimientoTemplates boMantenimientoTemplates) {
		this.boMantenimientoTemplates = boMantenimientoTemplates;
	}

	/**
	 * Metodo que permite establecer el valor del objeto de negocios externo
	 * @param boTemplatesExt valor del objeto de negocios a establecer
	 */
	public void setBoTemplatesExt(BOMantenimientoTemplatesExt boTemplatesExt) {
		this.boTemplatesExt = boTemplatesExt;
	}
}
