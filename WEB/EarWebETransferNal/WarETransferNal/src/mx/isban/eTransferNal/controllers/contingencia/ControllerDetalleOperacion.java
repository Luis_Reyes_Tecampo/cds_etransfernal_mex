/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerDetalleOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/10/2018 05:39:58 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.controllers.contingencia;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanReqTranCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanResTranCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanTranMensajeCanales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.contingencia.BODetalleOperacion;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtilsContingenciaExportarDetalle;
import mx.isban.eTransferNal.utilerias.ViewExcel;


/**
 * Class ControllerDetalleOperacion.
 *
 * @author FSW-Vector
 * @since 11/10/2018
 */
@Controller
public class ControllerDetalleOperacion extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 827176495110763304L;
	
	/** La constante BEANDETALLE. */
	private static final String BEANDETALLE = "beanResTranCanales";
	
	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "detalleContigCanal";
	
	/** La variable que contiene informacion con respecto a: bo detalle operacion. */
	private BODetalleOperacion boDetalleOperacion;
	
	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";
	
	/** La constante IMPORTE_PAGINA. */
	private static final String IMPORTE_PAGINA = "importePagina";
	
	/** La constante ARCHIVO. */
	private static final String ARCHIVO = "nombreArchivo";
	
	/** La constante NOMBRE_ARCHIVO. */
	private static final String NOMBRE_ARCHIVO = "nombreArch";

	/** La constante UTILERIAS. */
	private static final UtilsContingenciaExportarDetalle UTILERIAS = UtilsContingenciaExportarDetalle.getUtilerias();
	
	/** La constante TIPO_PANTALLA. */
	private static final String TIPO_PANTALLA = "historica";
	
	/**
	 * Detalle operacion.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param bean El objeto: bean
	 * @return Objeto model and view
	 */
	@RequestMapping (value = "detalleOperacion.do")
	public ModelAndView detalleOperacion(HttpServletRequest req, HttpServletResponse servletResponse, 
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) BeanFilter beanFilter,BeanReqConsultaOperContingente bean) {
		/** Se crean las variables necesarias para hacer la consulta**/
		ModelAndView model = new ModelAndView();
		BeanResTranCanales response = null;
		/** Se convierte a mayusculas el nombre del archivo **/
		String nombreArchivo = req.getParameter(NOMBRE_ARCHIVO).trim();
		try {
			/** Se llama al metodo de BO **/
			response = boDetalleOperacion.consultar(getArchitechBean(), nombreArchivo, beanFilter, beanPaginador, bean.getNomPantalla());
			response.setNomPantalla(bean.getNomPantalla());			
			
			if((TIPO_PANTALLA).equals(bean.getNomPantalla())) {
				response.setFechaOperacion(bean.getFechaOperacion());
			}
			
			/** Se asignan los objetos al modelo **/
			model = new ModelAndView(PAGINA_CONSULTA, BEANDETALLE, response);
			model.addObject(IMPORTE_PAGINA, obtenerImportePagina(response));
			model.addObject(ARCHIVO, nombreArchivo);
			/** Se validan los errores **/
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	
	
	/**
	 * Exportar detalle operacion.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param bean El objeto: bean
	 * @return Objeto model and view
	 */
	@RequestMapping(value  = "/exportarDetalleOperacion.do")
	public ModelAndView exportarDetalleOperacion(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) BeanFilter beanFilter,BeanReqTranCanales bean) {
		/** Se declaran las varibles necesarias **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		BeanResTranCanales response;
		String nombreArchivo = req.getParameter(NOMBRE_ARCHIVO).trim();
		String nomArchivo =nombreArchivo;
		try {
			response = boDetalleOperacion.consultar(getArchitechBean(), nombreArchivo, beanFilter, beanPaginador,bean.getNomPantalla());
			response.setNomPantalla(bean.getNomPantalla());			
			
			if((TIPO_PANTALLA).equals(bean.getNomPantalla())) {
				response.setFechaOperacion(bean.getFechaOperacion());
			}
			if (Errores.OK00000V.equals(response.getBeanError().getCodError()) ||
					Errores.ED00011V.equals(response.getBeanError().getCodError())) {
				/** Se forma la vista del archivo **/
				formatCellHeader = new FormatCell();
				formatCellHeader.setFontName("calibri");
				formatCellHeader.setFontSize((short) 11);
				formatCellHeader.setBold(true);
				
				formatCellBody = new FormatCell();
				formatCellBody.setFontName("calibri");
				formatCellBody.setFontSize((short) 11);
				formatCellBody.setBold(false);
				
				model = new ModelAndView(new ViewExcel());
				model.addObject("FORMAT_HEADER", formatCellHeader);
				model.addObject("FORMAT_CELL", formatCellBody);
				model.addObject("HEADER_VALUE", UTILERIAS.getHeaderExcel(ctx));
				model.addObject("BODY_VALUE", UTILERIAS.getBody(response.getRegistros()));
				if((TIPO_PANTALLA).equals(bean.getNomPantalla())) {
					nomArchivo= "HIS_"+nombreArchivo;
				}
				
				model.addObject("NAME_SHEET", "DETALLE DE " + nomArchivo);
			}else {
				/** Manejo de errores  **/
				model = new ModelAndView(PAGINA_CONSULTA, BEANDETALLE, response);
				String mensaje = ctx.getMessage("codError." + response.getBeanError().getCodError());
				model.addObject("codError", response.getBeanError().getCodError());
				model.addObject("tipoError", response.getBeanError().getTipoError());
				model.addObject("descError", mensaje);
			}
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	
	/**
	 * Exportar todos lo detalles operacion.
	 *
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param req El objeto: req
	 * @param bean El objeto: bean
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarTodoDetalleOperacion.do")
	public ModelAndView exportarTodoDetalleOperacion(BeanPaginador beanPaginador, BeanFilter beanFilter, 
			HttpServletRequest req,BeanReqTranCanales bean) {
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanResTranCanales responseConsulta = new BeanResTranCanales();
		RequestContext ctx = new RequestContext(req);
		String nombreArchivo = req.getParameter(NOMBRE_ARCHIVO).trim();
		try {
			responseConsulta = boDetalleOperacion.consultar(getArchitechBean(), nombreArchivo, beanFilter, beanPaginador,bean.getNomPantalla());		
			responseConsulta.setNomPantalla(bean.getNomPantalla());			
			/**sE ASGINA HIS al inicio si la pantalla es historica **/
			if(("historica").equals(bean.getNomPantalla())) {
				responseConsulta.setFechaOperacion(bean.getFechaOperacion());
			}
			
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEANDETALLE, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				model.addObject(ARCHIVO, nombreArchivo);
				return model;
			}
			
			response = boDetalleOperacion.exportarTodo(getArchitechBean(), nombreArchivo, beanFilter, responseConsulta.getTotalReg(), bean.getNomPantalla());
			model = new ModelAndView(PAGINA_CONSULTA, BEANDETALLE, responseConsulta);
			model.addObject(ARCHIVO, nombreArchivo);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (Exception e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	/**
	 * Obtener importe pagina.
	 *
	 * @param beanResTranCanales El objeto: bean res tran canales
	 * @return Objeto big decimal
	 */
	private BigDecimal obtenerImportePagina(BeanResTranCanales beanResTranCanales) {
		/** Se obtiene el importe de la pagina actual**/
		BigDecimal importePagina = BigDecimal.ZERO;
		/** Se suma el campo Importe mas el campo Importe Iva **/
		for(BeanTranMensajeCanales beanRes : beanResTranCanales.getRegistros()) {
			/** Importe **/
			importePagina = importePagina.add(validaImporte(beanRes.getBeanDatosGenerales().getImporte()));
			/** Importe Iva **/
			importePagina = importePagina.add(validaImporte(beanRes.getBeanDatosGenerales().getBeanDatosGeneralesExt().getImporteIva()));
		}
		/** Retorno del metodo **/
		return importePagina;
	}
	
	/**
	 * Valida importe.
	 *
	 * @param valorActual El objeto: valor actual
	 * @return Objeto big decimal
	 */
	public BigDecimal validaImporte(String valorActual) {
		BigDecimal importeActual = BigDecimal.ZERO;
		if(valorActual != null && !"".equals(valorActual)) {
			importeActual = new BigDecimal(valorActual);
		}
		return importeActual;
	}
	/**
	 * Obtener el objeto: bo detalle operacion.
	 *
	 * @return El objeto: bo detalle operacion
	 */
	public BODetalleOperacion getBoDetalleOperacion() {
		return boDetalleOperacion;
	}

	/**
	 * Definir el objeto: bo detalle operacion.
	 *
	 * @param boDetalleOperacion El nuevo objeto: bo detalle operacion
	 */
	public void setBoDetalleOperacion(BODetalleOperacion boDetalleOperacion) {
		this.boDetalleOperacion = boDetalleOperacion;
	}

}
