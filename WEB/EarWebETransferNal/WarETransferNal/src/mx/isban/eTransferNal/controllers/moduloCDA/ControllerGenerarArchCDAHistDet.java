/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerGenerarArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 18:00:24 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDet;
import mx.isban.eTransferNal.servicio.moduloCDA.BOGenerarArchCDAHistDet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Generar Archivo CDA Historico
 * Detalle
**/
@Controller
public class ControllerGenerarArchCDAHistDet extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	/**Constante del tipo String*/
	private static final String COD_ERROR ="codError";
	/**Constante del tipo String*/
	private static final String COD_ERROR_PUNTO ="codError.";
	/**Constante del tipo String*/
	private static final String TIPO_ERROR ="tipoError";
	/**Constante del tipo String*/
	private static final String DESC_ERROR ="descError";


	/**
	 * Referencia al servicio de generar archivo CDA historico detalle
	 */
	private BOGenerarArchCDAHistDet bOGenerarArchCDAHistDet;
	
	

    /**Metodo que permite desplegar y paginar la funcionalidad de generacion
    * de archivo cda historico
    * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
    * @param beanPaginador Objeto del tipo @see BeanPaginador
    * @param req Objeto del tipo HttpServletRequests
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/muestraGenerarArchCDAHistDet.do")
    public ModelAndView muestraGenerarArchCDAHistDet(BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet, 
    		BeanPaginador beanPaginador, HttpServletRequest req){      
	  ModelAndView modelAndView = null;
      BeanResConsGenArchHistDet beanResConsGenArchHistDet = null;
      RequestContext ctx = new RequestContext(req);
      try {
    	  beanResConsGenArchHistDet = bOGenerarArchCDAHistDet.consultaGenArchHistDet(
    			  beanReqConsGenArchCDAHistDet,new BeanPaginador(), getArchitechBean());
    	  modelAndView = new ModelAndView("generarArchivoCDAHistDet","archivoCDAHistDet",beanResConsGenArchHistDet);
    	  modelAndView.addObject("paginador", beanPaginador);
          modelAndView.addObject("idPeticion", beanReqConsGenArchCDAHistDet.getIdPeticion());

    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResConsGenArchHistDet.getCodError());
     	  modelAndView.addObject(COD_ERROR, beanResConsGenArchHistDet.getCodError());
     	  modelAndView.addObject(TIPO_ERROR, beanResConsGenArchHistDet.getTipoError());
     	  modelAndView.addObject(DESC_ERROR, mensaje);
	} catch (BusinessException e) {
		showException(e);
	}
	
      return modelAndView;
   }
  /**Metodo que permite desplegar y paginar la funcionalidad de generacion
	* de archivo cda historico
	* @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	* @param beanPaginador Objeto del tipo @see BeanPaginador
	* @param req Objeto del tipo HttpServletRequests
	* @return ModelAndView Objeto del tipo ModelAndView
	*/
   @RequestMapping(value = "/consultarGenerarArchHistDet.do")
   public ModelAndView consultarGenerarArchHistDet(BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet,
		   BeanPaginador beanPaginador, HttpServletRequest req){
		ModelAndView modelAndView = null;
	    BeanResConsGenArchHistDet beanResConsGenArchHistDet = null;
	    RequestContext ctx = new RequestContext(req);
	    try {
	    	  beanResConsGenArchHistDet = bOGenerarArchCDAHistDet.consultaGenArchHistDet(
	    			  beanReqConsGenArchCDAHistDet,beanReqConsGenArchCDAHistDet.getPaginador(), getArchitechBean());
	    	  modelAndView = new ModelAndView("generarArchivoCDAHistDet","archivoCDAHistDet",beanResConsGenArchHistDet);
	  		  modelAndView.addObject("paginador", beanPaginador);
	  		  modelAndView.addObject("idPeticion", beanReqConsGenArchCDAHistDet.getIdPeticion());
	    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResConsGenArchHistDet.getCodError());
	     	  modelAndView.addObject(COD_ERROR, beanResConsGenArchHistDet.getCodError());
	     	  modelAndView.addObject(TIPO_ERROR, beanResConsGenArchHistDet.getTipoError());
	     	  modelAndView.addObject(DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		
	    return modelAndView;
   }
   /**Metodo que permite desplegar y paginar la funcionalidad de generacion
	* de archivo cda historico
	* @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	* @param beanPaginador Objeto del tipo @see BeanPaginador
	* @param req Objeto del tipo HttpServletRequests
	* @return ModelAndView Objeto del tipo ModelAndView
	*/
   @RequestMapping(value = "/elimSelecGenArchCDAHistDet.do")
   public ModelAndView elimSelecGenArchCDAHistDet(BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet
		   ,BeanPaginador beanPaginador, HttpServletRequest req){
	   ModelAndView modelAndView = null;
	   BeanResElimSelecArchCDAHistDet beanResElimSelecArchCDAHistDet = null;
	   RequestContext ctx = new RequestContext(req);
	    try {
	    	beanResElimSelecArchCDAHistDet = bOGenerarArchCDAHistDet.elimSelGenArchCDAHistDet(
	    			  beanReqConsGenArchCDAHistDet, getArchitechBean());
			modelAndView = new ModelAndView("generarArchivoCDAHistDet","archivoCDAHistDet",beanResElimSelecArchCDAHistDet);
			modelAndView.addObject("paginador", beanPaginador);
			modelAndView.addObject("idPeticion", beanReqConsGenArchCDAHistDet.getIdPeticion());
			String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResElimSelecArchCDAHistDet.getCodError());
	     	  modelAndView.addObject(COD_ERROR, beanResElimSelecArchCDAHistDet.getCodError());
	     	  modelAndView.addObject(TIPO_ERROR, beanResElimSelecArchCDAHistDet.getTipoError());
	     	  modelAndView.addObject(DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
	    return modelAndView;
   }
   /**Metodo que permite generar el archivo CDA historico Detallle
   * @param beanReqGenArchCDAHistDet Objeto del tipo @see BeanReqGenArchCDAHistDet
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/generarArchCDAHistDet.do")
   public ModelAndView generarArchCDAHistDet(BeanReqGenArchCDAHistDet beanReqGenArchCDAHistDet,HttpServletRequest req){
	   BeanResGenArchCDAHistDet beanResGenArchCDAHistDet = null;
       ModelAndView modelAndView = null;
       RequestContext ctx = new RequestContext(req);
       try {
    	  beanResGenArchCDAHistDet = bOGenerarArchCDAHistDet.generarArchCDAHistDet(beanReqGenArchCDAHistDet, getArchitechBean());
    	  modelAndView = new ModelAndView("monitorCargaArchHist");
    	  String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResGenArchCDAHistDet.getCodError());
     	  modelAndView.addObject(COD_ERROR, beanResGenArchCDAHistDet.getCodError());
     	  modelAndView.addObject(TIPO_ERROR, beanResGenArchCDAHistDet.getTipoError());
     	  modelAndView.addObject(DESC_ERROR, mensaje);
	   } catch (BusinessException e) {
		  showException(e);
	   }
       return modelAndView;
   }
   /**
    * Metodo get del Servicio BOGenerarArchCDAHistDet
    * @return BOGenerarArchCDAHistDet
    */
   public BOGenerarArchCDAHistDet getBOGenerarArchCDAHistDet() {
		return bOGenerarArchCDAHistDet;
   }
   /**
    * Metodo set que modifica la referencia al servicio BOGenerarArchCDAHistDet
    * @param bOGenerarArchCDAHistDet Objeto del tipo BOGenerarArchCDAHistDet
    */
   public void setBOGenerarArchCDAHistDet(
			BOGenerarArchCDAHistDet bOGenerarArchCDAHistDet) {
		this.bOGenerarArchCDAHistDet = bOGenerarArchCDAHistDet;
   }
	   
	   


}
