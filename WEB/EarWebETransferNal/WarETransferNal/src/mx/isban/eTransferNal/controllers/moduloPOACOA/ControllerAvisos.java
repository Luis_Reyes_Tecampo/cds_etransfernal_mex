/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ContollerAvisos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:14 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOAvisos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportarAvisos;

/**
 * Class ContollerAvisos.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * avisos y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerAvisos extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2170953191004132473L;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "avisosTraspasosPC";
	
	/** La constante BEAN_AVISOS. */
	private static final String BEAN_AVISOS = "beanResAvisoTraspasos";
	
	/** La constante utilsExportar. */
	private static final UtileriasExportarAvisos utilsExportar = UtileriasExportarAvisos.getUtils();
	
	/** La variable que contiene informacion con respecto a: bo avisos. */
	private BOAvisos boAvisos;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * Consulta avisos traspasasos.
	 * 
	 * Muestra la pantalla con la informacion
	 * a la que hace referencia esta funcionbilidad.
	 *
	 * @param beanModulo El objeto: bean modulo
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanOrdenamiento El objeto: bean ordenamiento
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaAvisosTraspasos.do")
	public ModelAndView consultaAvisosTraspasasos(@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, BindingResult bindingResult) {
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Declaracion de los objetos de salida **/
		ModelAndView model = new ModelAndView(PAGINA_PRINCIPAL);
		BeanResAvisoTraspasos beanResAvisoTraspasos = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Consulta la session **/
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			/** Se realiza la peticion al BO **/
			beanResAvisoTraspasos = boAvisos.obtenerAvisos(beanPaginador, beanModulo,
					beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType(), sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion(),getArchitechBean());
			/** Seteo de datos la modelo **/
			model.addObject(BEAN_AVISOS, beanResAvisoTraspasos);
			model.addObject(Constantes.COD_ERROR, beanResAvisoTraspasos.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResAvisoTraspasos.getTipoError());
			model.addObject(Constantes.DESC_ERROR, beanResAvisoTraspasos.getMsgError());
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(ConstantesWebPOACOA.SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(ConstantesWebPOACOA.SORT_TYPE, beanOrdenamiento.getSortType());
			generarContadoresPaginacion(beanResAvisoTraspasos, model);
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;	
	}
	
	/**
	 * Exportar avisos.
	 *
	 * @param beanAvisos El objeto: bean avisos
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarAvisosTraspasos.do")
	public ModelAndView exportarAvisos(@Valid @ModelAttribute(BEAN_AVISOS) BeanResAvisoTraspasos beanAvisos, BindingResult bindingResult) {
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/** Se crea el model&view a retornar **/
		FormatCell formatCellBody = null;
		FormatCell formatCellHeader = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setFontName("Calibri");
		formatCellBody.setBold(false);
		formatCellHeader = new FormatCell();
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setBold(true);
		model = new ModelAndView(new ViewExcel());
		/** Seteo de datos la modelo **/
		model.addObject("FORMAT_HEADER", formatCellHeader);
		model.addObject("FORMAT_CELL", formatCellBody);
		model.addObject("HEADER_VALUE", utilsExportar.getHeaderExcel(ctx));
		model.addObject("BODY_VALUE", utilsExportar.getBody(beanAvisos.getListaConAviso()));
		model.addObject("NAME_SHEET", "AvisosTraspasos");
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Exportar todos avisos.
	 *
	 * @param beanModulo El objeto: bean modulo
	 * @param beanAvisos El objeto: bean avisos
	 * @param beanPaginador El objeto: bean paginador
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarTodoAvisosTraspasos.do")
	public ModelAndView exportarTodoAvisos(@Valid 
			@ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			@ModelAttribute(BEAN_AVISOS) BeanResAvisoTraspasos beanAvisos,BeanPaginador beanPaginador, BindingResult bindingResult) {
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/** Declaracion de los objetos de salida **/
		ModelAndView model = null; 
		BeanResAvisoTraspasos response = null;
		RequestContext ctx = new RequestContext(req);
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Consulta la session **/
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			/** Se realiza la peticion al BO **/
			response = boAvisos.exportarTodo(beanAvisos, beanModulo, null, getArchitechBean());
			model = new ModelAndView(PAGINA_PRINCIPAL);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());		
				/** Seteo de datos la modelo **/
				response.setListaConAviso(beanAvisos.getListaConAviso());
				response.setTotalReg(beanAvisos.getTotalReg());
				response.setBeanPaginador(beanPaginador);
				response.setImporteTotal(beanAvisos.getImporteTotal());
				model.addObject(BEAN_AVISOS, response);
				model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			/** Seteo de datos la modelo **/
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(ConstantesWebPOACOA.SORT_TYPE, "");
			model.addObject(ConstantesWebPOACOA.SORT_FIELD, "");
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			generarContadoresPaginacion(response, model);
		} catch (BusinessException e) {
	 		 showException(e);
	 	}
		/** Retorno del modelo **/
	    return model;
	}

	/**
	 * Generar contadores paginacion.
	 *
	 * @param beanResAviso El objeto: bean res aviso
	 * @param model El objeto: model
	 */
	protected void generarContadoresPaginacion(BeanResAvisoTraspasos beanResAviso, ModelAndView model) {
		if (beanResAviso.getTotalReg() > 0){
			Integer regInicio = 1;
			Integer regFinal = beanResAviso.getTotalReg();
			
			if (!"1".equals(beanResAviso.getBeanPaginador().getPagina())) {
				regInicio = 20 * (Integer.parseInt(beanResAviso.getBeanPaginador().getPagina()) - 1)+1;
			}
			if (beanResAviso.getTotalReg() >= 20 * Integer.parseInt(beanResAviso.getBeanPaginador().getPagina())){
				regFinal = 20 * Integer.parseInt(beanResAviso.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporteCon = BigDecimal.ZERO;
			for (BeanAvisoTraspasos beanAvisoTrasp : beanResAviso.getListaConAviso()){
				totalImporteCon = totalImporteCon.add(new BigDecimal(beanAvisoTrasp.getImporte()));
			}
			/** Seteo de datos la modelo actual**/
			model.addObject("importePagina", totalImporteCon.toString());
			model.addObject("regIni", regInicio);
			model.addObject("regFin", regFinal);
			
		}
	}
	
	/**
	 * Obtener el objeto: bo avisos.
	 *
	 * @return El objeto: bo avisos
	 */
	public BOAvisos getBoAvisos() {
		return boAvisos;
	}

	/**
	 * Definir el objeto: bo avisos.
	 *
	 * @param boAvisos El nuevo objeto: bo avisos
	 */
	public void setBoAvisos(BOAvisos boAvisos) {
		this.boAvisos = boAvisos;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
	
}
