/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerOrdenReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOOrdenReparacion;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Controlador para las ordenes por reparar
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerOrdenReparacion extends Architech {

	/**
	 * propiedad que mantiene el estado de un objeto a traves de las capas
	 */
	private static final long serialVersionUID = -1071881658409821989L;
	
	/**
	 * clasificacion de errores en properties
	 */
	private static final String COD_ERROR = "codError.";
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private static final String SORT_FIELD = "sortField";
	
	/**
	 * 
	 */
	private static final String SORT_TYPE = "sortType";
	
	/**
	 * propiedad ejb para realizar logica de negocio
	 */
	private BOOrdenReparacion bOOrdenReparacion;
	
	/**
	 * Metodo que muestra las ordenes de reparacion en estado PR
	 * @param beanPaginador objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/ordenesReparacion.do")
	public ModelAndView consultaOrdenesReparacion(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento){
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		ModelAndView model = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {			
			//Obtencion de bean resultado que sera enviado a la vista (JSP)
			beanResOrdenReparacion = bOOrdenReparacion.obtenerOrdenesReparacion(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());

			//Nombramiento y puesta de bean que sera enviado a la vista
			model = new ModelAndView("ordenesReparacion","beanResOrdenReparacion", beanResOrdenReparacion);
			
			String mensaje = messageSource.getMessage(COD_ERROR + beanResOrdenReparacion.getCodError(), null, null);
			
			//Mensajes de error, se envia tambien cuando es exitosa la operacion
			model.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			generarContadoresPaginacion(beanResOrdenReparacion, model,beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
		} catch (BusinessException e) {
			showException(e);
		}
		
		return model;
	}


	/**
	 * @param beanResOrdenReparacion Objeto del tipo BeanResOrdenReparacion
	 * @param model objeto del tipo ModelAndView
	 * @param sortField Objeto del tipo String
	 * @param sortType Objeto del tipo String
	 */
	protected void generarContadoresPaginacion(BeanResOrdenReparacion beanResOrdenReparacion, ModelAndView model, String sortField, String sortType) {
		if (beanResOrdenReparacion.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResOrdenReparacion.getTotalReg();
			
			if (!"1".equals(beanResOrdenReparacion.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResOrdenReparacion.getTotalReg() >= 20 * Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalPagina = BigDecimal.ZERO;
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanOrdenReparacion beanOrdenReparacion : beanResOrdenReparacion.getListaOrdenesReparacion()){
				totalPagina = totalPagina.add(new BigDecimal(beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono()));
			}
			
			try {
				totalImporte = bOOrdenReparacion.obtenerOrdenesReparacionMontos(getArchitechBean(), sortField, sortType);
			} catch (BusinessException e) {
				showException(e);
			}
			
			model.addObject("importePagina", totalPagina.toString());
			model.addObject("importeTotal", totalImporte.toString());
			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}
	
	/**
	 * Metodo envia una orden a repacion con estatus LI
	 * @param beanReqOrdeReparacion Objeto que tiene la orden para enviar a reparacion
	 * @return ModelAndView
	 */
	@RequestMapping(value="/enviarReparacion.do")
	public ModelAndView enviarOrdenesReparacion(BeanReqOrdeReparacion beanReqOrdeReparacion, BeanOrdenamiento beanOrdenamiento) {
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		ModelAndView model = new ModelAndView("ordenesReparacion");
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResOrdenReparacion = bOOrdenReparacion.enviarOrdenesReparacion(beanReqOrdeReparacion, getArchitechBean());
			model.addObject("beanResOrdenReparacion", beanResOrdenReparacion == null ? new BeanResOrdenReparacion():beanResOrdenReparacion);

			String mensaje = messageSource.getMessage(COD_ERROR + beanResOrdenReparacion.getCodError(), null, null);
			
	     	model.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
	     	model.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
	     	model.addObject(Constantes.DESC_ERROR, mensaje);
	     	model.addObject("sessionSPID", sessionSPID);
	     	generarContadoresPaginacion(beanResOrdenReparacion, model, beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
		} catch (BusinessException e) {
			showException(e);
		}		
		return model;
	}
	
	/**
	 * Metodo que exporta a excel las ordenes de reparacion
	 * @param beanPaginador Objeto para pagunacion de registros
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @param req Objeto que obtiene los mensajes de la aplicacion 
	 * @return {@link ModelAndView}
	 */
	@RequestMapping(value = "/exportarOrdenReparacion.do")
    public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, HttpServletRequest req){
		FormatCell formatCellHeaderOR = null;
		FormatCell formatCellBodyOR = null;
		ModelAndView model = null;
   	 	RequestContext ctx = new RequestContext(req);
  	 	BeanResOrdenReparacion beanResOrdenReparacion = null;
  	 	
  	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
  	 	
  	 	try {  	 	
  	 		beanPaginador.setAccion("ACT");
  	 		beanResOrdenReparacion = bOOrdenReparacion.obtenerOrdenesReparacion(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
    	   
  	 		if(Errores.OK00000V.equals(beanResOrdenReparacion.getCodError())||
 				Errores.ED00011V.equals(beanResOrdenReparacion.getCodError())){
  	 			formatCellBodyOR = new FormatCell();
  	 			formatCellBodyOR.setFontName("Calibri");
  	 			formatCellBodyOR.setFontSize((short)11);
  	 			formatCellBodyOR.setBold(false);
    	 	    
  	 			formatCellHeaderOR = new FormatCell();
  	 			formatCellHeaderOR.setFontName("Calibri");
  	 			formatCellHeaderOR.setFontSize((short)11);
  	 			formatCellHeaderOR.setBold(true);

  	 			model = new ModelAndView(new ViewExcel());
  	 			model.addObject("FORMAT_HEADER", formatCellHeaderOR);
  	 			model.addObject("FORMAT_CELL", formatCellBodyOR);
  	 			model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
  	 			model.addObject("BODY_VALUE", getBody(beanResOrdenReparacion.getListaOrdenesReparacion()));
  	 			model.addObject("NAME_SHEET", "OrdenReparacion");
  	 		}else{
  	 			model = consultaOrdenesReparacion(beanPaginador, beanOrdenamiento);
  	 			
  	 			String mensaje = messageSource.getMessage(COD_ERROR + beanResOrdenReparacion.getCodError(), null, null);
  	 			
  	 			model.addObject("codError", beanResOrdenReparacion.getCodError());
  	 			model.addObject("tipoError", beanResOrdenReparacion.getTipoError());
  	 			model.addObject("descError", mensaje);
  	 			model.addObject("sessionSPID", sessionSPID);
  	 			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
  				model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
  				generarContadoresPaginacion(beanResOrdenReparacion, model, beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
  	 		}
 		} catch (BusinessException e) {
 			showException(e);
	    }
	  
	    return model;
    }
    
	/**
	 * Metodo que exporta todas las ordenes de reparacion
	 * @param beanReqOrdeReparacion Objeto que contiene la informacion de exportacion de las ordenes
	 * @return {@link ModelAndView}
	 */
    @RequestMapping(value = "/exportarTodoOrdenReparacion.do")
    public ModelAndView expTodosBitacoraAdmon(BeanReqOrdeReparacion beanReqOrdeReparacion){
   	 	ModelAndView model = null; 

   	 	BeanResOrdenReparacion beanResOrdenReparacion = null;
   	 	
   	 BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
   	 	
   	 	try { 			
 			beanResOrdenReparacion = bOOrdenReparacion.exportarTodoOrdenesReparacion(beanReqOrdeReparacion, getArchitechBean()); 
 			model = new ModelAndView("ordenesReparacion");
 			
 			String mensaje = messageSource.getMessage("codError."+beanResOrdenReparacion.getCodError(),new Object[]{beanResOrdenReparacion.getNombreArchivo()}, null);
 			
			model.addObject("beanResOrdenReparacion", beanResOrdenReparacion);
	     	model.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
	     	model.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
	     	model.addObject(Constantes.DESC_ERROR, mensaje);
	     	model.addObject("sessionSPID", sessionSPID);
	     	generarContadoresPaginacion(beanResOrdenReparacion, model, "", "");
 	   } catch (BusinessException e) {
 		  showException(e);
 	   }
        return model;
    }
    
    /**
     * Metodo que genera las filas en el archivo Excel
     * @param listaBeanOrdenReparacion - Lista de Obejtos que contendra el archivo Excel
     * @return Lista de fialas para incluir en el Excel
     */
    private List<List<Object>> getBody(List<BeanOrdenReparacion> listaBeanOrdenReparacion){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	if(listaBeanOrdenReparacion!= null && 
		   !listaBeanOrdenReparacion.isEmpty()){
  		   objList = new ArrayList<List<Object>>();
  	
  		   for(BeanOrdenReparacion beanOrdenReparacion : listaBeanOrdenReparacion){ 
  	 		  objListParam = new ArrayList<Object>();
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveCola());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveTran());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveMecanismo());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveMedioEnt());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFechaCaptura());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeOrd());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeRec());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getEstatus());  	 		  	 		 
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFolioPaquete());
  	 		  objListParam.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFoloPago());
  	 		  objList.add(objListParam);
  	 	   }
  	   } 
   	return objList;
    }
    
    /**
     * Metodo que genera los Header que contendra el Excel
     * @param ctx - Objeto Request para obtener el nombre de las columnas
     * @return lista de nombres de Encabezados
     */
    private List<String> getHeaderExcel(RequestContext ctx){
   	 String []header = new String [] { 
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.refe"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.cola"),	
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.claveTran"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.macan"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.medioEnt"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.fchCap"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.ord"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.rec"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.importeAbono"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.est"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.folioPaquete"),
   			 ctx.getMessage("moduloSPID.recepcionOperacion.text.folioPago") 	  
   	 	   }; 
   	 	return Arrays.asList(header);
    }
	
    /**
     * Metodo que permite obtener el valor de EJB BOOrdenReparacion
     * @return Obejto BOOrdenReparacion
     */
	public BOOrdenReparacion getBOOrdenReparacion() {
		return bOOrdenReparacion;
	}

	/**
	 * Metodo que permite setear el valor del EJB por spring
	 * @param bOOrdenReparacion Objeto del tipo BOOrdenReparacion
	 */
	public void setBOOrdenReparacion(BOOrdenReparacion bOOrdenReparacion) {
		this.bOOrdenReparacion = bOOrdenReparacion;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
