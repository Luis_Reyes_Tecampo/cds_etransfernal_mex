/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonArchCanalesConting.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.contingencia;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.contingencia.BeanConsultaOperContingentes;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.contingencia.BOConsultaOperContingentes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOCargaArchivoEnvio;
import mx.isban.eTransferNal.utilerias.contingencias.UtilsContingenciasOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad del Monitor de carga de archivos.
 *
 * @author FSW-Vector
 * @since 18/10/2018
 */
@Controller
public class ControllerOperacionesContingentesArchivo extends Architech  {
	
	 /** Constante del Serial Version. */
		private static final long serialVersionUID = -102047098623676018L;
		
		/** Constante del tipo String que almacena el valor de LIST_CANALES. */
		private static final String LIST_CANALES = "listaCanales";
		
		
		/** BO para las operacions de carga de nombre de archivo. */
		private BOCargaArchivoEnvio boCargaArchivoEnvio;
		
		/** The bo consulta oper. */
		private BOConsultaOperContingentes boConsultaOper;
		
		/** La variable que contiene informacion con respecto a: util. */
		private UtilsContingenciasOperacion util = UtilsContingenciasOperacion.getInstance();
		
		/** Propiedad del tipo String que almacena el valor de BEAN_RES. */
		public static final String BEAN_RES = "beanResConsultaOperContingente";
		
		/** Constante con el nombre de la pagina. */
		private static final String PAG_NOMBRE_ARCH_PROCESAR ="operacionesContingentes";
		
		/** La constante DATO_FIJO. */
		public static final String DATO_FIJO = "NOMBRE_ARCH";
		
		/** La constante DATO_FIJO. */
		public static final String ERROR = "Error: ";
		
		/**
		 * Muestra nom arch procesar.
		 *
		 * @return the model and view
		 */
		@RequestMapping(value= "/muestraOperacionesContingentes.do")
		public ModelAndView muestraNomArchProcesar() {
			final Map<String, Object> model = new HashMap<String, Object>();
			BeanPaginador benaPaginador= new BeanPaginador();
			ModelAndView vista = null;
			/**Instancia paginador.*/
			benaPaginador.setPagina("");
			benaPaginador.setPaginaAnt("");
			benaPaginador.setPaginaFin("");
			benaPaginador.setPaginaIni("");
			benaPaginador.setPaginaSig("");
			benaPaginador.setPaginaIni("");
			try {
				BeanReqConsultaOperContingente bean = new BeanReqConsultaOperContingente();
				bean.setHistorico(util.obtenerHistorico("normal"));
				/**Inicia llamdo a consulta de Operaciones Contingentes.*/
				BeanResConsultaOperContingente beanResConsultaOperContingente = boConsultaOper.consultarOperContingentes(getArchitechBean(),benaPaginador,bean);
				beanResConsultaOperContingente.setBeanPaginador(benaPaginador);
		        /**Se asigna el nombre de la pantalla **/
				beanResConsultaOperContingente.setNomPantalla("normal");
				/**Se asigna el bean **/
				model.put(BEAN_RES, beanResConsultaOperContingente);
				/**Instancia del objeto de entrada .*/
				BeanResConsCanales respuesta = new BeanResConsCanales();	
				respuesta  = boConsultaOper.obtenerMedioEntrega(getArchitechBean());
				/**Se asigna la lista de canales**/
				model.put(LIST_CANALES,respuesta.getBeanResCanalList() );
				vista = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR, model);
				/**Se valida el codigo de error **/
				if(!Errores.OK00000V.equals(beanResConsultaOperContingente.getBeanbase().getCodError())){
					/**se asigna el mensaje**/
					vista.addObject(Constantes.COD_ERROR, beanResConsultaOperContingente.getBeanbase().getCodError());
					vista.addObject(Constantes.TIPO_ERROR,beanResConsultaOperContingente.getBeanbase().getTipoError());
					vista.addObject(Constantes.DESC_ERROR, beanResConsultaOperContingente.getBeanbase().getMsgError());
				}
			} catch (BusinessException e) {
				showException(e);
				error(ERROR +e.getMessage(), this.getClass());
			}
			
			/**Return model consulta de operaciones contingentes.*/
			return vista;
		}
		
		
		/**
		 * Carga archivo.
		 *
		 * @param req El objeto: req
		 * @param res El objeto: res
		 * @param bean El objeto: bean
		 * @return Objeto model and view
		 */
		@RequestMapping(value="/cargaArchivoOperaciones.do")
		public ModelAndView cargaArchivo(HttpServletRequest req,HttpServletResponse res,BeanReqConsultaOperContingente bean){
			bean.setHistorico(util.obtenerHistorico(bean.getNomPantalla()));
			/**Instancia modelandView.*/
			ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR);
			try {
				RequestContext ctx = new RequestContext(req);
				/**Instancia contiene respuesta del resultado de la carga de archivo.*/
				BeanResConsCanales beanResCanales = null;
				BeanResBase beanErrores = new BeanResBase(); 
				BeanPaginador benaPaginador= new BeanPaginador();
				/**Instancia de objeto del bean de entrada para la carga de archivos.*/
				BeanConsultaOperContingentes beanResArchivo = new BeanConsultaOperContingentes();
				/**Instancia del objeto para la respuesta de la consulta de operaciones contingentes.*/
				BeanResConsultaOperContingente beanConsultaOper = new BeanResConsultaOperContingente();
				String nombreArchivo = req.getParameter("archProc");
				nombreArchivo = obtieneNombre(nombreArchivo);
				beanResArchivo.setNombreArchivo(nombreArchivo);
				beanResArchivo.setCveMedioEnt(req.getParameter("cmbCanal"));
				String mensaje="";
				
				/**Instancia del objeto de entrada .*/
				BeanResConsCanales respuesta = new BeanResConsCanales();	
				respuesta  = boConsultaOper.obtenerMedioEntrega(getArchitechBean());
				
				/**Validacion de nombre de archivo a cargar.*/
				if(beanResArchivo.getNombreArchivo()!=null && !"".equals(beanResArchivo.getNombreArchivo())){
					/**Inicia llamado a la carga de archivo.*/
					beanResCanales  =  boConsultaOper.cargaArchivo(beanResArchivo, getArchitechBean(),bean);
					/**Se buscan los registro para actualizar la tabla **/
					beanConsultaOper = boConsultaOper.consultarOperContingentes(getArchitechBean(),benaPaginador,bean);
					beanConsultaOper.setNomPantalla(bean.getNomPantalla());
					/**Se asigna la lista de canales**/
					modelAndView.addObject(LIST_CANALES,respuesta.getBeanResCanalList() );
					modelAndView.addObject(BEAN_RES, beanConsultaOper);
					/**Seteo de codigo y mensaje de error.*/
					beanErrores.setCodError(beanResCanales.getBeanBaseError().getCodError());
					beanErrores.setMsgError(beanResCanales.getBeanBaseError().getMsgError());
					
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanErrores.getCodError());
					modelAndView.addObject(Constantes.COD_ERROR, beanErrores.getCodError());
					/**Validacion de codigo de errores.*/
					if (Errores.OK00000V.equals(beanErrores.getCodError())) {					
						modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
					} else {						
				     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
					}
					modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
					
				}else{
					/**llamado a consulta de operaciones y medios de entrega para volver a pintar la pantalla de incio.*/
					beanConsultaOper = boConsultaOper.consultarOperContingentes(getArchitechBean(),benaPaginador,bean);
					beanResCanales  = boConsultaOper.obtenerMedioEntrega(getArchitechBean());
					beanConsultaOper.setNomPantalla(bean.getNomPantalla());
					modelAndView.addObject(BEAN_RES, beanConsultaOper);
					/**Se asigna la lista de canales**/
					modelAndView.addObject(LIST_CANALES,respuesta.getBeanResCanalList() );
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00075V);
					modelAndView.addObject(Constantes.COD_ERROR, Errores.ED00075V);
			     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
				}
			} catch (BusinessException e) {
				showException(e);
				error(ERROR+e.getMessage(), this.getClass());
			}
		
			return modelAndView;
			
		}

		
		
		/**
		 * Actualiza estatus.
		 *
		 * @param req El objeto: req
		 * @param res El objeto: res
		 * @param bean El objeto: bean
		 * @return Objeto model and view
		 */
		@RequestMapping(value="/actualizaEstatus.do")
		public ModelAndView actualizaEstatus(HttpServletRequest req,HttpServletResponse res,BeanReqConsultaOperContingente bean){
			/**Se asigna el nombre del jsp**/
			ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR);
			String mensaje = "";
			try {
				
				/**Instancia contenedor de mensaje .*/
				RequestContext ctx = new RequestContext(req);			
				BeanResConsultaOperContingente beanResArchivo = new BeanResConsultaOperContingente();
				/**Instancia del objeto de entrada .*/
				BeanResConsCanales respuesta = new BeanResConsCanales();	
				/**se busca la lista de los canales**/
				respuesta  = boConsultaOper.obtenerMedioEntrega(getArchitechBean());
				/**se inicializan las variables**/
				BeanConsultaOperContingentes beanRes = new BeanConsultaOperContingentes();
				String nombreArchivo =""; 
				
				/**Validacion de nombre de archivo .*/
				if(bean.getListaBeanResConsOper()!=null){
					nombreArchivo = recorrerLista(bean);
				}
				
				
				
				BeanResConsCanales resActualizacion = new BeanResConsCanales();
				
				/**Se buscan los registro para actualizar la tabla **/
				beanRes.setNombreArchivo(nombreArchivo);	
				
				/**Inicia llamado para la actualización del estatus del archivo.*/
				resActualizacion = boConsultaOper.actualizaEstatus(beanRes, getArchitechBean(),bean);			
				/** se consulta todos los archivos contingentes**/
				beanResArchivo = boConsultaOper.consultarOperContingentes(getArchitechBean(),bean.getBeanPaginador(),bean);
				beanResArchivo.setHistorico(util.obtenerHistorico(bean.getNomPantalla()));
				beanResArchivo.setNomPantalla(bean.getNomPantalla());
				/**Se asgina el nombre del bean**/
				modelAndView.addObject(BEAN_RES, beanResArchivo);	
				/**Se asigna la lista de canales**/
				modelAndView.addObject(LIST_CANALES,respuesta.getBeanResCanalList() );			
				/**Se asignan los codigos de error **/
				modelAndView.addObject(Constantes.COD_ERROR,resActualizacion.getBeanBaseError().getTipoError());
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+resActualizacion.getBeanBaseError().getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);			
			    modelAndView.addObject(Constantes.TIPO_ERROR,resActualizacion.getBeanBaseError().getTipoError());		     	
				
			} catch (BusinessException e) {
				showException(e);
				error(ERROR +e.getMessage(), this.getClass());
			}
			return modelAndView;
			
		}
		
		
		/**
		 * Recorrer lista.
		 *
		 * @param bean El objeto: bean
		 * @return Objeto bean consulta oper contingentes
		 */
		private String recorrerLista(BeanReqConsultaOperContingente bean) {
			BeanConsultaOperContingentes beanRequest = null;
			String nombreArchivo="";
			/**se recorre la lista para obtener el seleccionado**/
			for(BeanConsultaOperContingentes archivo : bean.getListaBeanResConsOper()){
				if(archivo.isSeleccionado()){
					beanRequest =archivo;
				}
			}
			
			/**se obtiene el nombre **/
			if(beanRequest!=null) {
				nombreArchivo = obtieneNombre(beanRequest.getNombreArchivo());
			}
			
			return nombreArchivo;
		}
			
		
		
		/**
		 * Actualizar nombre.
		 *
		 * @param req El objeto: req
		 * @param res El objeto: res
		 * @param bean El objeto: bean
		 * @return Objeto model and view
		 */
		@RequestMapping(value = "/actualizarNombreArchivo.do")
		public ModelAndView actualizarNombre(HttpServletRequest req,
				HttpServletResponse res,BeanReqConsultaOperContingente bean) {
			/**Se asigna el nombre del jsp**/
			ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR);
			String mensaje = "";
			try {
				/**Instancia contenedor de mensaje .*/
				RequestContext ctx = new RequestContext(req);
				BeanResConsultaOperContingente beanResArchivo = new BeanResConsultaOperContingente();
				/**Instancia del objeto de entrada .*/
				BeanResConsCanales respuesta = new BeanResConsCanales();	
				/**se buscan la lista de los canales**/
				respuesta  = boConsultaOper.obtenerMedioEntrega(getArchitechBean());
				/**Instancia objeto para respuesta de peticion.*/
				BeanConsultaOperContingentes beanRes = new BeanConsultaOperContingentes();
				beanRes.setNombreArchivo(req.getParameter("nombreArchOculto"));
				beanRes.setCveMedioEnt(req.getParameter("cveMedEntregaOculto"));
				/**Inicia llamado para actualizar estatus y nombre del archivo ya existente .*/
				BeanResConsCanales beanResConsCanales  = boConsultaOper.actualizarNombreArchProcesar(beanRes,getArchitechBean(), bean.getNomPantalla());
				/**Se buscan los registro para actualizar la tabla **/
				beanResArchivo = boConsultaOper.consultarOperContingentes(getArchitechBean(),bean.getBeanPaginador(),bean);
				beanResArchivo.setHistorico(util.obtenerHistorico(bean.getNomPantalla()));
				beanResArchivo.setNomPantalla(bean.getNomPantalla());
				modelAndView.addObject(BEAN_RES, beanResArchivo);
				/**Se asigna la lista de canales**/
				modelAndView.addObject(LIST_CANALES,respuesta.getBeanResCanalList() );
				/**Validacion de codigo de error.*/
				if (Errores.OK00000V.equals(beanResConsCanales.getBeanBaseError().getCodError())) {
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsCanales.getBeanBaseError().getCodError());
					modelAndView.addObject(Constantes.COD_ERROR, beanResConsCanales.getBeanBaseError().getCodError());
			     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);				
				} else {
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsCanales.getBeanBaseError().getCodError(),new Object[]{beanRes.getNombreArchivo()});
					modelAndView.addObject(Constantes.COD_ERROR, beanResConsCanales.getBeanBaseError().getCodError());
			     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResConsCanales.getBeanBaseError().getTipoError());
			     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			     	modelAndView.addObject("archRespSustituirHide", beanRes.getNombreArchivo());	
				}
			} catch (BusinessException e) {
				showException(e);
				error(ERROR +e.getMessage(), this.getClass());
			}
			return modelAndView;

		}
		
		/**
		 * Metodo que sirve para obtener el nombre de la ruta.
		 *
		 * @param dir El objeto: dir
		 * @return String con el nombre
		 */
		private String obtieneNombre(String dir){
			String direccion = dir;
			if(direccion!= null ){
				int posx = direccion.lastIndexOf('\\');
				if( posx >= 0 && direccion.length() > posx ){
					direccion = direccion.substring(posx+1);
				}else{
					posx = direccion.lastIndexOf('/');
					if( posx >= 0 && direccion.length() > posx ){
						direccion = direccion.substring(posx+1);
					}				
				}
			}
			return direccion;
		}
		/**
		 * Gets the bo carga archivo envio.
		 *
		 * @return the bo carga archivo envio
		 */
		public BOCargaArchivoEnvio getBoCargaArchivoEnvio() {
			return boCargaArchivoEnvio;
		}
		
		/**
		 * Sets the bo carga archivo envio.
		 *
		 * @param boCargaArchivoEnvio the new bo carga archivo envio
		 */
		public void setBoCargaArchivoEnvio(BOCargaArchivoEnvio boCargaArchivoEnvio) {
			this.boCargaArchivoEnvio = boCargaArchivoEnvio;
		}



		/**
		 * Obtener el objeto: bo consulta oper.
		 *
		 * @return El objeto: bo consulta oper
		 */
		public BOConsultaOperContingentes getBoConsultaOper() {
			return boConsultaOper;
		}



		/**
		 * Definir el objeto: bo consulta oper.
		 *
		 * @param boConsultaOper El nuevo objeto: bo consulta oper
		 */
		public void setBoConsultaOper(BOConsultaOperContingentes boConsultaOper) {
			this.boConsultaOper = boConsultaOper;
		}

}
