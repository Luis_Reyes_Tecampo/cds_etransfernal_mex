/**
 * Clase de tipo controller para procesar la pantalla
 * de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.UtilsMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * The Class ControllerCatalogoParametros.
 */
@Controller
public class ControllerMantenimientoParametros extends Architech {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -5477123228477261487L;

	/** The bo mantenimiento parametros. */
	private BOMantenimientoParametros boMantenimientoParametros;

	/** The pagina. */
	private static final String PAGINA = "mantenimientoParametros";

	/** The bean response. */
	private static final String BEAN_RESPONSE = "beanResponse";
	
	/**
	 * Mostrar pantalla catalogo parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "muestraMantenimientoParametros.do")
	public ModelAndView mostrarPantallaMantenimientoAParametros(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		try{
			responseBO=boMantenimientoParametros.consultaOpcionesCombos(getArchitechBean());
			/**responseBO.setSelectedTrama("Seleccione");
			responseBO.setSelectCola("Seleccione");
			responseBO.setSelectMecan("Seleccione");*/
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE, responseBO);
		//metodo que genera la lista opciones para los combo define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		return modelAndView;
	}

	/**
	 * Consultar mantenimiento parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "consultarParametros.do")
	public ModelAndView consultarMantenimientoParametros(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParametros request=new BeanReqMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		
		//creacion de beanRequest de mantenimientoParametros
		request=utilsMantParam.requestToBeanReqMantenimientoParametros(req, modelAndView);
		try{
			//se consultan los parametros con los filtros de busqueda introducidos
			responseBO=boMantenimientoParametros.consultarParametros(getArchitechBean(), request);
			if( !Errores.OK00000V.equals(responseBO.getCodError()) ){
				modelAndView.addObject(Constantes.TIPO_ERROR, responseBO.getTipoError());
				modelAndView.addObject(Constantes.COD_ERROR, responseBO.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, responseBO.getMsgError());
			}
			//metodo que setea los filtro de busqueda para enviarlos en el response
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaPrincipal(request, responseBO);
		}catch(BusinessException e){
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			showException(e);
		}
		//metodo que genera la lista opciones para los combo define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		modelAndView.addObject(BEAN_RESPONSE, responseBO);
		return modelAndView;
	}

	/**
	 * Eliminar mantenimiento parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @param reqs2 the reqs2
	 * @return the model and view
	 */
	@RequestMapping(value = "eliminarParametros.do")
	public ModelAndView eliminarMantenimientoParametros(HttpServletRequest req, HttpServletResponse res){
		List<BeanMantenimientoParametros> listParametrosSeleccionados=new ArrayList<BeanMantenimientoParametros>();
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParametros request=new BeanReqMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA);
	    RequestContext ctx = new RequestContext(req);
		
		//creacion de beanRequest de mantenimientoParametros
		request=utilsMantParam.requestToBeanReqMantenimientoParametros(req, modelAndView);
		listParametrosSeleccionados=utilsMantParam.obtenerParametrosSeleccionados(request);
		try{
			//se eliminar los parametros seleccionados
			responseBO=boMantenimientoParametros.eliminarParametros(getArchitechBean(), request, listParametrosSeleccionados);

			//se genera y setea informacion de error
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+responseBO.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, responseBO.getTipoError());
			modelAndView.addObject(Constantes.COD_ERROR, responseBO.getCodError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			
			//metodo que setea los filtro de busqueda para enviarlos en el response
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaPrincipal(request, responseBO);
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE, responseBO);
		//metodo que genera la lista de opciones para los comobo define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		return modelAndView;
	}

	/**
	 * Export mantenimiento parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "exportarParametros.do")
	public ModelAndView exportMantenimientoParametros(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParametros request=new BeanReqMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA);

		//creacion de beanRequest de mantenimientoParametros
		request=utilsMantParam.requestToBeanReqMantenimientoParametros(req, modelAndView);

		RequestContext ctx = new RequestContext(req);
		//Se manda la consulta al bo
		try{
			//se consultan los parametros de la pagina actual
			responseBO = boMantenimientoParametros.consultarParametros(getArchitechBean(), request);
			//se consultaron correctament los parametros
			if ( Errores.OK00000V.equals(responseBO.getCodError()) ) {
				// se genera el modelAnView del reporte de excel
				modelAndView = new ModelAndView(new ViewExcel());
				modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
				modelAndView.addObject("BODY_VALUE",
						utilsMantParam.getBody(responseBO.getListaParametros()) );
				modelAndView.addObject("NAME_SHEET", "exportarParametros");
				return modelAndView;
			} else {
				//se obtienen los datos de error
				modelAndView.addObject(Constantes.TIPO_ERROR, responseBO.getTipoError());
				modelAndView.addObject(Constantes.COD_ERROR, responseBO.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, responseBO.getMsgError());
			}
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE, responseBO);
		//metodo que genera la lista de opciones para los comobo define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		return modelAndView;
	}

	/**
	 * Exportar todos mantenimiento parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "exportarTodoParametros.do")
	public ModelAndView ExportarTodoMantenimientoParametros(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParametros request=new BeanReqMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA);
	    RequestContext ctx = new RequestContext(req);

		//creacion de beanRequest de mantenimientoParametros
		request=utilsMantParam.requestToBeanReqMantenimientoParametros(req, modelAndView);

		try{
			//se realiza la insercion de query exportarTodo en bd
			responseBO = boMantenimientoParametros.exportarTodo(getArchitechBean(), request);

			//se genera y setea informacion de error
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+responseBO.getCodError()) + responseBO.getNombreArchivo();
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			modelAndView.addObject(Constantes.TIPO_ERROR, responseBO.getTipoError());
			modelAndView.addObject(Constantes.COD_ERROR, responseBO.getCodError());
			
			//metodo que setea los filtro de busqueda para enviarlos en el response
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaPrincipal(request, responseBO);
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE, responseBO);
		//metodo que genera la lista de opciones para los comobo define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		return modelAndView;
	}

	/**
	 * Sets the bo mantenimiento parametros.
	 *
	 * @param boMantenimientoParametros the new bo mantenimiento parametros
	 */
	public void setBoMantenimientoParametros(
			BOMantenimientoParametros boMantenimientoParametros) {
		this.boMantenimientoParametros = boMantenimientoParametros;
	}
	
	/**
	 * Gets the header excel.
	 *
	 * @param ctx the ctx
	 * @return the header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {
				// se obtiene los nombres de las columnas del reporte de excel
				ctx.getMessage("moduloCatalogo.mantenimientoParametros.label.claveParametro"),
				ctx.getMessage("moduloCatalogo.mantenimientoParametros.label.parametroFalta"),
				ctx.getMessage("moduloCatalogo.mantenimientoParametros.label.valorFalta"),
				ctx.getMessage("moduloCatalogo.mantenimientoParametros.label.tipoParametro"),
				ctx.getMessage("moduloCatalogo.mantenimientoParametros.label.descripcion")};
		return Arrays.asList(header);
	}
}