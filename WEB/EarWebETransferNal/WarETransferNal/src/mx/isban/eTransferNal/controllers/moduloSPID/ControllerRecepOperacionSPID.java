/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerRecepOperacionSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BORecepOperacionSPID;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerRecepOperacionSPID extends Architech{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1194869597038713887L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE_RECEPCION_OP = "recepcionOperacionSPID";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	private static final String BEAN_RES = "beanRes";
	/**
	 * Propiedad del tipo String que almacena el valor de TRIB
	 */
	private static final String TRIB = "TRIB"; 
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion
	 */
	private BORecepOperacionSPID bORecepOperacionSPID;
	 /**
	  * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraRecepOpSPID.do")
	 public ModelAndView muestraRecepOp() {
		 ModelAndView modelAndView = null;
		 BeanResReceptoresSPID beanResReceptoresSPID = bORecepOperacionSPID.consultaCveMiInstFchOp(getArchitechBean());
		 modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
		 return modelAndView;
	 }
	 
	/**
	 * Metodo que permite consultar las transferencias
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/consSPIDRecOp.do")
	public ModelAndView consTranSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;

		beanResReceptoresSPID = bORecepOperacionSPID.consultaTodasTransf(beanReqReceptoresSPID, getArchitechBean());
    	    debug("consTranSpeiRec bORecepOperacion.consTranSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(), null, null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	       return modelAndView;	    
	 }
	
	/**
	 * Metodo que permite hacer las transferencias
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/transferirSPIDRecOp.do")
	public ModelAndView transferirSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
		StringBuilder strBuilder = new StringBuilder();
		ModelAndView modelAndView = null;      
		String mensaje = null;
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.transferirSpeiRec(beanReqReceptoresSPID, getArchitechBean());
        modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
    	debug("transferirSpeiRec  bORecepOperacion.transferirSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
    	if(Errores.OK00001V.equals(beanResReceptoresSPID.getCodError())){
    	    for(BeanReceptoresSPID beanReceptoresSPID:beanResReceptoresSPID.getListReceptoresSPIDOK()){
    	    	if(beanReceptoresSPID.getCodError().contains("TRIB0000")){
           	        mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"OK00018V",
           	        		new Object[]{beanReceptoresSPID.getLlave().getFchOperacion(),beanReceptoresSPID.getLlave().getFolioPaquete()
           	        		,beanReceptoresSPID.getLlave().getFolioPago(), beanReceptoresSPID.getDatGenerales().getRefeTransfer(),beanReceptoresSPID.getEstatus().getEstatusTransfer()},null);
           	        strBuilder.append(mensaje+"  ");
           	    }
    	    }
    	    for(BeanReceptoresSPID beanReceptoresSPID:beanResReceptoresSPID.getListReceptoresSPIDNOK()){
    	    	if(beanReceptoresSPID.getCodError().contains(TRIB)||beanReceptoresSPID.getCodError().contains("TUBO")){
    	    		mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00087V",
    	    				new Object[]{beanReceptoresSPID.getLlave().getFchOperacion(),beanReceptoresSPID.getLlave().getFolioPaquete()
           	        		,beanReceptoresSPID.getLlave().getFolioPago(), beanReceptoresSPID.getDatGenerales().getRefeTransfer(),beanReceptoresSPID.getCodError()},null);
    	    		strBuilder.append(mensaje+"  ");
    	    		
           	        
    	    	}else if(Errores.ED00085V.equals(beanReceptoresSPID.getCodError())){
    	    		mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00085V,
    	    				new Object[]{beanReceptoresSPID.getBancoRec().getNumCuentaRec()},null);
    	    		strBuilder.append(mensaje+"  ");
    	    		
    	    	}else{
    	    		mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanReceptoresSPID.getCodError(),
    	    				new Object[]{beanReceptoresSPID.getLlave().getFchOperacion(),beanReceptoresSPID.getLlave().getFolioPaquete()
           	        		,beanReceptoresSPID.getLlave().getFolioPago(),beanReceptoresSPID.getBancoRec().getNumCuentaRec()},null);
    	    		strBuilder.append(mensaje+"  ");
    	    		
    	    	}
    	    }
    	    modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
    	}else{    	    	
    	       mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
    	       modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
    	       strBuilder.append(mensaje);    	       
    	}
	    
	    modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
     	modelAndView.addObject(Constantes.DESC_ERROR, strBuilder.toString());
	    return modelAndView;	    	    
	 }
	
	
	/**
	 * Metodo que permite rechazar las operaciones
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/rechazarSPIDRecOp.do")
	public ModelAndView rechazarTransfSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;

		beanResReceptoresSPID = bORecepOperacionSPID.rechazarTransfSpeiRec(beanReqReceptoresSPID, getArchitechBean());
    	    debug("consTranSpeiRec bORecepOperacion.consTranSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);

	       return modelAndView;	    
	 }
	/**
	 * Metodo que permite devolver las operaciones
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/devolverSPIDRecOp.do")
	public ModelAndView devolverTransfSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		String mensaje = null;
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.devolverTransferSpeiRec(beanReqReceptoresSPID, getArchitechBean());
    	debug("devolverTransfSpeiRec  bORecepOperacion.devolverTransferSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
	    if(beanResReceptoresSPID.getCodError().contains("TRIB0000")){
	        mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"OK00019V",new Object[]{beanResReceptoresSPID.getBeanReceptoresSPID().getDatGenerales().getRefeTransfer(),
	        		beanResReceptoresSPID.getBeanReceptoresSPID().getEstatus().getEstatusTransfer()},null);
	    }else if(beanResReceptoresSPID.getCodError().contains(TRIB)||beanResReceptoresSPID.getCodError().contains("TUBO")){
	    	mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"OK00019V",new Object[]{beanResReceptoresSPID.getBeanReceptoresSPID().getDatGenerales().getRefeTransfer(),
	    			beanResReceptoresSPID.getCodError()},null);
	    }else if("ED00090V".equals(beanResReceptoresSPID.getCodError())){
	    	mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00091V",null,null);
	    }else if("ED00103V".equals(beanResReceptoresSPID.getCodError())){
	    	BeanReceptoresSPID beanReceptoresSPID = beanResReceptoresSPID.getBeanReceptoresSPID(); 
	    	mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),
    				new Object[]{beanReceptoresSPID.getLlave().getFchOperacion(),beanReceptoresSPID.getLlave().getFolioPaquete()
   	        		,beanReceptoresSPID.getLlave().getFolioPago(),beanReceptoresSPID.getBancoRec().getNumCuentaRec()},null);
	    }else{    	    	
	        mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	    }
	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	    modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	    return modelAndView;	    	    	    	    
	 }
	/**
	 * Metodo que permite recuperar las operaciones
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/recuperarSPIDRecOp.do")
	public ModelAndView recuperarTransfSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.recuperarTransfSpeiRec(beanReqReceptoresSPID, getArchitechBean());
    	    debug("recuperarTransfSpeiRec bORecepOperacion.recuperarTransfSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	       return modelAndView;	    
	 }
	/**
	 * Metodo que permite actualizar el motivod de rechazo
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/actMotRechazoSPIDRecOp.do")
	public ModelAndView actMotRechazo(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.actMotRechazo(beanReqReceptoresSPID, getArchitechBean());
    	    debug("recuperarTransfSpeiRec bORecepOperacion.actMotRechazo Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	       return modelAndView;	    
	 }

	/**
	 * Metodo que permite actualizar el motivod de rechazo
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/apartarSPIDRecOp.do")
	public ModelAndView apartar(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.apartar(beanReqReceptoresSPID, getArchitechBean());
    	    debug("recuperarTransfSpeiRec bORecepOperacionSPID.apartar Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	       return modelAndView;	    
	 }
	
	/**
	 * Metodo que permite actualizar el motivod de rechazo
	 * @param beanReqReceptoresSPID Objeto con los datos de la peticion 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/desapartarSPIDRecOp.do")
	public ModelAndView bloquear(BeanReqReceptoresSPID beanReqReceptoresSPID){
		ModelAndView modelAndView = null;      
		BeanResReceptoresSPID beanResReceptoresSPID = null;
		beanResReceptoresSPID = bORecepOperacionSPID.desApartar(beanReqReceptoresSPID, getArchitechBean());
    	    debug("recuperarTransfSpeiRec bORecepOperacionSPID.actMotRechazo Codigo de error:"+beanResReceptoresSPID.getCodError());
    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
	        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(),null,null);
	     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	       return modelAndView;	    
	 }


	/**
	 * Metodo get que sirve para obtener la propiedad bORecepOperacionSPID
	 * @return bORecepOperacionSPID Objeto del tipo BORecepOperacionSPID
	 */
	public BORecepOperacionSPID getBORecepOperacionSPID() {
		return bORecepOperacionSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bORecepOperacionSPID
	 * @param recepOperacionSPID del tipo BORecepOperacionSPID
	 */
	public void setBORecepOperacionSPID(BORecepOperacionSPID recepOperacionSPID) {
		bORecepOperacionSPID = recepOperacionSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	


	
	

}
