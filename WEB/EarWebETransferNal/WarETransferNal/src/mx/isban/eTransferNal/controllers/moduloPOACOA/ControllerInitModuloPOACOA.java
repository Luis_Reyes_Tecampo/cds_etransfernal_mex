/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerInitModuloPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   22/09/2015     INDRA FSW      ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloPOACOA;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del monitor
**/
@Controller
public class ControllerInitModuloPOACOA extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	
	/**
	 * Metodo inicial del modulo CDA
	 * @return ModelAndView Objetode ModelAndView de Spring
	 */
	@RequestMapping(value="moduloPOACOAInit.do")
	public ModelAndView inicio(){
		ModelAndView model = new ModelAndView("inicio");
		/** Se setea el tipo de modulo **/
		model.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_ACTUAL);
		return model;
	}
	
	/**
	 * Inicio spid.
	 *
	 * @return Objeto model and view
	 */
	@RequestMapping(value="moduloPOACOASPIDInit.do")
	public ModelAndView inicioSpid(){
			ModelAndView model =  new ModelAndView("inicio");
			/** Se setea el tipo de modulo **/
			model.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_SPID);
			return model;
	}
	
}
