/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerNombreArchProcesar.java
 * 
 * La clase ha sido modificada.
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 06:22:41 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqArchPago;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanales;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOCargaArchivoEnvio;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion  de los parametros.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
@Controller
public class ControllerNombreArchProcesar extends Architech{

	 /** Constante del Serial Version. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Constante del tipo String que almacena el valor de LIST_CANALES. */
	private static final String LIST_CANALES = "listaCanales";
	
	/** Constante con el nombre de la pagina. */
	private static final String PAG_NOMBRE_ARCH_PROCESAR ="cargaArchEnvio";
	
	
	/** BO para las operacions de carga de nombre de archivo. */
	private BOCargaArchivoEnvio boCargaArchivoEnvio;
	
	/**
	 * Metodo que muestra la pantalla de parametros del POA/COA.
	 *
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="muestraNomArchProcesar.do")
	public ModelAndView muestraNomArchProcesar() {
		final Map<String, Object> model = new HashMap<String, Object>();
		/** Se realiza la peticion al BO **/
		BeanResConsCanales beanResConsCanales  = boCargaArchivoEnvio.obtenerMediosEntrega(getArchitechBean());
		model.put(LIST_CANALES,beanResConsCanales.getBeanResCanalList());
		/** Retorno del metodo **/
		return new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR, model);
	}
	
	/**
	 * Cargar arch resp.
	 *
	 * metodo para la carga de archvivos 
	 * 
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="cargarArchProc.do")
	public ModelAndView cargarArchResp() {
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/** Declaracion de las variables a ocupar por el flujo **/
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanResConsCanales respuestaConsCanales = null;
		ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR); 
		BeanReqArchPago beanReqArchPago = new BeanReqArchPago();
		String nomArchivo = req.getParameter("archProc");
		nomArchivo = obtieneNombre(nomArchivo);
		beanReqArchPago.setNombreArchivo(nomArchivo);
		beanReqArchPago.setCveMedioEntrega(req.getParameter("cmbCanal"));		
			if(beanReqArchPago.getNombreArchivo()!=null && !"".equals(beanReqArchPago.getNombreArchivo())){
				/** Se realiza la peticion al BO **/
				respuestaConsCanales  =  boCargaArchivoEnvio.agregarNombreArchProcesar(
						beanReqArchPago, getArchitechBean());
				modelAndView.addObject(LIST_CANALES, respuestaConsCanales.getBeanResCanalList());
				/** Se hacen las validaciones de la respuesta y se setean al model **/
				if (Errores.OK00000V.equals(respuestaConsCanales.getCodError())) {					
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+respuestaConsCanales.getCodError());
					modelAndView.addObject(Constantes.COD_ERROR, respuestaConsCanales.getCodError());
			     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);

				} else {
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+respuestaConsCanales.getCodError(),new Object[]{beanReqArchPago.getNombreArchivo()});
					modelAndView.addObject(Constantes.COD_ERROR, respuestaConsCanales.getCodError());
			     	modelAndView.addObject(Constantes.TIPO_ERROR, respuestaConsCanales.getTipoError());
			     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			     	modelAndView.addObject("archRespSustituirHide", beanReqArchPago.getNombreArchivo());
			     	modelAndView.addObject("cveMedioEntregaHide",beanReqArchPago.getCveMedioEntrega());
				}
			}else{
				/** Se realiza la peticion al BO **/
				respuestaConsCanales  = boCargaArchivoEnvio.obtenerMediosEntrega(getArchitechBean());
				/** Seteo de valores al modelo **/
				modelAndView.addObject(LIST_CANALES,respuestaConsCanales.getBeanResCanalList() );
				/** Seteo de datos al model **/
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00075V);
				modelAndView.addObject(Constantes.COD_ERROR, Errores.ED00075V);
		     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	
	
	/**
	 * Metodo que sirve para obtener el nombre de la ruta para el archivo a procesar.
	 * @param dir String con la direccion
	 * @return String con el nombre
	 */
	private String obtieneNombre(String dir){
		/** Se declara la variable de tipo String*/
		String dirNuevoNom = dir;
		/** Se valida la cadena*/
		if(dirNuevoNom!= null ){
			int posx = dirNuevoNom.lastIndexOf('\\');
			if( posx >= 0 && dirNuevoNom.length() > posx ){
				dirNuevoNom = dirNuevoNom.substring(posx+1);
			}else{
				posx = dirNuevoNom.lastIndexOf('/');
				if( posx >= 0 && dirNuevoNom.length() > posx ){
					dirNuevoNom = dirNuevoNom.substring(posx+1);
				}				
			}
		}
		/** Retorno del metodo **/
		return dirNuevoNom;
	}
	
	/**
	 * Actualizar nombre.
	 * actualiza el nombre del archivo 
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value = "actualizarArchProc.do")
	public ModelAndView actualizarNombre() {
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		ModelAndView modelAndViewActNom = new ModelAndView(PAG_NOMBRE_ARCH_PROCESAR);
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanReqArchPago beanReqArchPago = new BeanReqArchPago();
		beanReqArchPago.setNombreArchivo(req.getParameter("nombreArchOculto"));
		beanReqArchPago.setCveMedioEntrega(req.getParameter("cveMedEntregaOculto"));
		/** Se realiza la peticion al BO **/
		BeanResConsCanales beanResConsCanales  = boCargaArchivoEnvio
				.actualizarNombreArchProcesar(beanReqArchPago,getArchitechBean());
		/** Seteo de valores al modelo **/
		modelAndViewActNom.addObject(LIST_CANALES, beanResConsCanales.getBeanResCanalList());
		/** Se hacen las validaciones de la respuesta y se setean al model **/
		if (Errores.OK00000V.equals(beanResConsCanales.getCodError())) {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsCanales.getCodError());
			modelAndViewActNom.addObject(Constantes.COD_ERROR, beanResConsCanales.getCodError());
			modelAndViewActNom.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			modelAndViewActNom.addObject(Constantes.DESC_ERROR, mensaje);				
		} else {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsCanales.getCodError(),new Object[]{beanReqArchPago.getNombreArchivo()});
			modelAndViewActNom.addObject(Constantes.COD_ERROR, beanResConsCanales.getCodError());
			modelAndViewActNom.addObject(Constantes.TIPO_ERROR, beanResConsCanales.getTipoError());
			modelAndViewActNom.addObject(Constantes.DESC_ERROR, mensaje);
			modelAndViewActNom.addObject("archRespSustituirHide", beanReqArchPago.getNombreArchivo());	
		}
		/** Retorno del metodo **/
		return modelAndViewActNom;

	}

	/**
	 * Devuelve el valor de boCargaArchivoEnvio.
	 *
	 * @return the boCargaArchivoEnvio
	 */
	public BOCargaArchivoEnvio getBoCargaArchivoEnvio() {
		return boCargaArchivoEnvio;
	}

	/**
	 * Modifica el valor de boCargaArchivoEnvio.
	 *
	 * @param boCargaArchivoEnvio the boCargaArchivoEnvio to set
	 */
	public void setBoCargaArchivoEnvio(BOCargaArchivoEnvio boCargaArchivoEnvio) {
		this.boCargaArchivoEnvio = boCargaArchivoEnvio;
	}
}
