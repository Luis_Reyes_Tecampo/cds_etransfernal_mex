/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerGraficoSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.pagoInteres;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGrafico;
import mx.isban.eTransferNal.servicio.pagoInteres.BOGraficoSPID;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion de la pantalla de grafico SPID
**/
@Controller
public class ControllerGraficoSPID extends Architech{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2272477377374757239L;

	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE = "graficoSPID";
	/**
	 * Propiedad del tipo BOGraficoSPID que almacena el valor de bOGraficoSPID
	 */
	private BOGraficoSPID bOGraficoSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	 /**
	  * Metodo que se utiliza para mostrar la pagina de monitor de operaciones SPID
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraGraficoSPID.do")
	 public ModelAndView muestraGraficoSPID(HttpServletRequest req) {
		 ModelAndView modelAndView = new ModelAndView(PAGE);
		 RequestContext ctx = new RequestContext(req);
		 BeanResGrafico beanResGrafico = bOGraficoSPID.consultaCatalogos(getArchitechBean());
		 String horas[] = new String[]{"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"};
		 String minutos[] = new String[]{"00","01","02","03","04","05","06","07","08","09",
				 						  "10","11","12","13","14","15","16","17","18","19",
				 						  "20","21","22","23","24","25","26","27","28","29",
				 						 "30","31","32","33","34","35","36","37","38","39",
				 						  "40","41","42","43","44","45","46","47","48","49",
				 						  "50","51","52","53","54","55","56","57","58","59"
		 };
		 modelAndView.addObject("LEYENDA_HORA", ctx.getMessage("pagoInteres.graficoSPID.busqueda.text.rangoTiempo", new Object[]{beanResGrafico.getRango()}));
		 modelAndView.addObject("resBean", beanResGrafico);
		 modelAndView.addObject("horas", horas);
		 modelAndView.addObject("minutos", minutos);
		 return modelAndView;
	 }

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOGraficoSPID
	 * @return bOGraficoSPID Objeto del tipo BOGraficoSPID
	 */
	public BOGraficoSPID getBOGraficoSPID() {
		return bOGraficoSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOGraficoSPID
	 * @param graficoSPID del tipo BOGraficoSPID
	 */
	public void setBOGraficoSPID(BOGraficoSPID graficoSPID) {
		bOGraficoSPID = graficoSPID;
	}

}
