/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaNominaCutDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/11/2016 12:42:00 IDS		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ConfiguracionException;
import mx.isban.agave.sesiones.Sesiones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;
import mx.isban.eTransferNal.utilerias.capturaCentral.Utiluuid;
import mx.isban.eTransferNal.utilerias.UtilsCapturaCentral;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Class ControllerCapturaCentral.
 *
 * @author FSW-Vector
 * @since 29/03/2017
 */
@Controller
public class ControllerCapturaCentral extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1215371993784593044L;
	
	/** La constante PAGINA. */
	private static final String PAGINA = "consultaCapturaCentral";
	
	/** La constante BEAN. */
	private static final String BEAN = "beanRes";
	
	/** La constante BEAN. */
	private static final String BEAN_OPER = "bean";
	
	/** La constante ERROR. */
	private static final String ERROR = "error";
	
	/** La constante BEAN. */
	private static final String LISTA = "lista";
	
	/** La constante TEMPLATE. */
	private static final String TEMPLATE = "template";
	
	/** La constante ERRORES_PUNTO. */
	private static final String ERRORES_PUNTO = "moduloCapturasManuales.capturaCentral.error.";
	
	/** La constante ERROR_FALTA. */
	private static final String ERROR_FALTA = "CD00041V";
	
	/** La constante ERMCC003. Faltan datos Obligarorios */
	private static final String ERMCC003 = "ERMCC003";
	
	/** La constante ERMCC003. ERROR TIPOS DE DATO */
	private static final String ERMCC004 = "ERMCC004";
	
	/** La constante OKMCC000. Alta de operacion OK */
	private static final String OKMCC000 = "OKMCC000";
	
	/**  La constante CONSULTA. */
	private static final String CONSULTA = "consulta";
	
	/** La variable que contiene informacion con respecto a: bo captura central. */
	private BOCapturaCentral boCapturaCentral;

	/** La variable que contiene informacion con respecto a: utiluuid. */
	private static Utiluuid utiluuid = Utiluuid.getInstancia();
	/**
	 * Muestra captura central.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/muestraCapturaCentral.do")
	ModelAndView muestraCapturaCentral(HttpServletRequest req, HttpServletResponse res){
		//Utils
		UtilsCapturaCentral utilsCapturaCentral = new UtilsCapturaCentral();
		Map<String,Object> map = new HashMap<String,Object>();
		BeanCapturaCentralResponse beanRes = new BeanCapturaCentralResponse();
		beanRes.setStep(CONSULTA);
		RequestContext ctx = new RequestContext(req);
		//Nuevo objeto modelo
		ModelAndView modelAndView = null;
		//Agrega el jsp
		modelAndView = new ModelAndView(PAGINA);
		//Se consulta los datos al BO
		BeanCapturaCentralRequest request = new BeanCapturaCentralRequest();
		try{
			beanRes = boCapturaCentral.consultaTodosTemplates(request, getArchitechBean());
			beanRes.setTemplate("Seleccione");
			beanRes.getListTemplates().add(0, "Seleccione");
			if(!Errores.OK00000V.equals(beanRes.getCodError())){
				utilsCapturaCentral.agregarError(modelAndView, beanRes.getCodError(),beanRes.getTipoError(),ctx);
			}
		}catch(BusinessException e){
			showException(e);
			//En caso de un error se setea en la respuesta
			utilsCapturaCentral.agregarError(modelAndView, e.getCode(),Errores.TIPO_MSJ_ERROR,ctx);
		}
		//Se agrega bean a map
		beanRes.setStep(CONSULTA);
		map = modelAndView.getModel();
		map.put(BEAN, beanRes);
		//regresa el modelo
		return modelAndView;
	}	
	
	/**
	 * Consulta template.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaTemplate.do")
	ModelAndView consultaTemplate(HttpServletRequest req, HttpServletResponse res){
		//Utils
		UtilsCapturaCentral utilsCapturaCentral = new UtilsCapturaCentral();
		//Nuevo objeto modelo
		Map<String,Object> map = new HashMap<String,Object>();
		ModelAndView modelAndView = null;
		modelAndView = new ModelAndView(PAGINA);
		RequestContext ctx = new RequestContext(req);
		BeanCapturaCentralResponse beanRes = new BeanCapturaCentralResponse();
		BeanCapturaCentralRequest request = requestToBeanCapturaCentralRequest(req);
		request.setStep(CONSULTA);
		//Se pone el step de consulta
		debug("Step: " + request.getStep());
		
		if (StringUtils.EMPTY.equals(request.getTemplate())){
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + ERROR_FALTA);
	    	modelAndView.addObject(Constantes.COD_ERROR_PUNTO, ERROR_FALTA);
	    	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	    	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	    	return modelAndView;
		}
		try{
			beanRes = boCapturaCentral.consultaTemplate(request, getArchitechBean());
			if(!Errores.OK00000V.equals(beanRes.getCodError())){
				utilsCapturaCentral.agregarError(modelAndView, beanRes.getCodError(),beanRes.getTipoError(),ctx);
			}else{
				utilsCapturaCentral.agregarDatosDefault(beanRes,getArchitechBean().getUsuario(),getArchitechBean().getIPCliente());
			}
		}catch(BusinessException e){
			showException(e);
			//En caso de un error se setea en la respuesta
			utilsCapturaCentral.agregarError(modelAndView, e.getCode(),Errores.TIPO_MSJ_ERROR,ctx);
		}
		utilsCapturaCentral.anadirDatosDeEntrada(request, beanRes);
		map = modelAndView.getModel();
		map.put(BEAN, beanRes);
		//regresa el modelo
		return modelAndView;
	}
	
	/**
	 * Alta captura manual spid.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/altaCapturaManualSPID.do")
	ModelAndView altaCapturaManualSPID(HttpServletRequest req, HttpServletResponse res) {
		//Utils
		UtilsCapturaCentral utilsCapturaCentral = new UtilsCapturaCentral();
		//Nuevo objeto modelo
		Map<String,Object> map = new HashMap<String,Object>();
		//Nuevo objeto para procesar los datos de la peticion de alta	
		Map<String, Object> objetos = new HashMap<String, Object>();
		ModelAndView modelAndView = null;
		modelAndView = new ModelAndView(PAGINA);
		RequestContext ctx = new RequestContext(req);
		//Consulta la info del template
		BeanCapturaCentralRequest request = requestToBeanCapturaCentralRequest(req);
		request.setStep("alta");
		try{
			BeanCapturaCentralResponse beanRes = boCapturaCentral.consultaTemplate(request, getArchitechBean());
			utilsCapturaCentral.anadirDatosDeEntrada(request, beanRes);
			//Valida que esten los datos obligatorios
			List<String> obligatorios = utilsCapturaCentral.getObligatorios(beanRes);
			//Mapea el bean  para el alta de la operacion
			objetos = utilsCapturaCentral.mapeaBeanOperacion(req, obligatorios, beanRes);
			utilsCapturaCentral.agregarDatosDefault(beanRes,getArchitechBean().getUsuario(),getArchitechBean().getIPCliente());
			if (objetos.containsKey(ERROR)) {
				beanRes.setFolio(null);
				String mensaje = ctx.getMessage(ERRORES_PUNTO + ERMCC004, Arrays.asList(objetos.get(ERROR)));
				modelAndView.addObject(Constantes.COD_ERROR, ERMCC004);
				modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
				modelAndView.addObject(BEAN, beanRes);
				return modelAndView;
			}
			BeanOperacion operacion = (BeanOperacion) objetos.get(BEAN_OPER);
			obligatorios = (List<String>) objetos.get(LISTA);
			//Faltan datos obligatorios
			if (!obligatorios.isEmpty()) {
				beanRes.setFolio(null);
				String mensaje = ctx.getMessage(ERRORES_PUNTO + ERMCC003);
				modelAndView.addObject(Constantes.COD_ERROR, ERMCC003);
				modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
				modelAndView.addObject(BEAN, beanRes);
				return modelAndView;
			}
			operacion.setIdFolio(beanRes.getFolio());
			operacion.setIdTemplate(req.getParameter(TEMPLATE));
			operacion.setIdLayout(beanRes.getIdLayout());
			info("Bean: " + ReflectionToStringBuilder.toString(operacion));
			BeanResBase reponse =
					boCapturaCentral.altaCapturaManualSPID(operacion, getArchitechBean());
			//Valida errores en caso de exitir y los muestra al usuario 
			if (reponse.getCodError().equals(OKMCC000)){
				String mensaje = ctx.getMessage(ERRORES_PUNTO + OKMCC000);
				modelAndView.addObject(Constantes.COD_ERROR, OKMCC000);
				modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				//Se agrega mensaje de error
				String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + reponse.getCodError());
				modelAndView.addObject(Constantes.COD_ERROR, reponse.getCodError());
				modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			//Agrega la informacion de la operacion al modelo
			map = modelAndView.getModel();
			map.put(BEAN, beanRes);
		}catch(BusinessException e){
			showException(e);
			//Se agrega el error en caso de que se genere una excepcion
			utilsCapturaCentral.agregarError(modelAndView, e.getCode(),Errores.TIPO_MSJ_ERROR,ctx);
		}
		//Regresa el modelo
		return modelAndView;

	}

    /**
     * Request to bean.
     *
     * @param req the req
     * @param res the res
     * @return the bean req cons operaciones
     */
	private BeanCapturaCentralRequest requestToBeanCapturaCentralRequest(HttpServletRequest req){
        //se hace el mapeo del request al bean
        BeanCapturaCentralRequest beanCapturaCentralRequest = new BeanCapturaCentralRequest();
        List<String> listTemplates = new ArrayList<String>();
        List<String> listLayouts = new ArrayList<String>();
        UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countListTemplates"), listTemplates, "");
        beanCapturaCentralRequest.setListTemplates(listTemplates);
        UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countListLayouts"), listLayouts, "");
        beanCapturaCentralRequest.setListLayouts(listLayouts);
        UtilsMapeaRequest.getMapper().mapearObject(beanCapturaCentralRequest, req.getParameterMap());
        return beanCapturaCentralRequest;
    }

	/**
	 * Definir el objeto: bo captura central.
	 *
	 * @param boCapturaCentral El nuevo objeto: bo captura central
	 */
	public void setBoCapturaCentral(BOCapturaCentral boCapturaCentral) {
		this.boCapturaCentral = boCapturaCentral;
	}
	
	/**
	 * Muestra captura central.
	 * M/023776 Capturas Manuales - [VSF] Refrescar el tiempo de sesion
	 * 
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
    @RequestMapping(value = "refreshSession.do")
	public void refreshSession(HttpServletRequest req){
		info("[Capturas Manuales] - Se realiza el Refresh de la Sesion");
		Sesiones sesiones = Sesiones.getInstance();
		try {
			info("[Capturas Manuales] - TimeOut Establecido: " + sesiones.getTime());
		} catch (ConfiguracionException e) {
			error("[Capturas Manuales] - Error al obtener el parametro TimeOut: " + e.getMessage());
			showException(e);
		}
    }
    
    /**
	 **
     * Generar UETR.
     **
     * @param req El objeto: req
     * @param res El objeto: res
     * @return Objeto string
     */
    @RequestMapping(value = "generaUETR.do", method=RequestMethod.POST)
    @ResponseBody
    public String generarUETR(HttpServletRequest req, HttpServletResponse res) {
    	/** Se declara la  variable que gaurdara el valor del codigo**/
    	String codigo = "";
    	/** Se declara el parametro necesario para  hacer la  invocacion del metodo**/
    	Map<String, String> operacion = new HashMap<String, String>();
    	/** Se invoca al metodo que obtendra el valor**/
    	utiluuid.seteaUUID(operacion);
    	/** Se extrae el valor del objeto**/
    	codigo = operacion.get("UETR");
    	/** Retorno del metodo**/
    	return codigo;
    }
}
