/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerRecepOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018      SMEZA		VECTOR      Creacion
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecComun;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOperaRecuperaMotivo;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de Admon Saldos -
 * Recepcion de Operaciones.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepOperacion extends Architech {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;

	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();

	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BORecepOperacion bORecepOperacion;

	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperaRecuperaMotbORecepOperaRecuperaMotivoivo. */
	private BORecepOperaRecuperaMotivo bORecepOperaRecuperaMotivo;

	/** Constante con la cadena sessionBean. */
	private static final String SESSION = "sessionBean";

	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modeloVista;

	/** Propiedad del tipo ResourceBundleMessageSource que almacena el valor de messageSource. */
	private ResourceBundleMessageSource messageSource;

	/**
	 * Muestra rmuestra recep op htoecep op.
	 *
	 * @param res         El objeto: res
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/muestraRecepOpHto.do")
	public ModelAndView muestraRmuestraRecepOpHtoecepOp(HttpServletResponse res,
			@RequestParam("nomPantalla") String nomPantalla) {
		modeloVista = null;
		String mensaje = "";
		// incializa objeto
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		// inicializa bean comun
		recepOperaAdmonSaldos.setBeanComun(new BeanTranSpeiRecComun());
		recepOperaAdmonSaldos.getBeanComun().setNombrePantalla(nomPantalla);
		modeloVista = new ModelAndView(nomPantalla, ConstantesRecepOpePantalla.BEAN_RES, recepOperaAdmonSaldos);
		BeanReqTranSpeiRec beanReqConsTranSpei = new BeanReqTranSpeiRec();
		beanReqConsTranSpei.setBeanComun(recepOperaAdmonSaldos.getBeanComun());
		boolean historico = true;
		try {
			BeanTranSpeiRecOper beanTranSpeiRecOper = new BeanTranSpeiRecOper();
			//Consulta las listas para los filtros
			recepOperaAdmonSaldos = bORecepOperacion.consultaListas(recepOperaAdmonSaldos, beanReqConsTranSpei,
					getArchitechBean(), historico, beanTranSpeiRecOper);
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
		} catch (BusinessException e) {
			showException(e);
			//Vista con error
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(
					Constantes.COD_ERROR_PUNTO + recepOperaAdmonSaldos.getBeanError().getCodError(), null, null);
			modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage(), this.getClass());
		}
		ArchitechSessionBean sessionBean = getArchitechBean();
		modeloVista.addObject(SESSION, sessionBean);
		return modeloVista;

	}

	/**
	 * Metodo que realiza la busqueda de las recepciones apartadas.
	 *
	 * @param beanResTranSpeiRec the bean res tran spei rec
	 * @param beanReqConsTranSpei Objeto con los datos de la peticion
	 * @param res                 Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @param nomPantalla         El objeto: nom pantalla
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consTranSpeiRec.do")
	public ModelAndView consTranSpeiRec(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpei,
			HttpServletResponse res,
			@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper,
			@RequestParam("nomPantalla") String nomPantalla) {
		modeloVista = null;
		String fechaOperacion = null;
		// inicializa bean comun
		if (beanReqConsTranSpei.getBeanComun() != null) {
			fechaOperacion = beanReqConsTranSpei.getBeanComun().getFechaOperacion();
		}
		if (nomPantalla.length() > 0) {
			beanReqConsTranSpei.setBeanComun(new BeanTranSpeiRecComun());
			beanReqConsTranSpei.getBeanComun().setNombrePantalla(nomPantalla);
			beanReqConsTranSpei.getBeanComun().setFechaOperacion(fechaOperacion);
		}
		BeanResTranSpeiRec recepOperaAdmonSaldos = beanResTranSpeiRec;
		/** se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		String mensaje = "";
		SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
		Date dia = new Date();
		String fechEntrante = beanTranSpeiRecOper.getBeanDetalle().getDetalleGral().getFchOperacion();
		try {
			/** se valida si existe una fecha de operación para filtrado */
			if (beanTranSpeiRecOper.getBeanDetalle().getDetalleGral() != null && fechEntrante != null
					&& !fechEntrante.isEmpty()) {
				/** Convierte a variable de tipo Date la fecha de operacion del filtro */
				Date fechaOp = formatDate.parse(fechEntrante);
				/** Valida si la fecha de operacion del filtrado es posterior a la actual */
				if (fechaOp.after(dia)) {
					modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
					mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO + Errores.ED00020V, null, null);
					modeloVista.addObject(Constantes.COD_ERROR, Errores.ED00020V);
					modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
					modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
					return modeloVista;
				}
			}
			/** Se asigna el submenu y el nombre de la tabla **/
			/** re sealiza la consulta de los datos **/
			if (beanReqConsTranSpei.getPaginador() != null) {
				recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(beanResTranSpeiRec, beanReqConsTranSpei,
						getArchitechBean(), historico, beanTranSpeiRecOper);
				/** Se llama a la vista a mostrar o pantalla **/
				modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
				mensaje = messageSource.getMessage("codError." + recepOperaAdmonSaldos.getBeanError().getCodError(),
						null, null);
				modeloVista.addObject(Constantes.COD_ERROR, recepOperaAdmonSaldos.getBeanError().getCodError());
				modeloVista.addObject(Constantes.TIPO_ERROR, recepOperaAdmonSaldos.getBeanError().getTipoError());
				modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
				modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			//realiza la consulta de los combos
			} else {
				recepOperaAdmonSaldos = bORecepOperacion.consultaListas(recepOperaAdmonSaldos, beanReqConsTranSpei,
						getArchitechBean(), historico, beanTranSpeiRecOper);
				modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			}
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject(SESSION, sessionBean);

		} catch (BusinessException e) {
			showException(e);
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(
					Constantes.COD_ERROR_PUNTO + recepOperaAdmonSaldos.getBeanError().getCodError(), null, null);
			modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage(), this.getClass());
		} catch (ParseException pe) {
			showException(pe);
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO + "ED00138V", null, null);
			modeloVista.addObject(Constantes.COD_ERROR, "ED00138V");
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + pe.getMessage(), this.getClass());
		}
		return modeloVista;
	}

	/**
	 * Metodo que permite actualizar el motivod de rechazo.
	 *
	 * @param beanResTranSpeiRec the bean res tran spei rec
	 * @param beanReqConsTranSpei Objeto con los datos de la peticion
	 * @param res                 Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/actMotRechazo.do")
	public ModelAndView actMotRechazo(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpei,
			HttpServletResponse res,
			@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
		modeloVista = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		/** se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		String mensaje = "";
		try {
			BeanResTranSpeiRec beanResConsTranSpeiRec = null;
			/** se llama a la funcion para actualizar el motivo de rechazo **/
			beanResConsTranSpeiRec = bORecepOperaRecuperaMotivo.actMotRechazo(beanReqConsTranSpei, getArchitechBean(),
					historico);
			/** se realiza la busqueda de todos los datos **/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(beanResTranSpeiRec, beanReqConsTranSpei,
					getArchitechBean(), historico, beanTranSpeiRecOper);
			/** Se llama a la vista a mostrar o pantalla **/
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject(SESSION, sessionBean);
			mensaje = messageSource.getMessage(
					Constantes.COD_ERROR_PUNTO + beanResConsTranSpeiRec.getBeanError().getCodError(), null, null);
			modeloVista.addObject(Constantes.COD_ERROR, beanResConsTranSpeiRec.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, beanResConsTranSpeiRec.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
		} catch (BusinessException e) {
			showException(e);
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage("codError." + recepOperaAdmonSaldos.getBeanError().getCodError(), null,
					null);
			modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage());
		}
		return modeloVista;
	}

	/**
	 * Metodo que permite recuperar las operaciones.
	 *
	 * @param beanResTranSpeiRec the bean res tran spei rec
	 * @param beanReqConsTranSpei Objeto con los datos de la peticion
	 * @param res                 Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/recuperarTransfSpeiRec.do")
	public ModelAndView recuperarTransfSpeiRec(BeanResTranSpeiRec beanResTranSpeiRec,
			BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletResponse res,
			@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
		modeloVista = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		/** se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		String mensaje = "";
		try {
			//Se realiza la consulta al BO
			BeanResTranSpeiRec beanResConsTranSpei = bORecepOperaRecuperaMotivo.recuperarTransfSpeiRec(beanReqConsTranSpei,
					getArchitechBean(), historico);
			/** re sealiza la consulta de los datos **/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(beanResTranSpeiRec, beanReqConsTranSpei,
					getArchitechBean(), historico, beanTranSpeiRecOper);
			/** Se llama a la vista a mostrar o pantalla **/
			modeloVista = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject(SESSION, sessionBean);
			mensaje = messageSource.getMessage(
					Constantes.COD_ERROR_PUNTO + beanResConsTranSpei.getBeanError().getCodError(), null, null);
			modeloVista.addObject(Constantes.COD_ERROR, beanResConsTranSpei.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, beanResConsTranSpei.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
		} catch (BusinessException e) {
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage());
			showException(e);
		}
		return modeloVista;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad bORecepOperacion.
	 *
	 * @return bORecepOperacion Objeto de tipo @see BORecepOperacion
	 */
	public BORecepOperacion getBORecepOperacion() {
		return bORecepOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad bORecepOperacion.
	 *
	 * @param recepOperacion Objeto de tipo @see BORecepOperacion
	 */
	public void setBORecepOperacion(BORecepOperacion recepOperacion) {
		bORecepOperacion = recepOperacion;
	}

	/**
	 * getbORecepOperaRecuperaMotivo de tipo BORecepOperaRecuperaMotivo.
	 * 
	 * @author FSW-Vector
	 * @return bORecepOperaRecuperaMotivo de tipo BORecepOperaRecuperaMotivo
	 */
	public BORecepOperaRecuperaMotivo getbORecepOperaRecuperaMotivo() {
		return bORecepOperaRecuperaMotivo;
	}

	/**
	 * setbORecepOperaRecuperaMotivo para asignar valor a
	 * bORecepOperaRecuperaMotivo.
	 * 
	 * @author FSW-Vector
	 * @param bORecepOperaRecuperaMotivo de tipo BORecepOperaRecuperaMotivo
	 */
	public void setbORecepOperaRecuperaMotivo(BORecepOperaRecuperaMotivo bORecepOperaRecuperaMotivo) {
		this.bORecepOperaRecuperaMotivo = bORecepOperaRecuperaMotivo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource.
	 *
	 * @return messageSource Objeto del tipo ResourceBundleMessageSource
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource.
	 *
	 * @param messageSource del tipo ResourceBundleMessageSource
	 */
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}
}