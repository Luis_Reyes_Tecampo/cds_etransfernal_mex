/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerConsReceptorHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsReceptHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloSPID.BOConsultaReceptorHist;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;

/**
 * Controlador para las recepciones historicas
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerConsReceptorHist extends Architech {

	/**
	 * 
	 */
	private static final long serialVersionUID = -161982478470884654L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private BOConsultaReceptorHist bOConsultaReceptorHist;
	
	/**
	 * @return ModelAndView
	 */
	@RequestMapping(value="/inicioConsultaReceptorHist.do")
	public ModelAndView consultaOrdenesReparacion(){
		ModelAndView model = null;
		model = new ModelAndView("consultaReceptorHist");
		
		BeanResConsReceptHist beanResConsRecepHist = new BeanResConsReceptHist();
		beanResConsRecepHist.setFecha("DD/MM/YYYY");
		model.addObject("beanResConsRecepHist", beanResConsRecepHist);
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}		
		model.addObject("sessionSPID", sessionSPID);
		return model;
	}
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanReqConsReceptHist Objeto del tipo BeanReqConsReceptHist
	 * @return ModelAndView
	 */
	@RequestMapping(value="/realizaConsultaReceptorHist.do")
	public ModelAndView realizaConsultaOrdenesReparacion(BeanPaginador beanPaginador, BeanReqConsReceptHist beanReqConsReceptHist) {
		BeanResConsReceptHist beanResConsReceptHist = null;
		ModelAndView model = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		if ("".equals(beanReqConsReceptHist.getOpcion())){
			beanReqConsReceptHist.setOpcion("TODAS");
		}
		
		beanResConsReceptHist = bOConsultaReceptorHist.obtenerDatosReceptHist(beanPaginador,beanReqConsReceptHist,getArchitechBean());
		
		model = new ModelAndView("consultaReceptorHist","beanResConsRecepHist", beanResConsReceptHist);
		
		model.addObject(Constantes.COD_ERROR, beanResConsReceptHist.getCodError());
		model.addObject(Constantes.TIPO_ERROR, beanResConsReceptHist.getTipoError());
		model.addObject(Constantes.DESC_ERROR, beanResConsReceptHist.getMsgError());
		model.addObject("sessionSPID", sessionSPID);
		model.addObject("opcion", beanReqConsReceptHist.getOpcion());
		generarContadoresPaginacion(beanResConsReceptHist, model);
		return model;
	}
	
	/**
	 * @param beanResOrdenReparacion Objeto del tipo BeanResOrdenReparacion
	 * @param model Objeto del tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(BeanResConsReceptHist beanResConsReceptHist, ModelAndView model) {
		if (beanResConsReceptHist.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResConsReceptHist.getTotalReg();
			
			if (!"1".equals(beanResConsReceptHist.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResConsReceptHist.getBeanPaginador().getPagina()) - 1)+1;
			}
			if (beanResConsReceptHist.getTotalReg() >= 20 * Integer.parseInt(beanResConsReceptHist.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResConsReceptHist.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanConsRecepHist beanConsReceptHist : beanResConsReceptHist.getListaDatos()){
				totalImporte = totalImporte.add(new BigDecimal(beanConsReceptHist.getBeanConsRecepHistDos().getMonto()));
			}
			model.addObject("importePagina", totalImporte.toString());
			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
			
		}
	}
	
	/**
	 * @return BOConsultaReceptorHist
	 */
	public BOConsultaReceptorHist getBOConsultaReceptorHist() {
		return bOConsultaReceptorHist;
	}

	/**
	 * @param bOConsultaReceptorHist Objeto del tipo BOConsultaReceptorHist
	 */
	public void setBOConsultaReceptorHist(BOConsultaReceptorHist bOConsultaReceptorHist) {
		this.bOConsultaReceptorHist = bOConsultaReceptorHist;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
