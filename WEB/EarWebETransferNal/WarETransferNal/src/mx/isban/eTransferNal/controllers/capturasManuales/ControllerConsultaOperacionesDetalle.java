/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaOperacionesDetalle.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/03/2017 12:05:00  Vector		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperacionesDetalle;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral;
import mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperacionesDetalle;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;
import mx.isban.eTransferNal.utilerias.MetodosConsultaOperaciones;
import mx.isban.eTransferNal.utilerias.UtilsConsultaOperaciones;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Class ControllerConsultaOperacionesDetalle.
 *
 * @author FSW-Vector
 * @since 7/04/2017
 */
//Clase ControllerConsultaOperacionesDetalle
@Controller
public class ControllerConsultaOperacionesDetalle extends Architech {

	//Constante serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 6879191783887167043L;

	//Constante PAGINA
	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	private static final String PAGINA = "consultaOperacionesDetalle";

	//Constante BEAN
	/** Propiedad del tipo String que almacena el valor de BEAN. */
	private static final String BEAN = "beanRes";
	
	/** La constante ERMCC003. ERROR TIPOS DE DATO */
	private static final String ERMCC004 = "ERMCC004";
	
	//Variable boConsultaOperacionesDetalle
	/** Propiedad del tipo BOConsultaOperaciones que almacena el valor de boConsultaOperaciones. */
	private BOConsultaOperacionesDetalle boConsultaOperacionesDetalle;
	
	//Variable boCapturaCentral
	/** La variable que contiene informacion con respecto a: bo captura central. */
	private BOCapturaCentral boCapturaCentral;

	/**
	 * Definir el objeto: bo consulta operaciones detalle.
	 *
	 * @param boConsultaOperacionesDetalle the boConsultaOperacionesDetalle to set
	 */
	//set de boconsultaoperacionesdetale
	public void setBoConsultaOperacionesDetalle(
			BOConsultaOperacionesDetalle boConsultaOperacionesDetalle) {
		this.boConsultaOperacionesDetalle = boConsultaOperacionesDetalle;
	}

	/**
	 * Definir el objeto: bo captura central.
	 *
	 * @param boCapturaCentral the boCapturaCentral to set
	 */
	//set de bocapturacentral
	public void setBoCapturaCentral(BOCapturaCentral boCapturaCentral) {
		this.boCapturaCentral = boCapturaCentral;
	}

	/**
	 * Muestra consulta operaciones detalle.
	 *
	 * @param req El objeto: req
	 * @param beanReqConsOperaciones El objeto: bean req cons operaciones
	 * @return ModelAndView Objeto de Spring MVC
	 */
	//Metodo que se encarga de mostrar le detalle de una operacion
	@RequestMapping(value = "/consultaOperacionesDetalle.do")
	ModelAndView muestraConsultaOperacionesDetalle(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		UtilsConsultaOperaciones utilsConsultaOperaciones = new UtilsConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);
		
		//Creacion de objetos necesarios
		BeanResConsOperacionesDetalle beanResConsOperaciones = new BeanResConsOperacionesDetalle();
		utilsConsultaOperaciones.llenarDatosDeRequest(beanReqConsOperaciones, beanResConsOperaciones);
		beanResConsOperaciones.setEditando(false);
		beanResConsOperaciones.setPermisoModicar(false);
		BeanResBase resPermisoApartar = new BeanResBase();

		//Se consulta el permiso de apartar con el bo
		try{
			resPermisoApartar=boConsultaOperacionesDetalle.consultaPermisoApartar(getArchitechBean(), beanReqConsOperaciones.getFolio());
		}catch(BusinessException e){
			showException(e);
			resPermisoApartar.setCodError(Errores.EC00011B);
			resPermisoApartar.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		
		//Se valida permiso para apartar
		if(Errores.CODE_SUCCESFULLY.equals(resPermisoApartar.getCodError())){			
			BeanCapturaCentralResponse beanResConsultaTemplate = null;
			BeanCapturaCentralRequest requestConsultaTemplate = new BeanCapturaCentralRequest();
			requestConsultaTemplate.setIdFolio(beanReqConsOperaciones.getFolio());
			BeanResConsOperaciones beanResObtenerTemplate = new BeanResConsOperaciones();
			try{
				beanResObtenerTemplate = boConsultaOperacionesDetalle.obtenerTemplate(beanReqConsOperaciones, getArchitechBean());
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				beanResObtenerTemplate.setCodError(Errores.EC00011B);
				beanResObtenerTemplate.setTipoError(Errores.TIPO_MSJ_INFO);
			}
			//Se setea que sea editable
			requestConsultaTemplate.setEditable(false);
			
			//Se valida que se haya obtenido el tempalte
			if(Errores.CODE_SUCCESFULLY.equals(beanResObtenerTemplate.getCodError()) && beanResObtenerTemplate.getTemplate() != null){
				requestConsultaTemplate.setTemplate(beanResObtenerTemplate.getTemplate());
				try{
					beanResConsultaTemplate = boCapturaCentral.consultaTemplate(requestConsultaTemplate, getArchitechBean());
					beanResConsOperaciones.setLayout(beanResConsultaTemplate.getLayout());
					beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
					beanResConsOperaciones.setCodError(Errores.OK00000V);
				}catch(BusinessException e){
					//Manejo de BusinessException
					showException(e);
					beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
					beanResConsOperaciones.setCodError(e.getCode());	
				}
			}else{
				//Se setea el cod y tipo de error de la respuesta de obtener template
				beanResConsOperaciones.setCodError(beanResObtenerTemplate.getCodError());
				beanResConsOperaciones.setTipoError(beanResObtenerTemplate.getTipoError());
			}
			BeanResBase resPermisoModificar = new BeanResBase();
			try{
				resPermisoModificar=boConsultaOperacionesDetalle.consultaPermisoModificar(getArchitechBean(), beanReqConsOperaciones.getFolio());
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				resPermisoModificar.setCodError(Errores.EC00011B);
				resPermisoModificar.setTipoError(Errores.TIPO_MSJ_INFO);
			}
			
			//Se valida permiso para modificar
			if(Errores.CODE_SUCCESFULLY.equals(resPermisoModificar.getCodError())){
				beanResConsOperaciones.setPermisoModicar(true);
			}
		}else if(Errores.ED00011V.equals(resPermisoApartar.getCodError())){
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
			beanResConsOperaciones.setCodError(Errores.CMCO009V);
		}else{
			//Se setea el cod y tipo de error de la respuesta de obtener template
			beanResConsOperaciones.setTipoError(resPermisoApartar.getTipoError());
			beanResConsOperaciones.setCodError(resPermisoApartar.getCodError());
		}
		
		//Se setea el objeto y error en el modelo
		Map<String, Object> map = modelAndView.getModel();
		utilsConsultaOpe.llenaError(map, beanResConsOperaciones, request, null, null);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;
	}
	
	/**
	 * Modifica consulta operaciones detalle.
	 *
	 * @param req El objeto: req
	 * @param beanReqConsOperaciones El objeto: bean req cons operaciones
	 * @return ModelAndView Objeto de Spring MVC
	 */
	//Metodo para modificar un folio
	@RequestMapping(value = "/consultaOperacionesDetalleModificar.do")
	ModelAndView modificaConsultaOperacionesDetalle(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		UtilsConsultaOperaciones utilsConsultaOperaciones = new UtilsConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		//Se crean objetos necesarios y permisos
		BeanResConsOperacionesDetalle beanResConsOperaciones = new BeanResConsOperacionesDetalle();
		utilsConsultaOperaciones.llenarDatosDeRequest(beanReqConsOperaciones, beanResConsOperaciones);
		beanResConsOperaciones.setPermisoModicar(false);
		beanResConsOperaciones.setEditando(false);
		BeanResBase resPermisoApartar = new BeanResBase();

		//Se consulta el permiso de apartar con el bo
		try{
			resPermisoApartar=boConsultaOperacionesDetalle.consultaPermisoApartar(getArchitechBean(), beanReqConsOperaciones.getFolio());
		}catch(BusinessException e){
			//Manejo de BusinessException
			showException(e);
			resPermisoApartar.setCodError(Errores.EC00011B);
			resPermisoApartar.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		
		//Se valida que el usuario tenga apartado el folio
		if(Errores.CODE_SUCCESFULLY.equals(resPermisoApartar.getCodError())){
			beanResConsOperaciones.setPermisoModicar(true);
		}else if(Errores.ED00011V.equals(resPermisoApartar.getCodError())){
			beanResConsOperaciones.setCodError(Errores.CMCO009V);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else{
			beanResConsOperaciones.setCodError(resPermisoApartar.getCodError());
			beanResConsOperaciones.setTipoError(resPermisoApartar.getTipoError());
		}

		BeanResBase resPermisoModificar = new BeanResBase();
		try{
			resPermisoModificar=boConsultaOperacionesDetalle.consultaPermisoModificar(getArchitechBean(), beanReqConsOperaciones.getFolio());
		}catch(BusinessException e){
			//Manejo de BusinessException
			showException(e);
			resPermisoModificar.setCodError(Errores.EC00011B);
			resPermisoModificar.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		if(beanResConsOperaciones.isPermisoModicar() && Errores.CODE_SUCCESFULLY.equals(resPermisoModificar.getCodError())){
			beanResConsOperaciones.setEditando(true);
			BeanCapturaCentralRequest requestConsultaTemplate = new BeanCapturaCentralRequest();
			requestConsultaTemplate.setEditable(true);
			requestConsultaTemplate.setIdFolio(beanReqConsOperaciones.getFolio());
			BeanResConsOperaciones beanResObtenerTemplate = new BeanResConsOperaciones();
			try{
				beanResObtenerTemplate = boConsultaOperacionesDetalle.obtenerTemplate(beanReqConsOperaciones, getArchitechBean());
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				beanResObtenerTemplate.setCodError(Errores.EC00011B);
				beanResObtenerTemplate.setTipoError(Errores.TIPO_MSJ_INFO);
			}

			//Se ejecuta la modificacion
			modificaConsultaOperacionesDetalle2(beanResObtenerTemplate, requestConsultaTemplate, beanResConsOperaciones);
					
		}else if(Errores.ED00011V.equals(resPermisoModificar.getCodError())){
			beanResConsOperaciones.setCodError(Errores.CMCO010V);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else{
			beanResConsOperaciones.setCodError(resPermisoModificar.getCodError());
			beanResConsOperaciones.setTipoError(resPermisoModificar.getTipoError());
		}

		//Se setea el objeto y error en el modelo
		Map<String, Object> map = new HashMap<String, Object>();
		map = modelAndView.getModel();
		utilsConsultaOpe.llenaError(map, beanResConsOperaciones, request, null,null);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;
	}
	
	/**
	 * Modifica consulta operaciones detalle2.
	 *
	 * @param beanResObtenerTemplate the bean res obtener template
	 * @param requestConsultaTemplate the request consulta template
	 * @param beanResConsultaTemplate the bean res consulta template
	 * @param beanResConsOperaciones the bean res cons operaciones
	 */
	private void modificaConsultaOperacionesDetalle2(BeanResConsOperaciones beanResObtenerTemplate, 
			BeanCapturaCentralRequest requestConsultaTemplate, BeanResConsOperacionesDetalle beanResConsOperaciones){
		//Se obtiene el template a partir del folio
		if(Errores.CODE_SUCCESFULLY.equals(beanResObtenerTemplate.getCodError()) && beanResObtenerTemplate.getTemplate() != null){		
			requestConsultaTemplate.setTemplate(beanResObtenerTemplate.getTemplate());

			//Se consulta el template con el bo
			try{
				BeanCapturaCentralResponse beanResConsultaTemplate = boCapturaCentral.consultaTemplate(requestConsultaTemplate, getArchitechBean());
				beanResConsOperaciones.setCodError(Errores.OK00000V);
				beanResConsOperaciones.setLayout(beanResConsultaTemplate.getLayout());
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				beanResConsOperaciones.setCodError(e.getCode());
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
			}
		}else{
			//Se setea el cod y tipo de error
			beanResConsOperaciones.setCodError(beanResObtenerTemplate.getCodError());
			beanResConsOperaciones.setTipoError(beanResObtenerTemplate.getTipoError());
		}
	}
	
	/**
	 * Guarda consulta operaciones detalle.
	 *
	 * @param req El objeto: req
	 * @param beanReqConsOperaciones El objeto: bean req cons operaciones
	 * @return ModelAndView Objeto de Spring MVC
	 */
	//Metodo para guardar el folio
	@RequestMapping(value = "/consultaOperacionesDetalleGuardar.do")
	ModelAndView guardaConsultaOperacionesDetalle(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		UtilsConsultaOperaciones utilsConsultaOperaciones = new UtilsConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		//Se crean los objetos necesarios
		Map<String, Object> map = new HashMap<String, Object>();
		map = modelAndView.getModel();
		String errorParam=null;
		
		//se crean objetos de apoyo
		BeanResConsOperacionesDetalle beanResConsOperaciones = new BeanResConsOperacionesDetalle();
		utilsConsultaOperaciones.llenarDatosDeRequest(beanReqConsOperaciones, beanResConsOperaciones);
		BeanResBase resPermisoApartar = new BeanResBase();
		beanResConsOperaciones.setPermisoModicar(false);
		beanResConsOperaciones.setEditando(false);
		
		//Se realiza la consulta del permiso de apartado
		try{
			resPermisoApartar=boConsultaOperacionesDetalle.consultaPermisoApartar(getArchitechBean(), beanReqConsOperaciones.getFolio());
		}catch(BusinessException e){
			//Manejo de BusinessException
			showException(e);
			resPermisoApartar.setCodError(Errores.EC00011B);
			resPermisoApartar.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		
		//Se consulta que el usuario tenga apartada la operacion para poder guardarla
		if(Errores.CODE_SUCCESFULLY.equals(resPermisoApartar.getCodError())){
			beanResConsOperaciones.setPermisoModicar(true);
			
			//Se ejecuta el guardado en el bo
			try{
				BeanResBase resPermisoModificar=boConsultaOperacionesDetalle.consultaPermisoModificar(getArchitechBean(), beanReqConsOperaciones.getFolio());
				beanResConsOperaciones = validaErrorModificar(request, beanReqConsOperaciones, beanResConsOperaciones, resPermisoModificar);
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				beanResConsOperaciones.setCodError(Errores.EC00011B);
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
			}
			//Se valida cod de error
			if (ERMCC004.equals(beanResConsOperaciones.getCodError())){
				errorParam=beanResConsOperaciones.getMsgError();
			}
			//Se valida cod de error
		}else if(Errores.ED00011V.equals(resPermisoApartar.getCodError())){
			beanResConsOperaciones.setCodError(Errores.CMCO009V);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else{
			//Se setea el cod y tipo de error
			beanResConsOperaciones.setCodError(resPermisoApartar.getCodError());
			beanResConsOperaciones.setTipoError(resPermisoApartar.getTipoError());
		}
		
		//Se setea el objeto y error en el modelo
		utilsConsultaOpe.llenaError(map, beanResConsOperaciones, request, errorParam,null);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;
	}
	
	/**
	 * Valida error modificar.
	 *
	 * @param req El objeto: req
	 * @param beanReqConsOperaciones El objeto: bean req cons operaciones
	 * @param beanResConsOperaciones El objeto: bean res cons operaciones
	 * @param resPermisoModificar El objeto: res permiso modificar
	 * @return Objeto bean res cons operaciones detalle
	 */
	//Metodo para validar error al modificar
	private BeanResConsOperacionesDetalle validaErrorModificar(final HttpServletRequest req, BeanReqConsOperaciones beanReqConsOperaciones,
			BeanResConsOperacionesDetalle beanResConsOperaciones, BeanResBase resPermisoModificar){
		
		boolean validoPermisoModificar = false;
		if(Errores.CODE_SUCCESFULLY.equals(resPermisoModificar.getCodError())){
			validoPermisoModificar = true;
			//Si no pasa validacion se devuelven los errores
		}else if(Errores.ED00011V.equals(resPermisoModificar.getCodError())){
			beanResConsOperaciones.setCodError(Errores.CMCO010V);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}else{
			beanResConsOperaciones.setCodError(resPermisoModificar.getCodError());
			beanResConsOperaciones.setTipoError(resPermisoModificar.getTipoError());
		}
		
		//Si no se tiene permiso para modificar se cancela la operacion
		if(!validoPermisoModificar){
			return beanResConsOperaciones;
		}
		
		//Se setea permisos en bean
		beanResConsOperaciones.setEditando(true);
		BeanCapturaCentralResponse beanResConsultaTemplate = null;
		BeanResConsOperaciones beanResObtenerTemplate = new BeanResConsOperaciones();
		BeanCapturaCentralRequest requestConsultaTemplate = new BeanCapturaCentralRequest();
		requestConsultaTemplate.setIdFolio(beanReqConsOperaciones.getFolio());
		requestConsultaTemplate.setEditable(true);
		
		//Se manda a llamar el bo para obtener el template a partir del folio
		try{
		beanResObtenerTemplate = boConsultaOperacionesDetalle.obtenerTemplate(beanReqConsOperaciones, getArchitechBean());
		}catch(BusinessException e){
			//Manejo de BusinessException
			showException(e);
			beanResConsOperaciones.setCodError(Errores.EC00011B);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
		}
		
		if(Errores.CODE_SUCCESFULLY.equals(beanResObtenerTemplate.getCodError()) && beanResObtenerTemplate.getTemplate() != null){
			
			//Se setea el template obtenido del bo
			requestConsultaTemplate.setTemplate(beanResObtenerTemplate.getTemplate());
			try{
				beanResConsultaTemplate = boCapturaCentral.consultaTemplate(requestConsultaTemplate, getArchitechBean());
			}catch(BusinessException e){
				//Manejo de BusinessException
				showException(e);
				beanResConsOperaciones.setCodError(e.getCode());
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
				return beanResConsOperaciones;
			}
			beanResConsOperaciones.setLayout(beanResConsultaTemplate.getLayout());
			
			validarErrorModificar2(beanResConsOperaciones, beanReqConsOperaciones, req, beanResObtenerTemplate, beanResConsultaTemplate);
		}
		return beanResConsOperaciones;
	}

	/**
	 * Validar error modificar2.
	 *
	 * @param beanResConsOperaciones the bean res cons operaciones
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param req the req
	 * @param beanResObtenerTemplate the bean res obtener template
	 * @param beanResConsultaTemplate the bean res consulta template
	 */
	@SuppressWarnings("unchecked")
	private void validarErrorModificar2(BeanResConsOperacionesDetalle beanResConsOperaciones, BeanReqConsOperaciones beanReqConsOperaciones,
			HttpServletRequest req, BeanResConsOperaciones beanResObtenerTemplate, BeanCapturaCentralResponse beanResConsultaTemplate){
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		UtilsConsultaOperaciones utilsConsultaOperaciones = new UtilsConsultaOperaciones();
		//Se obtiene los campos que deben ser obligatorios
		Map<String, Object> objetos = new HashMap<String, Object>();
		List<String> obligatorios = utilsConsultaOpe.getObligatorios(beanResConsOperaciones);
		List<Map<String, String>> camposPistas=utilsConsultaOpe.obtieneCamposDiferentes(req, beanResConsOperaciones);
		objetos = utilsConsultaOpe.mapeaBeanOperacion(req, obligatorios, beanResConsOperaciones);
		if (objetos.containsKey(MetodosConsultaOperaciones.ERROR)) {
			beanResConsOperaciones.setCodError(ERMCC004);
			beanResConsOperaciones.setMsgError(objetos.get(MetodosConsultaOperaciones.ERROR).toString());
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ALERT);
			return;
		}
		BeanOperacion operacion = (BeanOperacion) objetos.get(MetodosConsultaOperaciones.BEAN_OPER);
		operacion.setIdFolio(beanReqConsOperaciones.getFolio());
		obligatorios = (List<String>) objetos.get(MetodosConsultaOperaciones.LISTA);
		//Faltan datos obligatorios
		if (obligatorios.isEmpty()) {
			operacion.setIdTemplate(beanResObtenerTemplate.getTemplate());
			operacion.setIdLayout(beanResObtenerTemplate.getLayoutID());
			info("Bean: " + ReflectionToStringBuilder.toString(operacion));
			
			//Si pasa todas las validaciones se ejecuta el guardado en el bo
			BeanResBase responseGuardarOperacion =
					boConsultaOperacionesDetalle.guardarOperacion(getArchitechBean(), operacion, camposPistas,beanResObtenerTemplate);
			if (responseGuardarOperacion.getCodError().equals(Errores.OK00000V)){
				beanResConsOperaciones.setCodError(Errores.CMCO017V);
				beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_INFO);
				beanResConsOperaciones.setEditando(false);
				//Se setea lista en el bean
				utilsConsultaOperaciones.ponerListaLayoutComoConstantes(beanResConsultaTemplate.getLayout());
			} else {
				//Se setea el cod y tipo de error
				beanResConsOperaciones.setCodError(responseGuardarOperacion.getCodError());
				beanResConsOperaciones.setTipoError(responseGuardarOperacion.getTipoError());
			}
		}else{
			//Se setea el cod de error CMCO016V
			beanResConsOperaciones.setCodError(Errores.CMCO016V);
			beanResConsOperaciones.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
	}
	
    /**
     * Request to bean req cons operaciones.
     *
     * @param req the req
     * @param res the res
     * @return the bean req cons operaciones
     */
	//Metodo que mapea el request al bean
    private BeanReqConsOperaciones requestToBeanReqConsOperaciones(HttpServletRequest req, HttpServletResponse res){
        BeanReqConsOperaciones beanReqConsOperaciones = new BeanReqConsOperaciones();
        List<BeanConsOperaciones> listBeanConsOperaciones = new ArrayList<BeanConsOperaciones>();
        UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistBeanConsOperaciones"), listBeanConsOperaciones, new BeanConsOperaciones());
        beanReqConsOperaciones.setListBeanConsOperaciones(listBeanConsOperaciones);
        UtilsMapeaRequest.getMapper().mapearObject(beanReqConsOperaciones, req.getParameterMap());
        return beanReqConsOperaciones;
    }
}

