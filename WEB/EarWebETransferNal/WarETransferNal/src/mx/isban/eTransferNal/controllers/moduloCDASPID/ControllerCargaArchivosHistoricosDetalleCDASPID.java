/**
 * Clase de tipo controller para procesar la pantalla
 * de Monitor de Carga Archivos Historicos Detalle
 * 
 * @author: Vector
 * @since: Enero 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanDetalleArchivoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOCargaArchivosHistoricosDetalleCDASPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class ControllerCargaArchivosHistoricosDetalleCDASPID.
 */
@Controller
public class ControllerCargaArchivosHistoricosDetalleCDASPID 
		extends Architech {

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = -7522257445155094293L;
	
	/**
	 * La constante ID_ARCHIVO
	 */
	private static final String ID_ARCHIVO = "idArchivo";

	/**
	 * La constante ID_ARCHIVO
	 */
	private static final String CODIGO_ERROR = "codigoError";

	/**
	 * La constante ID_ARCHIVO
	 */
	private static final String MENSAJE_ERROR = "mensajeError";

	/**
	 * La constante ID_ARCHIVO
	 */
	private static final String VISTA = "cargaArchivosHistoricosDetalleCDASPID";
	
	/**
	 * La variable de instancia para el servicio
	 */
	private BOCargaArchivosHistoricosDetalleCDASPID boCargaArchivosHistoricosDetalleCDASPID;
	
	
	/**
	 * Metodo para consultar los detalles del Archivo.
	 *
	 * @param request the request
	 * @param response the response
	 * @return La respuesta obtenida
	 */
	//Metodo para consultar los detalles del Archivo
	@RequestMapping(value="/detalleArhivoHistorico.do")
	public ModelAndView consutarDetallesArchivo(HttpServletRequest request, HttpServletResponse response) {
		debug("<<<Iniciando la consulta de detalle de archivo>>>");
		ModelAndView modelAndView = new ModelAndView(VISTA);

		//Objetos de entrada
		ArchitechSessionBean sessionBean =
				new ArchitechSessionBean();
		BeanReqMonitorCargaArchHistCADSPID datosEntrada = new BeanReqMonitorCargaArchHistCADSPID();
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		BeanPaginador paginador = UtilsMapeaRequest.getMapper().getPaginadorFromRequest(request.getParameterMap());
		
		Map<String, Object> map = modelAndView.getModel();
		
		//Se obtiene y se setea el id del archivo
		String idArchivoDetalle = request.getParameter(ID_ARCHIVO);
		datosEntrada.setIdArchivo(idArchivoDetalle);
		map.put(ID_ARCHIVO, idArchivoDetalle);
		map.put("paginador", paginador);
		
		//Se hace la consulta al bo
		try{
			respuesta = boCargaArchivosHistoricosDetalleCDASPID.
					consutarDetallesArchivo(datosEntrada, paginador, sessionBean);
		}catch(BusinessException e){
			//Se valida respuesta
			showException(e);
			map.put(CODIGO_ERROR, e.getCode());
			map.put(MENSAJE_ERROR, e.getMessage());
		}
		
		//Se informan los errores y total de paginas
		info("Total de paginas: "+respuesta.getRegistrosTotales());
		map.put("respuesta", respuesta);
		
		return modelAndView;
	}
	
	/**
	 * Metodo para generar el archivo historico.
	 *
	 * @param request the request
	 * @param response the response
	 * @return La respuesta obtenida
	 */
	//Metodo para generar el archivo historico
	@RequestMapping(value="/generarArchivoHistorico.do")
	public ModelAndView generarAchivoHistorico(HttpServletRequest request, HttpServletResponse response){
		debug("<<<Iniciando la generacion del archivo historico>>>");
		
		//Objetos de entrada
		ModelAndView modelAndView = new ModelAndView(VISTA);
		Map<String, Object> map = modelAndView.getModel();
		BeanReqMonitorCargaArchHistCADSPID datosEntrada = requestToBeanReqMonitorCargaArchHistCADSPID(request, modelAndView);

		//Se crean objetos de respuesta
		String idArchivoGenera = request.getParameter(ID_ARCHIVO);
		datosEntrada.setIdArchivo(idArchivoGenera);
		
		//Se hace la consulta al BO
		try{
		boCargaArchivosHistoricosDetalleCDASPID.
				generarArchivoHistorico(datosEntrada, 
						getArchitechBean());
		}catch(BusinessException e){
			//Se valida respuesta
			showException(e);
			map.put(CODIGO_ERROR, e.getCode());
			map.put(MENSAJE_ERROR, e.getMessage());
		}
		
		return modelAndView;
	}
	

	/**
	 * Metodo para la eliminacion de la seleccion.
	 *
	 * @param request the request
	 * @param response the response
	 * @return La respueta obtenida
	 */
	//Metodo para la eliminacion de la seleccion
	@RequestMapping(value="/eleminaSeleccionCDASPID.do")
	public ModelAndView eleminarSeleccionArchivo(HttpServletRequest request, HttpServletResponse response) {
		debug("<<<Iniciando la eleminacion de seleccion>>>");
		
		//Objetos de entrada
		ModelAndView modelAndView = new ModelAndView(VISTA);
		Map<String, Object> map = modelAndView.getModel();
		BeanReqMonitorCargaArchHistCADSPID datosEntrada = requestToBeanReqMonitorCargaArchHistCADSPID(request, modelAndView);
		BeanPaginador paginador = UtilsMapeaRequest.getMapper().getPaginadorFromRequest(request.getParameterMap());
		
		//Se crean objetos de respuesta
		String idArchivoElimina = request.getParameter(ID_ARCHIVO);
		BeanResMonitorCargaArchHistCADSPID respuesta =
				new BeanResMonitorCargaArchHistCADSPID();
		
		//Se setea cadena a eliminar
		List<BeanDetalleArchivoCDASPID> lista =
				new ArrayList<BeanDetalleArchivoCDASPID>();
		String cadenaElimina = request.getParameter("elimina").toString();
		info("La cadena a eliminar: "+cadenaElimina);
		
		datosEntrada.setListaDetalle(separarCadenas(cadenaElimina,lista));

		try{
			//Se valida que la lista no sea nula o vacia
			if(datosEntrada.getListaDetalle()!=null && 
					!datosEntrada.getListaDetalle().isEmpty()) {
				//se hace la consulta al BO
				respuesta = boCargaArchivosHistoricosDetalleCDASPID.
						eliminarSelecionArchivo(datosEntrada, getArchitechBean());
			} else {

				//Se hace la consulta al BO
				datosEntrada.setIdArchivo(idArchivoElimina);
				respuesta = boCargaArchivosHistoricosDetalleCDASPID.
						consutarDetallesArchivo(datosEntrada, paginador, getArchitechBean());
				map.put(CODIGO_ERROR, Errores.ED00026V);
				map.put(MENSAJE_ERROR, Errores.TIPO_MSJ_ERROR);
			}
		}catch(BusinessException e){
			//Se valida respuesta
			showException(e);
			map.put(CODIGO_ERROR, e.getCode());
			map.put(MENSAJE_ERROR, e.getMessage());
		}
		
		//Se setea el id del archivo, la respuesta y el paginador
		map.put(ID_ARCHIVO, datosEntrada.getIdArchivo());
		map.put("respuesta", respuesta);
		map.put("paginador", paginador);
		return modelAndView;
	}
	
	/**
	 * Metodo para para establecer la variable de instancia
	 * 
	 * @param boCargaArchivosHistoricosDetalleCDASPID 
	 * La variable de instancia boCargaArchivosHistoricosDetalleCDASPID
	 */
	public void setBoCargaArchivosHistoricosDetalleCDASPID(
			BOCargaArchivosHistoricosDetalleCDASPID 
			boCargaArchivosHistoricosDetalleCDASPID) {
		this.boCargaArchivosHistoricosDetalleCDASPID = 
				boCargaArchivosHistoricosDetalleCDASPID;
	}

	/**
	 * Metodo para llenar los datos a eliminar
	 * 
	 * @param cadena El parametro cadena
	 * @param lista El parametro lista
	 * @return La lista de datos
	 */
	private List<BeanDetalleArchivoCDASPID> separarCadenas(String cadena,
			List<BeanDetalleArchivoCDASPID> lista) {
		String[] cadenaSepara = cadena.split(",");
		char caracter =',';
		int car = contarCaracteres(cadena, caracter);
		double div = (double)car/7;
		int filasR = (int) Math.ceil(div);
		info("Caracteres: "+car);
		info("Filas: "+div);
		info("Filas reales: "+filasR);
		int i=0,  aux=0;
		while(i<filasR) {
			BeanDetalleArchivoCDASPID d = 
					new BeanDetalleArchivoCDASPID();
			d.setIdArchivo(cadenaSepara[aux]);
			d.setClaveRastreo(cadenaSepara[aux+1]);
			d.setFechaOperacion(cadenaSepara[aux+2]);
			d.setClaveInstitucion(cadenaSepara[aux+3]);
			d.setInstitucionOrdinaria(cadenaSepara[aux+4]);
			d.setFolioPaquete(cadenaSepara[aux+5]);
			d.setFolioPago(cadenaSepara[aux+6]);
			d.setSeleccionado(true);
			lista.add(d);
			aux = aux+7;
			i++;
		}
		return lista;
	}
	
	/**
	 * Metodo para definir las filas a definir
	 * 
	 * @param cadena El parametro cadena
	 * @param caracter El parametro caracter
	 * @return El nuemero de filas
	 */
	private static int contarCaracteres(String cadena, char caracter) {
        int posicion, contador = 1;
        posicion = cadena.indexOf(caracter);
        while (posicion != -1) { 
            contador++; 
            posicion = cadena.indexOf(caracter, posicion + 1);
        }
        return contador;
	}
	
	/**
	 * Request to bean.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req cons operaciones
	 */
	//Metodo que hace el mapeo del request al bean de tipo BeanReqMonitorCargaArchHistCADSPID
	public BeanReqMonitorCargaArchHistCADSPID requestToBeanReqMonitorCargaArchHistCADSPID(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqMonitorCargaArchHistCADSPID datosEntrada = new BeanReqMonitorCargaArchHistCADSPID();
		List<BeanDetalleArchivoCDASPID> listaDetalle = new ArrayList<BeanDetalleArchivoCDASPID>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistBeanConsOperaciones"), listaDetalle, new BeanDetalleArchivoCDASPID());
		datosEntrada.setListaDetalle(listaDetalle);
		UtilsMapeaRequest.getMapper().mapearObject(datosEntrada, req.getParameterMap());
		return datosEntrada;
	}
}