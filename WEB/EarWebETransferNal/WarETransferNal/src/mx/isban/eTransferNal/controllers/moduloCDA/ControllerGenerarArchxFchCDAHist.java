/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 11:14:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOGenerarArchCDAxFchHist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Generar Archivo CDA Historico
**/
@Controller
public class ControllerGenerarArchxFchCDAHist extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;

	/**Referencia privada del servicio boGenerarArchCDAxFchHist*/
	private BOGenerarArchCDAxFchHist boGenerarArchCDAxFchHist;
	
   /**
    * Metodo que muestra la generacion de Archivo CDA historico
    * @param req Objeto del tipo HttpServletRequests
    * @return ModelAndView Objeto del tipo ModelAndView
    */
   @RequestMapping(value = "/muestraGenerarArchCDAxFchHist.do")
   public ModelAndView muestraGenerarArchCDAHist(HttpServletRequest req){
      ModelAndView modelAndView = null;
      RequestContext ctx = new RequestContext(req);
      BeanResConsFechaOp beanResConsFechaOp = null;
      try{
    	  beanResConsFechaOp = boGenerarArchCDAxFchHist.consultaFechaOperacion(getArchitechBean());
    	  modelAndView = new ModelAndView("generarArchivoxFchCDAHist","generarArchCDAHist",beanResConsFechaOp);
    	  String mensaje = ctx.getMessage("codError."+beanResConsFechaOp.getCodError());
     	  modelAndView.addObject("codError", beanResConsFechaOp.getCodError());
     	  modelAndView.addObject("tipoError", beanResConsFechaOp.getTipoError());
     	  modelAndView.addObject("descError", mensaje);
      }catch(BusinessException e){
    	  showException(e);
      }
      return modelAndView;
   }
   /**
   * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/procesarGenerarArchCDAxFchHist.do")
   public ModelAndView procesarGenerarArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist,HttpServletRequest req){
      ModelAndView modelAndView = null;
	  
	  info("ControllerGenerarArchxFchCDAHist.procesarGenerarArchCDAHist: getFechaOp"+beanReqGenArchCDAHist.getFechaOp());
      BeanResProcGenArchCDAxFchHist beanResProcGenArchCDAxFchHist = null;
      RequestContext ctx = new RequestContext(req);
      try{
    	  beanResProcGenArchCDAxFchHist = boGenerarArchCDAxFchHist.procesarGenArchCDAHist(beanReqGenArchCDAHist,getArchitechBean());
    	  modelAndView = new ModelAndView("generarArchivoxFchCDAHist","generarArchCDAHist",beanResProcGenArchCDAxFchHist);
    	  if(Errores.OK00000V.equals(beanResProcGenArchCDAxFchHist.getCodError())){
    		  String mensaje = ctx.getMessage("codError."+"OK00016V",
    				  new Object[]{beanResProcGenArchCDAxFchHist.getNombreArchivo()});
	     	  modelAndView.addObject("codError", "OK00016V");
	     	  modelAndView.addObject("tipoError", Errores.TIPO_MSJ_INFO);
	     	  modelAndView.addObject("descError", mensaje);
    	  }else{
	    	  String mensaje = ctx.getMessage("codError."+beanResProcGenArchCDAxFchHist.getCodError());
	     	  modelAndView.addObject("codError", beanResProcGenArchCDAxFchHist.getCodError());
	     	  modelAndView.addObject("tipoError", beanResProcGenArchCDAxFchHist.getTipoError());
	     	  modelAndView.addObject("descError", mensaje);
	     	 }
      }catch(BusinessException e){
    	  showException(e);
      }
     
      return modelAndView;
   }
	/**
	 * Metodo get que obtiene el valor de la propiedad boGenerarArchCDAxFchHist
	 * @return boGenerarArchCDAxFchHist Objeto de tipo @see BOGenerarArchCDAxFchHist
	 */
	public BOGenerarArchCDAxFchHist getBoGenerarArchCDAxFchHist() {
		return boGenerarArchCDAxFchHist;
	}
	/**
	 * Metodo que modifica el valor de la propiedad boGenerarArchCDAxFchHist
	 * @param boGenerarArchCDAxFchHist Objeto de tipo @see BOGenerarArchCDAxFchHist
	 */
	public void setBoGenerarArchCDAxFchHist(
			BOGenerarArchCDAxFchHist boGenerarArchCDAxFchHist) {
		this.boGenerarArchCDAxFchHist = boGenerarArchCDAxFchHist;
	}
}
