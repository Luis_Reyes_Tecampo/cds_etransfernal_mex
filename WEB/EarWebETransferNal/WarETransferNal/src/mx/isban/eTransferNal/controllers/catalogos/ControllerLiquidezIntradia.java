/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *  
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2019 10:42:00 AM   		VSF
 */

package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaExportar;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaReq;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOLiquidezIntradia;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtileriasExportarLiquidez;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsLiquidezIntradia;

/**
 * Clase ControllerLiquidezIntradia.
 *
 * @author vector
 */
@Controller
public class ControllerLiquidezIntradia extends Architech{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4428618726600745619L;
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA ="catNotificacionLiquidezIntradia";
	
	/** La constante BEAN_LIQUIDEZ_INTRADIA. */
	private static final String BEAN_LIQUIDEZ_INTRADIA = "beanLiquidez";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaNotificacionLiquidez";
	/** La constante POOL. */
	private static final String POOL = "pool";
	
	/** La constante CAPTURA. */
	private static final String CAPTURA = "captura";
	
	/** La constante TIPO_CONSULTA. */
	private static final String TIPO_CONSULTA = "tipoConsulta";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante COMBO. */
	private static final String COMBO="combos";
	/**Constante bo liquidez intradia. */
	private BOLiquidezIntradia boLiquidezIntradia;

	/** La constante utileriasExportar. */
	private static final UtileriasExportarLiquidez utileriasExportar = UtileriasExportarLiquidez.getInstancia();

	
	/** La constante FIELD. */
	private static final String FIELD = "field";

	/** La constante FIELD_VALUE. */
	private static final String FIELD_VALUE = "fieldValue";
	 
	
	
	/**
	 * Consulta filtros.
	 *
	 * @param beanPaginador objeto  bean paginador
	 * @param beanFilter objeto  bean filter
	 * @param bindingResult El objeto --> binding result
	 * @return model objeto and view al front
	 */
	@RequestMapping(value="/consultaFiltros.do")
	public ModelAndView consultaFiltros(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanLiquidezIntradia beanFilter, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		return new ModelAndView(PAGINA_CONSULTA);
	}
	
	

	/**
	 * Consulta cat liquidez intradia.
	 *
	 * @param beanPaginador objeto bean paginador
	 * @param beanFilter objeto bean filter
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value="/consultaCatLiquidezIntradia.do")
	public ModelAndView consultaCatLiquidezIntradia(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanLiquidezIntradia beanFilter, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		ModelAndView model = new ModelAndView();
 		String canal=req.getParameter(TIPO_CONSULTA);
		BeanLiquidezIntradiaReq response = null;
		try {
			/** Validacion del parametro accion **/
			if (req.getParameter("filtro") != null && !req.getParameter("filtro").equals(StringUtils.EMPTY)) {
				/** Se valida los filtros guardados**/
				if("CVE_TRANSFER".equals(req.getParameter(FIELD))) {
					beanFilter.setCveTransFe(req.getParameter(FIELD_VALUE));
				}
				if("MECANISMO".equals(req.getParameter(FIELD))) {
					beanFilter.setCveMecanismo(req.getParameter(FIELD_VALUE));
				}
				if("ESTATUS_TRANSFER".equals(req.getParameter(FIELD))) {
					beanFilter.setEstatus(req.getParameter(FIELD_VALUE));
					
				}
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			//Se realiza la consulta
			response =boLiquidezIntradia.consultar(beanFilter, canal,beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_LIQUIDEZ_INTRADIA, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			model.addObject("listaValidar", response.getListaBeanLiquidezIntradia());
			model.addObject(TIPO_CONSULTA, canal);
			/** Seteo de listas para los combos de los filtros **/
			model.addObject(COMBO, boLiquidezIntradia.consultarListas(2, beanFilter,getArchitechBean(),canal));
			model.addObject(TIPO_CONSULTA, canal);
			if(!"OK00000V".equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			/**Control fr excepcionen*/
			showException(e);
		}
		return model;
	}
	
	
	/**
	 * Mantenimiento notificacion.
	 *
	 * @param beanLiquidez objeto bean liquidez
	 * @param beanFilter objeto bean filter
	 * @param beanPaginador objeto bean paginador
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value = "/mantenimientoNotificacion.do")
	public ModelAndView mantenimientoNotificacion(@ModelAttribute(BEAN_LIQUIDEZ_INTRADIA) BeanLiquidezIntradiaReq beanLiquidez,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanLiquidezIntradia beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult
) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Creacion de la vista de mantenimiento  **/
		/**Recupera tipo de consulta*/
		String canal= req.getParameter(TIPO_CONSULTA);
		ModelAndView model = null;
		/**Creacion del objeto de repsuesta*/
		BeanLiquidezIntradia liquidez = new BeanLiquidezIntradia();
		/**Valida si existe el registro*/
		if (beanLiquidez != null && beanLiquidez.getListaBeanLiquidezIntradia() != null) {
			for(BeanLiquidezIntradia not : beanLiquidez.getListaBeanLiquidezIntradia()) {
				if(not.isSeleccionado()) {
					liquidez = not;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		if (liquidez.getCveTransFe() != null) {
			liquidez.setCveTransFe(liquidez.getCveTransFe());
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		/** Se consultan las listas para los combos **/
		try {
			model.addObject(COMBO, boLiquidezIntradia.consultarListas(1,beanFilter,getArchitechBean(),canal));
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		} catch (BusinessException e) {
			showException(e);
		}
		model.addObject(BEAN_LIQUIDEZ_INTRADIA, liquidez);
		model.addObject(TIPO_CONSULTA, canal);
		
		/**Retorno del model and put*/
		return model;
	}
	
	/**
	 * Agregar Notificacion liquidez intradia.
	 *
	 * @param beanLiquidez El objeto --> bean liquidez
	 * @param bindingResult El objeto --> binding result
	 * @return Objeto model and view al front
	 */
	@RequestMapping(value = "/agregarNotificacionLiquidez.do")
	public ModelAndView agregarNotificacionLiquidez(@ModelAttribute(BEAN_LIQUIDEZ_INTRADIA) BeanLiquidezIntradia beanLiquidez, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		//Se crea el model&view a retornar
		/**Recupera tipo de consulta*/
		String canal=req.getParameter(TIPO_CONSULTA);
		ModelAndView model = null;
		BeanResBase response = null;
		RequestContext ctx = new RequestContext(req);
		beanLiquidez.setCveOperacion("DF");
	    String mensaje = "";
		try {
			//Se consume  el servicio de alta de Notificacion liquidez intradia
			response = boLiquidezIntradia.agregar(beanLiquidez,canal,getArchitechBean());
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError());
			if (Errores.OK00000V.equals(response.getCodError())) {
				//se consulta 
				BeanLiquidezIntradiaReq responseConsulta = boLiquidezIntradia.consultar(new BeanLiquidezIntradia(),canal,new BeanPaginador(),getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_LIQUIDEZ_INTRADIA, new BeanLiquidezIntradia());
				model.addObject(BEAN_LIQUIDEZ_INTRADIA, responseConsulta);
				model.addObject(COMBO, boLiquidezIntradia.consultarListas(2,null,getArchitechBean(),canal));
			} else {
				//Se consultan los combos
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION, "alta");	
				model.addObject(BEAN_LIQUIDEZ_INTRADIA, beanLiquidez);
				model.addObject(TIPO_CONSULTA, canal);
				model.addObject(COMBO, boLiquidezIntradia.consultarListas(2,null,getArchitechBean(),canal));
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(TIPO_CONSULTA, canal);
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	
	
	/**
	 * Editar liquidez.
	 *
	 * @param liquidez objeto liquidez
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value = "/editarLiquidez.do")
	public ModelAndView editarLiquidez(@ModelAttribute(BEAN_LIQUIDEZ_INTRADIA) BeanLiquidezIntradia liquidez, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		
		/** Comienza flujo de edicion  **/
		ModelAndView model = null;
		BeanLiquidezIntradiaReq liquidezs = null;
		/**Recupera tipo de consulta*/
		String canal = req.getParameter(TIPO_CONSULTA);
		/**Inicia llamdo de peticion editar*/
		try {
			BeanResBase response = boLiquidezIntradia.editar(liquidez,canal,UtilsLiquidezIntradia.getAnterior(req), getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				liquidezs = boLiquidezIntradia.consultar(new BeanLiquidezIntradia(), canal,new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(COMBO, boLiquidezIntradia.consultarListas(2,null,getArchitechBean(),canal));
				model.addObject(BEAN_LIQUIDEZ_INTRADIA, liquidezs);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(COMBO, boLiquidezIntradia.consultarListas(2,null,getArchitechBean(),canal));
				model.addObject(POOL,CAPTURA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				model.addObject(BEAN_LIQUIDEZ_INTRADIA, liquidez);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(TIPO_CONSULTA, canal);
		} catch (BusinessException e) {
			showException(e);
		}
		/**retorno de modelo */
		return model;
	}
	
	/**
	 * Exportar notificacion liquidez.
	 *
	 * @param beanLiquidez objeto bean liquidez
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value = "/exportarNotificacionLiquidez.do")
	public ModelAndView exportarNotificacionLiquidez(@ModelAttribute(BEAN_LIQUIDEZ_INTRADIA) BeanLiquidezIntradiaExportar beanLiquidez, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utileriasExportar.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utileriasExportar.getBody(beanLiquidez.getLiquidezExportarE()));
		modelAndView.addObject("NAME_SHEET", "Notificacion Liquidez Intradia");
		/**Retorno model **/
		return modelAndView;
	}
	

	/**
	 * Exportartodo notificacion liquidez.
	 *
	 * @param beanPaginador objeto bean paginador
	 * @param beanLiquidez objeto bean liquidez
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value="/exportarTodoNotificacionLiquidez.do")
	public ModelAndView exportarTodoNotificacionLiquidez(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanLiquidezIntradia beanLiquidez, BindingResult bindingResult) {

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanLiquidezIntradiaReq responseConsulta = new BeanLiquidezIntradiaReq();
		RequestContext ctx = new RequestContext(req);
		/**Recupera tipo de consulta*/
		String canal=req.getParameter(TIPO_CONSULTA);
		try {
			/** Ejecuta peticion al BO de la consulta **/
			responseConsulta = boLiquidezIntradia.consultar(beanLiquidez, canal,beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_LIQUIDEZ_INTRADIA, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boLiquidezIntradia.exportarTodo(beanLiquidez,canal, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_LIQUIDEZ_INTRADIA, responseConsulta);
			model.addObject(COMBO, boLiquidezIntradia.consultarListas(2,beanLiquidez,getArchitechBean(),canal));
			model.addObject(UtilsConstantes.BEAN_FILTER, beanLiquidez);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
			model.addObject(TIPO_CONSULTA, canal);
		} catch (BusinessException e) {
			showException(e);
		}
		/**Retorno de modelo*/
		return model;
	}

	/**
	 * Eliminar notificacion liquidez.
	 *
	 * @param liquidez objeto liquidez
	 * @param bindingResult El objeto --> binding result
	 * @return objeto model and view al front
	 */
	@RequestMapping(value = "/eliminarNotificacionLiquidez.do")
    public ModelAndView eliminarNotificacionLiquidez(@ModelAttribute(BEAN_LIQUIDEZ_INTRADIA) BeanLiquidezIntradiaReq liquidez, BindingResult bindingResult){
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		/** Comienza flujo de eliminacion **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		String canal= req.getParameter(TIPO_CONSULTA);
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		/**Inicio de llamado a la peticion eliminar*/
		try {
			response = boLiquidezIntradia.eliminar(liquidez,canal, getArchitechBean());
			BeanLiquidezIntradiaReq responseConsulta = boLiquidezIntradia.consultar(new BeanLiquidezIntradia(), canal,new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_LIQUIDEZ_INTRADIA, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
			modelAndView.addObject(TIPO_CONSULTA, canal);
			modelAndView.addObject(COMBO, boLiquidezIntradia.consultarListas(2,null,getArchitechBean(),canal));
		} catch (BusinessException e) {
		/**Excepion controlada*/
			showException(e);
		}
		/**Retorno del model and view*/
		return modelAndView;
	}
	/**
	 * Gets objeto bo liquidez intradia.
	 *
	 * @return objeto bo liquidez intradia
	 */
	public BOLiquidezIntradia getBoLiquidezIntradia() {
		return boLiquidezIntradia;
	}

	/**
	 * Sets objeto bo liquidez intradia.
	 *
	 * @param boLiquidezIntradia objeto new bo liquidez intradia
	 */
	public void setBoLiquidezIntradia(BOLiquidezIntradia boLiquidezIntradia) {
		this.boLiquidezIntradia = boLiquidezIntradia;
	}

	
	
}
