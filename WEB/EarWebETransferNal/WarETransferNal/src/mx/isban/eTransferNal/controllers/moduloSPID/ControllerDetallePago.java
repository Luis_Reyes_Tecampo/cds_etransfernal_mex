/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerDetallePago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOPago;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.exception.BusinessException;

/**
 * Controlador para la consulta de detalle de pago
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerDetallePago extends Architech{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6958084704573972267L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private BOPago bOPago;
	
	/**
	 * @param tipoOrden Objeto del tipo String
	 * @param referencia Objeto del tipo String
	 * @return ModelAndView
	 */
	@RequestMapping(value="/detallePago.do")
	public ModelAndView cargarDetallePago(@RequestParam("tipo") String tipoOrden, @RequestParam("ref") String referencia) {
		ModelAndView model = null;
		BeanDetallePago beanDetallePago = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanDetallePago = bOPago.consultaDetallePago(referencia, getArchitechBean());
			model = new ModelAndView("detallePago","beanDetallePago", beanDetallePago);
			model.addObject("tipoOrden", tipoOrden);
			model.addObject("referencia", referencia);
			model.addObject("sessionSPID", sessionSPID);
		}catch(BusinessException e) {
			showException(e);
		}
		return model;
	}

	/**
	 * @return BOPago
	 */
	public BOPago getBOPago() {
		return bOPago;
	}

	/**
	 * @param bOPago Objeto del tipo BOPago
	 */
	public void setBOPago(BOPago bOPago) {
		this.bOPago = bOPago;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
