/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerOrdenesReparacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:26 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;
import javax.validation.Valid;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOOrdenes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOReparacion;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

/**
 * Class ControllerOrdenesReparacion.
 *
 * Clase tipo controller utilizada para cargar la pantalla de Ordenens de
 * reparacion y mapear las funcionalides y disparar los flujos de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerExportarOrdenesReparacion extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5164918115190073517L;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "ordenesReparacion";

	/** La constante BEAN_ORDENES. */
	private static final String BEAN_ORDENES = "beanResOrdenReparacion";

	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;

	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;

	/** La variable que contiene informacion con respecto a: bo ordenes. */
	private BOOrdenes boOrdenes;

	/** La variable que contiene informacion con respecto a: bo reparacion. */
	private BOReparacion boReparacion;
	
	/**
	 * Exportar ordenes.
	 * 
	 * Metodo para la exportacion de todos
	 * los registros en BD para batch.
	 *
	 * @param beanResPantalla       El objeto: bean res pantalla
	 * @param beanModulo            El objeto: bean modulo
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param paginador             El objeto: paginador
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarTodoOrdenesReparacion.do")
	public ModelAndView exportarOrdenes(
			@Valid @ModelAttribute(BEAN_ORDENES) BeanResOrdenReparacion beanResPantalla,
			@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			BeanReqOrdeReparacion beanReqOrdeReparacion, BeanPaginador paginador, BindingResult bindingResult) {
		ModelAndView modelExp = null;
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			beanResOrdenReparacion = boOrdenes.exportarTodoOrdenesReparacion(beanModulo, beanReqOrdeReparacion,
					getArchitechBean());
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO + beanResOrdenReparacion.getCodError(),
					new Object[] { beanResOrdenReparacion.getNombreArchivo() }, null);
			modelExp = new ModelAndView(PAGINA_PRINCIPAL);
			modelExp.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
			modelExp.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
			modelExp.addObject(Constantes.DESC_ERROR, mensaje);
			modelExp.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			beanResOrdenReparacion.setBeanPaginador(paginador);
			beanResOrdenReparacion.setTotalReg(beanResPantalla.getTotalReg());
			beanResOrdenReparacion.setListaOrdenesReparacion(beanResPantalla.getListaOrdenesReparacion());
			modelExp.addObject(BEAN_ORDENES, beanResOrdenReparacion);
			generarContadoresPaginacion(beanModulo, beanResOrdenReparacion, modelExp, "", "");
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return modelExp;
	}

	
	/**
	 * Generar contadores paginacion para la exportacion del archivo.
	 * Funcion para validar la paginacion en base a la respuesta 
	 * obtenida.
	 * @param modulo                 El objeto: modulo parametro de entrada
	 * @param beanResOrdenReparacion El objeto: bean res orden reparacion parametro de entrada
	 * @param model                  El objeto: model parametro de entrada
	 * @param sortField              El objeto: sort field parametro de entrada
	 * @param sortType               El objeto: sort type parametro de entrada
	 */
	protected void generarContadoresPaginacion(BeanModulo modulo, BeanResOrdenReparacion beanResOrdenReparacion,
			ModelAndView model, String sortField, String sortType) {
		if (beanResOrdenReparacion.getTotalReg() > 0) {
			Integer regIniCon = 1;
			Integer regFinCon = beanResOrdenReparacion.getTotalReg();
			/** Se valida la condicion del tamano de la paginacion*/
			if (!"1".equals(beanResOrdenReparacion.getBeanPaginador().getPagina())) {
				regIniCon = 20 * (Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina()) - 1) + 1;
			}
			/** Se valida el tamaño de los registros*/
			if (beanResOrdenReparacion.getTotalReg() >= 20
					* Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina())) {
				regFinCon = 20 * Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina());
			}

			BigDecimal totalPaginaCon = BigDecimal.ZERO;
			BigDecimal totalImporteCon = BigDecimal.ZERO;
			for (BeanOrdenReparacion beanOrdenReparacion : beanResOrdenReparacion.getListaOrdenesReparacion()) {
				totalPaginaCon = totalPaginaCon
						.add(new BigDecimal(beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono()));
			}
			try {
				totalImporteCon = boReparacion.obtenerOrdenesReparacionMontos(modulo, getArchitechBean(), sortField,
						sortType);
			} catch (BusinessException e) {
				showException(e);
			}
			/** seteo de datos al modelo actual **/
			model.addObject("importePagina", totalPaginaCon.toString());
			model.addObject("importeTotal", totalImporteCon.toString());
			model.addObject("regIni", regIniCon);
			model.addObject("regFin", regFinCon);
		}
	}

	/**
	 * Obtener el objeto: message source.
	 * @return El objeto: message source se retorna la variable
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}


	/**
	 * Definir el objeto: bo init modulo SPID.
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}

	/**
	 * Obtener el objeto: bo ordenes.
	 *
	 * @return El objeto: bo ordenes
	 */
	public BOOrdenes getBoOrdenes() {
		return boOrdenes;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}
	/**
	 * Definir el objeto: bo ordenes.
	 *
	 * @param boOrdenes El nuevo objeto: bo ordenes
	 */
	public void setBoOrdenes(BOOrdenes boOrdenes) {
		this.boOrdenes = boOrdenes;
	}

	/**
	 * Obtener el objeto: bo reparacion.
	 *
	 * @return El objeto: bo reparacion
	 */
	public BOReparacion getBoReparacion() {
		return boReparacion;
	}
	
	/**
	 * Definir el objeto: bo reparacion.
	 *
	 * @param boReparacion El nuevo objeto: bo reparacion
	 */
	public void setBoReparacion(BOReparacion boReparacion) {
		this.boReparacion = boReparacion;
	}
}
