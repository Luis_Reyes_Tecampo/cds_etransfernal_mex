/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerExportarPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:32 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOPagos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportarPagos;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasWebPagos;

/**
 * Class ControllerPagos.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * pagos y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerExportarPagos extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4906730840033386283L;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "pagos";
	
	/** La constante BEAN_PAGOS. */
	private static final String BEAN_PAGOS = "beanResListadoPagos";
	
	/** La constante TITULO. */
	private static final String TITULO = "titulo";
	
	/** La variable que contiene informacion con respecto a: bo pagos. */
	private BOPagos boPagos;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La constante utilsExportar. */
	private static final UtileriasExportarPagos utilsExportar = UtileriasExportarPagos.getUtils();
	
	/** La constante TIPO_ORDEN. */
	private static final String TIPO_ORDEN = "tipoOrden";
	
	/** La constante utils. */
	private static final UtileriasWebPagos utils = UtileriasWebPagos.getUtils();
	
	/** La constante CONST_REF. */
	private static final String CONST_REF = "refModel";
	
	/**
	 * Exp bitacora admon.
	 * 
	 * Mapeo del flujo para exportar los datos 
	 * de la pantalla a un archivo de excel.
	 *
	 * @param beanResListadoPagos El objeto: bean res listado pagos
	 * @param tipoOrden El objeto: tipo orden
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarPagos.do")
    public ModelAndView expBitacoraAdmon(@Valid
    		@ModelAttribute(BEAN_PAGOS)BeanResListadoPagos beanResListadoPagos,
    		@RequestParam("tipoRef") String tipoOrden, BindingResult bindingResult){
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		ModelAndView model = null;
		FormatCell formatCellBody = null;
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = utils.getServlet();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setFontName("Calibri");
		formatCellBody.setBold(false);
		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setBold(true);
		formatCellHeader.setFontSize((short) 11);
		model = utilsExportar.getModel();
		model.addObject("FORMAT_HEADER", formatCellHeader);
		model.addObject("FORMAT_CELL", formatCellBody);
		model.addObject("HEADER_VALUE", utilsExportar.getHeaderExcel(utilsExportar.getRequest(req)));
		model.addObject("BODY_VALUE", utilsExportar.getBody(beanResListadoPagos.getListadoPagos()));
		model.addObject("NAME_SHEET", utils.obtenerNombre(tipoOrden));
		/** Retorno del modelo **/
		return model;
		
	}
	
	/**
	 * Exp todos bitacora admon.
	 * 
	 * Mapeo del flujo para exportar todos los registros
	 * a BD para el proceso batch.
	 *
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param beanModulo El objeto: bean modulo
	 * @param beanResListadoPagos El objeto: bean res listado pagos
	 * @param tipoOrden El objeto: tipo orden
	 * @param importe El objeto: importe
	 * @param paginador El objeto: paginador
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarTodoPagos.do")
    public ModelAndView expTodosBitacoraAdmon(BeanReqListadoPagos beanReqOrdeReparacion,
    		@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
    		@Valid @ModelAttribute(BEAN_PAGOS)BeanResListadoPagos beanResListadoPagos,
    		@Valid @RequestParam("tipoRef") String tipoOrden,
    		@Valid @RequestParam("importeTotal") String importe,
    		BeanPaginador paginador, BindingResult bindingResult){
   	 	ModelAndView model = null; 
   	 	BeanResListadoPagos response = null;
   	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
   	 	/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
   	 	try { 		
   	 		/** Se realiza la peticion al BO **/
   	 		response = boPagos.exportarTodoListadoPagos(beanModulo, utils.obtenerNombre(tipoOrden), tipoOrden, beanReqOrdeReparacion, getArchitechBean());
 			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError(),new Object[]{response.getNombreArchivo()}, null); 			
 			model = new ModelAndView(PAGINA_PRINCIPAL);
 			response.setListadoPagos(beanResListadoPagos.getListadoPagos());
 			response.setBeanPaginador(paginador);
 			response.setImporteTotal(importe);
 			response.setTotalReg(beanResListadoPagos.getTotalReg());
 			model.addObject(TIPO_ORDEN, tipoOrden);
 			model.addObject(CONST_REF, tipoOrden);
 			model.addObject(TITULO,  utils.obtenerTitulo(tipoOrden, beanModulo.getModulo()));
 			model.addObject(Constantes.COD_ERROR, response.getCodError());
 			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
 			model.addObject(Constantes.DESC_ERROR, mensaje);
 			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
 			response.setBeanPaginador(paginador);
 			response.setTotalReg(beanReqOrdeReparacion.getTotalReg());
 			model.addObject(BEAN_PAGOS,response);
 			generarContadoresPag(model, response);
 	   } catch (BusinessException e) {
 		  showException(e);
 	   }
   	 	/** Retorno del modelo **/
        return model;
	}
	
	/**
	 * Generar contadores pag.
	 *
	 * Metodo para validar la paginacion
	 * 
	 * @param modelAndView El objeto: model and view
	 * @param beanResListadoPagos El objeto: bean res listado pagos
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIniExp = 1;
			 Integer regFinExp = beanResListadoPagos.getTotalReg();
			
			/** Se valida el total de paginas*/
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIniExp = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1);
			}
			/** Se valida el valor de los registros*/
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFinExp = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporteExp = BigDecimal.ZERO;
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImporteExp = totalImporteExp.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			modelAndView.addObject("importeTotal", beanResListadoPagos.getImporteTotal());
			modelAndView.addObject("importePagina", totalImporteExp.toString());
			modelAndView.addObject("regIni", regIniExp);
			modelAndView.addObject("regFin", regFinExp);
		}
	}
	
	
	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Obtener el objeto: bo pagos.
	 *
	 * @return El objeto: bo pagos
	 */
	public BOPagos getBoPagos() {
		return boPagos;
	}

	

	
	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo pagos.
	 *
	 * @param boPagos El nuevo objeto: bo pagos
	 */
	public void setBoPagos(BOPagos boPagos) {
		this.boPagos = boPagos;
	}
	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
