/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaNominaCutHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/11/2016 12:42:00 IDS		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.admonSaldos;

import java.util.HashMap;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.admonSaldo.BeanReqConsNominaCUT;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTHist;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.admonSaldos.BOConsultaNominaCUTHist;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ControllerConsultaNominaCutHist extends Architech {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6359148343948339101L;

	/**
	 * Propiedad del tipo String que almacena el valor de PAGINA
	 */
	private static final String PAGINA = "consultaNominaCUTHist";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN
	 */
	private static final String BEAN = "beanRes";
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_REQ
	 */
	private static final String BEAN_REQ = "beanReq";

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BOConsultaNominaCUTDia que almacena el valor de bOConsultaNominaCUTDia
	 */
	private BOConsultaNominaCUTHist bOConsultaNominaCUTHist;
	/**
	 * @return ModelAndView Objeto de
	 *  Spring MVC 
	 */
	@RequestMapping(value = "/muestraConsultaNominaCutHist.do")
	ModelAndView muestraConsultaNominaCutHist(){
		Map<String,Object> map = new HashMap<String,Object>();
		ModelAndView modelAndView = null;
		BeanResConsNomCUTHist beanResConsNomCUTHist = null;
		modelAndView = new ModelAndView(PAGINA);
		beanResConsNomCUTHist = bOConsultaNominaCUTHist.consultar(getArchitechBean());
		map = modelAndView.getModel();
		map.put(BEAN, beanResConsNomCUTHist);
		return modelAndView;
	}

	/**
	 * Metodo que permite realizar la consulta de la nomina CUT
	 * @param req Bean con los datos de entrada
	 * @return ModelAndView Objeto de Spring MVC
	 */
	@RequestMapping(value = "/consultaNominaCUTHist.do")
	ModelAndView consultaNominaCutHist(BeanReqConsNominaCUT req){
		Map<String,Object> map = null;
		ModelAndView modelAndView = null;
		BeanResConsNomCUTHist beanResConsNomCUTHist = null;
		modelAndView = new ModelAndView(PAGINA);
		map = modelAndView.getModel();
		map.put(BEAN_REQ, req);
		if (StringUtils.EMPTY.equals(req.getFechaOperacion())){
			map.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	  	    map.put(Constantes.COD_ERROR, "ED00123V");
			map.put(Constantes.DESC_ERROR, messageSource.getMessage("codError.ED00123V", null, null));
			beanResConsNomCUTHist = bOConsultaNominaCUTHist.consultar(getArchitechBean());
			map = modelAndView.getModel();
			map.put(BEAN, beanResConsNomCUTHist);
			return modelAndView;
		}
		beanResConsNomCUTHist = bOConsultaNominaCUTHist.consultarNomina(req,getArchitechBean());
		if(!Errores.OK00000V.equals(beanResConsNomCUTHist.getCodError())){
			map.put(Constantes.TIPO_ERROR, beanResConsNomCUTHist.getTipoError());
	  	    map.put(Constantes.COD_ERROR, beanResConsNomCUTHist.getCodError());
			map.put(Constantes.DESC_ERROR, messageSource.getMessage("codError."+beanResConsNomCUTHist.getCodError(), null, null));
		}
		
		map.put(BEAN, beanResConsNomCUTHist);
		return modelAndView;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOConsultaNominaCUTHist
	 * @return bOConsultaNominaCUTHist Objeto del tipo BOConsultaNominaCUTHist
	 */
	public BOConsultaNominaCUTHist getBOConsultaNominaCUTHist() {
		return bOConsultaNominaCUTHist;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOConsultaNominaCUTHist
	 * @param consultaNominaCUTHist del tipo BOConsultaNominaCUTHist
	 */
	public void setBOConsultaNominaCUTHist(
			BOConsultaNominaCUTHist consultaNominaCUTHist) {
		bOConsultaNominaCUTHist = consultaNominaCUTHist;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	
	
}
