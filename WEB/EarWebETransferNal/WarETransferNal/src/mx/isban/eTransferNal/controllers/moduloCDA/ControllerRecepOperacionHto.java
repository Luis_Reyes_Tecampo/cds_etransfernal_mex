/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerRecepOperacionHto.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018      SMEZA		VECTOR      Creacion
 */
package mx.isban.eTransferNal.controllers.moduloCDA;
/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones Historico.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.SPEI.BODevExtemporanea;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de Admon Saldos - Recepcion de Operaciones Historico.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepOperacionHto extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;
		
	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BORecepOperacion bORecepOperacion;
	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BODevExtemporanea bODevExtemporanea;

	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modeloVista;
	
	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();
	
	/**
	 * funcion para aplicar una devolucion extemporanea.
	 *
	 * @param beanResTranSpeiRec the bean res tran spei rec
	 * @param beanReqConsTranSpeiRec El objeto: bean req cons tran spei rec
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/devolucionExtemporanea.do")
	public ModelAndView devolicionExtemporanea(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpeiRec,HttpServletRequest req,
			HttpServletResponse res,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper){
		modeloVista = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = beanResTranSpeiRec;
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpeiRec);
		beanReqConsTranSpeiRec.getPaginador().setAccion("ACT");	
	
		
		try {
			String mensaje = null;
			RequestContext ctx = new RequestContext(req);
			BeanResTranSpeiRec recepOperaHto = null;			
			recepOperaHto = bODevExtemporanea.devolucionExtemporanea(beanReqConsTranSpeiRec, getArchitechBean(), historico);
			
			/**Se llama a la vista a mostrar o pantalla **/
			modeloVista =  util.vistaMostrar(beanReqConsTranSpeiRec,recepOperaAdmonSaldos,historico);
		
		   if("OK00021V".equals(recepOperaHto.getBeanError().getCodError())) {		
			   String referencia="0000000";
			   if(recepOperaHto.getListBeanTranSpeiRecOK().get(0).getBeanDetalle().getDetEstatus().getRefeTransfer()!=null) {
				   referencia=recepOperaHto.getListBeanTranSpeiRecOK().get(0).getBeanDetalle().getDetEstatus().getRefeTransfer();
			   }
			   //se asigna el mensaje para la mostrar el exito de la devolucion extemporanea
			    mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+recepOperaHto.getBeanError().getCodError(),new Object[]{referencia, 
			    		recepOperaHto.getListBeanTranSpeiRecOK().get(0).getBeanDetalle().getDetEstatus().getEstatusTransfer(),
			    		recepOperaHto.getListBeanTranSpeiRecOK().get(0).getBeanDetalle().getDetalleGral().getFolioPaquete(),
			    		recepOperaHto.getListBeanTranSpeiRecOK().get(0).getBeanDetalle().getDetalleGral().getFolioPago()});
		    }else{
			   mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+recepOperaHto.getBeanError().getCodError());
		    }	   
			modeloVista.addObject(Constantes.COD_ERROR, recepOperaHto.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, recepOperaHto.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject("sessionBean", sessionBean); 
		}catch (BusinessException e) {
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage());
			showException(e);
		}
		return modeloVista;
 	}
	
	
	/**
	 * Enviar hto.
	 *
	 * @param beanReqConsTranSpeiRec El objeto: bean req cons tran spei rec
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping(value="/enviarHto.do")
	public ModelAndView enviarHto(BeanReqTranSpeiRec beanReqConsTranSpeiRec,HttpServletRequest req,
			HttpServletResponse res,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper){
		modeloVista = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpeiRec);
		beanReqConsTranSpeiRec.getPaginador().setAccion("ACT");	
		try {
			String mensaje = null;
			RequestContext ctx = new RequestContext(req);
			BeanResTranSpeiRec recOperaHto = null;			
			recOperaHto = bODevExtemporanea.enviarHTO(beanReqConsTranSpeiRec, getArchitechBean(), historico);
			 
			/**Se hace la busqueda de los datos**/			
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpeiRec, getArchitechBean(), historico,beanTranSpeiRecOper);
			/**Se asigna el nombre de la pantalla y la opcion elegida **/
			
			/**Se llama a la vista a mostrar o pantalla **/
			modeloVista =  util.vistaMostrar(beanReqConsTranSpeiRec,recepOperaAdmonSaldos,historico);
		
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+recOperaHto.getBeanError().getCodError());
		       
			modeloVista.addObject(Constantes.COD_ERROR, recOperaHto.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, recOperaHto.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject("sessionBean", sessionBean); 
		}catch (BusinessException e) {
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage());
			showException(e);
		}
		return modeloVista;
	 }
	
	/**
	 * Obtener el objeto: b O recep operacion hto.
	 *
	 * @return the bORecepOperacionHto
	 */
	public BORecepOperacion getbORecepOperacionHto() {
		return bORecepOperacion;
	}

	/**
	 * Definir el objeto: b O recep operacion.
	 *
	 * @param bORecepOperacion El nuevo objeto: b O recep operacion
	 */
	public void setbORecepOperacion(BORecepOperacion bORecepOperacion) {
		this.bORecepOperacion = bORecepOperacion;
	}

	/**
	 * Obtener el objeto: b O dev extemporanea.
	 *
	 * @return the bODevExtemporanea
	 */
	public BODevExtemporanea getbODevExtemporanea() {
		return bODevExtemporanea;
	}

	/**
	 * Definir el objeto: b O dev extemporanea.
	 *
	 * @param bODevExtemporanea the bODevExtemporanea to set
	 */
	public void setbODevExtemporanea(BODevExtemporanea bODevExtemporanea) {
		this.bODevExtemporanea = bODevExtemporanea;
	}

	

	

}