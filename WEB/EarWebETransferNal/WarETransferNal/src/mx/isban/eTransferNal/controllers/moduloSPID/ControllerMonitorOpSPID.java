/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorOpSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorOpSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloSPID.BORMonitorOpSPID;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerMonitorOpSPID extends Architech{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3778649778350345090L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE = "monitorOperacionSPID";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	private static final String BEAN_RES = "beanRes";
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BORMonitorOpSPID que almacena el valor de bORMonitorOpSPID
	 */
	private BORMonitorOpSPID bORMonitorOpSPID;

	 /**
	  * Metodo que se utiliza para mostrar la pagina de monitor de operaciones SPID
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraMonitorOpSPID.do")
	 public ModelAndView muestraMonitorOp() {
		 ModelAndView modelAndView = null;
		 BeanResMonitorOpSPID beanResMonitorOpSPID = bORMonitorOpSPID.obtenerDatosMonitorSPID(getArchitechBean());
		 modelAndView = new ModelAndView(PAGE,BEAN_RES,beanResMonitorOpSPID);
        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResMonitorOpSPID.getCodError(), null, null);
     	modelAndView.addObject(Constantes.COD_ERROR, beanResMonitorOpSPID.getCodError());
     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResMonitorOpSPID.getTipoError());
     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		 return modelAndView;
	 }

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bORMonitorOpSPID
	 * @return bORMonitorOpSPID Objeto del tipo BORMonitorOpSPID
	 */
	public BORMonitorOpSPID getBORMonitorOpSPID() {
		return bORMonitorOpSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bORMonitorOpSPID
	 * @param monitorOpSPID del tipo BORMonitorOpSPID
	 */
	public void setBORMonitorOpSPID(BORMonitorOpSPID monitorOpSPID) {
		bORMonitorOpSPID = monitorOpSPID;
	}
	 
	 
}
