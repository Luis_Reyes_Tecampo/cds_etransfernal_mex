/**
 * Clase de tipo controller para procesar la pantalla
 * de Autorizar Operaciones
 * 
 * @author: Vector
 * @since: Marzo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpe;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOAutorizarOperaciones;
import mx.isban.eTransferNal.utilerias.MetodosAutorizarOperaciones;
import mx.isban.eTransferNal.utilerias.UtilsCapturasManuales;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * The Class ControllerAutorizarOperaciones.
 */
@Controller
public class ControllerAutorizarOperaciones extends Architech{

	/** La Variable de serializacion. */
	private static final long serialVersionUID = -2057107837780603220L;

	/**  Constante de la pagina autorizacion operaciones. */
	private static final String PAGINA_AUTORIZACION_OPERACIONES = "autorizacionOperaciones";
	
	/** The Constant BEAN_RESPONSE. */
	private static final String BEAN_RESPONSE = "beanResponse";
	
	/** The Constant STR_COD_ERROR. */
	private static final String STR_COD_ERROR = "codError.";
	
	/** Propiedad del tipo BOAutorizarOperaciones que almacena el valor de boAutorizarOperaciones. */
	private transient BOAutorizarOperaciones boAutorizarOperaciones;
	
	/**
	 * Pantalla autorizar operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/mostrarOperacionesPendientes.do")
	public ModelAndView pantallaAutorizarOperaciones(HttpServletRequest request, HttpServletResponse response){
		ModelAndView modelAndView=new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanResAutorizarOpe beanUsrAdm=new BeanResAutorizarOpe();
		//se consulta si usr logado es administrador
		try{
			beanUsrAdm = boAutorizarOperaciones.consultarUsrAdmin(getArchitechBean());
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}

		modelAndView.addObject(BEAN_RESPONSE, beanUsrAdm);
		return modelAndView;
	}
	
	/**
	 * Consultar operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//Metodo para consulta de operaciones
	@RequestMapping(value = "/consultarOperacionesPendientes.do")
	public ModelAndView consultarOperaciones(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);
		
		BeanResAutorizarOpe beanResConsOperacionesPend=new BeanResAutorizarOpe();
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

		try{
			beanResConsOperacionesPend = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			
			beanResConsOperacionesPend.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
			beanResConsOperacionesPend.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
			beanResConsOperacionesPend.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
			
			modelAndView=new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES, BEAN_RESPONSE, beanResConsOperacionesPend);
			
			if(!Errores.OK00000V.equals(beanResConsOperacionesPend.getCodError())){
				modelAndView.addObject(Constantes.TIPO_ERROR, beanResConsOperacionesPend.getTipoError());
				modelAndView.addObject(Constantes.COD_ERROR, beanResConsOperacionesPend.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, beanResConsOperacionesPend.getMsgError());
			}
		}catch(BusinessException e){
			//Se procesa excepcion
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		
		return modelAndView;
	}
	
	/**
	 * Apartar operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/apartar.do")
	public ModelAndView apartarOperaciones(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);

		List<BeanAutorizarOpe> listOpeSelec=utilsAutorizarOper.obtenerRegistrosSeleccionados(beanReqConsOperacionesPend);
		BeanResAutorizarOpe beanResConsOperacionesPend= new BeanResAutorizarOpe();
		BeanResAutorizarOpe beanRespartarOpe= new BeanResAutorizarOpe();
	    RequestContext ctx = new RequestContext(request);
		boolean seleccionValida=true;
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

		modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		
		try{
			if( !listOpeSelec.isEmpty() ){
				//verificar si las operaciones seleccionadas estan apartadas
				seleccionValida = utilsAutorizarOper.validaOpeSelectPorApartar(listOpeSelec);
				if(seleccionValida){
					beanRespartarOpe = boAutorizarOperaciones.apartarOPeraciones(getArchitechBean(), listOpeSelec);
					modelAndView.addObject(Constantes.TIPO_ERROR, beanRespartarOpe.getTipoError() );
					modelAndView.addObject(Constantes.COD_ERROR, beanRespartarOpe.getCodError() );
					modelAndView.addObject(Constantes.DESC_ERROR, beanRespartarOpe.getMsgError() );
				} else {
					//una de las operaciones seleccionadas ya esta apartada
					modelAndView.addObject(Constantes.DESC_ERROR, ctx.getMessage( STR_COD_ERROR+Errores.CMCO001V ) );
				}
			} else {
				//no se selecciono ninguna operacion
				modelAndView.addObject(Constantes.DESC_ERROR, ctx.getMessage( STR_COD_ERROR+Errores.CMCO002V ) );
			}

			//se realiza consulta de operaciones para repintado de informacion
			beanResConsOperacionesPend = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			beanResConsOperacionesPend.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
			beanResConsOperacionesPend.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
			beanResConsOperacionesPend.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
		}catch(BusinessException e){
			showException(e);
			//se llama funcion para mostrar Excepcion y obtner codigo y mensaje de error
			utilsAutorizarOper.tratarBusinessException(modelAndView, e);
		}
		
		modelAndView.addObject(BEAN_RESPONSE, beanResConsOperacionesPend);
		return modelAndView;
	}
	
	/**
	 * Desapartar operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/desapartar.do")
	public ModelAndView desapartarOperaciones(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);

		BeanResAutorizarOpe beanResConsOperacionesPend=new BeanResAutorizarOpe();
		BeanResAutorizarOpe beanResDesapartarOpe= new BeanResAutorizarOpe();
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();
		
		try{
			beanResDesapartarOpe = boAutorizarOperaciones.desapartarOPeraciones(getArchitechBean(), beanReqConsOperacionesPend);
			modelAndView.addObject(Constantes.TIPO_ERROR, beanResDesapartarOpe.getTipoError() );
			modelAndView.addObject(Constantes.DESC_ERROR, beanResDesapartarOpe.getMsgError() );
			modelAndView.addObject(Constantes.COD_ERROR, beanResDesapartarOpe.getCodError() );

			//se realiza consulta de operaciones para repintado de informacion
			beanResConsOperacionesPend = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			beanResConsOperacionesPend.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
			beanResConsOperacionesPend.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
			beanResConsOperacionesPend.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
		}catch(BusinessException e){
			//se llama funcion para mostrar Excepcion y obtner codigo y mensaje de error
			utilsAutorizarOper.tratarBusinessException(modelAndView, e);
		}
		
		modelAndView.addObject(BEAN_RESPONSE, beanResConsOperacionesPend);
		return modelAndView;
	}

	/**
	 * Autorizar operacion.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/autorizarOperacion.do")
	public ModelAndView autorizarOperacion(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);

		List<BeanAutorizarOpe> listOpeSelec=utilsAutorizarOper.obtenerRegistrosSeleccionados(beanReqConsOperacionesPend);
		BeanResAutorizarOpe beanResponseAut=new BeanResAutorizarOpe();
	    RequestContext ctx = new RequestContext(request);
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

	    String  usuario = getArchitechBean().getUsuario();
		int estatusOpe = 1;

		modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		
		try{
			//metodo que valida las operaciones seleccionadas por el usuario
			validarOperacionesSeleccionadas(modelAndView, ctx, listOpeSelec, usuario, estatusOpe);

			//se realiza consulta de operaciones para repintado de informacion
			beanResponseAut = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			beanResponseAut.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
			beanResponseAut.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
			beanResponseAut.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
		}catch(BusinessException e){
			utilsAutorizarOper.tratarBusinessException(modelAndView, e);
		}
		
		modelAndView.addObject(BEAN_RESPONSE, beanResponseAut);
		return modelAndView;
	}
	
	/**
	 * Reparar operacion.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/repararOperacion.do")
	public ModelAndView repararOperacion(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);

		List<BeanAutorizarOpe> listOpeSelec=utilsAutorizarOper.obtenerRegistrosSeleccionados(beanReqConsOperacionesPend);
		BeanResAutorizarOpe beanResponseRepar=new BeanResAutorizarOpe();
	    RequestContext ctx = new RequestContext(request);
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

	    String  usuario = getArchitechBean().getUsuario();
		int estatusOpe = 2;

		modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		
		try{
			//metodo que valida las operaciones seleccionadas por el usuario
			validarOperacionesSeleccionadas(modelAndView, ctx, listOpeSelec, usuario, estatusOpe);
			
			//se realiza consulta de operaciones para repintado de informacion
			beanResponseRepar = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			beanResponseRepar.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
			beanResponseRepar.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
			beanResponseRepar.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
		}catch(BusinessException e){
			//se llama funcion para mostrar Excepcion y obtner codigo y mensaje de error
			showException(e);
			utilsAutorizarOper.tratarBusinessException(modelAndView, e);
		}
		
		modelAndView.addObject(BEAN_RESPONSE, beanResponseRepar);
		return modelAndView;
	}
	
	/**
	 * Cancelar operacion.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping(value = "/cancelarOPeracion.do")
	public ModelAndView cancelarOperacion(HttpServletRequest request, HttpServletResponse response){
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		ModelAndView modelAndView = new ModelAndView(PAGINA_AUTORIZACION_OPERACIONES);
		BeanReqAutorizarOpe beanReqConsOperacionesPend = utilsAutorizarOper.requestToBeanReqAutorizarOpe(request, modelAndView);

		List<BeanAutorizarOpe> listOpeSelec=utilsAutorizarOper.obtenerRegistrosSeleccionados(beanReqConsOperacionesPend);
		BeanResAutorizarOpe beanResponseCancel=new BeanResAutorizarOpe();
	    RequestContext ctx = new RequestContext(request);
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

	    String  usuario = getArchitechBean().getUsuario();
		int estatusOpe = 3;

		modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		
		try{
			//metodo que valida las operaciones seleccionadas por el usuario
			validarOperacionesSeleccionadas(modelAndView, ctx, listOpeSelec, usuario, estatusOpe);

			//se realiza consulta de operaciones para repintado de informacion
			beanResponseCancel = boAutorizarOperaciones.consultarOperacionesPendientes(getArchitechBean(), beanReqConsOperacionesPend);
			
			beanResponseCancel.setTemplate(utils.eliminarInjeccionDeCodigo(beanReqConsOperacionesPend.getTemplate()));
			beanResponseCancel.setFechaOperacion(beanReqConsOperacionesPend.getFechaCaptura());
			beanResponseCancel.setUsrAdmin(beanReqConsOperacionesPend.getUsrAdmin());
		}catch(BusinessException e){
			//se llama funcion para mostrar Excepcion y obtner codigo y mensaje de error
			utilsAutorizarOper.tratarBusinessException(modelAndView, e);
		}
		
		modelAndView.addObject(BEAN_RESPONSE, beanResponseCancel);
		return modelAndView;
	}
	
	/**
	 * Validar operaciones seleccionadas.
	 *
	 * @param modelAndView the model and view
	 * @param req the req
	 * @param ctx the ctx
	 * @param listOpeSelec the list ope selec
	 * @param usuario the usuario
	 * @param estatusOpe the estatus ope
	 * @throws BusinessException the business exception
	 */
	private void validarOperacionesSeleccionadas(ModelAndView modelAndView, RequestContext ctx,
			List<BeanAutorizarOpe> listOpeSelec, String usuario, int estatusOpe) throws BusinessException {
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Valida si la lista esta vacia
		if( !listOpeSelec.isEmpty() ){
			if( utilsAutorizarOper.validarOperacionesApartadas(listOpeSelec) ){
				// si el usuario no tiene permisos sobre la operacion
				if( utilsAutorizarOper.validarPermisosSobreOperacion( listOpeSelec, usuario ) ){
						//se realiza el cambio de estatus de operacion
						autCanRepOperaciones(listOpeSelec, estatusOpe, modelAndView, ctx);
				} else {
					//el usuario no tiene permisos sobre la operacion
					modelAndView.addObject(Constantes.DESC_ERROR, ctx.getMessage( STR_COD_ERROR+Errores.CMCO020V ) );
				}
			} else {
				//La operacion no esta apartada por el usuario
				modelAndView.addObject(Constantes.DESC_ERROR, ctx.getMessage( STR_COD_ERROR+Errores.CMCO001V ) );
			}
		} else {
			//no se selecciono ninguna operacion
			modelAndView.addObject(Constantes.DESC_ERROR, ctx.getMessage( STR_COD_ERROR+Errores.CMCO002V ) );
		}
	}
	
	/**
	 * Aut can rep operaciones.
	 *
	 * @param listOpeSelec the list ope selec
	 * @param estatusOpe the estatus ope
	 * @param modelAndView the model and view
	 * @param ctx the ctx
	 * @throws BusinessException the business exception
	 */
	private void autCanRepOperaciones(List<BeanAutorizarOpe> listOpeSelec,
			int estatusOpe, ModelAndView modelAndView, RequestContext ctx) throws BusinessException{
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		BeanResAutorizarOpe beanRespOperacion=new BeanResAutorizarOpe();
		boolean opePendAut = utilsAutorizarOper.validaEstatusOper(listOpeSelec,estatusOpe);
		
		if(opePendAut){
			if(estatusOpe==1){
				//operaciones autorizadas correctamente
				beanRespOperacion = boAutorizarOperaciones.autorizarOperaciones(getArchitechBean(), listOpeSelec);
				//se liberan las operaciones
				boAutorizarOperaciones.liberarOperacionAutorizada(getArchitechBean(), listOpeSelec);
			} else if(estatusOpe==2){
				//operaciones reparadas correctamente
				beanRespOperacion = boAutorizarOperaciones.repararOperaciones(getArchitechBean(), listOpeSelec);
				//se liberan las operaciones
				boAutorizarOperaciones.liberarOperacionAutorizada(getArchitechBean(), listOpeSelec);
			} else if(estatusOpe==3){
				//operaciones canceladas correctamente
				beanRespOperacion = boAutorizarOperaciones.cancelarOperaciones(getArchitechBean(), listOpeSelec);
				//se liberan las operaciones
				boAutorizarOperaciones.liberarOperacionAutorizada(getArchitechBean(), listOpeSelec);
			}
			//se obtiene cod y msg error de la operacion ejecutada
			modelAndView.addObject(Constantes.TIPO_ERROR, beanRespOperacion.getTipoError());
			modelAndView.addObject(Constantes.COD_ERROR, beanRespOperacion.getCodError());
			modelAndView.addObject(Constantes.DESC_ERROR, beanRespOperacion.getMsgError());
		} else {
			if( estatusOpe == 1 || estatusOpe == 3 ){
				//alguna de las operaciones no esta en estatus de PA
				modelAndView.addObject(Constantes.DESC_ERROR,  ctx.getMessage( STR_COD_ERROR+Errores.CMCO011V ) );
			} else {
				//alguna de las operaciones no esta en estatus de PA o AU
				modelAndView.addObject(Constantes.DESC_ERROR,  ctx.getMessage( STR_COD_ERROR+Errores.CMCO012V )  );
			}
		}
	}

	/**
	 * Sets the bo autorizar operaciones.
	 *
	 * @param boAutorizarOperaciones the new bo autorizar operaciones
	 */
	public void setBoAutorizarOperaciones(
			BOAutorizarOperaciones boAutorizarOperaciones) {
		this.boAutorizarOperaciones = boAutorizarOperaciones;
	}
}