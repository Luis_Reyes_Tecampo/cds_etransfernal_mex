/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 16:54:08 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaBO;
import mx.isban.eTransferNal.servicio.moduloCDA.BOMonitorCDA;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la consulta de Movimientos CDAs
 *
 */
@Controller
public class ControllerMonitorCDA extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -8396037496907638074L;
	
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOMonitorCDA boMonitorCDA;

	 /**
	  * Metodo que se utiliza para mostrar la pagina de monitorCDA
	  * monitor CDA
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraMonitorCDA.do")
	 public ModelAndView muestraMonitorCDA(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = consInfMonCDA(req,res);
		 return modelAndView;
	 }
	 
	/**
	 * Metodo que solicita la informacion para el monitor CDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/consInfMonCDA.do")
	public ModelAndView consInfMonCDA(HttpServletRequest req,
			HttpServletResponse res){
		ModelAndView modelAndView = null;      
		RequestContext ctx = new RequestContext(req);
	    BeanResMonitorCdaBO beanResMonitorCDA = null;
        try {
    	    beanResMonitorCDA = boMonitorCDA.obtenerDatosMonitorCDA(getArchitechBean());
    	    debug("consInfMonCDA boMonitorCDA.obtenerDatosMonitorCDA Codigo de error:"+beanResMonitorCDA.getCodError());
    	    modelAndView = new ModelAndView("monitorCDA","beanResMonitorCDA",beanResMonitorCDA);
	        String mensaje = ctx.getMessage("codError."+beanResMonitorCDA.getCodError());
	     	modelAndView.addObject("codError", beanResMonitorCDA.getCodError());
	     	modelAndView.addObject("tipoError", beanResMonitorCDA.getTipoError());
	     	modelAndView.addObject("descError", mensaje);
 	    } catch (BusinessException e) {
 		 showException(e);
 	    }
	       return modelAndView;	    
	 }
	
	
	/**
	 * Metodo que manda la solicitud para detener servicio CDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/detenerServicioMonCDA.do")
	public ModelAndView detenerServicioMonCDA(HttpServletRequest req,
			HttpServletResponse res){
		ModelAndView modelAndView = null;      
		RequestContext ctx = new RequestContext(req);
	       BeanResMonitorCdaBO beanResMonitorCDA = null;
	       try {
	    	   beanResMonitorCDA = boMonitorCDA.detenerServicioCDA(getArchitechBean());
	    	   modelAndView = new ModelAndView("monitorCDA","beanResMonitorCDA",beanResMonitorCDA);
	      	  String mensaje = ctx.getMessage("codError."+beanResMonitorCDA.getCodError());
	     	  modelAndView.addObject("codError", beanResMonitorCDA.getCodError());
	     	  modelAndView.addObject("tipoError", beanResMonitorCDA.getTipoError());
	     	  modelAndView.addObject("descError", mensaje);
	 	  } catch (BusinessException e) {
	 		showException(e);
	 	  }
	 	  
	       return modelAndView;	
	}

	/**
	 * Metodo set del servicio de Monitor CDA
	 * @param boMonitorCDA Objeto del tipo BOMonitorCDA
	 */
	public void setBoMonitorCDA(BOMonitorCDA boMonitorCDA) {
		this.boMonitorCDA = boMonitorCDA;
	}

	/**
	 * Metodo set del servicio de monitor CDA 
	 * @return Objeto del tipo BOMonitorCDA
	 */
	public BOMonitorCDA getBoMonitorCDA() {
		return boMonitorCDA;
	}

}
