/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaMovCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 16:54:08 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *   2.0   30/12/2016    Adrian Ricardo Martinez VECTOR SF Se agrega modulo CDA SPID
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCdaFiltrosParteDos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCdaFiltrosParteUno;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCDA;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOConsultaMovCDA;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 * Clase que se encarga de que se encarga de recibir y procesar las peticiones
 * para la consulta de Movimientos CDAs
 * 
 */
@Controller
public class ControllerConsultaMovCDA extends Architech {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**
	 * Constante para establecer la pagina de servicio
	 */
	private static final String PAGINA_SERVICIO = "consultaMovCDA";
	
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsMovCDA";
	
	/**
	 * Constante para establecer nombre del bean de detalle
	 */
	private static final String BEAN_RESULTADO_CONSULTA_DETALLE = "beanConsMovDetCDA";
	
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String PAGINA_SERVICIO_DETALLE = "consultaDetMovCDA";
	
	/**
	 * Constante para el parametro de OpeContingencia
	 */
	private static final String OPE_CONTINGENCIA = "paramOpeContingencia";
	/**
	 * Constante para el parametro de paramEstatusCDA
	 */
	private static final String ESTATUS_CDA = "paramEstatusCda";
	/**
	 * Constante para el parametro de fechaOperacion
	 */
	private static final String FECHA_OPERACION = "paramFechaOperacion";
	
	/**
	 * Constante que almacena parametro del tipo de pago
	 */
	private static final String PARAM_TIPO_PAGO = "paramTipoPago";
    
	/**Constante con la cadena codError.*/
    private static final String COD_ERROR_PUNTO = "codError.";
 
    /**Constante con la cadena codError*/
    private static final String COD_ERROR = "codError";

    /**Constante con la cadena tipoError*/
    private static final String TIPO_ERROR = "tipoError";
 
    /**Constante con la cadena descError*/
    private static final String DESC_ERROR = "descError";
	
    
    /**Constante con la cadena paramHoraAbonoIni*/
    private static final String PARAM_HORA_ABONO_INI = "paramHoraAbonoIni";
    /**Constante con la cadena paramHoraAbonoFin*/
    private static final String PARAM_HORA_ABONO_FIN = "paramHoraAbonoFin";
    /**Constante con la cadena paramHoraEnvioIni*/
    private static final String PARAM_HORA_ENVIO_INI = "paramHoraEnvioIni";
    /**Constante con la cadena paramHoraEnvioFin*/
    private static final String PARAM_HORA_ENVIO_FIN = "paramHoraEnvioFin";
    /**Constante con la cadena paramCveSpei*/
    private static final String PARAM_CVE_SPEI = "paramCveSpei";
    /**Constante con la cadena paramDescripcion*/
    private static final String PARAM_DESCRIPCION = "paramDescripcion";;
    /**Constante con la cadena paramCveRastreo*/
    private static final String PARAM_CVE_RASTREO = "paramCveRastreo";
    /**Constante con la cadena paramRefTransfer*/
    private static final String PARAM_REF_TRANSFER = "paramRefTransfer";
    /**Constante con la cadena paramCtaBeneficiario*/
    private static final String PARAM_CTA_BENEFICIARIO = "paramCtaBeneficiario";
    /**Constante con la cadena paramMonto*/
    private static final String PARAM_MONTO = "paramMonto";
    /** Constante ES_SPID */
    private static final String ES_SPID = "esSPID";
    
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOConsultaMovCDA boConsultaMovCDA;

	 
	 
	/**
	 * Metodo que se utiliza para mostrar la pagina de Consulta Mov CDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/muestraConsMovCDA.do")
	public ModelAndView muestraMovCD(HttpServletRequest req) {
		RequestContext ctx = new RequestContext(req);
		BeanResConsMovCDA beanResConsMovCDA = null;
		ModelAndView modelAndView = null;
		try {
			String esSPID = req.getParameter(ES_SPID);
			Utilerias utilerias = Utilerias.getUtilerias();			
			beanResConsMovCDA = boConsultaMovCDA.consultarFechaOperacion(getArchitechBean(), Boolean.parseBoolean(req.getParameter(ES_SPID)));
			Date fecha = utilerias.stringToDate(beanResConsMovCDA.getFechaOperacion(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			String fechaOperacion = utilerias.formateaFecha(fecha,Utilerias.FORMATO_FECHA_LARGA_LETRA_DIA_SEMANA);						
			String fechaOperacion2 = utilerias.formateaFecha(fecha,Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			beanResConsMovCDA.setFechaOperacion(fechaOperacion2);
			modelAndView = new ModelAndView(PAGINA_SERVICIO,BEAN_RESULTADO_CONSULTA,beanResConsMovCDA);
			modelAndView.addObject("fechaOperacionLetra",fechaOperacion);			
	    	String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResConsMovCDA.getCodError());
	    	modelAndView.addObject(COD_ERROR, beanResConsMovCDA.getCodError());
	     	modelAndView.addObject(TIPO_ERROR, beanResConsMovCDA.getTipoError());
	     	modelAndView.addObject(DESC_ERROR, mensaje);
	     	modelAndView.addObject(ES_SPID, esSPID);
			
		} catch (BusinessException e) {
			showException(e);
		} 
		
		return modelAndView;
	}

	/**
	 * Metodo que se utiliza para realizar la consulta
	 * de movimientos CDA
	 * @param beanReqConsMovCDA Objeto del tipo BeanReqConsMovCDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovCDA.do")
	public ModelAndView consMovCDA(BeanReqConsMovCDA beanReqConsMovCDA,HttpServletRequest req) {
		
		ModelAndView modelAndView = null;
		
	    BeanResConsMovCDA beanResConsMovCDA = null;
	    RequestContext ctx = new RequestContext(req);

	    beanReqConsMovCDA = armarObjetoConParametros(beanReqConsMovCDA,req);

		beanReqConsMovCDA.setFechaOperacion(req.getParameter(FECHA_OPERACION));
		String fechaOpConLetra = req.getParameter("paramFechaOpLarga");
				
	    try {	    	
	    	beanResConsMovCDA = boConsultaMovCDA.consultarMovCDA(beanReqConsMovCDA,getArchitechBean());
	    	beanResConsMovCDA.setFechaOperacion(beanReqConsMovCDA.getFechaOperacion());
	    	modelAndView = new ModelAndView(PAGINA_SERVICIO,BEAN_RESULTADO_CONSULTA,beanResConsMovCDA);
	    	String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResConsMovCDA.getCodError());
	     	modelAndView.addObject(COD_ERROR, beanResConsMovCDA.getCodError());
	     	modelAndView.addObject(TIPO_ERROR, beanResConsMovCDA.getTipoError());
	     	modelAndView.addObject(DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia()?beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia():"");
		modelAndView.addObject(PARAM_HORA_ABONO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoIniDesde());
		modelAndView.addObject(PARAM_HORA_ABONO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoFinHasta());
		modelAndView.addObject(PARAM_HORA_ENVIO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioIniDesde());
		modelAndView.addObject(PARAM_HORA_ENVIO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioFinHasta());
		modelAndView.addObject(PARAM_CVE_SPEI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getCveSpeiOrdenanteAbono());
		modelAndView.addObject(PARAM_DESCRIPCION,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getDesc());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCveRastreo());
		modelAndView.addObject(PARAM_REF_TRANSFER,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getRefTransfer());
		modelAndView.addObject(PARAM_CTA_BENEFICIARIO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCtaBeneficiario());
		modelAndView.addObject(PARAM_MONTO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getMontoPago());
		modelAndView.addObject(ESTATUS_CDA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getEstatus());
		modelAndView.addObject(FECHA_OPERACION,beanReqConsMovCDA.getFechaOperacion());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getTipoPago());
		
		modelAndView.addObject("fechaOperacionLetra",fechaOpConLetra);
		modelAndView.addObject(ES_SPID, beanReqConsMovCDA.getEsSPID());
				
	    return modelAndView;
	}

	/**
	 * Metodo utilizado para realizar solicitar la exportacion de todos
	 * los registros
	 * @param beanReqConsMovCDA Objeto del tipo BeanReqConsMovCDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expConsMovCDA.do")
	public ModelAndView expConsMovCDA(BeanReqConsMovCDA beanReqConsMovCDA,HttpServletRequest req,HttpServletResponse res) {
		RequestContext ctx = new RequestContext(req);
    	FormatCell formatCellHeader = null;
    	FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
        BeanResConsMovCDA beanResConMovCDA = null;
        beanReqConsMovCDA = armarObjetoConParametros(beanReqConsMovCDA,req);
		beanReqConsMovCDA.setFechaOperacion(req.getParameter(FECHA_OPERACION));
        
        try {

            beanResConMovCDA = boConsultaMovCDA.consultarMovCDA(beanReqConsMovCDA,getArchitechBean());

        	if(Errores.OK00000V.equals(beanResConMovCDA.getCodError())||
      			   Errores.ED00011V.equals(beanResConMovCDA.getCodError())){
       		 formatCellHeader = new FormatCell();
       	     formatCellHeader.setFontName("Calibri");
       	     formatCellHeader.setFontSize((short)11);
       	     formatCellHeader.setBold(true);
       	     
       	     formatCellBody = new FormatCell();
       	     formatCellBody.setFontName("Calibri");
       	     formatCellBody.setFontSize((short)11);
       	     formatCellBody.setBold(false);
       	 	   
       	 	 modelAndView = new ModelAndView(new ViewExcel());
       	 	 modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
  	 	     modelAndView.addObject("FORMAT_CELL", formatCellBody);
       	 	 modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
       	 	 modelAndView.addObject("BODY_VALUE", getBody(beanResConMovCDA.getListBeanMovimientoCDA()));
       	 	 modelAndView.addObject("NAME_SHEET", "expConsMovCDA");
       	 	 
       	 	 modelAndView.addObject(ES_SPID, beanReqConsMovCDA.getEsSPID());
       	   }else{
       		 modelAndView = consMovCDA(beanReqConsMovCDA,req);
       		 String mensaje = ctx.getMessage("codError."+beanResConMovCDA.getCodError());
       	      modelAndView.addObject("codError", beanResConMovCDA.getCodError());
       	      modelAndView.addObject("tipoError", beanResConMovCDA.getTipoError());
       	      modelAndView.addObject("descError", mensaje);
       	   }
 	    } catch (BusinessException e) {
 		 showException(e);
 	    }

 	    return modelAndView;		
	}
	
	   /**
     * Metodo privado que sirve para obtener los datos para el excel
     * @param listBeanMovimientoCDA Objeto (List<BeanMovimientoCDA>) con el listado de objetos del tipo BeanMovimientoCDA
     * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
     */
    private List<List<Object>> getBody(List<BeanMovimientoCDA> listBeanMovimientoCDA){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	  if(listBeanMovimientoCDA!= null && 
 			   !listBeanMovimientoCDA.isEmpty()){
  	 		   objList = new ArrayList<List<Object>>();
 	   for(BeanMovimientoCDA beanMovimientoCDA : listBeanMovimientoCDA){ 
 		  objListParam = new ArrayList<Object>();
 		  objListParam.add(beanMovimientoCDA.getReferencia());
 		  objListParam.add(beanMovimientoCDA.getFechaOpe());
 		  objListParam.add(beanMovimientoCDA.getFolioPaq());
 		  objListParam.add(beanMovimientoCDA.getFolioPago());
 		  objListParam.add(beanMovimientoCDA.getCveSpei());
 		  objListParam.add(beanMovimientoCDA.getNomInstEmisora());
 		  objListParam.add(beanMovimientoCDA.getCveRastreo());
 		  objListParam.add(beanMovimientoCDA.getCtaBenef());
 		  objListParam.add(beanMovimientoCDA.getMonto());
 		  objListParam.add(beanMovimientoCDA.getFechaAbono());
 		  objListParam.add(beanMovimientoCDA.getHrAbono());
 		  objListParam.add(beanMovimientoCDA.getHrEnvio());
 		  objListParam.add(beanMovimientoCDA.getEstatusCDA());
 		  objListParam.add(beanMovimientoCDA.getCodError());
 		  objListParam.add(beanMovimientoCDA.getTipoPago());
 		 objListParam.add(beanMovimientoCDA.getFolioPaqCda());
 		objListParam.add(beanMovimientoCDA.getFolioCda());
 		objListParam.add(beanMovimientoCDA.getModalidad());
 		objListParam.add(beanMovimientoCDA.getNombreOrd());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaOrd());
 		objListParam.add(beanMovimientoCDA.getNumCuentaOrd());
 		objListParam.add(beanMovimientoCDA.getRfcOrd());
 		objListParam.add(beanMovimientoCDA.getNombreBcoRec());
 		objListParam.add(beanMovimientoCDA.getNombreRec());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaRec());
 		objListParam.add(beanMovimientoCDA.getRfcRec());
 		objListParam.add(beanMovimientoCDA.getConceptoPago());
 		objListParam.add(beanMovimientoCDA.getIva());
 		objListParam.add(beanMovimientoCDA.getNumSerieCertif());
 		objListParam.add(beanMovimientoCDA.getSelloDigital());
 		objListParam.add(beanMovimientoCDA.getNombreRec2());
 		objListParam.add(beanMovimientoCDA.getRfcRec2());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaRec2());
 		objListParam.add(beanMovimientoCDA.getNumCuentaRec2());
 		objListParam.add(beanMovimientoCDA.getFolioCodi());
 		objListParam.add(beanMovimientoCDA.getPagoComision());
 		objListParam.add(beanMovimientoCDA.getMontoComision());
 		objListParam.add(beanMovimientoCDA.getNumCelOrd());
 		objListParam.add(beanMovimientoCDA.getDigVerifiOrd());
 		objListParam.add(beanMovimientoCDA.getNumCelRec());
 		objListParam.add(beanMovimientoCDA.getDigVerifiRec());
 		  objList.add(objListParam);
 	   }
    }
   	return objList;
    }
    
    /**
     * Metodo privado que sirve para obtener los encabezados del Excel
     * @param ctx Objeto del tipo RequestContext
     * @return List<String> Objeto con el listado de String
     */
    private List<String> getHeaderExcel(RequestContext ctx){
    	String []header = new String [] {
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.refere"),
  		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.fechaOp"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaq"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPag"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.instEmisora"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.cveRastreo"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.ctaBenef"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.monto"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.fechaAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrEnvio"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.estatusCDA"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.codError"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoPago"),  		 	   
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaqCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.modalidad"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreBcoRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.conceptoPago"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.iva"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numSerieCertif"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.selloDigital"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCodi"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.pagoComision"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.montoComision"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCelOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.digVerifiOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCelRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.digVerifiRec")
  	 	   };
   	 return Arrays.asList(header);
    }


	/**
	 * Metodo para generar el archivo de exportar todos los
	 * registros
	 * @param beanReqConsMovCDA objeto del tipo BeanReqConsMovCDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expTodosConsMovCDA.do")
	public ModelAndView expTodosConsMovCDA(BeanReqConsMovCDA beanReqConsMovCDA,HttpServletRequest req){
		 
		ModelAndView modelAndView = null;
	    BeanResConsMovCDA beanResConsMovCDA = null;
	    StringBuilder  builder = new StringBuilder();
	    RequestContext ctx = new RequestContext(req);
	    beanReqConsMovCDA = armarObjetoConParametros(beanReqConsMovCDA,req);
		beanReqConsMovCDA.setFechaOperacion(req.getParameter(FECHA_OPERACION));
		

	    try {
	    	beanResConsMovCDA = boConsultaMovCDA.consultaMovCDAExpTodos(beanReqConsMovCDA,getArchitechBean());
	    	modelAndView = new ModelAndView(PAGINA_SERVICIO,BEAN_RESULTADO_CONSULTA,beanResConsMovCDA); 
	    	String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResConsMovCDA.getCodError());
	    	modelAndView.addObject(COD_ERROR, beanResConsMovCDA.getCodError());
	     	modelAndView.addObject(TIPO_ERROR, beanResConsMovCDA.getTipoError());
	     	if(Errores.OK00001V.equals(beanResConsMovCDA.getCodError())){
		     	builder.append(mensaje);
		     	builder.append(beanResConsMovCDA.getNombreArchivo());
		     	mensaje = builder.toString();
	     	}
	     	modelAndView.addObject(DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia()?beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia():"");
		modelAndView.addObject(PARAM_HORA_ABONO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoIniDesde());
		modelAndView.addObject(PARAM_HORA_ABONO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoFinHasta());
		modelAndView.addObject(PARAM_HORA_ENVIO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioIniDesde());
		modelAndView.addObject(PARAM_HORA_ENVIO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioFinHasta());
		modelAndView.addObject(PARAM_CVE_SPEI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getCveSpeiOrdenanteAbono());
		modelAndView.addObject(PARAM_DESCRIPCION,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getDesc());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCveRastreo());
		modelAndView.addObject(PARAM_REF_TRANSFER,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getRefTransfer());
		modelAndView.addObject(PARAM_CTA_BENEFICIARIO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCtaBeneficiario());
		modelAndView.addObject(PARAM_MONTO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getMontoPago());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getTipoPago());
		modelAndView.addObject(ESTATUS_CDA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getEstatus());
		modelAndView.addObject(FECHA_OPERACION,beanReqConsMovCDA.getFechaOperacion());
		modelAndView.addObject(ES_SPID, beanReqConsMovCDA.getEsSPID());
		
		  return modelAndView;			
	}

	/**
	 * Metodo que consulta detalle de un Movimiento CDA Seleccionado
	 * @param beanReqConsMovCDA objeto del tipo BeanReqConsMovCDA
	 * @param beanReqConsMovCDADet Objeto del tipo @see BeanReqConsMovCDADet
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovDetCDA.do")
	public ModelAndView consMovDetCDA(BeanReqConsMovCDA beanReqConsMovCDA,BeanReqConsMovCDADet beanReqConsMovCDADet, HttpServletRequest req) {
		ModelAndView modelAndView = null;
	    BeanResConsMovDetCDA beanConsMovDetCDA = null;
	    beanReqConsMovCDA = armarObjetoConParametros(beanReqConsMovCDA,req);
		beanReqConsMovCDA.setFechaOperacion(req.getParameter(FECHA_OPERACION));
		String fechaOpConLetra = req.getParameter("paramFechaOpLarga");
		RequestContext ctx = new RequestContext(req);
	    try {
	    	beanConsMovDetCDA = boConsultaMovCDA.consMovDetCDA(beanReqConsMovCDADet, getArchitechBean());
	    	modelAndView = new ModelAndView(PAGINA_SERVICIO_DETALLE,BEAN_RESULTADO_CONSULTA_DETALLE,beanConsMovDetCDA);
	    	String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanConsMovDetCDA.getCodError());
	     	modelAndView.addObject(COD_ERROR, beanConsMovDetCDA.getCodError());
	     	modelAndView.addObject(TIPO_ERROR, beanConsMovDetCDA.getTipoError());
	     	modelAndView.addObject(DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia()?beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getOpeContingencia():"");
		modelAndView.addObject(PARAM_HORA_ABONO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoIniDesde());
		modelAndView.addObject(PARAM_HORA_ABONO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrAbonoFinHasta());
		modelAndView.addObject(PARAM_HORA_ENVIO_INI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioIniDesde());
		modelAndView.addObject(PARAM_HORA_ENVIO_FIN,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getHrEnvioFinHasta());
		modelAndView.addObject(PARAM_CVE_SPEI,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getCveSpeiOrdenanteAbono());
		modelAndView.addObject(PARAM_DESCRIPCION,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getDesc());
		modelAndView.addObject(PARAM_CVE_RASTREO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCveRastreo());
		modelAndView.addObject(PARAM_REF_TRANSFER,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getRefTransfer());
		modelAndView.addObject(PARAM_CTA_BENEFICIARIO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getCtaBeneficiario());
		modelAndView.addObject(PARAM_MONTO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getMontoPago());
		modelAndView.addObject(ESTATUS_CDA,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteDos().getEstatus());
		modelAndView.addObject(PARAM_TIPO_PAGO,beanReqConsMovCDA.getBeanReqConsMovCdaFiltrosParteUno().getTipoPago());
		modelAndView.addObject("beanPaginador",beanReqConsMovCDA.getPaginador());
		modelAndView.addObject(FECHA_OPERACION,beanReqConsMovCDA.getFechaOperacion());
		modelAndView.addObject("fechaOperacionLetra",fechaOpConLetra);
		modelAndView.addObject(ES_SPID, beanReqConsMovCDA.getEsSPID());
		return modelAndView;
	}
	/**
	 * Metodo que arma el bean con los parametros ingresados
	 * @param beanReqConsMovCDA objeto del tipo BeanReqConsMovCDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @return beanReqConsMovCDA Objeto del tipo BeanReqConsMovCDA
	 */
	private BeanReqConsMovCDA armarObjetoConParametros(BeanReqConsMovCDA beanReqConsMovCDA, HttpServletRequest req){
		
		BeanReqConsMovCdaFiltrosParteUno beanReqConsMovCdaFiUno = new BeanReqConsMovCdaFiltrosParteUno();
		BeanReqConsMovCdaFiltrosParteDos beanReqConsMovCdaFiDos = new BeanReqConsMovCdaFiltrosParteDos();
		req.getParameterNames();
		beanReqConsMovCdaFiUno.setHrAbonoIniDesde(req.getParameter("paramHoraAbonoIni"));
		beanReqConsMovCdaFiUno.setHrAbonoFinHasta(req.getParameter("paramHoraAbonoFin"));
		beanReqConsMovCdaFiUno.setHrEnvioIniDesde(req.getParameter("paramHoraEnvioIni"));
		beanReqConsMovCdaFiUno.setHrEnvioFinHasta(req.getParameter("paramHoraEnvioFin"));
		beanReqConsMovCdaFiUno.setCveSpeiOrdenanteAbono(req.getParameter("paramCveSpei"));
		beanReqConsMovCdaFiUno.setDesc(req.getParameter("paramDescripcion"));
		beanReqConsMovCdaFiUno.setTipoPago(req.getParameter("paramTipoPago"));

		beanReqConsMovCdaFiDos.setCveRastreo(req.getParameter("paramCveRastreo"));
		beanReqConsMovCdaFiDos.setCtaBeneficiario(req.getParameter("paramCtaBeneficiario"));
		beanReqConsMovCdaFiDos.setRefTransfer(req.getParameter("paramRefTransfer"));
		beanReqConsMovCdaFiDos.setMontoPago(req.getParameter("paramMonto"));
		beanReqConsMovCdaFiDos.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
		beanReqConsMovCdaFiDos.setEstatus(req.getParameter(ESTATUS_CDA));
		
		beanReqConsMovCDA.setBeanReqConsMovCdaFiltrosParteUno(beanReqConsMovCdaFiUno);
		beanReqConsMovCDA.setBeanReqConsMovCdaFiltrosParteDos(beanReqConsMovCdaFiDos);
		
		beanReqConsMovCDA.setEsSPID(Boolean.parseBoolean(req.getParameter(ES_SPID)));

		return beanReqConsMovCDA;
	}
	
	

	 /**
	    * Metodo get del servicio de consultar movimiento CDA
	    * @param boConsultaMovCDA Objeto del Servicio de consultar movimiento CDA
	    */
	public void setBoConsultaMovCDA(BOConsultaMovCDA boConsultaMovCDA) {
		this.boConsultaMovCDA = boConsultaMovCDA;
	}
	
	/**
	    * Metodo set del servicio de consultar movimiento CDA
	    * @return BOConsultaMovCDA Se setea objeto del Servicio de consultar movimiento CDA
	    */
	public BOConsultaMovCDA getBoConsultaMovCDA() {
		return boConsultaMovCDA;
	}

}
