/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerCatCuentasFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 01:21:53 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.controllers.catalogos;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOCuentasFideico;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarCuentasFideico;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsGeneral;

/**
 * Class ControllerCatCuentasFideico.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * de identificacion de cuentas de fideicomisos en spid.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Controller
public class ControllerCatCuentasFideico extends Architech implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1698873774968582074L;

	/** La variable que contiene informacion con respecto a: bo cuentas fideico. */
	private BOCuentasFideico boCuentasFideico;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaCuentasFideico";
	
	/** La constante BEAN_CUENTA. */
	private static final String BEAN_CUENTA = "beanCuenta";
	
	/** La constante BEAN_CUENTAS. */
	private static final String BEAN_CUENTAS = "beanCuentas";

	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaCuentaFideico";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante utilerias. */
	private static final UtilsExportarCuentasFideico utilerias = UtilsExportarCuentasFideico.getInstancia();
	
	/** La constante utilsGeneral. */
	private static final UtilsGeneral utilsGeneral = UtilsGeneral.getUtils();
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/consultaCuentasFideicomisoSPID.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanCuenta beanFilter, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanCuentas response = null;
		try {
			String accion = req.getParameter(ACCION);
			/** Validacion del parametro accion **/
			if (accion != null && !accion.equals(StringUtils.EMPTY) && utilsGeneral.validaFiltro(accion)) {
				/** Se valida los filtros guardados**/
				beanFilter.setCuenta(req.getParameter("cuentaF"));
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			/** Ejecuta peticion al BO **/
			response = boCuentasFideico.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
				
			}
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, response);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Agregar Cuenta.
	 * 
	 * Mapeo del metodo para iniciar el flujo de agregar 
	 * un nuevo registro al catalogo.
	 *
	 * @param beanCuenta --> objeto de tipo beanCuenta enviado para realizar el alta a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/agregarCuentaFideicomiso.do")
	public ModelAndView agregarCuenta(@ModelAttribute(BEAN_CUENTA) BeanCuenta beanCuenta, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de alta **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanCuentas beanCuentas = null;
		BeanResBase response = null;
		try {
			response = boCuentasFideico.agregar(beanCuenta, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				beanCuentas = boCuentasFideico.consultar(new BeanCuenta(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_CUENTA, new BeanCuenta());
				model.addObject(BEAN_CUENTAS, beanCuentas);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(BEAN_CUENTA, beanCuentas);
				model.addObject(ACCION, ACCION_ALTA);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION, ACCION_ALTA);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Editar Cuenta.
	 * 
	 * Mapeo del metodo para iniciar el flujo de 
	 * modificar un registro del catalogo.
	 *
	 * @param beanCuenta --> objeto de tipo beanCuenta enviado para realizar el update a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/editarCuentaFideicomiso.do")
	public ModelAndView editarCuenta(@ModelAttribute(BEAN_CUENTA) BeanCuenta beanCuenta, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de edicion  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanCuentas beanCuentas = null;
		String valorOld = null;
		try {
			valorOld = req.getParameter("valorOld");
			final BeanCuenta datoAnterior = new BeanCuenta();
			datoAnterior.setCuenta(valorOld);
			BeanResBase response = boCuentasFideico.editar(beanCuenta, datoAnterior, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				beanCuentas = boCuentasFideico.consultar(new BeanCuenta(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_CUENTAS, beanCuentas);
				model.addObject(BEAN_CUENTA, new BeanCuenta());
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				beanCuenta.setCuenta(valorOld);
				model.addObject(BEAN_CUENTA, beanCuenta);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION,ACCION_MODIFICAR);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Mantenimiento Cuentas.
	 * 
	 * Mapeo del flujo para mostrar el fomurlario de 
	 * matenimiento de registros del catalogo.
	 *
	 * @param beanCuentas --> objeto de tipo beanCuentas que es mostrado en el formulario
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/mantenimientoCuentasFideicomiso.do")
	public ModelAndView mantenimientoCuentas(@ModelAttribute(BEAN_CUENTAS) BeanCuentas beanCuentas,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanCuenta beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Creacion de la vista de mantenimiento  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanCuenta beanCuentaFideico = new BeanCuenta();
		if (beanCuentas != null && beanCuentas.getCuentas() != null) {
			for(BeanCuenta cuenta : beanCuentas.getCuentas()) {
				if(cuenta.isSeleccionado()) {
					beanCuentaFideico = cuenta;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		String banderaAlta = req.getParameter(ACCION);
		if (beanCuentaFideico.getCuenta() != null &&  !banderaAlta.equals(ACCION_ALTA)) {
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		model.addObject(BEAN_CUENTA, beanCuentaFideico);
		model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		model.addObject(UtilsConstantes.BEAN_PAGINADOR, beanPaginador);
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Eliminar Cuenta.
	 *
	 * @param beanCuentas --> objeto de tipo beanCuenta enviado para realizar el delete a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/eliminarCuentaFideicomiso.do")
    public ModelAndView eliminarCuenta(@ModelAttribute(BEAN_CUENTAS) BeanCuentas beanCuentas, BindingResult bindingResult){
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de eliminacion **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			response = boCuentasFideico.eliminar(beanCuentas, getArchitechBean());
			BeanCuentas responseConsulta = boCuentasFideico.consultar(new BeanCuenta(), new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
					
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
		} catch (BusinessException e) {
			showException(e);
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, null);
			modelAndView = utilsGeneral.setError(modelAndView);
		}
		/** Retorno del modelo a la vista **/
		return modelAndView;
    }
	
	/**
	 * Exportar Cuentas.
	 * 
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 * @param beanCuentas --> objeto de tipo cuenta enviado para realizar insertar los numeros de cuenta en el excel
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/exportarCuentasFideicomiso.do")
	public ModelAndView exportarCuentas(@ModelAttribute(BEAN_CUENTAS) BeanCuentas beanCuentas, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcelSPID(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBodySPID(beanCuentas.getCuentas()));
		modelAndView.addObject("NAME_SHEET", "Cuentas de Fideicomisos");
		/** Retorno del modelo a la vista **/
		return modelAndView;
	}
	
	/**
	 * Mapeo del metodo para iniciar el flujo de exportar los numeros de cuenta
	 * al proceso de inserccion en BD.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanCuenta --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value="/exportarTodoCuentasFideicomiso.do")
	public ModelAndView exportarTodosCuentas(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanCuenta beanCuenta, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanCuentas responseConsulta = new BeanCuentas();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boCuentasFideico.consultar(beanCuenta, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boCuentasFideico.exportarTodo(beanCuenta, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CUENTAS, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}

	/**
	 * Metodo Get para obtener el numero de cuenta
	 *
	 * @return boCuentasFideico --> variable de tipo  clase que regresa el numero de cuenta
	 */
	public BOCuentasFideico getBoCuentasFideico() {
		return boCuentasFideico;
	}

	/**
	 * Metodo Set para obtener el RFC
	 *
	 * @param boCuentasFideico --> variable de tipo  clase que asigna el RFC
	 */
	public void setBoCuentasFideico(BOCuentasFideico boCuentasFideico) {
		this.boCuentasFideico = boCuentasFideico;
	}
	
}
