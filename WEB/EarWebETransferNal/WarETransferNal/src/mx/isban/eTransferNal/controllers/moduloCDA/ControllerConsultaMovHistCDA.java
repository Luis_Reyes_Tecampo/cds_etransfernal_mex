/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 17:18:25 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovHistCDA;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOConsultaMovHistCDA;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar las peticiones
 * para la consulta de Movimientos CDAs
 *
 */
@Controller
public class ControllerConsultaMovHistCDA  extends Architech {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**
	 * Constante para establecer la pagina de servicio
	 */
	private static final String PAGINA_SERVICIO = "consultaMovHistCDA";
	/**
	 * Constante para el parametro de OpeContingencia
	 */
	private static final String OPE_CONTINGENCIA = "paramOpeContingencia";

	/**
	 * Constante para el parametro de fechaHoy
	 */
	private static final String FECHA_HOY = "fechaHoy";
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsMovHistCDA";
	
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOConsultaMovHistCDA boConsulMovHistCDA;

	/**
	* Metodo que se utiliza para mostrar la pagina de Consulta Mov CDA
	* @return ModelAndView Objeto del tipo ModelAndView
	*/ 
	@RequestMapping(value = "/muestraConsMovHistCDA.do")
	public ModelAndView muestraConsMovHistCDA() {
		
		  Utilerias utilerias = Utilerias.getUtilerias();
		  Date fechaHoy = null;
		  Date fecha3Ant = null;
		  ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		  Calendar cal = Calendar.getInstance();
	      fechaHoy = cal.getTime();
	      cal.add(Calendar.MONTH, -3);
	      cal.setLenient(true);
	      fecha3Ant = cal.getTime();
	      String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	      String strFecha3Antes = utilerias.formateaFecha(fecha3Ant, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	      modelAndView.addObject(FECHA_HOY, strFechaHoy);
	      modelAndView.addObject("fecha3Antes", strFecha3Antes);
	      modelAndView.addObject("fechaFin", strFechaHoy);	      
		
	    return modelAndView;

	}
	
	/**
	 * Metodo que se utiliza para realizar la consulta
	 * de movimientos Historicos CDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @param beanReqConsMovHistCDA Objeto del tipo BeanReqConsMovHistCDA
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovHistCDA.do")
	public ModelAndView consMovHistCDA(HttpServletRequest req,
			BeanReqConsMovHistCDA beanReqConsMovHistCDA) {
		
		ModelAndView modelAndView = null;
	    BeanResConsMovHistCDA beanResConsMovHistCDA = null;
	    RequestContext ctx = new RequestContext(req);
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    try {	    	
	    	beanResConsMovHistCDA = boConsulMovHistCDA.consultarMovHistCDA(beanReqConsMovHistCDA,getArchitechBean());
	    	String mensaje = ctx.getMessage("codError."+beanResConsMovHistCDA.getCodError());
	    	modelAndView = new ModelAndView(PAGINA_SERVICIO,BEAN_RESULTADO_CONSULTA,beanResConsMovHistCDA);
	    	modelAndView.addObject("codError", beanResConsMovHistCDA.getCodError());
	     	modelAndView.addObject("tipoError", beanResConsMovHistCDA.getTipoError());
	     	modelAndView.addObject("descError", mensaje);
		} catch (BusinessException e) {
			showException(e);
		}		
		modelAndView.addObject("paramFechaOpeInicio",beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject("paramFechaOpeFin",beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject("paramHrAbonoIni",beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject("paramHrAbonoFin",beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject("paramHrEnvioIni",beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject("paramHrEnvioFin",beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject("paramCveSpeiOrdenanteAbono",beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject("paramNombreInstEmisora",beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject("paramCveRastreo",beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject("paramCtaBeneficiario",beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject("paramMontoPago",beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject("paramTipoPago",beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovHistCDA.getOpeContingencia()?beanReqConsMovHistCDA.getOpeContingencia():"");
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);			
	    return modelAndView;
	}

	/**
	 * Metodo utilizado para solicitar la exportacion de
	 * los movimientos historicos CDA obtenidos en la consulta
	 * @param beanReqConsMovCDA objeto del tipo BeanReqConsMovCDA
	 *  * @param req Objeto del tipo HttpServletRequest
	 * @param res objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expConsMovHistCDA.do")
	public ModelAndView expConsMovHistCDA(BeanReqConsMovHistCDA beanReqConsMovCDA,HttpServletRequest req,HttpServletResponse res) {
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		RequestContext ctx = new RequestContext(req);
		ModelAndView modelAndView = null;
        BeanResConsMovHistCDA beanResConMovCDA = null;
        beanReqConsMovCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
       		        
        try {
        	beanResConMovCDA = boConsulMovHistCDA.consultarMovHistCDA(beanReqConsMovCDA,getArchitechBean());
      	   if(Errores.OK00000V.equals(beanResConMovCDA.getCodError())||
     			   Errores.ED00011V.equals(beanResConMovCDA.getCodError())){
      		 formatCellHeader = new FormatCell();
      	     formatCellHeader.setFontName("Calibri");
      	     formatCellHeader.setFontSize((short)11);
      	     formatCellHeader.setBold(true);
      	     formatCellBody = new FormatCell();
      	     formatCellBody.setFontName("Calibri");
    	     formatCellBody.setFontSize((short)11);
    	     formatCellBody.setBold(false);
      	   
      	 	   
      	 	 modelAndView = new ModelAndView(new ViewExcel());
       	 	 modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
  	 	     modelAndView.addObject("FORMAT_CELL", formatCellBody);
      	 	 modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
      	 	 modelAndView.addObject("BODY_VALUE", getBody(beanResConMovCDA.getListBeanMovimientoCDA()));
      	 	 modelAndView.addObject("NAME_SHEET", "expConsMovHistCDA");
      	   }else{
      		 modelAndView = consMovHistCDA(req,beanReqConsMovCDA);
      		 String mensaje = ctx.getMessage("codError."+beanResConMovCDA.getCodError());
      	      modelAndView.addObject("codError", beanResConMovCDA.getCodError());
      	      modelAndView.addObject("tipoError", beanResConMovCDA.getTipoError());
      	      modelAndView.addObject("descError", mensaje);
      	   }
 	    } catch (BusinessException e) {
 		 showException(e);
 	    }
 	    return modelAndView;		
	}
	

    /**
     * Metodo privado que sirve para obtener los datos para el excel
     * @param listBeanMovimientoCDA Objeto (List<BeanMovimientoCDA>) con el listado de objetos del tipo BeanMovimientoCDA
     * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
     */
    private List<List<Object>> getBody(List<BeanMovimientoCDA> listBeanMovimientoCDA){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	  if(listBeanMovimientoCDA!= null && 
 			   !listBeanMovimientoCDA.isEmpty()){
  	 		   objList = new ArrayList<List<Object>>();
 	   for(BeanMovimientoCDA beanMovimientoCDA : listBeanMovimientoCDA){ 
 		  objListParam = new ArrayList<Object>();
 		  objListParam.add(beanMovimientoCDA.getReferencia());
 		  objListParam.add(beanMovimientoCDA.getFechaOpe());
 		  objListParam.add(beanMovimientoCDA.getFolioPaq());
 		  objListParam.add(beanMovimientoCDA.getFolioPago());
 		  objListParam.add(beanMovimientoCDA.getCveSpei());
 		  objListParam.add(beanMovimientoCDA.getNomInstEmisora());
 		  objListParam.add(beanMovimientoCDA.getCveRastreo());
 		  objListParam.add(beanMovimientoCDA.getCtaBenef());
 		  objListParam.add(beanMovimientoCDA.getMonto());
 		 objListParam.add(beanMovimientoCDA.getFechaAbono());
 		  objListParam.add(beanMovimientoCDA.getHrAbono());
 		  objListParam.add(beanMovimientoCDA.getHrEnvio());
 		  objListParam.add(beanMovimientoCDA.getEstatusCDA());
 		  objListParam.add(beanMovimientoCDA.getCodError());
 		 objListParam.add(beanMovimientoCDA.getTipoPago());
 		objListParam.add(beanMovimientoCDA.getFolioPaqCda());
 		objListParam.add(beanMovimientoCDA.getFolioCda());
 		objListParam.add(beanMovimientoCDA.getModalidad());
 		objListParam.add(beanMovimientoCDA.getNombreOrd());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaOrd());
 		objListParam.add(beanMovimientoCDA.getNumCuentaOrd());
 		objListParam.add(beanMovimientoCDA.getRfcOrd());
 		objListParam.add(beanMovimientoCDA.getNombreBcoRec());
 		objListParam.add(beanMovimientoCDA.getNombreRec());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaRec());
 		objListParam.add(beanMovimientoCDA.getRfcRec());
 		objListParam.add(beanMovimientoCDA.getConceptoPago());
 		objListParam.add(beanMovimientoCDA.getIva());
 		objListParam.add(beanMovimientoCDA.getNumSerieCertif());
 		objListParam.add(beanMovimientoCDA.getSelloDigital());
 		objListParam.add(beanMovimientoCDA.getNombreRec2());
 		objListParam.add(beanMovimientoCDA.getRfcRec2());
 		objListParam.add(beanMovimientoCDA.getTipoCuentaRec2());
 		objListParam.add(beanMovimientoCDA.getNumCuentaRec2());
 		objListParam.add(beanMovimientoCDA.getFolioCodi());
 		objListParam.add(beanMovimientoCDA.getPagoComision());
 		objListParam.add(beanMovimientoCDA.getMontoComision());
 		objListParam.add(beanMovimientoCDA.getNumCelOrd());
 		objListParam.add(beanMovimientoCDA.getDigVerifiOrd());
 		objListParam.add(beanMovimientoCDA.getNumCelRec());
 		objListParam.add(beanMovimientoCDA.getDigVerifiRec());
 		  objList.add(objListParam);
 	   }
    }
   	return objList;
    }
    
    /**
     * Metodo privado que sirve para obtener los encabezados del Excel
     * @param ctx Objeto del tipo RequestContext
     * @return List<String> Objeto con el listado de String
     */
    private List<String> getHeaderExcel(RequestContext ctx){
    	String []header = new String [] {
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.refere"),
  		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.fechaOp"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaq"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPag"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.instEmisora"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.cveRastreo"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.ctaBenef"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.monto"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.fechaAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrAbono"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrEnvio"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.estatusCDA"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.codError"),
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoPago"),  		 	   
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaqCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.modalidad"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreBcoRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.conceptoPago"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.iva"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numSerieCertif"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.selloDigital"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaRec2"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCodi"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.pagoComision"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.montoComision"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCelOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.digVerifiOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCelRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.digVerifiRec")
  	 	   };
   	 return Arrays.asList(header);
    }

	

	/**
	 * Metodo utilizado para realizar solicitar la exportacion de todos
	 * los movimientos historicos CDA obtenidos en la consulta
	 * @param req Objeto del tipo HttpServletRequest
	 * @param beanReqConsMovHistCDA objeto del tipo BeanReqConsMovHistCDA
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expTodosConsMovHistCDA.do")
	public ModelAndView expTodosConsMovHistCDA(BeanReqConsMovHistCDA beanReqConsMovHistCDA,HttpServletRequest req) {
		
		ModelAndView modelAndView = null;
	    BeanResConsMovHistCDA beanResConsMovHistCDA = null;
	    StringBuilder  builder = new StringBuilder();
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    RequestContext ctx = new RequestContext(req);
	    
	    try {
	    	beanResConsMovHistCDA = boConsulMovHistCDA.consultaMovHistCDAExpTodos(beanReqConsMovHistCDA,getArchitechBean());
	    	modelAndView = new ModelAndView(PAGINA_SERVICIO,BEAN_RESULTADO_CONSULTA,beanResConsMovHistCDA);
	    	modelAndView.addObject("codError", beanResConsMovHistCDA.getCodError());
	     	modelAndView.addObject("tipoError", beanResConsMovHistCDA.getTipoError());
	     	String mensaje = ctx.getMessage("codError."+beanResConsMovHistCDA.getCodError());
	     	builder.append(mensaje);
	     	builder.append(beanResConsMovHistCDA.getNombreArchivo());
	     	mensaje = builder.toString();
	     	modelAndView.addObject("descError", mensaje);
		} catch (BusinessException e) {
			showException(e);
		}
		modelAndView.addObject("paramFechaOpeInicio",beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject("paramFechaOpeFin",beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject("paramHrAbonoIni",beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject("paramHrAbonoFin",beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject("paramHrEnvioIni",beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject("paramHrEnvioFin",beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject("paramCveSpeiOrdenanteAbono",beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject("paramNombreInstEmisora",beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject("paramCveRastreo",beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject("paramCtaBeneficiario",beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject("paramMontoPago",beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject("paramTipoPago",beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovHistCDA.getOpeContingencia()?beanReqConsMovHistCDA.getOpeContingencia():"");
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);		
		
		return modelAndView;			
	}

	/**
	 * Metodo que consulta detalle de un Movimiento Historico CDA Seleccionado
	 * @param beanReqConsMovHistCDA objeto del tipo BeanReqConsMovCDA
	 * @param beanReqConsMovCDADet objeto del tipo BeanReqConsMovCDADet
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovHistDetCDA.do")
	public ModelAndView consMovHistDetCDA(BeanReqConsMovHistCDA beanReqConsMovHistCDA,
			BeanReqConsMovCDADet beanReqConsMovCDADet, HttpServletRequest req) {
		ModelAndView modelAndView = null;
	    BeanResConsMovDetHistCDA beanResConsMovDetHistCDA = null;
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    
	    try {
	    	beanResConsMovDetHistCDA = boConsulMovHistCDA.consMovHistDetCDA(beanReqConsMovCDADet,getArchitechBean());
		} catch (BusinessException e) {
			showException(e);
		}
		
		modelAndView = new ModelAndView("consultaDetHistMovCDA","beanResConsMovDetHistCDA",beanResConsMovDetHistCDA);
		modelAndView.addObject("paramFechaOpeInicio",beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject("paramFechaOpeFin",beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject("paramHrAbonoIni",beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject("paramHrAbonoFin",beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject("paramHrEnvioIni",beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject("paramHrEnvioFin",beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject("paramCveSpeiOrdenanteAbono",beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject("paramNombreInstEmisora",beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject("paramCveRastreo",beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject("paramCtaBeneficiario",beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject("paramMontoPago",beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject("paramTipoPago",beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject("beanPaginador",beanReqConsMovHistCDA.getPaginador());
		modelAndView.addObject(OPE_CONTINGENCIA,beanReqConsMovHistCDA.getOpeContingencia()?beanReqConsMovHistCDA.getOpeContingencia():"");
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);	
		return modelAndView;
	}

	/**
	 * Metodo set del servicio de Consultar Movimientos Historicos CDA
	 * @param boConsulMovHistCDA objeto del tipo BOConsultaMovHistCDA
	 */
	public void setBoConsulMovHistCDA(BOConsultaMovHistCDA boConsulMovHistCDA) {
		this.boConsulMovHistCDA = boConsulMovHistCDA;
	}

	/**
	 * Metodo get del servicio de Consultar Movimientos Historicos CDA
	 * @return boConsulMovHistCDA objeto del tipo BOConsultaMovHistCDA
	 */
	public BOConsultaMovHistCDA getBoConsulMovHistCDA() {
		return boConsulMovHistCDA;
	}
}
