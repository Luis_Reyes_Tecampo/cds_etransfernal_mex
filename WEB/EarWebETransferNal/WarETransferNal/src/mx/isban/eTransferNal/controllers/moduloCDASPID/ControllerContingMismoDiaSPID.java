/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerContingMismoDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:02:04 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDia;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOContingMismoDiaSPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Contingencia CDA Mismo
 * Dia
**/
@Controller
public class ControllerContingMismoDiaSPID extends Architech implements Serializable  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	 /**
	 * Constante de CONTINGENCIA_CDA_MISMO_DIA_SPID
	 */
	private static final String CONTINGENCIA_CDA_MISMO_DIA_SPID = "contingenciaCDAMismoDiaSPID";
	
	 /**
	 * Constante de COD_ERROR_PUNTO
	 */
	private static final String COD_ERROR_PUNTO = "codError.";
	
	 /**
	 * Constante de COD_ERROR
	 */
	private static final String COD_ERROR = "codError";

	 /**
	 * Constante de TIPO_ERROR
	 */
	private static final String TIPO_ERROR = "tipoError";

	 /**
	 * Constante de DESC_ERROR
	 */
	private static final String DESC_ERROR = "descError";

	 /**
	 * Constante de BEAN_RES_CONTIG_MISMO_DIA
	 */
	private static final String BEAN_RES_CONTIG_MISMO_DIA = "beanResContingMismoDia";
	
	/**
	 * Referencia al servicio de contingencia mismo dia
	 */
	private transient BOContingMismoDiaSPID bOContingMismoDiaSPID;


/**
 * Metodo que se utiliza para mostrar la pagina de Contingencia
 * CDA Mismo Dia.
 *
 * @param req Objeto del tipo HttpServletRequests
 * @param res the res
 * @return ModelAndView Objeto del tipo ModelAndView
 */
   @RequestMapping(value = "/muestraContingCDAMismoDiaSPID.do")
   public ModelAndView muestraContingCDAMismoDia(HttpServletRequest req, HttpServletResponse res){
       ModelAndView modelAndView = null;      
       BeanResContingMismoDia beanResContingMismoDia = null;
       RequestContext ctx = new RequestContext(req);
       try{
    	   beanResContingMismoDia = bOContingMismoDiaSPID.consultaPendientesMismoDia(getArchitechBean());
    	   modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID,BEAN_RES_CONTIG_MISMO_DIA,beanResContingMismoDia);
       }catch(BusinessException e){
    	 //Manejo de excepciones
    	   showException(e);
           modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID);
           String mensaje = ctx.getMessage(COD_ERROR_PUNTO+e.getCode());
           modelAndView.addObject(COD_ERROR, e.getCode());
           modelAndView.addObject(DESC_ERROR, mensaje);
           modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
       }
       
       return modelAndView;
   }

   /**
    * Metodo que se utiliza para mostrar la pagina de Contingencia
    * CDA Mismo Dia.
    *
    * @param req Objeto del tipo HttpServletRequests
    * @param res the res
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/consultarContingCDAMismoDiaSPID.do")
    public ModelAndView consultarContingCDAMismoDia(HttpServletRequest req, HttpServletResponse res){
       ModelAndView modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID);
       BeanPaginador beanPaginador = UtilsMapeaRequest.getMapper().getPaginadorFromRequest(req.getParameterMap());
       BeanResContingMismoDia beanResContingMismoDia = null;
       RequestContext ctx = new RequestContext(req);

       try{
    	   beanResContingMismoDia = bOContingMismoDiaSPID.consultaContingMismoDia(beanPaginador, getArchitechBean());
    	   modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID,BEAN_RES_CONTIG_MISMO_DIA,beanResContingMismoDia);
       }catch(BusinessException e){
    	 //Manejo de excepciones
    	   showException(e);
           modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID);
           String mensaje = ctx.getMessage(COD_ERROR_PUNTO+e.getCode());
           modelAndView.addObject(DESC_ERROR, mensaje);
           modelAndView.addObject(COD_ERROR, e.getCode());
           modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
       }
       
       return modelAndView;
    }
    
    /**
     * Metodo que se utiliza para generar el archivo de Contingencia
     * CDA Mismo Dia.
     *
     * @param req Objeto del tipo HttpServletRequests
     * @param res the res
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/genArchContigMismoDiaSPID.do")
     public ModelAndView genArchContigMismoDia(HttpServletRequest req, HttpServletResponse res){
        ModelAndView modelAndView = null;
        BeanResGenArchContingMismoDia beanResGenArchContingMismoDia = null;
        RequestContext ctx = new RequestContext(req);

        try{
        	beanResGenArchContingMismoDia = bOContingMismoDiaSPID.genArchContigMismoDia(getArchitechBean());
        	modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID,BEAN_RES_CONTIG_MISMO_DIA,beanResGenArchContingMismoDia);
        }catch(BusinessException e){
        	//Manejo de excepciones
        	showException(e);
            modelAndView = new ModelAndView(CONTINGENCIA_CDA_MISMO_DIA_SPID);
            String mensaje = ctx.getMessage(COD_ERROR_PUNTO+e.getCode());
            modelAndView.addObject(DESC_ERROR, mensaje);
            modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
            modelAndView.addObject(COD_ERROR, e.getCode());
        }
        
        return modelAndView;
     }
     
     /**
      * @param bOContingMismoDiaSPID Valor a establecer en atributo: bOContingMismoDiaSPID
      */
     public void setbOContingMismoDiaSPID(BOContingMismoDiaSPID bOContingMismoDiaSPID) {
    	 this.bOContingMismoDiaSPID = bOContingMismoDiaSPID;
     }

}
