/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerCatNombresFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:50:29 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;
import mx.isban.eTransferNal.beans.catalogos.BeanNombresFideico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BONombresFideico;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarNombresFideico;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsGeneral;

/**
 * Class ControllerCatNombresFideico.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * de identificacion de nombres de fideicomisos en spid.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Controller
public class ControllerCatNombresFideico extends Architech implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1698873774968582074L;

	/** La variable que contiene informacion con respecto a: bo nombres fideico. */
	private BONombresFideico boNombresFideico;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaNombresFideico";
	
	/** La constante BEAN_NOMBRE. */
	private static final String BEAN_NOMBRE = "beanNombre";
	
	/** La constante BEAN_NOMBRES. */
	private static final String BEAN_NOMBRES = "beanNombres";

	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaNombreFideico";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante utilerias. */
	private static final UtilsExportarNombresFideico utilerias = UtilsExportarNombresFideico.getInstancia();
	
	/** La constante utilsGeneral. */
	private static final UtilsGeneral utilsGeneral = UtilsGeneral.getUtils();
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador El objeto --> bean paginador --> Objeto de entrada para realizar el paginado
	 * @param beanFilter El objeto --> bean filter --> Objeto de entrada para realizar la consulta con filtros
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value = "/consultaNombresFideicomiso.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNombreFideico beanFilter, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanNombresFideico response = null;
		try {
			String accion = req.getParameter(ACCION);
			/** Validacion del parametro accion **/
			if (accion != null && !accion.equals(StringUtils.EMPTY) && utilsGeneral.validaFiltro(accion)) {
				/** Se valida los filtros guardados**/
				beanFilter.setNombre(req.getParameter("nombreF"));
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			/** Ejecuta peticion al BO **/
			response = boNombresFideico.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
				
			}
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, response);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * agregarNombre.
	 * 
	 * Mapeo del metodo para iniciar el flujo de agregar 
	 * un nuevo registro al catalogo.
	 *
	 * @param beanNombre El objeto --> bean nombre --> Objeto con los datos de entrada del nuevo registro
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value = "/agregarNombreFideicomiso.do")
	public ModelAndView agregarNombre(@ModelAttribute(BEAN_NOMBRE) BeanNombreFideico beanNombre, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de alta **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanNombresFideico beanNombres = null;
		BeanResBase response = null;
		try {
			response = boNombresFideico.agregar(beanNombre, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				beanNombres = boNombresFideico.consultar(new BeanNombreFideico(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_NOMBRE, new BeanNombreFideico());
				model.addObject(BEAN_NOMBRES, beanNombres);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(BEAN_NOMBRE, beanNombres);
				model.addObject(ACCION, ACCION_ALTA);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION, ACCION_ALTA);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * editarNombre.
	 * 
	 * Mapeo del metodo para iniciar el flujo de 
	 * modificar un registro del catalogo.
	 *
	 * @param beanNombre El objeto --> bean nombre --> Objeto con los datos de entrada del nuevo registro
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value = "/editarNombreFideicomiso.do")
	public ModelAndView editarNombre(@ModelAttribute(BEAN_NOMBRE) BeanNombreFideico beanNombre, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de edicion  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanNombresFideico beanNombres = null;
		String valorOld = null;
		try {
			valorOld = req.getParameter("valorOld");
			final BeanNombreFideico datoAnterior = new BeanNombreFideico();
			datoAnterior.setNombre(valorOld);
			BeanResBase response = boNombresFideico.editar(beanNombre, datoAnterior, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				beanNombres = boNombresFideico.consultar(new BeanNombreFideico(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_NOMBRES, beanNombres);
				model.addObject(BEAN_NOMBRE, new BeanNombreFideico());
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				beanNombre.setNombre(valorOld);
				model.addObject(BEAN_NOMBRE, beanNombre);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION,ACCION_MODIFICAR);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * mantenimientoNombres.
	 * 
	 * Mapeo del flujo para mostrar el fomurlario de 
	 * matenimiento de registros del catalogo.
	 *
	 * @param beanNombres El objeto --> bean nombres --> Objeto de la respuesta guardada
	 * @param beanFilter El objeto --> bean filter --> Objeto con informacion de los filtros
	 * @param beanPaginador El objeto --> bean paginador --> Objeto con informacion del paginado
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */ 
	@RequestMapping(value = "/mantenimientoNombresFideicomiso.do")
	public ModelAndView mantenimientoNombres(@ModelAttribute(BEAN_NOMBRES) BeanNombresFideico beanNombres,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNombreFideico beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Creacion de la vista de mantenimiento  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanNombreFideico beanNombreFideico = new BeanNombreFideico();
		if (beanNombres != null && beanNombres.getNombres() != null) {
			for(BeanNombreFideico nombre : beanNombres.getNombres()) {
				if(nombre.isSeleccionado()) {
					beanNombreFideico = nombre;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		String banderaAlta = req.getParameter(ACCION);
		if (beanNombreFideico.getNombre() != null &&  !banderaAlta.equals(ACCION_ALTA)) {
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		model.addObject(BEAN_NOMBRE, beanNombreFideico);
		model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		model.addObject(UtilsConstantes.BEAN_PAGINADOR, beanPaginador);
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * eliminarNombre.
	 *
	 * @param beanNombres El objeto --> bean nombres  --> Objeto para recuperar los  nombres a ser eliminados
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value = "/eliminarNombreFideicomiso.do")
    public ModelAndView eliminarNombre(@ModelAttribute(BEAN_NOMBRES) BeanNombresFideico beanNombres, BindingResult bindingResult){
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		/** Comienza flujo de eliminacion **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			response = boNombresFideico.eliminar(beanNombres, getArchitechBean());
			BeanNombresFideico responseConsulta = boNombresFideico.consultar(new BeanNombreFideico(), new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
					
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
		} catch (BusinessException e) {
			showException(e);
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, null);
			modelAndView = utilsGeneral.setError(modelAndView);
		}
		/** Retorno del modelo a la vista **/
		return modelAndView;
    }
	
	/**
	 * exportarNombres.
	 * 
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 * @param beanNombres El objeto --> bean nombres --> Objeto con la lista de los nombres a ser exportados
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value = "/exportarNombreFideicomiso.do")
	public ModelAndView exportarNombres(@ModelAttribute(BEAN_NOMBRES) BeanNombresFideico beanNombres, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBody(beanNombres.getNombres()));
		modelAndView.addObject("NAME_SHEET", "Nombres de Fideicomisos");
		/** Retorno del modelo a la vista **/
		return modelAndView;
	}
	
	/**
	 * exportarTodosNombres.
	 * 
	 * Mapeo del metodo para iniciar el flujo de exportar todo
	 * al proceso de inserccion en BD
	 *
	 * @param beanPaginador El objeto --> bean paginador --> Objeto con informacion del paginado
	 * @param beanNombre El objeto --> bean nombre --> Objeto que contiene informacion del filtro guardado
	 * @param bindingResult El objeto --> binding result --> Objeto para validar los datos de entrada
	 * @return Objeto model and view --> Objeto con el resultado de la operacion presentado en el front
	 */
	@RequestMapping(value="/exportarTodosNombreFideicomiso.do")
	public ModelAndView exportarTodosNombres(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNombreFideico beanNombre, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanNombresFideico responseConsulta = new BeanNombresFideico();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boNombresFideico.consultar(beanNombre, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boNombresFideico.exportarTodo(beanNombre, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOMBRES, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Obtener el objeto --> bo nombres fideico.
	 *
	 * @return El objeto --> bo nombres fideico
	 */
	public BONombresFideico getBoNombresFideico() {
		return boNombresFideico;
	}

	/**
	 * Definir el objeto --> bo nombres fideico.
	 *
	 * @param boNombresFideico El nuevo objeto --> bo nombres fideico
	 */
	public void setBoNombresFideico(BONombresFideico boNombresFideico) {
		this.boNombresFideico = boNombresFideico;
	}
	
}
