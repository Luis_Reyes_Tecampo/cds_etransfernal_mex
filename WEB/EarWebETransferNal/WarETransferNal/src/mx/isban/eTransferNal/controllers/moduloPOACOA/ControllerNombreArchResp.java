/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerNombreArchResp.java
 * 
 *  La clase ha sido modificada
 *  
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 06:35:54 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import javax.servlet.http.HttpServletRequest;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOCargaArchivoRespuesta;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion  de los parametros.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
@Controller
public class ControllerNombreArchResp extends Architech{

	 /** Constante del Serial Version. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Constante con el nombre de la pagina. */
	private static final String PAG_NOMBRE_ARCH_RESPUESTA ="nombreArchRespuesta";
	
	/** Variable exito. */
	private static final String INICIO_POA_COA = "INICIO_POA_COA";
	
	/**BO con los metodos para insertar el nombre de archivo a procesar.*/
	private BOCargaArchivoRespuesta boCargaArchivoRespuesta;
	
	/**
	 * Metodo que muestra la pantalla de parametros del POA/COA.
	 *
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="muestraNomArchRespuesta.do")
	public ModelAndView muestraNomArchRespuesta(){
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
  	    /** Declaracion de variables **/
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_RESPUESTA);
		/** Se realiza la peticion al BO **/
		BeanResValInicioPOACOA beanResValInicioPOACOA = 
				boCargaArchivoRespuesta.validaInicioPOACOA(null, getArchitechBean());
		/** Se valida la respuesta y se setean los datos al modelo **/
		if(!Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError());
			modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}
		/** Se setea al modelo el modulo correspondiente para poder validar el JSP **/
		modelAndView.addObject(INICIO_POA_COA, beanResValInicioPOACOA.isActivoPOACOA());
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_ACTUAL);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
		/**
		 * Muestra nom arch respuesta spid.
		 *
		 * @return Objeto model and view
		 */
		@RequestMapping(value="muestraNomArchRespuestaSpid.do")
		public ModelAndView muestraNomArchRespuestaSpid(){
			/** Recuperar atributos usando el context **/
	  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	  	    /** Declaracion de variables **/
			String mensaje = "";
			RequestContext ctx = new RequestContext(req);
			ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_RESPUESTA);
			/** Se realiza la peticion al BO **/
			BeanResValInicioPOACOA beanResValInicioPOACOA = 
					boCargaArchivoRespuesta.validaInicioPOACOA("2",getArchitechBean());
			/** Se valida la respuesta y se setean los datos al modelo **/
			if(!Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError());
				modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
		     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Se setea al modelo el modulo correspondiente para poder validar el JSP **/
			modelAndView.addObject(INICIO_POA_COA, beanResValInicioPOACOA.isActivoPOACOA());
			modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_SPID);
			/** Retorno del metodo **/
			return modelAndView;
		}
	
	/**
	 * Cargar arch resp.
	 *
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="cargarArchResp.do")
	public ModelAndView cargarArchResp() {
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
  	   /** Declaracion de variables **/
		ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_RESPUESTA);
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanResValInicioPOACOA beanResValInicioPOACOA = null;
		String nomArchivo = req.getParameter("archResp");
		nomArchivo = obtieneNombre(nomArchivo);
		String modulo = req.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloView = ConstantesWebPOACOA.CONS_ACTUAL;
		if(nomArchivo!=null && !"".equals(nomArchivo)){
			/** Se realiza la peticion al BO **/
			beanResValInicioPOACOA = boCargaArchivoRespuesta.agregarNombreArchResp(
					nomArchivo, modulo, getArchitechBean());
			/** Se valida la respuesta y se setean los datos al modelo **/
			if(Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError());
				modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
		     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}else{
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError(),new Object[]{nomArchivo});
				mensaje = validaCodigoPorModulo(beanResValInicioPOACOA.getCodError(), mensaje, modulo);
				modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
		     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResValInicioPOACOA.getTipoError());
		     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		     	modelAndView.addObject("archRespSustituirHide", nomArchivo);
		     	modelAndView.addObject(ConstantesWebPOACOA.CONS_ACCION, modulo);
			}
		}else{
			/** Se realiza la peticion al BO **/
			beanResValInicioPOACOA = 
				boCargaArchivoRespuesta.validaInicioPOACOA(modulo,getArchitechBean());
			/** Seteo de mensajes de error **/
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00075V);
			modelAndView.addObject(Constantes.COD_ERROR, Errores.ED00075V);
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	     	
		}
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			moduloView = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se setea al modelo el modulo correspondiente para poder validar el JSP **/
		modelAndView.addObject(INICIO_POA_COA, beanResValInicioPOACOA.isActivoPOACOA());
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloView);
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	
	
	/**
	 * Actualizar nombre.
	 *
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value = "actualizarNombre.do")
	public ModelAndView actualizarNombre() {
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String mensaje = "";
		String modulo = req.getParameter(ConstantesWebPOACOA.CONS_ACCION);
		String moduloView = ConstantesWebPOACOA.CONS_ACTUAL;
		RequestContext ctx = new RequestContext(req);
		ModelAndView modelAndView = new ModelAndView(PAG_NOMBRE_ARCH_RESPUESTA);
		BeanResValInicioPOACOA beanResValInicioPOACOA = null;
		String nomArchivo = req.getParameter("nombreArchOculto");
		/** Se realiza la peticion al BO **/
		beanResValInicioPOACOA = boCargaArchivoRespuesta.actualizarNombreArchResp(
				nomArchivo, modulo, getArchitechBean());
		/** Se valida la respuesta y se setean los datos al modelo **/
		if(Errores.OK00000V.equals(beanResValInicioPOACOA.getCodError())){
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError());
			modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}else{
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResValInicioPOACOA.getCodError(),new Object[]{nomArchivo});
			modelAndView.addObject(Constantes.COD_ERROR, beanResValInicioPOACOA.getCodError());
	     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResValInicioPOACOA.getTipoError());
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	     	modelAndView.addObject("archRespSustituirHide", nomArchivo);	
		}
		/** Se valida el modulo **/
		if ("2".equals(modulo)) {
			moduloView = ConstantesWebPOACOA.CONS_SPID;
		}
		/** Se setea al modelo el modulo correspondiente para poder validar el JSP **/
		modelAndView.addObject(INICIO_POA_COA, beanResValInicioPOACOA.isActivoPOACOA());
		modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, moduloView);
		/** Retorno del metodo **/
		return modelAndView;

	}
	
	/**
	 * Metodo que sirve para obtener el nombre de la ruta.
	 *
	 * @param dir String con la direccion
	 * @return String con el nombre
	 */
	private String obtieneNombre(String dir){
		String dirNuevo = dir;
		if(dirNuevo!= null ){
			int posx = dirNuevo.lastIndexOf('\\');
			if( posx >= 0 && dirNuevo.length() > posx ){
				dirNuevo = dirNuevo.substring(posx+1);
			}else{
				posx = dirNuevo.lastIndexOf('/');
				if( posx >= 0 && dirNuevo.length() > posx ){
					dirNuevo = dirNuevo.substring(posx+1);
				}				
			}
		}
		/** Retorno del metodo **/
		return dirNuevo;
	}

	/**
	 * Devuelve el valor de boCargaArchivoRespuesta.
	 *
	 * @return the boCargaArchivoRespuesta
	 */
	public BOCargaArchivoRespuesta getBoCargaArchivoRespuesta() {
		return boCargaArchivoRespuesta;
	}

	/**
	 * Modifica el valor de boCargaArchivoRespuesta.
	 *
	 * @param boCargaArchivoRespuesta the boCargaArchivoRespuesta to set
	 */
	public void setBoCargaArchivoRespuesta(
			BOCargaArchivoRespuesta boCargaArchivoRespuesta) {
		this.boCargaArchivoRespuesta = boCargaArchivoRespuesta;
	}
	
	/**
	 * Valida codigo por modulo.
	 *
	 * @param codigo El objeto: codigo
	 * @param mensaje El objeto: mensaje
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	private String validaCodigoPorModulo(String codigo, String mensaje, String modulo) {
		/** Se valida el codigo para reutilizar el mensaje **/
		if ("ED00077V".equals(codigo) && "2".equals(modulo)) {
			mensaje = mensaje.replaceAll("SPEI", "SPID");
		}
		/** Retorno del metodo **/
		return mensaje;
	}
}
