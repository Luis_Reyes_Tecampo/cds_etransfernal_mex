/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerDetRecepOperacionSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018      SMEZA		VECTOR      Creacion
 */
package mx.isban.eTransferNal.controllers.moduloSPEI;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de SPEI - Detalle.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOpera;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BODetRecepOperacion;
/**
 * Clase tipo controlador que maneja los eventos para el flujo de SPEI - Detalle.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerDetRecepOperacionSPEI extends Architech{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4248240962642267027L;

	/** Constante de pantalla. */
	private static final String PAG_DET_RECEPCION = "detRecepcionOperaSPEI";
	
	/** Constante de pantalla. */
	private static final String PAG_RECEPCION = "recepcionOperaSPEI";

	/** Constante de pantalla. */
	private static final String PAGE_RECEPCION_HTO = "recepcionOperaHistoricaSPEI";
	
	/** Constante con la cadena codError.*/
    private static final String COD_ERROR_PUNTO = "codError.";
    
    /** Constante con la cadena codError. */
    private static final String COD_ERROR = "codError";
    
    /** Constante con la cadena tipoError. */
    private static final String TIPO_ERROR = "tipoError";
    
    /** Constante con la cadena descError. */
    private static final String DESC_ERROR = "descError";
    
    /**constante de error **/
	public static final String ERROR =" Se genero el error: ";

	/**  Referencia al servicio del catalogo. */
	private BODetRecepOperacion boDetRecepOperacion;
	
	/** Constante con la cadena pagina. */
    private static final String PAGINA = "pagina";
	
	 /** Constante con la cadena sessionBean. */
    private static final String SESSION = "sessionBean";
	
	/**
	 * Det recep operacion.
	 *
	 * @param bean the bean
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "/detRecepOperacion.do")
	public ModelAndView detRecepOperacion(BeanReqTranSpeiRec bean, HttpServletRequest req, HttpServletResponse res) {
	 
		return  mostrarDetalleSPEI(bean, req, false) ;		  
	}
	 
	 
	/**
	 * Det recep operacion hto.
	 *
	 * @param bean the bean
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "/detRecepOpHto.do")
	public ModelAndView detRecepOperacionHto(BeanReqTranSpeiRec bean, HttpServletRequest req, HttpServletResponse res) {
		/**se llama la funcion **/ 
		return  mostrarDetalleSPEI(bean, req, true) ;
	}
	
	/**
	 * Mostrar detalle spei.
	 *
	 * @param bean the bean
	 * @param req the req
	 * @param historico the historico
	 * @return the model and view
	 */
	public ModelAndView mostrarDetalleSPEI( BeanReqTranSpeiRec bean, HttpServletRequest req, boolean historico) {
		/**inicializa las variables **/
		ModelAndView modelSPEI = null;
		ArchitechSessionBean sessionBean = getArchitechBean();
		BeanDetRecepOperacionDAO detSPEI = new BeanDetRecepOperacionDAO();
		//se inicializa detalle
		detSPEI.setBeanDetRecepOperacion(new BeanDetRecepOperacion());
		detSPEI.getBeanDetRecepOperacion().setDetalleGral(new BeanDetRecepOpera());
		RequestContext ctx = new RequestContext(req);
		/**si es historico manda llama una pagina encaso la normal**/
		if(historico) {
			modelSPEI = new ModelAndView(PAGE_RECEPCION_HTO);
		}else {
			modelSPEI = new ModelAndView(PAG_RECEPCION);
		}
		/**inicializa las variables**/
		String fechaOpera = "";
		String cveMiInstitucion = "";
		String cveInstOrd = "";
		String folioPaq = "";
		String folioPago = "";
		String usuario = "";
		
			/**Se verifican los seleccionados**/
			List<BeanTranSpeiRecOper>  listSelecionados = new ArrayList<BeanTranSpeiRecOper>();
			
			/**se recorren los registros**/
			for(BeanTranSpeiRecOper beanTranSpeiRec:bean.getListBeanTranSpeiRec()){					    
				if(beanTranSpeiRec.isSeleccionado()){
				   	 listSelecionados.add(beanTranSpeiRec);
					 
				   	 fechaOpera = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion();
				   	 cveMiInstitucion = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveMiInstitucion();
				   	 cveInstOrd = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getCveInstOrd();
				   	 folioPaq = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete();
				   	 folioPago = beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago();
				   	 usuario=  beanTranSpeiRec.getUsuario();
				   	 // se asigna datos generals del objeto
				   	 detSPEI.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(fechaOpera);
				   	 detSPEI.getBeanDetRecepOperacion().getDetalleGral().setCveMiInstitucion(cveMiInstitucion);
				   	 detSPEI.getBeanDetRecepOperacion().getDetalleGral().setCveInstOrd(cveInstOrd);
				   	 detSPEI.getBeanDetRecepOperacion().getDetalleGral().setFolioPaquete(folioPaq);
				   	 detSPEI.getBeanDetRecepOperacion().getDetalleGral().setFolioPago(folioPago);
				   	 detSPEI.getBeanDetRecepOperacion().setNombrePantalla(bean.getBeanComun().getNombrePantalla());
				   	 
				    }					   
			}
			
			if(listSelecionados.isEmpty()){
				/**Seleccione un registro**/
				modelSPEI.addObject(COD_ERROR ,Errores.ED00026V);
				String mensaje = ctx.getMessage(COD_ERROR_PUNTO+Errores.ED00026V);
				modelSPEI.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
				modelSPEI.addObject(DESC_ERROR, mensaje);
				
			}else{
				modelSPEI = new ModelAndView(PAG_DET_RECEPCION);
				/**llenar las listas para la pantalla**/			 
				detSPEI.setParametro("SPEI");
				modelSPEI.addObject(COD_ERROR , detSPEI.getBeanDetRecepOperacion().getError().getCodError());
				String mensaje = ctx.getMessage(COD_ERROR_PUNTO + detSPEI.getBeanDetRecepOperacion().getError().getCodError());
				modelSPEI.addObject(TIPO_ERROR, detSPEI.getBeanDetRecepOperacion().getError().getTipoError());
				modelSPEI.addObject(DESC_ERROR, mensaje);
			 }
		
		/**En casod e existir el histoico llama una pagina**/
		if(historico) {
			detSPEI.setParametro("RECEPCIONHTO");
			modelSPEI.addObject(PAGINA, "rOHS");
		}else {
			detSPEI.setParametro("RECEPCION");
			modelSPEI.addObject(PAGINA, "rOS");
		}
		/** setea los valores**/
		modelSPEI.addObject("fchOperacion", fechaOpera);
		modelSPEI.addObject("cveMiInstituc", cveMiInstitucion);
		modelSPEI.addObject("cveInstOrd", cveInstOrd);
		modelSPEI.addObject("folioPaq", folioPaq);
		modelSPEI.addObject("folioPag", folioPago);
		modelSPEI.addObject("beanDatos", detSPEI);	
		modelSPEI.addObject("usuario", usuario);	
		modelSPEI.addObject(SESSION, sessionBean);
		
		// se regresa la vista al jsp	
		return modelSPEI;
	}
	
	

	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad boDetRecepOperacion.
	 *
	 * @return boDetRecepOperacion del tipo BODetRecepOperacion
	 */
	public BODetRecepOperacion getBoDetRecepOperacion() {
		return boDetRecepOperacion;
	}

	/**
	 * Metodo set que permite modificar el parametro boDetRecepOperacion.
	 *
	 * @param boDetRecepOperacion el boDetRecepOperacion a establecer
	 */
	public void setBoDetRecepOperacion(BODetRecepOperacion boDetRecepOperacion) {
		this.boDetRecepOperacion = boDetRecepOperacion;
	}
		
	
}