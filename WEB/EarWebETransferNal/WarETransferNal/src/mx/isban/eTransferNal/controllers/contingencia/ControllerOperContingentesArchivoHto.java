/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerOperContingentesArchivoHto.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/10/2018 10:42:47 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */


package mx.isban.eTransferNal.controllers.contingencia;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.contingencia.BOConsultaOperContingentes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOCargaArchivoEnvio;
import mx.isban.eTransferNal.utilerias.contingencias.UtilsContingenciasOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad del Monitor de carga de archivos.
 *
 * @author FSW-Vector
 * @since 18/10/2018
 */
@Controller
public class ControllerOperContingentesArchivoHto extends Architech  {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -102047098623676018L;
	
		
	/** La constante PANTALLA. */
	private static final String PANTALLA = "operacionesContingentes";
	
	/** La constante TIPO_PANTALLA. */
	private static final String TIPO_PANTALLA = "historica";
	/** La variable que contiene informacion con respecto a: bo carga archivo envio. */
	private BOCargaArchivoEnvio boCargaArchivoEnvio;
	
	/** La variable que contiene informacion con respecto a: bo consulta oper. */
	private BOConsultaOperContingentes boConsultaOper;
	
	/** La variable que contiene informacion con respecto a: util. */
	private UtilsContingenciasOperacion util = UtilsContingenciasOperacion.getInstance();
	
	
	/** Propiedad del tipo String que almacena el valor de BEAN_RES. */
	public static final String BEAN_RES = "beanResConsultaOperContingente";
	

	/**
	 * Muestra nom arch procesar.
	 *
	 * @return Objeto model and view
	 */
	@RequestMapping(value= "/muestraOperacContingentesHTO.do")
	public ModelAndView muestraNomArchProcesar() {
		/**se inicializan las variables**/
		ModelAndView modeloVista = new ModelAndView();		
		BeanPaginador benaPaginador= new BeanPaginador();		
		BeanResConsultaOperContingente beanResConsultaOperContingente = new BeanResConsultaOperContingente();
		/**se asigna el nombre de la pantalla**/
		beanResConsultaOperContingente.setNomPantalla(TIPO_PANTALLA);		
		beanResConsultaOperContingente.setBeanPaginador(benaPaginador);
		/**se llama al jsp**/		
		modeloVista = new ModelAndView(PANTALLA, BEAN_RES, beanResConsultaOperContingente);
		return  modeloVista;
	}
	
	
	/**
	 * Buscar operacion contin hto.
	 *
	 * @param bean El objeto: bean
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/buscarOperacionContinHto.do")
	public ModelAndView buscarOperacionContinHto(BeanReqConsultaOperContingente bean,HttpServletRequest req,
			HttpServletResponse res){
		ModelAndView modeloVista = new ModelAndView();
		/**Se inicializa el paginador**/
		BeanPaginador benaPaginador= new BeanPaginador();
		benaPaginador.setPagina("");
		benaPaginador.setPaginaAnt("");
		benaPaginador.setPaginaFin("");
		benaPaginador.setPaginaIni("");
		benaPaginador.setPaginaSig("");
		benaPaginador.setPaginaIni("");		
		try {
			/**se obtiene si es historica la pantalla**/
			bean.setHistorico( util.obtenerHistorico(bean.getNomPantalla()));
			/**se consultan todos los archivos segín la fecha seleccionada**/
			BeanResConsultaOperContingente beanResConsultaOperContingente = boConsultaOper.consultarOperContingentes(getArchitechBean(),benaPaginador,bean);
			/**se asigna el paginador**/
			beanResConsultaOperContingente.setBeanPaginador(benaPaginador);
			beanResConsultaOperContingente.setNomPantalla(bean.getNomPantalla());
			beanResConsultaOperContingente.setFechaOperacion(bean.getFechaOperacion());
			/**se asigna el bean y el nombre de la pntalla**/
			modeloVista = new ModelAndView(PANTALLA, BEAN_RES, beanResConsultaOperContingente);
			
			/**Se valida el codigo de error **/
			if(!Errores.OK00000V.equals(beanResConsultaOperContingente.getBeanbase().getCodError())){
				/**se asigna el mensaje**/
				modeloVista.addObject(Constantes.COD_ERROR, beanResConsultaOperContingente.getBeanbase().getCodError());
				modeloVista.addObject(Constantes.TIPO_ERROR,beanResConsultaOperContingente.getBeanbase().getTipoError());
				modeloVista.addObject(Constantes.DESC_ERROR, beanResConsultaOperContingente.getBeanbase().getMsgError());
			}
			
		} catch (BusinessException e) {
			showException(e);
			error("Error: "+ e.getMessage(), this.getClass());
		}	
		
		
		return  modeloVista;
	}
	
	
	/**
	 * Obtener el objeto: bo carga archivo envio.
	 *
	 * @return El objeto: bo carga archivo envio
	 */
	public BOCargaArchivoEnvio getBoCargaArchivoEnvio() {
		return boCargaArchivoEnvio;
	}
	
	
	/**
	 * Definir el objeto: bo carga archivo envio.
	 *
	 * @param boCargaArchivoEnvio El nuevo objeto: bo carga archivo envio
	 */
	public void setBoCargaArchivoEnvio(BOCargaArchivoEnvio boCargaArchivoEnvio) {
		this.boCargaArchivoEnvio = boCargaArchivoEnvio;
	}



	/**
	 * Obtener el objeto: bo consulta oper.
	 *
	 * @return El objeto: bo consulta oper
	 */
	public BOConsultaOperContingentes getBoConsultaOper() {
		return boConsultaOper;
	}



	/**
	 * Definir el objeto: bo consulta oper.
	 *
	 * @param boConsultaOper El nuevo objeto: bo consulta oper
	 */
	public void setBoConsultaOper(BOConsultaOperContingentes boConsultaOper) {
		this.boConsultaOper = boConsultaOper;
	}
	
	
	
	

}
