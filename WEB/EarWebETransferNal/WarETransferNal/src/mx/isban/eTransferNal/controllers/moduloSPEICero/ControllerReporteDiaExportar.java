/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerReporteDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	     By 	          Company 	 Description
 * ------- -----------   -----------        ---------- ----------------------
 * 	 1.0   18/04/2018       MTREJO            VECTOR 		Creacion
 *   2.0   25/04/2018       SMEZA             VECTOR 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPEICero;


/**
 * Anexo de Imports para la funcionalidad de la pantalla de SPEI Cero - Emision Reporte del Dia
 * 
 * @author FSW-Vector
 * @sice 18 abril 2018
 *
 */

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.SPEICero.BOEmisionRep;
import mx.isban.eTransferNal.utilerias.MetodosRepoDiaSpeiCero;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de SPEI Cero - Emision Reporte del Dia.
 *
 * @author FSW-Vector
 * @sice 18 abril 2018
 */
@Controller
public class ControllerReporteDiaExportar extends Architech {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8542182737311779841L;
	
	/**
	 * propiedad que almacena CERO
	 */	
	private static final String CERO = "0";
	
	/**
	 * propiedad que almacena UNO
	 */	
	private static final String UNO = "1";
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE
	 */
	public static final String PAGE = "moduloReporteDia";
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE
	 */
	public static final String PAGE_HTO = "moduloReporteHis";
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	public static final String BEAN_RES = "beanResSpeiCero";
	
	/**constante de error **/
	private static final String ERROR =" Se genero el error: ";
	
	/**  Llamada a BO desde el Controller. */
	private BOEmisionRep boEmisionRep;
	/**
	 * variable de utilerias
	 */
	public static final MetodosRepoDiaSpeiCero util = new MetodosRepoDiaSpeiCero();
	/**
	 * @return the util
	 */
	public static MetodosRepoDiaSpeiCero getUtil() {
		return util;
	}
	
	
	
	/**
	 * 
	 * @param beanReqSpeiCero
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping(value = "/bucarSPEICero.do")
	public ModelAndView buscarReporte(BeanReqSpeiCero beanReqSpeiCero, HttpServletRequest req, HttpServletResponse res) {
		/** Retornamos el valor al metodo que lo llamo**/
		return buscarTodo(beanReqSpeiCero, req,  1);
	}
	
	/**
	 * 
	 * @param beanReqSpeiCero
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping(value = "/exportarSPEICer.do")
	public ModelAndView exportarTodo(BeanReqSpeiCero beanReqSpeiCero, HttpServletRequest req, HttpServletResponse res) {
		/** Retornamos el valor al metodo que lo llamo**/
		return buscarTodo(beanReqSpeiCero, req,  2);
	}
	
	
	/**
	 * 
	 * @param beanReqSpeiCero
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping(value = "/edoCtaSPEICer.do")
	public ModelAndView generarEdoCta(BeanReqSpeiCero beanReqSpeiCero, HttpServletRequest req, HttpServletResponse res) {
		/** Retornamos el valor al metodo que lo llamo**/
		return buscarTodo(beanReqSpeiCero, req, 3);
	}
	
	/**
	 * 
	 * @param beanReqSpeiCero
	 * @param req
	 * @param res
	 * @param funcion
	 * @return
	 */
	public ModelAndView buscarTodo(BeanReqSpeiCero beanReqSpeiCero, HttpServletRequest req, int funcion) {
		/**Se inicializan los objetos **/
		ModelAndView mdlVista = null;
		RequestContext ctx = new RequestContext(req);
		BeanResSpeiCero beanResSpeiCero = null;		
		if(beanReqSpeiCero.getRptTodo().equals(CERO)) {
			beanReqSpeiCero.setTodo(false);
			util.buscarDto(beanReqSpeiCero); 
		}else {
			
			beanReqSpeiCero.setTodo(true);
			beanReqSpeiCero.setInstiAlterna(true);
			beanReqSpeiCero.setInstiPrincipal(true);		
			beanReqSpeiCero.setInstiAlterna(true);
			beanReqSpeiCero.setInstiPrincipal(true);
		}
		
		util.fechas(beanReqSpeiCero);
		
		beanReqSpeiCero.setExportar(CERO);
		
		try {
			/**Se realiza la busqueda **/	
			switch(funcion) {
				case 1:
					beanResSpeiCero = boEmisionRep.buscarDatos( getArchitechBean(), beanReqSpeiCero);
					break;
				case 2:
					beanReqSpeiCero.setExportar(UNO);
					beanResSpeiCero = boEmisionRep.exportarTodo(getArchitechBean(),  beanReqSpeiCero) ;	
					break;
				default:
					beanResSpeiCero = boEmisionRep.generarEdoCta(getArchitechBean(),  beanReqSpeiCero) ;	
					break;
			}
			mdlVista= datosBusqueda(beanReqSpeiCero,beanResSpeiCero,funcion,ctx );
		}catch (BusinessException e) {
			
			mdlVista= datosBusqueda(beanReqSpeiCero,beanResSpeiCero,4,ctx );
			error(ERROR+e.getMessage());
			showException(e);
		}
			
		// Retornamos el valor al metodo que lo llamo
		return mdlVista;
	}
	

	/**
	 * funcion para buscar el model de jsp
	 * 
	 * @param beanReqSpeiCero
	 * @param beanResSpeiCero
	 * @param funcion
	 * @param ctx
	 * @return
	 */
	private ModelAndView datosBusqueda(BeanReqSpeiCero beanReqSpeiCero,BeanResSpeiCero beanResSpeiCero,int funcion,RequestContext ctx ) {
		ModelAndView mdlVista = null;
		String mensaje ="";
		if(beanReqSpeiCero.getRptTodo().equals(CERO)) {
			beanReqSpeiCero.setTodo(false);
		}else {
			beanReqSpeiCero.setTodo(true);
		}
		debug("consTranSpeiRec responseEmiReporte Codigo de error:" + beanResSpeiCero.getCodError());
		/**Se asigna los errores **/
		
		if(beanReqSpeiCero.getHistorico().equals(CERO)) {
			mdlVista = new ModelAndView(PAGE, BEAN_RES, beanResSpeiCero);
		}else {
			mdlVista = new ModelAndView(PAGE_HTO, BEAN_RES, beanResSpeiCero);
		}
		
		if(funcion!=4) {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + beanResSpeiCero.getCodError());
			
			if(funcion==2) {
				mensaje = beanResSpeiCero.getMsgError(); 
			}
			
			
			mdlVista.addObject(Constantes.DESC_ERROR, mensaje);	
			mdlVista.addObject(Constantes.COD_ERROR, beanResSpeiCero.getCodError());
			mdlVista.addObject(Constantes.TIPO_ERROR, beanResSpeiCero.getTipoError());			
		}else {
			mdlVista = util.msjError(  mdlVista); 
		}
		
		if(beanReqSpeiCero.getHistorico().equals(CERO)) {
			mdlVista.addObject( "frm", "ReporteDia" );
		}else {
			mdlVista.addObject( "frm", "ReporteHis" );
		}
		info("<----- Enviamos los datos: " + mdlVista);
		return mdlVista;
	}
	
	/**
	 * @return the boEmisionRep
	 */
	public BOEmisionRep getBoEmisionRep() {
		return boEmisionRep;
	}

	/**
	 * @param boEmisionRep the boEmisionRep to set
	 */
	public void setBoEmisionRep(BOEmisionRep boEmisionRep) {
		this.boEmisionRep = boEmisionRep;
	}

	
	

	
	
	
}