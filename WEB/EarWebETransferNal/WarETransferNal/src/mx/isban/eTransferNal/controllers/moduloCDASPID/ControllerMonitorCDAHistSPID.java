/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 16:54:08 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonCDAHistSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistMontosSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistVolumenSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOMonitorCDAHistSPID;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la consulta de Movimientos Historicos CDAs
 *
 */
@Controller
public class ControllerMonitorCDAHistSPID extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -8396037496907638074L;
	
	/**
	 * La constante COD_ERROR
	 */
	//La constante COD_ERROR
	private static final String COD_ERROR = "codError";

	/**
	 * La constante COD_ERROR
	 */
	//La constante COD_ERROR
	private static final String COD_ERROR_PUNTO = "codError.";

	/**
	 * La constante DESC_ERROR
	 */
	//La constante DESC_ERROR
	private static final String DESC_ERROR = "descError";

	/**
	 * La constante TIPO_ERROR
	 */
	//La constante TIPO_ERROR
	private static final String TIPO_ERROR = "tipoError";
	
	/**
	 * La constante VISTA
	 */
	//La constante VISTA
	private static final String VISTA = "monitorCDAHistSPID";
	
	/**
	 * La constante BEAN
	 */
	//La constante BEAN
	private static final String BEAN = "beanResMonitorCDA";
	
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOMonitorCDAHistSPID boMonitorCDAHistSPID;

	/**
	 * Metodo que se utiliza para mostrar la pagina de monitorCDA
	 * monitor CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	 //Metodo que se utiliza para mostrar la pagina de monitorCDA
	 @RequestMapping(value = "/iniciaMonitorCDAHistSPID.do")
	 public ModelAndView muestraMonitorCDAHist(HttpServletRequest req, HttpServletResponse res) {		 
		 ModelAndView modelAndView = null;
		 BeanResMonitorCdaHistSPID beanResMonitorCDA = null;
		 BeanResMonitorCdaHistMontosSPID beanResMonitorCdaMontos = new BeanResMonitorCdaHistMontosSPID();
		 beanResMonitorCdaMontos.setMontoCDAConfirmadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAContingencia(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAEnviadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAPendEnviar(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDARechazadas(Constantes.CERO_DEC);
		 
		 BeanResMonitorCdaHistVolumenSPID beanResMonitorCdaVolumen = new BeanResMonitorCdaHistVolumenSPID();
		 beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAContingencia(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAEnviadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDARechazadas(Constantes.CERO);
		 
		 beanResMonitorCDA = new BeanResMonitorCdaHistSPID();
		 beanResMonitorCDA.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
		 beanResMonitorCDA.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
		 modelAndView = new ModelAndView(VISTA,BEAN,beanResMonitorCDA);
		//Se setea la fecha actual
		 modelAndView.addObject("fechaHoy", obtnerFechaActual());
		 
		 return modelAndView;
	 }
	 
	/**
	 * Metodo que solicita la informacion para el monitor CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	 //Metodo que solicita la informacion para el monitor CDA
	@RequestMapping(value="/consInfMonCDAHistSPID.do")
	public ModelAndView consInfMonCDA(HttpServletRequest req, HttpServletResponse res){
		Utilerias utilerias = Utilerias.getUtilerias();
		ModelAndView modelAndView = null;      
		BeanReqMonCDAHistSPID beanReqMonCDAHist = new BeanReqMonCDAHistSPID();
		RequestContext ctx = new RequestContext(req);
		BeanResMonitorCdaHistBO beanResMonitorCDA = null;
	    beanReqMonCDAHist.setFechaOperacion(utilerias.getString(req.getParameter("paramFechaOperacion")));
	    
	    if(beanReqMonCDAHist.getFechaOperacion()!=null && "".equals(beanReqMonCDAHist.getFechaOperacion())){
	    	modelAndView = new ModelAndView(VISTA,BEAN,beanResMonitorCDA);
	    	String mensaje = ctx.getMessage(COD_ERROR_PUNTO+Errores.ED00063V);
	     	modelAndView.addObject(COD_ERROR, Errores.ED00063V);
	     	modelAndView.addObject(TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	     	modelAndView.addObject(DESC_ERROR, mensaje);
	     	return modelAndView;
	    }
	    //Se hace la consulta al BO
        try {
    	    beanResMonitorCDA = boMonitorCDAHistSPID.obtenerDatosMonitorCDASPID(beanReqMonCDAHist, getArchitechBean());
    	    debug("consInfMonCDA boMonitorCDA.obtenerDatosMonitorCDA Codigo de error:"+beanResMonitorCDA.getCodError());
    	    modelAndView = new ModelAndView(VISTA,BEAN,beanResMonitorCDA);
	        String mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResMonitorCDA.getCodError());
	     	modelAndView.addObject(COD_ERROR, beanResMonitorCDA.getCodError());
	     	modelAndView.addObject(DESC_ERROR, mensaje);
	     	modelAndView.addObject(TIPO_ERROR, beanResMonitorCDA.getTipoError());
 	    } catch (BusinessException e) {
 	    	//Se procesa excepcion
			showException(e);
			modelAndView = new ModelAndView(VISTA,
					BEAN, beanResMonitorCDA);
			String mensaje = ctx.getMessage(COD_ERROR_PUNTO
					+ e.getCode());
			modelAndView.addObject(COD_ERROR, e.getCode());
			modelAndView.addObject(TIPO_ERROR,e.getClass());
			modelAndView.addObject(DESC_ERROR, mensaje);
 	    }
        //Se setea la fecha actual
		modelAndView.addObject("fechaHoy", obtnerFechaActual());

		return modelAndView;	    
	 }

	/**
	 * @param boMonitorCDAHistSPID Valor a establecer en atributo: boMonitorCDAHistSPID
	 */
	public void setBoMonitorCDAHistSPID(BOMonitorCDAHistSPID boMonitorCDAHistSPID) {
		this.boMonitorCDAHistSPID = boMonitorCDAHistSPID;
	}
	
	/**
	 * Metodo que obtienen la fecha actual
	 * @return Objeto del tipo String
	 */
	//Metodo que obtienen la fecha actual
	public String obtnerFechaActual(){
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		return utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	}
}
