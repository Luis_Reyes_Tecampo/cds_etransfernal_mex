/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerRecepcionOperacionExportar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/09/2018 04:41:32 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecComun;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOpApartada;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 * Clase tipo controlador que maneja los eventos de exportar de las pantalla de recepcion de operaciones de cuenta alterna y principal.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepOperacionApartadas extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;
	

	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BORecepOperacion bORecepOperacion;
	/** Propiedad del tipo BORecepOpApartada que almacena el valor de bORecepOpApartada. */	
	private BORecepOpApartada bORecepOpApartada;
	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modelApartadas;

	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();
	
		
	/**
	 * Apartar operaciones.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param req El objeto: req
	 * @param re El objeto: re
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "apartaOperaciones.do")
	public ModelAndView apartarOperaciones(BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletRequest req, 
			HttpServletResponse re,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
		
		modelApartadas = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		//inicializa bean comun
		recepOperaAdmonSaldos.setBeanComun(new BeanTranSpeiRecComun());
		BeanResTranSpeiRec recepOperaApart = new BeanResTranSpeiRec();
		RequestContext ctx = new RequestContext(req);
		/**se declara la variable de historico **/
		boolean historico =util.obtenerHistorico(beanReqConsTranSpei);
		
		try {			
			beanReqConsTranSpei.getPaginador().setAccion("ACT");
			/**funcion para apartar las operaciones**/
			recepOperaApart = bORecepOpApartada.apartaOperaciones(beanReqConsTranSpei, getArchitechBean(), historico);
			
			/**re sealiza la consulta de los datos**/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpei, getArchitechBean(), historico,beanTranSpeiRecOper);
			recepOperaAdmonSaldos.getBeanComun().setNombrePantalla(beanReqConsTranSpei.getBeanComun().getNombrePantalla());
			
			/**Se llama a la vista a mostrar o pantalla**/
			modelApartadas =  util.vistaMostrar(beanReqConsTranSpei,recepOperaAdmonSaldos,historico);		
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + recepOperaApart.getBeanError().getCodError());
			ArchitechSessionBean sessionBean = getArchitechBean();
		 	modelApartadas.addObject("sessionBean", sessionBean);
		 	modelApartadas.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			modelApartadas.addObject(Constantes.COD_ERROR, recepOperaApart.getBeanError().getCodError());
			modelApartadas.addObject(Constantes.TIPO_ERROR, recepOperaApart.getBeanError().getTipoError());
			modelApartadas.addObject(Constantes.DESC_ERROR, mensaje);
			
		}catch (BusinessException e) {
			showException(e);
			modelApartadas = util.errorVista(historico, recepOperaAdmonSaldos,beanReqConsTranSpei,ctx);	
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage(), this.getClass());
		}
		
		
		return modelApartadas;
	}
	
	
	/**
	 * Liberar oper apartadas.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param req El objeto: req
	 * @param re El objeto: re
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "liberarOperApartadas.do")
	public ModelAndView liberarOperApartadas(BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletRequest req, 
			HttpServletResponse re,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
		
		modelApartadas = null;
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		BeanResTranSpeiRec recepOperaApartada = new BeanResTranSpeiRec();
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		RequestContext ctx = new RequestContext(req);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		try {
			
			/**funcion para liberar las operaciones**/
			recepOperaApartada = bORecepOpApartada.apartaOperacionesEliminar(beanReqConsTranSpei, getArchitechBean(), historico);
			
			/**re sealiza la consulta de los datos**/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpei, getArchitechBean(), historico,beanTranSpeiRecOper);
			
			/**Se llama a la vista a mostrar o pantalla **/
			modelApartadas =  util.vistaMostrar(beanReqConsTranSpei,recepOperaAdmonSaldos,historico);
			
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + recepOperaApartada.getBeanError().getCodError());
			modelApartadas.addObject(Constantes.COD_ERROR, recepOperaApartada.getBeanError().getCodError());
			modelApartadas.addObject(Constantes.TIPO_ERROR, recepOperaApartada.getBeanError().getTipoError());
			modelApartadas.addObject(Constantes.DESC_ERROR, mensaje);
			ArchitechSessionBean sessionBean = getArchitechBean();
			modelApartadas.addObject("sessionBean", sessionBean); 
			modelApartadas.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
		}catch (BusinessException e) {
			showException(e);
			modelApartadas = util.errorVista(historico, recepOperaAdmonSaldos,beanReqConsTranSpei,ctx);	
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage(), this.getClass());
		}
		
		
		return modelApartadas;
	}
	
	
		
	
	/**
	 * Obtener el objeto: BO recep operacion.
	 *
	 * @return El objeto: BO recep operacion
	 */
	public BORecepOperacion getBORecepOperacion() {
		return bORecepOperacion;
	}

	
	/**
	 * Definir el objeto: BO recep operacion.
	 *
	 * @param recepOperacion El nuevo objeto: BO recep operacion
	 */
	public void setBORecepOperacion(BORecepOperacion recepOperacion) {
		bORecepOperacion = recepOperacion;
	}
	
	/**
	 * Obtener el objeto: b O recep op apartada.
	 *
	 * @return El objeto: b O recep op apartada
	 */
	public BORecepOpApartada getbORecepOpApartada() {
		return bORecepOpApartada;
	}

	/**
	 * Definir el objeto: b O recep op apartada.
	 *
	 * @param bORecepOpApartada El nuevo objeto: b O recep op apartada
	 */
	public void setbORecepOpApartada(BORecepOpApartada bORecepOpApartada) {
		this.bORecepOpApartada = bORecepOpApartada;
	}




	
	

}