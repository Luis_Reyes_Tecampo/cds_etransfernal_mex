/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMonitorNombreArchResp.java
 * 
 * La clase ha sido modificada.
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 06:19:54 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaArchRespuesta;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOConsultarArchivos;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion  de los parametros
**/
@Controller
public class ControllerMonitorNombreArchResp extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**
	 * Constante con el nombre de la pagina
	 */
	private static final String PAG_MON_NOMBRE_ARCH_RESPUESTA ="monitorCargaArchRespuesta";
		
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsultaArchRespuesta";
	
	
	/** BO para nomitor de archivos. */
	private BOConsultarArchivos boConsultarArchivos;

	
   /**Metodo que muestra la funcionalidad del monitor carga archivos
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraMonNomArchRespuesta.do")
   public ModelAndView muestraMonCargaArchCanContig(){
	   /** Declaracion de variables **/
	   ModelAndView model = new ModelAndView(PAG_MON_NOMBRE_ARCH_RESPUESTA);
	   model.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_ACTUAL);
	   model.addObject("tipo", "1");
	   /** Retorno del metodo **/
      return model;
   }
   
   /**
    * Metodo que muestra la funcionalidad del monitor carga archivos para spid.
    *
    * @param beanPaginador El objeto: bean paginador
    * @param bindingResult El objeto: binding result
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/muestraMonNomArchRespuestaSpid.do")
    public ModelAndView muestraMonCargaArchCanContigSpid(BeanPaginador beanPaginador, BindingResult bindingResult){
    	ModelAndView model = new ModelAndView(PAG_MON_NOMBRE_ARCH_RESPUESTA);
    	BeanResConsultaArchRespuesta beanResConsultaArchRespuesta = null;
    	/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        RequestContext ctx = new RequestContext(req);
        /** Validacion de los datos de entrada **/
        if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
        beanResConsultaArchRespuesta = boConsultarArchivos.consultarArchivosProc(beanPaginador, "2", getArchitechBean());
        model.addObject(BEAN_RESULTADO_CONSULTA,beanResConsultaArchRespuesta);
        String mensaje = ctx.getMessage("codError."+beanResConsultaArchRespuesta.getCodError());
        model.addObject("codError", beanResConsultaArchRespuesta.getCodError());
        model.addObject("tipoError", beanResConsultaArchRespuesta.getTipoError());
   	 	model.addObject("descError", mensaje);   
    	model.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_SPID);
    	model.addObject("tipo", "2");
    	/** Retorno del metodo **/
       return model;
    }
	
   /**
    * Metodo que muestra la funcionalidad del monitor carga archivos.
    *
    * @param beanPaginador Objeto del tipo @see BeanPaginador
    * @param bindingResult El objeto: binding result
    * @return ModelAndView Objeto del tipo ModelAndView
    */
   @RequestMapping(value = "/consMonCargaArchResp.do")
   public ModelAndView consultarMonCargaArchCanContig(BeanPaginador beanPaginador, BindingResult bindingResult){
      ModelAndView modelAndView = null;
      BeanResConsultaArchRespuesta beanResConsultaArchRespuesta = null;
      /** Recuperar atributos usando el context **/
	  HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
      RequestContext ctx = new RequestContext(req);
      String modulo = req.getParameter("accion-p");
      /** Validacion de los datos de entrada **/
      if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
      beanResConsultaArchRespuesta = boConsultarArchivos.consultarArchivosProc(beanPaginador, modulo, getArchitechBean());
	  modelAndView = new ModelAndView(PAG_MON_NOMBRE_ARCH_RESPUESTA,BEAN_RESULTADO_CONSULTA,beanResConsultaArchRespuesta);
 	  String mensaje = ctx.getMessage("codError."+beanResConsultaArchRespuesta.getCodError());
 	  modelAndView.addObject("codError", beanResConsultaArchRespuesta.getCodError());
 	  modelAndView.addObject("tipoError", beanResConsultaArchRespuesta.getTipoError());
 	  modelAndView.addObject("descError", mensaje);   
 	  if ("1".equals(modulo)) {
 		  modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_ACTUAL);
 		  modelAndView.addObject("tipo", "1");
 	  }
 	  if ("2".equals(modulo)) {
		  modelAndView.addObject(ConstantesWebPOACOA.CONS_MODULO, ConstantesWebPOACOA.CONS_SPID);
		  modelAndView.addObject("tipo", "2");
	  }
 	  /** Retorno del metodo **/
      return modelAndView;
   }

	/**
	 * Devuelve el valor de boConsultarArchivos
	 * @return the boConsultarArchivos
	 */
	public BOConsultarArchivos getBoConsultarArchivos() {
		return boConsultarArchivos;
	}

	/**
	 * Modifica el valor de boConsultarArchivos
	 * @param boConsultarArchivos the boConsultarArchivos to set
	 */
	public void setBoConsultarArchivos(BOConsultarArchivos boConsultarArchivos) {
		this.boConsultarArchivos = boConsultarArchivos;
	}
}
