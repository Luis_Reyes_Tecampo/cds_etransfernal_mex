/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	     By 	          Company 	 Description
 * ------- -----------   -----------        ---------- ----------------------
 *   1.0   11/04/2018  Salvador Meza Torres  VECTOR 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPEI;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de SPEI - Monitor.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de SPEI - Monitor.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerMonitorSPEI extends Architech{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 179974347403832647L;

	/**
	 * Monitor spei.
	 */
	@RequestMapping(value="/monitorSPEI.do")
	ModelAndView  monitorSPEI(){
		
		return new ModelAndView("monitorSPEI");
	}
}