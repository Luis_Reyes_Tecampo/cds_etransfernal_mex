/**
 * Clase de tipo controller para procesar la pantalla
 * de Catalogos Parametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOMantenimientoParametros;
import mx.isban.eTransferNal.utilerias.UtilsMantenimientoParametros;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * The Class ControllerMantenimientoParametrosGuardar.
 */
@Controller
public class ControllerMantenimientoParametrosGuardar extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5057371910923080911L;

	/** The pagina. */
	private static final String PAGINA_PRINCIPAL = "mantenimientoParametros";
	
	/** The pagina guardar. */
	private static final String PAGINA_GUARDAR = "mantenimientoParametrosGuardar";

	/** The bean response. */
	private static final String BEAN_RESPONSE = "beanResponse";
	
	/** The bean response guardar. */
	private static final String BEAN_RESPONSE_GUARDAR = "beanResponseGuardar";

	/** The bo mantenimiento parametros. */
	private BOMantenimientoParametros boMantenimientoParametros;
	
	/**
	 * Alta nuevo parametro.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "altaParametroNuevo.do")
	public ModelAndView altaNuevoParametro(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanReqMantenimientoParamGuardar request=new BeanReqMantenimientoParamGuardar();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		ModelAndView modelAndView = new ModelAndView(PAGINA_GUARDAR);

		//creacion de beanRequest de mantenimientoParametrosGuardar
		request = utilsMantParam.requestToBeanReqMantenimientoParamGuardar(req);

		try{
			responseBO=boMantenimientoParametros.consultaOpcionesCombos(getArchitechBean());
			//metodo que obtiene y setea en response los filtros de busqueda aplicados en la pantalla principal
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaGuardar(request, responseBO, modelAndView);
			//metodo que obtiene y setea en response la lista de opciones de los combos Defiene trama, cola y mecanismo
			utilsMantParam.generarOpcionesCombosDefine(modelAndView);
			modelAndView.addObject("esAltaNuevoParam", true);
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE_GUARDAR, responseBO);
		return modelAndView;
	}

	/**
	 * Editar mantenimiento parametros.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "editarParametros.do")
	public ModelAndView editarMantenimientoParametros(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParamGuardar requestG=new BeanReqMantenimientoParamGuardar();
		ModelAndView modelAndView = new ModelAndView(PAGINA_GUARDAR);
		String claveParametro="";

		//creacion de beanRequest de mantenimientoParametrosGuardar
		requestG = utilsMantParam.requestToBeanReqMantenimientoParamGuardar(req);
		//se obtiene el registro seleccionado, y de este se obtiene el campo cve parametro
		claveParametro=utilsMantParam.obtenerParametrosSeleccionados(requestG)
																		.get(0).getCveParametro();
		try{
			responseBO=boMantenimientoParametros.consultarDatosParametro(getArchitechBean(), claveParametro);
			//metodo que obtiene y setea en response los filtros de busqueda aplicados en la pantalla principal
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaGuardar(requestG, responseBO, modelAndView);
			//metodo que obtiene y setea en response las datos del parametro a editar
			utilsMantParam.setearDatosParametroSelecccionado(responseBO, responseBO);
			//metodo que obtiene y setea en response la lista de opciones de los combos Defiene trama, cola y mecanismo
			utilsMantParam.generarOpcionesCombosDefine(modelAndView);
			modelAndView.addObject("esAltaNuevoParam", false);
			modelAndView.addObject("cveParamKey", claveParametro);
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		modelAndView.addObject(BEAN_RESPONSE_GUARDAR, responseBO);
		return modelAndView;
	}

	/**
	 * Guardar param.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "guardarParametro.do")
	public ModelAndView guardarParam(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanResMantenimientoParametros resAltaParam=new BeanResMantenimientoParametros();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanResMantenimientoParametros resCombos=new BeanResMantenimientoParametros();
		BeanReqMantenimientoParamGuardar requestG=new BeanReqMantenimientoParamGuardar();
		ModelAndView modelAndView=new ModelAndView(PAGINA_PRINCIPAL);
	    RequestContext ctx = new RequestContext(req);
	    String mensaje = "";

		//creacion de beanRequest de mantenimientoParametros
		requestG=utilsMantParam.requestToBeanReqMantenimientoParamGuardar(req);
		try{
			resCombos=boMantenimientoParametros.consultaOpcionesCombos(getArchitechBean());
			if(requestG.isAltaParametro()){
				//si es altaNuevoParametro
				resAltaParam=boMantenimientoParametros.altaParametroNuevo(getArchitechBean(), requestG);
				//si codError=ED00124V (el parametro insertado ya existe)
				if( Errores.ED00124V.equals(resAltaParam.getCodError()) ){
					//se permanece en pantalla Guardar
					modelAndView=new ModelAndView(PAGINA_GUARDAR);
					
					//se obtien y setea mensaje de error
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+resAltaParam.getCodError());
					modelAndView.addObject(Constantes.DESC_ERROR, mensaje);

					//metodo que obtiene y setea en response las listas de opciones de los combos tipoParametro y tipoDato
					utilsMantParam.enviarResponseOpcionesCombos(resCombos, resAltaParam);
					//metodo que obtiene y setea en response los datos introducidos en los campos para editar el parametro
					utilsMantParam.setearCamposEdicionPantallaGuardar(modelAndView, requestG, resAltaParam);
					modelAndView.addObject(BEAN_RESPONSE_GUARDAR, resAltaParam);
				} else {
					//regresa a pantalla Principal realizando la consulta de parametros
					//se obtien y setea mensaje de error
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+resAltaParam.getCodError());
					modelAndView.addObject(Constantes.DESC_ERROR, mensaje);

					//metodo que realiza la consulta de los parametros para volver a la pantalla principal
					responseBO=realizarConsultaParametrosPantallaPrincipal(requestG);
					//metodo que obtiene y setea en response las listas de opciones de los combos tipoParametro y tipoDato
					utilsMantParam.enviarResponseOpcionesCombos(resCombos, responseBO);
					modelAndView.addObject(BEAN_RESPONSE, responseBO);
				}
				//se envia por modelAndView codigo y tipo de error de operacion altaParametro
				modelAndView.addObject(Constantes.TIPO_ERROR, resAltaParam.getTipoError());
				modelAndView.addObject(Constantes.COD_ERROR, resAltaParam.getCodError());
			} else {
				//si es editarParametro
				responseBO=boMantenimientoParametros.editarParametros(getArchitechBean(), requestG);

				//se genera y setea informacion de error
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+responseBO.getCodError());
				modelAndView.addObject(Constantes.TIPO_ERROR, responseBO.getTipoError());
				modelAndView.addObject(Constantes.COD_ERROR, responseBO.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);

				//metodo que realiza la consulta de los parametros para volver a la pantalla principal
				responseBO=realizarConsultaParametrosPantallaPrincipal(requestG);
				//metodo que obtiene y setea en response las listas de opciones de los combos tipoParametro y tipoDato
				utilsMantParam.enviarResponseOpcionesCombos(resCombos, responseBO);
				modelAndView.addObject(BEAN_RESPONSE, responseBO);
			}
			//metodo que obtiene y setea en response la lista de opciones de los combos define trama, cola y mecanismo
			utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		}catch(BusinessException e){
			showException(e);
			modelAndView=new ModelAndView(PAGINA_GUARDAR);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		return modelAndView;
	}

	/**
	 * Cancelar guardado param.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the model and view
	 */
	@RequestMapping(value = "cancelarGuardado.do")
	public ModelAndView cancelarGuardadoParam(HttpServletRequest req, HttpServletResponse res){
		UtilsMantenimientoParametros utilsMantParam = UtilsMantenimientoParametros.getUtilerias();
		BeanReqMantenimientoParamGuardar requestG=new BeanReqMantenimientoParamGuardar();
		BeanResMantenimientoParametros responseBO=new BeanResMantenimientoParametros();
		BeanResMantenimientoParametros resCombos=new BeanResMantenimientoParametros();
		ModelAndView modelAndView=new ModelAndView(PAGINA_PRINCIPAL);

		//creacion de beanRequest de mantenimientoParametros
		requestG=utilsMantParam.requestToBeanReqMantenimientoParamGuardar(req);
		try{
			resCombos=boMantenimientoParametros.consultaOpcionesCombos(getArchitechBean());
			//metodo que realiza la consulta de los parametros para volver a la pantalla principal
			responseBO=realizarConsultaParametrosPantallaPrincipal(requestG);
			//metodo que obtiene y setea en response las listas de opciones de los combos tipoParametro y tipoDato
			utilsMantParam.enviarResponseOpcionesCombos(resCombos, responseBO);
			modelAndView.addObject(BEAN_RESPONSE, responseBO);
		}catch(BusinessException e){
			showException(e);
			modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
			modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
		}
		//metodo que obtiene y setea en response la lista de opciones de los combos define trama, cola y mecanismo
		utilsMantParam.generarOpcionesCombosDefine(modelAndView);
		return modelAndView;
	}
	
	/**
	 * Realizar consulta parametros pantalla principal.
	 *
	 * @param requestG the request g
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	private BeanResMantenimientoParametros realizarConsultaParametrosPantallaPrincipal(BeanReqMantenimientoParamGuardar requestG) throws BusinessException {
		BeanReqMantenimientoParametros requestPrincipal=new BeanReqMantenimientoParametros();
		BeanResMantenimientoParametros responseConsulta= new BeanResMantenimientoParametros();
		UtilsMantenimientoParametros utilsMantParam=UtilsMantenimientoParametros.getUtilerias();
		String accion="";
		
		// metodo que obtiene los filtros de buesqueda de beanReqMantenimientoParamGuardar y los setea en beanReqMantenimientoParametros
		utilsMantParam.setearFiltrosBusquedaBeanReqMantantenimientoParametros(requestPrincipal, requestG);
		accion=requestPrincipal.getPaginador().getAccion();
		//si la paginaActual es diferente a vacio
		if("ACT".equals(accion)){
			// se realiza la consulta de parametros
			responseConsulta=boMantenimientoParametros.consultarParametros(getArchitechBean(), requestPrincipal);
			utilsMantParam.setearFiltrosBusquedaParametrosPantallaPrincipal(requestPrincipal, responseConsulta);
		}
		return responseConsulta;
	}
	
	/**
	 * Sets the bo mantenimiento parametros.
	 *
	 * @param boMantenimientoParametros the new bo mantenimiento parametros
	 */
	public void setBoMantenimientoParametros(
			BOMantenimientoParametros boMantenimientoParametros) {
		this.boMantenimientoParametros = boMantenimientoParametros;
	}
}
