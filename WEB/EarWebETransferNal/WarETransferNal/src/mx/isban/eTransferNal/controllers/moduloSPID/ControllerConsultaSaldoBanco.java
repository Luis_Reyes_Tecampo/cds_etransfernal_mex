/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaSaldoBanco.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBanco;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOConsultaSaldoBanco;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar la peticion de
 * los parametros
 **/
@Controller
public class ControllerConsultaSaldoBanco extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = 5402888531583955870L;

	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;

	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;

	/**
	 * 
	 */
	private static final String SORT_FIELD = "sortField";

	/**
	 * 
	 */
	private static final String SORT_TYPE = "sortType";

	/**
	 * 
	 */
	private BOConsultaSaldoBanco bOConsultaSaldoBanco;
	
	/**
	 * 
	 */
	private BigDecimal importe;

	/**
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento
	 *            Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/muestraConsultaSaldoBanco.do")
	public ModelAndView consultaSaldoBanco(BeanPaginador beanPaginador,
			BeanOrdenamiento beanOrdenamiento) {
		BeanResConsultaSaldoBanco beanResConsultaSaldoBanco = null;
		ModelAndView model = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResConsultaSaldoBanco = bOConsultaSaldoBanco
					.obtenerConsultaSaldoBanco(beanPaginador,
							getArchitechBean(),
							beanOrdenamiento.getSortField(),
							beanOrdenamiento.getSortType());
			model = new ModelAndView("consultaSaldoBanco",
					"beanResConsultaSaldoBanco", beanResConsultaSaldoBanco);

			String mensaje = messageSource.getMessage("codError."
					+ beanResConsultaSaldoBanco.getCodError(), null, null);
			model.addObject(Constantes.COD_ERROR,
					beanResConsultaSaldoBanco.getCodError());
			model.addObject(Constantes.TIPO_ERROR,
					beanResConsultaSaldoBanco.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			generarContadoresPaginacionMontos(beanResConsultaSaldoBanco, model,getArchitechBean(),
					beanOrdenamiento.getSortField(),beanOrdenamiento.getSortType());
		} catch (BusinessException e) {
			showException(e);
		}

		return model;
	}

	/**
	 * Metodo que exporta los registros que se muestran en el grid de la
	 * Consulta de Saldos por Bancos
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento
	 *            Objeto del tipo BeanOrdenamiento
	 * @param req
	 *            Objeto del tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarConsultaSaldoBanco.do")
	public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador,
			BeanOrdenamiento beanOrdenamiento, HttpServletRequest req) {
		FormatCell formatCellHeaderSB = null;
		FormatCell formatCellBodySB = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		BeanResConsultaSaldoBanco beanResConsultaSaldoBanco = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResConsultaSaldoBanco = bOConsultaSaldoBanco
					.obtenerConsultaSaldoBanco(beanPaginador,
							getArchitechBean(),
							beanOrdenamiento.getSortField(),
							beanOrdenamiento.getSortType());

			if (Errores.OK00000V
					.equals(beanResConsultaSaldoBanco.getCodError())
					|| Errores.ED00011V.equals(beanResConsultaSaldoBanco
							.getCodError())) {
				formatCellBodySB = new FormatCell();
				formatCellBodySB.setFontName("Calibri");
				formatCellBodySB.setFontSize((short) 11);
				formatCellBodySB.setBold(false);

				formatCellHeaderSB = new FormatCell();
				formatCellHeaderSB.setFontName("Calibri");
				formatCellHeaderSB.setFontSize((short) 11);
				formatCellHeaderSB.setBold(true);

				model = new ModelAndView(new ViewExcel());
				model.addObject("FORMAT_HEADER", formatCellHeaderSB);
				model.addObject("FORMAT_CELL", formatCellBodySB);
				model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
				model.addObject("BODY_VALUE", getBody(beanResConsultaSaldoBanco,
						ctx.getMessage("moduloSPID.consultaSaldoBanco.text.totales")
						));
				model.addObject("NAME_SHEET", "OrdenConSalBan");
			} else {
				model = consultaSaldoBanco(beanPaginador, beanOrdenamiento);
				String mensaje = messageSource.getMessage("codError."
						+ beanResConsultaSaldoBanco.getCodError(), null, null);
				model.addObject("codError",
						beanResConsultaSaldoBanco.getCodError());
				model.addObject("tipoError",
						beanResConsultaSaldoBanco.getTipoError());
				model.addObject("descError", mensaje);
				model.addObject("sessionSPID", sessionSPID);
				model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
				model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			}
		} catch (BusinessException e) {
			showException(e);
		}

		return model;
	}

	/**
	 * Metodo que exporta todos los registros existentes de la Consulta de
	 * Saldos por Banco
	 * 
	 * @param beanReqConsultaSaldoBanco
	 *            Objeto del tipo BeanReqConsultaSaldoBanco
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarTodoConsultaSaldoBanco.do")
	public ModelAndView expTodosBitacoraAdmon(
			BeanReqConsultaSaldoBanco beanReqConsultaSaldoBanco) {
		ModelAndView model = null;

		BeanResConsultaSaldoBanco beanResConsultaSaldoBanco = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResConsultaSaldoBanco = bOConsultaSaldoBanco
					.exportarTodoConsultaSaldoBanco(beanReqConsultaSaldoBanco,
							getArchitechBean());
			model = new ModelAndView("consultaSaldoBanco");

			String mensaje = messageSource.getMessage("codError."+beanResConsultaSaldoBanco.getCodError(),new Object[]{beanResConsultaSaldoBanco.getNombreArchivo()}, null);
			
			model.addObject("beanResConsultaSaldoBanco",
					beanResConsultaSaldoBanco);
			model.addObject(Constantes.COD_ERROR,
					beanResConsultaSaldoBanco.getCodError());
			model.addObject(Constantes.TIPO_ERROR,
					beanResConsultaSaldoBanco.getTipoError());
			model.addObject(Constantes.DESC_ERROR,mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("importePagina", importe.toString());
			
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}

	/**
	 * Metodo que obtiene los datos desde el bean los envia a la vista para ser
	 * mostrados
	 * 
	 * @param listaBeanConsultaSaldoBanco
	 *            objeto del tipo List<BeanConsultaSaldoBanco>
	 * @return objList objeto del tipo List<List<Object>>
	 */
	private List<List<Object>> getBody( BeanResConsultaSaldoBanco beanConsulta, String totales){
			List<BeanConsultaSaldoBanco> listaBeanConsultaSaldoBanco = beanConsulta.getListaConsultaSaldoBanco();
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listaBeanConsultaSaldoBanco != null
				&& !listaBeanConsultaSaldoBanco.isEmpty()) {
			objList = new ArrayList<List<Object>>();

			for (BeanConsultaSaldoBanco beanConsultaSaldoBanco : listaBeanConsultaSaldoBanco) {
				objListParam = new ArrayList<Object>();
				objListParam.add(beanConsultaSaldoBanco.getClaveBanco());
				objListParam.add(beanConsultaSaldoBanco.getNombreBanco());
				objListParam.add(beanConsultaSaldoBanco
						.getVolumenOperacionesEnviadas());
				objListParam.add(beanConsultaSaldoBanco
						.getImporteOperacionesEnviadas());
				objListParam.add(beanConsultaSaldoBanco
						.getVolumenOperacionesRecibidas());
				objListParam.add(beanConsultaSaldoBanco
						.getImporteOperacionesRecibidas());
				objListParam.add(beanConsultaSaldoBanco.getSaldo());
				objList.add(objListParam);
			}
			objListParam = new ArrayList<Object>();
			objListParam.add(" ");
			objListParam.add(totales);
			objListParam.add(beanConsulta.getTotalVolumenEnv());
			objListParam.add(beanConsulta.getTotalImporteEnv());
			objListParam.add(beanConsulta.getTotalVolumenRec());
			objListParam.add(beanConsulta.getTotalImporteRec());
			objList.add(objListParam);
		}
		return objList;
	}

	/**
	 * Metodo que genera el Header del archivo excel que se exportó
	 * 
	 * @param ctx
	 *            objeto del tipo RequestContext
	 * @return header objeto del tipo String[]
	 */
	private List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.banco"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.nombre"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.volumenOpeEnv"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.importeOpeEnv"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.volumenOpeRec"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.importeOpeRec"),
				ctx.getMessage("moduloSPID.consultaSaldoBanco.text.saldo"), };
		return Arrays.asList(header);
	}

	/**
	 * 
	 * @param consultaSaldoBanco
	 *            tipo BeanResConsultaSaldoBanco
	 * @param model
	 *            tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(
			BeanResConsultaSaldoBanco consultaSaldoBanco, ModelAndView model) {
		if (consultaSaldoBanco.getTotalReg() > 0) {
			Integer regIni = 1;
			Integer regFin = consultaSaldoBanco.getTotalReg();

			if (!"1".equals(consultaSaldoBanco.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(consultaSaldoBanco
						.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (consultaSaldoBanco.getTotalReg() >= 20 * Integer
					.parseInt(consultaSaldoBanco.getBeanPaginador().getPagina())) {
				regFin = 20 * Integer.parseInt(consultaSaldoBanco
						.getBeanPaginador().getPagina());
			}
			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}
	
	
	/**
	 * 
	 * @param consultaSaldoBanco
	 *            tipo BeanResConsultaSaldoBanco
	 * @param model
	 *            tipo ModelAndView
	 */
	protected void generarContadoresPaginacionMontos(
			BeanResConsultaSaldoBanco consultaSaldoBanco, ModelAndView model, ArchitechSessionBean architechSessionBean, String sortField, String sortType) {
		if (consultaSaldoBanco.getTotalReg() > 0) {
			Integer regIni = 1;
			Integer regFin = consultaSaldoBanco.getTotalReg();

			if (!"1".equals(consultaSaldoBanco.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(consultaSaldoBanco
						.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (consultaSaldoBanco.getTotalReg() >= 20 * Integer
					.parseInt(consultaSaldoBanco.getBeanPaginador().getPagina())) {
				regFin = 20 * Integer.parseInt(consultaSaldoBanco
						.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			try {
				totalImporte = bOConsultaSaldoBanco.obtenerConsultaSaldoBancoMontos(getArchitechBean(),sortField,sortType);
				importe = totalImporte;
			} catch (BusinessException e) {
				showException(e);
			}
			model.addObject("importePagina", totalImporte.toString());
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}

	/**
	 * Devuelve el valor de bOConsultaSaldoBanco
	 * 
	 * @return the bOConsultaSaldoBanco
	 */
	public BOConsultaSaldoBanco getBOConsultaSaldoBanco() {
		return bOConsultaSaldoBanco;
	}

	/**
	 * Modifica el valor de bOConsultaSaldoBanco
	 * 
	 * @param bOConsultaSaldoBanco
	 *            the bOConsultaSaldoBanco to set
	 */
	public void setBOConsultaSaldoBanco(
			BOConsultaSaldoBanco bOConsultaSaldoBanco) {
		this.bOConsultaSaldoBanco = bOConsultaSaldoBanco;
	}

	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}

	/**
	 * @param sessionSPID
	 *            the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID
	 *            the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
