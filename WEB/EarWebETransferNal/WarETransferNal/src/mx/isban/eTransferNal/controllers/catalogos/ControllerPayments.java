package mx.isban.eTransferNal.controllers.catalogos;

import javax.validation.Valid;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultasPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOPayments;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtileriasPayments;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * @version 1.0
 * @Controller ControllerPayments 
 * Catalogo Activar/Desactivar ID Canal �nico
 */
@Controller
public class ControllerPayments extends Architech {

	/**
	 * @author SNGEnterprise
	 * Fecha: 27-11-2020
	 * Hora: 01:08 p.m. 
	*/
	
	/** La constante serialVersionUID. 
	 * El proceso de serialización asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calculará un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 1L;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaPayments";

	/** La constante BEAN_CONSULTAS. */
	private static final String BEAN_CONSULTAS = "beanConsultasPayme";

	/** La constante BEAN_CONSULTA. */
	private static final String BEAN_CONSULTA = "beanConsultaPayme";

	/** La constante BEAN_CONSULTA. */
	private static final String BEAN_CONSUL = "beanConsult";

	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";

	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";
	
	/** La constante PAGINA_MOD. */
	private static final String PAGINA_MOD = "modificarPayments";
	
	/** La constante LISTAS. */
	private static final String LISTAS = "listas";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";

	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";

	/**
	 * La variable que contiene informacion con respecto boPayments
	 * Inicializada por Spring
	 */
	private BOPayments boPayments;

	/** 
	 * @category Object Type String
	 * La constante UTILERIAS
	*/
	private static final UtileriasPayments UTILERIAS = UtileriasPayments.getUtilerias();

	/**
	 * @category Object Type String La constante NAME_COLIBRI Inicializada con
	 *           el valor Colibri
	 */
	private static final String NAME_COLIBRI = "Colibri";

	/** La constante VALIDACION_JSR_303. */
	private static final String VALIDACION_JSR_303 = "Validaciones JSR-303, valores de entrada incorrectos.";

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private ResourceBundleMessageSource messageSource;

	

	/**
	 * Metodo consultaPayments Funcionalidad: Consulta las Activar/Desactivar ID Canal Unico en la BD con el beanFilter y beanPaginador para regresarlos al FE.
	 * @param beanPaginador Objeto con los valores ingresados desde FE respecto a la paginacion del formulario.
	 * @param beanFilter Objeto con los valores ingresados desde FE.
	 * @param bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/consultaPayments.do")
	public ModelAndView consultaPayments(@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador, @ModelAttribute(BEAN_FILTER) @Valid BeanConsultaPayme beanFilter,
			BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		
		ModelAndView model = new ModelAndView();
		BeanConsultasPayme response = null;
		BeanConsultasPayme res = null;

		try {
			res = boPayments.consultasCombos(getArchitechBean(), beanFilter, beanPaginador);
			response = boPayments.consultasPayments(getArchitechBean(), beanFilter, beanPaginador);
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_CONSULTAS, response);
			model.addObject(BEAN_CONSUL, res);
			beanFilter.setClave("");
			beanFilter.setTipoOperacion("TODOS");
			if (!Errores.OK00000V.equals(response.getBeanError().getCodError())) {
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR, response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}

	/**
	 * Metodo editarConsultaPayments Funcionalidad: Inicializa la vista modificarPayments para editar los componentes del bean.
	 * @param beanConsultasPayme Objeto con los valores ingresados desde FE.
	 * @param beanPaginador Objeto con los valores ingresados desde FE respecto a la paginacion del formulario.
	 * @param bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/editarConsultaPayments.do")
	public ModelAndView editarConsultaPayments(@ModelAttribute(BEAN_CONSULTAS) @Valid BeanConsultasPayme beanConsultasPayme,
		 BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		BeanConsultasPayme combos = null;
		ModelAndView model = null;
		BeanConsultaPayme beanReq = new BeanConsultaPayme();
		BeanPaginador beanPaginador = new BeanPaginador();

		if (beanConsultasPayme != null && beanConsultasPayme.getMedEntrega() != null) {
			for (BeanConsultaPayme payment : beanConsultasPayme.getMedEntrega()) {
				if (payment.isSeleccionado()) {
					beanReq = payment;
				}
			}
		}
		try {
			combos = boPayments.consultasPayments(getArchitechBean(), beanReq, beanPaginador);
			model = new ModelAndView(PAGINA_MOD);
			model.addObject(LISTAS, combos);
			if (beanReq.getClave() != null) {
				model.addObject(ACCION, ACCION_MODIFICAR);
			}
			model.addObject(BEAN_CONSULTA, beanReq);
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}

	/**
	 * Metodo modificarConsultaPayments Funcionalidad: Actualiza el registro en la BD de la tabla correspondiente con el bean recibido.
	 * @param beanConsultaPayments Objeto con los valores ingresados desde FE.
	 * @param bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/modificarConsultaPayments.do")
	public ModelAndView modificarConsultaPyments(
			@ModelAttribute(BEAN_CONSULTA) @Valid BeanConsultaPayme beanConsultaPayments,
			BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		ModelAndView model = null;
		BeanConsultasPayme beanConsultasPayments = null;
		BeanConsultasPayme beanCombo = null;

		try {
			// Se conusmen el servicio de edicion de consultaaoj
			BeanResBase response = boPayments.editarconsultasPayments(getArchitechBean(), beanConsultaPayments);
			if (Errores.OK00003V.equals(response.getCodError())) {
				beanConsultasPayments = boPayments.consultasPayments(getArchitechBean(), new BeanConsultaPayme(),
						new BeanPaginador());
				beanCombo =boPayments.consultasCombos(getArchitechBean(), new BeanConsultaPayme(), new BeanPaginador());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_CONSULTAS, beanConsultasPayments);
				model.addObject(BEAN_CONSULTA, new BeanConsultaPayme());
				model.addObject(BEAN_CONSUL, beanCombo);
			} else {
				model = new ModelAndView(PAGINA_MOD);
				model.addObject(BEAN_CONSULTA, beanConsultaPayments);
				model.addObject(ACCION, ACCION_MODIFICAR);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	/**
	 * Metodo exportarConsultaPayments Funcionalidad: Exporta una coleccion de datos recibidos dentro del bean en un archivo "Canales ID Unico.xls".
	 * @param beanConsultasPayme Objeto con los valores ingresados desde FE.
	 * @param bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/exportarPayments.do")
	public ModelAndView exportarConsultaPayments(@ModelAttribute(BEAN_CONSULTAS) @Valid BeanConsultasPayme beanConsultasPayme,
												BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		ModelAndView modelAndView = null;
		FormatCell formatCellBody = null;
		FormatCell formatCellHeader = null;	
		formatCellBody = new FormatCell();
		formatCellBody.setBold(false);
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setFontName(NAME_COLIBRI);
		formatCellHeader = new FormatCell();
		formatCellHeader.setBold(true);
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setFontName(NAME_COLIBRI);
		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);	
		modelAndView.addObject("BODY_VALUE", UTILERIAS.getBody(beanConsultasPayme.getMedEntrega()));
		modelAndView.addObject("NAME_SHEET", "Canales ID Unico");
		modelAndView.addObject("HEADER_VALUE", UTILERIAS.getHeaderExcel(messageSource));
		return modelAndView;
	}

	/**
	 * Metodo exportarTodoConsultaPayments Funcionalidad: Exporta todas las id del canal unico Activada/Desactivada en un repositorio en la BD.
	 * @param beanPaginador Objeto con los valores ingresados desde FE respecto a la paginacion del formulario.
	 * @param beanFilter Objeto con los valores ingresados desde FE.
	 * @param bindingResult Objeto validador de datos del bean.
	 * @throws BusinessException Exception controlada para los errores de ejecucion.
	 * @return ModelAndView Objeto que contiene los datos del modelo y el nombre de vista.
	 */
	@RequestMapping(value = "/exportarTodoConsultaPayments.do")
	public ModelAndView exportarTodoConsultaPayments(@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) @Valid BeanConsultaPayme beanFilter, BindingResult bindingResult) {
		validarBindingResult(bindingResult);
		ModelAndView modelConPay = new ModelAndView();
		BeanResBase responseConPay;
		BeanConsultasPayme responseConsultaPay = new BeanConsultasPayme();

		try {
			responseConsultaPay = boPayments.consultasPayments(getArchitechBean(), beanFilter, beanPaginador);
			if (Errores.ED00011V.equals(responseConsultaPay.getBeanError().getCodError())
					|| Errores.EC00011B.equals(responseConsultaPay.getBeanError().getCodError())) {
				modelConPay = new ModelAndView(PAGINA_CONSULTA, BEAN_CONSULTAS, responseConsultaPay);
				modelConPay.addObject(Constantes.COD_ERROR, responseConsultaPay.getBeanError().getCodError());
				modelConPay.addObject(Constantes.DESC_ERROR, responseConsultaPay.getBeanError().getMsgError());
				modelConPay.addObject(Constantes.TIPO_ERROR, responseConsultaPay.getBeanError().getTipoError());
				return modelConPay;
			}
			responseConPay = boPayments.exportarTodo(getArchitechBean(), beanFilter, responseConsultaPay.getTotalReg());
			modelConPay = new ModelAndView(PAGINA_CONSULTA, BEAN_CONSULTAS, responseConsultaPay);
			if (Errores.OK00002V.equals(responseConPay.getCodError())) {
				String mensajeConPay = messageSource.getMessage(
						Constantes.COD_ERROR_PUNTO + responseConPay.getCodError(),
						new Object[] { responseConPay.getMsgError() }, LocaleContextHolder.getLocale());
				modelConPay.addObject(Constantes.DESC_ERROR, mensajeConPay);
			} else {
				modelConPay.addObject(Constantes.DESC_ERROR, responseConPay.getMsgError());
			}
			modelConPay.addObject(Constantes.COD_ERROR, responseConPay.getCodError());
			modelConPay.addObject(Constantes.TIPO_ERROR, responseConPay.getTipoError());

		} catch (BusinessException e) {
			showException(e);
		}
		return modelConPay;
	}

	/**
	 * Metodo getBoPayments, Funcionalidad: Obtiene la propiedad BOPayments declarada dentro del DispatcherCatalogos-servlet.xml.
	 * @return boPayments Objeto del tipo BOPayments.
	 */
	public BOPayments getBoPayments() {
		return boPayments;
	}

	/**
	 * Metodo setBoPayments, Funcionalidad: Modifica la propiedad BOPayments declarada dentro del DispatcherCatalogos-servlet.xml.
	 * @return boPayments Objeto del tipo BOPayments.
	 */
	public void setBoPayments(BOPayments boPayments) {
		this.boPayments = boPayments;
	}

	/**
	 * Metodo getMessageSource Funcionalidad: Obtiene la propiedad messageSource
	 * declarada dentro del DispatcherCatalogos-servlet.xml.
	 * 
	 * @return messageSource Objeto del tipo ResourceBundleMessageSource.
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo setMessageSource Funcionalidad: Modifica la propiedad
	 * messageSource declarada dentro del DispatcherCatalogos-servlet.xml.
	 * 
	 * @param messageSource
	 *            Objeto del tipo ResourceBundleMessageSource.
	 */
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo validarBindingResult Funcionalidad: Validar si existen excepciones
	 * del Bean y enviarlas.
	 * 
	 * @param bindingResult Objeto validador de datos del bean.
	 */

	public void validarBindingResult(BindingResult bindingResult) {
		if (bindingResult.hasFieldErrors()) {
			error(VALIDACION_JSR_303);
			error(bindingResult.getAllErrors().get(0).getDefaultMessage());
		}

	}

}
