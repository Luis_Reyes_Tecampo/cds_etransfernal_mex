/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerBitacoraAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsBitAdmon2;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsReqBitacoraAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmon;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOBitacoraAdmon;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones de la bitacora administrativa
**/
@Controller
public class ControllerBitacoraAdmon extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**Constante que almacena el nombre de la pagina*/
	private static final String PAGINA_SERVICIO = "bitacoraAdmon";
	
	/** Constante ES_SPID */
	private static final String ES_SPID = "esSPID";
	
   /**
	* Referencia al servicio de bitacora administrativa
    */
	private BOBitacoraAdmon bOBitacoraAdmon;

  /**Metodo que se utiliza para mostrar la pagina de la bitacora administrativa
   * @param req Objeto del tipo HttpServletRequest
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraBitacoraAdmon.do")
   public ModelAndView muestraBitacoraAdmon(HttpServletRequest req){
	  Utilerias utilerias = Utilerias.getUtilerias();
	  Date fechaHoy = null;
	  ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
      Calendar cal = Calendar.getInstance();
      fechaHoy = cal.getTime();
      cal.add(Calendar.MONTH, -3);
      cal.setLenient(true);
      String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
      modelAndView.addObject("fechaHoy", strFechaHoy);
      modelAndView.addObject("fecha3Antes", strFechaHoy);
      modelAndView.addObject("fechaFin", strFechaHoy);
      modelAndView.addObject(ES_SPID, req.getParameter(ES_SPID));
      
      return modelAndView;
   }
   /**Metodo que se utiliza para buscar registros en la funcionalidad
    * de la bitacora administrativa
    * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
    * @param req Objeto del tipo HttpServletRequest
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/buscarBitacoraAdmon.do")
    public ModelAndView buscarBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, HttpServletRequest req){
       Utilerias utilerias = Utilerias.getUtilerias();
       Date fechaHoy = null;
       Calendar cal = Calendar.getInstance();
       fechaHoy = cal.getTime();
       ModelAndView modelAndView = null;
       BeanResConsBitAdmon beanResConsBitAdmon = null;
       RequestContext ctx = new RequestContext(req);
       try {
    	   beanResConsBitAdmon = bOBitacoraAdmon.consultaBitacoraAdmon(beanConsReqBitacoraAdmon, getArchitechBean());
    	   modelAndView = new ModelAndView(PAGINA_SERVICIO,PAGINA_SERVICIO,beanResConsBitAdmon);  
      	   String mensaje = ctx.getMessage("codError."+beanResConsBitAdmon.getCodError());
     	   modelAndView.addObject("codError", beanResConsBitAdmon.getCodError());
     	   modelAndView.addObject("tipoError", beanResConsBitAdmon.getTipoError());
     	   modelAndView.addObject("descError", mensaje);
    	   String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
    	   modelAndView.addObject("fechaHoy", strFechaHoy);
    	   modelAndView.addObject("fecha3Antes", beanConsReqBitacoraAdmon.getFechaAccionIni());
    	   modelAndView.addObject("fechaFin", beanConsReqBitacoraAdmon.getFechaAccionFin());
    	   modelAndView.addObject("strUsuario", beanConsReqBitacoraAdmon.getUsuario());
    	   modelAndView.addObject("strServicio", beanConsReqBitacoraAdmon.getServicio());
    	   modelAndView.addObject(ES_SPID, beanConsReqBitacoraAdmon.getEsSPID());
    	   
	   } catch (BusinessException e) {
		 showException(e);
	   }
	   
	   
       return modelAndView;
    }

    /**Metodo que se utiliza para exportar en la funcionalidad de la
     * bitacora administrativa
     * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
     * @param req Objeto del tipo HttpServletRequest
     * @param servletResponse Objeto del tipo HttpServletResponse
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/expBitacoraAdmon.do")
     public ModelAndView expBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon,HttpServletRequest req,HttpServletResponse servletResponse){
    	FormatCell formatCellHeader = null;
    	FormatCell formatCellBody = null;
    	ModelAndView modelAndView = null;
   	 	RequestContext ctx = new RequestContext(req);

        BeanResConsBitAdmon beanResConsBitAdmon = null;
        try {
           beanConsReqBitacoraAdmon.setServicio(beanConsReqBitacoraAdmon.getParamServicio());
           beanConsReqBitacoraAdmon.setUsuario(beanConsReqBitacoraAdmon.getParamUsuario());
           beanConsReqBitacoraAdmon.setFechaAccionFin(beanConsReqBitacoraAdmon.getParamFechaFin());
           beanConsReqBitacoraAdmon.setFechaAccionIni(beanConsReqBitacoraAdmon.getParamFecha3Antes());
     	   beanResConsBitAdmon = bOBitacoraAdmon.consultaBitacoraAdmon(beanConsReqBitacoraAdmon, getArchitechBean());
     	   if(Errores.OK00000V.equals(beanResConsBitAdmon.getCodError())||
     			   Errores.ED00011V.equals(beanResConsBitAdmon.getCodError())){
     	 	  formatCellBody = new FormatCell();
     	 	  formatCellBody.setFontName("Calibri");
     	 	  formatCellBody.setFontSize((short)11);
     	 	  formatCellBody.setBold(false);
     	 	    
     	 	  formatCellHeader = new FormatCell();
     	 	  formatCellHeader.setFontName("Calibri");
     	 	  formatCellHeader.setFontSize((short)11);
     	 	  formatCellHeader.setBold(true);

     	 	   modelAndView = new ModelAndView(new ViewExcel());
     	       modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
     	 	   modelAndView.addObject("FORMAT_CELL", formatCellBody);
     	 	   modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
     	 	   modelAndView.addObject("BODY_VALUE", getBody(beanResConsBitAdmon.getListBeanConsBitAdmon()));
     	 	   modelAndView.addObject("NAME_SHEET", "BitacoraAdmon");
     	   }else{
     		  modelAndView = buscarBitacoraAdmon(beanConsReqBitacoraAdmon,req);
     		  String mensaje = ctx.getMessage("codError."+beanResConsBitAdmon.getCodError());
       	      modelAndView.addObject("codError", beanResConsBitAdmon.getCodError());
       	      modelAndView.addObject("tipoError", beanResConsBitAdmon.getTipoError());
       	      modelAndView.addObject("descError", mensaje);
       	      
       	      modelAndView.addObject(ES_SPID, beanConsReqBitacoraAdmon.getEsSPID());
     	   }
 	    } catch (BusinessException e) {
 		 showException(e);
 	    }
 	  


 	
 	    return modelAndView;
     }

     /**
      * Metodo privado que sirve para obtener los datos para el excel
      * @param listBeanConsBitAdmon Objeto (List<BeanConsBitAdmon>) con el listado de objetos del tipo BeanConsBitAdmon
      * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
      */
     private List<List<Object>> getBody(List<BeanConsBitAdmon> listBeanConsBitAdmon){
	 	List<Object> objListParam = null;
   	 	List<List<Object>> objList = null;
    	 if(listBeanConsBitAdmon!= null && 
   			   !listBeanConsBitAdmon.isEmpty()){
   		   objList = new ArrayList<List<Object>>();
   	 	   for(BeanConsBitAdmon beanConsBitAdmon : listBeanConsBitAdmon){ 
   	 		  objListParam = new ArrayList<Object>();
   	 		  objListParam.add(beanConsBitAdmon.getServicio());
   	 		  objListParam.add(beanConsBitAdmon.getFechaOpe());
   	 		  objListParam.add(beanConsBitAdmon.getHrOpe());
   	 		  objListParam.add(beanConsBitAdmon.getIp());
   	 		  objListParam.add(beanConsBitAdmon.getModuloCanalOp());
   	 		  objListParam.add(beanConsBitAdmon.getUsuario());
   	 		  objListParam.add(beanConsBitAdmon.getDatoAfectado());
   	 		  objListParam.add(beanConsBitAdmon.getValAnterior());
   	 		  objListParam.add(beanConsBitAdmon.getValNuevo());
   	 		  objListParam.add(beanConsBitAdmon.getDescOperacion());
   	 		  objListParam.add(beanConsBitAdmon.getNumeroRegistros());
   	 		  objListParam.add(beanConsBitAdmon.getNomArchivo());
   	 		  objListParam.add(beanConsBitAdmon.getCodError());
   	 		  objListParam.add(beanConsBitAdmon.getDescCodError());
   	 		  BeanConsBitAdmon2 beanConsBitAdmon2 = beanConsBitAdmon.getBeanConsBitAdmon2();
   	 		  objListParam.add(beanConsBitAdmon2.getFolioOper());
   	 		  objListParam.add(beanConsBitAdmon2.getIdToken());
   	 		  objListParam.add(beanConsBitAdmon2.getIdOperacion());
   	 		  objListParam.add(beanConsBitAdmon2.getEstatusOperacion());
   	 		  objListParam.add(beanConsBitAdmon2.getCodOperacion());
   	 		  objListParam.add(beanConsBitAdmon2.getIdSesion());
   	 		  objListParam.add(beanConsBitAdmon2.getTablaAfectada());
   	 		  objListParam.add(beanConsBitAdmon2.getInstanciaWeb());
   	 		  objListParam.add(beanConsBitAdmon2.getHostWeb());
   	 		  objListParam.add(beanConsBitAdmon2.getDatoFijo());
   	 		 objList.add(objListParam);
   	 	   }
   	   } 
    	return objList;
     }
     
     /**
      * Metodo privado que sirve para obtener los encabezados del Excel
      * @param ctx Objeto del tipo RequestContext
      * @return List<String> Objeto con el listado de String
      */
     private List<String> getHeaderExcel(RequestContext ctx){
    	 String []header = new String [] {
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.servicio"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.fechaOp"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.hrOpe"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.ip"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.modCanalOpe"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.usuario"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.datoAfectado"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.valorAnt"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.valorNuevo"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.descOp"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.numReg"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.nomArchivo"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.codError"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.descCodError"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.folioOper"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.idToken"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.idOperacion"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.estatusOperacion"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.codOpera"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.idSesion"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.tablaAfectada"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.instanciaWeb"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.hostWeb"),
    		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.datoFijo")
    	 	   }; 
    	 return Arrays.asList(header);
     }
     
     /**Metodo que se utiliza para exportar todos en la funcionalidad
     * de la bitacora administrativa
     * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
     * @param req Objeto del tipo HttpServletResponse
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/expTodosBitacoraAdmon.do")
     public ModelAndView expTodosBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon,HttpServletRequest req){
    	 ModelAndView modelAndView = null; 
    	 RequestContext ctx = new RequestContext(req);
    	 Date fechaHoy = null;
         Calendar cal = Calendar.getInstance();
         fechaHoy = cal.getTime();
    	 Utilerias utilerias = Utilerias.getUtilerias();
    	 BeanResConsBitAdmon beanResConsBitAdmon = null;
         try {
    	 beanConsReqBitacoraAdmon.setServicio(beanConsReqBitacoraAdmon.getParamServicio());
         beanConsReqBitacoraAdmon.setUsuario(beanConsReqBitacoraAdmon.getParamUsuario());
         beanConsReqBitacoraAdmon.setFechaAccionFin(beanConsReqBitacoraAdmon.getParamFechaFin());
         beanConsReqBitacoraAdmon.setFechaAccionIni(beanConsReqBitacoraAdmon.getParamFecha3Antes());
      	   beanResConsBitAdmon = bOBitacoraAdmon.expTodosBitacoraAdmon(beanConsReqBitacoraAdmon, getArchitechBean());
      	   modelAndView = new ModelAndView(PAGINA_SERVICIO,PAGINA_SERVICIO,beanResConsBitAdmon);
      	 String mensaje = ctx.getMessage("codError."+beanResConsBitAdmon.getCodError(),new Object[]{beanResConsBitAdmon.getNomArchivo()});
   	     modelAndView.addObject("codError", beanResConsBitAdmon.getCodError());
   	     modelAndView.addObject("tipoError", beanResConsBitAdmon.getTipoError());
   	     modelAndView.addObject("descError", mensaje);
      	   
    	   String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
    	   modelAndView.addObject("fechaHoy", strFechaHoy);
    	   modelAndView.addObject("fecha3Antes", beanConsReqBitacoraAdmon.getFechaAccionIni());
    	   modelAndView.addObject("fechaFin", beanConsReqBitacoraAdmon.getFechaAccionFin());
    	   modelAndView.addObject("strUsuario", beanConsReqBitacoraAdmon.getUsuario());
    	   modelAndView.addObject("strServicio", beanConsReqBitacoraAdmon.getServicio());
    	   modelAndView.addObject(ES_SPID, beanConsReqBitacoraAdmon.getEsSPID());

  	   } catch (BusinessException e) {
  		 showException(e);
  	   }
         return modelAndView;
     }

    
    
    /**
    * Metodo get del servicio de la bitacora administrativa
    * @return bOBitacoraAdmon Objeto del servicio de la bitacora administrativa
    */
    public BOBitacoraAdmon getBOBitacoraAdmon() {
		return bOBitacoraAdmon;
	}
    /**
    * Metodo set del servicio de la bitacora administrativa
    * @param bitacoraAdmon Se setea objeto del servicio de la bitacora administrativa
    */
	public void setBOBitacoraAdmon(BOBitacoraAdmon bitacoraAdmon) {
		bOBitacoraAdmon = bitacoraAdmon;
	}

}
