/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerAdministraSaldo.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/09/2015 16:50:00 INDRA		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranfCtaAltPrincBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranfCtaAltPrincBO;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOAdministraSaldo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la consulta de Movimientos CDAs
 *
 */
@Controller
public class ControllerAdministraSaldo extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -8396037496907638074L;
	/**Constante con la cadena pagina de monitor*/
	private static final String PAG_MONITOR_CUENTA_ALTERNA = "administraSaldos";
	 /**Constante con la cadena descError*/
    private static final String SALDOS = "saldos";    
    /**
     * Constante del tipo String que almacena el valor de COD_ERROR_PUNTO
     */
    private static final String COD_ERROR_PUNTO = "codError."; 
    
 
    
    
    /**
	 *  Referencia al servicio del catalogo
	 * */
	private BOAdministraSaldo bOAdministraSaldo;
	
    
	 /**
	  * Metodo que se utiliza para mostrar la pagina de Administracion de Saldos
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraAdminSaldo.do")
	 public ModelAndView muestraMonitorCuentaAlterna(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 modelAndView = new ModelAndView(PAG_MONITOR_CUENTA_ALTERNA);
		 BeanAdministraSaldoBO beanAdministraSaldoBO = null;

			 beanAdministraSaldoBO = bOAdministraSaldo.obtenerSaldos(getArchitechBean());
			 modelAndView.addObject(SALDOS, beanAdministraSaldoBO);

		 return modelAndView;
	 }
	 
	 
	 /**
	  * Metodo que se utiliza para realizar el envio
	  * @param beanReqTranfCtaAltPrincBO Objeto del tipo BeanReqTranfCtaAltPrincBO
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/envioAlternaPrincipal.do")
	 public ModelAndView envioAlternaPrincipal(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 String mensaje = null;
		 modelAndView = new ModelAndView(PAG_MONITOR_CUENTA_ALTERNA);
	      RequestContext ctx = new RequestContext(req);
		 BeanResTranfCtaAltPrincBO beanResTranfCtaAltPrincBO = null;

			 beanResTranfCtaAltPrincBO = bOAdministraSaldo.envioAlternaPrincipal(beanReqTranfCtaAltPrincBO,getArchitechBean());
			 modelAndView.addObject(SALDOS, beanResTranfCtaAltPrincBO);
			 if("TRIB0000".equals(beanResTranfCtaAltPrincBO.getCodError())){
				 mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResTranfCtaAltPrincBO.getCodError(),
						 new String[]{beanResTranfCtaAltPrincBO.getReferencia(),beanResTranfCtaAltPrincBO.getEstatus()});
				  modelAndView.addObject(Constantes.COD_ERROR, beanResTranfCtaAltPrincBO.getCodError());
		     	  modelAndView.addObject("tipoError", Errores.TIPO_MSJ_INFO);
		     	  modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			 }else{
				 mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResTranfCtaAltPrincBO.getCodError(),
						 new String[]{beanResTranfCtaAltPrincBO.getMsgError()});
//						 new String[]{beanResTranfCtaAltPrincBO.getReferencia(),beanResTranfCtaAltPrincBO.getEstatus()});
				 modelAndView.addObject(Constantes.COD_ERROR, beanResTranfCtaAltPrincBO.getMsgError());
		     	  modelAndView.addObject(Constantes.TIPO_ERROR, beanResTranfCtaAltPrincBO.getTipoError());
		     	  modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			 }

		 return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para realizar el envio
	  * @param beanReqTranfCtaAltPrincBO Objeto del tipo BeanReqTranfCtaAltPrincBO
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/envioPrincipalAlterna.do")
	 public ModelAndView envioPrincipalAlterna(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO, HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 String mensaje = null;
		 modelAndView = new ModelAndView(PAG_MONITOR_CUENTA_ALTERNA);
	      RequestContext ctx = new RequestContext(req);
		 BeanResTranfCtaAltPrincBO beanResTranfCtaAltPrincBO = null;
			 beanResTranfCtaAltPrincBO = bOAdministraSaldo.envioPrincipalAlterna(beanReqTranfCtaAltPrincBO,getArchitechBean());
			 modelAndView.addObject(SALDOS, beanResTranfCtaAltPrincBO);
			 if("TRIB0000".equals(beanResTranfCtaAltPrincBO.getCodError())){
				 mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResTranfCtaAltPrincBO.getCodError(),
						 new String[]{beanResTranfCtaAltPrincBO.getReferencia(),beanResTranfCtaAltPrincBO.getEstatus()});
				 modelAndView.addObject(Constantes.COD_ERROR, beanResTranfCtaAltPrincBO.getCodError());
		     	  modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		     	  modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			 }else{
				 mensaje = ctx.getMessage(COD_ERROR_PUNTO+beanResTranfCtaAltPrincBO.getCodError(),
						 new String[]{beanResTranfCtaAltPrincBO.getMsgError()});
//						 new String[]{beanResTranfCtaAltPrincBO.getReferencia(),beanResTranfCtaAltPrincBO.getEstatus()});
				 modelAndView.addObject(Constantes.COD_ERROR, beanResTranfCtaAltPrincBO.getMsgError());
		     	  modelAndView.addObject(Constantes.TIPO_ERROR, beanResTranfCtaAltPrincBO.getTipoError());
		     	  modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			 }

	     	
		 return modelAndView;
	 }
	/**
	 * @return el bOAdministraSaldo
	 */
	public BOAdministraSaldo getBOAdministraSaldo() {
		return bOAdministraSaldo;
	}
	/**
	 * @param administraSaldo el bOAdministraSaldo a establecer
	 */
	public void setBOAdministraSaldo(BOAdministraSaldo administraSaldo) {
		bOAdministraSaldo = administraSaldo;
	}
	
	  
}
