package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;

/**
 * 
 * The Class MetodosAutorizarOperaciones.
 * 
 */
public class MetodosAutorizarOperaciones extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5067048018644552309L;
	
	/**
	 * Obtener registros seleccionados.
	 *
	 * @param reqAutorizaOperaciones the req autoriza operaciones
	 * @return the list
	 * 
	 */
	List<BeanAutorizarOpe> obtenerRegistrosSeleccionados(BeanReqAutorizarOpe reqAutorizaOperaciones){
		List<BeanAutorizarOpe> registrosSeleccionados= new ArrayList<BeanAutorizarOpe>();
		if(reqAutorizaOperaciones.getListOperacionesSeleccionadas() != null){
			for(BeanAutorizarOpe operacion:reqAutorizaOperaciones.getListOperacionesSeleccionadas()){
				if (operacion.isSeleccionado()){
					registrosSeleccionados.add(operacion);
				}
			}
		}
		return registrosSeleccionados;
	}
	
	/**
	 * Validar operaciones apartadas.
	 *
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @param usrAdmin the usr admin
	 * 
	 * @return true, if successful
	 * 
	 */
	boolean validarOperacionesApartadas(List<BeanAutorizarOpe> listOperacionesSeleccionadas, String usrAdmin){
		String estatusApartado;
		int esAdmin = Integer.parseInt(usrAdmin);
		boolean opeApartadas = true;
		if( esAdmin == 1 ){
			//si es usuario administrador
			for(BeanAutorizarOpe opeSeleccionada:listOperacionesSeleccionadas){
				estatusApartado = opeSeleccionada.getStatusOperacionApartada();
				//Si estatus no es 2 (no esta apartada por ningun usuario)
				if( !"2".equals(estatusApartado) ){
					opeApartadas = false;
				}
			}
		} else {
			//si es otro tipo usuario
			for(BeanAutorizarOpe opeSeleccionada:listOperacionesSeleccionadas){
				estatusApartado = opeSeleccionada.getStatusOperacionApartada();
				//Si estatus no es 2 (no esta apartada por ningun usuario) y operacion no esta apartada por usr logado
				if( !opeSeleccionada.isApartado() || !"2".equals(estatusApartado) ){
					opeApartadas = false;
				}
			}
		}
		return opeApartadas;
	}

	/**
	 * Valida estatus oper.
	 *
	 * @param listOpeSelec the list ope selec
	 * @param estatusOpe the estatus ope
	 * 
	 * @return true, if successful
	 * 
	 */
	boolean validaEstatusOper(List<BeanAutorizarOpe> listOpeSelec,int estatusOpe){
		boolean opePendAut = true;
		String estatus = "";
		for(BeanAutorizarOpe elemento:listOpeSelec){
			estatus = elemento.getEstatus();
			if( estatusOpe == 1 || estatusOpe == 3 ){
				//Si es estatus Pendiente
				if( !("PA".equals(estatus) )){
					opePendAut = false;
				}
			}else{
				//Si es estatus Autorizado o Pendiente
				if( !("AU".equals(estatus) || "PA".equals(estatus)) ){
					opePendAut = false;
				}
			}
		}
		return opePendAut;
	}

	/**
	 * Valida ope select por apartar.
	 *
	 * @param listOpeSelec the list ope selec
	 * 
	 * @return true, if successful
	 * 
	 */
	boolean validaOpeSelectPorApartar(List<BeanAutorizarOpe> listOpeSelec){
		boolean seleccionValida = true;
		for(BeanAutorizarOpe elemento:listOpeSelec){
			String usrApart = elemento.getUsrApartado();
			//Si usuario Apartado es vacio ""
			if( !"".equals(usrApart) ){
				seleccionValida=false;
			}
		}
		return seleccionValida;
	}
}
