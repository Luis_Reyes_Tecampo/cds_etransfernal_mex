/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerDetRecepOP.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqGuardarSPIDDet;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BODetRecepOpSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BORecepOperacionSPID;
import mx.isban.eTransferNal.utilerias.comunes.ValidaCadenas;
import mx.isban.eTransferNal.utilerias.comunes.Validate;
import mx.isban.eTransferNal.utilerias.comunes.ValidateFechas;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerDetRecepOP extends Architech{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1443903377994196394L;

	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE = "detalleRecepOpSPID";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	private static final String BEAN_RES = "beanRes";
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE_RECEPCION_OP = "recepcionOperacionSPID";
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BODetRecepOpSPID que almacena el valor de bODetRecepOpSPID
	 */
	private BODetRecepOpSPID bODetRecepOpSPID; 
	
	/**
	 * Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion
	 */
	private BORecepOperacionSPID bORecepOperacionSPID;
	
	 /**
	  * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	  * @param beanReqReceptoresSPID Objeto del tipo BeanReqReceptoresSPID
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraDetRecepOpSPID.do")
	 public ModelAndView muestraDetRecepOp(BeanReqReceptoresSPID beanReqReceptoresSPID) {
		 ModelAndView modelAndView = new ModelAndView();
		 BeanReceptoresSPID beanReceptoresSPIDTemp = null;
		 List<BeanReceptoresSPID> listBeanTranSpeiRec = new ArrayList<BeanReceptoresSPID>();
		 for(BeanReceptoresSPID beanReceptoresSPID:beanReqReceptoresSPID.getListBeanReceptoresSPID()){
	      	  if(beanReceptoresSPID.getOpciones().isSeleccionado()){
	      		beanReceptoresSPIDTemp = beanReceptoresSPID;
	      		listBeanTranSpeiRec.add(beanReceptoresSPID);
	      	  }
		}
		if(listBeanTranSpeiRec.size()>1){
			modelAndView = consTranSpeiRec(beanReqReceptoresSPID);
			modelAndView = consTranSpeiRec(beanReqReceptoresSPID);
			modelAndView.addObject(Constantes.COD_ERROR, "ED00094V");
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	     	modelAndView.addObject(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00094V", null, null));
			return modelAndView;
		}else if(listBeanTranSpeiRec.isEmpty()){
			modelAndView = consTranSpeiRec(beanReqReceptoresSPID);
			modelAndView.addObject(Constantes.COD_ERROR, "ED00068V");
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	     	modelAndView.addObject(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00068V", null, null));
			return modelAndView;
		}else if(beanReceptoresSPIDTemp == null){
			modelAndView = consTranSpeiRec(beanReqReceptoresSPID);
			modelAndView.addObject(Constantes.COD_ERROR, "ED00095V");
	     	modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
	     	modelAndView.addObject(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00095V", null, null));
			return modelAndView;
		} 
		beanReceptoresSPIDTemp.getOpciones().setServicio(beanReqReceptoresSPID.getServicio());
		beanReceptoresSPIDTemp.getOpciones().setOpcion(beanReqReceptoresSPID.getOpcion());
		beanReceptoresSPIDTemp.setPaginador(beanReqReceptoresSPID.getPaginador());
		return new ModelAndView(PAGE,BEAN_RES,beanReceptoresSPIDTemp);
	 }
	 
		/**
		 * @param beanReqReceptoresSPID Bean con la informacion para consultar
		 * @return ModelAndView Objeto de Spring MVC
		 */
		public ModelAndView consTranSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID){
			ModelAndView modelAndView = null;      
			BeanResReceptoresSPID beanResReceptoresSPID = null;

			beanResReceptoresSPID = bORecepOperacionSPID.consultaTodasTransf(beanReqReceptoresSPID, getArchitechBean());
	    	    debug("consTranSpeiRec bORecepOperacion.consTranSpeiRec Codigo de error:"+beanResReceptoresSPID.getCodError());
	    	    modelAndView = new ModelAndView(PAGE_RECEPCION_OP,BEAN_RES,beanResReceptoresSPID);
		        String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResReceptoresSPID.getCodError(), null, null);
		     	modelAndView.addObject(Constantes.COD_ERROR, beanResReceptoresSPID.getCodError());
		     	modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
		     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		       return modelAndView;	    
		 }
	 
	 /**
	  * Metodo que se utiliza para guardar la informacion modificada
	  * @param beanReqGuardarSPIDDet Objeto del tipo BeanReqGuardarSPIDDet
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/guardarDetRecepOpSPID.do")
	 public ModelAndView guardarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet) {
		 ModelAndView modelAndView  = new ModelAndView(PAGE);
		 BeanResReceptoresSPID beanResReceptoresSPID = new BeanResReceptoresSPID();
		 Map<String, String> model = valida(beanReqGuardarSPIDDet.getBeanReq());
		 String codError = "";
		if(model.containsKey(Constantes.COD_ERROR)){
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
			modelAndView.addAllObjects(model);
			modelAndView.addObject(BEAN_RES,beanReqGuardarSPIDDet.getBeanReq());
			codError = model.get(Constantes.COD_ERROR);
		}else{
			beanResReceptoresSPID = bODetRecepOpSPID.guardarDetRecepOp(beanReqGuardarSPIDDet,getArchitechBean());
			modelAndView.addObject(BEAN_RES,beanResReceptoresSPID.getBeanReceptoresSPID());
			codError = beanResReceptoresSPID.getCodError();
			modelAndView.addObject(Constantes.TIPO_ERROR, beanResReceptoresSPID.getTipoError());
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+codError, null, null);
			modelAndView.addObject(Constantes.COD_ERROR, codError);
	     	modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
		}
		
	       
	     	
		

		
		return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	  * @param beanReqGuardarSPIDDet Objeto del tipo BeanReqGuardarSPIDDet
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/refrescarDetRecepOpSPID.do")
	 public ModelAndView refrescarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet) {
		 
		 BeanResReceptoresSPID beanResReceptoresSPID = bODetRecepOpSPID.consultarDetRecepOp(beanReqGuardarSPIDDet, getArchitechBean());
		return new ModelAndView(PAGE,BEAN_RES,beanResReceptoresSPID.getBeanReceptoresSPID());
	 }


	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bODetRecepOpSPID
	 * @return bODetRecepOpSPID Objeto del tipo BODetRecepOpSPID
	 */
	public BODetRecepOpSPID getBODetRecepOpSPID() {
		return bODetRecepOpSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bODetRecepOpSPID
	 * @param detRecepOpSPID del tipo BODetRecepOpSPID
	 */
	public void setBODetRecepOpSPID(BODetRecepOpSPID detRecepOpSPID) {
		bODetRecepOpSPID = detRecepOpSPID;
	}
	
	/**
	 * Metodo encargado de realizar las validaciones
	 * @param beanReq Bean con los parametros a validar
	 * @return Map del tipo Map<String, Object> con el resultado
	 */
	private Map<String, String> valida(BeanReceptoresSPID beanReq){
		Validate validate =  new Validate();
		 String mensaje ="";
		 Map<String, String> model = new HashMap<String, String>();

		
		if(!validate.esValidoNumero(beanReq.getLlave().getCveInstOrd(), Validate.REG_NUMERICO_MAX_12)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Cve Inst Ord"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validate.esValidoNumero(beanReq.getLlave().getFolioPaquete(), Validate.REG_NUMERICO_MAX_12)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Folio Paquete"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validate.esValidoNumero(beanReq.getLlave().getFolioPago(), Validate.REG_NUMERICO_MAX_12)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Folio Pago"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validate.esValidoNumero(beanReq.getDatGenerales().getTopologia(), Validate.REG_TOPOLOGIA)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Topologia"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validate.esValidoNumero(beanReq.getDatGenerales().getEnviar(), Validate.REG_ENVIAR)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Enviar"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validate.esValidoNumero(beanReq.getDatGenerales().getCda(), Validate.REG_NUMERICO_1)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"CDA"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}

		model = valida2(beanReq);
		if(model.containsKey(Constantes.COD_ERROR)){
			return model;
		}
		model = valida3(beanReq);
		if(model.containsKey(Constantes.COD_ERROR)){
			return model;
		}
		model = valida4(beanReq);
		if(model.containsKey(Constantes.COD_ERROR)){
			return model;
		}
		model = valida5(beanReq);
		if(model.containsKey(Constantes.COD_ERROR)){
			return model;
		}
		
		return model;
	}
	
	/**
	 * Metodo encargado de realizar las validaciones
	 * @param beanReq Bean con los parametros a validar
	 * @return Map del tipo Map<String, Object> con el resultado
	 */
	private Map<String, String> valida2(BeanReceptoresSPID beanReq){
		Validate validate =  new Validate();
		ValidaCadenas validaCad = new ValidaCadenas();
		ValidateFechas validaFechas = new ValidateFechas();
		 String mensaje ="";
		 Map<String, String> model = new HashMap<String, String>();
		if(!validaFechas.esValidoFechaVacia(beanReq.getFchPagos().getFchInstrucPago(), ValidateFechas.FORMATO_FECHA_DDMMAAAA)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Fch Instruct Pago"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validaFechas.esValidoFechaVacia(beanReq.getFchPagos().getHoraInstrucPago(), ValidateFechas.FORMATO_FECHA_HHMMSSSS)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Hora Instruct Pago"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validaFechas.esValidoFechaVacia(beanReq.getFchPagos().getFchAceptPago(), ValidateFechas.FORMATO_FECHA_DDMMAAAA)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Fch Acept Pago"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validaFechas.esValidoFechaVacia(beanReq.getFchPagos().getHoraAceptPago(), ValidateFechas.FORMATO_FECHA_HHMMSSSS)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Hora Acept Pago"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		

	
		if(!validate.esValidoImporteMaximoMayor0(beanReq.getDatGenerales().getMonto())){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Monto"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}

		  if(!validaCad.esValidoCadena(beanReq.getBancoOrd().getNombreOrd(),120)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Nombre Ord"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
		  if(!validaCad.esValidoCadena(beanReq.getBancoRec().getNombreRec(),120)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Nombre Rec"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
		  

		if(!validaFechas.esValidoFechaVacia(beanReq.getBancoOrd().getFchConstitOrd(), ValidateFechas.FORMATO_FECHA_DDMMAAAA)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Fch Constit Ord"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
		  return model;
	}
	
	/**
	 * Metodo encargado de realizar las validaciones
	 * @param beanReq Bean con los parametros a validar
	 * @return Map del tipo Map<String, Object> con el resultado
	 */
	private Map<String, String> valida3(BeanReceptoresSPID beanReq){
		Validate validate =  new Validate();
		ValidaCadenas validaCad = new ValidaCadenas();
		 String mensaje ="";
		 Map<String, String> model = new HashMap<String, String>();
			if(!validate.esValidoNumero(beanReq.getBancoOrd().getTipoCuentaOrd(), Validate.REG_NUMERICO_2)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Tipo Cuenta Ord"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			if(!validate.esValidoNumero(beanReq.getBancoRec().getTipoCuentaRec(), Validate.REG_NUMERICO_2)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Tipo Cuenta Rec"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			if(!validate.esValidoNumero(beanReq.getDatGenerales().getClasifOperacion(), Validate.REG_NUMERICO_2)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Clasif Op"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			if(!validaCad.esValidoCadenaVacia(beanReq.getDatGenerales().getTipoOperacion(), Validate.REG_NUMERICO_2)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Tipo Operacion"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			
			if(!validaCad.esValidoCadenaVacia(beanReq.getBancoOrd().getCveIntermeOrd(), Validate.REG_CADENA_ALFANUMERICO_5)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Cve Interme Ord"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			if(!validaCad.esValidoCadenaVacia(beanReq.getBancoOrd().getCodPostalOrd(), Validate.REG_NUMERICO_5)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Codigo Postal"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}
			if(!validaCad.esValidoCadenaVacia(beanReq.getEstatus().getCodigoError(), Validate.REG_CADENA_ALFANUMERICO_8)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Codigo Error"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
			}

		  return model;
	}
	
	/**
	 * Metodo encargado de realizar las validaciones
	 * @param beanReq Bean con los parametros a validar
	 * @return Map del tipo Map<String, Object> con el resultado
	 */
	private Map<String, String> valida4(BeanReceptoresSPID beanReq){
		Validate validate = new Validate();
		 String mensaje ="";
		 Map<String, String> model = new HashMap<String, String>();
		 if(!validate.esValidoNumero(beanReq.getDatGenerales().getTipoPago(),Validate.REG_NUMERICO_MAX_7)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Tipo Pago"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		 if(!validate.esValidoNumero(beanReq.getDatGenerales().getPrioridad(),Validate.REG_NUMERICO_MAX_7)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Prioridad"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		 if(!validate.esValidoNumero(beanReq.getRastreo().getLongRastreo(),Validate.REG_NUMERICO_MAX_7)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Long rastreo"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		 if(!validate.esValidoNumeroVacio(beanReq.getEnvDev().getFolioPaqDev(),Validate.REG_NUMERICO_MAX_12)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Folio Paq Dev"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		 if(!validate.esValidoNumeroVacio(beanReq.getEnvDev().getFolioPagoDev(),Validate.REG_NUMERICO_MAX_12)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Folio Pago Dev"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		if(!validate.esValidoNumero(beanReq.getLlave().getCveMiInstituc(), Validate.REG_NUMERICO_MAX_12)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Cve mi Instituci\u00F3n"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			return model;
		}

		return model;
	}
	
	/**
	 * Metodo encargado de realizar las validaciones
	 * @param beanReq Bean con los parametros a validar
	 * @return Map del tipo Map<String, Object> con el resultado
	 */
	private Map<String, String> valida5(BeanReceptoresSPID beanReq){
		ValidaCadenas validaCad = new ValidaCadenas();
		Validate validate = new Validate();
		 String mensaje ="";
		 Map<String, String> model = new HashMap<String, String>();
		 if(!validate.esValidoNumero(beanReq.getDatGenerales().getRefeTransfer(),Validate.REG_NUMERICO_MAX_12)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Referencia Transfer"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		if(!validaCad.esValidoCadenaVacia(beanReq.getDatGenerales().getConceptoPago(),210)){
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
						, new String[]{"Concepto de Pago"}, null);
				model.put(Constantes.COD_ERROR, Errores.ED00092V);
				model.put(Constantes.DESC_ERROR, mensaje);
				return model;
		  }
		if(!validaCad.esValidoCadena(beanReq.getEstatus().getEstatusBanxico(), Validate.REG_CADENA_SOLO_LETRAS_2)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Estatus Banxico"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validaCad.esValidoCadena(beanReq.getEstatus().getEstatusTransfer(), Validate.REG_CADENA_SOLO_LETRAS_2)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Estatus Transfer"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		if(!validaCad.esValidoCadenaVacia(beanReq.getEnvDev().getMotivoDev(), Validate.REG_NUMERICO_2)){
			mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00092V
					, new String[]{"Motivo Dev"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.DESC_ERROR, mensaje);
			return model;
		}
		return model;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bORecepOperacionSPID
	 * @return bORecepOperacionSPID Objeto del tipo BORecepOperacionSPID
	 */
	public BORecepOperacionSPID getBORecepOperacionSPID() {
		return bORecepOperacionSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bORecepOperacionSPID
	 * @param recepOperacionSPID del tipo BORecepOperacionSPID
	 */
	public void setBORecepOperacionSPID(BORecepOperacionSPID recepOperacionSPID) {
		bORecepOperacionSPID = recepOperacionSPID;
	}
	 
	 
}
