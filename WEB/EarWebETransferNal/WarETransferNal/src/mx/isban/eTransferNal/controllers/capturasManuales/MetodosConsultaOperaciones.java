package mx.isban.eTransferNal.controllers.capturasManuales;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperacionesDetalle;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;


/**
 * The Class MetodosConsultaOperaciones.
 */
public class MetodosConsultaOperaciones extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 4816083253322216692L;

	//Constante FECHA_CAPTURA
	/** Propiedad del tipo String que almacena el valor de fechaCaptura. */
	private static final String FECHA_CAPTURA = "fechaCaptura";

	//Constante USUARIO_CAPTURA
	/** Propiedad del tipo String que almacena el valor de usuarioCaptura. */
	private static final String USUARIO_CAPTURA = "usuarioCaptura";
	
	//Constante TEMPLATE
	/** Propiedad del tipo String que almacena el valor de template. */
	private static final String TEMPLATE = "template";
	
	//Constante USUARIO_AUTORIZA
	/** Propiedad del tipo String que almacena el valor de usuarioAutoriza. */
	private static final String USUARIO_AUTORIZA = "usuarioAutoriza";

	//Constante BEAN_OPER
	/** La constante BEAN. */
	protected static final String BEAN_OPER = "bean";
	
	//Constante LISTA
	/** La constante BEAN. */
	protected static final String LISTA = "lista";
	
	/** La constante ERROR. */
	protected static final String ERROR = "error";
	
	/** The Constant INT_CERO. */
	private static final int INT_CERO = 0;
	
	/**
	 * Llenar datos de request.
	 *
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param modelAndView the model and view
	 * @param beanResConsOperaciones the bean res cons operaciones
	 */
	//Metodo que llena los datos del request
	protected void llenarDatosDeRequest(BeanReqConsOperaciones beanReqConsOperaciones, ModelAndView modelAndView, BeanResConsOperaciones beanResConsOperaciones){
		modelAndView.addObject(FECHA_CAPTURA,
				beanReqConsOperaciones.getFechaCaptura());
		modelAndView.addObject(USUARIO_CAPTURA,
				beanReqConsOperaciones.getUsuarioCaptura());
		modelAndView.addObject(TEMPLATE,
				beanReqConsOperaciones.getTemplate());
		modelAndView.addObject(USUARIO_AUTORIZA,
				beanReqConsOperaciones.getUsuarioAutoriza());
		
		//Se valida que exista una opcion, si no setea una por defecto
		String opcion=beanReqConsOperaciones.getEstatusOperacion();
		if(opcion==null){
			opcion="Seleccione";
		}
		beanResConsOperaciones.setSelectedEstatus(opcion);
		agregarOpcionesCombo(modelAndView);
	}
	
	
	/**
	 * Agregar opciones combo.
	 *
	 * @param modelAndView El objeto: model and view
	 */
	//Metodo que agrega las opciones del combo
	protected void agregarOpcionesCombo(ModelAndView modelAndView){
		List<String> estatusOperacionItems = new ArrayList<String>();
		estatusOperacionItems.add("Seleccione");
		estatusOperacionItems.add("PA");
		estatusOperacionItems.add("AU");
		estatusOperacionItems.add("CO");
		estatusOperacionItems.add("CA");
		estatusOperacionItems.add("RE");
		estatusOperacionItems.add("PV");
		estatusOperacionItems.add("LI");
		estatusOperacionItems.add("EN");
		estatusOperacionItems.add("DV");
		estatusOperacionItems.add("Otro");
		modelAndView.addObject("estatusOperacionItems", estatusOperacionItems);
	}
	
	/**
	 * Llena error.
	 *
	 * @param map the map
	 * @param beanResBase El objeto: bean res base
	 * @param req the reqs
	 */
	//Metodo que llena el error
	protected void llenaError(Map<String, Object> map, BeanResBase beanResBase, HttpServletRequest req, String param){
		if(!Errores.OK00000V.equals(beanResBase.getCodError())){
		    RequestContext ctx = new RequestContext(req);
			map.put(Constantes.TIPO_ERROR, beanResBase.getTipoError());
	  	    map.put(Constantes.COD_ERROR, beanResBase.getCodError());
	  	    //El mensaje se obtiene de los properties a partir del codigo de error
	  	    String mensaje = "";
	  	    if(param == null){
	  	    	mensaje = ctx.getMessage("codError."+beanResBase.getCodError());	
	  	    }else{
	  	    	mensaje = ctx.getMessage("codError."+beanResBase.getCodError(), Arrays.asList(param));
	  	    }
			map.put(Constantes.DESC_ERROR, mensaje);
		}
	}
	
	/**
	 * Metodo privado que sirve para obtener los datos para el excel.
	 *
	 * @param listBeanConsOperaciones El objeto: list bean cons operaciones
	 * @return List<List<Object>> Objeto lista de lista de objetos con los datos
	 *         del excel
	 */
	//Metodo que genera el body de la consulta
	protected List<List<Object>> getBody(
			List<BeanConsOperaciones> listBeanConsOperaciones) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listBeanConsOperaciones != null && !listBeanConsOperaciones.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanConsOperaciones beanConsOperaciones : listBeanConsOperaciones) {
				objListParam = new ArrayList<Object>();
				objListParam.add(beanConsOperaciones.getFolio());
				objListParam.add(beanConsOperaciones.getImporte());
				objListParam.add(beanConsOperaciones.getTemplate());
				objListParam.add(beanConsOperaciones.getUsrCaptura());
				objListParam.add(beanConsOperaciones.getUsrAutoriza());
				objListParam.add(beanConsOperaciones.getEstatus());
				objListParam.add(beanConsOperaciones.getFechaCaptura());
				objListParam.add(beanConsOperaciones.getUsrApartado());
				objListParam.add(beanConsOperaciones.getRefTransfer());
				objListParam.add(beanConsOperaciones.getCodErrorOpe());
				objListParam.add(beanConsOperaciones.getMsgErrorOpe());
				objList.add(objListParam);
			}
		}
		return objList;
	}

	/**
	 * Metodo privado que sirve para obtener los encabezados del Excel.
	 *
	 * @param ctx            Objeto del tipo RequestContext
	 * @return List<String> Objeto con el listado de String
	 */
	//Metodo que obtiene los headers para el archivo
	protected List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.folio"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.importe"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.template"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.usrCaptura"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.usrAutoriza"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.estatus"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.fechaCaptura"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.usrApartado"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.refeTransfer"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.codErrorOpe"),
				ctx.getMessage("moduloCapturasManuales.consultaOperaciones.grid.msgErrorOpe")};
		return Arrays.asList(header);
	}
	
	/**
	 * Mapea bean operacion.
	 *
	 * @param req El objeto: req
	 * @param obligatorios El objeto: obligatorios
	 * @param beanRes El objeto: bean res
	 * @return Objeto bean operacion
	 */
	//Metodo que mapea el bean de operacion
	protected Map<String, Object> mapeaBeanOperacion(HttpServletRequest req, List<String> obligatorios, 
			BeanResConsOperacionesDetalle beanRes) {
		Map<String, Object> objetos = new HashMap<String, Object>();
		BeanOperacion operacion = new BeanOperacion();
		for (Method method : operacion.getClass().getMethods()) {
			if (method.getName().startsWith("set")) {
				String name =  method.getName().substring(3);
				name = name.replace(name.charAt(0), name.toLowerCase().charAt(0));
				info("Variable: " + name);
				String parametro = req.getParameter(name);
				//Se valida que metodo sea del tipo que le corresponde y que no genere excepciones
				boolean resultValida = validaMetodo(method, name, parametro, operacion, objetos);
				if (null != parametro && !StringUtils.EMPTY.equals(parametro) && resultValida) {
					obligatorios.remove(name.toLowerCase());
					actualizaValor(beanRes, name.toLowerCase(), parametro);
				}
			}
		}
		objetos.put(BEAN_OPER, operacion);
		objetos.put(LISTA, obligatorios);
		return objetos;
	}
	
	/**
	 * Valida metodo.
	 *
	 * @param method the method
	 * @param name the name
	 * @param parametro the parametro
	 * @param operacion the operacion
	 * @param objetos the objetos
	 * @return true, if successful
	 */
	//Metodo para validar el seteo de la variable
	private boolean validaMetodo(Method method, String name, String parametro, BeanOperacion operacion, Map<String, Object> objetos){
		try {
			//Obtiene el tipo de dato del parametro
			Class<?>[] arr = method.getParameterTypes();
			if(arr[INT_CERO].isAssignableFrom(Integer.class) && !parametro.trim().isEmpty()){
				//Si es entero se convierte
				method.invoke(operacion, Integer.parseInt(parametro.trim()));
			}else if(arr[INT_CERO].isAssignableFrom(String.class)){
				//Si el parametro es de tipo cadena se asigna el valor obtenido del request
				method.invoke(operacion,  parametro);
			}else if(arr[INT_CERO].isAssignableFrom(Double.class) && !parametro.trim().isEmpty()){
				//Si el parametro es de tipo double
				method.invoke(operacion, Double.parseDouble(parametro.trim()));
			}
			return true;
			//Cacha las excepciones
		} catch (IllegalArgumentException e1) {
			debug("El parametro capturado no coincide con el tipo de dato.");
			objetos.put(ERROR, name);
			showException(e1);
			error(e1.getMessage(), e1.getClass());
		} catch (IllegalAccessException e2) {
			showException(e2);
			error(e2.getMessage(), e2.getClass());
		} catch (InvocationTargetException e3) {
			showException(e3);
			error(e3.getMessage(), e3.getClass());
		}
		return false;
	}
	
	/**
	 * Actualiza valor.
	 *
	 * @param beanRes the bean res
	 * @param name the name
	 * @param nuevoValor the nuevo valor
	 * @return the bean res cons operaciones detalle
	 */
	//Metodo que actualiza valor en el bean
	protected void actualizaValor(BeanResConsOperacionesDetalle beanRes, String name, String nuevoValor){
		List<BeanLayout> layout = beanRes.getLayout();
		for (BeanLayout beanLayout : layout) {
			if (beanLayout.getCampo().equals(name)) {
				beanLayout.setValorActual(nuevoValor);
			}
		}
	}
	
	/**
	 * Gets the obligatorios.
	 *
	 * @param beanResConsOperaciones the bean res cons operaciones
	 * @return the obligatorios
	 */
	//Metodo que obtiene los campos obligatorios
	protected List<String> getObligatorios(BeanResConsOperacionesDetalle beanResConsOperaciones) {
		List<String> obligatorios = new ArrayList<String>();
		debug("Inicia Validacion de Campos Obligatorios");
		for (BeanLayout campo : beanResConsOperaciones.getLayout()) {
			if (campo.getObligatorio() == 1) {
				String nombre = campo.getCampo();
				debug("El campo " + nombre + " Es Obligatorio");
				obligatorios.add(nombre);
			}
		}
		return obligatorios;
	}
	
	/**
	 * Obtiene campos diferentes.
	 *
	 * @param req El objeto: req
	 * @param beanRes El objeto: bean res
	 * @return Objeto list
	 */
	//Metodo que obtiene los campos que han cambiado
	protected List<Map<String, String>> obtieneCamposDiferentes(HttpServletRequest req, BeanResConsOperacionesDetalle beanRes) {
		Map<String, String> valsAnteriores = new HashMap<String, String>();
		Map<String, String> valsNuevos = new HashMap<String, String>();
		Map<String, String> layoutsDicc = new HashMap<String, String>();

		BeanOperacion operacion = new BeanOperacion();
		List<BeanLayout> layout = beanRes.getLayout();
		
		//Codigo que crea el diccionario de los valores actuales del layout
		for (BeanLayout beanLayout : layout) {
			if(beanLayout.getCampo() != null){
				String valorActual=beanLayout.getValorActual();
				if(valorActual == null){
					valorActual = "";
				}
				layoutsDicc.put(beanLayout.getCampo().toLowerCase(), valorActual);
			}
		}
		
		//Codigo que determina los cambios en los campos
		for (Method method : operacion.getClass().getMethods()) {
			if (method.getName().startsWith("set")) {
				String name =  method.getName().substring(3);
				name = name.replace(name.charAt(0), name.toLowerCase().charAt(0));
				info("Variable: " + name);
				String parametro = req.getParameter(name);
				String valorActual = layoutsDicc.get(name.toLowerCase());
				if (null != parametro && null != valorActual && !valorActual.equals(parametro)) {
					valsAnteriores.put(name, valorActual);
					valsNuevos.put(name,parametro);
				}
			}
		}
		
		//Se devuelve una lista de ambas listas de valores nuevos y anteriores
		List<Map<String, String>> response = new ArrayList<Map<String, String>>();
		response.add(valsAnteriores);
		response.add(valsNuevos);
		return response;
	}
}
