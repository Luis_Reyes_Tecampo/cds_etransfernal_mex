/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerAvisoTraspasos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOAvisoTraspasos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Controlador para los movimientos de tipo traspaso
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerAvisoTraspasos extends Architech {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8180536404946940093L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private static final String SORT_FIELD = "sortField";
	
	/**
	 * 
	 */
	private static final String SORT_TYPE = "sortType";
	
	/**
	 * propiedad privada del BO
	 */
	private BOAvisoTraspasos bOAvisoTraspasos;
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value="/muestraAvisoTraspasos.do")
	public ModelAndView consAvisoTraspasos(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento){
		ModelAndView model = new ModelAndView("avisosTraspasos");
		BeanResAvisoTraspasos beanResAvisoTraspasos = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			//Obtencion de bean resultado que sera enviado a la vista (JSP)
			beanResAvisoTraspasos = bOAvisoTraspasos.obtenerAviso(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType(), sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
			
			//Nombramiento y puesta de bean que sera enviado a la vista
			model.addObject("beanResAvisoTraspasos", beanResAvisoTraspasos);
			
			//Mensajes de error, se envia tambien cuando es exitosa la operacion
			model.addObject(Constantes.COD_ERROR, beanResAvisoTraspasos.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResAvisoTraspasos.getTipoError());
			model.addObject(Constantes.DESC_ERROR, beanResAvisoTraspasos.getMsgError());
			model.addObject("sessionSPID", sessionSPID);
			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			generarContadoresPaginacion(beanResAvisoTraspasos, model);
		} catch (BusinessException e) {
			showException(e);
		}
				
		return model;
	}
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarAvisoTraspasos.do")
    public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, HttpServletRequest req){
		FormatCell formatCellHeaderAT = null;
		FormatCell formatCellBodyAT = null;
		ModelAndView model = null;
  	 	RequestContext ctx = new RequestContext(req);

  	 	BeanResAvisoTraspasos beanResAvisoTraspasos = null;
  	 	
  	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
  	 	
  	 	try {  	 		
  	 		beanPaginador.setAccion("ACT");
  	 		beanResAvisoTraspasos = bOAvisoTraspasos.obtenerAviso(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType(), sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
    	   
  	 		if(Errores.OK00000V.equals(beanResAvisoTraspasos.getCodError())||
 				Errores.ED00011V.equals(beanResAvisoTraspasos.getCodError())){
  	 			formatCellBodyAT = new FormatCell();
  	 			formatCellBodyAT.setFontName("Calibri");
  	 			formatCellBodyAT.setFontSize((short)11);
  	 			formatCellBodyAT.setBold(false);
    	 	    
  	 			formatCellHeaderAT = new FormatCell();
  	 			formatCellHeaderAT.setFontName("Calibri");
  	 			formatCellHeaderAT.setFontSize((short)11);
  	 			formatCellHeaderAT.setBold(true);

  	 			model = new ModelAndView(new ViewExcel());
  	 			model.addObject("FORMAT_HEADER", formatCellHeaderAT);
  	 			model.addObject("FORMAT_CELL", formatCellBodyAT);
  	 			model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
  	 			model.addObject("BODY_VALUE", getBody(beanResAvisoTraspasos.getListaConAviso()));
  	 			model.addObject("NAME_SHEET", "AvisoTraspasos");
  	 		}else{
  	 			model = consAvisoTraspasos(beanPaginador, beanOrdenamiento);
  	 			String mensaje = ctx.getMessage("codError."+beanResAvisoTraspasos.getCodError());
  	 			model.addObject("codError", beanResAvisoTraspasos.getCodError());
  	 			model.addObject("tipoError", beanResAvisoTraspasos.getTipoError());
  	 			model.addObject("descError", mensaje);
  	 			model.addObject("sessionSPID", sessionSPID);
  	 			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
  				model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
  	 		}
 		} catch (BusinessException e) {
 			showException(e);
	    }
	  
	    return model;
    }
    
    /**
     * @param beanReqOrdeReparacion Objeto del tipo BeanReqAvisoTraspasos
     * @return ModelAndView
     */
    @RequestMapping(value = "/exportarTodoAvisoTraspasos.do")
    public ModelAndView expTodosBitacoraAdmon(BeanReqAvisoTraspasos beanReqOrdeReparacion){
   	 	ModelAndView model = null; 
   	 	BeanResAvisoTraspasos beanResAvisoTraspasos = null;
   	 	
   	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
   	 	
   	 	try { 			
   	 		beanResAvisoTraspasos = bOAvisoTraspasos.exportarTodoAvisoTraspasos(beanReqOrdeReparacion, getArchitechBean(),sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
 			model = new ModelAndView("avisosTraspasos");
 			
 			String mensaje = messageSource.getMessage("codError."+beanResAvisoTraspasos.getCodError(), new Object[]{beanResAvisoTraspasos.getNombreArchivo()}, null);
 			
 			model.addObject("beanResAvisoTraspasos",beanResAvisoTraspasos);
 			model.addObject("codError", beanResAvisoTraspasos.getCodError());
 			model.addObject("tipoError", beanResAvisoTraspasos.getTipoError());
 			model.addObject("descError", mensaje);
 			model.addObject("sessionSPID", sessionSPID);
 			model.addObject(SORT_FIELD, "");
			model.addObject(SORT_TYPE, "");
			
 	   } catch (BusinessException e) {
 		 showException(e);
 	   }
       return model;
    }
    
	/**
	 * @param listaBeanAvisoTraspasos Objeto del tipo List<BeanAvisoTraspasos>
	 * @return List<List<Object>>
	 */
	private List<List<Object>> getBody(List<BeanAvisoTraspasos> listaBeanAvisoTraspasos){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	if(listaBeanAvisoTraspasos!= null && 
		   !listaBeanAvisoTraspasos.isEmpty()){
  		   objList = new ArrayList<List<Object>>();
  	
  		   for(BeanAvisoTraspasos beanAvisoTraspasos : listaBeanAvisoTraspasos){ 
  	 		  objListParam = new ArrayList<Object>();
  	 		  objListParam.add(beanAvisoTraspasos.getHora());
  	 		  objListParam.add(beanAvisoTraspasos.getFolio());
  	 		  objListParam.add(beanAvisoTraspasos.getTypTrasp());
  	 		  objListParam.add(beanAvisoTraspasos.getImporte());
  	 		  objListParam.add(beanAvisoTraspasos.getSIAC());
  	 		  objList.add(objListParam);
  	 	   }
  	   } 
   	return objList;
    }
	
	/**
	 * 
	 * @param beanResAviso tipo BeanResAvisoTraspasos
	 * @param model tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(BeanResAvisoTraspasos beanResAviso, ModelAndView model) {
		if (beanResAviso.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResAviso.getTotalReg();
			
			if (!"1".equals(beanResAviso.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResAviso.getBeanPaginador().getPagina()) - 1)+1;
			}
			if (beanResAviso.getTotalReg() >= 20 * Integer.parseInt(beanResAviso.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResAviso.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanAvisoTraspasos beanAvisoTrasp : beanResAviso.getListaConAviso()){
				totalImporte = totalImporte.add(new BigDecimal(beanAvisoTrasp.getImporte()));
			}
			model.addObject("importePagina", totalImporte.toString());
			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
			
		}
	}
    
    /**
     * @param ctx Objeto del tipo RequestContext
     * @return List<String>
     */
    private List<String> getHeaderExcel(RequestContext ctx){
   	 String []header = new String [] { 
   			 ctx.getMessage("moduloSPID.avisosTraspasos.text.hora"),
   			 ctx.getMessage("moduloSPID.avisosTraspasos.text.folio"),	
   			 ctx.getMessage("moduloSPID.avisosTraspasos.text.tipo"),
   			 ctx.getMessage("moduloSPID.avisosTraspasos.text.importe"),
   			 ctx.getMessage("moduloSPID.avisosTraspasos.text.referencia")
   			 }; 
   	 	return Arrays.asList(header);
    }

	/**
	 * @return BOAvisoTraspasos
	 */
	public BOAvisoTraspasos getBOAvisoTraspasos() {
		return bOAvisoTraspasos;
	}

	/**
	 * @param bOAvisoTraspasos Objeto del tipo BOAvisoTraspasos
	 */
	public void setBOAvisoTraspasos(BOAvisoTraspasos bOAvisoTraspasos) {
		this.bOAvisoTraspasos = bOAvisoTraspasos;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
