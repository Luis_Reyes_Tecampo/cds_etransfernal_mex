/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOConsultaRecepciones;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar las peticiones
 * de la consulta de Recepciones
 **/
@Controller
public class ControllerConsultaRecepciones extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -3206579133865242639L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;

	/**
	 * Referencia al servicio de bo Consulta Recepciones
	 */
	private BOConsultaRecepciones bOConsultaRecepciones;
	
	/**
	 * Metodo que se utiliza para mostrar la pagina de la Consulta Recepciones
	 * 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param tipoOrden
	 *            Objeto del tipo @RequestParam
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 */
	@RequestMapping(value = "/consultaRecepciones.do")
	public ModelAndView consultaRecepciones(BeanPaginador beanPaginador, @RequestParam("ref") String tipoOrden, 
			BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		
		ModelAndView model = null;
		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		
		String tipo = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		tipo = tipoOrden.replace(",", "");
		beanReqConsultaRecepciones.setTipoOrden(tipo);
		beanReqConsultaRecepciones.setSortField("folioPago");
		beanReqConsultaRecepciones.setSortType("ASC");

		try {			
			beanResConsultaRecepciones = bOConsultaRecepciones.obtenerConsultaRecepciones(beanPaginador,
					getArchitechBean(), beanReqConsultaRecepciones, sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
			beanResConsultaRecepciones.setTipoOrden(tipo);
			if ("PA".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes  Recibidas por Aplicar");
			} else if ("TR".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas Aplicadas");
			} else if ("RE".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas por Rechazar");
			}
			String mensaje = messageSource.getMessage("codError." + beanResConsultaRecepciones.getCodError(), null, null);
			model = new ModelAndView("consultaRecepciones", "beanResConsultaRecepciones", beanResConsultaRecepciones);
			model.addObject(Constantes.COD_ERROR, beanResConsultaRecepciones.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResConsultaRecepciones.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("sortField", beanReqConsultaRecepciones.getSortField());
			model.addObject("sortType", beanReqConsultaRecepciones.getSortType());
			 generarContadoresPag(model, beanResConsultaRecepciones);
		} catch (BusinessException e) {
			showException(e);
		}

		return model;
	}


	/**
	 * Metodo que se utiliza para exportar los registros pagina de la Consulta
	 * Recepciones
	 * 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param req
	 *            Objeto del tipo HttpServletRequest
	 * @param tipoOrden
	 *            Objeto del tipo @RequestParam
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 */

	@RequestMapping(value = "/exportarConsultaRecepciones.do")
	public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador, HttpServletRequest req,
			@RequestParam("ref") String tipoOrden, BeanReqConsultaRecepciones beanReqConsultaRecepciones) {
		
		String tipo = null;
		
		tipo = tipoOrden.replace(",", "");
		FormatCell formatCellHeaderCR = null;
		FormatCell formatCellBodyCR = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		beanReqConsultaRecepciones.setTipoOrden(tipo);

		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanReqConsultaRecepciones.setSortField("folioPago");
			beanReqConsultaRecepciones.setSortType("ASC");
			beanPaginador.setAccion("ACT");
			beanResConsultaRecepciones = bOConsultaRecepciones.obtenerConsultaRecepciones(beanPaginador,
					getArchitechBean(), beanReqConsultaRecepciones, sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
			if (Errores.OK00000V.equals(beanResConsultaRecepciones.getCodError())
					|| Errores.ED00011V.equals(beanResConsultaRecepciones.getCodError())) {

				formatCellBodyCR = new FormatCell();
				formatCellBodyCR.setFontName("Calibri");
				formatCellBodyCR.setFontSize((short) 11);
				formatCellBodyCR.setBold(false);

				formatCellHeaderCR = new FormatCell();
				formatCellHeaderCR.setFontName("Calibri");
				formatCellHeaderCR.setFontSize((short) 11);
				formatCellHeaderCR.setBold(true);

				model = new ModelAndView(new ViewExcel());
				model.addObject("FORMAT_HEADER", formatCellHeaderCR);
				model.addObject("FORMAT_CELL", formatCellBodyCR);
				model.addObject("HEADER_VALUE", getHeaderExcel(ctx));

				model.addObject("BODY_VALUE", getBody(beanResConsultaRecepciones.getListaConsultaRecepciones()));
				model.addObject("NAME_SHEET", obtenerNombre(tipoOrden));
			} else {
				model = consultaRecepciones(beanPaginador, beanResConsultaRecepciones.getTipoOrden(),
						beanReqConsultaRecepciones);
				String mensaje = messageSource.getMessage("codError." + beanResConsultaRecepciones.getCodError(), null, null);
				model.addObject("codError", beanResConsultaRecepciones.getCodError());
				model.addObject("tipoError", beanResConsultaRecepciones.getTipoError());
				model.addObject("descError", mensaje);
				model.addObject("sessionSPID", sessionSPID);
				model.addObject("sortField", beanReqConsultaRecepciones.getSortField());
				model.addObject("sortType", beanReqConsultaRecepciones.getSortType());
			}
		} catch (BusinessException e) {
			showException(e);

		}

		return model;
	}
	
	/**
	 * @param tipo Objeto del tipo String
	 * @return String
	 */
	private String obtenerNombre(String tipo)
	{
		String nombre ="";
		
		if ("PA".equalsIgnoreCase(tipo)){
			nombre = "RECEPCIONES_PA";
		} else if ("TR".equalsIgnoreCase(tipo)){
			nombre = "RECEPCIONES_TR";
		} else if ("RE".equalsIgnoreCase(tipo)){
			nombre = "RECEPCIONES_RE";
		}
		return nombre;
	}

	/**
	 * Metodo que se utiliza para exportar todo de la pagina de la Consulta
	 * Recepciones
	 * 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 * @param tipoOrden
	 *            Objeto del tipo @RequestParam
	 * @param beanReqConsultaRecepciones
	 *            Objeto del tipo BeanReqConsultaRecepciones
	 */
	@RequestMapping(value = "/exportarTodoConsultaRecepciones.do")
	public ModelAndView expTodosBitacoraAdmon(BeanReqConsultaRecepciones beanReqConsultaRecepciones,
			@RequestParam("ref") String tipoOrden) {
		ModelAndView model = null;
		
		String tipo = null;
		
		tipo = tipoOrden.replace(",", "");

		BeanResConsultaRecepciones beanResConsultaRecepciones = null;
		beanReqConsultaRecepciones.setTipoOrden(tipo);
		
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResConsultaRecepciones = bOConsultaRecepciones.exportarTodoConsultaRecepciones(
					beanReqConsultaRecepciones, getArchitechBean(), sessionSPID.getCveInstitucion(), sessionSPID.getFechaOperacion());
			model = new ModelAndView("consultaRecepciones");
			beanResConsultaRecepciones.setTipoOrden(tipo);
			if ("PA".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes  Recibidas por Aplicar");
			} else if ("TR".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas Aplicadas");
			} else if ("RE".equalsIgnoreCase(tipo)) {
				beanResConsultaRecepciones.setTitulo("Movimientos de Ordenes Recibidas por Rechazar");
			}
			String mensaje = messageSource.getMessage("codError."+beanResConsultaRecepciones.getCodError(), new Object[]{beanResConsultaRecepciones.getNombreArchivo()}, null);
			
			model.addObject("beanResConsultaRecepciones", beanResConsultaRecepciones);
			model.addObject(Constantes.COD_ERROR, beanResConsultaRecepciones.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResConsultaRecepciones.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			generarContadoresPag(model, beanResConsultaRecepciones);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	  * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	  * @param beanReqReceptoresSPID Objeto del tipo BeanReqReceptoresSPID
	  * @param tipoOrden Objeto del tipo RequestParam String
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraDetRecep.do")
	 public ModelAndView muestraDetRecepOp(BeanPaginador beanPaginador, BeanSPIDLlave  beanLlave, @RequestParam("ref") String tipoOrden) {
		 
		 BeanReceptoresSPID beanReceptoresSPIDTemp = null;
		 
		 BeanResRecepcionSPID beanResRecepcionSPID;
		 
		 BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
		 
		try {
			beanResRecepcionSPID = bOConsultaRecepciones.obtenerDetalleRecepcion(beanLlave, getArchitechBean());
			beanReceptoresSPIDTemp = beanResRecepcionSPID.getBeanReceptorSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		 
		beanReceptoresSPIDTemp.setPaginador(beanPaginador);
		ModelAndView model = new ModelAndView("detRecep", "beanRes", beanReceptoresSPIDTemp);
		model.addObject("titulo", "Detalle de Recepci&oacute;n de Operaciones");
		model.addObject("opcion", tipoOrden);
		model.addObject("sessionSPID", sessionSPID);
		model.addObject("servicio", "consultaRecepciones.do?ref=" + tipoOrden);
		return model;
	 }
	
	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResConsultaRecepciones beanResConsultaRecepciones) {
		if (beanResConsultaRecepciones.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResConsultaRecepciones.getTotalReg();
			
			if (!"1".equals(beanResConsultaRecepciones.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResConsultaRecepciones.getTotalReg() >= 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina());
			}		
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanConsultaRecepciones beanCons : beanResConsultaRecepciones.getListaConsultaRecepciones()){
				totalImporte = totalImporte.add(new BigDecimal(beanCons.getBeanConsultaRecepcionesDos().getMonto()));
			}
			modelAndView.addObject("importePagina", totalImporte.toString());
		}
	}
	
	/**
	 * Metodo que se utiliza para obtener los datos para el excel Consulta
	 * Recepciones
	 * 
	 * @return List<List<Object>> Lista con Objetos de los datos del Excel
	 * @param listaBeanConsultaRecepciones
	 *            Lista del tipo BeanConsultaRecepciones
	 */

	private List<List<Object>> getBody(List<BeanConsultaRecepciones> listaBeanConsultaRecepciones) {

		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listaBeanConsultaRecepciones != null && !listaBeanConsultaRecepciones.isEmpty()) {
			objList = new ArrayList<List<Object>>();

			for (BeanConsultaRecepciones beanConsultaRecepciones : listaBeanConsultaRecepciones) {
				objListParam = new ArrayList<Object>();
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getTopologia());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getTipoPago());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getEstatusBanxico());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getEstatusTransfer());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFechOperacion());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFechCaptura());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getCveInsOrd());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getCveIntermeOrd());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFolioPaquete());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFolioPago());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getNumCuentaRec());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getMonto());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getMotivoDevol());
				objListParam.add(beanConsultaRecepciones.getCodigoError());
				objListParam.add(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getNumCuentaTran());
				objList.add(objListParam);
			}
		}
		return objList;
	}

	/**
	 * Metodo que se utiliza para obtener los datos para el encabezado del Excel
	 * Consulta Recepciones
	 * 
	 * @return <List<String> Lista con encabezado de los datos del Excel
	 * @param ctx
	 *            Objeto del tipo RequestContext
	 */
	private List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {

				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtTopo"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtTipo"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtEstatus"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtEstatus"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFchOperacion"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtHora"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtIntsOrd"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtInterOrd"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFoliosPaquete"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFoliosPago"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCuentaRec"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtImporte"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtDevol"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCodErr"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCuentaTran")

		};
		return Arrays.asList(header);
	}

	/**
	 * Metodo get del servicio de Consulta Recepciones
	 * 
	 * @return bOConsultaRecepciones Objeto del servicio de Consulta Recepciones
	 */
	public BOConsultaRecepciones getBOConsultaRecepciones() {
		return bOConsultaRecepciones;
	}

	/**
	 * Metodo set del servicio de Consulta Recepciones
	 * 
	 * @param bOConsultaRecepciones objeto del tipo BOConsultaRecepciones
	 *            Recepciones Se setea objeto del servicio de Consulta
	 *            Recepciones
	 */

	public void setBOConsultaRecepciones(BOConsultaRecepciones bOConsultaRecepciones) {
		this.bOConsultaRecepciones = bOConsultaRecepciones;
	}

	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
