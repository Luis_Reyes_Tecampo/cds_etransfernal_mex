package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanal;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaCanales;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOConfiguracionCanales;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion  de los parametros
**/
@Controller
public class ControllerCatalogoCanales extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Registros iniciales **/
	private static final String REG_INI = "1";
	
	/** Registros finales **/
	private static final String REG_FIN = "21";
	
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsultaCanales";
	
	/** Pagina inicial grid canales. */
	private final static String PAG_INI = "1";
	
	
	
	/**
	 * Constante con el nombre de la pagina
	 */
	private static final String PAG_CATALOGO ="catalogoCanales";
	
	/** Constante lista canales. */
	private static final String LISTA_CANALES = "listaCanales";
	
	/** BO con los metodos para las operaciones del catalogo de canales*/
	private BOConfiguracionCanales boConfiguracionCanales;
	
	/**
	 * Metodo que muestra la pantalla de inicial de catalogos de canales
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="catalogoCanalesInit.do")
	public ModelAndView muestraCatalogo(final HttpServletRequest request){
		Map<String, Object> model = new HashMap<String,Object>();
		RequestContext ctx = new RequestContext(request);
		BeanResCanal beanResCanal = boConfiguracionCanales.obtenerCanales(getArchitechBean());
		if(!Errores.OK00000V.equals(beanResCanal.getCodError())){
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResCanal.getCodError());
			model.put(Constantes.COD_ERROR, beanResCanal.getCodError());
			model.put(Constantes.TIPO_ERROR, beanResCanal.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		
		model.put("listaCanales", beanResCanal.getBeanResCanalList());
		return new ModelAndView(PAG_CATALOGO,model);
	}
	
	  /**Metodo que se utiliza para buscar registros en la funcionalidad
	    * del catalogo de canales
	    * @param beanReqCanal Objeto del tipo @see  beanReqCanal 
	    * @param req Objeto del tipo HttpServletRequest
	    * @return ModelAndView Objeto del tipo ModelAndView
	    */
	    @RequestMapping(value = "/buscarCanal.do")
	    public ModelAndView buscarCanal(BeanReqCanal beanReqCanal, HttpServletRequest req){
	    	Map<String, Object> model = new HashMap<String,Object>();
	        RequestContext ctx = new RequestContext(req);
	        BeanResConsultaCanales beanResConsultaCanales = boConfiguracionCanales.obtenerCanalesPag(beanReqCanal,getArchitechBean());
			if(!Errores.OK00000V.equals(beanResConsultaCanales.getCodError())){
				String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsultaCanales.getCodError());
				model.put(Constantes.COD_ERROR, beanResConsultaCanales.getCodError());
				model.put(Constantes.TIPO_ERROR, beanResConsultaCanales.getTipoError());
				model.put(Constantes.DESC_ERROR, mensaje);
			}
			model.put(BEAN_RESULTADO_CONSULTA, beanResConsultaCanales);
			model.put("listaCanales", beanResConsultaCanales.getBeanResCanalList());
			return new ModelAndView(PAG_CATALOGO,model);
	    }
	

	
	/**
	 * Metodo que muestra la pantalla de alta de catalogos de canales
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="altaCanalesInit.do")
	public ModelAndView muestraAltaCanal(HttpServletRequest req){
		Map<String, Object> model = new HashMap<String,Object>();
		RequestContext ctx = new RequestContext(req);
		BeanResMedEntrega beanResMedEntrega = boConfiguracionCanales.obtenerMediosEntrega(getArchitechBean());
		if(!Errores.OK00000V.equals(beanResMedEntrega.getCodError())){
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResMedEntrega.getCodError());
			model.put(Constantes.COD_ERROR, beanResMedEntrega.getCodError());
			model.put(Constantes.TIPO_ERROR, beanResMedEntrega.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
		}
		
		model.put("listaMedEntrega", beanResMedEntrega.getBeanMedioEntregaList());
		return new ModelAndView("altaCatalogoCanales", model);
	}
	
	/**
	 * Metodo que muestra la pantalla de alta de catalogos de canales
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="altaCanal.do")
	public ModelAndView altaCanal(HttpServletRequest request,
			HttpServletResponse response){
		Map<String, Object> model = new HashMap<String,Object>();
		RequestContext ctx = new RequestContext(request);
		ModelAndView modelAndView = new ModelAndView(); 
		BeanReqCanal reqCanalAlta = new BeanReqCanal();
		reqCanalAlta.setIdCanal((String) request.getParameter("cmbCanal"));
		String actDes = (String) request.getParameter("actDesCanal");
		reqCanalAlta.setActivoCanal(false);
		String mensaje = "";
		if(actDes != null) {
			reqCanalAlta.setActivoCanal(true);
		}
		BeanResConsultaCanales beanResConsultaCanales = boConfiguracionCanales.agregarCanal(reqCanalAlta, getArchitechBean());
		if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())){
			mensaje = "Se dio de alta correctamente al canal " + reqCanalAlta.getIdCanal();
		} else {
			mensaje = "Ocurri\u00f3 un error al intentar dar de alta el canal " + reqCanalAlta.getIdCanal();
		}
			BeanReqCanal reqCanal = new BeanReqCanal();
			reqCanal.setPaginador(new BeanPaginador());
			reqCanal.getPaginador().setRegFin(REG_FIN);
			reqCanal.getPaginador().setRegIni(REG_INI);
			reqCanal.getPaginador().setPagina(PAG_INI);
		if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())){
			model.put(Constantes.TIPO_ERROR, beanResConsultaCanales.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			model.put(BEAN_RESULTADO_CONSULTA, beanResConsultaCanales);
			model.put(LISTA_CANALES, beanResConsultaCanales.getBeanResCanalList());
			modelAndView = new ModelAndView(PAG_CATALOGO,model);
			
		}else{			
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsultaCanales.getCodError());
			model.put(Constantes.COD_ERROR, beanResConsultaCanales.getCodError());
			model.put(Constantes.TIPO_ERROR, beanResConsultaCanales.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			model.put("listaMedEntrega", beanResConsultaCanales.getBeanMedioEntregaList());
			return new ModelAndView("altaCatalogoCanales", model);
		}

		return modelAndView;
	}
	
	/**
	 * Metodo que actualiza el canal
	 * @param reqCanal Objeto del tipo BeanReqCanal de donde se obtiene los datos de la pantalla
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="actualizaCanal.do")
	public ModelAndView actualizaCanal(BeanReqCanal reqCanal, HttpServletRequest request,
			HttpServletResponse response){
		Map<String, Object> model = new HashMap<String,Object>();
		BeanReqCanal reqCanalAct = new BeanReqCanal();
		reqCanalAct.setIdCanal((String) request.getParameter("chBoxActDesHide"));
		reqCanalAct.setPaginador(reqCanal.getPaginador());
		
		String actDes = (String) request.getParameter(reqCanalAct.getIdCanal());
		reqCanalAct.setActivoCanal(false);
		String mensaje = "";
		if(actDes != null) {
			reqCanalAct.setActivoCanal(true);
		}
		BeanResConsultaCanales beanResConsultaCanales = boConfiguracionCanales.activarDesactivarCanal(reqCanalAct, getArchitechBean());
		if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())){
			if(reqCanalAct.isActivoCanal()) {
				mensaje = "Se activo correctamente al canal " + reqCanalAct.getIdCanal();
			} else {
				mensaje = "Se desactivo correctamente al canal " + reqCanalAct.getIdCanal();
			}			
		} else {
			mensaje = "Ocurri\u00f3 un error al intentar dar de alta el canal " + reqCanalAct.getIdCanal();
		}

			//BeanReqCanal reqCanal = new BeanReqCanal();
			reqCanal.setPaginador(new BeanPaginador());
			reqCanal.getPaginador().setRegFin(REG_FIN);
			reqCanal.getPaginador().setRegIni(REG_INI);
			reqCanal.getPaginador().setPagina(PAG_INI);


			model.put(Constantes.COD_ERROR, beanResConsultaCanales.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, mensaje);
			model.put(BEAN_RESULTADO_CONSULTA, beanResConsultaCanales);
			model.put(LISTA_CANALES, beanResConsultaCanales.getBeanResCanalList());
			
			return new ModelAndView(PAG_CATALOGO,model);
	}
	
	/**
	 * Metodo que elimina canales
	 * @param beanReqCanal Objeto del tipo BeanReqCanal de donde se obtiene los datos de la pantalla
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="eliminarCanales.do")
	public ModelAndView eliminarCanales(BeanReqCanal beanReqCanal,HttpServletRequest request,
			HttpServletResponse response){
		BeanResConsultaCanales beanResConsultaCanales = null;
		Map<String, Object> model = new HashMap<String,Object>();
		String listaEliminar = (String)request.getParameter("hdnCheckSeleccionados");		
		String[] idsCanales = listaEliminar.split("@");
		StringBuffer mensaje = new StringBuffer();
		for( int i = 0 ; i < idsCanales.length ; i++ ){
			BeanReqCanal reqCanal = new BeanReqCanal();
			reqCanal.setIdCanal(idsCanales[i]);
			beanResConsultaCanales = boConfiguracionCanales.borrarCanal(reqCanal, getArchitechBean());
			if(Errores.OK00000V.equals(beanResConsultaCanales.getCodError())) {
				mensaje.append("Se elimino el canal " + reqCanal.getIdCanal() + " \\n");
			} else {
				mensaje.append("Ocurri\u00f3 un error al intentar eliminar el canal " + reqCanal.getIdCanal() + " \\n");
			}
		}	
		
		beanResConsultaCanales = boConfiguracionCanales.obtenerCanalesPag(beanReqCanal, getArchitechBean());
		model.put(Constantes.COD_ERROR, beanResConsultaCanales.getCodError());
		model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		model.put(Constantes.DESC_ERROR, mensaje);
		model.put(BEAN_RESULTADO_CONSULTA, beanResConsultaCanales);
		model.put(LISTA_CANALES, beanResConsultaCanales.getBeanResCanalList());
		
		return new ModelAndView(PAG_CATALOGO,model);
		
	}
	
	/**
	 * Metodo para la paginacion.
	 *
	 * @param request the request
	 * @param beanReqCanal Request de canal.
	 * @return the model and view
	 */
	@RequestMapping(value = "/paginarCanales.do")
	public ModelAndView paginarAnterior(final HttpServletRequest request,
			BeanReqCanal beanReqCanal) {		
		ModelAndView modelAndView = null;
			
			BeanResConsultaCanales beanResConsultaCanales = boConfiguracionCanales
					.obtenerCanalesPag(beanReqCanal, getArchitechBean());
			modelAndView = new ModelAndView(PAG_CATALOGO,
					BEAN_RESULTADO_CONSULTA, beanResConsultaCanales);
			modelAndView.addObject(LISTA_CANALES,
					beanResConsultaCanales.getBeanResCanalList());			

		return modelAndView;
	}

	/**
	 * Devuelve el valor de boConfiguracionCanales
	 * @return the boConfiguracionCanales
	 */
	public BOConfiguracionCanales getBoConfiguracionCanales() {
		return boConfiguracionCanales;
	}

	/**
	 * Modifica el valor de boConfiguracionCanales
	 * @param boConfiguracionCanales the boConfiguracionCanales to set
	 */
	public void setBoConfiguracionCanales(
			BOConfiguracionCanales boConfiguracionCanales) {
		this.boConfiguracionCanales = boConfiguracionCanales;
	}
	
	
	




}
