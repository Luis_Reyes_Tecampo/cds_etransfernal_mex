/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 17:18:25 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloCDASPID;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCDA;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanMovimientoCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCDASPID;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDASPID.BOConsultaMovHistCDASPID;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar las peticiones
 * para la consulta de Movimientos CDAs.
 */
@Controller
public class ControllerConsultaMovHistCDASPID  extends Architech {
	
	 /** Constante del Serial Version. */
	private static final long serialVersionUID = -102047098623676018L;
	
	/** Constante para establecer la pagina de servicio. */
	private static final String PAGINA_SERVICIO = "consultaMovHistCDASPID";
	
	/** Constante para el parametro de OpeContingencia. */
	private static final String OPE_CONTINGENCIA = "paramOpeContingencia";

	/** Constante para el parametro de fechaHoy. */
	private static final String FECHA_HOY = "fechaHoy";

	/** Constante para establecer nombre del bean. */
	private static final String BEAN_RESULTADO_CONSULTA = "beanResConsMovHistCDA";
	
	/** The Constant COD_ERROR. */
	private static final String COD_ERROR = "codError.";
	
	/** The Constant STR_COD_ERROR. */
	private static final String STR_COD_ERROR = "codError";
	
	/** The Constant STR_TIPO_ERROR. */
	private static final String STR_TIPO_ERROR = "tipoError";
	
	/** The Constant STR_DESC_ERROR. */
	private static final String STR_DESC_ERROR = "descError";
	
	/** The Constant FECHA_OPE_INICIO. */
	private static final String FECHA_OPE_INICIO = "paramFechaOpeInicio";
	
	/** The Constant FECHA_OPE_FIN. */
	private static final String FECHA_OPE_FIN = "paramFechaOpeFin";
	
	/** The Constant HORA_ABONO_INICIO. */
	private static final String HORA_ABONO_INICIO = "paramHrAbonoIni";
	
	/** The Constant HORA_ABONO_FIN. */
	private static final String HORA_ABONO_FIN = "paramHrAbonoFin";
	
	/** The Constant HORA_ENVIO_INICIO. */
	private static final String HORA_ENVIO_INICIO = "paramHrEnvioIni";
	
	/** The Constant HORA_ENVIO_FIN. */
	private static final String HORA_ENVIO_FIN = "paramHrEnvioFin";
	
	/** The Constant CVE_SPEI_ORD_ABONO. */
	private static final String CVE_SPEI_ORD_ABONO = "paramCveSpeiOrdenanteAbono";
	
	/** The Constant NOMBRE_INST_EMISORA. */
	private static final String NOMBRE_INST_EMISORA = "paramNombreInstEmisora";
	
	/** The Constant CVE_RASTREO. */
	private static final String CVE_RASTREO = "paramCveRastreo";
	
	/** The Constant CTA_BENEFICIARIO. */
	private static final String CTA_BENEFICIARIO = "paramCtaBeneficiario";
	
	/** The Constant MONTO_PAGO. */
	private static final String MONTO_PAGO = "paramMontoPago";
	
	/** The Constant TIPO_PAGO. */
	private static final String TIPO_PAGO = "paramTipoPago";
	
	/** Referencia al servicio de consulta Mov CDA. */
	private BOConsultaMovHistCDASPID boConsulMovHistCDASPID;

	/**
	 * Metodo que se utiliza para mostrar la pagina de Consulta Mov CDA.
	 *
	 * @param req the req
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */ 
	@RequestMapping(value = "/muestraConsMovHistCDASPID.do")
	public ModelAndView muestraConsMovHistCDASPID(HttpServletRequest req, HttpServletRequest res) {
		ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
		Date fecha3Ant = null;
		Date fechaHoy = null;
		
	    fechaHoy = cal.getTime();
	    cal.add(Calendar.MONTH, -3);
	    cal.setLenient(true);
	    fecha3Ant = cal.getTime();
	    String strFecha3Antes = utilerias.formateaFecha(fecha3Ant, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
	    String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);

	    //se envian datos a traves de model and view
	    modelAndView.addObject("fecha3Antes", strFecha3Antes);
	    modelAndView.addObject("fechaFin", strFechaHoy);
	    modelAndView.addObject(FECHA_HOY, strFechaHoy);
		
	    return modelAndView;
	}
	
	/**
	 * Metodo que se utiliza para realizar la consulta
	 * de movimientos Historicos CDA.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovHistCDASPID.do")
	public ModelAndView consMovHistCDASPID(HttpServletRequest req, HttpServletResponse res) {
		
		ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		BeanReqConsMovHistCDA beanReqConsMovHistCDA = requestToBeanReqConsMovHistCDA(req, modelAndView);

	    BeanResConsMovHistCDASPID beanResConsMovHistCDA = null;
	    RequestContext ctx = new RequestContext(req);
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    try {	    	
	    	beanResConsMovHistCDA = boConsulMovHistCDASPID.consultarMovHistCDA(beanReqConsMovHistCDA,getArchitechBean());
	    	String mensaje = ctx.getMessage(COD_ERROR+beanResConsMovHistCDA.getCodError());
	    	modelAndView.addObject(BEAN_RESULTADO_CONSULTA,beanResConsMovHistCDA);
	    	modelAndView.addObject(STR_COD_ERROR, beanResConsMovHistCDA.getCodError());
	     	modelAndView.addObject(STR_TIPO_ERROR, beanResConsMovHistCDA.getTipoError());
	     	modelAndView.addObject(STR_DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
			//se obtiene mensaje y codigo error de excepcion
	    	modelAndView.addObject(STR_COD_ERROR, e.getCode());
	     	modelAndView.addObject(STR_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(STR_DESC_ERROR, e.getMessage());
		}
	    //se envian datos a traves de model and view
		modelAndView.addObject(FECHA_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());			// campo fechaOperacionFin
		modelAndView.addObject(FECHA_OPE_INICIO,beanReqConsMovHistCDA.getFechaOpeInicio());		// campo fechaOperacionInicio
		modelAndView.addObject(HORA_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());			// campo horaEnvioFin
		modelAndView.addObject(HORA_ENVIO_INICIO,beanReqConsMovHistCDA.getHrEnvioIni());		// campo horaEnvioInicio
		modelAndView.addObject(HORA_ABONO_FIN,beanReqConsMovHistCDA.getHrAbonoFin());			// campo horaAbonoFin
		modelAndView.addObject(HORA_ABONO_INICIO,beanReqConsMovHistCDA.getHrAbonoIni());		// campo horaAbonoInicio
		modelAndView.addObject(NOMBRE_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());		// campo InstitucionEmisora
		modelAndView.addObject(CVE_SPEI_ORD_ABONO,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());	// campo claveSpeiOrdAbono
		modelAndView.addObject(CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());				// campo claveRastreo
		modelAndView.addObject(CTA_BENEFICIARIO,beanReqConsMovHistCDA.getCtaBeneficiario());	// campo cuentaBeneficiario
		modelAndView.addObject(TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());					// campo tipoPago
		modelAndView.addObject(MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());				// campo montoPago
		
		//se llama funcion para agregar el campo Operacion contingencia
		agregarOperacionContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);			
	    return modelAndView;
	}

	/**
	 * Metodo utilizado para solicitar la exportacion de
	 * los movimientos historicos CDA obtenidos en la consulta.
	 *
	 * @param req the req
	 * @param res objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expConsMovHistCDASPID.do")
	public ModelAndView expConsMovHistCDASPID(HttpServletRequest req,HttpServletResponse res) {
		
		RequestContext ctx = new RequestContext(req);
		ModelAndView modelAndView = new ModelAndView(new ViewExcel());
        BeanResConsMovHistCDASPID beanResConMovCDA = null;
		BeanReqConsMovHistCDA beanReqConsMovCDA = requestToBeanReqConsMovHistCDA(req, modelAndView);
        beanReqConsMovCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
        
        try {
        	beanResConMovCDA = boConsulMovHistCDASPID.consultarMovHistCDA(beanReqConsMovCDA,getArchitechBean());
      	   if(Errores.OK00000V.equals(beanResConMovCDA.getCodError())||
     			   Errores.ED00011V.equals(beanResConMovCDA.getCodError())){

    	     //generacion de documento de Excel
      	 	 modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
      	 	 modelAndView.addObject("BODY_VALUE", getBody(beanResConMovCDA.getListBeanMovimientoCDA()));
      	 	 modelAndView.addObject("NAME_SHEET", "expConsMovHistCDASPID");
      	   }else{
      		 modelAndView = consMovHistCDASPID(req, res);
      		 String mensaje = ctx.getMessage(COD_ERROR+beanResConMovCDA.getCodError());
      	      modelAndView.addObject(STR_COD_ERROR, beanResConMovCDA.getCodError());
      	      modelAndView.addObject(STR_TIPO_ERROR, beanResConMovCDA.getTipoError());
      	      modelAndView.addObject(STR_DESC_ERROR, mensaje);
      	   }
 	    } catch (BusinessException e) {
 	    	showException(e);
			//se obtiene mensaje y codigo error de excepcion
	    	modelAndView.addObject(STR_COD_ERROR, e.getCode());
	     	modelAndView.addObject(STR_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(STR_DESC_ERROR, e.getMessage());
 	    }
 	    return modelAndView;		
	}
    
    /**
     * Metodo privado que sirve para obtener los encabezados del Excel.
     *
     * @param ctx Objeto del tipo RequestContext
     * @return List<String> Objeto con el listado de String
     */
    private List<String> getHeaderExcel(RequestContext ctx){
    	String []header = new String [] {
    			//se obtienen los encabezados para el documento de excel
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.refere"),				//campo refere
  		 	   ctx.getMessage("moduloCDA.bitacoraAdmon.text.fechaOp"),				//campo fechaOp
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaq"),			//campo folioPaq
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPag"),			//campo folioPag
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.claveSPIDOrdAbono"), 	//campo claveSPIDOrdAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.instEmisora"),			//campo instEmisora
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.cveRastreo"),			//campo cveRastreo
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.ctaBenef"),			//campo ctaBenef
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.monto"),				//campo monto
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.fechaAbono"),			//campo fechaAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrAbono"),				//campo hrAbono
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.hrEnvio"),				//campo hrEnvio
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.estatusCDA"),			//campo estatusCDA
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.codError"),			//campo codError
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoPago"),			//campo tipoPago
  		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioPaqCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.folioCda"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.modalidad"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.numCuentaOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcOrd"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.nombreBcoRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.tipoCuentaRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.rfcRec"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.conceptoPago"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.clasifOperacion"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.algoritmoFirma"),
		 	   ctx.getMessage("moduloCDA.consultaMovCDA.text.padding")
  	 	   };
   	 return Arrays.asList(header);
    }
	
    /**
     * Metodo privado que sirve para obtener los datos para el excel.
     *
     * @param listBeanMovimientoCDA Objeto (List<BeanMovimientoCDA>) con el listado de objetos del tipo BeanMovimientoCDA
     * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
     */
    private List<List<Object>> getBody(List<BeanMovimientoCDASPID> listMovimientosCDASPID){
  	 	List<List<Object>> objListSPID = null;
	 	List<Object> objListParam = null;
	 	if( !listMovimientosCDASPID.isEmpty() && 
	 			listMovimientosCDASPID!= null ){
	 		objListSPID = new ArrayList<List<Object>>();
	 		for(BeanMovimientoCDASPID beanMovimientoCDASPID : listMovimientosCDASPID){
	 			objListParam = new ArrayList<Object>();
	 			//se genera la lista de informacion para generar el excel
	 			objListParam.add(beanMovimientoCDASPID.getReferencia());
	 			objListParam.add(beanMovimientoCDASPID.getFechaOpe());
	 			objListParam.add(beanMovimientoCDASPID.getFolioPaq());
	 			objListParam.add(beanMovimientoCDASPID.getFolioPago());
	 			objListParam.add(beanMovimientoCDASPID.getCveSpei());
	 			objListParam.add(beanMovimientoCDASPID.getNomInstEmisora());
	 			objListParam.add(beanMovimientoCDASPID.getCveRastreo());
	 			objListParam.add(beanMovimientoCDASPID.getCtaBenef());
	 			objListParam.add(beanMovimientoCDASPID.getMonto());
	 			objListParam.add(beanMovimientoCDASPID.getFechaAbono());
	 			objListParam.add(beanMovimientoCDASPID.getHrAbono());
	 			objListParam.add(beanMovimientoCDASPID.getHrEnvio());
	 			objListParam.add(beanMovimientoCDASPID.getEstatusCDA());
	 			objListParam.add(beanMovimientoCDASPID.getCodError());
	 			objListParam.add(beanMovimientoCDASPID.getTipoPago());
	 			objListParam.add(beanMovimientoCDASPID.getFolioPaqCda());
	 			objListParam.add(beanMovimientoCDASPID.getFolioCda());
	 			objListParam.add(beanMovimientoCDASPID.getModalidad());
	 			objListParam.add(beanMovimientoCDASPID.getNombreOrd());
	 			objListParam.add(beanMovimientoCDASPID.getTipoCuentaOrd());
	 			objListParam.add(beanMovimientoCDASPID.getNumCuentaOrd());
	 			objListParam.add(beanMovimientoCDASPID.getRfcOrd());
	 			objListParam.add(beanMovimientoCDASPID.getNombreBcoRec());
	 			objListParam.add(beanMovimientoCDASPID.getNombreRec());
	 			objListParam.add(beanMovimientoCDASPID.getTipoCuentaRec());
	 			objListParam.add(beanMovimientoCDASPID.getRfcRec());
	 			objListParam.add(beanMovimientoCDASPID.getConceptoPago());
	 			objListParam.add(beanMovimientoCDASPID.getClasifOperacion());
	 			objListParam.add(beanMovimientoCDASPID.getNumSerieCertif());
	 			objListParam.add(beanMovimientoCDASPID.getSelloDigital());
	 			objListParam.add(beanMovimientoCDASPID.getAlgoritmoFirma());
	 			objListParam.add(beanMovimientoCDASPID.getPadding());
	 			objListSPID.add(objListParam);
	 		}
 		}
	 	return objListSPID;
	 }
    
	/**
	 * Metodo utilizado para realizar solicitar la exportacion de todos
	 * los movimientos historicos CDA obtenidos en la consulta.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/expTodosConsMovHistCDASPID.do")
	public ModelAndView expTodosConsMovHistCDASPID(HttpServletRequest req, HttpServletRequest res) {
		
		ModelAndView modelAndView = new ModelAndView(PAGINA_SERVICIO);
		BeanReqConsMovHistCDA beanReqConsMovHistCDA = requestToBeanReqConsMovHistCDA(req, modelAndView);
		
	    BeanResConsMovHistCDASPID beanResConsMovHistCDA = null;
	    StringBuilder  builder = new StringBuilder();
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    RequestContext ctx = new RequestContext(req);
	    
	    try {
	    	beanResConsMovHistCDA = boConsulMovHistCDASPID.consultaMovHistCDAExpTodos(beanReqConsMovHistCDA,getArchitechBean());
	    	modelAndView.addObject(BEAN_RESULTADO_CONSULTA,beanResConsMovHistCDA);
	    	modelAndView.addObject(STR_COD_ERROR, beanResConsMovHistCDA.getCodError());
	     	modelAndView.addObject(STR_TIPO_ERROR, beanResConsMovHistCDA.getTipoError());
	     	String mensaje = ctx.getMessage(COD_ERROR+beanResConsMovHistCDA.getCodError());
	     	builder.append(mensaje);
	     	builder.append(beanResConsMovHistCDA.getNombreArchivo());
	     	mensaje = builder.toString();
	     	modelAndView.addObject(STR_DESC_ERROR, mensaje);
		} catch (BusinessException e) {
			showException(e);
			//se obtiene mensaje y codigo error de excepcion
	    	modelAndView.addObject(STR_COD_ERROR, e.getCode());
	     	modelAndView.addObject(STR_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(STR_DESC_ERROR, e.getMessage());
		}
	    //se envian datos a traves de model and view
		modelAndView.addObject(FECHA_OPE_INICIO,beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject(FECHA_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject(HORA_ENVIO_INICIO,beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject(HORA_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject(HORA_ABONO_INICIO,beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject(HORA_ABONO_FIN,beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject(NOMBRE_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject(CVE_SPEI_ORD_ABONO,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject(CTA_BENEFICIARIO,beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject(CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject(MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject(TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());

		//se llama funcion para agregar el campo Operacion contingencia
		agregarOperacionContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);		
		
		return modelAndView;			
	}

	/**
	 * Metodo que consulta detalle de un Movimiento Historico CDA Seleccionado.
	 *
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res the res
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/consMovHistDetCDASPID.do")
	public ModelAndView consMovHistDetCDASPID(HttpServletRequest req, HttpServletResponse res) {
		
		BeanReqConsMovCDADet beanReqConsMovCDADet = new BeanReqConsMovCDADet();
		try {
			BeanUtils.populate(beanReqConsMovCDADet, req.getParameterMap());
		} catch (IllegalAccessException e) {
			showException(e);
		} catch (InvocationTargetException e) {
			showException(e);
		}

		ModelAndView modelAndView = new ModelAndView("consultaDetHistMovCDASPID");
		
		BeanReqConsMovHistCDA beanReqConsMovHistCDA = requestToBeanReqConsMovHistCDA(req, modelAndView);
	    BeanResConsMovDetHistCDA beanResConsMovDetHistCDA = null;
	    beanReqConsMovHistCDA.setOpeContingencia(Boolean.parseBoolean(req.getParameter(OPE_CONTINGENCIA)));
	    
	    try {
	    	beanResConsMovDetHistCDA = boConsulMovHistCDASPID.consMovHistDetCDA(beanReqConsMovCDADet,getArchitechBean());
		} catch (BusinessException e) {
			showException(e);
			//se obtiene mensaje y codigo error de excepcion
	    	modelAndView.addObject(STR_COD_ERROR, e.getCode());
	     	modelAndView.addObject(STR_TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
	     	modelAndView.addObject(STR_DESC_ERROR, e.getMessage());
		}
		
		modelAndView.addObject("beanResConsMovDetHistCDA",beanResConsMovDetHistCDA);
		modelAndView.addObject(HORA_ABONO_INICIO,beanReqConsMovHistCDA.getHrAbonoIni());
		modelAndView.addObject(HORA_ABONO_FIN,beanReqConsMovHistCDA.getHrAbonoFin());
		modelAndView.addObject(HORA_ENVIO_INICIO,beanReqConsMovHistCDA.getHrEnvioIni());
		modelAndView.addObject(HORA_ENVIO_FIN,beanReqConsMovHistCDA.getHrEnvioFin());
		modelAndView.addObject(FECHA_OPE_INICIO,beanReqConsMovHistCDA.getFechaOpeInicio());
		modelAndView.addObject(FECHA_OPE_FIN,beanReqConsMovHistCDA.getFechaOpeFin());
		modelAndView.addObject(NOMBRE_INST_EMISORA,beanReqConsMovHistCDA.getNombreInstEmisora());
		modelAndView.addObject(CVE_SPEI_ORD_ABONO,beanReqConsMovHistCDA.getCveSpeiOrdenanteAbono());
		modelAndView.addObject(CVE_RASTREO,beanReqConsMovHistCDA.getCveRastreo());
		modelAndView.addObject(CTA_BENEFICIARIO,beanReqConsMovHistCDA.getCtaBeneficiario());
		modelAndView.addObject(MONTO_PAGO,beanReqConsMovHistCDA.getMontoPago());
		modelAndView.addObject(TIPO_PAGO,beanReqConsMovHistCDA.getTipoPago());
		modelAndView.addObject("beanPaginador",beanReqConsMovHistCDA.getPaginador());

		//se llama funcion para agregar el campo Operacion contingencia
		agregarOperacionContingencia(modelAndView, beanReqConsMovHistCDA);
		Utilerias utilerias = Utilerias.getUtilerias();
		Calendar cal = Calendar.getInstance();
	    Date  fechaHoy = cal.getTime();
		String strFechaHoy = utilerias.formateaFecha(fechaHoy, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		modelAndView.addObject(FECHA_HOY, strFechaHoy);	
		return modelAndView;
	}

	/**
	 * Agregar operacion contingencia.
	 *
	 * @param modelAndView the model and view
	 * @param beanReqConsMovHistCDA the bean req cons mov hist cda
	 */
	private void agregarOperacionContingencia(ModelAndView modelAndView, BeanReqConsMovHistCDA beanReqConsMovHistCDA){
		boolean opeContigencia = false;
		if(beanReqConsMovHistCDA.getOpeContingencia()){
			//se obtiene valor de operacionContingencia
			opeContigencia = beanReqConsMovHistCDA.getOpeContingencia();
		}
		modelAndView.addObject(OPE_CONTINGENCIA, opeContigencia);
	}

	/**
	 * Metodo set del servicio de Consultar Movimientos Historicos CDA.
	 *
	 * @param boConsulMovHistCDASPID objeto del tipo BOConsultaMovHistCDA
	 */
	public void setBoConsulMovHistCDASPID(BOConsultaMovHistCDASPID boConsulMovHistCDASPID) {
		this.boConsulMovHistCDASPID = boConsulMovHistCDASPID;
	}
	
	/**
	 * Request to bean.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req cons operaciones
	 */
	public BeanReqConsMovHistCDA requestToBeanReqConsMovHistCDA(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqConsMovHistCDA beanReqConsMovHistCDA = new BeanReqConsMovHistCDA();
		UtilsMapeaRequest.getMapper().mapearObject(beanReqConsMovHistCDA, req.getParameterMap());
		return beanReqConsMovHistCDA;
	}
}