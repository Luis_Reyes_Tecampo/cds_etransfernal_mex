/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonArchCanalesConting.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.contingencia;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContig;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOMonCanCargaArchConting;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad del Monitor de carga de archivos
**/
@Controller
public class ControllerOperacionesContingentes extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;

	/**
	 * Propiedad del tipo String que almacena el valor de PAGINA_MONITOR
	 */
	private static final String PAGINA_MONITOR = "monitorCargaArchCanContig";
	/**
	 * Referencia al servicio del Monitor de carga de Archivos
	 */
	private BOMonCanCargaArchConting bOMonCanCargaArchConting = null;
	
   /**Metodo que muestra la funcionalidad del monitor carga archivos
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraMonCargaArchCanContig.do")
   public ModelAndView muestraMonCargaArchCanContig(){
      return new ModelAndView(PAGINA_MONITOR);
   }
   /**Metodo que muestra la funcionalidad del monitor carga archivos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/consultaOperContingentes.do")
   public ModelAndView consultarMonCargaArchCanContig(BeanPaginador beanPaginador,HttpServletRequest req){
      ModelAndView modelAndView = null;
      BeanResMonCargaArchCanContig beanResMonCargaArchCanContig = null;
      RequestContext ctx = new RequestContext(req);
      beanResMonCargaArchCanContig = bOMonCanCargaArchConting.consultarMonCanCargaArchConting(beanPaginador, getArchitechBean());
    	  modelAndView = new ModelAndView(PAGINA_MONITOR,"resMon",beanResMonCargaArchCanContig);
     	  String mensaje = ctx.getMessage("codError."+beanResMonCargaArchCanContig.getCodError());
     	  modelAndView.addObject("codError", beanResMonCargaArchCanContig.getCodError());
     	  modelAndView.addObject("tipoError", beanResMonCargaArchCanContig.getTipoError());
     	  modelAndView.addObject("descError", mensaje);      
      return modelAndView;
   }
	/**
	 * Metodo get que obtiene el valor de la propiedad bOMonCanCargaArchConting
	 * @return BOMonCanCargaArchConting Objeto de tipo @see BOMonCanCargaArchConting
	 */
	public BOMonCanCargaArchConting getBOMonCanCargaArchConting() {
		return bOMonCanCargaArchConting;
	}
	/**
	 * Metodo que modifica el valor de la propiedad bOMonCanCargaArchConting
	 * @param monCanCargaArchConting Objeto de tipo @see BOMonCanCargaArchConting
	 */
	public void setBOMonCanCargaArchConting(
			BOMonCanCargaArchConting monCanCargaArchConting) {
		bOMonCanCargaArchConting = monCanCargaArchConting;
	}
   
  

}
