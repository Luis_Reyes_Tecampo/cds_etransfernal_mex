/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerCatRFC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:48:36 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;
import mx.isban.eTransferNal.beans.catalogos.BeanResRFC;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BORFC;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarRFC;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsGeneral;

/**
 * Class ControllerCatRFC.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * de excepcion de RFC.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Controller
public class ControllerCatRFC extends Architech implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1196410624520949254L;

	/** La variable que contiene informacion con respecto a --> bo RFC. */
	private BORFC boRFC;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaExcepcionRFC";
	
	/** La constante BEAN_RFC. */
	private static final String BEAN_RFC = "beanRFC";
	
	/** La constante BEAN_RES_RFC. */
	private static final String BEAN_RES_RFC = "beanResRFC";

	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaRFC";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante utilerias. */
	private static final UtilsExportarRFC utilerias = UtilsExportarRFC.getInstancia();
	
	/** La constante utilsGeneral. */
	private static final UtilsGeneral utilsGeneral = UtilsGeneral.getUtils();
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/consultaExcepcionRFC.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanRFC beanFilter, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanResRFC response = null;
		try {
			String accion = req.getParameter(ACCION);
			/** Validacion del parametro accion **/
			if (accion != null && !accion.equals(StringUtils.EMPTY) && utilsGeneral.validaFiltro(accion)) {
				/** Se valida los filtros guardados**/
				beanFilter.setRfc(req.getParameter("RFCF"));
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			/** Ejecuta peticion al BO **/
			response = boRFC.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
				
			}
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, response);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Agregar RFC.
	 * 
	 * Mapeo del metodo para iniciar el flujo de agregar 
	 * un nuevo registro al catalogo.
	 *
	 * @param beanRFC --> objeto de tipo beanRFC enviado para realizar el alta a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/agregarRFC.do")
	public ModelAndView agregarRFC(@ModelAttribute(BEAN_RFC) BeanRFC beanRFC, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		/** Comienza flujo de alta **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanResRFC rfcRes = null;
		BeanResBase response = null;
		try {
			response = boRFC.agregar(beanRFC, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				rfcRes = boRFC.consultar(new BeanRFC(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_RFC, new BeanRFC());
				model.addObject(BEAN_RES_RFC, rfcRes);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(BEAN_RFC, beanRFC);
				model.addObject(ACCION, ACCION_ALTA);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION, ACCION_ALTA);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Editar RFC.
	 * 
	 * Mapeo del metodo para iniciar el flujo de 
	 * modificar un registro del catalogo.
	 *
	 * @param beanRfc --> objeto de tipo beanRFC enviado para realizar el update a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/editarRFC.do")
	public ModelAndView editarRFC(@ModelAttribute(BEAN_RFC) BeanRFC beanRfc, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de edicion  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanResRFC rfcRes = null;
		String valorOld = null;
		try {
			valorOld = req.getParameter("valorOld");
			final BeanRFC datoAnterior = new BeanRFC();
			datoAnterior.setRfc(valorOld);
			BeanResBase response = boRFC.editar(beanRfc, datoAnterior, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				rfcRes = boRFC.consultar(new BeanRFC(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_RES_RFC, rfcRes);
				model.addObject(BEAN_RFC, new BeanRFC());
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				beanRfc.setRfc(valorOld);
				model.addObject(BEAN_RFC, beanRfc);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = new ModelAndView(PAGINA_ALTA);
			model.addObject(ACCION,ACCION_MODIFICAR);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Mantenimiento RFC.
	 * 
	 * Mapeo del flujo para mostrar el fomurlario de 
	 * matenimiento de registros del catalogo.
	 *
	 * @param beanResRFC --> objeto de tipo beanRFC
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/mantenimientoRFC.do")
	public ModelAndView mantenimientoRFC(@ModelAttribute(BEAN_RES_RFC) BeanResRFC beanResRFC,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanRFC beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Creacion de la vista de mantenimiento  **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView model = null;
		BeanRFC beanRFC = new BeanRFC();
		if (beanResRFC != null && beanResRFC.getRfcs() != null) {
			for(BeanRFC rfc : beanResRFC.getRfcs()) {
				if(rfc.isSeleccionado()) {
					beanRFC = rfc;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		String banderaAlta = req.getParameter(ACCION);
		if (beanRFC.getRfc() != null &&  !banderaAlta.equals(ACCION_ALTA)) {
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		model.addObject(BEAN_RFC, beanRFC);
		model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		model.addObject(UtilsConstantes.BEAN_PAGINADOR, beanPaginador);
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	/**
	 * Eliminar RFC.
	 *
	 * @param beanResRFC --> objeto de tipo beanRFC enviado para realizar el delete a la base de datos  
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/eliminarRFC.do")
    public ModelAndView eliminarRFC(@ModelAttribute(BEAN_RES_RFC) BeanResRFC beanResRFC, BindingResult bindingResult){
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de eliminacion **/
		
		/** Declaracion del modelo a retornar **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			response = boRFC.eliminar(beanResRFC, getArchitechBean());
			BeanResRFC responseConsulta = boRFC.consultar(new BeanRFC(), new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
					
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
		} catch (BusinessException e) {
			showException(e);
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, null);
			modelAndView = utilsGeneral.setError(modelAndView);
		}
		/** Retorno del modelo a la vista **/
		return modelAndView;
    }
	
	/**
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 * @param beanResRFC --> objeto de tipo RFC enviado para realizar insertar los RFC en el excel
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/exportarRFC.do")
	public ModelAndView exportarRFC(@ModelAttribute(BEAN_RES_RFC) BeanResRFC beanResRFC, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBody(beanResRFC.getRfcs()));
		modelAndView.addObject("NAME_SHEET", "Excepcion de RFC");
		/** Retorno del modelo a la vista **/
		return modelAndView;
	}
	
	/**
	 * Mapeo del metodo para iniciar el flujo de exportar los RFC
	 * al proceso de inserccion en BD.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanRFC --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value="/exportarTodosRFC.do")
	public ModelAndView exportarTodosRFC(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanRFC beanRFC, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanResRFC responseConsulta = new BeanResRFC();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boRFC.consultar(beanRFC, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boRFC.exportarTodo(beanRFC, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_RES_RFC, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
			model = utilsGeneral.setError(model);
		}
		/** Retorno del modelo a la vista **/
		return model;
	}
	
	
	/**
	 *  Metodo Get para obtener el RFC
	 *
	 * @return boRFC --> variable de tipo  clase que regresa el RFC
	 */
	public BORFC getBoRFC() {
		return boRFC;
	}

	/**
	 * Metodo Set para obtener el RFC
	 *
	 * @param boRFC --> variable de tipo  clase que asigna el RFC
	 */
	public void setBoRFC(BORFC boRFC) {
		this.boRFC = boRFC;
	}
	
}
