/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerContingMismoDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:02:04 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDia;
import mx.isban.eTransferNal.servicio.moduloCDA.BOContingMismoDia;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Contingencia CDA Mismo
 * Dia
**/
@Controller
public class ControllerContingMismoDia extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	/**
	 * Referencia al servicio de contingencia mismo dia
	 */
	private BOContingMismoDia bOContingMismoDia;

   /**Metodo que se utiliza para mostrar la pagina de Contingencia
   * CDA Mismo Dia
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/muestraContingCDAMismoDia.do")
   public ModelAndView muestraContingCDAMismoDia(HttpServletRequest req){
       ModelAndView modelAndView = null;      
       BeanResContingMismoDia beanResContingMismoDia = null;
       RequestContext ctx = new RequestContext(req);
       try {
     	  beanResContingMismoDia = bOContingMismoDia.consultaPendientesMismoDia(getArchitechBean());
     	  modelAndView = new ModelAndView("contingenciaCDAMismoDia","beanResContingMismoDia",beanResContingMismoDia);
     	  String mensaje = ctx.getMessage("codError."+beanResContingMismoDia.getCodError());
     	  modelAndView.addObject("codError", beanResContingMismoDia.getCodError());
     	  modelAndView.addObject("tipoError", beanResContingMismoDia.getTipoError());
     	  modelAndView.addObject("descError", mensaje);
 	  } catch (BusinessException e) {
 		showException(e);
 	  }
 	  
       return modelAndView;
   }
   
   /**Metodo que se utiliza para mostrar la pagina de Contingencia
    * CDA Mismo Dia
    * @param beanPaginador Objeto del tipo @see BeanPaginador
    * que encapsula los parametros de la Contingencia Mismo Dia
    * @param req Objeto del tipo HttpServletRequests
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "/consultarContingCDAMismoDia.do")
    public ModelAndView consultarContingCDAMismoDia(BeanPaginador beanPaginador,HttpServletRequest req){
       ModelAndView modelAndView = null;      
       BeanResContingMismoDia beanResContingMismoDia = null;
       RequestContext ctx = new RequestContext(req);
       try {
     	  beanResContingMismoDia = bOContingMismoDia.consultaContingMismoDia(beanPaginador, getArchitechBean());
     	  modelAndView = new ModelAndView("contingenciaCDAMismoDia","beanResContingMismoDia",beanResContingMismoDia);
     	  String mensaje = ctx.getMessage("codError."+beanResContingMismoDia.getCodError());
     	  modelAndView.addObject("codError", beanResContingMismoDia.getCodError());
     	  modelAndView.addObject("tipoError", beanResContingMismoDia.getTipoError());
     	  modelAndView.addObject("descError", mensaje);
 	  } catch (BusinessException e) {
 		showException(e);
 	  }
 	  
       return modelAndView;
    }
    
    /**Metodo que se utiliza para generar el archivo de Contingencia
     * CDA Mismo Dia
     * @param req Objeto del tipo HttpServletRequests
     * @return ModelAndView Objeto del tipo ModelAndView
     */
     @RequestMapping(value = "/genArchContigMismoDia.do")
     public ModelAndView genArchContigMismoDia(HttpServletRequest req){
        ModelAndView modelAndView = null;
        BeanResGenArchContingMismoDia beanResGenArchContingMismoDia = null;
        RequestContext ctx = new RequestContext(req);
        try {
        	beanResGenArchContingMismoDia = bOContingMismoDia.genArchContigMismoDia(getArchitechBean());
        	modelAndView = new ModelAndView("contingenciaCDAMismoDia","beanResContingMismoDia",beanResGenArchContingMismoDia);
        	
    		String mensaje = ctx.getMessage("codError."+beanResGenArchContingMismoDia.getCodError());
        	modelAndView.addObject("codError", beanResGenArchContingMismoDia.getCodError());
        	modelAndView.addObject("tipoError", beanResGenArchContingMismoDia.getTipoError());
        	modelAndView.addObject("descError", mensaje);
    	
		} catch (BusinessException e) {
			showException(e);
		}
        return modelAndView;
     }
   /**
    * Metodo get del servicio de contingecia mismo dia
    * @return BOContingMismoDia Objeto del Servicio de contingencia mismo dia
    */
	public BOContingMismoDia getBOContingMismoDia() {
		return bOContingMismoDia;
	}
	/**
    * Metodo set del servicio de contingecia mismo dia
    * @param contingMismoDia Se setea objeto del Servicio de contingencia mismo dia
    */
	public void setBOContingMismoDia(BOContingMismoDia contingMismoDia) {
		bOContingMismoDia = contingMismoDia;
	}



}
