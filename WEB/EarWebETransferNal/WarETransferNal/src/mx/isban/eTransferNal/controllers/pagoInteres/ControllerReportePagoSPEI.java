/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerReportePagoSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.pagoInteres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePago;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.pagoInteres.BOReportePagoSPEI;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.comunes.Validate;
import mx.isban.eTransferNal.utilerias.comunes.ValidateRango;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion de la pantalla de grafico SPEI
**/
@Controller
public class ControllerReportePagoSPEI extends Architech{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2272477377374757239L;

	/**
	 * Propiedad del tipo String[] que almacena el valor de HORAS
	 */
	private static final String HORAS[] = new String[]{"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"};
	/**
	 * Propiedad del tipo String[] que almacena el valor de MINUTOS
	 */
	private static final String MINUTOS[] = new String[]{"00","01","02","03","04","05","06","07","08","09",
			 						  "10","11","12","13","14","15","16","17","18","19",
			 						  "20","21","22","23","24","25","26","27","28","29",
			 						 "30","31","32","33","34","35","36","37","38","39",
			 						  "40","41","42","43","44","45","46","47","48","49",
			 						  "50","51","52","53","54","55","56","57","58","59"};
	 /**
	 * Propiedad del tipo String que almacena el valor de STR_HORAS
	 */
	private static final String STR_HORAS="horas";
	 /**
	 * Propiedad del tipo String que almacena el valor de STR_MINUTOS
	 */
	private static final String STR_MINUTOS="minutos";
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE = "reportePagoSPEI";
	

	/**
	 * Propiedad del tipo String que almacena el valor de BEAN
	 */
	private static final String BEAN = "resBean";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REQ_BEAN
	 */
	private static final String REQ_BEAN = "reqBean";
	
	/**
	 * Propiedad del tipo String que almacena el valor de LEYENDA_HORA
	 */
	private static final String LEYENDA_HORA = "leyendaHora";
	
	/**
	 * Propiedad del tipo String que almacena el valor de LEYENDA_DIA
	 */
	private static final String LEYENDA_DIA = "leyendaDia";
	
	/**
	 * Propiedad del tipo String que almacena el valor de HORA_RANGO
	 */
	private static final String HORA_RANGO = "horaRango";
	/**
	 * Propiedad del tipo String que almacena el valor de DIA_RANGO
	 */
	private static final String DIA_RANGO = "diaRango";
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BOReportePagoSPEI que almacena el valor de bOReportePagoSPEI
	 */
	private BOReportePagoSPEI bOReportePagoSPEI;  
	
	 /**
	  * Metodo que se utiliza para mostrar la pagina de monitor de operaciones SPEI
	  * @param req Objeto del tipo HttpServletRequest
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraReportePagoSPEI.do")
	 public ModelAndView muestraReportePagoSPEI(HttpServletRequest req) {
		 RequestContext ctx = new RequestContext(req);
		 BeanReqReportePago beanReqReportePago = new BeanReqReportePago();
		 beanReqReportePago.setOpcion("A");
		 BeanResReportePago beanResReportePago = null;
		 Map<String,Object> map = new HashMap<String,Object>();
		 ModelAndView modelAndView = new ModelAndView(PAGE);
		 beanResReportePago = bOReportePagoSPEI.consultaCatalogos(getArchitechBean());
		 int rangoHoras =  beanResReportePago.getListRangoTiempo().get(0).getRango();
		 int rangoDias =  beanResReportePago.getListRangoTiempo().get(1).getRango();
		 
		 map.put(LEYENDA_HORA, ctx.getMessage("pagoInt.pagoIntSPEI.text.rangoHoras", new Object[]{rangoHoras}));
		 map.put(LEYENDA_DIA, "");
		 map.put(HORA_RANGO, rangoHoras);
		 map.put(DIA_RANGO, rangoDias);
		 
		 map.put(BEAN, beanResReportePago);
		 map.put(REQ_BEAN, beanReqReportePago);
		 map.put(STR_HORAS, HORAS);
		 map.put(STR_MINUTOS, MINUTOS);
		 modelAndView.addAllObjects(map);
		 return modelAndView;
	 }
	 
	 /**
	  * Metodo que se utiliza para buscar los registros
	  * @param req Objeto del tipo HttpServletRequest
	  * @param beanReqReportePago Bean del tipo BeanReqReportePago
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/consultaReportePagoSPEI.do")
	 public ModelAndView consultaReportePagoSPEI(HttpServletRequest req,BeanReqReportePago beanReqReportePago) {
		 BeanResReportePago beanResReportePago = null;
		 RequestContext ctx = new RequestContext(req);
		 Map<String,Object> map = new HashMap<String,Object>();
		 ModelAndView modelAndView = new ModelAndView(PAGE);
		 Map<String, String> model = valida(beanReqReportePago);
		 map.put(STR_HORAS, HORAS);
		 map.put(STR_MINUTOS, MINUTOS);
		 int rangoHoras =  beanReqReportePago.getHoraRango();
		 int rangoDias =  beanReqReportePago.getDiaRango();
		 map.put(HORA_RANGO, rangoHoras);
		 map.put(DIA_RANGO, rangoDias);
		 modelAndView.addObject(LEYENDA_HORA, ctx.getMessage("pagoInt.pagoIntSPEI.text.rangoHoras", new Object[]{rangoHoras}));
		 modelAndView.addObject(LEYENDA_DIA, "");
		if(model.containsKey(Constantes.COD_ERROR)){
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
			modelAndView.addAllObjects(model);
			 beanResReportePago = bOReportePagoSPEI.consultaCatalogos(getArchitechBean());
			 map.put(BEAN, beanResReportePago);
			 map.put(REQ_BEAN, beanReqReportePago);
			 modelAndView.addAllObjects(map);
			 return modelAndView;
		}else{
			 beanResReportePago = bOReportePagoSPEI.consultaReportePago(beanReqReportePago, getArchitechBean());
			 map.put(BEAN, beanResReportePago);
			 map.put(REQ_BEAN, beanReqReportePago);
			 modelAndView.addAllObjects(map);
		}
     	 String mensaje = ctx.getMessage("codError."+beanResReportePago.getCodError(),new Object[]{beanResReportePago.getNomArchivo()});
   	     modelAndView.addObject("codError", beanResReportePago.getCodError());
   	     modelAndView.addObject("tipoError", beanResReportePago.getTipoError());
   	     modelAndView.addObject("descError", mensaje);
   	     return modelAndView;
	 }
	 
		/**
		 * Metodo encargado de realizar las validaciones
		 * @param beanReqReportePago Bean con los parametros a validar
		 * @return Map del tipo Map<String, Object> con el resultado
		 */
		private Map<String, String> valida(BeanReqReportePago beanReqReportePago){
			Validate validate =  new Validate();
			ValidateRango validateRango =  new ValidateRango();
			 Map<String, String> model = new HashMap<String, String>();
			 if(beanReqReportePago.getTipoPago()== null || "".equals(beanReqReportePago.getTipoPago())){
					model.put(Constantes.COD_ERROR, "ED00114V");
					model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00114V", null, null));
					return model;
			 }
			 if(beanReqReportePago.getSigDifMinutos()!=null && !"".equals(beanReqReportePago.getSigDifMinutos()) &&
					 !validate.esValidoNumero(beanReqReportePago.getDifMinutos(), Validate.REG_NUMERICO_MAX_12)){
						model.put(Constantes.COD_ERROR, "ED00109V");
						model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00109V", null, null));
						return model;
			 }
			 if(validate.esValidoNumero(beanReqReportePago.getDifMinutos(), Validate.REG_NUMERICO_MAX_12 ) && 
					 (beanReqReportePago.getSigDifMinutos()==null || "".equals(beanReqReportePago.getSigDifMinutos()))){
						model.put(Constantes.COD_ERROR, "ED00110V");
						model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00110V", null, null));
						return model;
			 }
			 if(!validateRango.esValidaRangoImporteVacio(beanReqReportePago.getImporteInicio(),beanReqReportePago.getImporteFin())){
					model.put(Constantes.COD_ERROR, "ED00113V");
					model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00113V", new String[]{"Importe"}, null));
					return model;
				}
			 if("A".equals(beanReqReportePago.getOpcion())){
				 StringBuilder strBuilder = new StringBuilder();
				 StringBuilder strBuilder2 = new StringBuilder();
				 strBuilder.append(beanReqReportePago.getHorario().getHoraInicio()).append(":").append(beanReqReportePago.getHorario().getMinutoInicio());
				 strBuilder2.append(beanReqReportePago.getHorario().getHoraFin()).append(":").append(beanReqReportePago.getHorario().getMinutoFin());
				 double hrIni = Double.valueOf(strBuilder.toString().replaceAll(":", "."));
				 double hrFin = Double.valueOf(strBuilder2.toString().replaceAll(":", "."));
				 if(!validateRango.esValidaPeriodoTiempoHHMMVacio(strBuilder.toString(),strBuilder2.toString())){
						model.put(Constantes.COD_ERROR, "ED00111V");
						model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00111V", null, null));
						return model;
				}else if((hrFin-hrIni)>beanReqReportePago.getHoraRango()){
					model.put(Constantes.COD_ERROR, "ED00111V");
					model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00111V", null, null));
					return model;
				}
			 }else{
				 model = validaRango(beanReqReportePago);
			 }
			return model;
		}
		
		/**
		 * Metodo encargado de realizar las validaciones
		 * @param beanReqReportePago Bean con los parametros a validar
		 * @return Map del tipo Map<String, Object> con el resultado
		 */
		private Map<String, String> validaRango(BeanReqReportePago beanReqReportePago){
			ValidateRango validateRango =  new ValidateRango();
			Map<String, String> model = new HashMap<String, String>();
			if(beanReqReportePago.getHorario().getFechaInicio()== null || "".equals(beanReqReportePago.getHorario().getFechaInicio())){
				 model.put(Constantes.COD_ERROR,"ED00105V");
				 model.put(Constantes.DESC_ERROR,messageSource.getMessage("codError.ED00105V", null, null));
				 return model;
			 }
			 Utilerias util = Utilerias.getUtilerias();
			 Calendar cal = Calendar.getInstance(new Locale("es","MX"));
			 String fechaHoy = util.formateaFecha(cal.getTime(), Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
			 if(!validateRango.esValidaPeriodoTiempoDDMMYYYYVacio(beanReqReportePago.getHorario().getFechaInicio(), fechaHoy, -1)){
					model.put(Constantes.COD_ERROR, "ED00119V");
					model.put(Constantes.DESC_ERROR, messageSource.getMessage(Constantes.COD_ERROR_PUNTO+"ED00119V", null, null));
					return model;
			 }
			return model;
		}
		
		/**
		  * Metodo que se utiliza para buscar los registros
		  * @param req Objeto del tipo HttpServletRequest
	      * @param beanReqReportePago Bean del tipo BeanReqReportePago
		  * @return ModelAndView Objeto del tipo ModelAndView
		  */
		 @RequestMapping(value = "/exportarTodoReportePagoSPEI.do")
		 public ModelAndView exportaTodo(HttpServletRequest req, BeanReqReportePago beanReqReportePago){
			 RequestContext ctx = new RequestContext(req);
			 ModelAndView modelAndView = null; 
	    	 BeanResReportePago beanResReportePago = null;

	    	 beanResReportePago = bOReportePagoSPEI.exportarTodoReportePago(beanReqReportePago, getArchitechBean());
	      	 modelAndView = new ModelAndView(PAGE);
	      	 String mensaje = ctx.getMessage("codError."+beanResReportePago.getCodError(),new Object[]{beanResReportePago.getNomArchivo()});
	   	     modelAndView.addObject("codError", beanResReportePago.getCodError());
	   	     modelAndView.addObject("tipoError", beanResReportePago.getTipoError());
	   	     modelAndView.addObject("descError", mensaje);

	   	     int rangoHoras =  beanResReportePago.getListRangoTiempo().get(0).getRango();
			 int rangoDias =  beanResReportePago.getListRangoTiempo().get(1).getRango();
			 modelAndView.addObject(HORA_RANGO, rangoHoras);
			 modelAndView.addObject(DIA_RANGO, rangoDias);
			 modelAndView.addObject(LEYENDA_HORA, ctx.getMessage("pagoInt.pagoIntSPID.text.rangoHoras", new Object[]{rangoHoras}));
			 modelAndView.addObject(LEYENDA_DIA, "");
	   	     modelAndView.addObject(BEAN, beanResReportePago);
	   	  	 modelAndView.addObject(REQ_BEAN, beanReqReportePago);
	   	  	 modelAndView.addObject(STR_HORAS, HORAS);
	   	  	 modelAndView.addObject(STR_MINUTOS, MINUTOS);

	         return modelAndView;			
		 }	
	 /**
	  * Metodo para realizar el export de la informacion
	 * @param req Objeto del tipo HttpServletRequest
	 * @param beanReqReportePago Bean con los parametros de entrada
	 * @return ModelAndView objeto de Spring MVC
	 */
	 @RequestMapping(value = "/exportarReportePagoSPEI.do")
	 public ModelAndView exporta(HttpServletRequest req, BeanReqReportePago beanReqReportePago){
		 RequestContext ctx = new RequestContext(req);
	    	FormatCell formatCellHeader = null;
	    	FormatCell formatCellBody = null;
			ModelAndView modelAndView = null;
	        

       		 formatCellHeader = new FormatCell();
       	     formatCellHeader.setFontName("Calibri");
       	     formatCellHeader.setFontSize((short)11);
       	     formatCellHeader.setBold(true);
       	     
       	     formatCellBody = new FormatCell();
       	     formatCellBody.setFontName("Calibri");
       	     formatCellBody.setFontSize((short)11);
       	     formatCellBody.setBold(false);
       	 	   
       	 	 modelAndView = new ModelAndView(new ViewExcel());
       	 	 modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
  	 	     modelAndView.addObject("FORMAT_CELL", formatCellBody);
       	 	 modelAndView.addObject("HEADER_VALUE", getHeaderExcel(ctx));
       	 	 modelAndView.addObject("BODY_VALUE", getBody(beanReqReportePago.getBeanReportePagoList()));
       	 	 modelAndView.addObject("NAME_SHEET", "expReportePagoSPEI");
	       	   

	 	    return modelAndView;		
	 }

/**
 * Metodo privado que sirve para obtener los datos para el excel
 * @param listBeanReportePago Objeto (List<BeanReportePago>) con el listado de objetos del tipo BeanReportePago
 * @return List<List<Object>> Objeto lista de lista de objetos con los datos del excel
 */
private List<List<Object>> getBody(List<BeanReportePago> listBeanReportePago){
 	List<Object> objListParam = null;
	 	List<List<Object>> objList = null;
	  if(listBeanReportePago!= null && 
			   !listBeanReportePago.isEmpty()){
	 		   objList = new ArrayList<List<Object>>();
	   for(BeanReportePago beanReportePago : listBeanReportePago){ 
		  objListParam = new ArrayList<Object>();
		  objListParam.add(beanReportePago.getRefPago());
		  objListParam.add(beanReportePago.getRefInteres());
		  objListParam.add(beanReportePago.getCveRastreo());
		  objListParam.add(beanReportePago.getTipoPago());
		  objListParam.add(beanReportePago.getMedioEnt());
		  objListParam.add(beanReportePago.getImporteTransf());
		  objListParam.add(beanReportePago.getImporteInteres());
		  objListParam.add(beanReportePago.getFechaInicio());
		  objListParam.add(beanReportePago.getFechaFin());
		  objListParam.add(beanReportePago.getDiferencia());
		  objListParam.add(beanReportePago.getEstatus());
		  objList.add(objListParam);
	   }
	  }
	return objList;
}


	/**
	 * Metodo privado que sirve para obtener los encabezados del Excel
	 * @param ctx Objeto del tipo RequestContext
	 * @return List<String> Objeto con el listado de String
	 */
	private List<String> getHeaderExcel(RequestContext ctx){
		String []header = new String [] {
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.refPAgo"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.refInteres"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.cveRastreo"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.tipoPago"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.medioEntrega"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.impteTransf"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.impteInteres"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.fchInicio"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.fchFin"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.diferencia"),
			 	   ctx.getMessage("pagoInt.pagoIntSPEI.estatus"),

		 	   };
		 return Arrays.asList(header);
		 
	}
	 
	 
	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOReportePagoSPEI
	 * @return bOReportePagoSPEI Objeto del tipo BOReportePagoSPEI
	 */
	public BOReportePagoSPEI getBOReportePagoSPEI() {
		return bOReportePagoSPEI;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOReportePagoSPEI
	 * @param reportePagoSPEI del tipo BOReportePagoSPEI
	 */
	public void setBOReportePagoSPEI(BOReportePagoSPEI reportePagoSPEI) {
		bOReportePagoSPEI = reportePagoSPEI;
	}

}
