/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMttoMediosAutorizados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 10:42:00 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 * 1.1   27/09/2019 10:00:00 AM Juan Jesus Beltran. 		 VectorFSW Modificacion
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosAutorizados;

/**
 * Class ControllerMttoMediosAutorizados.
 * Clase que utiliza la pantalla del catalogo de Mantenimiento de Medios
 * @author FSW-Vector
 * @since 12/09/2018
 */
@Controller
public class ControllerMttoMediosAutorizados extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5176993756583662762L;
	
	/** La constante VIEWMEDIOSAUTORIZADOS. */
	private static final String VIEWMEDIOSAUTORIZADOS = "mantoMediosAutorizados";
	
	/** La constante VIEWNUEVOMEDIOSAUTORIZADOS. */
	private static final String VIEWNUEVOMEDIOSAUTORIZADOS = "nuevoMantomediosAutorizados";

	/** La constante LST_MEDIOS. */
	private static final String LST_MEDIOS = "lstMedEntrega";
	
	/** La constante LST_TRANFERENCIAS. */
	private static final String LST_TRANFERENCIAS = "lstTransferencias";
	
	/** La constante LST_OPERACIONES. */
	private static final String LST_OPERACIONES = "lstOperaciones";
	
	/** La constante LST_MECANISMOS. */
	private static final String LST_MECANISMOS = "lstMecanismos";
	
	/** La constante LST_HORARIOS. */
	private static final String LST_HORARIOS = "lstHorarios";
	
	/** La constante LST_CANALES. */
	private static final String LST_CANALES = "lstCanales";
	
	/** La constante LST_INHAB. */
	private static final String LST_INHAB = "lstInHab";	
	
	/** La constante NBEANMTTOMEDIOS. */
	private static final String NBEANMTTOMEDIOS = "beanMttoMediosAutorizadosRes";
	
	/** La constante NBEANMTTOMEDIOSREQ. */
	private static final String NBEANMTTOMEDIOSREQ = "beanMttoMediosAutorizadosReq";
	
		
	/** La constante TIPO_ACCION. */
	private static final String TIPO_ACCION = "accion";
	
	/** La constante INSERT. */
	private static final String INSERT = "INSERT";
	
	/** La constante TIPO. */
	private static final String TIPO = "tipo";
	
	/** La constante UPDATE. */
	private static final String UPDATE = "UPDATE";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La variable que contiene informacion con respecto a: bo mtto medios autorizados. */
	private BOMttoMediosAutorizados boMttoMediosAutorizados;
	
	
	/**
	 * Metodo de inicio de la pantalla de mantenimineto de medios.
	 *
	 * @return model --> objeto tipo ModelAndView que envia el tipo de accion a ejecutar
	 */
	@RequestMapping(value = "/mantenimientoMediosAutorizadosNuevo.do")
	public ModelAndView nuevoMantoMedios() {
		/**Se incicializa la variable de vista y se manda llama la fucnion de accion**/
		ModelAndView model = accion(new BeanMttoMediosAutorizadosReq(), 0);
		/**se inserta la funcion de insert**/
		model.addObject(TIPO_ACCION, INSERT);
		return model;
	}
	
	/**
	 * metodo para agregar un nuevo regisrtro al catalogo y actualizar .
	 * este se llama en el boton de guardar en la pantalla para agregar o modificar el registro de mantenimiento de medios
	 *
	 * @param beanMttoMediosAutorizadosReq --> objeto de tipo beanMttoMediosAutorizadosReq envia los datos a dar de alta o modificar
	 * @param beanPaginador --> objeto de tipo beanPaginador que permite paginar
	 * @param beanOrdenamiento --> objeto de tipo beanOrdenamiento que ordena los registros
	 * @param bindingResult --> objeto de tipo bindingResult que envia el resultado de la operacion
	 * @return model --> objeto ModelAndView que regresa el mensaje de exito o error al realizar la operacion
	 */
	@RequestMapping( value = "/addMttoMedios.do")
	public ModelAndView addMttMedios(@ModelAttribute(NBEANMTTOMEDIOSREQ) BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, BindingResult bindingResult) {
		/**se inicializa la accion**/
		int accion = 1;
		
		/**se pregunta si la accion es actutaliza **/
		if(beanMttoMediosAutorizadosReq.getAccion().equals(UPDATE)) {
			/**accion es igual a tres para indicar que se actualiza el registro */
			accion=3;
		}
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		/**Se incicializa la variable de vista y se manda llama la fucnion de accion**/
		ModelAndView model = accion(beanMttoMediosAutorizadosReq,accion);
		model.addObject("mensaje", true);
		return model;
	}
	
	/**
	 * Metodo que se utiliza para buscar los datos del registro seleccionado para realizar un cambio.
	 * este se utiliza en la pantalla principal de mantenimiento de madios
	 *
	 * @param beanMttoMediosAutorizadosReq --> objeto de tipo beanMttoMediosAutorizadosReq que realiza una consulta para mostrar todos los datos editables de un registro
	 * @param beanPaginador --> objeto de tipo beanPaginador que permite paginar
	 * @param beanOrdenamiento --> objeto de tipo beanOrdenamiento que ordena los registros
	 * @param bindingResult --> objeto de tipo bindingResult que envia el resultado de la operacion
	 * @return model --> objeto ModelAndView que regresa el mensaje de exito o error al realizar la operacion
	 */
	@RequestMapping( value = "/editMttoMedios.do")
	public ModelAndView editarMttMedios(@ModelAttribute(NBEANMTTOMEDIOSREQ) BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		BeanMttoMediosAutorizadosReq beanReq = new BeanMttoMediosAutorizadosReq();
		
		/**se recorre la lista para obtener el registro seleccionado **/
		for(BeanMttoMediosAutorizados li :beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()) {
			if(li.isSeleccionado()) {
				beanReq.setCveTransfe(li.getCveTransfe());
				beanReq.setCveOperacion(li.getCveOperacion());
				beanReq.setCveMedioEnt(li.getCveMedioEnt());
				beanReq.setCveMecanismo(li.getCveMecanismo());
			}
		}
		/**Se incicializa la variable de vista y se manda llama la fucnion de accion**/
		ModelAndView model = accion(beanReq, 2);
		model.addObject(TIPO_ACCION, UPDATE);
		model.addObject("mensaje", false);
		return model;
	}
	
	/**
	 *  metodo que realiza la eliminacion de algun registro de la pantalla principal de mantenimiento de medios
	 *  
	 *
	 * @param beanMttoMediosAutorizadosReq --> objeto de tipo beanMttoMediosAutorizadosReq que realiza el eliminado del registro
	 * @param paginador --> objeto de tipo beanPaginador que permite paginar
	 * @param beanOrdenamiento --> objeto de tipo beanOrdenamiento que ordena los registros
	 * @param bindingResult --> objeto de tipo bindingResult que envia el resultado de la operacion
	 * @return model --> objeto ModelAndView que regresa el mensaje de exito o error al realizar la operacion
	 */
	@RequestMapping( value = "/eliminarMttoMedios.do")
	public ModelAndView eliminarMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,
			BeanOrdenamiento beanOrdenamiento, BeanPaginador paginador, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/**Se incicializa la variables**/
		ModelAndView modelElimina =  null;
		BeanMttoMediosAutoBusqueda bean = new BeanMttoMediosAutoBusqueda();
		bean.setBeanOrdenamiento(beanOrdenamiento);
		RequestContext ctx = new RequestContext(req);
		bean.setBeanPaginador(paginador);	
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();
		BeanMttoMediosAutorizadosRes beanResults = new BeanMttoMediosAutorizadosRes();
		String mensaje="";
		/**funcion que incia cuando es una actualizacion**/
		try {
			
			/**si esta seleccionado se eliminan**/
			
			beanResults = boMttoMediosAutorizados.delMttMedios(beanMttoMediosAutorizadosReq,getArchitechBean());
			
			if(beanResults.getCodError().equals(Errores.OK00004V)) {
				beanResults.setCodError(Errores.OK00000V);
			}
			/**Se realiza la busqueda de **/
			bean.setDto("todo");
			bean.setFiltro("todo");
			beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.consultaTablaMA(bean, getArchitechBean(), new BeanMttoMediosAutorizados());
			/**Se crea la variable de vista**/			
			modelElimina = new ModelAndView(VIEWMEDIOSAUTORIZADOS,NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);
			
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + beanResults.getCodError());
			modelElimina.addObject(Constantes.COD_ERROR, beanResults.getCodError());
			modelElimina.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_INFO);
			modelElimina.addObject(Constantes.DESC_ERROR,mensaje);	
			
		} catch (BusinessException e) {
			showException(e);
			modelElimina =  new ModelAndView(VIEWMEDIOSAUTORIZADOS);
			modelElimina.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modelElimina.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
			modelElimina.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
			error("Ocurrio error al obtener los datos " + e.getMessage(), this.getClass());
		}
		return modelElimina;
	}
	
	/**
	 * Actualiza los medios a Seguro o No Seguro. 
	 *
	 * @param seguros --> variable de tipo booleano que camnia su valor a SI o NO al dar de alta o modificar
	 * @param beanMttoMediosAutorizadosReq --> envia el valor del campo para insertar o modificar
	 * @param filtros --> objeto de tipo BeanMttoMediosAutorizados que aplica filtro
	 * @param beanOrdenamiento --> objeto de tipo BeanOrdenamiento que ordena los registros
	 * @param paginador --> objeto de tipo BeanPaginador que permite paginar los registros
	 * @param bindingResult --> objeto de tipo bindingResult que envia datos
	 * @return model --> objeto de tipo ModelAndView que muestra los resultados de la operacion
	 */
	@RequestMapping( value = "/mediosAutorizadosSeguros.do")
	public ModelAndView mediosAutorizadosSeguros(@RequestParam boolean seguros, BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, BeanMttoMediosAutorizados filtros,
			BeanOrdenamiento beanOrdenamiento, BeanPaginador paginador, BindingResult bindingResult) {
		info("Inicia actualizacion de medios autorizados a seguros: " + seguros);
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/**Se incicializa la variables**/
		ModelAndView modelElimina =  null;
		BeanMttoMediosAutoBusqueda bean = new BeanMttoMediosAutoBusqueda();
		bean.setBeanOrdenamiento(beanOrdenamiento);
		RequestContext ctx = new RequestContext(req);
		bean.setBeanPaginador(paginador);	
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();
		BeanMttoMediosAutorizadosRes beanResults = new BeanMttoMediosAutorizadosRes();
		String mensaje="";
		String canal = "0";
		if (seguros) {
			canal = "1";
		}
		/**funcion que incia cuando es una actualizacion**/
		try {
			for (BeanMttoMediosAutorizados medio : beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()) {
				if (medio.isSeleccionado()) {
					beanMttoMediosAutorizadosReq.setAccion(medio.getAccion());
					beanMttoMediosAutorizadosReq.setCanal(canal);
					beanMttoMediosAutorizadosReq.setCveCLACON(medio.getCveCLACON());
					beanMttoMediosAutorizadosReq.setCveCLACONTEF(medio.getCveCLACONTEF());
					beanMttoMediosAutorizadosReq.setCveMecanismo(medio.getCveMecanismo());
					beanMttoMediosAutorizadosReq.setCveMedioEnt(medio.getCveMedioEnt());
					beanMttoMediosAutorizadosReq.setCveOperacion(medio.getCveOperacion());
					beanMttoMediosAutorizadosReq.setCveTransfe(medio.getCveTransfe());
					beanMttoMediosAutorizadosReq.setFlgInHab(medio.getFlgInHab());
					beanMttoMediosAutorizadosReq.setHoraCierre(medio.getHoraCierre());
					beanMttoMediosAutorizadosReq.setHoraInicio(medio.getHoraInicio());
					beanMttoMediosAutorizadosReq.setHorarioEstatus(medio.getHorarioEstatus());
					beanMttoMediosAutorizadosReq.setLimiteImporte(medio.getLimiteImporte());
					beanMttoMediosAutorizadosReq.setSucOperante(medio.getSucOperante());
					beanMttoMediosAutorizadosReq.setVersion(medio.getVersion());
					beanResults = boMttoMediosAutorizados.modificarMttMedios(beanMttoMediosAutorizadosReq, getArchitechBean());
				}
			}
			if(beanResults.getCodError().equals(Errores.OK00004V)) {
				beanResults.setCodError(Errores.OK00000V);
			}
			/**Se realiza la busqueda de **/
			bean.setDto("todo");
			bean.setFiltro("todo");
			beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.consultaTablaMA(bean, getArchitechBean(), filtros);
			/**Se crea la variable de vista**/
			modelElimina = new ModelAndView(VIEWMEDIOSAUTORIZADOS,NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + beanResults.getCodError());
			modelElimina.addObject(Constantes.COD_ERROR, beanResults.getCodError());
			modelElimina.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_INFO);
			modelElimina.addObject(Constantes.DESC_ERROR,mensaje);
		} catch (BusinessException e) {
			showException(e);
			modelElimina =  new ModelAndView(VIEWMEDIOSAUTORIZADOS);
			modelElimina.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modelElimina.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
			modelElimina.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
			error("Ocurrio un error en los datos " + e.getMessage(), this.getClass());
		}
		return modelElimina;
	}
	
	
	
	/**
	 * funcion para realizar las acciones de insert y update.
	 *
	 * @param beanMttoMediosAutorizadosReq --> objeto de tipo beanMttoMediosAutorizadosReq que envia datos
	 * @param accion El objeto --> accion(si es 1 se guarda el nuevo registro,  2 muestra el registro seleccionado para editarlo,
	 *  3 guarda el registro si se modifico y default muestra la pantalla para registrar un nuevo dato en el catalogo )
	 * @return model --> regresa la vista
	 */
	private ModelAndView accion(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq,int accion) {
			ModelAndView model = null;
			model =  new ModelAndView(VIEWNUEVOMEDIOSAUTORIZADOS);
			BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();
			BeanMttoMediosAutorizadosReq bean = new BeanMttoMediosAutorizadosReq();
			BeanMttoMediosAutorizadosRes listas = new BeanMttoMediosAutorizadosRes();
			String codigoError = "";
			/** Se crea una variable para almacenar el mensaje de la transaccion*/
			String mensaje = null;
			try {
				switch (accion) {				
				/**funcion que realiza el guardado de medio autorizado**/
				case 1:			
					/** llama la funcion para agregar**/
					beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.guardarNuevoMttMedios(beanMttoMediosAutorizadosReq, getArchitechBean());	
					mensaje = beanMttoMediosAutorizadosRes.getMsgError();
					codigoError = beanMttoMediosAutorizadosRes.getCodError();
					/**llama la pantalla**/					
					break;
				case 2:
					/**funcion que incia cuando es una actualizacion**/
					beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.mostrarRegistroEditarMttMedios(beanMttoMediosAutorizadosReq, getArchitechBean(),UPDATE);					
					break;
				case 3:
					/**funcion que incia cuando es una actualizacion**/
					beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.modificarMttMedios(beanMttoMediosAutorizadosReq, getArchitechBean());
					/** Se guarda el mensaje obtenido**/
					mensaje = beanMttoMediosAutorizadosRes.getMsgError();
					codigoError = beanMttoMediosAutorizadosRes.getCodError();
					break;
				default:
					
					bean.setAccion(INSERT);
					beanMttoMediosAutorizadosRes.setBeanMttoMediosAutoReq(bean);
					model = new ModelAndView(VIEWNUEVOMEDIOSAUTORIZADOS, NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);
					/**inicializa la pantalla cuando es nuevo**/					
					break;
				}			
				
				listas = boMttoMediosAutorizados.llenaListas(getArchitechBean());
				
				/**Se valida el tipo de accion que se esta ejecutando**/
				if(accion!= 0 && (accion != 3 || accion != 1 )) {
					model = new ModelAndView(VIEWNUEVOMEDIOSAUTORIZADOS, NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);
					
					model.addObject(Constantes.COD_ERROR, beanMttoMediosAutorizadosRes.getCodError());
					model.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
					model.addObject(Constantes.DESC_ERROR, beanMttoMediosAutorizadosRes.getMsgError());
				}
				
				/**Cuando el tipo es 2 se cambia la accion de la pantalla y se desactivan los combos**/
				if (accion==2) {
					model.addObject(TIPO, "2");
					model.addObject("disabled", true);
					model.addObject(TIPO_ACCION, UPDATE);
				}else {					
					model.addObject(TIPO, "1");
					model.addObject(TIPO_ACCION, INSERT);
				}
				
				/** Cuando el tipo es 3 entonces se llama a la pantalla de consulta y se agregan agrega el mensaje temporal**/
				if(accion == 3 || accion == 1) {
					BeanMttoMediosAutoBusqueda beanA = new BeanMttoMediosAutoBusqueda();
					/**Se realiza la busqueda de **/
					beanA.setDto("todo");
					beanA.setFiltro("todo");
					beanA.setBeanPaginador(BeanPaginador.crearPaginadorTodosRegistros());
					beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.consultaTablaMA(beanA, getArchitechBean(), new BeanMttoMediosAutorizados());
					/**Se crea la variable de vista**/			
					model = new ModelAndView(VIEWMEDIOSAUTORIZADOS,NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);					
					model.addObject(Constantes.COD_ERROR, codigoError);
					model.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
					model.addObject(Constantes.DESC_ERROR, mensaje);
				}else {
					model.addObject(LST_MEDIOS, listas.getListMediosEntrega());
					model.addObject(LST_TRANFERENCIAS, listas.getListTransferencia());
					model.addObject(LST_OPERACIONES, listas.getListOperaciones());
					model.addObject(LST_MECANISMOS, listas.getListMecanismo());
					model.addObject(LST_CANALES, listas.getListCanal());
					model.addObject(LST_HORARIOS, listas.getListHorario());
					model.addObject(LST_INHAB, listas.getListInHab());
				}
				
				
			} catch (BusinessException e) {
				showException(e);
				
				model.addObject(Constantes.COD_ERROR, Errores.EC00011B);
				model.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
				model.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);				
				error("Ocurrio un error al obtener los datos " + e.getMessage(), this.getClass());
			}
			
			
			return model;
	}
	

	
	
	
	/**
	 * Metodo Get que obtiene boMttoMediosAutorizados
	 *
	 * @return boMttoMediosAutorizados --> obtiene el objeto boMttoMediosAutorizados
	 */
	public BOMttoMediosAutorizados getBoMttoMediosAutorizados() {
		return boMttoMediosAutorizados;
	}

	/**
	 * Definine el objeto boMttoMediosAutorizados.
	 *
	 * param boMttoMediosAutorizados --> asigna el objeto boMttoMediosAutorizados
	 */
	public void setBoMttoMediosAutorizados(BOMttoMediosAutorizados boMttoMediosAutorizados) {
		this.boMttoMediosAutorizados = boMttoMediosAutorizados;
	}
	
}
