/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerHistoricoMensajes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:32 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOHistorialMsj;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

/**
 * Class ControllerHistoricoMensajes.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * pagos-historico de mensajes y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerHistoricoMensajes extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4906730840033386283L;
	
	/** La constante PANTALLA_HISTORICA. */
	private static final String PANTALLA_HISTORICA = "historialMensajesPC";
	
	/** La constante BEAN_HISTORICO. */
	private static final String BEAN_HISTORICO = "beanResHistorialMensajes";
	
	/** La variable que contiene informacion con respecto a: bo pagos. */
	private BOHistorialMsj boHistorialMsj;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	
	/** La constante TIPO_ORDEN. */
	private static final String TIPO_ORDEN = "tipoOrden";
	
	
	
	/**
	 * Consulta historico.
	 *
	 * @param paginador El objeto: paginador
	 * @param tipoOrden El objeto: tipo orden
	 * @param referencia El objeto: referencia
	 * @param beanModulo El objeto: bean modulo
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaHistorial.do") 
	public ModelAndView consultaHistorico( BeanPaginador paginador, @RequestParam("tipoRef") String tipoOrden, @RequestParam("ref") String referencia,
			@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			@Valid BeanReqHistorialMensajes beanReqHistorialMensajes, BindingResult bindingResult) {
		ModelAndView model = null;
		BeanResHistorialMensajes beanResHistorialMensajes = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			beanResHistorialMensajes = boHistorialMsj.obtenerHistorial(beanModulo, paginador, getArchitechBean(), beanReqHistorialMensajes);
			beanResHistorialMensajes.setRef(referencia);
			model = new ModelAndView(PANTALLA_HISTORICA);
			model.addObject(BEAN_HISTORICO, beanResHistorialMensajes);
			model.addObject(TIPO_ORDEN, tipoOrden);
			model.addObject("refe", referencia);
			model.addObject(Constantes.COD_ERROR, beanResHistorialMensajes.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResHistorialMensajes.getTipoError());
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResHistorialMensajes.getCodError(), null, null);
			model.addObject(Constantes.DESC_ERROR,mensaje);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			model.addObject(ConstantesWebPOACOA.SORT_FIELD, beanReqHistorialMensajes.getSortField());
			model.addObject(ConstantesWebPOACOA.SORT_TYPE, beanReqHistorialMensajes.getSortType());
			generarContadoresPaginacionHis(beanResHistorialMensajes, model);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * Generar contadores paginacion his.
	 *
	 * @param beanResHistorialMensajes El objeto: bean res historial mensajes
	 * @param model El objeto: model
	 */
	protected void generarContadoresPaginacionHis(BeanResHistorialMensajes beanResHistorialMensajes, ModelAndView model) {
		if (beanResHistorialMensajes.getTotalReg() > 0){
			Integer regIni = 1;
			Integer regFin = beanResHistorialMensajes.getTotalReg();
			
			if (!"1".equals(beanResHistorialMensajes.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResHistorialMensajes.getTotalReg() >= 20 * Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResHistorialMensajes.getBeanPaginador().getPagina());
			}			
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}
	
	/**
	 * Obtener el objeto: bo historial msj.
	 *
	 * @return El objeto: bo historial msj
	 */
	public BOHistorialMsj getBoHistorialMsj() {
		return boHistorialMsj;
	}

	/**
	 * Definir el objeto: bo historial msj.
	 *
	 * @param boHistorialMsj El nuevo objeto: bo historial msj
	 */
	public void setBoHistorialMsj(BOHistorialMsj boHistorialMsj) {
		this.boHistorialMsj = boHistorialMsj;
	}

	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
