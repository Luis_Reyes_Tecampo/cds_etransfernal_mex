/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   17/09/2019 09:03:03 AM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepCtasFideicomiso;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOExcepCtasFideicomiso;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarCuentasFideico;

/**
 * Class ControllerExcepCtasFideicomiso.
 * 
 * @author FSW-Vector
 * @sice 07 septiembre 2018
 */
@Controller
public class ControllerExcepCtasFideicomiso extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 6670009358021458911L;
	
	/** La constante PAGINA. */
	private static final String PAGINA = "excepcionCuentasFideicomiso";
	
	/** La constante FILTRO. */
	private static final String FILTRO = "beanFilter";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaCuentaFideicomiso";
	
	/** La constante ESTILO_LETRA. */
	private static final String ESTILO_LETRA = "Calibri";
	
	/** La constante NOMBRE_BEAN. */
	private static final String NOMBRE_BEAN = "beanExcepCtasFideicomiso";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante BEAN_CTA_FIDEI. */
	private static final String BEAN_CTA_FIDEI = "beanCtasFideico";
	
	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante TOTAL_REGISTROS. */
	private static final String TOTAL_REGISTROS = "totalReg";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante ALTA. */
	private static final String ALTA = "alta";
	
	/** La constante EDITAR. */
	private static final String EDITAR = "editar";
	
	/** La variable que contiene informacion con respecto a: bo excep ctas fideicomiso. */
	private BOExcepCtasFideicomiso boExcepCtasFideicomiso;
	
	/** La constante utilerias. */
	private static final UtilsExportarCuentasFideico utilerias = UtilsExportarCuentasFideico.getInstancia();
	
	/**
	 * Principal excep ctas fideicomiso.
	 *
	 * @param beanPaginador El objeto: bean paginador --> Objeto de entrada con datos de paginacion
	 * @param beanFilter El objeto: bean filter  --> Objeto de entrada con datos de los filtros
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "excepCuentasFideicomiso.do")
	public ModelAndView principalExcepCtasFideicomiso(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanExcepCtasFideicomiso beanFilter, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** se inicializan variables */
		ModelAndView modeloVista = null;
		BeanExcepCtasFideicomiso response = null;
		BeanExcepCtasFideicomiso beanRequest = beanFilter;
		beanRequest.setBeanPaginador(beanPaginador);
		
		try {
			/** se ejecuta la consulta con el filtro de entrada */
			response = boExcepCtasFideicomiso.consultaCuentas(getArchitechBean(),beanRequest);
			modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI, response);
			/** asigna mensaje de respuesta */
			modeloVista.addObject(FILTRO, beanFilter.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, response.getBeanPaginador());
			modeloVista.addObject(TOTAL_REGISTROS, response.getBeanFilter().getTotalReg());
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Alta excep cuentas fideicomiso.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "altaExcepCuentasFideicomiso.do")
	public ModelAndView altaExcepCuentasFideicomiso(
			@ModelAttribute(NOMBRE_BEAN) BeanExcepCtasFideicomiso beanExcepCtasFideicomiso, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** inicializa variables */
		ModelAndView modeloVista = null;
		/** valida cuenta entrante para el alta */
		if(beanExcepCtasFideicomiso.getBeanFilter().getCuenta() == null || beanExcepCtasFideicomiso.getBeanFilter().getCuenta().isEmpty() ) {
			/** si no llego la cuenta muestra la pantalla del alta */
			modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
			modeloVista.addObject(ACCION,ALTA);
			
		} else {
			/** en caso de que si, ejecuta flujo del alta */
			modeloVista = altaExcepCuentasFideico(beanExcepCtasFideicomiso);
		}
		return modeloVista;
	}
	
	/**
	 * Alta excep cuentas fideico.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	public ModelAndView altaExcepCuentasFideico( BeanExcepCtasFideicomiso beanExcepCtasFideicomiso) {
		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanExcepCtasFideicomiso response = null;
		BeanExcepCtasFideicomiso consultaCuentas = null;
		try {
			/** ejecuta el alta */
			response = boExcepCtasFideicomiso.altaCuentas(getArchitechBean(), beanExcepCtasFideicomiso.getBeanFilter().getCuenta());
			/** consulta los registros */
			consultaCuentas = boExcepCtasFideicomiso.consultaCuentas(getArchitechBean(),new BeanExcepCtasFideicomiso());
			/** valida la respuesta del alta */
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI,consultaCuentas);
			} else {
				modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
				modeloVista.addObject(ACCION,ALTA);
			}
			/** se agregan datos de respuesta */
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			modeloVista.addObject(FILTRO, consultaCuentas.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, consultaCuentas.getBeanPaginador());
			modeloVista.addObject(TOTAL_REGISTROS, consultaCuentas.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Editar excep cuentas fideicomiso.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "cambioExcepCuentasFideicomiso.do")
	public ModelAndView editarExcepCuentasFideicomiso(
			@ModelAttribute(NOMBRE_BEAN) BeanExcepCtasFideicomiso beanExcepCtasFideicomiso, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** inicializa variables */
		ModelAndView modeloVista = null;
		List<BeanCtaFideicomiso> listaCtasSeleccionadas = new ArrayList<BeanCtaFideicomiso>();
		/** obtiene una lista de los registros seleccionados */
		for(int i = 0; i < beanExcepCtasFideicomiso.getListaCuentas().size(); i++) {
			if(beanExcepCtasFideicomiso.getListaCuentas().get(i).isSeleccionado()) {
				listaCtasSeleccionadas.add(beanExcepCtasFideicomiso.getListaCuentas().get(i));
			}
		}
		
		/** valida si no llego la cuenta nueva */
		if(beanExcepCtasFideicomiso.getBeanFilter().getCuenta() == null || beanExcepCtasFideicomiso.getBeanFilter().getCuenta().isEmpty() ) {
			/** si no llego, muestra pantalla de editar */
			modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
			modeloVista.addObject(ACCION,EDITAR);
			modeloVista.addObject(FILTRO,listaCtasSeleccionadas.get(0));
			
		} else {
			/** si llego, ejecuta la modificacion */
			modeloVista = editarExcepCuentasFideico(beanExcepCtasFideicomiso);
		}
		return modeloVista;
	}
	
	/**
	 * Editar excep cuentas fideico.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	public ModelAndView editarExcepCuentasFideico( BeanExcepCtasFideicomiso beanExcepCtasFideicomiso) {
		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanExcepCtasFideicomiso response = null;
		BeanExcepCtasFideicomiso consultaCuentas = null;
		BeanCtaFideicomiso filtroPrincipal = new BeanCtaFideicomiso();
		filtroPrincipal.setCuenta(beanExcepCtasFideicomiso.getListaCuentas().get(0).getCuenta());
		try {
			/** ejecuta la modificacion */
			response = boExcepCtasFideicomiso.modificacionCuentas(getArchitechBean(), 
					beanExcepCtasFideicomiso.getListaCuentas().get(0).getCuenta(), beanExcepCtasFideicomiso.getBeanFilter().getCuenta());
			/** realiza una consulta una vez se haya modificado */
			consultaCuentas = boExcepCtasFideicomiso.consultaCuentas(getArchitechBean(),new BeanExcepCtasFideicomiso());
			/** valida la respuesta de la modificacion */
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI,consultaCuentas);
				modeloVista.addObject(FILTRO, consultaCuentas.getBeanFilter());
			} else {
				modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
				modeloVista.addObject(FILTRO, filtroPrincipal);
				modeloVista.addObject(ACCION,EDITAR);
			}
			/** agrega valores de respuesta */
			modeloVista.addObject(TOTAL_REGISTROS, consultaCuentas.getBeanFilter().getTotalReg());
			modeloVista.addObject(BEAN_PAGINADOR, consultaCuentas.getBeanPaginador());
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Baja excep cuentas fideicomiso.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "bajaExcepCuentasFideicomiso.do")
	public ModelAndView bajaExcepCuentasFideicomiso(
			@ModelAttribute(NOMBRE_BEAN) BeanExcepCtasFideicomiso beanExcepCtasFideicomiso, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** inicializa variables */
		ModelAndView modeloVista = new ModelAndView(PAGINA);
		BeanExcepCtasFideicomiso response = null;
		BeanExcepCtasFideicomiso nuevaConsulta = new BeanExcepCtasFideicomiso();
		List<BeanCtaFideicomiso> listaCtasSeleccionadas = new ArrayList<BeanCtaFideicomiso>();
		for(int i = 0; i < beanExcepCtasFideicomiso.getListaCuentas().size(); i++) {
			if(beanExcepCtasFideicomiso.getListaCuentas().get(i).isSeleccionado()) {
				listaCtasSeleccionadas.add(beanExcepCtasFideicomiso.getListaCuentas().get(i));
			}
		}
		try {
			response = boExcepCtasFideicomiso.bajaCuentas(getArchitechBean(), listaCtasSeleccionadas);
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				nuevaConsulta = boExcepCtasFideicomiso.consultaCuentas(getArchitechBean(), beanExcepCtasFideicomiso);
				modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI,nuevaConsulta);
			} else {
				modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
			}
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			modeloVista.addObject(BEAN_PAGINADOR, nuevaConsulta.getBeanPaginador());
			modeloVista.addObject(TOTAL_REGISTROS, nuevaConsulta.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		
		return modeloVista;
	}
	
	/**
	 * Exportar nombres.
	 *
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "exportarExcepCuentasFideicomiso.do")
	public ModelAndView exportarCuentas(
			@ModelAttribute(NOMBRE_BEAN) BeanExcepCtasFideicomiso beanExcepCtasFideicomiso, BindingResult bindingResult) {
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName(ESTILO_LETRA);
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName(ESTILO_LETRA);
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBody(beanExcepCtasFideicomiso.getListaCuentas()));
		modelAndView.addObject("NAME_SHEET", "Excepci\u00F3n de Cuentas de Fideicomisos");
		return modelAndView;
	}
	
	/**
	 * Exportar todos cuentas.
	 *
	 * @param beanPaginador El objeto: bean paginador  --> Objeto de entrada con datos de paginacion
	 * @param beanExcepCtasFideicomiso El objeto: bean excep ctas fideicomiso --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "exporTodoExcepCuentasFideicomiso.do")
	public ModelAndView exportarTodosCuentas(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(NOMBRE_BEAN) BeanExcepCtasFideicomiso beanExcepCtasFideicomiso, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanResBase response = null;
		try {
			modeloVista = new ModelAndView(PAGINA,BEAN_CTA_FIDEI,beanExcepCtasFideicomiso);
			response = boExcepCtasFideicomiso.exportarTodo(getArchitechBean());
			modeloVista.addObject(Constantes.COD_ERROR, response.getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getMsgError());
			modeloVista.addObject(FILTRO, beanExcepCtasFideicomiso.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, beanPaginador);
			modeloVista.addObject(TOTAL_REGISTROS, beanExcepCtasFideicomiso.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}

	/**
	 * Obtener el objeto: bo excep ctas fideicomiso.
	 *
	 * @return El objeto: bo excep ctas fideicomiso
	 */
	public BOExcepCtasFideicomiso getBoExcepCtasFideicomiso() {
		return boExcepCtasFideicomiso;
	}

	/**
	 * Definir el objeto: bo excep ctas fideicomiso.
	 *
	 * @param boExcepCtasFideicomiso El nuevo objeto: bo excep ctas fideicomiso
	 */
	public void setBoExcepCtasFideicomiso(BOExcepCtasFideicomiso boExcepCtasFideicomiso) {
		this.boExcepCtasFideicomiso = boExcepCtasFideicomiso;
	}
}
