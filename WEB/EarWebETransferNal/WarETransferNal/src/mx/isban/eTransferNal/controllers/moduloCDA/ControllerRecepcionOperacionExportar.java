/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerRecepcionOperacionExportar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/09/2018 04:41:32 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOperacionExportarTodo;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;



/**
 * Clase tipo controlador que maneja los eventos de exportar de las pantalla de recepcion de operaciones de cuenta alterna y principal.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepcionOperacionExportar extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;
	

	
	/** constante de error *. */
	public static final String ERROR =" Se genero el error: ";
	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BORecepOperacion bORecepOperacion;
	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion. */
	private BORecepOperacionExportarTodo bORecepOperacionExportarTodo;
    
	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView model;
	
	
	
	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();
	
	
	
	/**
	 * Exportar recep operacion alterna hto.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param req El objeto: req
	 * @param re El objeto: re
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "exportarRecepCtaAlterna.do")
	public ModelAndView exportarRecepOperacionAlternaHto(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletRequest req, HttpServletResponse re,
			@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		model = consultaExcel(beanResTranSpeiRec, beanReqConsTranSpei,  req,beanTranSpeiRecOper) ;
		ArchitechSessionBean sessionBean = getArchitechBean();
		model.addObject("sessionBean", sessionBean); 
		return model;
	}
	
	
	/**
	 * Consulta excel.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param req El objeto: req
	 * @param re El objeto: re
	 * @param opcionMenu El objeto: opcion menu
	 * @return Objeto model and view
	 */
	private  ModelAndView consultaExcel(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTran, HttpServletRequest req,BeanTranSpeiRecOper beanTranSpeiRecOper) {
		//incializa objeto resquest
//		BeanResTranSpeiRec recepOperaAdonSaldos = new BeanResTranSpeiRec();
		//inciailiza bean comun
//		recepOperaAdonSaldos.setBeanComun(new BeanTranSpeiRecComun());
		RequestContext ctx = new RequestContext(req);	
		model = null;
		/**se declara la variable de historico **/
//		boolean historico = util.obtenerHistorico(beanReqConsTran);
		
//		try {
			/**funcion para obtener los datos a exportar**/
//			if( Integer.parseInt(beanReqConsTran.getPaginador().getPagina())==1) {
//				beanReqConsTran.setPaginador(new BeanPaginador());				
//			}else {
//				int inicio= Integer.parseInt(beanReqConsTran.getPaginador().getPagina())-1;
//				int fin= Integer.parseInt(beanReqConsTran.getPaginador().getPaginaFin())-1;
//				BeanPaginador paginador = new BeanPaginador();
//				paginador.setPagina(Integer.toString(inicio));
//				paginador.setPaginaFin(Integer.toString(fin));
//				beanReqConsTran.setPaginador(paginador);
//			}
			
//			recepOperaAdonSaldos = bORecepOperacion.consultaTodasTransf(beanReqConsTran, getArchitechBean(), historico,beanTranSpeiRecOper);
//			recepOperaAdonSaldos.getBeanComun().setNombrePantalla(beanReqConsTran.getBeanComun().getNombrePantalla());		
			
//			model = util.exportarRecepcionOperacion(recepOperaAdonSaldos, ctx,beanReqConsTran);
			model = util.exportarRecepcionOperacion(beanResTranSpeiRec, ctx,beanReqConsTran);
			
//		} catch (BusinessException e) {
//			showException(e);		
//			model = util.errorVista(historico, recepOperaAdonSaldos,beanReqConsTran,ctx);				
//			/** Mostrar error**/
//			error(ERROR+e.getMessage(), this.getClass());
//		}
		
		return model;
	}

	/**
	 * Exportar los registros total.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param req El objeto: req
	 * @param re El objeto: re
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto model and view
	 */
	@RequestMapping( value = "/exportaTodoRecepOperAlterna.do")
	public ModelAndView exportarTodo(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletRequest req, HttpServletResponse re,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper) {
			RequestContext ctx = new RequestContext(req);	
		model = consultaExportarTodo(beanResTranSpeiRec, beanReqConsTranSpei, ctx,beanTranSpeiRecOper);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		ArchitechSessionBean sessionBean = getArchitechBean();
		model.addObject("sessionBean", sessionBean); 
		return model;
	}
	
	
	/**
	 * Consulta el total de los registros.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param ctx El objeto: ctx
	 * @param opcionMenu El objeto: opcion menu
	 * @return Objeto model and view
	 */
	private ModelAndView consultaExportarTodo(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqConsTranSpei, RequestContext ctx,BeanTranSpeiRecOper beanTranSpeiRecOper ) {
		//incializa objeto resquest
		BeanResTranSpeiRec recepOperaAdmonSaldos = beanResTranSpeiRec;
		
		model = null;
		String mensaje=""; 
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		
		try {
			
			/**funcion para obtener los datos a exportar**/
//			if( Integer.parseInt(beanReqConsTranSpei.getPaginador().getPagina())==1) {
//				beanReqConsTranSpei.setPaginador(new BeanPaginador());				
//			}else {
//				int inicio= Integer.parseInt(beanReqConsTranSpei.getPaginador().getPagina())-1;
//				int fin= Integer.parseInt(beanReqConsTranSpei.getPaginador().getPaginaFin())-1;
//				BeanPaginador paginador = new BeanPaginador();
//				paginador.setPagina(Integer.toString(inicio));
//				paginador.setPaginaFin(Integer.toString(fin));
//				beanReqConsTranSpei.setPaginador(paginador);
//			}
			/**funcion para obtener los datos a exportar**/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(recepOperaAdmonSaldos,beanReqConsTranSpei, getArchitechBean(), historico,beanTranSpeiRecOper);
			
			BeanResTranSpeiRec exp = new BeanResTranSpeiRec();
			recepOperaAdmonSaldos.getBeanComun().setNombrePantalla(beanReqConsTranSpei.getBeanComun().getNombrePantalla());
			exp = bORecepOperacionExportarTodo.exportarTodo(getArchitechBean(),recepOperaAdmonSaldos, beanReqConsTranSpei,historico,beanTranSpeiRecOper);
		
			mensaje =ctx.getMessage(Constantes.COD_ERROR_PUNTO + exp.getBeanError().getCodError(),new String[] {exp.getBeanError().getMsgError()});
			
			model = util.vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
						
			/** Se muestran el mensaje segun el error que regrese**/
			if(exp.getBeanError().getCodError().equals(Errores.OK00002V)) {
				model.addObject(Constantes.COD_ERROR, exp.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_INFO);
				model.addObject(Constantes.DESC_ERROR, mensaje);
			}else {
				model.addObject(Constantes.COD_ERROR, exp.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
				model.addObject(Constantes.DESC_ERROR, exp.getBeanError().getMsgError());
			}
			model.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			
		} catch (BusinessException e) {
			showException(e);		
			model = util.errorVista(historico, recepOperaAdmonSaldos,beanReqConsTranSpei,ctx);
			/** Mostrar error**/
			error(ERROR+e.getMessage(), this.getClass());
		}
		
		return model;
	}
	
	/**
	 * Obtener el objeto: BO recep operacion.
	 *
	 * @return El objeto: BO recep operacion
	 */
	public BORecepOperacion getBORecepOperacion() {
		return bORecepOperacion;
	}

	
	/**
	 * Definir el objeto: BO recep operacion.
	 *
	 * @param recepOperacion El nuevo objeto: BO recep operacion
	 */
	public void setBORecepOperacion(BORecepOperacion recepOperacion) {
		bORecepOperacion = recepOperacion;
	}





	/**
	 * getbORecepOperacionExportarTodo de tipo BORecepOperacionExportarTodo.
	 * @author FSW-Vector
	 * @return bORecepOperacionExportarTodo de tipo BORecepOperacionExportarTodo
	 */
	public BORecepOperacionExportarTodo getbORecepOperacionExportarTodo() {
		return bORecepOperacionExportarTodo;
	}





	/**
	 * setbORecepOperacionExportarTodo para asignar valor a bORecepOperacionExportarTodo.
	 * @author FSW-Vector
	 * @param bORecepOperacionExportarTodo de tipo BORecepOperacionExportarTodo
	 */
	public void setbORecepOperacionExportarTodo(BORecepOperacionExportarTodo bORecepOperacionExportarTodo) {
		this.bORecepOperacionExportarTodo = bORecepOperacionExportarTodo;
	}
	
	

}