/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerConsultaOperaciones.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/03/2017 12:05:00  Vector		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOConsultaOperaciones;
import mx.isban.eTransferNal.utilerias.UtilsMapeaRequest;
import mx.isban.eTransferNal.utilerias.MetodosConsultaOperaciones;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Class ControllerConsultaOperaciones.
 *
 * @author FSW-Vector
 * @since 7/04/2017
 */
@Controller
public class ControllerConsultaOperaciones extends Architech {

	//Constante serialVersionUID
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -5441068178198654680L;

	//Constante PAGINA
	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	private static final String PAGINA = "consultaOperaciones";

	//Constante BEAN
	/** Propiedad del tipo String que almacena el valor de BEAN. */
	private static final String BEAN = "beanRes";

	/** Propiedad del tipo BOConsultaOperaciones que almacena el valor de boConsultaOperaciones. */
	private BOConsultaOperaciones boConsultaOperaciones;

	/**
	 * Definir el objeto: bo consulta operaciones.
	 *
	 * @param boConsultaOperaciones the boConsultaOperaciones to set
	 */
	public void setBoConsultaOperaciones(
			BOConsultaOperaciones boConsultaOperaciones) {
		this.boConsultaOperaciones = boConsultaOperaciones;
	}
	
	/**
	 * Muestra consulta operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return ModelAndView Objeto de Spring MVC
	 */
	//Metodo de vista inicial de modulo
	@RequestMapping(value = "/consultaOperaciones.do")
	ModelAndView muestraConsultaOperaciones(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		Map<String, Object> map = new HashMap<String, Object>();
		ModelAndView modelAndView = null;
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		modelAndView = new ModelAndView(PAGINA);
		map = modelAndView.getModel();
		
		//Se setea por default Seleccione en el combo de status 
		beanResConsOperaciones.setSelectedEstatus("Seleccione");
		map.put(BEAN, beanResConsOperaciones);

		//Se agregan las opciones del combo
		utilsConsultaOpe.agregarOpcionesCombo(modelAndView);
		
		return modelAndView;
	}

	/**
	 * Busca consulta operaciones.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//Metodo que hace la busqueda de operaciones
	@RequestMapping(value = "/consultaOperacionesBusqueda.do")
	ModelAndView buscaConsultaOperaciones(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		//Se valida que exista algo seleccionado para no causar error en el bo
		if(" ".equals(beanReqConsOperaciones.getEstatusOperacion())){
			beanReqConsOperaciones.setEstatusOperacion("Seleccione");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		map = modelAndView.getModel();
		
		//Se manda la consulta al bo
		try{
			beanResConsOperaciones = boConsultaOperaciones.consultarOperaciones(
					getArchitechBean(), beanReqConsOperaciones,true);
			utilsConsultaOpe.llenaError(map, beanResConsOperaciones, request, null,null);
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			utilsConsultaOpe.llenaError(map, null, request, null,e);
		}

		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;
	}


	/**
	 * Apartar.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//Metodo que aparta folios
	@RequestMapping(value = "/apartarOperaciones.do")
	public ModelAndView apartar(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		map = modelAndView.getModel();
		//Se manda la consulta al bo
		try{
			BeanResBase beanResApartar=boConsultaOperaciones.apartar(getArchitechBean(),
					beanReqConsOperaciones);
			utilsConsultaOpe.llenaError(map, beanResApartar, request, null, null);
		}catch(BusinessException e){
			//Se procesa la excepcion
			utilsConsultaOpe.llenaError(map, null, request, null,e);
			showException(e);
		}

		try{
			beanResConsOperaciones = boConsultaOperaciones.consultarOperaciones(
					getArchitechBean(), beanReqConsOperaciones,false);
		}catch(BusinessException e){
			//Se procesa la excepcion
			utilsConsultaOpe.llenaError(map, null, request, null,e);
			showException(e);
		}
		
		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);
		
		return modelAndView;
	}


	/**
	 * Desapartar.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//Metodo que desaparta los folios
	@RequestMapping(value = "/desapartarOperaciones.do")
	public ModelAndView desapartar(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		map = modelAndView.getModel();
		//Se manda la consulta al bo
		try{
			BeanResBase beanResDesapartar=boConsultaOperaciones.desapartar(getArchitechBean(),
					beanReqConsOperaciones);
			utilsConsultaOpe.llenaError(map, beanResDesapartar, request, null, null);
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			utilsConsultaOpe.llenaError(map, null, request, null,e);
		}
		
		try{
			beanResConsOperaciones = boConsultaOperaciones.consultarOperaciones(
					getArchitechBean(), beanReqConsOperaciones,false);
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			utilsConsultaOpe.llenaError(map, null, request, null,e);
		}
		
		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;
	}

	/**
	 * Eliminar.
	 *
	 * @param request the request
	 * @param response the responses
	 * @return the model and view
	 */
	//Metodo para eliminar operaciones
	@RequestMapping(value = "/eliminarOperaciones.do")
	public ModelAndView eliminar(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		map = modelAndView.getModel();
		//Se manda la consulta al bo
		try{
			BeanResBase beanResEliminar=boConsultaOperaciones.eliminar(getArchitechBean(),
					beanReqConsOperaciones);
			utilsConsultaOpe.llenaError(map, beanResEliminar, request, null, null);
		}catch(BusinessException e){
			//Se procesa la excepcion
			utilsConsultaOpe.llenaError(map, null, request, null,e);
			showException(e);
		}
		
		try{
			beanResConsOperaciones = boConsultaOperaciones.consultarOperaciones(
					getArchitechBean(), beanReqConsOperaciones,false);
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			utilsConsultaOpe.llenaError(map, null, request, null,e);
		}
		
		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);
		
		return modelAndView;
	}

	/**
	 * Exportar.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//Metodo para exportar operaciones selecionadas
	@RequestMapping(value = "/exportarOperaciones.do")
	public ModelAndView exportar(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		RequestContext ctx = new RequestContext(request);
		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = null;
		//Se manda la consulta al bo
		try{
			beanResConsOperaciones = boConsultaOperaciones.consultarOperaciones(
					getArchitechBean(), beanReqConsOperaciones,false);
			if (Errores.OK00000V.equals(beanResConsOperaciones.getCodError())
					|| Errores.ED00011V.equals(beanResConsOperaciones.getCodError())) {

				modelAndView = new ModelAndView(new ViewExcel());
				modelAndView.addObject("HEADER_VALUE", utilsConsultaOpe.getHeaderExcel(ctx));
				modelAndView.addObject("BODY_VALUE",
						utilsConsultaOpe.getBody(beanResConsOperaciones.getListBeanConsOperaciones()));
				modelAndView.addObject("NAME_SHEET", "exportarOperaciones");
				return modelAndView;
			}else{
				//Se llena el error
				utilsConsultaOpe.llenaError(map, beanResConsOperaciones, request, null, null);
			}
		}catch(BusinessException e){
			//Se procesa la excepcion
			showException(e);
			utilsConsultaOpe.llenaError(map, null, request, null,e);
		}
		
		map = modelAndView.getModel();
		
		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);

		
		return modelAndView;
	}
	
	/**
	 * Metodo utilizado para realizar solicitar la exportacion de todas
	 * las operaciones obtenidas de la consulta.
	 *
	 * @param request the request
	 * @param response the response
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	//Metodo para exportar las operaciones consultadas
	@RequestMapping(value = "/exportarTodoOperaciones.do")
	public ModelAndView exportarTodo(HttpServletRequest request, HttpServletResponse response) {
		MetodosConsultaOperaciones utilsConsultaOpe=new MetodosConsultaOperaciones();
		//Se obtiene el bean a partir del request
		ModelAndView modelAndView = new ModelAndView(PAGINA);
		BeanReqConsOperaciones beanReqConsOperaciones = requestToBeanReqConsOperaciones(request, response);

		StringBuilder  builder = new StringBuilder();
	    RequestContext ctx = new RequestContext(request);
		Map<String, Object> map = new HashMap<String, Object>();
		BeanResConsOperaciones beanResConsOperaciones = new BeanResConsOperaciones();
		map = modelAndView.getModel();
		//Se manda la consulta al bo
		try{
			beanResConsOperaciones = boConsultaOperaciones.exportarTodo(
					getArchitechBean(), beanReqConsOperaciones);

			if(Errores.OK00001V.equals(beanResConsOperaciones.getCodError())){
				String mensaje = ctx.getMessage("codError."+beanResConsOperaciones.getCodError());
				builder.append(mensaje);
				builder.append(beanResConsOperaciones.getNombreArchivo());
				mensaje = builder.toString();
				beanResConsOperaciones.setMsgError(mensaje);
				map.put(Constantes.DESC_ERROR, mensaje);
			}else{
				//Se llena el error
				String mensaje = ctx.getMessage("codError."+beanResConsOperaciones.getCodError());
				map.put(Constantes.DESC_ERROR, mensaje);
			}
			map.put(Constantes.TIPO_ERROR, beanResConsOperaciones.getTipoError());
			map.put(Constantes.COD_ERROR, beanResConsOperaciones.getCodError());
		}catch(BusinessException e){
			//Se procesa la excepcion
			utilsConsultaOpe.llenaError(map, null, request, null,e);
			showException(e);
		}
		
		//Se llenan los datos
		utilsConsultaOpe.llenarDatosDeRequest(beanReqConsOperaciones,modelAndView,beanResConsOperaciones);
		map.put(BEAN, beanResConsOperaciones);

		return modelAndView;			
	}
	
	/**
	 * Request to bean.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the bean req cons operaciones
	 */
	private BeanReqConsOperaciones requestToBeanReqConsOperaciones(HttpServletRequest req, HttpServletResponse res){
		BeanReqConsOperaciones beanReqConsOperaciones = new BeanReqConsOperaciones();
		List<BeanConsOperaciones> listBeanConsOperaciones = new ArrayList<BeanConsOperaciones>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistBeanConsOperaciones"), listBeanConsOperaciones, new BeanConsOperaciones());
		beanReqConsOperaciones.setListBeanConsOperaciones(listBeanConsOperaciones);
		UtilsMapeaRequest.getMapper().mapearObject(beanReqConsOperaciones, req.getParameterMap());
		return beanReqConsOperaciones;
	}
}


