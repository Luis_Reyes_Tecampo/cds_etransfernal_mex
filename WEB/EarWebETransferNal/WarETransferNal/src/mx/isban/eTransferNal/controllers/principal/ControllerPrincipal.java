package mx.isban.eTransferNal.controllers.principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LookAndFeel;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.sesiones.Sesiones;
import mx.isban.agave.sesiones.beans.SesionDTO;
import mx.isban.eTransferNal.servicio.moduloCDA.BOUsuario;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerPrincipal extends Architech{

    /**
	 * Constante con el serial version
	 */
	private static final long serialVersionUID = -756474091452407727L;

	
	

	/**
     * Componente de negocio que administra los usuarios con
     * acceso a transfer.
     **/
    private BOUsuario boUsuario;

	/**
	 * Metodo que mapea el salir
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo res
	 * @return ModelAndView Objeto de Spring mvc
	 */
	@RequestMapping(value="salir.do")
	public ModelAndView salir(HttpServletRequest req,
			HttpServletResponse res){
		debug("Saliendo de aplicacion...");
		
		LookAndFeel       lobjLyFBean       = (LookAndFeel)req.getSession().getAttribute("LyFBean");
		ArchitechSessionBean lobjArchitechBean = (ArchitechSessionBean)req.getSession().getAttribute("ArchitechSession");
		String               lstrSalir         = lobjLyFBean.getLinkSalirSAM();
		
		if(lobjArchitechBean!=null){
			Sesiones ses = Sesiones.getInstance();
			debug("Eliminando sesion del usuario:" + lobjArchitechBean.getUsuario());
			debug("En la aplicacion             :" + lobjArchitechBean.getIdApp());
			SesionDTO         sesDTO = new SesionDTO();
			sesDTO.setIdAplic  (lobjArchitechBean.getIdApp());
			sesDTO.setIdUsuario(lobjArchitechBean.getUsuario());
			sesDTO.setEstado(false);
			//ses.eliminaSesion(sesDTO);
			ses.actualizarSesion(sesDTO);
		}
		
		req.getSession().invalidate();

		debug("Enviando a hacer logut a SAM[" + lstrSalir + "]...");
		return new ModelAndView("redirect:" + lstrSalir);
	}	
	
	/**
	 * Metodo que mapea el cerrar.do
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo res
	 * @return ModelAndView Objeto de Spring mvc
	 */
	@RequestMapping(value="cerrar.do")
	public ModelAndView cerrar(HttpServletRequest req,
			HttpServletResponse res){
		debug("Saliendo de aplicacion...");
		
		LookAndFeel          lobjLyFBean       = (LookAndFeel)req.getSession().getAttribute("LyFBean");
		ArchitechSessionBean lobjArchitechBean = (ArchitechSessionBean)req.getSession().getAttribute("ArchitechSession");
		String               lstrSalir         = lobjLyFBean.getLinkSalirSAM();
		
		if(lobjArchitechBean!=null){
			Sesiones ses = Sesiones.getInstance();
			debug("Eliminando sesion del usuario:" + lobjArchitechBean.getUsuario());
			debug("En la aplicacion             :" + lobjArchitechBean.getIdApp());
			SesionDTO         sesDTO = new SesionDTO();
			sesDTO.setIdAplic  (lobjArchitechBean.getIdApp());
			sesDTO.setIdUsuario(lobjArchitechBean.getUsuario());
			sesDTO.setEstado(false);
			//ses.eliminaSesion(sesDTO);
			ses.actualizarSesion(sesDTO);
		}
		
		req.getSession().invalidate();

		debug("Enviando a hacer logut a SAM[" + lstrSalir + "]...");
		return new ModelAndView("redirect:" + lstrSalir);
	}	

  /**Metodo que se utiliza para recibir la peticion de inicio.do
   * @param request Objeto del tipo HttpServletRequest
   * @return ModelAndView Objeto del tipo ModelAndView
   * @throws BusinessException Exception de negocio
   */
   @RequestMapping(value = "inicio.do")
   public ModelAndView inicio(HttpServletRequest request ) throws BusinessException{
	  String perfiles = request.getHeader("iv-groups");
	  if(perfiles!=null){
		  perfiles = perfiles.replaceAll("\"", "" );
	  }
	  getArchitechBean().setPerfil(perfiles);
	  boUsuario.guardarUsuario(getArchitechBean());
      return new ModelAndView("inicioFrame");
   }
	   
  /**Metodo que se utiliza para recibir la peticion de inicio.do
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "inicioAplicacion.do")
   public ModelAndView inicioAplicacion(){
      return new ModelAndView("inicio");
   }
   /**
    * Metodo get que regresa la referencia al servicio BoUsuario
    * @return boUsuario Servicio BoUsuario
    */
	public BOUsuario getBoUsuario() {
		return boUsuario;
	}
   /**
    * Metodo set que modifica la referencia al servicio BoUsuario
    * @param boUsuario Servicio BoUsuario
    */
	public void setBoUsuario(BOUsuario boUsuario) {
		this.boUsuario = boUsuario;
	}
	   
	   
	  

}

