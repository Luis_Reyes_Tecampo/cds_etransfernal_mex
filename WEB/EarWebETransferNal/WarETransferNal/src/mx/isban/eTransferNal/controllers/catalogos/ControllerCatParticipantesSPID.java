/**
 * Isban Mexico
 * Clase: ControllerCatParticipantesSPID.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 08/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.controllers.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanParticipantesSPID;
import mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes;
import mx.isban.eTransferNal.beans.catalogos.BeanResParticipantesSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOCatParticipantesSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;


/**
 * @author Vector SF
 * @version 1.0
 *
 */
@Controller
public class ControllerCatParticipantesSPID extends Architech{

	/** */
	private static final long serialVersionUID = 1335054830173706747L;
	
	/**
	 * Constante para establecer nombre del bean
	 */
	private static final String BEAN_RESULTADO_CONSULTA = "resultadoConsulta";
	
	/**
	 * Constante con el nombre de la pagina
	 */
	private static final String PAG_CATALOGO ="catalogoParticipantesSPID";	
		
	/**
	 * Constante con el nombre de la pagina
	 */
	private static final String PAG_CATALOGO_ALTA ="altaCatParticipantes";	
	
	/** Constante lista canales. */
	private static final String LISTA_CANALES = "listaParticipantes";
	
	
	/**
	 * bo BOCatParticipantesSPID 
	 */ 
	private BOCatParticipantesSPID boCatParticipantesSPID;
	
	/**
	 * @return Valor del atributo: boCatParticipantesSPID
	 */
	public BOCatParticipantesSPID getBoCatParticipantesSPID() {
		return boCatParticipantesSPID;
	}

	/**
	 * @param boCatParticipantesSPID Valor a establecer en atributo: boCatParticipantesSPID
	 */
	public void setBoCatParticipantesSPID(BOCatParticipantesSPID boCatParticipantesSPID) {
		this.boCatParticipantesSPID = boCatParticipantesSPID;
	}

	/**
	 * Metodo que muestra la pantalla de inicial de catalogos de canales
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="catalogoParticipantesSPID.do")
	public ModelAndView muestraCatalogo(BeanReqParticipantes reqBean, HttpServletRequest request){			
		return new ModelAndView(PAG_CATALOGO);
	}
	
	/**
	 * Metodo que muestra la busqueda de participantes
	 * 
	 * @param reqBean de tipo HttpServletResponse
	 * @param request de tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value="buscarParticipantes.do")
	public ModelAndView muestraParticipantes(BeanPaginador paginador,HttpServletRequest request){	
		Map<String, Object> model = new HashMap<String,Object>();
        RequestContext ctx = new RequestContext(request);
        ModelAndView modelAndView = new ModelAndView(); 
        BeanReqParticipantes reqBean = new BeanReqParticipantes();
        String cveIntermediario = request.getParameter("claveInter");
        reqBean.setCveIntermediario(cveIntermediario);
        String mensaje = "";
        BeanResParticipantesSPID response = boCatParticipantesSPID.consultaParticipantes(reqBean, getArchitechBean());        

        model.put("cveParticipante", cveIntermediario);
		model.put(BEAN_RESULTADO_CONSULTA, response);
		model.put(LISTA_CANALES, response.getListParticipantes());
		
		if(Errores.OK00000V.equals(response.getCodError())){
			model.put(Constantes.TIPO_ERROR, response.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			model.put(BEAN_RESULTADO_CONSULTA, response);
			model.put(LISTA_CANALES, response.getListParticipantes());
			modelAndView = new ModelAndView(PAG_CATALOGO,model);
			
		}else{			
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError());
			model.put(Constantes.COD_ERROR, response.getCodError());
			model.put(Constantes.TIPO_ERROR, response.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			model.put(LISTA_CANALES, response.getListParticipantes());
			return new ModelAndView(PAG_CATALOGO, model);
		}
		
		return modelAndView;
	}
	
	/**
	 * Metodo que muestra la pantalla de alta de catalogo participantes
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="altaCatalogo.do")
	public ModelAndView altaCatalogo(BeanReqParticipantes reqBean, HttpServletRequest request){				
		return new ModelAndView(PAG_CATALOGO_ALTA);
	}
	
	/**
	 * Metodo queguarda el catalogo
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="guardaCatalogo.do")
	public ModelAndView guardaCatalogo(HttpServletRequest request){
		Map<String, Object> model = new HashMap<String,Object>();
		BeanResBase response = new BeanResBase();
		RequestContext ctx = new RequestContext(request);
		BeanReqParticipantes reqBean = new BeanReqParticipantes();
		String cve = request.getParameter("cveInter");
		String mensaje = "";
		reqBean.setCveIntermediario(cve);
		response = boCatParticipantesSPID.guardarParticipante(reqBean, getArchitechBean());		
		
		if(Errores.OK00000V.equals(response.getCodError())){
			mensaje =  ctx.getMessage("codError.ED00118V");
			model.put(Constantes.TIPO_ERROR, response.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			return new ModelAndView(PAG_CATALOGO, model);			
			
		}else{			
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError());
			model.put(Constantes.COD_ERROR, response.getCodError());
			model.put(Constantes.TIPO_ERROR, response.getTipoError());
			model.put(Constantes.DESC_ERROR, mensaje);
			return new ModelAndView(PAG_CATALOGO_ALTA, model);
		}
		
	}
	
	/**
	 * Metodo que guarda el catalogo
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="validaCatalogo.do")
	public ModelAndView validaCatalogo(HttpServletRequest request){
		RequestContext ctx = new RequestContext(request);
		BeanReqParticipantes reqBean = new BeanReqParticipantes();
		Map<String, Object> model = new HashMap<String,Object>();
		
		String cve = request.getParameter("cveInter");
		
		reqBean.setCveIntermediario(cve);
		
		BeanResParticipantesSPID response = boCatParticipantesSPID.consultaPartiAlta(reqBean, getArchitechBean());
		if(response.getListParticipantes()!=null && !response.getListParticipantes().isEmpty()){			
			model.put("cveInter", response.getListParticipantes().get(0).getCveIntermediario());
			model.put("numBanco", response.getListParticipantes().get(0).getNumBanxico());
			model.put("nombreLargo", response.getListParticipantes().get(0).getNombre());
			model.put("datoFecha", response.getListParticipantes().get(0).getFecha());
		}else{
			String mensaje = ctx.getMessage("codError.ED00117V");
			model.put(Constantes.COD_ERROR, response.getCodError());
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
			model.put(Constantes.DESC_ERROR, "mensaje");
			model.put("msgError", "valida");
		}

				
		return new ModelAndView(PAG_CATALOGO_ALTA, model);
	}
	
	/**
	 * Metodo elimina registro
	 * @param request the request
	 * @return ModelAndView Objeto del ModelAndView de Spring
	 */
	@RequestMapping(value="eliminaCatalogo.do")
	public ModelAndView eliminaCatalogo(HttpServletRequest request){
		RequestContext ctx = new RequestContext(request);
		Map<String, Object> model = new HashMap<String,Object>();
		String listaEliminar = (String)request.getParameter("hdnCheckSeleccionados");		
		String[] idsParticipantes = listaEliminar.split("@");
		String mensaje = "";
		BeanResBase response = null;
		BeanResParticipantesSPID beanResConsultaPart = new BeanResParticipantesSPID();
		BeanReqParticipantes beanRequestConsulta = new BeanReqParticipantes();

		String cveIntermediario = request.getParameter("claveInter");
		beanRequestConsulta.setCveIntermediario(cveIntermediario);
	     
		for( int i = 0 ; i < idsParticipantes.length ; i++ ){
			BeanReqParticipantes reqBean = new BeanReqParticipantes();				
			reqBean.setCveIntermediario(idsParticipantes[i]);
			response = boCatParticipantesSPID.eliminaParticipante(reqBean, getArchitechBean());
			if(Errores.OK00003V.equals(response.getCodError())) {
				mensaje = ctx.getMessage("codError.ED00115V");
			} else {
				mensaje = ctx.getMessage("codError.ED00116V");
			}
		}
		beanResConsultaPart = boCatParticipantesSPID.consultaParticipantes(beanRequestConsulta, getArchitechBean());
		model.put(Constantes.COD_ERROR, beanResConsultaPart.getCodError());
		model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
		model.put(Constantes.DESC_ERROR, mensaje);
		model.put(BEAN_RESULTADO_CONSULTA, beanResConsultaPart);
		model.put(LISTA_CANALES, beanResConsultaPart.getListParticipantes());

				
		return new ModelAndView(PAG_CATALOGO, model);
	}
	
	/**
	 * Metodo para la paginacion.
	 *
	 * @param request the request
	 * @param beanReqCanal Request de canal.
	 * @return the model and view
	 */
	@RequestMapping(value = "/paginarCat.do")
	public ModelAndView paginarAnterior(final HttpServletRequest request,
			BeanReqParticipantes beanReq) {		
		ModelAndView modelAndView = null;
			
			BeanResParticipantesSPID beanResConsulta = boCatParticipantesSPID.consultaParticipantes(beanReq, getArchitechBean());
			modelAndView = new ModelAndView(PAG_CATALOGO,
					BEAN_RESULTADO_CONSULTA, beanResConsulta);
			modelAndView.addObject(LISTA_CANALES,
					beanResConsulta.getListParticipantes());			

		return modelAndView;
	}
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 * @param req Objeto del tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "exportar.do")
    public ModelAndView expCatalogo(BeanPaginador beanPaginador,HttpServletRequest request){
		FormatCell formatCellHeaderAT = null;
		FormatCell formatCellBodyAT = null;
  	 	
        RequestContext ctx = new RequestContext(request);
        ModelAndView model = null; 
        BeanReqParticipantes reqBean = new BeanReqParticipantes();
        String cveIntermediario = request.getParameter("claveInter");
        reqBean.setCveIntermediario(cveIntermediario);
        
        
  	 	beanPaginador.setAccion("ACT");
		BeanResParticipantesSPID beanResInter = boCatParticipantesSPID.consultaParticipantes(reqBean, getArchitechBean());
  	   
		if(Errores.OK00000V.equals(beanResInter.getCodError())||
			Errores.ED00011V.equals(beanResInter.getCodError())){
			formatCellBodyAT = new FormatCell();
			formatCellBodyAT.setFontName("Calibri");
			formatCellBodyAT.setFontSize((short)11);
			formatCellBodyAT.setBold(false);
		    
			formatCellHeaderAT = new FormatCell();
			formatCellHeaderAT.setFontName("Calibri");
			formatCellHeaderAT.setFontSize((short)11);
			formatCellHeaderAT.setBold(true);

			model = new ModelAndView(new ViewExcel());
			model.addObject("FORMAT_HEADER", formatCellHeaderAT);
			model.addObject("FORMAT_CELL", formatCellBodyAT);
			model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
			model.addObject("BODY_VALUE", getBody(beanResInter.getListParticipantes()));
			model.addObject("NAME_SHEET", "CatalogoParticipantes");
		}else{
			model = muestraParticipantes(beanPaginador,request);
			String mensaje = ctx.getMessage("codError."+beanResInter.getCodError());
			model.addObject("codError", beanResInter.getCodError());
			model.addObject("tipoError", beanResInter.getTipoError());
			model.addObject("descError", mensaje);
		
		}
	  
	    return model;
    }
	
	 /**
     * @param ctx Objeto del tipo RequestContext
     * @return List<String>
     */
    private List<String> getHeaderExcel(RequestContext ctx){
   	 String []header = new String [] { 
   			 ctx.getMessage("modulo.catParticipantes.SPID.cve"),
   			 ctx.getMessage("modulo.catParticipantes.SPID.banco"),	
   			 ctx.getMessage("modulo.catParticipantes.SPID.nombre"),
   			 ctx.getMessage("modulo.catParticipantes.SPID.fecha")
   			 }; 
   	 	return Arrays.asList(header);
    }
    
    /**
	 * @param listaBeanAvisoTraspasos Objeto del tipo List<BeanAvisoTraspasos>
	 * @return List<List<Object>>
	 */
	private List<List<Object>> getBody(List<BeanParticipantesSPID> listaBean){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	if(listaBean!= null && 
		   !listaBean.isEmpty()){
  		   objList = new ArrayList<List<Object>>();
  	
  		   for(BeanParticipantesSPID beanPart : listaBean){ 
  	 		  objListParam = new ArrayList<Object>();
  	 		  objListParam.add(beanPart.getCveIntermediario());
  	 		  objListParam.add(beanPart.getNumBanxico());
  	 		  objListParam.add(beanPart.getNombre());
  	 		  objListParam.add(beanPart.getFecha());
  	 		  objList.add(objListParam);
  	 	   }
  	   } 
   	return objList;
    }
}
