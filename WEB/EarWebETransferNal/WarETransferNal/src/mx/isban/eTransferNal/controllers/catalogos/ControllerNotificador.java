/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerNotificador.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/07/2019 12:38:41 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificaciones;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesExportar;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BONotificador;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarNotificador;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsNotificador;

/**
 * Class ControllerNotificador.
 *
 * Clase tipo controller que mapea los flujos del catalogo
 * notifcador de estatus
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
@Controller
public class ControllerNotificador extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4079930641778001866L;

	/** La variable que contiene informacion con respecto a: bo notificador. */
	private transient BONotificador boNotificador;

	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaNotificador";
	
	/** La constante BEAN_NOTIFICACION. */
	private static final String BEAN_NOTIFICACION = "beanNotificacion";
	
	/** La constante BEAN_NOTIFICACIONES. */
	private static final String BEAN_NOTIFICACIONES = "beanNotificaciones";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaNotificador";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante COMBOS. */
	private static final String COMBOS = "combos";
	
	/** La constante POOL. */
	private static final String POOL = "pool";
	
	/** La constante CAPTURA. */
	private static final String CAPTURA = "captura";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La constante utileriasExportar. */
	private static final UtilsExportarNotificador utileriasExportar = UtilsExportarNotificador.getInstancia();
	
	/** La constante TIPO_COMBO. */
	private static final int TIPO_COMBO = 1;
	
	/** La constante TIPO_FILTRO. */
	private static final int TIPO_FILTRO = 2;
	
	/**
	 * Mostrar catalogo.
	 * 
	 * Mapeo del metodo de consulta de informacion
	 * del catalogo.
	 *
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/consultaNotificador.do")
	public ModelAndView mostrarCatalogo(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNotificacion beanFilter, BindingResult bindingResult) {

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		ModelAndView model = null;
		BeanNotificaciones response = null;
		try {
			/** Validacion del parametro accion **/
			if (req.getParameter(ACCION) != null && !req.getParameter(ACCION).equals(StringUtils.EMPTY)) {
				/** Se valida los filtros guardados**/
				beanFilter.setTipoNotif(req.getParameter("tipoNotifF"));
				beanFilter.setCveTransfer(req.getParameter("cveTransferF"));
				beanFilter.setMedioEntrega(req.getParameter("medioEntregaF"));
				beanFilter.setEstatusTransfer(req.getParameter("estatusTransferF"));
				beanPaginador.setPagina(req.getParameter("pagina"));
				beanPaginador.setPaginaIni(req.getParameter("paginaIni"));
				beanPaginador.setPaginaFin(req.getParameter("paginaFin"));
			}
			response = boNotificador.consultar(beanFilter, beanPaginador, getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOTIFICACIONES, response);
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
			/** Seteo de listas para los combos de los filtros **/
			model.addObject(COMBOS, boNotificador.consultarListas(TIPO_FILTRO, beanFilter, getArchitechBean()));
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	
	/**
	 * Mantenimiento notificador.
	 * 
	 * Mapeo del flujo para mostrar el fomurlario de 
	 * matenimiento de registros del catalogo.
	 *
	 * @param beanNotificaciones --> objeto de tipo beanNotificaciones que muestra notificaciones
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/mantenimientoNotificador.do")
	public ModelAndView mantenimientoNotificador(@ModelAttribute(BEAN_NOTIFICACIONES) BeanNotificaciones beanNotificaciones,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNotificacion beanFilter,
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Creacion de la vista de mantenimiento  **/
		ModelAndView model = null;
		BeanNotificacion notificacion = new BeanNotificacion();
		if (beanNotificaciones != null && beanNotificaciones.getNotificaciones() != null) {
			for(BeanNotificacion not : beanNotificaciones.getNotificaciones()) {
				if(not.isSeleccionado()) {
					notificacion = not;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		if (notificacion.getTipoNotif() != null) {
			notificacion.setTipoNotif(notificacion.getTipoNotif());
			model.addObject(ACCION,ACCION_MODIFICAR);
		} else {
			model.addObject(ACCION,ACCION_ALTA);
		}
		/** Se consultan las listas para los combos **/
		try {
			model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, null, getArchitechBean()));
			model.addObject(UtilsConstantes.BEAN_FILTER, beanFilter);
		} catch (BusinessException e) {
			showException(e);
		}
		model.addObject(BEAN_NOTIFICACION, notificacion);
		/** Retorno del metodo **/
		return model;
	}
	
	/**
	 * Agregar notificador.
	 * 
	 * Mapeo del metodo para iniciar el flujo de agregar 
	 * un nuevo registro al catalogo.
	 *
	 * @param notificacion --> objeto de tipo beanNotificaciones que agrega notificaciones
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/agregarNotificador.do")
	public ModelAndView agregarNotificador(@ModelAttribute(BEAN_NOTIFICACION) BeanNotificacion notificacion, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Comienza flujo de alta **/
		ModelAndView model = null;
		BeanNotificaciones notificaciones = null;
		BeanResBase response = null;
		try {
			response = boNotificador.agregar(notificacion, getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				notificaciones = boNotificador.consultar(new BeanNotificacion(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_NOTIFICACION, new BeanNotificacion());
				model.addObject(BEAN_NOTIFICACIONES, notificaciones);
				model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, null, getArchitechBean()));
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(POOL,CAPTURA);
				model.addObject(BEAN_NOTIFICACION, notificacion);
				model.addObject(ACCION, ACCION_ALTA);
				model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, null, getArchitechBean()));
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	
	/**
	 * Editar notificador.
	 * 
	 * Mapeo del metodo para iniciar el flujo de 
	 * modificar un registro del catalogo.
	 *
	 * @param notificacion --> objeto de tipo beanNotificaciones que modifica notificaciones
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/editarNotificador.do")
	public ModelAndView editarBloque(@ModelAttribute(BEAN_NOTIFICACION) BeanNotificacion notificacion, BindingResult bindingResult) {

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		/** Comienza flujo de edicion  **/
		ModelAndView model = null;
		BeanNotificaciones notificaciones = null;
		try {
			BeanResBase response = boNotificador.editar(notificacion, UtilsNotificador.getAnterior(req), getArchitechBean());
			if (Errores.OK00000V.equals(response.getCodError())) {
				notificaciones = boNotificador.consultar(new BeanNotificacion(), new BeanPaginador(), getArchitechBean());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, null, getArchitechBean()));
				model.addObject(BEAN_NOTIFICACION, new BeanNotificacion());
				model.addObject(BEAN_NOTIFICACIONES, notificaciones);
			} else {
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, null, getArchitechBean()));
				model.addObject(POOL,CAPTURA);
				model.addObject(ACCION,ACCION_MODIFICAR);
				model.addObject(BEAN_NOTIFICACION, notificacion);
			}
			/** Seteo de errores a la pantalla*/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	
	/**
	 * Eliminar notificador
	 * 
	 * Mapeo del flujo para iniciar la eliminacion 
	 * de registros del catalogo.
	 *
	 * @param notificaciones --> objeto de tipo beanNotificaciones que elimina notificaciones
	 * @param beanFilter --> objeto de tipo beanFilter que permite realizar filtros
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/eliminarNotificador.do")
    public ModelAndView eliminaNotificador(@ModelAttribute(BEAN_NOTIFICACIONES) BeanNotificaciones notificaciones,
    		@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNotificacion beanFilter, BindingResult bindingResult){
		
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		
		/** Comienza flujo de eliminacion **/
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "";
		String erroneos = "";
		String mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			response = boNotificador.eliminar(notificaciones, getArchitechBean());
			BeanNotificaciones responseConsulta = boNotificador.consultar(beanFilter, new BeanPaginador(), getArchitechBean());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_NOTIFICACIONES, responseConsulta);
			if (Errores.OK00000V.equals(responseConsulta.getBeanError().getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos }) + "\n";
				} else if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje);
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
				}
				mensaje = mensaje.replaceAll("intermediarios", "registros");
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			/** Seteo de errores a la pantalla*/
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			modelAndView.addObject(Constantes.DESC_ERROR, mensaje.trim());
			if (responseConsulta.getTotalReg() == 0) {
				modelAndView.addObject(COMBOS, boNotificador.consultarListas(TIPO_FILTRO, new BeanNotificacion(), getArchitechBean()));
			} else {
				modelAndView.addObject(COMBOS, boNotificador.consultarListas(TIPO_FILTRO, beanFilter, getArchitechBean()));
			}
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return modelAndView;
    }
	
	/**
	 * Exportar notificador.
	 * 
	 * Mapeo del metodo para descargar la infomacion de la pantalla actual
	 * mostrada a un archivo de excel.
	 *
	 *  @param beanNotificaciones --> objeto de tipo beanNotificaciones que modifica notificaciones
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value = "/exportarNotificador.do")
	public ModelAndView exportarNotificador(@ModelAttribute(BEAN_NOTIFICACIONES) BeanNotificacionesExportar beanNotificaciones, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utileriasExportar.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utileriasExportar.getBody(beanNotificaciones.getNotificacionesE()));
		modelAndView.addObject("NAME_SHEET", "Notificador de Estatus");
		/** Retorno del metodo **/
		return modelAndView;
	}
	
	/**
	 * Exportar todos notificador
	 * 
	 * Mapeo del metodo para iniciar el flujo de exportar las notificaciones
	 * al proceso de inserccion en BD.
	 *
	 * @param beanNotificacion --> objeto de tipo beanNotificaciones que exporta las notificaciones
	 * @param beanPaginador --> objeto de tipo bean que permite paginar
	 * @param bindingResult --> objeto de tipo bindingResult que muestra los resultados.
	 * @return model --> objeto de tipo ModelAndView que muesta los datos
	 */
	@RequestMapping(value="/exportarTodoNotificador.do")
	public ModelAndView exportarTodoNotificador(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanNotificacion beanNotificacion, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Se crea el model&view a retornar**/
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanNotificaciones responseConsulta = new BeanNotificaciones();
		RequestContext ctx = new RequestContext(req);
		try {
			/** Ejecuta peticion al BO de la consulta **/
			responseConsulta = boNotificador.consultar(beanNotificacion, beanPaginador, getArchitechBean());
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOTIFICACIONES, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boNotificador.exportarTodo(beanNotificacion, responseConsulta.getTotalReg(), getArchitechBean());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_NOTIFICACIONES, responseConsulta);
			model.addObject(COMBOS, boNotificador.consultarListas(TIPO_COMBO, beanNotificacion, getArchitechBean()));
			model.addObject(UtilsConstantes.BEAN_FILTER, beanNotificacion);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del metodo **/
		return model;
	}
	
	/**
	 * Metodo GET boNotificador que obtiene el notificador
	 *
	 * @return boNotificador --> objeto de tipo  boNotificador que obtiene el notificador
	 */
	public BONotificador getBoNotificador() {
		return boNotificador;
	}

	/**
	 * Metodo SET boNotificador que asinga el notificador
	 *
	 * @param boNotificador --> objeto de tipo  boNotificador que asinga el notificador
	 */
	public void setBoNotificador(BONotificador boNotificador) {
		this.boNotificador = boNotificador;
	}
	
}
