/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMonitores.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:38:54 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOMonitores;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasWebMonitor;

/**
 * Class ControllerMonitores.
 *
 * Controler principal de las
 * pantallas de monitores de contingencia
 * de POACOA
 * 
 * @author FSW-Vector
 * @since 6/08/2019
 */
@Controller
public class ControllerMonitores extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7863127133672277625L;

	/** La variable que contiene informacion con respecto a: bo monitores. */
	private BOMonitores boMonitores;

	/** La constante PAGINA_MONITOR. */
	private static final String PAGINA_MONITOR = "monitor";

	private static final String BEAN_MONITOR = "beanMonitor";

	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;

	/**
	 * Muestra monitor.
	 * 
	 * Metodo para recibir la
	 * peticion de los monitores 
	 * POACOA SPEI y SPID
	 *
	 * @param modulo El objeto: modulo
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = { "/monitorContingentePOASPEI.do", "/monitorContingenteCOASPEI.do",
			"/monitorContingentePOASPID.do", "/monitorContingenteCOASPID.do" })
	public ModelAndView muestraMonitor(@Valid @ModelAttribute("modulo") BeanModulo modulo, BindingResult bindingResult) {
		ModelAndView model = null;
		try {
			/** Inicializacion del modelo **/
			model = new ModelAndView(PAGINA_MONITOR);
			/** Validacion de los datos de entrada **/
			if (bindingResult.hasErrors()) {
				error(ConstantesWebPOACOA.ERROR_BINDING);
			}
			/** Recuperar atributos usando el context **/
			HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			BeanModulo beanModulo = UtileriasWebMonitor.cargaModulo(req);
			BeanMonitor response = boMonitores.obtenerMonitor(beanModulo, getArchitechBean());
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+response.getError().getCodError(), null, null);
			model.addObject(Constantes.COD_ERROR, response.getError().getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getError().getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(BEAN_MONITOR, response);
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}

	/**
	 * Obtener el objeto: bo monitores.
	 *
	 * @return El objeto: bo monitores
	 */
	public BOMonitores getBoMonitores() {
		return boMonitores;
	}

	/**
	 * Definir el objeto: bo monitores.
	 *
	 * @param boMonitores El nuevo objeto: bo monitores
	 */
	public void setBoMonitores(BOMonitores boMonitores) {
		this.boMonitores = boMonitores;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
