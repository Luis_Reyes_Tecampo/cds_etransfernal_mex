/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 11:14:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;
import mx.isban.eTransferNal.servicio.moduloCDA.BOGenerarArchCDAHist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;


/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * las peticiones para la funcionalidad de Generar Archivo CDA Historico
**/
@Controller
public class ControllerGenerarArchCDAHist extends Architech  {
	
	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;

	/**Referencia privada del servicio bOGenerarArchCDAHist*/
	private BOGenerarArchCDAHist bOGenerarArchCDAHist;
	
   /**
    * Metodo que muestra la generacion de Archivo CDA historico
    * @param req Objeto del tipo HttpServletRequests
    * @return ModelAndView Objeto del tipo ModelAndView
    */
   @RequestMapping(value = "/muestraGenerarArchCDAHist.do")
   public ModelAndView muestraGenerarArchCDAHist(HttpServletRequest req){
      ModelAndView modelAndView = null;
      RequestContext ctx = new RequestContext(req);
      BeanResConsFechaOp beanResConsFechaOp = null;
      try{
    	  beanResConsFechaOp = bOGenerarArchCDAHist.consultaFechaOperacion(getArchitechBean());
    	  modelAndView = new ModelAndView("generarArchivoCDAHist","generarArchCDAHist",beanResConsFechaOp);
    	  String mensaje = ctx.getMessage("codError."+beanResConsFechaOp.getCodError());
     	  modelAndView.addObject("codError", beanResConsFechaOp.getCodError());
     	  modelAndView.addObject("tipoError", beanResConsFechaOp.getTipoError());
     	  modelAndView.addObject("descError", mensaje);
      }catch(BusinessException e){
    	  showException(e);
      }
      return modelAndView;
   }
   /**
   * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
   * @param req Objeto del tipo HttpServletRequests
   * @return ModelAndView Objeto del tipo ModelAndView
   */
   @RequestMapping(value = "/procesarGenerarArchCDAHist.do")
   public ModelAndView procesarGenerarArchCDAHist(BeanReqGenArchCDAHist beanReqGenArchCDAHist,HttpServletRequest req){
      ModelAndView modelAndView = null;
      BeanResProcGenArchCDAHist beanResProcGenArchCDAHist = null;
      RequestContext ctx = new RequestContext(req);
      try{
    	  beanResProcGenArchCDAHist = bOGenerarArchCDAHist.procesarGenArchCDAHist(beanReqGenArchCDAHist,getArchitechBean());
    	  modelAndView = new ModelAndView("generarArchivoCDAHist","generarArchCDAHist",beanResProcGenArchCDAHist);
    	  String mensaje = ctx.getMessage("codError."+beanResProcGenArchCDAHist.getCodError());
     	  modelAndView.addObject("codError", beanResProcGenArchCDAHist.getCodError());
     	  modelAndView.addObject("tipoError", beanResProcGenArchCDAHist.getTipoError());
     	  modelAndView.addObject("descError", mensaje);
      }catch(BusinessException e){
    	  showException(e);
      }
     
      return modelAndView;
   }
   /**
    * Metodo get del Servicio BOGenerarArchCDAHist
    * @return BOGenerarArchCDAHist
    */
   public BOGenerarArchCDAHist getBOGenerarArchCDAHist() {
	return bOGenerarArchCDAHist;
   }
   /**
    * Metodo set que modifica la referencia al servicio BOGenerarArchCDAHist
    * @param bOGenerarArchCDAHist Objeto del tipo BOGenerarArchCDAHist
    */
   public void setBOGenerarArchCDAHist(BOGenerarArchCDAHist bOGenerarArchCDAHist) {
	   this.bOGenerarArchCDAHist = bOGenerarArchCDAHist;
   }



}
