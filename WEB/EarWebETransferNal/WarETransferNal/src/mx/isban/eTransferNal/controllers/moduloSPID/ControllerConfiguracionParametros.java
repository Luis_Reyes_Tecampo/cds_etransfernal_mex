/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerConfiguracionParametros.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametros;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOConfiguracionParametros;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador para la configuracion de parametros.
 *
 * @author IDS
 */
@Controller
public class ControllerConfiguracionParametros extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5929883757870028945L;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La variable que contiene informacion con respecto a: session SPID. */
	@Autowired
	private BeanSessionSPID sessionSPIDG;
	
	/** Propiedad del tipo MessageSource que almacena el valor de messageSource. */
	private MessageSource messageSource;
	
	/** La variable que contiene informacion con respecto a: b O configuracion parametros. */
	private BOConfiguracionParametros bOConfiguracionParametros;
	
	/** propiedad privada del tipo String. */
	private static final String NCONFIGURACIONPARAMETROS = "configuracionParametros";
	
	/**
	 * Consulta configuracion parametros.
	 *
	 * @param nomPantalla El objeto: nom pantalla
	 * @return ModelAndView
	 */
	@RequestMapping(value="/configuracionParametros.do")
	public ModelAndView consultaConfiguracionParametros(@RequestParam("nomPantalla") String nomPantalla){
		BeanResConfiguracionParametros beanResConfiguracionParametros = null;
		ModelAndView model = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			//Obtencion de bean resultado que sera enviado a la vista (JSP)
			beanResConfiguracionParametros = bOConfiguracionParametros.obtenerConfiguracionParametros(Long.parseLong(sessionSPID.getCveInstitucion()), getArchitechBean(), nomPantalla);

			//Nombramiento y puesta de bean que sera enviado a la vista
			model = new ModelAndView(NCONFIGURACIONPARAMETROS,NCONFIGURACIONPARAMETROS, beanResConfiguracionParametros.getConfiguracionParametros());
			
			//Mensajes de error, se envia tambien cuando es exitosa la operacion
			model.addObject(Constantes.COD_ERROR, beanResConfiguracionParametros.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResConfiguracionParametros.getTipoError());
			model.addObject(Constantes.DESC_ERROR, beanResConfiguracionParametros.getCodError());
			model.addObject("nomPantalla", nomPantalla);
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException e) {
			showException(e);
		}		
		return model;
	}
	
	/**
	 * Guardar configuracion parametros.
	 *
	 * @param configuracionParametros Objeto del tipo BeanConfiguracionParametros
	 * @param nomPantalla El objeto: nom pantalla
	 * @return ModelAndView
	 */
	@RequestMapping(value="/guardarConfiguracionParametros.do")
	public ModelAndView guardarConfiguracionParametros(BeanConfiguracionParametros configuracionParametros, @RequestParam("nomPantalla") String nomPantalla) {
		BeanResConfiguracionParametros beanResConfiguracionParametros = null;
		ModelAndView model = new ModelAndView(NCONFIGURACIONPARAMETROS);
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		try {
			beanResConfiguracionParametros = bOConfiguracionParametros.guardarConfiguracionParametros(configuracionParametros, getArchitechBean(), nomPantalla);
			if (beanResConfiguracionParametros != null && beanResConfiguracionParametros.getConfiguracionParametros() != null) {
				model.addObject(NCONFIGURACIONPARAMETROS,beanResConfiguracionParametros.getConfiguracionParametros());
				model.addObject(Constantes.COD_ERROR, beanResConfiguracionParametros.getCodError());
		     	model.addObject(Constantes.TIPO_ERROR, beanResConfiguracionParametros.getTipoError());
		     	model.addObject(Constantes.DESC_ERROR, beanResConfiguracionParametros.getCodError());
			} else {
				model.addObject(NCONFIGURACIONPARAMETROS, new BeanConfiguracionParametros());
				model.addObject(Constantes.COD_ERROR, Errores.EC00011B);
		     	model.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
		     	model.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
			}
	     	
	     	model.addObject("nomPantalla", nomPantalla);
	     	model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * Obtener el objeto: BO configuracion parametros.
	 *
	 * @return BOConfiguracionParametros
	 */
	public BOConfiguracionParametros getBOConfiguracionParametros() {
		return bOConfiguracionParametros;
	}

	/**
	 * Definir el objeto: BO configuracion parametros.
	 *
	 * @param bOConfiguracionParametros Objeto del tipo BOConfiguracionParametros
	 */
	public void setBOConfiguracionParametros(BOConfiguracionParametros bOConfiguracionParametros) {
		this.bOConfiguracionParametros = bOConfiguracionParametros;
	}
	
	/**
	 * Obtener el objeto: session SPID.
	 *
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPIDG;
	}
	
	/**
	 * Definir el objeto: session SPID.
	 *
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPIDG = sessionSPID;
	}
	
	/**
	 * Obtener el objeto: message source.
	 *
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
