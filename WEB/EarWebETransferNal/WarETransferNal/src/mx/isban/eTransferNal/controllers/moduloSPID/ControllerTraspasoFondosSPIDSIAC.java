package mx.isban.eTransferNal.controllers.moduloSPID;

import java.util.HashMap;
import java.util.Map;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BORecepOperacionSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.utilerias.comunes.ValidaCadenas;
import mx.isban.eTransferNal.utilerias.comunes.Validate;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerTraspasoFondosSPIDSIAC extends Architech{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5056360021955227355L;
	 
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE_RECEPCION_OP
	 */
	private static final String PAGE = "traspasoFondosSPIDSIAC";
	
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	private static final String BEAN_RES = "beanRes";
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * Propiedad del tipo BORecepOperacion que almacena el valor de bORecepOperacion
	 */
	private BORecepOperacionSPID bORecepOperacionSPID;
	
	/**
	 * Propiedad del tipo BOTraspasoFondosSPIDSIAC que almacena el valor de bOTraspasoFondosSPIDSIAC
	 */
	private BOTraspasoFondosSPIDSIAC bOTraspasoFondosSPIDSIAC;
	
	/**
	  * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraTraspasoFondosSPID.do")
	 public ModelAndView muestraTraspasoFondos() {
		 BeanResReceptoresSPID beanResReceptoresSPID = bORecepOperacionSPID.consultaCveMiInstFchOp(getArchitechBean());
		return new ModelAndView(PAGE,BEAN_RES,beanResReceptoresSPID);
	 }
	 
	/**
	 * Metodo que se utiliza para mostrar la pagina de recepcion de operaciones SPID
	 * @param req Bean del tipo BeanReqTraspasoFondosSPIDSIAC 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	 @RequestMapping(value = "/traspasoFondosSPIDSIAC.do")
	 public ModelAndView traspasoFondosSPIDSIAC(BeanReqTraspasoFondosSPIDSIAC req) {
		 ValidaCadenas validaCad = new ValidaCadenas();
		 String mensaje ="";
		 Map<String, Object> model = new HashMap<String, Object>();
		Validate valida = new Validate(); 
		if(!valida.esValidoImporteMaximoMayor0(req.getImporte())){
			mensaje = messageSource.getMessage("codError."+Errores.ED00092V
					, new String[]{"Importe"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
			model.put(Constantes.DESC_ERROR, mensaje);
			return new ModelAndView(PAGE, model);
		}	
		if(!validaCad.esValidoCadena(req.getComentario(), 60)){
			mensaje = messageSource.getMessage("codError.ED00092V", new String[]{"Comentario"}, null);
			model.put(Constantes.COD_ERROR, Errores.ED00092V);
			model.put(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ALERT);
			model.put(Constantes.DESC_ERROR, mensaje);
			return new ModelAndView(PAGE, model);
		}
		req.setImporte(req.getImporte().replaceAll(",", ""));
		 BeanResTraspasoFondosSPIDSIAC beanResTraspasoFondosSPIDSIAC = bOTraspasoFondosSPIDSIAC.traspasoFondosSPIDSIAC(req, getArchitechBean());
		 if("TRIB0000".equals(beanResTraspasoFondosSPIDSIAC.getCodError())){
			 mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResTraspasoFondosSPIDSIAC.getCodError(),
					 new String[]{beanResTraspasoFondosSPIDSIAC.getReferencia(),beanResTraspasoFondosSPIDSIAC.getEstatus()},null);
			 model.put(Constantes.COD_ERROR, beanResTraspasoFondosSPIDSIAC.getCodError());
			 model.put("tipoError", Errores.TIPO_MSJ_INFO);
			 model.put(Constantes.DESC_ERROR, mensaje);
		 }else{
			 mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResTraspasoFondosSPIDSIAC.getCodError(),
					 new String[]{beanResTraspasoFondosSPIDSIAC.getReferencia(),beanResTraspasoFondosSPIDSIAC.getEstatus()},null);
			 model.put(Constantes.COD_ERROR, beanResTraspasoFondosSPIDSIAC.getCodError());
			 model.put(Constantes.TIPO_ERROR, beanResTraspasoFondosSPIDSIAC.getTipoError());
			 model.put(Constantes.DESC_ERROR, mensaje);
		 }
		 return new ModelAndView(PAGE,model);
	 }

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource
	 * @return messageSource Objeto del tipo MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource
	 * @param messageSource del tipo MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bORecepOperacionSPID
	 * @return bORecepOperacionSPID Objeto del tipo BORecepOperacionSPID
	 */
	public BORecepOperacionSPID getBORecepOperacionSPID() {
		return bORecepOperacionSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bORecepOperacionSPID
	 * @param recepOperacionSPID del tipo BORecepOperacionSPID
	 */
	public void setBORecepOperacionSPID(BORecepOperacionSPID recepOperacionSPID) {
		bORecepOperacionSPID = recepOperacionSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bOTraspasoFondosSPIDSIAC
	 * @return bOTraspasoFondosSPIDSIAC Objeto del tipo BOTraspasoFondosSPIDSIAC
	 */
	public BOTraspasoFondosSPIDSIAC getBOTraspasoFondosSPIDSIAC() {
		return bOTraspasoFondosSPIDSIAC;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bOTraspasoFondosSPIDSIAC
	 * @param traspasoFondosSPIDSIAC del tipo BOTraspasoFondosSPIDSIAC
	 */
	public void setBOTraspasoFondosSPIDSIAC(
			BOTraspasoFondosSPIDSIAC traspasoFondosSPIDSIAC) {
		bOTraspasoFondosSPIDSIAC = traspasoFondosSPIDSIAC;
	}

}
