/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerDetallePagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:32 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOPagos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;

/**
 * Class ControllerDetallePagos.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * detalle de pagos y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerDetallePagos extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4906730840033386283L;
	
	/** La constante PANTALLA_DETALLE. */
	private static final String PANTALLA_DETALLE = "detallePagoPC";
	
	/** La constante BEAN_DETALLE. */
	private static final String BEAN_DETALLE = "beanDetallePago";
	
	/** La variable que contiene informacion con respecto a: bo pagos. */
	private BOPagos boPagos;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La constante TIPO_ORDEN. */
	private static final String TIPO_ORDEN = "tipoOrden";
	
	
	/**
	 * Consulta detalle.
	 *
	 * @param tipoOrden El objeto: tipo orden
	 * @param referencia El objeto: referencia
	 * @param beanModulo El objeto: bean modulo
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaDetallePago.do") 
	public ModelAndView consultaDetalle(@Valid @RequestParam("tipoRef") String tipoOrden, @RequestParam("ref") String referencia,
			@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo, BindingResult bindingResult) {
		ModelAndView model = null;
		BeanDetallePago beanDetallePago = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			beanDetallePago = boPagos.consultaDetallePago(beanModulo, referencia, getArchitechBean());
			model = new ModelAndView(PANTALLA_DETALLE);
			model.addObject(BEAN_DETALLE, beanDetallePago);
			model.addObject(TIPO_ORDEN, tipoOrden);
			model.addObject("referencia", referencia);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
		} catch(BusinessException e) {
			showException(e);
		}
		return model;
	}
	

	/**
	 * Obtener el objeto: bo pagos.
	 *
	 * @return El objeto: bo pagos
	 */
	public BOPagos getBoPagos() {
		return boPagos;
	}

	/**
	 * Definir el objeto: bo pagos.
	 *
	 * @param boPagos El nuevo objeto: bo pagos
	 */
	public void setBoPagos(BOPagos boPagos) {
		this.boPagos = boPagos;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
	
	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
}
