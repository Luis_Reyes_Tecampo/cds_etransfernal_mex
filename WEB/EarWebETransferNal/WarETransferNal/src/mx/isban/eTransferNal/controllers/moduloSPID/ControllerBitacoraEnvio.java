/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerBitacoraEnvio.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvio;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOBitacoraEnvio;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar la peticion de
 * los parametros
 **/
@Controller
public class ControllerBitacoraEnvio extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -4935841331869243610L;

	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;

	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;

	/**
	 * 
	 */
	private static final String SORT_FIELD = "sortField";

	/**
	 * 
	 */
	private static final String SORT_TYPE = "sortType";

	/**
	 * 
	 */
	private BOBitacoraEnvio bOBitacoraEnvio;

	/**
	 * Metodo que muestra la pantalla de Bitácora de Envío
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento
	 *            Objeto del tipo BeanOrdenamiento
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/muestraBitacoraDeEnvio.do")
	public ModelAndView consultaBitacoraEnvio(BeanPaginador beanPaginador,
			BeanOrdenamiento beanOrdenamiento) {
		BeanResBitacoraEnvio beanResBitacoraEnvio = null;
		ModelAndView model = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResBitacoraEnvio = bOBitacoraEnvio.obtenerBitacoraEnvio(
					beanPaginador, getArchitechBean(),
					beanOrdenamiento.getSortField(),
					beanOrdenamiento.getSortType());
			model = new ModelAndView("bitacoraEnvios", "beanResBitacoraEnvio",
					beanResBitacoraEnvio);
			
			String mensaje = messageSource.getMessage("codError."
					+ beanResBitacoraEnvio.getCodError(), null, null);

			model.addObject(Constantes.COD_ERROR,
					beanResBitacoraEnvio.getCodError());
			model.addObject(Constantes.TIPO_ERROR,
					beanResBitacoraEnvio.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			generarContadoresPaginacion(beanResBitacoraEnvio, model);
		} catch (BusinessException e) {
			showException(e);
		}

		return model;
	}

	/**
	 * Metodo que exporta los registros que se muestran en el grid de la
	 * Bitácora de Envío
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo beanPaginador
	 * @param beanOrdenamiento
	 *            Objeto del tipo BeanOrdenamiento
	 * @param req
	 *            Objeto del tipo HttpServletRequest
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarBitacoraEnvio.do")
	public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador,
			BeanOrdenamiento beanOrdenamiento, HttpServletRequest req) {
		FormatCell formatCellHeaderBE = null;
		FormatCell formatCellBodyBE = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		BeanResBitacoraEnvio beanResBitacoraEnvio = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResBitacoraEnvio = bOBitacoraEnvio.obtenerBitacoraEnvio(
					beanPaginador, getArchitechBean(),
					beanOrdenamiento.getSortField(),
					beanOrdenamiento.getSortType());

			if (Errores.OK00000V.equals(beanResBitacoraEnvio.getCodError())
					|| Errores.ED00011V.equals(beanResBitacoraEnvio
							.getCodError())) {
				formatCellBodyBE = new FormatCell();
				formatCellBodyBE.setFontName("Calibri");
				formatCellBodyBE.setFontSize((short) 11);
				formatCellBodyBE.setBold(false);

				formatCellHeaderBE = new FormatCell();
				formatCellHeaderBE.setFontName("Calibri");
				formatCellHeaderBE.setFontSize((short) 11);
				formatCellHeaderBE.setBold(true);

				model = new ModelAndView(new ViewExcel());
				model.addObject("FORMAT_HEADER", formatCellHeaderBE);
				model.addObject("FORMAT_CELL", formatCellBodyBE);
				model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
				model.addObject("BODY_VALUE",
						getBody(beanResBitacoraEnvio.getListaBitacoraEnvio()));
				model.addObject("NAME_SHEET", "OrdenBitEnvios");
			} else {
				model = consultaBitacoraEnvio(beanPaginador, beanOrdenamiento);
				String mensaje = messageSource.getMessage("codError."
						+ beanResBitacoraEnvio.getCodError(), null, null);
				model.addObject("codError", beanResBitacoraEnvio.getCodError());
				model.addObject("tipoError",
						beanResBitacoraEnvio.getTipoError());
				model.addObject("descError", mensaje);
				model.addObject("sessionSPID", sessionSPID);
				model.addObject(SORT_FIELD, beanOrdenamiento.getSortField());
				model.addObject(SORT_TYPE, beanOrdenamiento.getSortType());
			}
		} catch (BusinessException e) {
			showException(e);
		}

		return model;
	}

	/**
	 * Metodo que exporta todos los registros existentes de la Bitácora de
	 * Envío
	 * 
	 * @param beanReqBitacoraEnvio
	 *            Objeto del tipo BeanReqBitacoraEnvio
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarTodoBitacoraEnvio.do")
	public ModelAndView expTodosBitacoraAdmon(
			BeanReqBitacoraEnvio beanReqBitacoraEnvio) {
		ModelAndView model = null;
		BeanResBitacoraEnvio beanResBitacoraEnvio = null;

		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}

		try {
			beanResBitacoraEnvio = bOBitacoraEnvio.exportarTodoBitacoraEnvio(
					beanReqBitacoraEnvio, getArchitechBean());
			model = new ModelAndView("bitacoraEnvios");

			String mensaje = messageSource.getMessage("codError."
					+ beanResBitacoraEnvio.getCodError(), new Object[]{beanResBitacoraEnvio.getNombreArchivo()}, null);
			
			model.addObject("beanResBitacoraEnvio", beanResBitacoraEnvio);
			model.addObject(Constantes.COD_ERROR,
					beanResBitacoraEnvio.getCodError());
			model.addObject(Constantes.TIPO_ERROR,
					beanResBitacoraEnvio.getTipoError());
			model.addObject(Constantes.DESC_ERROR,
					mensaje);
			model.addObject("sessionSPID", sessionSPID);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}

	/**
	 * Metodo que obtiene los datos desde el bean y los envia a la vista para
	 * ser mostrados
	 * 
	 * @param listaBeanBitacoraEnvio
	 *            Objeto del tipo List<BeanBitacoraEnvio>
	 * @return List<List<Object>>
	 */
	private List<List<Object>> getBody(
			List<BeanBitacoraEnvio> listaBeanBitacoraEnvio) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listaBeanBitacoraEnvio != null && !listaBeanBitacoraEnvio.isEmpty()) {
			objList = new ArrayList<List<Object>>();

			for (BeanBitacoraEnvio beanBitacoraEnvio : listaBeanBitacoraEnvio) {
				objListParam = new ArrayList<Object>();
				objListParam.add(beanBitacoraEnvio.getFolioSolicitud());
				objListParam.add(beanBitacoraEnvio.getTipoDeMensaje());
				objListParam.add(beanBitacoraEnvio.getFechaHoraEnvio());
				objListParam.add(beanBitacoraEnvio.getNumeroPagos());
				objListParam.add(beanBitacoraEnvio.getImporteAbono());
				objListParam.add(beanBitacoraEnvio.getCodBanco());
				objListParam.add(beanBitacoraEnvio.getNumReprocesos());
				objListParam.add(beanBitacoraEnvio.getAcuseRecibo());
				objListParam.add(beanBitacoraEnvio.getBytesAcumulados());
				objListParam.add(beanBitacoraEnvio.getOperacionCert());
				objListParam.add(beanBitacoraEnvio.getNumCertificado());
				objList.add(objListParam);
			}
		}
		return objList;
	}

	/**
	 * Metodo que genera el Header del archivo excel que se exportó
	 * 
	 * @param ctx
	 *            Objeto del tipo RequestContext
	 * @return List<String>
	 */
	private List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.folio"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.tipoMensaje"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.fechaHora"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.numPagos"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.importe"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.banco"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.numReprocesos"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.acuse"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.bytesAcumulados"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.operacionCert"),
				ctx.getMessage("moduloSPID.bitacoraEnvio.text.numCertificado") };
		return Arrays.asList(header);
	}

	/**
	 * 
	 * @param beanResBitEnvio tipo BeanResBitacoraEnvio 
	 * @param model tipo ModelAndView
	 */
	protected void generarContadoresPaginacion(
			BeanResBitacoraEnvio beanResBitEnvio, ModelAndView model) {
		if (beanResBitEnvio.getTotalReg() > 0) {
			Integer regIni = 1;
			Integer regFin = beanResBitEnvio.getTotalReg();

			if (!"1".equals(beanResBitEnvio.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResBitEnvio
						.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResBitEnvio.getTotalReg() >= 20 * Integer
					.parseInt(beanResBitEnvio.getBeanPaginador().getPagina())) {
				regFin = 20 * Integer.parseInt(beanResBitEnvio
						.getBeanPaginador().getPagina());
			}

			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanBitacoraEnvio beanBitacora : beanResBitEnvio
					.getListaBitacoraEnvio()) {
				totalImporte = totalImporte.add(new BigDecimal(beanBitacora
						.getImporteAbono()));
			}
			model.addObject("importePagina", totalImporte.toString());

			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}

	/**
	 * Devuelve el valor de bOBitacoraEnvio
	 * 
	 * @return the bOBitacoraEnvio
	 */
	public BOBitacoraEnvio getBOBitacoraEnvio() {
		return bOBitacoraEnvio;
	}

	/**
	 * Modifica el valor de bOBitacoraEnvio
	 * 
	 * @param bOBitacoraEnvio
	 *            the bOBitacoraEnvio to set
	 */
	public void setBOBitacoraEnvio(BOBitacoraEnvio bOBitacoraEnvio) {
		this.bOBitacoraEnvio = bOBitacoraEnvio;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID
	 *            the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
