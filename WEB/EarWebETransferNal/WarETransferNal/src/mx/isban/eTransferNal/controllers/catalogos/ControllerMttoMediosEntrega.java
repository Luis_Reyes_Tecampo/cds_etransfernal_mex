/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMttoMediosEntrega.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/09/2018 04:39:27 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosEntrega;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtilsExportacionMediosEntrega;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Class ControllerMttoMediosEntrega.
 *
 * @author FSW-Vector
 * @since 28/09/2018
 */
@Controller
public class ControllerMttoMediosEntrega extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 157352962534854918L;
	
	/** La constante UTILERIAS. */
	private static final UtilsExportacionMediosEntrega UTILERIAS = UtilsExportacionMediosEntrega.getUtilerias();
	
	/** La constante PAGINA_CONSULTA. */
	private static final String PAGINA_CONSULTA = "consultaMttoMediosEntrega";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaMediosEntrega";
	
	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante BEAN_FILTER. */
	private static final String BEAN_FILTER = "beanFilter";
	
	/** La constante BEAN_MEDIOS_ENTREGA. */
	private static final String BEAN_MEDIOS_ENTREGA = "beanMediosEntrega";
	
	/** La constante BEAN_MEDIO_ENTREGA. */
	private static final String BEAN_MEDIO_ENTREGA = "beanMedioEntrega";
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La variable que contiene informacion con respecto a: bo Mantenimiento Entrega. */
	private BOMttoMediosEntrega boMttoMediosEntrega;
	
	
	/**
	 * Muestra consulta inicial de Medios de entrega.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @return Objeto model and view
	 */
	@RequestMapping(value="/consultaMttoMediosEntrega.do")
	public ModelAndView muestraMttoEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_MEDIO_ENTREGA) BeanMedioEntrega beanMedio) {
		//Se crea el model&view a retornar
		ModelAndView model = new ModelAndView();
		BeanMediosEntrega response = null;
		try {
			//Se realiza la consulta
			response = boMttoMediosEntrega.consultar(getArchitechBean(), beanMedio, beanPaginador);
			// Seteamos los datos al Model para pasarlos a la Vista
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_MEDIOS_ENTREGA, response);
			model.addObject(BEAN_MEDIO_ENTREGA, beanMedio);
			if(!Errores.OK00000V.equals(response.getBeanError().getCodError())){
				model.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
				model.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
				model.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	
	/**
	 * Agregar medio entrega.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/agregarMedioEntrega.do")
	public ModelAndView agregarMedioEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_MEDIO_ENTREGA) BeanMedioEntrega beanMedio) {
		//Se crea el model&view a retornar
		ModelAndView model = null;
		BeanResBase response = null;
		RequestContext ctx = new RequestContext(req);
	    String mensaje = "";
		try {
			//Se conusmen el servicio de alta de Medio de Entrega
			response = boMttoMediosEntrega.agregarMedioEntrega(getArchitechBean(), beanMedio);
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError());
			if (Errores.OK00003V.equals(response.getCodError())) {
				//se consulta 
				BeanMediosEntrega responseConsulta = boMttoMediosEntrega.consultar(getArchitechBean(), new BeanMedioEntrega(), new BeanPaginador());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_MEDIOS_ENTREGA, responseConsulta);
				model.addObject(BEAN_MEDIO_ENTREGA, new BeanMedioEntrega());
			} else {
				//Se consultan los combos
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION, ACCION_ALTA);
				model.addObject(BEAN_MEDIOS_ENTREGA, beanMedio);
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje.replaceAll("Mantenimiento Medios Autorizados", "Medios de Entrega"));
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	/**
	 * Mantenimiento de Medios de Entrega.
	 * 
	 * Muestar la pantalla de Agregar o Modificar Certificado. 
	 * Si la lista contiene una seleccion se cargan los datos en el Bean 
	 * y se retorna el model&view
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanMediosEntrega El objeto: bean medios entrega
	 * @return Objeto model and view
	 * @throws BusinessException La business exception
	 */
	@RequestMapping(value = "/mantenimientoMediosEntrega.do")
	public ModelAndView mantenimientoMediosEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_MEDIOS_ENTREGA) BeanMediosEntrega beanMediosEntrega) throws BusinessException {
		//Se crea el model&view a retornar
		
		ModelAndView model = null;
		BeanMedioEntrega beanReq = new BeanMedioEntrega();
		if (beanMediosEntrega != null && beanMediosEntrega.getMediosEntrega() != null) {
			for(BeanMedioEntrega medioEntrega : beanMediosEntrega.getMediosEntrega()) {
				//se valida que haya una seleccion a dar mantenimiento
				if(medioEntrega.isSeleccionado()) {
					beanReq = medioEntrega;
				}
			}			
		}
		model = new ModelAndView(PAGINA_ALTA);
		model.addObject(ACCION, beanReq.getCveMedioEntrega() != null ? ACCION_MODIFICAR : ACCION_ALTA);
		model.addObject(BEAN_MEDIO_ENTREGA, beanReq);
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
	/**
	 * Editar medio de entrega.
	 *
	 * @param req El objeto: req
	 * @param servletResponse El objeto: servlet response
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/editarMedioEntrega.do")
	public ModelAndView editarMedioEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_MEDIO_ENTREGA) BeanMedioEntrega beanMedio) {
		//Se crea el model&view a retornar
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
	    String mensaje = "";
		try {
			//Se conusmen el servicio de edicion de certificado
			BeanResBase response = boMttoMediosEntrega.editarMedio(getArchitechBean(), beanMedio);
			if (Errores.OK00003V.equals(response.getCodError())) {
				//se consulta 
				BeanMediosEntrega responseConsulta = boMttoMediosEntrega.consultar(getArchitechBean(), new BeanMedioEntrega(), new BeanPaginador());
				model = new ModelAndView(PAGINA_CONSULTA);
				model.addObject(BEAN_MEDIOS_ENTREGA, responseConsulta);
				model.addObject(BEAN_MEDIO_ENTREGA, new BeanMedioEntrega());
			} else {
				//Se consultan los combos
				model = new ModelAndView(PAGINA_ALTA);
				model.addObject(ACCION, ACCION_MODIFICAR);
				model.addObject(BEAN_MEDIO_ENTREGA, beanMedio);
			}
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+response.getCodError());
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}
	
    /**
     * Eliminar medio entrega.
     *
     * @param req El objeto: req
     * @param servletResponse El objeto: servlet response
     * @param beanMedios El objeto: bean medios
     * @return Objeto model and view
     */
    @RequestMapping(value = "/eliminarMedioEntrega.do")
    public ModelAndView eliminarMedioEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
    		@ModelAttribute(BEAN_MEDIOS_ENTREGA) BeanMediosEntrega beanMedios){
    	//Se crea el model&view a retornar
		ModelAndView modelAndView = null;
		StringBuilder strBuilder = new StringBuilder();
		String correctos = "", erroneos = "", mensaje = "";
		RequestContext ctx = new RequestContext(req);
		BeanEliminarResponse response;
		try {
			//Se ejecuta la eliminacion de los registros seleccionados
			response = boMttoMediosEntrega.eliminarMedios(getArchitechBean(), beanMedios);
			//Se ejecuta la consulta inicial
			BeanMediosEntrega responseConsulta = boMttoMediosEntrega.consultar(getArchitechBean(),
					new BeanMedioEntrega(), new BeanPaginador());
			modelAndView = new ModelAndView(PAGINA_CONSULTA, BEAN_MEDIOS_ENTREGA, responseConsulta);
			if (Errores.OK00000V.equals(response.getCodError())) {
				modelAndView.addObject("paginador", responseConsulta.getBeanPaginador());
				correctos = response.getEliminadosCorrectos();
				erroneos = response.getEliminadosErroneos();
				if (!StringUtils.EMPTY.equals(correctos)) {
					correctos = correctos.substring(0, correctos.length() - 1);
					mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + "OK00020V", new String[] { correctos });
				} if (!StringUtils.EMPTY.equals(erroneos)) {
					erroneos = erroneos.substring(0, erroneos.length() - 1);
					strBuilder.append(mensaje.equals(StringUtils.EMPTY) ? StringUtils.EMPTY : ". ");
					strBuilder.append(ctx.getMessage(Constantes.COD_ERROR_PUNTO + "CD00171V", new String[] { erroneos }));
					mensaje += strBuilder.toString();
				}
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje.replaceAll("intermediarios", "Medios de Entrega"));
			} else {
				mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError());
				modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
			}
			modelAndView.addObject(Constantes.COD_ERROR, response.getCodError());
			modelAndView.addObject(Constantes.TIPO_ERROR, response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return modelAndView;
    }
    
    /**
     * Exportar medios entrega.
     *
     * @param req Objeto del tipo HttpServletRequest
     * @param servletResponse Objeto del tipo HttpServletResponse
     * @param beanPaginador El objeto: bean paginador
     * @param beanFilter El objeto: bean filter
     * @return ModelAndView Objeto del tipo ModelAndView
     */
	@RequestMapping(value = "/exportarMediosEntrega.do")
	public ModelAndView exportarMediosEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_MEDIOS_ENTREGA) BeanMediosEntrega beanMedios) {
		//Se crea el model&view a retornar
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", UTILERIAS.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", UTILERIAS.getBody(beanMedios.getMediosEntrega()));
		modelAndView.addObject("NAME_SHEET", "Medios de Entrega");
			
		
		// Seteamos los datos al Model para pasarlos a la Vista
		return modelAndView;
	}
	
	/**
	 * Exportar todos medios entrega.
	 *
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param req El objeto: req
	 * @return Objeto model and view
	 */
	@RequestMapping(value="/exportarTodosMediosEntrega.do")
	public ModelAndView exportarTodosMediosEntrega(HttpServletRequest req, HttpServletResponse servletResponse,
			@ModelAttribute(BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(BEAN_FILTER) BeanMedioEntrega beanFilter) {
		//Se crea el model&view a retornar
		ModelAndView model = new ModelAndView();
		BeanResBase response;
		BeanMediosEntrega responseConsulta = new BeanMediosEntrega();
		RequestContext ctx = new RequestContext(req);
		try {
			responseConsulta = boMttoMediosEntrega.consultar(getArchitechBean(), beanFilter, beanPaginador);
			if (Errores.ED00011V.equals(responseConsulta.getBeanError().getCodError()) || Errores.EC00011B.equals(responseConsulta.getBeanError().getCodError())) {
				model = new ModelAndView(PAGINA_CONSULTA, BEAN_MEDIOS_ENTREGA, responseConsulta);
				model.addObject(Constantes.COD_ERROR, responseConsulta.getBeanError().getCodError());
				model.addObject(Constantes.DESC_ERROR, responseConsulta.getBeanError().getMsgError());
				model.addObject(Constantes.TIPO_ERROR, responseConsulta.getBeanError().getTipoError());
				return model;
			}
			response = boMttoMediosEntrega.exportarTodo(getArchitechBean(), beanFilter, responseConsulta.getTotalReg());
			model = new ModelAndView(PAGINA_CONSULTA, BEAN_MEDIOS_ENTREGA, responseConsulta);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
		
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());				
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
		} catch (BusinessException e) {
			showException(e);
		}
		// Seteamos los datos al Model para pasarlos a la Vista
		return model;
	}


	/**
	 * Obtener el objeto: bo mtto medios entrega.
	 *
	 * @return El objeto: bo mtto medios entrega
	 */
	public BOMttoMediosEntrega getBoMttoMediosEntrega() {
		return boMttoMediosEntrega;
	}


	/**
	 * Definir el objeto: bo mtto medios entrega.
	 *
	 * @param boMttoMediosEntrega El nuevo objeto: bo mtto medios entrega
	 */
	public void setBoMttoMediosEntrega(BOMttoMediosEntrega boMttoMediosEntrega) {
		this.boMttoMediosEntrega = boMttoMediosEntrega;
	}
	

}
