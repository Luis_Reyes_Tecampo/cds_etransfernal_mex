/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:32 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOPagos;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasWebPagos;

/**
 * Class ControllerPagos.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * pagos y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerPagos extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4906730840033386283L;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "pagos";
	
	/** La constante BEAN_PAGOS. */
	private static final String BEAN_PAGOS = "beanResListadoPagos";
	
	/** La constante TITULO. */
	private static final String TITULO = "titulo";
	
	/** La constante CONST_REF. */
	private static final String CONST_REF = "refModel";
	
	/** La variable que contiene informacion con respecto a: bo pagos. */
	private BOPagos boPagos;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La constante TIPO_ORDEN. */
	private static final String TIPO_ORDEN = "tipoOrden";
	
	/** La constante utils. */
	private static final UtileriasWebPagos utils = UtileriasWebPagos.getUtils();
	
	/**
	 * Consulta avisos traspasasos.
	 * 
	 * Muestra la pantalla con la informacion
	 * a la que hace referencia esta funcionbilidad.
	 *
	 * @param beanModulo El objeto: bean modulo
	 * @param beanPaginador El objeto: bean paginador
	 * @param beanFilter El objeto: bean filter
	 * @param tipoOrden El objeto: tipo orden
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaPagos.do")
	public ModelAndView consultaAvisosTraspasasos(@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) 
			BeanModulo beanModulo,
			BeanPaginador beanPaginador,
			BeanFilter beanFilter,
			BindingResult bindingResult,
			@RequestParam("tipoRef") String tipoOrden
			) {
		ModelAndView model = null;
		BeanResListadoPagos beanResListadoPagos = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
  	    if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Consulta de la session **/
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			
			/** Valdiacion de los filtros y 
			 * ejecucion de la peticion al BO **/
			if (!utils.fieldIsEmpty(beanFilter.getField()) && !utils.valorIsEmpty(beanFilter.getFieldValue())){
				beanResListadoPagos = boPagos.buscarPagos(beanModulo,beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, beanPaginador, getArchitechBean());
			} else{
				beanResListadoPagos = boPagos.obtenerListadoPagos(beanModulo,tipoOrden, beanPaginador, getArchitechBean());
			}
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO+beanResListadoPagos.getCodError(), null, null);
			model = new ModelAndView(PAGINA_PRINCIPAL);
			/** Seteo de datos al modelo **/
			model.addObject(TITULO, utils.obtenerTitulo(tipoOrden, beanModulo.getModulo()));
			model.addObject(BEAN_PAGOS, beanResListadoPagos);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(CONST_REF, tipoOrden);
			model.addObject(TIPO_ORDEN, tipoOrden);
			model.addObject("field", beanFilter.getField());
			model.addObject("fieldValue", beanFilter.getFieldValue());
			model.addObject(Constantes.COD_ERROR, beanResListadoPagos.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResListadoPagos.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			
			if (!utils.fieldIsEmpty(beanFilter.getField()) && !utils.valorIsEmpty(beanFilter.getFieldValue())){
				generarContadoresPag1(beanModulo, model, beanResListadoPagos, beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, getArchitechBean());
			} else{
				generarContadoresPag2(beanModulo, model, beanResListadoPagos, tipoOrden, getArchitechBean());
			}
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno de la respuesta obtenida **/
		return model;
	}
	
	
	/**
	 * Generar contadores pag 1.
	 *
	 * Metodo para validar la paginacion de la consulta obtenida.
	 * 
	 * @param modulo El objeto: modulo
	 * @param modelAndView El objeto: model and view
	 * @param beanResListadoPagos El objeto: bean res listado pagos
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param tipoOrden El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 */
	protected void generarContadoresPag1(BeanModulo modulo, ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos, String field, String valor, String tipoOrden, ArchitechSessionBean architechSessionBean) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIniPagos = 1;
			 Integer regFinPagos = beanResListadoPagos.getTotalReg();
			
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIniPagos = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFinPagos = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImpPaginaPagos = BigDecimal.ZERO;
			BigDecimal totalImportePagos = BigDecimal.ZERO;
			
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImpPaginaPagos = totalImpPaginaPagos.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			try {
				totalImportePagos = boPagos.buscarPagosMonto(modulo, field, valor, tipoOrden, getArchitechBean());
			} catch (BusinessException e) {
				showException(e);
			}
			/** Seteo de valores al modelo actual **/
			modelAndView.addObject("importePagina", totalImpPaginaPagos.toString());
			modelAndView.addObject("importeTotal", totalImportePagos.toString());
			modelAndView.addObject("regIni", regIniPagos);
			modelAndView.addObject("regFin", regFinPagos);
		}
	}
	
	
	/**
	 * Generar contadores pag 2.
	 *
	 * @param modulo El objeto: modulo
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResListadoPagos Objeto del tipo BeanResListadoPagos
	 * @param tipoOrden El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 */
	protected void generarContadoresPag2(BeanModulo modulo, ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos, String tipoOrden, ArchitechSessionBean architechSessionBean) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIniConPagos = 1;
			 Integer regFinConPagos = beanResListadoPagos.getTotalReg();
			
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIniConPagos = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFinConPagos = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImpPaginaConPagos = BigDecimal.ZERO;
			BigDecimal totalImporteConPagos = BigDecimal.ZERO;
			
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImpPaginaConPagos = totalImpPaginaConPagos.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			
			try {
				totalImporteConPagos = boPagos.obtenerListadoPagosMontos(modulo, tipoOrden, getArchitechBean());
			} catch (BusinessException e) {
				showException(e);
			}
			/** Seteo de valores al modelo actual **/
			modelAndView.addObject("importePagina", totalImpPaginaConPagos.toString());
			modelAndView.addObject("importeTotal", totalImporteConPagos.toString());
			modelAndView.addObject("regIni", regIniConPagos);
			modelAndView.addObject("regFin", regFinConPagos);
		}
	}
	
	/**
	 * Obtener el objeto: bo pagos.
	 *
	 * @return El objeto: bo pagos
	 */
	public BOPagos getBoPagos() {
		return boPagos;
	}

	/**
	 * Definir el objeto: bo pagos.
	 *
	 * @param boPagos El nuevo objeto: bo pagos
	 */
	public void setBoPagos(BOPagos boPagos) {
		this.boPagos = boPagos;
	}

	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
