/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerMttoMediosAutoriBuscar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 07:03:56 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.controllers.catalogos;

import javax.servlet.http.HttpServletRequest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOMttoMediosAutorizados;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.UtilsMttoMedios;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Class ControllerMttoMediosAutoriBuscar. Clase que utiliza la pantalla de
 * mantenimiento de madios
 * 
 * @author FSW-Vector
 * @since 12/09/2018
 */
@Controller
public class ControllerMttoMediosAutoriBuscar extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5176993756583662762L;

	/** La constante VIEWMEDIOSAUTORIZADOS. */
	private static final String VIEWMEDIOSAUTORIZADOS = "mantoMediosAutorizados";

	/** La variable que contiene informacion con respecto a: mensaje. */
	private String mensaje = "";

	/** La constante NBEANMTTOMEDIOS. */
	private static final String NBEANMTTOMEDIOS = "beanMttoMediosAutorizadosRes";

	/** La constante NBEANMTTOMEDIOSREQ. */
	private static final String NBEANMTTOMEDIOSREQ = "beanMttoMediosAutorizadosReq";

	/** La constante BEAN_MEDIO_ENTREGA. */
	private static final String BEAN_MEDIO_AUTORIZACION = "beanMttoMediosAutorizados";

	/**
	 * La variable que contiene informacion con respecto a: bo mtto medios
	 * autorizados.
	 */
	private BOMttoMediosAutorizados boMttoMediosAutorizados;

	/** La constante ERROR. */
	private static final String ERROR = "Ocurrio un error al obtener los datos ";

	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";

	/**
	 * Muestra manto medios. metodo inicial que trae todos los datos del catalogo de
	 * mantenimiento de medios
	 *
	 * @param benReq           El objeto: ben req req como parametro de entrada
	 * @param beanOrdenamiento El objeto: bean ordenamiento req como parametro de entrada
	 * @param paginador        El objeto: paginador req como parametro de entrada
	 * @param beanAuto         El objeto: bean auto req como parametro de entrada
	 * @param bindingResult El objeto: binding result como parametro de entrada de validacion
	 * @return Objeto y vista como salida hacia el front
	 */
	@RequestMapping(value = "/mantenimientoMediosAutorizados.do")
	public ModelAndView muestraMantoMedios(BeanMttoMediosAutorizadosReq benReq, BeanOrdenamiento beanOrdenamiento,
			BeanPaginador paginador, @ModelAttribute(BEAN_MEDIO_AUTORIZACION) BeanMttoMediosAutorizados beanAuto, BindingResult bindingResult) {

		/** valida por bean validation */
		if (bindingResult.hasErrors()) {
			error(RESULTADOBINDING);
		}

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		/**
		 * Se incicializa la variable de vista y se manda llama la fucnion de buscar
		 **/
		BeanMttoMediosAutoBusqueda bean = new BeanMttoMediosAutoBusqueda();
		bean.setBeanOrdenamiento(beanOrdenamiento);
		bean.setBeanPaginador(paginador);
		benReq.setDtosBusqueda("todo-todo");
		ModelAndView model = null;
		/** funcion que incia cuando es una actualizacion **/
		try {
			/** se manda buscar , ya que es la consulta principal **/
			model = buscarDatos(req, bean, benReq, 0, beanAuto);

		} catch (BusinessException e) {
			showException(e);
			model = msjError();
			error(ERROR + e.getMessage(), this.getClass());
		}
		return model;
	}

	/**
	 * Metodo para mostrar el mensaje de erro en la vista.
	 *
	 * @return Objeto model and view como salida hacia el front
	 */ 
	private ModelAndView msjError() {
		ModelAndView model = new ModelAndView(VIEWMEDIOSAUTORIZADOS);
		model.addObject(Constantes.COD_ERROR, Errores.EC00011B);
		model.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
		model.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
		return model;
	}

	/**
	 * Muestra manto medios filtro.
	 *
	 * @param benReq           El objeto: ben req como parametro de entrada
	 * @param beanOrdenamiento El objeto: bean ordenamiento  como parametro de entrada
	 * @param paginador        El objeto: paginador  como parametro de entrada
	 * @param beanAuto         El objeto: bean auto  como parametro de entrada
	 * @param bindingResult El objeto: binding result  como parametro de entrada de validacion
	 * @return Objeto y vista  como salida hacia el front
	 */
	@RequestMapping(value = "/mttoMediosAutorizados.do")
	public ModelAndView muestraMantoMediosFiltro(BeanMttoMediosAutorizadosReq benReq, BeanOrdenamiento beanOrdenamiento,
			BeanPaginador paginador, @ModelAttribute(BEAN_MEDIO_AUTORIZACION) BeanMttoMediosAutorizados beanAuto, BindingResult bindingResult) {

		/** valida por bean validation */
		if (bindingResult.hasErrors()) {
			error(RESULTADOBINDING);
		}

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		/**
		 * Se incicializa la variable de vista y se manda llama la fucnion de buscar
		 **/
		BeanMttoMediosAutoBusqueda bean = new BeanMttoMediosAutoBusqueda();
		bean.setBeanOrdenamiento(beanOrdenamiento);
		bean.setBeanPaginador(paginador);

		ModelAndView model = null;
		/** funcion que incia cuando es una actualizacion **/
		try {
			/** se manda buscar , ya que es la consulta principal **/
			model = buscarDatos(req, bean, benReq, 0, beanAuto);
		} catch (BusinessException e) {
			showException(e);
			model = msjError();
			error(ERROR + e.getMessage(), this.getClass());
		}

		return model;
	}

	/**
	 * Metodo que busca los datos segun la accion que se mande.
	 *
	 * @param req      El objeto: req como parametro de entrada
	 * @param bean     El objeto: bean req como parametro de entrada
	 * @param benReq   El objeto: ben req req como parametro de entrada
	 * @param accion   El objeto: accion (accion=0 muestra el mensaje que trae el
	 *                 bean o accion = 1 muestra el mensaje de exito en)
	 * @param beanAuto El objeto: bean auto req como parametro de entrada
	 * @return Objeto model and view como salida hacia el front
	 * @throws BusinessException La business exception 
	 */
	public ModelAndView buscarDatos(HttpServletRequest req, BeanMttoMediosAutoBusqueda bean,
			BeanMttoMediosAutorizadosReq benReq, int accion, BeanMttoMediosAutorizados beanAuto)
			throws BusinessException {
		/** inicializa las variables **/
		BeanOrdenamiento beanOrdenamiento = new BeanOrdenamiento();
		ModelAndView model = null;
		/** List que se muestra en el front **/
		BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes = new BeanMttoMediosAutorizadosRes();
		RequestContext ctx = new RequestContext(req);

		/** Se realiza la busqueda total **/
		beanMttoMediosAutorizadosRes = boMttoMediosAutorizados.consultaTablaMA(bean, getArchitechBean(), beanAuto);
		/** Asigna el campo **/
		beanMttoMediosAutorizadosRes.setBeanMttoMediosAutoReq(benReq);
		/** Se asigna la vista **/
		model = new ModelAndView(VIEWMEDIOSAUTORIZADOS, NBEANMTTOMEDIOS, beanMttoMediosAutorizadosRes);
		model.addObject("sortField", beanOrdenamiento.getSortField());
		model.addObject("sortType", beanOrdenamiento.getSortType());
		model.addObject(BEAN_MEDIO_AUTORIZACION, beanAuto);
		/** Segun la accion regrea el mensaje **/
		if (accion == 1) {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + Errores.OK00000V);
			model.addObject(Constantes.COD_ERROR, Errores.OK00000V);
			model.addObject(Constantes.TIPO_ERROR, Errores.DESC_OK00000V);
			model.addObject(Constantes.DESC_ERROR, Errores.TIPO_MSJ_ALERT);
		} else {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + beanMttoMediosAutorizadosRes.getCodError());
			model.addObject(Constantes.COD_ERROR, beanMttoMediosAutorizadosRes.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanMttoMediosAutorizadosRes.getTipoError());
		}
		/** genera el paginador de la table **/
		generarContadoresPaginacion(beanMttoMediosAutorizadosRes, model);
		model.addObject(Constantes.DESC_ERROR, mensaje);
		return model;
	}

	/**
	 * Metodo para exportar Todo, genera un registro para que el proceso batch tome
	 * los datos y genere el reporte.
	 *
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados
	 *                                     req
	 * @param bindingResult El objeto: binding result como parametro de entrada de validacion
	 * @return Objeto model and view como salida hacia el front
	 */
	@RequestMapping(value = "/exportaMttoAutorizados.do")
	public ModelAndView exportarMttMedios(
			@ModelAttribute(NBEANMTTOMEDIOSREQ) BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, BindingResult bindingResult) {
		/** valida por bean validation */
		if (bindingResult.hasErrors()) {
			error(RESULTADOBINDING);
		}

		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		/** inicializa la vista y las variables **/
		ModelAndView model = new ModelAndView(VIEWMEDIOSAUTORIZADOS);
		BeanMttoMediosAutorizadosRes beansRes = new BeanMttoMediosAutorizadosRes();
		BeanMttoMediosAutorizadosRes exp = new BeanMttoMediosAutorizadosRes();
		BeanOrdenamiento beanOrdenamiento = new BeanOrdenamiento();
		RequestContext ctx = new RequestContext(req);
		/** funcion que incia cuando es una actualizacion **/
		try {
			/** busca el paginador **/
			BeanPaginador beanPaginador = new BeanPaginador();
			BeanMttoMediosAutoBusqueda bean = new BeanMttoMediosAutoBusqueda();
			bean.setBeanPaginador(beanPaginador);
			bean.setSortField(beanOrdenamiento.getSortField());
			bean.setFiltro("todo");
			bean.setDto("todo");

			/** Se realiza la busqueda total **/
			beansRes = boMttoMediosAutorizados.consultaTablaMA(bean, getArchitechBean(),
					new BeanMttoMediosAutorizados());
			beansRes.setBeanMttoMediosAutoReq(beanMttoMediosAutorizadosReq);
			/** exporta los datos a la tabla para que el bash lo obtenga **/
			exp = boMttoMediosAutorizados.exportarTodo(getArchitechBean(), beanMttoMediosAutorizadosReq,
					beansRes.getTotalReg());
			beansRes.setCodError(exp.getCodError());
			beansRes.setMsgError(exp.getMsgError());
			beansRes.setTipoError(exp.getTipoError());
			/** inicializa la vairable de vista **/
			model = new ModelAndView(VIEWMEDIOSAUTORIZADOS, NBEANMTTOMEDIOS, beansRes);
			model.addObject("sortField", beanOrdenamiento.getSortField());
			model.addObject("sortType", beanOrdenamiento.getSortType());
			/** segun el codigo de error manda el mensaje **/
			if (beansRes.getCodError().equals(Errores.OK00002V)) {
				model.addObject(Constantes.COD_ERROR, exp.getCodError());
				model.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_INFO);
				mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + exp.getCodError(),
						new Object[] { beansRes.getMsgError() }, LocaleContextHolder.getLocale());
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.COD_ERROR, exp.getCodError());
				model.addObject(Constantes.TIPO_ERROR, exp.getTipoError());
				model.addObject(Constantes.DESC_ERROR, exp.getMsgError());
			}

		} catch (BusinessException e) {
			/** mensaje de error **/
			showException(e);
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + Errores.EC00011B);
			model = msjError();
			error(ERROR + e.getMessage(), this.getClass());

		}
		/** genera el paginados **/
		generarContadoresPaginacion(beansRes, model);
		model.addObject(Constantes.DESC_ERROR, mensaje);

		return model;
	}

	/**
	 * Metodo que genera el excel de los datos que se encuentran en la pantalla de
	 * mantenimiento de medios.
	 *
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados
	 *                                     req
	 * @param bindingResult El objeto: binding result como parametro de entrada de validacion
	 * @return Objeto model and view como salida hacia el front
	 */
	@RequestMapping(value = "exportarMttoMedios.do")
	public ModelAndView exportarMttoMedios(
			@ModelAttribute(NBEANMTTOMEDIOS) BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosReq, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		/** valida por bean validation */
		if (bindingResult.hasErrors()) {
			error(RESULTADOBINDING);
		}

		/** inicio de variables **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);

		/** funcion que forma el excel a exportar **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		/** se inicializa la vista del form **/
		model = new ModelAndView(new ViewExcel());
		model.addObject("FORMAT_HEADER", formatCellHeader);
		model.addObject("FORMAT_CELL", formatCellBody);
		model.addObject("HEADER_VALUE", UtilsMttoMedios.getHeaderExcel(ctx));
		model.addObject("BODY_VALUE", UtilsMttoMedios.getBody(beanMttoMediosAutorizadosReq.getListaBeanMttoMedAuto()));
		model.addObject("NAME_SHEET", "MantenimientoMediosAutorizados");

		return model;
	}

	/**
	 * Generar contadores paginacion. metodo para la generacion del paginador del
	 * grid del catalogo de mantenimiento de medios
	 *
	 * @param beanMttoMediosAutorizadosRes El objeto: bean mtto medios autorizados
	 *                                     res
	 * @param model                        El objeto: model como salida hacia el front
	 */
	protected void generarContadoresPaginacion(BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes,
			ModelAndView model) {
		if (beanMttoMediosAutorizadosRes.getTotalReg() > 0) {
			Integer regIni = 1;
			Integer regFin = beanMttoMediosAutorizadosRes.getTotalReg();
			/**
			 * genera el mensaje que se muestra en la parte inferior izquiera del la tabla
			 **/
			if (!"1".equals(beanMttoMediosAutorizadosRes.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanMttoMediosAutorizadosRes.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanMttoMediosAutorizadosRes.getTotalReg() >= 20
					* Integer.parseInt(beanMttoMediosAutorizadosRes.getBeanPaginador().getPaginaAnt())) {
				regFin = 20 * Integer.parseInt(beanMttoMediosAutorizadosRes.getBeanPaginador().getPagina());
			}
			model.addObject("regIni", regIni);
			model.addObject("regFin", regFin);
		}
	}

	/**
	 * Obtener el objeto: bo mtto medios autorizados.
	 *
	 * @return El objeto: bo mtto medios autorizados
	 */
	public BOMttoMediosAutorizados getBoMttoMediosAutorizados() {
		return boMttoMediosAutorizados;
	}

	/**
	 * Definir el objeto: bo mtto medios autorizados.
	 *
	 * @param boMttoMediosAutorizados El nuevo objeto: bo mtto medios autorizados
	 */
	public void setBoMttoMediosAutorizados(BOMttoMediosAutorizados boMttoMediosAutorizados) {
		this.boMttoMediosAutorizados = boMttoMediosAutorizados;
	}

}
