/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerRecepOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/04/2018      SMEZA		VECTOR      Creacion
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de Admon Saldos - Recepcion de Operaciones.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOpRechazar;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.servicio.SPEI.BORecepOperacionAutorizar;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase tipo controlador que maneja los eventos para el flujo de Admon Saldos - Recepcion de Operaciones.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerRecepOperaRechazo extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -1194869597038713887L;

	
	/** Propiedad del tipo BORecepOperacion que almacena el valor de BORecepOperacionExt. */
	private BORecepOperacionAutorizar bORecepOperacionAutorizar;
	
		
	/** Propiedad del tipo BORecepOpRechazar que almacena el valor de BORecepOpRechazar. **/
	private BORecepOpRechazar boRecepOpRechazar;
	
	/** Propiedad del tipo BORecepOpRechazar que almacena el valor de BORecepOperacion. **/
	private  BORecepOperacion bORecepOperacion;
	
	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modelVista;
	
	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();
	
	/**
	 * Metodo que permite hacer las transferencias.
	 *
	 * @param beanReqConsTranSpei Objeto con los datos de la peticion
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/transferirSpeiRec.do")
	public ModelAndView transferirSpeiRec(BeanReqTranSpeiRec beanReqConsTranSpei, HttpServletRequest req, 
			HttpServletResponse res,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper){
		/**Se inicializan datos**/
		StringBuilder strBuilder = new StringBuilder();
		modelVista = null;
		BeanResTranSpeiRec beanResTranSpeiRec = null;
		RequestContext ctx = new RequestContext(req);
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		try {
			String mensaje = null;
			
			BeanResTranSpeiRec beanResConsTranSpeiRec = null;
			
			/**llama la funcion para hacer la tranferencia**/
			beanResConsTranSpeiRec = bORecepOperacionAutorizar.transferirSpeiRec(beanReqConsTranSpei, getArchitechBean(), historico);
			
			/**Se hace la busqueda de los datos**/			
			beanResTranSpeiRec = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpei, getArchitechBean(), historico,beanTranSpeiRecOper);
						
			/**Se llama a la vista a mostrar o pantalla**/
			modelVista =  util.vistaMostrar(beanReqConsTranSpei,beanResTranSpeiRec,historico);
		       
			
			if(Errores.OK00001V.equals(beanResConsTranSpeiRec.getBeanError().getCodError())){							
				mensaje = continua(beanResConsTranSpeiRec,ctx) ;	
			}else if(Errores.ETRAMA001.equals(beanResConsTranSpeiRec.getBeanError().getCodError())){
				mensaje = beanResConsTranSpeiRec.getBeanError().getMsgError();
			}else{			    
			    mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsTranSpeiRec.getBeanError().getCodError());
			}
			
			strBuilder.append(mensaje);	
			ArchitechSessionBean sessionBean = getArchitechBean();
			modelVista.addObject("sessionBean", sessionBean);
			modelVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
			/**Se asigna el codigo de error**/
			modelVista.addObject(Constantes.TIPO_ERROR, beanResConsTranSpeiRec.getBeanError().getTipoError());
			modelVista.addObject(Constantes.COD_ERROR, beanResConsTranSpeiRec.getBeanError().getCodError());
			modelVista.addObject(Constantes.DESC_ERROR, strBuilder.toString());
			 
			 
			 
		}catch (BusinessException e) {
			showException(e);
			modelVista = util.errorVista(historico, beanResTranSpeiRec,beanReqConsTranSpei, ctx );	
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage(), this.getClass());
		}
	    return modelVista;
	 }
	
	
	
	/**
	 * Continua.
	 *
	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
	 * @param ctx El objeto: ctx
	 * @return Objeto string
	 */
	private String continua(BeanResTranSpeiRec beanResConsTranSpeiRec, RequestContext ctx) {
		 /**inicializan los datos **/
		 String msj = null;
		 StringBuilder strBuild = new StringBuilder();
		 /**recorre la lista **/
		 for(BeanTranSpeiRecOper beanTranSpeiRec:beanResConsTranSpeiRec.getListBeanTranSpeiRecOK()){
		    	if(beanTranSpeiRec.getBeanDetalle().getError().getCodError().contains("TRIB0000")){
		    		msj = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"OK00018V",
		        		new Object[]{beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion(),
		        		beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()
		        		,beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago(), 
		        		beanTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer(),beanTranSpeiRec.getBeanDetalle().getDetEstatus().getEstatusTransfer()});
		    		strBuild.append(msj+"  ");
		    }
		}
		 /**recorre la lista **/
		for(BeanTranSpeiRecOper beanTranSpeiRec:beanResConsTranSpeiRec.getListBeanTranSpeiRecNOK()){
			if(beanTranSpeiRec.getBeanDetalle().getError().getCodError().contains(ConstantesRecepOpePantalla.TRIB)
					||beanTranSpeiRec.getBeanDetalle().getError().getCodError().contains("TUBO")){
				msj = ctx.getMessage(Constantes.COD_ERROR_PUNTO+"ED00087V",
						new Object[]{beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion(),beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()
		        		,beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago(), beanTranSpeiRec.getBeanDetalle().getDetEstatus().getRefeTransfer(),beanTranSpeiRec.getBeanDetalle().getError().getCodError()});
				strBuild.append(msj+"  ");
		
		
			}else if(Errores.ED00085V.equals(beanTranSpeiRec.getBeanDetalle().getError().getCodError())){
				msj = ctx.getMessage(Constantes.COD_ERROR_PUNTO+Errores.ED00085V,
						new Object[]{beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec()});
				strBuild.append(msj+"  ");
		
			}else{
				msj = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanTranSpeiRec.getBeanDetalle().getError().getCodError(),
						new Object[]{beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFchOperacion(),beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPaquete()
		        		,beanTranSpeiRec.getBeanDetalle().getDetalleGral().getFolioPago(),beanTranSpeiRec.getBeanDetalle().getReceptor().getNumCuentaRec()});
				strBuild.append(msj+"  ");
		
			}
		}
		return msj;
	}


	/**
	 * Metodo que permite rechazar las operaciones.
	 *
	 * @param beanReqConsTranSpei Objeto con los datos de la peticion
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/rechazarTransfSpeiRec.do")
	public ModelAndView rechazarTransfSpeiRec(BeanReqTranSpeiRec beanReqConsTranSpei,HttpServletRequest req,
			HttpServletResponse res,@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) BeanTranSpeiRecOper beanTranSpeiRecOper){
		/**inicializa los datos **/
		modelVista = null;
		/**se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqConsTranSpei);
		beanReqConsTranSpei.getPaginador().setAccion("ACT");
		BeanResTranSpeiRec beanResTranSpeiRec = null;
		BeanResTranSpeiRec beanResConsTranSpeiRec = null;
		RequestContext ctx = new RequestContext(req);
		try {
			
		
	
        	beanResConsTranSpeiRec = boRecepOpRechazar.rechazarTransfSpeiRec(beanReqConsTranSpei, getArchitechBean(), historico);
        	
        	/**Se hace la busqueda de los datos**/			
			beanResTranSpeiRec = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpei, getArchitechBean(), historico,beanTranSpeiRecOper);
			
			/**Se llama a la vista a mostrar o pantalla**/
			modelVista =  util.vistaMostrar(beanReqConsTranSpei,beanResTranSpeiRec,historico);
    	    
			ArchitechSessionBean sessionBean = getArchitechBean();
			modelVista.addObject("sessionBean", sessionBean); 
			modelVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, beanTranSpeiRecOper);
	        String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO+beanResConsTranSpeiRec.getBeanError().getCodError());
	        modelVista.addObject(Constantes.COD_ERROR, beanResConsTranSpeiRec.getBeanError().getCodError());
	        modelVista.addObject(Constantes.TIPO_ERROR, beanResConsTranSpeiRec.getBeanError().getTipoError());
	        modelVista.addObject(Constantes.DESC_ERROR, mensaje);
	        
		}catch (BusinessException e) {
			showException(e);
			modelVista = util.errorVista(historico, beanResTranSpeiRec,beanReqConsTranSpei,ctx);	
			error(ConstantesRecepOpePantalla.ERROR+e.getMessage(), this.getClass());
		}
	    return modelVista;
	 }
	
	
	
	
	/**
	 * Obtener el objeto: bo recep op rechazar.
	 *
	 * @return the boRecepOpRechazar
	 */
	public BORecepOpRechazar getBoRecepOpRechazar() {
		return boRecepOpRechazar;
	}

	/**
	 * Definir el objeto: bo recep op rechazar.
	 *
	 * @param boRecepOpRechazar the boRecepOpRechazar to set
	 */
	public void setBoRecepOpRechazar(BORecepOpRechazar boRecepOpRechazar) {
		this.boRecepOpRechazar = boRecepOpRechazar;
	}

	
	
	/**
	 * getbORecepOperacionAutorizar de tipo BORecepOperacionAutorizar.
	 * @author FSW-Vector
	 * @return bORecepOperacionAutorizar de tipo BORecepOperacionAutorizar
	 */
	public BORecepOperacionAutorizar getbORecepOperacionAutorizar() {
		return bORecepOperacionAutorizar;
	}



	/**
	 * setbORecepOperacionAutorizar para asignar valor a bORecepOperacionAutorizar.
	 * @author FSW-Vector
	 * @param bORecepOperacionAutorizar de tipo BORecepOperacionAutorizar
	 */
	public void setbORecepOperacionAutorizar(BORecepOperacionAutorizar bORecepOperacionAutorizar) {
		this.bORecepOperacionAutorizar = bORecepOperacionAutorizar;
	}



	/**
	 * Obtener el objeto: b O recep operacion.
	 *
	 * @return El objeto: b O recep operacion
	 */
	public BORecepOperacion getbORecepOperacion() {
		return bORecepOperacion;
	}



	/**
	 * Definir el objeto: b O recep operacion.
	 *
	 * @param bORecepOperacion El nuevo objeto: b O recep operacion
	 */
	public void setbORecepOperacion(BORecepOperacion bORecepOperacion) {
		this.bORecepOperacion = bORecepOperacion;
	}

	
	

	
}