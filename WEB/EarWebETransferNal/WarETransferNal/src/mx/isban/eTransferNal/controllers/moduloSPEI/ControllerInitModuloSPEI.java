/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerInitModuloSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	     By 	          Company 	 Description
 * ------- -----------   -----------        ---------- ----------------------
 *   1.0   09/04/2018  Salvador Meza Torres  VECTOR 		Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloSPEI;

/**
 * Anexo de Imports para la funcionalidad de la pantalla de SPEI - Detalle.
 * 
 * @author FSW-Vector
 * @sice 17 abril 2018
 *
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;



/**
 * Clase tipo controlador que maneja los eventos para el flujo de SPEI.
 *
 * @author FSW-Vector
 * @sice 17 abril 2018
 */
@Controller
public class ControllerInitModuloSPEI extends Architech{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -324388143800413821L;

	/**
	 * Inicio.
	 *
	 * @return the model and view
	 */
	@RequestMapping(value="moduloSPEIInit.do")
	public ModelAndView inicio(){
		
			return new ModelAndView("moduloSPEIInit");
	}
}