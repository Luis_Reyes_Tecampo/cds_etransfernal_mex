/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerExcepNomBeneficiarios.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      19/09/2019 11:05:31 PM     Uriel Alfredo Botello R.    VSF        Creacion
 */

package mx.isban.eTransferNal.controllers.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepNomBenef;
import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.catalogos.BOExcepNomBenef;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsConstantes;
import mx.isban.eTransferNal.utilerias.catalogos.UtilsExportarNomBenef;



/**
 * Class ControllerExcepNomBeneficiarios.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Controller
public class ControllerExcepNomBeneficiarios extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3778853751532504413L;
	
	/** La constante FILTRO. */
	private static final String FILTRO = "beanFilter";
	
	/** La constante ESTILO_LETRA. */
	private static final String ESTILO_LETRA = "Calibri";
	
	/** La constante NOMBRE_BEAN. */
	private static final String NOMBRE_BEAN = "beanExcepNomBenef";
	
	/** La constante ACCION. */
	private static final String ACCION = "accion";
	
	/** La constante BEAN_NOM_BENEF. */
	private static final String BEAN_NOM_BENEF = "beanExcNomBenef";
	
	/** La constante BEAN_PAGINADOR. */
	private static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante TOTAL_REGISTROS. */
	private static final String TOTAL_REGISTROS = "totalReg";
	
	/** La constante PAGINA_ALTA. */
	private static final String PAGINA_ALTA = "altaNombreBenef";
	
	/** La constante PAGINA. */
	private static final String PAGINA = "excepcionNomBeneficiarios";
	
	/** La constante ALTA. */
	private static final String ALTA = "alta";
	
	/** La constante EDITAR. */
	private static final String EDITAR = "editar";
	
	/** La constante RESULTADOBINDING. */
	private static final String RESULTADOBINDING = "Ha ocurrido un Error!";
	
	/** La variable que contiene informacion con respecto a: bo excep nom benef. */
	private BOExcepNomBenef boExcepNomBenef;
	
	/** La constante utilerias. */
	private static final UtilsExportarNomBenef utilerias = UtilsExportarNomBenef.getInstancia();
	
	/**
	 * Principal excep nom beneficiarios.
	 *
	 * @param beanPaginador El objeto: bean paginador --> Objeto de entrada con datos de paginacion
	 * @param beanFilter El objeto: bean filter  --> Objeto de entrada con datos de los filtros
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "excepNomBeneficiarios.do")
	public ModelAndView principalExcepNomBeneficiarios(
			@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(UtilsConstantes.BEAN_FILTER) BeanExcepNomBenef beanFilter, BindingResult bindingResult) {
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** se inicializan variables */
		BeanExcepNomBenef response = null;
		ModelAndView modeloVista = null;
		BeanExcepNomBenef beanRequest = beanFilter;
		beanRequest.setBeanPaginador(beanPaginador);
		
		try {
			/** se ejecuta la consulta con el filtro de entrada */
			response = boExcepNomBenef.consultaNombres(getArchitechBean(),beanRequest);
			modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,response);
			/** asigna mensaje de respuesta */
			modeloVista.addObject(FILTRO, beanFilter.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, response.getBeanPaginador());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(TOTAL_REGISTROS, response.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Alta excep nom beneficiarios.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "altaNomBeneficiarios.do")
	public ModelAndView altaExcepNomBeneficiarios(@ModelAttribute(NOMBRE_BEAN) BeanExcepNomBenef beanExcepNomBenef, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** inicializa variables */
		ModelAndView modeloVista = null;
		/** valida nombre entrante para el alta */
		if(beanExcepNomBenef.getBeanFilter().getNombre() == null || beanExcepNomBenef.getBeanFilter().getNombre().isEmpty() ) {
			/** si no llego el nombre muestra la pantalla del alta */
			modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_NOM_BENEF,beanExcepNomBenef);
			modeloVista.addObject(ACCION,ALTA);
			
		} else {
			/** en caso de que si, ejecuta flujo del alta */
			modeloVista = altaExcepNomBeneficiarios(beanExcepNomBenef);
		}
		return modeloVista;
	}
	
	/**
	 * Alta excep nom beneficiarios.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	public ModelAndView altaExcepNomBeneficiarios(BeanExcepNomBenef beanExcepNomBenef) {
		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanExcepNomBenef response = null;
		BeanExcepNomBenef consultaNombres = null;
		try {
			/** ejecuta el alta */
			response = boExcepNomBenef.altaNombres(getArchitechBean(), beanExcepNomBenef.getBeanFilter().getNombre());
			/** consulta los registros */
			consultaNombres = boExcepNomBenef.consultaNombres(getArchitechBean(),new BeanExcepNomBenef());
			/** valida la respuesta del alta */
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,consultaNombres);
			} else {
				modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_NOM_BENEF,beanExcepNomBenef);
				modeloVista.addObject(ACCION,ALTA);
			}
			/** se agregan datos de respuesta */
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			modeloVista.addObject(FILTRO, consultaNombres.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, consultaNombres.getBeanPaginador());
			modeloVista.addObject(TOTAL_REGISTROS, consultaNombres.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Editar excep nom beneficiarios.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "cambioNomBeneficiarios.do")
	public ModelAndView editarExcepNomBeneficiarios(@ModelAttribute(NOMBRE_BEAN) BeanExcepNomBenef beanExcepNomBenef, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** inicializa variables */
		ModelAndView modeloVista = null;
		List<BeanNomBeneficiario> listaNomsSeleccionadas = new ArrayList<BeanNomBeneficiario>();
		/** obtiene una lista de los registros seleccionados */
		for(int i = 0; i < beanExcepNomBenef.getListaNombres().size(); i++) {
			if(beanExcepNomBenef.getListaNombres().get(i).isSeleccionado()) {
				listaNomsSeleccionadas.add(beanExcepNomBenef.getListaNombres().get(i));
			}
		}
		/** valida si no llego el nombre nueva */
		if(beanExcepNomBenef.getBeanFilter().getNombre() == null || beanExcepNomBenef.getBeanFilter().getNombre().isEmpty() ) {
			/** si no llego, muestra pantalla de editar */
			modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_NOM_BENEF,beanExcepNomBenef);
			modeloVista.addObject(ACCION,EDITAR);
			modeloVista.addObject(FILTRO,listaNomsSeleccionadas.get(0));
			
		} else {
			/** si llego, ejecuta la modificacion */
			modeloVista = editarExcepNomBeneficiarios(beanExcepNomBenef);
		}
		return modeloVista;
	}
	
	/**
	 * Editar excep nom beneficiarios.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	public ModelAndView editarExcepNomBeneficiarios(BeanExcepNomBenef beanExcepNomBenef) {
		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanExcepNomBenef response = null;
		BeanExcepNomBenef consultaNombres = null;
		BeanNomBeneficiario filtroPrincipal = new BeanNomBeneficiario();
		filtroPrincipal.setNombre(beanExcepNomBenef.getListaNombres().get(0).getNombre());
		try {
			/** ejecuta la modificacion */
			response = boExcepNomBenef.modificacionNombres(getArchitechBean(), 
					beanExcepNomBenef.getListaNombres().get(0).getNombre(), beanExcepNomBenef.getBeanFilter().getNombre());
			/** realiza una consulta una vez se haya modificado */
			consultaNombres = boExcepNomBenef.consultaNombres(getArchitechBean(),new BeanExcepNomBenef());
			/** valida la respuesta de la modificacion */
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,consultaNombres);
				modeloVista.addObject(FILTRO, consultaNombres.getBeanFilter());
			} else {
				modeloVista = new ModelAndView(PAGINA_ALTA,BEAN_NOM_BENEF,beanExcepNomBenef);
				modeloVista.addObject(FILTRO, filtroPrincipal);
				modeloVista.addObject(ACCION,EDITAR);
			}
			/** agrega valores de respuesta */
			modeloVista.addObject(TOTAL_REGISTROS, consultaNombres.getBeanFilter().getTotalReg());
			modeloVista.addObject(BEAN_PAGINADOR, consultaNombres.getBeanPaginador());
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}
	
	/**
	 * Baja excep nom beneficiarios.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "bajaNomBeneficiarios.do")
	public ModelAndView bajaExcepNomBeneficiarios(@ModelAttribute(NOMBRE_BEAN) BeanExcepNomBenef beanExcepNomBenef, BindingResult bindingResult) {
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}

		
		/** inicializa variables */
		ModelAndView modeloVista = new ModelAndView(PAGINA);
		BeanExcepNomBenef response = null;
		BeanExcepNomBenef nuevaConsulta = new BeanExcepNomBenef();
		List<BeanNomBeneficiario> listaNomsSeleccionadas = new ArrayList<BeanNomBeneficiario>();
		for(int i = 0; i < beanExcepNomBenef.getListaNombres().size(); i++) {
			if(beanExcepNomBenef.getListaNombres().get(i).isSeleccionado()) {
				listaNomsSeleccionadas.add(beanExcepNomBenef.getListaNombres().get(i));
			}
		}
		try {
			response = boExcepNomBenef.bajaNombres(getArchitechBean(), listaNomsSeleccionadas);
			if(response.getBeanError().getCodError().equals(Errores.OK00000V)) {
				nuevaConsulta = boExcepNomBenef.consultaNombres(getArchitechBean(), beanExcepNomBenef);
				modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,nuevaConsulta);
			} else {
				modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,beanExcepNomBenef);
			}
			modeloVista.addObject(Constantes.COD_ERROR, response.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getBeanError().getMsgError());
			modeloVista.addObject(BEAN_PAGINADOR, nuevaConsulta.getBeanPaginador());
			modeloVista.addObject(TOTAL_REGISTROS, nuevaConsulta.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		
		return modeloVista;
	}
	
	/**
	 * Exportar nombres.
	 *
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "exportarNomBeneficiarios.do")
	public ModelAndView exportarNombres(@ModelAttribute(NOMBRE_BEAN) BeanExcepNomBenef beanExcepNomBenef, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		/**valida por bean validation */
		if(bindingResult.hasErrors()){
					error(RESULTADOBINDING);
				}
		
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		ModelAndView modelAndView = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName(ESTILO_LETRA);
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName(ESTILO_LETRA);
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);

		modelAndView = new ModelAndView(new ViewExcel());
		modelAndView.addObject("FORMAT_HEADER", formatCellHeader);
		modelAndView.addObject("FORMAT_CELL", formatCellBody);
		modelAndView.addObject("HEADER_VALUE", utilerias.getHeaderExcel(ctx));
		modelAndView.addObject("BODY_VALUE", utilerias.getBody(beanExcepNomBenef.getListaNombres()));
		modelAndView.addObject("NAME_SHEET", "Excepci\u00F3n de Nombres de Beneficiarios");
		return modelAndView;
	}
	
	/**
	 * Exportar todos nombres.
	 *
	 * @param beanPaginador El objeto: bean paginador--> Objeto de entrada con datos de paginacion
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Bean con informacion del registro
	 * @param bindingResult El objeto: binding result --> Objeto de entrada validador de los datos
	 * @return Objeto model and view --> Objeto de salida tipo modelo regresado al front
	 */
	@RequestMapping(value = "exporTodoNomBeneficiarios.do")
	public ModelAndView exportarTodosNombres(@ModelAttribute(UtilsConstantes.BEAN_PAGINADOR) BeanPaginador beanPaginador,
			@ModelAttribute(NOMBRE_BEAN) BeanExcepNomBenef beanExcepNomBenef, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			error(RESULTADOBINDING);
		}

		/** inicializa variables */
		ModelAndView modeloVista = null;
		BeanResBase response = null;
		try {
			modeloVista = new ModelAndView(PAGINA,BEAN_NOM_BENEF,beanExcepNomBenef);
			response = boExcepNomBenef.exportarTodo(getArchitechBean());
			modeloVista.addObject(Constantes.COD_ERROR, response.getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR,response.getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, response.getMsgError());
			modeloVista.addObject(FILTRO, beanExcepNomBenef.getBeanFilter());
			modeloVista.addObject(BEAN_PAGINADOR, beanPaginador);
			modeloVista.addObject(TOTAL_REGISTROS, beanExcepNomBenef.getBeanFilter().getTotalReg());
		} catch (BusinessException e) {
			showException(e);
		}
		return modeloVista;
	}

	/**
	 * Obtener el objeto: bo excep nom benef.
	 *
	 * @return El objeto: bo excep nom benef
	 */
	public BOExcepNomBenef getBoExcepNomBenef() {
		return boExcepNomBenef;
	}

	/**
	 * Definir el objeto: bo excep nom benef.
	 *
	 * @param boExcepNomBenef El nuevo objeto: bo excep nom benef
	 */
	public void setBoExcepNomBenef(BOExcepNomBenef boExcepNomBenef) {
		this.boExcepNomBenef = boExcepNomBenef;
	}
}
