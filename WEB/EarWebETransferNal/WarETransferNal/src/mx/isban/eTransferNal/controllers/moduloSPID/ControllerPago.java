/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOPago;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Controlador para los pagos
 * 
 * @author IDS
 *
 */
@Controller
public class ControllerPago extends Architech {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1071881658409821989L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;
	
	/**
	 * 
	 */
	private BOPago bOPago;
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanFilter Objeto del tipo BeanFilter
	 * @param tipoOrden Objeto del tipo @RequestParam String 
	 * @return ModelAndView
	 */
	@RequestMapping(value="/listadoPagos.do")
	public ModelAndView buscarPagos(BeanPaginador beanPaginador, BeanFilter beanFilter,
			@RequestParam("tipo") String tipoOrden){
		BeanResListadoPagos beanResListadoPagos = null;
		ModelAndView model = null;
		
		String titulo="";
		
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
		
		try {
		
			if (!fieldIsEmpty(beanFilter.getField()) && !valorIsEmpty(beanFilter.getFieldValue())){
				beanResListadoPagos = bOPago.buscarPagos(beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, beanPaginador, getArchitechBean());
			} else{
				//Obtencion de bean resultado que sera enviado a la vista (JSP)				
				beanResListadoPagos = bOPago.obtenerListadoPagos(tipoOrden, beanPaginador, getArchitechBean());
			}
			String mensaje = messageSource.getMessage("codError."+beanResListadoPagos.getCodError(), null, null);
			
			titulo = obtenerTitulo(tipoOrden);

			//Nombramiento y puesta de bean que sera enviado a la vista
			
			model = new ModelAndView("listadoPagos","beanResListadoPagos", beanResListadoPagos);
			model.addObject("tipoOrden", tipoOrden);
			model.addObject("field", beanFilter.getField());
			model.addObject("fieldValue", beanFilter.getFieldValue());
			
			//Mensajes de error, se envia tambien cuando es exitosa la operacion
			
			model.addObject("titulo", titulo);
			model.addObject(Constantes.COD_ERROR, beanResListadoPagos.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResListadoPagos.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			
			if (!fieldIsEmpty(beanFilter.getField()) && !valorIsEmpty(beanFilter.getFieldValue())){
				generarContadoresPag1(model, beanResListadoPagos, beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, getArchitechBean());
			} else{
				generarContadoresPag2(model, beanResListadoPagos, tipoOrden, getArchitechBean());
			}
			
			
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}

	/**
	 * @param tipoOrden Objeto del tipo String
	 * @return String
	 */
	protected String obtenerTitulo(String tipoOrden) {
		String titulo = "";
		if ("OPD".equals(tipoOrden)){
			titulo = "Ordenes por Devolver";
		} else if ("TSS".equals(tipoOrden)) {
			titulo = "Traspasos SPID-SIAC";
		} else if ("OEC".equals(tipoOrden)) {
			titulo = "Ordenes Enviadas Confirmadas";
		} else if ("DEC".equals(tipoOrden)) {
			titulo = "Devoluciones Enviadas Confirmadas";
		} else if ("TES".equals(tipoOrden)) {
			titulo = "Traspasos SPID-SIAC en Espera";
		} else if ("TEN".equals(tipoOrden)) {
			titulo = "Traspasos SPID-SIAC Enviadas";
		} else if ("TPR".equals(tipoOrden)) {
			titulo = "Traspasos SPID-SIAC por Reparar";
		} else if ("OES".equals(tipoOrden)) {
			titulo = "Ordenes en Espera";
		} else if ("OEN".equals(tipoOrden)) {
			titulo = "Listado de Pagos de Ordenes Enviadas";
		}
		return titulo;
	}
	
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param beanFilter Objeto del tipo BeanFilter
	 * @param req Objeto del tipo HttpServletRequest
	 * @param tipoOrden Objeto del tipo @RequestParam String 
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/exportarListadoPagos.do")
    public ModelAndView expBitacoraAdmon(BeanPaginador beanPaginador, BeanFilter beanFilter, HttpServletRequest req,
    		@RequestParam("tipo") String tipoOrden){
		FormatCell formatCellHeaderLP = null;
		FormatCell formatCellBodyLP = null;
		ModelAndView model = null;
  	 	RequestContext ctx = new RequestContext(req);
  	 	BeanResListadoPagos beanResListadoPagos = null;
  	 	
  	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		
  	 	try {
  	 		beanPaginador.setAccion("ACT");
  	 		
  	 		if (!fieldIsEmpty(beanFilter.getField()) && !valorIsEmpty(beanFilter.getFieldValue())){
				beanResListadoPagos = bOPago.buscarPagos(beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, beanPaginador, getArchitechBean());
			} else{
				//Obtencion de bean resultado que sera enviado a la vista (JSP)				
				beanResListadoPagos = bOPago.obtenerListadoPagos(tipoOrden, beanPaginador, getArchitechBean());
			}
    	   
  	 		if(Errores.OK00000V.equals(beanResListadoPagos.getCodError())||
 				Errores.ED00011V.equals(beanResListadoPagos.getCodError())){
  	 			formatCellBodyLP = new FormatCell();
  	 			formatCellBodyLP.setFontName("Calibri");
  	 			formatCellBodyLP.setFontSize((short)11);
  	 			formatCellBodyLP.setBold(false);
    	 	    
  	 			formatCellHeaderLP = new FormatCell();
  	 			formatCellHeaderLP.setFontName("Calibri");
  	 			formatCellHeaderLP.setFontSize((short)11);
  	 			formatCellHeaderLP.setBold(true);

  	 			
  	 			model = new ModelAndView(new ViewExcel());
  	 			model.addObject("FORMAT_HEADER", formatCellHeaderLP);
  	 			model.addObject("FORMAT_CELL", formatCellBodyLP);
  	 			model.addObject("HEADER_VALUE", getHeaderExcel(ctx));
  	 			model.addObject("BODY_VALUE", getBody(beanResListadoPagos.getListadoPagos()));
  	 			model.addObject("NAME_SHEET", obtenerNombre(tipoOrden));
  	 		}else{
  	 			model = buscarPagos(beanPaginador, beanFilter, tipoOrden);
  	 			String mensaje = messageSource.getMessage("codError."+beanResListadoPagos.getCodError(), null, null);
  	 			model.addObject("tipoOrden", tipoOrden);
  	 			model.addObject("codError", beanResListadoPagos.getCodError());
  	 			model.addObject("tipoError", beanResListadoPagos.getTipoError());
  	 			model.addObject("descError", mensaje);
  	 			model.addObject("sessionSPID", sessionSPID);
  	 			String titulo = obtenerTitulo(tipoOrden);
  	 			model.addObject("titulo", titulo);
  	 		}
  	 		
  	 		if (!fieldIsEmpty(beanFilter.getField()) && !valorIsEmpty(beanFilter.getFieldValue())){
				generarContadoresPag1(model, beanResListadoPagos, beanFilter.getField(), beanFilter.getFieldValue(), tipoOrden, getArchitechBean());
			} else{
				generarContadoresPag2(model, beanResListadoPagos, tipoOrden, getArchitechBean());
			}
  	 		
  	 		
 		} catch (BusinessException e) {
 			showException(e);
	    }
	  
	    return model;
    }
	
	/**Ganera la nomenclatura del archivo a exportar
	 * @param tipoOrden objeto del tipo String
	 * @return String
	 */
	private String obtenerNombre (String tipoOrden){
		String nom = "";
		
		if ("OPD".equals(tipoOrden)){
			nom = "ORDENES_DV";
		} else if ("TSS".equals(tipoOrden)){
			nom = "TRASPASOS_SS";
		} else if ("OEC".equals(tipoOrden)){
			nom = "ORDENES_CO";
		} else if ("DEC".equals(tipoOrden)){
			nom = "DEVOLUCIONES_CO";
		} else if ("TES".equals(tipoOrden)){
			nom = "TRASPASOS_ES";
		} else if ("TEN".equals(tipoOrden)){
			nom = "TRASPASOS_EN";
		} else if ("TPR".equals(tipoOrden)){
			nom = "TRASPASOS_PR";
		} else if ("OES".equals(tipoOrden)){
			nom = "ORDENES_ES";
		} else if ("OEN".equals(tipoOrden)){
			nom = "ORDENES_EN";
		} 
		return nom;
	}

	/**
	 * @param field objeto del tipo String
	 * @return field objeto del tipo String
	 */
	private boolean fieldIsEmpty(String field) {
		return field == null || "".equals(field) || "undefined".equals(field);
	}
	
    /**
     * @param valor objeto del tipo String
     * @return valor objeto del tipo String
     */
    private boolean valorIsEmpty(String valor) {
		return valor == null || "".equals(valor) || "undefined".equals(valor);
	}
    /**
     * @param beanReqOrdeReparacion Objeto del tipo BeanReqListadoPagos
     * @param tipoOrden Objeto del tipo @RequestParam String 
     * @return ModelAndView
     */
    @RequestMapping(value = "/exportarTodoListadoPagos.do")
    public ModelAndView expTodosBitacoraAdmon(BeanReqListadoPagos beanReqOrdeReparacion,
    		@RequestParam("tipo") String tipoOrden){
   	 	ModelAndView model = null; 
   	 	BeanResListadoPagos beanResListadoPagos = null;
   	 	
   	 	BeanSessionSPID sessionSPID = new BeanSessionSPID();
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
   	 	
   	 	try { 			
 			beanResListadoPagos = bOPago.exportarTodoListadoPagos(tipoOrden, beanReqOrdeReparacion, getArchitechBean());
 			String mensaje = messageSource.getMessage("codError."+beanResListadoPagos.getCodError(),new Object[]{beanResListadoPagos.getNombreArchivo()}, null); 			
 			
 			model = new ModelAndView("listadoPagos");
 			model.addObject("beanResListadoPagos",beanResListadoPagos);
 			model.addObject("tipoOrden", tipoOrden);
 			
 			String titulo = obtenerTitulo(tipoOrden);
 			model.addObject("titulo", titulo);
 			
 			model.addObject("codError", beanResListadoPagos.getCodError());
 			model.addObject("tipoError", beanResListadoPagos.getTipoError());
 			model.addObject("descError", mensaje);
 			model.addObject("sessionSPID", sessionSPID);
 			generarContadoresPag(model, beanResListadoPagos);
 	   } catch (BusinessException e) {
 		  showException(e);
 	   }
        return model;
    }
    
    /**
     * @param listaBeanPago Objeto del tipo List<BeanPago>
     * @return List<List<Object>>
     */
    private List<List<Object>> getBody(List<BeanPago> listaBeanPago){
	 	List<Object> objListParam = null;
  	 	List<List<Object>> objList = null;
  	 	if(listaBeanPago!= null && 
		   !listaBeanPago.isEmpty()){
  		   objList = new ArrayList<List<Object>>();
  	
  		   for(BeanPago beanPago : listaBeanPago){ 
  	 		  objListParam = new ArrayList<Object>();
  	 		  objListParam.add(beanPago.getReferencia());
  	 		  objListParam.add(beanPago.getCveCola());
  	 		  objListParam.add(beanPago.getCveTran());
  	 		  objListParam.add(beanPago.getCveMecanismo());
  	 		  objListParam.add(beanPago.getCveMedioEnt());
  	 		  objListParam.add(beanPago.getFechaCaptura());
  	 		  objListParam.add(beanPago.getCveIntermeOrd());
  	 		  objListParam.add(beanPago.getCveIntermeRec());
  	 		  objListParam.add(beanPago.getImporteAbono());
  	 		  objListParam.add(beanPago.getEstatus());  	 		  	 		 
  	 		  objListParam.add(beanPago.getFolioPaquete());
  	 		  objListParam.add(beanPago.getFoloPago());
  	 		  objList.add(objListParam);
  	 	   }
  	   } 
   	return objList;
    }
    
    /**
     * @param ctx Objeto del tipo RequestContext
     * @return List<String>
     */
    private List<String> getHeaderExcel(RequestContext ctx){
   	 String []header = new String [] { 
   			 ctx.getMessage("moduloSPID.listadoPagos.text.refe"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.cola"),	
   			 ctx.getMessage("moduloSPID.listadoPagos.text.claveTran"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.macan"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.medioEnt"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.fchCap"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.ord"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.rec"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.importeAbono"),
   			 ctx.getMessage("moduloSPID.listadoPagos.text.est")
   	 	   }; 
   	 	return Arrays.asList(header);
    }
	
	/**
	 * @return BOPago
	 */
	public BOPago getBOPago() {
		return bOPago;
	}

	/**
	 * @param bOPago Objeto del tipo BOPago
	 */
	public void setBOPago(BOPago bOPago) {
		this.bOPago = bOPago;
	}
	
	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
	
	
	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResListadoPagos Objeto del tipo BeanResListadoPagos
	 */
	protected void generarContadoresPag1(ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos, String field, String valor, String tipoOrden, ArchitechSessionBean architechSessionBean) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResListadoPagos.getTotalReg();
			
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImpPagina = BigDecimal.ZERO;
			BigDecimal totalImporte = BigDecimal.ZERO;
			
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImpPagina = totalImpPagina.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			
			try {
				totalImporte = bOPago.buscarPagosMonto(field, valor, tipoOrden, getArchitechBean());
			} catch (BusinessException e) {
				showException(e);
			}
			
			
			modelAndView.addObject("importePagina", totalImpPagina.toString());
			modelAndView.addObject("importeTotal", totalImporte.toString());
			
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
		}
	}
	
	
	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResListadoPagos Objeto del tipo BeanResListadoPagos
	 */
	protected void generarContadoresPag2(ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos, String tipoOrden, ArchitechSessionBean architechSessionBean) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResListadoPagos.getTotalReg();
			
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImpPagina = BigDecimal.ZERO;
			BigDecimal totalImporte = BigDecimal.ZERO;
			
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImpPagina = totalImpPagina.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			
			try {
				totalImporte = bOPago.obtenerListadoPagosMontos(tipoOrden, getArchitechBean());
			} catch (BusinessException e) {
				showException(e);
			}
			modelAndView.addObject("importePagina", totalImpPagina.toString());
			modelAndView.addObject("importeTotal", totalImporte.toString());
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
		}
	}
	
	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResListadoPagos Objeto del tipo BeanResListadoPagos
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResListadoPagos beanResListadoPagos) {
		if (beanResListadoPagos.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResListadoPagos.getTotalReg();
			
			if (!"1".equals(beanResListadoPagos.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina()) - 1);
			}
			if (beanResListadoPagos.getTotalReg() >= 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResListadoPagos.getBeanPaginador().getPagina());
			}
			
			BigDecimal totalImporte = BigDecimal.ZERO;
			for (BeanPago beanPago : beanResListadoPagos.getListadoPagos()){
				totalImporte = totalImporte.add(new BigDecimal(beanPago.getImporteAbono()));
			}
			modelAndView.addObject("importePagina", totalImporte.toString());
			
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
		}
	}
}
