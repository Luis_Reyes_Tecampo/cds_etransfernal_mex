/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerBitacoraAdmon.java
 * 
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *   1.0.1 06 Agu 2014 10:03:00 CST 2013 Rodolfo MȨndez  IDS 		Modificacion de sesion existente cuando cambia de nodo
 */


package mx.isban.eTransferNal.controllers.publico;


import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LookAndFeel;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.configuracion.ConfiguracionConfig;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.logging.Level;
import mx.isban.agave.sesiones.SesionesConfig;
import mx.isban.agave.sesiones.SesionesConfigs;
import mx.isban.agave.sesiones.beans.ConsultaSesionDTO;
import mx.isban.eTransferNal.servicio.moduloCDA.BOUsuario;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerPublico extends Architech{

        /**
         * Numero serial 
         */
        private static final long serialVersionUID = 6671774210038346253L;

    /**
     * Componente de negocio que administra los usuarios con
     * acceso a transfer. 
     */
    private BOUsuario boUsuario;

   /**
    * Metodo que se utiliza para recibir la peticion de sesionExistente.go.
    *    Valida si el usuario proviene de la misma IP, en caso de ser la misma IP continua dentro de la aplicacion
    * @param request peticion al componente
    * @return ModelAndView Objeto del tipo ModelAndView
    */
    @RequestMapping(value = "sesionExistente.go")
    public ModelAndView sesionExistente(HttpServletRequest request ){
    	ModelAndView pag = null;
    	ArchitechSessionBean arqSesion =new ArchitechSessionBean();;
    	HttpSession sesion = request.getSession();
    	LookAndFeel lyF = new LookAndFeel();
      debug("\n**************************\nENTRANDO SesionExistente.go******************");
      String paginaSiguiente = null;
      String mensaje=null;
      String sesionExistente ="/jsp/public/sesionExistente.jsp";
      String paginaContinua ="/principal/moduloCDAInit.do";
      String ipCliente = request.getHeader("iv-remote-address") != null ? request.getHeader("iv-remote-address") : request.getRemoteAddr();
      String usr =  request.getHeader("iv-user") != null ? request.getHeader("iv-user") : "";
      if(usr.charAt(0) == 'Y'){
    	  usr = usr.substring(1, usr.length());
      }
      String ipAlmacenada= null;
      ConsultaSesionDTO consultaSesionesDTO = new ConsultaSesionDTO();
      consultaSesionesDTO.setIdUsuario(usr);
      consultaSesionesDTO.setIdAplic(getConfigParam(ConfiguracionConfig.ID_CONFIG_APP.name()));
       consultaSesionesDTO.setIdApp("1");
      ipAlmacenada = consultaIP(consultaSesionesDTO);
      mensaje="IP Peticion:=".concat(ipCliente).concat("\tIP Almacenada=").concat(ipAlmacenada);
      debug(mensaje);
      if(ipCliente!=null && ipCliente.equals(ipAlmacenada)){
    	  actualizaSesion(consultaSesionesDTO);
          paginaSiguiente=paginaContinua;
          arqSesion.setUsuario(usr);
          lyF.setArchitechBean(arqSesion);
          lyF.showInfoLyF();
          //sesion.setAttribute("ArchitechSession", arqSesion);
          sesion.setAttribute("LyFBean", lyF);
          sesion.setAttribute("nombreUsuario",usr);
      }else{
    	  
          paginaSiguiente=sesionExistente;
      }
      pag = new ModelAndView(paginaSiguiente);
      return pag;
    }

    /**Metodo que se utiliza para recibir la peticion de inicio.go
     * @param request Objeto del tipo HttpServletRequest
     * @return ModelAndView Objeto del tipo ModelAndView
     * @throws BusinessException Exception de negocio
     */
     @RequestMapping(value = "inicio.go")
     public ModelAndView inicio(HttpServletRequest request ) throws BusinessException{
         ArchitechSessionBean architechSessionBean = null;
         HttpSession session = request.getSession();
         session.setAttribute("nuevo", "nuevo");
         String perfiles = request.getHeader("iv-groups");
         String user = request.getHeader("iv-user");
         if(perfiles!=null){
                 perfiles = perfiles.replaceAll("\"", "" );
         }
         if(user!= null){
                 user = user.replaceAll("\"", "" );
                 user = user.substring(1);
         }
         architechSessionBean = getArchitechBean();
         if(architechSessionBean == null){
                 architechSessionBean = new ArchitechSessionBean();
         }
         architechSessionBean.setPerfil(perfiles);
         architechSessionBean.setUsuario(user);
         boUsuario.guardarUsuario(getArchitechBean());
         return new ModelAndView("inicioFrame");
     }

     /**
      * Metodo get que regresa la referencia al servicio BoUsuario
      * @return boUsuario Servicio BoUsuario
      */
        public BOUsuario getBoUsuario() {
                return boUsuario;
        }
     /**
      * Metodo set que modifica la referencia al servicio BoUsuario
      * @param boUsuario Servicio BoUsuario
      */
        public void setBoUsuario(BOUsuario boUsuario) {
                this.boUsuario = boUsuario;
        }
        /**
         * Metodo para consultar la IP ya que la consulta de arquitectura no contiene ese dato
         * @param conDTO datos de consulta
         * @return ip de retorno
         */
        private  String consultaIP(ConsultaSesionDTO conDTO){
            String ip = null;
            RequestMessageDataBaseDTO requestDTO = new RequestMessageDataBaseDTO();
            DataAccess datos = null;
            List<HashMap<String, Object>> resultQuery = null;
           
            try {
                StringBuffer sql = new StringBuffer();
                String canal = SesionesConfigs.getInstance().getConfig(SesionesConfig.ID_CANAL);
                sql.append("SELECT ip_usuario as IP FROM EBE_APP_SESIONES  WHERE id_aplic = (SELECT ID_APLIC FROM EBE_APLIC WHERE IDENTIFICADOR = '").append(conDTO.getIdAplic()).append("' ) AND usuario = '").append(conDTO.getIdUsuario()).append('\'');
                requestDTO.setTimeout(1L);
                requestDTO.setTypeOperation(0);
                requestDTO.setCodeOperation("CONCONTAC0000");
                requestDTO.setQuery(sql.toString());
                datos = DataAccess.getInstance(requestDTO, this);
                ResponseMessageDataBaseDTO respDTO = (ResponseMessageDataBaseDTO)datos.execute(canal);
                debug((new StringBuilder()).append("Code Error: ").append(respDTO.getCodeError()).toString());
                debug((new StringBuilder()).append("Message Error: ").append(respDTO.getMessageError()).toString());
                if("DAE000".equals(respDTO.getCodeError()))
                {
                    resultQuery = respDTO.getResultQuery();
                    if(null  != resultQuery && null != resultQuery.get(0) )
                    {
                        ip = resultQuery.get(0).get("IP").toString(); 
                    }
                }
            }
            catch(ExceptionDataAccess e)
            {
                showException(e, Level.ERROR);
            }
            return ip;
        }
        /**
         * actualiza la sesion para permitir el acceso
         * @param conDTO datos para actualizacion
         */
        private void actualizaSesion(ConsultaSesionDTO conDTO){
            RequestMessageDataBaseDTO requestDTO = new RequestMessageDataBaseDTO();
            DataAccess datos = null;
        
            try {
                StringBuffer sql = new StringBuffer();
                String canal = SesionesConfigs.getInstance().getConfig(SesionesConfig.ID_CANAL);
                sql.append("UPDATE  EBE_APP_SESIONES set estado=0 WHERE id_aplic = (SELECT ID_APLIC FROM EBE_APLIC WHERE IDENTIFICADOR = '").append(conDTO.getIdAplic()).append("' ) AND usuario = '").append(conDTO.getIdUsuario()).append('\'');
                requestDTO.setTimeout(1L);
                requestDTO.setTypeOperation(2);
                requestDTO.setCodeOperation("CONCONTAC0000");
                requestDTO.setQuery(sql.toString());
                datos = DataAccess.getInstance(requestDTO, this);
                ResponseMessageDataBaseDTO respDTO = (ResponseMessageDataBaseDTO)datos.execute(canal);
                debug((new StringBuilder()).append("Code Error: ").append(respDTO.getCodeError()).toString());
                debug((new StringBuilder()).append("Message Error: ").append(respDTO.getMessageError()).toString());
            }
            catch(ExceptionDataAccess e)
            {
                showException(e, Level.ERROR);
            }
        }

}

