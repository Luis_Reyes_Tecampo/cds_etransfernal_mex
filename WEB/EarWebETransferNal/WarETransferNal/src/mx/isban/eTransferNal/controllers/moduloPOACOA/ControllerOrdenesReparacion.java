/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerOrdenesReparacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:26 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BOReparacion;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportarOrdenes;

/**
 * Class ControllerOrdenesReparacion.
 *
 * Clase tipo controller utilizada para cargar la pantalla de Ordenens de
 * reparacion y mapear las funcionalides y disparar los flujos de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerOrdenesReparacion extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5164918115190073517L;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "ordenesReparacionPC";

	/** La constante BEAN_ORDENES. */
	private static final String BEAN_ORDENES = "beanResOrdenReparacion";

	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;

	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La variable que contiene informacion con respecto a: bo reparacion. */
	private BOReparacion boReparacion;

	/** La constante utilsExportar. */
	private static final UtileriasExportarOrdenes utilsExportar = UtileriasExportarOrdenes.getUtils();

	/**
	 * Exportar ordenes.
	 * 
	 * Metodo para generar la exportacion
	 * en la pantalla.
	 *
	 * @param beanResOrdenReparacion El objeto: bean res orden reparacion
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarOrdenesReparacion.do")
	public ModelAndView exportarOrdenes(@Valid @ModelAttribute(BEAN_ORDENES) BeanResOrdenReparacion beanResOrdenReparacion, BindingResult bindingResult) {
		/** Recuperar atributos usando el context **/
  	    HttpServletRequest req = utilsExportar.getServlet();
  	    /** Validacion de los datos de entrada **/
  	    if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeaderRep = null;
		FormatCell formatCellBodyRep = null;
		ModelAndView model = null;
		RequestContext ctx = new RequestContext(req);
		/** Creacion del cuerpo del archivo **/
		formatCellBodyRep = new FormatCell();
		formatCellBodyRep.setFontName("Calibri");
		formatCellBodyRep.setFontSize((short) 11);
		formatCellBodyRep.setBold(false);
		formatCellHeaderRep = new FormatCell();
		formatCellHeaderRep.setFontName("Calibri");
		formatCellHeaderRep.setFontSize((short) 11);
		formatCellHeaderRep.setBold(true);
		model = new ModelAndView(new ViewExcel());
		model.addObject("FORMAT_HEADER", formatCellHeaderRep);
		model.addObject("FORMAT_CELL", formatCellBodyRep);
		model.addObject("HEADER_VALUE", utilsExportar.getHeaderExcel(ctx));
		model.addObject("BODY_VALUE", utilsExportar.getBody(beanResOrdenReparacion.getListaOrdenesReparacion()));
		model.addObject("NAME_SHEET", "OrdenesReparacion");
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Consulta avisos traspasasos.
	 * 
	 * Muestra la pantalla con la informacion a la que hace referencia esta
	 * funcionbilidad.
	 *
	 * @param beanModulo       El objeto: bean modulo
	 * @param beanPaginador    El objeto: bean paginador
	 * @param beanOrdenamiento El objeto: bean ordenamiento
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/consultaOrdenesReparacion.do")
	public ModelAndView consultaAvisosTraspasasos(@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento, BindingResult bindingResult) {
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		ModelAndView model = null;
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			beanResOrdenReparacion = boReparacion.obtenerOrdenesReparacion(beanModulo, beanPaginador, getArchitechBean(),
					beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			String mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO + beanResOrdenReparacion.getCodError(),
					null, null);
			model = new ModelAndView(PAGINA_PRINCIPAL);
			/** Seteo de datos al modelo **/
			model.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
			model.addObject(BEAN_ORDENES, beanResOrdenReparacion);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(ConstantesWebPOACOA.SORT_FIELD, beanOrdenamiento.getSortField());
			model.addObject(ConstantesWebPOACOA.SORT_TYPE, beanOrdenamiento.getSortType());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			generarContadoresPaginacion(beanModulo, beanResOrdenReparacion, model, beanOrdenamiento.getSortField(),
					beanOrdenamiento.getSortType());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Enviar ordenes reparacion.
	 * 
	 * Metodo para enviar los registros
	 * seleccionados a reparacion
	 *
	 * @param beanModulo            El objeto: bean modulo
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param beanOrdenamiento      El objeto: bean ordenamiento
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/enviarAReparacion.do")
	public ModelAndView enviarOrdenesReparacion(@Valid @ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			BeanReqOrdeReparacion beanReqOrdeReparacion, BeanOrdenamiento beanOrdenamiento, BindingResult bindingResult) {
		BeanResOrdenReparacion beanResOrdenReparacion = null;
		ModelAndView model = new ModelAndView(PAGINA_PRINCIPAL);
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			beanResOrdenReparacion = boReparacion.enviarOrdenesReparacion(beanModulo, beanReqOrdeReparacion,
					getArchitechBean());
			String mensaje = "";
			if (beanResOrdenReparacion != null) {
				model.addObject(BEAN_ORDENES,beanResOrdenReparacion);
				mensaje = messageSource.getMessage(Constantes.COD_ERROR_PUNTO + beanResOrdenReparacion.getCodError(),null, null);
				/** Seteo de datos al modelo **/
				model.addObject(Constantes.COD_ERROR, beanResOrdenReparacion.getCodError());
				model.addObject(Constantes.TIPO_ERROR, beanResOrdenReparacion.getTipoError());
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(BEAN_ORDENES,new BeanResOrdenReparacion());
				model.addObject(Constantes.COD_ERROR, Errores.EC00011B);
				model.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
				model.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
			}
			
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			generarContadoresPaginacion(beanModulo, beanResOrdenReparacion, model, beanOrdenamiento.getSortField(),
					beanOrdenamiento.getSortType());
		} catch (BusinessException e) {
			showException(e);
		}
		/** Retorno del modelo **/
		return model;
	}

	/**
	 * Generar contadores paginacion.
	 *
	 * Funcion para validar la paginacion en base a la respuesta 
	 * obtenida.
	 * 
	 * @param modulo                 El objeto: modulo
	 * @param beanResOrdenReparacion El objeto: bean res orden reparacion
	 * @param model                  El objeto: model
	 * @param sortField              El objeto: sort field
	 * @param sortType               El objeto: sort type
	 */
	protected void generarContadoresPaginacion(BeanModulo modulo, BeanResOrdenReparacion beanResOrdenReparacion,
			ModelAndView model, String sortField, String sortType) {
		if (beanResOrdenReparacion.getTotalReg() > 0) {
			Integer regIniGenCon = 1;
			Integer regFinGenCon = beanResOrdenReparacion.getTotalReg();

			if (!"1".equals(beanResOrdenReparacion.getBeanPaginador().getPagina())) {
				regIniGenCon = 20 * (Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResOrdenReparacion.getTotalReg() >= 20
					* Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina())) {
				regFinGenCon = 20 * Integer.parseInt(beanResOrdenReparacion.getBeanPaginador().getPagina());
			}

			BigDecimal totalPaginaOrdRep = BigDecimal.ZERO;
			BigDecimal totalImporteOrdRep = BigDecimal.ZERO;
			for (BeanOrdenReparacion beanOrdenReparacion : beanResOrdenReparacion.getListaOrdenesReparacion()) {
				totalPaginaOrdRep = totalPaginaOrdRep
						.add(new BigDecimal(beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono()));
			}
			try {
				totalImporteOrdRep = boReparacion.obtenerOrdenesReparacionMontos(modulo, getArchitechBean(), sortField,
						sortType);
			} catch (BusinessException e) {
				showException(e);
			}
			/** seteo de datos al modelo actual **/
			model.addObject("importePagina", totalPaginaOrdRep.toString());
			model.addObject("importeTotal", totalImporteOrdRep.toString());
			model.addObject("regIni", regIniGenCon);
			model.addObject("regFin", regFinGenCon);
		}
	}

	/**
	 * Obtener el objeto: message source.
	 *
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Definir el objeto: message source.
	 *
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID.
	 *
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * Definir el objeto: bo init modulo SPID.
	 *
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
	
	/**
	 * Obtener el objeto: bo reparacion.
	 *
	 * @return El objeto: bo reparacion
	 */
	public BOReparacion getBoReparacion() {
		return boReparacion;
	}
	
	/**
	 * Definir el objeto: bo reparacion.
	 *
	 * @param boReparacion El nuevo objeto: bo reparacion
	 */
	public void setBoReparacion(BOReparacion boReparacion) {
		this.boReparacion = boReparacion;
	}

	
}
