/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   11/12/2013 16:54:08 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.moduloCDA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqMonCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistMontos;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistVolumen;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BOMonitorCDAHist;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Clase que se encarga de que se encarga de recibir y procesar 
 * las peticiones para la consulta de Movimientos Historicos CDAs
 *
 */
@Controller
public class ControllerMonitorCDAHist extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -8396037496907638074L;
	
	/**
	 * Referencia al servicio de consulta Mov CDA
	 */
	 private BOMonitorCDAHist boMonitorCDAHist;

	 /**
	  * Metodo que se utiliza para mostrar la pagina de monitorCDA
	  * monitor CDA
	  * @param req Objeto del tipo HttpServletRequest
	  * @param res Objeto del tipo HttpServletResponse
	  * @return ModelAndView Objeto del tipo ModelAndView
	  */
	 @RequestMapping(value = "/muestraMonitorCDAHist.do")
	 public ModelAndView muestraMonitorCDAHist(HttpServletRequest req,
				HttpServletResponse res) {
		 ModelAndView modelAndView = null;
		 BeanResMonitorCdaHistBO beanResMonitorCDA = null;
		 BeanResMonitorCdaHistMontos beanResMonitorCdaMontos = new BeanResMonitorCdaHistMontos();
		 beanResMonitorCdaMontos.setMontoCDAConfirmadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAContingencia(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAEnviadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAOrdRecAplicadas(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDAPendEnviar(Constantes.CERO_DEC);
		 beanResMonitorCdaMontos.setMontoCDARechazadas(Constantes.CERO_DEC);
		 
		 BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen = new BeanResMonitorCdaHistVolumen();
		 beanResMonitorCdaVolumen.setVolumenCDAConfirmadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAContingencia(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAEnviadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAOrdRecAplicadas(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDAPendEnviar(Constantes.CERO);
		 beanResMonitorCdaVolumen.setVolumenCDARechazadas(Constantes.CERO);
		 
		 beanResMonitorCDA = new BeanResMonitorCdaHistBO();
		 beanResMonitorCDA.setBeanResMonitorCdaMontos(beanResMonitorCdaMontos);
		 beanResMonitorCDA.setBeanResMonitorCdaVolumen(beanResMonitorCdaVolumen);
		 modelAndView = new ModelAndView("monitorCDAHist","beanResMonitorCDA",beanResMonitorCDA);
		 return modelAndView;
	 }
	 
	/**
	 * Metodo que solicita la informacion para el monitor CDA
	 * @param req Objeto del tipo HttpServletRequest
	 * @param res Objeto del tipo HttpServletResponse
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value="/consInfMonCDAHist.do")
	public ModelAndView consInfMonCDA(HttpServletRequest req,
			HttpServletResponse res){
		Utilerias utilerias = Utilerias.getUtilerias();
		ModelAndView modelAndView = null;      
		BeanReqMonCDAHist beanReqMonCDAHist = new BeanReqMonCDAHist();
		RequestContext ctx = new RequestContext(req);
	    BeanResMonitorCdaHistBO beanResMonitorCDA = null;
	    beanReqMonCDAHist.setFechaOperacion(utilerias.getString(req.getParameter("paramFechaOperacion")));
	    
	    if(beanReqMonCDAHist.getFechaOperacion()!=null && "".equals(beanReqMonCDAHist.getFechaOperacion())){
	    	modelAndView = new ModelAndView("monitorCDAHist","beanResMonitorCDA",beanResMonitorCDA);
	    	String mensaje = ctx.getMessage("codError."+Errores.ED00063V);
	     	modelAndView.addObject("codError", Errores.ED00063V);
	     	modelAndView.addObject("tipoError", Errores.TIPO_MSJ_ALERT);
	     	modelAndView.addObject("descError", mensaje);
	     	return modelAndView;
	    }
        try {
    	    beanResMonitorCDA = boMonitorCDAHist.obtenerDatosMonitorCDA(beanReqMonCDAHist, getArchitechBean());
    	    debug("consInfMonCDA boMonitorCDA.obtenerDatosMonitorCDA Codigo de error:"+beanResMonitorCDA.getCodError());
    	    modelAndView = new ModelAndView("monitorCDAHist","beanResMonitorCDA",beanResMonitorCDA);
	        String mensaje = ctx.getMessage("codError."+beanResMonitorCDA.getCodError());
	     	modelAndView.addObject("codError", beanResMonitorCDA.getCodError());
	     	modelAndView.addObject("tipoError", beanResMonitorCDA.getTipoError());
	     	modelAndView.addObject("descError", mensaje);
 	    } catch (BusinessException e) {
 		 showException(e);
 	    }
	       return modelAndView;	    
	 }

	/**
	 * Metodo get que obtiene el valor de la propiedad boMonitorCDAHist
	 * @return boMonitorCDAHist Objeto de tipo @see BOMonitorCDAHist
	 */
	public BOMonitorCDAHist getBoMonitorCDAHist() {
		return boMonitorCDAHist;
	}

	/**
	 * Metodo que modifica el valor de la propiedad boMonitorCDAHist
	 * @param boMonitorCDAHist Objeto de tipo @see BOMonitorCDAHist
	 */
	public void setBoMonitorCDAHist(BOMonitorCDAHist boMonitorCDAHist) {
		this.boMonitorCDAHist = boMonitorCDAHist;
	}

	
	
	

}
