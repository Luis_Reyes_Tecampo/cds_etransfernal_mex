/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerDetRecepOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   28/09/2015 11:47:00 INDRA		        Creacion
 *   1.1   17/04/2018      SMEZA		VECTOR      Creacion
 *
 */
package mx.isban.eTransferNal.controllers.moduloCDA;

/**
 * Anexo de Imports para la funcionalidad del Modulo Admon Saldos - Detalle
 * 
 * @author FSW-Vector
 * @sice 17 Abril 2018
 *
 */
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecOpRecep;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOpera;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecComun;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloCDA.BODetRecepOperacion;
import mx.isban.eTransferNal.servicio.moduloCDA.BORecepOperacion;
import mx.isban.eTransferNal.utilerias.SPEI.ConstantesRecepOpePantalla;
import mx.isban.eTransferNal.utilerias.SPEI.UtilsExportarRecepcionOperacion;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar las peticiones
 * del detalle de recepcion de pagos.
 *
 * @author FSW-Vector
 * @since 26/09/2018
 */
@Controller
public class ControllerDetRecepOperacion extends Architech {

	/** Constante del Serial Version. */
	private static final long serialVersionUID = -5366481743702190274L;

	/** Constante de pantalla. */
	private static final String PAG_DET_RECEP_ADMON_SALDOS = "detRecepcionOperacion";

	/** Constante de pantalla. */
	private static final String PAG_DET_RECEP_SPEI = "detRecepcionOperaSPEI";

	/** Constante con la cadena codError. */
	private static final String COD_ERROR_PUNTO = "codError.";

	/** Constante con la cadena codError. */
	private static final String COD_ERROR = "codError";

	/** Constante con la cadena tipoError. */
	private static final String TIPO_ERROR = "tipoError";

	/** Constante con la cadena tipoError. */
	private static final String PAGINA = "pagina";

	/** Constante con la cadena descError. */
	private static final String DESC_ERROR = "descError";

	/** Constante con la cadena sessionBean. */
	private static final String SESSION = "sessionBean";

	/** The Constant FECHA_OPER. */
	private static final String FECHA_OPER = "fechaOperacion";

	/** Referencia al servicio del catalogo. */
	private BODetRecepOperacion boDetRecepOperacion;

	/** La variable que contiene informacion con respecto a: b O recep operacion. */
	private BORecepOperacion bORecepOperacion;
	/** La constante util. */
	public static final UtilsExportarRecepcionOperacion util = UtilsExportarRecepcionOperacion.getInstance();

	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modeloVista;

	/** Propiedad del tipo ResourceBundleMessageSource que almacena el valor de messageSource. */
	private ResourceBundleMessageSource messageSource;

	/**
	 * Metodo que se utiliza para consultar el detalle de recepcion de pagos.
	 *
	 * @param bean                Objeto del tipo BeanReqConsTranSpeiRec
	 * @param res                 Objeto del tipo HttpServletResponse
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @param result              Objeto del tipo BindingResult
	 * @return ModelAndView Objeto del tipo ModelAndView
	 */
	@RequestMapping(value = "/muestraDetRecepOperacion.do")
	public ModelAndView muestraDetRecepOperacion(BeanReqTranSpeiRec bean, HttpServletResponse res,
			@ModelAttribute(ConstantesRecepOpePantalla.BEAN_TRANS) @Valid BeanTranSpeiRecOper beanTranSpeiRecOper,
			BindingResult result) {
		/** Se revisan mensajes de validaciones JSR-303 */
		if (result.hasFieldErrors()) {
			error("Validaciones JSR-303, valores de entrada incorrectos.");
			error(result.getAllErrors().get(0).getDefaultMessage());
		}
		/** se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(bean);
		return validarInformacion(bean, historico, beanTranSpeiRecOper);

	}

	/**
	 * Validar informacion.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param historico          El objeto: historico
	 * @param beanTranSpeiRecOper the bean tran spei rec oper
	 * @return Objeto model and view
	 */
	private ModelAndView validarInformacion(BeanReqTranSpeiRec beanReqTranSpeiRec, boolean historico,
			BeanTranSpeiRecOper beanTranSpeiRecOper) {
		/** inicializa las variables **/
		modeloVista = null;
		BeanResTranSpeiRec beanReqTran = util.validarLista(beanReqTranSpeiRec);
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();

		if (!beanReqTran.getListBeanTranSpeiRec().isEmpty()) {
			modeloVista = consultaDetalle(beanReqTran.getListBeanTranSpeiRec().get(0), historico, beanReqTranSpeiRec);
			return modeloVista;
		}

		String mensaje = "";

		try {
			/** Se asigna el submenu y el nombre de la tabla **/
			/** re sealiza la consulta de los datos **/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqTranSpeiRec,
					getArchitechBean(), historico, beanTranSpeiRecOper);

			/** Se llama a la vista a mostrar o pantalla **/
			modeloVista = util.vistaMostrar(beanReqTranSpeiRec, recepOperaAdmonSaldos, historico);

			mensaje = messageSource.getMessage(COD_ERROR_PUNTO + beanReqTran.getBeanError().getCodError(), null, null);
			modeloVista.addObject(Constantes.COD_ERROR, beanReqTran.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, beanReqTran.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject("beanTranSpeiRecOper", beanTranSpeiRecOper);
			modeloVista.addObject(FECHA_OPER, beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
		} catch (BusinessException e) {
			showException(e);
			modeloVista = util.vistaMostrar(beanReqTranSpeiRec, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(COD_ERROR_PUNTO + recepOperaAdmonSaldos.getBeanError().getCodError(),
					null, null);
			modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage());
		}

		return modeloVista;
	}

	/**
	 * Consulta detalle.
	 *
	 * @param beans              El objeto: beans
	 * @param historico          El objeto: historico
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto model and view
	 */
	private ModelAndView consultaDetalle(BeanTranSpeiRecOper beans, boolean historico,
			BeanReqTranSpeiRec beanReqTranSpeiRec) {
		// SE INICIALIZA EL OBJETO
		BeanDetRecepOperacionDAO detAdmonSaldos = new BeanDetRecepOperacionDAO();
		BeanDetRecepOperacionDAO detBean = new BeanDetRecepOperacionDAO();
		ArchitechSessionBean sessionBean = getArchitechBean();
		modeloVista = null;
		if (beanReqTranSpeiRec.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_DIA)
				|| beanReqTranSpeiRec.getBeanComun().getNombrePantalla()
						.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO)) {
			modeloVista = new ModelAndView(PAG_DET_RECEP_SPEI);
		} else {
			modeloVista = new ModelAndView(PAG_DET_RECEP_ADMON_SALDOS);
		}

		// SE INICIALIZA lo datos gral
		detAdmonSaldos.setBeanDetRecepOperacion(new BeanDetRecepOperacion());
		detAdmonSaldos.getBeanDetRecepOperacion().setDetalleGral(new BeanDetRecepOpera());
		/** llenar las listas para la pantalla . */
		detAdmonSaldos.setParametro("SALDOS");
		detAdmonSaldos.getBeanDetRecepOperacion().getDetalleGral()
				.setFchOperacion(beans.getBeanDetalle().getDetalleGral().getFchOperacion());
		detAdmonSaldos.getBeanDetRecepOperacion().getDetalleGral()
				.setCveMiInstitucion(beans.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		detAdmonSaldos.getBeanDetRecepOperacion().getDetalleGral()
				.setCveInstOrd(beans.getBeanDetalle().getDetalleGral().getCveInstOrd());
		detAdmonSaldos.getBeanDetRecepOperacion().getDetalleGral()
				.setFolioPaquete(beans.getBeanDetalle().getDetalleGral().getFolioPaquete());
		detAdmonSaldos.getBeanDetRecepOperacion().getDetalleGral()
				.setFolioPago(beans.getBeanDetalle().getDetalleGral().getFolioPago());

		/** llenar las listas para la pantalla . */
		detBean.setBeanDetRecepOperacion(beans.getBeanDetalle());
		detBean.getBeanDetRecepOperacion().setNombrePantalla(beanReqTranSpeiRec.getBeanComun().getNombrePantalla());

		/** En casod e existir el histoico llama una pagina **/
		if (historico) {
			detBean.setParametro("RECEPCIONHTO");
			modeloVista.addObject(PAGINA, "rOH");
		} else {
			detBean.setParametro("RECEPCION");
			modeloVista.addObject(PAGINA, "rO");
		}

		/** setea los valores **/
		modeloVista.addObject("fchOperacion", beans.getBeanDetalle().getDetalleGral().getFchOperacion());
		modeloVista.addObject("cveMiInstitucion", beans.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
		modeloVista.addObject("cveInstOrd", detBean.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd());
		modeloVista.addObject("folioPaq", beans.getBeanDetalle().getDetalleGral().getFolioPaquete());
		modeloVista.addObject("folioPag", beans.getBeanDetalle().getDetalleGral().getFolioPago());
		modeloVista.addObject("nombrePantalla", beanReqTranSpeiRec.getBeanComun().getNombrePantalla());
		modeloVista.addObject("beanDatos", detBean);
		modeloVista.addObject("usuario", beans.getUsuario());
		modeloVista.addObject(SESSION, sessionBean);
		modeloVista.addObject(FECHA_OPER, beanReqTranSpeiRec.getBeanComun().getFechaOperacion());

		return modeloVista;
	}

	/**
	 * Regresar pantalla det recep.
	 *
	 * @param beans El objeto: beans
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/regresarPantallaDetRecep.do")
	public ModelAndView regresarPantallaDetRecep(BeanDetRecepOperacionDAO beans) {
		modeloVista = null;
		BeanReqTranSpeiRec beanReqTranSpeiRec = new BeanReqTranSpeiRec();

		// inicializa el bean de comun
		beanReqTranSpeiRec.setBeanComun(new BeanTranSpeiRecComun());
		/** se asigna el ombre de la pantalla para buscar si es historico **/
		beanReqTranSpeiRec.getBeanComun().setNombrePantalla(beans.getBeanDetRecepOperacion().getNombrePantalla());
		/** se declara la variable de historico **/
		boolean historico = util.obtenerHistorico(beanReqTranSpeiRec);
		// inicilia objetos
		BeanResTranSpeiRec recepOperaAdmonSaldos = new BeanResTranSpeiRec();
		BeanReqTranSpeiRec beanReqConsTranSpei = new BeanReqTranSpeiRec();
		String mensaje = "";
		try {
			/** re sealiza la consulta de los datos **/
			recepOperaAdmonSaldos = bORecepOperacion.consultaTodasTransf(new BeanResTranSpeiRec(), beanReqConsTranSpei,
					getArchitechBean(), historico, new BeanTranSpeiRecOper());
			/** Se llama a la vista a mostrar o pantalla **/
			modeloVista = util.vistaMostrar(beanReqTranSpeiRec, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(COD_ERROR_PUNTO + recepOperaAdmonSaldos.getBeanError().getCodError(),
					null, null);
			modeloVista.addObject(Constantes.COD_ERROR, recepOperaAdmonSaldos.getBeanError().getCodError());
			modeloVista.addObject(Constantes.TIPO_ERROR, recepOperaAdmonSaldos.getBeanError().getTipoError());
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			modeloVista.addObject(ConstantesRecepOpePantalla.BEAN_TRANS, new BeanTranSpeiRecOper());
			ArchitechSessionBean sessionBean = getArchitechBean();
			modeloVista.addObject(SESSION, sessionBean);

		} catch (BusinessException e) {
			showException(e);
			modeloVista = util.vistaMostrar(beanReqTranSpeiRec, recepOperaAdmonSaldos, historico);
			mensaje = messageSource.getMessage(COD_ERROR_PUNTO + recepOperaAdmonSaldos.getBeanError().getCodError(),
					null, null);
			modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
			modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
			modeloVista.addObject(Constantes.DESC_ERROR, mensaje);
			error(ConstantesRecepOpePantalla.ERROR + e.getMessage(), this.getClass());
		}
		return modeloVista;

	}

	/**
	 * Update cta receptora.
	 *
	 * @param cuentaReceptora     the cuenta receptora
	 * @param beans               the beans
	 * @param result              the result
	 * @param beanTranSpeiRecOper the bean tran spei rec oper
	 * @param beanReqTranSpeiRec  the bean req tran spei rec
	 * @return the model and view
	 */
	@RequestMapping(value = "/updateCtaReptora.do")
	public ModelAndView updateCtaReceptora(String cuentaReceptora, BeanDetRecepOperacionDAO beans, BindingResult result,
			BeanTranSpeiRecOper beanTranSpeiRecOper, BeanReqTranSpeiRec beanReqTranSpeiRec) {
		BeanReqTranSpeiRec bean = new BeanReqTranSpeiRec();
		/** Se revisan mensajes de validaciones JSR-303 */
		if (result.hasFieldErrors()) {
			error("Validaciones JSR-303, valores de entrada incorrectos.");
			error(result.getAllErrors().get(0).getDefaultMessage());
		}
		// inicializa el bean de comun
		bean.setBeanComun(new BeanTranSpeiRecComun());
		bean.getBeanComun().setNombrePantalla(beans.getBeanDetRecepOperacion().getNombrePantalla());
		boolean historico = util.obtenerHistorico(bean);
		// variable que guarda la cuentaReceptoraAnterior
		String ctaRecepAnt = beans.getBeanDetRecepOperacion().getReceptor().getNumCuentaRec();

		modeloVista = null;
		String mensaje = null;
		if (beans.getBeanDetRecepOperacion().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_DIA)
				|| beans.getBeanDetRecepOperacion().getNombrePantalla()
						.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO)) {
			modeloVista = new ModelAndView(PAG_DET_RECEP_SPEI);
		} else {
			modeloVista = new ModelAndView(PAG_DET_RECEP_ADMON_SALDOS);
		}
		/** inicializa las variables de fechas **/
		String fecha = beans.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion();
		String fechaForm = "";
		/** validacion de las fechas **/
		if (fecha.length() > 10) {
			String[] fecha2 = fecha.split("-");
			String ano = fecha2[0];
			String mes = fecha2[1];
			String dia = fecha2[2];
			String[] diaSplit = dia.split(" ");
			String dia1 = diaSplit[0];
			fechaForm = dia1 + "-" + mes + "-" + ano;
		} else {
			fechaForm = beans.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion();
		}
		// SE INICIALIZAN VARIABLE
		BeanDetRecepOperacionDAO beanDet = new BeanDetRecepOperacionDAO();
		beanDet.setBeanDetRecepOperacion(new BeanDetRecepOperacion());
		beanDet.getBeanDetRecepOperacion().setDetalleGral(new BeanDetRecepOpera());
		beanDet.getBeanDetRecepOperacion().setReceptor(new BeanDetRecOpRecep());
		// se inicialzian datos grales
		beans.getBeanDetRecepOperacion().getDetalleGral().setFchOperacion(fechaForm);
		beans.getBeanDetRecepOperacion().getReceptor().setNumCuentaRec(cuentaReceptora);
		try {
			BeanDetRecepOperacionDAO beanDetalleUpdate = new BeanDetRecepOperacionDAO();
			beanDetalleUpdate = boDetRecepOperacion.updateCtaReceptora(getArchitechBean(), beans, historico,
					ctaRecepAnt);

			modeloVista.addObject("beanDetalleUpdate", beanDetalleUpdate);

			/** Se asigna el valor **/
			beanDet.setBeanDetRecepOperacion(beans.getBeanDetRecepOperacion());

			modeloVista.addObject(COD_ERROR, beanDetalleUpdate.getBeanDetRecepOperacion().getError().getCodError());
			mensaje = messageSource.getMessage(
					COD_ERROR_PUNTO + beanDetalleUpdate.getBeanDetRecepOperacion().getError().getCodError(), null,
					null);
			modeloVista.addObject(TIPO_ERROR, beanDetalleUpdate.getBeanDetRecepOperacion().getError().getTipoError());
			modeloVista.addObject(DESC_ERROR, mensaje);

		} catch (BusinessException e) {
			error("Se genero el error: " + e.getMessage());
			showException(e);
		}

		/** En casod e existir el histoico llama una pagina **/
		if (historico) {
			beanDet.setParametro("RECEPCIONHTO");
			modeloVista.addObject(PAGINA, "rOH");
		} else {
			beanDet.setParametro("RECEPCION");
			modeloVista.addObject(PAGINA, "rO");
		}

		/** se asigna el valor para que se muestre en pantalla **/
		modeloVista.addObject("fchOperacion", beanDet.getBeanDetRecepOperacion().getDetalleGral().getFchOperacion());
		modeloVista.addObject("cveMiInstitucion",
				beanDet.getBeanDetRecepOperacion().getDetalleGral().getCveMiInstitucion());
		modeloVista.addObject("cveInstOrd", beanDet.getBeanDetRecepOperacion().getDetalleGral().getCveInstOrd());
		modeloVista.addObject("folioPaq", beanDet.getBeanDetRecepOperacion().getDetalleGral().getFolioPaquete());
		modeloVista.addObject("folioPag", beanDet.getBeanDetRecepOperacion().getDetalleGral().getFolioPago());
		modeloVista.addObject("nombrePantalla", beanDet.getBeanDetRecepOperacion().getNombrePantalla());
		modeloVista.addObject("beanTranSpeiRecOper", beanTranSpeiRecOper);
		modeloVista.addObject("beanDatos", beans);
		modeloVista.addObject("beanReqTranSpeiRec", beanReqTranSpeiRec);
		modeloVista.addObject(FECHA_OPER, beanReqTranSpeiRec.getBeanComun().getFechaOperacion());
		/** retorna el valor **/
		return modeloVista;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad
	 * boDetRecepOperacion.
	 *
	 * @return boDetRecepOperacion del tipo BODetRecepOperacion
	 */
	public BODetRecepOperacion getBoDetRecepOperacion() {
		return boDetRecepOperacion;
	}

	/**
	 * Metodo set que permite modificar el parametro boDetRecepOperacion.
	 *
	 * @param boDetRecepOperacion el boDetRecepOperacion a establecer
	 */
	public void setBoDetRecepOperacion(BODetRecepOperacion boDetRecepOperacion) {
		this.boDetRecepOperacion = boDetRecepOperacion;
	}

	/**
	 * getbORecepOperacion de tipo BORecepOperacion.
	 * 
	 * @author FSW-Vector
	 * @return bORecepOperacion de tipo BORecepOperacion
	 */
	public BORecepOperacion getbORecepOperacion() {
		return bORecepOperacion;
	}

	/**
	 * setbORecepOperacion para asignar valor a bORecepOperacion.
	 * 
	 * @author FSW-Vector
	 * @param bORecepOperacion de tipo BORecepOperacion
	 */
	public void setbORecepOperacion(BORecepOperacion bORecepOperacion) {
		this.bORecepOperacion = bORecepOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad messageSource.
	 *
	 * @return messageSource Objeto del tipo ResourceBundleMessageSource
	 */
	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad messageSource.
	 *
	 * @param messageSource del tipo ResourceBundleMessageSource
	 */
	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}

}