/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ControllerInitModuloPagoInteres.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Sep 09 10:11:45 CST 2016 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.controllers.pagoInteres;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *Clase del tipo Controller que se encarga de recibir y procesar
 * la peticion del monitor
**/
@Controller
public class ControllerInitModuloPagoInteres extends Architech{

	 /**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -102047098623676018L;
	
	
	/**
	 * Metodo inicial del modulo CDA
	 * @return ModelAndView Objetode ModelAndView de Spring
	 */
	@RequestMapping(value="moduloPagoInteresInit.do")
	public ModelAndView inicio(){
			return new ModelAndView("inicio");
	}

	
}
