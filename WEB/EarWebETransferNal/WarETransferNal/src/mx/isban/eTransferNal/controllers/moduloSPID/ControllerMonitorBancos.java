/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ControllerMonitorBancos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.controllers.moduloSPID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorBancos;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.servicio.moduloSPID.BOMonitorBancos;

/**
 * Clase del tipo Controller que se encarga de recibir y procesar las peticiones
 * de Monitor Bancos
 **/
@Controller
public class ControllerMonitorBancos extends Architech {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -5678072475684004735L;
	
	/**
	 * 
	 */
	private BOInitModuloSPID boInitModuloSPID;
	
	/**
	 * 
	 */
	@Autowired
	private BeanSessionSPID sessionSPID;
	
	/**
	 * Propiedad del tipo MessageSource que almacena el valor de messageSource
	 */
	private MessageSource messageSource;

	/**
	 * Referencia al servicio de bo Monitor Bancos
	 */
	private BOMonitorBancos bOMonitorBancos;

	/**
	 * Metodo que se utiliza para mostrar la pagina de Monitor Bancos
	 * 
	 * @return ModelAndView Objeto del tipo ModelAndView
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 * @param beanOrdenamiento Objeto del tipo BeanOrdenamiento
	 */
	@RequestMapping(value = "/monitorBancos.do")
	public ModelAndView consultaMonitorBancos(BeanPaginador beanPaginador, BeanOrdenamiento beanOrdenamiento) {
		ModelAndView model = null;
		BeanResMonitorBancos beanResMonitorBancos = null;
		
		try {			
			beanResMonitorBancos = bOMonitorBancos.obtenerMonitorBancos(beanPaginador, getArchitechBean(), beanOrdenamiento.getSortField(), beanOrdenamiento.getSortType());
			model = new ModelAndView("monitorBancos", "beanResMonitorBancos", beanResMonitorBancos);

			BeanSessionSPID sessionSPID = new BeanSessionSPID();
			try {
				sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
			} catch (BusinessException e) {
				showException(e);
			}
			
			String mensaje = messageSource.getMessage("codError." + beanResMonitorBancos.getCodError(), null, null);
			model.addObject(Constantes.COD_ERROR, beanResMonitorBancos.getCodError());
			model.addObject(Constantes.TIPO_ERROR, beanResMonitorBancos.getTipoError());
			model.addObject(Constantes.DESC_ERROR, mensaje);
			model.addObject("sessionSPID", sessionSPID);
			model.addObject("sortField", beanOrdenamiento.getSortField());
			model.addObject("sortType", beanOrdenamiento.getSortType());
			generarContadoresPag(model, beanResMonitorBancos);
		} catch (BusinessException e) {
			showException(e);
		}
		return model;
	}
	
	/**
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResMonitorBancos beanResMonitorBancos) {
		if (beanResMonitorBancos.getTotalReg() > 0){
			 Integer regIni = 1;
			 Integer regFin = beanResMonitorBancos.getTotalReg();
			
			if (!"1".equals(beanResMonitorBancos.getBeanPaginador().getPagina())) {
				regIni = 20 * (Integer.parseInt(beanResMonitorBancos.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResMonitorBancos.getTotalReg() >= 20 * Integer.parseInt(beanResMonitorBancos.getBeanPaginador().getPagina())){
				regFin = 20 * Integer.parseInt(beanResMonitorBancos.getBeanPaginador().getPagina());
			}		
			modelAndView.addObject("regIni", regIni);
			modelAndView.addObject("regFin", regFin);
		}
	}


	/**
	 * Metodo get del servicio de Monitor Bancos
	 * 
	 * @return bOConsultaRecepciones Objeto del servicio de Monitor Bancos
	 */
	public BOMonitorBancos getBOMonitorBancos() {
		return bOMonitorBancos;
	}

	/**
	 * Metodo set del servicio de Monitor Bancos
	 * 
	 * @param bOMonitorBancos objeto del tipo BOMonitorBancos
	 *            Recepciones Se setea objeto del servicio de Monitor Bancos
	 */
	public void setBOMonitorBancos(BOMonitorBancos bOMonitorBancos) {
		this.bOMonitorBancos = bOMonitorBancos;
	}

	/**
	 * @return the sessionSPID
	 */
	public BeanSessionSPID getSessionSPID() {
		return sessionSPID;
	}
	/**
	 * @param sessionSPID the sessionSPID to set
	 */
	public void setSessionSPID(BeanSessionSPID sessionSPID) {
		this.sessionSPID = sessionSPID;
	}
	
	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @return the boInitModuloSPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}

	/**
	 * @param boInitModuloSPID the boInitModuloSPID to set
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}
}
