/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ControllerExportarRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:28:21 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.controllers.moduloPOACOA;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.moduloPOACOA.BORecepciones;
import mx.isban.eTransferNal.servicio.moduloSPID.BOInitModuloSPID;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.ConstantesWebPOACOA;
import mx.isban.eTransferNal.utilerias.moduloPOACOA.UtileriasExportarRecepciones;

/**
 * Class ControllerExportarRecepciones.
 *
 * Clase tipo controller utilizada para cargar la pantalla de
 * recepciones y mapear las funcionalides y disparar los flujos
 * de la misma.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Controller
public class ControllerExportarRecepciones extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2528505424478686009L;
	
	/** La variable que contiene informacion con respecto a: bo recepciones. */
	private BORecepciones boRecepciones;
	
	/** La variable que contiene informacion con respecto a: bo init modulo SPID. */
	private BOInitModuloSPID boInitModuloSPID;
	
	/** La variable que contiene informacion con respecto a: message source. */
	private MessageSource messageSource;

	/** La constante PAGINA_PRINCIPAL. */
	private static final String PAGINA_PRINCIPAL = "consultaRecepciones";
	
	/** La constante BEAN_RECEPCIONES. */
	private static final String BEAN_RECEPCIONES = "beanResConsultaRecepciones";
 	
 	/** La constante utilsExportar. */
	 private static final UtileriasExportarRecepciones utilsExportar = UtileriasExportarRecepciones.getUtils();
	 
	 /** La constante CONST_REF. */
	 private static final String CONST_REF = "refModel";
	 	
	/**
	 * Exp recepciones.
	 *
	 * @param resConsultaRecepciones El objeto: res consulta recepciones
	 * @param tipoOrden El objeto: tipo orden
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarRecepciones.do")
	public ModelAndView expRecepciones(
			@Valid @ModelAttribute(BEAN_RECEPCIONES) BeanResConsultaRecepciones resConsultaRecepciones,
			@Valid @RequestParam("ref") String tipoOrden, BindingResult bindingResult) {
		String tipo = null;
		tipo = tipoOrden.replace(",", "");
		/** Se crea el model&view a retornar **/
		FormatCell formatCellHeaderExpRec = null;
		FormatCell formatCellBodyExpRec = null;
		ModelAndView model = null;
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		/** Recuperar atributos usando el context **/
		HttpServletRequest reqExpRec = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		RequestContext ctxExpRec = new RequestContext(reqExpRec);
		/** Creacion del cuerpo del archivo **/
		formatCellBodyExpRec = new FormatCell();
		formatCellBodyExpRec.setFontName("Calibri");
		formatCellBodyExpRec.setFontSize((short) 11);
		formatCellBodyExpRec.setBold(false);
		formatCellHeaderExpRec = new FormatCell();
		formatCellHeaderExpRec.setFontName("Calibri");
		formatCellHeaderExpRec.setFontSize((short) 11);
		formatCellHeaderExpRec.setBold(true);
		model = new ModelAndView(new ViewExcel());
		/** Seteo de datos al modeolo **/
		model.addObject("FORMAT_HEADER", formatCellHeaderExpRec);
		model.addObject("FORMAT_CELL", formatCellBodyExpRec);
		model.addObject("HEADER_VALUE", utilsExportar.getHeaderExcel(ctxExpRec));
		model.addObject("BODY_VALUE", utilsExportar.getBody(resConsultaRecepciones.getListaConsultaRecepciones()));
		model.addObject("NAME_SHEET", utilsExportar.obtenerNombre(tipo));
		/** Retorno del modelo **/
		return model;
	}
	
	/**
	 * Exportar todos recepciones.
	 *
	 * @param beanModulo El objeto: bean modulo
	 * @param resConsultaRecepciones El objeto: res consulta recepciones
	 * @param tipoOrden El objeto: tipo orden
	 * @param paginador El objeto: paginador
	 * @param bindingResult El objeto: binding result
	 * @return Objeto model and view
	 */
	@RequestMapping(value = "/exportarTodoRecepciones.do")
	public ModelAndView exportarTodoRecepciones(
			@Valid@ModelAttribute(ConstantesWebPOACOA.BEAN_MODULO) BeanModulo beanModulo,
			@Valid @ModelAttribute(BEAN_RECEPCIONES) BeanResConsultaRecepciones resConsultaRecepciones,
			@Valid @RequestParam("ref") String tipoOrden,
			BeanPaginador paginador, BindingResult bindingResult) {
		ModelAndView model = null; 
		BeanResConsultaRecepciones response = null;
		/** Recuperar atributos usando el context **/
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		RequestContext ctx = new RequestContext(req);
		BeanSessionSPID sessionSPID = new BeanSessionSPID();
		String tipo = null;
		
		/** Validacion de los datos de entrada **/
		if (bindingResult.hasErrors()) {
			error(ConstantesWebPOACOA.ERROR_BINDING);
		}
		tipo = tipoOrden.replace(",", "");
		/** Validacion de a session**/
		try {
			sessionSPID = boInitModuloSPID.beanCheckSessionSPID();
		} catch (BusinessException e) {
			showException(e);
		}
		try {
			resConsultaRecepciones.setTipoOrden(tipo);
			/** Se realiza la peticion al BO **/
			response = boRecepciones.exportarTodo(resConsultaRecepciones, beanModulo, sessionSPID, utilsExportar.obtenerNombre(tipo), getArchitechBean());
			/** Inicializacion del modelo **/
			model = new ModelAndView(PAGINA_PRINCIPAL);
			response.setListaConsultaRecepciones(resConsultaRecepciones.getListaConsultaRecepciones());
			response.setBeanPaginador(paginador);
			response.setImporteTotal(resConsultaRecepciones.getImporteTotal());
			response.setTotalReg(resConsultaRecepciones.getTotalReg());
			/** Valdiacion del tipo de orden **/
			if ("PA".equalsIgnoreCase(tipoOrden)) {
				response.setTitulo("Movimientos de Ordenes  Recibidas por Aplicar");
			} else if ("TR".equalsIgnoreCase(tipoOrden)) {
				response.setTitulo("Movimientos de Ordenes Recibidas Aplicadas");
			} else if ("RE".equalsIgnoreCase(tipoOrden)) {
				response.setTitulo("Movimientos de Ordenes Recibidas por Rechazar");
			}
			response.setTipoOrden(resConsultaRecepciones.getTipoOrden());
			resConsultaRecepciones.setTipoOrden(tipoOrden);
			if (Errores.OK00002V.equals(response.getCodError())) {
				String mensaje = ctx.getMessageSource().getMessage(Constantes.COD_ERROR_PUNTO + response.getCodError(),
						new Object[] { response.getMsgError() }, LocaleContextHolder.getLocale());		
				/** Seteo de datos al modeolo **/
				model.addObject(BEAN_RECEPCIONES, response);
				model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
				model.addObject(Constantes.DESC_ERROR, mensaje);
			} else {
				model.addObject(Constantes.DESC_ERROR, response.getMsgError());
			}
			/** Seteo de datos al modeolo **/
			model.addObject(Constantes.COD_ERROR, response.getCodError());
			model.addObject(Constantes.TIPO_ERROR,response.getTipoError());
 			model.addObject(ConstantesWebPOACOA.SORT_FIELD, "");
			model.addObject(ConstantesWebPOACOA.SORT_TYPE, "");
			model.addObject(ConstantesWebPOACOA.SESSION, sessionSPID);
			model.addObject(ConstantesWebPOACOA.BEAN_MODULO, beanModulo);
			model.addObject(CONST_REF, tipoOrden);
			generarContadoresPag(model, response);
		} catch (BusinessException e) {
	 		 showException(e);
	 	}
		/** Retorno del modelo **/
	    return model;
	}
	
	/**
	 * Generar contadores paginacion de la consulta.
	 * @param modelAndView Objeto del tipo ModelAndView
	 * @param beanResConsultaRecepciones El objeto: bean res consulta recepciones
	 */
	protected void generarContadoresPag(ModelAndView modelAndView, BeanResConsultaRecepciones beanResConsultaRecepciones) {
		if (beanResConsultaRecepciones.getTotalReg() > 0){
			 Integer regIniConPag = 1;
			 Integer regFinConPag = beanResConsultaRecepciones.getTotalReg();
			
			if (!"1".equals(beanResConsultaRecepciones.getBeanPaginador().getPagina())) {
				regIniConPag = 20 * (Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina()) - 1) + 1;
			}
			if (beanResConsultaRecepciones.getTotalReg() >= 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina())){
				regFinConPag = 20 * Integer.parseInt(beanResConsultaRecepciones.getBeanPaginador().getPagina());
			}		
			modelAndView.addObject("regIni", regIniConPag);
			modelAndView.addObject("regFin", regFinConPag);
			BigDecimal totalImporteConPag = BigDecimal.ZERO;
			for (BeanConsultaRecepciones beanCons : beanResConsultaRecepciones.getListaConsultaRecepciones()){
				totalImporteConPag = totalImporteConPag.add(new BigDecimal(beanCons.getBeanConsultaRecepcionesDos().getMonto()));
			}
			/** Seteo de importe de pagina **/
			modelAndView.addObject("importePagina", totalImporteConPag.toString());
		}
	}
	
	/**
	 * Obtener el objeto: bo recepciones de la exportacion de recepciones
	 * @return El objeto: bo recepciones
	 */
	public BORecepciones getBoRecepciones() {
		return boRecepciones;
	}

	/**
	 * Definir el objeto: message source de la exportacion de recepciones
	 * @param messageSource El nuevo objeto: message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	/**
	 * Definir el objeto: bo recepciones de la exportacion de recepciones
	 * @param boRecepciones El nuevo objeto: bo recepciones
	 */
	public void setBoRecepciones(BORecepciones boRecepciones) {
		this.boRecepciones = boRecepciones;
	}

	

	/**
	 * Definir el objeto: bo init modulo SPID de la exportacion de recepciones
	 * @param boInitModuloSPID El nuevo objeto: bo init modulo SPID
	 */
	public void setBoInitModuloSPID(BOInitModuloSPID boInitModuloSPID) {
		this.boInitModuloSPID = boInitModuloSPID;
	}

	/**
	 * Obtener el objeto: bo init modulo SPID de la exportacion de recepciones
	 * @return El objeto: bo init modulo SPID
	 */
	public BOInitModuloSPID getBoInitModuloSPID() {
		return boInitModuloSPID;
	}
	/**
	 * Obtener el objeto: message source de la exportacion de recepciones
	 * @return El objeto: message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	
}
