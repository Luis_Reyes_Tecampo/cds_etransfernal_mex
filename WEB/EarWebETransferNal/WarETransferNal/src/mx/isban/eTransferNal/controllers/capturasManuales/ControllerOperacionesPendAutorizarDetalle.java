/**
 * Clase de tipo controller para procesar la pantalla
 * de Autorizar Operaciones
 * 
 * @author: Vector
 * @since: Marzo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.controllers.capturasManuales;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqOpePendAutDetalle;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperacionesDetalle;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.servicio.capturasManuales.BOCapturaCentral;
import mx.isban.eTransferNal.utilerias.MetodosAutorizarOperaciones;
import mx.isban.eTransferNal.utilerias.UtilsCapturasManuales;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class ControlleOperacionesPendAutorizarDetalle.
 */
@Controller
public class ControllerOperacionesPendAutorizarDetalle extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 3105611550248615991L;
	
	/**  Constante de la pagina autorizacion operaciones. */
	private static final String PAGINA_DETALLE_AUTORIZACION_OPERACIONES = "detalleAutorizacionOperaciones";

	/** La variable que contiene informacion con respecto a: bo captura central. */
	private BOCapturaCentral boCapturaCentral;
	
	/**
	 * mostrar detalle operacion pendiente aprobacion.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 */
	//mostrar detalle operacion pendiente aprobacion.
	@RequestMapping(value = "/mostrarDetalleOperacionPendiente.do")
	public ModelAndView mostrarDetalleOpePendAprobacion(HttpServletRequest request, HttpServletResponse response){
		ModelAndView modelAndView= new ModelAndView(PAGINA_DETALLE_AUTORIZACION_OPERACIONES);
		MetodosAutorizarOperaciones utilsAutorizarOper=new MetodosAutorizarOperaciones();
		//Request to bean mapping
		BeanReqOpePendAutDetalle reqDetalleOperacion = utilsAutorizarOper.requestToBeanReqOpePendAutDetalle(request, modelAndView);
		BeanResConsOperacionesDetalle opePendAutDetalle = new BeanResConsOperacionesDetalle();
		BeanCapturaCentralResponse responseCapturaCentral = new BeanCapturaCentralResponse();
		BeanCapturaCentralRequest requestCapturaCentral = new BeanCapturaCentralRequest();
		BeanResAutorizarOpe filtrosAutorizarOperaciones = new BeanResAutorizarOpe();
		UtilsCapturasManuales utils = UtilsCapturasManuales.getUtilerias();

		//setear filtros para consultar detalle de operacion
		requestCapturaCentral.setTemplate(reqDetalleOperacion.getCveTemplate());
		requestCapturaCentral.setIdFolio(reqDetalleOperacion.getFolio());
		requestCapturaCentral.setEditable(false);
		
		//consultar detalle de operacion
		try{
			responseCapturaCentral = boCapturaCentral.consultaTemplate(requestCapturaCentral, getArchitechBean());
			//setear resultados de consulta
			setearListaLayout(responseCapturaCentral.getLayout());
			opePendAutDetalle.setLayout(responseCapturaCentral.getLayout());
			opePendAutDetalle.setCodError(responseCapturaCentral.getCodError());
			opePendAutDetalle.setTipoError(responseCapturaCentral.getTipoError());
		}catch(BusinessException e){
			//Se controla BusinessException
			showException(e);
			opePendAutDetalle.setCodError(e.getCode());
			opePendAutDetalle.setTipoError(Errores.TIPO_MSJ_ERROR);
		}
		//setear filtros de busqueda para pantalla principal
		filtrosAutorizarOperaciones.setTemplate(utils.eliminarInjeccionDeCodigo(reqDetalleOperacion.getTemplate()));
		filtrosAutorizarOperaciones.setFechaOperacion(reqDetalleOperacion.getFechaCaptura());
		filtrosAutorizarOperaciones.setPaginador(reqDetalleOperacion.getPaginador());
		filtrosAutorizarOperaciones.setUsrAdmin(reqDetalleOperacion.getUsrAdmin());

		//agregar objetos a modelAndView
		modelAndView.addObject("beanRes", opePendAutDetalle);
		modelAndView.addObject("beanFiltroAutorizarOpe", filtrosAutorizarOperaciones);
		modelAndView.addObject("paramCveTemplate", reqDetalleOperacion.getCveTemplate());
		modelAndView.addObject("paramFolio", reqDetalleOperacion.getFolio());
		return modelAndView;
	}

	/**
	 * Poner lista layout como constantes.
	 *
	 * @param listLayout the list layout
	 */
	//Poner lista layout como constantes.
	private void setearListaLayout(List<BeanLayout> listLayout){
		String constante="";
		//recorrer lista del layout
		for(BeanLayout layout : listLayout){
			String valorNuevo=" ";
			constante=layout.getConstante();
			//verificar que valor de constante no sea vacio
			if( "".equals(constante) || " ".equals(constante) || constante == null ){
				String valActual=layout.getValorActual();
				//verificar que valor actual no sea vacio
				if( !("".equals(valActual) || " ".equals(valActual) || valActual == null) ){
					//valor nuevo es igual a valor actual
					valorNuevo=valActual;
				}
			}else{
				//valor nuevo es igual a constante
				valorNuevo=constante;
			}
			layout.setConstante(valorNuevo);
		}
	}
	
	/**
	 * Sets the bo captura central.
	 *
	 * @param boCapturaCentral the new bo captura central
	 */
	//BO Captura central
	public void setBoCapturaCentral(BOCapturaCentral boCapturaCentral) {
		this.boCapturaCentral = boCapturaCentral;
	}
}
