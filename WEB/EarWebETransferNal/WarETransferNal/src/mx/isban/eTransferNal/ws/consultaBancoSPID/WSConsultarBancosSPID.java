/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: WSConsultarBancosSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      5/10/2017 07:20:45 PM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.ws.consultaBancoSPID;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import mx.isban.eTransferNal.ws.BeanResponseConsultaBancoSPID;

/**
 * The Interface WSConsultarBancosSPID.
 * @author agarcia
 *
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface WSConsultarBancosSPID {

	//metodo consultaBancoSPID()
	/**
	 * Metodo para realizar la consulta.
	 *
	 * @return String con la cadena prefijoCuentaClabe de respuesta.
	 *
	 */
	@WebMethod
	@WebResult(name = "respuestaBancosSPID")
	BeanResponseConsultaBancoSPID consultaBancoSPID();
}