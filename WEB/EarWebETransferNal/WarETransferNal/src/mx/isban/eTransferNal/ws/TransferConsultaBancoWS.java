/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * TransferConsultaBancoWS.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.servicios.ws.BORConsultaBancoEJB;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Clase encargada de recibir la peticiones del webService
 * @author cchong
 *
 */
@WebService(wsdlLocation="WEB-INF/wsdl/TransferConsultaBancoService.wsdl", endpointInterface="mx.isban.eTransferNal.ws.TransferConsultaBancoEndPoint", serviceName="TransferConsultaBancoService")
public class TransferConsultaBancoWS extends SpringBeanAutowiringSupport implements TransferConsultaBancoEndPoint{
	/**Web Service Context*/
	@Resource
	private transient WebServiceContext wsContext; 
	/**Propiedad que almacena la referencia al objeto BORProcConsEstatusDet*/
	@Autowired
	private BORConsultaBancoEJB bORConsultaBanco;
	
	/**
	 * Metodo que se utiliza para consultar el banco por medio del prefijo de la cuenta CLABE
	 * @param prefijoCuentaClabe String con el prefijo
	 * @return ResBeanConsultaBanco bean con los datos del banco
	 */
	public ResBeanConsultaBanco consultaBanco(String prefijoCuentaClabe) {
    	MessageContext mc = wsContext.getMessageContext();
		HttpServletRequest req = (HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST); 
    	
		ArchitechSessionBean sessionBean = new ArchitechSessionBean();
    	sessionBean.setIPCliente(req.getRemoteAddr());
    	sessionBean.setIPServidor(req.getLocalAddr());
    	sessionBean.setNombreServidor(req.getServerName());
		ResBeanConsultaBanco resBeanConsultaBanco = null;
		resBeanConsultaBanco = bORConsultaBanco.consultaBanco(prefijoCuentaClabe,sessionBean);
		return resBeanConsultaBanco;
	}
	/**Metodo get que sirve para obtener una referencia del BORConsultaBancoEJB
	 * @return BORConsultaBancoEJB Referencia del servicio de consulta banco
	 */
	public BORConsultaBancoEJB getBORConsultaBanco() {
		return bORConsultaBanco;
	}
	/**Metodo set que sirve para cambiar la referencia del BORConsultaBancoEJB
	 * @param consultaBanco Referencia del servicio de consulta banco
	 */
	public void setBORConsultaBanco(BORConsultaBancoEJB consultaBanco) {
		bORConsultaBanco = consultaBanco;
	}
	
	

}
