/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WSConsultaMovimientos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * @author mcamarillo
 *
 */
@WebService
public interface WSConsultaMovimientos {
	
	/**
	 * @param movimientos 
	 * 			EntradaStringJsonDTO con la trama de entrada en forma de json o un objeto json
	 * @return
	 * 			EntradaStringJsonDTO con trama de salida en forma de json o un objeto json
	 */
	@WebMethod
	@WebResult(name = "respuestaJson")
	ResEntradaStringJsonDTO getConsultaMovimientos(final EntradaStringJsonDTO movimientos);

}
