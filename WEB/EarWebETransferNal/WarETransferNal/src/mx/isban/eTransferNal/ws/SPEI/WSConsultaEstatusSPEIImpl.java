/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaEstatusSPEIImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:39:44 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.ws.SPEI;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOConsultaEstatusSPEI;

/**
 * Class WSConsultaEstatusSPEIImpl.
 * Implementa el Servicio Web SOAP
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.SPEI.WSConsultaEstatusSPEI" ,serviceName="ConsultaEstatusSPEIService" )
public class WSConsultaEstatusSPEIImpl implements WSConsultaEstatusSPEI{

	/**
	 * Consulta estatus.
	 * Implemneta al conulta del estatus SPEI
	 *
	 * @return Objeto res entrada string json DTO
	 */
	@Override
	public ResEntradaStringJsonDTO consultaEstatus() {
		//Enlaza la interfaz del Servicio
		BOConsultaEstatusSPEI boConsultaEstatusSPEI =null;
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		boConsultaEstatusSPEI = (BOConsultaEstatusSPEI)globalJNDIName.withAppName("EarEjbETransferNal")
		.withModuleName("EjbTransferNal").withBeanName("BOConsultaEstatusSPEI")
		.withBusinessInterface(BOConsultaEstatusSPEI.class).locate();
		//Realiza la consulta al BO
		return boConsultaEstatusSPEI.consultaEstatusSPEI();
	}
}
