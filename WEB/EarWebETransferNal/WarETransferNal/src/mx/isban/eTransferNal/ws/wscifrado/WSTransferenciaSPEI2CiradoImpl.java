/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaMovimientosSeguroImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-06-2018 02:04:02 PM Juan Jesus Beltran R. VECTOR SWF Creacion
 */
package mx.isban.eTransferNal.ws.wscifrado;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOTransferCifrado;

/**
 * Class WSConsultaMovimientosSeguroImpl.
 * 
 * WebService para generacion de transferencias SPID con segundo Beneficiario
 * Cifrado
 *
 * @author FSW-Vector
 * @since 08-06-2018
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.wscifrado.WSTransferenciaSPEI2Cifrado", serviceName = "TransferenciaSPEI2Cifrado")
public class WSTransferenciaSPEI2CiradoImpl implements WSTransferenciaSPEI2Cifrado {
		
	/** La constante STRANSANT. */
	public static final String STRANTRF2 = "STRATRF2";

	/* (sin Javadoc)
	 * @see mx.isban.eTransferNal.ws.WSConsultaMovimientos#getConsultaMovimientos(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO)
	 */
	@Override
	public ResEntradaStringJsonDTO transferenciaSPEI2(EntradaStringJsonDTO movimientos) {
		BOTransferCifrado boConsultaMovimientos = null;
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		boConsultaMovimientos = (BOTransferCifrado) globalJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BOTransferCifrado")
				.withBusinessInterface(BOTransferCifrado.class).locate();
		return boConsultaMovimientos.getResponseService(movimientos, STRANTRF2);
	}

}
