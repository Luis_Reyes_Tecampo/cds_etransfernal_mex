/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaMovimientosSeguro.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-06-2018 04:48:37 PM Juan Jesus Beltran R. VECTOR SWF Creacion
 */
package mx.isban.eTransferNal.ws.wscifrado;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface WSConsultaMovimientosSeguro.
 *
 * @author FSW-Vector
 * @since 08-06-2018
 */
@WebService
public interface WSTransferenciaSPEICifrado {
	

	/**
	 * Transferencia SPEI.
	 *
	 * @param movimientos El objeto: movimientos
	 * @return Objeto res entrada string json DTO
	 */
	@WebMethod
	@WebResult(name = "jsonResponse")
	ResEntradaStringJsonDTO transferenciaSPEI(@WebParam(name="jsonRequest") EntradaStringJsonDTO movimientos);

}
