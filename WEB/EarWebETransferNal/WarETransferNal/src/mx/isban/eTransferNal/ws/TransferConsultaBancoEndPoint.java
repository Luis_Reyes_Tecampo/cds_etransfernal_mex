/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * TransferConsultaBancoEndPoint.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * Endpoint del Webservice del detalle de la operacion
 * @author cchong
 *
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface TransferConsultaBancoEndPoint {

	/**
	 * Metodo para realizar la consulta
	 * @param  prefijoCuentaClabe Candena con el prefijo del banco
	 * @return String con la cadena prefijoCuentaClabe de respuesta.
	 */
	@WebMethod
	@WebResult(name = "respuestaCuentaClabe")
	public ResBeanConsultaBanco consultaBanco( @WebParam(name = "prefijoCuentaClabe") String prefijoCuentaClabe);
}
