/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: WSConsultarBancosSPIDImpl.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      5/10/2017 07:20:45 PM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.ws.consultaBancoSPID;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.xml.ws.WebServiceContext;

import mx.isban.eTransferNal.servicios.ws.BOConsultaBancoSPID;
import mx.isban.eTransferNal.utilerias.PropertiesUtils;
import mx.isban.eTransferNal.ws.BeanResponseConsultaBancoSPID;

import org.apache.log4j.Logger;

/**
 * The Class WSConsultarBancosSPIDImpl.
 * @author agarcia
 *
 */
@WebService(wsdlLocation="WEB-INF/wsdl/TransferConsultaBancoSPIDService.wsdl", endpointInterface = "mx.isban.eTransferNal.ws.consultaBancoSPID.WSConsultarBancosSPID", serviceName = "TransferConsultaBancoSPIDService")
public class WSConsultarBancosSPIDImpl implements WSConsultarBancosSPID {

	/**  Logeo de la clase. */
	private static final Logger LOG = Logger.getLogger(WSConsultarBancosSPIDImpl.class.getName());

	private static final String JNDI_CONSULTA_BANCOS_SPID = "mx.isban.eTransferNal.servicios.ws.BOConsultaBancoSPID";

	private static final String INTERFAZ_ENLAZADA_EXITOSAMENTE = "Interfaz enlazada Exitosamente...";

	private static final String SERVICIO_NO_DISPONIBLE = "Servicio no disponible";

	private static final String ERROR_500 = "500";

	/** Web Service Context. */
	@Resource
	private transient WebServiceContext wsContext;

	/** Propiedad que almacena la referencia al objeto BORProcConsEstatusDet. */
	private BOConsultaBancoSPID boConsultaBancosSPID;

	/**
	 *
	 * Metodo que se utiliza para consultar los bancos SPID
	 * @return BeanResponseConsultaBancoSPID bean con los datos de los bancos SPID
	 *
	 */
	public BeanResponseConsultaBancoSPID consultaBancoSPID() {
		BeanResponseConsultaBancoSPID resConsultaBancoSPID = new BeanResponseConsultaBancoSPID();

    	//se consulta metodo del BO consultaBancosSPID()
		/**resConsultaBancoSPID = boConsultaBancosSPID.consultaBancosSPID();*/
		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			//Busca el BO
			Object obj = ic.lookup(PropertiesUtils.CLUSTERNAME + JNDI_CONSULTA_BANCOS_SPID);
			obj = PortableRemoteObject.narrow(obj, BOConsultaBancoSPID.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaBancoSPID){
				//Obtiene BO
				boConsultaBancosSPID = (BOConsultaBancoSPID) obj;
				LOG.info(INTERFAZ_ENLAZADA_EXITOSAMENTE);

				//Consulta BO
				resConsultaBancoSPID = boConsultaBancosSPID.consultaBancosSPID();
			}else{
				resConsultaBancoSPID.getResultOPeracion().setMsgError(SERVICIO_NO_DISPONIBLE);
				resConsultaBancoSPID.getResultOPeracion().setCodError(ERROR_500);
				LOG.error(SERVICIO_NO_DISPONIBLE);
			}
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			resConsultaBancoSPID.getResultOPeracion().setMsgError(SERVICIO_NO_DISPONIBLE);
			resConsultaBancoSPID.getResultOPeracion().setCodError(ERROR_500);
			LOG.error(e.getMessage(),e);
		}
		return resConsultaBancoSPID;
	}

	/**
	 * Metodo set que sirve para cambiar la referencia del BOConsultaBancoSPID.
	 *
	 * @param consultaBancoSPID Referencia del servicio de consulta banco
	 */
	public void setBORConsultaBanco(BOConsultaBancoSPID consultaBancoSPID) {
		boConsultaBancosSPID = consultaBancoSPID;
	}
}