package mx.isban.eTransferNal.ws.SPID;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOTransferenciaSPID;

@WebService(endpointInterface = "mx.isban.eTransferNal.ws.SPID.WSTransferenciaSPID" ,serviceName="TransferenciaSPID")
public class WSTransferenciaSPIDImpl implements WSTransferenciaSPID {

	@Override
	public ResEntradaStringJsonDTO transferenciaSPID(EntradaStringJsonDTO transferencia) {
		BOTransferenciaSPID bOTransferenciaSPID =null;
			GlobalJNDIName globalJNDIName = new GlobalJNDIName();
			bOTransferenciaSPID = (BOTransferenciaSPID)globalJNDIName.withAppName("EarEjbETransferNal")
			.withModuleName("EjbTransferNal").withBeanName("BOTransferenciaSPID")
			.withBusinessInterface(BOTransferenciaSPID.class).locate();
			return bOTransferenciaSPID.transferencia(transferencia);
	}
}
