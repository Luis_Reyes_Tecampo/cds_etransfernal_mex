package mx.isban.eTransferNal.ws.SPEI;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * @author mcamarillo
 *
 */
@WebService
public interface WSConsultaImporteSpei {
	/**
	 * Metodo que consulta los datos de una operacion
	 * @param reqConsultaImporteSpei bean con los parametros de la consulta
	 * @return
	 * 			EntradaStringJsonDTO bean de respuesta con los datos de la operacion
	 */
	@WebMethod
	@WebResult(name = "respuesta")
	ResEntradaStringJsonDTO consultaImporteSpei(@WebParam(name = "param") final EntradaStringJsonDTO param);
}
