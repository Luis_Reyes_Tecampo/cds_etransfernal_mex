/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaEstatusSPEI.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:39:11 PM Juan Jesus Beltran R. VectorFSW Creacion
 */

package mx.isban.eTransferNal.ws.SPEI;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface WSConsultaEstatusSPEI.
 * Define los metodos para el Servicio de Consulta de Estatus BO
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@WebService
public interface WSConsultaEstatusSPEI {
		
	/**
	 * Consulta estatus.
	 *
	 * @return Objeto res entrada string json DTO
	 */
	@WebMethod
	@WebResult(name = "respuesta")
	ResEntradaStringJsonDTO consultaEstatus();

}
