/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WSConsultaCertificado.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   06/02/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws.moduloSPID;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificado;

/**
 * @author mcamarillo
 *
 */
@WebService
public interface WSConsultaCertificado {
	
	/**
	 * Metodo que consulta los datos de un certificado a partir de numero de serie del certificado
	 * @param reqBeanConsultaCertificado bean con el numero de certificado
	 * @return
	 * 			BeanResConsultaCertificado bean de respuesta con los datos del certificado
	 */
	@WebMethod
	@WebResult(name = "respuesta")
	BeanResConsultaCertificado consultaCertificado(@WebParam(name = "param") final BeanReqConsultaCertificado reqBeanConsultaCertificado);

}
