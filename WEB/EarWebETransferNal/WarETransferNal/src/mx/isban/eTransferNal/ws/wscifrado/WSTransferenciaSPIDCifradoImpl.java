/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSTransferenciaSPIDSeguroImpl.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   07-30-2018 06:18:11 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.ws.wscifrado;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOTransferCifrado;

/**
 * Class WSTransferenciaSPIDSeguroImpl.
 * 
 * WebService para generacion de transferencias SPID
 * Cifrado
 *
 * @author FSW-Vector
 * @since 07-30-2018
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.wscifrado.WSTransferenciaSPIDCifrado", serviceName = "TransferenciaSPIDCifrado")
public class WSTransferenciaSPIDCifradoImpl implements WSTransferenciaSPIDCifrado {
	
	/** La constante STRANSPID. */
	public static final String STRANSPID = "STRASPID";

	/* (sin Javadoc)
	 * 
	 * @see mx.isban.eTransferNal.ws.SPID.WSTransferenciaSPIDSeguro#transferenciaSPID(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO)
	 */
	@Override
	public ResEntradaStringJsonDTO transferenciaSPID(EntradaStringJsonDTO transferencia) {
		BOTransferCifrado bOTransferenciaSPID = null;
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		bOTransferenciaSPID = (BOTransferCifrado) globalJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BOTransferCifrado")
				.withBusinessInterface(BOTransferCifrado.class).locate();
		return bOTransferenciaSPID.getResponseService(transferencia, STRANSPID);
	}
}
