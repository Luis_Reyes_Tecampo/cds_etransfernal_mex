/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaDetallada.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2017 11:24:45 AM Juan Jesus Beltran R. Isban Creacion
 */

package mx.isban.eTransferNal.ws.consultaDetallada;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface WSConsultaDetallada.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@WebService
public interface WSConsultaDetallada {

	/**
	 * Consulta detallada.
	 *
	 * @param reqConsultaDet El objeto: req consulta det
	 * @return Objeto res entrada string json dto
	 */
	@WebMethod
	@WebResult(name = "respuesta")
	ResEntradaStringJsonDTO consultaDetallada(
			@WebParam(name = "param") final EntradaStringJsonDTO reqConsultaDet);
	
}
