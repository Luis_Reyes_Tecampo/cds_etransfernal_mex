/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WSConsultaEstDetSPIDImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 Indra FSW  ISBAN Creacion
 */

package mx.isban.eTransferNal.ws.SPID;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOConsultaEstatusSPID;

@WebService(endpointInterface = "mx.isban.eTransferNal.ws.SPID.WSConsultaEstDetSPID" ,serviceName="ConsultaEstDetSPID")
public class WSConsultaEstDetSPIDImpl implements WSConsultaEstDetSPID {

	@Override
	public ResEntradaStringJsonDTO consultaEstatusDetSPID(EntradaStringJsonDTO reqConsultaEstatusDet) {
		BOConsultaEstatusSPID bOConsultaEstatusSPID =null;
			GlobalJNDIName globalJNDIName = new GlobalJNDIName();
			bOConsultaEstatusSPID = (BOConsultaEstatusSPID)globalJNDIName.withAppName("EarEjbETransferNal")
			.withModuleName("EjbTransferNal").withBeanName("BOConsultaEstatusSPID")
			.withBusinessInterface(BOConsultaEstatusSPID.class).locate();
			return bOConsultaEstatusSPID.consultaEstatusDetSPID(reqConsultaEstatusDet);
	}
}
