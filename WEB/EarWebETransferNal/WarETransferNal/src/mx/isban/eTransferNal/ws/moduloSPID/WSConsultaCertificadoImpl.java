/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaCertificadoImpl.java
 * 
 * Control de versiones:
 * Version	Date/Hour				By							 Company	Description
 * -------	----------------------	---------------------------  ----------	----------------------
 * 1.0.0	06/02/2015 00:00:00  	Carlos Alberto Chong Antonio Isban		Creacion
 * 1.0.1	04/04/2018 12:00:00 	Juan Manuel Fuentes Ramos	 CSA		Implementacion modulo metrics
 */

package mx.isban.eTransferNal.ws.moduloSPID;
//Imports java
import java.util.Arrays;
//Imports javax
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
//Imports etransferNal
import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificado;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOConsultaCertificado;
import mx.isban.eTransferNal.ws.consultaDetallada.WSConsultaDetalladaImpl;
//Imports metrics
import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;
//Import log4j
import org.apache.log4j.Logger;

/**
 * @author mcamarillo
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.moduloSPID.WSConsultaCertificado" ,serviceName="ConsultaCertificadoService")
public class WSConsultaCertificadoImpl implements WSConsultaCertificado {

	/** Utileria para impresion de log*/
	private static final Logger LOGGER = Logger.getLogger(WSConsultaCertificadoImpl.class);
	/** EJB para consultar certificado */
	private transient BOConsultaCertificado bOConsultaCertificado =null;
	
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** Contexto de la aplicacion */
	@Resource
	private transient WebServiceContext context;
	/** Instancia que atiende la peticion */
	private transient String instanciaWeb = null;
	/** Variable para error*/
	private transient String error = "N"; 
	/** Utilerias Metricas MetricsSenderInit*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	/** The timeIn*/
	private transient String timeIn = null;
	/** The timeOut*/
	private transient String timeOut = null;
	/** The ipClient*/
	private transient String ipClient = null;
	/** EJB Remoto para bitacora metrics*/
	private transient BitacorizaMetrics metricsBo = null;
	// END [ADD: JMFR CSA Monitoreo Metrics]
		
	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.isban.eTransferNal.ws.moduloSPID.WSConsultaCertificado#
	 * consultaCertificado(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO)
	 */
	@Override
	public BeanResConsultaCertificado consultaCertificado(final BeanReqConsultaCertificado reqBeanConsultaCertificado) {
		// Se crea objeto de respuesta
		BeanResConsultaCertificado response = new BeanResConsultaCertificado();
		initMetricsParams();//Se obtienen parametros de peticion
		senderInit.initMetrics();//Se inicializan dto's para metricas
		timeIn = String.valueOf(System.currentTimeMillis());//Se obtiene el tiempo de inicio para peticion
		initRemoteEJB();//Inicializa ejb's remotos
		// [ADD: JMFR CSA - Se agrega try para evitar caida del servicio y obligar a la insercion del monitoreo metrics]
		try {
			response =  bOConsultaCertificado.consultaCertificado(reqBeanConsultaCertificado);
			if(validaResponse(response)){//Se valida si hubo alguna excepcion
				error = "S";
			}
		} catch (EJBException e) {//Se agrega catch para errores no controlados en el EJB
			LOGGER.error(e);//Se imprime excepcion
			//Se ejecuta insert asincrono para error
			metricsBo.bitacorizaError(senderInit.getDTOError(EnumMetricsErrors.DELTA_INFRA_WS.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_WS.getMensaje() + WSConsultaDetalladaImpl.class.getCanonicalName(),
					e.getMessage() + " " + Arrays.toString(e.getStackTrace())));
			error = "S";//Se asigna estatus de error
		} finally{//Se obliga a finalizar la operativa con insercion asincrona a metrics
			// Se obtiene tiempo de respuesta
			timeOut = String.valueOf(System.currentTimeMillis());
			metricsBo.bitacorizaRequest(senderInit.getMetricRequest(Constantes.SERVICE_FIEL, timeIn, 
					timeOut, ipClient, instanciaWeb,error));//Se ejecuta insert asincrono
		}
		//envia respuesta del servicio
		return response;
	}
	
	/**
	 * Metodo para validar respuesta del servicio y cambiar los estatus de error 
	 * para monitoreo metrics
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
	 * 
	 * @param response respuesta del servicio
	 * @return true si existe un error en el servicio
	 */
	private boolean validaResponse(BeanResConsultaCertificado response){
		boolean errorResponse = false;
		if(Errores.ED00096V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		if(Errores.ED00097V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		if(Errores.ED00098V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		if(Errores.ED00099V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		if(Errores.ED00100V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		if(Errores.ED00101V.equals(response.getCodRespuesta())){
			errorResponse = true;
		}
		return errorResponse;
	}
	
	/**
	 * Metodo para inicializar EJB Remotos
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
	 */
	private void initRemoteEJB(){
		//Utileria para ejb remotos
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		bOConsultaCertificado = (BOConsultaCertificado)globalJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BOConsultaCertificado")
				.withBusinessInterface(BOConsultaCertificado.class).locate();
		//Utileria para ejb remotos
		GlobalJNDIName metricsJNDIName = new GlobalJNDIName();
		metricsBo = (BitacorizaMetrics) metricsJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BitacorizaMetrics")
				.withBusinessInterface(BitacorizaMetrics.class).locate();
	}
	
	/**
     * Metodo para obtener datos de la peticion. Ip del orignen y la instancia web donde
     * se va atender la peticion.
     * 
     * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
     */
	private void initMetricsParams() {
		final ServletContext servletContext = (ServletContext) context
				.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		HttpServletRequest req = (HttpServletRequest) context
				.getMessageContext().get(MessageContext.SERVLET_REQUEST);
		instanciaWeb = servletContext.getAttribute(
				"com.ibm.websphere.servlet.application.host").toString();
		ipClient = req.getRemoteAddr();
	}
}
