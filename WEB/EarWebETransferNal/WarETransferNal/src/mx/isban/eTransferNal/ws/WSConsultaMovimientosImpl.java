/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WSConsultaMovimientosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOConsultaMovimientos;


/**
 * @author mcamarillo
 *
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.WSConsultaMovimientos" ,serviceName="ConsultaMovimientosService")
public class WSConsultaMovimientosImpl implements WSConsultaMovimientos {
	
	/**
	 * Variable serializable
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param movimientos 
	 * 			EntradaStringJsonDTO con la trama de entrada en forma de json o un objeto json
	 * @return
	 * 			EntradaStringJsonDTO con trama de salida en forma de json o un objeto json
	 */
	@Override
	public ResEntradaStringJsonDTO getConsultaMovimientos(EntradaStringJsonDTO movimientos) {
		BOConsultaMovimientos boConsultaMovimientos =null;
			GlobalJNDIName globalJNDIName = new GlobalJNDIName();
			boConsultaMovimientos = (BOConsultaMovimientos)globalJNDIName.withAppName("EarEjbETransferNal")
			.withModuleName("EjbTransferNal").withBeanName("BOConsultaMovimientos")
			.withBusinessInterface(BOConsultaMovimientos.class).locate();
			return  boConsultaMovimientos.getConsultaMovimientos(movimientos);
	}
	

}
