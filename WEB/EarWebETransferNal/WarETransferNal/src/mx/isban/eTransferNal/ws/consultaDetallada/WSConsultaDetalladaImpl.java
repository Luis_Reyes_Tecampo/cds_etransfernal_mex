/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: WSConsultaDetalladaImpl.java
 * 
 * Control de versiones:
 * Version	Date/Hour				By							Company		Description
 * -------	----------------------	--------------------------- ----------	----------------------
 * 1.0.0	 7/02/2017 11:27:15 AM	Juan Jesus Beltran R.		Isban		Creacion
 * 1.0.1	04/04/2018 12:00:00 AM  Juan Manuel Fuentes Ramos	CSA			Implementacion modulo metrics
 */
package mx.isban.eTransferNal.ws.consultaDetallada;

//Imports java
import java.util.Arrays;
//Imports javax
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
//Imports etransferNal
import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;
import mx.isban.eTransferNal.gc.utils.GlobalJNDIName;
import mx.isban.eTransferNal.servicios.ws.BOConsultaDetallada;
//Imports metrics
import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.senders.MetricsSenderInit;
import mx.isban.metrics.service.BitacorizaMetrics;
import mx.isban.metrics.util.EnumMetricsErrors;
//Import log4j
import org.apache.log4j.Logger;

/**
 * Class WSConsultaDetalladaImpl.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@WebService(endpointInterface = "mx.isban.eTransferNal.ws.consultaDetallada.WSConsultaDetallada", serviceName = "ConsultaDetalladaService")
public class WSConsultaDetalladaImpl implements WSConsultaDetallada {
	
	/** Utileria para impresion de log*/
	private static final Logger LOGGER = Logger.getLogger(WSConsultaDetalladaImpl.class);
	/** EJB Remoto para consulta detallada*/
	private transient BOConsultaDetallada boConsultaDetallada = null;

	/** Mensaje de error para TimeOut. ADD: JMFR CSA*/
	private static final String MSG_ERROR_TIMEOUT = 
			"{COD_ERROR:\"EC00030D\",DESCRIPCION:\"No se pudo establecer comunicacion con los servicios\"}.";
	// INI [ADD: JMFR CSA Monitoreo Metrics]
	/** EJB Remoto para bitacora metrics*/
	private transient BitacorizaMetrics metricsBo = null;
	/** Contexto de la aplicacion */
 	@Resource
 	private transient WebServiceContext context;
	/** The ipClient*/
	private transient String ipClient = null;
	/** Utilerias Metricas MetricsSenderInit*/
	private transient MetricsSenderInit senderInit = new MetricsSenderInit();
	/** Instancia que atiende la peticion */
 	private transient String instanciaWeb = null;
 	/** The timeIn*/
	private transient String timeIn = null;
	/** The timeOut*/
	private transient String timeOut = null;
	/** Variable para error*/
 	private transient String error = "N"; 
	// END [ADD: JMFR CSA Monitoreo Metrics]

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.isban.eTransferNal.ws.consultaDetallada.WSConsultaDetallada#
	 * consultaDetallada(mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO)
	 */
	@Override
	public ResEntradaStringJsonDTO consultaDetallada(EntradaStringJsonDTO reqConsultaDet) {
		//Se crea objeto de respuesta
		ResEntradaStringJsonDTO resEntradaStringJsonDTO = new ResEntradaStringJsonDTO();
		initMetricsParams();//Se obtienen parametros de peticion
		senderInit.initMetrics();//Se inicializan dto's para metricas
		timeIn = String.valueOf(System.currentTimeMillis());//Se obtiene el tiempo de inicio para peticion
		initRemoteEJB();//Inicializa ejb's remotos
		// [ADD: JMFR CSA - Se agrega try para evitar caida del servicio y obligar a la insercion del monitoreo metrics]
		try {
			resEntradaStringJsonDTO = boConsultaDetallada.consultaDetallada(reqConsultaDet);
			validaResponse(resEntradaStringJsonDTO);
		} catch (EJBException e) {//Se agrega catch para errores no controlados en el EJB
			LOGGER.error(e);//Se imprime excepcion
			//Se ejecuta insert asincrono para error
			metricsBo.bitacorizaError(senderInit.getDTOError(EnumMetricsErrors.DELTA_INFRA_WS.getCodeError(),
					EnumMetricsErrors.DELTA_INFRA_WS.getMensaje() + WSConsultaDetalladaImpl.class.getCanonicalName(),
					e.getMessage() + " " +  Arrays.toString(e.getStackTrace())));
			error = "S";//Se asigna estatus de error
		} finally{//Se obliga a finalizar la operativa con insercion asincrona a metrics
			// Se obtiene tiempo de respuesta
			timeOut = String.valueOf(System.currentTimeMillis());
			metricsBo.bitacorizaRequest(senderInit.getMetricRequest(Constantes.SERVICE_SPID, timeIn, 
					timeOut, ipClient, instanciaWeb,error));//Se ejecuta insert asincrono
		}
		//envia respuesta del servicio
		return resEntradaStringJsonDTO;
	}
	
	/**
	 * Metodo para validar error en flujo del servicio
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
	 * 
	 * @param resEntradaStringJsonDTO respuesta del servicio
	 */
	private void validaResponse(ResEntradaStringJsonDTO resEntradaStringJsonDTO) {
		if("DELTA999".equals(resEntradaStringJsonDTO.getJson())){
			resEntradaStringJsonDTO.setJson(MSG_ERROR_TIMEOUT);
			error = "S";//Se asigna estatus de error
		}
	}

	/**
     * Metodo para obtener datos de la peticion. Ip del orignen y la instancia web donde
     * se va atender la peticion.
     * 
     * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
     */
	private void initMetricsParams() {
		final ServletContext servletContext = (ServletContext) context
				.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		HttpServletRequest req = (HttpServletRequest) context
				.getMessageContext().get(MessageContext.SERVLET_REQUEST);
		instanciaWeb = servletContext.getAttribute(
				"com.ibm.websphere.servlet.application.host").toString();
		ipClient = req.getRemoteAddr();
	}
	
	/**
	 * Metodo para inicializar EJB Remotos
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos. CSA
     * Plan Delta Sucursales 2018 - 06
	 */
	private void initRemoteEJB(){
		//Utileria para ejb remotos
		GlobalJNDIName globalJNDIName = new GlobalJNDIName();
		boConsultaDetallada = (BOConsultaDetallada) globalJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BOConsultaDetallada")
				.withBusinessInterface(BOConsultaDetallada.class).locate();
		//Utileria para ejb remotos
		GlobalJNDIName metricsJNDIName = new GlobalJNDIName();
		metricsBo = (BitacorizaMetrics) metricsJNDIName.withAppName("EarEjbETransferNal")
				.withModuleName("EjbTransferNal").withBeanName("BitacorizaMetrics")
				.withBusinessInterface(BitacorizaMetrics.class).locate();
	}
}