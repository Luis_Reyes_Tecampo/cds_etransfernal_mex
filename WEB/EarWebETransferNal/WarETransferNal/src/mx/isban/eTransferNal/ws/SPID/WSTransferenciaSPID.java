/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WSTransferenciaSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/01/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.ws.SPID;

import javax.jws.WebService;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * @author cchong
 *
 */
@WebService
public interface WSTransferenciaSPID {
	/**
	 * @param transferencia 
	 * 			EntradaStringJsonDTO con la trama de entrada en forma de json o un objeto json
	 * @return
	 * 			ResEntradaStringJsonDTO con trama de salida en forma de json o un objeto json
	 */
	public ResEntradaStringJsonDTO transferenciaSPID(EntradaStringJsonDTO transferencia);
}
