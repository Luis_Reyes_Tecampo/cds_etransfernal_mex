/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMttoMediosValidar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10-13-2018 10:17:18 PM Ana Gloria Martinez Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import javax.servlet.http.HttpServletRequest;

import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;

 /**
 * The Class UtilsLiquidezIntradia.
 * @author FSW-Vector
 * @since 10-13-2018
 */
public final class UtilsLiquidezIntradia {

/** The instancia. */
private static UtilsLiquidezIntradia instancia;
	
	private UtilsLiquidezIntradia() {
		/**Metodo vacio por ser constructor por default**/
	}

	/**
	 * Obtener el objeto: instancia del objeto UtilsLiquidezIntradia
	 *
	 * @return El objeto: instancia - UtilsLiquidezIntradia
	 */
	public static UtilsLiquidezIntradia getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsLiquidezIntradia();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: anterior.
	 *
	 * Obtiene losd datos del registro anterior
	 * 
	 * @param req El objeto: req - request para valor anterior
	 * @return El objeto: anterior -  valor anterios devuelto
	 */
	public static BeanLiquidezIntradia getAnterior(HttpServletRequest req) {
		BeanLiquidezIntradia liq = new BeanLiquidezIntradia();
		liq.setCveTransFe(req.getParameter("cveTransferOld"));
		liq.setCveMecanismo(req.getParameter("mecanismoOld"));
		liq.setEstatus(req.getParameter("estatusTransferOld"));
		return liq;
	}
}
