/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:39:47 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;

/**
 * Class UtileriasMonitor.
 *
 * Clase de utilerias utilizada por los disitintos flujos para para validar el
 * path del monitor actual.
 * 
 * @author FSW-Vector
 * @since 6/08/2019
 */
public final class UtileriasWebMonitor implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4465230743920607423L;

	/**
	 * Nueva instancia utilerias monitor.
	 */
	private UtileriasWebMonitor() {
		super();
	}

	/**
	 * Carga modulo.
	 * 
	 * Proceso para evaluar el modulo al que se accedio
	 * 
	 * @param req El objeto: req
	 * @return Objeto bean modulo
	 */
	public static BeanModulo cargaModulo(HttpServletRequest req) {
		BeanModulo modulo = new BeanModulo();
		final String path = req.getPathInfo();
		if ("/monitorContingentePOASPEI.do".equals(path)) {
			modulo.setModulo(ConstantesWebPOACOA.CONS_SPEI);
			modulo.setTipo(ConstantesWebPOACOA.CONS_POA);
		}
		if ("/monitorContingenteCOASPEI.do".equals(path)) {
			modulo.setModulo(ConstantesWebPOACOA.CONS_SPEI);
			modulo.setTipo(ConstantesWebPOACOA.CONS_COA);
		}
		if ("/monitorContingentePOASPID.do".equals(path)) {
			modulo.setModulo(ConstantesWebPOACOA.CONS_SPID);
			modulo.setTipo(ConstantesWebPOACOA.CONS_POA);
		}
		if ("/monitorContingenteCOASPID.do".equals(path)) {
			modulo.setModulo(ConstantesWebPOACOA.CONS_SPID);
			modulo.setTipo(ConstantesWebPOACOA.CONS_COA);
		}
		modulo.setPath(path);
		return modulo;
	}
}
