/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsGeneral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     12/09/2019 03:46:28 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

import org.springframework.web.servlet.ModelAndView;

import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;


/**
 * Class UtilsGeneral.
 *
 * Clase de utilerias que contiene
 * metodos utilizados por varios catalogos.
 * 
 * @author FSW-Vector
 * @since 12/09/2019
 */
public final class UtilsGeneral implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8817912925885713289L;
	
	/** La variable que contiene informacion con respecto a: utils. */
	private static UtilsGeneral utils;
	
	/** La constante ACCION_ALTA. */
	private static final String ACCION_ALTA = "alta";
	
	/** La constante ACCION_MODIFICAR. */
	private static final String ACCION_MODIFICAR = "modificar";
	
	/**
	 * Obtiene el objeto utils
	 *
	 * @return utils: regresa el objeto utils cuando este está vacio
	 */
	public static UtilsGeneral getUtils() {
		if (utils == null) {
			utils = new UtilsGeneral();
		}
		return utils;
	}
	
	/**
	 * Sets the error.
	 * 
	 * Metodo para setear el error
	 * al modelo y no repetirlo en todos lo flujos
	 * 
	 * @param model --> objeto tipo ModelAndView añade el objeto al presentarse alguno de los erroes
	 * @return model --> devuelve el codigo, el tipo y la descipción del error.
	 */
	public ModelAndView setError(ModelAndView model) {
		model.addObject(Constantes.COD_ERROR, Errores.EC00011B);
		model.addObject(Constantes.TIPO_ERROR,Errores.TIPO_MSJ_ERROR);
		model.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);
		return model;
	}
	
	/**
	 * Valida filtro.
	 *
	 * Valida que el parametro sea una accion valida
	 * 
	 * @param accion --> variable de tipo String que contiene la accion a realizar
	 * @return estado --> variable de tipo boolean que permite identificar el resultado del filtro. 
	 */
	public boolean validaFiltro(String accion) {
		boolean estado = false;
		if (accion.equals(ACCION_ALTA) || accion.equals(ACCION_MODIFICAR)) {
			estado = true;
		}
		return estado;
	}
}
