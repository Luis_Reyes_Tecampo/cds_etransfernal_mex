package mx.isban.eTransferNal.utilerias;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;


import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

/**
 * @author vectormx
 *
 */
public final class UtilsMapeaRequest {

	/**  Logeo de la clase. */
	private static final Logger LOG = Logger.getLogger(UtilsMapeaRequest.class.getName());

	/**Referencia del Objeto MapeaBean*/
	private static final UtilsMapeaRequest UTILERIAS_INSTACE = new UtilsMapeaRequest();

	/**
	 * Constructor privado
	 */
	private UtilsMapeaRequest() {super();}

	/**
	 * Metodo singleton de MapeaBean
	 * @return MapeaBean Referencia de utilerias
	 */
	public static UtilsMapeaRequest getMapper(){
		return UTILERIAS_INSTACE;
	}

	/**
	 * Anadir a lista.
	 *
	 * @param <T> the generic type
	 * @param numItems the num items
	 * @param lista the lista
	 * @param object the object
	 */
	@SuppressWarnings("unchecked")
	public <T> void anadirALista(String numItems, List<T> lista, T object){
		int count = 0;
		//Se valida que el parametro nos devuelva el num de registros
		if(numItems!=null){
			count = Integer.parseInt(numItems);
		}
		//Se recorre el num de nuevos registros
		for(int x = 0; x < count; x ++){
			Object newObject = null;
			try {
				//Se instancia nuevo objeto
				newObject = object.getClass().newInstance();
			} catch (IllegalAccessException e) {
				LOG.error(e.getMessage(),e);
			} catch (InstantiationException e) {
				LOG.error(e.getMessage(),e);
			}
			//Se valida que no sea nulo
			if (newObject != null){
				lista.add((T) newObject);
			}
		}
	}
	
    /**
     * Gets the paginador from request.
     *
     * @param parameters the parameters
     * @return the paginador from request
     */
    //Metodo que mapea un paginador a partir del request
    @SuppressWarnings("rawtypes")
	public BeanPaginador getPaginadorFromRequest(Map parameters){
        BeanPaginador beanPaginador = new BeanPaginador();
        mapearObject(beanPaginador, parameters);
        return beanPaginador;
    }
	
	/**
	 * Mapear object.
	 *
	 * @param object the object
	 * @param parameters the parameters
	 * @return true, if successful
	 */
    @SuppressWarnings("rawtypes")
	public void mapearObject(Object object, Map parameters){
		try {
			BeanUtils.populate(object, parameters);
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(),e);
			//Se muestra excepcion, no es necesario mayor procesamiento ya que el bean ya esta inicializado
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(),e);
			//Se muestra excepcion, no es necesario mayor procesamiento ya que el bean ya esta inicializado
		}
	}
}
