/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsConstantes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:37:04 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;

/**
 * Class UtilsConstantes.
 *
 * Clase que contiene variables utilizadas en los
 * catalogos.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public final class UtilsConstantes implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4065019124121286807L;

	/** La constante BEAN_PAGINADOR. */
	public static final String BEAN_PAGINADOR = "beanPaginador";
	
	/** La constante BEAN_FILTER. */
	public static final String BEAN_FILTER = "beanFilter";
}
