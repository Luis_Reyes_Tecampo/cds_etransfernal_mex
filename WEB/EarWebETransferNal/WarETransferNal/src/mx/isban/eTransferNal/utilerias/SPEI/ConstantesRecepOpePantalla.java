package mx.isban.eTransferNal.utilerias.SPEI;

/**
 * en esta clase se encuentran las variables que se ocupan en los controladores 
 * de detalle de operaciones,recepcion de operaciones historicas y del dia.
 *
 * @author FSW-Vector
 * @since 18/09/2018
 */
public final class ConstantesRecepOpePantalla {
	
	
	/** La constante RECEPCION_OPERACION_SDO_DIA. */
	public static final String RECEPCION_OPERACION_SDO_DIA= "recepcionOperacion";
	
	/** La constante RECEPCION_OPERACION_SDO_HTO. */
	public static final String RECEPCION_OPERACION_SDO_HTO = "recepcionOperacionHto";
	
	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	public static final String RECEPCION_OPERACION_SPEI_HTO = "recepcionOperaHistoricaSPEI";
	
	/** Propiedad del tipo String que almacena el valor de PAGINA. */
	public static final String RECEPCION_OPERACION_SPEI_DIA = "recepcionOperaSPEI";
	
	/** Propiedad del tipo String que almacena el valor de BEAN_RES. */
	public static final String BEAN_RES = "beanResTranSpeiRec";
	
	/** Propiedad del tipo String que almacena el valor de BEAN_RES. */
	public static final String BEAN_TRANS = "beanTranSpeiRecOper";
	
	/** constante de error *. */
	public static final String ERROR =" Se genero el error: ";
		
	/** Propiedad del tipo String que almacena el valor de TRIB. */
	public static final String TRIB = "TRIB";
	
	/** La constante ENTIDADSPEICA. */
	public static final String ENTIDADSPEICA ="ENTIDAD_SPEI_CA";
	
	/** La constante SALDOS. */
	public static final String SALDOS ="SALDOS";
	
	/** La constante HISTORICO. */
	public static final String HISTORICO = "rOperaHis";
	
	/** La constante ROPERA. */
	public static final String ROPERA ="rOpera"; 
	
	/** La constante SPEI. */
	public static final String SPEI ="SPEI";
	
	/** La constante ENTIDAD_SPEI. */
	public static final String ENTIDAD_SPEI ="ENTIDAD_SPEI";

	/**
	 *  
	 * constructor.
	 */
	 private ConstantesRecepOpePantalla() {
	 }
}
