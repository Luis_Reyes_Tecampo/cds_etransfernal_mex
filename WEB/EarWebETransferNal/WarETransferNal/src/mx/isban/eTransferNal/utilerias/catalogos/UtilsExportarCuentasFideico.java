/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarCuentasFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 01:22:00 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;

/**
 * Class UtilsExportarCuentasFideico.
 *
 * Clase que contiene metodos utilizados en el flujo
 * de exportacion del catalogo.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
public final class UtilsExportarCuentasFideico extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3553024843195371790L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarCuentasFideico instancia;

	/**
	 * Nueva instancia utils exportar nombres fideico.
	 */
	private UtilsExportarCuentasFideico() {
	}
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @return El objeto: instancia unica de la clase
	 */
	public static UtilsExportarCuentasFideico getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarCuentasFideico();
		}
		return instancia;
	}
	
	/**
	 * Obtener el objeto: header excel.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar.
	 *  
	 * @param ctx El objeto: ctx para los encabezados
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcelSPID(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.cuentasFideico.lbl.cuenta"),
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtener el objeto: body.
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 *  
	 *
	 * @param cuentas El objeto: cuentas datos del cuerpo del archivo
	 * @return El objeto: body
	 */
	public List<List<Object>> getBodySPID(List<BeanCuenta> cuentas) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (cuentas != null && !cuentas.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanCuenta nombre : cuentas) {
				objListParam = new ArrayList<Object>();
				objListParam.add(nombre.getCuenta());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
	 /**
     * Obtener el objeto: header excel.
     *
     * Metodo que crear el cuerpo del archivo de
     * excel par el flujo de exportar.
     *  
     * @param ctx El objeto: ctx datos del encabezado
     * @return El objeto: header excel
     */
    public List<String> getHeaderExcel(RequestContext ctx) {
        String[] header = new String[] { 
                ctx.getMessage("catalogos.excepCtasFideicomiso.numCuenta"),
                };
        return Arrays.asList(header);
    }
    
    /**
     * Obtener el objeto: body.
     * 
     * Metodo que crear el cuerpo del archivo de
     * excel par el flujo de exportar
     *  
     *
     * @param cuentas El objeto: cuentas para el cuerpo del archivo
     * @return El objeto: body
     */
    public List<List<Object>> getBody(List<BeanCtaFideicomiso> cuentas) {
        List<Object> objListParam = null;
        List<List<Object>> objList = null;
        if (cuentas != null && !cuentas.isEmpty()) {
            objList = new ArrayList<List<Object>>();
            for (BeanCtaFideicomiso cuenta : cuentas) {
                objListParam = new ArrayList<Object>();
                objListParam.add(cuenta.getCuenta());
                objList.add(objListParam);
            }
        }
        return objList;
    }
}
