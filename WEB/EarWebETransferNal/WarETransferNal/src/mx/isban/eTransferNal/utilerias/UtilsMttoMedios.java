/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsMttoMedios.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     21/11/2019 01:53:35 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
/**
 * La clase UtilsMttoMedios.
 * 
 * Clase utilizada para llevar a cabo la construccion del encabezado
 * y cuerpo del archivo excel al momento de exportar del catalogo
 *  de Medios Autorizados
 */
public final class UtilsMttoMedios {
	
	/**Se crea el constructor*/
	private UtilsMttoMedios() {
	}

	/**
	 * Procedimiento para retornar los valores del encabezado en el archivo
	 *
	 * @param ctx the ctx  --> Parametro para obtener el contexto de los encabezados
	 * @return the header excel --> Lista qque contiene el valor de los encabezados
	 */
	public static List<String> getHeaderExcel(RequestContext ctx){
		/** Se crea un array donde se incluyen como mensaje la referencia de los properties */
	   	 String []header = new String [] { 
	   			 /**Asignamos los valores al array*/
	   			 /** En el mensaje ponemos las properties**/
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtClaveMedioEntrega"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtDescripcionMedioE"),	
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtClaveTransfer"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtDescripcionTrasfer"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtClaveOperacion"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtDescOperacion"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtCLACON"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtSucOperante"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtInicio"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtCierre"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtHorarioMin"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtInhab"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtCLACONTEF"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtMecanismo"),
	   			 ctx.getMessage("catalogos.mantoMedios.txt.txtLimiteImporte"),
	   			ctx.getMessage("catalogos.mantoMedios.txt.txtSeguro")
	   			
	   			
	   			 }; 
	   	/** Retornamos el encabezado*/
	   	 	return Arrays.asList(header);
	    }
	
	/**
	 * Procedimiento para retornar los valores de la consulta que se agregan al archivo
	 *
	 * @param listaBean the lista bean --> Lista con la informacion de la pantalla
	 * @return the body --> Lista con el con el cuerpo del archivo
	 */
	public static List<List<Object>> getBody(List<BeanMttoMediosAutorizadosReq> listaBean){
		/** Se crea la lista que almacenara cada registro**/
	 	List<Object> objListParam = null;
	 	/**Se crea una lista para almacenar todos los registros**/
  	 	List<List<Object>> objList = null;
  	 	/**Se valida que el bean no veanga nulo o este vacio*/
  	 	if(listaBean!= null && 
		   !listaBean.isEmpty()){
  	 		/** Se inicializa la lista*/
  		   objList = new ArrayList<List<Object>>();
  		   /**Se comienza a asignar los valores**/
  		   for(BeanMttoMediosAutorizados bean : listaBean){ 
  			   /**Inicializamos el objecto que guardara el registro actual*/
  	 		  objListParam = new ArrayList<Object>();
  	 		  String seguro ="Seguro";
  	 		  if(bean.getCanal()==null ||!"1".equals(bean.getCanal())) {
  	 			seguro ="No Seguro";
  	 		  }
  	 		  /**Asigamos los valores al bean*/
  	 		  objListParam.add(bean.getCveMedioEnt());
  	 		  objListParam.add(bean.getDesCveMed());
  	 		  objListParam.add(bean.getCveTransfe());
  	 		  objListParam.add(bean.getDescTrasferencia());
  	 		  objListParam.add(bean.getCveOperacion());
  	 		  objListParam.add(bean.getDescOperacion());
  	 		  objListParam.add(bean.getCveCLACON());
  	 		  objListParam.add(bean.getSucOperante());
  	 		  objListParam.add(bean.getHoraInicio());
  	 		  objListParam.add(bean.getHoraCierre());
  	 		  objListParam.add(bean.getHorarioEstatus());
  	 		  objListParam.add(bean.getFlgInHab());
  	 		  objListParam.add(bean.getCveCLACONTEF());
  	 		  objListParam.add(bean.getCveMecanismo());
  	 		  objListParam.add(bean.getLimiteImporte());
  	 		  objListParam.add(seguro);
  	 		  /**Agregamos el objecto a las lista**/
  	 		  objList.add(objListParam);
  	 	   }
  	   }
  	/** Retornamos el objecto de listas*/
   	return objList;
	}
	
}
