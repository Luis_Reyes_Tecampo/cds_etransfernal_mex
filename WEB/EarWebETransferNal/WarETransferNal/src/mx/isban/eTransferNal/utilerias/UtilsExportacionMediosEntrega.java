/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportacionCertificados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 01:04:19 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;

/**
 * Class UtilsMttoMedios.
 * 
 * Permite realizar la exportacion Online de la pantalla de 
 * Mantenimiento de Certificados
 *
 * @author FSW-Vector
 * @since 12/09/2018
 */
public class UtilsExportacionMediosEntrega extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 685191533596096048L;
	
	/** Referencia del Objeto Utilerias. */
	private static final UtilsExportacionMediosEntrega UTILERIAS = new UtilsExportacionMediosEntrega();
	
	/**
	 * Se crea el constructor.
	 */
	private UtilsExportacionMediosEntrega() {
	}

	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtilsExportacionMediosEntrega getUtilerias(){
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { ctx.getMessage("catalogos.mttomedioentrega.txt.txtcvemedioent"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtdescripcion"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtcomanualpf"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtcomanualpm"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtckkseguridad"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtcanal"),
				ctx.getMessage("catalogos.mttomedioentrega.txt.txtimporteMax")};

		return Arrays.asList(header);
	}
	
	/**
	 * Metodo privado que sirve para obtener los datos para el excel.
	 *
	 * @param certificados El objeto: certificados
	 * @return List<List<Object>> Objeto lista de lista de objetos con los datos del
	 *         excel
	 */
	public List<List<Object>> getBody(List<BeanMedioEntrega> medios) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (medios != null && !medios.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanMedioEntrega certificado : medios) {
				objListParam = new ArrayList<Object>();
				objListParam.add(certificado.getCveMedioEntrega());
				objListParam.add(certificado.getDesc());
				objListParam.add(certificado.getComAnualPF());
				objListParam.add(certificado.getComAnualPM());
				objListParam.add(certificado.getSeguridad());
				objListParam.add(certificado.getCanal());
				objListParam.add(certificado.getImporteMax());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
}
