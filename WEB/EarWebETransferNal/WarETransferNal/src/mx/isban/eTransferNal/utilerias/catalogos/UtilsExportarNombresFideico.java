/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarNombresFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 02:22:06 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;

/**
 * Class UtilsExportarNombresFideico.
 *
 * Clase que contiene metodos utilizados en el flujo
 * de exportacion del catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public final class UtilsExportarNombresFideico extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3553024843195371790L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarNombresFideico instancia;

	/**
	 * Nueva instancia utils exportar nombres fideico.
	 */
	private UtilsExportarNombresFideico() {
	}
	
	/**
	 * Obtiene el objeto instancia
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @return instancia: variable de tipo clase UtilsExportarNombresFideico, permite hacer la instancia
	 */
	public static UtilsExportarNombresFideico getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarNombresFideico();
		}
		return instancia;
	}
	
	/**
	 * Obtiene el objeto header excel.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar.
	 *  
	 * @param ctx --> objeto de tipo RequestContext que obtiene los requisitos del contexto para generar el excel
	 * @return header --> objeto excel que obtiene el header
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.nombresFideico.lbl.nombre"),
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtiene el objeto body.
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 *  
	 *
	 * @param nombres --> obtiene el listado de nombres de fideicomiso
	 * @return objList --> devuelve el listado de nombres 
	 */
	public List<List<Object>> getBody(List<BeanNombreFideico> nombres) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (nombres != null && !nombres.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanNombreFideico nombre : nombres) {
				objListParam = new ArrayList<Object>();
				objListParam.add(nombre.getNombre());
				objList.add(objListParam);
			}
		}
		return objList;
	}
}
