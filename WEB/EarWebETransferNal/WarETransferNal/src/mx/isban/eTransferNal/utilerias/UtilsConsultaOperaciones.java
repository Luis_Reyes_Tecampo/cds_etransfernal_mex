package mx.isban.eTransferNal.utilerias;

import java.util.List;

import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperacionesDetalle;

/**
 * The Class UtilsConsultaOperaciones.
 */
public class UtilsConsultaOperaciones {

	/**
	 * Llenar datos de request.
	 *
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param beanResConsOperaciones the bean res cons operaciones
	 */
	//Metodo para llenar los datos del request en el bean
	public void llenarDatosDeRequest(BeanReqConsOperaciones beanReqConsOperaciones, BeanResConsOperacionesDetalle beanResConsOperaciones){
		beanResConsOperaciones.setUsuarioAutoriza(beanReqConsOperaciones.getUsuarioAutoriza());
		beanResConsOperaciones.setUsuarioCaptura(beanReqConsOperaciones.getUsuarioCaptura());
		beanResConsOperaciones.setTemplate(beanReqConsOperaciones.getTemplate());
		beanResConsOperaciones.setFechaCaptura(beanReqConsOperaciones.getFechaCaptura());
		String estatusOperacion=beanReqConsOperaciones.getEstatusOperacion();
		//Si esta seleccionado seleccione se pasa a un espacio en blanco
		if("Seleccione".equals(estatusOperacion)){
			estatusOperacion=" ";
		}
		beanResConsOperaciones.setEstatusOperacion(estatusOperacion);
		beanResConsOperaciones.setPaginador(beanReqConsOperaciones.getPaginador());
		beanResConsOperaciones.setFolio(beanReqConsOperaciones.getFolio());
	}
	
	/**
	 * Poner lista layout como constantes.
	 *
	 * @param listLayout the list layout
	 */
	//Metodo para poner la lista de layouts como constantes
	public void ponerListaLayoutComoConstantes(List<BeanLayout> listLayout){
		for(BeanLayout beanLayout : listLayout){
			//Se obtiene la constnte
			String constante=beanLayout.getConstante();
			String newValue=" ";
			if(constante == null || "".equals(constante) || " ".equals(constante)){
				String valActual=beanLayout.getValorActual();
				//Se valida valorActual
				if(!(valActual == null || "".equals(valActual) || " ".equals(valActual))){
					newValue=valActual;
				}
			}else{
				newValue=constante;
			}
			//Se setea la constante
			beanLayout.setConstante(newValue);
		}
	}
	
}
