package mx.isban.eTransferNal.utilerias;

import java.text.SimpleDateFormat;
import java.util.Date;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

/**
 * 
 * The Class MetodosAutorizarOperaciones.
 * 
 */
public class MetodosRepoDiaSpeiCero extends Architech{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4781062562930111492L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE
	 */
	public static final String PAGE = "moduloReporteDia";
	/**
	 * Propiedad del tipo String que almacena el valor de PAGE
	 */
	public static final String PAGE_HTO = "moduloReporteHis";
	/**
	 * Propiedad del tipo String que almacena el valor de BEAN_RES
	 */
	public static final String BEAN_RES = "beanResSpeiCero";
	/**
	 * propiedad que almacena instiPrincipal
	 */	
	public static final String PRINCIPAL = "instiPrincipal";
	/**
	 * propiedad que almacena instiAlterna
	 */	
	public static final String ALTERNA = "instiAlterna";
	/**
	 * propiedad que almacena instiPrincipal
	 */	
	public static final String PREVIOA = "prevSpeiCero";
	/**
	 * propiedad que almacena duranSpeiCero
	 */	
	public static final String DURANTEA = "duranSpeiCero";
	
	/**
	 * propiedad que almacena CERO
	 */	
	public static final String CERO = "0";
	
	/**
	 * propiedad que almacena UNO
	 */	
	public static final String UNO = "1";
	
	
	/** constructor**/
	public MetodosRepoDiaSpeiCero(){
		super();
	}

	
	
	/**
	 * 
	 * @param beanResSpeiCero
	 * @param historico
	 * @param ctx
	 * @param correcto
	 * @return
	 */
	public ModelAndView continuaInicializa(BeanResSpeiCero beanResSpeiCero,boolean historico, RequestContext ctx, boolean correcto){
		ModelAndView mdlVista = null;
		/**
		 * se pasan valroes
		 */
		beanResSpeiCero.setOpcion(PRINCIPAL);
		beanResSpeiCero.setOpcionCero(PREVIOA);
		/** si viene historico asigna el valor de uno**/
		if(historico) {
			beanResSpeiCero.setHistorico(UNO);			
		}else {
			beanResSpeiCero.setHistorico(CERO);
		}
		/**Asigna valores**/
		beanResSpeiCero.setExportar(CERO);		
		beanResSpeiCero.setRptTodo(CERO);
		debug("consTranSpeiRec responseEmiReporte Codigo de error:" + beanResSpeiCero.getCodError());
		/**Selecciona la pantalla **/
		if(historico) {
			mdlVista = new ModelAndView(PAGE_HTO, BEAN_RES, beanResSpeiCero);
		}else {
			mdlVista = new ModelAndView(PAGE, BEAN_RES, beanResSpeiCero);
		}
		/**en caso se ser incorrecto manda el error **/
		if(correcto) {
			String mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + beanResSpeiCero.getCodError());
			mdlVista.addObject(Constantes.COD_ERROR, beanResSpeiCero.getCodError());
			mdlVista.addObject(Constantes.TIPO_ERROR, beanResSpeiCero.getTipoError());
			mdlVista.addObject(Constantes.DESC_ERROR, mensaje);
		}else {
			mdlVista = msjError(  mdlVista); 
		}
		/**selecciona el froma de la pantalla**/
		if(!historico) {
			mdlVista.addObject( "frm", "ReporteDia" );
		}else {
			mdlVista.addObject( "frm", "ReporteHis" );
		}
		
		return mdlVista;
	}
	/**
	 * 
	 * @param beanReqSpeiCero
	 */	
	public void buscarDto(BeanReqSpeiCero beanReqSpeiCero) {	
		/** Asigna el dato segun el instituto seleccionado**/
		if(PRINCIPAL.equals(beanReqSpeiCero.getOpcion())) {
			beanReqSpeiCero.setInstiAlterna(false);
			beanReqSpeiCero.setInstiPrincipal(true);
		}else if(ALTERNA.equals(beanReqSpeiCero.getOpcion())){
			beanReqSpeiCero.setInstiAlterna(true);
			beanReqSpeiCero.setInstiPrincipal(false);
		}else {
			/**todo falso en caso de que evenga la obcion todo **/
			beanReqSpeiCero.setInstiAlterna(false);
			beanReqSpeiCero.setInstiPrincipal(false);
		}
		/**Asiogna el valor segun lo seleccionado **/
		if(PREVIOA.equals(beanReqSpeiCero.getOpcionCero())) {
			beanReqSpeiCero.setPrevSpeiCero(false);
			beanReqSpeiCero.setDuranSpeiCero(true);
		}else if(DURANTEA.equals(beanReqSpeiCero.getOpcionCero())){
			beanReqSpeiCero.setPrevSpeiCero(true);
			beanReqSpeiCero.setDuranSpeiCero(false);
		}else {
			/**todo falso en caso de que evenga la obcion todo **/
			beanReqSpeiCero.setPrevSpeiCero(false);
			beanReqSpeiCero.setDuranSpeiCero(false);
		}
		
		
	}
	
	/**
	 * 
	 * @param beanReqSpeiCero
	 */
	public  void fechas (BeanReqSpeiCero beanReqSpeiCero) {
		String fi = beanReqSpeiCero.getHrInicio()+":"+beanReqSpeiCero.getMinInicio();
		String fin = beanReqSpeiCero.getHrFin()+":"+beanReqSpeiCero.getMinFin();
		/**Obntiene la fecha y la junta para la consulta**/
		if(beanReqSpeiCero.getHistorico().equals(UNO)) {
			/**fechas segun si es historico**/
			fi = beanReqSpeiCero.getHoraFecOperacion()+" "+ fi;
			fin = beanReqSpeiCero.getHoraFecOperacion()+" "+ fin;			
		}else {
			Date ahora = new Date();
			SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
			String fecha =d.format(ahora);
			/**concatena las fechas **/
			fi = fecha+" "+fi;
			fin =fecha+" "+fin;
		}
		/**regresa la fecha obtenida **/
		beanReqSpeiCero.setHoraInicio(fi);
		beanReqSpeiCero.setHoraFin(fin);
	}
	
	
	
	
	/**
	 * funcion para meter el codigo de error cuando suge una excepcion
	 * @param mdlVista
	 * @return
	 */
	public ModelAndView msjError( ModelAndView mdlVista) {
		/**Siexiste un error lo manda **/
		mdlVista.addObject(Constantes.DESC_ERROR, Errores.DESC_EC00011B);	
		mdlVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
		mdlVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
		/**regresa objeto **/
		return  mdlVista;
	}

	
	
}
