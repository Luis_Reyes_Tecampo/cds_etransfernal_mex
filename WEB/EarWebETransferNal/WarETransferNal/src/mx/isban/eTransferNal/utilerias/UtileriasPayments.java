package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.support.RequestContext;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * @version 1.0
 * @Class UtileriasPayments
 * Utilerias para payments
 */
public class UtileriasPayments extends Architech{

	/** La constante serialVersionUID. 
	 * El proceso de serializaci�n asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calcular� un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 1L;
	
	/** Referencia del Objeto instancia. */
	protected static UtileriasPayments instancia;
	
	/** Referencia del Objeto Utilerias. */
	private static final UtileriasPayments UTILERIAS = new UtileriasPayments();
	
	
	/**
	 * Se crea el constructor.
	 */
	public UtileriasPayments() {
		// Constructor de la clase UtilCausasDevolucion
	}


	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasPayments getUtilerias() {
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: header excel.
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx){
		String[] header = new String[]{
			ctx.getMessage("catalogos.payments.clave"),
			ctx.getMessage("catalogos.payments.descripcion"),
			ctx.getMessage("catalogos.payments.actdes")
		};
		return Arrays.asList(header);
	}
	

	/**
	 * Metodo privado que sirve para obtener los datos para el excel.
	 * @param consulPayments lista de canales de id �nico
	 * @return List<List<Object>> Objeto lista de lista de objetos con los datos del
	 * excel
	 */	     
	public List<List<Object>> getBody(List<BeanConsultaPayme> consulPayments){
		List<Object> objectListParam = null;
		List<List<Object>> objList = null;
		
		if(consulPayments != null && !consulPayments.isEmpty()){
			objList = new ArrayList<List<Object>>();
			for(BeanConsultaPayme cod : consulPayments ){
				objectListParam = new ArrayList<Object>();
				
				objectListParam.add(cod.getClave());
				objectListParam.add(cod.getDescripcion());
				objectListParam.add(cod.getActDes());
				objList.add(objectListParam);
			}
		}
		return objList;
	}
	

	/**
	 * Obtener el objeto: header excel.
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(ResourceBundleMessageSource ctx){
		String[] header = new String[]{
			ctx.getMessage("catalogos.payments.clave", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.payments.descripcion", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.payments.actdes", null, LocaleContextHolder.getLocale())
		};
		return Arrays.asList(header);
	}	
	
	/**
	 * Obtener el objeto: error.
	 * @param responseDAO respuesta de BD
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO){
		BeanResBase error = new BeanResBase();
		if(responseDAO == null){
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
	

}
