/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExportarOrdenes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/08/2019 12:20:28 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;

/**
 * Class UtileriasExportarOrdenes.
 *
 * Clase de utilerias utilizada por la pantalla de ordenes que contiene metodos
 * necesarios para realizar el fujo de exportacion.
 * 
 * @author FSW-Vector
 * @since 9/08/2019
 */
public final class UtileriasExportarOrdenes implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3760046646363525969L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasExportarOrdenes utils;

	/**
	 * Obtener el objeto: utils.
	 *
	 * Obtener la instancia de la clase 
	 * de utilerias.
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasExportarOrdenes getUtils() {
		if (utils == null) {
			utils = new UtileriasExportarOrdenes();
		}
		return utils;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * Metodo para obtener el header usado en el 
	 * archivo de excel cuando se realiza el flujo
	 * de exportar.
	 * 
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { ctx.getMessage("moduloSPID.recepcionOperacion.text.refe"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.cola"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.claveTran"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.macan"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.medioEnt"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.fchCap"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.ord"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.rec"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.importeAbono"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.est"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.folioPaquete"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.folioPago"),
				ctx.getMessage("moduloSPID.recepcionOperacion.text.mensajeError") };
		return Arrays.asList(header);
	}

	/**
	 * Obtener el objeto: body.
	 *
	 * Metodo para obtener el cuerpo del
	 * archivo excel cuando se realiza el flujo de
	 * exportar.
	 * 
	 * @param listaBeanOrdenReparacion El objeto: lista bean orden reparacion
	 * @return El objeto: body
	 */
	public List<List<Object>> getBody(List<BeanOrdenReparacion> listaBeanOrdenReparacion) {
		List<Object> objListParamExpOrd = null;
		List<List<Object>> objListExpOrd = null;
		if (listaBeanOrdenReparacion != null && !listaBeanOrdenReparacion.isEmpty()) {
			objListExpOrd = new ArrayList<List<Object>>();

			for (BeanOrdenReparacion beanOrdenReparacion : listaBeanOrdenReparacion) {
				objListParamExpOrd = new ArrayList<Object>();
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getReferencia());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveCola());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveTran());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveMecanismo());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveMedioEnt());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFechaCaptura());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeOrd());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getCveIntermeRec());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getImporteAbono());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getEstatus());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFolioPaquete());
				objListParamExpOrd.add(beanOrdenReparacion.getBeanOrdenReparacionDos().getFoloPago());
				objListParamExpOrd.add(beanOrdenReparacion.getMensajeError());
				objListExpOrd.add(objListParamExpOrd);
			}
		}
		return objListExpOrd;
	}
	
	/**
	 * Obtener el objeto: servlet.
	 *
	 * Metodo para obtener el request 
	 * auxiliar al controller.
	 * 
	 * @return El objeto: servlet
	 */
	public HttpServletRequest getServlet() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
}
