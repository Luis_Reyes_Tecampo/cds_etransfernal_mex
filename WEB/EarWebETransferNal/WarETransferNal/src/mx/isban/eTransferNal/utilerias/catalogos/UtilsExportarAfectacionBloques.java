/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarAfectacioBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:37:41 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;

/**
 * Class UtilsExportarAfectacioBloques.
 * 
 * Clase que contiene metodos utilizados en el flujo
 * de exportacion del catalogo.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public final class UtilsExportarAfectacionBloques extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 685191533596096048L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarAfectacionBloques instancia;
	
	/**
	 * Se crea el constructor.
	 */
	private UtilsExportarAfectacionBloques() {
	}

	/**
	 * Obtiene el objeto instancia
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @return instancia: variable de tipo clase UtilsExportarNombresFideico, permite hacer la instancia
	 */
	public static UtilsExportarAfectacionBloques getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarAfectacionBloques();
		}
		return instancia;
	}

	/**
	 * Obtiene el objeto header excel.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar.
	 *  
	 * @param ctx --> objeto de tipo RequestContext que obtiene los requisitos del contexto para generar el excel
	 * @return header --> objeto excel que obtiene el header
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.afectacionBloques.lblNumBloque"),
				ctx.getMessage("catalogos.afectacionBloques.lblValor"),
				ctx.getMessage("catalogos.afectacionBloques.lblDescripcion") 
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtiene el objeto body.
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 *  
	 *
	 * @param bloques --> obtiene el listado de bloques
	 * @return objList --> devuelve el listado de bloques 
	 */
	public List<List<Object>> getBody(List<BeanBloque> bloques) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (bloques != null && !bloques.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanBloque bloque : bloques) {
				objListParam = new ArrayList<Object>();
				objListParam.add(validaInteger(bloque.getNumBloque()));
				objListParam.add(bloque.getValor());
				objListParam.add(bloque.getDescripcion());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
	/**
	 * Valida el objeto número de bloque
	 * 
	 * @param numeroBloque --> Objeto numero de bloque que es validado
	 * @return valor --> variable de tipo integer que trae el resultado de la validación
	 */
	private Integer validaInteger(Object numeroBloque) {
		Integer valor = 0;
		if (numeroBloque != null && !numeroBloque.toString().isEmpty()) {
			valor = Integer.parseInt(numeroBloque.toString());
		}
		return valor;
	}
}
