/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportacionCertificados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 01:04:19 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;

/**
 * Class UtilsMttoMedios.
 * 
 * Permite realizar la exportacion Online de la pantalla de 
 * Mantenimiento de Certificados
 *
 * @author FSW-Vector
 * @since 12/09/2018
 */
public class UtilsExportacionCertificados extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 685191533596096048L;
	
	/** Referencia del Objeto Utilerias. */
	private static final UtilsExportacionCertificados UTILERIAS = new UtilsExportacionCertificados();
	
	/**
	 * Se crea el constructor.
	 */
	private UtilsExportacionCertificados() {
	}

	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtilsExportacionCertificados getUtilerias(){
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { ctx.getMessage("catalogos.certificadosCifrado.canal"),
				ctx.getMessage("catalogos.certificadosCifrado.alias"),
				ctx.getMessage("catalogos.certificadosCifrado.numero"),
				ctx.getMessage("catalogos.certificadosCifrado.firma"),
				ctx.getMessage("catalogos.certificadosCifrado.cifrado"),
				ctx.getMessage("catalogos.certificadosCifrado.tecnologiaOrigen"),
				ctx.getMessage("catalogos.certificadosCifrado.vigencia"),
				ctx.getMessage("catalogos.certificadosCifrado.estatus") };
		return Arrays.asList(header);
	}
	
	/**
	 * Metodo privado que sirve para obtener los datos para el excel.
	 *
	 * @param certificados El objeto: certificados
	 * @return List<List<Object>> Objeto lista de lista de objetos con los datos del
	 *         excel
	 */
	public List<List<Object>> getBody(List<BeanCertificadoCifrado> certificados) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (certificados != null && !certificados.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanCertificadoCifrado certificado : certificados) {
				objListParam = new ArrayList<Object>();
				objListParam.add(certificado.getCanal());
				objListParam.add(certificado.getAlias());
				objListParam.add(certificado.getNumCertificado());
				objListParam.add(certificado.getAlgDigest());
				objListParam.add(certificado.getAlgSimetrico());
				objListParam.add(certificado.getTecOrigen());
				objListParam.add(certificado.getFchVencimiento());
				objListParam.add(certificado.getEstatus());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
}
