/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/08/2019 12:20:21 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Class UtileriasPagos.
 *
 * Clase de utilerias utilizada por la pantalla de pagos que contiene metodos
 * necesarios para culminar los flujos de respuesta.
 * 
 * @author FSW-Vector
 * @since 9/08/2019
 */
public final class UtileriasWebPagos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8903804012892729492L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasWebPagos utils;

	/** La constante ORDENES_DV. */
	private static final String ORDENES_DV = "ORDENES_DV";
	
	/** La constante TRASPASOS_SS. */
	private static final String TRASPASOS_SS = "TRASPASOS_SS";
	
	/** La constante ORDENES_CO. */
	private static final String ORDENES_CO = "ORDENES_CO";
	
	/** La constante DEVOLUCIONES_CO. */
	private static final String DEVOLUCIONES_CO = "DEVOLUCIONES_CO";
	
	/** La constante TRASPASOS_ES. */
	private static final String TRASPASOS_ES = "TRASPASOS_ES";
	
	/** La constante TRASPASOS_EN. */
	private static final String TRASPASOS_EN = "TRASPASOS_EN";
	
	/** La constante TRASPASOS_PR. */
	private static final String TRASPASOS_PR = "TRASPASOS_PR";
	
	/** La constante ORDENES_ES. */
	private static final String ORDENES_ES = "ORDENES_ES";
	
	/** La constante ORDENES_EN. */
	private static final String ORDENES_EN = "ORDENES_EN";
	
	/** La constante ORDENES_POR_DEV. */
	private static final String ORDENES_POR_DEV = "Ordenes por Devolver";
	
	/** La constante TRASP_SPID_SIAC. */
	private static final String TRASP_SPID_SIAC = "Traspasos SPID-SIAC";
	
	/** La constante ORDENES_ENV_CO. */
	private static final String ORDENES_ENV_CO = "Ordenes Enviadas Confirmadas";
	
	/** La constante DEVOL_ENV_CO. */
	private static final String DEVOL_ENV_CO = "Devoluciones Enviadas Confirmadas";
	
	/** La constante TRASP_SPID_SIAC_ES. */
	private static final String TRASP_SPID_SIAC_ES = "Traspasos SPID-SIAC en Espera";
	
	/** La constante TRASP_SPID_SIAC_EN. */
	private static final String TRASP_SPID_SIAC_EN = "Traspasos SPID-SIAC Enviadas";
	
	/** La constante TRASP_SPID_SIAC_POR_RE. */
	private static final String TRASP_SPID_SIAC_POR_RE = "Traspasos SPID-SIAC por Reparar";
	
	/** La constante ORDENES_ESP. */
	private static final String ORDENES_ESP = "Ordenes en Espera";
	
	/** La constante LISTADO_PAGOS_ORD_ENV. */
	private static final String LISTADO_PAGOS_ORD_ENV = "Listado de Pagos de Ordenes Enviadas";
	
	
	/** La variable que contiene informacion con respecto a: claves. */
	private static String[] claves = new String[9];
	
	static {
		claves[0]="OPD";
		claves[1]="TSS";
		claves[2]="OEC";
		claves[3]="DEC";
		claves[4]="TES";
		claves[5]="TEN";
		claves[6]="TPR";
		claves[7]="OES";
		claves[8]="OEN";
	}
	/**
	 * Obtener el objeto: utils.
	 *
	 * Obtener la instancia de la clase 
	 * de utilerias.
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasWebPagos getUtils() {
		if (utils == null) {
			utils = new UtileriasWebPagos();
		}
		return utils;
	}
	
	/**
	 * Obtener nombre.
	 *
	 * Metodo para obtener el nombre de archivo
	 * en base al tipo de la orden.
	 * 
	 * @param tipoOrden El objeto: tipo orden
	 * @return Objeto string
	 */
	public String obtenerNombre (String tipoOrden){
		String nomWebPag = "";
		switch (getTipoOrden(tipoOrden)) {
		case 0:
			nomWebPag = ORDENES_DV;
			break;
		case 1:
			nomWebPag = TRASPASOS_SS;
			break;
		case 2:
			nomWebPag = ORDENES_CO;
			break;
		case 3:
			nomWebPag = DEVOLUCIONES_CO;
			break;
		case 4:
			nomWebPag = TRASPASOS_ES;
			break;
		case 5:
			nomWebPag = TRASPASOS_EN;
			break;
		case 6:
			nomWebPag = TRASPASOS_PR;
			break;
		case 7:
			nomWebPag = ORDENES_ES;
			break;
		case 8:
			nomWebPag = ORDENES_EN;
			break;
		default:
			nomWebPag = "";
			break;
		}
		return nomWebPag;
	}
	
	/**
	 * Obtener el objeto: tipo orden.
	 *
	 * Metodo para obtener el index
	 * del array de claves para tratar 
	 * el switch.
	 *  
	 * @param tipoOrden El objeto: tipo orden
	 * @return El objeto: tipo orden
	 */
	private int getTipoOrden(String tipoOrden) {
		int index = 0;
		for (int i = 0; i < claves.length; i++) {
			if (claves[i].equalsIgnoreCase(tipoOrden)) {
				index = i;
				break;
			}
		}
		return index;
	}

	/**
	 * Field is empty.
	 *
	 * Metodo para validar que un parametor no sea nulo
	 * o este indefinido. 
	 * 
	 * @param field El objeto: field
	 * @return true, si exitoso
	 */
	public boolean fieldIsEmpty(String field) {
		return field == null || "".equals(field) || "undefined".equals(field);
	}
	
	/**
	 * Valor is empty.
	 *
	 * Metodo para validar que un parametor no sea nulo
	 * o este indefinido.
	 * 
	 * @param valor El objeto: valor
	 * @return true, si exitoso
	 */
	public boolean valorIsEmpty(String valor) {
		return valor == null || "".equals(valor) || "undefined".equals(valor);
	}
	
	/**
	 * Obtener titulo.
	 *
	 * Metodo para obtener el titulo de la 
	 * pantalla a mostrar.
	 * 
	 * @param tipoOrden El objeto: tipo orden
	 * @param modulo El objeto: modulo
	 * @return Objeto string
	 */
	public String obtenerTitulo(String tipoOrden, String modulo) {
		String tituloWebPag = "";
		switch (getTipoOrden(tipoOrden)) {
		case 0:
			tituloWebPag = ORDENES_POR_DEV;
			break;
		case 1:
			tituloWebPag = TRASP_SPID_SIAC;
			break;
		case 2:
			tituloWebPag = ORDENES_ENV_CO;
			break;
		case 3:
			tituloWebPag = DEVOL_ENV_CO;
			break;
		case 4:
			tituloWebPag = TRASP_SPID_SIAC_ES;
			break;
		case 5:
			tituloWebPag = TRASP_SPID_SIAC_EN;
			break;
		case 6:
			tituloWebPag = TRASP_SPID_SIAC_POR_RE;
			break;
		case 7:
			tituloWebPag = ORDENES_ESP;
			break;
		case 8:
			tituloWebPag = LISTADO_PAGOS_ORD_ENV;
			break;
		default:
			tituloWebPag = "";
			break;
		}
		if (modulo.equalsIgnoreCase(ConstantesWebPOACOA.CONS_SPEI)) {
			tituloWebPag = tituloWebPag.replaceAll(ConstantesWebPOACOA.CONS_SPID.toUpperCase(), ConstantesWebPOACOA.CONS_SPEI.toUpperCase());
		}
		return tituloWebPag;
	}

	/**
	 * Obtener el objeto: servlet.
	 *
	 * Metodo para obtener el request 
	 * auxiliar al controller.
	 * 
	 * @return El objeto: servlet
	 */
	public HttpServletRequest getServlet() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
}
