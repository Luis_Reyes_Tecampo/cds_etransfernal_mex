/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: MapeaParametrosPOACOA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 05:00:04 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;

/**
 * Class MapeaParametrosPOACOA.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
public class MapeaParametrosPOACOA implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5938013835984515117L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static MapeaParametrosPOACOA instancia;
	
	/** Constante cero. */
	private static final String CERO = "0";
	
	/** Constante uno. */
	private static final String UNO = "1";
	
	/** Constante DOS. */
	private static final String DOS = "2";
	/** Constante TRES. */
	private static final String TRES = "3";
	
	/**  Constante boton activado desactivado de ACTIVAR POA. */
	private static final String ES_ACT_POA = "esActivoPOA";
	
	/**  Constante boton activado desactivado de ACTIVAR COA. */
	private static final String ES_ACT_COA = "esActivoCOA"; 
	
	/**  Constante boton activado desactivado de DESACTIVAR POA. */
	private static final String ES_DES_POA = "esdesActivacionPOA";
	
	/**  Constante boton activado desactivado de DESACTIVAR COA. */
	private static final String ES_DES_COA = "esdesActivacionCOA";
	
	/**  Constante boton activado desactivado de Envio Recepcion de archivos. */
	private static final String ES_ACT_ENVREC_ARCH = "esActivoEnvRecArch";
	
	/**  Constante boton activado desactivado de final devolucion. */
	private static final String ES_ACT_FINAL_DEV = "esActivoFinalDevo";
							
	/**  Constante boton activado desactivado de liquidacion final. */
	private static final String ES_ACT_LIQ_FINAL = "esActivoLiqFinal";
	
	/**  Constante boton activado desactivado de ACTIVAR COA. */
	private static final String ES_ACT_GEN_PAGOS_DEV_TRANS = "esActGenPagosDevTras";
	
	/**  Constante boton activado desactivado de ACTIVAR COA. */
	private static final String ES_ACT_GEN_DEV = "esActGenDev";
	
	/**  Constante boton activado desactivado de ACTIVAR COA. */
	private static final String ES_ACT_GEN_TRANS = "esActGenTras";
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static MapeaParametrosPOACOA getInstancia() {
		if (instancia == null) {
			instancia = new MapeaParametrosPOACOA();
		}
		return instancia;
	}
	
	/**
	 * Metodo que permite setear los valores para habilitar o deshabilitar los botones.
	 *
	 * @param actPOA boolean Habilita o deshabilita el boton de POA
	 * @param actCOA boolean Habilita o deshabilita el boton de COA
	 * @param desactPOA boolean Habilita o deshabilita el boton de desactivar POA
	 * @param desactCOA boolean Habilita o deshabilita el boton de desactivar COA
	 * @param envRecArch boolean Habilita el envio y Recepcion de de archivos
	 * @param devFinal boolean Habilita la devolucionb final
	 * @param liqFinal boolean Habilita la liquidacion final
	 * @param esActGenPagosDevTras boolean que dispara la generacion de pagos devolucion, transpasos
	 * @param esActGenDev boolean que dispara la generacion de devolucion
	 * @param esActGenTras boolean que dispara la generacion de transpasos
	 * @return Map<String, Object> Mapa con las variables para habilitar o deshabilitar
	 */
	public Map<String, Object> seteaValores(boolean actPOA, boolean actCOA,boolean desactPOA,boolean desactCOA,
			boolean envRecArch, boolean devFinal, boolean liqFinal,boolean esActGenPagosDevTras,
			boolean esActGenDev, boolean esActGenTras){
		final Map<String, Object> model = new HashMap<String, Object>();
		model.put(ES_ACT_POA, actPOA);
		model.put(ES_ACT_COA, actCOA);
		model.put(ES_DES_POA, desactPOA);
		model.put(ES_DES_COA, desactCOA);
		model.put(ES_ACT_ENVREC_ARCH, envRecArch);
		model.put(ES_ACT_FINAL_DEV, devFinal);
		model.put(ES_ACT_LIQ_FINAL, liqFinal);
		model.put(ES_ACT_GEN_PAGOS_DEV_TRANS, esActGenPagosDevTras);
		model.put(ES_ACT_GEN_DEV, esActGenDev);
		model.put(ES_ACT_GEN_TRANS, esActGenTras);
		return model; 
	}
	
	/**
	 * Obtiene mapa de los parametros de las fases de POACOA.
	 *
	 * @param beanResPOACOA Bean con los parametros POACOA
	 * @return  Map<String, Object> mapa con los valores de parametros
	 */
	public Map<String, Object> mapeaFase1(BeanResPOACOA beanResPOACOA) {
		Map<String, Object> model = null;
		if(CERO.equals(beanResPOACOA.getNoHayRetorno())){
			if(CERO.equals(beanResPOACOA.getGenerar())){
				if(UNO.equals(beanResPOACOA.getActivacion()) ){
					model = seteaValores(false,false,true,false,false,true,false,true,true,true);
				}else if(DOS.equals(beanResPOACOA.getActivacion())){
					model = seteaValores(true,false,false,true,false,true,false,true,true,true);
				}
			}else if(!CERO.equals(beanResPOACOA.getGenerar())){
				model = mapeaFases4(model, beanResPOACOA);
			}
		}else{
			model = mapeaFases3(model, beanResPOACOA);
		}

		return model;
	}
	
	/**
	 * Mapea fases 3.
	 *
	 * @param model El objeto: model
	 * @param beanResPOACOA El objeto: bean res POACOA
	 * @return Objeto map
	 */
	private Map<String, Object> mapeaFases3(Map<String, Object> model, BeanResPOACOA beanResPOACOA){
		if(CERO.equals(beanResPOACOA.getGenerar())){
			if(UNO.equals(beanResPOACOA.getActivacion()) ){
				model = seteaValores(false,false,false,false,false,true,false,true,true,true);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,false,false,true,false,true,true,true);
			}
		}else if(!CERO.equals(beanResPOACOA.getGenerar())){
			if(UNO.equals(beanResPOACOA.getActivacion()) ){
				model = seteaValores(false,false,false,false,false,false,false,false,false,false);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,false,false,false,false,false,false,false);
			}
		}
		return model;
	}
	
	/**
	 * Mapea fases 4.
	 *
	 * @param model El objeto: model
	 * @param beanResPOACOA El objeto: bean res POACOA
	 * @return Objeto map
	 */
	private Map<String, Object> mapeaFases4(Map<String, Object> model, BeanResPOACOA beanResPOACOA){
		if(UNO.equals(beanResPOACOA.getActivacion()) ){
			model = seteaValores(false,false,false,false,false,false,false,false,false,false);
		}else if(DOS.equals(beanResPOACOA.getActivacion())){
			model = seteaValores(true,false,false,false,false,false,false,false,false,false);
		}
		return model;
	}
	/**
	 * Obtiene mapa de los parametros de las fases de POACOA.
	 *
	 * @param beanResPOACOA Bean con los parametros POACOA
	 * @return  Map<String, Object> mapa con los valores de parametros
	 */
	public Map<String, Object> mapeaFases(BeanResPOACOA beanResPOACOA) {
		Map<String, Object> model = null;
		if(CERO.equals(beanResPOACOA.getFase())){
			model = mapeaFase0(beanResPOACOA);
		}else if(UNO.equals(beanResPOACOA.getFase())){
			model = mapeaFase1(beanResPOACOA);

		}else if(DOS.equals(beanResPOACOA.getFase())){
			model = mapeaFases2(model, beanResPOACOA);
		}else if(TRES.equals(beanResPOACOA.getFase())){
			model = seteaValores(false,false,false,false,false,false,false,false,false,false);
		}
		return model;
	}
	
	/**
	 * Mapea fases 2.
	 *
	 * @param model El objeto: model
	 * @param beanResPOACOA El objeto: bean res POACOA
	 * @return Objeto map
	 */
	private Map<String, Object> mapeaFases2(Map<String, Object> model, BeanResPOACOA beanResPOACOA){
		if(CERO.equals(beanResPOACOA.getGenerar())){
			if(UNO.equals(beanResPOACOA.getActivacion()) ){
				model = seteaValores(false,false,false,false,false,false,true,false,true,false);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,false,false,false,true,false,true,false);
			}
		}else if(!CERO.equals(beanResPOACOA.getGenerar())){
			if(UNO.equals(beanResPOACOA.getActivacion()) ){
				model = seteaValores(false,false,false,false,false,false,false,false,false,false);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,false,false,false,false,false,false,false);
			}
		}
		return model;
	}
	/**
	 * Obtiene mapa de los parametros de las fases de POACOA.
	 *
	 * @param beanResPOACOA Bean con los parametros POACOA
	 * @return  Map<String, Object> mapa con los valores de parametros
	 */
	public Map<String, Object> mapeaFase0(BeanResPOACOA beanResPOACOA) {
		Map<String, Object> model = null;
		if(CERO.equals(beanResPOACOA.getNoHayRetorno())){
			if(UNO.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(false,false,true,false,true,false,false,false,false,false);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,true,true,false,false,false,false,false);
			}
		}else{
			if(UNO.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(false,false,false,false,true,false,false,false,false,false);
			}else if(DOS.equals(beanResPOACOA.getActivacion())){
				model = seteaValores(true,false,false,false,true,false,false,false,false,false);
			}
		}
		return model;
	}
	
}
