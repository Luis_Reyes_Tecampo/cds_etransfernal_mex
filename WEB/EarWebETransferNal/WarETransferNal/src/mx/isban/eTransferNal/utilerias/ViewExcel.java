/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ViewExcel.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/04/2014 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.utilerias;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

/**
 *Clase del tipo View que se encarga de generar los excels
 *
 **/
public class ViewExcel extends AbstractExcelView {
	
	@SuppressWarnings("unchecked")
	@Override
	/**
	 * Metodo protegido que permite generar el archivo Excel
	 * @param datos Objetos del tipo Map<String, Object>
	 * @param hSSFWorkbook Objeto del tipo HSSFWorkbook
	 * @param httpServletRequest Objeto del tipo HttpServletRequest
	 * @param httpServletResponse Objeto del tipo HttpServletResponse
     */
	protected void buildExcelDocument(Map<String, Object> datos,HSSFWorkbook hSSFWorkbook, 
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		StringBuffer strBuffer =new StringBuffer("attachment;filename=");
		int cellIndex =0;
		
		/**nombre de la hoja**/
		String nombre = (String) datos.get("NAME_SHEET");
		
		/**nombre del libro, archivo excel**/
		HSSFSheet sheet = hSSFWorkbook.createSheet(nombre);
		int rowIndex =0;

		/**genera el encabezado del archivo**/
		generaEncabezado(datos,hSSFWorkbook,sheet,rowIndex++);
		
		/**genera el cuerpo del archivo**/
		generaBody(datos,hSSFWorkbook,sheet, rowIndex);
		List<String> headerValue = Collections.emptyList();
		headerValue = (List<String>)datos.get("HEADER_VALUE");
		
		/**validacion del header**/
		if(headerValue!=null && !headerValue.isEmpty()){
			for( cellIndex=0; cellIndex< headerValue.size();cellIndex++){
				sheet.autoSizeColumn(cellIndex);
			}
		}
		/**uso del metodo httpServeletResponse
		 * para la generacion del archivo excel**/
		httpServletResponse.setContentType("application/vnd.ms-excel");
		String fileName = nombre;
		if(fileName == null || "".equals(fileName.trim())){
			strBuffer.append("eTransferNal.xls");
		}else{
			strBuffer.append(fileName);
			strBuffer.append(".xls");
		}
		/**devuelve el encabezado con el objeto
		 * el cual contiene los registros de la bd
		 * y que seran exportados en el archivo excel**/
		httpServletResponse.setHeader("content-disposition", strBuffer.toString());

		
	}
	
	/**
	 * Metodo privado que permite generar el encabezado del archivo Excel
	 * @param datos Objetos del tipo Map<String, Object>
	 * @param hSSFWorkbook Objeto del tipo HSSFWorkbook
	 * @param sheet Objeto del tipo HSSFSheet
	 * @param rowIndex variable del tipo entero que almacena el registro
     */
	private void generaEncabezado(Map<String, Object> datos,
			HSSFWorkbook hSSFWorkbook,HSSFSheet sheet, int rowIndex){
		String value = null;
		int cellIndex =0;
		HSSFCellStyle cellStyleHeader = null;
		List<String> headerValue = Collections.emptyList();
		FormatCell formatHeader = (FormatCell) datos.get("FORMAT_HEADER");
		headerValue = (List<String>)datos.get("HEADER_VALUE");
		
		/**genera el formato del header
		 * del archivo**/
		if(formatHeader!=null){
			cellStyleHeader = hSSFWorkbook.createCellStyle();
			HSSFFont font= hSSFWorkbook.createFont();
			font.setFontName(formatHeader.getFontName());
			if(formatHeader.getFontSize()>0){
				font.setFontHeightInPoints(formatHeader.getFontSize());
			}
			/**asigna un formato al texto
			 * de las celdas del archivo**/
			if(formatHeader.isBold()){
				font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			}
			/**asigna el ajuste al texto
			 * del archivo a exportar**/
			cellStyleHeader.setFont(font);
			cellStyleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			cellStyleHeader.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			cellStyleHeader.setWrapText(false);
		}
		
		if(headerValue!=null && !headerValue.isEmpty()){
			HSSFRow row = sheet.createRow(rowIndex++);
			for( cellIndex=0; cellIndex< headerValue.size();cellIndex++){
				value = headerValue.get(cellIndex);
				HSSFCell cell = row.createCell(cellIndex);
				cell.setCellValue(value);
				if(cellStyleHeader !=null){
					cell.setCellStyle(cellStyleHeader);		
				}
			}
		}	
	}
	
	/**
	 * Metodo privado que permite generar el encabezado del archivo Excel
	 * @param datos Objetos del tipo Map<String, Object>
	 * @param hSSFWorkbook Objeto del tipo HSSFWorkbook
	 * @param sheet Objeto del tipo HSSFSheet
	 * @param rowIndex variable del tipo entero que almacena el registro
     */
	private void generaBody(Map<String, Object> datos,
			HSSFWorkbook hSSFWorkbook,HSSFSheet sheet, int rowIndex){
		FormatCell formatCell = (FormatCell) datos.get("FORMAT_CELL"); 
		HSSFCellStyle cellStyleBody = null;
		
		List<List<Object>> bodyValue = Collections.emptyList();
		bodyValue = (List<List<Object>>) datos.get("BODY_VALUE");
		if(formatCell!=null){
			cellStyleBody = hSSFWorkbook.createCellStyle();
			HSSFFont font= hSSFWorkbook.createFont();
			font.setFontName(formatCell.getFontName());
			if(formatCell.getFontSize()>0){
				font.setFontHeightInPoints(formatCell.getFontSize());
			}
			/**asigna un formato al texto
			 * de las celdas del archivo**/
			if(formatCell.isBold()){
				font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			}
			/**da el alineado al texto del cuerpo del archivo
			 * el cual contenra los registros**/
			cellStyleBody.setFont(font);
			cellStyleBody.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			cellStyleBody.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			cellStyleBody.setWrapText(false);
			cellStyleBody.setShrinkToFit(false);
		}
		
		/**agrega como contenido del archivo los registros 
		 * existentes en la base de datos
		 * al cuerpo del archivo para su exportacion**/
		if (bodyValue!= null && !bodyValue.isEmpty()){
			recorreBody(bodyValue,cellStyleBody,sheet,rowIndex);
			
		}
	}

	/**
	 * Recorre body.
	 *
	 * @param bodyValue El objeto: body value
	 * @param cellStyleBody El objeto: cell style body
	 * @param sheet El objeto: sheet
	 * @param rowIndex El objeto: row index
	 */
	private void recorreBody(List<List<Object>> bodyValue, HSSFCellStyle cellStyleBody,HSSFSheet sheet, int rowIndex) {
		for(List<Object> listObject:bodyValue){
			HSSFRow row = sheet.createRow(rowIndex++);
			int cellIndex =0;
			for(Object objProp : listObject){
				if(objProp instanceof Short){
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
				}else if(objProp instanceof Integer){
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
					cellStyleBody.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
					cellStyleBody.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
					cellStyleBody.setWrapText(false);
					cellStyleBody.setShrinkToFit(false);
					cell.setCellStyle(cellStyleBody);
				}else if(objProp instanceof BigInteger){
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
				}else if(objProp instanceof Number){
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
				}else if(objProp instanceof Date){
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
				}else{
					HSSFCell cell = row.createCell(cellIndex++);
					cell.setCellValue(objProp.toString());
					
				}	
			}
			
			/**
			HSSFCell cell = row.createCell(cellIndex++);
			cell.setCellStyle(cellStyleDate);
			cell.setCellValue("14-04-2014");
			
			cell = row.createCell(cellIndex++);
			cell.setCellStyle(cellStyleDecNumeric);
			cell.setCellValue(10988999.00);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			**/

		}
	}
	
	
	
}

