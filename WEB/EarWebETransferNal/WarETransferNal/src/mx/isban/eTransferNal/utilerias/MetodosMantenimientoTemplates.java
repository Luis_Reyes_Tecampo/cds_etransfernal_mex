package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.capturasManuales.BeanBaseReqResTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCampoLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;

import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * The Class MetodosAutorizarOperaciones.
 * 
 */
public class MetodosMantenimientoTemplates extends Architech{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -2110102936467055807L;

	/**
	 * Metodo que simplifica el anexo de errores al modelo
	 * @param modelAndView que respondera a la vista
	 * @param codError codigo de error del componente
	 * @param tipoError del componente
	 * @param descripcion del problema
	 */
	public void asignarErroresProceso(ModelAndView modelAndView, String codError, String tipoError, String descripcion){
		modelAndView.addObject(Constantes.COD_ERROR, codError);
		modelAndView.addObject(Constantes.TIPO_ERROR, tipoError);
		modelAndView.addObject(Constantes.DESC_ERROR, descripcion);
	}
	
	/**
	 * Request to BeanReqConsultaTemplates.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req cons operaciones
	 */
	public BeanReqConsultaTemplates requestToBeanReqConsultaTemplates(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqConsultaTemplates beanReq = new BeanReqConsultaTemplates();
		List<BeanTemplate> listaTemplates = new ArrayList<BeanTemplate>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistaTemplates"), listaTemplates, new BeanTemplate());
		beanReq.setListaTemplates(listaTemplates);
		
		requestToBeanBaseReqResTemplate(beanReq, req);
		
		return beanReq;
	}
	
	/**
	 * Request to bean req alta edit template.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req consulta templates
	 */
	public BeanReqAltaEditTemplate requestToBeanReqAltaEditTemplate(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqAltaEditTemplate beanReq = new BeanReqAltaEditTemplate();
		
		requestToBeanBaseReqResTemplate(beanReq, req);
		
		return beanReq;
	}
	
	/**
	 * Request to BeanReqConsultaTemplates.
	 *
	 * @param beanReq the bean req
	 * @param req the req
	 * @return the bean req cons operaciones
	 */
	private BeanBaseReqResTemplate requestToBeanBaseReqResTemplate(BeanBaseReqResTemplate beanReq, HttpServletRequest req){
		//Se setea la listaCamposMaster
		List<BeanCampoLayout> listaCamposMaster = new ArrayList<BeanCampoLayout>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistaCamposMaster"), listaCamposMaster, new BeanCampoLayout());
		beanReq.setListaCamposMaster(listaCamposMaster);
		//Se setea la listaLayouts
		List<String> listaLayouts = new ArrayList<String>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistaLayouts"), listaLayouts, "");
		beanReq.setListaLayouts(listaLayouts);
		//Se setea la listaUsuarios
		List<String> listaUsuarios = new ArrayList<String>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistaUsuarios"), listaUsuarios, "");
		beanReq.setListaUsuarios(listaUsuarios);
		//Se setea la listaUsuariosAutorizan
		List<BeanUsuarioAutorizaTemplate> listaUsuariosAutorizan = new ArrayList<BeanUsuarioAutorizaTemplate>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistaUsuariosAutorizan"), listaUsuariosAutorizan, new BeanUsuarioAutorizaTemplate());
		if(beanReq.getTemplate() != null){
			beanReq.getTemplate().setUsuariosAutorizan(listaUsuariosAutorizan);
		}
		UtilsMapeaRequest.getMapper().mapearObject(beanReq, req.getParameterMap());
		return beanReq;
	}
}