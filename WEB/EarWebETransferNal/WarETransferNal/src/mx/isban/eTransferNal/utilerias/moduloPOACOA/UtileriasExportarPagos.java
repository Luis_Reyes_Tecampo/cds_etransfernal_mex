/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExportarPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/08/2019 12:20:21 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * Class UtileriasExportarPagos.
 *
 * Clase de utilerias utilizada por la pantalla de pagos que contiene metodos
 * necesarios para realizar el fujo de exportacion.
 * 
 * @author FSW-Vector
 * @since 9/08/2019
 */
public final class UtileriasExportarPagos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8903804012892729492L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasExportarPagos utils;

	/**
	 * Obtener el objeto: utils.
	 *
	 * Metodo para obtener el header usado en el 
	 * archivo de excel cuando se realiza el flujo
	 * de exportar.
	 * 
	 * Obtener la instancia de la clase 
	 * de utilerias.
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasExportarPagos getUtils() {
		if (utils == null) {
			utils = new UtileriasExportarPagos();
		}
		return utils;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("moduloSPID.listadoPagos.text.refe"),
				ctx.getMessage("moduloSPID.listadoPagos.text.cola"),
				ctx.getMessage("moduloSPID.listadoPagos.text.claveTran"),
				ctx.getMessage("moduloSPID.listadoPagos.text.macan"),
				ctx.getMessage("moduloSPID.listadoPagos.text.medioEnt"),
				ctx.getMessage("moduloSPID.listadoPagos.text.fchCap"),
				ctx.getMessage("moduloSPID.listadoPagos.text.ord"), 
				ctx.getMessage("moduloSPID.listadoPagos.text.rec"),
				ctx.getMessage("moduloSPID.listadoPagos.text.importeAbono"),
				ctx.getMessage("moduloSPID.listadoPagos.text.est") };
		return Arrays.asList(header);
	}

	/**
	 * Obtener el objeto: body.
	 *
	 *  Metodo para obtener el cuerpo del
	 * archivo excel cuando se realiza el flujo de
	 * exportar.
	 * 
	 * @param listaBeanPago El objeto: lista bean pago
	 * @return El objeto: body
	 */
	public List<List<Object>> getBody(List<BeanPago> listaBeanPago) {
		List<Object> objListParamExpPagos = null;
		List<List<Object>> objListExpPagos = null;
		if (listaBeanPago != null && !listaBeanPago.isEmpty()) {
			objListExpPagos = new ArrayList<List<Object>>();

			for (BeanPago beanPago : listaBeanPago) {
				objListParamExpPagos = new ArrayList<Object>();
				objListParamExpPagos.add(beanPago.getReferencia());
				objListParamExpPagos.add(beanPago.getCveCola());
				objListParamExpPagos.add(beanPago.getCveTran());
				objListParamExpPagos.add(beanPago.getCveMecanismo());
				objListParamExpPagos.add(beanPago.getCveMedioEnt());
				objListParamExpPagos.add(beanPago.getFechaCaptura());
				objListParamExpPagos.add(beanPago.getCveIntermeOrd());
				objListParamExpPagos.add(beanPago.getCveIntermeRec());
				objListParamExpPagos.add(beanPago.getImporteAbono());
				objListParamExpPagos.add(beanPago.getEstatus());
				objListExpPagos.add(objListParamExpPagos);
			}
		}
		return objListExpPagos;
	}
	
	/**
	 * Obtener el objeto: model.
	 *
	 * Obtener un model and view
	 * 
	 * @return El objeto: model
	 */
	public ModelAndView getModel() {
		return new ModelAndView(new ViewExcel());
	}
	
	/**
	 * Obtener el objeto: request.
	 *
	 * Obtener el cotexto actual
	 * 
	 * @param req El objeto: req
	 * @return El objeto: request
	 */
	public RequestContext getRequest(HttpServletRequest req) {
		return new RequestContext(req);
	}
}