package mx.isban.eTransferNal.utilerias;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanDatosGenerales;
import mx.isban.eTransferNal.beans.capturasManuales.BeanDatosIp;
import mx.isban.eTransferNal.beans.capturasManuales.BeanLayout;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;


/**
 * The Class UtilsCapturasManuales.
 */
public final class UtilsCapturaCentral {
	
	/**  Logeo de la clase. */
	private static final Logger LOG = Logger.getLogger(UtilsMapeaRequest.class.getName());
	
	/** La constante ERROR. */
	private static final String ERROR = "error";
	
	/** The Constant INT_CERO. */
	private static final int INT_CERO = 0;
	
	/** La constante ERRORES_PUNTO. */
	private static final String ERRORES_PUNTO = "moduloCapturasManuales.capturaCentral.error.";
	
	/** La constante BEAN. */
	private static final String BEAN_OPER = "bean";
	
	/** La constante BEAN. */
	private static final String LISTA = "lista";
	
	/**
	 * Agregar datos default.
	 *
	 * @param beanRes the bean res
	 * @param usuario the usuario
	 * @param ip the ip
	 */
	public void agregarDatosDefault(BeanCapturaCentralResponse beanRes,String usuario,String ip){
		if (beanRes.getLayout() == null){
			return;
		}
		Map <String,String> datosDefault = new HashMap<String,String>();
		if(usuario != null){
			datosDefault.put("cveusuariosol", usuario);
			datosDefault.put("cveusuariocap", usuario);
			datosDefault.put("direccionip", ip);
		}
		for(BeanLayout layout : beanRes.getLayout()){
			if(datosDefault.containsKey(layout.getCampo())){
				layout.setConstante(datosDefault.get(layout.getCampo()));
			}
		}
	}
	
	/**
	 * Actualiza valor.
	 *
	 * @param beanRes El objeto: bean res
	 * @param name El objeto: name
	 * @param nuevoValor El objeto: nuevo valor
	 * @return Objeto bean captura central response
	 */
	public BeanCapturaCentralResponse actualizaValor(BeanCapturaCentralResponse beanRes, 
			String name, String nuevoValor){
		//Obtiene el layout
		List<BeanLayout> layout = beanRes.getLayout();
		//Recorre y actualiza valor 
		for (BeanLayout beanLayout : layout) {
			if (beanLayout.getCampo().equals(name)) {
				beanLayout.setValorActual(nuevoValor);
			}
		}
		return beanRes;
	}
	
	/**
	 * Obtener el objeto: obligatorios.
	 *
	 * @param beanResOper El objeto: bean res oper
	 * @return El objeto: obligatorios
	 */
	public List<String> getObligatorios(BeanCapturaCentralResponse beanResOper) {
		//Lisat de obligatoriso
		List<String> obligatorios = new ArrayList<String>();
		//Recorre el layout y agrega los objetos obligatorios a la lista 
		for (BeanLayout campo : beanResOper.getLayout()) {
			if (campo.getObligatorio() == 1) {
				String nombre = campo.getCampo();
				obligatorios.add(nombre);
			}
		}
		//Retorna la lista con los objetos obligatorios
		return obligatorios;
	}
	

	/**
	 * Asigna valor.
	 *
	 * @param method the method
	 * @param parametro the parametro
	 * @param operacion the operacion
	 * @param objetos the objetos
	 * @param name the name
	 */
	public void asignaValor(Method method, String parametro, Object operacion, Map<String, Object> objetos, String name){
		try {
			//Obtiene el tipo de dato del parametro
			Class<?>[] arr = method.getParameterTypes();
			if(arr[INT_CERO].isAssignableFrom(Long.class) && !parametro.trim().isEmpty()){
				//Si es entero se convierte
				method.invoke(operacion, Long.parseLong(parametro.trim()));
			}else if(arr[INT_CERO].isAssignableFrom(String.class)){
				//Si el parametro es de tipo cadena se asigna el valor obtenido del request
				method.invoke(operacion,  parametro);
			}else if(arr[INT_CERO].isAssignableFrom(BigDecimal.class) && !parametro.trim().isEmpty()){
				//Si el parametro es de tipo double
				method.invoke(operacion,  new BigDecimal(parametro.trim().replaceAll(",", "")));
			}
			//Cacha las excepciones
		} catch (IllegalArgumentException e1) {
			LOG.error(e1.getMessage(),e1);
			objetos.put(ERROR, name);
		} catch (IllegalAccessException e2) {
			LOG.error(e2.getMessage(),e2);
		} catch (InvocationTargetException e3) {
			LOG.error(e3.getMessage(),e3);
		}
	}
	
	/**
	 * Agregar error.
	 *
	 * @param modelAndView the model and view
	 * @param codigo the codigo
	 * @param tipo the tipo
	 * @param ctx the ctx
	 */
	public void agregarError(ModelAndView modelAndView, String codigo,String tipo, RequestContext ctx){
		String mensaje = "";
		if (Errores.EC00011B.equals(codigo)) {
			mensaje = ctx.getMessage(Constantes.COD_ERROR_PUNTO + codigo);
		} else {
			mensaje = ctx.getMessage(ERRORES_PUNTO + codigo);
		}
		modelAndView.addObject(Constantes.COD_ERROR, codigo);
		modelAndView.addObject(Constantes.TIPO_ERROR, tipo);
		modelAndView.addObject(Constantes.DESC_ERROR, mensaje);
	}
	
	/**
	 * Mapea bean operacion.
	 *
	 * @param req the req
	 * @param obligatorios El objeto: obligatorios
	 * @param beanRes El objeto: bean res
	 * @return Objeto bean operacion
	 */
	public Map<String, Object> mapeaBeanOperacion(HttpServletRequest req, List<String> obligatorios, 
			BeanCapturaCentralResponse beanRes) {
		//Nuevo objeto Map para almacenar ambos objetos de respuesta
		Map<String, Object> objetos = new HashMap<String, Object>();
		//Nuevo objeto Opreacion
		BeanOperacion operacion = new BeanOperacion();
		BeanDatosIp beanDatosIp = new BeanDatosIp();
		BeanDatosGenerales beanDatosGenerales = new BeanDatosGenerales();
		
		//Invoca al metodo que recorre los metodos del bean y obtiene la info de la pantalla
		mapeaBean(req, objetos, obligatorios, beanRes, operacion);
		mapeaBean(req, objetos, obligatorios, beanRes, beanDatosGenerales);
		mapeaBean(req, objetos, obligatorios, beanRes, beanDatosIp);
		
		/** Se asignan bean */
		operacion.setBeanDatosGenerales(beanDatosGenerales);
		operacion.setBeanDatosIp(beanDatosIp);
		
		
		//Agrega los objetos de respuesta al map
		objetos.put(BEAN_OPER, operacion);
		objetos.put(LISTA, obligatorios);
		//Regresa el objeto de respuesta
		return objetos;
	}
	
	/**
	 * Metodo enacargado de llenar los bean con la informacion capturada en la pantalla
	 * @since 08/10/2018
	 * @author FSW-Vector
	 * @param req
	 * @param objetos
	 * @param obligatorios
	 * @param beanRes
	 * @param bean
	 */
	public void  mapeaBean(HttpServletRequest req, Map<String, Object> objetos, List<String> obligatorios, 
			BeanCapturaCentralResponse beanRes, Object bean) {
		
		for (Method method : bean.getClass().getMethods()) {
			if (method.getName().startsWith("set")) {
				String name =  method.getName().substring(3);
				name = name.replace(name.charAt(0), name.toLowerCase().charAt(0));
				
				String parametro = req.getParameter(name);
					//Setea el valor obtenido
				asignaValor(method, parametro, bean, objetos, name);
				
				if (null != parametro && !StringUtils.EMPTY.equals(parametro)) {
					obligatorios.remove(name.toLowerCase());
					actualizaValor(beanRes, name.toLowerCase(), parametro);
				}
			}
		}
	}
	
	
	/**
	 * Anadir datos de entrada.
	 *
	 * @param request the request
	 * @param response the response
	 */
	public void anadirDatosDeEntrada(BeanCapturaCentralRequest request, BeanCapturaCentralResponse response){
		response.setListLayouts(request.getListLayouts());
		response.setListTemplates(request.getListTemplates());
		response.setTemplate(request.getTemplate());
	}
}
