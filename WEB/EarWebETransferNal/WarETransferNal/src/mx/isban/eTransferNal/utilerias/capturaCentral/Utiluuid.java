/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: Utiluuid.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/10/2018 07:10:08 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.capturaCentral;

import java.util.Map;
import java.util.UUID;



/**
 * Class Utiluuid.
 *
 * @author FSW-Vector
 * @since 31/10/2018
 */
public class Utiluuid {
	
	/** Constante con los campos UniqueEndToEndTransactionReference. */
	private static final String DESC_CAMPO_UETR = "UETR";
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static Utiluuid instancia;
	
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * @return El objeto: instancia
	 */
	public static Utiluuid getInstancia() {
		if(instancia == null) {
			instancia = new Utiluuid();
		}
		return instancia;
	}
	
	/**
	 * Metodo privado para setear en la operacion .
	 *
	 * @param operacion Map<String, String> mapa con la operacion
	 */
	public void seteaUUID(Map<String, String> operacion){
				UUID uuid = UUID.randomUUID();
				operacion.put(DESC_CAMPO_UETR, uuid.toString());

	}
			
}
