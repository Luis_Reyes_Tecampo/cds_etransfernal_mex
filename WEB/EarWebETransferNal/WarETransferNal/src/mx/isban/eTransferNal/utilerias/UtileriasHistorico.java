package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.support.RequestContext;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

/**
 * UtileriasHistorico
 * 
 * Creada para obtener los datos y registrois que recibe
 * la tabla principal de la ventana Cat. Intermediarios - Historico
 * para generar asi el metodo exportar el cual genera un archivo excel con
 * los datos obtenidos.
 * 
 * @version 1.0
 * @Class UtileriasPayments
 * Utilerias para payments
 */

/**
 * @author SNGEnterprise
 * Fecha 29/30/2020
 * Hora 00:00:00
 */
public class UtileriasHistorico extends Architech{

	/** La constante serialVersionUID. 
	 * El proceso de serializaci�n asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calcular� un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 1L;
	
	/** Referencia del Objeto instancia. */
	protected static UtileriasHistorico instancia;
	
	/** Referencia del Objeto Utilerias. */
	private static final UtileriasHistorico UTILERIAS = new UtileriasHistorico();
		
	/**
	 * Se crea el constructor.
	 * 
	 * Constructor de la clase UtilCausasDevolucion
	 */
	public UtileriasHistorico() {
		// Constructor de la clase UtilCausasDevolucion
	}

	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasHistorico getUtilerias() {
		return UTILERIAS;
	}

	/**
	 * Obtener el objeto: header excel.
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx){
		String[] header = new String[]{
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.cveIntermeH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.fchModifH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoModifH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.usuarioModifH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoIntermeORIH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoIntermeNVOH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numCecobanORIH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numCecobanNVOH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreCortoORIH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreCortoNVOH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreLargoORIH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreLargoNVOH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numBanxicoORIH"),
				ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numBanxicoNVOH")
		};
		return Arrays.asList(header);
	}
	
	/**
	 * Metodo privado que sirve para obtener los datos para el excel.
	 * @param consulHist lista de canales de id �nico
	 * @return List<List<Object>> Objeto lista de lista de objetos con los datos del
	 * excel
	 */	     
	public List<List<Object>> getBody(List<BeanIntFinancieroHistorico> consulHist){
		List<Object> objectListParam = null;
		List<List<Object>> objList = null;
		
		if(consulHist != null && !consulHist.isEmpty()){
			objList = new ArrayList<List<Object>>();
			for(BeanIntFinancieroHistorico cod : consulHist ){
				objectListParam = new ArrayList<Object>();
				
				objectListParam.add(cod.getCveInterme());
				objectListParam.add(cod.getFchModif());
				objectListParam.add(cod.getTipoModif());
				objectListParam.add(cod.getUsuario());
				objectListParam.add(cod.getTipoIntermeOri());
				objectListParam.add(cod.getTipoIntermeNvo());
				objectListParam.add(cod.getNumCecobanOri());
				objectListParam.add(cod.getNumCecobanNvo());
				objectListParam.add(cod.getNombreCortoOri());
				objectListParam.add(cod.getNombreCortoNvo());
				objectListParam.add(cod.getNombreLargoOri());
				objectListParam.add(cod.getNombreLargoNvo());
				objectListParam.add(cod.getNumBanxicoOri());
				objectListParam.add(cod.getNumBanxicoNvo());
				objList.add(objectListParam);
			}
		}
		return objList;
	}
	
	/**
	 * Obtener el objeto: header excel.
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(ResourceBundleMessageSource ctx){
		String[] header = new String[]{
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.cveIntermeH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.fchModifH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoModifH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.usuarioModifH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoIntermeORIH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.tipoIntermeNVOH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numCecobanORIH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numCecobanNVOH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreCortoORIH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreCortoNVOH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreLargoORIH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.nombreLargoNVOH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numBanxicoORIH", null, LocaleContextHolder.getLocale()),
			ctx.getMessage("catalogos.consultaCatIntFinancieros.text.numBanxicoNVOH", null, LocaleContextHolder.getLocale())
		};
		return Arrays.asList(header);
	}	
	
	/**
	 * Obtener el objeto: error.
	 * @param responseDAO respuesta de BD
	 * @return El objeto: error
	 */
	public BeanResBase getError(ResponseMessageDataBaseDTO responseDAO){
		BeanResBase error = new BeanResBase();
		if(responseDAO == null){
			error.setCodError(Errores.EC00011B);
			return error;
		}
		error.setCodError(responseDAO.getCodeError());
		error.setMsgError(responseDAO.getMessageError());
		return error;
	}
}
