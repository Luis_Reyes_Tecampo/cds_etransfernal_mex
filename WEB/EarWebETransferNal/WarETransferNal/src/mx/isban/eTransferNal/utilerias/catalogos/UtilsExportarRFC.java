/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarRFC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 02:26:10 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;

/**
 * Class UtilsExportarRFC.
 *
 * Clase que contiene metodos utilizados en el flujo
 * de exportacion del catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public final class UtilsExportarRFC extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6744306670521030626L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarRFC instancia;
	
	/**
	 * Método Get para obtiener el objeto instancia
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @return instancia: obtiene los properties para crear el header
	 */
	public static UtilsExportarRFC getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarRFC();
		}
		return instancia;
	}
	
	/**
	 * Obtiene el header
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel para el flujo de exportar.
	 *  
	 * @param ctx --> obtiene el header con los properties
	 * @return header --> regresa el listado con el header
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.rfc.lbl.rfc"),
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtiene el body
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 * 
	 * @param rfcs --> obtiene los rfc para insertar en el archivo
	 * @return body --> regresa el ojeto a insertar en el archivo
	 */
	public List<List<Object>> getBody(List<BeanRFC> rfcs) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (rfcs != null && !rfcs.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanRFC rfc : rfcs) {
				objListParam = new ArrayList<Object>();
				objListParam.add(rfc.getRfc());
				objList.add(objListParam);
			}
		}
		return objList;
	}
}
