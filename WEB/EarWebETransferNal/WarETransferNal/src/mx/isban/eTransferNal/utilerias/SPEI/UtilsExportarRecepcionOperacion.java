/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarRecepcionOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:49:12 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.utilerias.SPEI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;
import mx.isban.eTransferNal.utilerias.FormatCell;
import mx.isban.eTransferNal.utilerias.ViewExcel;

/**
 * clase de tipo utilerias y tiene las variables comunes de las pantallas de
 * recepcion de operaciones historicas y del dia, muesta los titulos de las
 * pantallas para saber si vienen del modulo de SPEI o Admon Sdo.
 *
 * @author FSW-Vector
 * @since 14/09/2018
 */
public class UtilsExportarRecepcionOperacion {

	/** La variable que contiene informacion con respecto a: mensaje. */
	private String mensaje = "";

	/** La variable que contiene informacion con respecto a: model. */
	private ModelAndView modeloVista;

	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtilsExportarRecepcionOperacion utilExportOperacion = new UtilsExportarRecepcionOperacion();

	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtilsExportarRecepcionOperacion getInstance() {
		return utilExportOperacion;
	}

	/**
	 * Procedimiento para retornar los valores del encabezado en el archivo.
	 *
	 * @param ctx the ctx
	 * @return the header excel
	 */
	private static List<String> getHeaderExcel(RequestContext ctx) {
		/**
		 * Se crea un array donde se incluyen como mensaje la referencia de los
		 * properties
		 */
		String[] header = new String[] {
				/** Asignamos los valores al array */
				/** En el mensaje ponemos las properties **/
				ctx.getMessage("moduloCDA.recepcionOperacion.text.txtRefTrans"),
				ctx.getMessage("general.RepOperacion.txtInstOrd"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.txtIntitu"),
				ctx.getMessage("general.RepOperacion.txtImporte"),
				ctx.getMessage("general.RepOperacion.txtEstatusTranfer"),
				ctx.getMessage("general.RepOperacion.txtFecha"), ctx.getMessage("general.RepOperacion.txtHora"),
				ctx.getMessage("general.RepOperacion.txtCuentaRec"),
				ctx.getMessage("general.RepOperacion.txtDevolucion"),
				ctx.getMessage("general.RepOperacion.txtCodError"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.txtDesError"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.txtCveRastreo"),
				ctx.getMessage("moduloSPEI.recepOperacion.text.apartado"),
				ctx.getMessage("general.RepOperacion.txtFoliosPaq"), ctx.getMessage("general.RepOperacion.txtFolioPag"),
				ctx.getMessage("general.RepOperacion.txtTipo"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.descTipoPago"),
				ctx.getMessage("general.RepOperacion.txtEstatusBanxico"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.estatusOpe"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.cveTransfer"),
				ctx.getMessage("moduloCDA.recepcionOperacion.text.desCveTransf") };
		/** Retornamos el encabezado */
		return Arrays.asList(header);
	}

	/**
	 * Procedimiento para retornar los valores de la consulta que se agregan al
	 * archivo.
	 *
	 * @param listaBean the lista bean
	 * @return the body
	 */
	private static List<List<Object>> getBody(List<BeanTranSpeiRecOper> listaBean) {
		/** Se crea la lista que almacenara cada registro **/
		List<Object> objListParam = null;
		/** Se crea una lista para almacenar todos los registros **/
		List<List<Object>> objList = null;
		/** Se valida que el bean no veanga nulo o este vacio */
		if (listaBean != null && !listaBean.isEmpty()) {
			/** Se inicializa la lista */
			objList = new ArrayList<List<Object>>();
			/** Se comienza a asignar los valores **/
			for (BeanTranSpeiRecOper bean : listaBean) {
				/** Inicializamos el objecto que guardara el registro actual */
				objListParam = new ArrayList<Object>();
				/** Asigamos los valores al bean */
				objListParam.add(bean.getBeanDetalle().getDetEstatus().getRefeTransfer());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getCveInstOrd());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getCveMiInstitucion());
				objListParam.add(bean.getBeanDetalle().getOrdenante().getMonto());
				objListParam.add(bean.getBeanDetalle().getDetEstatus().getEstatusTransfer());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getFchOperacion());
				objListParam.add(bean.getBeanDetalle().getBeanCambios().getFchCaptura());
				objListParam.add(bean.getNumCuentaTran());
				objListParam.add(bean.getBeanDetalle().getBeanCambios().getMotivoDevol());
				objListParam.add(bean.getBeanDetalle().getCodigoError());
				objListParam.add(bean.getBeanDetalle().getBeanCambios().getOtros().getDesError());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getCveRastreo());
				objListParam.add(bean.getUsuario());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getFolioPaquete());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getFolioPago());
				objListParam.add(bean.getBeanDetalle().getDetalleTopo().getTipoPago());
				objListParam.add(bean.getBeanDetalle().getBeanCambios().getOtros().getDescTipoPgo());
				objListParam.add(bean.getBeanDetalle().getDetEstatus().getEstatusBanxico());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getEstatusOpe());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getCveTransfer());
				objListParam.add(bean.getBeanDetalle().getDetalleGral().getDesCveTransf());
				/** Agregamos el objecto a las lista **/
				objList.add(objListParam);
			}
		}
		/** Retornamos el objecto de listas */
		return objList;
	}

	/**
	 * Exportar recepcion operacion.
	 *
	 * @param recepOperaAdmonSaldos El objeto: recep opera admon saldos
	 * @param ctx                   El objeto: ctx
	 * @param beanReqConsTranSpei   El objeto: bean req cons tran spei
	 * @return Objeto model and view
	 */
	public ModelAndView exportarRecepcionOperacion(BeanResTranSpeiRec recepOperaAdmonSaldos, RequestContext ctx,
			BeanReqTranSpeiRec beanReqConsTranSpei) {
		/** inicio de variables **/
		FormatCell formatCellHeader = null;
		FormatCell formatCellBody = null;
		modeloVista = null;
		String nombreReporte = "";
		/** funcion para mostrar la pantalla segun de donde venga la peticion **/
		if (recepOperaAdmonSaldos.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_DIA)) {
			nombreReporte = "RecepcionOperacionesCtaAlterna";
		} else if (recepOperaAdmonSaldos.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_HTO)) {
			nombreReporte = "RecepcionOperacionesHtoCtaAlterna";
		} else if (recepOperaAdmonSaldos.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_DIA)) {
			nombreReporte = "RecepcionOperacionesCtaPrincipal";
		} else if (recepOperaAdmonSaldos.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO)) {
			nombreReporte = "RecepcionOperacionesHtoCtaPrincipal";
		}

		/** funcion que forma el excel a exportar **/
		formatCellBody = new FormatCell();
		formatCellBody.setFontName("Calibri");
		formatCellBody.setFontSize((short) 11);
		formatCellBody.setBold(false);

		formatCellHeader = new FormatCell();
		formatCellHeader.setFontName("Calibri");
		formatCellHeader.setFontSize((short) 11);
		formatCellHeader.setBold(true);
		/** se inicializa la vista del form **/
		modeloVista = new ModelAndView(new ViewExcel());
		modeloVista.addObject("FORMAT_HEADER", formatCellHeader);
		modeloVista.addObject("FORMAT_CELL", formatCellBody);
		modeloVista.addObject("HEADER_VALUE", getHeaderExcel(ctx));
		modeloVista.addObject("BODY_VALUE", getBody(recepOperaAdmonSaldos.getListBeanTranSpeiRec()));
		modeloVista.addObject("NAME_SHEET", nombreReporte);

		return modeloVista;
	}

	/**
	 * funcion para mostrar el error.
	 *
	 * @param historico             El objeto: historico
	 * @param recepOperaAdmonSaldos El objeto: recep opera admon saldos
	 * @param beanReqConsTranSpei   El objeto: bean req cons tran spei
	 * @param ctx                   El objeto: ctx
	 * @return Objeto model and view
	 */
	public ModelAndView errorVista(boolean historico, BeanResTranSpeiRec recepOperaAdmonSaldos,
			BeanReqTranSpeiRec beanReqConsTranSpei, RequestContext ctx) {
		modeloVista = vistaMostrar(beanReqConsTranSpei, recepOperaAdmonSaldos, historico);
		mensaje = ctx.getMessage("codError." + recepOperaAdmonSaldos.getBeanError().getCodError());
		modeloVista.addObject(Constantes.COD_ERROR, Errores.EC00011B);
		modeloVista.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
		modeloVista.addObject(Constantes.DESC_ERROR, mensaje);

		return modeloVista;
	}

	/**
	 * funcion para mostrar el error.
	 *
	 * @param beanReqConsTranSpei   El objeto: bean req cons tran spei
	 * @param recepOperaAdmonSaldos El objeto: recep opera admon saldos
	 * @param historico             El objeto: historico
	 * @return Objeto model and view
	 */
	public ModelAndView vistaMostrar(BeanReqTranSpeiRec beanReqConsTranSpei, BeanResTranSpeiRec recepOperaAdmonSaldos,
			boolean historico) {
		recepOperaAdmonSaldos.getBeanComun().setNombrePantalla(beanReqConsTranSpei.getBeanComun().getNombrePantalla());
		recepOperaAdmonSaldos.getBeanComun().setOpcion(beanReqConsTranSpei.getBeanComun().getOpcion());
		/**
		 * pregunta de que pantalla viene la peticion para hacer el return de la vista
		 **/
		if (historico) {
			/** pantalla historica normal **/
			recepOperaAdmonSaldos.getBeanComun().setFechaActual(beanReqConsTranSpei.getBeanComun().getFechaActual());
			recepOperaAdmonSaldos.getBeanComun()
					.setFechaOperacion(beanReqConsTranSpei.getBeanComun().getFechaOperacion());
			if (beanReqConsTranSpei.getBeanComun().getNombrePantalla()
					.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_HTO)) {
				/** pantalla historica normal **/
				modeloVista = new ModelAndView(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_HTO,
						ConstantesRecepOpePantalla.BEAN_RES, recepOperaAdmonSaldos);
			}
			if (beanReqConsTranSpei.getBeanComun().getNombrePantalla()
					.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO)) {
				/** pantalla historica con operaciones apartadas **/
				modeloVista = new ModelAndView(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO,
						ConstantesRecepOpePantalla.BEAN_RES, recepOperaAdmonSaldos);
			}
		} else {
			recepOperaAdmonSaldos.getBeanComun().setFechaActual(beanReqConsTranSpei.getBeanComun().getFechaActual());
			recepOperaAdmonSaldos.getBeanComun()
					.setFechaOperacion(beanReqConsTranSpei.getBeanComun().getFechaOperacion());

			if (beanReqConsTranSpei.getBeanComun().getNombrePantalla()
					.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_DIA)) {
				/** pantalla de recepcion de operaciones del dia **/
				modeloVista = new ModelAndView(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_DIA,
						ConstantesRecepOpePantalla.BEAN_RES, recepOperaAdmonSaldos);
			}
			if (beanReqConsTranSpei.getBeanComun().getNombrePantalla()
					.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_DIA)) {
				/** pantalla de recepcion de operaciones del dia **/
				modeloVista = new ModelAndView(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_DIA,
						ConstantesRecepOpePantalla.BEAN_RES, recepOperaAdmonSaldos);
			}
		}
		return modeloVista;
	}

	/**
	 * Obtener historico.
	 *
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @return true, si exitoso
	 */
	public boolean obtenerHistorico(BeanReqTranSpeiRec beanReqConsTranSpei) {
		boolean historico = false;
		/**
		 * pregunta si la pantalla corresponde a las historicas del menu de admon saldos
		 * pone historico en verdadero
		 **/
		if (beanReqConsTranSpei.getBeanComun().getNombrePantalla()
				.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SDO_HTO)
				|| beanReqConsTranSpei.getBeanComun().getNombrePantalla()
						.equals(ConstantesRecepOpePantalla.RECEPCION_OPERACION_SPEI_HTO)) {
			historico = true;
		}
		return historico;
	}

	/**
	 * Validar lista.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return Objeto bean res cons tran spei rec
	 */
	public BeanResTranSpeiRec validarLista(BeanReqTranSpeiRec beanReqTranSpeiRec) {
		BeanResTranSpeiRec beanResConsTranSpeiRec = new BeanResTranSpeiRec();
		// inicializa bean de error
		beanResConsTranSpeiRec.setBeanError(new BeanResBase());
		List<BeanTranSpeiRecOper> listBeanTranSpeiRecTemp = new ArrayList<BeanTranSpeiRecOper>();
		for (BeanTranSpeiRecOper lista : beanReqTranSpeiRec.getListBeanTranSpeiRec()) {
			if (lista.isSeleccionado()) {
				listBeanTranSpeiRecTemp.add(lista);
			}
		}
		beanResConsTranSpeiRec.setListBeanTranSpeiRec(listBeanTranSpeiRecTemp);
		/**
		 * Condicion para ver si la lista de datos seleccionados contiene informacion
		 **/
		if (listBeanTranSpeiRecTemp.isEmpty()) {
			/** En caso de no tener un registro selecciona manda el mensaje **/
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00068V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRec;
		} else if (listBeanTranSpeiRecTemp.size() > 1) {
			beanResConsTranSpeiRec.getBeanError().setCodError(Errores.ED00029V);
			beanResConsTranSpeiRec.getBeanError().setTipoError(Errores.TIPO_MSJ_ALERT);
			return beanResConsTranSpeiRec;
		}
		return beanResConsTranSpeiRec;
	}
}
