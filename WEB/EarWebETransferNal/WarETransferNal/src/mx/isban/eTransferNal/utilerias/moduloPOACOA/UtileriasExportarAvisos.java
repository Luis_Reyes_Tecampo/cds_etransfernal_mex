/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExportarAvisos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/08/2019 12:20:33 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.moduloSPID.BeanAvisoTraspasos;

/**
 * Class UtileriasExportarAvisos.
 *
 * Clase de utilerias utilizada por la pantalla de avisos que contiene metodos
 * necesarios para realizar el fujo de exportacion.
 * 
 * @author FSW-Vector
 * @since 9/08/2019
 */
public final class UtileriasExportarAvisos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3444201497746703169L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasExportarAvisos utils;

	/**
	 * Obtener el objeto: utils.
	 *
	 * Obtener la instancia de la clase 
	 * de utilerias.
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasExportarAvisos getUtils() {
		if (utils == null) {
			utils = new UtileriasExportarAvisos();
		}
		return utils;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * Metodo para obtener el header usado en el 
	 * archivo de excel cuando se realiza el flujo
	 * de exportar.
	 * 
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { ctx.getMessage("moduloSPID.avisosTraspasos.text.hora"),
				ctx.getMessage("moduloSPID.avisosTraspasos.text.folio"),
				ctx.getMessage("moduloSPID.avisosTraspasos.text.tipo"),
				ctx.getMessage("moduloSPID.avisosTraspasos.text.importe"),
				ctx.getMessage("moduloSPID.avisosTraspasos.text.referencia") };
		return Arrays.asList(header);
	}

	/**
	 * Obtener el objeto: body.
	 *
	 * Metodo para obtener el cuerpo del
	 * archivo excel cuando se realiza el flujo de
	 * exportar.
	 * 
	 * @param listaBeanAvisoTraspasos El objeto: lista bean aviso traspasos
	 * @return El objeto: body
	 */
	public List<List<Object>> getBody(List<BeanAvisoTraspasos> listaBeanAvisoTraspasos) {
		List<Object> objListParamExp = null;
		List<List<Object>> objListExp = null;
		if (listaBeanAvisoTraspasos != null && !listaBeanAvisoTraspasos.isEmpty()) {
			objListExp = new ArrayList<List<Object>>();

			for (BeanAvisoTraspasos beanAvisoTraspasos : listaBeanAvisoTraspasos) {
				objListParamExp = new ArrayList<Object>();
				objListParamExp.add(beanAvisoTraspasos.getHora());
				objListParamExp.add(beanAvisoTraspasos.getFolio());
				objListParamExp.add(beanAvisoTraspasos.getTypTrasp());
				objListParamExp.add(beanAvisoTraspasos.getImporte());
				objListParamExp.add(beanAvisoTraspasos.getSIAC());
				objListExp.add(objListParamExp);
			}
		}
		return objListExp;
	}
}
