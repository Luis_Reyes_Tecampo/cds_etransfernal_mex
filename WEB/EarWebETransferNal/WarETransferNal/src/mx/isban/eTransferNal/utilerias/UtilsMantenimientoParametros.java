/**
 * Clase UtilsMantenimientoParametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;

import org.springframework.web.servlet.ModelAndView;

/**
 * The Class UtilsMantenimientoParametros.
 */
public final class UtilsMantenimientoParametros extends MetodosMantenimientoParametros {
	
	/** Referencia del Objeto Utilerias. */
	private static final UtilsMantenimientoParametros UTILS_MANT_PARAM_INSTACE = new UtilsMantenimientoParametros();

	/**
	 * Constructor privado.
	 */
	private UtilsMantenimientoParametros() {super();}
	
	/**
	 * Metodo singleton de Utilerias.
	 *
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtilsMantenimientoParametros getUtilerias(){
		return UTILS_MANT_PARAM_INSTACE;
	}

	/**
	 * Enviar parametros response.
	 *
	 * @param request the request
	 * @param responseBO the response bo
	 */
	public void setearFiltrosBusquedaParametrosPantallaPrincipal(BeanReqMantenimientoParametros request, BeanResMantenimientoParametros responseBO){
		responseBO.setParametro(request.getParametro());			//se setea campo parametro
		responseBO.setDescripParam(request.getDescripParam());		//se setea campo descripcion
		responseBO.setTipoParametro(request.getTipoParametro());	//se setea campo tipoParametro
		responseBO.setTipoDato(request.getTipoDato());				//se setea campo tipoDato
		responseBO.setLongitudDato(request.getLongitudDato());		//se setea campo longitudDato
		responseBO.setValorFalta(request.getValorFalta());			//se setea campo valorFalta
		responseBO.setTrama(request.getTrama());					//se setea campo trama
		responseBO.setCola(request.getCola());						//se setea campo cola
		responseBO.setMecanismo(request.getMecanismo());			//se setea campo cola
		responseBO.setPosicion(request.getPosicion());				//se setea campo posicion
		responseBO.setParametroFalta(request.getParametroFalta());	//se setea campo parametroFalta
		responseBO.setPrograma(request.getPrograma());				//se setea campo programa
	}
	
	/**
	 * Setear filtros busqueda parametros pantalla guardar.
	 *
	 * @param request the request
	 * @param responseBO the response bo
	 * @param modelAndView the model and view
	 */
	public void setearFiltrosBusquedaParametrosPantallaGuardar(BeanReqMantenimientoParamGuardar request, BeanResMantenimientoParametros responseBO, ModelAndView modelAndView){
		modelAndView.addObject("filtroParametro", request.getParametro());
		modelAndView.addObject("filtroDescripcion", request.getDescripParam());
		modelAndView.addObject("filtroTipoParametro", request.getTipoParametro());
		modelAndView.addObject("filtroTipoDato", request.getTipoDato());
		modelAndView.addObject("filtroLongitudDato", request.getLongitudDato());
		modelAndView.addObject("filtroValorFalta", request.getValorFalta());
		modelAndView.addObject("filtroTrama", request.getTrama());
		modelAndView.addObject("filtroCola", request.getCola());
		modelAndView.addObject("filtroMecanismo", request.getMecanismo());
		modelAndView.addObject("filtroPosicion", request.getPosicion());
		modelAndView.addObject("filtroParametroFalta", request.getParametroFalta());
		modelAndView.addObject("filtroPrograma", request.getPrograma());
		responseBO.setPaginador(request.getPaginador());
	}

	/**
	 * Setear filtros busqueda bean req mantantenimiento parametros.
	 *
	 * @param requestPrincipal the request principal
	 * @param requestG the request g
	 */
	public void setearFiltrosBusquedaBeanReqMantantenimientoParametros(
			BeanReqMantenimientoParametros requestPrincipal, BeanReqMantenimientoParamGuardar requestG){
		requestPrincipal.setParametro(requestG.getFiltroParametro());
		requestPrincipal.setDescripParam(requestG.getFiltroDescripcion());
		requestPrincipal.setTipoParametro(requestG.getFiltroTipoParametro());
		requestPrincipal.setTipoDato(requestG.getFiltroTipoDato());
		requestPrincipal.setLongitudDato(requestG.getFiltroLongitudDato());
		requestPrincipal.setValorFalta(requestG.getFiltroValorFalta());
		requestPrincipal.setTrama(requestG.getFiltroTrama());
		requestPrincipal.setCola(requestG.getFiltroCola());
		requestPrincipal.setMecanismo(requestG.getFiltroMecanismo());
		requestPrincipal.setPosicion(requestG.getFiltroPosicion());
		requestPrincipal.setParametroFalta(requestG.getFiltroParametroFalta());
		requestPrincipal.setPrograma(requestG.getFiltroPrograma());
		requestPrincipal.setPaginador(requestG.getPaginador());
	}
	
	/**
	 * Obtener filtros busqueda.
	 *
	 * @param modelAndView the model and view
	 * @param request the request
	 * @param responseBO the response bo
	 */
	public void setearCamposEdicionPantallaGuardar(ModelAndView modelAndView,
			BeanReqMantenimientoParamGuardar request, BeanResMantenimientoParametros responseBO){	
		modelAndView.addObject("filtroParametro", request.getFiltroParametro());			//filtro Parametro
		modelAndView.addObject("filtroDescripcion", request.getFiltroDescripcion());		//filtro descripcion
		modelAndView.addObject("filtroTipoParametro", request.getFiltroTipoParametro());	//filtro tipoParametro
		modelAndView.addObject("filtroTipoDato", request.getFiltroTipoDato());				//filtro tipoDato
		modelAndView.addObject("filtroLongitudDato", request.getFiltroLongitudDato());		//filtro LongitudDato
		modelAndView.addObject("filtroValorFalta", request.getFiltroValorFalta());			//filtro valorFalta
		modelAndView.addObject("filtroTrama", request.getFiltroTrama());					//filtro trama
		modelAndView.addObject("filtroCola", request.getFiltroCola());						//filtro cola
		modelAndView.addObject("filtroMecanismo", request.getFiltroMecanismo());			//filtro mecanismo
		modelAndView.addObject("filtroPosicion", request.getFiltroPosicion());				//filtro posicion
		modelAndView.addObject("filtroParametroFalta", request.getFiltroParametroFalta());	//filtro parametroFalta
		modelAndView.addObject("filtroPrograma", request.getFiltroPrograma());				//filtro programa
		modelAndView.addObject("esAltaNuevoParam", true);									//identificador esAltaNuevoParametro 
		responseBO.setPaginador(request.getPaginador());
		responseBO.setPrograma(request.getPrograma());
		responseBO.setParametroFalta(request.getParametroFalta());
		responseBO.setPosicion(request.getPosicion());
		responseBO.setMecanismo(request.getMecanismo());
		responseBO.setCola(request.getCola());
		responseBO.setTrama(request.getTrama());
		responseBO.setValorFalta(request.getValorFalta());
		responseBO.setLongitudDato(request.getLongitudDato());
		responseBO.setTipoDato(request.getTipoDato());
		responseBO.setTipoParametro(request.getTipoParametro());
		responseBO.setDescripParam(request.getDescripParam());
		responseBO.setParametro(request.getParametro());
	}
	
	/**
	 * Enviar opciones combos.
	 *
	 * @param resCombos the res combos
	 * @param responseBO the response bo
	 */
	public void enviarResponseOpcionesCombos(BeanResMantenimientoParametros resCombos, BeanResMantenimientoParametros responseBO){
		responseBO.setOpcionesTipoParam(resCombos.getOpcionesTipoParam());
		responseBO.setOpcionesTipoDato(resCombos.getOpcionesTipoDato());
	}

	/**
	 * Generar opciones trama cola mecanismo.
	 *
	 * @param modelAndView the model and view
	 */
	public void generarOpcionesCombosDefine(ModelAndView modelAndView){
		List<BeanMantenimientoParametros> listaOpciones=new ArrayList<BeanMantenimientoParametros>();
		BeanMantenimientoParametros beanSelec=new BeanMantenimientoParametros();
		BeanMantenimientoParametros beanSi=new BeanMantenimientoParametros();
		BeanMantenimientoParametros beanNo=new BeanMantenimientoParametros();
		BeanMantenimientoParametros beanNull=new BeanMantenimientoParametros();
		beanSelec.setValorOpcion("");
		beanSelec.setOpcion("Seleccione");
		beanSi.setValorOpcion("S");
		beanSi.setOpcion("Si");
		beanNo.setValorOpcion("N");
		beanNo.setOpcion("No");
		beanNull.setValorOpcion("Nulo");
		beanNull.setOpcion("Nulo");
		listaOpciones.add(beanSelec);
		listaOpciones.add(beanSi);
		listaOpciones.add(beanNo);
		listaOpciones.add(beanNull);
		modelAndView.addObject("defineTramaColaMecanismo", listaOpciones);
	}
	
	/**
	 * Obtener datos param selec.
	 *
	 * @param resConsultaDatosParam the res consulta datos param
	 * @param responseBO the response bo
	 */
	public void setearDatosParametroSelecccionado(BeanResMantenimientoParametros resConsultaDatosParam, BeanResMantenimientoParametros responseBO){
		for(BeanMantenimientoParametros bean:resConsultaDatosParam.getDatosParametro()){
			responseBO.setParametro(bean.getCveParametro());
			responseBO.setDescripParam(bean.getDescripParam());
			responseBO.setTipoParametro(bean.getTipoParametro());
			responseBO.setTipoDato(bean.getTipoDato());
			responseBO.setLongitudDato(bean.getLongitud());
			responseBO.setValorFalta(bean.getValorFalta());
			responseBO.setTrama(bean.getTrama());
			responseBO.setCola(bean.getCola());
			responseBO.setMecanismo(bean.getMecanismo());
			responseBO.setPosicion(bean.getPosicion());
			responseBO.setParametroFalta(bean.getParametroFalta());
			responseBO.setPrograma(bean.getPrograma());
		}
	}
	
	/**
	 * Request to bean req mantenimiento parametros.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req mantenimiento parametros
	 */
	public BeanReqMantenimientoParametros requestToBeanReqMantenimientoParametros(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqMantenimientoParametros request = new BeanReqMantenimientoParametros();
		List<BeanMantenimientoParametros> listParamSeleccionados = new ArrayList<BeanMantenimientoParametros>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countListParamSeleccionados"), listParamSeleccionados, new BeanMantenimientoParametros());
		request.setListSelecParam(listParamSeleccionados);
		UtilsMapeaRequest.getMapper().mapearObject(request, req.getParameterMap());
		return request;
	}
}