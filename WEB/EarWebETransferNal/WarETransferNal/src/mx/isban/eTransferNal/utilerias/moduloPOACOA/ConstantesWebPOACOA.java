/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: ConstantesPOACOA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 01:54:23 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;

/**
 * Class ConstantesPOACOA.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
public class ConstantesWebPOACOA implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3835578638857042604L;

	/** La constante CONS_MODULO. */
	public static final String CONS_MODULO = "modulo";

	/** La constante CONS_ACTUAL. */
	public static final String CONS_ACTUAL = "actual";

	/** La constante CONS_SPID. */
	public static final String CONS_SPID = "spid";

	/** La constante CONS_ACCION. */
	public static final String CONS_ACCION = "accion";

	/** La constante BEAN_MODULO. */
	public static final String BEAN_MODULO = "beanModulo";

	/** La constante CONS_SPEI. */
	public static final String CONS_SPEI = "spei";

	/** La constante CONS_POA. */
	public static final String CONS_POA = "poa";

	/** La constante CONS_COA. */
	public static final String CONS_COA = "coa";

	/** La constante SESSION. */
	public static final String SESSION = "sessionSPID";

	/** La constante SORT_FIELD. */
	public static final String SORT_FIELD = "sortField";

	/** La constante SORT_TYPE. */
	public static final String SORT_TYPE = "sortType";
	
	public static final String ERROR_BINDING = "Ha ocurrido error de validacion en los datos de entrada";
}
