package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqOpePendAutDetalle;
import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;
import mx.isban.eTransferNal.constantes.moduloCDA.Errores;

import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * The Class MetodosAutorizarOperaciones.
 * 
 */
public class MetodosAutorizarOperaciones extends Architech{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5067048018644552309L;
	
	/**
	 * Obtener registros seleccionados.
	 *
	 * @param reqAutorizaOperaciones the req autoriza operaciones
	 * @return the list
	 * 
	 */
	public List<BeanAutorizarOpe> obtenerRegistrosSeleccionados(BeanReqAutorizarOpe reqAutorizaOperaciones){
		List<BeanAutorizarOpe> registrosSeleccionados= new ArrayList<BeanAutorizarOpe>();
		if(reqAutorizaOperaciones.getListOperacionesSeleccionadas() != null){
			for(BeanAutorizarOpe operacion:reqAutorizaOperaciones.getListOperacionesSeleccionadas()){
				if (operacion.isSeleccionado()){
					registrosSeleccionados.add(operacion);
				}
			}
		}
		return registrosSeleccionados;
	}
	
	/**
	 * Validar operaciones apartadas.
	 *
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return true, if successful
	 */
	public boolean validarOperacionesApartadas(List<BeanAutorizarOpe> listOperacionesSeleccionadas){
		String estatusApartado;
		boolean opeApartadas = true;

		for(BeanAutorizarOpe opeSeleccionada:listOperacionesSeleccionadas){
			estatusApartado = opeSeleccionada.getStatusOperacionApartada();
			//Si estatus no es 2 (operacion no apartada por ningun usuario en el modulo AutorizarOperacion) u operacion no esta apartada por usuario logado
			if( !opeSeleccionada.isApartado() || !"2".equals(estatusApartado) ){
				opeApartadas = false;
			}
		}
		return opeApartadas;
	}

	/**
	 * Validar permisos sobre operacion.
	 *
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @param usuario the usuario
	 * @return true, if successful
	 */
	public boolean validarPermisosSobreOperacion(List<BeanAutorizarOpe> listOperacionesSeleccionadas, String usuario){
		String usuariosAutorizados;
		boolean usuarioConPermisos = true;

		//se recorre la lista de operaciones seleccionadas
		for(BeanAutorizarOpe opeSeleccionada:listOperacionesSeleccionadas){
			usuariosAutorizados = opeSeleccionada.getUsrsAutorizanTemplate();
			//Si usuario logeado no esta entre usuariosAutorizados
			if( !usuariosAutorizados.contains(usuario) ){
				usuarioConPermisos = false;
			}
		}
		return usuarioConPermisos;
	}
	
	/**
	 * Valida estatus oper.
	 *
	 * @param listOpeSelec the list ope selec
	 * @param estatusOpe the estatus ope
	 * 
	 * @return true, if successful
	 * 
	 */
	public boolean validaEstatusOper(List<BeanAutorizarOpe> listOpeSelec,int estatusOpe){
		boolean opePendAut = true;
		String estatus = "";
		for(BeanAutorizarOpe elemento:listOpeSelec){
			estatus = elemento.getEstatus();
			if( estatusOpe == 1 || estatusOpe == 3 ){
				//Si es estatus Pendiente
				if( !("PA".equals(estatus) )){
					opePendAut = false;
				}
			}else{
				//Si es estatus Autorizado o Pendiente
				if( !("AU".equals(estatus) || "PA".equals(estatus)) ){
					opePendAut = false;
				}
			}
		}
		return opePendAut;
	}

	/**
	 * Valida ope select por apartar.
	 *
	 * @param listOpeSelec the list ope selec
	 * 
	 * @return true, if successful
	 * 
	 */
	public boolean validaOpeSelectPorApartar(List<BeanAutorizarOpe> listOpeSelec){
		boolean seleccionValida = true;
		for(BeanAutorizarOpe elemento:listOpeSelec){
			String usrApart = elemento.getUsrApartado();
			//Si usuario Apartado es vacio ""
			if( !"".equals(usrApart) ){
				seleccionValida=false;
			}
		}
		return seleccionValida;
	}
	
	/**
	 * Tratar business exception.
	 *
	 * @param modelAndView the model and view
	 * @param e the e
	 */
	public void tratarBusinessException(ModelAndView modelAndView, BusinessException e){
		modelAndView.addObject(Constantes.TIPO_ERROR, Errores.TIPO_MSJ_ERROR);
		modelAndView.addObject(Constantes.COD_ERROR, e.getCode());
		modelAndView.addObject(Constantes.DESC_ERROR, e.getMessage());
	}

	/**
	 * Request to BeanReqAutorizarOpe.
	 *
	 * @param req the req
	 * @param modelAndView the model and view
	 * @return the bean req autoriza operaciones
	 */
	//Metodo que mapea diccionario a BeanReqAutorizarOpe
	public BeanReqAutorizarOpe requestToBeanReqAutorizarOpe(HttpServletRequest req, ModelAndView modelAndView){
		BeanReqAutorizarOpe beanReqConsOperacionesPend = new BeanReqAutorizarOpe();
		List<BeanAutorizarOpe> listOperacionesSeleccionadas = new ArrayList<BeanAutorizarOpe>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countlistOperacionesSeleccionadas"), listOperacionesSeleccionadas, new BeanAutorizarOpe());
		beanReqConsOperacionesPend.setListOperacionesSeleccionadas(listOperacionesSeleccionadas);
		UtilsMapeaRequest.getMapper().mapearObject(beanReqConsOperacionesPend, req.getParameterMap());
		return beanReqConsOperacionesPend;
	}
	
    /**
     * Request to BeanReqOpePendAutDetalle.
     *
     * @param req the req
     * @param modelAndView the model and view
     * @return the bean req autoriza operaciones
     */
    //Metodo que mapea diccionario a BeanReqOpePendAutDetalle
    public BeanReqOpePendAutDetalle requestToBeanReqOpePendAutDetalle(HttpServletRequest req, ModelAndView modelAndView){
        BeanReqOpePendAutDetalle reqDetalleOperacion = new BeanReqOpePendAutDetalle();
        UtilsMapeaRequest.getMapper().mapearObject(reqDetalleOperacion, req.getParameterMap());
        return reqDetalleOperacion;
    }
}
