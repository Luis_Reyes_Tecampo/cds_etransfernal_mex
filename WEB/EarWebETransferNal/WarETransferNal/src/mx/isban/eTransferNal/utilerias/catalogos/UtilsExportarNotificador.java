/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarNotificador.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:37:35 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;

/**
 * Class UtilsExportarNotificador.
 * 
 * Clase que contiene metodos utilizados en el flujo
 * de exportacion del catalogo.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public final class UtilsExportarNotificador extends Architech {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 685191533596096048L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarNotificador instancia;
	
	/**
	 * Se crea el constructor.
	 */
	private UtilsExportarNotificador() {
	}

	/**
	 * Funcion para crear la instancia a la clase.
	 * 
	 * @return instancia: regresa el objeto instancia cuando este está vacio
	 */
	public static UtilsExportarNotificador getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarNotificador();
		}
		return instancia;
	}

	/**
	 * Obtiene el objeto header excel.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar.
	 *  
	 * @param ctx --> objeto de tipo RequestContext que obtiene los requisitos del contexto para generar el excel
	 * @return header --> objeto excel que obtiene el header
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.notificador.lblTipoNot"),
				ctx.getMessage("catalogos.notificador.lblClaveTrans"),
				ctx.getMessage("catalogos.notificador.lblMedioEntrega"), 
				ctx.getMessage("catalogos.notificador.lblEstatus") 
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtiene el objeto body.
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 *  
	 *
	 * @param notificaciones --> obtiene el listado de notificaciones
	 * @return objList --> devuelve el listado de notificaciones 
	 */
	public List<List<Object>> getBody(List<BeanNotificacion> notificaciones) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (notificaciones != null && !notificaciones.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanNotificacion not : notificaciones) {
				objListParam = new ArrayList<Object>();
				objListParam.add(not.getTipoNotif());
				objListParam.add(not.getCveTransfer());
				objListParam.add(not.getMedioEntrega());
				objListParam.add(not.getEstatusTransfer());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
}
