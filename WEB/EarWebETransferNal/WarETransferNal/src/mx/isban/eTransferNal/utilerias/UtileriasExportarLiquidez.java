/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMttoMediosValidar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10-13-2018 10:17:18 PM Ana Gloria Martines  Isban Creacion
 */
 package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;


/**
 * The Class UtileriasExportarLiquidez.
 * Utilerias de formato y 
 * obtencion de datos de los queries y 
 * transformacion en Beans
 *
 * @author FSW-Vector
 * @since 10-13-2018
 */
public final class UtileriasExportarLiquidez {

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtileriasExportarLiquidez instancia ;
	
	/**
	 * Se crea el constructor.
	 */
	private UtileriasExportarLiquidez() {
		//Método vacío por ser constructor por default
	}

	/**
	 * Metodo singleton de Utilerias.
	 *
	 * funcion para crear la instancia a la clase.
	 * 
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtileriasExportarLiquidez getInstancia() {
		if( instancia == null ) {
			instancia = new UtileriasExportarLiquidez();
		}
		return instancia;
	}

	/**
	 * Obtener el objeto: header excel.
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @param ctx El objeto: ctx - requestcontext para message
	 * @return El objeto: header excel 
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
 				ctx.getMessage("catalogos.notificacionLiquidezIn.lbl.cveTrasfe"),
				ctx.getMessage("catalogos.notificacionLiquidezIn.lbl.cveMecanismo"), 
				ctx.getMessage("catalogos.notificacionLiquidezIn.lbl.cveEstatus") 
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtener el objeto: body.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 *  excel par el flujo de exportar.
	 *  
	 * @param liquidez El objeto: liquidez
	 * @return El objeto: body del exportar
	 */
	public List<List<Object>> getBody(List<BeanLiquidezIntradia> liquidez) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (liquidez != null && !liquidez.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanLiquidezIntradia not : liquidez) {
				objListParam = new ArrayList<Object>();
 				objListParam.add(not.getCveTransFe());
				objListParam.add(not.getCveMecanismo());
				objListParam.add(not.getEstatus());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
}
