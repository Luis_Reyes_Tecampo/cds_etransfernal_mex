package mx.isban.eTransferNal.utilerias;


/**
 * The Class UtilsCapturasManuales.
 */
public final class UtilsCapturasManuales {
	
	/**Referencia del Objeto Utilerias*/
	private static final UtilsCapturasManuales UTILERIAS_INSTACE = new UtilsCapturasManuales();

	/**
	 * Constructor privado
	 */
	private UtilsCapturasManuales() {super();}
	
	/**
	 * Metodo singleton de Utilerias
	 * @return Utilerias Referencia de utilerias
	 */
	public static UtilsCapturasManuales getUtilerias(){
		return UTILERIAS_INSTACE;
	}
	
	/**
	 * Eliminar injeccion de codigo.
	 *
	 * @param in the in
	 * @return the string
	 */
	public String eliminarInjeccionDeCodigo(String in){
		if(in!=null){
			return in.replaceAll(">", "").replaceAll("<", "").replaceAll("\"", "").replaceAll("\'", "");
		}
		return null;
	}
}
