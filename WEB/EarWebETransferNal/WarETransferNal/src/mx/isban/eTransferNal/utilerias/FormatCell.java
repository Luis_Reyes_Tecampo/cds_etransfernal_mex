package mx.isban.eTransferNal.utilerias;

/**Clase que permite aplicar un formato */
public class FormatCell {
	/**Propiedad del tipo String que almacena el nombre del Font*/
	private String fontName = "";
	/**Propiedad del tipo boolean que indica si la fuente esta en negrita*/
	private boolean bold = false;
	/**Propiedad del tipo int que almacena el tamano de la font*/
	private short fontSize = 10;
	
	/**
	 * Metodo get que obtiene el nombre del font
	 * @return fontName Objeto String que regresa el nombre del font
	 */
	public String getFontName() {
		return fontName;
	}
	/**
	 * Metodo set que modifica el nombre del font
	 * @param fontName Objeto String que regresa el nombre del font
	 */
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	/**
	 * Metodo get que obtiene si la letra es con negrilla
	 * @return bold boolean que indica si la letra es con negrilla
	 */
	public boolean isBold() {
		return bold;
	}
	/**
	 * Metodo set que modifica si la letra es con negrilla
	 * @param bold boolean que indica si la letra es con negrilla
	 */
	public void setBold(boolean bold) {
		this.bold = bold;
	}
	/**
	 * Metodo get que obtiene el tamano de la letra
	 * @return fontSize variable del tipo short que almacena el tamano de la letra
	 */
	public short getFontSize() {
		return fontSize;
	}
	/**
	 * Metodo set que modifica el tamano de la letra
	 * @param fontSize variable del tipo short que modifica el tamano de la letra
	 */
	public void setFontSize(short fontSize) {
		this.fontSize = fontSize;
	}
	
	
}
