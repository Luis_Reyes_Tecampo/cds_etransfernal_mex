/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * LogSimpleMappingExceptionResolver.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/04/2014 13:32:33 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.utilerias;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class LogSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver{
	/**Referencia constante del LOG*/
	private static final Logger LOG = Logger.getLogger(LogSimpleMappingExceptionResolver.class);

	/**
	 * Metodo que sirve para resolver las exceptiones
	 * @param request Objeto del tipo HttpServletRequest
	 * @param response Objeto del tipo HttpServletResponse
	 * @param handler Objeto del tipo Object
	 * @param ex Objeto del tipo Exception 
	 * @return ModelAndView Objeto de Spring hacia donde se va redireccionar segun la pagina
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex) {
		LOG.debug(getStackTrace(ex));
		return super.resolveException(request, response, handler, ex);
	}

	/**
	* @param t Objeto del tipo Throwable
	* @return String con la traza de la exception
	*/
	public String getStackTrace(Throwable t) {
		StringWriter stringWritter = new StringWriter();
		PrintWriter printWritter = new PrintWriter(stringWritter, true);
		t.printStackTrace(printWritter);
		printWritter.flush();
		stringWritter.flush();
		return stringWritter.toString();
	}

}
