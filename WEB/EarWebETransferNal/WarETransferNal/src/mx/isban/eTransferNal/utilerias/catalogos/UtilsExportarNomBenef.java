/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsExportarNomBenef.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      19/09/2019 10:51:26 PM     Uriel Alfredo Botello R.    VSF        Creacion
 */

package mx.isban.eTransferNal.utilerias.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;


/**
 * Class UtilsExportarNomBenef.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
public final class UtilsExportarNomBenef extends Architech {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3553024843195371790L;
	
	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsExportarNomBenef instancia;

	/**
	 * Nueva instancia utils exportar nombres fideico.
	 */
	private UtilsExportarNomBenef() {
	}
	
	/**
	 * Obtener el objeto: instancia.
	 *
	 * Funcion para crear el header del archivo
	 * excel con los properties necesarios.
	 * 
	 * @return instancia: variable estatica de tipo UtilsExportarCuentasFideico 
	 */
	public static UtilsExportarNomBenef getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsExportarNomBenef();
		}
		return instancia;
	}
	
	/**
	 * Obtener header excel.
	 *
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar.
	 *  
	 * @param ctx --> objeto de tipo RequestContext que obtiene el listado de cuentas
	 * @return El objeto --> header excel, devuelve el listado
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] { 
				ctx.getMessage("catalogos.excepNomBeneficiarios.nombre"),
				};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtener el objeto: body.
	 * 
	 * Metodo que crear el cuerpo del archivo de
	 * excel par el flujo de exportar
	 *  
	 *
	 * @param nombres --> obtiene el listado de nombres
	 * @return objList -->  devuelve el listado de nombres para agregar al archivo excel
	 */
	public List<List<Object>> getBody(List<BeanNomBeneficiario> nombres) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (nombres != null && !nombres.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanNomBeneficiario nombre : nombres) {
				objListParam = new ArrayList<Object>();
				objListParam.add(nombre.getNombre());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
	
}
