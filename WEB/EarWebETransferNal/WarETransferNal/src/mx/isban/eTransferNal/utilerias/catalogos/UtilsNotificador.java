/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsNotificador.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/07/2019 01:01:21 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.catalogos;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;

/**
 * Class UtilsNotificador.
 *
 * Clase que contiene metodos de ayuda 
 * para los flujos del controller
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
public final class UtilsNotificador implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4625034177417172406L;

	/** La variable que contiene informacion con respecto a: instancia. */
	private static UtilsNotificador instancia;
	
	/**
	 * Obtiene el objeto instancia
	 *
	 * @return instancia: regresa el objeto instancia cuando este está vacio
	 */
	public static UtilsNotificador getInstancia() {
		if( instancia == null ) {
			instancia = new UtilsNotificador();
		}
		return instancia;
	}
	
	/**
	 * Obtiene los datos del registro anterior
	 * 
	 * @param req -->objeto de tipo HttpServletRequest que recibe los datos del registro anterior
	 * @return not --> devuelve los datos del registro anterior
	 */
	public static BeanNotificacion getAnterior(HttpServletRequest req) {
		BeanNotificacion not = new BeanNotificacion();
		not.setTipoNotif(req.getParameter("tipoNotifOld"));
		not.setCveTransfer(req.getParameter("cveTransferOld"));
		not.setMedioEntrega(req.getParameter("medioEntregaOld"));
		not.setEstatusTransfer(req.getParameter("estatusTransferOld"));
		return not;
	}
}
