/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasExportarRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     9/08/2019 12:20:17 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.utilerias.moduloPOACOA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;


import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaRecepciones;

/**
 * Class UtileriasExportarRecepciones.
 *
 * Clase de utilerias utilizada por la pantalla de recepciones que contiene
 * metodos necesarios para realizar el fujo de exportacion.
 * 
 * @author FSW-Vector
 * @since 9/08/2019
 */
public final class UtileriasExportarRecepciones implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 6294129166879373573L;

	/** La variable que contiene informacion con respecto a: utils. */
	private static UtileriasExportarRecepciones utils;

	/**
	 * Obtener el objeto: utils.
	 *
	 * Obtener la instancia de la clase 
	 * de utilerias.
	 * 
	 * @return El objeto: utils
	 */
	public static UtileriasExportarRecepciones getUtils() {
		if (utils == null) {
			utils = new UtileriasExportarRecepciones();
		}
		return utils;
	}

	/**
	 * Obtener nombre.
	 *
	 * Metodo para obtener el nombre
	 * del archivo que se exporta.
	 * 
	 * @param tipo El objeto: tipo
	 * @return Objeto string
	 */
	public String obtenerNombre(String tipo) {
		String nombre = "";

		if ("PA".equalsIgnoreCase(tipo)) {
			nombre = "RECEPCIONES_PA";
		} else if ("TR".equalsIgnoreCase(tipo)) {
			nombre = "RECEPCIONES_TR";
		} else if ("RE".equalsIgnoreCase(tipo)) {
			nombre = "RECEPCIONES_RE";
		}
		return nombre;
	}

	/**
	 * Obtener el objeto: body.
	 *
	 * Metodo para obtener el cuerpo del
	 * archivo excel cuando se realiza el flujo de
	 * exportar.
	 * 
	 * @param listaBeanConsultaRecepciones El objeto: lista bean consulta
	 *                                     recepciones
	 * @return El objeto: body
	 */
	public List<List<Object>> getBody(List<BeanConsultaRecepciones> listaBeanConsultaRecepciones) {

		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listaBeanConsultaRecepciones != null && !listaBeanConsultaRecepciones.isEmpty()) {
			objList = new ArrayList<List<Object>>();

			for (BeanConsultaRecepciones beanConsultaRecepciones : listaBeanConsultaRecepciones) {
				objListParam = new ArrayList<Object>();
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getTopologia()));
				objListParam.add(getNumber(beanConsultaRecepciones));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getEstatusBanxico()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getEstatusTransfer()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFechOperacion()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFechCaptura()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getCveInsOrd()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getCveIntermeOrd()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFolioPaquete()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getFolioPago()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getNumCuentaRec()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getMonto()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getMotivoDevol()));
				objListParam.add(getValor(beanConsultaRecepciones.getCodigoError()));
				objListParam.add(getValor(beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getNumCuentaTran()));
				objList.add(objListParam);
			}
		}
		return objList;
	}

	/**
	 * Obtener el objeto: number.
	 *
	 * @param beanConsultaRecepciones El objeto: bean consulta recepciones
	 * @return El objeto: number
	 */
	private Integer getNumber(BeanConsultaRecepciones beanConsultaRecepciones) {
		Integer retorno = Integer.valueOf(1);
		if (beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getTipoPago() != null) {
			retorno = beanConsultaRecepciones.getBeanConsultaRecepcionesDos().getTipoPago();
		}
		return retorno;
	}
	/**
	 * Obtener el objeto: valor.
	 *
	 * @param valor El objeto: valor
	 * @return El objeto: valor
	 */
	private String getValor(String valor) {
		String retorno = "";
		if(valor != null) {
			retorno = valor;
		}
		return retorno;
	}
		
	/**
	 * Obtener el objeto: header excel.
	 *
	 * Metodo para obtener el header usado en el 
	 * archivo de excel cuando se realiza el flujo
	 * de exportar.
	 * 
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {

				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtTopo"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtTipo"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtEstatus"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtEstatus"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFchOperacion"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtHora"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtIntsOrd"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtInterOrd"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFoliosPaquete"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtFoliosPago"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCuentaRec"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtImporte"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtDevol"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCodErr"),
				ctx.getMessage("moduloSPID.consultaRecepciones.text.txtCuentaTran")

		};
		return Arrays.asList(header);
	}
}
