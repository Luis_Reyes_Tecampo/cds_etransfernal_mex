package mx.isban.eTransferNal.utilerias.contingencias;




/**
 * The Class UtilsMttoMedios.
 *
 * @author FSW-Vector
 * @since 14/09/2018
 */
public class UtilsContingenciasOperacion {
	
		

	/** Propiedad del tipo UtileriasMQ que almacena el valor de utileriasMQ. */
	private static UtilsContingenciasOperacion utilExportOperacion = new UtilsContingenciasOperacion();
	
	/**
	 * Metodo que sirve para obtene la instancia del IsbanDataAccess.
	 *
	 * @return UtileriasMQ Objeto del tipo UtileriasMQ
	 */
	public static UtilsContingenciasOperacion getInstance(){
		return utilExportOperacion;
	}

	
	/**
	 * Obtener historico.
	 *
	 * @param pantalla El objeto: pantalla
	 * @return true, si exitoso
	 */
	public boolean obtenerHistorico(String pantalla) {
		boolean historico = true;
		
		if(("normal").equals(pantalla)) {
			historico= false;
		}
		
		return historico;
	}
}
