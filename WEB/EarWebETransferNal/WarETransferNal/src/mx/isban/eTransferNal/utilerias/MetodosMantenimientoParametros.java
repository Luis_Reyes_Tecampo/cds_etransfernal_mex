/**
 * Clase MetodosMantenimientoParametros
 * 
 * @author: Vector
 * @since: Mayo 2017
 * @version: 1.0
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;

/**
 * The Class MetodosMantenimientoParametros.
 */
public class MetodosMantenimientoParametros {
	
	/**
	 * Asignar valor combos tipo dato tipo param.
	 *
	 * @param request the request
	 */
	//Asignar valor combos tipo dato tipo param.
	public void asignarValorCombosTipoDatoTipoParam(BeanReqMantenimientoParametros request){
		//si tipoParametro no viene vacio
		if( !"".equals(request.getTipoParametro()) ){
			//se obtiene y setea tipoParametro
			request.setTipoParametro(request.getTipoParametro());
		} else {
			//tipoParametro se setea nulo
			request.setTipoParametro(null);
		}
		//si tipoDato no viene vacio
		if( !"".equals(request.getTipoDato()) ){
			//se obtiene y setea tipoDato
			request.setTipoDato(request.getTipoDato());
		} else {
			//tipoDato se setea nulo
			request.setTipoDato(null);
		}
	}

	/**
	 * Request to bean req mantenimiento param guardar.
	 *
	 * @param req the req
	 * @return the bean req mantenimiento param guardar
	 */
	public BeanReqMantenimientoParamGuardar requestToBeanReqMantenimientoParamGuardar(HttpServletRequest req){
		BeanReqMantenimientoParamGuardar beanRequest = new BeanReqMantenimientoParamGuardar();
		List<BeanMantenimientoParametros> listParamSeleccionados = new ArrayList<BeanMantenimientoParametros>();
		UtilsMapeaRequest.getMapper().anadirALista(req.getParameter("countListParamSeleccionados"), listParamSeleccionados, new BeanMantenimientoParametros());
		beanRequest.setListSelecParam(listParamSeleccionados);
		UtilsMapeaRequest.getMapper().mapearObject(beanRequest, req.getParameterMap());
		return beanRequest;
	}
	
	/**
	 * Obtener parametros seleccionados.
	 *
	 * @param request the request
	 * @return the list
	 */
	public List<BeanMantenimientoParametros> obtenerParametrosSeleccionados(BeanReqMantenimientoParametros request){
		List<BeanMantenimientoParametros> listParamsSel=new ArrayList<BeanMantenimientoParametros>();
		for(BeanMantenimientoParametros bean: request.getListSelecParam()){
			if(bean.isSeleccionado()){
				listParamsSel.add(bean);
			}
		}
		return listParamsSel;
	}

	/**
	 * Gets the body.
	 *
	 * @param listBeanConsOperaciones the list bean cons operaciones
	 * @return the body
	 */
 	public List<List<Object>> getBody(
			List<BeanMantenimientoParametros> listBeanConsOperaciones) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listBeanConsOperaciones != null && !listBeanConsOperaciones.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanMantenimientoParametros beanMantParam : listBeanConsOperaciones) {
				objListParam = new ArrayList<Object>();
				objListParam.add(beanMantParam.getCveParametro());
				objListParam.add(beanMantParam.getParametroFalta());
				objListParam.add(beanMantParam.getValorFalta());
				objListParam.add(beanMantParam.getTipoParametro());
				objListParam.add(beanMantParam.getDescripParam());
				objList.add(objListParam);
			}
		}
		return objList;
	}
}