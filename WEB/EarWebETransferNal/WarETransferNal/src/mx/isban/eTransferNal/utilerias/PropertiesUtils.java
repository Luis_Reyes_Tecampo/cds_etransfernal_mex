/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: PropertiesUtils.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      9/11/2017 11:24:45 AM 	Alan Garcia Villagran. Vector 		Creacion
 */
package mx.isban.eTransferNal.utilerias;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * The Class PropertiesUtils.
 */
public final class PropertiesUtils {

	/** Referencia constante al Log4j. */
	private static final Logger LOG = Logger.getLogger(PropertiesUtils.class);

	/** Propiedad que almacena el valor de RUTA_ARQ. */
	private static final String RUTA_ARQ = "/arquitecturaAgave/DistV1";

	/** The Constant CONFIGURACION_AMBIENTE_EJB_PROPERTIES. */
	private static final String CONFIGURACION_AMBIENTE_PROPERTIES = "/Configuracion/AmbienteEJB.properties";

	/** The cluster name. */
	public static final String CLUSTERNAME = cargaNombreCluster();

	/**
	 * Constructor privado.
	 */
	private PropertiesUtils() {}

	/**
	 * Load cluster name.
	 *
	 * @return the string
	 */
	private static String cargaNombreCluster() {
		String archivoAmbiente = RUTA_ARQ + CONFIGURACION_AMBIENTE_PROPERTIES;
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(new File(archivoAmbiente)));
			String clusterEjb = p.getProperty("CLUSTEREJB");
			//Se valida que la propiedad exista
			if (clusterEjb != null) {
				//se retorna nombre de cluster
				return clusterEjb;
			}
		} catch (FileNotFoundException e) {
			LOG.error("No se encontro el archivo de configuracion...",e);
		} catch (IOException e) {
			LOG.error("No se puede cargar el archivo de configuracion...",e);
		}
		//se retorna valor vacio para nombre de cluster
		return "";
	}
}