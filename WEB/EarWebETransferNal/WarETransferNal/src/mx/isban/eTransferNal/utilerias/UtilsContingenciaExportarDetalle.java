/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtilsContingenciaExportarDetalle.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/10/2018 06:03:04 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.utilerias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.support.RequestContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.eTransferNal.beans.contingencia.BeanTranMensajeCanales;

/**
 * Class UtilsContingenciaExportarDetalle.
 *
 * Clase que contiene  los metodos utilados para formar el archivo con la informacion exportada
 * 
 * @author FSW-Vector
 * @since 12/10/2018
 */
public class UtilsContingenciaExportarDetalle extends Architech{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 9220571603284011065L;

	/** La variable que contiene informacion con respecto a: utilerias. */
	private static UtilsContingenciaExportarDetalle UTILERIAS;
	
	/**
	 * Nueva instancia utils contingencia exportar detalle.
	 */
	private UtilsContingenciaExportarDetalle() {
		
	}
	
	/**
	 * Obtener el objeto: utilerias.
	 *
	 * @return El objeto: utilerias
	 */
	public static UtilsContingenciaExportarDetalle getUtilerias() {
		if(UTILERIAS == null) {
			UTILERIAS = new UtilsContingenciaExportarDetalle();
		}
		return UTILERIAS;
	}
	
	/**
	 * Obtener el objeto: header excel.
	 *
	 * @param ctx El objeto: ctx
	 * @return El objeto: header excel
	 */
	public List<String> getHeaderExcel(RequestContext ctx) {
		String[] header = new String[] {
				ctx.getMessage("moduloContingencia.nombrearch"),
				ctx.getMessage("moduloContingencia.secuenciaregarch"),       
				ctx.getMessage("moduloContingencia.tipocuentaord"),
				ctx.getMessage("moduloContingencia.tipocuentarec"),
				ctx.getMessage("moduloContingencia.tipocuentarec2"),
				ctx.getMessage("moduloContingencia.cveempresa"),       
				ctx.getMessage("moduloContingencia.cvetransfe"),
				ctx.getMessage("moduloContingencia.cvemedioent"),
				ctx.getMessage("moduloContingencia.cveptovta"),
				ctx.getMessage("moduloContingencia.cveptovtaord"),       
				ctx.getMessage("moduloContingencia.cvedivisa"),
				ctx.getMessage("moduloContingencia.cveintermeord"),
				ctx.getMessage("moduloContingencia.cveintermerec"),
				ctx.getMessage("moduloContingencia.cveusuariocap"),      
				ctx.getMessage("moduloContingencia.cveusuariosol"),
				ctx.getMessage("moduloContingencia.cveoperacion"),
				ctx.getMessage("moduloContingencia.numcuentaord"),
				ctx.getMessage("moduloContingencia.numcuentarec"),      
				ctx.getMessage("moduloContingencia.numcuentarec2"),
				ctx.getMessage("moduloContingencia.referenciamed"),
				ctx.getMessage("moduloContingencia.importe"),
				ctx.getMessage("moduloContingencia.nombreord"),      
				ctx.getMessage("moduloContingencia.nombrerec"),
				ctx.getMessage("moduloContingencia.nombrerec2"),
				ctx.getMessage("moduloContingencia.rfcrec2"),
				ctx.getMessage("moduloContingencia.conceptopago2"),
				ctx.getMessage("moduloContingencia.comentario1"),    
				ctx.getMessage("moduloContingencia.comentario2"),
				ctx.getMessage("moduloContingencia.comentario3"),
				ctx.getMessage("moduloContingencia.comentario4"),
				ctx.getMessage("moduloContingencia.fchcaptura"),  
				ctx.getMessage("moduloContingencia.refetransfer"),
				ctx.getMessage("moduloContingencia.coderror"),
				ctx.getMessage("moduloContingencia.estatusinicial"),
				ctx.getMessage("moduloContingencia.mensaje"), 
				ctx.getMessage("moduloContingencia.direccionip"), 
				ctx.getMessage("moduloContingencia.buccliente"), 
				ctx.getMessage("moduloContingencia.rfcord"), 
				ctx.getMessage("moduloContingencia.rfcrec"), 
				ctx.getMessage("moduloContingencia.importeiva"), 
				ctx.getMessage("moduloContingencia.refenumerica"), 
				ctx.getMessage("moduloContingencia.uetrswift"), 
				ctx.getMessage("moduloContingencia.camposwift1"), 
				ctx.getMessage("moduloContingencia.camposswift2"),
				ctx.getMessage("moduloContingencia.refeadicional")
		};
		return Arrays.asList(header);
	}
	
	/**
	 * Obtener el objeto: body.
	 *
	 * @param listaDetalle El objeto: lista detalle
	 * @return El objeto: body
	 */
	public List<List<Object>> getBody(List<BeanTranMensajeCanales> listaDetalle) {
		List<Object> objListParam = null;
		List<List<Object>> objList = null;
		if (listaDetalle != null && !listaDetalle.isEmpty()) {
			objList = new ArrayList<List<Object>>();
			for (BeanTranMensajeCanales detalle : listaDetalle) {
				objListParam = new ArrayList<Object>();
				objListParam.add(detalle.getBeanDatosGenerales().getNombreArch());
				objListParam.add(detalle.getBeanDatosGenerales().getSecuencia());
				objListParam.add(detalle.getBeanDatosOrdenante().getTipoCuentaOrd());
				objListParam.add(detalle.getBeanDatosReceptor().getTipoCuentaRec());
				objListParam.add(detalle.getBeanDatosReceptor().getTipoCuentaRec2());
				objListParam.add(detalle.getBeanClaves().getCveEmpresa());
				objListParam.add(detalle.getBeanClaves().getCveTransfe());
				objListParam.add(detalle.getBeanClaves().getCveMedioEnt());
				objListParam.add(detalle.getBeanClaves().getCvePtoVta());
				objListParam.add(detalle.getBeanDatosOrdenante().getCvePtoVtaOrd());
				objListParam.add(detalle.getBeanClaves().getCveDivisa());
				objListParam.add(detalle.getBeanDatosOrdenante().getCveIntermeOrd());
				objListParam.add(detalle.getBeanDatosReceptor().getCveIntermeRec());
				objListParam.add(detalle.getBeanClaves().getCveUsuarioCap());
				objListParam.add(detalle.getBeanClaves().getCveUsuarioSol());
				objListParam.add(detalle.getBeanClaves().getCveOperacion());
				objListParam.add(detalle.getBeanDatosOrdenante().getNumCuentaOrd());
				objListParam.add(detalle.getBeanDatosReceptor().getNumCuentaRec());
				objListParam.add(detalle.getBeanDatosReceptor().getNumCuentaRec2());
				objListParam.add(detalle.getBeanDatosGenerales().getReferenciaMed());
				objListParam.add(detalle.getBeanDatosGenerales().getImporte());
				objListParam.add(detalle.getBeanDatosOrdenante().getNombreOrd());
				objListParam.add(detalle.getBeanDatosReceptor().getNombreRec());
				objListParam.add(detalle.getBeanDatosReceptor().getNombreRec2());
				objListParam.add(detalle.getBeanDatosReceptor().getRfcRec2());
				objListParam.add(detalle.getBeanDatosGenerales().getConceptoPago2());
				objListParam.add(detalle.getBeanComentarios().getComentario1());
				objListParam.add(detalle.getBeanComentarios().getComentario2());
				objListParam.add(detalle.getBeanComentarios().getComentario3());
				objListParam.add(detalle.getBeanComentarios().getComentario4());
				objListParam.add(detalle.getBeanDatosGenerales().getFchCapTransfer());
				objListParam.add(detalle.getBeanDatosGenerales().getRefeTransfer());
				objListParam.add(detalle.getBeanDatosOperacion().getCodError());
				objListParam.add(detalle.getBeanDatosOperacion().getEstatusInicial());
				objListParam.add(detalle.getBeanDatosOperacion().getMensaje());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getDireccionIp());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getBucCliente());
				objListParam.add(detalle.getBeanDatosOrdenante().getRfcOrd());
				objListParam.add(detalle.getBeanDatosReceptor().getRfcRec());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getImporteIva());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getRefeNumerica());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getUetrSwift());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getCampoSwift1());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getCampoSwift2());
				objListParam.add(detalle.getBeanDatosGenerales().getBeanDatosGeneralesExt().getRefeAdicional1());
				objList.add(objListParam);
			}
		}
		return objList;
	}
	
}
