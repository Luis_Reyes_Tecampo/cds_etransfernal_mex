/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [WarETransferNal]
 * mx.isban.csa.valtransfer.ws.facade.FacadeValTransfe.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ws.facade;

import javax.ejb.EJBException;
import javax.xml.ws.WebServiceContext;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.beans.ResConsultaDbDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;
import mx.isban.csa.valtransfer.ejb.BOValTransfer;
import mx.isban.csa.valtransfer.ktes.ECatalogoError;
import mx.isban.csa.valtransfer.util.UtilValidador;
import mx.isban.csa.valtransfer.ws.util.RemoteEjbLocator;
import mx.isban.csa.valtransfer.ws.util.UtilWsPeticion;

/**
 * The Class FacadeValTransfe.
 */
public class FacadeValTransfe extends Architech {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6623286967828157499L;

	/**
	 * Monitor.
	 *
	 * @param reqTransfer the req transfer
	 * @param context     the context
	 * @return the res val transfer DTO
	 */
	public ResValTransferDTO monitor(ReqValidacionDTO reqTransfer, WebServiceContext context) {
		// Se crea obj de respuesta para el servicio de cierre.
		ResValTransferDTO responseMonitoreo = new ResValTransferDTO();
		// se obtiene ojb EJB Remoto para la capa logica.
		final BOValTransfer boMonitor = RemoteEjbLocator.getInstance().getBoConsultaIniDia();

		// Se valida la creacion del ejb remoto
		if (UtilWsPeticion.validaEjbRemoto(boMonitor)) {
			// se agrega try catch para evitar alguna caida del servicio
			try {
				// se invoca funcion del ejbRemoto para monitoreo de operaciones
				responseMonitoreo = boMonitor.monitor(getArchitechBean(), reqTransfer,
						UtilWsPeticion.getIpRemota(context));
			} catch (BusinessException be2) {
				// captura errores propagados por medio de la arq agave y lo imprime en logs
				showException(be2);
				responseMonitoreo.setCodeError(be2.getCode());
				responseMonitoreo.setMsgError(be2.getMessage());
			} catch (EJBException ejbe2) {
				// se agrega ejbException para evitar caida del servicio por un error no
				// controlado en la capa logica.
				showException(ejbe2);
				responseMonitoreo.setCodeError(ECatalogoError.VTDEF999.getCodeError());
				responseMonitoreo.setMsgError(ejbe2.getMessage());
			}
		} else {
			// En caso de fallo el servicio envia el msg de error
			responseMonitoreo.setCodeError(ECatalogoError.VTINI999.getCodeError());
			responseMonitoreo.setMsgError(ECatalogoError.VTINI999.getMsgError());
		}
		return responseMonitoreo;
	}

	/**
	 * Consulta.
	 *
	 * @param reqTransfer the req transfer
	 * @param context     the context
	 * @return the res consulta db DTO
	 */
	public ResConsultaDbDTO consulta(ReqConsultaDTO reqTransfer, WebServiceContext context) {
		// Se crea obj de respuesta para el servicio de consultas
		ResConsultaDbDTO responseConsulta = new ResConsultaDbDTO();
		final BOValTransfer boConsultaIniDia = RemoteEjbLocator.getInstance().getBoConsultaIniDia();

		// Se valida la creacion del ejb remoto
		if (UtilWsPeticion.validaEjbRemoto(boConsultaIniDia)) {
			// se agrega try catch para evitar alguna caida del servicio
			try {
				// se invoca funcion del ejbRemoto para CONSULTAS a BD
				UtilValidador.validaParametrosEntrada(reqTransfer);
				reqTransfer.setQuery(reqTransfer.getQuery().toUpperCase());
				responseConsulta = boConsultaIniDia.consulta(getArchitechBean(), reqTransfer,
						UtilWsPeticion.getIpRemota(context));
			} catch (BusinessException be3) {
				// captura errores propagados por medio de la arq agave y lo imprime en logs
				showException(be3);
				responseConsulta.setCodeErrConsulta(be3.getCode());
				responseConsulta.setMsgErrConsulta(be3.getMessage());
			} catch (EJBException ejbe3) {
				// se agrega ejbException para evitar caida del servicio por un error no
				// controlado en la capa logica.
				showException(ejbe3);
				responseConsulta.setCodeErrConsulta(ECatalogoError.VTDEF999.getCodeError());
				responseConsulta.setMsgErrConsulta(ejbe3.getMessage());
			}
		} else {
			// En caso de fallo el servicio envia el msg de error
			responseConsulta.setCodeErrConsulta(ECatalogoError.VTINI999.getCodeError());
			responseConsulta.setMsgErrConsulta(ECatalogoError.VTINI999.getMsgError());
		}
		return responseConsulta;
	}

	/**
	 * Carga querys bd.
	 *
	 * @param context the context
	 * @return the res carga querys DTO
	 */
	public String cargaQuerysBd(WebServiceContext context) {
		// Se crea obj de respuesta para el servicio de consultas
		StringBuilder responseCarga = new StringBuilder();
		final BOValTransfer boConsultaIniDia = RemoteEjbLocator.getInstance().getBoConsultaIniDia();

		// Se valida la creacion del ejb remoto
		if (UtilWsPeticion.validaEjbRemoto(boConsultaIniDia)) {
			// se agrega try catch para evitar alguna caida del servicio
			try {
				// se invoca funcion del ejbRemoto para CONSULTAS a BD
				responseCarga.append(boConsultaIniDia.cargaQuerysBd(getArchitechBean(), UtilWsPeticion.getIpRemota(context)));
			} catch (BusinessException be3) {
				// captura errores propagados por medio de la arq agave y lo imprime en logs
				showException(be3);
				
				responseCarga.append(be3.getCode()).append(" :: ").append(be3.getMessage());
			} catch (EJBException ejbe3) {
				// se agrega ejbException para evitar caida del servicio por un error no
				// controlado en la capa logica.
				showException(ejbe3);
				responseCarga.append(ECatalogoError.VTDEF999.getCodeError()).append(" :: ").append(ejbe3.getMessage());
			}
		} else {
			// En caso de fallo el servicio envia el msg de error
			responseCarga.append(ECatalogoError.VTINI999.getCodeError()).append(" :: ").append(ECatalogoError.VTINI999.getMsgError());
		}
		return responseCarga.toString();
	}

	/**
	 * Consulta programados.
	 *
	 * @param context the context
	 * @return the res programados DTO
	 */
	public ResValTransferDTO consultaProgramados(WebServiceContext context) {
		// Se crea obj de respuesta para el servicio de cierre.
		ResValTransferDTO responseProgramado = new ResValTransferDTO();
		// se obtiene ojb EJB Remoto para la capa logica.
		final BOValTransfer boMonitor = RemoteEjbLocator.getInstance().getBoConsultaIniDia();

		// Se valida la creacion del ejb remoto
		if (UtilWsPeticion.validaEjbRemoto(boMonitor)) {
			// se agrega try catch para evitar alguna caida del servicio
			try {
				// se invoca funcion del ejbRemoto para monitoreo de operaciones
				responseProgramado = boMonitor.consultaProgramados(getArchitechBean());
			} catch (EJBException ejbe2) {
				// se agrega ejbException para evitar caida del servicio por un error no
				// controlado en la capa logica.
				showException(ejbe2);
				responseProgramado.setCodeError(ECatalogoError.VTDEF999.getCodeError());
				responseProgramado.setMsgError(ejbe2.getMessage());
			}
		} else {
			// En caso de fallo el servicio envia el msg de error
			responseProgramado.setCodeError(ECatalogoError.VTINI999.getCodeError());
			responseProgramado.setMsgError(ECatalogoError.VTINI999.getMsgError());
		}
		return responseProgramado;
	}
}
