/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [WarETransferNal]
 * mx.isban.csa.valtransfer.ws.util.RemoteEjbLocator.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ws.util;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mx.isban.csa.valtransfer.ejb.BOValTransfer;

/**
 * The Class RemoteEjbLocator.
 */
public class RemoteEjbLocator {

	/** The Constant SPRING_CONTEXT. */
	private static final String SPRING_CONTEXT = "/WEB-INF/DispatcherValTransfer-servlet.xml";

	/** The instance. */
	private static RemoteEjbLocator instance;

	/** The bean factory. */
	private BeanFactory beanFactory;

	/** The bo val transfer. */
	private BOValTransfer boValTransfer;

	/**
	 * Instantiates a new remote ejb locator.
	 */
	public RemoteEjbLocator() {
		this.beanFactory = new ClassPathXmlApplicationContext(SPRING_CONTEXT);
		boValTransfer = (BOValTransfer) beanFactory.getBean("boValTransfer");
	}

	/**
	 * Gets the single instance of RemoteEjbLocator.
	 *
	 * @return single instance of RemoteEjbLocator
	 */
	public static RemoteEjbLocator getInstance() {
		if (instance == null) {
			synchronized (RemoteEjbLocator.class) {
				if (instance == null) {
					instance = new RemoteEjbLocator();
				}
			}
		}
		return instance;
	}

	/**
	 * Gets the bo consulta ini dia.
	 *
	 * @return the bo consulta ini dia
	 */
	public BOValTransfer getBoConsultaIniDia() {
		return boValTransfer;
	}
}
