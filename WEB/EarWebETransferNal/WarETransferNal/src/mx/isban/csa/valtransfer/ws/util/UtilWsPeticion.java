/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [WarETransferNal]
 * mx.isban.csa.valtransfer.ws.util.UtilWsPeticion.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ws.util;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.csa.valtransfer.ejb.BOValTransfer;

/**
 * The Class UtilWsPeticion.
 */
public final class UtilWsPeticion {

	/**
	 * Instantiates a new util ws peticion.
	 */
	private UtilWsPeticion() {
		throw new UnsupportedOperationException("No instanciar");
	}
	
	/**
	 * Valida ejb remoto.
	 *
	 * @param boConsultaIniDia the bo consulta ini dia
	 * @return true, if successful
	 */
	public static boolean validaEjbRemoto(BOValTransfer boConsultaIniDia) {
		boolean validacion = true;
		// se valida la creacion del ejbRemoto
		if (boConsultaIniDia == null) {
			// bandera de validacion se coloca el false para evitar un nullpointer al
			// acceder invocar funcionalidad del ejbRemoto
			validacion = false;
		}
		return validacion;
	}

	/**
	 * Gets the ip remota.
	 *
	 * @param context the context
	 * @return the ip remota
	 */
	public static String getIpRemota(WebServiceContext context) {
		// se genera obj httpreq para obtener el origen de la peticion
		final HttpServletRequest req = (HttpServletRequest) context.getMessageContext()
				.get(MessageContext.SERVLET_REQUEST);
		return req.getRemoteAddr();
	}
}
