/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [WarETransferNal]
 * mx.isban.csa.valtransfer.ws.WsValTransfe.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceContext;

import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.beans.ResConsultaDbDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;
import mx.isban.csa.valtransfer.ws.facade.FacadeValTransfe;

/**
 * The Class WsValTransfe.
 */
@WebService(targetNamespace = "http://ws.csa.isban.com/", serviceName = "ValTransferService", portName = "ValTransferPort")
@SOAPBinding(style = javax.jws.soap.SOAPBinding.Style.RPC)
public class WsValTransfe {

	/** The context. */
	@Resource
	private WebServiceContext context;

	/**
	 * Monitoreo.
	 *
	 * @param peticion the peticion
	 * @return the res val transfer DTO
	 */
	@WebMethod(operationName = "monitoreo")
	@WebResult(name = "ResValTransferDTO")
	public ResValTransferDTO monitoreo(@WebParam(name = "peticion") ReqValidacionDTO peticion) {
		//Se crea fachada para la peticion a la capa EJB 
		//esto para evitar algun posible fallo al crear el WSDL service 
		//suele pasar cuando algun componente requerido para el servico no se encuentra disponible durante el deploy 
		//De esta manera el servicio solo dependera de la existencia del componente FacadeValTRansfer para su correcta creacion. 
		FacadeValTransfe facade = new FacadeValTransfe();
		return facade.monitor(peticion,context);
	}

	/**
	 * Consulta.
	 *
	 * @param peticion the peticion
	 * @return the res consulta db DTO
	 */
	@WebMethod(operationName = "consulta")
	@WebResult(name = "ResValTransferDTO")
	public ResConsultaDbDTO consulta(@WebParam(name = "peticion") ReqConsultaDTO peticion) {
		FacadeValTransfe facade = new FacadeValTransfe();
		return facade.consulta(peticion, context);
	}
	
	/**
	 * Carga querys bd.
	 *
	 * @return the res carga querys DTO
	 */
	@WebMethod(operationName = "cargaQuerysBd")
	@WebResult(name = "ResValTransferDTO")
	public String cargaQuerysBd() {
		FacadeValTransfe facade = new FacadeValTransfe();
		return facade.cargaQuerysBd(context);
	}
	
	/**
	 * Programados.
	 *
	 * @return the res programados DTO
	 */
	@WebMethod(operationName = "programados")
	@WebResult(name = "ResProgramadosDTO")
	public ResValTransferDTO programados() {
		FacadeValTransfe facade = new FacadeValTransfe();
		return facade.consultaProgramados(context);
	}
}
