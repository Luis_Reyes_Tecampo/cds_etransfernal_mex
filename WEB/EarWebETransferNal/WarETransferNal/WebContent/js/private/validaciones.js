/**
 * SCRIPT para validaciones de campos dentro de los formularios 
 */
$(function() {
    // Eventos KEYPRESS de los objetos
    // Validamos que solo se ingresen numeros
    $('.onlyNumero').on('keypress', function(e) {
        validarObj('^([0-9]+)$', e.key, e.keyCode, this);
    });
    // Procedimiento para validarObj cuando sean datos Decimales al ingresarlos
    $('.onlyDecimal').on('keypress', function(e) {
        validarObj('^([0-9.])$', e.key, e.keyCode, this);
    });
    // Validamos el objeto al cambiar de focus de objeto decimal
    $('.onlyDecimal').on('blur', function(e) {
        validarObj (/^(\d*\.\d+|\d+)$/, $(this).val(), e.keyCode, this);
    });
    // Procedimiento para validarObj solo letras
    $('.onlyLetras').on('keypress', function(e) {
        validarObj('^([a-zA-Z ]+)$', e.key, e.keyCode, this);
    });
    // Procedimiento para validarObj solo letras
    $('.letrasNum').on('keypress', function(e) {
        validarObj('^([a-zA-Z0-9 ]+)$', e.key, e.keyCode, this);
    });
    
    // Eventos CHANGE de los objetos
    $('.onlyNumero').on('change', function() {
        changeValObj('^([0-9]+)$', this);
    });
    // Procedimiento para validarObj cuando sean datos Decimales al ingresarlos
    $('.onlyDecimal').on('change', function() {
        changeValObj('^([0-9.])$', this);
    });
 // Procedimiento para validarObj solo letras
    $('.onlyLetras').on('change', function() {
        changeValObj('^([a-zA-Z ]+)$', this);
    });
 // Procedimiento para validarObj solo letras
    $('.letrasNum').on('change', function() {
        changeValObj('^([a-zA-Z0-9 ]+)$', this);
    });
    
    // Procedimiento para validarObj cuando sean datos Decimales al ingresarlos
    $('.onlyDecimalNEgativos').on('keypress', function(e) {
        validarObj('^([0-9.-])$', e.key, e.keyCode, this);
    });
    // Validamos el objeto al cambiar de focus de objeto decimal
    $('.onlyDecimalNEgativos').on('blur', function(e) {
        validarObj (/^-?\d*(\.\d+)?$/, $(this).val(), e.keyCode, this);
    });
    // Procedimiento para validarObj cuando sean datos Decimales al ingresarlos
    $('.onlyDecimalNEgativos').on('change', function() {
        changeValObj('^([0-9.-])$', this);
    });
 
});




/**
 * Procedimiento para validarObj los campos dentro del formulario
 * @param reg
 * @param valor
 * @param valTecla
 * @param obj
 * @returns
 */
function validarObj (reg, valor, valTecla, obj) {
    // Areglo de datos para los valores de teclas validas por el modulo
    var lstTeclas = [13, 32];
    // Verificamos su la funcion existen dentro del arreglo
    var pos = $.inArray(valTecla, lstTeclas);
    var reg = new RegExp ( reg );
    var resFnc = true;
    // Validamos el valor de la tecla
    if( pos !== -1 ) {
        return;
    }
    if( !reg.test( valor )) {
        resFnc = false;
        $(obj).addClass('error-obj');
    }
    return resFnc;
}


/**
 * Evaluamos el contenido para eliminar el mensaje de error
 * @param reg
 * @param obj
 * @returns
 */
function changeValObj(reg, obj) {
    var reg = new RegExp ( reg );
    if( reg.test( $(obj).val() )) {
        $(obj).removeClass('error-obj');
    }
}


/**
 * Procedimiento para validar los campos dentro de un formulario
 * @param nomFrm
 * @returns
 */
function validarFrm( nomFrm ) {
    var obj = '#' + nomFrm;
    var resFnc = true;
    var datSend = {};
    var mensajes = '';
    // Recorremos los objetos del formulario para validar sus campos
    $(obj).find('.objFrm').each(function() {
        if($(this).hasClass('error-obj')) {
            $(this).removeClass('error-obj');
        }
        // Obtenemos la clase que contiene el objeto
        var allClases = $(this).attr('class');
        // Separamos las clases y obtenemos la lista de las mismas
        var lstClases = allClases.split(' ');
        var valorCampo = '';
        // Verificamos que el campo sea un textbox, select
        if( $(this).is(':text') || $(this).is('select') ) {
            valorCampo = $(this).val();
        }
        // Conocemos el objeto 
        var obj = this;
        // Recorremos las clases para validar sus datos
        $.each(lstClases, function( index, value ) {
            var datRecibed = lstFunciones( value, valorCampo, obj );
            if( datRecibed.seguir === false ) {
                resFnc = false;
                mensajes += datRecibed.mensaje +'#';
            }
        });
    });
    // Seteamos los datos dentro del arreglo
    datSend.seguir = resFnc;
    datSend.mensajes = mensajes;
    // Retornamos el arreglo de datos
    return datSend;
}


/**
 * Procedimiento para validacion de valores dentro de los campos
 * @param fncBuscar
 * @param valor
 * @param objeto
 * @returns
 */
function lstFunciones( fncBuscar, valor, objeto ) {
    var msgValidacion = '';
    // Variable de respuesta de las validaciones
    var resFnc = true;
    // Arreglo de datos que devolveremos al usaurio
    var datSend = {};
    // Areglo de datos para las funciones validas por el modulo
    var lstFuncion = ['required', 'onlyNumero', 'onlyLetras', 'onlyDecimal', 'letrasNum', 'onlyDecimalNEgativos'];
    // Verificamos su la funcion existen dentro del arreglo
    var pos = $.inArray(fncBuscar, lstFuncion);
    
    // Verificamos si existe la funcion dentro del procedimiento
    if( pos !== -1 && valor!=='') {
        if(fncBuscar === 'onlyNumero') {
            msgValidacion = validaNumeros(valor, objeto);
        }
        if(fncBuscar === 'onlyLetras') {
            msgValidacion = validaLetras(valor, objeto);
        }
        if(fncBuscar === 'onlyDecimal') {
            msgValidacion = validaDecimales(valor, objeto);
        }
        if(fncBuscar === 'letrasNum') {
            msgValidacion = validaLetrasNum(valor, objeto);
        }
        if(fncBuscar === 'onlyDecimalNEgativos') {
            msgValidacion = validaDecimalesNegativos(valor, objeto);
        }
        
    } else if( pos !== -1 && fncBuscar === 'required') {
        msgValidacion = requeridoValor(valor, objeto);
    }
    // Verificamos si llamaremos a la funcion
    if( msgValidacion !=='' ) {
        resFnc = false;
    }
    // Seteamos los datos
    datSend.mensaje = msgValidacion;
    datSend.seguir = resFnc;
    // Devolvemos una arreglo de datos
    return datSend;
}

/**
 * Procedimiento para validar campos con valores indispensables por llenar
 * @param valor
 * @param objeto
 * @returns
 */
function requeridoValor(valor, objeto) {
    var msgValidacion = '';
    if( $.trim(valor) === '') {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo '+ objeto.id.replace("param", "") + ', es requerido.';
    }
    return msgValidacion;
}

/**
 * Procedimiento para validar campos con numeros
 * @param valor
 * @param objeto
 * @returns
 */
function validaNumeros(valor, objeto) {
    var msgValidacion = '';
    reg = new RegExp ( /^([0-9]+)$/ );
    resFnc = validarObj(reg, valor, objeto);
    if( resFnc === false ) {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo ' +  objeto.id.replace("param", "") + ', solo permite n\u00fameros enteros.';
    }
    return msgValidacion;
}

/**
 * Procedimiento para validar campos con Letras y espacios
 * @param valor
 * @param objeto
 * @returns
 */
function validaLetras(valor, objeto) {
    var msgValidacion = '';
    reg = new RegExp ( /^([a-zA-Z ]+)$/ );
    resFnc = validarObj(reg, valor, objeto);
    if( resFnc === false ) {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo ' +  objeto.id.replace("param", "") + ', solo perminte letras.';
    }
    return msgValidacion;
}


/**
 * Procedimiento para validar campos con valores decimales
 * @param valor
 * @param objeto
 * @returns
 */
function validaDecimales(valor, objeto) {
    var msgValidacion = '';
    reg = new RegExp ( /^(\d*\.\d+|\d+)$/ );
    resFnc = validarObj(reg, valor, objeto);
    if( resFnc === false ) {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo ' +  objeto.id.replace("param", "") + ', solo permite n\u00fameros decimales.';
    }
    return msgValidacion;
}

/**
 * Procedimiento para validar campos con valores decimales
 * @param valor
 * @param objeto
 * @returns
 */ ///'^([0-9.]\d*(\.\d+)?$
function validaDecimalesNegativos(valor, objeto) {
    var msgValidacion = '';
    reg = new RegExp ( /^-?\d*(\.\d+)?$/ );
    resFnc = validarObj(reg, valor, objeto);
    if( resFnc === false ) {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo ' +  objeto.id.replace("param", "") + ', solo permite n\u00fameros decimales.';
    }
    return msgValidacion;
}


/**
 * Procedimiento para la validacion de campos numericos y letras
 * @param valor
 * @param objeto
 * @returns
 */
function validaLetrasNum(valor, objeto) {
    var msgValidacion = '';
    reg = new RegExp ( /^([a-zA-Z0-9 ]+)$/ );
    resFnc = validarObj(reg, valor, objeto);
    if( resFnc === false ) {
        $(objeto).addClass('error-obj');
        msgValidacion = 'El campo ' +  objeto.id.replace("param", "") + ', solo permite letras y n\u00fameros.';
    }
    return msgValidacion;
}