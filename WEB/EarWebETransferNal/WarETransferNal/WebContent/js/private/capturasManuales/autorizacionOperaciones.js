var formulario = "idForm";
autorizarOperaciones = {
        inicioAutorizarOpe:function(){
            createCalendarGuion('inputFechaCaptura','cal1');

            $('#idBuscar').click(function(){
                document.body.style.cursor = "wait";
                var form = document.getElementById("idForm");
                form.action="consultarOperacionesPendientes.do";
                $('#accion').val('ACT');
                $('#pagina').val(1);
                $('#template').val($('#inputTemplate').val());
                $('#fechaCaptura').val($('#inputFechaCaptura').val());
                form.submit();
            });

            $('#idApartar').click(function(){
                var form = document.getElementById("idForm");
                form.action="apartar.do";
                document.body.style.cursor = "wait";
                $('#template').val($('#inputTemplate').val());
                $('#FechaCaptura').val($('#inputFechaCaptura').val());
                $('#accion').val('ACT');
                form.submit();
            });

            $('#idDespartar').click(function(){
                var form = document.getElementById("idForm");
                document.body.style.cursor = "wait";
                form.action="desapartar.do";
                $('#FechaCaptura').val($('#inputFechaCaptura').val());
                $('#accion').val('ACT');
                $('#template').val($('#inputTemplate').val());
                form.submit();
            });

            $('#idAutorizar').click(function(){
                document.body.style.cursor = "wait";
                $('#accion').val('ACT');
                $('#template').val($('#inputTemplate').val());
                $('#FechaCaptura').val($('#inputFechaCaptura').val());
                $('#idForm').attr('action','autorizarOperacion.do');
                $('#idForm').submit();
            });

            $('#idReparar').click(function(){
                $('#accion').val('ACT');
                $('#template').val($('#inputTemplate').val());
                $('#FechaCaptura').val($('#inputFechaCaptura').val());
                document.body.style.cursor = "wait";
                $('#idForm').attr('action','repararOperacion.do');
                $('#idForm').submit();
            });

            $('#idCancelar').click(function(){
                $('#template').val($('#inputTemplate').val());
                document.body.style.cursor = "wait";
                $('#FechaCaptura').val($('#inputFechaCaptura').val());
                $('#accion').val('ACT');
                $('#idForm').attr('action','cancelarOPeracion.do');
                $('#idForm').submit();
            });
        },

        validadRegistrosSeleccionados:function(){
        },

        despliegaDetalle:function(numFolio, cveTemplate){
            $('#template').val($('#inputTemplate').val());
            $('#FechaCaptura').val($('#inputFechaCaptura').val());
            $('#cveTemplate').val(cveTemplate);
            $('#folio').val(numFolio);
            $('#accion').val('ACT');
            $('#idForm').attr('action','mostrarDetalleOperacionPendiente.do');
            $('#idForm').submit();
        }
};

$(document).ready(autorizarOperaciones.inicioAutorizarOpe);