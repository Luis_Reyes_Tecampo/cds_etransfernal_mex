/** Declaracionde las variables con las funciones correspondientes **/
var capturaCentralSPID = {
        regNum : new RegExp("^([0-9]{1,40})$"),
        regExpMontoPago : new RegExp("^([0-9]{1,19}[/.]{0,1}[0-9]{0,2})$"),
        /** Inicializacion de las funciones **/
        init:function(){
            document.body.style.cursor = "default";
            /** Evento buscar **/
            $('#idBuscar').click(function(){
                var comboTemplate = document.getElementById("selectListTemplates");
                var selectedIndex = comboTemplate.selectedIndex;
                if (selectedIndex>0){
                    $('#step').val("consulta");
                    hacerSubmit('idForm','consultaTemplate.do');
                }else{
                    jAlert(mensajes["CMCO021V"],
                            mensajes["aplicacion"],
                            'CMCO021V','');
                }
            });
            /** Evento para generar el campo uetr swift**/
            $('#generarUETR').click(function() {
                var codigoNuevo = null;
                $.ajax({
                    type : "POST",
                    cache : false,
                    async : false,
                    url : "/eTransferNal/capturasManuales/generaUETR.do",
                    success : function(response) {
                        codigoNuevo = response;
                    },
                    error : function(e) {
                        codigoNuevo = "";
                        /**console.log("Error " + e);*/
                    }
                });
                $('#paramUetr_swift').val(codigoNuevo);
            });
            
            /** Evento del boton regresar **/
            $('#idRegresar').click(function(){
                if (document.getElementById('idFol')) {
                    hacerSubmit('idForm','muestraCapturaCentral.do');
                } else {
                    hacerSubmit('idForm','capturasManualesInit.do');
                }
            });
            /** Evento para limpiar **/
            $('#idLimpiar').click(function(){
                if (document.getElementById('idFol')) {
                    hacerSubmit('idForm','consultaTemplate.do');
                } else {
                    hacerSubmit('idForm','muestraCapturaCentral.do');
                }
            });
            
            /** Inicializando las funciones **/
            capturaCentralSPID.init2();
        },
        init2:function(){
            /** Evento del boton alta **/
            $('#idAlta').click(function(){
                capturaCentralSPID.setValues();
                hacerSubmit('idForm','altaCapturaManualSPID.do');
            });
            /** Evento para validar los filtros **/
            $('#paramImporte_dls').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramImporte_dls', capturaCentralSPID.regExpMontoPago, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramFolio_pago').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramFolio_pago', capturaCentralSPID.regNum, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramFolio_paquete').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramFolio_paquete', capturaCentralSPID.regNum, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramImporte_abono').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramImporte_abono', capturaCentralSPID.regExpMontoPago, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramImporte_cargo').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramImporte_cargo', capturaCentralSPID.regExpMontoPago, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramReferencia_med').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramReferencia_med', capturaCentralSPID.regNum, 0, e);
            });
            /** Evento para validar los filtros **/
            $('#paramTipo_cambio').keypress(function(e){
                capturaCentralSPID.soloNumeros('#paramTipo_cambio', capturaCentralSPID.regExpMontoPago, 0, e);
            });
            
        },
        /** Validaciones **/
        soloNumeros:function(id, expReg, long, e){
            var k;
            if (document.all) {
                k = e.keyCode;
            } else {
                k = e.which;
            }
            if(k !== 8){
                var cad;
                capturaCentralSPID.cancel2(e);
                cad = $(id).val()+String.fromCharCode(k);
                if(!(cad ==="") && !expReg.test(cad) && k !== long){
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        },
        /** Funcion para setear os valores y prepararlos para su envio **/
        setValues:function(){
            $('#codPostalOrd').val($('#paramCod_postal_ord').val());
            $('#comentario1').val($('#paramComentario1').val());
            $('#comentario2').val($('#paramComentario2').val());
            $('#comentario3').val($('#paramComentario3').val());
            $('#comentario4').val($('#paramComentario4').val());
            $('#conceptoPago').val($('#paramConcepto_pago').val());
            $('#conceptoPago2').val($('#paramConcepto_pago2').val());
            $('#cveDivisaOrd').val($('#paramCve_divisa_ord').val());
            $('#cveDivisaRec').val($('#paramCve_divisa_rec').val());
            $('#cveEmpresa').val($('#paramCve_empresa').val());
            $('#cveIntermeOrd').val($('#paramCve_interme_ord').val());
            $('#cveIntermeRec').val($('#paramCve_interme_rec').val());
            $('#cveMedioEnt').val($('#paramCve_medio_ent').val());
            $('#cveOperacion').val($('#paramCve_operacion').val());
            $('#cvePtoVta').val($('#paramCve_pto_vta').val());
            $('#cvePtoVtaOrd').val($('#paramCve_pto_vta_ord').val());
            $('#cvePtoVtaRec').val($('#paramCve_pto_vta_rec').val());
            $('#cveRastreo').val($('#paramCve_rastreo').val());
            $('#cveTransfe').val($('#paramCve_transfe').val());
            $('#cveUsuariocap').val($('#paramCve_usuario_cap').val());
            $('#cveUsuarioSol').val($('#paramCve_usuario_sol').val());
            $('#direccionIp').val($('#paramDireccion_ip').val());
            $('#domicilioOrd').val($('#paramDomicilio_ord').val());
            $('#fchConstitOrd').val($('#paramFch_constit_ord').val());
            $('#fchInstrucPago').val($('#paramFch_instruc_pago').val());
            $('#folioPago').val($('#paramFolio_pago').val());
            $('#folioPaquete').val($('#paramFolio_paquete').val());
            $('#horaInstrucPago').val($('#paramHora_instruc_pago').val());
            $('#importeAbono').val($('#paramImporte_abono').val());
            $('#importeCargo').val($('#paramImporte_cargo').val());
            $('#motivoDevol').val($('#paramMotivo_devol').val());
            $('#nombreOrd').val($('#paramNombre_ord').val());
            $('#nombreRec').val($('#paramNombre_rec').val());
            $('#nombreRec2').val($('#paramNombre_rec2').val());
            $('#numCuentaOrd').val($('#paramNum_cuenta_ord').val());
            $('#numCuentaRec').val($('#paramNum_cuenta_rec').val());
            $('#refeNumerica').val($('#paramRefe_numerica').val());
            $('#referenciaMed').val($('#paramReferencia_med').val());
            $('#rfcOrd').val($('#paramRfc_ord').val());
            $('#rfcrec').val($('#paramRfc_rec').val());
            $('#rfcrec2').val($('#paramRfc_rec2').val());
            $('#tipoCambio').val($('#paramTipo_cambio').val());
            $('#tipoCuentaOrd').val($('#paramTipo_cuenta_ord').val());
            $('#tipoCuentaRec').val($('#paramTipo_cuenta_rec').val());
            $('#ciudadBcoRec').val($('#paramCiudad_bco_rec').val());
            $('#cveAba').val($('#paramCve_aba').val());
            $('#cvePaisBcoRec').val($('#paramCve_pais_bco_rec').val());
            $('#formaLiq').val($('#paramForma_liq').val());
            $('#importeDls').val($('#paramImporte_dls').val());
            $('#nombreBcoRec').val($('#paramNombre_bco_rec').val());
            $('#numCuentaRec2').val($('#paramNum_cuenta_rec2').val());
            $('#plazaBanxico').val($('#paramPlaza_banxico').val());
            $('#sucursalBcoRec').val($('#paramSucursal_bco_rec').val());
            $('#tipoCuentaRec2').val($('#paramTipo_cuenta_rec2').val());
            /** Se gregan los campos nuevos **/
            $('#bucCliente').val($('#paramBuc_cliente').val());
            $('#uetrSwift').val($('#paramUetr_swift').val());
            $('#campoSwift1').val($('#paramCampo_swift1').val());
            $('#campoSwift2').val($('#paramCampo_swift2').val());
            $('#importeMiva').val($('#paramImporte_iva').val());
            $('#dirIpGenTran').val($('#paramDir_ip_gen_tran').val());
            $('#dirIpFirma').val($('#paramDir_ip_firma').val());
            $('#refeAdicional1').val($('#paramRefe_adicional1').val());
        },
        cancel2:function(e){
            var k;
            if (document.all) {
                k = e.keyCode;
            } else {
                k = e.which;
            }
            if(!((k===190 ) || (k>=46 && k<=57)) ){
                detener(e);
            }else if(k===107|| k===109 || k=== 111 || k===18){
                detener(e);
            }
        }
};
/** Se inicializan las funciones **/
$(document).ready(capturaCentralSPID.init);

/** funcion que detiene los procesos **/
function detener(e) {
    e.preventDefault();
    e.stopPropagation();
}
/** Funcion para enviar **/
function hacerSubmit(formName,action){
    document.body.style.cursor = "wait";
    var form = document.getElementById(formName);
    form.action=action;
    form.submit();
}
/** Funcion que valida el template seleccionado y lo muestra **/
function changeSelectedTemplate(){
    var comboTemplate = document.getElementById("selectListTemplates");
    var selectedIndex = comboTemplate.selectedIndex;
    if (selectedIndex>0){
        selectedIndex = selectedIndex-1;
        if($('#idLayout'+selectedIndex).length !== 0) {
            $('#layout').val($('#idLayout'+selectedIndex).val());
            $('#template').val(comboTemplate.options[comboTemplate.selectedIndex].value);
        }
    }else{
        $('#layout').val('');
        $('#template').val('');
    }
}
    
/** M/023776 Capturas Manuales - [VSF] Refrescar el tiempo de sesion */
function refreshSession(){
    $.ajax({
        type : "GET",
        cache : false,
        url : "/eTransferNal/capturasManuales/refreshSession.do", 
            error : function() {
                /** console.log("[Capturas Manuales] - Se genero un error al actualizar la sesion: " + e); **/
            }
    });  
}