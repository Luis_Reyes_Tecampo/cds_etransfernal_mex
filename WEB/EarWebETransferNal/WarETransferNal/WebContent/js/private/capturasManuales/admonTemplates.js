/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: admonTemplates.js
 * 
 * Control de versiones:
 * Version  Date/Hour     By        Company    Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   Tue Mar 27 14:00:00 GTM 2017    Hermenegildo Fernandez Santos     Nexsol    Creacion 
 * */
consultaTemplates ={
        init:function(){
            /**
             * Evento de click en el boton de BuscarTemplate
             */
            $('#idBuscarTemplate').click(function(){
                //Campos hidden para enviar
                $('#filtroEstado').val($('#selectEstatus').val());

                $('#idForm').attr('action','consultarTemplatesBuscar.do');
                $('#idForm').submit();
            });

            /**
             * Evento de click en el boton de agregar template
             */
            $('#idAltaTemplate').click(function(){
                //Campos hidden para enviar
                $('#filtroEstado').val($('#selectEstatus').val());

                $('#idForm').attr('action','muestraAltaTemplates.do');
                $('#idForm').submit();
            });
            
            /**
             * Evento de click en agregar a usuarios autorizados
             */
            $('#idAddToRight').click(function(){
                //Agregar a la derecha
                $('#listaUsuariosSelect option:selected').each( function(){
                    $('#listaAutorizan').append($("<option></option>").attr("value", this.value).text(this.value));
                });
                //Eliminar de la fuente
                $('#listaUsuariosSelect option:selected').remove();
                $('#countlistaUsuariosAutorizan').attr("value", document.getElementById("listaAutorizan").options.length);
                $('#countlistaUsuarios').attr("value", document.getElementById("listaUsuarios").options.length);
            });

            /**
             * Evento de click en agregar a usuarios disponibles
             */
            $('#idAddToLeft').click(function(){
                //Agregar a la derecha
                $('#listaAutorizan option:selected').each( function(){
                    $('#listaUsuariosSelect').append($("<option></option>").attr("value", this.value).text(this.value));
                });
                //Eliminar de la fuente
                $('#listaAutorizan option:selected').remove();
                $('#countlistaUsuariosAutorizan').attr("value", document.getElementById("listaAutorizan").options.length);
                $('#countlistaUsuarios').attr("value", document.getElementById("listaUsuarios").options.length);
            });


            /**
             * Evento click de limpiar campos del template: ID_LAYOUT, Selected Layout, Usuarios, Campos
             */
            $('#idLimpiarTemplate').click(function(){
                $('#tipoSolicitud').val('0');

                //View: ID_LAYOUT, selectedLayout.
                $('#inputTemplate').val('');
                $('#selectLayout').val('');
                //Request
                $('#idTemplate').val('');
                $('#selectedLayout').val('');

                $('#idForm').attr('action','muestraAltaTemplates.do');
                $('#idForm').submit();
            });

            /**
             * Evento click de regresar a la pantalla de consulta
             */
            $('#idRegresarTemplate').click(function(){
                $('#idForm').attr('action','consultarTemplatesBuscar.do');
                $('#idForm').submit();

            });
            consultaTemplates.init2();
            consultaTemplates.init3();
            consultaTemplates.init4();
        },
        init2:function(){

            /**
             * Evento click de eliminar un conjunto de templates
             */
            $('#idEliminarTemplate').click(function(){
                //Verificar si existe un componente seleccionado.
                if(verificarSeleccion()>0){
                    $('#idForm').attr('action','eliminarTemplates.do');
                    $('#idForm').submit();
                }else{
                    jAlert(mensajes["CMMT007V"],
                            mensajes["aplicacion"],
                            'CMMT007V','');
                }

            });

            /**
             * Evento click que invoca la modificacion/detalles del template
             */
            $('.editTemplateAction').click(function(){
                //Establecer el modo a edicion y el id del template a editar
                $('#idTemplate').val($(this).attr('id'));
                $('#modoEdicion').val(true);

                $('#idForm').attr('action','muestraEdicionTemplate.do');
                $('#idForm').submit();

            });

            /**
             * Metodo a traves del cual se limpia el template de edicion actual
             */
            $('#idLimpiarEditTemplate').click(function(){
                $('#modoEdicion').val(true);

                $('#idForm').attr('action','muestraEdicionTemplate.do');
                $('#idForm').submit();
            });


            /**
             * Funcion que permite verificar si se han seleccionado templates
             */
            function verificarSeleccion(){
                var count=0;

                $(".groupSeleccionTemplate").each( function (){
                    if(this.checked){
                        count++;
                    }
                });

                return count;
            }
        },
        init3:function(){
            /**
             * Evento click que invoca el guardado de la edicion de un template
             */
            $('#idActualizarTemplate').click(function(){
                //Debido a que el ID y el LAYOUT no pueden cambiarse en el Load ya estan definidos

                //Propagar el estado del template
                $('#estatus').val($('#selectEstado').val());

                //Respaldar las listas de usuarios para que sean enviadas como input hyde
                consultaTemplates.respaldarListaLayouts();
                consultaTemplates.respaldarListaUsuarios();
                consultaTemplates.respaldarListaUsuariosAutorizados();

                $('#idForm').attr('action', 'actualizarTemplate.do');
                $('#idForm').submit();
            });
        },
        init4:function(){
             /**
             * Evento de click en el boton de guardar termplate
             */
            $('#idGuardarTemplate').click( function(){
                //Validar campos seleccionados.
                if(consultaTemplates.validarVacio($('#inputTemplate').val())
                        || consultaTemplates.validarVacio($('#selectLayout').val()) ) {

                    jAlert(mensajes["CMMT002V"],
                            mensajes["aplicacion"],
                            'CMMT002V',
                    '');
                }else{
                    $('#idTemplate').val($('#inputTemplate').val());
                    $('#selectedLayout').val($('#selectLayout').val());

                    //Respaldar las listas de usuarios para que sean enviadas como input hyde
                    consultaTemplates.respaldarListaLayouts();
                    consultaTemplates.respaldarListaUsuarios();
                    consultaTemplates.respaldarListaUsuariosAutorizados();

                    $('#idForm').attr('action', 'guardarTemplate.do');
                    $('#idForm').submit();
                }
            });
            
            /**
             * Evento de change seleccion del layout
             */
            $('#selectLayout').change(function(){
                //Si se ha seleccionado un template valido
                if(this.value!==""){
                    $('#tipoSolicitud').val('1');
                    $('#idTemplate').val($('#inputTemplate').val());
                    $('#selectedLayout').val($('#selectLayout').val());
                    $("#selectLayout").prop("disabled", true);
                    $('#idGuardarTemplate').off('click');
                    //Respaldar las listas de usuarios para que sean enviadas como input hyde
                    consultaTemplates.respaldarListaLayouts();
                    consultaTemplates.respaldarListaUsuarios();
                    consultaTemplates.respaldarListaUsuariosAutorizados();

                    $('#idForm').attr('action','muestraAltaTemplates.do');
                    $('#idForm').submit();
                }

            });
            
            /**
             * Evento click que invoca la clonacion del template
             */
            $('#idClonarTemplate').click(function(){
                var count=0;
                var id = "";
                $(".groupSeleccionTemplate").each( function (i,obj){
                    if(obj.checked){
                        count++;
                        id = $('#templateName'+i).val();
                    }
                });
                if(count === 1){
                    $('#idTemplate').val(id);
                    $('#idForm').attr('action','clonadoTemplate.do');
                    $('#idForm').submit();   
                }else{
                    jAlert(mensajes["CMMT008V"],
                            mensajes["aplicacion"],
                            'CMMT008V','');
                }
            });
        },
        /**
         * Funcion que permite validar una entrada vacia
         */
        validarVacio:function(input){
            if(input!==null){
                var str = input.replace(/^\s+|\s+$/gm,'');
                return  str.length === 0;
            }

            return true;
        },
        
        /**
         * Metodo que respalda la lista de layouts disponibles para utilizarse
         */
        respaldarListaLayouts:function(){
            var indexAdd=0;
            $('#selectLayout option').each( function(){
                if(!consultaTemplates.validarVacio($(this).val())){
                    $('<input/>').attr({type: 'hidden' , name: 'listaLayouts['+indexAdd+']', value: $(this).val()}).appendTo('#idForm');
                    indexAdd+=1;
                }
            });
        },

        /**
         * Metodo que respalda la lista de usuarios disponibles para autorizar
         */
        respaldarListaUsuarios:function(){
            $('#listaUsuariosSelect option').each( function(index){
                $('<input/>').attr({type: 'hidden' , name: 'listaUsuarios['+index+']', value: $(this).val()}).appendTo('#idForm');
            });
        },

        /**
         * Metodo que respalda la lista de usuarios autorizados
         */
        respaldarListaUsuariosAutorizados:function(){
            $('#listaAutorizan option').each( function(index){
                $('<input/>').attr({type: 'hidden' , name: 'template.usuariosAutorizan['+index+'].usrAutoriza', value: $(this).val()}).appendTo('#idForm');
            });
        }
};

function comboChanged(idInput){
    var selectedValue = $("#combo"+idInput+" option:selected").val();
    $('#'+idInput).val(selectedValue);
}


$(document).ready(consultaTemplates.init);