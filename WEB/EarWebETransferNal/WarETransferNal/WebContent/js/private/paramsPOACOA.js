/** Script que contiene las funciones necesarias para darle funcionalidad a la pantalla de parametros POA/COA y POA/COA SPID **/

/** Se declaran variables que se utilizaran para mostrar en las ventanas modales **/
var tituloModulo = "Par\u00E1metros POA/COA";
var moduloSpid = " SPID";

/**
 * Funcionalidad carga archivos respuesta
 **/
var paramPOACOA = {
    init:function(){
    
        /** Evento para el boton Generar Devoluciones **/
        $('#idGenerarDevoluciones').click(function(){
            var moduloDev = $('#idGenerarDevoluciones').hasClass('spid');
            var nuevoMensajeDev = tituloModulo;
            /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la     validacion **/
            var accionDev = 1;
            if (validarModulo(moduloDev)) {
                nuevoMensajeDev += moduloSpid;
                accionDev = 2;
            }
            jConfirm("\u00bfEst\u00E1 seguro de generar solo devoluciones?", nuevoMensajeDev, "",
                    "", function(respuesta) {
                        if (respuesta) {
                            document.getElementById('tipoArchivoGenerar').value = "2";
                            document.forms['formParamPOACOA'].action = "generarArchivoPOACOA.do";
                            document.getElementById('accion').value = accionDev;
                            document.forms['formParamPOACOA'].submit();
                        }
                    }, "Cancelar");
            
            
        });
        /** Evento para el boton Generar todos los archivos **/
        $('#idGenerarTodos').click(function(){
            var moduloGen = $('#idGenerarTodos').hasClass('spid');
            var nuevoMensaje = tituloModulo;
            /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la     validacion **/
            var accionGen = 1;
            if (validarModulo(moduloGen)) {
                nuevoMensaje += moduloSpid;
                accionGen = 2;
            }
            jConfirm("\u00bfEst\u00E1 seguro de generar de todos los archivos?", nuevoMensaje, "",
                    "", function(respuesta) {
                        if (respuesta) {
                            document.getElementById('tipoArchivoGenerar').value = "1";
                            document.forms['formParamPOACOA'].action = "generarArchivoPOACOA.do";
                            document.getElementById('accion').value = accionGen;
                            document.forms['formParamPOACOA'].submit();
                        }
                    }, "Cancelar");
            
            
        });
        /** Evento para la opcion Generar Traspasos**/
        $('#idGenerarTranspasos').click(function(){
            var moduloTransp = $('#idGenerarTranspasos').hasClass('spid');
            var nuevoMensajeTransp = tituloModulo;
            /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
            var accionTransp = 1;
            if (validarModulo(moduloTransp)) {
                nuevoMensajeTransp += moduloSpid;
                accionTransp = 2;
            }
            jConfirm("\u00bfEst\u00E1 seguro de generar solo traspasos?", nuevoMensajeTransp, "",
                    "", function(respuesta) {
                        if (respuesta) {

                            document.forms['formParamPOACOA'].action = "generarArchivoPOACOA.do";
                            document.getElementById('tipoArchivoGenerar').value = "3";
                            document.getElementById('accion').value = accionTransp;
                            document.forms['formParamPOACOA'].submit();
                        }
                    }, "Cancelar");
            
            
        });
        /** Evento para refrescar la pantalla**/
        $('#idActualizar').click(function(){
            var modulo = $('#idActualizar').hasClass('spid');
            /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
            var accion = 1;
            var url = 'parametrosPOACOA.do';
            if (validarModulo(modulo)) {
                accion = 2;
                url = 'parametrosPOACOA.do?spid=2';
            }
            $('#formParamPOACOA').attr('action', url);
            document.getElementById('accion').value = accion;
            $('#formParamPOACOA').submit();
        });        
        
        
    }

    

};

/**
 * Funcion para procesar
 */
function cifradoActivar(){
    document.getElementById('parametroCifrado').value = "1";
    document.forms['formParamPOACOA'].action = "activarDesactivarCifrado.do";
    document.forms['formParamPOACOA'].submit();
}
/** Funcion para desactivar el crifrado **/
function cifradoDesactivar(){
    document.getElementById('parametroCifrado').value = "0";
    document.forms['formParamPOACOA'].action = "activarDesactivarCifrado.do";
    document.forms['formParamPOACOA'].submit();
}

/** Funcion para descactivar el proceso POA **/
function desactivarPOA(modulo){
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        accion = 2;
    }
    document.getElementById('procesoPOA').value = "0";
    document.getElementById('accion').value = accion;
    document.forms['formParamPOACOA'].action = "activarDesactivarPOA.do";
    document.forms['formParamPOACOA'].submit();
}

/** Funcion para activar el POA **/
function activarPOA(modulo){
    var nuevoMensaje = tituloModulo;
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        nuevoMensaje += moduloSpid;
        accion = 2;
    }
    jConfirm("\u00bfEst\u00E1 seguro de activar el POA?", nuevoMensaje, "",
            "", function(respuesta) {
                if (respuesta) {
                    document.getElementById('procesoPOA').value = "1";
                    document.getElementById('accion').value = accion;
                    document.forms['formParamPOACOA'].action = "activarDesactivarPOA.do";
                    document.forms['formParamPOACOA'].submit();
                }
            }, "Cancelar");
    
    
}
/** Funcion para desactivar el proceso COA **/
function desactivarCOA(modulo){
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        accion = 2;
    }
    document.getElementById('procesoCOA').value = "0";
    document.getElementById('accion').value = accion;
    document.forms['formParamPOACOA'].action = "activarDesactivarCOA.do";
    document.forms['formParamPOACOA'].submit();
}
/** Funcion para activar el COA **/
function activarCOA(modulo){
    var nuevoMensaje = tituloModulo;
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        nuevoMensaje += moduloSpid;
        accion = 2;
    }
    jConfirm("\u00bfEst\u00E1 seguro de activar el COA?", nuevoMensaje, "",
            "", function(respuesta) {
                if (respuesta) {
                    document.getElementById('procesoCOA').value = "1";
                    document.getElementById('accion').value = accion;
                    document.forms['formParamPOACOA'].action = "activarDesactivarCOA.do";
                    document.forms['formParamPOACOA'].submit();
                }
            }, "Cancelar");    
    
    
}
/** Funcion para activar la Fase Envio y Recepcion de Archivos **/
function activarEnvRecArch(modulo){
    var nuevoMensaje = tituloModulo;
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        nuevoMensaje += moduloSpid;
        accion = 2;
    }
    jConfirm("\u00bfEst\u00E1 seguro de activar el env\u00EDo y recepci\u00F3n?", nuevoMensaje, "",
            "", function(respuesta) {
                if (respuesta) {
                    document.getElementById('numeroFase').value = "1";
                    document.getElementById('accion').value = accion;
                    document.forms['formParamPOACOA'].action = "parametrosFase.do";
                    document.forms['formParamPOACOA'].submit();
                }
            }, "Cancelar");
    
    
}
/** Funcion para activar la fase Final Devoluciones **/
function activarFinalDev(modulo){
    var nuevoMensaje = tituloModulo;
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la     validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        nuevoMensaje += moduloSpid;
        accion = 2;
    }
    jConfirm("\u00bfEst\u00E1 seguro de activar el final de las Devoluciones?", nuevoMensaje, "",
            "", function(respuesta) {
                if (respuesta) {
                    document.getElementById('numeroFase').value = "2";
                    document.forms['formParamPOACOA'].action = "parametrosFase.do";
                    document.getElementById('accion').value = accion;
                    document.forms['formParamPOACOA'].submit();
                }
            }, "Cancelar");            
    }

/** Funcion para activar la Fase Liquidacion Final **/
function activarLiqFinal(modulo){
    var nuevoMensaje = tituloModulo;
    /** Se declara la variable que servira como auxiliar de la pantalla y se reliza la     validacion **/
    var accion = 1;
    if (validarModulo(modulo)) {
        nuevoMensaje += moduloSpid;
        accion = 2;
    }
    jConfirm("\u00bfEst\u00E1 seguro de activar la Liquidaci\u00F3n final?", nuevoMensaje, "",
            "", function(respuesta) {
                if (respuesta) {
                    document.getElementById('numeroFase').value = "3";
                    document.getElementById('liqFinal').value = "1";
                    document.forms['formParamPOACOA'].action = "parametrosFase.do";
                    document.getElementById('accion').value = accion;
                    document.forms['formParamPOACOA'].submit();
                }
            }, "Cancelar");
    
     
}
/** Funcion que se utiliza para validar la pantalla del modulo en el que se encuentra el usuario**/
function validarModulo(nombre) {
    var estatus = false;
    if (nombre === 'spid' || nombre === true){
        estatus = true;
    }
    return estatus;
}
$(document).ready(paramPOACOA.init);