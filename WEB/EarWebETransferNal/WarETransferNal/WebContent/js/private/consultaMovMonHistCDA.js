consultaMovMonHistCDA={
		diasMes:{0:31,1:28,2:31,3:30,4:31,5:30,6:31,7:31,8:30,9:31,10:30,11:31},
		regExpCveSpei : new RegExp("^([0-9]{1,6})$"),
		regExpNum : new RegExp("^([0-9]{1,20})$"),
		regExpMontoPago : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}(?!(,|[0-9]{1,2},|/.|[0-9]{1,2}/.))[0-9]{0,3}){0,}([/.]{1}[0-9]{0,2}){0,1})$"),
		regExpMontoPagoValida : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}[0-9]{3,3}){0,4}([/.]{1}[0-9]{1,2}){0,1})$"),
		validarPeriodoOperacion:function(pInicio,pFin) {
			if(!Utilerias.isEmpty(pInicio) && !Utilerias.isEmpty(pFin)) {
				inicio = Utilerias.stringToDate($('#'+pInicio).val());
				fin = Utilerias.stringToDate($('#'+pFin).val());
				if(consultaMovMonHistCDA.esFechaFinOperacion(fin)){
					if(consultaMovMonHistCDA.esPeriodoCorrecto(inicio,fin)){
						if(consultaMovMonHistCDA.esEnElMes(inicio,fin)) {
							return "";
						}else {
							return "ED00019V";
						}
					}
					else {
						return "ED00020V";
					}
				}
				else {
					return "ED00020V";
				}
			}
			else {
				return "ED00021V";
			}
		},

		validarPeriodoHrs:function(pInicio,pFin) {
			periodo=consultaMovMonHistCDA.esPeriodoEspecificado(pInicio,pFin);
			if(periodo===1) {
				hrInicio = $('#'+pInicio).val();
				hrFin = $('#'+pFin).val();
				pInicio= Utilerias.stringToTime(hrInicio);
				pFin= Utilerias.stringToTime(hrFin);
				if(consultaMovMonHistCDA.esPeriodoCorrecto(pInicio,pFin)){
					return "";
				}
				else {
					return "ED00022V";
				}
			} else if(periodo===2|| periodo=== 3) {
				return "ED00023V";
			}
			else {
				return "";
			}
		},

		esPeriodoCorrecto:function(pInicio,pFin) {
			return pInicio.getTime()<= pFin.getTime();
		},

		esPeriodoEspecificado:function(pInicio,pFin) {
			if(!Utilerias.isEmpty(pInicio) && !Utilerias.isEmpty(pFin)){
				return 1;
			}else if (!Utilerias.isEmpty(pInicio)){
				return 2;
			}else if (!Utilerias.isEmpty(pFin)){
				return 3;
			}
			return 4;
		},

		esFechaFinOperacion:function(fin){
			strFechaHoy=$('#fechaHoy').val();
			dFechaHoy = Utilerias.stringToDate(strFechaHoy);
			return fin.getTime()<dFechaHoy.getTime();
		},


		esEnElMes:function(dFechaInicio,dFechaFin) {
			return (dFechaInicio.getFullYear() === dFechaFin.getFullYear() && dFechaInicio.getMonth() === dFechaFin.getMonth());
		},

		esPeriodoMenorTresMeses:function(dFechaInicio,dFechaFin) {
			mes = dFechaInicio.getMonth()+3;
			anio = dFechaInicio.getFullYear();
			if(mes>12){
				mes = mes-12;
				anio = anio+1;
			}
			dia = dFechaInicio.getDate();
			diaBase = consultaMovMonHistCDA.diasMes[mes];
			if(mes === 1 && ((anio % 4 === 0) && ((anio % 100 !== 0) || (anio % 400 === 0)))){
					diaBase =29;
			}
			if(dia>diaBase){
				dFecha = new Date(anio,mes,diaBase);
			}else{
				dFecha = new Date(anio,mes,dia);
			}


			return !(dFechaFin.getTime()>dFecha.getTime());
		},

		init:function(){
			createCalendarTime('paramHrAbonoIni','cal3');
			createCalendarTime('paramHrAbonoFin','cal4');
			createCalendarTime('paramHrEnvioIni','cal5');
			createCalendarTime('paramHrEnvioFin','cal6');

			$('#opeContingencia').attr("checked", $('#paramOpeContingencia').val());

			$('#idBuscar').click(function(){

				if(!Utilerias.isEmpty('paramMontoPago') && !consultaMovMonHistCDA.validaCampoMonto('paramMontoPago')){
					jAlert(mensajes["ED00027V"],mensajes["aplicacion"],'ED00027V','');
					return;
				}


				var codigo=consultaMovMonHistCDA.validarPeriodoOperacion('paramFechaOpeInicio',
																	  'paramFechaOpeFin');
				if(codigo.length===0) {
					codigo= consultaMovMonHistCDA.validarPeriodoHrs('paramHrAbonoIni','paramHrAbonoFin');
					if(codigo.length===0) {
						codigo= consultaMovMonHistCDA.validarPeriodoHrs('paramHrEnvioIni','paramHrEnvioFin');
						if(codigo.length===0){
							$('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
							$('#fechaOpeInicio').val($('#paramFechaOpeInicio').val());
							$('#fechaOpeFin').val($('#paramFechaOpeFin').val());
							$('#hrAbonoIni').val($('#paramHrAbonoIni').val());
							$('#hrAbonoFin').val($('#paramHrAbonoFin').val());
							$('#hrEnvioIni').val($('#paramHrEnvioIni').val());
							$('#hrEnvioFin').val($('#paramHrEnvioFin').val());
							$('#cveSpeiOrdenanteAbono').val($('#paramCveSpeiOrdenanteAbono').val());
							$('#nombreInstEmisora').val($('#paramNombreInstEmisora').val());
							$('#cveRastreo').val($('#paramCveRastreo').val());
							$('#ctaBeneficiario').val($('#paramCtaBeneficiario').val());
							$('#montoPago').val($('#paramMontoPago').val());
							$('#tipoPago').val($('#paramTipoPago').val());
							$('#idForm').attr('action', 'consMovMonHistCDA.do');
							$('#idForm').submit();
						}
						else {
							jError(mensajes[codigo],mensajes["aplicacion"],codigo,'');
						}
					}
					else {
						jError(mensajes[codigo],mensajes["aplicacion"],codigo,'');
					}
				}
				else {
					jError(mensajes[codigo],mensajes["aplicacion"],codigo,'');
				}

			});

			$('#opeContingencia').change(function() {
				if ($('#opeContingencia').is(":checked")) {
					$("#selectEstatus option[value="+ 0 +"]").attr("selected",true);
					$('#selectEstatus').attr('disabled',true);
					$('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
			        }else {
			            $('#selectEstatus').attr('disabled',false);
			            $('#paramOpeContingencia').val("");
			        }
			    }
			);

			$('#idRegresar').click(function(){
				$('#idForm').attr('action','consInfMonCDAHist.do');
				$('#idForm').submit();
			});

			 $('#idExportar').click(function(){
				 	$('#accion').val('ACT');
				 	$('#idForm').attr('action', 'expConsMovMonHistCDA.do');
					$('#idForm').submit();
			   });


			   $('#idExportarTodo').click(function(){
				    $('#idForm').attr('action', 'expTodosConsMovMonHistCDA.do');
					$('#idForm').submit();
			   });

			   $('#idLimpiar').click(function(){
				   $('#idForm').find('input').each(function() {
					      switch(this.type) {
					         case 'text':
					        	 if(!(this.name ==='paramFechaOpeInicio' || this.name ==='paramFechaOpeFin')){
					        		 $(this).val('');
					        	 }
 
					              break;
					         case 'checkbox':
					              this.checked = false;
					              break;
					         default:
					      }
					   });
				   		$('#idForm').attr('action', 'consMovMonHistCDA.do');
				   		$('#idForm').submit();
				});

				$('#paramCveSpeiOrdenanteAbono').keypress(function(e){
					var cad;
					var k;

					document.all ? k = e.keyCode : k = e.which;
					consultaMovMonHistCDA.cancel2(e);
					cad = $('#paramCveSpeiOrdenanteAbono').val()+String.fromCharCode(k);
					if(!(cad ==="") && !consultaMovMonHistCDA.regExpCveSpei.test(cad) && k !== 8){
						e.preventDefault();
						e.stopPropagation();
					}
		        });
				
				$('#paramTipoPago').keypress(function(e){
					var cad;
					var k;

					document.all ? k = e.keyCode : k = e.which;
					consultaMovMonHistCDA.cancel2(e);
					cad = $('#paramTipoPago').val()+String.fromCharCode(k);
					if(!(cad ==="") && !consultaMovCDA.regExpNum.test(cad) && k !== 8){
						e.preventDefault();
						e.stopPropagation();
					}
		        });
				

				$('#paramMontoPago').keypress(function(e){

					var cad;
					var k;

					document.all ? k = e.keyCode : k = e.which;
					consultaMovMonHistCDA.cancel(e);
					cad = $('#paramMontoPago').val()+String.fromCharCode(k);
					if(!(cad ==="") && !consultaMovMonHistCDA.regExpMontoPago.test(cad) && k !== 8){
						e.preventDefault();
						e.stopPropagation();
					}
		        });
		},
		cancel:function(e){
			document.all ? k = e.keyCode : k = e.which;
			if(!((k === 8 || k===46|| k===44 ) || (k>=48 && k<=57)) ){
				e.preventDefault();
				e.stopPropagation();
			}else if(k===107|| k===109 || k=== 111 || k===18){
				e.preventDefault();
				e.stopPropagation();
			}
		},
		cancel2:function(e){
			document.all ? k = e.keyCode : k = e.which;
			if(!((k === 8 || k===190 ) || (k>=48 && k<=57)) ){
				e.preventDefault();
				e.stopPropagation();
			}else if(k===107|| k===109 || k=== 111 || k===18){
				e.preventDefault();
				e.stopPropagation();
			}
		},

		despliegaDetalle:function(valores){
			//referencia,fechaOpe,cveMiInstituc,cveSpei,folioPaq,folioPago,fchCDA,estatusCDA
			$('#referencia').val(valores["referencia"]);
			$('#fechaOpeLink').val(valores["fechaOpe"]);
			$('#cveMiInstitucLink').val(valores["cveMiInstituc"]);
			$('#cveSpeiLink').val(valores["cveSpei"]);
			$('#folioPaqLink').val(valores["folioPaq"]);
			$('#folioPagoLink').val(valores["folioPago"]);
			$('#fchCDALink').val(valores["fchCDA"]);
			$('#estatusCDALink').val(valores["estatusCDA"]);
			$('#accion').val('ACT');
			$('#idForm').attr('action','consMovMonHistDetCDA.do');
			$('#idForm').submit();
		},

		 validaCampoMonto:function(idMonto){
			   var monto = $('#'+idMonto).val();
			   return consultaMovMonHistCDA.regExpMontoPagoValida.test(monto);
		   }
		};



$(document).ready(consultaMovMonHistCDA.init);