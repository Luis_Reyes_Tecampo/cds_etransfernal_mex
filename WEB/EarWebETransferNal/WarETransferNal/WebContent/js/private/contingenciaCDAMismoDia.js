contingenciaCDAMismoDia ={
		init:function(){
			$('#idGeneracionNuevoArchivo').click(function(e){
				
				jConfirm(mensajes["CD00020V"],mensajes["aplicacion"],'CD00020V','', function (respuesta){
					if(respuesta == true){
						isSubmit = true;
						isSubmitSend = true;
						$('#idForm').attr('action', 'genArchContigMismoDia.do');
						$('#idForm').submit();
					} 
				}
				);
			});
			
			$('#idActualizar').click(function(e){
				$('#idForm').attr('action', 'consultarContingCDAMismoDia.do');
				$('#idForm').submit();
			});
		}
};

$(document).ready(contingenciaCDAMismoDia.init);