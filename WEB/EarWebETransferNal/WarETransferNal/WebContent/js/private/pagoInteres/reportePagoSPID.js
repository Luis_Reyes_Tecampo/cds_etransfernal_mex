reportePagoSPID ={
	regExpMontoPago : new RegExp("^([0-9]{1,12}[/.]{0,1}[0-9]{0,2})$"),
	regExpEntero : new RegExp("^([0-9]{1,4})"),
	init:function(){
	$('#idRangoDia').show();
	$('#idRangoHist').hide();
		document.body.style.cursor = "default";
		var opcion = document.getElementsByName("opcion");
		for(var i=0;i<opcion.length; i++){
			if(opcion[i].checked){
				if(opcion[i].value === 'A'){
					$('#idHoraIni').show();
					$('#idHoraFin').show();
					$('#idFechaIni').hide();
					$('#idFechaFin').hide();
					$('#idRangoDia').show();
					$('#idRangoHist').hide();
					
				}else{
					$('#idHoraIni').hide();
					$('#idHoraFin').hide();
					$('#idFechaIni').show();
					$('#idFechaFin').show();
					$('#idRangoDia').hide();
					$('#idRangoHist').show();
				}
			}
		}
		createCalendarGuion('txtFechaInicio','cal3');
		
		
		$('#idLimpiar').click(function(){
			var form = document.getElementById("idForm");
			document.getElementById("idTipoPago").value = ""; 
			document.getElementById("selTipoPago").value = ""; 
			document.getElementById("idEstatus").value = ""; 
			document.getElementById("selEstatus").value = ""; 
			document.getElementById("idMedioEnt").value = ""; 
			document.getElementById("selMedioEnt").value = "";
			document.getElementById("idHInicio").value = "";
			document.getElementById("idMInicio").value = ""; 
			
			document.getElementById("selHoraIni").value = "0";
			document.getElementById("selHoraFin").value = "0";
			document.getElementById("selMinutoIni").value = "0";
			document.getElementById("selMinutoFin").value = "0";
			
			document.getElementById("idHFin").value = "";
			document.getElementById("idMFin").value = "";
			 
			document.getElementById("idFInicio").value = ""; 
			document.getElementById("txtFechaInicio").value = ""; 
			document.getElementById("idMinutos").value = ""; 
			document.getElementById("selMinutos").value = "";
			document.getElementById("idDifMinutos").value = ""; 
			document.getElementById("txtDifMinutos").value = ""; 
			document.getElementById("idImporteInicio").value = ""; 
			document.getElementById("txtImporteInicio").value = "";
			document.getElementById("idImporteFin").value = ""; 
			document.getElementById("txtImporteFin").value = "";
		});
		
		$('#idExportar').click(function(){
			document.body.style.cursor = "wait";
			var form = document.getElementById("idForm");
			form.action="exportarReportePagoSPID.do";
			setTimeout(function (){
				document.body.style.cursor = "default";
			},2000);
			form.submit();
		});
		
		$('#idExportarTodo').click(function(){
			document.body.style.cursor = "wait";
			var form = document.getElementById("idForm");
			form.action="exportarTodoReportePagoSPID.do";
			form.submit();
		});
		
		var idBuscar = $('#idBuscar').click(function(){
			document.body.style.cursor = "wait";
			document.getElementById("idTipoPago").value = document.getElementById("selTipoPago").value;
			document.getElementById("idEstatus").value = document.getElementById("selEstatus").value;
			document.getElementById("idMedioEnt").value = document.getElementById("selMedioEnt").value;
			
			document.getElementById("idHInicio").value = document.getElementById("selHoraIni").value;
			document.getElementById("idMInicio").value = document.getElementById("selMinutoIni").value;
			document.getElementById("idHFin").value = document.getElementById("selHoraFin").value;
			document.getElementById("idMFin").value = document.getElementById("selMinutoFin").value;
			
			document.getElementById("idFInicio").value = document.getElementById("txtFechaInicio").value;
			document.getElementById("idMinutos").value = document.getElementById("selMinutos").value;
			document.getElementById("idDifMinutos").value = document.getElementById("txtDifMinutos").value;
			document.getElementById("idImporteInicio").value = document.getElementById("txtImporteInicio").value;
			document.getElementById("idImporteFin").value = document.getElementById("txtImporteFin").value;
			
			
			var form = document.getElementById("idForm");
			form.action='consultaReportePagoSPID.do';
			form.submit();
			
		});	
		
		var idActualizar = $('#idActualizar').click(function(){
			document.body.style.cursor = "wait";
			var form = document.getElementById("idForm");
			form.action='consultaReportePagoSPID.do';
			form.submit();
		});	
	
		$('#txtReporteLinea').click(function(){
			$('#idHoraIni').show();
			$('#idHoraFin').show();
			$('#idFechaIni').hide();
			$('#idRangoDia').show();
			$('#idRangoHist').hide();
			
			
		});
		
		$('#txtReporteHistorico').click(function(){
			$('#idHoraIni').hide();
			$('#idHoraFin').hide();
			$('#idFechaIni').show();
			$('#idRangoDia').hide();
			$('#idRangoHist').show();
		});	
		
		$('#txtImporteInicio').keypress(function(e){
			var cad;
			var k;
			document.all ? k = e.keyCode : k = e.which;
			reportePagoSPID.cancelConPunto(e);
			cad = $('#txtImporteInicio').val()+String.fromCharCode(k);
			if(!(cad ==="") && !reportePagoSPID.regExpMontoPago.test(cad) && k !== 8){
				e.preventDefault();
				e.stopPropagation();
			}
	    });
		
		$('#txtImporteFin').keypress(function(e){
			var cad;
			var k;
			document.all ? k = e.keyCode : k = e.which;
			reportePagoSPID.cancelConPunto(e);
			cad = $('#txtImporteFin').val()+String.fromCharCode(k);
			if(!(cad ==="") && !reportePagoSPID.regExpMontoPago.test(cad) && k !== 8){
				e.preventDefault();
				e.stopPropagation();
			}
	    });
		
		$('#txtDifMinutos').keypress(function(e){
			var cad;
			var k;
			document.all ? k = e.keyCode : k = e.which;
			reportePagoSPID.cancelSinComasPuntos(e);
			cad = $('#txtImporteInicio').val()+String.fromCharCode(k);
			if(!(cad ==="") && !reportePagoSPID.regExpEntero.test(cad) && k !== 8){
				e.preventDefault();
				e.stopPropagation();
			}
	    });
		
		

	},
	cancelConPunto:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===46 || k===44 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	},
	cancelSinComasPuntos:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8  ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
	
};

$(document).ready(reportePagoSPID.init);