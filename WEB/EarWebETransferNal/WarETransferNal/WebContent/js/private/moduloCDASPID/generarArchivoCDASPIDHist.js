generarArchivoCDAHistSPID = {
        regExp : new RegExp("^[a-z|A-Z|0-9|\d]+([/.]txt)$","i"),
        regExpValCad : RegExp("^[a-z|A-Z|0-9|\d]+([/.]|[/.][a-z|0-9]{1,}){0,1}$","i"),
        regExpCad : new RegExp("^([a-z|A-Z|0-9]{1,}([/.]|[/.]t|[/.]tx|[/.]txt){0,1})$","i"),
        copiar:false,
        inicioGeneraArchivoSPID:function(){
            createCalendarGuion('fechaOperacion','cal1');
            generarArchivoCDAHistSPID.procesaFileName('nombreArchivo');

            $('#idProcesar').click(function(){
                if(Utilerias.isEmpty('fechaOperacion')){
                    jAlert(mensajes["ED00025V"],mensajes["aplicacion"],'ED00025V','');
                    return false;
                }
                strFechaBanMex=$('#fechaOperacionBancoMex').val();
                var dFechaBanMex = Utilerias.stringToDate(strFechaBanMex);
                strFechaOp=$('#fechaOperacion').val();
                dFechaOp = Utilerias.stringToDate(strFechaOp);
                if(dFechaBanMex.getTime()>dFechaOp.getTime()){
                    nomArchivo=$('#nombreArchivo').val();
                    if(generarArchivoCDAHistSPID.regExpValCad.test(nomArchivo)){
                        if(generarArchivoCDAHistSPID.regExp.test(nomArchivo)){
                            hacerSubmit('idForm', 'procesaGeneracionArchHisCDASPID.do');
                        }else{
                            jAlert(mensaje["ED00013V"],mensaje["aplicacion"],'ED00013V','');
                        }
                    }else{
                        jAlert(mensaje["ED00028V"],mensaje["aplicacion"],'ED00028V','');
                    }

                }else{
                    jAlert(mensaje["ED00014V"],mensaje["aplicacion"],'ED00014V','');
                }
            });
        },
        
        procesar:function(){
            if(Utilerias.isEmpty('fechaOperacion')){
                jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');
            };
            Utilerias.isEmpty('nombreArchivo');
        },

        procesaFileName:function(id){
            $('#'+id).keypress(function(e){
                var ke;
                var bandCaracteres=false;
                if (document.all){
                    ke = e.keyCode;
                }else{
                    ke = e.which;
                }
                if(!( ke === 8 || ke === 46 || ke===190 ||ke ===0 )){
                    bandCaracteres=true;
                }
                if(bandCaracteres && generarArchivoCDAHistSPID.validarLetras(ke)){
                    e.preventDefault();
                    e.stopPropagation();
                }
                generarArchivoCDAHistSPID.validarExtensionArchivo(id,ke,e);
            });
        },

        validarLetras:function(ke){
            var val1 = (ke > 64 && ke < 91);
            var val2 = (ke > 96 && ke < 123);
            var val3 = (ke >= 48 && ke <= 57);
            return !( val1 || val2 || val3 );
        },

        validarExtensionArchivo:function(id,ke,e){
            var cad;
            if(generarArchivoCDAHistSPID.copiar === false){
                cad = $('#'+id).val()+String.fromCharCode(ke);
            }else{
                cad = $('#'+id).val();
            }
            var validacion = (ke === 'v' || ke ==='c' || ke === 'V' || ke === 'C');
            if(generarArchivoCDAHistSPID.copiar && !validacion){
                e.preventDefault();
                e.stopPropagation();
            }else if(!generarArchivoCDAHistSPID.regExpCad.test(cad) && !(ke === 8 || ke === 0 )){
                e.preventDefault();
                e.stopPropagation();
            }
        }
    };

$(document).ready(generarArchivoCDAHistSPID.inicioGeneraArchivoSPID);