monitorCDAHistSPID ={
        init:function(){
            createCalendarGuion('fechaOpe','cal1');
            $('#idBuscar').click(function(){
                var strFechaHoySPID=$('#fechaHoy').val();
                var fechaOpeSPID =$('#fechaOpe').val();
                var dFechaHoySPID = Utilerias.stringToDate(strFechaHoySPID);
                var dFechaOpSPID = Utilerias.stringToDate(fechaOpeSPID);

                if(Utilerias.isEmpty('fechaOpe')){
                    jError(mensajes["ED00063V"],mensajes["aplicacion"],'ED00063V','');
                    return false;
                }
                if( dFechaOpSPID.getTime() >= dFechaHoySPID.getTime() ){
                    jError(mensajes["ED00063V"],mensajes["aplicacion"],'ED00063V','');
                    return false;
                }
                $('#paramFechaOperacion').val(fechaOpeSPID);
                hacerSubmit('idForm', 'consInfMonCDAHistSPID.do');
            });

            $('#idCDAOrdRecAplicadas').click(function(){
                var fechaOp =$('#fechaOpe').val();
                $('#paramFechaOperacion').val(fechaOp);
                $('#fechaOpeInicio').val(fechaOp);
                $('#fechaOpeFin').val(fechaOp);
                $('#estatusCDA').val('');
                $('#idForm').attr('action','consMovMonHistCDASPID.do');
                $('#idForm').submit();
            });

            $('#idCDAPendEnviar').click(function(){
                var fechaOpeSPID1 =$('#fechaOpe').val();
                $('#fechaOpeInicio').val(fechaOpeSPID1);
                $('#fechaOpeFin').val(fechaOpeSPID1);
                $('#paramFechaOperacion').val(fechaOpeSPID1);
                $('#estatusCDA').val('0,3');
                $('#idForm').attr('action','consMovMonHistCDASPID.do');
                $('#idForm').submit();
            });

            $('#idCDAEnviadas').click(function(){
                var fechaOpeSPID2 =$('#fechaOpe').val();
                $('#fechaOpeInicio').val(fechaOpeSPID2);
                $('#fechaOpeFin').val(fechaOpeSPID2);
                $('#paramFechaOperacion').val(fechaOpeSPID2);
                $('#estatusCDA').val('1');
                $('#idForm').attr('action','consMovMonHistCDASPID.do');
                $('#idForm').submit();
            });

            $('#idCDAConfirmadas').click(function(){
                var fechaOpeSPID4 =$('#fechaOpe').val();
                $('#paramFechaOperacion').val(fechaOpeSPID4);
                $('#fechaOpeInicio').val(fechaOpeSPID4);
                $('#fechaOpeFin').val(fechaOpeSPID4);
                $('#estatusCDA').val('2');
                $('#idForm').attr('action','consMovMonHistCDASPID.do');
                $('#idForm').submit();
            });
            monitorCDAHistSPID.init2();
     },
     init2:function(){
         $('#idCDAContingencia').click(function(){
             var fechaOpeSPID5 =$('#fechaOpe').val();
             $('#fechaOpeInicio').val(fechaOpeSPID5);
             $('#fechaOpeFin').val(fechaOpeSPID5);
             $('#paramFechaOperacion').val(fechaOpeSPID5);
             $('#estatusCDA').val('4');
             $('#idForm').attr('action','consMovMonHistCDASPID.do');
             $('#idForm').submit();
         });

         $('#idCDARechazadas').click(function(){
             var fechaOpeSPID6 =$('#fechaOpe').val();
             $('#fechaOpeInicio').val(fechaOpeSPID6);
             $('#fechaOpeFin').val(fechaOpeSPID6);
             $('#paramFechaOperacion').val(fechaOpeSPID6);
             $('#estatusCDA').val('5');
             $('#idForm').attr('action','consMovMonHistCDASPID.do');
             $('#idForm').submit();
         });
     }
};

$(document).ready(monitorCDAHistSPID.init);