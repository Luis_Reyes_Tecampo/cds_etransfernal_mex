$(document).ready(function() {
    $('#idGenerarArchivon').click(function() {
        jConfirm(mensajes["CD00030V"],mensajes["aplicacion"],'CD00030V','',function(respuesta){
            if(respuesta) {
                hacerSubmit('archivosHistoricosDetalleCADSPID', 'generarArchivoHistorico.do');
            }
        });
    });

    $('#idEliminarSeleccion').click(function() {
        jConfirm(mensajes["CD00040V"],mensajes["aplicacion"],'CD00040V','',function(respuesta){
            if(respuesta) {
                hacerSubmit('archivosHistoricosDetalleCADSPID', 'eleminaSeleccionCDASPID.do');
            }
        });
    });
});

function generarArchivo(idCampo, idFormulario, urlAccion) {
    $('#'+idCampo).click(function() {
        jConfirm(mensajes["CD00030V"],mensajes["aplicacion"],'CD00030V','',function(respuesta){
            if(respuesta) {
                hacerSubmit(idFormulario, urlAccion);
            }
        });
    });
}

function eliminarSeleccion(idCampo, idFormulario, urlAccion) {
    $('#'+idCampo).click(function() {
        jConfirm(mensajes["CD00040V"],mensajes["aplicacion"],'CD00040V','',function(respuesta){
            if(respuesta) {
                hacerSubmit(idFormulario, urlAccion);
            }
        });
    });
}

/**
 * Funcion para realizar submit
 * @param formName
 * @param action
 */
function hacerSubmit(formName,action){
    var form = $('form[name="'+formName+'"]');
    form.attr("action", action);
    form.submit();
}

function guardaTemporal(numeroIndice) {
    /**var checkedValues = $('input:checkbox:checked').map(function() {
        //out.print("<input type='text' value='"+idCampo+"'>");
        alert(this.value);
        return this.value;
    }).get();*/
    var eliminaA = document.getElementById('elimina').value;
    var idArch = document.getElementById('idArchivo'+numeroIndice).value;
    var clvRast = document.getElementById('claveRastreo'+numeroIndice).value;
    var fchOp = document.getElementById('fechaOperacion'+numeroIndice).value;
    var clvInst = document.getElementById('claveInstitucion'+numeroIndice).value;
    var instOrd = document.getElementById('institucionOrdinaria'+numeroIndice).value;
    var folPaq = document.getElementById('folioPaquete'+numeroIndice).value;
    var folPag = document.getElementById('folioPago'+numeroIndice).value;

    if(eliminaA === null || eliminaA === '') {
        eliminaA = idArch+','+clvRast+','+fchOp+','+clvInst+','+instOrd+','+folPaq+','+folPag;
    }
    else {
        eliminaA = eliminaA+','+idArch+','+clvRast+','+fchOp+','+clvInst+','+instOrd+','+folPaq+','+folPag;
    }
    document.getElementById("elimina").value=eliminaA;
}