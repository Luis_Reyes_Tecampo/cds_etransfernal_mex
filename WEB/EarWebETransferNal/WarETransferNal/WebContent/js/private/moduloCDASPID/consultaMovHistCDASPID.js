consultaMovHistCDASPID={
        diasMes:{0:31,1:28,2:31,3:30,4:31,5:30,6:31,7:31,8:30,9:31,10:30,11:31},
        regExpCveSpei : new RegExp("^([0-9]{1,6})$"),
        regExpNum : new RegExp("^([0-9]{1,20})$"),
        regExpMontoPago : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}(?!(,|[0-9]{1,2},|/.|[0-9]{1,2}/.))[0-9]{0,3}){0,}([/.]{1}[0-9]{0,2}){0,1})$"),
        regExpMontoPagoValida : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}[0-9]{3,3}){0,4}([/.]{1}[0-9]{1,2}){0,1})$"),
        validarPeriodoOperacion:function(pInicioM,pFinM) {
            if(!Utilerias.isEmpty(pInicioM) && !Utilerias.isEmpty(pFinM)) {
                inicio = Utilerias.stringToDate($('#'+pInicioM).val());
                fin = Utilerias.stringToDate($('#'+pFinM).val());
                if(consultaMovHistCDASPID.esFechaFinOperacion(fin)){
                    return consultaMovHistCDASPID.validarPeriodoOperacion2(inicio, fin);
                }
                else {
                    return "ED00020V";
               }
            }
            else {
                return "ED00021V";
            }
        },

        validarPeriodoOperacion2:function(inicio,fin){
            if(consultaMovHistCDASPID.esPeriodoCorrecto(inicio,fin)){
                if(consultaMovHistCDASPID.esEnElMes(inicio,fin)) {
                    return "";
                }else {
                    return "ED00014V";
                }
            }
            else {
                return "ED00020V";
            }
        },

        validarPeriodoHrs:function(pInicio,pFin) {
            var respt = "";
            periodo=consultaMovHistCDASPID.esPeriodoEspecificado(pInicio,pFin);
            if(periodo===1) {
                hrInicio = $('#'+pInicio).val();
                hrFin = $('#'+pFin).val();
                pInicio= Utilerias.stringToTime(hrInicio);
                pFin= Utilerias.stringToTime(hrFin);
                if(consultaMovHistCDASPID.esPeriodoCorrecto(pInicio,pFin)){
                    return respt;
                }
                else {
                    respt = "ED00022V";
                    return respt;
                }
            } else if(periodo===2|| periodo=== 3) {
                respt = "ED00023V";
                return respt;
            }
            else {
                return respt;
            }
        },

        esPeriodoCorrecto:function(pInicioSPID,pFin) {
            return pInicioSPID.getTime()<= pFin.getTime();
        },

        esPeriodoEspecificado:function(pInicioSPID,pFin) {
            if(!Utilerias.isEmpty(pInicioSPID) && !Utilerias.isEmpty(pFin)){
                return 1;
            }else if (!Utilerias.isEmpty(pInicioSPID)){
                return 2;
            }else if (!Utilerias.isEmpty(pFin)){
                return 3;
            }
            return 4;
        },

        esFechaFinOperacion:function(fin){
            strFechaHoy=document.getElementById('fechaHoy').value;
            dFechaHoy = Utilerias.stringToDate(strFechaHoy);
            return fin.getTime()<dFechaHoy.getTime();
        },

        esEnElMes:function(fechaInicio,fechaFin) {
            mes = fechaInicio.getMonth()+1;
            anio = fechaInicio.getFullYear();
            if(mes === 12){
                mes = 0;
                anio = anio+1;
            }
            dia = fechaInicio.getDate();
            diaBase = consultaMovHistCDASPID.diasMes[mes];
            if( mes === 1 && anio % 4 === 0 && ((anio % 100 !== 0) || (anio % 400 === 0)) ){
                diaBase =29;
            }
            if(dia>diaBase){
                dFecha = new Date(anio,mes,diaBase);
            }else{
                dFecha = new Date(anio,mes,dia);
            }
            return !(fechaFin.getTime()>dFecha.getTime());
        },

        inicioMovHist:function(){
            consultaMovHistCDASPID.inicioMovHist2();
            consultaMovHistCDASPID.inicioMovHist3();
            $('#paramMontoPago').keypress(function(e){
                var cad;
                var key;
                if(document.all){
                    key = e.keyCode;
                } else {
                    key = e.which;
                }
                consultaMovHistCDASPID.cancel(e);
                cad = $('#paramMontoPago').val()+String.fromCharCode(key);
                if(!(cad ==="") && !consultaMovHistCDASPID.regExpMontoPago.test(cad) && key !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        },
        inicioMovHist2:function(){
            createCalendarGuion('paramFechaOpeInicio','cal1');
            createCalendarGuion('paramFechaOpeFin','cal2');
            createCalendarTimeHrMn('paramHrAbonoIni','cal3');
            createCalendarTimeHrMn('paramHrAbonoFin','cal4');
            createCalendarTimeHrMn('paramHrEnvioIni','cal5');
            createCalendarTimeHrMn('paramHrEnvioFin','cal6');

            buscarMov();

            abrirDetallesContingencia();

            $('#idActualizar').click(function(){
                document.getElementById('paginaIni').value = '1';
                document.getElementById('pagina').value = '1';
                document.getElementById('accion').value = 'ACT';
                hacerSubmit('idForm', 'consMovHistCDASPID.do');
            });

            $('#idExportar').click(function(){
                document.getElementById('accion').value = 'ACT';
                hacerSubmit('idForm', 'expConsMovHistCDASPID.do');
            });

            $('#idExportarTodo').click(function(){
                hacerSubmit('idForm','expTodosConsMovHistCDASPID.do');
            });
            
            $('#idLimpiar').click(function(){
                hacerSubmit('idForm', 'muestraConsMovHistCDASPID.do');
            });

            $('#paramCveSpeiOrdenanteAbono').keypress(function(e){
                var cadSPID;
                var key;

                if(document.all){
                    key = e.keyCode;
                } else {
                    key = e.which;
                }
                
                consultaMovHistCDASPID.cancel2(e);
                cadSPID = $('#paramCveSpeiOrdenanteAbono').val()+String.fromCharCode(key);
                if(!(cadSPID ==="") && !consultaMovHistCDASPID.regExpCveSpei.test(cadSPID) && key !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        },
        inicioMovHist3:function(){
            if(!Utilerias.isEmpty('paramOpeContingencia')){
                $('#opeContingencia').attr("checked", $('#paramOpeContingencia').val());
            }
            
            $('#paramTipoPago').keypress(function(e){
                var cad;
                var key;

                if(document.all){
                    key = e.keyCode;
                } else {
                    key = e.which;
                }

                consultaMovHistCDASPID.cancel2(e);
                cad = $('#paramTipoPago').val()+String.fromCharCode(key);
                if(!(cad ==="") && !consultaMovCDA.regExpNum.test(cad) && key !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });  
        },
        cancel:function(e){
            if(document.all){
                key = e.keyCode;
            } else {
                key = e.which;
            }
            var bandKey = (key === 8 || key===46|| key===44 );
            if( !(bandKey || (key>=48 && key<=57)) ){
                e.preventDefault();
                e.stopPropagation();
            }else{
                consultaMovHistCDASPID.cancel_(key,e);
            }
        },
        cancel_:function(key,e){
            if(key===107|| key===109 || key=== 111 || key===18){
                e.preventDefault();
                e.stopPropagation();
            }
        },
        cancel2:function(e){
            if(document.all){
                key = e.keyCode;
            } else {
                key = e.which;
            }
            if(!((key === 8 || key===190 ) || (key>=48 && key<=57)) ){
                e.preventDefault();
                e.stopPropagation();
            }else if(key===107|| key===109 || key=== 111 || key===18){
                e.preventDefault();
                e.stopPropagation();
            }
        },

        despliegaDetalle:function(referencia,fechaOpe,cveMiInstituc,cveSpei,folioPaq,folioPago,fchCDA){
            document.getElementById('referencia').value = referencia;
            document.getElementById('fechaOpeLink').value = fechaOpe;
            document.getElementById('cveMiInstitucLink').value = cveMiInstituc;
            document.getElementById('cveSpeiLink').value = cveSpei;
            document.getElementById('folioPaqLink').value = folioPaq;
            document.getElementById('folioPagoLink').value = folioPago;
            document.getElementById('fchCDALink').value = fchCDA;
            document.getElementById('accion').value = 'ACT';
            $('#idForm').attr('action','consMovHistDetCDASPID.do');
            $('#idForm').submit();
        },

        validaCampoMonto:function(idMonto){
             var monto = $('#'+idMonto).val();
             return consultaMovHistCDASPID.regExpMontoPagoValida.test(monto);
        }
    };

$(document).ready(consultaMovHistCDASPID.inicioMovHist);

function buscarMov() {
    $('#idBuscar').click(function(){
        if(!Utilerias.isEmpty('paramMontoPago') && !consultaMovHistCDASPID.validaCampoMonto('paramMontoPago')){
            jAlert(mensajesMov["ED00027V"],mensajesMov["aplicacion"],'ED00027V','');
            return;
        }
        var codigo=consultaMovHistCDASPID.validarPeriodoOperacion('paramFechaOpeInicio','paramFechaOpeFin');
        if(codigo.length===0) {
            codigo= consultaMovHistCDASPID.validarPeriodoHrs('paramHrAbonoIni','paramHrAbonoFin');
            if(codigo.length===0) {
                codigo= consultaMovHistCDASPID.validarPeriodoHrs('paramHrEnvioIni','paramHrEnvioFin');
                if(codigo.length===0){
                    $('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
                    $('#fechaOpeInicio').val($('#paramFechaOpeInicio').val());
                    $('#fechaOpeFin').val($('#paramFechaOpeFin').val());
                    $('#hrAbonoIni').val($('#paramHrAbonoIni').val());
                    $('#hrAbonoFin').val($('#paramHrAbonoFin').val());
                    $('#hrEnvioIni').val($('#paramHrEnvioIni').val());
                    $('#hrEnvioFin').val($('#paramHrEnvioFin').val());
                    $('#cveSpeiOrdenanteAbono').val($('#paramCveSpeiOrdenanteAbono').val());
                    $('#nombreInstEmisora').val($('#paramNombreInstEmisora').val());
                    $('#cveRastreo').val($('#paramCveRastreo').val());
                    $('#ctaBeneficiario').val($('#paramCtaBeneficiario').val());
                    $('#montoPago').val($('#paramMontoPago').val());
                    $('#tipoPago').val($('#paramTipoPago').val());
                    hacerSubmit('idForm', 'consMovHistCDASPID.do');
                }
                else {
                    jError(mensajesMov[codigo],mensajesMov["aplicacion"],codigo,'');
                }
            }
            else {
                jError(mensajesMov[codigo],mensajesMov["aplicacion"],codigo,'');
            }
        }
        else {
            jError(mensajesMov[codigo],mensajesMov["aplicacion"],codigo,'');
        }
    });
}

function abrirDetallesContingencia() {
    $('#opeContingencia').change(function() {
        if ($('#opeContingencia').is(":checked")) {
            $("#selectEstatus option[value="+ 0 +"]").attr("selected",true);
            $('#selectEstatus').attr('disabled',true);
            $('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
            }else {
                $('#selectEstatus').attr('disabled',false);
                $('#paramOpeContingencia').val("");
            }
        }
    );
}