consultaMovCDA = {
	regExpCveSpei : new RegExp("^([0-9]{1,6})$"),
	regExpNum : new RegExp("^([0-9]{1,20})$"),
	regExpMontoPago : new RegExp(
			"^(([0-9]{1,3}){1,1}([,]{1}(?!(,|[0-9]{1,2},|/.|[0-9]{1,2}/.))[0-9]{0,3}){0,}([/.]{1}[0-9]{0,2}){0,1})$"),
	regExpMontoPagoValida : new RegExp(
			"^(([0-9]{1,3}){1,1}([,]{1}[0-9]{3,3}){0,4}([/.]{1}[0-9]{1,2}){0,1})$"),
	despliegaDetalle : function(referencia, fechaOpe, cveMiInstituc, cveSpei,
			folioPaq, folioPago) {
		$('#referencia').val(referencia);
		$('#fechaOpeLink').val(fechaOpe);
		$('#cveMiInstitucLink').val(cveMiInstituc);
		$('#cveSpeiLink').val(cveSpei);
		$('#folioPaqLink').val(folioPaq);
		$('#folioPagoLink').val(folioPago);
		$('#paramConsulta').val(1);
		$('#accion').val('ACT');
		$('#idForm').attr('action', 'consMovDetCDA.do');
		$('#idForm').submit();
	},

	cancel : function(ev) {
		document.all ? k = ev.keyCode : k = ev.which;
		if (!((k === 8 || k === 190) || (k >= 48 && k <= 57))) {
			ev.preventDefault();
			ev.stopPropagation();
		} else if (k === 107 || k === 109 || k === 111 || k === 18) {
			ev.preventDefault();
			ev.stopPropagation();
		}
	},
	cancel2 : function(e) {
		document.all ? k = e.keyCode : k = e.which;
		if (consultaMovCDA.validarCaracteres(k)) {
			e.preventDefault();
			e.stopPropagation();
		} else if (k === 107 || k === 109 || k === 111 || k === 18) {
			e.preventDefault();
			e.stopPropagation();
		}
	},
	validarCaracteres : function(k) {
		if (!((k === 8 || k === 46 || k === 44) || (k >= 48 && k <= 57))) {
			return true;
		} else {
			return false;
		}
	},
	validarCampoHrAbono : function() {
		if (Utilerias.isEmpty('hrAbonoIniDesde')
				&& !(Utilerias.isEmpty('hrAbonoFinHasta'))) {
			return true;
		} else {
			return false;
		}
	},
	validarCampoHrEnvio : function() {
		if (Utilerias.isEmpty('hrEnvioIniDesde')
				&& !(Utilerias.isEmpty('hrEnvioFinHasta'))) {
			return true;
		} else {
			return false;
		}
	},
	validarCampoHrAbonoHasta : function() {
		if (!(Utilerias.isEmpty('hrAbonoIniDesde'))
				&& Utilerias.isEmpty('hrAbonoFinHasta')) {
			return true;
		} else {
			return false;
		}
	},

	init : function() {

		createCalendarTime('hrAbonoIniDesde', 'cal1');
		createCalendarTime('hrAbonoFinHasta', 'cal2');
		createCalendarTime('hrEnvioIniDesde', 'cal3');
		createCalendarTime('hrEnvioFinHasta', 'cal4');

		$('#cveSpeiOrdenanteAbono').keypress(
				function(e) {
					var cad;
					var k;
					document.all ? k = e.keyCode : k = e.which;
					consultaMovCDA.cancel(e);
					cad = $('#cveSpeiOrdenanteAbono').val()
							+ String.fromCharCode(k);
					if (!(cad === "")
							&& !consultaMovCDA.regExpCveSpei.test(cad)
							&& k !== 8) {
						e.preventDefault();
						e.stopPropagation();
					}
				});

		$('#refTransfer').keypress(
				function(ev) {
					var cad;
					var l;

					document.all ? l = ev.keyCode : l = ev.which;
					consultaMovCDA.cancel(ev);
					cad = $('#refTransfer').val() + String.fromCharCode(l);
					if (!(cad === "") && !consultaMovCDA.regExpNum.test(cad)
							&& l !== 8) {
						ev.preventDefault();
						ev.stopPropagation();
					}
				});

		$('#tipoPago').keypress(
				function(e) {
					var cad;
					var k;
					document.all ? k = e.keyCode : k = e.which;
					consultaMovCDA.cancel(e);
					cad = $('#tipoPago').val() + String.fromCharCode(k);
					if (!(cad === "") && !consultaMovCDA.regExpNum.test(cad)
							&& k !== 8) {
						e.preventDefault();
						e.stopPropagation();
					}
				});

		$('#montoPago').keypress(
				function(e) {
					var cad;
					var k;
					document.all ? k = e.keyCode : k = e.which;
					consultaMovCDA.cancel2(e);
					cad = $('#montoPago').val() + String.fromCharCode(k);
					if (!(cad === "")
							&& !consultaMovCDA.regExpMontoPago.test(cad)
							&& k !== 8) {
						e.preventDefault();
						e.stopPropagation();
					}
				});

		$("#selectEstatus option[value=" + $('#paramEstatusCda').val() + "]")
				.attr("selected", true);
		$('#opeContingencia').attr("checked", $('#paramOpeContingencia').val());

		if ($('#opeContingencia').is(":checked")) {
			$("#selectEstatus option[value=" + 0 + "]").attr("selected", true);
			$('#selectEstatus').attr('disabled', true);
		} else {
			$('#selectEstatus').attr('disabled', false);
		}
		if ($("#selectEstatus").val() !== ''
				&& $("#selectEstatus").val() !== undefined) {
			$('#opeContingencia').attr('disabled', true);
		} else {
			$('#opeContingencia').attr('disabled', false);
		}

		$('#opeContingencia').change(
				function() {
					if ($('#opeContingencia').is(":checked")) {
						$("#selectEstatus option[value=" + 0 + "]").attr(
								"selected", true);
						$('#selectEstatus').attr('disabled', true);
						$('#paramOpeContingencia').val(
								$('#opeContingencia').is(":checked"));
					} else {
						$('#selectEstatus').attr('disabled', false);
						$('#paramOpeContingencia').val("");
					}
				});

		$('#selectEstatus').change(
				function() {

					if ($("#selectEstatus").val() !== ''
							&& $("#selectEstatus").val() !== undefined) {
						$('#opeContingencia').attr('disabled', true);
					} else {
						$('#opeContingencia').attr('disabled', false);
					}
					$('#paramEstatusCda').val($('#selectEstatus').val());
				});

		$('#idBuscar').click(
				function() {
					strHoraAbonoIni = $('#hrAbonoIniDesde').val();
					strHoraAbonoFin = $('#hrAbonoFinHasta').val();
					strHoraEnvioIni = $('#hrEnvioIniDesde').val();
					strHoraEnvioFin = $('#hrEnvioFinHasta').val();
					tHoraAbonoIni = Utilerias.stringToTime(strHoraAbonoIni);
					tHoraAbonoFin = Utilerias.stringToTime(strHoraAbonoFin);
					tHoraEnvioIni = Utilerias.stringToTime(strHoraEnvioIni);
					tHoraEnvioFIn = Utilerias.stringToTime(strHoraEnvioFin);

					if (!Utilerias.isEmpty('montoPago')
							&& !consultaMovCDA.validaCampoMonto('montoPago')) {
						jAlert(mensajes["ED00027V"], mensajes["aplicacion"],
								'ED00027V', '');
					} else if (consultaMovCDA.validarCampoHrAbono()) {
						jAlert(mensajes["ED00023V"], mensajes["aplicacion"],
								'ED00023V', '');
					} else if (consultaMovCDA.validarCampoHrEnvio()) {
						jAlert(mensajes["ED00023V"], mensajes["aplicacion"],
								'ED00023V', '');
					} else if (consultaMovCDA.validarCampoHrAbonoHasta()) {
						jAlert(mensajes["ED00023V"], mensajes["aplicacion"],
								'ED00023V', '');
					} else if (!(Utilerias.isEmpty('hrEnvioIniDesde'))
							&& Utilerias.isEmpty('hrEnvioFinHasta')) {
						jAlert(mensajes["ED00023V"], mensajes["aplicacion"],
								'ED00023V', '');
					} else if (tHoraAbonoFin.getTime() < tHoraAbonoIni
							.getTime()) {
						jAlert(mensajes["ED00022V"], mensajes["aplicacion"],
								'ED00022V', '');

					} else if (tHoraEnvioFIn.getTime() < tHoraEnvioIni
							.getTime()) {
						jAlert(mensajes["ED00022V"], mensajes["aplicacion"],
								'ED00022V', '');
					} else {
						$('#paramHoraAbonoIni')
								.val($('#hrAbonoIniDesde').val());
						$('#paramHoraAbonoFin')
								.val($('#hrAbonoFinHasta').val());
						$('#paramHoraEnvioIni')
								.val($('#hrEnvioIniDesde').val());
						$('#paramHoraEnvioFin')
								.val($('#hrEnvioFinHasta').val());
						$('#paramCveSpei').val(
								$('#cveSpeiOrdenanteAbono').val());
						$('#paramDescripcion').val($('#desc').val());
						$('#paramCveRastreo').val($('#cveRastreo').val());
						$('#paramRefTransfer').val($('#refTransfer').val());
						$('#paramCtaBeneficiario').val(
								$('#ctaBeneficiario').val());
						$('#paramMonto').val($('#montoPago').val());
						$('#paramTipoPago').val($('#tipoPago').val());
						$('#idForm').attr('action', 'consMovCDA.do');
						$('#idForm').submit();
					}
					/*
					 * }else{ $('#idForm').attr('action', 'consMovCDA.do');
					 * $('#idForm').submit(); }
					 */

				});

		$('#idActualizar').click(function() {
			$('#paginaIni').val('1');
			hacerSubmit('idForm','consMovCDA.do');
		});

		$('#idExportar').click(function() {
			$('#accion').val('ACT');
			hacerSubmit('idForm','expConsMovCDA.do');
		});

		$('#idExportarTodo').click(function() {
			hacerSubmit('idForm','expTodosConsMovCDA.do');
		});

		$('#idLimpiar').click(function() {
			$('#idForm').find('input').each(function() {
				switch (this.type) {
				case 'text':
					$(this).val('');
					break;
				case 'checkbox':
					this.checked = false;
					break;
				default:
				}

			});
			$('#idForm').attr('action', 'muestraConsMovCDA.do');
			$('#idForm').submit();
		});
	},

	/*
	 * validarCamposVacios:function (){ var cont = 0; var selectEstatus =
	 * $("#selectEstatus").val();
	 * 
	 * if(Utilerias.isEmpty('hrAbonoIniDesde')){ cont++; }
	 * if(Utilerias.isEmpty('hrAbonoFinHasta')){ cont++; }
	 * if(Utilerias.isEmpty('hrEnvioIniDesde')){ cont++; }
	 * if(Utilerias.isEmpty('hrEnvioFinHasta')){ cont++; }
	 * if(Utilerias.isEmpty('cveSpeiOrdenanteAbono')){ cont++; }
	 * if(Utilerias.isEmpty('desc')){ cont++; }
	 * if(Utilerias.isEmpty('cveRastreo')){ cont++; }
	 * if(Utilerias.isEmpty('refTransfer')){ cont++; }
	 * if(Utilerias.isEmpty('ctaBeneficiario')){ cont++; }
	 * if(Utilerias.isEmpty('montoPago')){ cont++; }
	 * if(!$('#"opeContingencia"').is(':checked')){ cont++; } if(selectEstatus
	 * === 0){ cont++; } return cont; },
	 */

	validaCampoMonto : function(idMonto) {
		var monto = $('#' + idMonto).val();
		return consultaMovCDA.regExpMontoPagoValida.test(monto);
	}
};

$(document).ready(consultaMovCDA.init);