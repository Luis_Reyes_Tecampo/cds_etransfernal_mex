/** Declracion de variables globales **/
const SORT = '#sortType';
/** Declaracion de la funcion general **/
var historialMensajes = {
    init:function(){ 
    
      /** Evento seleccionar todos **/
      $('#chkTodos').click(function(){       
          var objCheckBoxes = $( "[type=checkbox]" );
          if($('#chkTodos').is(':checked')){
            for(var i=0;i<objCheckBoxes.length;i++){
                  objCheckBoxes[i].checked = true;
                
            }
          } else{
            for(var j=0;j<objCheckBoxes.length;j++){
               objCheckBoxes[j].checked = false;
            }
          }
       });
      /** Evento buscar**/
      $('#idBuscar').click(function(){
          $('#paginaIni').val('1');
          $('#idForm').attr('action', 'historial.do?ref=' + referencia + '&tipo=' + tipoOrden);
          $('#idForm').submit();
      });
      $('#paginaIni').val('1');
      
      
      /** Evento de ordenamiento **/
      $('.sortField').click(function(){
         if ($('#sortField').val() !== $(this).attr('data-id')){
            $(SORT).val('');
         }
         
         $('#sortField').val($(this).attr('data-id'));
         $(SORT).val($(SORT).val() === 'ASC' ? 'DESC' : 'ASC');
         $('#idForm').attr('action', 'historial.do?ref=' + referencia + '&tipo=' + tipoOrden);
         $('#idForm').submit();
      });
      
      /** Evento regresar **/
      $('#idRegresar').click(function(){       
         $('#tipoRef').val(tipoOrden);
         $('#ref').val(referencia);
         $('#idForm').attr('action', 'consultaDetallePago.do');
         $('#idForm').submit();
      });
    }    
};
/** Inicializacion de la funcion general **/
$(document).ready(historialMensajes.init);