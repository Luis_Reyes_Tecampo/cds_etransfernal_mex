var monitorCargaArchResp = {
    init:function(){

        $('#idActualizar').click(function(){
            var modulo = $('#idActualizar').hasClass('spid');
            var acceso;
            /** Validación del módulo **/
            if (!modulo){
                acceso = 1;
            }
            if (modulo) {
                acceso = 2;
            }
            $('#idForm').attr('action', 'consMonCargaArchResp.do');
            $('#accion-p').val(acceso);
            $('#idForm').submit();
        });

    }
};
/** Inicializacion de las funciones **/
$(document).ready(monitorCargaArchResp.init);