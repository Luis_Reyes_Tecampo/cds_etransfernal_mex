/** Funciones para capturar los eventos de los monitores **/
const MONITOR_FIELD = '#monitor';

/** Funcion que valida el tipo de monitor al que se esta
 * accediendo **/
function valores() {
    /** Condicion para el modulo SPEI **/
    if ($('#bmModulo').val() === 'spei'){
        $('#moduloTitle').text($('#moduloPOACOASPEImt').val());
        $('#transpaso1').text($('#txtTraspasoSIACSPEI').val());
        $('#transpaso2').text($('#txtTraspasoSPEISIAC').val());
        $('#TrasEspera').text($('#txtTraspasoSPEISIACEsp').val());
        $('#TrasEnviados').text($('#txtTraspasoSPEISIACEnv').val());
        $('#TrasReparar').text($('#txtTraspasoSPEISIACRep').val());
        /** Condicion POA y COA **/
        if ($('#bmTipo').val() === "coa"){
            $(MONITOR_FIELD).text($('#lblMonitorCOAspei').val());
        } else {
            $(MONITOR_FIELD).text($('#lblMonitorPOAspei').val());
        }
        /** Declarativa para SPID **/
    } else {
        $('#moduloTitle').text($('#moduloPOACOASPIDmt').val());
        $('#transpaso1').text($('#txtTraspasoSIACSPID').val());
        $('#transpaso2').text($('#txtTraspasoSPIDSIAC').val());
        $('#TrasEspera').text($('#txtTraspasoSPIDSIACEsp').val());
        $('#TrasEnviados').text($('#txtTraspasoSPIDSIACEnv').val());
        $('#TrasReparar').text($('#txtTraspasoSPIDSIACRep').val());
        /** Condicion POA y COA **/
        if ($('#bmTipo').val() === "coa"){
            $(MONITOR_FIELD).text($('#lblMonitorCOAspid').val());
        } else {
            $(MONITOR_FIELD).text($('#lblMonitorPOAspid').val());
            
        }
    }
}

/** Funcion para cargar el titulo en pantalla **/
function cargaTitulo(pantalla,submodulo) {
    var titulo = '';
    
    if ($('#bmModulo').val() === 'spei'){
        titulo = $('#moduloPOACOASPEImt').val();
    } else {
        titulo = $('#moduloPOACOASPIDmt').val();
    }
    $('#moduloTitle').text(titulo);
    cargaSubtitulo(pantalla,submodulo);
}
/** Funcion para cargar el subitulo en pantalla**/
function cargaSubtitulo(pantalla,submodulo) {
    var subtitulo = pantalla;
    if ($('#bmModulo').val() === 'spei'){
        subtitulo = subtitulo.replace('SPID', 'SPEI');
    }
    if (submodulo) {
        $(MONITOR_FIELD).text(subtitulo);
    }
}
/** Funcion para validar y acomodar la fecha con 
 * espacios en la pantalla **/
function acomodaFecha() {
    var tope = 140;
    var topeInicial = 80;
    var topeIntermedio = $(MONITOR_FIELD).text();
    var longTopeInterm = topeIntermedio.length;
    var diferencia;
    var cadenaEspacios = '';
    var i = 0;
    var espacio = String.fromCharCode(160);
    topeInicial += longTopeInterm;
    diferencia = tope - topeInicial;
    while (i < diferencia) {
        cadenaEspacios += espacio;
        i++;
    }
    $('#espaciosBlanco').text(cadenaEspacios);
}

