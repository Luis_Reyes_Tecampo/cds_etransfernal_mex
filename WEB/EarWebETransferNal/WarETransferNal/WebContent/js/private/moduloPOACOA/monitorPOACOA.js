/** Declaracion de la funcion general **/
var monitorPOACOA = {
    init:function() {
    
    
    /** Evento del campo ordRecibidasPorDevolver **/
    $('.ordRecibidasPorDevolver').click(function(){
    /** SE valida Operacion*/
    if (validaCero(this)) {    
    $('#tipoRef').val('OPD');
    $('#idForm').attr('action','consultaPagos.do');
    $('#idForm').submit();
    }
    });

    /** Evento del campo traspasoTipoSIAC **/
    $('.traspasoTipoSIAC').click(function(){    
    if (validaCero(this)) {    
    
    $('#idForm').attr('action','consultaPagos.do');
    /** Se establece el valor TSS*/
    $('#tipoRef').val('TSS');
    $('#idForm').submit();
    }
    });

    /** Evento actualizar **/
    $('#idActualizar').click(function(){    
        $('#idForm').attr('action',$('#path').val().substring(1));
        $('#idForm').submit();
    });
    
    /** Evento del campo ordRecibidasRechazadas **/
    $('.ordRecibidasRechazadas').click(function(){   
    if (validaCero(this)) {    

    $('#idForm').attr('action','consultaRecepcionesConting.do');
    $('#ref').val('RE');
    /** Se envia el formulario*/
    $('#idForm').submit();
    }
    });
    
    /** Evento del campo ordRecibidasAplicadas **/
    $('.ordRecibidasAplicadas').click(function(){
    if (validaCero(this)) {   
    $('#ref').val('TR');
    /** Se agrega accion de POA COA*/
    $('#idForm').attr('action','consultaRecepcionesConting.do');
    $('#idForm').submit();
    }
    });
    
    /** Evento del campo ordRecibidasPorAplic **/
    $('.ordRecibidasPorAplic').click(function(){
    if (validaCero(this)) {

    /** Se valida la accion de poa coa*/
    $('#idForm').attr('action','consultaRecepcionesConting.do');
    /** Se valida la referencia PA*/
    $('#ref').val('PA');
    $('#idForm').submit();
    }
    });
    /** Evento del campo devolucionesEnvConf **/
    $('.devolucionesEnvConf').click(function(){    
    if (validaCero(this)) {
    /** se valida la opcion dec*/
    $('#tipoRef').val('DEC');
    $('#idForm').attr('action','consultaPagos.do');
    $('#idForm').submit();
    
    }
    });
    
    
    
    /** Evento del campo traspasosSIACTipo **/
    $('.traspasosSIACTipo').click(function(){
    if (validaCero(this)) {
    $('#idForm').attr('action','consultaAvisosTraspasos.do');
    $('#idForm').submit();
    }
    });
    
    /** Evento del campo ordEnviadasConf **/
    $('.ordEnviadasConf').click(function(){ 
    if (validaCero(this)) {
    
    $('#tipoRef').val('OEC'); 
    $('#idForm').attr('action','consultaPagos.do');
    $('#idForm').submit();
    }
    });
    
    events();
    }
};

/** Funcion que continua los eventos **/
function events() {
/** Evento del campo traspasosSIACTipoEnviados **/
    $('.traspasosSIACTipoEnviados').click(function(){   
    /** Se valida laoperacion enviados*/
    if (validaCero(this)) {
    $('#idForm').attr('action','consultaPagos.do');
    $('#tipoRef').val('TEN');
    $('#idForm').submit();
    }
    });
    
	/** Evento del campo traspasosSIACTipoEspera **/
    $('.traspasosSIACTipoEspera').click(function(){    
    if (validaCero(this)) {
    
    $('#tipoRef').val('TES');
    /** Se establece la accion del formulario*/
    $('#idForm').attr('action','consultaPagos.do');
    $('#idForm').submit();
    }
    });
    
    
    /** Evento del campo ordenesEspera **/
    $('.ordenesEspera').click(function(){   
    if (validaCero(this)) {
    $('#idForm').attr('action','consultaPagos.do');
    $('#tipoRef').val('OES');
    /** Se envia la operacion del formulario*/
    $('#idForm').submit();
    }
    }); 
    
    /** Evento del campo traspasosSIACTipoReparar **/
    $('.traspasosSIACTipoReparar').click(function(){  
    if (validaCero(this)) {
    
    $('#tipoRef').val('TPR');
    /** Se establece la accion en el formulario*/
    $('#idForm').attr('action','consultaPagos.do');
    $('#idForm').submit();
    }
    });
    
    /** Evento del campo ordenesEnviadas **/
    $('.ordenesEnviadas').click(function(){
/** Se valdia la opcion de la operacion*/
    if (validaCero(this)) {
    $('#idForm').attr('action','consultaPagos.do');
    $('#tipoRef').val('OEN');
    /** Se envia el formulario*/
    $('#idForm').submit();
    }
    });
    /** Evento del campo ordenesReparar **/
    $('.ordenesReparar').click(function(){  
    if (validaCero(this)) {
    $('#idForm').attr('action','consultaOrdenesReparacion.do');
    $('#idForm').submit();
    }
    });
}
/** Funcion para bloquear el campo cuando el valor
 * este en cero **/
function validaCero(obj) {
    var valor = obj.value;
    if (valor === 0) {
        return false;
    }
    return true;
}
/** Inicializacion de la funcion general **/
$(document).ready(monitorPOACOA.init);