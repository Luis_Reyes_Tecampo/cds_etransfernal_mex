/** Declaracion de las variables globales **/
const SORT = '#sortType';
/** Declaracion de la funcion general **/
var consultaRecepciones = {
    init:function(){ 
/** Evento exportar todos **/
        $('#idExportarTodo').click(function(){
      	  $('#accion').val('ACT');
      	  $('#idForm').attr('action', 'exportarTodoRecepciones.do');
      	  /** Se envia el formulario*/
            $('#idForm').submit();
        });
/** Evento Detalle**/
        $('.idVerDetalle').click(function(){
            var parentTr = $(this).parent().parent();

            /** Se declaran valores*/
            $('#fchOperacion').val($('td.campos input.fchOperacion', parentTr).val());
            $('#cveMiInstituc').val($('td.campos input.cveMiInstituc', parentTr).val());              
            $('#folioPago').val($('td.campos input.folioPago', parentTr).val());       
            /** Se declara el folio*/
            $('#folioPaquete').val($('td.campos input.folioPaquete', parentTr).val());       
            $('#cveInstOrd').val($('td.campos input.cveInstOrd', parentTr).val());
            /** Se llama a la accion del formulario*/
            $('#idForm').attr('action','detalleRecepcion.do');
            /** Se envia el formulario*/
            $('#idForm').submit();
         });

    	
        

      /** Evento exportar para las recepciones **/
      $('#idExportar').click(function(){
          $('#idForm').attr('action', 'exportarRecepciones.do');
          $('#idForm').submit(); 
      });
      
     
      /** Evento de ordenamiento **/
      $('.sortField').click(function(){
    	 $('#idForm').attr('action', 'consultaRecepcionesConting.do');
         if ($('#sortField').val() !== $(this).attr('data-id')){
            $(SORT).val('');
         }
         
         $('#sortField').val($(this).attr('data-id'));
         $(SORT).val($(SORT).val() === 'ASC' ? 'DESC' : 'ASC');         
         $('#idForm').submit();
      });
      
      /** Evento actualizar para las recepciones **/
      $('#idActualizar').click(function(){            
          $('#paginaIni').val('1');
          /** Se delara la acccion a realizar*/
          $('#idForm').attr('action', 'consultaRecepcionesConting.do');
          /** Se envia el formulario*/
          $('#idForm').submit();
       });
      
      /** Evento regresar **/
      $('#idRegresar').click(function(){
         $('#idForm').attr('action', $('#path').val().substring(1));
         $('#idForm').submit();
      });
    }
};
/** Inicializacion de la funcion general **/
$(document).ready(consultaRecepciones.init);