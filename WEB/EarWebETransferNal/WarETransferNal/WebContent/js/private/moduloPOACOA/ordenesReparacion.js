/** Declaracion de las variables locales **/
const SORT = '#sortType';
/** Declaracion de funcion enviar **/
var enviar = function(){
    var val = $('input[type="checkbox"]:checked').val();
    
    if(val === undefined || val === null || val.length === 0){
        jAlert("Se requiere palomear un registro",
            mensajes["aplicacion"],
            'ED00068V',
          '');
        return false;
    }
       jConfirm(mensajes["CD00167V"], mensajes["aplicacion"], 'CD00167V','', function (respuesta){
           if (respuesta){
            $('#paginaIni').val('1');
            $('#idForm').attr('action', 'enviarAReparacion.do');
            $('#idForm').submit();
           }
       });    
     return true;
};
/** Declaracion de variable todos **/
var    seleccionarTodos = function(){
        var objCheckBoxes = $( "[type=checkbox]" );
        if($('#chkTodos').is(':checked')){
           for(var i=0;i<objCheckBoxes.length;i++){
                  objCheckBoxes[i].checked = true;
                
           }
        } else{
           for(var j=0;j<objCheckBoxes.length;j++){
              objCheckBoxes[j].checked = false;
           }
        }
    };

/** Se declara la funcion general **/
var ordenesReparacion = {
    /** Declaracion de las funciones**/
    init:function(){ 

       /** Evento de exportar todos **/
       $('#idExportarTodo').click(function(){
            
            $('#idForm').attr('action', 'exportarTodoOrdenesReparacion.do');
            $('#accion').val('ACT');
            $('#idForm').submit(); 
       });
       
       /** Evento enviar **/
       $('#idEnviar').click(enviar);
       
       /** Evento exportar **/
       $('#idExportar').click(function(){
            $('#idForm').attr('action', 'exportarOrdenesReparacion.do');
            $('#idForm').submit(); 
       });
       
       
       /** Evento regresar**/
       $('#idRegresar').click(function(){
           $('#idForm').attr('action', $('#path').val().substring(1));
            $('#idForm').submit();
        });
       /** Evento actualizar **/
       $('#idActualizar').click(function(){
               $('#paginaIni').val('1');
            $('#idForm').attr('action', 'consultaOrdenesReparacion.do');
            $('#idForm').submit();
        });
       
       /** Evento ordenar**/
       $('.sortField').click(function(){
           if ($('#sortField').val() !== $(this).attr('data-id')){
               $(SORT).val('');
           }
           
           $('#sortField').val($(this).attr('data-id'));
           $(SORT).val($(SORT).val() === 'ASC' ? 'DESC' : 'ASC');
           $('#idForm').attr('action', 'consultaOrdenesReparacion.do');
           $('#idForm').submit();
       });
       /** Evento de seleccionar todos **/
       $('#chkTodos').click(seleccionarTodos);
    }
};
/** Inicializacion de las funciones **/
$(document).ready(ordenesReparacion.init);