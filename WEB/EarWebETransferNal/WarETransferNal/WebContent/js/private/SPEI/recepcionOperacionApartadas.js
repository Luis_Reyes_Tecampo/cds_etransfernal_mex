var lista = [];
var listaTodo = [];
var recepcionOperacionApartadas = {
    lock:0,
    init:function(){	
    	//funcion para liberar los registros
    	$('#idLiberarTodo').click(function(){
    		//variable que obtiene la referencias
			var eliminarTodo = $(this).attr("ref");
			//asigna valor al input
			$('#eliminarTodo').val(eliminarTodo);			
			recepcionOperacionApartadas.comun();
			//llama la funcion de liberar 
			recepcionOperacionApartadas.liberarTodo();		    
		});			
		//libera de uno o dos registros
		$('#idLiberar').click(function(){	
			//variable que obtiene la referencias
			var eliminar = $(this).attr("ref");
			//asigna valor al input
			$('#eliminarTodo').val(eliminar);
			recepcionOperacionApartadas.comun();
			//llama la funcion de liberar 
			recepcionOperacionApartadas.liberar();		
		});	
		//envia el registro a autorizar
		$('#idEnvio').click(function(){
			recepcionOperacionApartadas.idEnvio();
		});
		//funcion para rechazar la operacion
		$('#idActMotRechazo').click(function(){
			//obtiene si el usuario aparta la operacion
			var continu = recepcionOperacionApartadas.validarUsuAparta(2);
			//si es igual a uno
			if(continu === 1){
				if(recepcionOperacionApartadas.lock===0){
					recepcionOperacionApartadas.lock=1;
					//llama la funvion para rechazar
					recepcionOperacionApartadas.motrechazar();					
				}
			}else if(continu===4){
				//mensaje de error
				jAlert(mensajes["ED00029V"],
						mensajes["aplicacion"],
					   	   'ED00029V',
					   	   '');
			}else{ 
				//manda mensaje de acutalizacion
				jAlert(mensajes["ED00135V"]+'Actualizar Motivo Rechazo',
						mensajes["aplicacion"],
					   	   'ED00135V',
					   	   '');
			}
		});
		
		//funcion que muestra el detalle
		$('#idVerDetalle').click(function(){	
			
			//si es igual a uno
			if(lista.length === 1 ){
				//se llama a la funcion del controller
				$('#idForm').attr('action','muestraDetRecepOperacion.do');
				$('#idForm').submit();
			}else {
				//mensaje de error
				jAlert(mensajes["ED00029V"],
						mensajes["aplicacion"],
					   	   'ED00029V',
					   	   '');
			}
			
		});
		
		 //funcion para generar el excel
	    $('#idEnviarHto').on('click', function () {	    	
	    	var seUsu = $('#sessionUsu').val();
	    	var to = recepcionOperacionApartadas.recorrerHto(seUsu.trim());
	    	
	    	if(to == 0 && lista.length == 1){
	    		recepcionOperacionApartadas.enviaHTO();				
			}else{
				
				if(to === 1){
					//mensaje de error
					jAlert(mensajes["ED00131V"],
							mensajes["aplicacion"],
						   	   'ED00131V',
						   	   '');
				} else if(to === 9999) {
					//mensaje de error
					jAlert(mensajes["ED00135V"]+'Enviar Historico',
							mensajes["aplicacion"],
						   	   'ED00135V',
						   	   '');
				} else {
			    	//mensaje de error
					jAlert(mensajes["ED00029V"],
							mensajes["aplicacion"],
						   	   'ED00029V','');
				}
			}
	    });
		//funcion para seleccionar los registros de toda la tabla
		$('#chkTodos').click(function(){		
			recepcionOperacionApartadas.seleccionados();
		});
		//funcion para rechazar
		$('#idRechazar').click(function(){
			//llama a la funcion de rechazar
			recepcionOperacionApartadas.idRechazar();
		});
		$('#idApartar').on('click', function () {
			//se obtiene el usuario
	    	var sesUsu = $('#sessionUsu').val();
	    	var t=0;
			for (var x = 0; x < lista.length; x+=1) {
				var checkUsu = $('#usuario'+lista[x]).val();
				if(checkUsu.trim() != '' && checkUsu.trim() != "undefined" && checkUsu.trim() != null){
					t = recepcionOperacionApartadas.validarApart(t,checkUsu.trim(),sesUsu.trim());
				}				  
			}
			
			//valida que tenga datos seleccionados
		    if(lista.length>=1 && t===0){
		    	recepcionOperacionApartadas.idApartar();
		    }else{
		    	if(t>0){
		    		//mensaje de error
					jAlert(mensajes["ED00131V"],
							mensajes["aplicacion"],
						   	   'ED00131V',
						   	   '');
		    	}else{
			    	//mensaje de error
					jAlert(mensajes["ED00029V"],
							mensajes["aplicacion"],
						   	   'ED00029V',
						   	   '');
		    	}
		    }
		});
		
    },
    validarApart:function(t,checkUsu,sesUsu){
    	if(checkUsu.trim() != sesUsu.trim()){
    		t=t+1;
    	}
    	return t;
	},	
    enviaHTO:function(){
    	jConfirm(mensajes["CD00004V"],mensajes["aplicacion"],'CD00004V','', function (respuesta){
            if(respuesta){
            	//se llama a la funcion del controller
			    $('#idForm').attr('action', 'enviarHto.do');
    		    $('#idForm').submit();
            }
        }); 
    },
    idApartar:function(){
    	jConfirm(mensajes["CD00003V"],mensajes["aplicacion"],'CD00003V','', function (respuesta){
            if(respuesta){
            	//se llama a la funcion del controller
            	$('#idForm').attr('action', 'apartaOperaciones.do');
    		    $('#idForm').submit();
            }
        }); 
	},
	  //incia el rechazo
    idRechazar:function(){
    	//obtiene si el usuario aparta la operacion
		var cont = recepcionOperacionApartadas.validarUsuAparta(2);
		
		//valida que la opcion selecciona se pueda rechazar
		if(cont === 1){
			//funcion para rechazar
			recepcionOperacionApartadas.rechazar();				
			
		}else if(cont===4){
			//mensaje de error
			jAlert(mensajes["ED00029V"],
					mensajes["aplicacion"],
				   	   'ED00029V',
				   	   '');
		}else{ 
			//mensaje de error
			jAlert(mensajes["ED00135V"]+'Rechazar',
					mensajes["aplicacion"],
				   	   'ED00135V',
				   	   '');
		}
    },
    //funcion para enviar
    idEnvio:function(){
    	//obtiene si el usuario aparta la operacion
    	var continuar = recepcionOperacionApartadas.validarUsuAparta(1);		
		if(continuar === 2){
			//manda el mensaje de confirmacion
			jConfirm(mensajes["CD00167V"],mensajes["aplicacion"],'CD00167V','', function (respuesta){
				if(respuesta === true){
					var val = $('[name=opcionParam]:checked').val();
					recepcionOperacionApartadas.envioIni(val);
				} 
			}
			);
		}else if(continuar===1){
			//mensaje de error
			jAlert(mensajes["ED00131V"],
					mensajes["aplicacion"],
				   	   'ED00131V',
				   	   '');
		}else if(continuar===4){
			//mensaje de error
			jAlert(mensajes["ED00029V"],
					mensajes["aplicacion"],
				   	   'ED00029V',
				   	   '');
		}else{ 
			//mensaje de error
			jAlert(mensajes["ED00135V"]+'Enviar a Transferencias',
					mensajes["aplicacion"],
				   	   'ED00135V',
				   	   '');
		}
    },    
    //funcion para liberar los registros de la tabla
    liberarTodo:function(){
    	jConfirm(mensajes["CD00002V"],mensajes["aplicacion"],'CD00002V','', function (respuesta){
            if(respuesta){
            	//se llama a la funcion del controller
            	$('#idForm').attr('action','liberarOperApartadas.do');
    			$('#idForm').submit();
            }
        });
    },
    //funcion para liberar de uno o varios registros
    liberar:function(){
    	//se asigna variable
    	var errores = 0;
    	var usuAparta = 0;
    	var sinApartar = false;
    	//se obtiene el usuario
    	var sesUsu = $('#sessionUsu').val();
    	var usuPer = $('#perfil').val();
    	var arrPerfiles = usuPer.split(",");
    	var noAdmin = true;
    	for(var i = 0; i < arrPerfiles.length; i+=1) {
    		if('grp_appnal_admin' === arrPerfiles[i]) {
    			noAdmin = false;
    		}
    	}
    	//obtiene si el usuario aparta la operacion
    	for (var x = 0; x < lista.length; x+=1) {
			var checkUsu = $('#usuario'+lista[x]).val();
			if(noAdmin && checkUsu.trim() != sesUsu.trim() ){
				errores = errores + 1;
			}
			if(!checkUsu || checkUsu.trim() === "") {
				sinApartar = true;
			}
		}
    	
    	
		
    	//valida que tenga datos seleccionados
	    if(lista.length>=1 && errores===0 && !sinApartar){
	    	//valida que la opcion selecciona se pueda rechazar
			recepcionOperacionApartadas.conLiberar();
	    } else if(sinApartar) {
			//mensaje de error
			jAlert(mensajes["ED00135V"]+'Liberar',
					mensajes["aplicacion"],
				   	   'ED00135V',
				   	   '');
	    } else if(errores > 0) {
	    	//mensaje de error
			jAlert(mensajes["ED00131V"],
					mensajes["aplicacion"],
				   	   'ED00131V',
				   	   '');
	    } else {
		   	//mensaje de error
			jAlert(mensajes["ED00029V"],
					mensajes["aplicacion"],
				   	   'ED00029V',
				   	   '');
	    }
	},	
	//continua liberar
	conLiberar:function(){
		jConfirm(mensajes["CD00001V"],mensajes["aplicacion"],'CD00001V','', function (respuesta){
 		   if(respuesta){
 			   //se llama a la funcion del controller
 	        	$('#idForm').attr('action','liberarOperApartadas.do');
 				$('#idForm').submit();
 	        }
	      });
	},
	//verifica los registros seleccionado
	seleccionados:function(){
		var objCheckBoxes = $( "[type=checkbox]" );
        if($('#chkTodos').is(':checked')){
           for(var i=1;i<objCheckBoxes.length;i++){
	         objCheckBoxes[i].checked = true; 
	         lista.push(i);
           }
           $('.seleccionaReg').css("background-color", "#FFECD2");
        } else{
           for(var j=0;j<objCheckBoxes.length;j++){
              objCheckBoxes[j].checked = false;
           }
           $('.seleccionaReg').css("background-color", "");
           lista = [];
        }
        
	},
    
    comun:function(){
    	var val = $('#opcion').val();	
		$('#opcion').val(val);
	},	
	//funcon de envio
    envioIni:function(){
    	//valida que la opcion sea la correcta
		if(recepcionOperacionApartadas.lock===0){
			recepcionOperacionApartadas.lock=1;
			//envia la fucion
			recepcionOperacionApartadas.envio();
		}
		
	},
	
	
	envio:function(){
		//se llama a la funcion del controller
		$('#idForm').attr('action','transferirSpeiRec.do');
		$('#idForm').submit();
	},
	
	motrechazar:function(){		
		//se llama a la funcion del controller
		$('#idForm').attr('action','actMotRechazo.do');	
		$('#idForm').submit();
	},
	
	rechazar:function(){
		if(recepcionOperacionApartadas.lock===0){
			recepcionOperacionApartadas.lock=1;	
			//se llama a la funcion del controller
			$('#idForm').attr('action','rechazarTransfSpeiRec.do');
			$('#idForm').submit();
		}
	},
	//funcon para devolver la operacion
	devolver:function(index){
		if($('#selectMotivo'+index).val().length == 0) {
			//mensaje de error
		jAlert(mensajes["ED00067V"]+'Rechazar',
				mensajes["aplicacion"],
			   	   'ED00067V',
			   	   '');
		} else {
		recepcionOperacionApartadas.devolucion(index);
		}
	},
	//funcon para devolver la operacion
	devolucion:function(index){
		//mensaje de confirmacion
		jConfirm(mensajes["CD00169V"],mensajes["aplicacion"],'CD00169V','', function (respuesta){		
			if(recepcionOperacionApartadas.lock===0 && respuesta){
				//accion para devolver
				recepcionOperacionApartadas.oprimirBotonDev(index);		
			}
		});
	},
	//funcion para devolucion extemporanea
	devolucionExtemporanea:function(index){
		//mensaje de confirmacion
		jConfirm(mensajes["CD00172V"],mensajes["aplicacion"],'CD00172V','', function (respuesta){		
			if(recepcionOperacionApartadas.lock===0 && respuesta){
				$('#selecDevolucion'+index).val('true');
				$('#cveMiInst').val($('#cveMiInstitucion').val());
				$('#fActual').val($('#fechaActual').val());
				//se llama a la funcion del controller
				$('#idForm').attr('action','devolucionExtemporanea.do');
				$('#idForm').submit();
			}
	
		});
	
	},
	//funcon del boton de devolucion
	oprimirBotonDev:function(index){			
			recepcionOperacionApartadas.lock=1;
			$('#selecDevolucion'+index).val('true');
			//se llama a la funcion del controller
			$('#idForm').attr('action','devolverTransfSpeiRec.do');
			$('#idForm').submit();
		
	},
	
	validarUsuAparta:function(total){
		var sessionUsu = $('#sessionUsu').val();
		var resul;
		if(total === 1 && lista.length ===1){
			resul=recepcionOperacionApartadas.primeraValidacion(sessionUsu.trim());
		}
		
		if((total === 1 || total === 2) && (lista.length >1 ||lista.length===0) ){			
			resul= 4;			
		}
		
		if(total === 2 && lista.length >= 1){
			resul= recepcionOperacionApartadas.recorrer(sessionUsu.trim());
		}
		
		return resul;
	},
	//primera validacion
	primeraValidacion:function(sessionUsu){
		var resul;
		var checkUsu = $('#usuario'+lista[0]).val();
		if(!checkUsu || checkUsu.trim() === ''){
			resul= 3;
		}else if(checkUsu.trim() != sessionUsu.trim() ){
			resul= 1;
		}else{
			resul= 2;
		}
		return resul;
	},
	
	// recorre la lista y regresa el total
	recorrer:function(sessionUsu){
		var tot =0;
		for (var i = 0; i < lista.length; i+=1) {
			var checkUs = $('#usuario'+lista[0]).val();
			if(checkUs.trim() === sessionUsu.trim() ){
				tot = tot+1;
			}				  
		}
		
		if(tot===lista.length){
			return 1;
		}else{
			return 3;
		}
		
		
	},
	
	// recorre la lista y regresa el total
	recorrerHto:function(sesUsu){
		var tot =0;
		var sinApartar=false;
		for (var i = 0; i < lista.length; i+=1) {
			var checUs = $('#usuario'+lista[i]).val();			
			if(checUs.trim() != sesUsu.trim()){
				tot = tot+1;
				if(!checUs || checUs.trim() === "") {
					sinApartar=true;
				}
			}
		}
		if(sinApartar && tot === 1) {
			tot = 9999;
		}
		return tot;
	}
	
};
//funcion que verifica los registros seleccionados
function verificarSeleccion(index){  
	//pregunta cual fue seleccionado y lo agrega a las lista
	if( $('#seleccionado'+index).is(':checked') ) {
        lista.push(index);
        $('#lineaRegistro'+index).css("background-color", "#FFECD2");
    } else {
    	var i = lista.indexOf( index );
    	$('#lineaRegistro'+index).css("background-color", "");
        lista.splice( i, 1 );
    }
	
}

$(document).ready(recepcionOperacionApartadas.init);


