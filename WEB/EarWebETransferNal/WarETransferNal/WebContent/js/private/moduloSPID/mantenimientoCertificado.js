const idFormC = '#idForm';
const numCertC = '#numCert';
var    editar = function(){
        var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
        var nomPantalla =document.getElementById("nomPantalla").value;
        
           if (sizeCheckBoxes > 1) {
               jAlert("Solo puede seleccionar un registro", "Seleccion Todo", "", "", "", "");
               var objCheckBoxes = $( "[type=checkbox]" );
               for(var i=0;i<objCheckBoxes.length;i++){
                   objCheckBoxes[i].checked = false;                            
               }
               
           } else {
               if (sizeCheckBoxes === 0) {
                   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
               } else {    
                    
                    if(nomPantalla ==='mantenimientoCertificadoSPEI'){
                        $(idFormC).attr('action', 'editarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPEI');
                        $(idFormC).submit();
                    }
                    else{
                        $(idFormC).attr('action', 'editarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPID');
                        $(idFormC).submit();
                    }
                    
               }
           }
    };
var    solicitarConfirmacion = function (objCheckBoxes){
    var nomPantalla =document.getElementById("nomPantalla").value;
    
    if(nomPantalla ==='mantenimientoCertificadoSPEI'){
        
        jConfirm("Confirmar borrado de registro",mensajes["aplicacion"],'CD00167V','', function (respuesta){
            if(respuesta === true){    
                $('#paginaIni').val('1');
                $(idFormC).attr('action', 'eliminarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPEI');
                $(idFormC).submit(); 
            } else {
                for(var i=0;i<objCheckBoxes.length;i++){
                       objCheckBoxes[i].checked = false;                            
               }
            }
           });
        
    }
    else{
        
        jConfirm("Confirmar borrado de registro",mensajes["aplicacion"],'CD00167V','', function (respuesta){
            if(respuesta === true){    
                $('#paginaIni').val('1');
                $(idFormC).attr('action', 'eliminarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPID');
                $(idFormC).submit(); 
            } else {
                for(var i=0;i<objCheckBoxes.length;i++){
                       objCheckBoxes[i].checked = false;                            
               }
            }
           });
        
    }
        
    };
var    eliminar = function(){
           var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
           var objCheckBoxes = $( "[type=checkbox]" );
           
           if (sizeCheckBoxes > 1) {
               jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
               for(var i=0;i<objCheckBoxes.length;i++){
                   objCheckBoxes[i].checked = false;                            
               }
           } else {
               if (sizeCheckBoxes === 0) {
                   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
               } else {
                   solicitarConfirmacion(objCheckBoxes);                           
               }
           }    
       };
var    actualizar = function(){
    var nomPantalla =document.getElementById("nomPantalla").value;
    
    if(nomPantalla ==='mantenimientoCertificadoSPEI'){
        $('#paginaIni').val('1');
        window.location = 'mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI';
    }
    else{
        $('#paginaIni').val('1');
        window.location = 'mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPID';
    }
           
    };
var    seleccionarTodos = function(){        
           jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
            var objCheckBoxes = $( "[type=checkbox]" );
            if($('#chkTodos').is(':checked')){
               for(var i=0;i<objCheckBoxes.length;i++){
                      objCheckBoxes[i].checked = false;
                    
               }
            } else{
               for(var j=0;j<objCheckBoxes.length;j++){
                  objCheckBoxes[j].checked = false;
               }
            }
        };
var    check = function(){
    var nomPantalla =document.getElementById("nomPantalla").value;
    
    if(nomPantalla ==='mantenimientoCertificadoSPEI'){
        
        if (!(isNaN(Number($(numCertC).val()))) && $(numCertC).val().indexOf('.') === -1) {
            $(idFormC).attr('action', 'guardarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPEI');
            $(idFormC).submit();
        } else {
            jAlert(mensajes["CD00041V"],"El campo de Numero Certificado debe ser solo numeros",'CD00041V','');
        }
        
    }
    else{
        
        if (!(isNaN(Number($(numCertC).val()))) && $(numCertC).val().indexOf('.') === -1) {
            $(idFormC).attr('action', 'guardarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPID');
            $(idFormC).submit();
        } else {
            jAlert(mensajes["CD00041V"],"El campo de Numero Certificado debe ser solo numeros",'CD00041V','');
        }
        
    }
        
    };    
var    guardar = function(){
        var exp1 = function (){
            return Utilerias.isEmpty('numCert') || Utilerias.isEmpty('palClave') || Utilerias.isEmpty('mant') || Utilerias.isEmpty('sFF');
        };
        
        var exp2 = function (){
            return Utilerias.isEmpty('sFM') || Utilerias.isEmpty('sDF') || Utilerias.isEmpty('sDM');
        };
        
        if(exp1() || exp2()){
            jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
        } else {
            check();
        }
     };
var    editarG = function(){
        
        var edit1 = function (){
            return Utilerias.isEmpty('numCert') || Utilerias.isEmpty('palClave') || Utilerias.isEmpty('mant');
        };
        
        var edit2 = function (){
            return Utilerias.isEmpty('sFF') || Utilerias.isEmpty('sFM') || Utilerias.isEmpty('sDF');
        };
        
        if(edit1() || edit2() || Utilerias.isEmpty('sDM')){
            jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
        } else {
            verificarNum();
        }           
       };
var validarMantenimiento2 = function(mantenimiento){
    if ((mantenimiento === "A" || mantenimiento === "1") && $("#mantOld").val() !== "E") {
        jAlert("Operaci\u00F3n no permitida, el certificado ya existe", "ED0078V", "ED0078V", "", "");
    } else {
        if (($("#dFirmaOld").val() === "S" || $("#dMantOld").val() === "S") && (mantenimiento === "B")) { 
            jAlert("Certificado default no puede darse de baja", "ED0078V", "ED0078V", "", "");
        } else { 
            validarMantenimiento3(mantenimiento);
        }
    }
};
var validarMantenimiento3 = function(mantenimiento){
    var nomPantalla =document.getElementById("nomPantalla").value;
    
    if ( $('#mantOld').val() !== mantenimiento && (($("#dFirmaOld").val() === "S" && mantenimiento === "2") || 
            ($("#dMantOld").val() === "S" && mantenimiento === "3")) ) { 
        jAlert("Certificado default no puede perder facultades", "ED0078V", "ED0078V", "", "");
    } else { 
        if (($("#fFirmaOld").val() === "S" && mantenimiento === "F") || 
            (($("#fMantOld").val() === "S" && mantenimiento === "M"))) {
            jAlert("Facultad ya asignada", "ED0078V", "ED0078V", "", "");
        } else {
            
            if(nomPantalla ==='mantenimientoCertificadoSPEI'){
                $(idFormC).attr('action', 'guardarEdicionMantenimiento.do?nomPantalla=mantenimientoCertificadoSPEI');
                $(idFormC).submit();
            }
            else{
                
                $(idFormC).attr('action', 'guardarEdicionMantenimiento.do?nomPantalla=mantenimientoCertificadoSPID');
                $(idFormC).submit();
            }
            
        }
    }
};       
var validarMantenimiento = function() {
    var mantenimiento = $("#mant").val();                    
    
    if (mantenimiento === "E" || mantenimiento === "T") {
    mensajes["ED0078V"];
        jAlert("Operaci\u00F3n Inv\u00E1ida", "ED0078V", "ED0078V", "", "");
        $("#mant").val($("#mantOld").val());
    } else { 
        validarMantenimiento2(mantenimiento);
    }
};
var    verificarNum = function(){
        if (!(isNaN(Number($(numCertC).val())))) {
            validarMantenimiento();
        } else {
            jAlert(mensajes["CD00041V"],"El campo de Numero Certificado debe ser solo numeros",'CD00041V','');
        }        
    };
    
    var limpia=function(obj){
        obj.value='';
    };
var ordenReparacion = {
    init:function(){ 
       $('#idAgregar').click(function(){
           var nomPantalla =document.getElementById("nomPantalla").value;
           
           if(nomPantalla ==='mantenimientoCertificadoSPEI'){
               window.location = "agregarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPEI";   
           }
           else{
               window.location = "agregarMantenimiento.do?nomPantalla=mantenimientoCertificadoSPID";
           }
       });

       $('#idEditar').click(editar);
       
       $('#idEliminar').click(eliminar);
           
       $('#idActualizar').click(actualizar);
       
       $('#chkTodos').click(seleccionarTodos);
       
       $('#idGuardar').click(guardar);
       
       $('#idEditarG').click(editarG);
       
       $('#idRegresar').click(function(){        
           var nomPantalla =document.getElementById("nomPantalla").value;
           
           if(nomPantalla ==='mantenimientoCertificadoSPEI'){
               window.location = "mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI";   
           }
           else{
               window.location = "mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPID";
           }
       });
    }
};


$(document).ready(ordenReparacion.init);