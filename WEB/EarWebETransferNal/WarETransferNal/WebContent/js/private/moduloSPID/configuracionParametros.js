var guardar = function(){
    var fecPassword =document.getElementById("fec.fecPassword").value;
    var nomPantalla =document.getElementById("nomPantalla").value;
    var guardarParametro = new String(""); 
    
    if(nomPantalla==='parametrosOperativosSPEI'){
        guardarParametro = 'guardarConfiguracionParametros.do?nomPantalla=parametrosOperativosSPEI';
    }else{
        guardarParametro = 'guardarConfiguracionParametros.do?nomPantalla=parametrosOperativosSPID';
    }
    
    if(fecPassword!==''){
        jConfirm("Confirmar modificaci\xF3n",mensajes["aplicacion"],'CD00167V','', function (respuesta){
            if (respuesta === true){
                $('#idForm').attr('action', guardarParametro);
                $('#idForm').submit();
                }
        });
    }else{
        jAlert('Es mandatorio llenar el campo de password',mensajes["aplicacion"],'ED000100V','');
    }
    
    
};

var limpia=function(obj){
    obj.value='';
};

var configuracionParametros = {
    init:function(){ 
        var nomPantalla =document.getElementById("nomPantalla").value;
        $('#guardar').click(guardar);
       
        if(nomPantalla==='parametrosOperativosSPEI'){
             $('#idActualizar').click(function(){
                    $('#idForm').attr('action', 'configuracionParametros.do?nomPantalla=parametrosOperativosSPEI');
                    $('#idForm').submit();
               });
        }else{
             $('#idActualizar').click(function(){
                    $('#idForm').attr('action', 'configuracionParametros.do?nomPantalla=parametrosOperativosSPID');
                    $('#idForm').submit();
               });
        }
      
    }
};

$(document).ready(configuracionParametros.init);