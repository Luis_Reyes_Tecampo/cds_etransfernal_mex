var avisoTraspasos = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#idForm').attr('action', 'exportarAvisoTraspasos.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoAvisoTraspasos.do');
			$('#idForm').submit(); 
	   });
	   $('#idActualizar').click(function(){
			$('#idForm').attr('action', 'muestraAvisoTraspasos.do');
			$('#idForm').submit();
	   });
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'muestraAvisoTraspasos.do');
		   $('#idForm').submit();
	   });
	   
	   $('#paginaIni').val('1');
	   
	   $('#idRegresarMain').click(function(){
		   $('#idForm').attr('action', 'muestraMonitorOpSPID.do');
		   $('#idForm').submit();
	   });
	}
}


$(document).ready(avisoTraspasos.init);