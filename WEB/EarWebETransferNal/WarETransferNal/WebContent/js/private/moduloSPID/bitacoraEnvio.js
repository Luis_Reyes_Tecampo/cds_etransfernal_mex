var ordenReparacion = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarBitacoraEnvio.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoBitacoraEnvio.do');
			$('#idForm').submit();
	   });
	   
	   
	   $('#idActualizar').click(function(){
		   $('#paginaIni').val('1');
			$('#idForm').attr('action', 'muestraBitacoraDeEnvio.do');
			$('#idForm').submit();
		});
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'muestraBitacoraDeEnvio.do');
		   $('#idForm').submit();
	   });
	   
	   $('#paginaIni').val('1');
	}
}

$(document).ready(ordenReparacion.init);