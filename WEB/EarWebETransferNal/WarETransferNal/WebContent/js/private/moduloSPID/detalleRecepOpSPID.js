detalleRecepOpSPID = {
		init:function(){
			$('#idGuardar').click(function(){	
				jConfirm(mensajes["CD00043V"],mensajes["aplicacion"],'CD00043V','', function (respuesta){
					if(respuesta){
						$('#idForm').attr('action','guardarDetRecepOpSPID.do');
						$('#idForm').submit();
					} 
				});
			});
			
			$('#idActualizar').click(function(){	
				$('#idForm').attr('action','refrescarDetRecepOpSPID.do');
				$('#idForm').submit();
			});
			
			$('#idRegresar').click(function(){	
				servicio = $('#servicio').val();
				$('#idForm').attr('action',servicio);
				$('#idForm').submit();
			});
			
			
		}
	};

$(document).ready(detalleRecepOpSPID.init);

