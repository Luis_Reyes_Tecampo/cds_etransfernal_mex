var consultaRecepciones = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#idForm').attr('action', 'exportarConsultaRecepciones.do');
			$('#idForm').submit(); 
	   });
	   $('#idActualizar').click(function(){
		   	$('#paginaIni').val('1');
			$('#idForm').attr('action', 'consultaRecepciones.do');
			$('#idForm').submit();
		});
	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoConsultaRecepciones.do');
			$('#idForm').submit(); 
	   });
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'consultaRecepciones.do');
		   $('#idForm').submit();
	   });
	   
	   $('#idRegresar').click(function(){
		   $('#idForm').attr('action', 'muestraMonitorOpSPID.do');
		   $('#idForm').submit();
	   });
	   
	   $('a.idVerDetalle').click(function(){
	    	var parentTr = $(this).parent().parent();
	    	
	    	$('#fchOperacion').val($('td.campos input.fchOperacion', parentTr).val());
			$('#cveMiInstituc').val($('td.campos input.cveMiInstituc', parentTr).val());
		    $('#cveInstOrd').val($('td.campos input.cveInstOrd', parentTr).val());
		    $('#folioPaquete').val($('td.campos input.folioPaquete', parentTr).val());
		    $('#folioPago').val($('td.campos input.folioPago', parentTr).val());
			$('#idForm').attr('action','muestraDetRecep.do');
			$('#idForm').submit();
		});
	}
}

$(document).ready(consultaRecepciones.init);