var tipoF; 
var cancelar = function(tipo) {
	var seleccion = $('input[type="checkbox"]:checked')
	var mensaje;
	var topologia = seleccion.parent().find('input.topologia').val();
	var mecan = seleccion.parent().parent().find('td.mecan').text();
	
	if(seleccion.length == 0){
		mensaje = "Se requiere palomear un registro";
	}else{
		if (mecan === 'TRSPISIA') {
			mensaje = 'Desea Cancelar el Traspaso';
		}else{
			if (topologia === 'T') {
				mensaje = 'Desea Cancelar Ord. Pago Paquete Individual';
			}else{
				mensaje = 'Desea Cancelar Ord. Pago Paquete Completo';
			}
		}
	}
	
	

	jConfirm(mensaje, mensajes["aplicacion"], 'CD00167V', '', function(
			respuesta) {
		var val = $('input[type="checkbox"]:checked').val();
		if (respuesta === true && val !== undefined) {
			$('#paginaIni').val('1');
			$('#idForm').attr('action', 'NacCancelar.do');
			$('#idForm').submit();
		}
	});
};
var seleccionar = function() {
	var objCheckBoxes = $("input.seleccionCheck");

	if ($(this).is(':checked')) {
		for (var i = 0; i < objCheckBoxes.length; i++) {
			objCheckBoxes[i].checked = false;
		}
		this.checked = true;
	}
}

var cancelacionPagos = {
	init : function() {
		$('#idBuscar').click(function() {
			$('#paginaIni').val('1');
			$('#idForm').attr('action', 'cancelacionPagos.do');
			$('#idForm').submit();
		});
		$('#idExportar').click(function() {
			$('#accion').val('ACT');
			$('#idForm').attr('action', 'exportarCancelacionPagos.do');
			$('#idForm').submit();
		});

		$('#idExportarTodo').click(function() {
			$('#accion').val('ACT');
			$('#idForm').attr('action', 'exportarTodoCancelacionPagos.do');
			$('#idForm').submit();
		});

		$('#idActualizar').click(function() {
			$('#paginaIni').val('1');
			$('#idForm').attr('action', url);
			$('#idForm').submit();
		});

		$('.opcion').click(function() {
			$('#paginaIni').val('1');

			var tF = $(this).val();
			if (tF === '3') {
				$('#idForm').attr('action', 'cancelacionPagos.do');
			} else if (tF === '2') {
				$('#idForm').attr('action', 'cancelacionPagosTraspasos.do');
			} else if (tF === '1') {
				$('#idForm').attr('action', 'cancelacionPagosOrden.do');
			}
			$('#idForm').submit();
		});

		$('#idCancelar').click(cancelar);

		$('#paginaIni').val('1');

		$('.sortField')
				.click(
						function() {
							if ($('#sortField').val() !== $(this).attr(
									'data-id')) {
								$('#sortType').val('');
							}
						
							$('#sortField').val($(this).attr('data-id'));
							$('#sortType').val(
									$('#sortType').val() === 'ASC' ? 'DESC'
											: 'ASC');
							if (tipoFiltro === '3') {
								$('#idForm').attr('action',
										'cancelacionPagos.do');
							} else if (tipoFiltro === '2') {
								$('#idForm').attr('action',
										'cancelacionPagosTraspasos.do');
							} else if (tipoFiltro === '1') {
								$('#idForm').attr('action',
										'cancelacionPagosOrden.do');
							}
							$('#idForm').submit();
						});

		$('input.seleccionCheck').click(seleccionar);
	}
}

$(document).ready(cancelacionPagos.init);
