var consReceptorHist = {
	init:function(){ 
	   $('#idBuscar').click(function(){
		    $('#paginaIni').val('1');
		    $('#idForm').attr('action', 'realizaConsultaReceptorHist.do');
			$('#idForm').submit(); 
	   });
	   
	    $('a.idVerDetalle').click(function(){
	    	var parentTr = $(this).parent().parent();
	    	
	    	$('#fchOperacion').val($('td.campos input.fchOperacion', parentTr).val());
			$('#cveMiInstituc').val($('td.campos input.cveMiInstituc', parentTr).val());
		    $('#cveInstOrd').val($('td.campos input.cveInstOrd', parentTr).val());
		    $('#folioPaquete').val($('td.campos input.folioPaquete', parentTr).val());
		    $('#folioPago').val($('td.campos input.folioPago', parentTr).val());
			$('#idForm').attr('action','muestraDetalleRecepcion.do');
			$('#idForm').submit();
		});
	}	
}

$(document).ready(consReceptorHist.init);

var topovliqr=0;
var topotliqr=0;
var topotxliqr=0;
var rechr=0;
var topovliqm=0;
var topotliqm=0;
var topotxliqm=0;
var rechm=0;

function calcula(chk){
	var topo ="";
	var banxico = "";
	var transfer = "";
	var monto=0;
	
	topo = document.getElementById(chk.id+".topologia").value;
	banxico = document.getElementById(chk.id+".estatusBanxico").value;
	transfer = document.getElementById(chk.id+".estatusTransfer").value;
	monto = document.getElementById(chk.id+".monto").value;
	if (chk.checked) {
		sumarSeleccionados(topo, banxico, transfer, monto);
	} else {
		restarNoSeleccionados(topo, banxico, transfer, monto);
	}
	document.getElementById("regsTopoVLiq").innerHTML =topovliqr;
	document.getElementById("regsTopoLiq").innerHTML =topotliqr;
	document.getElementById("regsTopoXLiq").innerHTML =topotxliqr;
	document.getElementById("regsRechazos").innerHTML =rechr;
	
	document.getElementById("sumaTopoVLiq").innerHTML =topovliqm;
	document.getElementById("sumaTopoLiq").innerHTML =topotliqm;
	document.getElementById("sumaTopoXLiq").innerHTML =topotxliqm;
	document.getElementById("sumaRechazos").innerHTML =rechm;
}

function restarNoSeleccionados(topo, banxico, transfer, monto) {
	if (topo==="V" && banxico==="LQ") {
		topovliqr = +topovliqr-1;
		topovliqm = +topovliqm - +monto;
	}
	if (topo==="T" && banxico==="LQ") {
		topotliqr = +topotliqr-1;
		topotliqm = +topotliqm - +monto;
	}
	if (topo==="T" && banxico==="PL") {
		topotxliqr = +topotxliqr-1;
		topotxliqm = +topotxliqm - +monto;
	}
	if (transfer==="RE") {
		rechr = +rechr-1;
		rechm = +rechm - +monto;
	}
}

function sumarSeleccionados(topo, banxico, transfer, monto) {
	if (topo==="V" && banxico==="LQ") {
		topovliqr = +topovliqr+1;
		topovliqm = +topovliqm + +monto;
	}
	if (topo==="T" && banxico==="LQ") {
		topotliqr = +topotliqr+1;
		topotliqm = +topotliqm + +monto;
	}
	if (topo==="T" && banxico==="PL") {
		topotxliqr = +topotxliqr+1;
		topotxliqm = +topotxliqm + +monto;
	}
	if (transfer==="RE") {
		rechr = +rechr+1;
		rechm = +rechm + +monto;
	}
}

consReceptorHist.init();

//<!--
function doSearch(elemento, col) {
	var t = document.getElementById("tablaDatos"),
    tableRows = t.getElementsByTagName("tr"),
    i, len, tds, j, jlen, dato;
	var q = elemento;
	var v = q.value.toLowerCase();
	var on = 0;
	for ( i =0, len = tableRows.length; i<len; i++) {
	    tds = tableRows[i].getElementsByTagName('td');
	    for( j = 0, jlen = tds.length; j < jlen; j++) {
	        if (j===col) {
	        	dato = tds[j].innerHTML.toLowerCase();
	        	on = checkFilter(tableRows, i, dato, v, on);
	        	break;
	        }
    	}	       
	}
}
//-->

function checkFilter(tableRows, i, dato, v, on) {
	var eval1 = function(){
		return v.length === 0 || (v.length >0 && dato.indexOf(v) === 0);
	}
	
	var eval2 = function(){
		return v.length > 0 && dato.indexOf(v) > -1;
	}
	
	if (dato) {
	  if (eval1() || eval2()) {
		tableRows[i].style.display = "";
	    on++;
	  } else {
		tableRows[i].style.display = "none";
	  }
	} else {
		if ( v.length === 0) {
			tableRows[i].style.display = "";
		}
		else {
			tableRows[i].style.display = "none";
		}
	}
	return on;
}