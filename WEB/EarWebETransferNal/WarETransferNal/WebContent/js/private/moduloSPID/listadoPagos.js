var arr = [8,32,48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,
104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;

var listadoPagos = {
	init:function(){ 
	   $('#idExportar').click(function(){
		   if ($(this).attr('data-field') !== '' && $(this).val() !== '')
		   {
			   $('#field').val($(this).attr('data-field'));
			   $('#fieldValue').val($(this).val());
		   }
		   $('#idForm').attr('action', 'exportarListadoPagos.do?tipo=' + tipoOrden);
		   $('#idForm').submit();
	   });
	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoListadoPagos.do?tipo=' + tipoOrden);
			$('#idForm').submit(); 
	   });
	   
	   $('#idActualizar').click(function(){
			$('#paginaIni').val('1');
			$('#field').val('');
		    $('#fieldValue').val('');
			$('#idForm').attr('action', 'listadoPagos.do?tipo=' + tipoOrden);
			$('#idForm').submit();
		});
	   
	   $('.filField').keypress(function (e) {		   
		   var charCode = (e.which) ? e.which : e.keyCode;
		   
		    cadena=$(this).val();
			cadenaLength=cadena.length;
			cadenaLenghtMenos=cadenaLength-1;
			cadenaAux=cadena.substring(0, cadenaLenghtMenos);
			
			if (charCode === 8 || charCode === 37 || charCode === 39){
				return true;
			}
		   
		   if (charCode === 13) {
		    	if ($(this).attr('data-field') !== '' && $(this).val() !== '')
				{
				   $('#field').val($(this).attr('data-field'));
				   $('#fieldValue').val($(this).val());
				}
		    	$('#idForm').attr('action', 'listadoPagos.do?tipo=' + tipoOrden);
				$('#idForm').submit();
		    }
		   else if($(this).attr('data-field')==='referencia'){
			   if (charCode != 8 && charCode != 0 && (charCode < 48 || charCode > 57) || cadenaLength>=12 ) {
					//$(this).val(cadenaAux);
				   return false;
				}
		   }
		   else if($(this).attr('data-field')==='imp'){
			   if ($(this).val().indexOf('.') > -1 && charCode === 46)
				   return false;
			   
			   if (charCode != 46 && charCode != 8 && charCode != 0 && (charCode < 48 || charCode > 57) || cadenaLength>=38 ) {
					//$(this).val(cadenaAux);
				   return false;
				}
		   }   
		   else if($(this).attr('data-field')==='cveTran'){
			   carVal = 0;
			   for(var i=0; i<=arrLeng; i++){
					if(arr[i] === charCode){
						carVal = 1;
					}
				}
			   if (carVal != 1 || cadenaLength>=3){
					//$(this).val(cadenaAux);
				   return false;
				}
		   }
		   else if(($(this).attr('data-field')==='cola')||($(this).attr('data-field')==='mecan')){
			   carVal = 0;
			   for(var i=0; i<=arrLeng; i++){
					if(arr[i] === charCode){
						carVal = 1;
					}
				}
			   if (carVal != 1 || cadenaLength>=8){
					//$(this).val(cadenaAux);
				   return false;
				}
		   }
		   else if(($(this).attr('data-field')==='ord')||($(this).attr('data-field')==='rec')){
			   carVal = 0;
			   for(var i=0; i<=arrLeng; i++){
					if(arr[i] === charCode){
						carVal = 1;
					}
				}
			   if (carVal != 1 || cadenaLength>=5){
					//$(this).val(cadenaAux);
				   return false;
				}
		   }		   
		   
		 
	   });
	   	   
	   $('#idRegresar').click(function(){		
		   window.location = "listadoPagos.do?tipo=" + tipoOrden;
	   });
	   
	   $('#idHistoriaMensaje').click(function(){		
		   window.location = "historial.do?ref=" + referencia +'&tipo='+tipoOrden;
	   });
	   
	   
	   $('a.detalle').click(function(){
		   var referencia = $(this).attr('data-ref');
		   $('#idForm').attr('action', 'detallePago.do?tipo=' + tipoOrden + '&ref=' + referencia);
		   $('#idForm').submit();
	   });
	   
	   $('#idRegresarMain').click(function(){
		   $('#idForm').attr('action', 'muestraMonitorOpSPID.do');
		   $('#idForm').submit();
	   });
	}
}

$(document).ready(listadoPagos.init);