                    
var conectados = 0;
var desconectados = 0;
var bloqueados = 0;  
var bajas = 0;
var receptivos = 0;                    
var noReceptivos = 0; 
var noReg = 0; 

function sumaConectados(){
	conectados ++;
}
function sumaDesonectados(){
	desconectados ++;
}
function sumaBajas(){
	bajas ++;
}
function sumaBloqueados(){
	bloqueados ++;
}
function sumaReceptivos(){
	receptivos ++;
}
function sumaNoReceptivos(){
	noReceptivos ++;
}
function sumaNoReg(){
	noReg ++;
}

var monitorBancos = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#idForm').attr('action', 'exportarmonitorBancos.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodomonitorBancos.do');
			$('#idForm').submit(); 
	   });
	   
	   $('#idEnviar').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'enviarmonitorBancos.do');
			$('#idForm').submit(); 
	   });
	   $('#idActualizar').click(function(){
		   	$('#paginaIni').val('1');
			$('#idForm').attr('action', 'monitorBancos.do');
			$('#idForm').submit();
		});
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'monitorBancos.do');
		   $('#idForm').submit();
	   });
	   
	   $('#paginaIni').val('1');
	}

}
$(document).ready(monitorBancos.init);