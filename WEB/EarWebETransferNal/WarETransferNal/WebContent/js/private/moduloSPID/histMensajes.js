var histMensahes = {
	init:function(){ 
	   $('#idBuscar').click(function(){
		    $('#paginaIni').val('1');
		    $('#idForm').attr('action', 'historial.do?ref=' + referencia + '&tipo=' + tipoOrden);
			$('#idForm').submit();
	   });
	   
	   $('#chkTodos').click(function(){		
			var objCheckBoxes = $( "[type=checkbox]" );
	        if($('#chkTodos').is(':checked')){
	           for(var i=0;i<objCheckBoxes.length;i++){
		              objCheckBoxes[i].checked = true;
		            
	           }
	        } else{
	           for(var j=0;j<objCheckBoxes.length;j++){
	              objCheckBoxes[j].checked = false;
	           }
	        }
		});
	   $('#paginaIni').val('1');
	   
	   $('#idRegresar').click(function(){		
		   window.location = "detallePago.do?ref=" + referencia +'&tipo='+tipoOrden;
	   });
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'historial.do?ref=' + referencia + '&tipo=' + tipoOrden);
		   $('#idForm').submit();
	   });
	}	
}

$(document).ready(histMensahes.init);