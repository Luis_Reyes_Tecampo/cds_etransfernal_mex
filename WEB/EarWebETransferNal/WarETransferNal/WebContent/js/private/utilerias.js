Utilerias = {
		
		/**Funcion que valida si un campo es vacio**/
		isEmpty:function(id){
			var element; 
			var value = '';
			var flagEmpty = true;
			value = $('#'+id).val();
			return $.trim(value) == '';
		},
		
		stringToDate:function(cad){
			var dia  = cad.substring(0,2); 
	        var mes = cad.substring(3,5); 
	        var year  = cad.substring(6,10); 
	        return new Date(year,mes-1,dia);
		},
		
		
		evitaCortarCopiarPegar:function(id){
		     $('#'+id).bind("cut copy paste",function(e) {
		       e.preventDefault();
		       e.stopPropagation();
		     });
		},

		stringToTime:function(cad){
			var hora  = cad.substring(0,2); 
	        var min = cad.substring(3,5); 
	        var sec  = cad.substring(6,8);	        
	        var fecha = new Date();
	        return new Date(fecha.getDate(),fecha.getMonth(),fecha.getFullYear(),hora,min,sec);
		}		
	
};
