/** Funcion para procesar
 */
var lista = [];

var contingencias = {
     init : function() {
        /**Valida contigencia seleccionada*/
         $('#idProcesar').click(function (){
			var check=0;			
		 $('input[type="checkbox"]:checked').each(function() {
			
			check++;
			});
		 var seleccionados = $("#tableConsulta tbody tr td input:checked");
		 var i;
         
		if(check===1){
			$("#formArchProc").attr("action","actualizaEstatus.do");
			$("#formArchProc").submit();
		}else if(check === 0) {
			jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
					'ED00068V', '');
		} else {
			jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
					'ED00126V', '');
		}
			
			
			
		});
         
         /**funcion para buscar actualizar **/
         $('#idActualizar').click(function(){	
        	 document.forms['formArchProc'].action = "muestraOperacionesContingentes.do";
        	document.forms['formArchProc'].submit();
         });
         
	}
};
/**funcion que llama al pantalla de detalle**/
function verDetalle(nombre){
	$('#nombreArch').val(nombre);
	
	document.forms['formArchProc'].action = "detalleOperacion.do";
	document.forms['formArchProc'].submit();
}
/**funcion para cargar el archivo **/
function cargaArchivo(){
	document.forms['formArchProc'].action = "cargaArchivoOperaciones.do";
	document.forms['formArchProc'].submit();
}
/**funcion que valida lo seleccionado **/
function verificarSeleccion(index){ 
    if( $('#seleccionado'+index).is(':checked') ) {
        lista.push('1');
    } else {
        lista.pop();
    }
}

/**inicia el js **/
$(document).ready(contingencias.init);