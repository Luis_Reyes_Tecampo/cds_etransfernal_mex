/**
 * Funcionalidad catalogo de canales
 **/

/**
 * Funcion para agregar canal Inicial
 */
function agregarCanal(){
	document.forms['formValorCanal'].action = "altaCanal.do";
	document.forms['formValorCanal'].submit();
}

/**
 * Funcion para actualiza el estatus del canal
 */
function actualizaEstatusCanal(checkActDes){
	if(checkActDes.checked){
		jConfirm("\u00bfEst\u00E1 seguro de que desea activar el canal para contingencia?", "Catalogo de Canales", "",
				"", function(respuesta) {
					if (respuesta) {
						document.getElementById('chBoxActDesHide').value = checkActDes.id;
						document.forms['formAdmCanales'].action = "actualizaCanal.do";
						document.forms['formAdmCanales'].submit();
					} else {
						if(checkActDes.checked){
							checkActDes.checked =false;
						}else{
							checkActDes.checked =true;
						}
					}
				}, "Cancelar");	
	} else {
		jConfirm("\u00bfEst\u00E1 seguro de que desea desactivar el canal para contingencia?", "Catalogo de Canales", "",
				"", function(respuesta) {
					if (respuesta) {
						document.getElementById('chBoxActDesHide').value = checkActDes.id;
						document.forms['formAdmCanales'].action = "actualizaCanal.do";
						document.forms['formAdmCanales'].submit();
					} else {
						if(checkActDes.checked){
							checkActDes.checked =false;
						}else{
							checkActDes.checked =true;
						}
					}
				}, "Cancelar");	
	}
}

/**
 * Funcion para eliminar canales
 */
function eliminarCanales(){
	var checkboxes = document.getElementById("formAdmCanales").chkCierre;
	var cont = 0; 
	if(checkboxes.length){
		for (var x=0; x < checkboxes.length; x++) {
		 if (checkboxes[x].checked) {
		  cont = cont + 1;
		 }
		}
	}else if(checkboxes && checkboxes.checked) {
			  cont = cont + 1;
	}
	if(cont>0){
		jConfirm("\u00bfEst\u00E1 seguro de que desea eliminar canal(es) del catalogo?", "Catalogo de Canales", "",
				"", function(e) {
					if (e) {
						var checkboxValues = "";
						$('input[name="chkCierre"]:checked').each(function() {
							checkboxValues += $(this).val() + "@";
						});
						//eliminamos la ultima coma
						checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
						document.forms['formAdmCanales'].hdnCheckSeleccionados.value = checkboxValues;
						document.forms['formAdmCanales'].action = "eliminarCanales.do";
						document.forms['formAdmCanales'].submit();
					}
				}, "Cancelar");	
	} else {
		jInfo("Seleccione al menos un canal", "Catalogo de Canales", "","");	
	}
}

/**
 * Funcion para buscar canal
 */
function buscarCanal(){
	document.forms['formAdmCanales'].action = "buscarCanal.do";
	document.forms['formAdmCanales'].submit();
}

/**
 * Funcion para realizar la paginacion siguiente
 */

function paginarSiguiente() 
{	
	document.forms['formAdmCanales'].action = "paginarSiguienteCanales.do";
	document.forms['formAdmCanales'].submit();
}

/**
 * Funcion para realizar la paginacion anterior
 */

function paginarAnterior() 
{	
	document.forms['formAdmCanales'].action = "paginarAnteriorCanales.do";
	document.forms['formAdmCanales'].submit();
}	