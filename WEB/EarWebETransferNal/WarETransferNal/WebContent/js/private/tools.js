/**
 * Utilerias para los formularios 
 */
$(function() {
    $('#btnClean').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('Limpieza de campos de los filtros de busqueda...');
        cleanForm( $(this).attr('data-frm') );
    });
});


/**
 * Procedimiento para la limpieza de formularios
 * @param mForm
 * @returns
 */
function cleanForm( mForm ) {
    obj = '#' + mForm;
    $(obj).find('input').each(function() {
        $(this).val('');
    });
    console.log('Limpieza terminada...');
}


/**
 * Procedimiento para mostrar las ventanas modales dentro de los formularios
 * @param tipo
 * @param mensaje
 * @param titulo
 * @param encabezado
 * @param titleObj
 * @returns
 */
function showModal(tipo, mensaje, titulo, encabezado, titleObj) {
	if(tipo === 'alert') {
		jAlert(mensaje, titulo, encabezado, titleObj);
	} 
	if(tipo === 'info') {
		jInfo(mensaje, titulo, encabezado, titleObj);
	}
	if(tipo === 'confirm') {
		jConfirm(mensaje, titulo, encabezado, titleObj);
	}
	if(tipo === 'help') {
		jHelp(mensaje, titulo, encabezado, titleObj);
	}
	if(tipo === 'error') {
		jError(mensaje, titulo, encabezado, titleObj);
	}
	if(tipo === 'input') {
		jPrompt(mensaje, titulo, encabezado, titleObj);
	}
}