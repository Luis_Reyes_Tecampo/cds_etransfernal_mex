var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 
	65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
	97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

var mediosEntrega = {
	init : function() {
		$('#btnExportar').click(function() {
			$('#accion').val('ACT');
			$('#idForm').attr('action', 'exportarMediosEntrega.do');
			$('#idForm').submit();
		});
		$('#btnExportarTodo').click(function() {
			$('#idForm').attr('action', 'exportarTodosMediosEntrega.do');
			$('#idForm').submit();
		});
		$('#btnActualizar').click(function() {
			$('#paginaIni').val('1');
			$('#field').val('');
			$('#fieldValue').val('');
			$('#idForm').attr('action', 'consultaMttoMediosEntrega.do');
			$('#idForm').submit();
		});
		$('#btnEliminar')
				.click(
						function() {
							var checkboxCount = 0;
							$('input[id="check"]:checked').each(function() {
								checkboxCount++;
							});
							if (checkboxCount > 0) {
								jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','',
										function(respuesta) {
											if (respuesta) {
												$('#idForm').attr('action','eliminarMedioEntrega.do');
												$('#idForm').submit();
											}
										});
							} else {
								jAlert(mensajes["ED00068V"],
										mensajes["aplicacion"], 'ED00068V', '');
							}
						});	
		$('.filField').keypress(function(e) {
			var charCode = (e.which) ? e.which : e.keyCode;
			cadena = $(this).val();
			cadenaLength = cadena.length;
			cadenaLenghtMenos = cadenaLength - 1;
			cadenaAux = cadena.substring(0, cadenaLenghtMenos);
			if (charCode === 8 || charCode === 37 || charCode === 39) {
				return true;
			}
			if (charCode === 13) {
				if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
					$('#field').val($(this).attr('data-field'));
					$('#fieldValue').val($(this).val());
				}
				$('#idForm').attr('action', 'consultaMttoMediosEntrega.do');
				$('#idForm').submit();
			}
		});
		$('#btnGuardar').on(
				'click',
				function(e) {
					jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
							'CD00010V', '', function(respuesta) {
								if (respuesta) {
									e.stopPropagation();
									e.preventDefault();
									var resValidation = validarFrm('mform');
									if (resValidation.seguir) {
										preparaEnvio();
									} else {
										showModal('error',
												resValidation.mensajes,
												'Datos No Validos', '', '');
									}
								}
							});
				});
		$('#btnEditar').click(
				function() {
					var checkboxCount = 0;
					$('input[id="check"]:checked').each(function() {
						checkboxCount++;
					});
					if (checkboxCount === 1) {
						$('#idForm').attr('action',
								'mantenimientoMediosEntrega.do');
						$('#idForm').submit();
					} else if(checkboxCount === 0) {
						jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
								'ED00068V', '');
					} else {
						jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
								'ED00126V', '');
					}
				});
		}
	};


function preparaEnvio() {
	if ($('#accion').val() === 'alta') {
		$('#mform').attr('action', 'agregarMedioEntrega.do');
	} else {
		$('#mform').attr('action', 'editarMedioEntrega.do');
	}
	$('body #mform').submit();
};

$(document).ready(mediosEntrega.init);