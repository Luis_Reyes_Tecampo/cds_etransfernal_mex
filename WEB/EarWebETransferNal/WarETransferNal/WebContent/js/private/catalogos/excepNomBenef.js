/** Declaracion de las variables a utilizar * */
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

/** Se declaran las funciones* */
var nombresBenef = {
    init : function() {
    $('#idCargar').click(function() {
        $('#idForm').attr('action', 'cargaArchivos.do');
        $('#idForm').submit();
    });
    /** Evento exportar * */
    $('#btnExportar').click(function() {
        $('#idForm').attr('action', 'exportarNomBeneficiarios.do');
        $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
        $('#idForm').attr('action', 'exporTodoNomBeneficiarios.do');
        $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
        $('#paginaIni').val('1');
        $('#nombreInput').val('');
        $('#idForm').attr('action', 'excepNomBeneficiarios.do');
        $('#idForm').submit();
    });
    /** Evento eliminar * */
    $('#btnEliminar').click(
        function() {
            continuaEliminando();
        });
    /** Eventos del filtro de busqueda* */
    $('.filField').keypress(function(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        cadena = $(this).val();
        cadenaLength = cadena.length;
        cadenaLenghtMenos = cadenaLength - 1;
        cadenaAux = cadena.substring(0, cadenaLenghtMenos);
        if (charCode === 8 || charCode === 37 || charCode === 39) {
        return true;
        }
        if (charCode === 13) {
        if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
            $('#field').val($(this).attr('data-field'));
            $('#fieldValue').val($(this).val());
            $('#pagina').val('1');
        }
        $('#idForm').attr('action', 'excepNomBeneficiarios.do');
        $('#idForm').submit();
        }
        return true;
    });
    /** Evento guardar* */
    $('#btnGuardar').on('click', function(e) {
        continuaGuardando(e);
    });
    /** Evento para remover clases* */
    $('.focusCombo').on('focus', function(e) {
        $('#' + e.target.id).removeClass('error-obj');
    });
    /** Evento editar* */
    $('#btnEditar').click(
        function() {
    $('#nombreInput').val("");
            /** Validacion de la seleccion * */
            var checkboxCount = 0;
            $('input[id="check"]:checked').each(function() {
            checkboxCount++;
            });
            if (checkboxCount === 1) {
            $('#idForm').attr('action', 'cambioNomBeneficiarios.do');
            $('#idForm').submit();
            } else if (checkboxCount === 0) {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            } else {
            jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
                'ED00126V', '');
            }
        });
    $('#btnReturn').click(function() {
    $('#paramNombre').val("");
    $('#paramCuenta').val("");
    $('#paramComentarios').val("");
    $('#valorOLd').val("");
        $('#mform').attr('action', 'excepNomBeneficiarios.do');
        $('#mform').submit();
    });
    $('#btnAgregar').click(function() {
    $('#nombreInput').val("");
        $('#idForm').attr('action', 'altaNomBeneficiarios.do');
        $('#idForm').submit();
    });
    }
};
/** Funcion para continuar la funcion inicial * */
function continuaGuardando(e) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
        if (respuesta) {
            e.stopPropagation();
            e.preventDefault();
            var resValidation = validarFrm('mform');
            if (resValidation.seguir) {
    if(validaCampoVacio()){
    preparaEnvio();
    }else{
    showModal('error', 'El campo Nombre no puede estar vacio',
                            'Datos No Validos', '', '');
    }
            } else {
            showModal('error', resValidation.mensajes,
                'Datos No Validos', '', '');
            }
        }
        });
}

function validaCampoVacio(){
    var cadena1 = $('#paramNombre').val();
    if(cadena1.trim() === ""){
        return false;
    }
    return true;
}

/** Funcion para continuar la funcion inicial * */
function continuaEliminando() {
    var checkboxCount = 0;
    $('input[id="check"]:checked').each(function() {
    checkboxCount++;
    });
    if (checkboxCount > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
        'CD00010V', '', function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action','bajaNomBeneficiarios.do');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
        'ED00068V', '');
    }
}
/** Funcion para enviar los datos al controller * */
function preparaEnvio() {
    /** Validacion * */
    if ($('#accion').val() === 'alta') {
    $('#mform').attr('action', 'altaNomBeneficiarios.do');
    } else {
    $('#mform').attr('action', 'cambioNomBeneficiarios.do');
    }
    /** Envio del formulario * */
    $('body #mform').submit();
}

/** Inicializacion de las funciones* */
$(document).ready(nombresBenef.init);