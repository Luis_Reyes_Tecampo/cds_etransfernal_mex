catInstFinancieras = {
	regCveBanxico : new RegExp("^([0-9]{1,12})$"),
	init:function(){	   
		$('#idBuscar').click(function(){
			$('#numBanxico').val($('#paramNumBanxico').val());
			$('#nombre').val($('#paramNombre').val());			
			$('#idForm').attr('action','buscarInstitucionesPOACOA.do');
			$('#idForm').submit();
		});	
		$('#idAlta').click(function(){
			$('#idForm').attr('action','muestraAltaCatInstFinancieras.do');
			$('#idForm').submit();
		});	
		$('#idEliminar').click(function(){
			jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','', function (respuesta){
				if(respuesta){
					$('#idForm').attr('action','elimCatInstFinancieras.do');
					$('#idForm').submit();
				} 
			});
		});

		$('#paramNumBanxico').keypress(function(e){
			var cad;
			var k;

			document.all ? k = e.keyCode : k = e.which;
			catInstFinancieras.cancel2(e);
			cad = $('#paramNumBanxico').val()+String.fromCharCode(k);
			if(!(cad ==="") && !catInstFinancieras.regCveBanxico.test(cad) && k !== 8){
				e.preventDefault();
				e.stopPropagation();
			}
        });
	},
	
	cancel2:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===190 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	},
	habilitarDeshabilitarPOACOA:function(numBanxico,nombre,participantePOACOA,element){
		var res ="";
		var str ="";
		var cod ="";
		if(participantePOACOA === 0){
			str = mensajes["CD00163V"];
			cod = "CD00163V";
			res = str.replace("{0}", nombre);
			$('#habDesValor').val(1);
		}else{
			str = mensajes["CD00164V"];
			res = str.replace("{0}", nombre);
			cod = "CD00164V";
			$('#habDesValor').val(0);
		}		
		jConfirm(res,mensajes["aplicacion"],cod,'', function (respuesta){
			if(respuesta){
				$('#habDesNumBanxico').val(numBanxico);
				$('#habDesNombre').val(nombre);
				$('#accion').val("ACT");
				$('#idForm').attr('action', 'habDesPOACOA.do');
				$('#idForm').submit();
			}else{
				if(element.checked){
					element.checked =false;
				}else{
					element.checked =true;
				}
			}
		});
		
	},
	habilitarDeshabilitarValidarCuenta:function(numBanxico,nombre,validaCuenta,element){
		var res ="";
		var str ="";
		var cod ="";
		if(validaCuenta === 0){
			str = mensajes["CD00161V"];
			cod = "CD00161V";
			res = str.replace("{0}", nombre);
			$('#habDesValor').val(1);
		}else{
			str = mensajes["CD00162V"];
			res = str.replace("{0}", nombre);
			cod = "CD00162V";
			$('#habDesValor').val(0);
		}
		jConfirm(res,mensajes["aplicacion"],cod,'', function (respuesta){
			if(respuesta){
				$('#habDesNumBanxico').val(numBanxico);
				$('#habDesNombre').val(nombre);
				$('#accion').val("ACT");
				$('#idForm').attr('action', 'habDesValidacionCuenta.do');
				$('#idForm').submit();
			}else{
				if(element.checked){
					element.checked =false;
				}else{
					element.checked =true;
				}
			} 
		});
	}
};

$(document).ready(catInstFinancieras.init);