/** Declaracion de las variables a utilizar * */
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

/** Se declaran las funciones* */
var afectacionBloques = {
    init : function() {
    /** Evento exportar * */
    $('#btnExportar').click(function() {
        $('#accion').val('ACT');
        $('#idForm').attr('action', 'exportarAfectacionBloques.do');
        $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
        $('#idForm').attr('action', 'exportarTodosAfectacionBloques.do');
        $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
        $('#paginaIni').val('1');
        $('#field').val('');
        $('#fieldValue').val('');
        $('#idForm').attr('action', 'consultaAfectacionBloques.do');
        $('#idForm').submit();
    });
    /** Evento eliminar * */
    $('#btnEliminar').click(
        function() {
            continuaEliminando();
        });
    /** Eventos del filtro de busqueda* */
    $('.filField').keypress(function(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        cadena = $(this).val();
        cadenaLength = cadena.length;
        cadenaLenghtMenos = cadenaLength - 1;
        cadenaAux = cadena.substring(0, cadenaLenghtMenos);
        if (charCode === 8 || charCode === 37 || charCode === 39) {
        return true;
        }
        if (charCode === 13) {
        if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
            $('#field').val($(this).attr('data-field'));
            $('#fieldValue').val($(this).val());
            $('#pagina').val('1');
        }
        $('#idForm').attr('action', 'consultaAfectacionBloques.do');
        $('#idForm').submit();
        }
        return true;
    });
    /** Evento guardar* */
    $('#btnGuardar').on('click', function(e) {
        continuaGuardando(e);
    });
    /** Evento para remover clases* */
    $('.focusCombo').on('focus', function(e) {
        $('#' + e.target.id).removeClass('error-obj');
    });
    /** Evento editar* */
    $('#btnEditar').click(
        function() {
            /** Validacion de la seleccion * */
            var checkboxCount = 0;
            $('input[id="check"]:checked').each(function() {
            checkboxCount++;
            });
            if (checkboxCount === 1) {
            $('#idForm').attr('action', 'mantenimientoBloques.do');
            $('#idForm').submit();
            } else if (checkboxCount === 0) {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            } else {
            jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
                'ED00126V', '');
            }
        });
    $('#btnReturn').click(function() {
        $('#mform').attr('action', 'consultaAfectacionBloques.do');
        $('#mform').submit();
    });
    $('#btnAgregar').click(function() {
        $('#idForm').attr('action', 'mantenimientoBloques.do');
        $('#idForm').submit();
    });
    $('#numBloqueInput').on('keypress copy change focus', function(e){
        return soloNumeros(e);
    });
    $('#numBloqueInput').on('paste', function(e){
    e.stopPropagation();
    e.preventDefault();
    });
    $('.valid-letrasNum').on('keypress copy change focus', function(e){
    return letrasNumeros(e);
    });
    $('.valid-letrasNum').on('paste', function(e){
    e.stopPropagation();
    e.preventDefault();
    });
    }
};
/** Funcion para continuar la funcion inicial * */
function continuaGuardando(e) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
        if (respuesta) {
            e.stopPropagation();
            e.preventDefault();
            var resValidation = validarFrm('mform');
            if (resValidation.seguir) {
            preparaEnvio();
            } else {
            showModal('error', resValidation.mensajes,
                'Datos No Validos', '', '');
            }
        }
        });
}

/** Funcion para continuar la funcion inicial * */
function continuaEliminando() {
    var checkboxCount = 0;
    $('input[id="check"]:checked').each(function() {
    checkboxCount++;
    });
    if (checkboxCount > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
        'CD00010V', '', function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action',
                'eliminarBloque.do');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
        'ED00068V', '');
    }
}
/** Funcion para enviar los datos al controller * */
function preparaEnvio() {
    /** Validacion * */
    if ($('#accion').val() === 'alta') {
    $('#mform').attr('action', 'agregarBloque.do');
    } else {
    $('#mform').attr('action', 'editarBloque.do');
    }
    /** Envio del formulario * */
    $('body #mform').submit();
}
/** Funcion solo numeros en la entrada **/
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
        e.preventDefault();
    }
    /** Validador de copiado mediante el historial */
    var numero = $('#numBloqueInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#numBloqueInput').val('');
    }
}

/** Funcion para validar la entrada los filtros de busqueda**/
function letrasNumeros(e) {
    var reg = new RegExp ( /^([a-zA-Z0-9 ]+)$/ );
    var resFnc = validarObj(reg, e.key, '#'+e.target.id);
    if( resFnc === false ) {
        e.preventDefault();
    }
    /** Validador de copiado mediante el historial */
    var texto = $('#'+e.target.id).val();
     if (!/^([a-zA-Z0-9 ]+)$/.test(texto)){
         $('#'+e.target.id).val('');
     }
}
/** Inicializacion de las funciones* */
$(document).ready(afectacionBloques.init);