altaCatIntFinancieros = {
	regNumDoce : new RegExp("^([0-9]{1,12})$"),
	regNumSiete : new RegExp("^([0-9]{1,7})$"),
	init:function(){  
		createCalendarGuion('paramFchBaja','cal2');
		$('#idAceptar').click(function(){
			if($('#paramCveInterme').val() != ""){
				setValues();
				$('#idForm').attr('action','altaCatIntFinancieros.do');
				$('#idForm').submit();
			}else{
				jAlert(mensajes["ED00086V"],
						mensajes["aplicacion"],
					   	   'ED00086V',
					   	   '');
			}
		});
		
		$('#idEditar').click(function(){
			setValues();
			
			$('#idForm').attr('action','editarIntFinancieros.do');
			$('#idForm').submit();
		});
		
		$('#idValidar').click(function(){
			if($('#paramCveInterme').val() != ""){
				setValues();
				$('#idForm').attr('action','muestraAltaCatIntFinancieros.do?bandera=' + 'V');
				$('#idForm').submit();
			}else{
				jAlert(mensajes["ED00086V"],
						mensajes["aplicacion"],
					   	   'ED00086V',
					   	   '');
			}
		});
		
		$('#idCancelar').click(function(){
			jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','', function (respuesta){
				if(respuesta){
					$('#idForm').attr('action','muestraCatIntFinancieros.do');
					$('#idForm').submit();
				} 
			});
		});
		
		$('#idLimpiar').click(function(){
			var form = document.getElementById("idForm");
			if ($('#checkSeleccionados').val() == "1") {
				$('#cveInterme').val($('#paramCveInterme').val());
			}			
			$('#idForm').attr('action','muestraAltaCatIntFinancieros.do?bandera=' + 'L');
			$('#idForm').submit();
		});
		
		$('#paramNumBanxico').keypress(function(e){
			soloNumeros('#paramNumBanxico', altaCatIntFinancieros.regNumDoce, 8, e);
        });
		
		$('#paramNumCecoban').keypress(function(e){
			soloNumeros('#paramNumCecoban', altaCatIntFinancieros.regNumSiete, 8, e);
        });
		
		$('#paramNumIndeval').keypress(function(e){
			soloNumeros('#paramNumIndeval', altaCatIntFinancieros.regNumDoce, 8, e);
        });
		
		$('#paramIdIntIndeval').keypress(function(e){
			soloNumeros('#paramIdIntIndeval', altaCatIntFinancieros.regNumSiete, 8, e);
        });
		
		$('#paramFolIntIndeval').keypress(function(e){
			soloNumeros('#paramFolIntIndeval', altaCatIntFinancieros.regNumSiete, 8, e);
        });
		
		$( "#paramNumIndeval" ).bind('paste', function() {
			setTimeout(function() {
				var SOLO_NUMEROS = "0123456789";  
				var data= $( '#paramNumIndeval' ).val() ;
				var test="[^"+SOLO_NUMEROS+"]"; 
				var re = new RegExp(test,"gi"); 
			    var valor = data.replace(re,'');
		        $("#paramNumIndeval").val($.trim( valor ));
			});
		});		
		
		$( "#paramIdIntIndeval" ).bind('paste', function() {
			setTimeout(function() {
				var SOLO_NUMEROS = "0123456789";  
				var data= $( '#paramIdIntIndeval' ).val() ;
				var test="[^"+SOLO_NUMEROS+"]"; 
				var re = new RegExp(test,"gi"); 
				var valor = data.replace(re,'');
				$("#paramIdIntIndeval").val($.trim( valor ));
			});
		});

		$( "#paramNumCecoban" ).bind('paste', function() {
			setTimeout(function() {
				var SOLO_NUMEROS = "0123456789";  
				var data= $( '#paramNumCecoban' ).val() ;
				var test="[^"+SOLO_NUMEROS+"]"; 
				var re = new RegExp(test,"gi"); 
				var valor = data.replace(re,'');
				$("#paramNumCecoban").val($.trim( valor ));
			});
		});				
		
		$( "#paramFolIntIndeval" ).bind('paste', function() {
			setTimeout(function() {
				var SOLO_NUMEROS = "0123456789";  
				var data= $( '#paramFolIntIndeval' ).val() ;
				var test="[^"+SOLO_NUMEROS+"]"; 
				var re = new RegExp(test,"gi"); 
			    var valor = data.replace(re,'');
		        $("#paramFolIntIndeval").val($.trim( valor ));
			});
		});
		
		$( "#paramNumBanxico" ).bind('paste', function() {
			setTimeout(function() {
				var SOLO_NUMEROS = "0123456789";  
				var data= $( '#paramNumBanxico' ).val() ;
				var test="[^"+SOLO_NUMEROS+"]"; 
				var re = new RegExp(test,"gi"); 
				var valor = data.replace(re,'');
				$("#paramNumBanxico").val($.trim( valor ));
			});
		});
		
		function soloNumeros(id, expReg, long, e){
			var cad, k;
			document.all ? k = e.keyCode : k = e.which;
			altaCatIntFinancieros.cancel2(e);
			cad = $(id).val()+String.fromCharCode(k);
			if(!(cad ==="") && !expReg.test(cad) && k !== long){
				e.preventDefault();
				e.stopPropagation();
			}
		};
		
		$('#paramFchBaja').click(function(){
			$('#paramFchBaja').val('');		
		});
		
		function setValues(){
			$('#cveInterme').val($('#paramCveInterme').val());
			$('#tipoInterme').val($('#selectTipoInterme').val());
			$('#numCecoban').val($('#paramNumCecoban').val());
			$('#nombreCorto').val($('#paramNombreCorto').val());
			$('#nombreLargo').val($('#paramNombreLargo').val());
			$('#numPersona').val($('#paramNumPersona').val());
			$('#status').val($('#paramStatus').val());
			$('#numBanxico').val($('#paramNumBanxico').val());
			$('#numIndeval').val($('#paramNumIndeval').val());
			$('#idIntIndeval').val($('#paramIdIntIndeval').val());
			$('#folIntIndeval').val($('#paramFolIntIndeval').val());
			$('#cveSwiftCor').val($('#paramCveSwiftCor').val());
			$('#fchBaja').val($('#paramFchBaja').val());
		};
	
	},
	cancel2:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===190 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
	
	
};

$(document).ready(altaCatIntFinancieros.init);