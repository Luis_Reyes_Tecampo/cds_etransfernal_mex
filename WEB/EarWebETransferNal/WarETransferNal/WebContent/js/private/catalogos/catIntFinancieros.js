catIntFinancieros = {
	regNumDoce : new RegExp("^([0-9]{1,12})$"),
	regNumSiete : new RegExp("^([0-9]{1,7})$"),
	init:function(){
		createCalendarGuion('paramFchAlta','cal1');
		createCalendarGuion('paramFchBaja','cal2');
		createCalendarGuion('paramFchUltModif','cal3');
		$('#idBuscar').click(function(){
			$('#cveInterme').val($('#paramCveInterme').val());
			$('#tipoInterme').val($('#selectTipoInterme').val());
			$('#numCecoban').val($('#paramNumCecoban').val());
			$('#nombreCorto').val($('#paramNombreCorto').val());
			$('#nombreLargo').val($('#paramNombreLargo').val());
			$('#numPersona').val($('#paramNumPersona').val());
			$('#fchAlta').val($('#paramFchAlta').val());
			$('#fchBaja').val($('#paramFchBaja').val());
			$('#fchUltModif').val($('#paramFchUltModif').val());
			$('#usuarioModif').val($('#paramUsuarioModif').val());
			$('#numBanxico').val($('#paramNumBanxico').val());
			$('#cveSwiftCor').val($('#paramCveSwiftCor').val());
			$('#selecOpcion').val($('#tipoSeleccion').val());
			$('#idForm').attr('action','buscarIntFinancieros.do');
			$('#idForm').submit();
		});	
		$('#idAlta').click(function(){
			$('#idForm').attr('action','muestraAltaCatIntFinancieros.do?bandera=' + 'A');
			$('#idForm').submit();
		});	
		$('#idEliminar').click(function(){
			var checkboxCount = 0;
			$('input[id="check"]:checked').each(function() {
				checkboxCount++;
			});
			if (checkboxCount > 0) {
				jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','', function (respuesta){
					if(respuesta){
						$('#idForm').attr('action','elimCatIntFinancieros.do');
						$('#idForm').submit();					
					} 
				});
			} else {
				jAlert(mensajes["ED00068V"],
						mensajes["aplicacion"],
					   	   'ED00068V',
					   	   '');
			}
		});
		$('#idEditar').click(function(){
			var checkboxCount = 0;
			$('input[id="check"]:checked').each(function() {
				checkboxCount++;
			});
			if (checkboxCount == 1) {
			jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','', function (respuesta){
					if(respuesta){
						$('#checkSeleccionados').val(checkboxCount);
						$('#idForm').attr('action','muestraAltaCatIntFinancieros.do?bandera=' + 'E');
						$('#idForm').submit();
					} 
				});
			} else {
				jAlert(mensajes["ED00029V"],
					mensajes["aplicacion"],
					 	   'ED00029V',
					   	   '');
			}
		});
		$('#idExportar').click(function(){
			    $('#accion').val('ACT');
			    $('#idForm').attr('action', 'expIntFin.do');
				$('#idForm').submit(); 
		   });
		$('#btnExportarTodo').click(function() {
			$('#idForm').attr('action','exportarTodoIntFinancieros.do');
			$('#idForm').submit();
			});
		   
		$('#idLimpiar').click(function(){
			$('#idForm').attr('action','muestraCatIntFinancieros.do');
			$('#idForm').submit();
		});
		$('#paramNumBanxico').keypress(function(e){
			soloNumeros('#paramNumBanxico', catIntFinancieros.regNumDoce, 8, e);
        });
		$('#paramNumCecoban').keypress(function(e){
			soloNumeros('#paramNumCecoban', catIntFinancieros.regNumSiete, 8, e);
        });
		function soloNumeros(id, expReg, long, e){
			var cad, k;
			document.all ? k = e.keyCode : k = e.which;
			catIntFinancieros.cancel2(e);
			cad = $(id).val()+String.fromCharCode(k);
			if(!(cad ==="") && !expReg.test(cad) && k !== long){
				e.preventDefault();
				e.stopPropagation();
			}
		};
	},
	cancel2:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===190 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
};
$(document).ready(catIntFinancieros.init);