/** Declaracion de las variables a utilizar * */
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
/** Variables de busqueda **/
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

/** Se declaran las funciones* */
var cuentasFideico = {
    init : function() {
    $('#idCargar').click(function() {
    $('#idForm').attr('action', 'cargaArchivos.do');
    $('#idForm').submit();
    });
    /** Evento exportar * */
    $('#btnExportar').click(function() {
    $('#accion').val('ACT');
    $('#idForm').attr('action', 'exportarCuentasFideicomiso.do');
    $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
    $('#idForm').attr('action', 'exportarTodoCuentasFideicomiso.do');
    $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
    $('#paginaIni').val('1');
    $('#field').val('');
    $('#fieldValue').val('');
    $('#idForm').attr('action', 'consultaCuentasFideicomisoSPID.do');
    $('#idForm').submit();
    });
    /** Evento eliminar * */
    $('#btnEliminar').click(
    function() {
        continuaEliminando();
    });
    /** Eventos del filtro de busqueda* */
    $('.filField').keypress(function(e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    cadena = $(this).val();
    cadenaLength = cadena.length;
    cadenaLenghtMenos = cadenaLength - 1;
    cadenaAux = cadena.substring(0, cadenaLenghtMenos);
    if (charCode === 8 || charCode === 37 || charCode === 39) {
    return true;
    }
    if (charCode === 13) {
    if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
        $('#field').val($(this).attr('data-field'));
        $('#fieldValue').val($(this).val());
        $('#pagina').val('1');
    }
    $('#idForm').attr('action', 'consultaCuentasFideicomisoSPID.do');
    $('#idForm').submit();
    }
    return true;
    });
    /** Evento guardar* */
    $('#btnGuardar').on('click', function(e) {
    continuaGuardando(e);
    });
    /** Evento para remover clases* */
    $('.focusCombo').on('focus', function(e) {
    $('#' + e.target.id).removeClass('error-obj');
    });
    /** Evento editar* */
    $('#btnEditar').click(
    function() {
        /** Validacion de la seleccion * */
        var checkboxCount = 0;
        $('input[id="check"]:checked').each(function() {
        checkboxCount++;
        });
        if (checkboxCount === 1) {
        $('#idForm').attr('action', 'mantenimientoCuentasFideicomiso.do');
        $('#idForm').submit();
        } else if (checkboxCount === 0) {
        jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
        'ED00068V', '');
        } else {
        jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
        'ED00126V', '');
        }
    });
    $('#btnReturn').click(function() {
    $('#mform').attr('action', 'consultaCuentasFideicomisoSPID.do');
    $('#mform').submit();
    });
    $('#btnAgregar').click(function() {
    $('#idForm').attr('action', 'mantenimientoCuentasFideicomiso.do');
    $('#accion').val('alta'); 
    $('#idForm').submit();
    });
    $('#cuentaInput').on('keypress copy change focus', function(e){
    return soloNumeros(e);	
    });
    $('#cuentaInput').on('paste', function(){
    validaPaste();
    });
    }
};
/** Funcion para continuar la funcion inicial * */
function continuaGuardando(e) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
    function(respuesta) {
    if (respuesta) {
        e.stopPropagation();
        e.preventDefault();
        var resValidation = validarFrm('mform');
        if (resValidation.seguir) {
        preparaEnvio();
        } else {
        showModal('error', resValidation.mensajes,
        'Datos No Validos', '', '');
        }
    }
    });
}

/** Funcion para continuar la funcion inicial * */
function continuaEliminando() {
    var checkboxCount = 0;
    $('input[id="check"]:checked').each(function() {
    checkboxCount++;
    });
    if (checkboxCount > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
    'CD00010V', '', function(respuesta) {
        if (respuesta) {
        $('#idForm').attr('action',
        'eliminarCuentaFideicomiso.do');
        $('#idForm').submit();
        }
    });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
    'ED00068V', '');
    }
}
/** Funcion para enviar los datos al controller * */
function preparaEnvio() {
    /** Validacion * */
    if ($('#accion').val() === 'alta') {
    $('#mform').attr('action', 'agregarCuentaFideicomiso.do');
    } else {
    $('#mform').attr('action', 'editarCuentaFideicomiso.do');
    }
    /** Envio del formulario * */
    $('body #mform').submit();
}

/** Funcion solo numeros en la entrada **/
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
    e.preventDefault();
    }
    var numero = $('#cuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#cuentaInput').val('');
    }
}
function validaPaste() {
	var numero = $('#cuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#cuentaInput').val('');
    }
}
/** Inicializacion de las funciones* */
$(document).ready(cuentasFideico.init);