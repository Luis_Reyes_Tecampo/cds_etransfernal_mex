
$(document).ready(function() {	

$('#btnExport').click(function(){
	    $('#formCatPartSPID').attr('action', 'exportar.do');
		$('#formCatPartSPID').submit(); 
   });

});

function exportar(){
	$('#formCatPartSPID').attr('action', 'exportar.do');
	$('#formCatPartSPID').submit(); 
}

/**
 * funcion que obtiene el valor de un elemnto es especifico
 * @param elemento
 */
function obtieneValor(elemento){
	var valor= $(elemento).find("input")[1].value;
	var estatusCuenta = document.getElementById("cveIntermediario").value;
	
	if(estatusCuenta === "Cancelada"){
		jError("No se permite modificar cuentas con estatus Cancelada",'Error', "","");
	}else{
		document.forms['formCatPartSPID'].action = "buscarParticipantes.do";
		document.forms['formCatPartSPID'].submit();
	}

}

function cancelar(){
	document.forms['formAltaPartSPID'].action = "buscarParticipantes.do";
	document.forms['formAltaPartSPID'].submit();
}

/**
 * Funcion para buscar canal
 */
function buscarParticipantes(){
	document.forms['formCatPartSPID'].action = "buscarParticipantes.do";
	document.forms['formCatPartSPID'].submit();
}

/**
 * Funcion para guardar nuevo participante
 * @returns
 */
function guardaParticipantes(){
	
	var clave = document.getElementById("cveInter").value;
	
	if(clave == null || clave ===""){
		jInfo("Debe capturar el campo Clave",'Error', "","");
	}else{		
		document.forms['formAltaPartSPID'].action = "guardaCatalogo.do";
		document.forms['formAltaPartSPID'].submit();
	}
	
}

/**
 * funcion que valida los datos para el alta
 * 
 */
function validaDatos(e){
    if (e.keyCode == 13) {
        var tb = document.getElementById("cveInter");
        document.forms['formAltaPartSPID'].action = "validaCatalogo.do";
    	document.forms['formAltaPartSPID'].submit();
        
    }
}

/**
 * Limpia formulario
 * @returns
 */
function limpiarForm(){
	perform_submit("formCatPartSPID", "catalogoParticipantesSPID.do");
}

/**
 * Limpia formulario
 * @returns
 */
function limpiarParticipantes(){
	perform_submit("formAltaPartSPID", "altaCatalogo.do");
}

/**
 * funcion submit para el formulario
 * @param form - objeto formulario
 * @param action - objeto action
 */
function perform_submit(form, action) {
	$('#'+form).attr('action', action).submit();
}

/**
 * funcion que valida los datos para el alta
 * 
 */
function eliminaRegistro(){
	
	var checkboxes = document.getElementById("formCatPartSPID").marca;
	var cont = 0; 
	if(checkboxes.length){
		for (var x=0; x < checkboxes.length; x++) {
		 if (checkboxes[x].checked) {
		  cont = cont + 1;
		 }
		}
	}else if(checkboxes && checkboxes.checked) {
			  cont = cont + 1;
	}
	if(cont>0){
		jConfirm("\u00bfEst\u00E1 seguro de que desea eliminar Participante(s) del catalogo?", "Catalogo de Participantes", "",
				"", function(e) {
					if (e) {
						var checkboxValues = "";
						$('input[name="marca"]:checked').each(function() {
							checkboxValues += $(this).val() + "@";
						});
						//eliminamos la ultima coma
						checkboxValues = checkboxValues.substring(0, checkboxValues.length-1);
						document.forms['formCatPartSPID'].hdnCheckSeleccionados.value = checkboxValues;
						document.forms['formCatPartSPID'].action = "eliminaCatalogo.do";
						document.forms['formCatPartSPID'].submit();
					}
				}, "Cancelar");	
	} else {
		jInfo("Seleccione al menos un Participante", "Catalogo de Particpantes", "","");	
	}
}

/**
 * Funcion para el alta de nuevo participante
 * @returns
 */
function altaParticipantes(){
	document.forms['formCatPartSPID'].action = "altaCatalogo.do";
	document.forms['formCatPartSPID'].submit();
	
}