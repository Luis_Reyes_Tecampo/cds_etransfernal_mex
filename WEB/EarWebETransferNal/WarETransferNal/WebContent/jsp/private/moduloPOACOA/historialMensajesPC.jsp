<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00019V" var="ED00019V"/>
<spring:message code="codError.ED00020V" var="ED00020V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>

<spring:message code="moduloSPID.historialMensajes.text.titulo"      var="titulo"/>
<spring:message code="moduloSPID.historialMensajes.text.tituloDes"      var="tituloFuncionalidad"/>
<spring:message code="moduloSPID.historialMensajes.text.estatus"      var="estatus"/>
<spring:message code="moduloSPID.historialMensajes.text.al"      var="al"/>
<spring:message code="moduloSPID.historialMensajes.text.referencia"      var="referencia"/>
<spring:message code="moduloSPID.historialMensajes.text.del"      var="del"/>
<spring:message code="moduloSPID.historialMensajes.text.usuario"      var="usuario"/>
<spring:message code="moduloSPID.historialMensajes.text.Fecha"      var="Fecha"/>
<spring:message code="moduloSPID.historialMensajes.text.linkRegresar" var="regresar"/>
<spring:message code="moduloSPID.historialMensajes.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.historialMensajes.text.cveInstitucion" var="cveInstitucion"/>


<script type="text/javascript">var mensajes = { "aplicacion" : '${aplicacion}', "ED00019V" : '${ED00019V}', "ED00020V" : '${ED00020V}',"ED00021V" : '${ED00021V}', "ED00022V" : '${ED00022V}', "ED00023V" : '${ED00023V}', "ED00027V" : '${ED00027V}' };var referencia = '${e:forHtmlAttribute(refe)}';var tipoOrden = '${e:forHtmlAttribute(tipoOrden)}';</script>
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/historialMensajes.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/historialMensajes.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle" > </span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResHistorialMensajes.beanPaginador.paginaIni)}"/>
	<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResHistorialMensajes.beanPaginador.pagina)}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResHistorialMensajes.beanPaginador.paginaFin)}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${e:forHtmlAttribute(sortField)}">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
    <input type="hidden" id="tipo" name="tipo" value="${e:forHtmlAttribute(beanModulo.tipo)}"/>
	<input type="hidden" name="sortType" id="sortType" value="${e:forHtmlAttribute(sortType)}">
	<input type="hidden" id="modulo" name="modulo" value="${e:forHtmlAttribute(beanModulo.modulo)}"/>
	<input type="hidden" id="path" name="path" value="${e:forHtmlAttribute(beanModulo.path)}"/>
	<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
	<input type="hidden" value="${beanModulo.modulo}" id="bmModulo" />
	<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
	<input type="hidden" value="${beanModulo.tipo}" id="bmTipo" /> 
	<input type="hidden" name="ref" id="ref" value="${e:forHtmlAttribute(refe)}">
	<input type="hidden" name="tipoRef" id="tipoRef" value="${e:forHtmlAttribute(tipoOrden)}"/>
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${titulo}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			
			
				<table >
				<caption></caption>
					<tr>
						<td  class="nowrap widthx50">${referencia}</td>
						<td ><label for="fiel1d"></label><input id="fiel1d" class="Campos" type="text" size="40"  value="${beanResHistorialMensajes.ref}" readonly="readonly" ></td>
					</tr>
				</table>
				<table><caption></caption>
					<tr>
						<%@ include file="historialMsjCol1.jsp" %>
						<%@ include file="historialMsjCol2.jsp" %>
					</tr>
					<body>
						<c:forEach var="beanHistMsj" items="${beanResHistorialMensajes.listaHisMsj}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								  <td class="text_izquierda">${beanHistMsj.del}</td>
								  <td class="text_izquierda">${beanHistMsj.estatus}</td>
								  <td class="text_izquierda">${beanHistMsj.al}</td>
								  <td class="text_izquierda">${beanHistMsj.estatus2}</td>
								  <td class="text_izquierda">${beanHistMsj.usuario}</td>
								  <td class="text_izquierda">${beanHistMsj.fecha}</td>
							</tr>
						</c:forEach>
					</body>
				</table> 
			</div>
				<c:if test="${not empty beanResHistorialMensajes.listaHisMsj}">
				<%@ include file="historialMsjNav.jsp" %>
			</c:if>
		</div>

	<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
					<caption></caption>
						<tr>													
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="der_Des"><a href="javascript:;" >${regresar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>
				</div>
			</div>

</form>
<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');cargaTitulo('',false);</script></c:if>