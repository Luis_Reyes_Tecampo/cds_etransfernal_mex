<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${not empty beanResAvisoTraspasos.listaConAviso}">
		<div class="paginador">
			<div style="text-align: left; float:left; margin-left: 5px;">
			<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResAvisoTraspasos.totalReg} registros | Importe página: ${importePagina}</label>
			</div>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaIni == beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaIni != beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaAvisosTraspasos.do','INI');">&lt;&lt;${inicio}</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaAnt!='0' && beanResAvisoTraspasos.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaAvisosTraspasos.do','ANT');">&lt;${anterior}</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaAnt=='0' || beanResAvisoTraspasos.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
			<label id="txtPagina">${e:forHtmlAttribute(beanResAvisoTraspasos.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResAvisoTraspasos.beanPaginador.paginaFin)}</label>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaFin != beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaAvisosTraspasos.do','SIG');">${siguiente}&gt;</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaFin == beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaFin != beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaAvisosTraspasos.do','FIN');">${fin}&gt;&gt;</a></c:if>
			<c:if test="${beanResAvisoTraspasos.beanPaginador.paginaFin == beanResAvisoTraspasos.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
		</div>
		<input type="hidden" name="importeTotal" id="importeTotal" value="${e:forHtmlAttribute(beanResAvisoTraspasos.importeTotal)}" />
		<label>Importe Total: ${e:forHtmlAttribute(beanResAvisoTraspasos.importeTotal)}</label>
</c:if>