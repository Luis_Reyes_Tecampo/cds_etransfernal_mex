<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<th data-id="importe" class="text_centro sortField widthx122"
	scope="col">${importe}<c:if test="${sortField == 'importe'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th data-id="SIAC" class="text_centro sortField widthx122" scope="col">${e:forHtmlAttribute(referencia)}
	<c:if test="${sortField == 'SIAC'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>