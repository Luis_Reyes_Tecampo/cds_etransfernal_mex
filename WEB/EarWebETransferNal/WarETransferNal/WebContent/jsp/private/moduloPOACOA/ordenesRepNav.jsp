<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${not empty beanResOrdenReparacion.listaOrdenesReparacion}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResOrdenReparacion.totalReg} registros | Importe página: ${importePagina}</label>
					</div>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaIni == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaIni != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaOrdenesReparacion.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaAnt!='0' && beanResOrdenReparacion.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaOrdenesReparacion.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaAnt=='0' || beanResOrdenReparacion.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${e:forHtmlAttribute(beanResOrdenReparacion.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResOrdenReparacion.beanPaginador.paginaFin)}</label>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaOrdenesReparacion.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaOrdenesReparacion.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
				<label>Importe Total: ${e:forHtmlAttribute(importeTotal)}</label>
			</c:if>