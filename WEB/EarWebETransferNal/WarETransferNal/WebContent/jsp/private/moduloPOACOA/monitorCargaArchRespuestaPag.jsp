<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<div class="frameTablaEstandar">
		<%@ include file="monitorCargaArchRespuestaTabla.jsp" %>				
		<c:if test="${not empty beanResConsultaArchRespuesta.listaBeanResArchProc}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.regIni)} al ${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.regFin)} de un total de ${e:forHtmlAttribute(beanResConsultaArchRespuesta.totalReg)} registros</label>
				</div>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaIni == beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaIni != beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchResp.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaAnt!='0' && beanResConsultaArchRespuesta.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchResp.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaAnt=='0' || beanResConsultaArchRespuesta.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.paginaFin)}</label>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaFin != beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchResp.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaFin == beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaFin != beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchResp.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResConsultaArchRespuesta.beanPaginador.paginaFin == beanResConsultaArchRespuesta.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>
		</div>