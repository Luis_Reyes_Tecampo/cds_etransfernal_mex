<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<div class="paginador">
	<div style="text-align: left; float:left; margin-left: 5px;">
		<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResHistorialMensajes.totalReg} registros</label>
	</div>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaIni == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaIni != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','INI');">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaAnt!='0' && beanResHistorialMensajes.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','ANT');">&lt;${anterior}</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaAnt=='0' || beanResHistorialMensajes.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
	<label id="txtPagina">${e:forHtmlAttribute(beanResHistorialMensajes.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResHistorialMensajes.beanPaginador.paginaFin)}</label>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','SIG');">${siguiente}&gt;</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','FIN');">${fin}&gt;&gt;</a></c:if>
	<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
</div>