<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${not empty beanResListadoPagos.listadoPagos}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResListadoPagos.totalReg} registros | Importe página: ${importePagina}</label>
				</div>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaIni == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaIni != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaPagos.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaAnt!='0' && beanResListadoPagos.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaPagos.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaAnt=='0' || beanResListadoPagos.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${e:forHtmlAttribute(beanResListadoPagos.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResListadoPagos.beanPaginador.paginaFin)}</label>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaPagos.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaPagos.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${e:forHtmlAttribute(importeTotal)}</label>
			<input type="hidden" name="importeTotal" id="importeTotal" value="${e:forHtmlAttribute(importeTotal)}" />
		</c:if>