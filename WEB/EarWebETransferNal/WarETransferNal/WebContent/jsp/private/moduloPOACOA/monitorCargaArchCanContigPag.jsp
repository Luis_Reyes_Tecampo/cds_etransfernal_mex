<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
 
 			<c:if test="${not empty resMon.listBeanMonCargaArchCanConting}">
			<div class="paginador">
				<c:if test="${resMon.paginador.paginaIni == resMon.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.paginador.paginaIni != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchCanContig.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.paginador.paginaAnt!='0' && resMon.paginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchCanContig.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${resMon.paginador.paginaAnt=='0' || resMon.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${resMon.paginador.pagina} - ${resMon.paginador.paginaFin}</label>
				<c:if test="${resMon.paginador.paginaFin != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchCanContig.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin == resMon.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMonCargaArchCanContig.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin == resMon.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
		</div>
		</c:if>