<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../myHeader.jsp" %>
<c:set var="menuItem" value="contingencia" scope="request"/>
<c:set var="menuSubitem" value="catCanales" scope="request"/>
<%@ include file="../myMenuModuloContingencia.jsp" %>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<script	src="${pageContext.servletContext.contextPath}/js/private/cargaArchEnvio.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<spring:message code="moduloPOACOA.cargaArchEnv.text.txtProcesar" var="txtProcesar"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.textNomArchivo" var="textNomArchivo"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.textMedEnt" var="textMedEnt"/>

<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="moduloPOACOA.myMenu.text.contingCanales" var="contingCanales"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

	<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}'};</script>

	
	<div class="pageTitleContainer">
		<span class="pageTitle">${moduloPOACOA}</span> -${contingCanales}
	</div>

	<form name="formArchProc" id="formArchProc" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			
	<input id="nombreArchOculto" name="nombreArchOculto" type="hidden" value="${archRespSustituirHide}" />	
	<input id="cveMedEntregaOculto" name="cveMedEntregaOculto" type="hidden" value="${cveMedioEntregaHide}" />
			
		
	
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
			<caption></caption>
				<tbody>
					<tr>
							<td>${textNomArchivo}<label for="archProc" class="hide"></label><input id="archProc" name="archProc" type="file"/></td>	
					</tr>
					<tr>
							<td>${textMedEnt} 
							<label for="cmbCanal" class="hide"></label>
								<select id="cmbCanal" name="cmbCanal" class="CamposCompletar" >
									<c:if test="${listaCanales!=null}">						
										<c:forEach var="canalBean" items="${listaCanales}">
											<option  value="${canalBean.idCanal}">${canalBean.nombreCanal}</option>
										</c:forEach>
									</c:if>
								</select>
							</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		
	</div>
	</form>

	<div>
		<br/>
		<br/>		
	</div>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">				
				<table class="classWidth100">
				<caption></caption>
				<tr>
					<td class="cero classWidth50"></td>
					<td class="der classWidth50">
						<span><a href="javascript:cargaArchivo();" id="btnPocesar1a">${txtProcesar}</a></span>						
					</td>
				</tr>
			</table>
			</div>
		</div>
			
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}',''<c:if test="${codError == 'CD00166V'}">,function(e) {if (e) {document.forms['formArchProc'].action = "actualizarArchProc.do";document.forms['formArchProc'].submit();}}	</c:if>);</script>
	</c:if>
	
		