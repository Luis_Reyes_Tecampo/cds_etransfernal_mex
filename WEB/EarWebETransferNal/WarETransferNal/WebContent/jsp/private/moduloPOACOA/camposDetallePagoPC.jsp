<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>

<spring:message code="moduloSPID.detallePago.text.txtReferencia" var="txtReferencia"/>
<spring:message code="moduloSPID.detallePago.text.txtTransferencia" var="txtTransferencia"/>
<spring:message code="moduloSPID.detallePago.text.txtPuntoVenta" var="txtPuntoVenta"/>
<spring:message code="moduloSPID.detallePago.text.txtCola" var="txtCola"/>
<spring:message code="moduloSPID.detallePago.text.txtFormaLiquidacion" var="txtFormaLiquidacion"/>
<spring:message code="moduloSPID.detallePago.text.txtEmpresa" var="txtEmpresa"/>
<spring:message code="moduloSPID.detallePago.text.txtMedioEntrega" var="txtMedioEntrega"/>
<spring:message code="moduloSPID.detallePago.text.txtReferenciaMedio" var="txtReferenciaMedio"/>
<spring:message code="moduloSPID.detallePago.text.txtFechaCaptura" var="txtFechaCaptura"/>
<spring:message code="moduloSPID.detallePago.text.txtUsuarioSolicito" var="txtUsuarioSolicito"/>
<spring:message code="moduloSPID.detallePago.text.txtClaveOperacion" var="txtClaveOperacion"/>
<spring:message code="moduloSPID.detallePago.text.txtUsuarioCapturo" var="txtUsuarioCapturo"/>
<spring:message code="moduloSPID.detallePago.text.txtRefeConcent" var="txtRefeConcent"/>
<spring:message code="moduloSPID.detallePago.text.txtRefeMecanismo" var="txtRefeMecanismo"/>
<spring:message code="moduloSPID.detallePago.text.txtfechaEnvioConf" var="txtfechaEnvioConf"/>
<spring:message code="moduloSPID.detallePago.text.txtClaveRastreo" var="txtClaveRastreo"/>
<spring:message code="moduloSPID.detallePago.text.txtDireccionIP" var="txtDireccionIP"/>
<spring:message code="moduloSPID.detallePago.text.txtHoraInstruccionPago" var="txtHoraInstruccionPago"/>
<spring:message code="moduloSPID.detallePago.text.txtRefeNumerica" var="txtRefeNumerica"/>
<spring:message code="moduloSPID.detallePago.text.txtFolioPaqute" var="txtFolioPaqute"/>
<spring:message code="moduloSPID.detallePago.text.txtClaveDevolucion" var="txtClaveDevolucion"/>
<spring:message code="moduloSPID.detallePago.text.txtMotivoDevolucion" var="txtMotivoDevolucion"/>
<spring:message code="moduloSPID.detallePago.text.txtFechaInstruccionPago" var="txtFechaInstruccionPago"/>
<spring:message code="moduloSPID.detallePago.text.txtFolioPago" var="txtFolioPago"/>


<div >
	<div class="titleBuscadorSimple">Datos Generales</div>
	<table class="tablaContent">
	<caption></caption>
		<thead>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
		<tr>
			<td>${txtReferencia}</td>
			
			<td>
				<div>
				<label for="referencia"></label>
					<input type="text" disabled="disabled" id="referencia"
						name="referencia"
						value="${beanDetallePago.beanDatosGeneralesPago.referencia}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtTransferencia}</td>
			<td>
				<div>
				<label for="transferencia"></label>
					<input type="text" disabled="disabled" id="transferencia" name="tTransferencia"
						value="${beanDetallePago.beanDatosGeneralesPago.claveTransferencia}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtFormaLiquidacion}</td>
			<td>
				<div>
				<label for="formaLiquidacion"></label>
					<input type="text" disabled="disabled" id="formaLiquidacion"
						name="formaLiquidacion"
						value="${beanDetallePago.beanDatosGeneralesPago.formaLiquidacion}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>

		<tr>

			<td>${txtEmpresa}</td>
			<td>
				<div>
				<label for="empresa"></label>
					<input type="text" disabled="disabled" id="empresa" name="empresa"
						value="${beanDetallePago.beanDatosGeneralesPago.empresa}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtPuntoVenta}</td>
			<td>
				<div>
				<label for="puntoVenta"></label>
					<input type="text" disabled="disabled" id="puntoVenta"
						name="puntoVenta"
						value="${beanDetallePago.beanDatosGeneralesPago.puntoVenta}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtCola}</td>
			<td>
				<div>
				<label for="cola"></label>
					<input type="text" disabled="disabled" id="cola" name="cola"
						value="${beanDetallePago.beanDatosGeneralesPago.cola}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>


		</tr>

		<tr>
			<td>${txtMedioEntrega}</td>
			<td>
				<div>
				<label for="medioEntrega"></label>
					<input type="text" disabled="disabled" id="medioEntrega"
						name="medioEntrega"
						value="${beanDetallePago.beanDatosGeneralesPago.medioEntrega}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtReferenciaMedio}</td>
			<td>
				<div>
				<label for="referenciaMedio"></label>
					<input type="text" disabled="disabled" id="referenciaMedio"
						name="referenciaMedio"
						value="${beanDetallePago.beanDatosGeneralesPago.referenciaMedio}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtClaveOperacion}</td>
			<td>
				<div>
				<label for="claveOperacion"></label>
					<input type="text" disabled="disabled" id="claveOperacion"
						name="claveOperacion"
						value="${beanDetallePago.beanDatosGeneralesPago.claveOperacion}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

		</tr>

		<tr>
			<td>${txtUsuarioCapturo}</td>
			<td>

				<div>
				<label for="usuarioCapturo"></label>
					<input type="text" disabled="disabled" id="usuarioCapturo"
						name="usuarioCapturo"
						value="${beanDetallePago.beanDatosGeneralesPago.usuarioCapturo}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
			<td>${txtFechaCaptura}</td>
			<td>

				<div>
				<label for="fechaCaptura"></label>
					<input type="text" disabled="disabled" id="fechaCaptura"
						name="fechaCaptura"
						value="${beanDetallePago.beanDatosGeneralesPago.fechaCaptura}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtUsuarioSolicito}</td>
			<td>

				<div>
				<label for="usuarioSolicito"></label>
					<input type="text" disabled="disabled" id="usuarioSolicito"
						name="usuarioSolicito"
						value="${beanDetallePago.beanDatosGeneralesPago.usuarioSolicito}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>
		<tr>
			<td>${txtRefeConcent}</td>
			<td>

				<div>
				<label for="refeConcent"></label>
					<input type="text" disabled="disabled" id="refeConcent"
						name="refeConcent"
						value="${beanDetallePago.beanDatosGeneralesPago.refeConcent}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
			<td>${txtRefeMecanismo}</td>
			<td>

				<div>
				<label for="refeMecanismo"></label>
					<input type="text" disabled="disabled" id="refeMecanismo"
						name="refeMecanismo"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.refeMecanismo}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtClaveDevolucion}</td>
			<td>

				<div>
				<label for="claveDevolucion"></label>
					<input type="text" disabled="disabled" id="claveDevolucion"
						name="claveDevolucion"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.claveDevolucion}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>

		<tr>
			<td>${txtMotivoDevolucion}</td>
			<td>

				<div>
				<label for="motivoDevolucion"></label>
					<input type="text" disabled="disabled" id="motivoDevolucion"
						name="motivoDevolucion"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.motivoDevolucion}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
			<td>${txtFechaInstruccionPago}</td>
			<td>

				<div>
				<label for="fechaInstruccionPago"></label>
					<input type="text" disabled="disabled" id="fechaInstruccionPago"
						name="fechaInstruccionPago"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.fechaInstruccionPago}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtHoraInstruccionPago}</td>
			<td>

				<div>
				
				<label for="horaInstruccionPago"></label>
					<input type="text" disabled="disabled" id="horaInstruccionPago"
						name="horaInstruccionPago"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.horaInstruccionPago}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>
		<tr>
			<td>${txtRefeNumerica}</td>
			<td>

				<div>
				<label for="refeNumerica"></label>
					<input type="text" disabled="disabled" id="refeNumerica"
						name="refeNumerica"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.refeNumerica}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
			<td>${txtFolioPaqute}</td>
			<td>

				<div>
				<label for="folioPaquete"></label>
					<input type="text" disabled="disabled" id="folioPaquete"
						name="folioPaquete"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.folioPaquete}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtFolioPago}</td>
			<td>

				<div>
				<label for="folioPago"></label>
					<input type="text" disabled="disabled" id="folioPago"
						name="folioPago"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.folioPago}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>

		<tr>
			<td>${txtfechaEnvioConf}</td>
			<td>

				<div>
				<label for="fechaEnvioConf"></label>
					<input type="text" disabled="disabled" id="fechaEnvioConf"
						name="fechaEnvioConf"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.envConf}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
			<td>${txtClaveRastreo}</td>
			<td>

				<div>
				<label for="claveRastreo"></label>
					<input type="text" disabled="disabled" id="claveRastreo"
						name="claveRastreo"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.claveRastreo}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>

			<td>${txtDireccionIP}</td>
			<td>

				<div>
				<label for="direccionIP"></label>
					<input type="text" disabled="disabled" id="direccionIP"
						name="direccionIP"
						value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.direccionIP}"
						class="inputT Campos" size="20" maxlength="12">
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</div>