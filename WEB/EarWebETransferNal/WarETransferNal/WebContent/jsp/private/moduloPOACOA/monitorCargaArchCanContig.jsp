<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../myHeader.jsp" %>
<c:set var="menuItem" value="moduloPOACOA" scope="request"/>
<c:set var="menuSubitem" value="monCargaArchCanContig" scope="request"/>
<%@ include file="../myMenuModuloPOACOA.jsp" %>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
 
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.estatusCargaArchivo" var="estatusCargaArchivo"/>
<spring:message code="moduloPOACOA.myMenu.text.monitorContingCanales" var="monitorContingCanales"/>


<spring:message code="moduloPOACOA.monCargaArchCanContig.text.nomArchivo" var="labNomArchivo"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.estatus" var="labEstatus"/> 
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.numPagos" var="labNumPagos"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.numPagosErr" var="labNumPagosErr"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.totMonto" var="labTotMonto"/> 
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.totMontoErr" var="labTotMontoErr"/> 


<spring:message code="moduloCDA.monitorCargaArchivosHist.botonLink.actualizar" var="actualizar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitorCargaArchCanContig.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>


	<%-- Componente titulo de página --%>
	<div class="pageTitleContainer">
		<span class="pageTitle">${moduloPOACOA}</span> - ${monitorContingCanales}
	</div>

	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas"></div>
		<div class="contentTablaVariasColumnas">
			<table>
			<caption></caption>
				<tr>

					<th id="colNombre"  class="text_centro classWidth300" scope="col">${labNomArchivo}</th>
					<th id="colEstatus"  class="text_centro classWidth300" scope="col">${labEstatus}</th>
					<th id="colNumPagos" class="text_centro classWidth300" scope="col">${labNumPagos}</th>
					<th id="colNumPagosErr"  class="text_centro classWidth300" scope="col">${labNumPagosErr}</th>					
					<th id="colMontoTot"  class="text_centro classWidth300" scope="col">${labTotMonto}</th>
					<th id="colMontoErr" class="text_centro classWidth300" scope="col">${labTotMontoErr}</th>
					
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
					<c:forEach var="beanMonCargaArchCanConting" items="${resMon.listBeanMonCargaArchCanConting}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}"> 	
					
								
						<td class="text_izquierda">${beanMonCargaArchCanConting.nombreArchivo}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.estatus}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.numPagos}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.numPagosErr}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.totalMonto}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.totalMontoErr}</td>
						
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="monitorCargaArchRespuestaPag.jsp" %>	
		 
				
	</div>
	
	
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
			<caption></caption>
				<tr>
					<td class="cero classWidth50">${espacioEnBlanco}</td>
					
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
							<td class="der classWidth50"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
						</c:when>
						<c:otherwise>
							<td class="der_Des classWidth50"><a href="javascript:;" >${actualizar}</a></td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
		</div>
	</div>
	
	</div>
	
	<form name="idForm" id="idForm" method="post" action="">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="paginaIni" id="paginaIni" value="${resMon.paginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${resMon.paginador.pagina}"/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${resMon.paginador.paginaFin}"/>
		<input type="hidden" name="accion" id="accion" value=""/>
		<input type="hidden" name="idPeticion" id="idPeticion" value=""/>
		
	</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
	</c:if>
 