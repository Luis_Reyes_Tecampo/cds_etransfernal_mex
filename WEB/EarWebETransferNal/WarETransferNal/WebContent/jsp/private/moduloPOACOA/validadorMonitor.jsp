<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>  
<c:if test="${beanModulo.modulo eq 'spei'}">
	<c:if test="${beanModulo.tipo eq 'coa'}">
		<jsp:include page="../myMenuModuloPOACOA.jsp" flush="true">
 		   <jsp:param name="menuItem"    value="moduloPOACOA" />
    	   <jsp:param name="menuSubitem" value="monitorCOA" />
		</jsp:include>

	</c:if>
	<c:if test="${beanModulo.tipo eq 'poa'}">
		<jsp:include page="../myMenuModuloPOACOA.jsp" flush="true">
 		   <jsp:param name="menuItem"    value="moduloPOACOA" />
    	   <jsp:param name="menuSubitem" value="monitorPOA" />
		</jsp:include>
	</c:if>
</c:if>
<c:if test="${beanModulo.modulo eq 'spid'}">
	<c:if test="${beanModulo.tipo eq 'coa'}">
		<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
 		   <jsp:param name="menuItem"    value="moduloPOACOASPID" />
    	   <jsp:param name="menuSubitem" value="monitorCOA" />
		</jsp:include>
	</c:if>
	<c:if test="${beanModulo.tipo eq 'poa'}">
		<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
 		   <jsp:param name="menuItem"    value="moduloPOACOASPID" />
    	   <jsp:param name="menuSubitem" value="monitorPOA" />
		</jsp:include>
	</c:if>
</c:if>