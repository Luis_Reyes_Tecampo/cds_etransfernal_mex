<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<th data-id="estatus2" class="text_centro nowrap sortField widthx122"
	scope="col">${estatus}<c:if test="${sortField == 'estatus2'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th data-id="usuario" class="text_centro nowrap sortField widthx122"
	scope="col">${e:forHtmlAttribute(usuario)}<c:if test="${sortField == 'usuario'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th data-id="fecha" class="text_centro nowrap sortField widthx122"
	scope="col">${e:forHtmlAttribute(Fecha)}<c:if test="${sortField == 'fecha'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>