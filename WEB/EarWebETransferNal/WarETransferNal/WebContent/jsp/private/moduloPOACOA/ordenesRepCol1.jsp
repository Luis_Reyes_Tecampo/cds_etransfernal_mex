<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<th data-id="referencia" class="text_centro sortField widthx223"
	scope="col">${e:forHtmlAttribute(refe)}<c:if test="${sortField == 'referencia'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th data-id="cveCola" class="text_centro sortField widthx223"
	scope="col">${cola}<c:if test="${sortField == 'cveCola'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th data-id="cveTran" class="text_centro sortField widthx223"
	scope="col">${claveTran}<c:if test="${sortField == 'cveTran'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>