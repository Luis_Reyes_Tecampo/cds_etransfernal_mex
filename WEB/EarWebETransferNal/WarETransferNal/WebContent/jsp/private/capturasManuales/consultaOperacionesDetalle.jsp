<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* consultaOperacionesDetalle.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     31/03/2017 12:00 Creacion de la pantalla
*
***********************************************************************************************
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="ConsultaOperaciones" />
</jsp:include>

<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.titulo" var="titulo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.detalleOperacion" var="txtSubtitulo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.regresar" var="regresar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.modificar" var="modificar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.guardar" var="guardar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.usuarioCaptura" var="lblUsuarioCaptura"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.usuarioAutoriza" var="lblUsuarioAutoriza"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.estatusOperacion" var="lblEsatusOperacion"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.template" var="lblTemplate"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.fechaCaptura" var="lblFechaCaptura"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.folio" var="lblFolio"/>

<%-- Generales --%>
<spring:message code="general.espacioEnBlanco" 	var="espacioEnBlanco"/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/consultaOperacionesDetalle.js"></script>

<c:set var="searchString" value="${seguTareas}"/>
<form id="idForm" action="" method="post">

	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanRes.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanRes.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanRes.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.paginaSig" id="paginaSig" value="${beanRes.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.paginaAnt" id="paginaAnt" value="${beanRes.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.accion" id="accion" value="ACT"/>
	
	<input type="hidden" id="usuarioCaptura" name="usuarioCaptura" value="${beanRes.usuarioCaptura}"/> 		
	<input type="hidden" id="usuarioAutoriza" name="usuarioAutoriza" value="${beanRes.usuarioAutoriza}"/> 
	<input type="hidden" id="estatusOperacion" name="estatusOperacion" value="${beanRes.estatusOperacion}"/> 
	<input type="hidden" id="template" name="template" value="${beanRes.template}"/> 
 	<input type="hidden" id="fechaCaptura" name="fechaCaptura" value="${beanRes.fechaCaptura}"/>
 	<input type="hidden" id="folio" name="folio" value="${beanRes.folio}"/>
 	
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${txtSubtitulo}</div>
		<div class="contentBuscadorSimple">
			<table>
			<caption></caption>
				<tbody>
					<tr>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblUsuarioAutoriza}</label></td>
						<td class="text_izquierda"><input type="text" id="idUsuarioAutoriza"
							name="idUsuarioAutoriza" value="${beanRes.usuarioAutoriza}" readonly="readonly" class="Campos" style="font-size: 14px;  text-align: center;"/></td>
						<td class="text_derecha"></td>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblTemplate}</label></td>
						<td class="text_izquierda"><input type="text" id="idTemplate"
							name="idTemplate" value="${beanRes.template}" readonly="readonly"class="Campos" style="font-size: 14px;  text-align: center;" />
						</td>
					</tr>
					<tr>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblUsuarioCaptura}</label></td>
						<td class="text_izquierda"><input type="text" id="idUsuarioCaptura"
							name="idUsuarioCaptura" value="${beanRes.usuarioCaptura}" readonly="readonly" class="Campos" style="font-size: 14px;  text-align: center;" /></td>
						<td class="text_derecha"></td>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblFechaCaptura}</label></td>
						<td class="text_izquierda"><input type="text" id="idFechaCaptura"
							name="idFechaCaptura" value="${beanRes.fechaCaptura}" readonly="readonly" class="Campos" style="font-size: 14px;  text-align: center;"/>
						</td>
					</tr>
					<tr>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblEsatusOperacion}</label></td>
						<td class="text_izquierda"><input type="text" id="idEstatusOperacion"
							name="idEstatusOperacion" value="${beanRes.estatusOperacion}" readonly="readonly" class="Campos" style="font-size: 14px;  text-align: center;" /></td>
						<td class="text_derecha"></td>
						<td class="text_derecha" width="80px"><label style="font-size: 11px;">${lblFolio}</label></td>
						<td class="text_izquierda"><input type="text" id="idFolio"
							name="idFolio" value="${beanRes.folio}" readonly="readonly" class="Campos" style="font-size: 14px;  text-align: center;"/>
						</td>
					</tr>
				</tbody>
			</table>
			<jsp:include page="layoutOperacion.jsp" />
		</div>
	</div>
	<%-- Componente tabla estandar --%>
	
	
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
				<caption></caption>
					<tr>
						<c:choose>
    						<c:when test="${beanRes.permisoModicar && beanRes.editando == false && fn:containsIgnoreCase(searchString,'MODIFICAR')}">
    						<td width="279" class="izq"><a id="idModificarDetalle" href="javascript:;">${modificar}</a></td>
    						</c:when>    
    						<c:otherwise>
    						<td width="279" class="izq_Des"><a href="javascript:;">${modificar}</a></td>
    						</c:otherwise>
						</c:choose>						
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idRegresarDetalle" href="javascript:;">${regresar}</a></td>
					</tr>
					<tr>
					<c:choose>
    						<c:when test="${beanRes.permisoModicar && beanRes.editando && fn:containsIgnoreCase(searchString,'MODIFICAR')}">
    						<td width="279" class="izq"><a id="idGuardarDetalle" href="javascript:;">${guardar}</a></td>
    						</c:when>    
    						<c:otherwise>
    						<td width="279" class="izq_Des"><a href="javascript:;">${guardar}</a></td>
    						</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td class="cero"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>	


<jsp:include page="../myFooter.jsp" flush="true"/>

<%-- Valida Error --%>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>