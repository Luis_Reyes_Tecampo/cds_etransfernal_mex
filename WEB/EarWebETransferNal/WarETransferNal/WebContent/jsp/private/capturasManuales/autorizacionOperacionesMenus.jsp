<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.autorizar" var="autorizar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.cancelar" var="cancelar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.reparar" var="reparar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.apartar" var="apartar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.desapartar" var="liberarTodo"/>

<c:set var="searchString" value="${seguTareas}"/>

<table>
	<caption></caption>
	<tr>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && beanResponse.operacionesApartadasPorPagina}">
				<td class="izq"><a id="idAutorizar" href="javascript:;">${autorizar}</a></td>
			</c:when>
			<c:otherwise>
				<td class="izq_Des"><a href="javascript:;">${autorizar}</a></td>
			</c:otherwise>
		</c:choose>

		<td class="odd">${espacioEnBlanco}</td>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') && beanResponse.operacionesApartadasPorPagina}">
				<td width="279" class="der"><a id="idReparar" href="javascript:;" >${reparar}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="der_Des"><a href="javascript:;" >${reparar}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR') && beanResponse.operacionesApartadasPorPagina}">
				<td width="279" class="izq"><a id="idCancelar" href="javascript:;">${cancelar}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="izq_Des"><a href="javascript:;">${cancelar}</a></td>
			</c:otherwise>
		</c:choose>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<c:choose>
			<c:when test="${beanResponse.operacionesApartadasUsr}">
				<td width="279" class="der"><a id="idDespartar" href="javascript:;">${liberarTodo}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="der_Des"><a href="javascript:;">${liberarTodo}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<td width="279" class="izq_Des"></td>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<c:choose>
			<c:when test="${not empty beanResponse.listOperacionesPendAutorizar}">
				<td width="279" class="der"><a id="idApartar" href="javascript:;">${apartar}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="der_Des"><a href="javascript:;">${apartar}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>
</table>