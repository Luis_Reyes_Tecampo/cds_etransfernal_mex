<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="AutorizarOperaciones" />
</jsp:include>

<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="capturasManuales"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.titulo" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.subtituloFun" var="subtituloFuncion"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.template" var="template"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.fechacaptura" var="fechaCaptura"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.button.buscar" var="buscar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.seleccion" var="seleccion"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.folio" var="folio"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.importe" var="importe"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.usr.captura" var="usrCaptura"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.usr.aut.cancel" var="usrAutCancel"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.estatus" var="estatus"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.apartado" var="apartado"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.autorizar" var="autorizar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.cancelar" var="cancelar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.reparar" var="reparar"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.link.desapartar" var="desapartar"/>


<script src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/autorizacionOperaciones.js"
		type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


<form id="idForm" action="" method="post">
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResponse.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResponse.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResponse.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.accion" id="accion" value=""/>
	
	<input type="hidden" id="template" name="template" value="${beanResponse.template}"/>
	<input type="hidden" id="FechaCaptura" name="FechaCaptura" value="${beanResponse.fechaOperacion}"/>
	
	<div class="pageTitleContainer">
			<span class="pageTitle">${capturasManuales}</span> - ${tituloModulo}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tr>
					<td width="150" >
						<span>${template}:</span>
					</td>
					<td width="180">
						<input id="inputTemplate" name="inputTemplate" value="${beanResponse.template}" class="Campos" type="text" size="20"/>
					</td>
					
					<td width="150" >
						<span>${fechaCaptura}:</span>
					</td>
					<td width="150">
						<input id="inputFechaCaptura" name="inputFechaCaptura" value="${beanResponse.fechaOperacion}" class="Campos" type="text" size="10" readonly="readonly" />
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
					</td>
						
					<td width="121" class="izq">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
					</td>			
				</tr>
			</table>
		</div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th style="white-space:nowrap; width:122px" class="text_izquierda">${seleccion}</th>
					<th style="white-space:nowrap; width:223px" class="text_centro" scope="col">${folio}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${importe}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${template}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${usrCaptura}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${usrAutCancel}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${estatus}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${fechaCaptura}</th>
					<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${apartado}</th>
				</tr>
		
				<tr>
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanOpericonesPend" items="${beanResponse.listOperacionesPendAutorizar}" varStatus="rowCounter">
				   	<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_centro">
							<input type="checkbox" id="idSeleccion" name="listOperacionesSeleccionadas[${rowCounter.index}].seleccionado">
						</td>
						<td class="text_centro">
							<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].folio" value="${listOperacionesSeleccionadas.folio}"/>
							<a id="idFolio${beanOpericonesPend.folio}" href="javascript:;" onclick="autorizarOperaciones.despliegaDetalle('${beanOpericonesPend.folio}')">${beanOpericonesPend.folio}</a>
						</td>
						<td class="text_centro">${beanOpericonesPend.importe}</td>
						<td class="text_centro">${beanOpericonesPend.template}</td>
						<td class="text_centro">${beanOpericonesPend.usrCaptura}</td>
						<td class="text_centro">${beanOpericonesPend.usrAut_Cancela}</td>
						<td class="text_centro">${beanOpericonesPend.estatus}</td>
						<td class="text_centro">${beanOpericonesPend.fechaCaptura}</td>
						<td class="text_centro">${beanOpericonesPend.usrApartado}</td>
					</tr>
				</c:forEach>
		
		<%-- 		   <c:forEach var="beanMovimientoHistCDA" items="${beanResConsMovHistCDA.listBeanMovimientoCDA}" varStatus="rowCounter"> --%>
<!-- 					<tr class=""> -->
<!-- 						<td class="text_centro"> -->
<!-- 							<input type="checkbox" id="idSeleccion" name="idSeleccion"> -->
<!-- 						</td> -->
<!-- 						<td class="text_centro"> -->
<%-- 							<a id="idFolio" href="javascript:;" onclick="autorizarOperaciones.despliegaDetalle('${beanOpericonesPend.folio}')">numOpe 12345</a> --%>
<!-- 						</td> -->
<!-- 						<td class="text_centro">$14,890</td> -->
<!-- 						<td class="text_centro">Template 1</td> -->
<!-- 						<td class="text_centro">User1</td> -->
<!-- 						<td class="text_centro">User2</td> -->
<!-- 						<td class="text_centro">N/A</td> -->
<!-- 						<td class="text_centro">21/03/2017</td> -->
<!-- 						<td class="text_centro">---Apartada---</td> -->
<!-- 					</tr> -->
		<%-- 		</c:forEach> --%>
				</tbody>
			</table>
		</div>
		
		<c:if test="${not empty beanResponse.listOperacionesPendAutorizar}">
			<jsp:include page="../paginador.jsp" >	  					
						<jsp:param name="paginaIni" value="${beanResponse.paginador.paginaIni}" />
						<jsp:param name="pagina" value="${beanResponse.paginador.pagina}" />
						<jsp:param name="paginaAnt" value="${beanResponse.paginador.paginaAnt}" />
						<jsp:param name="paginaFin" value="${beanResponse.paginador.paginaFin}" />
						<jsp:param name="servicio" value="consultarOperacionesPendientes.do" />
			</jsp:include>
		</c:if>
	
		<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'')}">
									<td class="izq"><a id="idAutorizar" href="javascript:;">${autorizar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${autorizar}</a></td>
								</c:otherwise>
							</c:choose>
	
							<td class="odd">${espacioEnBlanco}</td>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'')}">
								<td width="279" class="der"><a id="idReparar" href="javascript:;" >${reparar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;" >${reparar}</a></td>
							</c:otherwise>
						</c:choose>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'')}">
									<td width="279" class="izq"><a id="idCancelar" href="javascript:;">${cancelar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;">${cancelar}</a></td>
								</c:otherwise>
							</c:choose>
							<td width="6" class="odd">${espacioEnBlanco}</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'')}">
									<td width="279" class="der"><a id="idDespartar" href="javascript:;">${desapartar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;">${desapartar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>
				</div>
			</div>
	</div>
</form>
