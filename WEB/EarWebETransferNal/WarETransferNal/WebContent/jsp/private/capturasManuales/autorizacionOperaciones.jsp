<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="AutorizarOperaciones" />
</jsp:include>

<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="capturasManuales"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.titulo" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.subtituloFun" var="subtituloFuncion"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.template" var="template"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.fechacaptura" var="fechaCaptura"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.button.buscar" var="buscar"/>

<c:set var="searchString" value="${seguTareas}"/>

<script src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/autorizacionOperaciones.js"
		type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>

<form id="idForm" action="" method="post">
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResponse.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResponse.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResponse.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.regIni" id="regIni" value="${beanResponse.paginador.regIni}"/>
	<input type="hidden" name="paginador.regFin" id="regFin" value="${beanResponse.paginador.regFin}"/>
	<input type="hidden" name="paginador.paginaAnt" id="regIni" value="${beanResponse.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.paginaSig" id="regFin" value="${beanResponse.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.accion" id="accion" value="${beanResponse.paginador.accion}"/>
	
	<input type="hidden" id="template" name="template" value="${beanResponse.template}"/>
	<input type="hidden" id="fechaCaptura" name="fechaCaptura" value="${beanResponse.fechaOperacion}"/>
	<input type="hidden" id="usrAdmin" name="usrAdmin" value="${beanResponse.usrAdmin}"/>
	
	<input type="hidden" id="cveTemplate" name="cveTemplate" value=""/>
	<input type="hidden" id="folio" name="folio" value=""/>
	
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span>
	</div>
	
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td width="150" >
						<span>${template}:</span>
					</td>
					<td width="180">
						<input id="inputTemplate" value="${beanResponse.template}" class="Campos" type="text" size="20" name="inputTemplate"/>
					</td>
					
					<td width="150" >
						<span>${fechaCaptura}:</span>
					</td>
					<td width="150">
						<input id="inputFechaCaptura" value="${beanResponse.fechaOperacion}" class="Campos" type="text" size="10" readonly="readonly" name="inputFechaCaptura"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
					</td>
						
					<td width="121" class="izq">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
					</td>			
				</tr>
			</table>
		</div>
	</div>
	<div class="frameTablaVariasColumnas">
		
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">			
			<jsp:include page="autorizacionOperacionesGreed.jsp" />
		</div>

		<c:if test="${not empty beanResponse.listOperacionesPendAutorizar}">
			<jsp:include page="../paginador.jsp" >	  					
						<jsp:param name="paginaIni" value="${beanResponse.paginador.paginaIni}" />
						<jsp:param name="pagina" value="${beanResponse.paginador.pagina}" />
						<jsp:param name="paginaAnt" value="${beanResponse.paginador.paginaAnt}" />
						<jsp:param name="paginaFin" value="${beanResponse.paginador.paginaFin}" />
						<jsp:param name="servicio" value="consultarOperacionesPendientes.do" />
			</jsp:include>
		</c:if>

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<jsp:include page="autorizacionOperacionesMenus.jsp" />
			</div>
		</div>
		
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>