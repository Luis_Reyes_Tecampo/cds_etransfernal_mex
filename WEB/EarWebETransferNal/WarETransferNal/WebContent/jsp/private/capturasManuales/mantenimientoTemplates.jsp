<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- 
/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * mantenimientoTemplates.jsp
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
 --%>
<%-- Librerias tags --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<%-- Include de Menu --%>
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="mantenimientoTemplates" />
</jsp:include>

<%-- Mensajes: Etiquetas i18 --%>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.sel.estatus" var="lEstatus"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.seleccion" var="tseleccion"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.folio" var="tfolio"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.template" var="ttemplate"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.layout" var="tlayout"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.estatus" var="testatus"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.usuario" var="tusuario"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.fechaCaptura" var="tcaptura"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.table.text.fechaBaja" var="tmodifica"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.boton.buscar" var="bbuscar"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.link.alta" var="lalta"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.link.eliminar" var="leliminar"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.consultaTemplates.link.clonar" var="lclonar"/>

<spring:message code="general.filtroBusqueda" var="filtroBusqueda"/>
<spring:message code="general.regresar" var="regresar"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<%-- Control de excepciones o errores --%>
<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.ED00068V" var="ED00068V" /> <%-- Se requiere seleccionar una --%>
<spring:message code="codError.CD00010V" var="CD00010V"/> <%-- solicitud de confirma --%>
<spring:message code="codError.OK00020V" var="OK00020V"/> <%-- Se eliminaron los siguientes --%>
<spring:message code="codError.CD00171V" var="CD00171V"/> <%-- No se pudieron eliminar --%>

<spring:message code="codError.CMMT001V" var="CMMT001V"/> <%--Template guardado --%>
<spring:message code="codError.CMMT003V" var="CMMT003V"/> <%--Template con operaciones as --%>
<spring:message code="codError.CMMT004V" var="CMMT004V"/> <%--Template eliminados --%>
<spring:message code="codError.CMMT007V" var="CMMT007V"/> <%-- campos oblig. --%>
<spring:message code="codError.CMMT008V" var="CMMT008V"/> <%-- seleccionar un template --%>

<%-- Declaracion de mensajes relacionados al JS --%> 
 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"CD00010V":'${CD00010V}', "ED00068V":'${ED00068V}', 
			 "OK00020V":'${OK00020V}', "CD00171V":'${CD00171V}', 
			 "CMMT001V" : '${CMMT001V}', "CMMT003V" : '${CMMT003V}', "CMMT004V" : '${CMMT004V}',
			 "CMMT007V" : '${CMMT007V}', "CMMT008V" : '${CMMT008V}'
          };
 </script>


<%-- JS Manipulacion manual de formularios --%>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/admonTemplates.js"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloFuncionalidad}</span>
</div>

<%-- FORMULARIO --%>
<form id="idForm" action="" method="post">
	
	<%-- Campos adicionales para enviarse con el formulario POST --%>
	<input type="hidden" name="paginador.accion" id="accion" value=""/>
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanViewRespuesta.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanViewRespuesta.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanViewRespuesta.paginador.paginaFin}"/>	
	<input type="hidden" name="totalRegistros" id="totalRegs" value="${beanViewRespuesta.totalRegistros}"/>
	
	<%-- Filtro, busquedaActiva --%>
	<input type="hidden" name="filtroEstado" id="filtroEstado"  value="${beanViewRespuesta.filtroEstado}"/>	
	<input type="hidden" name="forward" id="forward"  value="${beanViewRespuesta.forward}"/>
	
	<%-- idTemplate a editar, modoEdicion --%>
	<input type="hidden" id="idTemplate" name="template.idTemplate" value="-1"/>
	<input type="hidden" name="modoEdicion" id="modoEdicion"  value="false"/>
		
	<input type="hidden" name="countlistaTemplates" value="${fn:length(beanViewRespuesta.listaTemplates)}"/>
		
	<%-- FRAME DEL BUSCADOR --%>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">		
		<table>
		<caption></caption>
			<tbody>
			<tr>
				<td class="text_izquierda"> ${lEstatus}</td>
				<td>
					<select id="selectEstatus" name="selectEstatus" class="Campos" >
						<option value="">${seleccionarOpcion}</option>
						
						<c:forEach items ="${beanViewRespuesta.estados}" var="opt" varStatus="rowCounter">
						 <c:choose>
							<c:when test="${opt == beanViewRespuesta.filtroEstado}">
								<option value="${opt}" selected="selected">${opt}</option>
							</c:when>
							<c:otherwise>
								<option value="${opt}" >${opt}</option>
							</c:otherwise>
						  </c:choose>
						</c:forEach>
					</select>
					
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(  searchString,\"CONSULTAR\")}">
							<span><a id="idBuscarTemplate" href="javascript:;">${bbuscar}</a></span>
						</c:when>
						<c:otherwise>
							<span class="btn_Des"><a href="javascript:;">${bbuscar}</a></span>
						</c:otherwise>
					</c:choose> 
				</td>					
			</tr>
			</tbody>
		</table>
		</div>
		
		<%-- TABLA DE INFORMACION --%>		
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
			<caption></caption>
				<tr>
					<th style="font-size:11px; width:5%;">${tseleccion}</th>
					<th style="font-size:11px; width:10%;">${tfolio}</th>
					<th style="font-size:11px; width:25%;">${ttemplate}</th>
					<th style="font-size:11px; width:25%;">${tlayout}</th>
					<th style="font-size:11px; width:10%;">${testatus}</th>
					<th style="font-size:11px; width:20%;">${tusuario}</th>
					<th style="font-size:11px; width:15%;">${tcaptura}</th>
					<th style="font-size:11px; width:15%;">${tmodifica}</th>
				</tr>
		
				<tr>
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
					<c:forEach var="template" items="${beanViewRespuesta.listaTemplates}" varStatus="rowCounter">
						<tr class="${rowCounter.count % 2 eq 0 ? 'odd3' : 'odd'}">
							<td class="text_centro">
								<input name="listaTemplates[${rowCounter.index}].seleccionado" type="checkbox" id="chkSel${rowCounter.index}"
									${template.seleccionado? 'checked' : ''}
									class="groupSeleccionTemplate" />
							</td>
						
							<td class="text_centro">								
								<c:choose>
									<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
										<a id="${template.idTemplate}" href="javascript:;" class="editTemplateAction"><fmt:formatNumber pattern="0000" value="${template.idRegistro}" /></a>
									</c:when>
									<c:otherwise>
										<fmt:formatNumber pattern="0000" value="${template.idRegistro}" />
									</c:otherwise>
								</c:choose>
								
								<input type="hidden" name="listaTemplates[${rowCounter.index}].idRegistro" value="${template.idRegistro}"/>	
							</td>
							
							<td class="text_centro">
								<label style="font-size:11px;">${template.idTemplate}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].idTemplate" value="${template.idTemplate}" id="templateName${rowCounter.index}"/>
							</td>
							
							<td class="text_centro">
								<label style="font-size:11px;">${template.idLayout}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].idLayout" value="${template.idLayout}"/>
							</td>
							
							<td class="text_centro">
								<label style="font-size:11px;">${template.estatus}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].estatus" value="${template.estatus}"/>
							</td>
							
							<td class="text_centro">
								<label style="font-size:11px;">${template.usrAlta}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].usrAlta" value="${template.usrAlta}"/>
							</td>
							<td class="text_centro">
								<label style="font-size:11px;">${template.fechaCaptura}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].fechaCaptura" value="${template.fechaCaptura}"/>
							</td>
							<td class="text_centro">
								<label style="font-size:11px;">${template.fechaModifica}</label>
								<input type="hidden" name="listaTemplates[${rowCounter.index}].fechaModifica" value="${template.fechaModifica}"/>
							</td>							
						</tr>
					</c:forEach>			
				</tbody>
			</table>
		 </div>
		
			
			<%-- PAGINADOR --%>
			<c:if test="${not empty beanViewRespuesta.listaTemplates}">
			<div class="paginador">
				<c:if test="${beanViewRespuesta.paginador.paginaIni == beanViewRespuesta.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaIni != beanViewRespuesta.paginador.pagina}"><a href="javascript:;" onKeyPress="" onclick="nextPage('idForm','consultarTemplatesBuscar.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaAnt!='0' && beanViewRespuesta.paginador.paginaAnt!=null}"><a href="javascript:;" onKeyPress="" onclick="nextPage('idForm','consultarTemplatesBuscar.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaAnt=='0' || beanViewRespuesta.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanViewRespuesta.paginador.pagina} - ${beanViewRespuesta.paginador.paginaFin}</label>
				<c:if test="${beanViewRespuesta.paginador.paginaFin != beanViewRespuesta.paginador.pagina}"><a href="javascript:;" onKeyPress="" onclick="nextPage('idForm','consultarTemplatesBuscar.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaFin == beanViewRespuesta.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaFin != beanViewRespuesta.paginador.pagina}"><a href="javascript:;" onKeyPress="" onclick="nextPage('idForm','consultarTemplatesBuscar.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanViewRespuesta.paginador.paginaFin == beanViewRespuesta.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		   </c:if> 
		</div>
		
		<%-- Componente tabla estandar --%>
		<div class="frameTablaVariasColumnas"> 	
			
			<%-- PIE DE PAGINA --%>
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
					<caption></caption>
						<tr>
						 <c:choose>
							<c:when	test="${fn:containsIgnoreCase(searchString,\"AGREGAR\")}">
								<td width="279" class="izq"><a id="idAltaTemplate" href="javascript:;">${lalta}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${lalta}</a></td>
							</c:otherwise>
						</c:choose>
														
							<td width="6" class="odd">${espacioEnBlanco}</td>
							
							<c:choose>							
								<c:when test="${fn:containsIgnoreCase(searchString,\"ELIMINAR\") && beanViewRespuesta.totalRegistros > 0}">
									<td width="279" class="der"><a id="idEliminarTemplate" href="javascript:;">${leliminar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;">${leliminar}</a></td>
								</c:otherwise>
							</c:choose>
							
						</tr>
					<tr>
						<c:choose>
							<c:when	test="${fn:containsIgnoreCase(searchString,\"AGREGAR\") && beanViewRespuesta.totalRegistros > 0}">
								<td width="279" class="izq"><a id="idClonarTemplate" href="javascript:;">${lclonar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${lclonar}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class="odd"></td>
							<td width="279" class="izq_Des"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
</form>

<%-- CATCH Exception Messages. Proceso --%>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>