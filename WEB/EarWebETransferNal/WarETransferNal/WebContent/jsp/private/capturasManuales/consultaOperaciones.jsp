<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* consultaOperaciones.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     21/03/2017 12:00 VECTOR Creación de la pantalla
*
***********************************************************************************************
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
              
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="ConsultaOperaciones" />
</jsp:include>



<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.titulo" var="titulo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.exportar" var="exportar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.eliminar" var="eliminar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.regresar" var="regresar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.apartar" var="apartar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.text.desapartar" var="desapartar"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.usuarioCaptura" var="lblUsuarioCaptura"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.usuarioAutoriza" var="lblUsuarioAutoriza"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.estatusOperacion" var="lblEsatusOperacion"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.template" var="lblTemplate"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.label.fechaCaptura" var="lblFechaCaptura"/>

<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.seleccion" var="gridSeleccion"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.folio" var="gridFolio"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.importe" var="gridImporte"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.template" var="gridTemplate"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.usrCaptura" var="gridUsrCaptura"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.usrAutoriza" var="gridUsrAutoriza"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.estatus" var="gridEstatus"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.fechaCaptura" var="gridFechaCaptura"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.usrApartado" var="gridUsrApartado"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.refeTransfer" var="gridRefeTransfer"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.codErrorOpe" var="gridCodErrorOpe"/>
<spring:message code="moduloCapturasManuales.consultaOperaciones.grid.msgErrorOpe" var="gridMsgErrorOpe"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="general.buscar"                             	var="buscar"/>
<spring:message code="general.filtroBusqueda"                   	var="filtroBusqueda"/>
<spring:message code="general.regresar" var="regresar"/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/consultaOperaciones.js"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>
	
<form id="idForm" action="" method="post">
		
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanRes.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanRes.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanRes.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.paginaSig" id="paginaSig" value="${beanRes.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.paginaAnt" id="paginaAnt" value="${beanRes.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.accion" id="accion" value=""/>
			
	<input type="hidden" id="idUsuarioCaptura" name="idUsuarioCaptura" value="${usuarioCaptura}"/> 		
	<input type="hidden" id="idUsuarioAutoriza" name="idUsuarioAutoriza" value="${usuarioAutoriza}"/> 
	<input type="hidden" id="estatusOperacion" name="estatusOperacion" value="${estatusOperacion}"/> 
	<input type="hidden" id="idTemplate" name="idTemplate" value="${template}"/> 
 	<input type="hidden" id="idFechaCaptura" name="idFechaCaptura" value="${fechaCaptura}"/>
 	<input type="hidden" id="folio" name="folio" value=""/>
	
	<input type="hidden" name="countlistBeanConsOperaciones" value="${fn:length(beanRes.listBeanConsOperaciones)}"/>
	
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
			
			<table>
			<caption></caption>
				<tbody>
					<tr>
						<td class="text_izquierda">
						<label style="font-size: 11px;">${lblUsuarioCaptura}</label>
						</td>
						<td>
							<input type="text" class="Campos" id="usuarioCaptura" name="usuarioCaptura" value="${usuarioCaptura}" style="font-size: 14px;  text-align: center; width:150px;"/>
						</td>
						<td  class="text_izquierda">
							<label style="font-size: 11px;">${lblTemplate}</label>
						</td>
						<td>
						<input type="text" class="Campos" id="inputTemplate" name="template" value="${template}" style="font-size: 14px;  text-align: center; width:150px;"/>
						<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						<label style="font-size: 11px;">${lblUsuarioAutoriza}</label>
						</td>
						<td>
							<input type="text" class="Campos" id="usuarioAutoriza" name="usuarioAutoriza" value="${usuarioAutoriza}" style="font-size: 14px;  text-align: center; width:150px;"/>
						</td>
						<td  class="text_izquierda">
							<label style="font-size: 11px;">${lblFechaCaptura}</label>
						</td>
						<td>
						<input name="fechaCaptura" type="text" readonly="readonly" class="Campos" id="fechaCaptura" size="10" value="${fechaCaptura}" style="font-size: 14px;  text-align: center; width:150px;"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						<label style="font-size: 11px;">${lblEsatusOperacion}</label>
						</td>
						<td>
							<select name="servicio" class="Campos" id="inputEstatusOperacion" onChange="consultaOperacion.changeEstatusOperacion(this)" style="font-size: 14px;  text-align: center; width:154px;">
							<c:forEach var="item" items="${estatusOperacionItems}">
							<c:choose>
    						<c:when test="${item == beanRes.selectedEstatus}">
								<option value="${item}" selected="selected">
								${item}
								</option>
    						</c:when>    
    						<c:otherwise>
								<option value="${item}">
								${item}
								</option>
    						</c:otherwise>
    						</c:choose>
  							</c:forEach>
							</select> 
						</td>
						<td class="text_derecha"></td>
					</tr>
					<tr>
					<td class="text_izquierda">
					</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table >
			<caption></caption>
				<tr>
					<th  width="132" class="text_centro">${gridSeleccion}</th>
					<th  width="132" class="text_centro" scope="col">${gridFolio}</th>
					<th  width="132" class="text_centro" scope="col">${gridImporte}</th>
					<th  width="122" class="text_centro" scope="col">${gridTemplate}</th>
					<th  width="122" class="text_centro" scope="col">${gridUsrCaptura}</th>
					<th  width="122" class="text_centro" scope="col">${gridUsrAutoriza}</th>
					<th  width="122" class="text_centro" scope="col">${gridEstatus}</th>
					<th  width="122" class="text_centro" scope="col">${gridFechaCaptura}</th>
					<th  width="122" class="text_centro" scope="col">${gridUsrApartado}</th>
					<th  width="122" class="text_centro" scope="col">${gridRefeTransfer}</th>
					<th  width="122" class="text_centro" scope="col">${gridCodErrorOpe}</th>
					<th  width="122" class="text_centro" scope="col">${gridMsgErrorOpe}</th>
				</tr>
			
				<tr>
					<Td colspan="4" class="special"></Td>
				</tr>
				
				<tbody>
				<c:forEach var="beanConsOperaciones" items="${beanRes.listBeanConsOperaciones}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td width="250" class="text_centro"><input name="listBeanConsOperaciones[${rowCounter.index}].seleccionado" type="checkbox"/>
						<input type="hidden" name="listBeanConsOperaciones[${rowCounter.index}].folio" value="${beanConsOperaciones.folio}"/>
						<input type="hidden" name="listBeanConsOperaciones[${rowCounter.index}].cuentaOrdenante" value="${beanConsOperaciones.cuentaOrdenante}"/>
						<input type="hidden" name="listBeanConsOperaciones[${rowCounter.index}].cuentaReceptora" value="${beanConsOperaciones.cuentaReceptora}"/>
						<input type="hidden" name="listBeanConsOperaciones[${rowCounter.index}].monto" value="${beanConsOperaciones.monto}"/></td>
						<c:choose>
    						<c:when test="${beanConsOperaciones.apartado==true}">
    					<td class="text_centro"><a id="${beanConsOperaciones.folio}" href="javascript:;" onclick="consultaOperacion.despliegaDetalle('${beanConsOperaciones.folio}')" onKeyPress="">${beanConsOperaciones.folio}</a></td>	
    						</c:when>
    						<c:otherwise>
    						<td class="text_centro">${beanConsOperaciones.folio}</td>
    						</c:otherwise>
						</c:choose>
						<td class="text_centro">${beanConsOperaciones.importe}</td>
						<td class="text_centro">${beanConsOperaciones.template}</td>
						<td class="text_centro">${beanConsOperaciones.usrCaptura}</td>
						<td class="text_centro">${beanConsOperaciones.usrAutoriza}</td>
						<td class="text_centro">${beanConsOperaciones.estatus}</td>
						<td class="text_centro">${beanConsOperaciones.fechaCaptura}</td>
						<td class="text_centro">${beanConsOperaciones.usrApartado}</td>
						<td class="text_centro">${beanConsOperaciones.refTransfer}</td>
						<td class="text_centro">${beanConsOperaciones.codErrorOpe}</td>
						<td class="text_centro">${beanConsOperaciones.msgErrorOpe}</td>
					</tr>
				</c:forEach>
					
				</tbody>
				 
			</table>
		</div>
			
		<c:if test="${not empty beanRes.listBeanConsOperaciones}">
			<jsp:include page="../paginador.jsp" >	  					
  					<jsp:param name="paginaIni" value="${beanRes.paginador.paginaIni}" />
  					<jsp:param name="pagina" value="${beanRes.paginador.pagina}" />
  					<jsp:param name="paginaAnt" value="${beanRes.paginador.paginaAnt}" />
  					<jsp:param name="paginaFin" value="${beanRes.paginador.paginaFin}" />
  					<jsp:param name="servicio" value="consultaOperacionesBusqueda.do" />
				</jsp:include>
			</c:if>
			
	</div>

<%-- Componente tabla estandar --%>
<div class="frameTablaVariasColumnas"> 

<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
				<caption></caption>
					<tr>
					<c:choose>
    						<c:when test="${not empty beanRes.listBeanConsOperaciones}">
    							<td class="izq"><a id="idApartar" href="javascript:;">${apartar}</a></td>	
    						</c:when>    
    						<c:otherwise>
    							<td class="izq_Des"><a href="javascript:;">${apartar}</a></td>
    						</c:otherwise>
    				</c:choose>	
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
    						<c:when test="${not empty beanRes.listBeanConsOperaciones && beanRes.puedeDesapartar}">
    							<td class="der"><a id="idDesapartar" href="javascript:;">${desapartar}</a></td>	
    						</c:when>    
    						<c:otherwise>
    							<td class="der_Des"><a href="javascript:;">${desapartar}</a></td>
    						</c:otherwise>
    				</c:choose>	
					</tr>
					<tr>
						<c:choose>
    						<c:when test="${not empty beanRes.listBeanConsOperaciones && fn:containsIgnoreCase(searchString,'EXPORTAR')}">
    						<td  width="279" class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
    						</c:when>    
    						<c:otherwise>
    						<td  width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
    						</c:otherwise>
						</c:choose>		
						<td  width="6" class="odd">${espacioEnBlanco}</td>
						<c:choose>
    						<c:when test="${not empty beanRes.listBeanConsOperaciones && fn:containsIgnoreCase(searchString,'EXPORTAR')}">
    						<td  width="279" class="der"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
    						</c:when>    
    						<c:otherwise>
    						<td  width="279" class="der_Des"><a href="javascript:;">${exportarTodo}</a></td>
    						</c:otherwise>
						</c:choose>
					</tr>
					<tr>	
						<c:choose>
    						<c:when test="${not empty beanRes.listBeanConsOperaciones && fn:containsIgnoreCase(searchString,'ELIMINAR') && beanRes.puedeEliminar}">
    						<td class="izq"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
    						</c:when>    
    						<c:otherwise>
    						<td class="izq_Des"><a href="javascript:;">${eliminar}</a></td>
    						</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>		
						<td class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
					</tr>
				</table>				
			</div>
		</div>	
	</div>

	
</form>
              
<jsp:include page="../myFooter.jsp" flush="true"/>
	
	    <script type = "text/javascript" defer="defer">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
    </script>
    


<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>