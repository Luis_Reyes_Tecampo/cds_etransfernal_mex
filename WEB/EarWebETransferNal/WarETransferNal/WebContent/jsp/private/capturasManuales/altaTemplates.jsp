<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- 
/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * altaTemplates.jsp
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
 --%>

<%-- Librerias tags --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<%-- Include de Menu --%>
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="mantenimientoTemplates" />
</jsp:include>

<%-- Mensajes: Etiquetas i18 --%>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.template" var="ltemplate"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.editTemplate.label.template" var="letemplate"/>


<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.layout" var="llayout"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.optionLayout" var="optionLayout"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.estatus" var="lestatus"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.finalEst" var="finalEst"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.usuariosAt" var="lautorizan"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.may" var="smay"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.label.men" var="smen"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.table.text.seleccion" var="tseleccion"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.table.text.ValDefecto" var="tdefecto"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.table.text.Campo" var="tcampo"/>

<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.link.limpiar" var="llimpiar"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.boton.guardar" var="bbuscar"/>
<spring:message code="moduloCapturasManuales.mantenimientoTemplates.altaTemplate.boton.guardar" var="bguardar"/>

<spring:message code="general.filtroBusqueda" var="filtroBusqueda"/>
<spring:message code="general.regresar" var="regresar"/>

<%-- Control de excepciones o errores --%>
<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.CMMT002V" var="CMMT002V"/> <%-- campos oblig. --%>

<%-- Declaracion de mensajes relacionados al JS --%> 
<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"CMMT002V":'${CMMT002V}'};
</script>


<%-- JS Manipulacion manual de formularios --%>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/admonTemplates.js"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloFuncionalidad}</span>
</div>

<%-- FORMULARIO --%>
<form id="idForm" action="" method="post">
	<%-- Campos adicionales para enviarse con el formulario POST --%>
	<input type="hidden" id="selectedLayout" name="selectedLayout" value="${beanViewRespuesta.selectedLayout}"/>	
	<input type="hidden" id="idTemplate" name="template.idTemplate" value="${beanViewRespuesta.template.idTemplate}"/>
	<input type="hidden" id="estatus" name="template.estatus" value="${beanViewRespuesta.template.estatus}"/>
	
	<input type="hidden" name="paginador.accion" id="accion" value="ACT"/>
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanViewRespuesta.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanViewRespuesta.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanViewRespuesta.paginador.paginaFin}"/>	
	
	<%-- Filtro, busquedaActiva --%>
	<input type="hidden" name="filtroEstado" id="filtroEstado"  value="${beanViewRespuesta.filtroEstado}"/>	
	<input type="hidden" name="forward" id="forward"  value="${beanViewRespuesta.forward}"/>
	<input type="hidden" id="tipoSolicitud" name="tipoSolicitud" value="-1"/>
	
	<%-- idTemplate a editar, modoEdicion --%>	
	<input type="hidden" name="modoEdicion" id="modoEdicion"  value="${beanViewRespuesta.modoEdicion}"/>
	<input type="hidden" name="template.operacionesAsociadas" id="operaciones"  value="${beanViewRespuesta.template.operacionesAsociadas}"/>
	
	<input type="hidden" name="countlistaCamposMaster" value="${fn:length(beanViewRespuesta.listaCamposMaster)}"/>
	<input type="hidden" id = "countlistaUsuariosAutorizan" name="countlistaUsuariosAutorizan" value="${fn:length(beanViewRespuesta.template.usuariosAutorizan)}"/>
	<input type="hidden" id = "countlistaUsuarios" name="countlistaUsuarios" value="${fn:length(beanViewRespuesta.listaUsuarios)}"/>
	<input type="hidden" name="countlistaLayouts" value="${fn:length(beanViewRespuesta.listaLayouts)}"/>	
		
	<%-- FRAME DEL BUSCADOR --%>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
		<table>
		<caption></caption>
			<tbody>
			<tr>
				<td class="text_izquierda">
					${beanViewRespuesta.modoEdicion? letemplate:ltemplate}		
				</td>
				
				<td>
					<input type="text" class="Campos" id="inputTemplate" name="inputTemplate"  size="14" maxlength="14" 
						value="${beanViewRespuesta.template.idTemplate}"
						 ${beanViewRespuesta.modoEdicion? 'disabled':''}/>
				</td>
				
				<td class="text_izquierda">${llayout}</td>
				<td>
					<select id="selectLayout" name="selectLayout" class="Campos" ${beanViewRespuesta.modoEdicion? 'disabled':''}>
						<option value="">${optionLayout}</option>
						<c:forEach items ="${beanViewRespuesta.listaLayouts}" var="layout" varStatus="rowCounter">
						  <c:choose>
							<c:when test="${layout == beanViewRespuesta.selectedLayout}">
								<option value="${layout}" selected="selected">${layout}</option>
							</c:when>
							<c:otherwise>
								<option value="${layout}">${layout}</option>
							</c:otherwise>
						 </c:choose>					
						</c:forEach>							 
					</select>					
				</td>
				
				<%-- Nueva funcionalidad de estatus --%>
				<c:if test="${beanViewRespuesta.modoEdicion}">
					<td class="text_izquierda">${lestatus}</td>
					<td>
					 <c:choose>
					 	<c:when test="${finalEst == beanViewRespuesta.template.estatus}">
					 		<select id="selectEstado" name="selectEstado" class="Campos" disabled>
					 		  <c:forEach items ="${beanViewRespuesta.estados}" var="opt" varStatus="rowCounter">
							   <c:choose>
								  <c:when test="${opt == beanViewRespuesta.template.estatus}">
									  <option value="${opt}" selected="selected">${opt}</option>
								  </c:when>
								  <c:otherwise>
									  <option value="${opt}" >${opt}</option>
								  </c:otherwise>
							    </c:choose>
							   </c:forEach>
							 </select>
					 	</c:when>
					 	<c:otherwise>
					 		<select id="selectEstado" name="selectEstado" class="Campos" >
					 		  <c:forEach items ="${beanViewRespuesta.estados}" var="opt" varStatus="rowCounter">
							   <c:choose>
								  <c:when test="${opt == beanViewRespuesta.template.estatus}">
									  <option value="${opt}" selected="selected">${opt}</option>
								  </c:when>
								  <c:otherwise>
									  <option value="${opt}" >${opt}</option>
								  </c:otherwise>
							    </c:choose>
							   </c:forEach>
							 </select>
					 	</c:otherwise>
					 </c:choose>
					</td>
				</c:if>
				<%-- Fin de nueva funcionalidad --%>
				
				<c:choose>
					<c:when test="${beanViewRespuesta.modoEdicion}"><%-- Modifica --%>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(  searchString,'MODIFICAR')}">
								<td><span><a id="idActualizarTemplate" href="javascript:;">${bguardar}</a></span></td>
							</c:when>
							<c:otherwise>
								<td><span class="btn_Des"><a href="javascript:;">${bguardar}</a></span></td>
							</c:otherwise>
						</c:choose>							
					</c:when>
					
					<c:when test="${!beanViewRespuesta.modoEdicion}"><%-- Agregar --%>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(  searchString,'AGREGAR')}">
								<td><span><a id="idGuardarTemplate" href="javascript:;">${bguardar}</a></span></td>
							</c:when>
							<c:otherwise>
								<td><span class="btn_Des"><a href="javascript:;">${bguardar}</a></span></td>
							</c:otherwise>
						</c:choose>							
					</c:when>					
				</c:choose>				
			</tr>
			
			
			<tr > 
				<td class="text_izquierda" rowspan="4">${lautorizan}</td>				
				<td rowspan="4">
				 <select id="listaUsuariosSelect" name="listaUsuariosSelect" class="Campos" multiple size="5" style="width: 200px;">						
						<c:forEach items ="${beanViewRespuesta.listaUsuarios}" var="usuario" varStatus="row">							
							<option value="${usuario}" > ${usuario}</option>
						</c:forEach>							 
				 </select>		
				</td>
				
				<td></td>
				
				<td rowspan="4">
					<select id="listaAutorizan" name="listaAutorizan" class="Campos" multiple size="5" style="width: 200px;">						
						<c:forEach items ="${beanViewRespuesta.template.usuariosAutorizan}" var="usuario" varStatus="rowCounter">
							<option value="${usuario.usrAutoriza}" > ${usuario.usrAutoriza}</option>
						</c:forEach>							 
				 	</select>
				</td>
			</tr>
			
			<tr> 
				<td><span><a id="idAddToRight" href="javascript:;" style="display: inline-block; width: 20px; height: 20px;">${smay}</a></span></td>
			</tr>
			
			<tr> 
				<td><span><a id="idAddToLeft" href="javascript:;" style="display: inline-block; width: 20px; height: 20px;">${smen}</a></span></td>
			</tr>
			
			<tr> 
				<td></td>
			</tr>			
			
			</tbody>
		</table>
		</div>
		
		<%-- TABLA DE CAMPOS --%>		
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
			<caption></caption>
				<tr>
					<th style="font-size:11px; width:5%;">${tseleccion}</th>
					<th style="font-size:11px; width:35%;">${tdefecto}</th>
					<th style="font-size:11px; width:65%;">${tcampo}</th>
				</tr>
		
				<tr>
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
					<c:forEach var="campo" items="${beanViewRespuesta.listaCamposMaster}" varStatus="rowCounter">
						<tr class="${rowCounter.count % 2 eq 0 ? 'odd3' : 'odd'}">
							<%-- Obligatorio --%>
							<td class="text_centro">
								<c:choose>
									<c:when test="${campo.obligatorio == '1' && beanViewRespuesta.template.operacionesAsociadas}">
										<input name="listaCamposMaster[${rowCounter.index}].obligatorio" type="checkbox" id="chkSel${campo.idCampo}"
										 checked disabled/>	
									</c:when>
									
									<c:when test="${campo.obligatorio == '1' && !beanViewRespuesta.template.operacionesAsociadas}">
										<input name="listaCamposMaster[${rowCounter.index}].obligatorio" type="checkbox" id="chkSel${campo.idCampo}"
										 checked/>	
									</c:when>
									
									<c:when test="${campo.obligatorio != '1' && beanViewRespuesta.template.operacionesAsociadas}">
										<input name="listaCamposMaster[${rowCounter.index}].obligatorio" type="checkbox" id="chkSel${campo.idCampo}"
										 disabled/>	
									</c:when>
									
									<c:when test="${campo.obligatorio != '1' && !beanViewRespuesta.template.operacionesAsociadas}">
										<input name="listaCamposMaster[${rowCounter.index}].obligatorio" type="checkbox" id="chkSel${campo.idCampo}"
										 />	
									</c:when>
								</c:choose>								
							</td>
							
							<td class="text_centro">
								<%-- Constante --%>
								<c:if test="${fn:toUpperCase(campo.tipo) != 'COMBO' || beanViewRespuesta.template.operacionesAsociadas == true}">
									<input name="listaCamposMaster[${rowCounter.index}].constante"
									   type="text" id="cons${campo.idCampo}" value="${campo.constante}" size="20" 
									   maxlength="${campo.longitud}"
									   ${beanViewRespuesta.template.operacionesAsociadas? 'disabled':''}/>
								</c:if>
								<%-- Combo --%>
								<c:if test="${fn:toUpperCase(campo.tipo) == 'COMBO' && beanViewRespuesta.template.operacionesAsociadas == false}">
									<input name="listaCamposMaster[${rowCounter.index}].constante"
									   type="hidden" id="cons${campo.idCampo}" value="${campo.constante}"/>
									<select name="select" class="Campos" style="width: 145px;" id="combocons${campo.idCampo}" onchange="comboChanged('cons${campo.idCampo}');">
										<c:forEach var="valorCombo" items="${campo.comboValores}">
											<c:if test="${campo.constante == valorCombo.id}">
												<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
											</c:if>
											<c:if test="${campo.constante != valorCombo.valor}">
												<option value="${valorCombo.id}">${valorCombo.valor}</option>
											</c:if>
										</c:forEach>
									</select>
								</c:if>
							</td>
							<td class="text_izq">
								<label style="font-size:11px;">${campo.campo}-${campo.descripcion}</label>
								<%-- Anexar campos disabled --%>
								<c:if test="${beanViewRespuesta.template.operacionesAsociadas}">
									<input type="hidden" name="listaCamposMaster[${rowCounter.index}].obligatorio" value="${campo.obligatorio}"/>
									<input type="hidden" name="listaCamposMaster[${rowCounter.index}].constante" value="${campo.constante}"/>
								</c:if>
								
								<input type="hidden" name="listaCamposMaster[${rowCounter.index}].idCampo" value="${campo.idCampo}"/>
								<input type="hidden" name="listaCamposMaster[${rowCounter.index}].campo" value="${campo.campo}"/>
								<input type="hidden" name="listaCamposMaster[${rowCounter.index}].descripcion" value="${campo.descripcion}"/>
								<input type="hidden" name="listaCamposMaster[${rowCounter.index}].tipo" value="${campo.tipo}"/>	
								<input type="hidden" name="listaCamposMaster[${rowCounter.index}].longitud" value="${campo.longitud}"/>															
							</td>							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		 </div>
	</div>
	
	
		<%-- Componente tabla estandar --%>
		<div class="frameTablaVariasColumnas"> 	
			
			<%-- PIE DE PAGINA --%>
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
					<caption></caption>
						<tr>
							<c:choose>
								<c:when test="${beanViewRespuesta.modoEdicion}">
									<td width="279" class="izq"><a id="idLimpiarEditTemplate" href="javascript:;">${llimpiar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq"><a id="idLimpiarTemplate" href="javascript:;">${llimpiar}</a></td>
								</c:otherwise>
							</c:choose>
							<td width="6" class="odd">${espacioEnBlanco}</td>
							<td width="279" class="der"><a id="idRegresarTemplate" href="javascript:;">${regresar}</a></td>
						</tr>
					<tr>
							<td width="279" class="izq_Des"></td>
							<td width="6" class="odd"></td>
							<td width="279" class="izq_Des"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
</form>

<%-- CATCH Exception Messages. Proceso --%>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>