<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="mx.isban.agave.configuracion.Configuracion"%>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"           type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css"/>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css"/>

<spring:message code="moduloCDA.myMenu.text.moduloCDA" var="moduloCDA"/>
<spring:message code="moduloCDA.myMenu.text.monitorCDA" var="monitorCDA"/>
<spring:message code="moduloCDA.myMenu.text.monitorCDAHist" var="monitorCDAHist"/>
<spring:message code="moduloCDA.myMenu.text.consultaMovCDA" var="consultaMovCDA"/>
<spring:message code="moduloCDA.myMenu.text.consultaMovHistCDA" var="consultaMovHistCDA"/>
<spring:message code="moduloCDA.myMenu.text.contingenciaCDAMismoDia" var="contingenciaCDAMismoDia"/>
<spring:message code="moduloCDA.myMenu.text.generarArchivoCDAHist" var="generarArchivoCDAHist"/>
<spring:message code="moduloCDA.myMenu.text.monitorCargaArchHist" var="monitorCargaArchHist"/>
<spring:message code="moduloCDA.myMenu.text.bitacoraAdmon" var="bitacoraAdmon"/>
<spring:message code="moduloCDA.myMenu.text.monitorArchExp" var="monitorArchExp"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI" />

<spring:message code="moduloCDA.myMenu.text.generacionArchivosXFecha" var="generacionArchivosXFecha"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloCatalogos" var="moduloCatalogos"/>
<spring:message code="moduloAdmonSaldo.myMenu.text.admonSaldo" var="adminSaldos"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID" var="txtModuloSPID"/>
<spring:message code="pagoInteres.myMenu.text.moduloPagoInteres" var="txtModuloPagoInteres"/>
<spring:message code="moduloCDA.myMenu.text.moduloCDASPID" var="moduloCDASPID"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloSPEI" var="moduloSPEI"/>
<spring:message code="SPEICero.formulario.title" var="moduloSPEICero"/>
<spring:message code="catalogos.menuCatalogos.txt.txtPrincipal" var="principal"/>
<spring:message code="catalogos.menuCatalogos.txt.txtCapturas" var="capturasManuales"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="moduloContingencia"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
	
<c:set var="searchString" value="${seguServicios}" />
	
<body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
					<li id="principal"   	  class="startMenuGroup">              <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">             	 <span>${principal}</span></a></li>
					<li id="admonSaldo"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/admonSaldo/admonSaldoInit.do">             <span>${adminSaldos}</span></a></li>					
					<li id="capturasManuales" class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/capturasManuales/capturasManualesInit.do"> <span>${capturasManuales}</span></a></li>
					<li id="catalogos"        class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/catalogos/catalogosInit.do">             	 <span>${moduloCatalogos}</span></a></li>
					<li id="moduloCDA"   	  class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">             	 <span>${moduloCDA}</span></a>
						<ul>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'consInfMonCDA.do')}">
									<li id="consInfMonCDA">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/consInfMonCDA.do">&gt;
											<span class="subMenuText">${monitorCDA}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="consInfMonCDA">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitorCDA}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraMonitorCDAHist.do')}">
									<li id="muestraMonitorCDAHist">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraMonitorCDAHist.do">&gt;
											<span class="subMenuText">${monitorCDAHist}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraMonitorCDAHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitorCDAHist}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraConsMovCDA.do')}">
									<li id="muestraConsMovCDA">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraConsMovCDA.do?esSPID=false">&gt;
											<span class="subMenuText">${consultaMovCDA}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="consInfMonCDA">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${consultaMovCDA}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraConsMovHistCDA.do')}">
									<li id="muestraConsMovHistCDA">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraConsMovHistCDA.do">&gt;
											<span class="subMenuText">${consultaMovHistCDA}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="consInfMonCDA">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${consultaMovHistCDA}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraContingCDAMismoDia.do')}">
									<li id="muestraContingCDAMismoDia">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraContingCDAMismoDia.do">&gt;
											<span class="subMenuText">${contingenciaCDAMismoDia}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraContingCDAMismoDia">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${contingenciaCDAMismoDia}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraGenerarArchCDAHist.do')}">
									<li id="muestraGenerarArchCDAHist">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraGenerarArchCDAHist.do">&gt;
											<span class="subMenuText">${generarArchivoCDAHist}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraGenerarArchCDAHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${generarArchivoCDAHist}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraGenerarArchCDAxFchHist')}">
									<li id="muestraGenerarArchCDAxFchHist">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraGenerarArchCDAxFchHist.do">&gt;
											<span class="subMenuText">${generacionArchivosXFecha}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraGenerarArchCDAxFchHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${generacionArchivosXFecha}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraMonitorCargaArchHist.do')}">
									<li id="muestraMonitorCargaArchHist">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraMonitorCargaArchHist.do">&gt;
											<span class="subMenuText">${monitorCargaArchHist}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraMonitorCargaArchHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitorCargaArchHist}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraBitacoraAdmon.do')}">
									<li id="muestraBitacoraAdmon">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraBitacoraAdmon.do?esSPID=false">&gt;
											<span class="subMenuText">${bitacoraAdmon}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraBitacoraAdmon">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${bitacoraAdmon}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraMonitorArchExp.do')}">
									<li id="muestraMonitorArchExp">
										<a href="${pageContext.servletContext.contextPath}/moduloCDA/muestraMonitorArchExp.do">&gt;
											<span class="subMenuText">${monitorArchExp}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraMonitorArchExp">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitorArchExp}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</li>
					<li id="moduloCDASPID"    class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloCDASPID/moduloCDASPIDInit.do">       <span>${moduloCDASPID}</span></a>
					<li id="moduloPOACOA"     class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOAInit.do">         <span>${moduloPOACOASPEI}</span></a></li>
					<li id="moduloPOACOASPID"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOASPIDInit.do">         <span>${moduloPOACOASPID}</span></a></li>
					<li id="moduloSPID"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPID/moduloSPIDInit.do">             <span>${txtModuloSPID}</span></a></li>
					<li id="moduloSPEI"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEI/moduloSPEIInit.do">             <span>${moduloSPEI}</span></a></li>
					<li id="moduloSPEICero"   class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEICero/moduloSPEICero.do">         <span>${moduloSPEICero}</span></a></li>
					<li id="pagoInteres"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/pagoInteres/moduloPagoInteresInit.do">     <span>${txtModuloPagoInteres}</span></a></li>
					<li id="contingencia"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/contingencia/contingenciaInit.do">         <span>${moduloContingencia}</span></a></li>
				</ul>
				<div id="menuFooter">
					<div></div>
				</div>
			</div>
		</c:if>
	</div>
<div id="content_container">
