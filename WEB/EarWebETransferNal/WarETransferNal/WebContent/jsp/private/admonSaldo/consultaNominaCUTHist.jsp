<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* reportePagoSPEI.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     13/09/2011 12:00 Creación de la pantalla
*
***********************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuAdmonSaldo.jsp" flush="true">
	<jsp:param name="menuItem" value="admonSaldo" />
	<jsp:param name="menuSubitem" value="muestraConsultaNominaCutHist" />
</jsp:include>

<spring:message code="moduloCDA.myMenu.text.adminSaldos" var="tituloModulo"/>
<spring:message code="moduloCDA.administraSaldos.text.titulo" var="titulo"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="general.buscar"                             	var="buscar"/>
<spring:message code="general.filtroBusqueda"                   	var="filtroBusqueda" text=""/>
<spring:message code="general.regresar" var="regresar"/>

<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.label.subtitulo" var="labelSubtitulo"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.label.fechaOperacion" var="txtFechaOperacion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.columna.fechaOperacion" var="columFechaOperacion"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.columna.cantidadRecepCUT" var="columCantidadRecepCUT"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.columna.montoRecepCUT" var="columMontoRecepCUT"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.columna.cantidadPreCUT" var="columCantidadPreCUT"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.columna.montoPreCUT" var="columMontoPreCUT"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutHist.boton.consultar" var="btnConsultar"/>


<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/admonSaldo/consultaNominaCUTHist.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>
<form id="idForm" action="" method="post">
<input type="hidden" id="idFechaOperacion" name="fechaOperacion" value="${beanReq.fechaOperacion}"/> 		
		

	<div class="pageTitleContainer">
		<span class="pageTitle">${titulo}</span>  - ${labelSubtitulo}
		<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.fechaOperacion}</span>
	</div>


	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
			${instruccionBuscador}
			<table>
				<tbody>
					<tr>
						<td class="text_izquierda">${txtFechaOperacion}</td>
						<!-- <td class="text_derecha"><input type="text" id="idFchOperacion" name="fchOperacion" value="${beanReq.fechaOperacion}"/></td> -->
						<td class="text_izquierda"><input name="fchOperacion" type="text" class="Campos" id="fchOperacion" size="15" readonly="readonly" value="${beanReq.fechaOperacion}"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/></td>
						<td class="text_izquierda"><c:choose>
								<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
	                        		<span><a id="idBuscar" href="javascript:;">${btnConsultar}</a></span>
	                        	</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${btnConsultar}</a></span>
							</c:otherwise>
						</c:choose></td>
						
						<td class="text_derecha">
							
						</td>
					</tr>
					
					<tr>
					<td class="text_izquierda">
					</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<!-- Componente tabla estandar -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infoEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<thead>
					<tr>
						<th class="text_centro">${columFechaOperacion}</th>
						<th class="text_centro">${columCantidadRecepCUT}</th>
						<th class="text_centro">${columMontoRecepCUT}</th>
						<th class="text_centro">${columCantidadPreCUT}</th>
						<th class="text_centro">${columMontoPreCUT}</th>
					</tr>
				</thead>
				<tr>
					<Td colspan="8" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="bean" items="${beanRes.listBeanConsNomCUT}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
	                        <td class="text_izquierda">${bean.fechaOperacion}</td>
	                        <td class="text_izquierda">${bean.cantidadPagos}</td>
	                        <td class="text_izquierda">${bean.monto}</td>
	                        <td class="text_izquierda">${bean.cantidadPagosPre}</td>
	                        <td class="text_izquierda">${bean.montoPre}</td>
						</tr>
			    	</c:forEach>
				</tbody>
			</table>
		</div>


        <!-- Componente Paginador -->
        <c:if test="${not empty resBean.reportePagoList}">
					<jsp:include page="../paginador.jsp" >	  					
	  					<jsp:param name="paginaIni" value="${resBean.paginador.paginaIni}" />
	  					<jsp:param name="pagina" value="${resBean.paginador.pagina}" />
	  					<jsp:param name="paginaAnt" value="${resBean.paginador.paginaAnt}" />
	  					<jsp:param name="paginaFin" value="${resBean.paginador.paginaFin}" />
	  					<jsp:param name="servicio" value="consultaReportePagoSPEI.do" />
					</jsp:include>
				</c:if>   

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<td width="279" class="cero"></td>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
					</tr>
				</table>
			</div>
		</div>
			
	</div>

	
</form>	

<jsp:include page="../myFooter.jsp" flush="true"/>
	
	    <script type = "text/javascript" defer="defer">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
    </script>
    


<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>