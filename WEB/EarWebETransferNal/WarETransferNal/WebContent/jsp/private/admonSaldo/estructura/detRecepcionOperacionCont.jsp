<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"  %>

<spring:message code="moduloCDA.detRecepcionOperacion.text.topologia" var="topologia"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.prioridad" var="prioridad"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.tipoOper" var="tipoOper"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.bancoOrd" var="bancoOrd"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.folioPaquete" var="folioPaquete"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.folio" var="folio"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cuentaOrd" var="cuentaOrd"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.rastreo" var="rastreo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.bancoRec" var="bancoRec"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cuentaRec1" var="cuentaRec1"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cuentaRec2" var="cuentaRec2"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cuentaModif" var="cuentaModif"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cvePago" var="cvePago"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.concepto" var="concepto"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.estatusBanxico" var="estatusBanxico"/>
<spring:message code="moduloSPEI.recepOperacion.text.transfer" var="transfer"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.importe" var="importe"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.tipoPago" var="tipo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.tipoPago2" var="tipoPago2"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.rfc" var="rfc"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.rfc2" var="rfc2"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.nombre" var="nombre"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.nombre2" var="nombre2"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.refeCobranza" var="refeCobranza"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.envioDevolucion" var="envioDevolucion"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.motivo" var="motivo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.folioPaquete" var="folioPaquete"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.folioPago" var="folioPago"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.referencia" var="referencia"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.iva" var="iva"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.refeNumerica" var="refeNumerica"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.ctaReceptora" var="ctaReceptora"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.datosGrales" var="dtosGrales"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.guardar" var="guardar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.regresar" var="regresar"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.EMO0002V" var="EMO0002V"/>
<%-- Mensaje de confirmacion para guardar un cambio --%>
<spring:message code="codError.CD00043V" var="CD00043V"/>
<spring:message code="codError.ED00136V" var="ED00136V"/>
<%--Subtitulos --%>
<spring:message code="moduloCDA.detRecepcionOperacion.text.datosGrales" var="datosGrales"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.envioDev" var="envioDev"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.bancoOrde" var="bancoOrde"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.bancoRece" var="bancoRece"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.cambios" var="cambios"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.otros" var="otros"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.detalleCta" var="detalleCta"/>
<%-- Campos Adicionales --%>
<spring:message code="moduloCDA.detRecepcionOperacion.text.bucCliente" var="bucCliente"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.ctaRecOrig" var="ctaRecOrig"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.ctaRecInterna" var="ctaRecInterna"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.aplicativo" var="aplicativo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.regresar" var="regresar"/>

<spring:message code="moduloCDA.detRecepcionOperacion.text.switf" var="switf"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.switfUno" var="switfUno"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.switfDos" var="switfDos"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.refeAdicional" var="refeAdicional"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.beneRecu" var="beneRecu"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.montoPAgo" var="montoPAgo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.FchOperOrig" var="FchOperOrig"/>

<spring:message code="moduloCDA.recepcionOperacion.text.estatusOpe"      var="estatusOpe"/>
<spring:message code="moduloCDA.recepcionOperacion.text.cveTransfer"      var="cveTransfer"/>
<spring:message code="moduloCDA.recepcionOperacion.text.desCveTransf"      var="desCveTransf"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtRefTrans"  var="txtRefTrans"/>
<spring:message code="moduloSPEI.recepOperacion.text.msjUpdate"  var="msjUpdate"/>

<spring:message code="moduloCDA.detRecepcionOperacion.text.cveOperacion" var="cveOperacion"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.importeCargo" var="importeCargo"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.importeAbono" var="importeAbono"/>
<spring:message code="moduloCDA.detRecepcionOperacion.text.pagoCompen" var="pagoCompen"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fechaLiquidacion" var="txtFechaLiq"/>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/detallePago.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/SPEI/detRecepcionOperacion.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',
			"OK00000V":'${OK00000V}',"OK00013V":'${OK00013V}',"EMO0002V":'${EMO0002V}',"ED00136V":'${ED00136V}'};
   	var parametro = "${beanDatos.parametro}";
   	var codErrorUpdate = "${codError}";
</script>




<form name="idForm" id="idForm" method="post">
	<input type="hidden" id="fechaOpeLink" name="fechaOpeLink" value=""/>
	<input type="hidden" id="cuentaReceptora" name="cuentaReceptora" value=""/>
	<input type="hidden" id="fchOperacion" name="beanDetRecepOperacion.detalleGral.fchOperacion" value=""/>
	<input type="hidden" id="cveMiInstitucion" name="beanDetRecepOperacion.detalleGral.cveMiInstitucion" value=""/>
	<input type="hidden" id="mensajeUpdate" name="mensajeUpdate" value="${msjUpdate}">
	<input type="hidden" id="msjConfUpdate" name="msjConfUpdate" value="${CD00043V}">
	<input type="hidden" id="nombrePantalla" name="beanDetRecepOperacion.nombrePantalla" value="${beanDatos.beanDetRecepOperacion.nombrePantalla}"/>
	<input type="hidden" id="sessionUsu" name="sessionUsu" value="${sessionBean.usuario}"/>
	<input type="hidden" id="sessionPerf" name="sessionPerf" value="${sessionBean.perfil}"/>
	<input type="hidden" id="usuarioAparta" name="usuarioAparta" value="${usuario}"/>
	<input type="hidden" id="numCuentaTran" name="numCuentaTran" value="${beanTranSpeiRecOper.numCuentaTran}"/>
	<input type="hidden" id="usuario" name="usuario" value="${beanTranSpeiRecOper.usuario}"/>
	<input type="hidden" id="beanDetalle.detEstatus.refeTransfer" name="beanDetalle.detEstatus.refeTransfer" value="${beanTranSpeiRecOper.beanDetalle.detEstatus.refeTransfer}"/>
	<input type="hidden" id="beanDetalle.detEstatus.estatusTransfer" name="beanDetalle.detEstatus.estatusTransfer" value="${beanTranSpeiRecOper.beanDetalle.detEstatus.estatusTransfer}"/>
	<input type="hidden" id="beanDetalle.detalleGral.cveMiInstitucion" name="beanDetalle.detalleGral.cveMiInstitucion" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveMiInstitucion}"/>
	<input type="hidden" id="beanDetalle.receptor.numCuentaRec" name="beanDetalle.receptor.numCuentaRec" value="${beanTranSpeiRecOper.beanDetalle.receptor.numCuentaRec}"/>
	<input type="hidden" id="beanDetalle.beanCambios.motivoDevol" name="beanDetalle.beanCambios.motivoDevol" value="${beanTranSpeiRecOper.beanDetalle.beanCambios.motivoDevol}"/>
	<input type="hidden" id="beanDetalle.codigoError" name="beanDetalle.codigoError" value="${beanTranSpeiRecOper.beanDetalle.codigoError}"/>
	<input type="hidden" id="beanDetalle.detalleGral.cveRastreo" name="beanDetalle.detalleGral.cveRastreo" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveRastreo}"/>
	<input type="hidden" id="beanDetalle.detalleGral.folioPaquete" name="beanDetalle.detalleGral.folioPaquete" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.folioPaquete}"/>
	<input type="hidden" id="beanDetalle.detalleGral.folioPago" name="beanDetalle.detalleGral.folioPago" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.folioPago}"/>
	<input type="hidden" id="beanDetalle.detalleTopo.tipoPago" name="beanDetalle.detalleTopo.tipoPago" value="${beanTranSpeiRecOper.beanDetalle.detalleTopo.tipoPago}"/>
	<input type="hidden" id="beanDetalle.beanCambios.otros.descTipoPgo" name="beanDetalle.beanCambios.otros.descTipoPgo" value="${beanTranSpeiRecOper.beanDetalle.beanCambios.otros.descTipoPgo}"/>
	<input type="hidden" id="beanDetalle.detEstatus.estatusBanxico" name="beanDetalle.detEstatus.estatusBanxico" value="${beanTranSpeiRecOper.beanDetalle.detEstatus.estatusBanxico}"/>
	<input type="hidden" id="tareaMod" name="tareaMod" value="${fn:containsIgnoreCase(searchString,'MODIFICAR')}" >
	<input type="hidden" id="beanComun.fechaOperacion" name="beanComun.fechaOperacion" value="${fechaOperacion}"/>
	<input type="hidden" id="beanDetalle.detalleGral.estatusOpe" name="beanDetalle.detalleGral.estatusOpe" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.estatusOpe}"/>
	<input type="hidden" id="beanDetalle.detalleGral.cveTransfer" name="beanDetalle.detalleGral.cveTransfer" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveTransfer}"/>
	<input type="hidden" id="beanDetalle.detalleGral.desCveTransf" name="beanDetalle.detalleGral.desCveTransf" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.desCveTransf}"/>
	<input type="hidden" id="beanDetalle.detalleGral.fchOperacion" name="beanDetalle.detalleGral.fchOperacion" value="${beanTranSpeiRecOper.beanDetalle.detalleGral.fchOperacion}"/>
	<input type="hidden" id="paginador.accion" name="paginador.accion" value="${beanReqTranSpeiRec.paginador.accion}"/>
	<input type="hidden" id="paginador.pagina" name="paginador.pagina" value="${beanReqTranSpeiRec.paginador.pagina}"/>
	<input type="hidden" id="paginador.paginaAnt" name="paginador.paginaAnt" value="${beanReqTranSpeiRec.paginador.paginaAnt}"/>
	<input type="hidden" id="paginador.paginaFin" name="paginador.paginaFin" value="${beanReqTranSpeiRec.paginador.paginaFin}"/>
	<input type="hidden" id="paginador.paginaIni" name="paginador.paginaIni" value="${beanReqTranSpeiRec.paginador.paginaIni}"/>
	<input type="hidden" id="paginador.paginaSig" name="paginador.paginaSig" value="${beanReqTranSpeiRec.paginador.paginaSig}"/>
	<input type="hidden" id="paginador.regFin" name="paginador.regFin" value="${beanReqTranSpeiRec.paginador.regFin}"/>
	<input type="hidden" id="paginador.regIni" name="paginador.regIni" value="${e:forHtmlAttribute(beanReqTranSpeiRec.paginador.regIni)}"/>
	<input type="hidden" id="parametro" name="parametro" value="${beanDatos.parametro}"/>
	<input type="hidden" id="beanDetalle.detalleGral.cveInstOrd" name="beanDetalle.detalleGral.cveInstOrd" value="${e:forHtmlAttribute(beanTranSpeiRecOper.beanDetalle.detalleGral.cveInstOrd)}"/>
	<input type="hidden" id="beanDetalle.ordenante.monto" name="beanDetalle.ordenante.monto" value="${e:forHtmlAttribute(beanTranSpeiRecOper.beanDetalle.ordenante.monto)}"/>
    <input type="hidden" id="beanDetalle.detalleGral.fechaCaptura" name="beanDetalle.detalleGral.fchCaptura" value="${e:forHtmlAttribute(beanTranSpeiRecOper.beanDetalle.detalleGral.fchCaptura)}"/>
	

	<%-- Componente buscador simple --%>
	<div class="frameBuscadorSimple">


		<div class="titleBuscadorSimple">
			<p>${bancoOrde}: <input type="text" name="beanDetRecepOperacion.detalleGral.cveInstOrd" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleGral.cveInstOrd)}" readonly="readonly" class="tamanioDefinidoEncabezado"/> </p>
		</div>
		<table>
			<caption> </caption>
			<tbody>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>
			<tr>
				<td  class="text_derecha" width="80px">${importe}</td>
				<td  colspan="3" >
					<input type="text" name="beanDetRecepOperacion.ordenante.monto" value="${beanDatos.beanDetRecepOperacion.ordenante.monto}" readonly="readonly" class="tamanioDefinidoBO "/>
				</td>

				<td class="text_derecha" width="100px">${iva}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.ordenante.iva" value="${beanDatos.beanDetRecepOperacion.ordenante.iva}" readonly="readonly" class="tamanioDefinido" />
				</td>
			</tr>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>

			<tr>
				<td class="text_derecha" width="80px">${folioPaquete}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.detalleGral.folioPaquete" value="${beanDatos.beanDetRecepOperacion.detalleGral.folioPaquete}" readonly="readonly" class="tamanioDefinidoBO"/>
				</td>
				<td class="text_derecha"></td>
				<td class="text_derecha" width="100px">${refeNumerica}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.detalleGral.refeNumerica" value="${beanDatos.beanDetRecepOperacion.detalleGral.refeNumerica}" readonly="readonly" class="tamanioDefinido"/>
				</td>
			</tr>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>

			<tr>
				<td class="text_derecha" width="80px">${folioPago}</td>
				<td   colspan="2">
					<input type="text" name="beanDetRecepOperacion.detalleGral.folioPago" value="${beanDatos.beanDetRecepOperacion.detalleGral.folioPago}" readonly="readonly" class="tamanioDefinidoBO"/>
				</td>
				<td class="text_derecha"></td>
				<td class="text_derecha" width="100px">${cuentaOrd}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.ordenante.numCuentaOrd" value="${beanDatos.beanDetRecepOperacion.ordenante.numCuentaOrd}" readonly="readonly" class="tamanioDefinido"/>
				</td>
			</tr>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>

			<tr>
				<td class="text_derecha" width="80px">${tipo}</td>
				<td  colspan="2" >
					<input type="text" name="beanDetRecepOperacion.detalleTopo.tipoPago" value="${beanDatos.beanDetRecepOperacion.detalleTopo.tipoPago}" readonly="readonly" class="tamanioDefinidoBO" class="text_personalizado"/>
				</td>
				<td class="text_derecha"></td>
				<td class="text_derecha" width="100px">${rfc}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.ordenante.rfcOrd" value="${beanDatos.beanDetRecepOperacion.ordenante.rfcOrd}" readonly="readonly" class="tamanioDefinido"/>
				</td>
			</tr>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>

			<tr>
				<td class="text_derecha" width="80px">${nombre}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.ordenante.nombreOrd" value="${beanDatos.beanDetRecepOperacion.ordenante.nombreOrd}" readonly="readonly" class="tamanioDefinidoBO"/>
				</td>
				<td class="text_derecha"></td>
				<td class="text_derecha" width="100px">${rastreo}</td>
				<td colspan="2">
					<input type="text" name="beanDetRecepOperacion.detalleGral.cveRastreo" value="${beanDatos.beanDetRecepOperacion.detalleGral.cveRastreo}" readonly="readonly" class="tamanioDefinido"/>
				</td>
			</tr>
			<tr>
				<td class="text_derecha" width="80px"></td>
			</tr>

			<tr>
				<td class="text_derecha" width="80px">${bucCliente}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.beanCambios.bucCliente" value="${beanDatos.beanDetRecepOperacion.beanCambios.bucCliente}" readonly="readonly" class="tamanioDefinidoBO"/>
				</td>
				<td class="text_derecha"></td>
				<td class="text_derecha" width="80px">${importeCargo}</td>
				<td  colspan="2">
					<input type="text" name="beanDetRecepOperacion.ordenante.importeCargo" value="${beanDatos.beanDetRecepOperacion.ordenante.importeCargo}" readonly="readonly" class="tamanioDefinido"/>
				</td>
			</tr>
			</tbody>
		</table>


		<div class="titleBuscadorSimple">
			<p>${bancoRece}: <input type="text" name="beanDetRecepOperacion.receptor.bancoRec" value="${beanDatos.beanDetRecepOperacion.receptor.bancoRec}" readonly="readonly" class="tamanioDefinidoEncabezado2" /> </p>
		</div>
		<table>
			<caption> </caption>
			<tbody>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${cuentaRec1}</td>
					<td  colspan="2" >
						<input type="text" value="${beanDatos.beanDetRecepOperacion.receptor.numCuentaRec}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${tipo}</td>
					<td  colspan="2">
						<input type="text" value="${beanDatos.beanDetRecepOperacion.detalleTopo.tipoPago}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${rfc}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.rfcRec" value="${beanDatos.beanDetRecepOperacion.receptor.rfcRec}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${nombre}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.nombreRec" value="${beanDatos.beanDetRecepOperacion.receptor.nombreRec}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${cuentaRec2}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.numCuentaRec2" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.receptor.numCuentaRec2)}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${tipoPago2}</td>
					<td  colspan="2">
						<input type="text" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.receptor.numCuentaRec2)}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${rfc2}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.rfcRec2" value="${beanDatos.beanDetRecepOperacion.receptor.rfcRec2}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${nombre2}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.nombreRec2" value="${beanDatos.beanDetRecepOperacion.receptor.nombreRec2}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${ctaReceptora}</td>
					<td  colspan="2">
						<input id="ctaReceptora" name="beanDetRecepOperacion.receptor.numCuentaRec" type="text" value="${beanDatos.beanDetRecepOperacion.receptor.numCuentaRec}" class="tamanioDefinidoEspecial" />

						<div class ="tooltip">

						 <span class="tooltiptext">${EMO0002V}</span>
						 <img  id="messageHelp2" src="${pageContext.servletContext.contextPath}/lf/default/img/ayuda.jpg" alt="Ayuda">
						</div>

					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${cvePago}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detEstatus.cvePago" value="${beanDatos.beanDetRecepOperacion.detEstatus.cvePago}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${refeCobranza}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detEstatus.refeCobranza" value="${beanDatos.beanDetRecepOperacion.detEstatus.refeCobranza}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${importeAbono}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.importeAbono" value="${beanDatos.beanDetRecepOperacion.receptor.importeAbono}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${pagoCompen}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.receptor.importeDls" value="${beanDatos.beanDetRecepOperacion.receptor.importeDls}" readonly="readonly" class="tamanioDefinidoBR"/>
					</td>
				</tr>
			</tbody>
		</table>


		<div class="titleBuscadorSimple">
			<p>${detalleCta}</p>
		</div>
		<table>
			<caption> </caption>
			<tbody>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${ctaRecOrig}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.ctaReceptoraOri" value="${beanDatos.beanDetRecepOperacion.beanCambios.ctaReceptoraOri}" readonly="readonly" class="tamanioDefinidoCR"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${ctaRecInterna}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.numCuentClv" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.numCuentClv}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${aplicativo}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.aplicativo" value="${beanDatos.beanDetRecepOperacion.beanCambios.aplicativo}" readonly="readonly" class="tamanioDefinidoCR"/>
					</td>
				</tr>
			</tbody>
		</table>


				<div class="titleBuscadorSimple">${envioDev}</div>
		<table>
			<caption> </caption>
			<tbody>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${motivo}</td>
					<td  colspan="2">
						<input type="text" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.beanCambios.motivoDevol)} - ${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.beanCambios.motivoDevolDesc)}" readonly="readonly" class="tamanioDefinidoED"/>
						<input type="hidden" name="beanDetRecepOperacion.beanCambios.motivoDevol" id="motivoDevol" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.beanCambios.motivoDevol)}">
						<input type="hidden" name="beanDetRecepOperacion.beanCambios.motivoDevolDesc" id="motivoDevolDesc" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.beanCambios.motivoDevolDesc)}">
					</td>
					<td class="text_derecha"></td>

					<td class="text_derecha" width="100px">${folioPaquete}</td>
					<td  colspan="2">
						<input type="text" value="${beanDatos.beanDetRecepOperacion.detalleGral.folioPaquete}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${folioPago}</td>
					<td  colspan="2">
						<input type="text" value="${beanDatos.beanDetRecepOperacion.detalleGral.folioPago}" readonly="readonly" class="tamanioDefinidoED"/>
					</td>
				</tr>
			</tbody>
		</table>

		<div class="titleBuscadorSimple">
			<p>${otros}</p>
		</div>
		<table>
			<caption> </caption>
			<tbody>
				<tr>
					<td class="text_derecha" width="80px">${switf}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.swift" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.swift}" readonly="readonly" class="tamanioDefinidoOT"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${switfUno}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.swiftUno" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.swiftUno}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${switfDos}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.swiftDos" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.swiftDos}" readonly="readonly" class="tamanioDefinidoOT"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${refeAdicional}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.refeAdi" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.refeAdi}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${beneRecu}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.indBenefRec" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.indBenefRec}" readonly="readonly" class="tamanioDefinidoOT"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${montoPAgo}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.montoPgoOri" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.montoPgoOri}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${FchOperOrig}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.beanCambios.otros.fchOperOri" value="${beanDatos.beanDetRecepOperacion.beanCambios.otros.fchOperOri}" readonly="readonly" class="tamanioDefinidoOT"/>
					</td>
				    <td class="text_derecha"></td>
					<td class="text_derecha" width="80px">${concepto}</td>
					<td  colspan="2">
						<textarea class="tamanioDefinidoEspecialTxtArea">${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.conceptoPago1)} - ${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.conceptoPago2)}</textarea>
						<input type="hidden" id="conceptoPago1" name="beanDetRecepOperacion.detEstatus.conceptoPago1" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.conceptoPago1)}">
						<input type="hidden" id="conceptoPago2" name="beanDetRecepOperacion.detEstatus.conceptoPago2" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.conceptoPago2)}">
					</td>
				</tr>
			</tbody>
		</table>



		<div class="titleBuscadorSimple">${dtosGrales}</div>
		<table>
			<caption> </caption>
			<tbody>
				<tr>
					<td class="text_derecha" width="80px">${estatusBanxico}</td>
					<td  colspan="2">
						<input type="text" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.estatusBanxico)} - ${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.descripcionBanxico)}" readonly="readonly" class="tamanioDefinidoDG"/>
						<input type="hidden" id="estatusBanxico" name="beanDetRecepOperacion.detEstatus.estatusBanxico" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.estatusBanxico)}">
						<input type="hidden" id="descripcionBanxico" name="beanDetRecepOperacion.detEstatus.descripcionBanxico" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.descripcionBanxico)}">
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${transfer}</td>
					<td  colspan="2">
						<input type="text" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.estatusTransfer)}  - ${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.descripcionTrasnfer)}" readonly="readonly" class="tamanioDefinido"/>
						<input type="hidden" id="estatusTransfer" name="beanDetRecepOperacion.detEstatus.estatusTransfer" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detEstatus.estatusTransfer)}">
						<input type="hidden" id="descripcionTrasnfer" name="beanDetRecepOperacion.detEstatus.descripcionTrasnfer" value="${beanDatos.beanDetRecepOperacion.detEstatus.descripcionTrasnfer}">
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${topologia}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detalleTopo.topologia" value="${beanDatos.beanDetRecepOperacion.detalleTopo.topologia}" readonly="readonly" class="tamanioDefinidoDG"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${tipoPago}</td>
					<td  colspan="2">
						<input type="text" value="${beanDatos.beanDetRecepOperacion.detalleTopo.tipoPago}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px"></td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${prioridad}</td>
					<td  colspan="2">
						<input type="text" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleTopo.prioridad)} - ${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleTopo.descripcionPrioridad)}" readonly="readonly" class="tamanioDefinidoDG"/>
						<input type="hidden" id="prioridad" name="beanDetRecepOperacion.detalleTopo.prioridad" value="${beanDatos.beanDetRecepOperacion.detalleTopo.prioridad}">
						<input type="hidden" id="descripcionPrioridad" name="beanDetRecepOperacion.detalleTopo.descripcionPrioridad" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleTopo.descripcionPrioridad)}">
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${tipoOper}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detalleTopo.tipoOperacion" value="${beanDatos.beanDetRecepOperacion.detalleTopo.tipoOperacion}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>

				<tr>
					<td class="text_derecha" width="80px">${estatusOpe}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detalleGral.estatusOpe" value="${beanDatos.beanDetRecepOperacion.detalleGral.estatusOpe}" readonly="readonly" class="tamanioDefinidoDG"/>
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${cveTransfer}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detalleGral.cveTransfer" value="${beanDatos.beanDetRecepOperacion.detalleGral.cveTransfer}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${desCveTransf}</td>
					<td  colspan="2">
						<textarea class="tamanioDefinidoEspecialTxtArea">${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleGral.desCveTransf)}</textarea>
						<input type="hidden" id="desCveTransf" name="beanDetRecepOperacion.detalleGral.desCveTransf" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleGral.desCveTransf)}">
					</td>
					<td class="text_derecha"></td>
					<td class="text_derecha" width="100px">${txtRefTrans}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detEstatus.refeTransfer" value="${beanDatos.beanDetRecepOperacion.detEstatus.refeTransfer}" readonly="readonly" class="tamanioDefinido"/>
					</td>
				</tr>
				<tr>
					<td class="text_derecha" width="80px">${cveOperacion}</td>
					<td  colspan="2">
						<input type="text" name="beanDetRecepOperacion.detalleGral.cveOperacion" value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleGral.cveOperacion)}" readonly="readonly" class="tamanioDefinidoDG"/>
					</td>
                    <td class="text_derecha"></td>
					<td class="text_derecha tamanofechaliq"><label for="beanDetRecepOperacion.detalleGral.fchCaptura">${txtFechaLiq}</label></td>
					<td  colspan="2"> 
						<input class="tamanioDefinidoDG" type="text" id="beanDetRecepOperacion.detalleGral.fchCaptura" name="beanDetRecepOperacion.detalleGral.fchCaptura"  value="${e:forHtmlAttribute(beanDatos.beanDetRecepOperacion.detalleGral.fchCaptura)}" readonly="readonly" />				
					</td>                        
				</tr>
			</tbody>
		</table>

	</div>

	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption> </caption>
					<tr>
						<td width="279" class="izq" id="guardar" >
                            <a id="idGuardar" href="javascript:;" ref=" ${e:forHtmlAttribute(fchOperacion)}" ref2="${e:forHtmlAttribute(cveMiInstitucion)}" ref3="${e:forHtmlAttribute(cveInstOrd)}" ref4 ="${e:forHtmlAttribute(folioPag)}" ref5="${e:forHtmlAttribute(folioPag)}" ref6="SALDOS" ref7="${e:forHtmlAttribute(nombrePantalla)}">${guardar}</a>
						</td>

                        <td width="279" class="izq_Des" id="guardarA" ><a href="javascript:;" > ${guardar} </a></td>



						<td width="279"  class="odd">${espacioEnBlanco}</td>

						<td width="279" class="der">
							<a id="idRegresar" href="javascript:;" ref="SALDOS" ref2="${pagina}" ref3="${nombrePantalla}">${regresar}</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>
