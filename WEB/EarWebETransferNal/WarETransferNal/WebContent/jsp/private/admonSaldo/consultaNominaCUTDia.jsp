<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* reportePagoSPEI.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     13/09/2011 12:00 Creación de la pantalla
*
***********************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuAdmonSaldo.jsp" flush="true">
	<jsp:param name="menuItem" value="admonSaldo" />
	<jsp:param name="menuSubitem" value="muestraConsultaNominaCutDia" />
</jsp:include>

<spring:message code="moduloCDA.myMenu.text.adminSaldos" var="tituloModulo"/>
<spring:message code="moduloCDA.administraSaldos.text.titulo" var="titulo"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.label.subtitulo" var="txtSubtitulo"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.label.recepcionNomina" var="txtRecepcionNomina"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.label.recepcionNominaPre" var="txtRecepcionNominaPre"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.label.cantidad" var="txtCantidad"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.label.monto" var="txtMonto"/>
<spring:message code="moduloAdmonSaldo.consultaNominaCutDia.boton.consultar" var="btnConsultar"/>


<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="general.buscar"                             	var="buscar"/>
<spring:message code="general.filtroBusqueda"                   	var="filtroBusqueda" text=""/>
<spring:message code="general.regresar" var="regresar"/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/admonSaldo/consultaNominaCUTDia.js"></script>

<c:set var="searchString" value="${seguTareas}"/>
<form id="idForm" action="" method="post">
		
		

	<div class="pageTitleContainer">
		<span class="pageTitle">${titulo}</span>  - ${txtSubtitulo}
		<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.fechaOperacion}</span>
	</div>


	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
			${instruccionBuscador}
			<table>
				<tbody>
					<tr>
						<td class="text_derecha"></td>
						<td class="text_izquierda" colspan="2"> ${txtRecepcionNomina} </td>
						<td class="text_derecha"></td>
						<td class="text_izquierda" colspan="2"> ${txtRecepcionNominaPre} </td>
					</tr>
					<tr>
						<td class="text_derecha" width="80px">${txtCantidad}</td>
						<td class="text_izquierda" > 
							<input type="text" value="${beanRes.cantidadPagos}" readonly="readonly"/>							
						</td>
						<td class="text_derecha"></td>
						<td class="text_derecha" width="100px">${txtCantidad}</td>
						<td class="text_izquierda">
							<input type="text" value="${beanRes.cantidadPagosPre}" readonly="readonly"/>
						</td>
					</tr>
					
					<tr>
						<td class="text_derecha" width="80px">${txtMonto}</td>
						<td class="text_izquierda" > 
							<input type="text" value="${beanRes.monto}" readonly="readonly"/>
														
						</td>
						<td class="text_derecha"></td>
						<td class="text_derecha" width="100px">${txtMonto}</td>
						<td class="text_izquierda">
							<input type="text" value="${beanRes.montoPre}" readonly="readonly"/>
						</td>
					</tr>
					
                    <tr>
                    	<td  class="text_derecha" width="100" colspan="2"></td>
                        <td class="text_izquierda" colspan="2">	  
                        </td>
                        <td class="text_derecha" >
                    	<c:choose>
								<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
	                        		<span><a id="idBuscar" href="javascript:;">${btnConsultar}</a></span>
	                        	</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${btnConsultar}</a></span>
							</c:otherwise>
						</c:choose>
                    	</td>
                    </tr>
					<tr>
					<td class="text_izquierda">
					</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<!-- Componente tabla estandar -->
	<div class="frameTablaVariasColumnas"> 

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<td width="279" class="cero"></td>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
					</tr>
				</table>
			</div>
		</div>
			
	</div>

	
</form>	

<jsp:include page="../myFooter.jsp" flush="true"/>
	
	    <script type = "text/javascript" defer="defer">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
    </script>
    


<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>