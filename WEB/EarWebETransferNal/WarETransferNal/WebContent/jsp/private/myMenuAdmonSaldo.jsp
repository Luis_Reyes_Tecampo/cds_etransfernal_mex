<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="mx.isban.agave.configuracion.Configuracion"%>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"           type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css"/>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css"/>

<spring:message code="moduloCatalogo.myMenu.text.moduloCDA" var="moduloCDA"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI" />
<spring:message code="moduloCatalogo.myMenu.text.catIntsFinancieras" var="catIntsFinancieras"/>
<spring:message code="moduloCatalogo.myMenu.text.parametrosPOACOA" var="parametrosPOACOA"/>
<spring:message code="moduloCatalogo.myMenu.text.horaEnvioSPEI" var="horaEnvioSPEI"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloCatalogos" var="moduloCatalogos"/>
<spring:message code="moduloCatalogo.myMenu.text.principal" var="principal"/>
<spring:message code="moduloCDA.myMenu.text.adminSaldos" var="adminSaldos"/>
<spring:message code="moduloCDA.myMenu.text.recepcionOperacion" var="recepcionOperacion"/>	
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="pagoInteres.myMenu.text.moduloPagoInteres" var="txtModuloPagoInteres"/>
<spring:message code="moduloCDA.myMenu.text.moduloCDASPID" var="moduloCDASPID"/>
<spring:message code="moduloAdmonSaldo.myMenu.text.admonSaldo" var="txtAdmonSaldo"/>
<spring:message code="moduloAdmonSaldo.myMenu.text.consultaNominaCutDia" var="txtConsultaNominaCutDia"/>
<spring:message code="moduloAdmonSaldo.myMenu.text.consultaNominaCutHistorico" var="txtConsultaNominaCutHistorico"/>
<spring:message code="moduloCDA.myMenu.text.recepcionOperacionHist" var="recepcionOperacionHto"/>
	<spring:message code="moduloCatalogo.myMenu.text.moduloSPEI" var="moduloSPEI"/>
<spring:message code="SPEICero.formulario.title" var="moduloSPEICero"/>
<spring:message code="catalogos.menuCatalogos.txt.txtCapturas" var="capturasManuales"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="moduloContingencia"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloSPEI.recepOperacion.text.monitor" var="monitor"/>

<c:set var="searchString" value="${seguServicios}" />
	
<body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
			
					<li id="principal"   class="startMenuGroup">             <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do"><span>${principal}</span></a></li>	
					<li id="admonSaldo"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">             <span>${txtAdmonSaldo}</span></a>
						<ul>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraMonitorOpSPID.do')}">
									<li id="monitorAdmon">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/muestraMonitorOpSPID.do?nomPantalla=ADMON">&gt;
											<span class="subMenuText">${monitor}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="monitorAdmon">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitor}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraRecepOp.do')}">
									<li id="muestraRecepOp">
										<a href="${pageContext.servletContext.contextPath}/admonSaldo/consTranSpeiRec.do?nomPantalla=recepcionOperacion">&gt;
											<span class="subMenuText">${recepcionOperacion}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraRecepOp">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${recepcionOperacion}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'consTranSpeiRec.do')}">
									<li id="muestraRecepOpHto">
										<a href="${pageContext.servletContext.contextPath}/admonSaldo/muestraRecepOpHto.do?nomPantalla=recepcionOperacionHto">&gt;
											<span class="subMenuText">${recepcionOperacionHto}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraRecepOpHto">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${recepcionOperacionHto}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraAdminSaldo.do')}">
									 <li id="muestraAdminSaldo">
									 	<a href="${pageContext.servletContext.contextPath}/admonSaldo/muestraAdminSaldo.do">&gt;
		                             		<span class="subMenuText">${adminSaldos}</span>
		                             	</a>
		                             </li>
								</c:when>
								<c:otherwise>
									<li id="muestraAdminSaldo">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${adminSaldos}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraConsultaNominaCutDia.do')}">
									<li id="muestraConsultaNominaCutDia">
										<a href="${pageContext.servletContext.contextPath}/admonSaldo/muestraConsultaNominaCutDia.do">&gt;
											<span class="subMenuText">${txtConsultaNominaCutDia}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraConsultaNominaCutDia">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${txtConsultaNominaCutDia}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraConsultaNominaCutHist.do')}">
									<li id="muestraConsultaNominaCutHist">
										<a href="${pageContext.servletContext.contextPath}/admonSaldo/muestraConsultaNominaCutHist.do">&gt;
											<span class="subMenuText">${txtConsultaNominaCutHistorico}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraConsultaNominaCutHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${txtConsultaNominaCutHistorico}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</li>					
				    <li id="capturasManuales" class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/capturasManuales/capturasManualesInit.do"> <span>${capturasManuales}</span></a></li>
				    <li id="catalogos"        class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/catalogos/catalogosInit.do">               <span>${moduloCatalogos}</span></a></li>
					<li id="moduloCDA"        class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloCDA/moduloCDAInit.do">               <span>${moduloCDA}</span></a></li>					
					<li id="moduloCDASPID"    class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloCDASPID/moduloCDASPIDInit.do">       <span>${moduloCDASPID}</span></a>
					<li id="moduloPOACOA"     class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOAInit.do">         <span>${moduloPOACOASPEI}</span></a></li>
					<li id="moduloPOACOASPID"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOASPIDInit.do">         <span>${moduloPOACOASPID}</span></a></li>
					<li id="moduloSPID"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPID/moduloSPIDInit.do">             <span>${txtModuloSPID}</span></a></li>
					<li id="moduloSPEI"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEI/moduloSPEIInit.do">             <span>${moduloSPEI}</span></a></li>
					<li id="moduloSPEICero"    class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEICero/moduloSPEICero.do">         <span>${moduloSPEICero }</span></a></li>
					<li id="pagoInteres"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/pagoInteres/moduloPagoInteresInit.do">     <span>${txtModuloPagoInteres}</span></a></li>
					<li id="contingencia"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/contingencia/contingenciaInit.do">         <span>${moduloContingencia}</span></a></li>
			
				</ul>
			
				<div id="menuFooter">
					<div></div>
				</div>
	
			</div>
		</c:if>
	</div>
<div id="content_container">
