<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraBitacoraDeEnvio" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloSPID.myMenu.text.bitacoraDeEnvio" var="tituloModulo"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.bitacoraEnvio.text.titulo" var="tituloModulo"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.folio" var="folio"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.tipoMensaje" var="tipoMensaje"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.fechaHora" var="fechaHora"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.numPagos" var="numPagos"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.importe" var="importe"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.banco" var="banco"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.numReprocesos" var="reprocesos"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.acuse" var="acuse"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.bytesAcumulados" var="bytes"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.operacionCert" var="operacion"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.numCertificado" var="numCertificado"/>
<spring:message code="moduloSPID.bitacoraEnvio.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.bitacoraEnvio.botonLink.exportar" var="exportar"/>
<spring:message code="moduloSPID.bitacoraEnvio.botonLink.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.cveInstitucion" var="cveInstitucion"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>

 <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["OK00002V"] = '${OK00002V}';
		mensajes["ED00019V"] = '${ED00019V}';
		mensajes["ED00020V"] = '${ED00020V}';
		mensajes["ED00021V"] = '${ED00021V}';
		mensajes["ED00022V"] = '${ED00022V}';
		mensajes["ED00023V"] = '${ED00023V}';
		mensajes["ED00027V"] = '${ED00027V}';
    </script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/bitacoraEnvio.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="accion" id="accion" value=""/>
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResBitacoraEnvio.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResBitacoraEnvio.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResBitacoraEnvio.beanPaginador.paginaFin}"/>
	<input type="hidden" name="idPeticion" id="idPeticion" value=""/>
	<input type="hidden" name="totalReg" id="totalReg" value="${beanResBitacoraEnvio.totalReg}"/>
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
					<tr>
						<th width="223" data-id="folioSolicitud" class="text_centro sortField" scope="col">${folio}
							<c:if test="${sortField == 'folioSolicitud'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="tipoDeMensaje" class="text_centro sortField" scope="col">${tipoMensaje}
							<c:if test="${sortField == 'tipoDeMensaje'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="fechaHoraEnvio" class="text_centro sortField" scope="col">${fechaHora}
							<c:if test="${sortField == 'fechaHoraEnvio'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="numeroPagos" class="text_centro sortField" scope="col">${numPagos}
							<c:if test="${sortField == 'numeroPagos'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="importeAbono" class="text_centro sortField" scope="col">${importe}
							<c:if test="${sortField == 'importeAbono'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="codBanco" class="text_centro sortField" scope="col">${banco}
							<c:if test="${sortField == 'codBanco'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="numReprocesos" class="text_centro sortField" scope="col">${reprocesos}
							<c:if test="${sortField == 'numReprocesos'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="acuseRecibo" class="text_centro sortField" scope="col">${acuse}
							<c:if test="${sortField == 'acuseRecibo'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="bytesAcumulados" class="text_centro sortField" scope="col">${bytes}
							<c:if test="${sortField == 'bytesAcumulados'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="operacionCert" class="text_centro sortField" scope="col">${operacion}
							<c:if test="${sortField == 'operacionCert'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="numCertificado" class="text_centro sortField" scope="col">${numCertificado}
							<c:if test="${sortField == 'numCertificado'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th> 
					</tr>
					<tbody>				
						<c:forEach var="bitEnv" items="${beanResBitacoraEnvio.listaBitacoraEnvio}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td>${bitEnv.folioSolicitud}</td>
								<td>${bitEnv.tipoDeMensaje}</td>
								<td>${bitEnv.fechaHoraEnvio}</td>
								<td>${bitEnv.numeroPagos}</td>
								<td>${bitEnv.importeAbono}</td>
								<td>${bitEnv.codBanco}</td>
								<td>${bitEnv.numReprocesos}</td>
								<td>${bitEnv.acuseRecibo}</td>
								<td>${bitEnv.bytesAcumulados}</td>
								<td>${bitEnv.operacionCert}</td>
								<td>${bitEnv.numCertificado}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		<c:if test="${not empty beanResBitacoraEnvio.listaBitacoraEnvio}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResBitacoraEnvio.totalReg} registros | Importe p�gina: ${importePagina}</label>
				</div>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaIni == beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaIni != beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraBitacoraDeEnvio.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaAnt!='0' && beanResBitacoraEnvio.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','muestraBitacoraDeEnvio.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaAnt=='0' || beanResBitacoraEnvio.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResBitacoraEnvio.beanPaginador.pagina} - ${beanResBitacoraEnvio.beanPaginador.paginaFin}</label>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaFin != beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraBitacoraDeEnvio.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaFin == beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaFin != beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraBitacoraDeEnvio.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResBitacoraEnvio.beanPaginador.paginaFin == beanResBitacoraEnvio.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${beanResBitacoraEnvio.importeTotal}</label>
		</c:if>
		
		<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class="odd">&nbsp;</td>
							<td width="279" class="cero">&nbsp;</td>
						</tr> 
					</table>
				</div>
			</div>
		
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>