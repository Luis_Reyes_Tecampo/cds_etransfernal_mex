<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraMonitor" />
</jsp:include>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00019V" var="ED00019V"/>
<spring:message code="codError.ED00020V" var="ED00020V"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>

<spring:message code="moduloSPID.historialMensajes.text.titulo"      var="titulo"/>
<spring:message code="moduloSPID.historialMensajes.text.tituloDes"      var="tituloFuncionalidad"/>
<spring:message code="moduloSPID.historialMensajes.text.referencia"      var="referencia"/>
<spring:message code="moduloSPID.historialMensajes.text.del"      var="del"/>
<spring:message code="moduloSPID.historialMensajes.text.estatus"      var="estatus"/>
<spring:message code="moduloSPID.historialMensajes.text.al"      var="al"/>
<spring:message code="moduloSPID.historialMensajes.text.usuario"      var="usuario"/>
<spring:message code="moduloSPID.historialMensajes.text.Fecha"      var="Fecha"/>
<spring:message code="moduloSPID.historialMensajes.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.historialMensajes.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.historialMensajes.text.linkRegresar" var="regresar"/>


  <script type="text/javascript">
       var mensajes = { "aplicacion" : '${aplicacion}', "ED00019V" : '${ED00019V}', "ED00020V" : '${ED00020V}',
		"ED00021V" : '${ED00021V}', "ED00022V" : '${ED00022V}', "ED00023V" : '${ED00023V}', "ED00027V" : '${ED00027V}' };
	   var referencia = '${refe}';
	   var tipoOrden = '${tipoOrden}';
		
    </script>
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/historialMensajes.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/histMensajes.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<!-- Componente titulo de p�gina -->

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="paginaIni" id="paginaIni" value="${beanResHistorialMensajes.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResHistorialMensajes.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResHistorialMensajes.beanPaginador.paginaFin}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${titulo}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			
			
				<table >
					<tr>
						<td width="50" class="nowrap">${referencia}</td>
						<td ><input class="Campos" type="text" size="40"  value="${beanResHistorialMensajes.ref}" readonly="readonly" ></td>
					</tr>
				</table>
				<table>
					<tr>
						<th width="122" data-id="del" class="text_izquierda nowrap sortField">${del}
							<c:if test="${sortField == 'del'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="estatus" class="text_centro nowrap sortField" scope="col">${estatus}
							<c:if test="${sortField == 'estatus'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="al" class="text_centro nowrap sortField" scope="col">${al}
							<c:if test="${sortField == 'al'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="estatus2" class="text_centro nowrap sortField" scope="col">${estatus}
							<c:if test="${sortField == 'estatus2'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="usuario" class="text_centro nowrap sortField" scope="col">${usuario}
							<c:if test="${sortField == 'usuario'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="fecha" class="text_centro nowrap sortField" scope="col">${Fecha}
							<c:if test="${sortField == 'fecha'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
					</tr>
					<body>
						<c:forEach var="beanHistMsj" items="${beanResHistorialMensajes.listaHisMsj}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								  <td class="text_izquierda">${beanHistMsj.del}</td>
								  <td class="text_izquierda">${beanHistMsj.estatus}</td>
								  <td class="text_izquierda">${beanHistMsj.al}</td>
								  <td class="text_izquierda">${beanHistMsj.estatus2}</td>
								  <td class="text_izquierda">${beanHistMsj.usuario}</td>
								  <td class="text_izquierda">${beanHistMsj.fecha}</td>
							</tr>
						</c:forEach>
					</body>
				</table>
			</div>
				<c:if test="${not empty beanResHistorialMensajes.listaHisMsj}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResHistorialMensajes.totalReg} registros</label>
					</div>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaIni == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaIni != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaAnt!='0' && beanResHistorialMensajes.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaAnt=='0' || beanResHistorialMensajes.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResHistorialMensajes.beanPaginador.pagina} - ${beanResHistorialMensajes.beanPaginador.paginaFin}</label>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin != beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','historial.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResHistorialMensajes.beanPaginador.paginaFin == beanResHistorialMensajes.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
			</c:if>
		</div>

	<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>													
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="der_Des"><a href="javascript:;" >${regresar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>
				</div>
			</div>

</form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>