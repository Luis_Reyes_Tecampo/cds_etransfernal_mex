<%@ include file="../myHeader.jsp" %>

<c:choose>
    <c:when test="${nomPantalla == 'mantenimientoCertificadoSPEI'}">
		<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloSPEI" />
			<jsp:param name="menuSubitem" value="mantoCerti" />
		</jsp:include> 
        <br />
    </c:when>    
    <c:otherwise>
       	<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
			<jsp:param name="menuItem"    value="moduloSPID" />
			<jsp:param name="menuSubitem" value="IntMantenimientoCertificados" />
		</jsp:include> 
        <br />
    </c:otherwise>
</c:choose>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<%-- Titulo pagina --%>
<spring:message code="moduloSPID.mantenimiento.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<%-- Etiquetas para pagina --%>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="moduloSPID.configuracionParametros.text.txtModuloSPEI" var="txtModuloSPEI"/>

<%-- Mensajes Generales de la pantalla --%>
<%-- Menasajes para la aplicacion --%>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.CD00041V" var="CD00041V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.ED00061V" var="ED00061V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.ED00078V" var="ED0078V"/>
<spring:message code="general.S" var="S"/>
<spring:message code="general.N" var="N"/>
<spring:message code="general.guardar" var="Guardar"/>
<spring:message code="general.regresar" var="Regresar"/>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/mantenimientoCertificado.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}', "ED00065V":'${ED00065V}', "CD00041V":'${CD00041V}',"EC00011B":'${EC00011B}',"ED00061V":'${ED00061V}',"OK00000V":'${OK00000V}',"ED00078V":'${ED00078V}'};</script>

<div class="pageTitleContainer">
	<c:choose>
		<c:when test="${nomPantalla =='mantenimientoCertificadoSPEI'}">
			<span class="pageTitle">${txtModuloSPEI}</span> - ${tituloFuncionalidad}
			<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 		</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
		<br/>
		</c:when>
		<c:otherwise>
			<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
			<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 		</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
		<br/>
		</c:otherwise>
	</c:choose>
</div>

<input id="nomPantalla" name="nomPantalla" value="${nomPantalla}" type="hidden"></input>

<form:form name="idForm" id="idForm" method="post" commandName="beanMantenimientoCertificado">
	<input type="hidden" id="numCertOld" name="numCertOld" value="${beanMantenimientoCertificadoOld.beanMantenimientoCertificadoDos.numeroCertificado}"/>
	<input type="hidden" id="palCveOld" name="palCveOld" value="${beanMantenimientoCertificadoOld.beanMantenimientoCertificadoDos.palabraClave}"/>
	<input type="hidden" id="mantOld" name="mantOld" value="${beanMantenimientoCertificadoOld.beanMantenimientoCertificadoDos.mantenimiento}"/>
	<input type="hidden" id="fFirmaOld" name="fFirmaOld" value="${beanMantenimientoCertificadoOld.SFacultadFirma}"/>
	<input type="hidden" id="fMantOld" name="fMantOld" value="${beanMantenimientoCertificadoOld.SFacultadMant}"/>
	<input type="hidden" id="dFirmaOld" name="dFirmaOld" value="${beanMantenimientoCertificadoOld.SDefaultFirma}"/>
	<input type="hidden" id="dMantOld" name="dMantOld" value="${beanMantenimientoCertificadoOld.SDefaultMant}"/>
	<input type="hidden" id="mensajeOld" name="mensajeOld" value="${beanMantenimientoCertificadoOld.beanMantenimientoCertificadoDos.mensaje}"/>
	<input type="hidden" id="cveInsOld" name="cveInsOld" value="${beanMantenimientoCertificadoOld.claveIns}"/>
	
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">
			<c:if test="${isEditar ne 'editar' }">Alta Mantenimiento Certificado</c:if>
			<c:if test="${isEditar eq 'editar' }">Editar Mantenimiento Certificado<td class="izq"></c:if>
		</div>
			<div class="contentBuscadorSimple">
				<table>
					<caption><%--Formulario alta de certificados--%></caption>
					<tr>
						<td th="150">
							N&uacute;mero Certificado*:
						</td>
						<td>
							<form:input id="numCert" path="beanMantenimientoCertificadoDos.numeroCertificado" maxlength="21"/>
						</td>
					</tr>
					<tr>
						<td>
							Palabra Clave*:
						</td>
						<td>
							<form:password id="palClave" path="beanMantenimientoCertificadoDos.palabraClave" showPassword="true" maxlength="50" onfocus="javascript:limpia(this);"/>
						</td>
					</tr>
					<tr>
						<td>
							Mantenimiento*:
						</td>
						<td>
							<form:select id="mant" path="beanMantenimientoCertificadoDos.mantenimiento" maxlength="21">
								<form:option value=""> --Selecciona--</form:option>
								<form:option value="A">Alta Cert. con Facul. Firma</form:option>
								<form:option value="1">Alta Cert. con Facul. Mant.</form:option>
								<form:option value="B">Baja Certificado</form:option>
								<form:option value="F">Alta Facultad Firma</form:option>
								<form:option value="2">Baja Facultad Firma</form:option>
								<form:option value="M">Alta Facultad Mant.</form:option>
								<form:option value="3">Baja Facultad Mant.</form:option>
								<form:option value="N">No hay Mant. Pendiente</form:option>
								<form:option value="E">Error Ultimo Mant.</form:option>
								<form:option value="T">Mant. en Transito</form:option>	
							</form:select>
						</td>
					</tr>	
					
					<tr>
						<td>
							Facultades*:
						</td>
						<td>Firma:
							<form:select id="sFF" path="sFacultadFirma" >
								<form:option value=""> --Selecciona--</form:option>
								<form:option value="N">${N}</form:option>
								<form:option value="S">${S}</form:option>	
							</form:select>
							&nbsp;Mant.:
							<form:select id="sFM" path="sFacultadMant">
								<form:option value=""> --Selecciona--</form:option>
								<form:option value="N">${N}</form:option>
								<form:option value="S">${S}</form:option>	
							</form:select>
						</td>
					</tr>										
					<tr>
						<td>
							Default*:
						</td>
						<td>Firma:
							<form:select id="sDF" path="sDefaultFirma">
								<form:option value=""> --Selecciona--</form:option>
								<form:option value="N">${N}</form:option>
								<form:option value="S">${S}</form:option>	
							</form:select>
							&nbsp;Mant.:
							<form:select id="sDM" path="sDefaultMant">
								<form:option value=""> --Selecciona--</form:option>
								<form:option value="N">${N}</form:option>
								<form:option value="S">${S}</form:option>	
							</form:select>
						</td>
					</tr>
					<tr>
						<td>
							Mensaje:
						</td>
						<td>
							<form:input id="mensaje" path="beanMantenimientoCertificadoDos.mensaje" maxlength="60"/>
						</td>
					</tr>
				</table>
				
			</div>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<caption><%--Formulario editar certificados--%></caption>
						<tr>
							<c:choose>
								<c:when test="${'editar' eq isEditar}">
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
											<td class="izq"><a href="javascript:;" id="idEditarG">${Guardar}</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">${Guardar}</a></td>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td class="izq"><a href="javascript:;" id="idGuardar">${Guardar}</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">${Guardar}</a></td>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							<td class="der"><a href="javascript:;" id="idRegresar">${Regresar}</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form:form>
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
	</c:if>