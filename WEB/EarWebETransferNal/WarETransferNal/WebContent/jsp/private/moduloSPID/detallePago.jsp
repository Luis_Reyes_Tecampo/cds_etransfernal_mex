<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraMonitor" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<!-- Titulo pagina -->
<spring:message code="moduloSPID.detallePago.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.detallePago.text.txtIntermediario" var="txtIntermediario"/>
<spring:message code="moduloSPID.detallePago.text.txtNombre" var="txtNombre"/>
<spring:message code="moduloSPID.detallePago.text.txtNumeroCuenta" var="txtNumeroCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtPuntoVenta" var="txtPuntoVenta"/>
<spring:message code="moduloSPID.detallePago.text.txtDivisa" var="txtDivisa"/>
<spring:message code="moduloSPID.detallePago.text.txtImporteCargo" var="txtImporteCargo"/>
<spring:message code="moduloSPID.detallePago.text.txtTipoCuenta" var="txtTipoCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtCodigoPostal" var="txtCodigoPostal"/>
<spring:message code="moduloSPID.detallePago.text.txtFechaConstit" var="txtFechaConstit"/>
<spring:message code="moduloSPID.detallePago.text.txtRfc" var="txtRfc"/>
<spring:message code="moduloSPID.detallePago.text.txtDomicilio" var="txtDomicilio"/>
<spring:message code="moduloSPID.detallePago.text.txtSucursal" var="txtSucursal"/>
<spring:message code="moduloSPID.detallePago.text.txtNumeroCuenta" var="txtNumeroCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtClaveSwiftCuenta" var="txtClaveSwiftCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtImporteAbono" var="txtImporteAbono"/>
<spring:message code="moduloSPID.detallePago.text.txtTipoCuenta" var="txtTipoCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtConceptoPago" var="txtConceptoPago"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario1" var="txtComentario1"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario2" var="txtComentario2"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario3" var="txtComentario3"/>

<!-- Etiquetas para pagina -->
<spring:message code="moduloSPID.detallePago.text.txtEstatus" var="txtEstatus"/>
<spring:message code="moduloSPID.detallePago.text.linkHistoriaMensaje" var="historiaMensaje"/>
<spring:message code="moduloSPID.detallePago.text.linkRegresar" var="regresar"/>

<!-- Mensajes Generales de la pantalla -->
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<!-- Menasajes para la aplicacion -->
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}', "ED00065V":'${ED00065V}', "CD00167V":'${CD00167V}', "ED00068V":'${ED00068V}'};
    var tipoOrden = '${tipoOrden}';
    var referencia = '${referencia}';

</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/detallePago.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/listadoPagos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco} 
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<div>
<table>
	<thead>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
	<tbody>	
	<tr>
		<td>${txtEstatus}</td>
		<td>
			<div>
				<input type="text" disabled="disabled" id="estatus" name="status" 
					value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.estatus}" 
					class="inputT Campos" size="20" maxlength="12"/>
			</div>
	    </td>
	</tr>
	</tbody>
</table>
</div>
<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResTopologia.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResTopologia.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResTopologia.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">

			<jsp:include page="_camposDetallePago.jsp" flush="true"/>
			
			<!-- Ordenante -->
			<div>
				<div class="titleBuscadorSimple">Ordenante</div>
			
				<table class="tablaContent">
					<thead>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtIntermediario}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="intermediario"
									name="intermediario"
									value="${beanDetallePago.beanOrdenantePago.intermediario}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNombre}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="nombre" name="nombre"
									value="${beanDetallePago.beanOrdenantePago.nombre}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNumeroCuenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="numeroCuenta"
									name="numeroCuenta"
									value="${beanDetallePago.beanOrdenantePago.numeroCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
			
			
					<tr>
			
						<td>${txtPuntoVenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="puntoVenta"
									name="puntoVenta"
									value="${beanDetallePago.beanOrdenantePago.puntoVenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDivisa}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="divisa" name="divisa"
									value="${beanDetallePago.beanOrdenantePago.divisa}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtImporteCargo}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="importeCargo"
									name="importeCargo"
									value="${beanDetallePago.beanOrdenantePago.importeCargo}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
			
					</tr>
			
					<tr>
			
						<td>${txtTipoCuenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="tipoCuenta"
									name="tipoCuenta"
									value="${beanDetallePago.beanOrdenantePago.tipoCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtCodigoPostal}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="codigoPostal"
									name="codigoPostal"
									value="${beanDetallePago.beanOrdenantePago.codigoPostal}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtFechaConstit}</td>
						<td>
			
							<div>
								<input type="text" disabled="disabled" id="fechaConstit"
									name="fechaConstit"
									value="${beanDetallePago.beanOrdenantePago.fechaConstit}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
					</tr>
			
					<tr>
						<td>${txtRfc}</td>
						<td>
			
							<div>
								<input type="text" disabled="disabled" id="rfc" name="rfc"
									value="${beanDetallePago.beanOrdenantePago.rfc}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDomicilio}</td>
						<td>
			
							<div>
								<input type="text" disabled="disabled" id=domicilio
									name="domicilio"
									value="${beanDetallePago.beanOrdenantePago.domicilio}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			
			<!-- Receptor -->
			<div>
				<div class="titleBuscadorSimple">Receptor</div>
				<table class="tablaContent">
						<thead>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtIntermediario}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="intermediarioDos"
									name="intermediarioDos"
									value="${beanDetallePago.beanReceptorPago.intermediario}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtSucursal}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="sucursal"
									name="sucursal"
									value="${beanDetallePago.beanReceptorPago.sucursal}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNumeroCuenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="numeroCuenta"
									name="numeroCuenta"
									value="${beanDetallePago.beanReceptorPago.numeroCuentaReceptor}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
			
			
					<tr>
			
						<td>${txtNombre}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="nombreDos"
									name="nombreDos"
									value="${beanDetallePago.beanReceptorPago.nombre}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDivisa}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="divisaDos"
									name="divisaDos"
									value="${beanDetallePago.beanReceptorPago.divisa}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtClaveSwiftCuenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="claveSwiftCuenta"
									name="claveSwiftCuenta"
									value="${beanDetallePago.beanReceptorPago.claveSwiftCorres}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
			
					</tr>
			
					<tr>
			
						<td>${txtImporteAbono}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="importeAbono"
									name="importeAbono"
									value="${beanDetallePago.beanReceptorPago.importeAbono}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtTipoCuenta}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="tipoCuenta"
									name="tipoCuenta"
									value="${beanDetallePago.beanReceptorPago.tipoCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtRfc}</td>
						<td>
							<div>
								<input type="text" disabled="disabled" id="rfcDos" name="rfcDos"
									value="${beanDetallePago.beanReceptorPago.rfc}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
					</tr>
			</tbody>
				</table>
			</div>
			
			<!-- Comentario -->
			<div>
				<div class="titleBuscadorSimple">Otros</div>
				<table class="tablaContent">
					<thead>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtConceptoPago}</td>
						<td colspan="2">
							<div>
								<input type="text" disabled="disabled" id="conceptoPago"
									name="conceptoPago"
									value="${beanDetallePago.beanReceptorPago.conceptoPago}"
									class="inputT Campos" size="51">
							</div>
						</td>
			
						<td>${txtComentario1}</td>
						<td colspan="2">
							<div>
								<input type="text" disabled="disabled" id="comentario1"
									name="comentario1"
									value="${beanDetallePago.beanReceptorPago.comentario1}"
									class="inputT Campos" size="52">
							</div>
						</td>
					</tr>
			
			
					<tr>
						<td>${txtComentario2}</td>
						<td colspan="2">
							<div>
								<input type="text" disabled="disabled" id="comentario12"
									name="comentario2"
									value="${beanDetallePago.beanReceptorPago.comentario2}"
									class="inputT Campos" size="51">
							</div>
						</td>
			
						<td>${txtComentario3}</td>
						<td colspan="2">
							<div>
								<input type="text" disabled="disabled" id="comentario3"
									name="comentario3"
									value="${beanDetallePago.beanReceptorPago.comentario3}"
									class="inputT Campos" size="52">
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="izq"><a id="idHistoriaMensaje" href="javascript:;">${historiaMensaje}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${historiaMensaje}</a></td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="der_Des"><a href="javascript:;" >${regresar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>
				</div>
			</div>

	</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
	</c:if>