<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntConfiguracionTopologias" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<!-- Titulo pagina -->
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.topologia.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<!-- Etiquetas para pagina -->

<spring:message code="moduloSPID.topologia.text.claveMedio" var="claveMedio"/>
<spring:message code="moduloSPID.topologia.text.claveOperacion" var="claveOperacion"/>
<spring:message code="moduloSPID.topologia.text.topologia" var="topologia"/>
<spring:message code="moduloSPID.topologia.text.prioridad" var="prioridad"/>
<spring:message code="moduloSPID.topologia.text.importeMinimo" var="importeMinimo"/>
<spring:message code="moduloSPID.topologia.text.agregar" var="agregar"/>
<spring:message code="moduloSPID.topologia.text.editar" var="editar"/>
<spring:message code="moduloSPID.topologia.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.topologia.text.eliminar" var="eliminar"/>

<!-- Mensajes Generales de la pantalla -->
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<!-- Menasajes para la aplicacion -->
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}',
                    "ED00065V":'${ED00065V}',
                    "CD00167V":'${CD00167V}',
                    "ED00068V":'${ED00068V}'
                    };
	 
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/confTopologia.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResTopologia.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResTopologia.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResTopologia.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" name="sortField" id="sortField" value="${sortField}">
		<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
					<tr>
						<th width="110" class="text_centro" scope="col"></th>
						<th width="150" data-id="claveMedio" class="text_centro sortField" scope="col">${claveMedio}
							<c:if test="${sortField == 'claveMedio'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="claveOperacion" class="text_centro sortField" scope="col">${claveOperacion}
							<c:if test="${sortField == 'claveOperacion'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="topologia" class="text_centro sortField" scope="col">${topologia}
							<c:if test="${sortField == 'topologia'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="prioridad" class="text_centro sortField" scope="col">${prioridad}
							<c:if test="${sortField == 'prioridad'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="270" data-id="importeMinimo" class="text_centro sortField" scope="col">${importeMinimo}
							<c:if test="${sortField == 'importeMinimo'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
					</tr>
					<tbody>				
						<c:forEach var="beanTopologia" items="${beanResTopologia.listaTopologias}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td class="text_centro">
									<input name="listaTopologias[${rowCounter.index}].seleccionado" value="true" type="checkbox"/>
									<input type="hidden" name="listaTopologias[${rowCounter.index}].claveMedio" value="${beanTopologia.claveMedio}"/>
									<input type="hidden" name="listaTopologias[${rowCounter.index}].claveOperacion" value="${beanTopologia.claveOperacion}"/>
									<input type="hidden" name="listaTopologias[${rowCounter.index}].topologia" value="${beanTopologia.topologia}"/>
									<input type="hidden" name="listaTopologias[${rowCounter.index}].prioridad" value="${beanTopologia.prioridad}"/>
									<input type="hidden" name="listaTopologias[${rowCounter.index}].importeMinimo" value="${beanTopologia.importeMinimo}"/>																	
								</td>
								<td class="text_centro">${beanTopologia.claveMedio}</td>
								<td class="text_centro">${beanTopologia.claveOperacion}</td>
								<td class="text_centro">${beanTopologia.topologia}</td>
								<td class="text_centro">${beanTopologia.prioridad}</td>
								<td style="text-align: right;">${beanTopologia.importeMinimo}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<c:if test="${not empty beanResTopologia.listaTopologias}">
				<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResTopologia.totalReg} registros | Importe p�gina: ${importePagina}</label>
				</div>
					<c:if test="${beanResTopologia.beanPaginador.paginaIni == beanResTopologia.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaIni != beanResTopologia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaAnt!='0' && beanResTopologia.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaAnt=='0' || beanResTopologia.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResTopologia.beanPaginador.pagina} - ${beanResTopologia.beanPaginador.paginaFin}</label>
					<c:if test="${beanResTopologia.beanPaginador.paginaFin != beanResTopologia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaFin == beanResTopologia.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaFin != beanResTopologia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResTopologia.beanPaginador.paginaFin == beanResTopologia.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
				<label>Importe Total: ${beanResTopologia.importeTotal}</label>
			</c:if>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a id="idAgregar" href="javascript:;" >${agregar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${agregar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class=""></td>
						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
								<td class="izq"><a id="idEditar" href="javascript:;">${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${editar}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class=""></td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
									<td width="279" class="der"><a id="idEliminar" href="javascript:;" >${eliminar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
								</c:otherwise>	
							</c:choose>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
	</c:if>