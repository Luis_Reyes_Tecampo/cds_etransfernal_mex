<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntConfiguracionTopologias" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<!-- Titulo pagina -->
<spring:message code="moduloSPID.topologia.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloSPID.topologia.text.agregar" var="agregar"/>
<spring:message code="moduloSPID.topologia.text.editar" var="editar"/>
<spring:message code="moduloSPID.topologia.text.eliminar" var="eliminar"/>
<spring:message code="moduloSPID.topologia.text.regresar" var="regresar"/>
<spring:message code="moduloSPID.topologia.text.guardar" var="guardar"/>
<!-- Etiquetas para pagina -->
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<!-- Mensajes Generales de la pantalla -->
<!-- Menasajes para la aplicacion -->
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.CD00041V" var="CD00041V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/confTopologia.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}',
                    "ED00065V":'${ED00065V}',
                    "CD00041V":'${CD00041V}',
                    "EC00011B":'${EC00011B}'
                    };
	 
</script>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form:form name="idForm" id="idForm" method="post" commandName="beanTopologia">
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">
			<c:if test="${isEditar ne 'editar' }">Alta Topolog&iacute;a</c:if>
			<c:if test="${isEditar eq 'editar' }">Editar Topolog&iacute;a<td class="izq"></c:if>
		</div>
			<div class="contentBuscadorSimple">
				<input type="hidden" name="cveOperacion" value="${beanOldTopologia.claveOperacion}"/>
				<input type="hidden" name="cveMedio" value="${beanOldTopologia.claveMedio}"/>
				<input type="hidden" name="toPri" value="${beanOldTopologia.topologia}${beanTopologia.prioridad}"/>
				<table>				
					<tr>
						<td class="text_izquierda">Clave Medio Entidad:</td>
						<td colspan="2">
						 	<form:select id="cveMedio" path="claveMedio" itemValue="beanTopologia.claveMedio" style="width: 140px;">
    							<form:option value=""> --Selecciona--</form:option>
								<form:options items="${beanResTopologia.listaCveMedio}" itemValue="claveMedio" itemLabel="claveMedio"></form:options>
  							</form:select>
						</td>
						
						<td class="text_izquierda">Clave Operaci&oacute;n:</td>
						<td colspan="2">
							<form:select id="cveOperacion" path="claveOperacion" itemValue="beanTopologia.claveOperacion" style="width: 140px;">
    							<form:option value=""> --Selecciona--</form:option>
								<form:options items="${beanResTopologia.listaCveOperacion}" itemValue="claveOperacion" itemLabel="claveOperacion"></form:options>
  							</form:select>
						</td>
					</tr>
					<tr></tr>
					<tr>
						<td class="text_izquierda">Topolog�a:</td>
						<td>
							<form:radiobutton id="topologia" path="topologia" name="topologia" value="T"/>T
							&nbsp;													
							<form:radiobutton id="topologia" path="topologia" name="topologia" value="V"/>V
						</td>
						
						<td class="text_izquierda">Prioridad:</td>
						<td>
							<form:radiobutton id="prioridad" path="prioridad" name="prioridad" value="A"/>Alta  
							&nbsp;													
							<form:radiobutton id="prioridad" path="prioridad" name="prioridad" value="B"/>Baja
						</td>

					</tr>
					<tr class="text_izquierda">
						<td>Importe Minimo</td>
						<td><form:input id="importeMinimo" maxlength="40" path="importeMinimo" class="campos" type="text" name="importeMinimo"/> </td>
					</tr>
				</table>
				
			</div>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${'editar' eq isEditar}">
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
											<td class="izq"><a href="javascript:;" id="idEditarG">${guardar}</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">${guardar}</a></td>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td class="izq"><a href="javascript:;" id="idGuardar">${guardar}</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">${guardar}</a></td>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							<td class="der"><a href="javascript:;" id="idRegresar">${regresar}</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form:form>
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
	</c:if>