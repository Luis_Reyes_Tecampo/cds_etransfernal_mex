<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="muestraRecepOpSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
       
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="spid.recepOperacion.titulo.txtRecepcionesSpid" var="txtRecepcionesSpid"/>

<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="spid.detalleRecepOp.titulo.txtDetRecepOperaciones" var="txtDetRecepOperaciones"/>
<spring:message code="spid.detalleRecepOp.txt.txtGuardar" var="txtGuardar"/>
<spring:message code="spid.detalleRecepOp.txt.txtActualizar" var="txtActualizar"/>
<spring:message code="spid.detalleRecepOp.txt.txtRegresar" var="txtRegresar"/>
<spring:message code="spid.detalleRecepOp.txt.txtDatosGenerales" var="txtDatosGenerales"/>
<spring:message code="spid.detalleRecepOp.txt.txtOrdenante" var="txtOrdenante"/>
<spring:message code="spid.detalleRecepOp.txt.txtReceptor" var="txtReceptor"/>
<spring:message code="spid.detalleRecepOp.txt.txtFolioPaquete" var="txtFolioPaquete"/>
<spring:message code="spid.detalleRecepOp.txt.txtFolioPago" var="txtFolioPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtTopologia" var="txtTopologia"/>
<spring:message code="spid.detalleRecepOp.txt.txtCDA" var="txtCDA"/>
<spring:message code="spid.detalleRecepOp.txt.txtEnviar" var="txtEnviar"/>
<spring:message code="spid.detalleRecepOp.txt.txtEstatusBanxico" var="txtEstatusBanxico"/>
<spring:message code="spid.detalleRecepOp.txt.txtEstatusTransfer" var="txtEstatusTransfer"/>
<spring:message code="spid.detalleRecepOp.txt.txtModitoDev" var="txtModitoDev"/>
<spring:message code="spid.detalleRecepOp.txt.txtClasifOp" var="txtClasifOp"/>
<spring:message code="spid.detalleRecepOp.txt.txtTipoOp" var="txtTipoOp"/>
<spring:message code="spid.detalleRecepOp.txt.txtCodError" var="txtCodError"/>
<spring:message code="spid.detalleRecepOp.txt.txtFchInstruPago" var="txtFchInstruPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtHoraInstruPago" var="txtHoraInstruPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtFchAcepPago" var="txtFchAcepPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtHoraAcepPago" var="txtHoraAcepPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtRefeNum" var="txtRefeNum"/>
<spring:message code="spid.detalleRecepOp.txt.txtTipoPago" var="txtTipoPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtPrioridad" var="txtPrioridad"/>
<spring:message code="spid.detalleRecepOp.txt.txtLongRastreo" var="txtLongRastreo"/>
<spring:message code="spid.detalleRecepOp.txt.txtFolioPaqDev" var="txtFolioPaqDev"/>
<spring:message code="spid.detalleRecepOp.txt.txtFolioPagoDev" var="txtFolioPagoDev"/>
<spring:message code="spid.detalleRecepOp.txt.txtReferenciaTransfer" var="txtReferenciaTransfer"/>
<spring:message code="spid.detalleRecepOp.txt.txtMonto" var="txtMonto"/>
<spring:message code="spid.detalleRecepOp.txt.txtFchCaptura" var="txtFchCaptura"/>
<spring:message code="spid.detalleRecepOp.txt.txtCveRastreo" var="txtCveRastreo"/>
<spring:message code="spid.detalleRecepOp.txt.txtCveRastreoOri" var="txtCveRastreoOri"/>
<spring:message code="spid.detalleRecepOp.txt.txtDirIP" var="txtDirIP"/>
<spring:message code="spid.detalleRecepOp.txt.txtCveInstOrd" var="txtCveInstOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtTipoCtaOrd" var="txtTipoCtaOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtCveIntermeOrd" var="txtCveIntermeOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtCodPostalord" var="txtCodPostalord"/>
<spring:message code="spid.detalleRecepOp.txt.txtFchConstitOrd" var="txtFchConstitOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtRfcOrd" var="txtRfcOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtNumCtaOrd" var="txtNumCtaOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtNombreOrd" var="txtNombreOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtDomOrd" var="txtDomOrd"/>
<spring:message code="spid.detalleRecepOp.txt.txtTipoCtaRec" var="txtTipoCtaRec"/>
<spring:message code="spid.detalleRecepOp.txt.txtRfcRec" var="txtRfcRec"/>
<spring:message code="spid.detalleRecepOp.txt.txtNumCtaRec" var="txtNumCtaRec"/>
<spring:message code="spid.detalleRecepOp.txt.txtNumCtaTran" var="txtNumCtaTran"/>
<spring:message code="spid.detalleRecepOp.txt.txtNombreRec" var="txtNombreRec"/>
<spring:message code="spid.detalleRecepOp.txt.txtConceptoPago" var="txtConceptoPago"/>
<spring:message code="spid.detalleRecepOp.txt.txtFechaLiquidacion" var="txtFechaLiq"/>


<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.CD00043V" var="CD00043V"/>

	 <script type="text/javascript">
	 	var mensajes = {"aplicacion": '${aplicacion}',"CD00043V":'${CD00043V}'};
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/detalleRecepOpSPID.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${txtModuloSPID}</span> - ${txtRecepcionesSpid}
		<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.llave.cveMiInstituc} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.llave.fchOperacion}</span>
	</div>
 
	<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value="ACT"/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanRes.paginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="pagina" value="${beanRes.paginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanRes.paginador.paginaFin}"/>
		<input type="hidden" name="opcion" id="opcion" value="${beanRes.opciones.opcion}"/>
		<input type="hidden" name="servicio" id="servicio" value="${beanRes.opciones.servicio}"/>
		
		<input type="hidden" name="beanReq.opciones.opcion" id="beanReq.opciones.opcion" value="${beanRes.opciones.opcion}"/>
		<input type="hidden" name="beanReq.opciones.servicio" id="beanReq.opciones.servicio" value="${beanRes.opciones.servicio}"/>
		
		<input type="hidden" name="beanReqAnt.llave.fchOperacion" value="${beanRes.llave.fchOperacion}"/>				
		<input type="hidden" name="beanReqAnt.llave.cveMiInstituc" value="${beanRes.llave.cveMiInstituc}"/>
		<input type="hidden" name="beanReqAnt.llave.cveInstOrd" value="${beanRes.llave.cveInstOrd}"/>
		<input type="hidden" name="beanReqAnt.llave.folioPaquete" value="${beanRes.llave.folioPaquete}"/>
		<input type="hidden" name="beanReqAnt.llave.folioPago" value="${beanRes.llave.folioPago}"/>						
		<input type="hidden" name="beanReqAnt.datGenerales.topologia" value="${beanRes.datGenerales.topologia}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.enviar" value="${beanRes.datGenerales.enviar}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.cda" value="${beanRes.datGenerales.cda}"/>
		<input type="hidden" name="beanReqAnt.estatus.estatusBanxico" value="${beanRes.estatus.estatusBanxico}"/>
		<input type="hidden" name="beanReqAnt.estatus.estatusTransfer" value="${beanRes.estatus.estatusTransfer}"/>
		<input type="hidden" name="beanReqAnt.envDev.motivoDev" value="${beanRes.envDev.motivoDev}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.tipoCuentaOrd" value="${beanRes.bancoOrd.tipoCuentaOrd}"/>
		<input type="hidden" name="beanReqAnt.bancoRec.tipoCuentaRec" value="${beanRes.bancoRec.tipoCuentaRec}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.clasifOperacion" value="${beanRes.datGenerales.clasifOperacion}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.tipoOperacion" value="${beanRes.datGenerales.tipoOperacion}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.cveIntermeOrd" value="${beanRes.bancoOrd.cveIntermeOrd}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.codPostalOrd" value="${beanRes.bancoOrd.codPostalOrd}"/>
		<input type="hidden" name="beanReqAnt.estatus.codigoError" value="${beanRes.estatus.codigoError}"/>
		
		<input type="hidden" name="beanReqAnt.bancoOrd.fchConstitOrd" value="${beanRes.bancoOrd.fchConstitOrd}"/>
		<input type="hidden" name="beanReqAnt.fchPagos.fchInstrucPago" value="${beanRes.fchPagos.fchInstrucPago}"/>
		<input type="hidden" name="beanReqAnt.fchPagos.horaInstrucPago" value="${beanRes.fchPagos.horaInstrucPago}"/>
		<input type="hidden" name="beanReqAnt.fchPagos.fchAceptPago" value="${beanRes.fchPagos.fchAceptPago}"/>
		<input type="hidden" name="beanReqAnt.fchPagos.horaAceptPago" value="${beanRes.fchPagos.horaAceptPago}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.rfcOrd" value="${beanRes.bancoOrd.rfcOrd}"/>
		<input type="hidden" name="beanReqAnt.bancoRec.rfcRec" value="${beanRes.bancoRec.rfcRec}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.refeNumerica" value="${beanRes.bancoOrd.refeNumerica}"/>
		<input type="hidden" name="beanReqAnt.bancoRec.numCuentaTran" value="${beanRes.bancoRec.numCuentaTran}"/>

		<input type="hidden" name="beanReqAnt.bancoOrd.numCuentaOrd" value="${beanRes.bancoOrd.numCuentaOrd}"/>
		<input type="hidden" name="beanReqAnt.bancoRec.numCuentaRec" value="${beanRes.bancoRec.numCuentaRec}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.tipoPago" value="${beanRes.datGenerales.tipoPago}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.prioridad" value="${beanRes.datGenerales.prioridad}"/>
		<input type="hidden" name="beanReqAnt.rastreo.longRastreo" value="${beanRes.rastreo.longRastreo}"/>
		
		<input type="hidden" name="beanReqAnt.envDev.folioPaqDev" value="${beanRes.envDev.folioPaqDev}"/>
		<input type="hidden" name="beanReqAnt.envDev.folioPagoDev" value="${beanRes.envDev.folioPagoDev}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.refeTransfer" value="${beanRes.datGenerales.refeTransfer}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.monto" value="${beanRes.datGenerales.monto}"/>
   		<input type="hidden" name="beanReqAnt.datGenerales.fchCaptura" value="${beanRes.datGenerales.fchCaptura}"/>
   		<input type="hidden" name="beanReqAnt.rastreo.cveRastreo" value="${beanRes.rastreo.cveRastreo}"/>
		<input type="hidden" name="beanReqAnt.rastreo.cveRastreoOri" value="${beanRes.rastreo.cveRastreoOri}"/>
		<input type="hidden" name="beanReqAnt.rastreo.direccionIp" value="${beanRes.rastreo.direccionIp}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.cde" value="${beanRes.datGenerales.cde}"/>
		<input type="hidden" name="beanReqAnt.bancoOrd.nombreOrd" value="${beanRes.bancoOrd.nombreOrd}"/>
		<input type="hidden" name="beanReqAnt.bancoRec.nombreRec" value="${beanRes.bancoRec.nombreRec}"/>
   		<input type="hidden" name="beanReqAnt.bancoOrd.domicilioOrd" value="${beanRes.bancoOrd.domicilioOrd}"/>
   		<input type="hidden" name="beanReqAnt.datGenerales.conceptoPago" value="${beanRes.datGenerales.conceptoPago}"/>
		<input type="hidden" name="beanReqAnt.envDev.motivoDevolAnt" value="${beanRes.envDev.motivoDev}"/>
		
		<input type="hidden" name="beanReq.llave.fchOperacion" value="${beanRes.llave.fchOperacion}"/>				
		<input type="hidden" name="beanReq.llave.cveMiInstituc" value="${beanRes.llave.cveMiInstituc}"/>
		<input type="hidden" name="beanReq.datGenerales.cde" value="${beanRes.datGenerales.cde}"/>
		<input type="hidden" name="beanReqAnt.datGenerales.fechaCaptura" value="${beanRes.datGenerales.fechaCaptura}"/>


		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtDatosGenerales}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						${txtFolioPaquete}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="folioPaquete" name="beanReq.llave.folioPaquete" value="${beanRes.llave.folioPaquete}" maxlength="12" size="12" readonly="readonly"/>
					</td> 
					<td  class="text_izquierda">
						${txtFolioPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="folioPago" name ="beanReq.llave.folioPago" value="${beanRes.llave.folioPago}" maxlength="12" size="12" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtTopologia}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="topologia" name ="beanReq.datGenerales.topologia" value="${beanRes.datGenerales.topologia}" maxlength="1" size="1"/>
					</td>
					<td  class="text_izquierda">
						${txtCDA}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="cda" name ="beanReq.datGenerales.cda" value="${beanRes.datGenerales.cda}" maxlength="1" size="1"/>
					</td>
				</tr>
				
				
				<tr>
					<td  class="text_izquierda">
						${txtEnviar}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="enviar" name ="beanReq.datGenerales.enviar" value="${beanRes.datGenerales.enviar}" maxlength="1" size="1"/>
					</td>
					<td  class="text_izquierda">
						${txtEstatusBanxico}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="estatusBanxico" name="beanReq.estatus.estatusBanxico" value="${beanRes.estatus.estatusBanxico}" maxlength="2" size="2"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtEstatusTransfer}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="estatusTransfer" name="beanReq.estatus.estatusTransfer" value="${beanRes.estatus.estatusTransfer}" maxlength="2" size="2"/>
					</td>
					<td  class="text_izquierda">
						${txtModitoDev}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="motivoDev" name="beanReq.envDev.motivoDev" value="${beanRes.envDev.motivoDev}" maxlength="2" size="2"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtClasifOp}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="clasifOperacion" name="beanReq.datGenerales.clasifOperacion" value="${beanRes.datGenerales.clasifOperacion}" maxlength="2" size="2"/>
					</td>
					<td  class="text_izquierda">
						${txtTipoOp}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="tipoOperacion" name="beanReq.datGenerales.tipoOperacion" value="${beanRes.datGenerales.tipoOperacion}" maxlength="2" size="2"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtCodError}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="codigoError" name="beanReq.estatus.codigoError" value="${beanRes.estatus.codigoError}" maxlength="8" size="8"/>
					</td>
					<td  class="text_izquierda">
						${txtFchInstruPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="fchInstrucPago" name="beanReq.fchPagos.fchInstrucPago" value="${beanRes.fchPagos.fchInstrucPago}" maxlength="8" size="8"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtHoraInstruPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="horaInstrucPago" name="beanReq.fchPagos.horaInstrucPago" value="${beanRes.fchPagos.horaInstrucPago}" maxlength="11" size="11"/>
					</td>
					<td  class="text_izquierda">
						${txtFchAcepPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="fchAceptPago" name="beanReq.fchPagos.fchAceptPago" value="${beanRes.fchPagos.fchAceptPago}" maxlength="8" size="8"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtHoraAcepPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="horaAceptPago" name="beanReq.fchPagos.horaAceptPago" value="${beanRes.fchPagos.horaAceptPago}" maxlength="11" size="11"/>
					</td>
					<td  class="text_izquierda">
						${txtRefeNum}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="refeNumerica" name="beanReq.bancoOrd.refeNumerica" value="${beanRes.bancoOrd.refeNumerica}" maxlength="20" size="20"/>
					</td>
				</tr>
				
				
				<tr>
					<td  class="text_izquierda">
						${txtTipoPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="tipoPago" name="beanReq.datGenerales.tipoPago" value="${beanRes.datGenerales.tipoPago}" maxlength="7" size="7"/>
					</td>
					<td  class="text_izquierda">
						${txtPrioridad}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="prioridad" name="beanReq.datGenerales.prioridad" value="${beanRes.datGenerales.prioridad}" maxlength="7" size="7"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtLongRastreo}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="longRastreo" name="beanReq.rastreo.longRastreo" value="${beanRes.rastreo.longRastreo}" maxlength="7" size="7"/>
					</td>
					<td  class="text_izquierda">
						${txtFolioPaqDev}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="folioPaqDev" name="beanReq.envDev.folioPaqDev" value="${beanRes.envDev.folioPaqDev}" maxlength="12" size="12"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtFolioPagoDev}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="folioPagoDev" name="beanReq.envDev.folioPagoDev" value="${beanRes.envDev.folioPagoDev}" maxlength="12" size="12"/>
					</td>
					<td  class="text_izquierda">
						${txtReferenciaTransfer}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="refeTransfer" name="beanReq.datGenerales.refeTransfer" value="${beanRes.datGenerales.refeTransfer}" maxlength="12" size="12"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtMonto}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="monto" name="beanReq.datGenerales.monto" value="${beanRes.datGenerales.strMonto}" maxlength="17" size="17"/>
					</td>
					<td  class="text_izquierda">
						${txtFchCaptura}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="fchCaptura" name="beanReq.datGenerales.fchCaptura" value="${beanRes.datGenerales.fchCaptura}" maxlength="10" size="10"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtCveRastreo}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="cveRastreo" name="beanReq.rastreo.cveRastreo" value="${beanRes.rastreo.cveRastreo}" maxlength="30" size="30"/>
					</td>
					<td  class="text_izquierda">
						${txtCveRastreoOri}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="cveRastreoOri" name="beanReq.rastreo.cveRastreoOri" value="${beanRes.rastreo.cveRastreoOri}" maxlength="30" size="30"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtDirIP}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="direccionIp" name="beanReq.rastreo.direccionIp" value="${beanRes.rastreo.direccionIp}" maxlength="39" size="39"/>
					</td>
					<td  class="text_izquierda"><label for="beanReq.datGenerales.fechaCaptura">${txtFechaLiq}</label></td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="beanReq.datGenerales.fechaCaptura" name="beanReq.datGenerales.fechaCaptura" value="${beanRes.datGenerales.fechaCaptura}" maxlength="39" size="39"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
	
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtOrdenante}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						${txtCveInstOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="cveInstOrd" name="beanReq.llave.cveInstOrd" value="${beanRes.llave.cveInstOrd}" maxlength="12" size="12"/>
					</td>
					<td  class="text_izquierda">
						${txtTipoCtaOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="tipoCuentaOrd" name="beanReq.bancoOrd.tipoCuentaOrd" value="${beanRes.bancoOrd.tipoCuentaOrd}" maxlength="2" size="2"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtCveIntermeOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="cveIntermeOrd" name="beanReq.bancoOrd.cveIntermeOrd" value="${beanRes.bancoOrd.cveIntermeOrd}" maxlength="5" size="5"/>
					</td>
					<td  class="text_izquierda">
						${txtCodPostalord}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="codPostalOrd" name="beanReq.bancoOrd.codPostalOrd" value="${beanRes.bancoOrd.codPostalOrd}" maxlength="5" size="5"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtFchConstitOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="fchConstitOrd" name="beanReq.bancoOrd.fchConstitOrd" value="${beanRes.bancoOrd.fchConstitOrd}" maxlength="8" size="8"/>
					</td>
					<td  class="text_izquierda">
						${txtRfcOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="rfcOrd" name="beanReq.bancoOrd.rfcOrd" value="${beanRes.bancoOrd.rfcOrd}" maxlength="18" size="18"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtNumCtaOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="numCuentaOrd" name="beanReq.bancoOrd.numCuentaOrd" value="${beanRes.bancoOrd.numCuentaOrd}" maxlength="20" size="20"/>
					</td>
					<td  class="text_izquierda">
						${txtNombreOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="nombreOrd" name="beanReq.bancoOrd.nombreOrd" value="${beanRes.bancoOrd.nombreOrd}" maxlength="120" size="120"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtDomOrd}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="domicilioOrd" name="beanReq.bancoOrd.domicilioOrd" value="${beanRes.bancoOrd.domicilioOrd}" maxlength="120" size="120"/>
					</td>
				</tr>
				
			</table>
		</div>
	</div>
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtReceptor}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						${txtTipoCtaRec}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="tipoCuentaRec"  name="beanReq.bancoRec.tipoCuentaRec" value="${beanRes.bancoRec.tipoCuentaRec}" maxlength="2" size="2"/>
					</td>					
					<td  class="text_izquierda">
						${txtRfcRec}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="rfcRec" name="beanReq.bancoRec.rfcRec" value="${beanRes.bancoRec.rfcRec}" maxlength="18" size="18"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtNumCtaRec}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="numCuentaRec" name="beanReq.bancoRec.numCuentaRec" value="${beanRes.bancoRec.numCuentaRec}" maxlength="20" size="20"/>
					</td>
				
				
				<td  class="text_izquierda">
						${txtNumCtaTran}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="numCuentaTran" name="beanReq.bancoRec.numCuentaTran" value="${beanRes.bancoRec.numCuentaTran}" maxlength="20" size="20"/>
					</td>
					
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtNombreRec}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="nombreRec" name="beanReq.bancoRec.nombreRec" value="${beanRes.bancoRec.nombreRec}" maxlength="120" size="120"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
			<div class="frameBuscadorSimple">
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						${txtConceptoPago}
					</td>
					<td  class="text_izquierda">
						<input class="text_izquierda" type="text" id="conceptoPago" name="beanReq.datGenerales.conceptoPago" value="${beanRes.datGenerales.conceptoPago}" maxlength="210" size="210"/>
					</td>	
					<td  class="text_izquierda">
						
					</td>
					<td  class="text_izquierda">
					</td>					
				</tr>
			</table>
		</div>
	</div>


		

<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idGuardar" href="javascript:;">${txtGuardar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${txtGuardar}</a></td>
							</c:otherwise>
						</c:choose>	
						<td class="odd">${espacioEnBlanco}</td>
						<td class="der"><a id="idActualizar" href="javascript:;">${txtActualizar}</a></td>
						
					</tr>
					
					
					<tr>
						<td class="izq"></td>	
						<td class="odd">${espacioEnBlanco}</td>
						<td class="der"><a id="idRegresar" href="javascript:;">${txtRegresar}</a></td>
					</tr>
					
				</table>

			</div>
		</div>



		
       
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>