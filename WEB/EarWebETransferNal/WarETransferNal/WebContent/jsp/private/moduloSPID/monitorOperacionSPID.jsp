<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="muestraMonitorOpSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
       
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="spid.menuSPID.txt.txtMonitor"                   var="txtMonitor"/>
<spring:message code="spid.monitoOperativo.titulo.txtMonitorOperativo"    var="txtMonitorOperativo"/>
<spring:message code="spid.monitoOperativo.txt.txtOperLiquidadas"         var="txtOperLiquidadas"/>
<spring:message code="spid.monitoOperativo.txt.txtActualizar"             var="txtActualizar"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoInicial"           var="txtSaldoInicial"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSIACSPID"        var="txtTraspasoSIACSPID"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdRecibidasPorAplic"   var="txtOrdRecibidasPorAplic"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdRecibidasAplic"      var="txtOrdRecibidasAplic"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdRecibidasRechazadas" var="txtOrdRecibidasRechazadas"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdRecibidasPorDev"     var="txtOrdRecibidasPorDev"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIAC"        var="txtTraspasoSPIDSIAC"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdEnviadasConf"        var="txtOrdEnviadasConf"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldos"                 var="txtSaldos"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoCalculad"          var="txtSaldoCalculad"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoNoReservado"       var="txtSaldoNoReservado"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoReservado"         var="txtSaldoReservado"/>
<spring:message code="spid.monitoOperativo.txt.txtDiferencia"             var="txtDiferencia"/>
<spring:message code="spid.monitoOperativo.txt.txtDevolucionesEnvConf"    var="txtDevolucionesEnvConf"/>
<spring:message code="spid.monitoOperativo.txt.txtOperNoConfirmadas"      var="txtOperNoConfirmadas"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACEspera"  var="txtTraspasoSPIDSIACEspera"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACEnv"     var="txtTraspasoSPIDSIACEnv"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACReparar" var="txtTraspasoSPIDSIACReparar"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdEspera"              var="txtOrdEspera"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdEnviadas"            var="txtOrdEnviadas"/>
<spring:message code="spid.monitoOperativo.txt.txtOrdPorReparar"          var="txtOrdPorReparar"/>
<spring:message code="spid.monitoOperativo.txt.txtCambioEstadoA"          var="txtCambioEstadoA"/>
<spring:message code="spid.monitoOperativo.txt.txtNoReceptivo"            var="txtNoReceptivo"/>
<spring:message code="spid.monitoOperativo.txt.txtReservacionSaldos"      var="txtReservacionSaldos"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoPorReservar"       var="txtSaldoPorReservar"/>
<spring:message code="spid.monitoOperativo.txt.txtConfirmar"              var="txtConfirmar"/>
<spring:message code="spid.monitoOperativo.txt.txtDesbloquear"            var="txtDesbloquear"/>
<spring:message code="spid.monitoOperativo.txt.txtUltimaReserva"          var="txtUltimaReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtEstadoReserva"          var="txtEstadoReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtMensajeReserva"         var="txtMensajeReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtIncrementos"            var="txtIncrementos"/>
<spring:message code="spid.monitoOperativo.txt.txtDE"                     var="txtDE"/>
<spring:message code="spid.monitoOperativo.txt.txtMaxApart"               var="txtMaxApart"/>
<spring:message code="spid.monitoOperativo.txt.txtPorcentaje"             var="txtPorcentaje"/>
<spring:message code="spid.monitoOperativo.txt.txtHoraReservada"          var="txtHoraReservada"/>
<spring:message code="spid.monitoOperativo.txt.txtUsuario"                var="txtUsuario"/>
<spring:message code="spid.monitoOperativo.txt.txtActualizar"             var="txtActualizar"/>
<spring:message code="spid.monitoOperativo.txt.linkSaldosxBanco"             var="linkSaldosxBanco"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>
<spring:message code="codError.CD00169V" var="CD00169V"/>



	 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                        "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                        "ED00064V":'${ED00064V}',
                        "ED00065V":'${ED00065V}',"CD00167V":'${CD00167V}',"CD00168V":'${CD00168V}',"CD00169V":'${CD00169V}'
                        };
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/monitorOperacionSPID.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${txtModuloSPID}</span> - ${txtMonitor}
		<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.cveMiInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.fchOperacion}</span>
	</div>

	<div class="SPID">
		<div class="SPID1">
			<div class="titleBuscadorSimple">${txtOperLiquidadas}</div>
			<div class="contentBuscadorSimple">
				<table>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoInicial}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.saldoInicial}" readonly="readonly"/></td>
						<td class="tdSPID3"></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtTraspasoSIACSPID}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="traspasoSIACSPIDSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.traspasoSIACSPID}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="traspasoSIACSPIDVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.traspasoSIACSPID}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasPorAplic}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="ordRecibidasPorAplicSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.ordRecibidasPorAplic}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="ordRecibidasPorAplicVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.ordRecibidasPorAplic}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasAplic}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="ordRecibidasAplicSaldo"  type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.ordRecibidasAplic}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="ordRecibidasAplicVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.ordRecibidasAplic}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasRechazadas}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="ordRecibidasRechazadasSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.ordRecibidasRechazadas}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="ordRecibidasRechazadasVoluemn" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.ordRecibidasRechazadas}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasPorDev}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="ordRecibidasPorDevSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.ordRecibidasPorDev}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="ordRecibidasPorDevVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.ordRecibidasPorDev}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtTraspasoSPIDSIAC}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="traspasoSPIDSIACSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.traspasoSPIDSIAC}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="traspasoSPIDSIACVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.traspasoSPIDSIAC}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEnviadasConf}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="ordEnviadasConfSaldo" type="text" maxlength="17" size="17" value="${beanRes.operLiquidadasSaldo.ordEnviadasConf}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="ordEnviadasConfVolumen" type="text" maxlength="7" size="7" value="${beanRes.operLiquidadasVolumen.ordEnviadasConf}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
			
			<div class="titleBuscadorSimple">${txtSaldos}</div>
			<div class="contentBuscadorSimple">
				<table>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoCalculad}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.saldos.saldoCalculado}" readonly="readonly"/></td>
						<td rowspan="4"></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoNoReservado}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.saldos.saldoNoReservado}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoReservado}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.saldos.saldoReservado}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtDiferencia}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.saldos.diferencia}" readonly="readonly"/></td>
					</tr>
					
					<tr><td></td></tr>
					<tr><td></td></tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtDevolucionesEnvConf}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" id="devolucionesEnvConfSaldo" type="text" maxlength="17" size="17" value="${beanRes.saldos.devolucionesEnvConf}" readonly="readonly"/></td>
						<td class="tdSPID3"><input class="text_derechaSPID2" id="devolucionesEnvConfVolumen" type="text" maxlength="7" size="7" value="${beanRes.saldosVolumen.devolucionesEnvConf}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
			
			<div class="titleBuscadorSimple">${txtOperNoConfirmadas}</div>
			<div class="contentBuscadorSimple">
				<table>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtTraspasoSPIDSIACEspera}</span></td>
						<td class="tdSPID2"><input id="traspasoSPIDSIACEsperaSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.traspasoSPIDSIACEspera}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="traspasoSPIDSIACEsperaVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.traspasoSPIDSIACEspera}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtTraspasoSPIDSIACEnv}</span></td>
						<td class="tdSPID2"><input id="traspasoSPIDSIACEnvSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.traspasoSPIDSIACEnv}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="traspasoSPIDSIACEnvVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.traspasoSPIDSIACEnv}" readonly="readonly"/></td>

					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtTraspasoSPIDSIACReparar}</span></td>
						<td class="tdSPID2"><input id="traspasoSPIDSIACRepararSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.traspasoSPIDSIACReparar}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="traspasoSPIDSIACRepararVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.traspasoSPIDSIACReparar}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEspera}</span></td>
						<td class="tdSPID2"><input id="ordEsperaSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.ordEspera}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="ordEsperaVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.ordEspera}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEnviadas}</span></td>
						<td class="tdSPID2"><input id="ordEnviadasSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.ordEnviadas}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="ordEnviadasVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.ordEnviadas}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdPorReparar}</span></td>
						<td class="tdSPID2"><input id="ordPorRepararSaldo" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanRes.operNoConfirmadasSaldo.ordPorReparar}" readonly="readonly"/></td>
						<td class="tdSPID3"><input id="ordPorRepararVolumen" class="text_derechaSPID2" type="text" maxlength="7" size="7" value="${beanRes.operNoConfirmadasVolumen.ordPorReparar}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
		
		
			
			
		</div>

	</div>

	<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>							
						<td class="izq"  width="279"><a id="SaldosPorBanco" href="javascript:;">${linkSaldosxBanco}</a></td>
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td class="der"  width="279"><a id="idActualizar" href="javascript:;">${txtActualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"  width="279"><a href="javascript:;">${txtActualizar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					
				</table>

			</div>
		</div>
<form name="idForm" id="idForm" method="post" action="">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="ref" id="ref" value="">
		<input type="hidden" name="tipo" id="tipo" value="">
		
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>