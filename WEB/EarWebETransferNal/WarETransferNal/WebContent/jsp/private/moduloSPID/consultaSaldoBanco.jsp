<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraMonitor" />
</jsp:include>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloSPID.myMenu.text.consultaDeSaldosBanco" var="tituloModulo"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.consultaSaldoBanco.text.titulo" var="tituloModulo"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.banco" var="banco"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.nombre" var="nombre"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.volumenOpeEnv" var="volumenOpeEnv"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.importeOpeEnv" var="importeOpeEnv"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.volumenOpeRec" var="volumenOpeRec"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.importeOpeRec" var="importeOpeRec"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.saldo" var="saldo"/>
<spring:message code="moduloSPID.consultaSaldoBanco.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.consultaSaldoBanco.botonLink.exportar" var="exportar"/>
<spring:message code="moduloSPID.consultaSaldoBanco.botonLink.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.operacionesEnviadas" var="opeEnviadas"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.operacionesRecibidas" var="opeRecibidas"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.importe" var="importe"/>
<spring:message code="moduloSPID.consultaSaldoBanco.text.volumen" var="volumen"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>

 <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["OK00002V"] = '${OK00002V}';
		mensajes["ED00019V"] = '${ED00019V}';
		mensajes["ED00020V"] = '${ED00020V}';
		mensajes["ED00021V"] = '${ED00021V}';
		mensajes["ED00022V"] = '${ED00022V}';
		mensajes["ED00023V"] = '${ED00023V}';
		mensajes["ED00027V"] = '${ED00027V}';
    </script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/consultaSaldoBanco.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="accion" id="accion" value=""/>
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResConsultaSaldoBanco.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResConsultaSaldoBanco.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResConsultaSaldoBanco.beanPaginador.paginaFin}"/>
	<input type="hidden" name="idPeticion" id="idPeticion" value=""/>
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
				<thead>
					<tr>
						<th rowspan="2" data-id="claveBanco" width="223" class="text_centro sortField" scope="col">${banco}
							<c:if test="${sortField == 'claveBanco'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th rowspan="2" data-id="nombreBanco" width="223" class="text_centro sortField" scope="col">${nombre}
							<c:if test="${sortField == 'nombreBanco'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" class="text_centro" scope="col" colspan="2">${opeEnviadas}</th>
						<th width="223" class="text_centro" scope="col" colspan="2">${opeRecibidas}</th>
						<th rowspan="2" data-id="saldo" width="223" class="text_centro sortField" scope="col">${saldo}
							<c:if test="${sortField == 'saldo'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
					</tr>
					<tr>
						<th width="223" data-id="volumenOperacionesEnviadas" class="text_centro sortField" scope="col">${volumen}
							<c:if test="${sortField == 'volumenOperacionesEnviadas'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="importeOperacionesEnviadas" class="text_centro sortField" scope="col">${importe}
							<c:if test="${sortField == 'importeOperacionesEnviadas'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="volumenOperacionesRecibidas" class="text_centro sortField" scope="col">${volumen}
							<c:if test="${sortField == 'volumenOperacionesRecibidas'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="importeOperacionesRecibidas" class="text_centro sortField" scope="col">${importe}
							<c:if test="${sortField == 'importeOperacionesRecibidas'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th> 
					</tr>
					</thead>
					<tbody>				
						<c:forEach var="conSaBan" items="${beanResConsultaSaldoBanco.listaConsultaSaldoBanco}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td>${conSaBan.claveBanco}</td>
								<td>${conSaBan.nombreBanco}</td>
								<td>${conSaBan.volumenOperacionesEnviadas}</td>
								<td>${conSaBan.importeOperacionesEnviadas}</td>
								<td>${conSaBan.volumenOperacionesRecibidas}</td>
								<td>${conSaBan.importeOperacionesRecibidas}</td>
								<td>${conSaBan.saldo}</td>

							</tr>
						</c:forEach>
					</tbody>
					<tr>
						<td></td>
						<td>Totales</td>
						<td><input width="220" type="text" readonly="readonly" value="${beanResConsultaSaldoBanco.totalVolumenEnv}"></td>
						<td><input width="220" type="text" readonly="readonly" value="${beanResConsultaSaldoBanco.totalImporteEnv}"></td>
						<td><input width="220" type="text" readonly="readonly" value="${beanResConsultaSaldoBanco.totalVolumenRec}"></td>
						<td><input width="220" type="text" readonly="readonly" value="${beanResConsultaSaldoBanco.totalImporteRec}"></td>
						<td></td>
					</tr>
				</table>
			</div>
		<c:if test="${not empty beanResConsultaSaldoBanco.listaConsultaSaldoBanco}">
			<div class="paginador">
			<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResConsultaSaldoBanco.totalReg} registros</label>
			</div>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaIni == beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaIni != beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraConsultaSaldoBanco.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaAnt!='0' && beanResConsultaSaldoBanco.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','muestraConsultaSaldoBanco.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaAnt=='0' || beanResConsultaSaldoBanco.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResConsultaSaldoBanco.beanPaginador.pagina} - ${beanResConsultaSaldoBanco.beanPaginador.paginaFin}</label>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaFin != beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraConsultaSaldoBanco.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaFin == beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaFin != beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraConsultaSaldoBanco.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResConsultaSaldoBanco.beanPaginador.paginaFin == beanResConsultaSaldoBanco.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${importePagina}</label>
		</c:if>
		
		<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class="odd">&nbsp;</td>
							<td width="279" class="der"><a id="idRegresarMain" href="javascript:;">${Regresar}</a></td>
						</tr> 
					</table>
				</div>
			</div>
		
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>