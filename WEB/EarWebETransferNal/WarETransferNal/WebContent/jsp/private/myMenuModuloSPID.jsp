<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="mx.isban.agave.configuracion.Configuracion"%>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"           type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css"/>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css"/>

<spring:message code="moduloCDA.myMenu.text.moduloCDA" var="moduloCDA"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloPOACOA" var="moduloPOACOA" />
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI" />

<spring:message code="moduloCatalogo.myMenu.text.moduloSPID" var="moduloSPID" />
<spring:message code="moduloCatalogo.myMenu.text.moduloCatalogos" var="moduloCatalogos" />
<spring:message code="moduloCatalogo.myMenu.text.principal" var="principal" />
<spring:message code="moduloSPID.myMenu.text.envioRecepcionOperacion" var="envRecepcionOperacion" />
<spring:message code="moduloSPID.myMenu.text.monitor" var="monitorSPID" />
<spring:message code="spid.menuSPID.txt.txtMonitor" var="txtMonitor"/>
<spring:message code="spid.menuSPID.txt.txtRecepcionOperaciones"      var="txtRecepcionOperaciones"/>
<spring:message code="moduloSPID.myMenu.text.monitorBancos" var="monitorBancos" />
<spring:message code="moduloSPID.myMenu.text.recepcionDeOperaciones" var="receptOper" />
	<spring:message code="spid.menuSPID.txt.txtTraspasosFondos"           var="txtTraspasosFondos"/>
<spring:message code="moduloSPID.myMenu.text.cancelaciones" var="cancelaciones" />
<spring:message code="moduloSPID.myMenu.text.bitacoraDeEnvio" var="bitacoraEnvioSPID" />
<spring:message code="moduloSPID.myMenu.text.consultaReceptorHist" var="consrechis" />
<spring:message code="moduloSPID.myMenu.text.configuracionParametros" var="configParams" />
<spring:message code="moduloSPID.myMenu.text.mantenimientoCert" var="mantenimiento"/>
<spring:message code="moduloSPID.myMenu.text.configuracionEquivalencia" var="configEquiv" />
<spring:message code="moduloSPID.myMenu.text.configuracionTopologias" var="confTopo"/>
<spring:message code="moduloSPID.myMenu.text.recepcionDeOperaciones" var="recepOper"/>
<spring:message code="pagoInteres.myMenu.text.moduloPagoInteres" var="txtModuloPagoInteres"/>
<spring:message code="moduloCDA.myMenu.text.moduloCDASPID" var="moduloCDASPID"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloSPEI" var="moduloSPEI"/>
<spring:message code="SPEICero.formulario.title" var="moduloSPEICero"/>
<spring:message code="catalogos.menuCatalogos.txt.txtCapturas" var="capturasManuales"/>
<spring:message code="catalogos.menuCatalogos.txt.txtAdmonSaldo" var="adminSaldos"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="moduloContingencia"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<c:set var="searchString" value="${seguServicios}" />

<body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
					<li id="principal"        class="startMenuGroup">               <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">               <span>${principal}</span></a></li>
					<li id="admonSaldo"       class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/admonSaldo/admonSaldoInit.do">             <span>${adminSaldos}</span></a></li>
					<li id="capturasManuales" class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/capturasManuales/capturasManualesInit.do"> <span>${capturasManuales}</span> </a></li>
					<li id="catalogos"        class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/catalogos/catalogosInit.do">               <span>${moduloCatalogos}</span> </a></li>
					<li id="moduloCDA"        class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/moduloCDA/moduloCDAInit.do">               <span>${moduloCDA}</span> </a></li>
					<li id="moduloCDASPID"    class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/moduloCDASPID/moduloCDASPIDInit.do">       <span>${moduloCDASPID}</span></a>
					<li id="moduloPOACOA"     class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOAInit.do">         <span>${moduloPOACOASPEI}</span></a></li>
					<li id="moduloPOACOASPID"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOASPIDInit.do">         <span>${moduloPOACOASPID}</span></a></li>
					<li id="moduloSPID"       class="withSubMenus startMenuGroup">  <a href="${pageContext.servletContext.contextPath}/principal/moduloSPIDInit.do">              <span>${moduloSPID}</span></a>
						<ul>		
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraMonitorOpSPID.do')}">
									<li id="muestraMonitorOpSPID">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/muestraMonitorOpSPID.do?nomPantalla=SPID">&gt;
											<span class="subMenuText">${txtMonitor}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraMonitorOpSPID">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${txtMonitor}</span>
										</a>
									</li>	
								</c:otherwise>
						   	</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraRecepOpSPID.do')}">
									<li id="muestraRecepOpSPID">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/muestraRecepOpSPID.do">&gt;
											<span class="subMenuText">${txtRecepcionOperaciones}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraRecepOpSPID">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${txtRecepcionOperaciones}</span>
										</a>
									</li>	
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraTraspasoFondosSPID.do')}">
									<li id="muestraTraspasoFondosSPID">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/muestraTraspasoFondosSPID.do">&gt;
											<span class="subMenuText">${txtTraspasosFondos}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="muestraTraspasoFondosSPID">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${txtTraspasosFondos}</span>
										</a>
									</li>	
								</c:otherwise>
							</c:choose>		
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'monitorBancos.do')}">
									<li id="IntMonitorBancos">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/monitorBancos.do">&gt;
											<span class="subMenuText">${monitorBancos}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntMonitorBancos">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${monitorBancos}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'inicioConsultaReceptorHist.do')}">
									<li id="IntInicioConsultaReceptorHist">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/inicioConsultaReceptorHist.do">&gt;
											<span class="subMenuText">${consrechis}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntInicioConsultaReceptorHist">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${consrechis}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<%-- <li id="moduloSPID"><a 
								href="javascript:;">
								<span class="subMenuText">${recepOper}</span></a>
								<ul>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString, 'consultaRecepciones.do')}">
											<li id="IntConsultaRecepciones"><a
												href="${pageContext.servletContext.contextPath}/moduloSPID/consultaRecepciones.do">&cd;
													<span class="subMenuText">${recepOper}</span></a>
											</li>
										</c:when>
										<c:otherwise>
											<li id="IntConsultaRecepciones"><a
												href="javascript:;">
											<span class="subMenuText">${recepOper}</span></a></li>
										</c:otherwise>
									</c:choose>
									
								</ul>
							</li>							 --%>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraBitacoraDeEnvio.do')}">
									<li id="IntMuestraBitacoraDeEnvio">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/muestraBitacoraDeEnvio.do">&gt;
											<span class="subMenuText">${bitacoraEnvioSPID}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntMuestraBitacoraDeEnvio">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${bitacoraEnvioSPID}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'cancelacionPagos.do')}">
									<li id="IntCancelacionPagos">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/cancelacionPagos.do">&gt;
											<span class="subMenuText">${cancelaciones}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntCancelacionPagos">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${cancelaciones}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'mantenimientoCertificados.do')}">
									<li id="IntMantenimientoCertificados">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPID">&gt;
											<span class="subMenuText">${mantenimiento}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntMantenimientoCertificados">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${mantenimiento}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'configuracionParametros.do')}">
									<li id="IntConfiguracionParametros">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/configuracionParametros.do?nomPantalla=parametrosOperativosSPID">&gt;
											<span class="subMenuText">${configParams}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntConfiguracionParametros">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${configParams}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'configuracionTopologias.do')}">
									<li id="IntConfiguracionTopologias">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/configuracionTopologias.do">&gt;
											<span class="subMenuText">${confTopo}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntConfiguracionTopologias">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${confTopo}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'listarEquivalencias.do')}">
									<li id="IntListarEquivalencias">
										<a href="${pageContext.servletContext.contextPath}/moduloSPID/listarEquivalencias.do">&gt;
											<span class="subMenuText">${configEquiv}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="IntListarEquivalencias">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${configEquiv}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>							
						</ul>
					</li>
					<li id="moduloSPEI"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEI/moduloSPEIInit.do">             <span>${moduloSPEI}</span></a></li>
					<li id="moduloSPEICero"   class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEICero/moduloSPEICero.do">         <span>${moduloSPEICero}</span></a></li>
					<li id="pagoInteres"  class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/pagoInteres/moduloPagoInteresInit.do"> <span>${txtModuloPagoInteres}</span></a></li>
					<li id="contingencia"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/contingencia/contingenciaInit.do">         <span>${moduloContingencia}</span></a></li>
				</ul>
				<div id="menuFooter">
					<div></div>
				</div>
			</div>
		</c:if>
	</div>
	<div id="content_container">