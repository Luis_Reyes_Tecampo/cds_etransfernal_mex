<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="catParticipantesSPID" />
</jsp:include>

<script	src="${pageContext.servletContext.contextPath}/js/private/catalogos/catalogoParticipantes.js"	type="text/javascript"></script>															
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>

<spring:message code="modulo.catParticipantes.SPID.titulo"       var="titulo"/>
<spring:message code="modulo.catParticipantes.SPID.clave"       var="clave_inter"/>
<spring:message code="modulo.catParticipantes.SPID.tituloP"       var="tituloP"/>
<spring:message code="modulo.catParticipantes.SPID.boton.buscar"       var="buscar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.baja"       var="baja"/>
<spring:message code="modulo.catParticipantes.SPID.boton.limpiar"       var="limpiar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.agregar"       var="agregar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.actualizar"       var="actualizar"/>
<spring:message code="modulo.catParticipantes.SPID.col1"       var="col1"/>
<spring:message code="modulo.catParticipantes.SPID.col2"       var="col2"/>
<spring:message code="modulo.catParticipantes.SPID.col3"       var="col3"/>
<spring:message code="modulo.catParticipantes.SPID.col4"       var="col4"/>
<spring:message code="modulo.catParticipantes.SPID.col5"       var="col5"/>

<spring:message code="modulo.catParticipantes.SPID.cve"       var="cve"/>
<spring:message code="modulo.catParticipantes.SPID.banco"       var="banco"/>
<spring:message code="modulo.catParticipantes.SPID.nombre"       var="nombre"/>
<spring:message code="modulo.catParticipantes.SPID.fecha"       var="fecha"/>

<spring:message code="general.limpiar" var="limpiar" />
<spring:message code="general.guardar" var="guardar" />
<spring:message code="general.cancelar" var="cancelar" />
<spring:message code="general.alta" var="alta" />
<spring:message code="general.modificar" var="modificar" />
<spring:message code="general.eliminar" var="eliminar" />
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>

<spring:message code="codError.ED00117V" var="mensajeInter1"/>
<spring:message code="codError.ED00120V" var="mensajeInter2"/>
<spring:message code="codError.ED00121V" var="mensajeInter3"/>
<spring:message code="codError.ED00122V" var="mensajeInter4"/>


<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span>-${titulo}
</div>

<div class="frameBuscadorSimple" >
	<div class="titleBuscadorSimple">${tituloP}
	</div>
	<form name="formAltaPartSPID" id="formAltaPartSPID" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value="" />
		
		<div class="contentBuscadorSimple" style="width:99.9%">
			<table width="100%" border="0">
				<tr>
					<td width="50%" class="text_izquierda">${cve}</td>
					<td> 
						<input  name="cveInter" type="text" class="Campos" id="cveInter" size="15" maxlength="5"  value="${cveInter}" onkeypress="return validaDatos(event)"/>										
					</td>
				</tr>
				<tr>
					<td width="50%" class="text_izquierda">${banco} </td>
					<td> 
						<input  name="numBanco" type="text" class="Campos" id="numBanco" size="15" maxlength="12"   readonly="readonly" value="${numBanco}" />										
					</td>
				</tr>
				<tr>
					<td width="50%" class="text_izquierda">${nombre} </td>
					<td> 
						<input  name="nombreLargo" type="text" class="Campos" id="nombreLargo" size="40" maxlength="40"  readonly="readonly" value="${nombreLargo}" />										
					</td>
				</tr>
				<tr>
					<td width="50%" class="text_izquierda">${fecha} </td>
					<td> 
						<input  name="datoFecha" type="text" class="Campos" id="datoFecha" size="15" maxlength="16"   readonly="readonly" value="${datoFecha}" />
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						<script type="text/javascript">
							createCalendarGuion('datoFecha','cal1');
						</script>										
					</td>
				</tr>		
			</table>
			<table  width="100%" border="0">
				<tr>
					<td class="izq">													
						<span><a id="idLimpiar" href="javascript:limpiarParticipantes();">${limpiar}</a></span>
					</td>							
				</tr>
			</table>
		</div>
		
		<div class="framePieContenedor">
		<div class="contentPieContenedor">
		<table>
			<tr>
				<td width="279" class="izq"><a id="idAceptar" href="javascript:guardaParticipantes();">${guardar}</a></td>
				<td width="279" class="der"><a id="idCancelar" href="javascript:cancelar();">${cancelar}</a></td>	
			</tr>
		</table>
		
		</div>
		</div>
			
	</form>
	
</div>
<jsp:include page="../myFooter.jsp" flush="true" />

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
		
		var respuesta = '${msgError}';
		if (respuesta === "valida"){
			var mensaje = '${msj}';
				   
		    jInfo("Info", jqueryInfo, "", "1.- ${mensajeInter1}  <br>  "+
				   "2.- ${mensajeInter2} <br> "+
				   "3.- ${mensajeInter3} <br> "+
				   " ${mensajeInter4} ", "");
		}else{		
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		}
		</script>
</c:if>