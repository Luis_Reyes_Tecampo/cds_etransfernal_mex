<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td  class="izq widthx279"><a class="btnSubMenu"
									id="btnAgregar"
									href="javascript:;">${lblAgregar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${lblAgregar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd widthx6" >${espacioEnBlanco}</td>
						<td class="der widthx279"><a id="btnActualizar"
							href="javascript:;">${lblActualizar}</a></td>
					</tr>
					<tr>
						<c:choose>
							<c:when
								test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanBloques.bloques}">
								<td  class="izq widthx279"><a id="btnExportar"
									href="javascript:;">${lblExportar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${lblExportar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd widthx6">${espacioEnBlanco}</td>

						<c:choose>
							<c:when
								test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanBloques.bloques}">
								<td  class="der widthx279"><a id="btnEditar"
									href="javascript:;">${lblEditar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="der_Des widthx279"><a href="javascript:;">${lblEditar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when
								test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanBloques.bloques}">
								<td  class="izq widthx279"><a id="btnExportarTodo"
									href="javascript:;">${lblExportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${lblExportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd widthx6" >${espacioEnBlanco}</td>
						<%@ include file="panelAfectacionBloquesAux.jsp" %>
					</tr>
				</table>
			</div>
		</div>