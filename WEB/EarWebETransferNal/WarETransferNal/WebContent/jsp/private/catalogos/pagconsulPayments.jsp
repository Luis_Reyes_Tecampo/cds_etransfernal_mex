<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>


<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<div class="paginador">
	<c:if test="${beanConsultasPayme.beanPaginador.paginaIni == beanConsultasPayme.beanPaginador.pagina}"> <a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaIni != beanConsultasPayme.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('mform','consultaPayments.do','INI');">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaAnt!='0' && beanConsultasPayme.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('mform','consultaPayments.do','ANT');">&lt;${anterior}</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaAnt=='0' || beanConsultasPayme.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
	<label id="txtPagina">${e:forHtml(beanConsultasPayme.beanPaginador.pagina)} - ${e:forHtml(beanConsultasPayme.beanPaginador.paginaFin)}</label>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaFin != beanConsultasPayme.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('mform','consultaPayments.do','SIG');">${siguiente}&gt;</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaFin == beanConsultasPayme.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaFin != beanConsultasPayme.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('mform','consultaPayments.do','FIN');">${fin}&gt;&gt;</a></c:if>
	<c:if test="${beanConsultasPayme.beanPaginador.paginaFin == beanConsultasPayme.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a> </c:if>
</div>

