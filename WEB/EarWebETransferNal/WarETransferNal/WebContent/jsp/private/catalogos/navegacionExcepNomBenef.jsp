<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanExcNomBenef.listaNombres}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanPaginador.regIni} al
						${beanPaginador.regFin} de un total de
						${totalReg} registros</label>
				</div>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaIni == beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaIni != beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepNomBeneficiarios.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaAnt!='0' && beanExcNomBenef.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepNomBeneficiarios.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaAnt=='0' || beanExcNomBenef.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanPaginador.pagina}
					- ${beanPaginador.paginaFin}</label>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaFin != beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepNomBeneficiarios.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaFin == beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaFin != beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepNomBeneficiarios.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanExcNomBenef.beanPaginador.paginaFin == beanExcNomBenef.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>