<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="catalogos.mantoMedios.txt.txtClaveMedioEntrega" var="ClaveMedioEntrega"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescripcionTrasfer" var="DescripcionTrasfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescripcionMedioE" var="DescripcionMedioE"/>
<spring:message code="catalogos.mantoMedios.txt.txtDosPuntosHelper" var="DosPuntosHelper"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveTransfer" var="ClaveTransfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveOperacion" var="ClaveOperacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtCLACON" var="CLACON"/>
<spring:message code="catalogos.mantoMedios.txt.txtSucursalOperan" var="SucursalOperan"/>
<spring:message code="catalogos.mantoMedios.txt.txtHorarioMay" var="HorarioMay"/>
<spring:message code="catalogos.mantoMedios.txt.txtHorarioMin" var="HorarioMin"/>
<spring:message code="catalogos.mantoMedios.txt.txtInicio" var="Inicio"/>
<spring:message code="catalogos.mantoMedios.txt.txtCierre" var="Cierre"/>
<spring:message code="catalogos.mantoMedios.txt.txtInhab" var="Inhab"/>
<spring:message code="catalogos.mantoMedios.txt.txtCLACONTEF" var="CLACONTEF"/>
<spring:message code="catalogos.mantoMedios.txt.txtMecanismo" var="Mecanismo"/>
<spring:message code="catalogos.mantoMedios.txt.txtLimiteImporte" var="LimiteImporte"/>
<spring:message code="catalogos.mantoMedios.txt.txtCanal" var="Canal"/>
<spring:message code="catalogos.mantoMedios.txt.txtBuscar" var="Buscar"/>
<spring:message code="catalogos.mantoMedios.txt.txtAbierto" var="Abierto"/>
<spring:message code="catalogos.mantoMedios.txt.txtCerrado" var="Cerrado"/>
<spring:message code="catalogos.mantoMedios.txt.txtSi" var="si"/>
<spring:message code="catalogos.mantoMedios.txt.txtNo" var="no"/>
<spring:message code="general.verdetalle" var="verdetalle"/>
<spring:message code="general.exportar" var="exportar"/>
<spring:message code="general.exportarAll" var="exportarAll"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.agregar" var="newRegistro"/>
<spring:message code="general.eliminar" var="eliminar"/>
<spring:message code="general.limpiar" var="limpiar"/>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="catalogos.menuCatalogos.txt.txtMantoMedios" var="tituloFuncionalidad"/>
<spring:message code="general.inicio" var="lblInicio"/>
<spring:message code="general.anterior" var="lblAnterior"/>
<spring:message code="general.siguiente" var="lblSiguiente"/>
<spring:message code="general.fin" var="lblFin"/>
<spring:message code="catalogos.mantoMedios.txt.txtSeguro" var="Seguro"/>
<spring:message code="catalogos.mantoMedios.txt.txtNoSeguro" var="NoSeguro"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="catalogos.parametrosPOACOA.text.actualizar" var="Actualizar"/>

<spring:message code="catalogos.mantoMedios.txt.txtCMedioEntrega" var="CMedioEntrega"/>
<spring:message code="catalogos.mantoMedios.txt.txtCTransfer" var="CTransfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtCOperacion" var="COperacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtLimImporte" var="LimImporte"/>
<spring:message code="catalogos.mantoMedios.txt.txtInhabPregunta" var="InhabPregunta"/>
<spring:message code="catalogos.mantoMedios.txt.txtNivel" var="Nivel"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescOperacion" var="DescOperacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescripcionTrasfer" var="descTrasferencia"/>

<spring:message code="general.EditRegistro" var="editar"/>

<spring:message code="catalogos.mantoMedios.accion.seguro" var="seguro"/>
<spring:message code="catalogos.mantoMedios.accion.noSeguro" var="noSeguro"/>


<jsp:include page="../myHeader.jsp" flush="true"/> 
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="mantenimientoMediosAutorizados" />
</jsp:include>
 
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" >
 <script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
 <script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mttoMediosAutorizados.js" type="text/javascript"></script>


<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00068V" var="ED00068V" />
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"OK00000V":'${OK00000V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}'};</script>

<c:set var="searchString" value="${seguTareas}"/>
    
<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>



<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  	<input type="hidden" name="paginaIni" id="paginaIni" value="${beanMttoMediosAutorizadosRes.beanPaginador.paginaIni}">
	<input type="hidden" name="pagina" id="pagina" value="${beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin}">
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="opcion" id="opcion" value="">
	<input type="hidden" name="opcionDto" id="opcionDto" value="">
	<input type="hidden" name="dtosBusqueda" id="dtosBusqueda" value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.dtosBusqueda}">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	<input type="hidden" name="field" id="field" value="${field}">
	<input type="hidden" name="fieldValue" id="fieldValue" value="${fieldValue}">
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden"> 	
				<table id="tablaDatosListado" name="tablaDatosListado">
					<caption></caption>
					<tr>
						
						<th class="text_centro filField" scope="col" rowspan="2">${CMedioEntrega}
						<div>
							<label for="chkTodos"></label>
							<input name="chkTodos" id="chkTodos" type="checkbox"  style=" width:20px"/>
							<label for="busClaveMedioE"></label>
							<input type="text" id="busClaveMedioE"  name="cveMedioEnt" data-field="CVE_MEDIO_ENT" class="filField" size="7" maxlength="5" style="width:80px" value="${beanMttoMediosAutorizados.cveMedioEnt}"/>
						</div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${DescripcionMedioE}
						<div>
						<label for="busDescripcionMedioE"></label>
						<input type="text" id="busDescripcionMedioE" name="desCveMed" data-field="DESCRIPCION" class="filField" size="9" maxlength="30" style="width:130px" value="${beanMttoMediosAutorizados.desCveMed}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${CTransfer}
						<div>
						<label for="busClaveTransfer"></label>
						<input type="text" id="busClaveTransfer" name="cveTransfe" data-field="CVE_TRANSFE" class="filField" size="7" maxlength="3" style="width:90px" value="${beanMttoMediosAutorizados.cveTransfe}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${descTrasferencia}
						<div>
						<label for="busDescrTransf"></label>
						<input type="text" id="busDescrTransf" name="descTrasferencia" data-field="DESCRIPCION" class="filField" size="9" maxlength="60" style="width:130px" value="${beanMttoMediosAutorizados.descTrasferencia}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${COperacion}
						<div>
						<label for="busClaveOperacion"></label>
						<input type="text" id="busClaveOperacion" name="cveOperacion" data-field="CVE_OPERACION" class="filField" size="5" maxlength="8" style="width:80px" value="${beanMttoMediosAutorizados.cveOperacion}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${DescOperacion}
						<div>
						<label for="busDescOperacion"></label>
						<input type="text" id="busDescOperacion" name="descOperacion" data-field="DESCRIPCION" class="filField" size="8" maxlength="60" style="width:130px" value="${beanMttoMediosAutorizados.descOperacion}" /></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${CLACON}
						<div>
						<label for="busclacon"></label>
						<input type="text" id="busclacon" name="cveCLACON" data-field="CVE_CLACON" class="filField" size="3" maxlength="4" style="width:60px" value="${beanMttoMediosAutorizados.cveCLACON}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${CLACONTEF}
						<div>
						<label for="busClacontef"></label>
						<input type="text" id="busClacontef" name="cveCLACONTEF" data-field="CVE_CLACON_TEF" class="filField" size="5" maxlength="4" style="width:70px" value="${beanMttoMediosAutorizados.cveCLACONTEF}"/></div>
						</th>
						
						<th class="text_centro filField" scope="col" rowspan="2">${SucursalOperan}
						<div>
						<label for="buScuOpera"></label>
						<input type="text" id="buScuOpera" name="sucOperante" data-field="SUC_OPERANTE" class="filField" size="5" maxlength="7" style="width:100px" value="${beanMttoMediosAutorizados.sucOperante}"/></div>
						</th> 
						<th class="text_centro filField" scope="col" colspan="3">${HorarioMay}</th>
						<th class="text_centro filField" scope="col" rowspan="2">${InhabPregunta}
						<div>
						<label for="busInhabil"></label>
						<select id="busInhabil" name="flgInHab" data-field="flg_inhab" class="enfocaCombo" style="width:100px">
							<option></option>
							<option value="1"  ${beanMttoMediosAutorizados.flgInHab eq 1 ? "selected" : ""}>${si}</option>
							<option value="0" ${beanMttoMediosAutorizados.flgInHab eq 0 ? "selected" : ""}>${no}</option>
						</select></div>
						</th>			
						<th class="text_centro filField" scope="col" rowspan="2">${Mecanismo}
						<div>
						<label for="busMecanismo"></label>
						<input type="text" id="busMecanismo" name="cveMecanismo" data-field="mecanismo" class="filField" size="3" maxlength="8" style="width:80px" value="${beanMttoMediosAutorizados.cveMecanismo}"/></div>
						</th>
						<th class="text_centro filField" scope="col" rowspan="2">${LimImporte}
						<div>
						<label for="busImporte"></label>
						<input type="text" id="busImporte" name="limiteImporte" data-field="IMPORTE_MAX" class="filField" size="3" maxlength="39" style="width:80px" value="${beanMttoMediosAutorizados.limiteImporte}"/></div>
						</th>
						<th  class="text_centro filField widthx125" scope="col" rowspan="2">${Nivel}
						<div>
						<label for="busNivel"></label>
						<select id="busNivel" name="canal" data-field="SEGURO" class="enfocaCombo" style="width:100px">
							<option></option>
							<option value="1" ${beanMttoMediosAutorizados.canal eq 1 ? "selected" : ""}>${Seguro}</option>
							<option value="0" ${beanMttoMediosAutorizados.canal eq 0 ? "selected" : ""}>${NoSeguro}</option>
						</select></div>
						</th>
					</tr>
					<tr>
						<th  scope="col">${Inicio}</th>
						<th scope="col">${Cierre}</th>
						<th  class="widthx125"  scope="col">${HorarioMin}</th>
					</tr>
					<tbody>	
					<c:if test="${beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto ne null}">
						<c:forEach var="datos" items="${beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}" varStatus="rowCounter">
							<tr class="odd1">
							<td class="text_izquierda" >
								<label for="seleccionado${rowCounter.index}"></label>				
								<input type="checkbox" name="listaBeanMttoMedAuto[${rowCounter.index}].seleccionado" 
								onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}"  />  ${datos.cveMedioEnt} 
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveTransfe" id="cveTransfe"  value="${datos.cveTransfe}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveOperacion" id="cveOperacion"  value="${datos.cveOperacion}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveMecanismo" id="cveMecanismo"  value="${datos.cveMecanismo}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveMedioEnt" id="cveMedioEnt"  value="${datos.cveMedioEnt}" />								
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].desCveMed" id="desCveMed"  value="${datos.desCveMed}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].descTrasferencia" id="descTrasferencia"  value="${datos.descTrasferencia}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].descOperacion" id="descOperacion"  value="${datos.descOperacion}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveCLACON" id="cveCLACON"  value="${datos.cveCLACON}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].cveCLACONTEF" id="cveCLACONTEF"  value="${datos.cveCLACONTEF}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].sucOperante" id="sucOperante"  value="${datos.sucOperante}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].horaInicio" id="horaInicio"  value="${datos.horaInicio}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].horaCierre" id="horaCierre"  value="${datos.horaCierre}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].horarioEstatus" id="horarioEstatus"  value="${datos.horarioEstatus}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].flgInHab" id="flgInHab"  value="${datos.flgInHab}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].limiteImporte" id="limiteImporte"  value="${datos.limiteImporte}" />
								<input type="hidden" name="listaBeanMttoMedAuto[${rowCounter.index}].canal" id="canal"  value="${datos.canal}" />
							
							</td>
							
							<td class="text_izquierda">${datos.desCveMed}</td>
							<td class="text_centro">${datos.cveTransfe}</td>
							<td class="text_izquierda">${datos.descTrasferencia}</td>
							<td class="text_izquierda">${datos.cveOperacion}</td>
							<td class="text_izquierda">${datos.descOperacion}</td>
							<td class="text_centro">${datos.cveCLACON}</td>
							<td class="text_centro">${datos.cveCLACONTEF}</td>
							<td class="text_centro">${datos.sucOperante}</td>
							<td class="text_centro">${datos.horaInicio}</td>
							<td class="text_centro">${datos.horaCierre}</td>
							<td class="text_centro"><c:if test="${datos.horarioEstatus eq 'A'}">${Abierto}</c:if><c:if test="${datos.horarioEstatus eq 'H'}">${Cerrado}</c:if></td>
							<td class="text_centro"><c:if test="${datos.flgInHab eq '1'}">${si}</c:if><c:if test="${datos.flgInHab eq '0'}">${no}</c:if><c:if test="${datos.flgInHab eq '-1'}">${no}</c:if></td>				
							<td class="text_izquierda">${datos.cveMecanismo}</td>
							<td class="text_centro"><c:if test="${datos.limiteImporte eq '-1.0'}"></c:if> <c:if test="${datos.limiteImporte ne '-1.0'}">${datos.limiteImporte}</c:if></td>
							<td class="text_izquierda"><c:if test="${datos.canal eq '1'}">${Seguro}</c:if><c:if test="${datos.canal eq '0'}">${NoSeguro}</c:if><c:if test="${datos.canal eq '-1'}">${NoSeguro}</c:if></td>
							</tr>			
						</c:forEach>
					</c:if>
					</tbody>
				</table>
		</div>
		<div>
			<c:if test="${not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanMttoMediosAutorizadosRes.totalReg} registros</label>
					</div>
					<c:if test="${!beanMttoMediosAutorizadosRes.filtros}">
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaIni == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaIni != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mantenimientoMediosAutorizados.do','INI');" onKeyPress="nextPage('idForm','mantenimientoMediosAutorizados.do','INI');">&lt;&lt;${inicio}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt!='0' && beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt!=null}">
						<a href="javascript:;" onclick="nextPage('idForm','mantenimientoMediosAutorizados.do','ANT');" onKeyPress="nextPage('idForm','mantenimientoMediosAutorizados.do','ANT');">&lt;${anterior}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt=='0' || beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt==null}">
						<a href="javascript:;">&lt;${anterior}</a></c:if>
						<label id="txtPagina">${beanMttoMediosAutorizadosRes.beanPaginador.pagina} - ${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin}</label>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mantenimientoMediosAutorizados.do','SIG');" onKeyPress="nextPage('idForm','mantenimientoMediosAutorizados.do','SIG');">${siguiente}&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mantenimientoMediosAutorizados.do','FIN');" onKeyPress="nextPage('idForm','mantenimientoMediosAutorizados.do','FIN');">${fin}&gt;&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
					</c:if>
					<c:if test="${beanMttoMediosAutorizadosRes.filtros}">
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaIni == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaIni != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mttoMediosAutorizados.do','INI');" onKeyPress="nextPage('idForm','mttoMediosAutorizados.do','INI');">&lt;&lt;${inicio}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt!='0' && beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt!=null}">
						<a href="javascript:;" onclick="nextPage('idForm','mttoMediosAutorizados.do','ANT');" onKeyPress="nextPage('idForm','mttoMediosAutorizados.do','ANT');">&lt;${anterior}</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt=='0' || beanMttoMediosAutorizadosRes.beanPaginador.paginaAnt==null}">
						<a href="javascript:;">&lt;${anterior}</a></c:if>
						<label id="txtPagina">${beanMttoMediosAutorizadosRes.beanPaginador.pagina} - ${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin}</label>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mttoMediosAutorizados.do','SIG');"  onKeyPress="nextPage('idForm','mttoMediosAutorizados.do','SIG');">${siguiente}&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin != beanMttoMediosAutorizadosRes.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','mttoMediosAutorizados.do','FIN');" onKeyPress="nextPage('idForm','mttoMediosAutorizados.do','FIN');">${fin}&gt;&gt;</a></c:if>
						<c:if test="${beanMttoMediosAutorizadosRes.beanPaginador.paginaFin == beanMttoMediosAutorizadosRes.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
						
					</c:if>
				</div>
			</c:if>
		</div>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
				<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td  class="izq widthx279"><a id="idNuevoRegistro" href="${pageContext.servletContext.contextPath}/catalogos/mantenimientoMediosAutorizadosNuevo.do" >${newRegistro}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a  href="javascript:;" >${newRegistro}</a></td>
							</c:otherwise>
						</c:choose>
						<td  class="odd widthx6"> &nbsp;</td>				
						<td  class="der widthx279"><a id="idActualizar" href="javascript:;" >${Actualizar}</a></td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
									<td  class="izq widthx279"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
									<td  class="izq_Des widthx279"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>				
						<td  class="odd widthx6"> &nbsp;</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
								<td  class="der widthx279"><a id="idEditar" href="javascript:;" >${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="der_Des widthx279"><a  href="javascript:;" >${editar}</a></td>
							</c:otherwise>
						 </c:choose>
					</tr>
					<tr>			
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
								<td  class="izq widthx279"><a id="idExportarTodo" href="${pageContext.servletContext.contextPath}/catalogos/exportaMttoAutorizados.do">${exportarAll}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${exportarAll}</a></td>
							</c:otherwise>
						</c:choose>
						<td  class="odd widthx6"> &nbsp;</td>				
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
								<td  class="der widthx279"><a id="idEliminar" href="javascript:;" >${eliminar}</a></td>
							</c:when>
							<c:otherwise>
							<td  class="der_Des widthx279"><a href="javascript:;" >${eliminar}</a></td>
							</c:otherwise>
						</c:choose>		
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
									<td  class="izq widthx279"><a id="idMedioSeguro" href="javascript:;">${seguro}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a  href="javascript:;" >${seguro}</a></td>
							</c:otherwise>
						</c:choose>
						<td  class="odd widthx6"> &nbsp;</td>				
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanMttoMediosAutorizadosRes.listaBeanMttoMedAuto}">
								<td  class="der widthx279"><a id="idMedioNoSeguro" href="javascript:;">${noSeguro}</a></td>
							</c:when>
							<c:otherwise>
							<td  class="der_Des widthx279"><a href="javascript:;">${noSeguro}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
			    </table>
			</div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>