<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="afectacionBloques" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/afectacionBloques.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<%--Inicio de llamado de datos de Properties--%>
<spring:message code="catalogos.menuCatalogos.txt.txtModuloSPEI" var="tituloModulo"/>
<spring:message code="general.nombreAplicacion" 						var="app"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.eliminar" 								var="lblEliminar"/>
<spring:message code="general.guardar" 									var="lblGuardar"/>
<spring:message code="general.cancelar" 								var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.asteriscoHelper"                          var="asteriscoHelper" />
<spring:message code="general.dosPuntosHelper"                          var="DosPuntosHelper" />
<spring:message code="catalogos.certificadosCifrado.alta" 				var="alta"/>
<spring:message code="catalogos.certificadosCifrado.modificar" 			var="modificar"/>
<spring:message code="catalogos.afectacionBloques.titulo" var="afectacionBloques"/>
<spring:message code="catalogos.afectacionBloques.lblNumBloque"var="lblNumBloque" />
<spring:message code="catalogos.afectacionBloques.lblValor"var="lblValor" />
<spring:message code="catalogos.afectacionBloques.lblDescripcion"var="lblDescripcion" />
<spring:message code="catalogos.afectacionBloques.lblAgregar"var="lblAgregar" />
<spring:message code="catalogos.afectacionBloques.lblActualizar"var="lblActualizar" />
<spring:message code="catalogos.afectacionBloques.lblExportar"var="lblExportar" />
<spring:message code="catalogos.afectacionBloques.lblExportarTodo"var="lblExportarTodo" />
<spring:message code="catalogos.afectacionBloques.lblEditar"var="lblEditar" />
<spring:message code="catalogos.afectacionBloques.lblEliminar"var="lblEliminar" />
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="catalogos.afectacionBloques.titulo"var="tituloFuncionalidad" />
<spring:message code="catalogos.afectacionBloques.lblAgregarView"var="lblAgregarView" />
<spring:message code="catalogos.afectacionBloques.lblEditarView"var="lblEditarView" />
<%-- Fin de Variables Constantes --%>

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00130V" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',"ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}'  };</script>	
 <form id="mform" method="post">
 	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
 	<input type="hidden" name="accion" id="accion" value="${accion}">
 	<input type="hidden" name="canal" id="canal" value="${beanCertificadoCifrado.canal}" ${accion eq 'alta' ? 'disabled="disabled"' : ''}>
	<div class="pageTitleContainer">
	 <c:choose>
	  <c:when test="${accion eq 'alta'}">
	  <span class="pageTitle">${tituloModulo}</span> - ${lblAgregarView} ${tituloFuncionalidad}
	  </c:when>
	  <c:otherwise>
	  <span class="pageTitle">${tituloModulo}</span> - ${lblEditarView} ${tituloFuncionalidad}
	  </c:otherwise>
	 </c:choose>
		
	</div>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblNumBloque}${DosPuntosHelper}
			    </label>
			    <c:choose>
			    <c:when test="${accion eq 'alta'}">
			    <label for="paramNumBloque"></label>
			   		<input type="text" id="paramNumBloque" name="numBloque" maxlength="5" value="${beanBloque.numBloque}" class="onlyNumero objFrm Campos required" />
			   		</c:when>
			   		<c:otherwise>
			   		<label for="paramNumBloque"></label>
			   			<input type="text" id="paramNumBloque" name="numBloque" disabled maxlength="5" value="${beanBloque.numBloque}" class="Campos" />
			   			<input type="hidden" id="paramNumBloque" name="numBloque" readonly maxlength="5" value="${beanBloque.numBloque}" class="Campos" />
			   		</c:otherwise>
			   		</c:choose>
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblValor}${DosPuntosHelper}
			    </label>
			    <label for="paramValor"></label>
			    <input type="text" name="valor" id="paramValor" maxlength="30" value="${beanBloque.valor}" class="letrasNum objFrm Campos required" />
			    <c:if test="${accion eq 'modificar'}">
			    	 <input type="hidden" name="valorOld" id="paramValorOld" maxlength="30" value="${beanBloque.valor}" class="" />
			   	</c:if>
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        ${lblDescripcion}${DosPuntosHelper}
			    </label>
			    <label for="paramDescripcion"></label>
			    <input type="text" name="descripcion" id="paramDescripcion" size="60" maxlength="60" value="${beanBloque.descripcion}" class="letrasNum objFrm Campos" />
			</div>
			
	    </div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		        <caption></caption>
		             <tr>
		                 <td class="izq">
		                     <a id="btnGuardar" href="javascript:;" >${lblGuardar}</a>
		                 </td>
		                 <td class="odd">${espacioEnBlanco}</td>
		                 <td class="der">
		                     <a id="btnReturn" href="javascript:;" >${lblRegresar}</a>
		                 </td>
		             </tr>
		        </table>
		    </div>
		</div>
	</div>
	<input type="hidden" id="numBloqueInput" name="numBloqueF" class="filField" data-field="NUM_BLOQUE" maxlength="5" value="${beanFilter.numBloque}">
	<input type="hidden" id="valorInput" name="valorF" data-field="VALOR" class="filField" maxlength="30" value="${beanFilter.valor}" />
	<input type="hidden" id="descripcionInput" name="descripcionF" data-field="DESCRIPCION" class="filField" maxlength="60" value="${beanFilter.descripcion}" />
	<input type="hidden" name="pagina" id="pagina" value="${beanPaginador.pagina}" /> 
	<input type="hidden" name="paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}" /> 
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}" />
</form>

<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>