<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<tr>
	<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblMedioEntrega}${DosPuntosHelper}</td>
	<td><label for="paramMedioEntrega"></label> <select
		class="Campos comboDes required objFrm focusCombo comboWidth"
		id="paramMedioEntrega" name="medioEntrega">
			<option value="">${na}</option>
			<c:forEach var="reg" items="${combos.mediosEntrega}">
				<option value="${reg.cve}"
					${reg.cve eq beanNotificacion.medioEntrega?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
			</c:forEach>
	</select> <c:if test="${accion eq 'modificar'}">
			<label for="medioEntregaOld"></label>
			<input type="hidden" name="medioEntregaOld"
				value="${beanNotificacion.medioEntrega}" />
		</c:if></td>
</tr>

<tr>
	<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblEstatus}${DosPuntosHelper}</td>
	<td><label for="paramEstatus"></label> <select
		class="Campos comboDes required objFrm focusCombo comboWidth" id="paramEstatus"
		name="estatusTransfer">
			<option value="">${na}</option>
			<c:forEach var="reg" items="${combos.estatus}">
				<option value="${reg.cve}"
					${reg.cve eq beanNotificacion.estatusTransfer?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
			</c:forEach>
	</select> <c:if test="${accion eq 'modificar'}">
			<label for="estatusTransferOld"></label>
			<input type="hidden" name="estatusTransferOld"
				value="${beanNotificacion.estatusTransfer}" />
		</c:if></td>
</tr>