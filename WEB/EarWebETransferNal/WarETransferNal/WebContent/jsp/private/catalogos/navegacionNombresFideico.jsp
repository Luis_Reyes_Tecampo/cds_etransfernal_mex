<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanNombres.nombres}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanNombres.beanPaginador.regIni} al
						${beanNombres.beanPaginador.regFin} de un total de
						${beanNombres.totalReg} registros</label>
				</div>
				<c:if
					test="${beanNombres.beanPaginador.paginaIni == beanNombres.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaIni != beanNombres.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNombresFideicomiso.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaAnt!='0' && beanNombres.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNombresFideicomiso.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaAnt=='0' || beanNombres.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanNombres.beanPaginador.pagina}
					- ${beanNombres.beanPaginador.paginaFin}</label>
				<c:if
					test="${beanNombres.beanPaginador.paginaFin != beanNombres.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNombresFideicomiso.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaFin == beanNombres.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaFin != beanNombres.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNombresFideicomiso.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanNombres.beanPaginador.paginaFin == beanNombres.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>