<%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/> 
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="ctaLiquidezIntradia" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script
	src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/global.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/utilerias.js"
	type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/notificacionLiquidez.js" type="text/javascript"></script>


 
<%-- Inicio de llamado de datos de Properties --%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="catalogos.notificacionLiquidezIn.lbl.tituloFuncionalidad"var="tituloFuncionalidad" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco" />
<%-- Opciones de la Pantalla --%>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.tipoConsulta" var="lblTipoNot"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveTrasfe" var="lblClaveTrans"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveMecanismo" var="lblMedioEntrega"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveEstatus" var="lblEstatus"/>

<spring:message code="general.exportar" var="lblExportar"/>
<spring:message code="general.exportarAll" var="lblExportarTodo"/>
<spring:message code="general.newRegistro" var="lblAgregar"/>
<spring:message code="general.EditRegistro" var="lblEditar"/>
<spring:message code="general.Actualizar" var="lblActualizar"/>
<spring:message code="general.eliminar" var="lblEliminar"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.buscar" var="buscar"></spring:message>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.gfi" var="gfi"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.trfplus" var="trfplus"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.na" var="na"/> 
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<%-- Constantes de Error --%>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>

<%-- Fin de Variables Constantes --%>
<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"ED00065V":'${ED00065V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};</script>
<%-- Fin de Variables Constantes --%>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>
<c:set var="searchString" value="${seguTareas}" />
<form id="idForm" name="idForm" method="post" action="">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="pagina" id="pagina"
		value="${beanLiquidez.beanPaginador.pagina}"> <input
		type="hidden" name="paginaIni" id="paginaIni"
		value="${beanLiquidez.beanPaginador.paginaIni}"> <input
		type="hidden" name="paginaFin" id="paginaFin"
		value="${beanLiquidez.beanPaginador.paginaFin}"> <input
		type="hidden" name="field" id="field"
		value="${beanLiquidez.beanFilter.field}"> <input
		type="hidden" name="fieldValue" id="fieldValue"
		value="${beanLiquidez.beanFilter.fieldValue}"> <input
		type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="filtro" id="filtro" value="">
		<input type="hidden" name="tipoConsulta" id="tipoConsulta" value="${tipoConsulta}">
 
<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
			<caption></caption>
				<tbody>
					<tr>
						<td class="text_izquierda">
						${lblTipoNot}
						</td><span class="asterisco">${asteriscoHelper}</span>
						<td>
						<label for="tipoConsultaAccion"></label>
							<select class="Campos comboDes required objFrm" id="tipoConsultaAccion" name="tipoConsultaAccion">
							
									<option value="">${na}</option>
									<option value="NA" ${'NA' eq tipoConsulta ?"selected":""}>${gfi}</option>
									<option value="TI" ${'TI' eq tipoConsulta ?"selected":""}>${trfplus}</option>
									
									 
							</select>
							&nbsp;
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
							<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
								
							</c:otherwise>
						</c:choose>  
						</td>
					</tr>
					</tbody>
			</table>
		</div>
	</div>
					
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloFuncionalidad}</div>
		<div class="contentTablaVariasColumnas">
			<table class="table-cons table" id="tblConsulta">
				<caption></caption>
				<thead>
					<tr>
						<th colspan="2" class="text_centro" scope="col" style="width: 40%">${lblClaveTrans}<br/>
						<label for="paramClaveTrans"></label>
						<select class="Campos comboDes required objFrm focusCombo select " data-field="CVE_TRANSFER"
						id="paramClaveTrans" name="cveTransFe">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${combos.trasferencias}">
								<option value="${reg.cve}"
									${reg.cve eq beanFilter.cveTransFe?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select>
						</th>
						<th class="text_centro" scope="col">${lblMedioEntrega}<br/>
						<label for="paramMedioEntrega"></label>
						<select
							class="Campos comboDes required objFrm focusCombo select " data-field="MECANISMO"
							id="paramMedioEntrega" name="cveMecanismo">
								<option value="">${na}</option>
								<c:forEach var="reg" items="${combos.mecanismos}">
									<option value="${reg.cve}"
										${reg.cve eq beanFilter.cveMecanismo?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
								</c:forEach>
						</select>
						</th>
						<th class="text_centro" scope="col">${lblEstatus}<br/>
						<label for="paramEstatus"></label>
						<select
							class="Campos comboDes required objFrm focusCombo select" id="paramEstatus" data-field="ESTATUS_TRANSFER"
							name="estatus">
								<option value="">${na}</option>
								<c:forEach var="reg" items="${combos.estatus}">
									<option value="${reg.cve}"
										${reg.cve eq beanFilter.estatus?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
								</c:forEach>
						</select>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="liquidez" items="${beanLiquidez.listaBeanLiquidezIntradia}" varStatus="rowCounter">
							<input type="hidden" name="liquidezExportarE[${rowCounter.index}].cveTransFe" id="cveTransfer" value="${liquidez.cveTransFe}" />
							<input type="hidden" name="liquidezExportarE[${rowCounter.index}].cveMecanismo" id="mecanismo" value="${liquidez.cveMecanismo}" />
							<input type="hidden" name="liquidezExportarE[${rowCounter.index}].estatus" id="estatus" value="${liquidez.estatus}" />
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td>
							<label for="check"> 
							${espacioEnBlanco}${espacioEnBlanco}
							<input type="checkbox" id="check" name="listaBeanLiquidezIntradia[${rowCounter.index}].seleccionado" onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}" />
							${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
							<c:set var="cveTransferData" value="${fn:split(liquidez.cveTransFe, '-')}"/>
							<c:set var="mecanismoData" value="${fn:split(liquidez.cveMecanismo, '-')}"/>
							<c:set var="estatusData" value="${fn:split(liquidez.estatus, '-')}"/>
							<input type="hidden" name="listaBeanLiquidezIntradia[${rowCounter.index}].cveTransFe" id="cveTransFe" value="${fn:trim(cveTransferData[0])}" />
							<input type="hidden" name="listaBeanLiquidezIntradia[${rowCounter.index}].cveMecanismo" id="cveMecanismo" value="${fn:trim(mecanismoData[0])}" />
							<input type="hidden" name="listaBeanLiquidezIntradia[${rowCounter.index}].estatus" id="estatusTransfer" value="${fn:trim(estatusData[0])}" />
							</label>
							 </td>
							<td>
							${liquidez.cveTransFe} 
							</td>
							<td class="text_centro">${liquidez.cveMecanismo}</td>
							<td class="text_centro">${liquidez.estatus}</td>
						</tr>
					</c:forEach>
					<tr style="display: none;" id="noresults">
						<td>No Hay Resultados</td>
					</tr>	
				</tbody>
			</table>
		</div> 
	<%@include file="paginadorLiquidez.jsp" %>
	<c:if test="${not empty beanLiquidez.listaBeanLiquidezIntradia}">
	<%@ include file="panelLiquidez.jsp" %> 
	</c:if>
	  
	</div>
</form>

<c:if test="${codError!=''}">
<script type="text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>
