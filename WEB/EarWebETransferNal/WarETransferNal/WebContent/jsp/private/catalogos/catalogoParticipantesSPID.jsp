<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="catParticipantesSPID" />
</jsp:include>

<script	src="${pageContext.servletContext.contextPath}/js/private/catalogos/catalogoParticipantes.js"	type="text/javascript"></script>															
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>

<spring:message code="modulo.catParticipantes.SPID.titulo"       var="titulo"/>
<spring:message code="modulo.catParticipantes.SPID.clave"       var="clave_inter"/>
<spring:message code="modulo.catParticipantes.SPID.tituloP"       var="tituloP"/>
<spring:message code="modulo.catParticipantes.SPID.boton.buscar"       var="buscar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.baja"       var="baja"/>
<spring:message code="modulo.catParticipantes.SPID.boton.limpiar"       var="limpiar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.exportar"       var="exportar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.agregar"       var="agregar"/>
<spring:message code="modulo.catParticipantes.SPID.boton.actualizar"       var="actualizar"/>
<spring:message code="modulo.catParticipantes.SPID.col1"       var="col1"/>
<spring:message code="modulo.catParticipantes.SPID.col2"       var="col2"/>
<spring:message code="modulo.catParticipantes.SPID.col3"       var="col3"/>
<spring:message code="modulo.catParticipantes.SPID.col4"       var="col4"/>
<spring:message code="modulo.catParticipantes.SPID.col5"       var="col5"/>

<spring:message code="general.buscar" var="buscar" />
<spring:message code="general.alta" var="alta" />
<spring:message code="general.modificar" var="modificar" />
<spring:message code="general.eliminar" var="eliminar" />
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>


<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span>-${titulo}
</div>

<div class="frameBuscadorSimple" >
	<div class="titleBuscadorSimple">${tituloP}
	</div>
	<form name="formCatPartSPID" id="formCatPartSPID" method="post">
		<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${resultadoConsulta.beanPaginador.paginaIni}" />
		<input type="hidden" name="paginador.pagina" id="pagina" value="${resultadoConsulta.beanPaginador.pagina}" />
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${resultadoConsulta.beanPaginador.paginaFin}" />
		<input type="hidden" name="paginador.accion" id="accion" value="" />
		<input type="hidden" name="cveParticipante" id="cveParticipante" value="${cveParticipante}">
		
		<input id="hdnCheckSeleccionados" name="hdnCheckSeleccionados" type="hidden" value="" />
		
		<div class="contentBuscadorSimple" style="width:99%">
			<table width="100%" border="0">
				<tr>
					<td width="50%" class="text_izquierda">${clave_inter} 
						<input  name="claveInter" type="text" class="Campos" id="claveInter" size="15" maxlength="5"  value="${cveParticipante}" />										
					</td>
				</tr>
				<tr>				
					<td width="20%" class="izq">
						<span><a id="idBuscar" href="javascript:buscarParticipantes();">${buscar}</a></span>
						<span><a id="idLimpiar" href="javascript:limpiarForm();">${limpiar}</a></span>
					</td>
				</tr>
			</table>
		</div>
		
		<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloP}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table id="consultaInter">
				<tr>
					<th width="100" class="text_centro" scope="col">${col1}</th>
					<th width="100" class="text_centro" scope="col">${col2}</th>
					<th width="100" class="text_centro" scope="col">${col3}</th>
					<th width="180" class="text_centro" scope="col">${col4}</th>
					<th width="180" class="text_centro" scope="col">${col5}</th>
					
				</tr>
				<tbody>				
					<c:forEach var="lista" items="${listaParticipantes}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_centro">
								<input type="checkbox" name="marca" id="marca${rowCounter.count}" value="${lista.cveIntermediario}"/>
								 <input id="cveAux" name="cveAux" value="${lista.cveIntermediario}" type="hidden" /> 
							</td>	
							<td class="text_centro">${lista.cveIntermediario}
							</td>
							<td class="text_centro">${lista.numBanxico}
							</td>
							<td class="text_centro">${lista.nombre}
							</td>
							<td class="text_centro">${lista.fecha}
							</td>																
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
	
	 	<c:if test="${not empty listaParticipantes}">
			<div class="paginador">
				<c:if test="${resultadoConsulta.beanPaginador.paginaIni == resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaIni != resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formCatPartSPID','paginarCat.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaAnt!='0' && resultadoConsulta.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('formCatPartSPID','paginarCat.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaAnt=='0' || resultadoConsulta.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${resultadoConsulta.beanPaginador.pagina} - ${resultadoConsulta.beanPaginador.paginaFin}</label>
				<c:if test="${resultadoConsulta.beanPaginador.paginaFin != resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formCatPartSPID','paginarCat.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaFin == resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaFin != resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formCatPartSPID','paginarCat.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${resultadoConsulta.beanPaginador.paginaFin == resultadoConsulta.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		</c:if>
	
		<div class="framePieContenedor">
			<div class="contentPieContenedor">			
				<table>
					<tr>
						<td class="izq">
							<a href="javascript:altaParticipantes();" id="btnAdd">${alta}</a>
						</td>
						<td class="der">
							<a href="javascript:buscarParticipantes();" id="btnAct">${actualizar}</a>
						</td>
					</tr>
					<tr>
						<td class="izq">
							<a href="javascript:exportar();">${exportar}</a>
							<!-- <a href="#" id="btnExport">${exportar}</a> -->
						</td>
						<td class="der">
							<a href="javascript:eliminaRegistro();" id="btnEli">${eliminar}</a>
						</td>			
					</tr>		
				</table>
				
			</div>
		</div>
		

		</div>
	</form>
	
</div>
<jsp:include page="../myFooter.jsp" flush="true" />
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>