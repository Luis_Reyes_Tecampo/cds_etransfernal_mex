<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="catalogoParametros" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.nombreAplicacion" var="aplicacion" />

<spring:message code="moduloCatalogoParametros.myMenu.title.catalogoParametros" var="catalogoParametros"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.button.limpiar" var="btnLimpiar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.button.buscar" var="btnBuscar"/>

<spring:message code="moduloCatalogo.mantenimientoParametros.link.exportarTodo" var="lnkExportarTodo"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.cancelar" var="lnkCancelar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.eliminar" var="lnkEliminar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.exportar" var="lnkExportar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.guardar" var="lnkGuardar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.agregar" var="lnkAgregar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.editar" var="lnkEditar"/>

<spring:message code="moduloCatalogo.mantenimientoParametros.label.campoObligatorio" var="campoObligatorio"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.tipoParametro"var="lbltipoParametro"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.parametroFalta" var="lblParamFalta"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.LongitudDato" var="lblLogitudDato"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.descripcion" var="lblDescripcion"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.claveParametro" var="lblCveParam"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.valorFalta" var="lblValorFalta"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.parametro" var="lblParametro"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.mecanismo" var="lblMecanismo"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.posicion" var="lblPosicion"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.programa" var="lblPrograma"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.tipoDato" var="lblTipoDato"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.define" var="lblDefine"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.trama" var="lblTrama"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.cola" var="lblCola"/>

<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>

<script type="text/javascript">
	var mensajesMov = new Array();
	mensajesMov["aplicacion"] = '${aplicacion}';
	mensajesMov["CD00010V"] = '${CD00010V}';
	mensajesMov["ED00021V"] = '${ED00021V}';
</script>

<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mantenimientoParametrosGuardar.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<form id="idFormG" action="" method="post">
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResponseGuardar.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResponseGuardar.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResponseGuardar.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.regIni" id="regIni" value="${beanResponseGuardar.paginador.regIni}"/>
	<input type="hidden" name="paginador.regFin" id="regFin" value="${beanResponseGuardar.paginador.regFin}"/>
	<input type="hidden" name="paginador.paginaAnt" id="regIni" value="${beanResponseGuardar.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.paginaSig" id="regFin" value="${beanResponseGuardar.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.accion" id="accion" value="${beanResponseGuardar.paginador.accion}"/>

	<input type="hidden" id="parametro" name="parametro" value="${beanResponseGuardar.parametro}"/>
	<input type="hidden" id="descripParam" name="descripParam" value="${beanResponseGuardar.descripParam}"/>
	<input type="hidden" id="tipoParametro" name="tipoParametro" value="${beanResponseGuardar.tipoParametro}"/>
	<input type="hidden" id="tipoDato" name="tipoDato" value="${beanResponseGuardar.tipoDato}"/>
	<input type="hidden" id="longitudDato" name="longitudDato" value="${beanResponseGuardar.longitudDato}"/>
	<input type="hidden" id="valorFalta" name="valorFalta" value="${beanResponseGuardar.valorFalta}"/>
	<input type="hidden" id="trama" name="trama" value="${beanResponseGuardar.trama}"/>
	<input type="hidden" id="cola" name="cola" value="${beanResponseGuardar.cola}"/>
	<input type="hidden" id="mecanismo" name="mecanismo" value="${beanResponseGuardar.mecanismo}"/>
	<input type="hidden" id="posicion" name="posicion" value="${beanResponseGuardar.posicion}"/>
	<input type="hidden" id="parametroFalta" name="parametroFalta" value="${beanResponseGuardar.parametroFalta}"/>
	<input type="hidden" id="programa" name="programa" value="${beanResponseGuardar.programa}"/>
	
	<input type="hidden" id="altaParametro" name="altaParametro" value="${esAltaNuevoParam}"/>
	<input type="hidden" id="keyCveParam" name="keyCveParam" value="${cveParamKey}"/>
	<input type="hidden" id="filtroParametro" name="filtroParametro" value="${filtroParametro}"/>
	<input type="hidden" id="filtroDescripcion" name="filtroDescripcion" value="${filtroDescripcion}"/>
	<input type="hidden" id="filtroTipoParametro" name="filtroTipoParametro" value="${filtroTipoParametro}"/>
	<input type="hidden" id="filtroTipoDato" name="filtroTipoDato" value="${filtroTipoDato}"/>
	<input type="hidden" id="filtroLongitudDato" name="filtroLongitudDato" value="${filtroLongitudDato}"/>
	<input type="hidden" id="filtroValorFalta" name="filtroValorFalta" value="${filtroValorFalta}"/>
	<input type="hidden" id="filtroTrama" name="filtroTrama" value="${filtroTrama}"/>
	<input type="hidden" id="filtroCola" name="filtroCola" value="${filtroCola}"/>
	<input type="hidden" id="filtroMecanismo" name="filtroMecanismo" value="${filtroMecanismo}"/>
	<input type="hidden" id="filtroPosicion" name="filtroPosicion" value="${filtroPosicion}"/>
	<input type="hidden" id="filtroParametroFalta" name="filtroParametroFalta" value="${filtroParametroFalta}"/>
	<input type="hidden" id="filtroPrograma" name="filtroPrograma" value="${filtroprograma}"/>
	<input type="hidden" id="filtroAltaParametro" name="filtroAltaParametro" value="${esAltaNuevoParam}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${catalogoParametros}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">${lblParametro}:</td>
					<td>
						<c:choose>
	   						<c:when test="${esAltaNuevoParam}">
								<input type="text" class="Campos" id="idParametroG" name="idParametroG" size="20" maxlength="16" value="${beanResponseGuardar.parametro}"/>
	   						</c:when>    
	   						<c:otherwise>
								<input type="text" class="Campos" id="idParametroG" name="idParametroG" disabled="disabled" size="20" maxlength="16" value="${beanResponseGuardar.parametro}"/>
	   						</c:otherwise>
  						</c:choose>
						<span><a style="color:red;">${campoObligatorio}</a></span>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblDescripcion}:</td>
					<td><input type="text" class="Campos" id="idDescripcionG" name="idDescripcionG" size="60" maxlength="60" value="${beanResponseGuardar.descripParam}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lbltipoParametro}:</td>
					<td>
						<select name="idTipoParametroG" class="Campos" id="idTipoParametroG" >
							<c:forEach var="opcionParam" items="${beanResponseGuardar.opcionesTipoParam}">
								<c:choose>
									<c:when test="${opcionParam.valorOpcion == beanResponseGuardar.tipoParametro}">
										<option value="${opcionParam.valorOpcion}" selected="selected">${opcionParam.opcion}</option>
									</c:when>
									<c:otherwise>
										<option value="${opcionParam.valorOpcion}">${opcionParam.opcion}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblTipoDato}:</td>
					<td>
						<select name="idTipoDatoG" class="Campos" id="idTipoDatoG" >
							<c:forEach var="opcionDato" items="${beanResponseGuardar.opcionesTipoDato}">
								<c:choose>
									<c:when test="${opcionDato.valorOpcion == beanResponseGuardar.tipoDato}">
										<option value="${opcionDato.valorOpcion}" selected="selected">${opcionDato.opcion}</option>
									</c:when>
									<c:otherwise>
										<option value="${opcionDato.valorOpcion}">${opcionDato.opcion}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblLogitudDato}:</td>
					<td><input type="text" class="Campos" id="idLongitudDatoG" name="idLongitudDatoG"  size="5" maxlength="5" value="${beanResponseGuardar.longitudDato}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblValorFalta}:</td>
					<td>
						<input type="text" class="Campos" id="idValorFaltaG" name="idValorFaltaG"  size="20" maxlength="20" value="${beanResponseGuardar.valorFalta}"/>
						<span><a style="color:red;">${campoObligatorio}</a></span>
					</td>
				</tr>
			</table>
			<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">${lblDefine}:</td>
					<td  class="text_izquierda">${lblTrama}:
						<select name="idTramaG" class="Campos" id="idTramaG" >
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion  == beanResponseGuardar.trama}">
										<option value="${opcion.valorOpcion }" selected="selected">${opcion.opcion }</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion }">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
					<td width="1"></td>
					<td  class="text_izquierda">${lblCola}:
						<select name="idColaG" class="Campos" id="idColaG" >
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion  == beanResponseGuardar.cola}">
										<option value="${opcion.valorOpcion }" selected="selected">${opcion.opcion}</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion }">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
					<td width="1"></td>
					<td  class="text_izquierda">${lblMecanismo}:
						<select name="idMecanismoG" class="Campos" id="idMecanismoG" >
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion  == beanResponseGuardar.mecanismo}">
										<option value="${opcion.valorOpcion }" selected="selected">${opcion.opcion}</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion }">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblPosicion}:</td>
					<td>
						<c:choose>
    						<c:when test="${beanResponseGuardar.trama == 'S'}">
								<input type="text" class="Campos" id="idPosicionG" name="idPosicionG" size="5" maxlength="5" value="${beanResponseGuardar.posicion}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idPosicionG" name="idPosicionG" size="5" maxlength="5" disabled value="${beanResponseGuardar.posicion}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblParamFalta}:</td>
					<td>
						<c:choose>
    						<c:when test="${beanResponseGuardar.trama == 'S'}">
								<input type="text" class="Campos" id="idParamFaltaG" name="idParamFaltaG" size="16" maxlength="16" value="${beanResponseGuardar.parametroFalta}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idParamFaltaG" name="idParamFaltaG" size="16" maxlength="16" disabled value="${beanResponseGuardar.parametroFalta}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblPrograma}:</td>
					<td></td>
					<td width="1"></td>
					<td>
						<c:choose>
    						<c:when test="${beanResponseGuardar.cola == 'S'}">
								<input type="text" class="Campos" id="idProgramaG" name="idProgramaG" size="16" maxlength="16" value="${beanResponseGuardar.programa}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idProgramaG" name="idProgramaG" size="16" maxlength="16" disabled value="${beanResponseGuardar.programa}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr>
					<td></td>
					<td></td>
					<td width="1"></td>
					<td></td>
					<td width="1"></td>
					<td class="text_derecha">
						<span><a id="idLimpiarG" href="javascript:;">${btnLimpiar}</a></span>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idGuardarG" href="javascript:;">${lnkGuardar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${lnkGuardar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idCancelarG" href="javascript:;">${lnkCancelar}</a></td>
					</tr>
					<tr>
						<td width="279" class="izq_Des"></td>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der_Des"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>