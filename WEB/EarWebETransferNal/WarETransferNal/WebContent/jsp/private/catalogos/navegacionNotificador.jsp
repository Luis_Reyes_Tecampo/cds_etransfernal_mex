<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanNotificaciones.notificaciones}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanNotificaciones.beanPaginador.regIni} al
						${beanNotificaciones.beanPaginador.regFin} de un total de
						${beanNotificaciones.totalReg} registros</label>
				</div>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaIni == beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaIni != beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNotificador.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaAnt!='0' && beanNotificaciones.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNotificador.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaAnt=='0' || beanNotificaciones.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanNotificaciones.beanPaginador.pagina}
					- ${beanNotificaciones.beanPaginador.paginaFin}</label>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaFin != beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNotificador.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaFin == beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaFin != beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaNotificador.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanNotificaciones.beanPaginador.paginaFin == beanNotificaciones.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>