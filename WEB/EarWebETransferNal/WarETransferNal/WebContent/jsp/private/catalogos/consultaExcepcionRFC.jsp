<%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true"> 
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="excepRFC" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script
	src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/global.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/utilerias.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/catalogos/catRFC.js"
	type="text/javascript"></script>

<%-- Inicio de llamado de datos de Properties --%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="catalogos.rfc.title"var="tituloFuncionalidad" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco" />
<%-- Opciones de la Pantalla --%>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="catalogos.rfc.lbl.rfc" var="lblRFC"/>
<spring:message code="general.lblCargaArchivo" var="lblCarga"/>
<spring:message code="general.exportar" var="lblExportar"/>
<spring:message code="general.exportarAll" var="lblExportarTodo"/>
<spring:message code="general.newRegistro" var="lblAgregar"/>
<spring:message code="general.EditRegistro" var="lblEditar"/>
<spring:message code="general.Actualizar" var="lblActualizar"/> 
<spring:message code="general.eliminar" var="lblEliminar"/>

<%-- Constantes de Error --%>     
<spring:message code="general.nombreAplicacion" var="aplicacion"/>  
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>  
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/> 
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>

<%-- Fin de Variables Constantes --%>
<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};</script>
<%-- Fin de Variables Constantes --%>
 
<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>
<c:set var="searchString" value="${seguTareas}" />
<form id="idForm" name="idForm" method="post" action="">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="pagina" id="pagina"
		value="${beanResRFC.beanPaginador.pagina}"> <input
		type="hidden" name="paginaIni" id="paginaIni"
		value="${beanResRFC.beanPaginador.paginaIni}"> <input
		type="hidden" name="paginaFin" id="paginaFin"
		value="${beanResRFC.beanPaginador.paginaFin}"> <input
		type="hidden" name="field" id="field"
		value="${beanResRFC.beanFilter.field}"> <input
		type="hidden" name="fieldValue" id="fieldValue"
		value="${beanResRFC.beanFilter.fieldValue}"> <input
		type="hidden" name="accion" id="accion" value=""> <input
		type="hidden" name="opcion" id="opcion" value="">

	<input type="hidden" id="ruta" name="ruta" value="consultaExcepcionRFC.do"/>					
		<input type="hidden" id="idCat" name="idCat" value="3"/>
		<input type="hidden" id="nombreCat" name="nombreCat" value="excepRFC"/>	
		
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloFuncionalidad}</div>
		<div class="contentTablaVariasColumnas">
			<table class="table-cons table" id="tblConsulta"> 
				<caption></caption>
				<thead>
					<tr>
						<th colspan="2" class="text_centro filField" scope="col">${lblRFC}<br/> 
						<label for="RFCInput"></label>
						<input type="text" id="RFCInput" name="rfc" class="filField" 
							data-field="RFC" maxlength="13" size="20" value="${beanFilter.rfc}">
						</th> 
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rfc"
						items="${beanResRFC.rfcs}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="primerCelda"><label for="check"> <input type="checkbox" id="check" name="rfcs[${rowCounter.index}].seleccionado" onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}" /> 
							<input type="hidden" name="rfcs[${rowCounter.index}].rfc" id="rfc" value="${rfc.rfc}" /> 
							</label>
							</td> 
							<td class="text_izquierda">${rfc.rfc} </td>
						</tr>
					</c:forEach>
					<tr style="display: none;" id="noresults"> 
						<td>No Hay Resultados</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%@ include file="navegacionRFC.jsp" %>
		<%@ include file="panelRFC.jsp" %>
	</div>
</form>

<c:if test="${codError!=''}">
<script type="text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>
