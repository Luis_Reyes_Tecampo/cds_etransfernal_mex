<%-- 
  * ------------------------------------------------------------------------------------------
  * Isban Mexico
  * CatalogoCatIntermediarios.jsp
  * Descripcion: Archivo de definicion de la pantalla "Catalogo Intermediario - Vigente/Historico".
  *
  * Version  Date        By                                         Description
  * -------  ----------  ---------------------------------          ------------------------------------------
  * 1.0      24/12/2019  [1565817: Alberto Olvera Juarez]           Creacion de JSP Pantalla de Catalogo Intermediario
  * ------------------------------------------------------------------------------------------
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ include file="../myHeader.jsp" %>
<jsp:include page="../myMenuSubCatalagos.jsp" flush="true">
	<jsp:param name="menuItem"    value="catalogos" />
	<jsp:param value="catIntFinancieros" name="menuSubitem"/>
</jsp:include>

<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	
<spring:message code="general.nombreAplicacion" var="app"/>
<spring:message code="general.bienvenido"       var="welcome"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${welcome}</span> - ${app}
</div>