<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

				<tr>
					<td>Fases:</td>
					<td></td>
					<c:choose>
						<c:when test="${esActivoEnvRecArch}">
						<c:choose>
						<c:when test="${modulo eq 'spid'}">
							<td colspan="2"><span><a id="idEnvRecArch" href="javascript:activarEnvRecArch('spid');">${txtEnvRecArch}</a></span></td>
							</c:when>
							<c:otherwise>
							<td colspan="2"><span><a id="idEnvRecArch" href="javascript:activarEnvRecArch('actual');">${txtEnvRecArch}</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td colspan="2"><span class="btn_Des"><a id="idEnvRecArch" href="javascript:;">${txtEnvRecArch}</a></span></td>
						</c:otherwise>
					</c:choose>					
					<td><span></span></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<c:choose>
						<c:when test="${esActivoFinalDevo}">
						<c:choose>
						<c:when test="${modulo eq 'spid'}">
							<td colspan="2"> <span ><a id="idFinalDev" href="javascript:activarFinalDev('spid');">${txtFinalDev}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:when>
							<c:otherwise>
							<td colspan="2"> <span ><a id="idFinalDev" href="javascript:activarFinalDev('actual');">${txtFinalDev}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td colspan="2"> <span class="btn_Des"><a id="idFinalDev" href="javascript:;">${txtFinalDev}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
						</c:otherwise>
					</c:choose>					
					<td><span></span></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<%@ include file="parametrosPOACOAFases2.jsp" %>