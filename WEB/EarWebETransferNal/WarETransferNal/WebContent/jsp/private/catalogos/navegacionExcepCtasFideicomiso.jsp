<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanCtasFideico.listaCuentas}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanPaginador.regIni} al
						${beanPaginador.regFin} de un total de
						${totalReg} registros</label>
				</div>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaIni == beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaIni != beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepCuentasFideicomiso.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaAnt!='0' && beanCtasFideico.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepCuentasFideicomiso.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaAnt=='0' || beanCtasFideico.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanPaginador.pagina}
					- ${beanPaginador.paginaFin}</label>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaFin != beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepCuentasFideicomiso.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaFin == beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaFin != beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','excepCuentasFideicomiso.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanCtasFideico.beanPaginador.paginaFin == beanCtasFideico.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>