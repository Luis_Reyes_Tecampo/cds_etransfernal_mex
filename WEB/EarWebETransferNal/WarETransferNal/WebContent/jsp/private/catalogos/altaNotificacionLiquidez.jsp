<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="ctaLiquidezIntradia" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/notificacionLiquidez.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<%--Inicio de llamado de datos de Properties--%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="general.nombreAplicacion" 						var="app"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.eliminar" 								var="lblEliminar"/>
<spring:message code="general.guardar" 									var="lblGuardar"/>
<spring:message code="general.cancelar" 								var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.asteriscoHelper"                          var="asteriscoHelper" />
<spring:message code="general.dosPuntosHelper"                          var="DosPuntosHelper" />
<spring:message code="catalogos.certificadosCifrado.alta" 				var="alta"/>
<spring:message code="catalogos.certificadosCifrado.modificar" 			var="modificar"/>
<spring:message code="catalogos.notificacionLiquidezIn.title"var="tituloFuncionalidad" />
<spring:message code="catalogos.notificacionLiquidezIn.lbl.tipoConsulta" var="lblTipoNot"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveTrasfe" var="lblClaveTrans"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveMecanismo" var="lblMedioEntrega"/>
<spring:message code="catalogos.notificacionLiquidezIn.lbl.cveEstatus" var="lblEstatus"/>
<spring:message code="general.newRegistro" var="lblAgregar"/>
<spring:message code="general.EditRegistro" var="lblEditar"/>

<spring:message code="catalogos.notificacionLiquidezIn.lbl.na" var="na"/> 
<%-- Fin de Variables Constantes --%>

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="catalogos.notificacionLiquidezIn.msg" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',"ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}'  };</script>	
 <form id="mform" method="post">
 	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
 	<input type="hidden" name="accion" id="accion" value="${accion}">
 	<input type="hidden" name="canal" id="canal" value="${beanLiquidez.cveTransFe}" ${accion eq 'alta' ? 'disabled="disabled"' : ''}>
 	<input type="hidden" name="tipoConsulta" id="tipoConsulta" value="${tipoConsulta}">
	<div class="pageTitleContainer">
	 <c:choose>
	  <c:when test="${accion eq 'alta'}">
	  <span class="pageTitle">${tituloModulo}</span> - ${lblAgregarView} ${tituloFuncionalidad}
	  </c:when>
	  <c:otherwise>
	  <span class="pageTitle">${tituloModulo}</span> - ${lblEditarView} ${tituloFuncionalidad}
	  </c:otherwise>
	 </c:choose>
		
	</div>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblClaveTrans}${DosPuntosHelper}</td>
					<td>
					<label for="paramClaveTrans"></label>
					<select class="Campos comboDes required objFrm focusCombo comboWidth"
						id="paramClaveTrans" name="cveTransFe">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${combos.trasferencias}">
								<option value="${reg.cve}"
									${reg.cve eq beanLiquidez.cveTransFe?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>	
					</select> <c:if test="${accion eq 'modificar'}">
							<label for="cveTransferOld"></label>
							<input type="hidden" name="cveTransferOld"
								value="${beanLiquidez.cveTransFe}" />
						</c:if></td>
				</tr>
				<tr>
				<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblMedioEntrega}${DosPuntosHelper}</td>
				<td><label for="paramMedioEntrega"></label> <select
					class="Campos comboDes required objFrm focusCombo comboWidth"
					id="paramMedioEntrega" name="cveMecanismo">
						<option value="">${na}</option>
						<c:forEach var="reg" items="${combos.mecanismos}">
							<option value="${reg.cve}"
								${reg.cve eq beanLiquidez.cveMecanismo?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
						</c:forEach>
				</select> <c:if test="${accion eq 'modificar'}">
						<label for="mecanismoOld"></label>
						<input type="hidden" name="mecanismoOld"
							value="${beanLiquidez.cveMecanismo}" />
					</c:if></td>
			</tr>
			
			<tr>
				<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblEstatus}${DosPuntosHelper}</td>
				<td><label for="paramEstatus"></label> <select
					class="Campos comboDes required objFrm focusCombo comboWidth" id="paramEstatus"
					name="estatus">
						<option value="">${na}</option>
						<c:forEach var="reg" items="${combos.estatus}">
							<option value="${reg.cve}"
								${reg.cve eq beanLiquidez.estatus?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
						</c:forEach>
				</select> <c:if test="${accion eq 'modificar'}">
						<label for="estatusTransferOld"></label>
						<input type="hidden" name="estatusTransferOld"
							value="${beanLiquidez.estatus}" />
					</c:if></td>
			</tr>
			</table>			
	    </div> 
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		        <caption></caption>
		             <tr>
		                 <td class="izq">
		                     <a id="btnGuardar" href="javascript:;" >${lblGuardar}</a>
		                 </td>
		                 <td class="odd">${espacioEnBlanco}</td>
		                 <td class="der">
		                     <a id="btnReturn" href="javascript:;" >${lblRegresar}</a>
		                 </td>
		             </tr>
		        </table>
		    </div>
		</div>
	</div>
	<input type="hidden" id="cveTransferF" name="cveTransferF" class="filField" value="${beanFilter.cveTransFe}">
	<input type="hidden" id="medioEntregaF" name="medioEntregaF" class="filField" value="${beanFilter.cveMecanismo}">
	<input type="hidden" id="estatusTransferF" name="estatusTransferF" class="filField" value="${beanFilter.estatus}">
	<input type="hidden" name="pagina" id="pagina" value="${beanPaginador.pagina}" /> 
	<input type="hidden" name="paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}" /> 
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}" />
</form>

<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script></c:if>