<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<input type="hidden" id="esSPID" name="esSPID" value="${esSPID}" />
<jsp:include page="../myHeader.jsp" flush="true"/>
<c:choose>
	<c:when test="${esSPID}">
		<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloCDASPID" />
			<jsp:param name="menuSubitem" value="muestraBitacoraAdmon" />
		</jsp:include>
	</c:when>
	<c:otherwise>
		<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
			<jsp:param name="menuItem"    value="moduloCDA" />
			<jsp:param name="menuSubitem" value="muestraBitacoraAdmon" />
		</jsp:include>
	</c:otherwise>
</c:choose>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.servicio" var="servicio"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.usuario" var="usuario"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.FechaAccionIni" var="FechaAccionIni"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.FechaAccionFin" var="FechaAccionFin"/>
<spring:message code="moduloCDA.bitacoraAdmon.botonLink.Buscar" var="Buscar"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.infoEncontrada" var="infoEncontrada"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.fechaOp" var="fechaOp"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.hrOpe" var="hrOpe"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.ip" var="ip"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.modCanalOpe" var="modCanalOpe"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.datoAfectado" var="datoAfectado"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.valorAnt" var="valorAnt"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.valorNuevo" var="valorNuevo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.descOp" var="descOp"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.numReg" var="numReg"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.codError" var="codError"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.descCodError" var="descCodError"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.exportar" var="exportar"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.detenerServicio" var="detenerServicio"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.generarArchivoMismoDia" var="generarArchivoMismoDia"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.generarArchivoHistorico" var="generarArchivoHistorico"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.na" var="na"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.folioOper" var="folioOper"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idToken" var="idToken"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idOperacion" var="idOperacion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.estatusOperacion" var="estatusOperacion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.codOpera" var="codOpera"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idSesion" var="idSesion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.tablaAfectada" var="tablaAfectada"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.instanciaWeb" var="instanciaWeb"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.hostWeb" var="hostWeb"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.datoFijo" var="datoFijo"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00015V" var="ED00015V"/>
<spring:message code="codError.ED00016V" var="ED00016V"/>
<spring:message code="codError.ED00017V" var="ED00017V"/>
<spring:message code="codError.ED00018V" var="ED00018V"/>

	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["ED00015V"] = '${ED00015V}';
		mensajes["ED00016V"] = '${ED00016V}';
		mensajes["ED00017V"] = '${ED00017V}';
		mensajes["ED00018V"] = '${ED00018V}';
    </script>
<script src="${pageContext.servletContext.contextPath}/js/private/bitacoraAdmon.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

		<form name="idForm" id="idForm" method="post">
		    <input type="hidden" name="fechaHoy" id="fechaHoy" value="${fechaHoy}"/>
			<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${bitacoraAdmon.paginador.paginaIni}"/>
			<input type="hidden" name="paginador.pagina" id="pagina" value="${bitacoraAdmon.paginador.pagina}"/>
			<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${bitacoraAdmon.paginador.paginaFin}"/>
			<input type="hidden" name="paginador.accion" id="accion" value=""/>
			<input type="hidden" name="paramUsuario" value="${strUsuario}"/>
			<input type="hidden" name="paramServicio" id="paramServicio" value="${strServicio}"/>
			<input type="hidden" name="paramFecha3Antes" id="paramFecha3Antes" value="${fecha3Antes}"/>
			<input type="hidden" name="paramFechaFin" id="paramFechaFin" value="${fechaFin}"/>
			<input type="hidden" name="esSPID" id="esSPID" value="${esSPID}"/>
			
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>
					<tr>
						<td class="text_izquierda">
						${servicio}
						</td>
						<td>
							<select name="servicio" class="Campos" id="servicio">
								<option value="">${na}</option>
								<option value="detenerServicioMonCDA.do" ${strServicio=="detenerServicioMonCDA.do"?"selected":"" } >${detenerServicio}</option>
								<option value="genArchContigMismoDia.do" ${strServicio=="genArchContigMismoDia.do"?"selected":"" }>${generarArchivoMismoDia}</option>
								<option value="generarArchCDAHistDet.do" ${strServicio=="generarArchCDAHistDet.do"?"selected":"" }>${generarArchivoHistorico}</option>
							</select> 
						</td>
						<td>
							${FechaAccionIni} 
						</td>
						<td>     
						<input name="fechaAccionIni" type="text" readonly="readonly" class="Campos" id="fechaAccionIni" size="10" value="${fecha3Antes}"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						${usuario}
						</td>
						<td>
							<input name="usuario" type="text" class="Campos" id="usuario" size="10" maxlength="10" value="${strUsuario}"/> 
						</td>
						<td>
							${FechaAccionFin} 
						</td>
						<td>
						<input name="fechaAccionFin" type="text" readonly="readonly" class="Campos" id="fechaAccionFin" size="10" value="${fechaFin}"/>
						<img id="cal2" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						 
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${Buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${Buscar}</a></span>
							</c:otherwise>
						</c:choose>
						
						
						</td>
						
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	


	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infoEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table >
				<tr>
					<th  width="132" class="text_izquierda">${servicio}</th>
					<th  width="132" class="text_centro" scope="col">${fechaOp}</th>
					<th  width="132" class="text_centro" scope="col">${hrOpe}</th>
					<th  width="122" class="text_centro" scope="col">${ip}</th>
					<th  width="122" class="text_centro" scope="col">${modCanalOpe}</th>
					<th  width="122" class="text_centro" scope="col">${usuario}</th>
					<th  width="122" class="text_centro" scope="col">${datoAfectado}</th>
					<th  width="122" class="text_centro" scope="col">${valorAnt}</th>
					<th  width="122" class="text_centro" scope="col">${valorNuevo}</th>
					<th  width="122" class="text_centro" scope="col">${descOp}</th>
					<th  width="122" class="text_centro" scope="col">${numReg}</th>
					<th  width="122" class="text_centro" scope="col">${nomArchivo}</th>
					<th  width="122" class="text_centro" scope="col">${codError}</th>
					<th  width="122" class="text_centro" scope="col">${descCodError}</th>
					<th  width="122" class="text_centro" scope="col">${folioOper}</th>
					<th  width="122" class="text_centro" scope="col">${idToken}</th>
					<th  width="122" class="text_centro" scope="col">${idOperacion}</th>
					<th  width="122" class="text_centro" scope="col">${estatusOperacion}</th>
					<th  width="122" class="text_centro" scope="col">${codOpera}</th>
					<th  width="122" class="text_centro" scope="col">${idSesion}</th>
					<th  width="122" class="text_centro" scope="col">${tablaAfectada}</th>
					<th  width="122" class="text_centro" scope="col">${instanciaWeb}</th>
					<th  width="122" class="text_centro" scope="col">${hostWeb}</th>
					<th  width="122" class="text_centro" scope="col">${datoFijo}</th>		
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanConsBitAdmon" items="${bitacoraAdmon.listBeanConsBitAdmon}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td width="250" class="text_izquierda">${beanConsBitAdmon.servicio}</td>
						<td class="text_izquierda">${beanConsBitAdmon.fechaOpe}</td>
						<td class="text_izquierda">${beanConsBitAdmon.hrOpe}</td>
						<td class="text_derecha">${beanConsBitAdmon.ip}</td>
						<td class="text_izquierda">${beanConsBitAdmon.moduloCanalOp}</td>
						<td class="text_derecha">${beanConsBitAdmon.usuario}</td>
						<td class="text_derecha">${beanConsBitAdmon.datoAfectado}</td>
						<td class="text_derecha">${beanConsBitAdmon.valAnterior}</td>
						<td class="text_derecha">${beanConsBitAdmon.valNuevo}</td>
						<td class="text_derecha">${beanConsBitAdmon.descOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.numeroRegistros}</td>
						<td class="text_derecha">${beanConsBitAdmon.nomArchivo}</td>
						<td class="text_derecha">${beanConsBitAdmon.codError}</td>
						<td class="text_derecha">${beanConsBitAdmon.descCodError}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.folioOper}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idToken}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.estatusOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.codOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idSesion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.tablaAfectada}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.instanciaWeb}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.hostWeb}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.datoFijo}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
			
		</div>
		<c:if test="${not empty bitacoraAdmon.listBeanConsBitAdmon}">
			<jsp:include page="../paginador.jsp" >	  					
  					<jsp:param name="paginaIni" value="${bitacoraAdmon.paginador.paginaIni}" />
  					<jsp:param name="pagina" value="${bitacoraAdmon.paginador.pagina}" />
  					<jsp:param name="paginaAnt" value="${bitacoraAdmon.paginador.paginaAnt}" />
  					<jsp:param name="paginaFin" value="${bitacoraAdmon.paginador.paginaFin}" />
  					<jsp:param name="servicio" value="buscarBitacoraAdmon.do" />
				</jsp:include>
			</c:if>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
			
				<table>
					<tr>
							
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
				
						<td class="odd">${espacioEnBlanco}</td>
						<td class="cero"></td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero"></td>
					</tr>
				</table>
				
			</div>
		</div>
		
	</div>

		</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
