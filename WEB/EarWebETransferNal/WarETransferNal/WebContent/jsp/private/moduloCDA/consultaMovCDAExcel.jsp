
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.consultaMovCDA.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.consultaMovCDA.text.refere" var="refere"/>
<spring:message code="moduloCDA.consultaMovCDA.text.fechaOpe" var="fechaOpe"/>
<spring:message code="moduloCDA.consultaMovCDA.text.folioPaq" var="folioPaq"/>
<spring:message code="moduloCDA.consultaMovCDA.text.folioPag" var="folioPag"/>
<spring:message code="moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.instEmisora" var="instEmisora"/>
<spring:message code="moduloCDA.consultaMovCDA.text.cveRastreo" var="cveRastreo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.ctaBenef" var="ctaBenef"/>
<spring:message code="moduloCDA.consultaMovCDA.text.monto" var="monto"/>
<spring:message code="moduloCDA.consultaMovCDA.text.hrAbono" var="hrAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.hrEnvio" var="hrEnvio"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusCDA" var="estatusCDA"/>
<spring:message code="moduloCDA.consultaMovCDA.text.codError" var="codError"/>


	
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

<head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>reporte</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->

</head>
		
<body>

			<table >
				<tr>
					<th width="122" class="text_izquierda">${refere}</th>
					<th width="122" class="text_centro" scope="col">${fechaOpe}</th>
					<th width="122" class="text_centro" scope="col">${folioPaq}</th>
					<th width="122" class="text_centro" scope="col">${folioPag}</th>
					<th width="122" class="text_centro" scope="col">${claveSPEIOrdAbono}</th>
					<th width="122" class="text_centro" scope="col">${instEmisora}</th>
					<th width="122" class="text_centro" scope="col">${cveRastreo}</th>
					<th width="122" class="text_centro" scope="col">${ctaBenef}</th>
					<th width="122" class="text_centro" scope="col">${monto}</th>					
					<th width="122" class="text_centro" scope="col">${hrAbono}</th>
					<th width="122" class="text_centro" scope="col">${hrEnvio}</th>
					<th width="122" class="text_centro" scope="col">${estatusCDA}</th>
					<th width="122" class="text_centro" scope="col">${codError}</th>
				</tr>
			
				<tbody>
				  <c:forEach var="beanMovimientoCDA" items="${beanResConsMovCDA.listBeanMovimientoCDA}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda">${beanMovimientoCDA.referencia}</td>						
						<td class="text_izquierda">${beanMovimientoCDA.fechaOpe}</td>
						<td class="text_izquierda">${beanMovimientoCDA.folioPaq}</td>
						<td class="text_derecha">${beanMovimientoCDA.folioPago}</td>
						<td class="text_izquierda">${beanMovimientoCDA.cveSpei}</td>
						<td class="text_derecha">${beanMovimientoCDA.nomInstEmisora}</td>
						<td class="text_izquierda">${beanMovimientoCDA.cveRastreo}</td>
						<td class="text_derecha">${beanMovimientoCDA.ctaBenef}</td>
						<td class="text_izquierda">${beanMovimientoCDA.monto}</td>
						<td class="text_derecha">${beanMovimientoCDA.hrAbono}</td>
						<td class="text_derecha">${beanMovimientoCDA.hrEnvio}</td>
						<td class="text_derecha">${beanMovimientoCDA.estatusCDA}</td>
						<td class="text_derecha">${beanMovimientoCDA.codError}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
</body>			
</html>


		
	

	
