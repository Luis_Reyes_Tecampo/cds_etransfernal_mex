<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraMonitorCDAHist" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.buscar" var="buscar" />
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDAHist.monitorCDA.text.tituloFuncionalidad" var="tituloFuncionalidad" />
<spring:message code="moduloCDAHist.monitorCDA.text.fechaOperacion" var="fechaOperacion" />
<spring:message code="moduloCDAHist.monitorCDA.text.speiRecibidas" var="speiRecibidas" />
<spring:message code="moduloCDAHist.monitorCDA.text.ordRecibidasAplicadas"
	var="ordRecibidasAplicadas" />
<spring:message code="moduloCDAHist.monitorCDA.text.CDA" var="CDA" />
<spring:message code="moduloCDAHist.monitorCDA.text.cdaPendEnviar"
	var="cdaPendEnviar" />
<spring:message code="moduloCDAHist.monitorCDA.text.cdaEnviadas"
	var="cdaEnviadas" />
<spring:message code="moduloCDAHist.monitorCDA.text.cdaConfirmadas"
	var="cdaConfirmadas" />
<spring:message code="moduloCDAHist.monitorCDA.text.cdaContingencia"
	var="cdaContingencia" />
<spring:message code="moduloCDAHist.monitorCDA.text.cdaRechazadas"
	var="cdaRechazadas" />
<spring:message code="moduloCDAHist.monitorCDA.text.monto" var="monto" />
<spring:message code="moduloCDAHist.monitorCDA.text.volumen" var="volumen" />
<spring:message code="moduloCDAHist.monitorCDA.text.estadoConexion"
	var="estadoConexion" />
<spring:message code="moduloCDAHist.monitorCDA.botonLink.buscar"
	var="buscar" />
<spring:message code="moduloCDAHist.monitorCDA.botonLink.actualizar"
	var="actualizar" />
<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.ED00025V" var="ED00025V" />

	
	    <script type="text/javascript">
        var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["CD00010V"] = '${CD00010V}';
		mensajes["ED00025V"] = '${ED00025V}';
    </script>

<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/monitorCDAHist.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>  

<%-- Componente titulo de p�gina --%>
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${tituloFuncionalidad}</div>

<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>

					<tr>
						<td width="150" ><span class="red">
						${fechaOperacion}</span></td>
						<td width="150"><input id="fechaOpe" value="${beanResMonitorCDA.fechaOperacion}" class="Campos" type="text" size="10" readonly="readonly" name="fechaOpe"/>
							<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/></td>
							
						<td width="121" class="izq">
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
								</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
								</c:otherwise>
							</c:choose>
						</td>
						
					</tr>
			</table>
		</div>
	</div>
	

<div class="frameTablaVariasColumnas">
<div class="contentTablaVariasColumnas">
<table>
	<tr>
		<th>${speiRecibidas}</th>
		<td width="100"></td>
		<th>${monto}</th>
		<th>${volumen}</th>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${ordRecibidasAplicadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
		${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAOrdRecAplicadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAOrdRecAplicadas}
		</td>
	</tr>

	<tr>
		<td colspan="4"></td>

	</tr>

	<tr>
		<td colspan="4"></td>

	</tr>

	<tr>
		<th>${CDA}</th>
		<td></td>
		<th>${monto}</th>
		<th>${volumen}</th>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaPendEnviar}</td>
		<td></td>

		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
		${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAPendEnviar}
		</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAPendEnviar ne '0'}">
				<a id="idCDAPendEnviar" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAPendEnviar}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAPendEnviar eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAPendEnviar}
			</c:if>
		</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaEnviadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAEnviadas}</td>
		
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">	
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAEnviadas ne '0'}">
				<a id="idCDAEnviadas" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAEnviadas}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAEnviadas eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAEnviadas}
			</c:if>
		</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaConfirmadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAConfirmadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
			
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAConfirmadas ne '0'}">
				<a id="idCDAConfirmadas" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAConfirmadas}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAConfirmadas eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAConfirmadas}
			</c:if>
			
		</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaContingencia}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAContingencia}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAContingencia ne '0'}">
				<a id="idCDAContingencia" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAContingencia}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAContingencia eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAContingencia}
			</c:if>
		</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaRechazadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDARechazadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas ne '0'}">
				<a id="idCDARechazadas" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas}
			</c:if>	
		</td>
	</tr>
</table>

</div>

</div>


<div class="framePieContenedor">
	<div class="contentPieContenedor">
		<table>
			<tr>
				<td width="279" class="izq_Des"></td>
				<td width="6" class="odd">&nbsp;</td>
				<td width="279" class="der_Des"></td>
			</tr>
		</table>
	</div>
</div>

<form name="idForm" id="idForm" method="post" action="">	
	<input type="hidden" name="accion" id="accion" value=""/>
	<input type="hidden" name="paramFechaOperacion" id="paramFechaOperacion" value="${beanResMonitorCDA.fechaOperacion}"/>
	<input type="hidden" name="estatusCDA" id="estatusCDA" value=""/>
	<input type="hidden" name="fechaOpeInicio" id="fechaOpeInicio" value=""/>
	<input type="hidden" name="fechaOpeFin"    id="fechaOpeFin" value=""/>
				
	</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>