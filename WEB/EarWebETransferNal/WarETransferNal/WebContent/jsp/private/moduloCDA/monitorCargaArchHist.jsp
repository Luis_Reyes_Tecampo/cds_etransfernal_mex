<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraMonitorCargaArchHist" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.estatusCargaArchivo" var="estatusCargaArchivo"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.botonLink.actualizar" var="actualizar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<script src="${pageContext.servletContext.contextPath}/js/private/monitorCargaArchHist.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>


	<!-- Componente titulo de p�gina -->
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<!-- Componente tabla de varias columnas -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${subtituloFuncion}</div>
		<div class="contentTablaVariasColumnas">
			<table>
				<tr>
					<th width="300" class="text_izquierda">${nomArchivo}</th>
					<th width="300" class="text_centro" scope="col">${estatusCargaArchivo}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
					<c:forEach var="beanMonCargaArchHist" items="${resMon.listBeanMonCargaArchHist}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<c:if test="${beanMonCargaArchHist.estatusCargaArch == 'FINALIZADO' }"> 				
							<td class="text_izquierda"><a id="${beanMonCargaArchHist.idPeticion}" href="javascript:;" onclick="monitorCargaArchHist.despliegaDetalle(${beanMonCargaArchHist.idPeticion})">${beanMonCargaArchHist.nomArchivo}</a></td>
							<td class="text_izquierda">${beanMonCargaArchHist.estatusCargaArch}</td>
						</c:if>
						<c:if test="${beanMonCargaArchHist.estatusCargaArch != 'FINALIZADO' }"> 				
							<td class="text_izquierda">${beanMonCargaArchHist.nomArchivo}</td>
							<td class="text_izquierda">${beanMonCargaArchHist.estatusCargaArch}</td>
						</c:if>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:if test="${not empty resMon.listBeanMonCargaArchHist}">
			<div class="paginador">
				<c:if test="${resMon.paginador.paginaIni == resMon.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.paginador.paginaIni != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorCargaArchHist.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.paginador.paginaAnt!='0' && resMon.paginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorCargaArchHist.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${resMon.paginador.paginaAnt=='0' || resMon.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${resMon.paginador.pagina} - ${resMon.paginador.paginaFin}</label>
				<c:if test="${resMon.paginador.paginaFin != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorCargaArchHist.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin == resMon.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin != resMon.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorCargaArchHist.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${resMon.paginador.paginaFin == resMon.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
		</div>
		</c:if>
		
				
	</div>
	
	
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
				<tr>
					<td class="cero">&nbsp;</td>
					<td class="odd">&nbsp;</td>
					
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
							<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
						</c:when>
						<c:otherwise>
							<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td width="279" class="cero">&nbsp;</td>
					<td width="6" class="odd">&nbsp;</td>
					<td width="279" class="cero">&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	
	</div>
	
	<form name="idForm" id="idForm" method="post" action="">
		<input type="hidden" name="paginaIni" id="paginaIni" value="${resMon.paginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${resMon.paginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${resMon.paginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		
	</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
