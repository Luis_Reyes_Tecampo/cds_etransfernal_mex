<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDA" />
	<jsp:param name="menuSubitem" value="consInfMonCDA" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>
<spring:message code="general.buscar" var="buscar" />
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDA.monitorCDA.text.tituloFuncionalidad" var="tituloFuncionalidad" />
<spring:message code="moduloCDA.monitorCDA.text.speiRecibidas"
	var="speiRecibidas" />
<spring:message code="moduloCDA.monitorCDA.text.ordRecibidasAplicadas"
	var="ordRecibidasAplicadas" />
<spring:message code="moduloCDA.monitorCDA.text.CDA" var="CDA" />
<spring:message code="moduloCDA.monitorCDA.text.cdaPendEnviar"
	var="cdaPendEnviar" />
<spring:message code="moduloCDA.monitorCDA.text.cdaEnviadas"
	var="cdaEnviadas" />
<spring:message code="moduloCDA.monitorCDA.text.cdaConfirmadas"
	var="cdaConfirmadas" />
<spring:message code="moduloCDA.monitorCDA.text.cdaContingencia"
	var="cdaContingencia" />
<spring:message code="moduloCDA.monitorCDA.text.cdaRechazadas"
	var="cdaRechazadas" />
<spring:message code="moduloCDA.monitorCDA.text.estadoConexion"
	var="estadoConexion" />
<spring:message code="moduloCDA.monitorCDA.text.monto" var="monto" />
<spring:message code="moduloCDA.monitorCDA.text.volumen" var="volumen" />
<spring:message code="moduloCDA.monitorCDA.text.estadoConexion"
	var="estadoConexion" />
<spring:message code="moduloCDA.monitorCDA.botonLink.detenerServicioCDA"
	var="detenerServicioCDA" />
<spring:message code="moduloCDA.monitorCDA.botonLink.actualizar"
	var="actualizar" />
<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.CD00010V" var="CD00010V" />
	
	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["CD00010V"] = '${CD00010V}';
	 
    </script>

<spring:message code="general.dosPuntosHelper" var="dosPuntos" />
<spring:message code="moduloCDA.monitorCDA.text.contingencia.enviado" var="contingenciaEnviado" />
<spring:message code="moduloCDA.monitorCDA.text.contingencia.confirmado" var="contingenciaConfirmado" />
<spring:message code="moduloCDA.monitorCDA.text.procesadas" var="procesadas" />
<spring:message code="moduloCDA.monitorCDA.text.contingencia.total" var="contingenciaTotal" />

<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/monitorCDA.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>

<!-- Componente titulo de p�gina -->
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${tituloFuncionalidad}</div>

<!-- Componente tabla de varias columnas -->
<div class="frameTablaVariasColumnas"><!--  <div class="titleTablaVariasColumnas"></div>  -->
<div class="contentTablaVariasColumnas">
<table>
	<tr>
		<th>${speiRecibidas}</th>
		<td width="100"></td>
		<th>${monto}</th>
		<th>${volumen}</th>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${ordRecibidasAplicadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
		${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAOrdRecAplicadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
		${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAOrdRecAplicadas}</td>
	</tr>
	<tr>
		<td class="td-border-cda">${procesadas}</td>
		<td></td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaMontos.montoProcesadas}</td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenProcesadas}</td>
	</tr>
	<tr>
		<td colspan="4"></td>
	</tr>

	<tr>
		<th>${CDA}</th>
		<td></td>
		<th>${monto}</th>
		<th>${volumen}</th>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaPendEnviar}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAPendEnviar}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAPendEnviar}</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaEnviadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAEnviadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAEnviadas}</td>
	</tr>

	<tr>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${cdaConfirmadas}</td>
		<td></td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAConfirmadas}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAConfirmadas}</td>
	</tr><tr>
		<td class="td-border-cda">${cdaRechazadas}</td>
		<td></td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDARechazadas}</td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas}</td></tr>
	<tr><th>${cdaContingencia}${dosPuntos}</th>
		<td></td>
		<th>${monto}</th>
		<th>${volumen}</th></tr>
	<tr><td class="td-border-cda">${contingenciaTotal}</td>
		<td></td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaMontos.montoCDAContingencia}</td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDAContingencia}</td></tr>
	<tr><td class="td-border-cda">${contingenciaEnviado}</td>
		<td></td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaMontos.montoEnvContingencia}</td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenEnvContingencia}</td></tr>
	<tr><td class="td-border-cda">${contingenciaConfirmado}</td>
		<td></td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaMontos.montoConfContingencia}</td>
		<td class="td-border-cda">${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenConfContingencia}</td></tr>
	<tr><td colspan="4"></td></tr>
	<tr><th>${estadoConexion}</th><td></td><td colspan="2" bgcolor="${beanResMonitorCDA.colorSemaforo}"></td></tr>
	<tr><td colspan="4"></td></tr>
</table>
</div>
<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
				<td width="279" class="izq"><a id="idDetenerServicioCDA" href="javascript:;" >${detenerServicioCDA}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="izq_Des"><a href="javascript:;" >${detenerServicioCDA}</a></td>
			</c:otherwise>
		</c:choose>
		<td width="6" class="odd">&nbsp;</td>
		
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
				<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>

</table>
</div>
</div>
</div>

<form name="idForm" id="idForm" method="post" action="">
		
		<input type="hidden" name="accion" id="accion" value="">
				
	</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
		${e:forHtml(tipoError)}('${e:forHtml(descError)}',
		   	   '${e:forHtml(aplicacion)}',
		   	   '${e:forHtml(codError)}',
		   	   '');
		</script>
	</c:if>