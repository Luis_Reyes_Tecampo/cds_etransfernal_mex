<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraMonitorArchExp" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.monitorArchExp.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.monitorArchExp.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.monitorArchExp.text.modulo" var="modulo"/>
<spring:message code="moduloCDA.monitorArchExp.text.subModulo" var="subModulo"/>
<spring:message code="moduloCDA.monitorArchExp.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.monitorArchExp.text.estatus" var="estatus"/>
<spring:message code="moduloCDA.monitorArchExp.text.numRegistros" var="numRegistros"/>
<spring:message code="moduloCDA.monitorArchExp.text.fechaCarga" var="fechaCarga"/>
<spring:message code="moduloCDA.monitorArchExp.text.actualizar" var="actualizar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/monitorArchExp.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<!-- Componente titulo de p�gina -->
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	



	<!-- Componente tabla de varias columnas -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${subtituloFuncion}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th  width="122" class="text_izquierda">${modulo}</th>
					<th  width="223" class="text_centro" scope="col">${subModulo}</th>
					<th  width="122" class="text_centro" scope="col">${nomArchivo}</th>
					<th  width="122" class="text_centro" scope="col">${estatus}</th>
					<th  width="122" class="text_centro" scope="col">${numRegistros}</th>
					<th  width="122" class="text_centro" scope="col">${fechaCarga}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanConsMonitorArchExp" items="${resMon.listBeanConsMonitorArchExp}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda">${beanConsMonitorArchExp.modulo}</td>
						<td class="text_izquierda">${beanConsMonitorArchExp.subModulo}</td>
						<td class="text_izquierda">${beanConsMonitorArchExp.nomArchivo}</td>
						<td class="text_derecha">${beanConsMonitorArchExp.estatus}</td>
						<td class="text_izquierda">${beanConsMonitorArchExp.numRegistros}</td>
						<td class="text_derecha">${beanConsMonitorArchExp.fechaCarga}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
			</div>
			<c:if test="${not empty resMon.listBeanConsMonitorArchExp}">
			<div class="paginador">
				<c:if test="${resMon.beanPaginador.paginaIni == resMon.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaIni != resMon.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorArchExp.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaAnt!='0' && resMon.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorArchExp.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaAnt=='0' || resMon.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${resMon.beanPaginador.pagina} - ${resMon.beanPaginador.paginaFin}</label>
				<c:if test="${resMon.beanPaginador.paginaFin != resMon.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorArchExp.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaFin == resMon.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaFin != resMon.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarMonitorArchExp.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${resMon.beanPaginador.paginaFin == resMon.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		</c:if>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<td width="279" class="cero">&nbsp;</td>
						<td width="6" class="odd"></td>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<form name="idForm" id="idForm" method="post" action="">
		<input type="hidden" name="paginaIni" id="paginaIni" value="${resMon.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${resMon.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${resMon.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		
	</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>

		
	
