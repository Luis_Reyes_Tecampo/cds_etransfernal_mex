<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatHorariosSPEI" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
	
<spring:message code="moduloCDA.consultaHorariosSPEI.text.titulo" var="titulo" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na" var="na" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.default" var="lbDefault" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.opTopVA" var="opTopVA" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.opTopVB" var="opTopVB" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.opTopTA" var="opTopTA" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.opTopTB" var="opTopTB" />

<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveMedEnt" var="cveMedEnt" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveTransfer" var="cveTransfer" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveOpera" var="cveOpera" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.topologiaOp" var="topologiaOp" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.horaInicio" var="horaInicio" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.horaCierre" var="horaCierre" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.banderaDiaHabil" var="banderaDiaHabil" />

<spring:message code="general.aceptar" var="aceptar" />
<spring:message code="general.cancelar" var="cancelar" />
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="codError.CD00040V" var="CD00040V"/>
<spring:message code="codError.CD00041V" var="CD00041V"/>
<spring:message code="codError.CD00042V" var="CD00042V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.ED00082V" var="ED00082V"/>
<spring:message code="codError.ED00083V" var="ED00083V"/>
<spring:message code="codError.ED00084V" var="ED00084V"/>



	<c:set var="searchString" value="${seguTareas}"/>
	<script src="${pageContext.servletContext.contextPath}/js/private/altaModifHorariosSPEI.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
	

	<script type="text/javascript">
		var mensajes = {"aplicacion": '${aplicacion}',"CD00040V":'${CD00040V}',
                        "CD00041V":'${CD00041V}',"CD00042V":'${CD00042V}',
                        "ED00082V":'${ED00082V}',"ED00083V":'${ED00083V}',
                        "ED00084V":'${ED00084V}'
                        };
    </script>
    

<c:set var="searchString" value="${seguTareas}"/>

	<%-- Componente titulo de pagina --%>
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>

<form name="idForm" id="idForm" method="post">	
<input type="hidden" name="altaMod" id="altaMod" value="${altaMod}"/>
<input type="hidden" name="auxCveMedios" id="auxCveMedios" value="${horarioSPEI.cveMedioEnt}"/>
<input type="hidden" name="auxCveTransfer" id="auxCveTransfer" value="${horarioSPEI.cveTransfe}"/>
<input type="hidden" name="auxCveOper" id="auxCveOper" value="${horarioSPEI.cveOperacion}"/>
	<%-- Componente buscador simple --%>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>
					<tr>
						<td class="text_izquierda">
						${cveMedEnt}
						</td>
						<td>
							<select name="selMedEntrega" class="Campos" id="selMedEntrega" ${'UPDATE' == altaMod ? 'disabled' : ''}>
	          					<option value="">${na}</option> 
	          					<option value="00"  ${'00' == horarioSPEI.cveMedioEnt ? 'selected' : ''}>${lbDefault}</option>
	          					<c:forEach var="reg" items="${lstMedEntrega}">
	           						<option value="${reg.cve}" ${reg.cve == horarioSPEI.cveMedioEnt ? 'selected' : ''} >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
							${cveTransfer} 
						</td>
						<td>     
							<select name="selTransfer" class="Campos" id="selTransfer" ${'UPDATE' == altaMod ? 'disabled' : ''}>
	          					<option value="">${na}</option> 
	          					<option value="00"  ${'00' == horarioSPEI.cveTransfe ? 'selected' : ''}>${lbDefault}</option>
	          					<c:forEach var="reg" items="${lstTransfer}">
	           						<option value="${reg.cve}" ${reg.cve == horarioSPEI.cveTransfe ? 'selected' : ''}  >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						${cveOpera}
						</td>
						<td>
							<select name="selOperacion" class="Campos" id="selOperacion" ${'UPDATE' == altaMod ? 'disabled' : ''}>
	          					<option value="">${na}</option> 
	          					<option value="00"  ${'00' == horarioSPEI.cveOperacion ? 'selected' : ''}>${lbDefault}</option>
	          					<c:forEach var="reg" items="${lstOperacion}">
	           						<option value="${reg.cve}" ${reg.cve == horarioSPEI.cveOperacion ? 'selected' : ''} >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
						</td>	
					</tr>
					<tr>
						<td class="text_izquierda">
						${topologiaOp}
						</td>
						<td>
							<select name="selTopo" class="Campos" id="selTopo">
	          					<option value="">${na}</option> 
	          					<option value="00"  ${"00" == horarioSPEI.cveTopoPri ? "selected" : ""}>${lbDefault}</option>
	          					<option value="${opTopVA}" ${"VA" == horarioSPEI.cveTopoPri ? "selected" : ""}>${opTopVA}</option> 
	          					<option value="${opTopVB}" ${"VB" == horarioSPEI.cveTopoPri ? "selected" : ""}>${opTopVB}</option> 
	          					<option value="${opTopTA}" ${"TA" == horarioSPEI.cveTopoPri ? "selected" : ""}>${opTopTA}</option> 
	          					<option value="${opTopTB}" ${"TB" == horarioSPEI.cveTopoPri ? "selected" : ""}>${opTopTB}</option>              				                        		
	             			</select>
						</td>	
					</tr>
					<tr>
						<td class="text_izquierda">
						${horaInicio}
						</td>
						<td>
						
						<input id="horaInicio" value="${horarioSPEI.horaIncio}" class="Campos" type="text" size="6" maxlength="5" name="horaInicio"/>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						${horaCierre}
						</td>
						<td>
							<input id="horaCierre" value="${horarioSPEI.horaCierre}" class="Campos" type="text" size="6" maxlength="5" name="horaCierre"/>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						${banderaDiaHabil}
						</td>
						<td>
							<input type="checkbox" class="Campos" maxlength="1" size="15" name="banderaDiaHabil" id="banderaDiaHabil" value="1" ${1==horarioSPEI.flgInhabil?'checked':''}/>
						</td>	
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	


	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
			
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td width="279" class="izq"><a id="idAceptar" href="javascript:;">${aceptar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${aceptar}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der">
							<a id="idCancelar" href="javascript:;">${cancelar}</a>
						</td>
					</tr>
				</table>
				
			</div>
		</div>
		
	</div>

		</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
	