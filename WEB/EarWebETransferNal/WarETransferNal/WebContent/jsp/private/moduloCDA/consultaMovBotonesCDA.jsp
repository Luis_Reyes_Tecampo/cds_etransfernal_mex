<!DOCTYPE jsp:include PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.limpiar" var="limpiar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.exportar" var="exportar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.exportarTodo" var="exportarTodo"/>

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${param.tareaExportar}">
								<td class="izq"><a id="idExportar" href="javascript:;"><label style="font-size: 11px;">${exportar}</label></a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;"><label style="font-size: 11px;">${exportar}</label></a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
						<c:when test="${param.tareaConsultar}">
							<td width="279" class="der"><a id="idActualizar" href="javascript:;" ><label style="font-size: 11px;">${actualizar}</label></a></td>
						</c:when>
						<c:otherwise>
							<td width="279" class="der_Des"><a href="javascript:;" ><label style="font-size: 11px;">${actualizar}</label></a></td>
						</c:otherwise>
					</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${param.tareaExportar}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;"><label style="font-size: 11px;">${exportarTodo}</label></a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;"><label style="font-size: 11px;">${exportarTodo}</label></a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idLimpiar" href="javascript:;"><label style="font-size: 11px;">${limpiar}</label></a></td>
					</tr>
				</table>

			</div>
		</div>
