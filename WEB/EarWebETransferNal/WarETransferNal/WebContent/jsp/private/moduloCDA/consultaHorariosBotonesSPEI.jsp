<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.buscar" var="buscar" />
<spring:message code="general.alta" var="alta" />
<spring:message code="general.modificar" var="modificar" />
<spring:message code="general.eliminar" var="eliminar" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>


		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idAlta" href="javascript:;">${alta}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${alta}</a></td>
							</c:otherwise>
						</c:choose>
				
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
								<td width="279" class="der"><a id="idModificar" href="javascript:;">${modificar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${modificar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
								<td width="279" class="izq"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero"></td>
					</tr>
					
				</table>
				
			</div>
		</div>