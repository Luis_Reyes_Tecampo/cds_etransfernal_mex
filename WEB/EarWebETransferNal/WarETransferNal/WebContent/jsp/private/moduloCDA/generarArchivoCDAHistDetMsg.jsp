<!DOCTYPE jsp:include PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.CD00040V" var="CD00040V" />
<spring:message code="codError.CD00030V" var="CD00030V" />
<spring:message code="codError.ED00026V" var="ED00026V" />
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.OK00000V" var="OK00000V" />
<script type="text/javascript">
	var mensajes = new Array();
	mensajes["aplicacion"] = '${aplicacion}';
	mensajes["CD00030V"] = '${CD00030V}';
	mensajes["CD00040V"] = '${CD00040V}';
	mensajes["CD00010V"] = '${CD00010V}';
	mensajes["ED00026V"] = '${ED00026V}';
	mensajes["EC00011B"] = '${EC00011B}';
	mensajes["OK00000V"] = '${OK00000V}';
</script>
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
	
	<c:if test="${codError!=''}">
	<c:choose>
		<c:when test="${codError eq 'EC00011B'}">
			<script type="text/javascript" defer="defer">
			 	jError(mensajes['EC00011B'], '${aplicacion}', 'EC00011B', '', '');
			</script>
		</c:when>
		<c:when test="${codError eq 'ED00026V' && msgElimina eq 'E'}">
			<script type="text/javascript" defer="defer">
			 	jError(mensajes['ED00026V'], '${aplicacion}', 'ED00026V', '', '', '');	
			</script>
		</c:when>
		<c:when test="${codError eq 'OK00000V' && msgElimina eq 'E'}">
			<script type="text/javascript" defer="defer">
			 	jInfo(mensajes['OK00000V'], '${aplicacion}', 'OK00000V', '', '', '');
			</script>
		</c:when>
	</c:choose>
</c:if>