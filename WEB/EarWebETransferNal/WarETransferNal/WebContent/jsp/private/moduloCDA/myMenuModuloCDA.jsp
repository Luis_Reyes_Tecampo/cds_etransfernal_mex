	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
	<%@ page import="mx.isban.agave.configuracion.Configuracion"%>

	<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"           type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>
	<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css">
	<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css">

	<spring:message code="moduloCDA.myMenu.text.moduloCDA" var="moduloCDA"/>
	<spring:message code="moduloCDA.myMenu.text.monitorCDA" var="monitorCDA"/>
	<spring:message code="moduloCDA.myMenu.text.consultaMovCDA" var="consultaMovCDA"/>
	<spring:message code="moduloCDA.myMenu.text.consultaMovHistCDA" var="consultaMovHistCDA"/>
	<spring:message code="moduloCDA.myMenu.text.contingenciaCDAMismoDia" var="contingenciaCDAMismoDia"/>
	<spring:message code="moduloCDA.myMenu.text.generarArchivoCDAHist" var="generarArchivoCDAHist"/>
	<spring:message code="moduloCDA.myMenu.text.monitorCargaArchHist" var="monitorCargaArchHist"/>
	<spring:message code="moduloCDA.myMenu.text.bitacoraAdmon" var="bitacoraAdmon"/>
	<spring:message code="moduloCDA.myMenu.text.monitorArchExp" var="monitorArchExp"/>
	
	<c:set var="searchString" value="${seguServicios}" />
	
	<!-- <body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); addMenuItem('eight','Mi opcion dinamica','','', 'true', 'true'); disabledMenuItem('three'); disabledMenuItem('fiveDotTwo');"> -->
<body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
			
					<li id="principal"   class="startMenuGroup">             <a href="${pageContext.servletContext.contextPath}/principal/inicio.do">             <span>Principal</span></a></li>					
				
					<li id="moduloCDA"   class="startMenuGroup">             <a href="${pageContext.servletContext.contextPath}/moduloCDA/moduloCDAInit.do">             <span>${moduloCDA}</span></a></li>					
				
				
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'consInfMonCDA.do')}">
							<li id="consInfMonCDA"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/consInfMonCDA.do">&gt;
							<span class="subMenuText">${monitorCDA}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="consInfMonCDA"><a
								href="javascript:;">
							<span class="subMenuText">${monitorCDA}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraConsMovCDA.do')}">
							<li id="muestraConsMovCDA"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraConsMovCDA.do">&gt;
							<span class="subMenuText">${consultaMovCDA}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="consInfMonCDA"><a
								href="javascript:;">
							<span class="subMenuText">${consultaMovCDA}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraConsMovHistCDA.do')}">
							<li id="muestraConsMovHistCDA"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraConsMovHistCDA.do">&gt;
							<span class="subMenuText">${consultaMovHistCDA}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="consInfMonCDA"><a
								href="javascript:;">
							<span class="subMenuText">${consultaMovHistCDA}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraContingCDAMismoDia.do')}">
							<li id="muestraContingCDAMismoDia"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraContingCDAMismoDia.do">&gt;
							<span class="subMenuText">${contingenciaCDAMismoDia}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="muestraContingCDAMismoDia"><a
								href="javascript:;">
							<span class="subMenuText">${contingenciaCDAMismoDia}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraGenerarArchCDAHist.do')}">
							<li id="muestraGenerarArchCDAHist"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraGenerarArchCDAHist.do">&gt;
							<span class="subMenuText">${generarArchivoCDAHist}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="muestraGenerarArchCDAHist"><a
								href="javascript:;">
							<span class="subMenuText">${generarArchivoCDAHist}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraMonitorCargaArchHist.do')}">
							<li id="muestraMonitorCargaArchHist"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraMonitorCargaArchHist.do">&gt;
							<span class="subMenuText">${monitorCargaArchHist}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="muestraMonitorCargaArchHist"><a
								href="javascript:;">
							<span class="subMenuText">${monitorCargaArchHist}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraBitacoraAdmon.do')}">
							<li id="muestraBitacoraAdmon"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraBitacoraAdmon.do">&gt;
							<span class="subMenuText">${bitacoraAdmon}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="muestraBitacoraAdmon"><a
								href="javascript:;">
							<span class="subMenuText">${bitacoraAdmon}</span></a></li>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${fn:containsIgnoreCase(searchString, 'muestraMonitorArchExp.do')}">
							<li id="muestraMonitorArchExp"><a
								href="${pageContext.servletContext.contextPath}/moduloCDA/muestraMonitorArchExp.do">&gt;
							<span class="subMenuText">${monitorArchExp}</span></a></li>
						</c:when>
						<c:otherwise>
							<li id="muestraMonitorArchExp"><a
								href="javascript:;">
							<span class="subMenuText">${monitorArchExp}</span></a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			
				<div id="menuFooter">
					<div></div>
				</div>
	
			</div>
		</c:if>
	</div>
<!-- </div>  -->
<!-- </body> -->
<!-- </html> -->
<div id="content_container">
