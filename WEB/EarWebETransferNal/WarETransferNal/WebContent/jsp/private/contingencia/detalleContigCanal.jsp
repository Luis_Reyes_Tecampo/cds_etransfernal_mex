<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<jsp:include page="../myHeader.jsp" flush="true"/>

<c:if  test="${beanResTranCanales.nomPantalla eq 'normal' }" >
	<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
		<jsp:param name="menuItem"    value="contingencia" />				
		<jsp:param name="menuSubitem" value="operacionesContingentes" />			
	</jsp:include>
</c:if>	

<c:if  test="${beanResTranCanales.nomPantalla eq 'historica' }" >
	<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
		<jsp:param name="menuItem"    value="contingencia" />				
		<jsp:param name="menuSubitem" value="operacionesContingentesHto" />			
	</jsp:include>
</c:if>	
	
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
	<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/contingencia/detalleOperacion.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<spring:message code="general.nombreAplicacion" var="app"/>
		<spring:message code="general.bienvenido"       var="welcome"/>
		
		<spring:message code="general.inicio" var="inicio"/>
		<spring:message code="general.fin" var="fin"/>
		<spring:message code="general.anterior" var="anterior"/>
		<spring:message code="general.siguiente" var="siguiente"/>
		<spring:message code="general.exportar" var="exportar"/>
		<spring:message code="general.exportarAll" var="exportarAll"/>
		<spring:message code="general.regresar" var="regresar"/>
		<spring:message code="general.Actualizar" var="actualizar"/>
		
		<spring:message code="moduloContingencia.nombrearch" var="nombrearch"/>  
		<spring:message code="moduloContingencia.secuenciaregarch" var="secuenciaregarch"/>
		<spring:message code="moduloContingencia.cveempresa" var="cveempresa"/>
		<spring:message code="moduloContingencia.cveptovta" var="cveptovta"/>
		<spring:message code="moduloContingencia.cvemedioent" var="cvemedioent"/> 
		<spring:message code="moduloContingencia.referenciamed" var="referenciamed"/>
		<spring:message code="moduloContingencia.fchcaptura" var="fchcaptura"/>
		<spring:message code="moduloContingencia.cveusuariocap" var="cveusuariocap"/>
		<spring:message code="moduloContingencia.cveusuariosol" var="cveusuariosol"/>    
		<spring:message code="moduloContingencia.cvetransfe" var="cvetransfe"/>
		<spring:message code="moduloContingencia.cvedivisaord" var="cvedivisaord"/>
		<spring:message code="moduloContingencia.nombreord" var="nombreord"/>
		<spring:message code="moduloContingencia.importecargo" var="importecargo"/>       
		<spring:message code="moduloContingencia.importedls" var="importedls"/>
		<spring:message code="moduloContingencia.cveintermerec" var="cveintermerec"/>
		<spring:message code="moduloContingencia.numcuentarec" var="numcuentarec"/>
		<spring:message code="moduloContingencia.cveptovtarec" var="cveptovtarec"/>       
		<spring:message code="moduloContingencia.cvedivisarec" var="cvedivisarec"/>
		<spring:message code="moduloContingencia.nombrerec" var="nombrerec"/>
		<spring:message code="moduloContingencia.cveaba" var="cveaba"/>
		<spring:message code="moduloContingencia.plazabanxico" var="plazabanxico"/>
		<spring:message code="moduloContingencia.nombrebcorec" var="nombrebcorec"/>
		<spring:message code="moduloContingencia.sucursalbcorec" var="sucursalbcorec"/>
		<spring:message code="moduloContingencia.ciudadbcorec" var="ciudadbcorec"/>  
		<spring:message code="moduloContingencia.cvepaisbcorec" var="cvepaisbcorec"/>
		<spring:message code="moduloContingencia.importeabono" var="importeabono"/>
		<spring:message code="moduloContingencia.comentario1" var="comentario1"/>
		<spring:message code="moduloContingencia.comentario2" var="comentario2"/>      
		<spring:message code="moduloContingencia.comentario3" var="comentario3"/>
		<spring:message code="moduloContingencia.cveswiftcor" var="cveswiftcor"/>
		<spring:message code="moduloContingencia.detenido" var="detenido"/>
		<spring:message code="moduloContingencia.estatus" var="estatus"/>  
		<spring:message code="moduloContingencia.referenciameca" var="referenciameca"/>
		<spring:message code="moduloContingencia.referenciacon" var="referenciacon"/>
		<spring:message code="moduloContingencia.proteccion" var="proteccion"/>
		<spring:message code="moduloContingencia.dispositivo" var="dispositivo"/>  
		<spring:message code="moduloContingencia.cvedevolucion" var="cvedevolucion"/>
		<spring:message code="moduloContingencia.referenciadep" var="referenciadep"/>
		<spring:message code="moduloContingencia.estatusservidor" var="estatusservidor"/>
		<spring:message code="moduloContingencia.importecomision" var="importecomision"/> 
		<spring:message code="moduloContingencia.cvedivisacom" var="cvedivisacom"/>
		<spring:message code="moduloContingencia.fchaplicacion" var="fchaplicacion"/>
		<spring:message code="moduloContingencia.buccliente" var="buccliente"/>
		<spring:message code="moduloContingencia.tipoidord" var="tipoidord"/>    
		<spring:message code="moduloContingencia.idord" var="idord"/>
		<spring:message code="moduloContingencia.tipoidrec" var="tipoidrec"/>
		<spring:message code="moduloContingencia.idrec" var="idrec"/>
		<spring:message code="moduloContingencia.tipoidrec2" var="tipoidrec2"/>       
		<spring:message code="moduloContingencia.importeiva" var="importeiva"/>
		<spring:message code="moduloContingencia.refenumerica" var="refenumerica"/>
		<spring:message code="moduloContingencia.refeadicional" var="refeadicional"/>
		<spring:message code="moduloContingencia.tipocuentaord" var="tipocuentaord"/>
		<spring:message code="moduloContingencia.tipocuentarec" var="tipocuentarec"/>       
		<spring:message code="moduloContingencia.motivodevol" var="motivodevol"/>
		<spring:message code="moduloContingencia.codpostalord" var="codpostalord"/>
		<spring:message code="moduloContingencia.fchconstitord" var="fchconstitord"/>
		<spring:message code="moduloContingencia.fchinstrucpago" var="fchinstrucpago"/>
		<spring:message code="moduloContingencia.horainstrucpago" var="horainstrucpago"/>
		<spring:message code="moduloContingencia.rfcord" var="rfcord"/>
		<spring:message code="moduloContingencia.rfcrec" var="rfcrec"/>  
		<spring:message code="moduloContingencia.cverastreo" var="cverastreo"/>
		<spring:message code="moduloContingencia.direccionip" var="direccionip"/>
		<spring:message code="moduloContingencia.domicilioord" var="domicilioord"/>
		<spring:message code="moduloContingencia.conceptopago" var="conceptopago"/>      
		<spring:message code="moduloContingencia.foliopaquete" var="foliopaquete"/>
		<spring:message code="moduloContingencia.foliopago" var="foliopago"/>
		<spring:message code="moduloContingencia.diripgentran" var="diripgentran"/>
		<spring:message code="moduloContingencia.diripfirma" var="diripfirma"/>  
		<spring:message code="moduloContingencia.canalfirma" var="canalfirma"/>
		<spring:message code="moduloContingencia.uetrswift" var="uetrswift"/>
		<spring:message code="moduloContingencia.camposwift1" var="camposwift1"/>
		<spring:message code="moduloContingencia.camposswift2" var="camposswift2"/>  
		<spring:message code="moduloContingencia.cveptovtaord" var="cveptovtaord"/>
		<spring:message code="moduloContingencia.cveintermeord" var="cveintermeord"/>
		<spring:message code="moduloContingencia.numcuentaord" var="numcuentaord"/>
		<spring:message code="moduloContingencia.cveoperacion" var="cveoperacion"/> 
		<spring:message code="moduloContingencia.formaliq" var="formaliq"/>
		<spring:message code="moduloContingencia.tipocuentarec2" var="tipocuentarec2"/>
		<spring:message code="moduloContingencia.cvedivisa" var="cvedivisa"/>
		<spring:message code="moduloContingencia.numcuentarec2" var="numcuentarec2"/>    
		<spring:message code="moduloContingencia.importe" var="importe"/>
		<spring:message code="moduloContingencia.nombrerec2" var="nombrerec2"/>
		<spring:message code="moduloContingencia.rfcrec2" var="rfcrec2"/>
		<spring:message code="moduloContingencia.conceptopago2" var="conceptopago2"/>       
		<spring:message code="moduloContingencia.comentario4" var="comentario4"/>
		<spring:message code="moduloContingencia.refetransfer" var="refetransfer"/>
		<spring:message code="moduloContingencia.coderror" var="coderror"/>
		<spring:message code="moduloContingencia.estatusinicial" var="estatusinicial"/>       
		<spring:message code="moduloContingencia.mensaje" var="mensaje"/>
		<spring:message code="menu.operacionesContingentes.text.tituloHtoDet" var="tituloHtoDet"/>
		<spring:message code="menu.operacionesContingentes.text.tituloDet" var="tituloDet"/>
		<spring:message code="menu.moduloContingencia.text.titulo" var="labtituloModulo"/>
		
		
		<spring:message code="codError.ED00011V" var="ED00011V"/>
		<spring:message code="codError.OK00000V" var="OK00000V"/>
		<spring:message code="codError.EC00011B" var="EC00011B"/>
		<spring:message code="codError.OK00001V" var="OK00001V"/>
		<spring:message code="codError.OK00002V" var="OK00002V"/>
		
		
		<script type="text/javascript">
			var mensajes = {"aplicacion": '${aplicacion}',
		                    "ED00011V":'${ED00011V}',
		                    "OK00002V":'${OK00002V}',
		                    "OK00000V":'${OK00000V}',
		                    "EC00011B":'${EC00011B}',
		                    "OK00001V":'${OK00001V}'
		                    };
		                    
		    var tipoOrden = '${tipoOrden}';	 
		</script>

<c:set var="searchString" value="${seguTareas}"/>
	
	<div class="pageTitleContainer">
		<c:choose>								
			<c:when test="${beanResTranCanales.nomPantalla eq 'normal' }">
				<span class="pageTitle">${labtituloModulo}</span> - ${tituloDet}			
			</c:when>
			<c:otherwise>			
				<span class="pageTitle">${labtituloModulo}</span> - ${tituloHtoDet}			
			</c:otherwise>
		</c:choose>	
	</div>
	
		
		
		<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="nombreArch" id="nombreArch" value="${nombreArchivo}"/>
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResTranCanales.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResTranCanales.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResTranCanales.beanPaginador.paginaFin}"/>
	<input type="hidden" name="field" id="field" value="${beanResTranCanales.beanFilter.field}" />
	<input type="hidden" name="fieldValue" id="fieldValue" value="${beanResTranCanales.beanFilter.fieldValue}"/>
	<input type="hidden" name="accion" id="accion" value="" />
	<input type="hidden" id="nomPantalla" name="nomPantalla" value="${beanResTranCanales.nomPantalla}"/>
	<input type="hidden" id="fechaOperacion" name="fechaOperacion" value="${beanResTranCanales.fechaOperacion}"/>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table id="tablaDatosListado" name="tablaDatosListado">
					<tr>
						<th class="text_centro filField" scope="col">${cveempresa} <br/><input name="cveempresaInput" id="cveempresaInput" type="text" data-field="CVE_EMPRESA" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="3"></input></th>
						<th class="text_centro filField" scope="col">${cvetransfe} <br/><input name="cvetransfeInput" id="cvetransfeInput" type="text" data-field="CVE_TRANSFE" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="3"></input></th>
						<th class="text_centro filField" scope="col">${cvemedioent} <br/><input name="cvemedioentInput" id="cvemedioentInput" type="text" data-field="CVE_MEDIO_ENT" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="4"></input></th>
						<th class="text_centro filField" scope="col">${cveptovta} <br/><input name="cveptovtaInput" id="cveptovtaInput" type="text" data-field="CVE_PTO_VTA" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="4"></input></th>
						<th class="text_centro filField" scope="col">${cveptovtaord} <br/><input name="cveptovtaordInput" id="cveptovtaordInput" type="text" data-field="CVE_PTO_VTA_ORD" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="4"></input></th>
						<th class="text_centro filField" scope="col">${cvedivisa} <br/><input name="cvedivisaInput" id="cvedivisaInput" type="text" data-field="CVE_DIVISA" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="4"></input></th>
						<th class="text_centro filField" scope="col">${cveintermeord} <br/><input name="cveintermeordInput" id="cveintermeordInput" type="text" data-field="CVE_INTERME_ORD" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="5"></input></th>
						<th class="text_centro filField" scope="col">${cveintermerec} <br/><input name="cveintermerecInput" id="cveintermerecInput" type="text" data-field="CVE_INTERME_REC" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="5"></input></th>
						<th class="text_centro filField" scope="col">${cveoperacion} <br/><input name="cveoperacionInput" id="cveoperacionInput" type="text" data-field="CVE_OPERACION" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="8"></input></th>
						<th class="text_centro filField" scope="col">${numcuentaord} <br/><input name="numcuentaordInput" id="numcuentaordInput" type="text" data-field="NUM_CUENTA_ORD" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="20"></input></th>
						<th class="text_centro filField" scope="col">${numcuentarec} <br/><input name="numcuentarecInput" id="numcuentarecInput" type="text" data-field="NUM_CUENTA_REC" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="20"></input></th>
						<th class="text_centro filField" scope="col">${numcuentarec2} <br/><input name="numcuentarec2Input" id="numcuentarec2Input" type="text" data-field="NUM_CUENTA_REC2" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="35"></input></th>
						<th class="text_centro filField" scope="col">${referenciamed} <br/><input name="referenciamedInput" id="referenciamedInput" type="text" data-field="REFERENCIA_MED" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="12"></input></th>
						<th class="text_centro filField" scope="col">${importe} <br/><input name="importeInput" id="importeInput" type="text" data-field="IMPORTE" class="filField validaEntrada" onkeypress="return validaEntradaNumeros(event);" maxlength="16"></input></th>
						<th class="text_centro filField" scope="col">${nombreord} <br/><input name="nombreordInput" id="nombreordInput" type="text" data-field="NOMBRE_ORD" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="50"></input></th>
						<th class="text_centro filField" scope="col">${nombrerec} <br/><input name="nombrerecInput" id="nombrerecInput" type="text" data-field="NOMBRE_REC" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="50"></input></th>
						<th class="text_centro filField" scope="col">${nombrerec2} <br/><input name="nombrerec2Input" id="nombrerec2Input" type="text" data-field="NOMBRE_REC2" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="40"></input></th>
						<th class="text_centro filField" scope="col">${conceptopago2} <br/><input name="conceptopago2Input" id="conceptopago2Input" type="text" data-field="CONCEPTO_PAGO2" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="40"></input></th>
						<th class="text_centro filField" scope="col">${buccliente} <br/><input name="bucclienteInput" id="bucclienteInput" type="text" data-field="BUC_CLIENTE" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="8"></input></th>
						<th class="text_centro filField" scope="col">${importeiva} <br/><input name="importeivaInput" id="importeivaInput" type="text" data-field="IMPORTE_IVA" class="filField validaEntrada" onkeypress="return validaEntradaNumeros(event);" maxlength="16"></input></th>
						<th class="text_centro filField" scope="col">${rfcord} <br/><input name="rfcOrdenante" id="rfcOrdenante" type="text" data-field="RFC_ORD" class="filField validaEntrada" onkeypress="return validaEntradaNumeros(event);" maxlength="16"></input></th>
						<th class="text_centro filField" scope="col">${rfcrec} <br/><input name="rfcBeneficiario" id="rfcBeneficiario" type="text" data-field="RFC_REC" class="filField validaEntrada" onkeypress="return validaEntradaNumeros(event);" maxlength="16"></input></th>
						<th class="text_centro filField" scope="col">${refenumerica} <br/><input name="refenumericaInput" id="refenumericaInput" type="text" data-field="REFE_NUMERICA" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="20"></input></th>
						<th class="text_centro filField" scope="col">${uetrswift} <br/><input name="uetrswiftInput" id="uetrswiftInput" type="text" data-field="UETR_SWIFT" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="36"></input></th>
						<th class="text_centro filField" scope="col">${camposwift1} <br/><input name="camposwift1Input" id="camposwift1Input" type="text" data-field="CAMPO_SWIFT1" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="40"></input></th>
						<th class="text_centro filField" scope="col">${camposswift2} <br/><input name="camposwift2Input" id="camposwift2Input" type="text" data-field="CAMPO_SWIFT2" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="40"></input></th>
						<th class="text_centro filField" scope="col">${refeadicional} <br/><input name="refeadicional1Input" id="refeadicional1Input" type="text" data-field="REFE_ADICIONAL1" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="40"></input></th>
						<th class="text_centro filField" scope="col">${coderror} <br/><input name="coderrorInput" id="coderrorInput" type="text" data-field="COD_ERROR" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="8"></input></th>
						
						<th class="text_centro filField" scope="col">${refetransfer} <br/><input name="refeTransfer" id="refeTransfer" type="text" data-field="REFE_TRANSFER" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="8"></input></th>
						
						<th class="text_centro filField" scope="col">${estatusinicial} <br/><input name="estatusinicialInput" id="estatusinicialInput" type="text" data-field="ESTATUS_INICIAL" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="2"></input></th>
						<th class="text_centro filField" scope="col">${mensaje} <br/><input name="mensajeInput" id="mensajeInput" type="text" data-field="MENSAJE" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="80"></input></th>
						<th class="text_centro filField" scope="col">${tipocuentaord} <br/><input name="tipocuentaordInput" id="tipocuentaordInput" type="text" data-field="TIPO_CUENTA_ORD" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="2"></input></th>
						<th class="text_centro filField" scope="col">${tipocuentarec} <br/><input name="tipocuentarecInput" id="tipocuentarecInput" type="text" data-field="TIPO_CUENTA_REC" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="2"></input></th>
						<th class="text_centro filField" scope="col">${tipocuentarec2} <br/><input name="tipocuentarec2Input" id="tipocuentarec2Input" type="text" data-field="TIPO_CUENTA_REC2" class="filField validaEntrada" onkeypress="return validaEntrada(event);" onkeyup="this.value = this.value.toUpperCase();" maxlength="2"></input></th>
						
												
					</tr>
					<tbody>				
						<c:forEach var="reg" items="${beanResTranCanales.registros}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								
								<td class="text_centro">${reg.beanClaves.cveEmpresa}</td>
								<td class="text_centro">${reg.beanClaves.cveTransfe}</td>
								<td class="text_centro">${reg.beanClaves.cveMedioEnt}</td>
								<td class="text_centro">${reg.beanClaves.cvePtoVta}</td>
								<td class="text_centro">${reg.beanDatosOrdenante.cvePtoVtaOrd}</td>
								<td class="text_centro">${reg.beanClaves.cveDivisa}</td>
								<td class="text_centro">${reg.beanDatosOrdenante.cveIntermeOrd}</td>
								<td class="text_centro">${reg.beanDatosReceptor.cveIntermeRec}</td>
								<td class="text_centro">${reg.beanClaves.cveOperacion}</td>
								<td class="text_centro">${reg.beanDatosOrdenante.numCuentaOrd}</td>
								<td class="text_centro">${reg.beanDatosReceptor.numCuentaRec}</td>
								<td class="text_centro">${reg.beanDatosReceptor.numCuentaRec2}</td>
								<td class="text_centro">${reg.beanDatosGenerales.referenciaMed}</td>
								<td class="text_centro">${reg.beanDatosGenerales.importe}</td>
								<td class="text_centro">${reg.beanDatosOrdenante.nombreOrd}</td>
								<td class="text_centro">${reg.beanDatosReceptor.nombreRec}</td>
								<td class="text_centro">${reg.beanDatosReceptor.nombreRec2}</td>
								<td class="text_centro">${reg.beanDatosGenerales.conceptoPago2}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.bucCliente}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.importeIva}</td>								
								<td class="text_centro">${reg.beanDatosOrdenante.rfcOrd}</td>
								<td class="text_centro">${reg.beanDatosReceptor.rfcRec}</td>							
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.refeNumerica}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.uetrSwift}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.campoSwift1}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.campoSwift2}</td>
								<td class="text_centro">${reg.beanDatosGenerales.beanDatosGeneralesExt.refeAdicional1}</td>
								<td class="text_centro">${reg.beanDatosOperacion.codError}</td>
								<td class="text_centro">${reg.beanDatosGenerales.refeTransfer}</td>
								<td class="text_centro">${reg.beanDatosOperacion.estatusInicial}</td>
								<td class="text_centro">${reg.beanDatosOperacion.mensaje}</td>
								<td class="text_centro">${reg.beanDatosOrdenante.tipoCuentaOrd}</td>
								<td class="text_centro">${reg.beanDatosReceptor.tipoCuentaRec}</td>
								<td class="text_centro">${reg.beanDatosReceptor.tipoCuentaRec2}</td>
							</tr>
						</c:forEach>
						<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
					</tbody>
				</table>
			</div>
		<c:if test="${not empty beanResTranCanales.registros}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${beanResTranCanales.beanPaginador.regIni} al ${beanResTranCanales.beanPaginador.regFin} de un total de ${beanResTranCanales.totalReg} registros | Importe p�gina: ${importePagina}</label>
				</div>
				<c:if test="${beanResTranCanales.beanPaginador.paginaIni == beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaIni != beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','detalleOperacion.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaAnt!='0' && beanResTranCanales.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','detalleOperacion.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaAnt=='0' || beanResTranCanales.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResTranCanales.beanPaginador.pagina} - ${beanResTranCanales.beanPaginador.paginaFin}</label>
				<c:if test="${beanResTranCanales.beanPaginador.paginaFin != beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','detalleOperacion.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaFin == beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaFin != beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','detalleOperacion.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResTranCanales.beanPaginador.paginaFin == beanResTranCanales.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${beanResTranCanales.importeTotalActual}</label>
		</c:if>
		<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResTranCanales.registros}">
									<td width="279" class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${exportar}</a></td>
								</c:otherwise>
							</c:choose>							
							<td width="279" class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR') and not empty beanResTranCanales.registros}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>							
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResTranCanales.registros}">
									<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarAll}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${exportarAll}</a></td>
								</c:otherwise>
							</c:choose>							
							<td width="279" class="odd"></td>
							<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
</form>
	
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>