<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<c:if test="${beanDatos.parametro eq 'RECEPCION'}">
	<jsp:include page="../myHeader.jsp" flush="true" />
	<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
		<jsp:param name="menuItem" value="moduloSPEI" />
		<jsp:param name="menuSubitem" value="recepcionOperaSPEI" />
	</jsp:include>
	<spring:message code="moduloSPEI.recepOperacion.text.tituloDet" var="titulo"/>
</c:if>

<c:if test="${beanDatos.parametro eq 'RECEPCIONHTO'}">
	<jsp:include page="../myHeader.jsp" flush="true" />
	<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
		<jsp:param name="menuItem" value="moduloSPEI" />
		<jsp:param name="menuSubitem" value="recepcionOperaHistoricaSPEI" />
	</jsp:include>
	<spring:message code="moduloSPEI.recepOperacion.text.tituloDetHto" var="titulo"/>
</c:if>


<spring:message code="moduloSPEI.recepOperacion.text.tituloModulo" var="tituloModulo"/>


<span class="pageTitle">${tituloModulo}</span> - ${titulo}

<jsp:include page="../admonSaldo/estructura/detRecepcionOperacionCont.jsp" flush="true"/>