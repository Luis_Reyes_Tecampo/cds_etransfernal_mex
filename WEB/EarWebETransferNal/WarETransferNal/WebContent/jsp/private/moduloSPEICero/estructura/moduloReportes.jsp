<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="SPEICero.reporteEmision.txt.filtroBusq" var="filtroBusq"/>
<spring:message code="SPEICero.reporteEmision.txt.instPrincipal" var="instPrincipal"/>
<spring:message code="SPEICero.reporteEmision.txt.instAlterna" var="instAlterna"/>
<spring:message code="SPEICero.reporteDia.txt.hrIniSpeiCero" var="hrIniSpeiCero"/>
<spring:message code="SPEICero.reporteEmision.txt.prvSpeiCero" var="prvSpeiCero"/>
<spring:message code="SPEICero.reporteEmision.txt.drtSpeiCero" var="drtSpeiCero"/>
<spring:message code="SPEICero.reporteDia.txt.hrCambFechaOp" var="hrCambFechaOp"/>
<spring:message code="SPEICero.reporteEmision.txt.estOperacion" var="estOperacion"/>
<spring:message code="SPEICero.reporteEmision.txt.hrInicio" var="hrInicio"/>
<spring:message code="SPEICero.reporteEmision.txt.hrFin" var="hrFin"/>
<spring:message code="SPEICero.reporteEmision.txt.rgHoraBusqueda" var="rgHoraBusqueda"/>
<spring:message code="SPEICero.reporteEmision.txt.consideraCriterios" var="consideraCriterios"/>
<spring:message code="SPEICero.reporteEmision.txt.buscar" var="buscar"/>

<spring:message code="SPEICero.reporteEmision.txt.refTransfer" var="refTransfer"/>
<spring:message code="SPEICero.reporteEmision.txt.tipoOperacion" var="tipoOperacion"/>
<spring:message code="SPEICero.reporteEmision.txt.medioEntrega" var="medioEntrega"/>
<spring:message code="SPEICero.reporteEmision.txt.clvTransfer" var="clvTransfer"/>
<spring:message code="SPEICero.reporteEmision.txt.clvOperacion" var="clvOperacion"/>
<spring:message code="SPEICero.reporteEmision.txt.mntOperacion" var="mntOperacion"/>
<spring:message code="SPEICero.reporteEmision.txt.nomOrdenante" var="nomOrdenante"/>
<spring:message code="SPEICero.reporteEmision.txt.nomReceptor" var="nomReceptor"/>
<spring:message code="SPEICero.reporteEmision.txt.tOperacion" var="tOperacion"/>
<spring:message code="SPEICero.reporteEmision.txt.exportar" var="exportar"/>
<spring:message code="SPEICero.reporteEmision.txt.regresar" var="regresar"/>
<spring:message code="SPEICero.reporteDia.txt.genEstCuentaSpeiTrans" var="genEstCuentaSpeiTrans"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="SPEI.codError.ED00001V" var="ED00127V"/>
<spring:message code="SPEI.codError.ED00002V" var="ED00002V"/>
<spring:message code="SPEI.codError.EC00001B" var="EC00001B"/>
<spring:message code="SPEI.codError.ED00003V" var="ED00003V"/>
<spring:message code="SPEI.codError.ED00004V" var="ED00004V"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00063V" var="ED00063V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>
<spring:message code="codError.CD00169V" var="CD00169V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.OK00002V" var="OK00002V"/>

	 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
             "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',
             "OK00013V":'${OK00013V}',"EC00011B":'${EC00011B}',"EC00001B":'${EC00001B}',
             "ED00063V":'${ED00063V}',"ED00064V":'${ED00064V}',"ED00065V":'${ED00065V}',
             "CD00167V":'${CD00167V}',"CD00168V":'${CD00168V}',"CD00169V":'${CD00169V}',
             "ED00127V":'${ED00127V}',"ED00002V":'${ED00002V}',"OK00000V":'${OK00000V}',
             "OK00002V":'${OK00002V}',"ED00003V":'${ED00003V}',"ED00004V":'${ED00004V}'
                        };
	 
    </script>

<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>

<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="paginador.accion" id="accion" value=""/>
    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResSpeiCero.beanPaginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResSpeiCero.beanPaginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResSpeiCero.beanPaginador.paginaFin}"/>
	<input type="hidden" name="opcion" id="opcion" value="${beanResSpeiCero.opcion}"/>
	<input type="hidden" name="opcionCero" id="opcionCero" value="${beanResSpeiCero.opcionCero}"/>
	<input type="hidden" id="horaInicio" name="horaInicio" value="${beanResSpeiCero.horaInicio}"/> 
	<input type="hidden" id="horaFin" name="horaFin" value="${beanResSpeiCero.horaFin}"/>
	<input type="hidden" id="historico" name="historico" value="${beanResSpeiCero.historico}"/>
	<input type="hidden" id="mInstitucion" name="mInstitucion" value="${beanResSpeiCero.mInstitucion}"/>	
	<input type="hidden" id="exportar" name="exportar" value="${beanResSpeiCero.exportar}"/> 
	<input type="hidden" name="rptTodo" id="rptTodo" value="${beanResSpeiCero.rptTodo}"/>
	<input type="hidden" id="horaFecOperacion" name="horaFecOperacion" value="${beanResSpeiCero.horaFecOperacion}"/>	
	<div class="frameBuscadorSimple">
	    <div class="titleBuscadorSimple">${filtroBusq}</div>
	    <div class="contentBuscadorSimple">
	        <table width="100%" class="text_derecha">
	        	<caption> </caption>
	            <tbody>
	            	<c:if test="${!('0' eq beanResSpeiCero.historico)}">
			            <tr>
		                    <td class="text_izquierda" style="width:150px">
		                    	${fechaOp}<%-- Fecha Operaci&oacute;n --%>
		                    </td>
		                    <td class="text_izquierda" style="width:180px">
		                        <input name="fchOperacion" type="text" class="Campos center" id="fchOperacion" 
		                        size="20" readonly="readonly" value="${beanResSpeiCero.fchOperacion}" required="required">
		                        <img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
		                    </td>
		                    <td></td>
		                </tr>
	                </c:if>
	                <tr>
	                    <td class="text_izquierda" style="width:150px">
	                        <label style="font-size: 11px;">
	                            <input type="radio" name="institucion" id="institucionA"
	                            value="instiPrincipal" ${"instiPrincipal" eq beanResSpeiCero.opcion?"checked":""} disabled>
	                           <%-- Instituci&oacute;n Principal --%>${instPrincipal}
	                        </label>
	                    </td>	                    
	                    <td class="text_izquierda" style="width:150px">
	                        <label style="font-size: 11px;">
	                            <input type="radio" name="institucion" id="institucionB"
	                            value="instiAlterna" ${"instiAlterna" eq beanResSpeiCero.opcion?"checked":""} disabled>
	                            <%-- Instituci&oacute;n Alterna --%>${instAlterna}
	                        </label>
	                    </td>
	                    <td></td>
	                    <c:if test="${('0' eq beanResSpeiCero.historico)}">
		                    <td style="width:140px">
		                        <input type="text" class="Campos center" id="horaIniSpeiCero" name="horaIniSpeiCero" size="19" maxlength="40" 
		                        placeholder="DD/MM/YYYY HH:MM:SS" readonly="readonly" value="${beanResSpeiCero.horaIniSpeiCero}">
		                    </td>
		                    <td style="width:180px" >
		                        <%-- Hora Inicio SPEI CERO --%>${hrIniSpeiCero}
		                    </td>
	                    </c:if>
	                </tr>
	                <tr>
	                    <td class="text_izquierda">
	                        <label style="font-size: 11px;">
	                            <input type="radio" name="institucion2" id="institucionC"
	                            value="prevSpeiCero"  ${"prevSpeiCero" eq beanResSpeiCero.opcionCero?"checked":""} disabled>
	                         <%-- Previo SPEI CERO --%>${prvSpeiCero}
	                        </label>
	                    </td>
	                    <td class="text_izquierda">
	                        <label style="font-size: 11px;">
	                            <input type="radio" name="institucion2"  id="institucionD"
	                            value="duranSpeiCero"  ${"duranSpeiCero" eq beanResSpeiCero.opcionCero?"checked":""} disabled>
	                             <%-- Durante SPEI CERO --%>${drtSpeiCero}
	                        </label>
	                    </td>
	                    <c:if test="${('0' eq beanResSpeiCero.historico)}">
		                    <td></td>
		                    <td>
		                        <input type="text" class="Campos center" id="horaFecOperacion" name="horaFecOperacion" size="19" maxlength="40" 
		                        placeholder="DD/MM/YYYY HH:MM:SS" readonly="readonly" value="${beanResSpeiCero.horaFecOperacion}">
		                    </td>
		                    <td class="text_derecha">
		                       <%-- Horario cambio fecha operaci&oacute;n --%>${hrCambFechaOp}
		                    </td>
	                    </c:if>
	                </tr>
	                <tr>
	                    <td class="text_centro"><%-- Estatus Operaci&oacute;n --%>${estOperacion}</td>
	                    <td class="text_izquierda">
	                        <select name="estatusOpera" id="estatusOpera" >
		                        <c:forEach items="${beanResSpeiCero.listaEstatusOpera}" var="val" varStatus="rowCounter">
		                        	<option value="${val.clave}" ${val.clave eq beanResSpeiCero.estatusOpera?"selected":""}>${val.valor}</option>
		                        </c:forEach>
	                        </select>
	                    </td>
	                    <td colspan="3"></td>
	                </tr>	                          
	                <tr>
	                    <td colspan="5" class="text_izquierda">
	                        <div id="boxBusqueda-in">
	                            <div class="bloque">
		                            <span><%-- Hora inicio --%>${hrInicio} ${ b.codError }</span>
		                            <select id="hrInicio" name="hrInicio">
	                                    <c:forEach items="${beanResSpeiCero.hInicio1}" var="val">
	                                        <option value="${val.clave}" ${val.clave eq beanResSpeiCero.hrInicio?"selected":""}>${val.valor}</option>
	                                    </c:forEach>
		                            </select>
		                            <select id="minInicio" name="minInicio">
		                               <c:forEach items="${beanResSpeiCero.hFin1}" var="val">
	                                        <option value="${val.clave}" ${val.clave eq beanResSpeiCero.minInicio?"selected":""}>${val.valor}</option>
	                                    </c:forEach>
		                            </select>
	                            </div>
	                            <div class="bloque">
		                            <span><%-- Hora Fin --%>${hrFin}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}</span>
		                            <select id="hrFin" name="hrFin">
		                                <c:forEach items="${beanResSpeiCero.hInicio2}" var="val">
	                                        <option value="${val.clave}" ${val.clave eq beanResSpeiCero.hrFin?"selected":""}>${val.valor}</option>
	                                    </c:forEach>
		                            </select>
		                            <select id="minFin" name="minFin">
		                                <c:forEach items="${beanResSpeiCero.hFin2}" var="val">
	                                        <option value="${val.clave}" ${val.clave eq beanResSpeiCero.minFin?"selected":""}>${val.valor}</option>
	                                    </c:forEach>
		                            </select>
	                            </div>
	                            <div class="bloque">
	                                <%-- Rango hora de B&uacute;squeda (Periodo m&aacute;ximo 60 minutos) --%>${rgHoraBusqueda}
	                            </div>
	                        </div>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4"></td>
	                    <td>
	                        <label style="font-size: 11px;">
	                        	<input type="checkbox" name="todoFlt" id="todoFlt" value="${beanResSpeiCero.todo}" ${beanResSpeiCero.todo?"checked":""}
	                        	onclick="reporteDia.activarCps()" onkeypress="reporteDia.activarCps()"> 	                            
	                            <%-- Considera todos los criterios --%>${consideraCriterios}
	                        </label>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4"></td>
	                    <td class="derecha">
	                        <span><a id="idBuscar" href="javascript:;"><label style="font-size: 11px;"><%-- Buscar --%>${buscar}</label></a></span>
	                    </td>
	                </tr>
	                <tr>
	                    <td></td>
	                    <td></td>
	                    <td></td>
	                    <td></td>
	                    <td></td>
	                </tr>
	            </tbody>
	        </table>
	       
	    </div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<caption> </caption>
				 <tbody>
					 <tr>
		                <th width="122" class="text_centro"><%-- Referencia Transfer --%>${refTransfer}</th>
		                <th width="122" class="text_centro"><%-- Tipo Operaci&oacute;n --%>${tipoOperacion}</th>
		                <th width="122" class="text_centro"><%-- Medio de Entrega --%>${medioEntrega}</th>
		                <th width="122" class="text_centro"><%-- Clave Transfer --%>${clvTransfer}</th>
		                <th width="122" class="text_centro"><%-- Clave Operaci&oacute;n --%>${clvOperacion}</th>
		                <th width="122" class="text_centro"><%-- Monto Operaci&oacute;n --%>${mntOperacion}</th>
		                <th width="122" class="text_centro"><%-- Nombre Ordenante --%>${nomOrdenante}</th>
		                <th width="122" class="text_centro"><%-- Nombre Receptor --%>${nomReceptor}</th>
		                <th width="122" class="text_centro"><%-- Topologia Operaci&oacute;n --%>${tOperacion}</th>
		            </tr>
		        </tbody>
				<tbody>				
					<c:forEach var="beanSpeiCero" items="${beanResSpeiCero.listaSpeiCero}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">													
							<td class="text_izquierda">${beanSpeiCero.referencia}</td>
							<td class="text_centro">${beanSpeiCero.tipoOperacion}</td>
							<td class="text_centro">${beanSpeiCero.descripcion}</td>
							<td class="text_centro">${beanSpeiCero.cveTransferencia}</td>
							<td class="text_centro">${beanSpeiCero.cveOperacion}</td>
							<td class="text_centro">${beanSpeiCero.importeAbono}</td>
							<td class="text_centro">${beanSpeiCero.nombreOrd}</td>
							<td class="text_centro">${beanSpeiCero.nombreRec}</td>
							<td class="text_centro">${beanSpeiCero.estatusSer}</td>							
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<c:if test="${not empty beanResSpeiCero.listaSpeiCero}">
				<div class="paginador">
					<c:if test="${beanResSpeiCero.beanPaginador.paginaIni == beanResSpeiCero.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaIni != beanResSpeiCero.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','bucarSPEICero.do','INI');" 
							onKeyPress="nextPage('idForm','bucarSPEICero.do','INI');">&lt;&lt;${inicio}</a>
					</c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaAnt!='0' && beanResSpeiCero.beanPaginador.paginaAnt!=null}">
						<a href="javascript:;" onclick="nextPage('idForm','bucarSPEICero.do','ANT');"
							 onKeyPress="nextPage('idForm','bucarSPEICero.do','ANT');">&lt;${anterior}</a>
					</c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaAnt=='0' || beanResSpeiCero.beanPaginador.paginaAnt==null}">
						<a href="javascript:;">&lt;${anterior}</a>
					</c:if>
					<label id="txtPagina">${beanResSpeiCero.beanPaginador.pagina} - ${beanResSpeiCero.beanPaginador.paginaFin}</label>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaFin != beanResSpeiCero.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','bucarSPEICero.do','SIG');"
							 onKeyPress="nextPage('idForm','bucarSPEICero.do','SIG');">${siguiente}&gt;</a>
					</c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaFin == beanResSpeiCero.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaFin != beanResSpeiCero.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','bucarSPEICero.do','FIN');"
							onKeyPress="nextPage('idForm','bucarSPEICero.do','FIN');">${fin}&gt;&gt;</a>
					</c:if>
					<c:if test="${beanResSpeiCero.beanPaginador.paginaFin == beanResSpeiCero.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
			</c:if>

		</div>
	
  <div class="framePieContenedor">
	    <div class="contentPieContenedor">
	        <table>
	        	<caption> </caption>
	            <tbody>
	                <tr>
	                	<c:if test="${('0' eq beanResSpeiCero.historico)}">
		                	<c:choose>
			                	<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR') and not empty beanResSpeiCero.listaSpeiCero}">
			                		<td width="279" class="izq">
										<a id="idExportar" href="javascript:;" ><label style="font-size: 11px;" ><%-- Exportar --%>${exportar}</label></a>
									</td>
									<td width="6" class="odd"> </td>
									<td width="279" class="der">
										<a id="idEdoCta" href="javascript:;" ><label style="font-size: 11px;" ><%-- Exportar --%>${genEstCuentaSpeiTrans}</label></a>
									</td>
								</c:when>
								<c:otherwise>								
									<td width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
									<td width="6" class="odd"> </td>
									<td width="279" class="der_Des"><a href="javascript:;">${genEstCuentaSpeiTrans}</a></td>
								</c:otherwise>                  
		                    </c:choose>
	                   </c:if>
				       
				        <c:if test="${!('0' eq beanResSpeiCero.historico)}">
					        <c:choose>
				                	<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR') and not empty beanResSpeiCero.listaSpeiCero}">
				                		<td width="279" class="izq">
											<a id="idExportar" href="javascript:;" ><label style="font-size: 11px;" ><%-- Exportar --%>${exportar}</label></a>
										</td>									
									</c:when>
									<c:otherwise>								
										<td width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
									</c:otherwise>                  
			                </c:choose>
			                <td width="6" class="odd"> </td>
							<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>	
						</c:if>
	                </tr>
	                <c:if test="${('0' eq beanResSpeiCero.historico)}">
						<tr>
		                    <td></td>
		                    <td width="6" class="odd"> </td>
		                    <td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
	                	</tr>
					</c:if>
	                
	            </tbody>
	        </table>
	    </div>
	</div>
</form>