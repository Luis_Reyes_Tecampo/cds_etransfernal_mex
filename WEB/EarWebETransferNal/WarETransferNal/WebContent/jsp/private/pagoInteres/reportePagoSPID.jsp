<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* reportePagoSPID.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     13/09/2011 12:00 Creaci�n de la pantalla
*
***********************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuPagoInteres.jsp" flush="true">
	<jsp:param name="menuItem" value="pagoInteres" />
	<jsp:param name="menuSubitem" value="muestraReportePagoSPID" />
</jsp:include>

<spring:message code="pagoInt.pagoIntSPID.titulo"                   var="titulo" />
<spring:message code="pagoInt.pagoIntSPID.subTitulo"                var="subTitulo" />
<spring:message code="pagoInt.pagoIntSPID.reporteLinea"             var="reporteLinea" />
<spring:message code="pagoInt.pagoIntSPID.reporteHistorico"         var="reporteHistorico" />
<spring:message code="pagoInt.pagoIntSPID.fchOperacion"             var="fchOperacion" />

<spring:message code="pagoInt.pagoIntSPID.tipoPago"                 var="labelTipoPago" />
<spring:message code="pagoInt.pagoIntSPID.estatus"                  var="estatus" />
<spring:message code="pagoInt.pagoIntSPID.medioEnt"                 var="medioEnt" />
<spring:message code="pagoInt.pagoIntSPID.rangoHoraBusqueda"        var="rangoHoraBusqueda" />
<spring:message code="pagoInt.pagoIntSPID.rangoImporte"             var="rangoImporte" />
<spring:message code="pagoInt.pagoIntSPID.horaInicio"               var="horaInicio" />
<spring:message code="pagoInt.pagoIntSPID.horaFin"                  var="horaFin" />
<spring:message code="pagoInt.pagoIntSPID.importeInicio"            var="importeInicio" />
<spring:message code="pagoInt.pagoIntSPID.importeFin"               var="importeFin" />
<spring:message code="pagoInt.pagoIntSPID.difMinutos"               var="difMinutos" />

<spring:message code="pagoInt.pagoIntSPID.infoEncontrada"           var="infoEncontrada" />
<spring:message code="pagoInt.pagoIntSPID.refPAgo"                  var="refPAgo"/>
<spring:message code="pagoInt.pagoIntSPID.refInteres"               var="refInteres"/>
<spring:message code="pagoInt.pagoIntSPID.cveRastreo"               var="cveRastreo"/>
<spring:message code="pagoInt.pagoIntSPID.medioEntrega"             var="medioEntrega" />
<spring:message code="pagoInt.pagoIntSPID.impteTransf"              var="impteTransf" />
<spring:message code="pagoInt.pagoIntSPID.impteInteres"             var="impteInteres" />
<spring:message code="pagoInt.pagoIntSPID.fchInicio"                var="fchInicio" />
<spring:message code="pagoInt.pagoIntSPID.fchFin"                   var="fchFin" />
<spring:message code="pagoInt.pagoIntSPID.diferencia"               var="diferencia" />
<spring:message code="pagoInt.pagoIntSPID.estatus"                  var="labelEstatus" />
<spring:message code="pagoInt.pagoIntSPID.text.exportar"            var="txtExportar"/>
<spring:message code="pagoInt.pagoIntSPID.text.exportarTodo"        var="txtExportarTodo"/>
<spring:message code="pagoInt.pagoIntSPID.text.actualizar"          var="txtActualizar"/>
<spring:message code="pagoInt.pagoIntSPID.text.limpiar"             var="txtLimpiar"/>

<spring:message code="general.buscar"                             	var="buscar"/>
<spring:message code="general.filtroBusqueda"                   	var="filtroBusqueda" text=""/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/pagoInteres/reportePagoSPID.js"></script>

<c:set var="searchString" value="${seguTareas}"/>
<form id="idForm" action="" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value=""/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${resBean.paginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="pagina" value="${resBean.paginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${resBean.paginador.paginaFin}"/>
		<input type="hidden" name="tipoPago" id="idTipoPago" value="${reqBean.tipoPago}"/>
		<input type="hidden" name="estatus" id="idEstatus" value="${reqBean.estatus}"/>
		<input type="hidden" name="medioEnt"  id="idMedioEnt" value="${reqBean.medioEnt}"/>  
		
		<input type="hidden" name="horario.horaInicio"  id="idHInicio" value="${reqBean.horario.horaInicio}"/>
		<input type="hidden" name="horario.minutoInicio"  id="idMInicio" value="${reqBean.horario.minutoInicio}"/>
		<input type="hidden" name="horario.horaFin"  id="idHFin" value="${reqBean.horario.horaFin}"/>
		<input type="hidden" name="horario.minutoFin"  id="idMFin" value="${reqBean.horario.minutoFin}"/>
		
		<input type="hidden" name="horario.fechaInicio"  id="idFInicio" value="${reqBean.horario.fechaInicio}"/>
		<input type="hidden" name="horario.fechaFin"  id="idFFin" value="${reqBean.horario.fechaFin}"/>
		<input type="hidden" name="sigDifMinutos"  id="idMinutos" value="${reqBean.sigDifMinutos}"/>
		<input type="hidden" name="difMinutos"  id="idDifMinutos" value="${reqBean.difMinutos}"/>
		<input type="hidden" name="importeInicio"  id="idImporteInicio" value="${reqBean.importeInicio}"/>
		<input type="hidden" name="importeFin"  id="idImporteFin" value="${reqBean.importeFin}"/>
		<input type="hidden" name="totalReg"  id="totalReg" value="${resBean.totalReg}"/>
		<input type="hidden" name="horaRango"  id="horaRango" value="${horaRango}"/>
		<input type="hidden" name="diaRango"  id="diaRango" value="${diaRango}"/>
		
		

	<div class="pageTitleContainer">
		<span class="pageTitle">${titulo}</span> - ${subTitulo}
	</div>


	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
			${instruccionBuscador}
			<table>
				<tbody>
					<tr>
						<td class="text_derecha">
							<input name="opcion" type="radio" class="Campos" id="txtReporteLinea" value="A" ${reqBean.opcion eq "A"?"checked":""} />
						</td>
						<td class="text_izquierda" width="80px"> ${reporteLinea} </td>
						<td class="text_derecha">
							<input name="opcion" type="radio" class="Campos" id="txtReporteHistorico" value="H" ${reqBean.opcion eq "H"?"checked":""}/>
						</td>
						<td class="text_izquierda" width="80px"> ${reporteHistorico} </td>
						<td class="text_derecha" colspan="3"> ${fchOperacion}${resBean.fechaOperacion}</td>
					</tr>
					
					<tr>
						<td class="text_derecha" width="80px">${labelTipoPago} </td>
						<td class="text_izquierda" > 
							<select name="selTipoPago" class="Campos" id="selTipoPago"  style=" width:150px">
	                        	<option value="">---</option>
	                    		<c:forEach var="tipoPago" items="${resBean.tipoPagoList}">
	                    			<c:choose>
		                    			<c:when test="${reqBean.tipoPago == tipoPago.cveTipoPago}">
											<option value="${tipoPago.cveTipoPago}" selected="selected">${tipoPago.descripcion}</option>
										</c:when>
										<c:otherwise>
	      									<option value="${tipoPago.cveTipoPago}">${tipoPago.descripcion}</option>
	      								</c:otherwise>
			    					</c:choose>
			    				</c:forEach>
	                        </select>
						</td>
						<td class="text_derecha" width="100px">${labelEstatus} </td>
						<td class="text_izquierda">
							<select name="selEstatus" class="Campos" id="selEstatus" style=" width:150px">
	                        	<option value="">---</option>
	                    		<c:forEach var="estatus" items="${resBean.estatusList}">
	                    			<c:choose>
		                    			<c:when test="${reqBean.estatus == estatus.cveEstatus}">
											<option value="${estatus.cveEstatus}" selected="selected">${estatus.descripcion}</option>
										</c:when>
										<c:otherwise>
	      									<option value="${estatus.cveEstatus}">${estatus.descripcion}</option>
	      								</c:otherwise>
			    					</c:choose>
			    				</c:forEach>
	                        </select>
						</td>
						<td class="text_derecha" width="100px" colspan="2">${medioEnt} </td>
						<td class="text_izquierda">
							<select name="selMedioEnt" class="Campos" id="selMedioEnt" style=" width:250px">
	                        	<option value="">---</option>
	                    		<c:forEach var="medioEnt" items="${resBean.medioEntList}">
	                    			<c:choose>
		                    			<c:when test="${reqBean.medioEnt == medioEnt.cveMedioEnt}">
											<option value="${medioEnt.cveMedioEnt}" selected="selected">${medioEnt.cveMedioEnt}</option>
										</c:when>
										<c:otherwise>
	      									<option value="${medioEnt.cveMedioEnt}">${medioEnt.cveMedioEnt}</option>
	      								</c:otherwise>
			    					</c:choose>
			    				</c:forEach>
	                        </select>
							
							
						</td>
					</tr>
					
					<tr>
						<td class="text_centro" colspan="4"><span id="idRangoDia">${leyendaHora}</span><span id="idRangoHist">${leyendaDia}</span></td>
						<td class="text_centro" colspan="4">${rangoImporte}</td>
					</tr>
					
					
					<tr>
					
                        <td class="text_izquierda" colspan="2" style=" width:50px">
                        	<span id="idHoraIni">
	                        	${horaInicio}
	                        	<select name="selHoraIni" class="Campos" id="selHoraIni" style=" width:50px">
		                    		<c:forEach var="hora" items="${horas}">
		                    			<c:choose>
			                    			<c:when test="${hora == reqBean.horario.horaInicio}">
			      								<option value="${hora}" selected="selected">${hora}</option>
											</c:when>
											<c:otherwise>
		      									<option value="${hora}">${hora}</option>
		      								</c:otherwise>
				    					</c:choose>
				    				</c:forEach>
		                        </select>
	                            <select name="selMinutoIni" class="Campos" id="selMinutoIni" style=" width:50px">
		                    		<c:forEach var="minuto" items="${minutos}">
		                    			<c:choose>
			                    			<c:when test="${minuto == reqBean.horario.minutoInicio}">
			      								<option value="${minuto}" selected="selected">${minuto}</option>
											</c:when>
											<c:otherwise>
		      									<option value="${minuto}">${minuto}</option>
		      								</c:otherwise>
				    					</c:choose>
				    				</c:forEach>
		                        </select>
		                     </span>
                        	
                        	<span id="idFechaIni">
	                             ${fchInicio} <input name="txtFechaInicio" type="text" class="CamposCompletar" id="txtFechaInicio" value="${reqBean.horario.fechaInicio}" size="12" maxlength="12" readonly />
	                             <img id="cal3" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
                             </span>
                             
                        </td> 
                        <td class="text_izquierda" colspan="2">
                        	<span id="idHoraFin">
	                        	${horaFin}
	                        	<select name="selHoraFin" class="Campos" id="selHoraFin" style=" width:50px">
		                    		<c:forEach var="hora" items="${horas}">
		                    			<c:choose>
			                    			<c:when test="${hora == reqBean.horario.horaFin}">
			      								<option value="${hora}" selected="selected">${hora}</option>
											</c:when>
											<c:otherwise>
		      									<option value="${hora}">${hora}</option>
		      								</c:otherwise>
				    					</c:choose>
				    				</c:forEach>
		                        </select>
	                            <select name="selMinutoFin" class="Campos" id="selMinutoFin" style=" width:50px">
		                    		<c:forEach var="minuto" items="${minutos}">
		      							<c:choose>
			                    			<c:when test="${minuto == reqBean.horario.minutoFin}">
			      								<option value="${minuto}" selected="selected">${minuto}</option>
											</c:when>
											<c:otherwise>
		      									<option value="${minuto}">${minuto}</option>
		      								</c:otherwise>
				    					</c:choose>
				    				</c:forEach>
		                        </select>
		                     </span>

                        </td>
                        
                        <td class="text_izquierda" colspan="4">	                        
	                       ${importeInicio} <input name="txtImporteInicio" type="text" class="Campos" id="txtImporteInicio" value="${reqBean.importeInicio}" value="" maxlength="10" size="10"/>
	                        ${importeFin}<input name="txtImporteFin" type="text" class="Campos" id="txtImporteFin" value="${reqBean.importeFin}" value="" maxlength="10" size="10"/>
	                       
	                        
                        </td>
                        
                    </tr>
                    <tr>
                    	<td  class="text_derecha" width="100" colspan="2">${difMinutos} </td>
                        <td class="text_izquierda" colspan="2">	  
                        	<select name="selMinutos" class="Campos" id="selMinutos" >
	                        	<option value="">---</option>
	                    		<c:forEach var="minutos" items="${resBean.difMinutosList}">
	                    			<c:choose>
		                    			<c:when test="${reqBean.sigDifMinutos == minutos}">
											<option value="${minutos}" selected="selected">${minutos}</option>
										</c:when>
										<c:otherwise>
	      									<option value="${minutos}">${minutos}</option>
	      								</c:otherwise>
			    					</c:choose>
			    				</c:forEach>
	                        </select>                      
	                       <input name="txtDifMinutos" type="text" class="Campos" id="txtDifMinutos" value="${reqBean.difMinutos}" maxlength="4"" size="4"/>
                        </td>
                        
                        <td class="text_derecha" >
                    	<c:choose>
								<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
	                        		<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
	                        	</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
                    	</td>
                    </tr>
					<tr>
					<td class="text_izquierda">
					</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<!-- Componente tabla estandar -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infoEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<thead>
					<tr>
						<th class="text_centro"> ${refPAgo}</th>
						<th class="text_centro">${refInteres}</th>
						<th class="text_centro">${cveRastreo}</th>
						<th class="text_centro">${labelTipoPago}</th>
						<th class="text_centro">${medioEntrega}</th>
						<th class="text_centro">${impteTransf}</th>
						<th class="text_centro">${impteInteres}</th>
						<th class="text_centro">${fchInicio}</th>
						<th class="text_centro">${fchFin}</th>
						<th class="text_centro">${diferencia}</th>
						<th class="text_centro">${labelEstatus}</th>
					</tr>
				</thead>
				<tr>
					<Td colspan="8" class="special"></Td>
				</tr>
				<tbody>
				<%
					String estilo="odd1";				
				%>
				<c:forEach var="bean" items="${resBean.reportePagoList}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							
							
						
							<td class="text_izquierda">${bean.refPago}
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].refPago" value="${bean.refPago}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].refInteres" value="${bean.refInteres}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].cveRastreo" value="${bean.cveRastreo}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].tipoPago" value="${bean.tipoPago}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].medioEnt" value="${bean.medioEnt}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].importeTransf" value="${bean.importeTransf}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].importeInteres" value="${bean.importeInteres}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].fechaInicio" value="${bean.fechaInicio}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].fechaFin" value="${bean.fechaFin}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].diferencia" value="${bean.diferencia}"/>
							<input type="hidden" name="beanReportePagoList[${rowCounter.index}].estatus" value="${bean.estatus}"/>
							</td>
	                        <td class="text_izquierda">${bean.refInteres}</td>
	                        <td class="text_izquierda">${bean.cveRastreo}</td>
	                        <td class="text_izquierda">${bean.tipoPago}</td>
	                        <td class="text_izquierda">${bean.medioEnt}</td>
	                        <td class="text_izquierda">${bean.importeTransf}</td>
	                        <td class="text_izquierda">${bean.importeInteres}</td>
	                        <td class="text_izquierda">${bean.fechaInicio}</td>
	                        <td class="text_izquierda">${bean.fechaFin}</td>
	                        <td class="text_izquierda">${bean.diferencia}</td>
	                        <td class="text_izquierda">${bean.estatus}</td>
						</tr>
			    	</c:forEach>
				</tbody>
			</table>
		</div>


        <!-- Componente Paginador -->
        <c:if test="${not empty resBean.reportePagoList}">
					<jsp:include page="../paginador.jsp" >	  					
	  					<jsp:param name="paginaIni" value="${resBean.paginador.paginaIni}" />
	  					<jsp:param name="pagina" value="${resBean.paginador.pagina}" />
	  					<jsp:param name="paginaAnt" value="${resBean.paginador.paginaAnt}" />
	  					<jsp:param name="paginaFin" value="${resBean.paginador.paginaFin}" />
	  					<jsp:param name="servicio" value="consultaReportePagoSPID.do" />
					</jsp:include>
				</c:if>   

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
								<c:when test="${fn:containsIgnoreCase(  searchString,'EXPORTAR') and not empty resBean.reportePagoList}">
									<td width="279" class="izq"><a href="javascript:;" id="idExportar">${txtExportar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;">${txtExportar}</a></td>
								</c:otherwise>
						</c:choose>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
								<td width="279" class="der">
								<a href="javascript:;" id="idActualizar">${txtActualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${txtActualizar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(  searchString,'EXPORTAR') and not empty resBean.reportePagoList}">
							<td width="279" class="izq"><a href="javascript:;" id="idExportarTodo">${txtExportarTodo}</a></td>
						</c:when>
						<c:otherwise>
							<td width="279" class="izq_Des"><a href="javascript:;">${txtExportarTodo}</a></td>
						</c:otherwise>
					</c:choose>
							<td width="279" class="odd">${espacioEnBlanco}</td>
							<td width="279" class="der"><a href="javascript:;" id="idLimpiar">${txtLimpiar}</a></td>
							
							
					</tr>
				</table>
			</div>
		</div>
			
	</div>

	
</form>	

<jsp:include page="../myFooter.jsp" flush="true"/>
	
	    <script>
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
    </script>
    


<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>