<!DOCTYPE spring:message PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
			<div class="paginador">
				<c:if test="${respuesta.paginador.paginaIni == respuesta.paginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${respuesta.paginador.paginaIni != respuesta.paginador.pagina}">
					<a href="javascript:;" onclick="nextPage('archivosHistoricosDetalleCADSPID','detalleArhivoHistorico.do','INI','accion');" onKeyPress=""
						>&lt;&lt;${inicio}</a></c:if>
				<c:if test="${respuesta.paginador.paginaAnt!='0' && respuesta.paginador.paginaAnt!=null}">
					<a href="javascript:;"  onKeyPress=""
						onclick="nextPage('archivosHistoricosDetalleCADSPID','detalleArhivoHistorico.do','ANT','accion');">&lt;${anterior}
							</a></c:if>
				<c:if test="${respuesta.paginador.paginaAnt=='0' ||respuesta.paginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${respuesta.paginador.pagina} - ${respuesta.paginador.paginaFin}
					</label>
				<c:if test="${respuesta.paginador.paginaFin != respuesta.paginador.pagina}">
					<a href="javascript:;" onclick="nextPage('archivosHistoricosDetalleCADSPID','detalleArhivoHistorico.do','SIG','accion');" onKeyPress=""
						>${siguiente}&gt;</a></c:if>
				<c:if test="${respuesta.paginador.paginaFin == respuesta.paginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${respuesta.paginador.paginaFin != respuesta.paginador.pagina}">
					<a href="javascript:;" onclick="nextPage('archivosHistoricosDetalleCADSPID','detalleArhivoHistorico.do','FIN','accion');" onKeyPress=""
						>${fin}&gt;&gt;</a></c:if>
				<c:if test="${respuesta.paginador.paginaFin == respuesta.paginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>