<!DOCTYPE jsp:include PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="monitorCargArchHistCDASPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.monitorCDA.botonLink.actualizar" var="actualizar" />
<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosCDASPID.tituloFuncionalidad" var="tituloFuncionalidad" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosCDASPID.subTitulo" var="tituloTabla" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosCDASPID.nombreArchivo" var="nombreArchivo" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosCDASPID.estatusArchivo" var="estatusArchivo" />

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.ED00026V" var="ED00026V" />
<spring:message code="codError.EC00011B" var="EC00011B" />

	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["ED00011V"] = '${ED00011V}';
	 	mensajes["ED00026V"] = '${ED00026V}';
	 	mensajes["EC00011B"] = '${EC00011B}';
    </script>

<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/cargaArchivosHistoricosCDASPID.js"
		type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


<form id="cargaArchivosHistoricosCADSPID" name="cargaArchivosHistoricosCADSPID" method="post">
<input type="hidden" id="codigoError" name="codigoError" value="${codigoError}"/>
<input type="hidden" id="mensajeError" name="mensajeError" value="${mensajeError}"/>
<input type="hidden" name="paginaIni" id="paginaIni" value="${respuesta.paginador.paginaIni}"/>
<input type="hidden" name="pagina" id="pagina" value="${respuesta.paginador.pagina}"/>
<input type="hidden" name="paginaFin" id="paginaFin" value="${respuesta.paginador.paginaFin}"/>
<input type="hidden" name="accion" id="accion" value=""/>
<input type="hidden" name="idArchivo" id="idArchivo" value=""/>
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${tituloFuncionalidad}</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloTabla}</div>
		<div class="contentTablaVariasColumnas">
			<table>
				<caption></caption>
				<tr>
					<th style="font-size:11px; text-align:left; width: 50%;">${nombreArchivo}</th>
					<th style="font-size:11px; width: 50%;">${estatusArchivo}</th>
				</tr>
				<c:forEach var="respuesta" items="${respuesta.listaResultado}" varStatus="rowCounter">
					<tr class="${rowCounter.count % 2 eq 0 ? 'odd3' : 'odd'}">
						<c:choose>
							<c:when test="${respuesta.estatusArchivo eq 'FINALIZADO'}">
								<td class="text_centro" style="word-break: break-all;">
									<a onKeyPress="" onclick="verDetalles('${respuesta.idArchivo}', 'cargaArchivosHistoricosCADSPID', 
											'idArchivo', 'detalleArhivoHistorico.do')">
										<label style="cursor:pointer;cursor:hand; display:block; font-size:11px;">
											${respuesta.nombreArchivo}</label>
									</a>
								</td>
								<td class="text_centro" style="word-break: break-all;">
									<label style="display:block; font-size:11px;">${respuesta.estatusArchivo}</label>
								</td>
							</c:when>
							<c:otherwise>
								<td class="text_centro" style="word-break: break-all;">
									<label style="display:block; font-size:11px;">${respuesta.nombreArchivo}</label>
								</td>
								<td class="text_centro" style="word-break: break-all;">
									<label style="display:block; font-size:11px;">${respuesta.estatusArchivo}</label>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</table>
		</div>
	<c:if test="${not empty respuesta.listaResultado}">
		<jsp:include page="cargaArchivosHistoricosPagCDASPID.jsp" flush="true" />
	</c:if>
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
				<caption></caption>
				<tr>
					<td width="279" class="izq"><a href="javascript:;" ></a></td>
					<td width="6" class="odd">${espacioEnBlanco}</td>
					<td width="279" class="der"><a id="idActualizar" href="javascript:;" onKeyPress=""
							onclick="hacerSubmit('cargaArchivosHistoricosCADSPID','monitorCargArchHistCDASPID.do');"
									><label style="font-size:11px;">${actualizar}</label></a></td>
				</tr>
			</table>
		</div>
	</div>
	</div>
</form>
<c:if test="${codigoError!=''}">
	<c:choose>
		<c:when test="${codigoError eq 'EC00011B'}">
			<script type="text/javascript" defer="defer">
			 	jError(mensajes['EC00011B'], '${aplicacion}', 'EC00011B', '', '');
			</script>
		</c:when>
		<c:when test="${codigoError eq 'ED00011V'}">
			<script type="text/javascript" defer="defer">
			 	jInfo(mensajes['ED00011V'], '${aplicacion}', 'ED00011V', '', '', ''); 	
			</script>
		</c:when>
	</c:choose>
</c:if>
	<c:if test="${codigoError!=''}">
		<script type = "text/javascript" defer="defer">
		
		
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codigoError}',
			   	   '');		
		</script>
	</c:if>