<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="muestraContingCDAMismoDia" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.CD00020V" var="CD00020V" />
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.fechaHora" var="fechaHora"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.numOpeGeneradas" var="numOpeGeneradas"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.numOpeGenerar" var="numOpeGenerar"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.usuario" var="usuario"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.nomArchivoOrig" var="nomArchivoOrig"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.generarNuevoArchivo" var="generarNuevoArchivo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.actualizar" var="actualizar"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.estatus" var="estatus"/>
	
<script type = "text/javascript">
	var mensajes = new Array();
	mensajes["aplicacion"] = '${aplicacion}';
	mensajes["CD00020V"] = '${CD00020V}';
</script>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/contingenciaCDAMismoDiaSPID.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<%-- Componente titulo de p�gina --%>
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${subtituloFuncion}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<caption></caption>
				<tr>
					<th style="width:122px; white-space:nowrap;" class="text_izquierda">${nomArchivo}</th>
					<th style="width:122px; white-space:nowrap;" class="text_izquierda">${nomArchivoOrig}</th>
					<th style="width:122px; white-space:nowrap;" class="text_centro" scope="col">${fechaHora}</th>
					<th style="width:122px; white-space:nowrap;" class="text_centro" scope="col">${usuario}</th>
					<th style="width:122px; white-space:nowrap;" class="text_centro" scope="col">${estatus}</th>
					<th style="width:122px; white-space:nowrap;" class="text_centro" scope="col">${numOpeGeneradas}</th>
					<th style="width:122px; white-space:nowrap;" class="text_centro" scope="col">${numOpeGenerar}</th>
				</tr>
				<tr>
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanContingMismoDia" varStatus="rowCounter" items="${beanResContingMismoDia.listBeanContingMismoDia}">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda">${beanContingMismoDia.nomArchivo}</td>
						<td class="text_izquierda">${beanContingMismoDia.nomArchivoOrig}</td>
						<td class="text_izquierda">${beanContingMismoDia.fechaHora}</td>
						<td class="text_izquierda">${beanContingMismoDia.usuario}</td>
						<td class="text_izquierda">${beanContingMismoDia.estatus}</td>
						<td class="text_izquierda">${beanContingMismoDia.numOpGeneradas}</td>
						<td class="text_derecha">${beanContingMismoDia.numOpGenerar}</td>
					</tr>
				</c:forEach>
				
				</tbody>
			</table>
			</div>
			<c:if test="${not empty beanResContingMismoDia.listBeanContingMismoDia}">
				<jsp:include page="../paginador.jsp" >	  					
  					<jsp:param name="paginaFin" value="${beanResContingMismoDia.beanPaginador.paginaFin}" />
  					<jsp:param name="paginaIni" value="${beanResContingMismoDia.beanPaginador.paginaIni}" />
  					<jsp:param name="paginaAnt" value="${beanResContingMismoDia.beanPaginador.paginaAnt}" />
  					<jsp:param name="pagina" value="${beanResContingMismoDia.beanPaginador.pagina}" />
  					<jsp:param name="servicio" value="consultarContingCDAMismoDiaSPID.do" />
				</jsp:include>
			</c:if>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<c:if test="${beanResContingMismoDia.hayOperacionespendientes}">
									<td width="279" class="izq"><a id="idGeneracionNuevoArchivo" href="javascript:;">${generarNuevoArchivo}</a></td>
								</c:if>
								<c:if test="${!beanResContingMismoDia.hayOperacionespendientes}">
									<td class="izq_Des" width="279"><a href="javascript:;">${generarNuevoArchivo}</a></td>
								</c:if>
							</c:when>
							<c:otherwise>
								<td class="izq_Des" width="279"><a href="javascript:;">${generarNuevoArchivo}</a></td>
							</c:otherwise>
						</c:choose>
					
						
						<td width="6" class="odd"></td>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td class="der" width="279"><a id="idActualizar" href="javascript:;">${actualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des" width="279"><a href="javascript:;">${actualizar}</a></td>
							</c:otherwise>
						</c:choose>
						
						
					</tr>
				</table>
			</div>
		</div>
	</div>

	<form name="idForm" id="idForm" method="post" action="">
		<input type="hidden" name="accion" id="accion" value=""/>
		<input type="hidden" name="idPeticion" id="idPeticion" value=""/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResContingMismoDia.beanPaginador.paginaFin}"/>
		<input type="hidden" name="paginaIni" id="paginaIni" value="${beanResContingMismoDia.beanPaginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${beanResContingMismoDia.beanPaginador.pagina}"/>
		
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>