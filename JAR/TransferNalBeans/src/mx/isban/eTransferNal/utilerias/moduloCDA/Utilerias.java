/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * Utilerias.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:30:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.utilerias.moduloCDA;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;

import org.apache.log4j.Logger;

/**
 *Clase de Utilerias que sirve para dar soporte a la funcionalidad
**/
public final class Utilerias {
	
	/**
	 * Constante que almacena el formato para decimales
	 */
	public static final String FORMATO_DECIMAL_NUMBER = "###,##0.00";
	
	/**
	 * Constante que almacena el formato para decimales
	 */
	public static final String FORMATO_DECIMAL_NUMBER_2 = "###########0.00";
	
	/**

	 * Constante que almacena el formato para decimales
	 */
	public static final String FORMATO_DECIMAL_NUMBER_3 = "#########0.0000";
	
	/**

	 * Constante que almacena el formato para decimales
	 */
	public static final String FORMATO_16_ENT_2_DECIMAL = "##############0.00";
	
	/**
	 * Constante que almacena el formato para decimales largos
	 */
	public static final String FORMATO_16_ENT_2_DECIMAL_COMAS = "###,###,###,###,##0.00";
	
	/**

	 * Constante que almacena el formato para numeros
	 */
	public static final String FORMATO_NUMBER = "###,##0";
	
	/**
	 * Constante que almacena formato para decimales
	 */
	public static final String FORMATO_DECIMAL_NUMBER_LARGO = "###,###,###,###.###";
	
	/**
	 * Constante que almacena formato para decimales
	 */
	public static final String FORMATO_DECIMAL_NUMBER_LARGO_DOS = "###,###,###,##0.00";
	
	/**
	 * Constante que almacena el formato para fechas con guion
	 */
	public static final String FORMATO_DD_GUION_MM_GUION_YYYY = "dd-MM-yyyy";
	
	/**
	 * Constante que almacena el formato para fechas con guion
	 */
	public static final String FORMATO_YYYY_GUION_MM_GUION_DD = "yyyy-MM-dd";
	
	
	/**
	 * Constante que almacena el formato para fechas con guion
	 */
	public static final String FORMATO_DD_GUION_MM_GUION_YY = "dd/MM/yy";

	/**
	 * Constante que almacena el formato para hora y minutos
	 */
	public static final String FORMATO_HH_MM = "HH:mm";
	/**
	 * Constante que almacena el formato para hora, minutos y segundos
	 */
	public static final String FORMATO_HH_MM_SS = "HH:mm:ss";
	/**
	 * Constante que almacena el formato para fechas yyyy/MM/dd
	 */
	public static final String FORMATO_YYYY_DIAG_MM_DIAG_DD = "yyyy/MM/dd";
	
	/**
	 * Constante que almacena el formato para fecha larga con guion
	 */
	public static final String FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS = "dd-MM-yyyy HH:mm:ss";

	/**
	 * Constante que almacena el formato para fecha larga con diagonales
	 */
	public static final String FORMATO_DD_DIAG_MM_DIAG_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
	
	/**
	 * Constante que almacena el formato para fecha larga con diagonales
	 */
	public static final String FORMATO_DD_DIAG_MM_DIAG_YYYY_HH_MM = "dd/MM/yyyy HH:mm";
	
	/**
	 * Constante que almacena fecha larga sin espacios
	 */
	public static final String FORMATO_SIN_ESPACIOS_YYYY_MM_DD_HH_MM_SS_SSS = "ddMMyyyyHHmmssSSS";
	
	/**
	 * Constante que almacena fecha sin espacios
	 */
	public static final String FORMATO_SIN_ESPACIOS_YYYYMMDD = "yyyyMMdd";
	
	/**
	 * Constante que almacena fecha sin espacios
	 */
	public static final String FORMATO_SIN_ESPACIOS_DDMMYYYY = "ddMMyyyy";
	
	/**
	 * Constante obtener fecha larga con dia y mes con letra
	 */
	public static final String FORMATO_FECHA_LARGA_LETRA_DIA_SEMANA = "EEEE dd 'de' MMMM yyyy";

	/**
	 * Constante para obtener el locale de mexico
	 */
	public static final Locale LOCALE_ESPANOL_MEXICO = new Locale("es","MX");

	/**Referencia del Objeto Utilerias*/
	private static final Utilerias UTILERIAS_INSTACE = new Utilerias();

	/**Referencia al LOG */
	private static final Logger LOG = Logger.getLogger(Utilerias.class);


	/**
	 * Constante que almacena fecha sin espacios
	 */
	public static final String FORMATO_YYYYMMDD = "yyyyMMdd";
	
	
	/**
	 * Constructor privado
	 */
	private Utilerias() {super();}
	
	/**
	 * Metodo singleton de Utilerias
	 * @return Utilerias Referencia de utilerias
	 */
	public static Utilerias getUtilerias(){
		return UTILERIAS_INSTACE;
	}
	
	/**
	 * Metodo que permite formatear objetos del tipo Number, al formato indicado en el parametro
	 * @author CCHONG
	 * @param obj Objecto que se pasa como parametro 
	 * @param formato Cadena con el formato
	 * @return String Regresa el valor formateado
	 */
	public String formateaDecimales(final Object obj,final String formato){
		String formatStr = "";
		if( obj instanceof Number){
			NumberFormat formatDecimalNumber = new DecimalFormat(formato);
			formatStr = formatDecimalNumber.format(obj);
		}else if(obj instanceof String){
			BigDecimal bigDecimal = new BigDecimal(obj.toString());
			NumberFormat formatDecimalNumber = new DecimalFormat(formato);
			formatStr = formatDecimalNumber.format(bigDecimal);
		}
		return formatStr;
	}
	
	/**
	 * Metodo que permite formatear objetos del tipo Number, al formato indicado en el parametro
	 * @author CCHONG
	 * @param num Objecto que se pasa como parametro 
	 * @param formato Cadena con el formato
	 * @return String Regresa el valor formateado
	 */
	public String formateaStringAEntero(final String num,final String formato){
		String _num=num;
		Integer number;
		String result="";
		if( _num!=null && !"".equals(_num.trim())){
			number = Integer.valueOf(_num);
			result = formateaDecimales(number,formato);
		}
		return result;
	}

	/**
	 * Metodo que permite formatear objetos del tipo Date, al formato indicado en el parametro
	 * @author CCHONG
	 * @param fecha Objeto que se pasa como parametro 
	 * @param formato Cadena con el formato
	 * @return String, Regresa el valor formateado
	 */
	public String formateaFecha(final Object fecha,final String formato){
		String formatStr = "";
		if(fecha instanceof Date){
			DateFormat formateaFecha = new SimpleDateFormat(formato,LOCALE_ESPANOL_MEXICO);
			formatStr = formateaFecha.format(fecha);
		}
		return formatStr;
	}
	
	/**
	 * Metodo que permite transformar una cadena en cierto formato a Date
	 * @author CCHONG
	 * @param strFecha Objeto que se pasa como parametro 
	 * @param formato Cadena con el formato
	 * @return String, Regresa el valor formateado
	 */
	public Date stringToDate(final String strFecha,final String formato){
		Date fecha = null;
		if(strFecha!= null && !strFecha.isEmpty()){
			DateFormat formateaFecha = new SimpleDateFormat(formato,LOCALE_ESPANOL_MEXICO);
			try {
				fecha = formateaFecha.parse(strFecha);
			} catch (ParseException e) {
				LOG.error(e);
			}
		}
		return fecha;
	}
	
	/**
	 * Metodo que permite generar un Objeto Date a partir de un String
	 * @param fecha Objeto del tipo String que almaecena una fecha
	 * @param formato Cadena con el formato
	 * @return Date, Regresa la fecha en un Objeto Date
	 */
	public Date cadenaToFecha(final String fecha,final String formato){
		Date date = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato,LOCALE_ESPANOL_MEXICO);
		try {
			date = simpleDateFormat.parse(fecha);
		} catch (ParseException e) {
			LOG.error(e);
		}
		return date;
	}
	
	/**
	 * Metodo que permite generar un Objeto Date a partir de un String
	 * @param fecha Objeto del tipo String que almacena una fecha
	 * @param formatoOrigen Cadena con el formato 
	 * @param formatoDestino Cadena con el formato
	 * @return String, Regresa la fecha en un Objeto Date
	 */
	public String formatoToformatoFecha(final String fecha,final String formatoOrigen, final String formatoDestino){
		Date date = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatoOrigen,LOCALE_ESPANOL_MEXICO);
		String fechaFormato = "";
		try {
			date = simpleDateFormat.parse(fecha);
			simpleDateFormat.applyPattern(formatoDestino);
			fechaFormato = simpleDateFormat.format(date);
		} catch (ParseException e) {
			LOG.error(e);
		}
		return fechaFormato;
	}
	
	
	/**
	 * Metodo que sirve para obtener el valor String de un campo
	 * @param obj Objeto que se quiere transformar a String
	 * @return String del objeto
	 */
	public String getString(Object obj){
		String cad = "";
		if(obj!= null){
			cad = obj.toString().trim();
		}
		return cad;
	}
	
	/**
	 * Metodo que sirve para obtener el Date de un Object
	 * @param obj Objeto que se quiere transformar a String
	 * @return Date Objeto de retorno
	 */
	public Date getDate(Object obj){
		Date cad = null;
		if(obj instanceof Date){
			cad = (Date)obj;
		}
		return cad;
	}
	
	/**
	 * Metodo que sirve para obtener el Integer de un Object
	 * @param obj Objeto que se quiere transformar a String
	 * @return BigInteger Objeto de retorno
	 */
	public Integer getInteger(Object obj){
		Integer cad = null;
		if(obj instanceof Number){
			cad = ((Number)obj).intValue();
		}
		return cad;
	}
	
	/**
	 * Metodo que sirve para obtener el BigInteger de un Object
	 * @param obj Objeto que se quiere transformar a String
	 * @return BigInteger Objeto de retorno
	 */
	public BigInteger getBigInteger(Object obj){
		BigInteger cad = null;
		if(obj instanceof BigInteger){
			cad = ((BigInteger)obj);
		}
		return cad;
	}
	
	/**
	 * Metodo que sirve para obtener el BigDecimal de un Object
	 * @param obj Objeto que se quiere transformar a String
	 * @return BigInteger Objeto de retorno
	 */
	public BigDecimal getBigDecimal(Object obj){
		BigDecimal cad = BigDecimal.ZERO;
		if(obj instanceof BigDecimal){
			cad = ((BigDecimal)obj);
		}
		return cad;
	}
	
	/**
	 * Metodo que sirve para generar un objeto del tipo BeanReqInsertBitacora
	 * @param servicio Objeto del tipo String que almacena la propiedad del servicio
	 * @param numReg Objeto del tipo String que almacena la propiedad del numero de registros
	 * @param nomArchivo Objeto del tipo String que almacena el nombre del archivo
	 * @param fechaHora Objeto del tipo Date que almacena la fecha y hora de la operacion
	 * @param estatusOperacion Objeto del tipo String que almacena el estatus de la operacion
	 * @param codOperacion Objeto del tipo String que almacena el codigo de operacion
	 * @param tablaAfectada Objeto del tipo String que almacena la tabla afectada
	 * @param datoFijo Objeto del tipo String que almacena el dato fijo
	 * @param codError Objeto del tipo String que almacena el codigo de error
	 * @param descCodError Objeto del tipo String que almacena la descripcion del codigo de error
	 * @param descOperacion Objeto del tipo String que almacena la descripcion de la operacion
	 * @param valNuevo Objeto del tipo String que almacena el valor nuevo
	 * @return BeanReqInsertBitacora objeto del tipo BeanReqInsertBitacora
	 */
	public BeanReqInsertBitacora creaBitacora(String servicio,String numReg,String nomArchivo,Date fechaHora,
			String estatusOperacion,String codOperacion,
			String tablaAfectada,String datoFijo,
             String codError,String descCodError,String descOperacion, String valNuevo){
		BeanReqInsertBitacora beanReqInsertBitacora = new BeanReqInsertBitacora();
		beanReqInsertBitacora.setServicio(servicio);
		beanReqInsertBitacora.setNumeroRegistros(numReg);
		beanReqInsertBitacora.setNomArchivo(nomArchivo);
		beanReqInsertBitacora.setFechaHora(fechaHora);
		beanReqInsertBitacora.setEstatusOperacion(estatusOperacion);
		beanReqInsertBitacora.setCodOperacion(codOperacion);
		beanReqInsertBitacora.setTablaAfectada(tablaAfectada);
		beanReqInsertBitacora.setCodError(codError);
		beanReqInsertBitacora.setDescCodError(descCodError);
		beanReqInsertBitacora.setDescOperacion(descOperacion);
		beanReqInsertBitacora.setDatoFijo(datoFijo);
		beanReqInsertBitacora.setValNuevo(valNuevo);
		return beanReqInsertBitacora;
	}
	
	/**
	 * Metodo que sirve para generar un objeto del tipo BeanReqInsertBitacora
	 * @param servicio Objeto del tipo String que almacena la propiedad del servicio
	 * @param numReg Objeto del tipo String que almacena la propiedad del numero de registros
	 * @param nomArchivo Objeto del tipo String que almacena el nombre del archivo
	 * @param fechaHora Objeto del tipo Date que almacena la fecha y hora de la operacion
	 * @param estatusOperacion Objeto del tipo String que almacena el estatus de la operacion
	 * @param codOperacion Objeto del tipo String que almacena el codigo de operacion
	 * @param tablaAfectada Objeto del tipo String que almacena la tabla afectada
	 * @param datoFijo Objeto del tipo String que almacena el dato fijo
	 * @param codError Objeto del tipo String que almacena el codigo de error
	 * @param descCodError Objeto del tipo String que almacena la descripcion del codigo de error
	 * @param descOperacion Objeto del tipo String que almacena la descripcion de la operacion
	 * @param valNuevo Objeto del tipo String que almacena el valor nuevo
	 * @param valAnt Objeto del tipo String que almacena el valor anterior
	 * @return BeanReqInsertBitacora objeto del tipo BeanReqInsertBitacora
	 */
	public BeanReqInsertBitacora creaBitacoraMod(String servicio,String numReg,String nomArchivo,Date fechaHora,
			String estatusOperacion,String codOperacion,
			String tablaAfectada,String datoFijo,
             String codError,String descCodError,String descOperacion, String valNuevo, String valAnt){
		BeanReqInsertBitacora beanReqInsertBitacora = new BeanReqInsertBitacora();
		beanReqInsertBitacora.setServicio(servicio);
		beanReqInsertBitacora.setNumeroRegistros(numReg);
		beanReqInsertBitacora.setNomArchivo(nomArchivo);
		beanReqInsertBitacora.setFechaHora(fechaHora);
		beanReqInsertBitacora.setEstatusOperacion(estatusOperacion);
		beanReqInsertBitacora.setCodOperacion(codOperacion);
		beanReqInsertBitacora.setTablaAfectada(tablaAfectada);
		beanReqInsertBitacora.setCodError(codError);
		beanReqInsertBitacora.setDescCodError(descCodError);
		beanReqInsertBitacora.setDescOperacion(descOperacion);
		beanReqInsertBitacora.setDatoFijo(datoFijo);
		beanReqInsertBitacora.setValNuevo(valNuevo);
		beanReqInsertBitacora.setValAnt(valAnt);
		return beanReqInsertBitacora;
	}
	
	/**
	 * Metodo publico que realiza el Split
	 * @param cad String con la cadena
	 * @param c Char con el caracter que se quiere separar
	 * @return String[] Arreglo de String que regresa la respuesta
	 */
	public String[] split(String cad, char c){
		List<String> list = new ArrayList<String>();
		int posIni=0;
		int posFin=0;
		char arrChar[]= cad.toCharArray();
		if(cad == null){
			return list.toArray(new String[0]);
		}
		for(int i=0;i<cad.length();i++){
			char temp = arrChar[i];
			if(temp==c){			
				list.add(new String(arrChar,posIni,posFin-posIni));
				posIni=posFin+1;
				
			}
			posFin++;
		}
		if(posFin<=arrChar.length){
			list.add(new String(arrChar,posIni,posFin-posIni));
		}else{
			list.add("");
		}
		return list.toArray(new String[list.size()]);
	}
	
	
}
