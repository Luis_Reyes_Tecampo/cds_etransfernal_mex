package mx.isban.eTransferNal.utilerias.comunes;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

import org.apache.log4j.Logger;

public class ValidateFechas {
	
	/**
	 * Propiedad del tipo String que almacena el valor de FORMATO_FECHA_DDMMAAAA
	 */
	public final static String FORMATO_FECHA_DDMMAAAA = "ddMMyyyy";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FORMATO_FECHA_DD_DIAG_MM_DIAG_AAAA
	 */
	public final static String FORMATO_FECHA_DD_DIAG_MM_DIAG_AAAA = "dd/MM/yyyy";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FORMATO_FECHA_HHMMSSSS
	 */
	public final static String FORMATO_FECHA_HHMMSSSS = "HH:mm:ss.SS";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FORMATO_FECHA_HHMMSS
	 */
	public final static String FORMATO_FECHA_HHMMSS = "HH:mm:ss";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FORMATO_FECHA_HHMM
	 */
	public final static String FORMATO_FECHA_HHMM = "HH:mm";
	
	
	/**Referencia al LOG */
	private static final Logger LOG = Logger.getLogger(Validate.class);
	
	/**
	 * Metodo que valida la fecha
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoFechaVacia(String cad,String formato){
		boolean flag = false;
		if(cad == null){
			return false;
		}else if("".equals(cad.trim())){
			return true;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato,Utilerias.LOCALE_ESPANOL_MEXICO);
		try {
			simpleDateFormat.parse(cad);
			flag = true;
		} catch (ParseException e) {
			LOG.error("Error al validar fecha",e);
		}
		return flag;
	}
	
	/**
	 * Metodo que valida la fecha
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidaFecha(String cad,String formato){
		boolean flag = false;
		if(cad == null||"".equals(cad.trim())){
			return false;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato,Utilerias.LOCALE_ESPANOL_MEXICO);
		try {
			simpleDateFormat.parse(cad);
			flag = true;
		} catch (ParseException e) {
			LOG.error("Error al validar fecha",e);
		}
		return flag;
	}
	


}
