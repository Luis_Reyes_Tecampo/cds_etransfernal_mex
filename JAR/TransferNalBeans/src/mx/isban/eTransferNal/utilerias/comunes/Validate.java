package mx.isban.eTransferNal.utilerias.comunes;



public class Validate{
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_IMPORTE_MAXIMO
	 */
	public final static String REG_IMPORTE_MAXIMO = "([0-9]{0,12})|([0-9]{0,12}[.]{1}[0-9]{1,2})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_IMPORTE_MAXIMO
	 */
	public final static String REG_IMPORTE_MAXIMO_13_2 = "([0-9]{0,13})|([0-9]{0,13}[.]{1}[0-9]{1,2})";
	/**
	 * Propiedad del tipo String que almacena el valor de REG_NUMERICO_12
	 */
	public final static String REG_NUMERICO_MAX_12 = "([0-9]{1,12})";
	/**
	 * Propiedad del tipo String que almacena el valor de REG_NUMERICO_12
	 */
	public final static String REG_NUMERICO_MAX_7 = "([0-9]{1,7})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_NUMERICO_1
	 */
	public final static String REG_NUMERICO_1 = "([0-9]{1})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_NUMERICO_2
	 */
	public final static String REG_NUMERICO_2 = "([0-9]{2})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_NUMERICO_5
	 */
	public final static String REG_NUMERICO_5 = "([0-9]{5})";

	/**
	 * Propiedad del tipo String que almacena el valor de REG_TOPOLOGIA
	 */
	public final static String REG_TOPOLOGIA = "([V|T|v|t]{1})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_ENVIAR
	 */
	public final static String REG_ENVIAR = "([S|N|s|n]{1})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_CADENA
	 */
	public final static String REG_CADENA = "([A-Z|a-z|0-9|\\s|\\.|,]{1,})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_CADENA_SOLO_LETRAS_2
	 */
	public final static String REG_CADENA_SOLO_LETRAS_2 = "([A-Z|a-z]{2})";
	/**
	 * Propiedad del tipo String que almacena el valor de REG_CADENA_ALFANUMERICO_8
	 */
	public final static String REG_CADENA_ALFANUMERICO_8 = "([A-Z|a-z|0-9]{8})";
	
	/**
	 * Propiedad del tipo String que almacena el valor de REG_CADENA_ALFANUMERICO_5
	 */
	public final static String REG_CADENA_ALFANUMERICO_5 = "([A-Z|a-z|0-9]{5})";
	
	
	/**
	 * Metodo que valida el importe maximo
	 * @param number Objeto del tipo Number que almnacena el valor a valiodar
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoImporteMaximo(Number number){
		String num;
		if(number == null){
			return false;
		}
		num = number.toString();
		return num.matches(REG_IMPORTE_MAXIMO);
	}
	
	/**
	 * Metodo que valida el importe maximo
	 * @param number Objeto del tipo Number que almnacena el valor a valiodar
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoImporteMaximoMayor0(Number number){
		String num;
		if(number == null){
			return false;
		}
		double d = number.doubleValue();
		if(d==0){
			return false;
		}
		num = number.toString();
		return num.matches(REG_IMPORTE_MAXIMO);
	}
	
	/**
	 * Metodo que valida enteros
	 * @param number Objeto del tipo Number que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoNumero(Number number,String formato){
		String num;
		if(number == null){
			return false;
		}
		num = number.toString();
		return num.matches(formato);
	}
	
	/**
	 * Metodo que valida enteros
	 * @param number Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoNumero(String number,String formato){
		String num;
		if(number == null||"".equals(number)){
			return false;
		}
		num = number.replaceAll(",", ""); 
		return num.matches(formato);
	}
	
	/**
	 * Metodo que valida enteros
	 * @param number Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoNumeroVacio(String number,String formato){
		String num;
		if(number == null){
			return false;
		}else if("".equals(number)){
			return true;
		}
		num = number.replaceAll(",", ""); 
		return num.matches(formato);
	}
	/**
	 * Metodo que valida el importe maximo
	 * @param number Objeto del tipo String que almacena el valor a valiodar
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoImporteMaximo(String number){
		String num;
		if(number == null||"".equals(number)){
			return false;
		}
		num = number.replaceAll(",", ""); 
		return num.matches(REG_IMPORTE_MAXIMO);
	}
	
	/**
	 * Metodo que valida el importe maximo
	 * @param number Objeto del tipo String que almacena el valor a valiodar
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoImporteMaximoMayor0(String number){
		String num;
		if(number == null||"".equals(number)){
			return false;
		}
		num = number.replaceAll(",", "");
		if(num.matches(REG_IMPORTE_MAXIMO)){
			double d = Double.parseDouble(num);
			if(d>0){
				return true;
			}
		}
		return false;
	}
	



	

}

