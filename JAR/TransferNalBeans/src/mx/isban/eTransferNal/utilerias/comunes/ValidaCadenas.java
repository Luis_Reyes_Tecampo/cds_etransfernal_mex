package mx.isban.eTransferNal.utilerias.comunes;

public class ValidaCadenas {
	/**
	 * Metodo que valida cadenas
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param longitud String con el valor de la longitud
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoCadena(String cad, int longitud){
		String cadena=cad;
		if(cadena == null||"".equals(cadena)){
			return false;
		}
		return cadena.matches(Validate.REG_CADENA)&&cadena.length()<=longitud;
	}
	/**
	 * Metodo que valida cadenas
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param longitud String con el valor de la longitud
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoCadenaVacia(String cad, int longitud){
		String cadena=cad;
		if(cadena == null){
			return false;
		}else if("".equals(cadena)){
			return true;
		}
		return cadena.matches(Validate.REG_CADENA)&&cadena.length()<=longitud;
	}
	/**
	 * Metodo que valida cadena que pueden ser vacias
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoCadenaVacia(String cad,String formato){
		if(cad == null){
			return false;
		}else if("".equals(cad.trim())){
			return true;
		}
		return cad.matches(formato);
	}
	
	/**
	 * Metodo que valida cadena que pueden ser vacias
	 * @param cad Objeto del tipo String que almacena el valor a valiodar
	 * @param formato String con el formato
	 * @return boolean true si es correcto false en caso contrario
	 */
	public boolean esValidoCadena(String cad,String formato){
		if(cad == null){
			return false;
		}else if("".equals(cad.trim())){
			return true;
		}
		return cad.matches(formato);
	}
}
