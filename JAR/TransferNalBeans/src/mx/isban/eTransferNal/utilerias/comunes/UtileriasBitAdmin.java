/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* UtileriasBitAdmin.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.utilerias.comunes;

import java.util.Date;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdminDos;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

public class UtileriasBitAdmin {
	
	/**
	 * Propiedad del tipo UtileriasBitTrans que almacena el valor de utileriasBitTrans
	 */
	private static final UtileriasBitAdmin UTILERIAS = new UtileriasBitAdmin();
	
	/**
	 * Propiedad del tipo String que almacena el valor de NO_APLICA
	 */

	
	/**
	 * @return UtileriasBitTrans
	 */
	public static UtileriasBitAdmin getUtileriasBitAdmin(){
		return UTILERIAS;
	}	
	
	/**
	 * @param codOper Objeto del tipo String
	 * @param tablaAfect Objeto del tipo String
	 * @param tipoOper Objeto del tipo String
	 * @param servTran Objeto del tipo String
	 * @param valAnt Objeto del tipo String
	 * @param valNvo Objeto del tipo String
	 * @param datoModi Objeto del tipo String
	 * @param datoFijo Objeto del tipo String
	 * @param comentarios Objeto del tipo String
	 * @param sesion Objeto del tipo String
	 * @return BeanReqInsertBitAdmin
	 */
	public BeanReqInsertBitAdmin createBitacoraBitAdmin(String codOper, String tablaAfect, String tipoOper,
		String servTran, String valAnt, String valNvo, String datoModi, String datoFijo, String	comentarios, 
		ArchitechSessionBean sesion){
		
		Date fecha = new Date();
		Utilerias utilerias  = Utilerias.getUtilerias();
		
		BeanReqInsertBitAdmin beanReqInsertBitAdmin = new BeanReqInsertBitAdmin();
		beanReqInsertBitAdmin.setBeanReqInsertBitAdminDos(new BeanReqInsertBitAdminDos());
		
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setCodOper(codOper);
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setTablaAfec(tablaAfect);
		beanReqInsertBitAdmin.setTipoOper(tipoOper);
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setServTran(servTran);
		beanReqInsertBitAdmin.setValAnt(valAnt);
		beanReqInsertBitAdmin.setValNvo(valNvo);
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDatoMobi(datoModi);
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDatoFijo(datoFijo);
		beanReqInsertBitAdmin.setComentarios(comentarios);

		beanReqInsertBitAdmin.setUserOper(sesion.getUsuario());
		beanReqInsertBitAdmin.setIdToken("0");
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDirIp(sesion.getIPCliente());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setCanApli("Transfer Nacional");
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setInstWeb(sesion.getNombreServidor());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setHostWeb(sesion.getIPServidor());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setIdSession(sesion.getIdSesion());
		beanReqInsertBitAdmin.setFchOper(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanReqInsertBitAdmin.setFchAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanReqInsertBitAdmin.setHoraAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
		return beanReqInsertBitAdmin;
	}
}
