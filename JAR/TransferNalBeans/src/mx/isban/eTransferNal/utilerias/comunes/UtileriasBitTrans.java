package mx.isban.eTransferNal.utilerias.comunes;

import java.util.Date;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanDatosGenerales;
import mx.isban.eTransferNal.beans.comunes.BeanDatosOpe;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitTrans;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoOrd;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDBancoRec;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDDatGenerales;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

public class UtileriasBitTrans {
	
	/**
	 * Propiedad del tipo UtileriasBitTrans que almacena el valor de utileriasBitTrans
	 */
	private static final UtileriasBitTrans UTILERIAS = new UtileriasBitTrans();
	
	/**
	 * Propiedad del tipo String que almacena el valor de NO_APLICA
	 */
	private static final String NO_APLICA ="No aplica";
	/**
	 * @return UtileriasBitTrans
	 */
	public static UtileriasBitTrans getUtileriasBitTrans(){
		return UTILERIAS;
	}
	
	
	
	/**
	 * Metodo que permite generar el bean de la bitacora Trans
	 * @param idOperacion String idOperacion
	 * @param codError String con el codigo de error
	 * @param descErr String con la descripcion del error
	 * @param descOper String con la descripcion de la operacion
	 * @param comentarios String con los comentarios
	 * @param servTran String con el servTran 
	 * @param nombreArchivo String con el nombre del Archivo
	 * @param bean Objeto del tipo BeanReceptoresSPID
	 * @param sesion Objeto del tipo ArchitechSessionBean
	 * @return BeanReqInsertBitTrans Objeto de respuesta que encapsula los datos de la bitacora
	 */
	public BeanReqInsertBitTrans createBitacoraBitTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, BeanReceptoresSPID bean,ArchitechSessionBean sesion){
		BeanReqInsertBitTrans beanReqInsertBitTrans = new BeanReqInsertBitTrans();
		beanReqInsertBitTrans.setDatosOpe(createDatosOpeBitTran(bean, idOperacion, codError,descErr,descOper, comentarios,servTran));
		beanReqInsertBitTrans.setDatosGenerales(createDatosGenOpeBitTran(nombreArchivo, sesion));
		return beanReqInsertBitTrans;
	}
	
	/**
	 * Metodo que permite generar el bean de la bitacora Trans
	 * @param idOperacion String idOperacion
	 * @param codError String con el codigo de error
	 * @param descErr String con la descripcion del error
	 * @param descOper String con la descripcion de la operacion
	 * @param comentarios String con los comentarios
	 * @param servTran String con el servTran 
	 * @param nombreArchivo String con el nombre del Archivo
	 * @param fchOperacion String con la fecha de la operacion
	 * @param referencia String con la referencia
	 * @param numCuentaOrd String con el numero de cuenta ordenante
	 * @param numCuentaRec String con el numero de cuenta receptor
     * @param estatus String con el estatus 
     * @param monto Objeto BigDecimal
	 * @param sesion Objeto del tipo ArchitechSessionBean
	 * @return BeanReqInsertBitTrans Objeto de respuesta que encapsula los datos de la bitacora
	 */
	public BeanReqInsertBitTrans createBitacoraBitTrans(String idOperacion, String codError, String descErr, String descOper, 
			String comentarios, String servTran, String nombreArchivo, String fchOperacion,String referencia,
			String numCuentaOrd, String numCuentaRec,
			String estatus,String monto, ArchitechSessionBean sesion){
		BeanReqInsertBitTrans beanReqInsertBitTrans = new BeanReqInsertBitTrans();
		beanReqInsertBitTrans.setDatosOpe(createDatosOpeBitTran(fchOperacion,referencia,
				numCuentaOrd, numCuentaRec,
				estatus,monto, 
				idOperacion, codError,descErr,descOper, comentarios,
				servTran));
		beanReqInsertBitTrans.setDatosGenerales(createDatosGenOpeBitTran(nombreArchivo, sesion));
		return beanReqInsertBitTrans;
	}
	
	/**
	 * Metodo que permite generar el bean de la bitacora Trans
	 * @param bean Objeto del tipo BeanReceptoresSPID
	 * @param idOperacion String idOperacion
	 * @param codError String con el codigo de error
	 * @param descErr String con la descripcion del error
	 * @param descOper String con la descripcion de la operacion
	 * @param comentarios String con los comentarios
	 * @param servTran String con el servTran
	 * @return BeanDatosOpe Objeto de respuesta que encapsula los datos de la bitacora
	 */
	private BeanDatosOpe createDatosOpeBitTran(BeanReceptoresSPID bean, 
			String idOperacion, String codError,String descErr,String descOper, String comentarios,
			String servTran){
		Utilerias utilerias  = Utilerias.getUtilerias();
		BeanDatosOpe datosOpe = new BeanDatosOpe();
		StringBuilder builder = new StringBuilder();
		BeanSPIDLlave llave = bean.getLlave();
		BeanSPIDBancoOrd bancoOrd = bean.getBancoOrd();
		BeanSPIDBancoRec bancoRec = bean.getBancoRec();
		BeanSPIDDatGenerales datGenerales = bean.getDatGenerales();
		datosOpe.setFchOperacion(llave.getFchOperacion());
		datosOpe.setEstatusOperacion("");
		datosOpe.setMonto(utilerias.getString(datGenerales.getMonto()));
		datosOpe.setTipoCambio("");
		builder.append("FCH_OPERACION =");
		builder.append(llave.getFchOperacion());
		builder.append("|CVE_MI_INSTITUC =");
		builder.append(llave.getCveMiInstituc());
		builder.append("|CVE_INST_ORD =");
		builder.append(llave.getCveInstOrd());
		builder.append("|FOLIO_PAQUETE =");
		builder.append(llave.getFolioPaquete());
		builder.append("|FOLIO_PAGO =");
		builder.append(llave.getFolioPago());
		
		builder.append("|REFE_TRANSFER =");
		builder.append(bean.getDatGenerales().getRefeTransfer());
		 
		datosOpe.setReferencia(builder.toString());
		datosOpe.setCtaOrigen(bancoOrd.getNumCuentaOrd());
		datosOpe.setCtaDestino(bancoRec.getNumCuentaRec());
		datosOpe.setIdOperacion(idOperacion);
		datosOpe.setBancoDestino(NO_APLICA);
		datosOpe.setCodigoErr(codError);
		datosOpe.setDescErr(descErr);
		datosOpe.setDescOper(descOper);
		datosOpe.setComentarios(comentarios);
		datosOpe.setServTran(servTran);
		return datosOpe;
	}
	
	/**
	 * Metodo que permite generar el bean de la bitacora Trans
	 * @param fchOperacion String con la fecha de la operacion
	 * @param referencia String con la referencia
	 * @param numCuentaOrd String con el numero de cuenta ordenante
	 * @param numCuentaRec String con el numero de cuenta receptor
     * @param estatus String con el estatus 
     * @param monto Objeto BigDecimal
     * @param idOperacion String idOperacion
	 * @param codError String con el codigo de error
	 * @param descErr String con la descripcion del error
	 * @param descOper String con la descripcion de la operacion
	 * @param comentarios String con los comentarios
	 * @param servTran String con el servTran
	 * @return BeanDatosOpe Objeto de respuesta que encapsula los datos de la bitacora
	 */
	private BeanDatosOpe createDatosOpeBitTran(String fchOperacion,String referencia,
			String numCuentaOrd, String numCuentaRec,
			String estatus,String monto, 
			String idOperacion, String codError,String descErr,String descOper, String comentarios,
			String servTran){
		Utilerias utilerias  = Utilerias.getUtilerias();
		BeanDatosOpe datosOpe = new BeanDatosOpe();
		StringBuilder builder = new StringBuilder();
		
		datosOpe.setFchOperacion(fchOperacion);
		datosOpe.setEstatusOperacion(estatus);
		datosOpe.setMonto(utilerias.getString(monto));
		datosOpe.setTipoCambio("");
		builder.append("REFERENCIA =");
		builder.append(referencia);
		datosOpe.setReferencia(builder.toString());
		datosOpe.setCtaOrigen(numCuentaOrd);
		datosOpe.setCtaDestino(numCuentaRec);
		datosOpe.setIdOperacion(idOperacion);
		datosOpe.setBancoDestino(NO_APLICA);
		datosOpe.setCodigoErr(codError);
		datosOpe.setDescErr(descErr);
		datosOpe.setDescOper(descOper);
		datosOpe.setComentarios(comentarios);
		datosOpe.setServTran(servTran);
		return datosOpe;
	}
	
	
	/**
	 * Metodo que permite generar el bean de la bitacora Trans
	 * @param nombreArchivo String con el nombre del archivo
	 * @param sesion Objeto del tipo ArchitechSessionBean
	 * @return BeanDatosGenerales Objeto de respuesta que encapsula los datos de la bitacora
	 */
	private BeanDatosGenerales createDatosGenOpeBitTran(String nombreArchivo, ArchitechSessionBean sesion){
		Utilerias utilerias  = Utilerias.getUtilerias();
		Date fecha = new Date();
		BeanDatosGenerales beanDatosGenerales = new BeanDatosGenerales();
		beanDatosGenerales.setIdToken("0");
		beanDatosGenerales.setDirIp(sesion.getIPCliente());
		beanDatosGenerales.setAplicacionCanal("Transfer Nacional");
		beanDatosGenerales.setInstanciaWeb(sesion.getNombreServidor());
		beanDatosGenerales.setHostWeb(sesion.getIPServidor());
		beanDatosGenerales.setIdSesion(sesion.getIdSesion());
		beanDatosGenerales.setCodCliente(NO_APLICA);
		beanDatosGenerales.setContrato(NO_APLICA);
		beanDatosGenerales.setNumeroTitulos("0");
		beanDatosGenerales.setFchProgramada("");
		beanDatosGenerales.setFchAplica(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanDatosGenerales.setFchAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanDatosGenerales.setHoraAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
		beanDatosGenerales.setNombreArchivo(nombreArchivo);
		return beanDatosGenerales;
	}
	
	/**
	 * Metodo que permite generar la llave
	 * @param fchOperacion String con la fecha de operacion
	 * @param cveMiInstituc String cve mi institucion
	 * @param cveInstOrd String cve Institucion Ordenante
	 * @param folioPaquete String con el folio paquete
	 * @param folioPago String con el folio pago
	 * @return String con los datos de la llave
	 */
	public String seteaKey(String fchOperacion,String cveMiInstituc, String cveInstOrd, String folioPaquete, String folioPago){
		StringBuilder strBuild = new StringBuilder();
		String pipe ="|";
		strBuild.append("FCH_OPERACION=");
		strBuild.append(fchOperacion);
		strBuild.append(pipe);
		strBuild.append("CVE_MI_INSTITUC=");
		strBuild.append(cveMiInstituc);
		strBuild.append(pipe);
		strBuild.append("CVE_INST_ORD=");
		strBuild.append(cveInstOrd);
		strBuild.append(pipe);
		strBuild.append("FOLIO_PAQUETE=");
		strBuild.append(folioPaquete);
		strBuild.append(pipe);
		strBuild.append("FOLIO_PAGO=");
		strBuild.append(folioPago);
		return strBuild.toString();
	}
	
	/**
	 * Metodo que permite generar la llave
	 * @param numCuentaOrd String con el numero de cuenta ordenante
	 * @param numCuentaRec String cve mi institucion
	 * @param importe String con el importe
	 * @return String con la trama de campos
	 */
	public String seteaCampos(String numCuentaOrd,String numCuentaRec,String importe){
		StringBuilder strBuild = new StringBuilder();
		String pipe ="|";
		strBuild.append(pipe);
		strBuild.append("NUM_CUENTA_ORD=");
		strBuild.append(numCuentaOrd);
		strBuild.append(pipe);
		strBuild.append("NUM_CUENTA_REC=");
		strBuild.append(numCuentaRec);
		strBuild.append(pipe);
		strBuild.append("IMPORTE=");
		strBuild.append(importe);
		strBuild.append(pipe);
		return strBuild.toString();
	}
	
    /**
     * @param beanReceptoresSPID Bean con los datos del registro
     * @param estatus String con el estatus
     * @return BeanReqActTranSpeiRec bean con los datos del registro a actualizar
     */
    public BeanReqActTranSpeiRec getBeanReqActTranSpeiRec(BeanReceptoresSPID beanReceptoresSPID, String estatus){
    	BeanReqActTranSpeiRec beanReqActTranSpeiRec = new BeanReqActTranSpeiRec();
    	beanReqActTranSpeiRec.setFechaOperacion(beanReceptoresSPID.getLlave().getFchOperacion());
		beanReqActTranSpeiRec.setCveInstOrd(beanReceptoresSPID.getLlave().getCveInstOrd());
		beanReqActTranSpeiRec.setCveMiInstituc(beanReceptoresSPID.getLlave().getCveMiInstituc());
		beanReqActTranSpeiRec.setFolioPago(beanReceptoresSPID.getLlave().getFolioPago());
		beanReqActTranSpeiRec.setFolioPaquete(beanReceptoresSPID.getLlave().getFolioPaquete());
		beanReqActTranSpeiRec.setMotivoDevol(beanReceptoresSPID.getEnvDev().getMotivoDev());
		beanReqActTranSpeiRec.setEstatus(estatus);
		beanReqActTranSpeiRec.setReferenciaTransfer(beanReceptoresSPID.getDatGenerales().getRefeTransfer());
		beanReqActTranSpeiRec.setFchCaptura(beanReceptoresSPID.getDatGenerales().getFchCaptura());
		return beanReqActTranSpeiRec;
    }
	
   
    

}
