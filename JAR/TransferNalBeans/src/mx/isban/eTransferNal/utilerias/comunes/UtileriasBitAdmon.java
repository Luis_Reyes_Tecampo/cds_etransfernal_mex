package mx.isban.eTransferNal.utilerias.comunes;

import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmon;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;

public class UtileriasBitAdmon {
	/**
	 * Propiedad del tipo UtileriasBitAdmon que almacena el valor de UtileriasBitAdmon
	 */
	private static final UtileriasBitAdmon UTILERIAS = new UtileriasBitAdmon();
	
	/**
	 * @return UtileriasBitAdmon Objeto del tipo UtileriasBitAdmon
	 */
	public static UtileriasBitAdmon getUtileriasBitAdmon(){
		return UTILERIAS;
	}
	
	
	/**
	 * @param fechaOperacion String con la fecha de operacion
	 * @param tipoOperacion String con el tipo de operacion
	 * @param valAnterior String con el valor anterior
	 * @param valNuevo String con el Valor nuevo
	 * @param comentarios String con los comentarios
	 * @param datoFijo String con los datos fijos
	 * @param servTran String con el servicio
	 * @param datoMod String con los datos modificados
	 * @param tablaAfectada String con el nombre de la tabla afectada
	 * @param codOperacion String con el codigo de operacion
	 * @param canalAplica String con el canal que aplica
	 * @return BeanReqInsertBitAdmon  Bean con los datos para insertar en la bitacora administrativa
	 */
	public BeanReqInsertBitAdmon createBitacoraBitTrans(
			String fechaOperacion, String tipoOperacion, String valAnterior, 
			String valNuevo, String comentarios, String datoFijo, 
			String servTran, String datoMod, String tablaAfectada,
			String codOperacion, String canalAplica){
		BeanReqInsertBitAdmon beanReqInsertBitAdmon = new BeanReqInsertBitAdmon();
		beanReqInsertBitAdmon.setFechaOperacion(fechaOperacion);
		beanReqInsertBitAdmon.setTipoOperacion(tipoOperacion);
		beanReqInsertBitAdmon.setValAnterior(valAnterior);
		beanReqInsertBitAdmon.setValNuevo(valNuevo);
		beanReqInsertBitAdmon.setComentarios(comentarios);
		beanReqInsertBitAdmon.setDatoFijo(datoFijo);
		beanReqInsertBitAdmon.setServTran(servTran);
		beanReqInsertBitAdmon.setDatoMod(datoMod);
		beanReqInsertBitAdmon.setTablaAfectada(tablaAfectada);
		beanReqInsertBitAdmon.setCodOperacion(codOperacion);
		beanReqInsertBitAdmon.setCanalAplica(canalAplica);
		return beanReqInsertBitAdmon;
	}
	
	/**
	 * @param fchOperacion String con la fecha de operacion
	 * @param cveMiInstituc String con la clave mi institucion
	 * @param cveInstOrd String con la cve Institucion ordenante
	 * @param folioPaquete String con el folio paquete
	 * @param folioPago String con el folio pago
	 * @return String con los datos de la llave
	 */
	public String createDatoFijo(String fchOperacion, String cveMiInstituc, String cveInstOrd, String folioPaquete, String folioPago){
		final String PIPE = "|";
		StringBuilder builder = new StringBuilder();
		builder.append("fchOperacion =");
		builder.append(fchOperacion);
		builder.append(PIPE);
		builder.append("cveMiInstituc =");
		builder.append(cveMiInstituc);
		builder.append(PIPE);
		builder.append("cveInstOrd =");
		builder.append(cveInstOrd);
		builder.append(PIPE);
		builder.append("folioPaquete =");
		builder.append(folioPaquete);
		builder.append(PIPE);
		builder.append("folioPago =");
		builder.append(folioPago);			
		return builder.toString();
	}
	
	/**
	 * @param beanReceptoresSPID Objecto a BeanReceptoresSPID
	 * @return String con la cadena json
	 */
	public String transformJavaToJson(BeanReceptoresSPID beanReceptoresSPID){
		final String PIPE = "|";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("fchOperacion=").append(beanReceptoresSPID.getLlave().getFchOperacion()).append(PIPE);
		strBuilder.append("cveMiInstituc=").append(beanReceptoresSPID.getLlave().getCveMiInstituc()).append(PIPE);
		strBuilder.append("cveInstOrd=").append(beanReceptoresSPID.getLlave().getCveInstOrd()).append(PIPE);
		strBuilder.append("folioPago=").append(beanReceptoresSPID.getLlave().getFolioPago()).append(PIPE);
		strBuilder.append("folioPaquete=").append(beanReceptoresSPID.getLlave().getFolioPaquete()).append(PIPE);
		strBuilder.append("cveRastreo=").append(beanReceptoresSPID.getRastreo().getCveRastreo()).append(PIPE);
		strBuilder.append("cveRastreoOri=").append(beanReceptoresSPID.getRastreo().getCveRastreoOri()).append(PIPE);
		strBuilder.append("longRastreo=").append(beanReceptoresSPID.getRastreo().getLongRastreo()).append(PIPE);
		strBuilder.append("direccionIp=").append(beanReceptoresSPID.getRastreo().getDireccionIp()).append(PIPE);
		strBuilder.append("topologia=").append(beanReceptoresSPID.getDatGenerales().getTopologia()).append(PIPE);
		strBuilder.append("cda=").append(beanReceptoresSPID.getDatGenerales().getCda()).append(PIPE);
		strBuilder.append("enviar=").append(beanReceptoresSPID.getDatGenerales().getEnviar()).append(PIPE);
		strBuilder.append("clasifOperacion=").append(beanReceptoresSPID.getDatGenerales().getClasifOperacion()).append(PIPE);
		strBuilder.append("tipoOperacion=").append(beanReceptoresSPID.getDatGenerales().getTipoOperacion()).append(PIPE);
		strBuilder.append("prioridad=").append(beanReceptoresSPID.getDatGenerales().getPrioridad()).append(PIPE);
		strBuilder.append("tipoPago=").append(beanReceptoresSPID.getDatGenerales().getTipoPago()).append(PIPE);
		strBuilder.append("refeTransfer=").append(beanReceptoresSPID.getDatGenerales().getRefeTransfer()).append(PIPE);
		strBuilder.append("conceptoPago=").append(beanReceptoresSPID.getDatGenerales().getConceptoPago()).append(PIPE);
		strBuilder.append("fchCaptura=").append(beanReceptoresSPID.getDatGenerales().getFchCaptura()).append(PIPE);
		strBuilder.append("cde=").append(beanReceptoresSPID.getDatGenerales().getCde()).append(PIPE);
		strBuilder.append("monto=").append(beanReceptoresSPID.getDatGenerales().getMonto()).append(PIPE);		
		strBuilder.append("estatusBanxico=").append(beanReceptoresSPID.getEstatus().getEstatusBanxico()).append(PIPE);
		strBuilder.append("estatusTransfer=").append(beanReceptoresSPID.getEstatus().getEstatusTransfer()).append(PIPE);
		strBuilder.append("codigoError=").append(beanReceptoresSPID.getEstatus().getCodigoError()).append(PIPE);
		strBuilder.append("numCuentaOrd=").append(beanReceptoresSPID.getBancoOrd().getNumCuentaOrd()).append(PIPE);
		strBuilder.append("tipoCuentaOrd=").append(beanReceptoresSPID.getBancoOrd().getTipoCuentaOrd()).append(PIPE);
		strBuilder.append("rfcOrd=").append(beanReceptoresSPID.getBancoOrd().getRfcOrd()).append(PIPE);
		strBuilder.append("nombreOrd=").append(beanReceptoresSPID.getBancoOrd().getNombreOrd()).append(PIPE);
		strBuilder.append("cveIntermeOrd=").append(beanReceptoresSPID.getBancoOrd().getCveIntermeOrd()).append(PIPE);
		strBuilder.append("codPostalOrd=").append(beanReceptoresSPID.getBancoOrd().getCodPostalOrd()).append(PIPE);
		strBuilder.append("fchConstitOrd=").append(beanReceptoresSPID.getBancoOrd().getFchConstitOrd()).append(PIPE);
		strBuilder.append("domicilioOrd=").append(beanReceptoresSPID.getBancoOrd().getDomicilioOrd()).append(PIPE);
		strBuilder.append("refeNumerica=").append(beanReceptoresSPID.getBancoOrd().getRefeNumerica()).append(PIPE);
		strBuilder.append("nombreRec=").append(beanReceptoresSPID.getBancoRec().getNombreRec()).append(PIPE);		
		strBuilder.append("tipoCuentaRec=").append(beanReceptoresSPID.getBancoRec().getTipoCuentaRec()).append(PIPE);
		strBuilder.append("numCuentaRec=").append(beanReceptoresSPID.getBancoRec().getNumCuentaRec()).append(PIPE);
		strBuilder.append("numCuentaTran=").append(beanReceptoresSPID.getBancoRec().getNumCuentaTran()).append(PIPE);
		strBuilder.append("rfcRec=").append(beanReceptoresSPID.getBancoRec().getRfcRec()).append(PIPE);
		strBuilder.append("motivoDev=").append(beanReceptoresSPID.getEnvDev().getMotivoDev()).append(PIPE);
		strBuilder.append("folioPaqDev=").append(beanReceptoresSPID.getEnvDev().getFolioPaqDev()).append(PIPE);
		strBuilder.append("folioPagoDev=").append(beanReceptoresSPID.getEnvDev().getFolioPagoDev()).append(PIPE);
		strBuilder.append("fchInstrucPago=").append(beanReceptoresSPID.getFchPagos().getFchInstrucPago()).append(PIPE);
		strBuilder.append("horaInstrucPago=").append(beanReceptoresSPID.getFchPagos().getHoraInstrucPago()).append(PIPE);
		strBuilder.append("fchAceptPago=").append(beanReceptoresSPID.getFchPagos().getFchAceptPago()).append(PIPE);
		strBuilder.append("horaAceptPago=").append(beanReceptoresSPID.getFchPagos().getHoraAceptPago()).append(PIPE);
		return strBuilder.toString();
	}

}
