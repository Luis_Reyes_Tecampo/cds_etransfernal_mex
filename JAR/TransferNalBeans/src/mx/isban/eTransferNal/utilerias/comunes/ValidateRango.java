package mx.isban.eTransferNal.utilerias.comunes;

import java.math.BigDecimal;
import java.util.Date;

import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

public class ValidateRango {

	/**
	 * @param importeIni Es el importe inicial a validar
	 * @param importeFin Es el importe final a validar
	 * @return true si es un importe valido falso en caso contrario
	 */
	public boolean esValidaRangoImporteVacio(String importeIni, String importeFin){
		BigDecimal bImporteIni = null;
		BigDecimal bImporteFin= null;
		String impIni;
		String impFin;
		if(importeIni!=null && !"".equals(importeIni) && importeFin!=null && "".equals(importeFin)){
			return false;
		}else if(importeIni!=null && "".equals(importeIni)){
			if(importeFin!=null && !"".equals(importeFin)){
				return false;
			}else{
				return true;
			}
		}
		
		impIni = importeIni.replaceAll(",", "");
		if(impIni.matches(Validate.REG_IMPORTE_MAXIMO)){
			bImporteIni = new BigDecimal(impIni);
		}else{
			return false;
		}
		
		impFin = importeFin.replaceAll(",", "");
		if(impFin.matches(Validate.REG_IMPORTE_MAXIMO)){
			bImporteFin = new BigDecimal(impFin);
		}else{
			return false;
		}
	
		return bImporteFin.compareTo(bImporteIni)>0;
	}

	/**
	 * @param horaIni Es el horario inicial a validar
	 * @param horaFin Es el horario final a validar
	 * @return true si es un periodo valido falso en caso contrario
	 */
	public boolean esValidaPeriodoTiempoHHMMVacio(String horaIni, String horaFin){
		int hrIni =0;
		int hrFin =0;
		if(horaIni!=null && !"".equals(horaIni) && horaFin!=null && "".equals(horaFin)){
				return false;	
		}else if(horaIni!=null && "".equals(horaIni)){
			if(horaFin!=null && !"".equals(horaFin)){
				return false;
			}else{
				return true;
			}
		}
		if(horaIni!=null && !"".equals(horaIni)){
			hrIni = Integer.parseInt(horaIni.replaceAll(":", ""));
		}
		if(horaFin!=null && !"".equals(horaFin)){
			hrFin = Integer.parseInt(horaFin.replaceAll(":", ""));
		}
		return hrIni<hrFin;
	}
	
	/**
	 * @param fechaIni Es la fecha inicial a validar
	 * @param fechaFin Es la fecha fin a validar
	 * @param dias es el numero de dias permitido
	 * @return true si es un periodo valido falso en caso contrario
	 */
	public boolean esValidaPeriodoTiempoDDMMYYYYVacio(String fechaIni, String fechaFin,int dias){
		Utilerias utilerias = Utilerias.getUtilerias();
		Date fchIni = null;
		Date fchFin = null;
		long time =0;
		int iDias =0;
		if(fechaIni!=null && !"".equals(fechaIni) && fechaFin !=null && "".equals(fechaFin)){
				return false;
		}else if(fechaIni!=null && "".equals(fechaIni)){
			if(fechaFin!=null && !"".equals(fechaFin)){
				return false;
			}else{
				return true;
			}
		}
		if(fechaIni!=null && !"".equals(fechaIni)){
			fchIni = utilerias.stringToDate(fechaIni, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		}
		if(fechaFin!=null && !"".equals(fechaFin)){
			fchFin = utilerias.stringToDate(fechaFin, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY);
		}
		if(fchIni!=null && fchFin!=null){
			time = fchFin.getTime()-fchIni.getTime();
			iDias= (int)(time/(1000*60*60*24));
		}else{
			return true;
		}
		if(dias<0){
			return iDias>=0;
		}
		return iDias>=0&&iDias<=dias;
	}
}
