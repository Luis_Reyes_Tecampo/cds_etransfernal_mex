/**
 * 
 */
package mx.isban.eTransferNal.constantes.modBIP;

import java.util.HashMap;
import java.util.Map;


/**
 * @author mcamarillo
 *
 */
public final class ConstantesConsultaMov{
	
	/**
	 * Constante de Select de visibilidad de pregunta
	 */
	public static final String SELECT_VISIBILIDAD_PREG = "SELECT FLG_VISBL FROM AAC_MX_MAE_CUESTIONARIO "
			+ "WHERE ID_CONF_CUEST_FK = ? AND ID_PREG_FK = ?";
		
	/**
	 * Constante de campo 1
	 */
	public static final String CVE_EMPRESA = "CVE_EMPRESA";
	
	/**
	 * Constante de campo 2
	 */
	public static final String CVE_PTO_VTA = "CVE_PTO_VTA";
	
	/**
	 * Constante de campo 3
	 */
	public static final String CVE_MEDIO_ENT = "CVE_MEDIO_ENT";
	
	/**
	 * Constante de campo 4
	 */
	public static final String REFERENCIA_MED = "REFERENCIA_MED";
	
	/**
	 * Constante de campo 5
	 */
	public static final String CVE_USUARIO_CAP = "CVE_USUARIO_CAP";
	
	/**
	 * Constante de campo 6
	 */
	public static final String CVE_USUARIO_SOL = "CVE_USUARIO_SOL";
	
	/**
	 * Constante de campo 7
	 */
	public static final String CVE_TRANSFE = "CVE_TRANSFE";
	
	/**
	 * Constante de campo 8
	 */
	public static final String CVE_OPERACION = "CVE_OPERACION";
	
	/**
	 * Constante de campo 9
	 */
	public static final String FORMA_LIQ = "FORMA_LIQ";
	
	/**
	 * Constante de campo 10
	 */
	public static final String CVE_INTERME_ORD = "CVE_INTERME_ORD";
	
	/**
	 * Constante de campo 11
	 */
	public static final String NUM_CUENTA_ORD = "NUM_CUENTA_ORD";
	
	/**
	 * Constante de campo 12
	 */
	public static final String CVE_PTO_VTA_ORD = "CVE_PTO_VTA_ORD";
	
	/**
	 * Constante de campo 13
	 */
	public static final String CVE_DIVISA_ORD = "CVE_DIVISA_ORD";
	
	/**
	 * Constante de campo 14
	 */
	public static final String NOMBRE_ORD = "NOMBRE_ORD";
	
	/**
	 * Constante de campo 15
	 */
	public static final String CVE_INTERME_REC = "CVE_INTERME_REC";
	
	/**
	 * Constante de campo 16
	 */
	public static final String NUM_CUENTA_REC = "NUM_CUENTA_REC";
	
	/**
	 * Constante de campo 17
	 */
	public static final String PLAZA_BANXICO = "PLAZA_BANXICO";
	
	/**
	 * Constante de campo 18
	 */
	public static final String CVE_PTO_VTA_REC = "CVE_PTO_VTA_REC";
	
	/**
	 * Constante de campo 19
	 */
	public static final String CVE_DIVISA_REC = "CVE_DIVISA_REC";
	
	/**
	 * Constante de campo 20
	 */
	public static final String NOMBRE_REC = "NOMBRE_REC";
	
	/**
	 * Constante de campo 21
	 */
	public static final String IMPORTE_CARGO = "IMPORTE_CARGO";
	
	/**
	 * Constante de campo 22
	 */
	public static final String IMPORTE_ABONO = "IMPORTE_ABONO";
	
	/**
	 * Constante de campo 23
	 */
	public static final String IMPORTE_DLS = "IMPORTE_DLS";
	
	/**
	 * Constante de campo 24
	 */
	public static final String NOMBRE_BCO_REC = "NOMBRE_BCO_REC";
	
	/**
	 * Constante de campo 25
	 */
	public static final String SUCURSAL_BCO_REC = "SUCURSAL_BCO_REC";
	
	/**
	 * Constante de campo 26
	 */
	public static final String CVE_PAIS_BCO_REC = "CVE_PAIS_BCO_REC";
	
	/**
	 * Constante de campo 27
	 */
	public static final String CIUDAD_BCO_REC = "CIUDAD_BCO_REC";
	
	/**
	 * Constante de campo 28
	 */
	public static final String CVE_ABA = "CVE_ABA";
	
	/**
	 * Constante de campo 29
	 */
	public static final String COMENTARIO1 = "COMENTARIO1";
	
	/**
	 * Constante de campo 30
	 */
	public static final String COMENTARIO2 = "COMENTARIO2";
	
	/**
	 * Constante de campo 31
	 */
	public static final String COMENTARIO3 = "COMENTARIO3";
	
	/**
	 * Constante de campo 32
	 */
	public static final String NUM_CUENTA_SEG_BENE = "NUM_CUENTA_SEG_BENE";
	
	/**
	 * Constante de campo 33
	 */
	public static final String NOMBRE_SEG_BENE = "NOMBRE_SEG_BENE";
	
	/**
	 * Constante de campo 34
	 */
	public static final String RFC_SEG_BENE = "RFC_SEG_BENE";
	
	/**
	 * Constante de campo 35
	 */
	public static final String CONCEPTO_SEG_BENE = "CONCEPTO_SEG_BENE";
	
	/**
	 * Constante de campo 36
	 */
	public static final String TIPO_CUENTA_SEG_BENE = "TIPO_CUENTA_SEG_BENE";
	
	/**
	 * Constante de campo 37
	 */
	public static final String COMENTARIOS = "COMENTARIOS";
	
	/**
	 * Constante de campo 38
	 */
	public static final String TIPO_CUENTA_ORDENANTE_SPEI = "TIPO_CUENTA_ORDENANTE_SPEI";
	
	/**
	 * Constante de campo 39
	 */
	public static final String TIPO_CUENTA_BENE_SPEI = "TIPO_CUENTA_BENE_SPEI";
	
	/**
	 * Constante de mensaje de error en tipo de dato
	 */
	public static final String ERROR_TIPO_DATO = "ES INCORRECTO EL TIPO DE DATO DEL CAMPO ";
	
	/**
	 * Constante de error en longitud
	 */
	public static final String ERROR_LONGITUD = "ES INCORRECTA LA LONGITUD DEL CAMPO ";
	
	/**
	 * Constante de error en longitud de retorno
	 */
	public static final String ERROR_LONGITUD_RETORNO = "ES INCORRECTA LA LONGITUD DEL CAMPO DE RETORNO ";
	
	/**
	 * Constante de campo codigo de error
	 */
	public static final String CAMPO_COD_ERROR = "COD_ERROR";
	
	/**
	 * Constante de campo de referencia
	 */
	public static final String CAMPO_REFERENCIA = "REFERENCIA";
	
	/**
	 * Constante de campo de estatus
	 */
	public static final String CAMPO_ESTATUS = "ESTATUS";
	
	/**
	 * Constante de campo de descripcion
	 */
	public static final String CAMPO_DESCRIPCION = "DESCRIPCION";
	
	/**
	 * Constante de codigo de error de tipos y longitud
	 */
	public static final String COD_ERROR_TIPO_LONGITUD = "TRIB9008";
	
	/**
	 * Constante de String
	 */
	public static final String STRING = "String";
	
	/**
	 * Constante de DECIMAL
	 */
	public static final String DECIMAL = "DECIMAL";
	
	/**
	 * Constante de primer campo
	 */
	public static final String PRIMER_CAMPO = "TRANSANT";
	
	/**
	 * Constante de PIPE
	 */
	public static final String PIPE = "|";
	
	/**
	 * Constante de punto
	 */
	public static final String PUNTO = ".";
	
	/**
	 * Constante de diagonal inversa
	 */
	public static final String DIAGONALES_INVERSA = "\\";
	
	/**
	 * Constante de exp regular
	 */
	public static final String EXP_REGULAR_NUMEROS = "([0-9]+(\\.[0-9]{1,2}){0,1})";
	
	/**
	 * Constante de nada
	 */
	public static final String NADA = "";
	
	/**
	 * Constante de 0
	 */
	public static final String CERO = "0";
	
	/**
	 * Constante de 0.0
	 */
	public static final String CERO_PUNTO = "0.0";
		
	/**
	 * Constante del canal para la peticion
	 */
	//public static final String CANAL = "TRANS.CLNT.D";
	public static final String CANAL = "ARQ_MENSAJERIA";
	
	/**
	 * Constante de servicio tuxedo a consultar
	 */
	public static final String SERVICIO_TUXEDO = "TranTransfe2";
	
	/**
	 * Constante de error en ida
	 */
	public static final String ERROR_IDA = "Ocurrio un error de Isban Data Access";
	
	/**
	 * Constante de num8
	 */
	public static final int NUM_8 = 8;
	
	/**
	 * Constante de num3
	 */
	public static final int NUM_3 = 3;
	
	/**
	 * Constante de num7
	 */
	public static final int NUM_7 = 7;
	
	/**
	 * Constante de num2
	 */
	public static final int NUM_2 = 2;
	
	/**
	 * Constante de error en respuesta
	 */
	public static final String ERROR_RESPUESTA = "Datos de respuesta erroneos";
	
	/**
	 * Constante de Json
	 */
	public static final String JSON = "Json";
	
	
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static Map<String, Integer> LONGITUD = new HashMap<String, Integer>();
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static String CAMPOS[] = {	
		ConstantesConsultaMov.CVE_EMPRESA,
		ConstantesConsultaMov.CVE_PTO_VTA,
		ConstantesConsultaMov.CVE_MEDIO_ENT,
		ConstantesConsultaMov.REFERENCIA_MED,
		ConstantesConsultaMov.CVE_USUARIO_CAP,
		ConstantesConsultaMov.CVE_USUARIO_SOL,
		ConstantesConsultaMov.CVE_TRANSFE,
		ConstantesConsultaMov.CVE_OPERACION,
		ConstantesConsultaMov.FORMA_LIQ,
		ConstantesConsultaMov.CVE_INTERME_ORD,
		ConstantesConsultaMov.NUM_CUENTA_ORD,
		ConstantesConsultaMov.CVE_PTO_VTA_ORD,
		ConstantesConsultaMov.CVE_DIVISA_ORD,
		ConstantesConsultaMov.NOMBRE_ORD,
		ConstantesConsultaMov.CVE_INTERME_REC,
		ConstantesConsultaMov.NUM_CUENTA_REC,
		ConstantesConsultaMov.PLAZA_BANXICO,
		ConstantesConsultaMov.CVE_PTO_VTA_REC,
		ConstantesConsultaMov.CVE_DIVISA_REC,
		ConstantesConsultaMov.NOMBRE_REC,
		ConstantesConsultaMov.IMPORTE_CARGO,
		ConstantesConsultaMov.IMPORTE_ABONO,
		ConstantesConsultaMov.IMPORTE_DLS,
		ConstantesConsultaMov.NOMBRE_BCO_REC,
		ConstantesConsultaMov.SUCURSAL_BCO_REC,
		ConstantesConsultaMov.CVE_PAIS_BCO_REC,
		ConstantesConsultaMov.CIUDAD_BCO_REC,
		ConstantesConsultaMov.CVE_ABA,
		ConstantesConsultaMov.COMENTARIO1,
		ConstantesConsultaMov.COMENTARIO2,
		ConstantesConsultaMov.COMENTARIO3,
		ConstantesConsultaMov.NUM_CUENTA_SEG_BENE,
		ConstantesConsultaMov.NOMBRE_SEG_BENE,
		ConstantesConsultaMov.RFC_SEG_BENE,
		ConstantesConsultaMov.CONCEPTO_SEG_BENE,
		ConstantesConsultaMov.TIPO_CUENTA_SEG_BENE, 
		ConstantesConsultaMov.COMENTARIOS,
		ConstantesConsultaMov.TIPO_CUENTA_ORDENANTE_SPEI,
		ConstantesConsultaMov.TIPO_CUENTA_BENE_SPEI
	};
	
	/**
	 * Propiedad del tipo Map<String,String> que almacena el valor de TIPO
	 */
	public final static Map<String, String> TIPO = new HashMap<String, String>();

	//Inicializacion estatica
	static {
		LONGITUD.put(ConstantesConsultaMov.CVE_EMPRESA, 3);
		LONGITUD.put(ConstantesConsultaMov.CVE_PTO_VTA, 4);
		LONGITUD.put(ConstantesConsultaMov.CVE_MEDIO_ENT, 6);
		LONGITUD.put(ConstantesConsultaMov.REFERENCIA_MED, 12);
		LONGITUD.put(ConstantesConsultaMov.CVE_USUARIO_CAP, 8);
		LONGITUD.put(ConstantesConsultaMov.CVE_USUARIO_SOL, 8);
		LONGITUD.put(ConstantesConsultaMov.CVE_TRANSFE, 3);
		LONGITUD.put(ConstantesConsultaMov.CVE_OPERACION, 8);
		LONGITUD.put(ConstantesConsultaMov.FORMA_LIQ, 1);
		LONGITUD.put(ConstantesConsultaMov.CVE_INTERME_ORD, 5);
		LONGITUD.put(ConstantesConsultaMov.NUM_CUENTA_ORD, 20);
		LONGITUD.put(ConstantesConsultaMov.CVE_PTO_VTA_ORD, 4);
		LONGITUD.put(ConstantesConsultaMov.CVE_DIVISA_ORD, 4);
		LONGITUD.put(ConstantesConsultaMov.NOMBRE_ORD, 50);
		LONGITUD.put(ConstantesConsultaMov.CVE_INTERME_REC, 5);
		LONGITUD.put(ConstantesConsultaMov.NUM_CUENTA_REC, 20);
		LONGITUD.put(ConstantesConsultaMov.PLAZA_BANXICO, 5);
		LONGITUD.put(ConstantesConsultaMov.CVE_PTO_VTA_REC, 4);
		LONGITUD.put(ConstantesConsultaMov.CVE_DIVISA_REC, 4);
		LONGITUD.put(ConstantesConsultaMov.NOMBRE_REC, 50);
		LONGITUD.put(ConstantesConsultaMov.IMPORTE_CARGO, 15);
		LONGITUD.put(ConstantesConsultaMov.IMPORTE_ABONO, 15);
		LONGITUD.put(ConstantesConsultaMov.IMPORTE_DLS, 10);
		LONGITUD.put(ConstantesConsultaMov.NOMBRE_BCO_REC, 40);
		LONGITUD.put(ConstantesConsultaMov.SUCURSAL_BCO_REC, 30);
		LONGITUD.put(ConstantesConsultaMov.CVE_PAIS_BCO_REC, 4);
		LONGITUD.put(ConstantesConsultaMov.CIUDAD_BCO_REC, 30);
		LONGITUD.put(ConstantesConsultaMov.CVE_ABA, 9);
		LONGITUD.put(ConstantesConsultaMov.COMENTARIO1, 120);
		LONGITUD.put(ConstantesConsultaMov.COMENTARIO2, 30);
		LONGITUD.put(ConstantesConsultaMov.COMENTARIO3, 30);
		LONGITUD.put(ConstantesConsultaMov.NUM_CUENTA_SEG_BENE, 35);
		LONGITUD.put(ConstantesConsultaMov.NOMBRE_SEG_BENE, 40);
		LONGITUD.put(ConstantesConsultaMov.RFC_SEG_BENE, 18);
		LONGITUD.put(ConstantesConsultaMov.CONCEPTO_SEG_BENE, 40);
		LONGITUD.put(ConstantesConsultaMov.TIPO_CUENTA_SEG_BENE, 2);
		LONGITUD.put(ConstantesConsultaMov.COMENTARIOS, 120);
		LONGITUD.put(ConstantesConsultaMov.TIPO_CUENTA_ORDENANTE_SPEI,2);
		LONGITUD.put(ConstantesConsultaMov.TIPO_CUENTA_BENE_SPEI, 2);
	}

	//Inicializacion estatica
	static {
		TIPO.put(ConstantesConsultaMov.CVE_EMPRESA,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_PTO_VTA,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_MEDIO_ENT,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.REFERENCIA_MED,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_USUARIO_CAP,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_USUARIO_SOL,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_TRANSFE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_OPERACION,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.FORMA_LIQ,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_INTERME_ORD,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NUM_CUENTA_ORD,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_PTO_VTA_ORD,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_DIVISA_ORD,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NOMBRE_ORD,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_INTERME_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NUM_CUENTA_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.PLAZA_BANXICO,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_PTO_VTA_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_DIVISA_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NOMBRE_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.IMPORTE_CARGO,ConstantesConsultaMov.DECIMAL);
		TIPO.put(ConstantesConsultaMov.IMPORTE_ABONO,ConstantesConsultaMov.DECIMAL);
		TIPO.put(ConstantesConsultaMov.IMPORTE_DLS,ConstantesConsultaMov.DECIMAL);
		TIPO.put(ConstantesConsultaMov.NOMBRE_BCO_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.SUCURSAL_BCO_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_PAIS_BCO_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CIUDAD_BCO_REC,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CVE_ABA,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.COMENTARIO1,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.COMENTARIO2,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.COMENTARIO3,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NUM_CUENTA_SEG_BENE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.NOMBRE_SEG_BENE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.RFC_SEG_BENE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.CONCEPTO_SEG_BENE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.TIPO_CUENTA_SEG_BENE,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.COMENTARIOS,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.TIPO_CUENTA_ORDENANTE_SPEI,ConstantesConsultaMov.STRING);
		TIPO.put(ConstantesConsultaMov.TIPO_CUENTA_BENE_SPEI,ConstantesConsultaMov.STRING);
	}

}
