/**
 * 
 */
package mx.isban.eTransferNal.constantes.modSPID;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jcgarciamo
 *
 */
public final class ConstantesConsultaEstatusSPID {

	/**
	 * Constante de campo 1 de la referencia
	 */
	public static final String REFERENCIA = "REFERENCIA";
	
	/**
	 * Constante de campo 2 de la cve de rastreo
	 */
	public static final String CVE_RASTREO = "CVE_RASTREO";
	
	/**
	 * Constante de campo 3 de la fecha
	 */
	public static final String FECHA = "FECHA";
	
	/**
	 * Constante de campo 4 del numero de cuenta
	 */
	public static final String NUMERO_CUENTA = "NUMERO_CUENTA";
	
	/**
	 * Constante de campo 4 informacion adicional
	 */
	public static final String INF_ADICIONAL = "INF_ADICIONAL";
	
	/**
	 * Constante de error en respuesta
	 */
	public static final String ERROR_RESPUESTA = "Datos de respuesta erroneos";
	
	/**
	 * Constante de Json
	 */
	public static final String JSON = "Json";
	
	/**
	 * Constante de String
	 */
	public static final String STRING = "String";
	
	/**
	 * Constante de DECIMAL
	 */
	public static final String DECIMAL = "DECIMAL";
	

	/**
	 * Constante de exp regular
	 */
	public static final String EXP_REGULAR_NUMEROS = "([0-9]+(\\.[0-9]{1,2}){0,1})";
	
	/**
	 * Constante de PIPE
	 */
	public static final String PIPE = "|";
	
	/**
	 * Constante de nada
	 */
	public static final String NADA = "";
	
	/**
	 * Constante de 0
	 */
	public static final String CERO = "0";
	
	/**
	 * Constante de 0.0
	 */
	public static final String CERO_PUNTO = "0.0";
	
	/**
	 * Constante de error en ida
	 */
	public static final String ERROR_IDA = "Ocurrio un error de Isban Data Access";
	
	/**
	 * Constante de num10
	 */
	public static final int NUM_10 = 10;
	
	/**
	 * Constante de num60
	 */
	public static final int NUM_60 = 60;
	
	/**
	 * Constante de num2
	 */
	public static final int NUM_2 = 2;
	
	/**
	 * Constante de num7
	 */
	public static final int NUM_40 = 40;
	
	/**
	 * Constante de num120
	 */
	public static final int NUM_120 = 120;
	
	/**
	 * Constante de num19
	 */
	public static final int NUM_19 = 19;
	
	/**
	 * Constante de num30
	 */
	public static final int NUM_30 = 30;
	
	/**
	 * Constante de num12
	 */
	public static final int NUM_12 = 12;
	
	/**
	 * Constante de num18
	 */
	public static final int NUM_18 = 18;
	
	/**
	 * Constante de num8
	 */
	public static final int NUM_8 = 8;
	
	/**
	 * Constante de num50
	 */
	public static final int NUM_50 = 50;
	
	
	/**
	 * Constante de campo codigo de error
	 */
	public static final String CAMPO_COD_ERROR = "COD_ERROR";
	
	/**
	 * Constante de campo de referencia
	 */
	public static final String CAMPO_DESC_ERROR = "DESC_ERROR";
	
	/**
	 * Constante de campo de estatus
	 */
	public static final String CAMPO_ESTATUS = "ESTATUS";
	
	/**
	 * Constante de campo de descripcion
	 */
	public static final String CAMPO_DESCRIPCION = "DESCRIPCION";
	/**
	 * Constante de campo de descripcion devolucion
	 */
	public static final String CAMPO_DESC_DEVOLUCION = "DESC_DEVOLUCION";
	
	/**
	 * Constante de campo codigo de fecha aplica
	 */
	public static final String CAMPO_FCH_APLICA = "FCH_APLICA";
	
	/**
	 * Constante de campo de clave rastreo
	 */
	public static final String CAMPO_CVE_RASTREO = "CVE_RASTREO";
	
	/**
	 * Constante de campo de referncia txt
	 */
	public static final String CAMPO_REFERENCIA_TXT = "REFERENCIA_TXT";
	
	/**
	 * Constante de campo de Nombre del Ordenante.
	 */
	public static final String CAMPO_NOM_ORD = "NOM_ORD";
	/**
	 * Constante de campo de Nombre del Beneficiario.
	 */
	public static final String CAMPO_NOM_REC = "NOM_REC";
	
	/**
	 * Constante de campo de monto
	 */
	public static final String CAMPO_MONTO = "MONTO";
	
	/**
	 * Constante de campo Clave del intermediario receptor
	 */
	public static final String CAMPO_INTERME_REC = "INTERM_REC";
	/**
	 * Constante de campo Clave del intermediario ordenante.
	 */
	public static final String CAMPO_INTERME_ORD = "INTERME_ORD";
	
	/**
	 * Constante de campo Nombre corto del intermediario receptor
	 */
	public static final String CAMPO_NOMBRE_CORTO = "NOMBRE_CORTO";
	
	/**
	 * Constante de campo RFC del ordenante
	 */
	public static final String CAMPO_RFC_ORDENANTE = "RFC_ORDENANTE";
	
	/**
	 * Constante de campo de referncia concepto
	 */
	public static final String CAMPO_CONCEPTO = "CONCEPTO";
	
	/**
	 * Constante de campo Fecha aprobacion.
	 */
	public static final String CAMPO_FCH_APROBACION = "FCH_APROBACION";
	/**
	 * Constante de campo referencia numerica.
	 */
	public static final String CAMPO_REFERENCIA_NUM = "REFERENCIA_NUM";

	/**
	 * Constante de campo referencia numerica.
	 */
	public static final String CAMPO_NUM_CUENTA_ORD = "NUM_CUENTA_ORD";
	/**
	 * Constante de campo referencia numerica.
	 */
	public static final String CAMPO_NUM_CUENTA_REC = "NUM_CUENTA_REC";
	
	/**
	 * Constante de codigo de error de tipos y longitud
	 */
	public static final String COD_ERROR_TIPO_LONGITUD = "TRIB9008";
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static Map<String, Integer> LONGITUD = new HashMap<String, Integer>();
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static String CAMPOS[] = {	
		ConstantesConsultaEstatusSPID.REFERENCIA,
		ConstantesConsultaEstatusSPID.CVE_RASTREO,
		ConstantesConsultaEstatusSPID.FECHA,
		ConstantesConsultaEstatusSPID.NUMERO_CUENTA,
		ConstantesConsultaEstatusSPID.INF_ADICIONAL
	};
	
	/**
	 * Propiedad del tipo Map<String,String> que almacena el valor de TIPO
	 */
	public final static Map<String, String> TIPO = new HashMap<String, String>();

	//Inicializacion estatica
	static {
		LONGITUD.put(ConstantesConsultaEstatusSPID.REFERENCIA, 12);
		LONGITUD.put(ConstantesConsultaEstatusSPID.CVE_RASTREO, 30);
		LONGITUD.put(ConstantesConsultaEstatusSPID.FECHA, 8);
		LONGITUD.put(ConstantesConsultaEstatusSPID.NUMERO_CUENTA, 11);
		LONGITUD.put(ConstantesConsultaEstatusSPID.INF_ADICIONAL, 1);
	}
	
	//Inicializacion estatica
	static {		
		TIPO.put(ConstantesConsultaEstatusSPID.REFERENCIA,ConstantesSPID.STRING);
		TIPO.put(ConstantesConsultaEstatusSPID.CVE_RASTREO,ConstantesSPID.STRING);
		TIPO.put(ConstantesConsultaEstatusSPID.FECHA,ConstantesSPID.STRING);
		TIPO.put(ConstantesConsultaEstatusSPID.NUMERO_CUENTA,ConstantesSPID.STRING);
		TIPO.put(ConstantesConsultaEstatusSPID.INF_ADICIONAL,STRING);
		
	}
	
}
