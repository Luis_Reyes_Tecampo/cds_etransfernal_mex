/**
 * 
 */
package mx.isban.eTransferNal.constantes.modSPID;

import java.util.HashMap;
import java.util.Map;


/**
 * @author mcamarillo
 *
 */
public final class ConstantesSPID{
	
	/**
	 * Constante de campo 1
	 */
	public static final String CVE_EMPRESA = "CVE_EMPRESA";
	
	/**
	 * Constante de campo 2
	 */
	public static final String CVE_PTO_VTA = "CVE_PTO_VTA";
	
	/**
	 * Constante de campo 3
	 */
	public static final String CVE_MEDIO_ENT = "CVE_MEDIO_ENT";
	
	/**
	 * Constante de campo 4
	 */
	public static final String REFERENCIA_MED = "REFERENCIA_MED";
	
	/**
	 * Constante de campo 5
	 */
	public static final String CVE_USUARIO_CAP = "CVE_USUARIO_CAP";
	
	/**
	 * Constante de campo 6
	 */
	public static final String CVE_USUARIO_SOL = "CVE_USUARIO_SOL";
	
	/**
	 * Constante de campo 7
	 */
	public static final String CVE_TRANSFE = "CVE_TRANSFE";
	
	/**
	 * Constante de campo 8
	 */
	public static final String CVE_OPERACION = "CVE_OPERACION";
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de REFE_NUMERICA
	 */
	public static final String REFE_NUMERICA = "REFE_NUMERICA";
	/**
	 * Propiedad del tipo String que almacena el valor de DIRECCION_IP
	 */
	public static final String DIRECCION_IP   = "DIRECCION_IP";
	/**
	 * Propiedad del tipo String que almacena el valor de FCH_INSTRUC_PAGO
	 */
	public static final String FCH_INSTRUC_PAGO = "FCH_INSTRUC_PAGO";
	/**
	 * Propiedad del tipo String que almacena el valor de HORA_INSTRUC_PAGO
	 */
	public static final String HORA_INSTRUC_PAGO = "HORA_INSTRUC_PAGO";
	/**
	 * Propiedad del tipo String que almacena el valor de CVE_RASTREO
	 */
	public static final String CVE_RASTREO = "CVE_RASTREO";
	
	
	/**
	 * Constante de campo 10
	 */
	public static final String CVE_INTERME_ORD = "CVE_INTERME_ORD";
	
	/**
	 * Propiedad del tipo String que almacena el valor de TIPO_CUENTA_ORD
	 */
	public static final String TIPO_CUENTA_ORD = "TIPO_CUENTA_ORD";
	
	/**
	 * Constante de campo 11
	 */
	public static final String NUM_CUENTA_ORD = "NUM_CUENTA_ORD";
	
	/**
	 * Constante de campo 12
	 */
	public static final String CVE_PTO_VTA_ORD = "CVE_PTO_VTA_ORD";
	
	/**
	 * Constante de campo 13
	 */
	public static final String CVE_DIVISA_ORD = "CVE_DIVISA_ORD";
	
	/**
	 * Constante de campo 14
	 */
	public static final String NOMBRE_ORD = "NOMBRE_ORD";
	
	/**
	 * Propiedad del tipo String que almacena el valor de RFC_ORD
	 */
	public static final String RFC_ORD = "RFC_ORD";
	/**
	 * Propiedad del tipo String que almacena el valor de DOMICILIO_ORD
	 */
	public static final String DOMICILIO_ORD = "DOMICILIO_ORD";
	/**
	 * Propiedad del tipo String que almacena el valor de COD_POSTAL_ORD
	 */
	public static final String COD_POSTAL_ORD = "COD_POSTAL_ORD";
	/**
	 * Propiedad del tipo String que almacena el valor de FCH_CONSTIT_ORD
	 */
	public static final String FCH_CONSTIT_ORD = "FCH_CONSTIT_ORD";
	
	/**
	 * Constante de campo 15
	 */
	public static final String CVE_INTERME_REC = "CVE_INTERME_REC";
	
	/**
	 * Propiedad del tipo String que almacena el valor de TIPO_CUENTA_REC
	 */
	public static final String TIPO_CUENTA_REC = "TIPO_CUENTA_REC";
	
	/**
	 * Constante de campo 16
	 */
	public static final String NUM_CUENTA_REC = "NUM_CUENTA_REC";
	
	/**
	 * Constante de campo 18
	 */
	public static final String CVE_PTO_VTA_REC = "CVE_PTO_VTA_REC";
	
	/**
	 * Constante de campo 19
	 */
	public static final String CVE_DIVISA_REC = "CVE_DIVISA_REC";
	
	/**
	 * Constante de campo 20
	 */
	public static final String NOMBRE_REC = "NOMBRE_REC";
	
	/**
	 * Propiedad del tipo String que almacena el valor de RFC_REC
	 */
	public static final String RFC_REC = "RFC_REC";
	/**
	 * Propiedad del tipo String que almacena el valor de MOTIVO_DEVOL
	 */
	public static final String MOTIVO_DEVOL = "MOTIVO_DEVOL"; 
	
	/**
	 * Propiedad del tipo String que almacena el valor de FOLIO_PAQUETE
	 */
	public static final String FOLIO_PAQUETE = "FOLIO_PAQUETE";
	
	/**
	 * Propiedad del tipo String que almacena el valor de FOLIO_PAGO
	 */
	public static final String FOLIO_PAGO = "FOLIO_PAGO";
	
	/**
	 * Propiedad del tipo String que almacena el valor de TIPO_CAMBIO
	 */
	public static final String TIPO_CAMBIO = "TIPO_CAMBIO";
	
	/**
	 * Constante de campo 21
	 */
	public static final String IMPORTE_CARGO = "IMPORTE_CARGO";
	
	/**
	 * Constante de campo 22
	 */
	public static final String IMPORTE_ABONO = "IMPORTE_ABONO";

	/**
	 * Constante de campo 30
	 */
	public static final String COMENTARIO2 = "COMENTARIO2";
	
	/**
	 * Constante de campo 31
	 */
	public static final String COMENTARIO3 = "COMENTARIO3";
	
	/**
	 * Propiedad del tipo String que almacena el valor de CONCEPTO_PAGO
	 */
	public static final String CONCEPTO_PAGO = "CONCEPTO_PAGO";


	
	
	/**
	 * Constante de mensaje de error en tipo de dato
	 */
	public static final String ERROR_TIPO_DATO = "ES INCORRECTO EL TIPO DE DATO DEL CAMPO ";
	
	/**
	 * Constante de error en longitud
	 */
	public static final String ERROR_LONGITUD = "ES INCORRECTA LA LONGITUD DEL CAMPO ";
	
	/**
	 * Constante de error en longitud de retorno
	 */
	public static final String ERROR_LONGITUD_RETORNO = "ES INCORRECTA LA LONGITUD DEL CAMPO DE RETORNO ";
	
	/**
	 * Constante de campo codigo de error
	 */
	public static final String CAMPO_COD_ERROR = "COD_ERROR";
	
	/**
	 * Constante de campo de referencia
	 */
	public static final String CAMPO_REFERENCIA = "REFERENCIA";
	
	/**
	 * Constante de campo de estatus
	 */
	public static final String CAMPO_ESTATUS = "ESTATUS";
	
	/**
	 * Constante de campo de descripcion
	 */
	public static final String CAMPO_DESCRIPCION = "DESCRIPCION";
	
	/**
	 * Constante de campo de descripcion estatus
	 */
	public static final String CAMPO_DESCRIPCION_ESTATUS = "DESCRIPCION_ESTATUS";
	
	/**
	 * Constante de codigo de error de tipos y longitud
	 */
	public static final String COD_ERROR_TIPO_LONGITUD = "TRIB9008";
	
	/**
	 * Constante de String
	 */
	public static final String STRING = "String";
	
	/**
	 * Constante de FECHA
	 */
	public static final String FECHA = "FECHA";
	
	/**
	 * Constante de String
	 */
	public static final String NUMERICO = "NUMERICO";
	
	/**
	 * Constante de DECIMAL
	 */
	public static final String DECIMAL = "DECIMAL";
	
	/**
	 * Constante de primer campo
	 */
	public static final String PRIMER_CAMPO = "TRANSANT";
	
	/**
	 * Constante de PIPE
	 */
	public static final String PIPE = "|";
	
	/**
	 * Constante de punto
	 */
	public static final String PUNTO = ".";
	
	/**
	 * Constante de diagonal inversa
	 */
	public static final String DIAGONALES_INVERSA = "\\";
	
	/**
	 * Constante de exp regular
	 */
	public static final String EXP_REGULAR_NUMEROS = "([0-9]+(\\.[0-9]{1,2}){0,1})";
	
	/**
	 * Constante de exp regular
	 */
	public static final String EXP_REGULAR_ENTERO = "([0-9]+)";
	
	/**
	 * Constante de nada
	 */
	public static final String NADA = "";
	
	/**
	 * Constante de 0
	 */
	public static final String CERO = "0";
	
	/**
	 * Constante de 0.0
	 */
	public static final String CERO_PUNTO = "0.0";
		
	/**
	 * Constante del canal para la peticion
	 */
	//public static final String CANAL = "TRANS.CLNT.D";
	public static final String CANAL = "ARQ_MENSAJERIA";
	
	/**
	 * Constante de servicio tuxedo a consultar
	 */
	public static final String SERVICIO_TUXEDO = "TranTransfe2";
	
	/**
	 * Constante de error en ida
	 */
	public static final String ERROR_IDA = "Ocurrio un error de Isban Data Access";
	
	/**
	 * Constante de num70
	 */
	public static final int NUM_70 = 70;
	
	/**
	 * Constante de num8
	 */
	public static final int NUM_8 = 8;
	
	/**
	 * Constante de num30
	 */
	public static final int NUM_30 = 30;
	
	/**
	 * Constante de num7
	 */
	public static final int NUM_7 = 7;
	
	/**
	 * Constante Numerico de 1 posicion
	 */
	public static final int NUM_1 = 1;
	
	/**
	 * Constante de num2
	 */
	public static final int NUM_2 = 2;
	/**
	 * Constante de num20
	 */
	public static final int NUM_20 = 20;
	
	/**
	 * Constante de error en respuesta
	 */
	public static final String ERROR_RESPUESTA = "Datos de respuesta erroneos";
	
	/**
	 * Constante de Json
	 */
	public static final String JSON = "Json";
	
	
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static Map<String, Integer> LONGITUD = new HashMap<String, Integer>();
	
	/**
	 * Propiedad del tipo Map<String,Integer> que almacena el valor de LONGITUD
	 */
	public final static String CAMPOS[] = {	
		ConstantesSPID.CVE_EMPRESA,
		ConstantesSPID.CVE_PTO_VTA,
		ConstantesSPID.CVE_MEDIO_ENT,
		ConstantesSPID.REFERENCIA_MED,
		ConstantesSPID.CVE_USUARIO_CAP,
		ConstantesSPID.CVE_USUARIO_SOL,
		ConstantesSPID.CVE_TRANSFE,
		ConstantesSPID.CVE_OPERACION,
		ConstantesSPID.REFE_NUMERICA,
		ConstantesSPID.DIRECCION_IP,
		ConstantesSPID.FCH_INSTRUC_PAGO,
		ConstantesSPID.HORA_INSTRUC_PAGO,
		ConstantesSPID.CVE_RASTREO,
		ConstantesSPID.CVE_DIVISA_ORD,
		ConstantesSPID.CVE_DIVISA_REC,
		ConstantesSPID.IMPORTE_CARGO,
		ConstantesSPID.IMPORTE_ABONO,
		ConstantesSPID.CVE_PTO_VTA_ORD,
		ConstantesSPID.CVE_INTERME_ORD,
		ConstantesSPID.TIPO_CUENTA_ORD,
		ConstantesSPID.NUM_CUENTA_ORD,
		ConstantesSPID.NOMBRE_ORD,
		ConstantesSPID.RFC_ORD,
		ConstantesSPID.DOMICILIO_ORD,
		ConstantesSPID.COD_POSTAL_ORD,
		ConstantesSPID.FCH_CONSTIT_ORD,
		ConstantesSPID.CVE_PTO_VTA_REC,
		ConstantesSPID.CVE_INTERME_REC,
		ConstantesSPID.TIPO_CUENTA_REC,
		ConstantesSPID.NUM_CUENTA_REC,
		ConstantesSPID.NOMBRE_REC,
		ConstantesSPID.RFC_REC,
		ConstantesSPID.MOTIVO_DEVOL,
		ConstantesSPID.FOLIO_PAQUETE,
		ConstantesSPID.FOLIO_PAGO,
		ConstantesSPID.TIPO_CAMBIO,
		ConstantesSPID.COMENTARIO2,
		ConstantesSPID.COMENTARIO3,
		ConstantesSPID.CONCEPTO_PAGO
	};
	
	/**
	 * Propiedad del tipo Map<String,String> que almacena el valor de TIPO
	 */
	public final static Map<String, String> TIPO = new HashMap<String, String>();

	//Inicializacion estatica
	static {
		LONGITUD.put(ConstantesSPID.CVE_EMPRESA, 3);
		LONGITUD.put(ConstantesSPID.CVE_PTO_VTA, 4);
		LONGITUD.put(ConstantesSPID.CVE_MEDIO_ENT, 6);
		LONGITUD.put(ConstantesSPID.REFERENCIA_MED, 12);
		LONGITUD.put(ConstantesSPID.CVE_USUARIO_CAP, 8);
		LONGITUD.put(ConstantesSPID.CVE_USUARIO_SOL, 8);
		LONGITUD.put(ConstantesSPID.CVE_TRANSFE, 3);
		LONGITUD.put(ConstantesSPID.CVE_OPERACION, 8);
		LONGITUD.put(ConstantesSPID.REFE_NUMERICA, 20);
		LONGITUD.put(ConstantesSPID.DIRECCION_IP, 39);
		LONGITUD.put(ConstantesSPID.FCH_INSTRUC_PAGO, 8);
		LONGITUD.put(ConstantesSPID.HORA_INSTRUC_PAGO, 11);
		LONGITUD.put(ConstantesSPID.CVE_RASTREO, 30);
		LONGITUD.put(ConstantesSPID.CVE_DIVISA_ORD, 4);
		LONGITUD.put(ConstantesSPID.CVE_DIVISA_REC, 4);
		LONGITUD.put(ConstantesSPID.IMPORTE_CARGO, 18);
		LONGITUD.put(ConstantesSPID.IMPORTE_ABONO, 18);
		LONGITUD.put(ConstantesSPID.CVE_PTO_VTA_ORD, 4);
		LONGITUD.put(ConstantesSPID.CVE_INTERME_ORD, 5);
		LONGITUD.put(ConstantesSPID.TIPO_CUENTA_ORD, 3);
		LONGITUD.put(ConstantesSPID.NUM_CUENTA_ORD, 20);
		LONGITUD.put(ConstantesSPID.NOMBRE_ORD, 120);
		LONGITUD.put(ConstantesSPID.RFC_ORD, 18);
		LONGITUD.put(ConstantesSPID.DOMICILIO_ORD, 120);
		LONGITUD.put(ConstantesSPID.COD_POSTAL_ORD, 5);
		LONGITUD.put(ConstantesSPID.FCH_CONSTIT_ORD, 8);
		LONGITUD.put(ConstantesSPID.CVE_PTO_VTA_REC, 4);
		LONGITUD.put(ConstantesSPID.CVE_INTERME_REC, 5);
		LONGITUD.put(ConstantesSPID.TIPO_CUENTA_REC, 2);
		LONGITUD.put(ConstantesSPID.NUM_CUENTA_REC, 20);
		LONGITUD.put(ConstantesSPID.NOMBRE_REC, 120);
		LONGITUD.put(ConstantesSPID.RFC_REC, 18);
		LONGITUD.put(ConstantesSPID.MOTIVO_DEVOL, 2);
		LONGITUD.put(ConstantesSPID.FOLIO_PAQUETE, 12);
		LONGITUD.put(ConstantesSPID.FOLIO_PAGO, 12);
		LONGITUD.put(ConstantesSPID.TIPO_CAMBIO, 15);
		LONGITUD.put(ConstantesSPID.COMENTARIO2, 30);
		LONGITUD.put(ConstantesSPID.COMENTARIO3, 30);
		LONGITUD.put(ConstantesSPID.CONCEPTO_PAGO, 210);
		
		
		
	}

	//Inicializacion estatica
	static {		
		TIPO.put(ConstantesSPID.CVE_EMPRESA,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_PTO_VTA,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.CVE_MEDIO_ENT,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.REFERENCIA_MED,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.CVE_USUARIO_CAP,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_USUARIO_SOL,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_TRANSFE,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.CVE_OPERACION,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.REFE_NUMERICA,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.DIRECCION_IP,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.FCH_INSTRUC_PAGO,ConstantesSPID.FECHA);
		TIPO.put(ConstantesSPID.HORA_INSTRUC_PAGO,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_RASTREO,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_DIVISA_ORD,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CVE_DIVISA_REC,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.IMPORTE_CARGO,ConstantesSPID.DECIMAL);
		TIPO.put(ConstantesSPID.IMPORTE_ABONO,ConstantesSPID.DECIMAL);
		TIPO.put(ConstantesSPID.CVE_PTO_VTA_ORD,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.CVE_INTERME_ORD,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.TIPO_CUENTA_ORD,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.NUM_CUENTA_ORD,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.NOMBRE_ORD,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.RFC_ORD,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.DOMICILIO_ORD,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.COD_POSTAL_ORD,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.FCH_CONSTIT_ORD,ConstantesSPID.FECHA);
		TIPO.put(ConstantesSPID.CVE_PTO_VTA_REC,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.CVE_INTERME_REC,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.TIPO_CUENTA_REC,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.NUM_CUENTA_REC,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.NOMBRE_REC,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.RFC_REC,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.MOTIVO_DEVOL,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.FOLIO_PAQUETE,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.FOLIO_PAGO,ConstantesSPID.NUMERICO);
		TIPO.put(ConstantesSPID.TIPO_CAMBIO,ConstantesSPID.DECIMAL);
		TIPO.put(ConstantesSPID.COMENTARIO2,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.COMENTARIO3,ConstantesSPID.STRING);
		TIPO.put(ConstantesSPID.CONCEPTO_PAGO,ConstantesSPID.STRING);

	}

}
