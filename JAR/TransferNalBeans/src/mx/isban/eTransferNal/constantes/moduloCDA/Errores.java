/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: Errores.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     12/09/2019 01:01:52 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.constantes.moduloCDA;


/**
 * Interfaz con los codigos de error.
 *
 * @author cchong
 */
public final class Errores {

	/**  Constante que almacena el tipo de mensaje info. */
	public static final String TIPO_MSJ_INFO = "jInfo";
	
	/**  Constante que almacena el tipo de mensaje alert. */
	public static final String TIPO_MSJ_ALERT = "jAlert";
	
	/**  Constante que almacena el tipo de mensaje error. */
	public static final String TIPO_MSJ_ERROR = "jError";
	
	/**  Constante que almacena el tipo de mensaje de confirmacion. */
	public static final String TIPO_MSJ_CONFIRM = "jConfirm";

	/**  No existe informacion para la consulta solicitada. */
	public static final String ED00011V = "ED00011V";
	
	/**  Estado Conectado, no es posible generar archivo. */
	public static final String ED00012V = "ED00012V";
	
	/**  El campo nombre del archivo esta vacio o no tiene el formato indicado. */
	public static final String ED00013V = "ED00013V";
	
	/** La fecha seleccionada no puede ser mayor a la fecha de operacion del Banco de Mexico. */
	public static final String ED00014V = "ED00014V";

	/** El sist. esta en contingencia */
	public static final String ED00024V = "ED00024V";
	
	/**  Codigo de error, La fecha de operacion es mandatorio no pueda estar vacia. */
	public static final String ED00025V = "ED00025V";
	
	/**  codigo de error si las operaciones ya se encuentran apartadas*. */
	public static final String ED00131V="ED00131V";
	
	
	/**  La fecha de operacion es mandatorio no pueda estar vacia. */
	public static final String DESC_ED00025V = "La fecha de operaci\u00F3n es mandatoria no puede estar vac\u00EDo";
	
	/** No hay registros/movimientos seleccionados. */
	public static final String ED00026V = "ED00026V";
	
	/**  Constante de exito del ISBAN DATA ACCESS. */
	public static final String CODE_SUCCESFULLY = "DAE000";
	/**
	 * Constante de Error desconocido al ejecutar el query u obtener los datos
	 * de respuesta. Fue imposible establecer conexion a la base de datos -
	 * 17002 : Error de E/S: The Network Adapter could not establish the
	 * connectionDSRA0010E: Estado SQL = 08006, Codigo de error = 17,002
	 * */
	public static final String DBEX004 = "DBEX004";
	
	/** No se pudo establecer comunicacion con los servicios. */
	public static final String EC00011B = "EC00011B";

	/**  Transaccion exitosa. */
	public static final String OK00000V = "OK00000V";

	/**   Generacion de Exportar. */
	public static final String OK00001V = "OK00001V";
	
	/**   Generacion de Exportar  con parametro. */
	public static final String OK00002V = "OK00002V";
	
	/**  Operacion exitosa. */
	public static final String OK00003V = "OK00003V";
	
	/**  Operacion exitosa. */
	public static final String OK00022V = "OK00022V";
	
	/**  Operacion exitosa. */
	public static final String OK00023V = "OK00023V";
	
	
	/**  Operacion exitosa. */
	public static final String OK00004V = "OK00004V";
	
	/** Propiedad del tipo public static final String que almacena el valor de OK00013V. */
	public static final String OK00013V = "OK00013V";
     
     /** La institucion financiera {0} esta {1} para enviar pagos a traves del POA. */
	public static final String OK00014V = "OK00014V";

	/** La institucion financiera {0} esta {1} para validar la cuenta. */
	public static final String OK00015V = "OK00015V";
	
	/**  Descripcion del codigo DESC_ED00130V existe informacion en el catalogo. */
	public static final String ED00130V = "ED00130V";
	
	/**  Descripcion del codigo OK00000V Transaccion exitosa. */
	public static final String DESC_OK00000V = "TRANSACCION EXITOSA";
	
	/**  Descripcion del codigo OK00003V La informaci\u00F3n se ha guardado exitosamente. */    
	public static final String DESC_OK00003V = "La informaci\u00F3n se ha guardado exitosamente";  
	
	/**  Descripcion del codigo EC00011B No se pudo establecer comunicacion con los servicios. */
	public static final String DESC_EC00011B = "NO SE PUDO ESTABLECER COMUNICACION CON LOS SERVICIOS";

	/**  Descripcion del codigo ED00011V No existe informacion para la consulta solicitada. */
	public static final String DESC_ED00011V = "No se encontro informaci\u00F3n";
	
	/**  Descripcion del codigo DESC_ED00130V existe informacion en el catalogo. */
	public static final String DESC_ED00130V = "El registro ya existe en el cat\u00e1logo. ";
	
	/** Constante que indica que el formato es invalido. */
	public static final String ED00027V = "ED00027V";
	
	/** Constante Descripcion de la clave ED00027V. */
	public static final String DESC_ED00027V = "EL FORMATO DEL PREFIJO CUENTA CLABE ES INVALIDO";
	
	/**  Seleccione solo un registro. */
	public static final String ED00029V = "ED00029V";
	
	/**  Seleccione solo un registro. */
	public static final String ED00030V = "ED00030V";
	
	/** ERROR DE MAPEO DE OBJETO. */
	public static final String EMO0001V = "EMO0001V";	
		
	/** Propiedad del tipo public static final String que almacena el valor de ED00061V. */
	public static final String ED00061V = "ED00061V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00063V. */
	public static final String ED00063V= "ED00063V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00064V. */
	public static final String ED00064V= "ED00064V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00066V. */
	public static final String ED00066V = "ED00066V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00067V. */
	public static final String ED00067V = "ED00067V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00067V. */
	public static final String ED00068V = "ED00068V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00067V. */
	public static final String DES_ED00068V = "Se requiere palomear el checkbox de al menos un registro";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00069V. */
	public static final String ED00069V = "ED00069V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00070V. */
	public static final String ED00070V = "ED00070V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00071V. */
	public static final String ED00071V = "ED00071V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00072V. */
	public static final String ED00072V = "ED00072V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00073V. */
	public static final String ED00073V = "ED00073V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00074V. */
	public static final String ED00074V = "ED00074V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00075V. */
	public static final String ED00075V = "ED00075V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00076V. */
	public static final String ED00076V = "ED00076V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00077V. */
	public static final String ED00077V = "ED00077V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00079V. */
	public static final String ED00079V = "ED00079V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00080V. */
	public static final String ED00080V = "ED00080V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00081V. */
	public static final String ED00081V = "ED00081V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00085V. */
	public static final String ED00085V = "ED00085V";
	
	/** La variable que contiene informacion con respecto a: ED00135V. */
	public static final String ED00135V = "ED00135V";
	
	/** La variable que contiene informacion con respecto a: ED00135V. */
	public static final String ED00136V = "ED00136V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00085V. */
	public static final String ED00132V = "ED00132V";
		
	/** Propiedad del tipo public static final String que almacena el valor de CD00166V. */
	public static final String CD00166V = "CD00166V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CD00173V. */
	public static final String CD00173V = "CD00173V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00092V. */
	public static final String ED00092V = "ED00092V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00093V. */
	public static final String ED00093V = "ED00093V";
	
	/**
	 * Propiedad del tipo public static final String que almacena el valor de ED00096V Archivo de Configuracion cfg.properties
	 */
	public static final String ED00096V = "ED00096V"; 
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00097V. */
	public static final String ED00097V = "ED00097V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00098V TimeOut. */
	public static final String ED00098V = "ED00098V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00099V Host Desconocido. */
	public static final String ED00099V = "ED00099V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00100V Error Desconocido. */
	public static final String ED00100V = "ED00100V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00100V Error Desconocido Resspuesta. */
	public static final String ED00101V = "ED00101V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00102V. */
	public static final String ED00102V = "ED00102V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00103V. */
	public static final String ED00103V = "ED00103V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00102V. */
	public static final String ED00104V = "ED00104V";

	/** Propiedad del tipo public static final String que almacena el valor de ED00102V. */
	public static final String ED00124V = "ED00124V";

	/** Propiedad del tipo public static final String que almacena el valor de ED00125V. */
	public static final String ED00125V = "ED00125V";

	/** Propiedad del tipo public static final String que almacena el valor de ED00126V. */
	public static final String ED00126V = "ED00126V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00127V. */
	public static final String ED00127V = "ED00127V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00127V. */
	public static final String ED00129V = "ED00129V";
	
	/** Propiedad del tipo public static final String que almacena el valor de ED00020V. */
	public static final String ED00020V = "ED00020V";
	
	/** Descripcion del error ED00020V. */
	public static final String DESC_ED00020V = "Fecha de Operaci\u00F3n no v\u00E1lida";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO001V. */
	public static final String CMCO001V = "CMCO001V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO002V. */
	public static final String CMCO002V = "CMCO002V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO003V. */
	public static final String CMCO003V = "CMCO003V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO004V. */
	public static final String CMCO004V = "CMCO004V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO005V. */
	public static final String CMCO005V = "CMCO005V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO006V. */
	public static final String CMCO006V = "CMCO006V";

	/** Propiedad del tipo public static final String que almacena el valor de CMCO007V. */
	public static final String CMCO007V = "CMCO007V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO008V. */
	public static final String CMCO008V = "CMCO008V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO009V. */
	public static final String CMCO009V = "CMCO009V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO010V. */
	public static final String CMCO010V = "CMCO010V";	
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO011V. */
	public static final String CMCO011V = "CMCO011V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO012V. */
	public static final String CMCO012V = "CMCO012V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO013V. */
	public static final String CMCO013V = "CMCO013V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO014V. */
	public static final String CMCO014V = "CMCO014V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO015V. */
	public static final String CMCO015V = "CMCO015V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO016V. */
	public static final String CMCO016V = "CMCO016V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO017V. */
	public static final String CMCO017V = "CMCO017V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO018V. */
	public static final String CMCO018V = "CMCO018V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO019V. */
	public static final String CMCO019V = "CMCO019V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMCO020V. */
	public static final String CMCO020V = "CMCO020V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMMT001V. */
	public static final String CMMT001V = "CMMT001V";	
	
	/** Propiedad del tipo public static final String que almacena el valor de CMMT003V. */
	public static final String CMMT003V = "CMMT003V";	
	
	/** Propiedad del tipo public static final String que almacena el valor de CMMT004V. */
	public static final String CMMT004V = "CMMT004V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMMT005V. */
	public static final String CMMT005V= "CMMT005V";
	
	/** Propiedad del tipo public static final String que almacena el valor de CMMT006V. */
	public static final String CMMT006V= "CMMT006V";
	
	/** La variable que contiene informacion con respecto a: ED00133V. */
	public static final String ED00133V ="ED00133V";
	
	/** La variable que contiene informacion con respecto a: ED00134V. */
	public static final String ED00134V ="ED00134V";
	
	/** La variable que contiene informacion con respecto a: ED00134V. */
	public static final String ETRAMA001 ="ETRAMA001";
	 
 	/** Propiedad del tipo public static final String que almacena el valor de OK00021V. */
    public static final String OK00021V = "OK00021V";
    
    /** La variable que contiene informacion con respecto a: ED00137V. */
    public static final String ED00137V = "ED00137V";
    
    /** La variable que contiene informacion con respecto a: ed00140v. */
    public static final String ED00140V = "ED00140V";
    
    /** La variable que contiene informacion con respecto a: ed00141v. */
    public static final String ED00141V = "ED00141V";
    
    /** La variable que contiene informacion con respecto a: ok00142v. */
    public static final String OK00142V = "OK00142V";
    
    /** La variable que contiene informacion con respecto a: ed00130v. */

    public static final String ED00139V = "ED00139V";
     
    /** La variable que contiene informacion con respecto a: ed00145v. */
    public static final String ED00145V = "ED00145V";
    
    /**Constructor por default**/
    private Errores() {
    //constructor vacio
    }
}
