package mx.isban.eTransferNal.constantes.moduloCDA;

import java.util.HashMap;
import java.util.Map;

public interface Constantes {
	/**Constante del tipo String*/
	String COD_ERROR ="codError";
	/**Constante del tipo String*/
	String COD_ERROR_PUNTO ="codError.";
	/**Constante del tipo String*/
	String TIPO_ERROR ="tipoError";
	/**Constante del tipo String*/
	String DESC_ERROR ="descError";
	/**
	 * Constante con el valor de 0.0
	 */
	String CERO_DEC = "0.0";
	
	/**
	 * Constante con el valor de 0
	 */
	String CERO = "0";
	
	/**
	 * Propiedad del tipo Integer que almacena el valor de INT_CERO
	 */
	Integer INT_CERO = Integer.valueOf("0");
	
	/**
	 * Propiedad del tipo Map<String,Object> que almacena el valor de map
	 */
	Map<String,Object> CACHE = new HashMap<String,Object>();
	
	/**
	 * Propiedad del tipo Object que almacena el valor de LOCK
	 */
	Object LOCK = new Object();
	
	

	
	
}
