/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BeanResponseConsultaBancoSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      25/10/2017 11:35:45 AM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class BeanListBancosSPID.
 */
public class BeanListBancosSPID implements Serializable{

	/**
	 * Variable de Serializacion
	 */
	private static final long serialVersionUID = 8367710743488122551L;

	/** The lista bancos spid. */
	private List<BeanConsultaBancoSPID> bancoSPID;

	/**
	 * Gets the banco spid.
	 *
	 * @return the banco spid
	 */
	public List<BeanConsultaBancoSPID> getBancoSPID() {
		return new ArrayList<BeanConsultaBancoSPID>(bancoSPID);
	}

	/**
	 * Sets the banco spid.
	 *
	 * @param bancoSPID the new banco spid
	 */
	public void setBancoSPID(List<BeanConsultaBancoSPID> bancoSPID) {
		this.bancoSPID = new ArrayList<BeanConsultaBancoSPID>(bancoSPID);
	}
}