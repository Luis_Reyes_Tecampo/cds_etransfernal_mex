/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BeanConsultaBancoSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 05:31:45 PM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.ws;

import java.io.Serializable;

/**
 * The Class BeanConsultaBancoSPID.
 */
public class BeanConsultaBancoSPID implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -8018463697595946062L;

	/**  Propiedad que almacena el valor de numCECOBAN. */
	private String numCECOBAN;

	/**  Propiedad que almacena el valor de cveInterme. */
	private String cveInterme;

	/**  Propiedad que almacena el valor de nombreCorto. */
	private String nombreCorto;

	/**  Propiedad que almacena el valor de nombreLargo. */
	private String nombreLargo;

	/**
	 * Gets the num cecoban.
	 *
	 * @return the num cecoban
	 */
	public String getNumCECOBAN() {
		return numCECOBAN;
	}

	/**
	 * Sets the num cecoban.
	 *
	 * @param numCECOBAN the new num cecoban
	 */
	public void setNumCECOBAN(String numCECOBAN) {
		this.numCECOBAN = numCECOBAN;
	}

	/**
	 * Gets the cve interme.
	 *
	 * @return the cve interme
	 */
	public String getCveInterme() {
		return cveInterme;
	}

	/**
	 * Sets the cve interme.
	 *
	 * @param cveInterme the new cve interme
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}

	/**
	 * Gets the nombre corto.
	 *
	 * @return the nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * Sets the nombre corto.
	 *
	 * @param nombreCorto the new nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * Gets the nombre largo.
	 *
	 * @return the nombre largo
	 */
	public String getNombreLargo() {
		return nombreLargo;
	}

	/**
	 * Sets the nombre largo.
	 *
	 * @param nombreLargo the new nombre largo
	 */
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
}