/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BeanResponseConsultaBancoSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 11:05:45 AM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.ws;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Bean que almacena la informacion de respuesta de la consulta del banco.
 *
 * @author agarcia
 */
public class BeanResponseConsultaBancoSPID implements Serializable{

	/**  Propiedad que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 8407465984721578801L;

	/** The lista bancos spid. */
	private BeanListBancosSPID listBancosSPID;

	/**  Bean del tipo BeanResBase que almacena el Resultado OPeracion. */
	private BeanResBase ResultOPeracion;

	/**
	 * Gets the list bancos spid.
	 *
	 * @return the list bancos spid
	 */
	public BeanListBancosSPID getListBancosSPID() {
		return listBancosSPID;
	}

	/**
	 * Sets the list bancos spid.
	 *
	 * @param listBancosSPID the new list bancos spid
	 */
	public void setListBancosSPID(BeanListBancosSPID listBancosSPID) {
		this.listBancosSPID = listBancosSPID;
	}

	/**
	 * Gets the result o peracion.
	 *
	 * @return the result o peracion
	 */
	public BeanResBase getResultOPeracion() {
		return ResultOPeracion;
	}

	/**
	 * Sets the result o peracion.
	 *
	 * @param resultOPeracion the new result o peracion
	 */
	public void setResultOPeracion(BeanResBase resultOPeracion) {
		ResultOPeracion = resultOPeracion;
	}
}