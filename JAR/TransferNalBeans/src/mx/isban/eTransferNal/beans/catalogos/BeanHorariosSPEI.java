/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanHorariosSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:30 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que  forman el catalogo
 * de horarios SPEI
 **/
public class BeanHorariosSPEI implements Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7201884987335257662L;
	
	/** Propiedad del tipo String almacena la clave de Medio de Entrega */
	private String cveMedioEnt;
	/** Propiedad del tipo String almacena la clave Transferencia */
	private String cveTransfe;
	/** Propiedad del tipo String almacena la clave Operacion */
	private String cveOperacion;
	/** Propiedad del tipo String almacena la clave de Topologia de operacion */
	private String cveTopoPri;
	/** Propiedad del tipo String almacena la hora de inicio */
	private String horaIncio;
	/** Propiedad del tipo String almacena la hora de cierre */
	private String horaCierre;
	/** Propiedad del tipo String almacena el dia habil */
	private String flgInhabil;
	/** Propiedad del tipo String almacena si esta seleccionado o no en la lista */
	private boolean seleccionado;
	/**
	 * @return el seleccionado
	 */
	public boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado el seleccionado a establecer
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return el cveMedioEnt
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	/**
	 * @param cveMedioEnt el cveMedioEnt a establecer
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	/**
	 * @return el cveTransfe
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	/**
	 * @param cveTransfe el cveTransfe a establecer
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	/**
	 * @return el cveOperacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	/**
	 * @param cveOperacion el cveOperacion a establecer
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	/**
	 * @return el cveTopoPri
	 */
	public String getCveTopoPri() {
		return cveTopoPri;
	}
	/**
	 * @param cveTopoPri el cveTopoPri a establecer
	 */
	public void setCveTopoPri(String cveTopoPri) {
		this.cveTopoPri = cveTopoPri;
	}
	/**
	 * @return el horaIncio
	 */
	public String getHoraIncio() {
		return horaIncio;
	}
	/**
	 * @param horaIncio el horaIncio a establecer
	 */
	public void setHoraIncio(String horaIncio) {
		this.horaIncio = horaIncio;
	}
	/**
	 * @return el horaCierre
	 */
	public String getHoraCierre() {
		return horaCierre;
	}
	/**
	 * @param horaCierre el horaCierre a establecer
	 */
	public void setHoraCierre(String horaCierre) {
		this.horaCierre = horaCierre;
	}
	/**
	 * @return el flgInhabil
	 */
	public String getFlgInhabil() {
		return flgInhabil;
	}
	/**
	 * @param flgInhabil el flgInhabil a establecer
	 */
	public void setFlgInhabil(String flgInhabil) {
		this.flgInhabil = flgInhabil;
	}

	

}
