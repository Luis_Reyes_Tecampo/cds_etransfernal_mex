package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el el histotico de Intemediarios
**/
public class BeanIntFinancieroHistorico extends BeanIntFinancieroHisto implements Serializable {
	
	/** La constante serialVersionUID.
	* El proceso de serializaciÃ³n asocia cada clase serializable a un serialVersionUID.
	* Si la clase no especifica un serialVersionUID el proceso de serializacion  
	* calcularÃ¡ un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = -9065925398170478004L;
	/** La variable que contiene informacion con respecto a: nombreLargoOri. */
	protected String nombreLargoOri;  
	/** La variable que contiene informacion con respecto a: nombreLargoNvo. */
	protected String nombreLargoNvo;
	/** La variable que contiene informacion con respecto a: numBanxicoOri. */
	protected String numBanxicoOri;  
	/** La variable que contiene informacion con respecto a: numBanxicoNvo. */
	protected String numBanxicoNvo;
	/** Propiedad del tipo @see BeanPaginador que almacena la abstraccion de la paginacion**/
	private BeanPaginador paginador;
	/**
	* Obtener el objeto: nombreLargoOri
	*
	* @return El objeto: nombreLargoOri
	*/
	public String getNombreLargoOri() {
		return nombreLargoOri;
	}
	/**
	* Obtener el objeto: nombreLargoOri
	*
	* @param El objeto: nombreLargoOri
	*/
	public void setNombreLargoOri(String nombreLargoOri) {
		this.nombreLargoOri = nombreLargoOri;
	}
	/**
	* Obtener el objeto: nombreLargoNvo.
	*
	* @return El objeto: nombreLargoNvo
	*/
	public String getNombreLargoNvo() {
		return nombreLargoNvo;
	}
	/**
	* Obtener el objeto: nombreLargoNvo.
	*
	* @param El objeto: nombreLargoNvo
	*/
	public void setNombreLargoNvo(String nombreLargoNvo) {
		this.nombreLargoNvo = nombreLargoNvo;
	}
	/**
	* Obtener el objeto: numBanxicoOri.
	*
	* @return El objeto: numBanxicoOri
	*/
	public String getNumBanxicoOri() {
		return numBanxicoOri;
	}
	/**
	* Obtener el objeto: numBanxicoOri.
	*
	* @param El objeto: numBanxicoOri
	*/
	public void setNumBanxicoOri(String numBanxicoOri) {
		this.numBanxicoOri = numBanxicoOri;
	}
	/**
	* Obtener el objeto: numBanxicoNvo
	*
	* @return El objeto: numBanxicoNvo
	*/
	public String getNumBanxicoNvo() {
		return numBanxicoNvo;
	}
	/**
	* Obtener el objeto: numBanxicoNvo
	*
	* @param El objeto: numBanxicoNvo
	*/
	public void setNumBanxicoNvo(String numBanxicoNvo) {
		this.numBanxicoNvo = numBanxicoNvo;
	}
	/**
	* Obtener el objeto: paginador
	*
	* @return El objeto: paginador
	*/
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	* Obtener el objeto: paginador
	*
	* @param El objeto: paginador
	*/
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
}