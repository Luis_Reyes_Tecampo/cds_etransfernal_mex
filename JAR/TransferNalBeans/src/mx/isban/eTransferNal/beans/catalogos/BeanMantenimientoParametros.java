/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * The Class BeanMantenimientoParametros.
 */
public class BeanMantenimientoParametros implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5982844791760384111L;

	/** The cve parametro. */
	private String cveParametro;
	
	/** The parametro falta. */
	private String parametroFalta;
	
	/** The valor falta. */
	private String valorFalta;
	
	/** The tipo parametro. */
	private String tipoParametro;
	
	/** The descripcion. */
	private String descripParam;

	/** The valor. */
	private String opcion;
	
	/** The valor opcion. */
	private String valorOpcion;
	
	/** The seleccionado. */
	private boolean seleccionado=false;
	
	/** The tipo dato. */
	private String tipoDato;
	
	/** The longitud. */
	private String longitud;
	
	/** The trama. */
	private String trama;
	
	/** The cola. */
	private String cola;
	
	/** The mecanismo. */
	private String mecanismo;
	
	/** The posicion. */
	private String posicion;
	
	/** The programa. */
	private String programa;

	/**
	 * Gets the cve parametro.
	 *
	 * @return the cve parametro
	 */
	public String getCveParametro() {
		return cveParametro;
	}

	/**
	 * Sets the cve parametro.
	 *
	 * @param cveParametro the new cve parametro
	 */
	public void setCveParametro(String cveParametro) {
		this.cveParametro = cveParametro;
	}

	/**
	 * Gets the parametro falta.
	 *
	 * @return the parametro falta
	 */
	public String getParametroFalta() {
		return parametroFalta;
	}

	/**
	 * Sets the parametro falta.
	 *
	 * @param parametroFalta the new parametro falta
	 */
	public void setParametroFalta(String parametroFalta) {
		this.parametroFalta = parametroFalta;
	}

	/**
	 * Gets the valor falta.
	 *
	 * @return the valor falta
	 */
	public String getValorFalta() {
		return valorFalta;
	}

	/**
	 * Sets the valor falta.
	 *
	 * @param valorFalta the new valor falta
	 */
	public void setValorFalta(String valorFalta) {
		this.valorFalta = valorFalta;
	}

	/**
	 * Gets the tipo parametro.
	 *
	 * @return the tipo parametro
	 */
	public String getTipoParametro() {
		return tipoParametro;
	}

	/**
	 * Sets the tipo parametro.
	 *
	 * @param tipoParametro the new tipo parametro
	 */
	public void setTipoParametro(String tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	/**
	 * Gets the descrip param.
	 *
	 * @return the descrip param
	 */
	public String getDescripParam() {
		return descripParam;
	}

	/**
	 * Sets the descrip param.
	 *
	 * @param descripParam the new descrip param
	 */
	public void setDescripParam(String descripParam) {
		this.descripParam = descripParam;
	}

	/**
	 * Gets the opcion.
	 *
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Sets the opcion.
	 *
	 * @param opcion the new opcion
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Gets the valor opcion.
	 *
	 * @return the valor opcion
	 */
	public String getValorOpcion() {
		return valorOpcion;
	}

	/**
	 * Sets the valor opcion.
	 *
	 * @param valorOpcion the new valor opcion
	 */
	public void setValorOpcion(String valorOpcion) {
		this.valorOpcion = valorOpcion;
	}

	/**
	 * Checks if is seleccionado.
	 *
	 * @return true, if is seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Sets the seleccionado.
	 *
	 * @param seleccionado the new seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Gets the tipo dato.
	 *
	 * @return the tipo dato
	 */
	public String getTipoDato() {
		return tipoDato;
	}

	/**
	 * Sets the tipo dato.
	 *
	 * @param tipoDato the new tipo dato
	 */
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	/**
	 * Gets the longitud.
	 *
	 * @return the longitud
	 */
	public String getLongitud() {
		return longitud;
	}

	/**
	 * Sets the longitud.
	 *
	 * @param longitud the new longitud
	 */
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	/**
	 * Gets the trama.
	 *
	 * @return the trama
	 */
	public String getTrama() {
		return trama;
	}

	/**
	 * Sets the trama.
	 *
	 * @param trama the new trama
	 */
	public void setTrama(String trama) {
		this.trama = trama;
	}

	/**
	 * Gets the cola.
	 *
	 * @return the cola
	 */
	public String getCola() {
		return cola;
	}

	/**
	 * Sets the cola.
	 *
	 * @param cola the new cola
	 */
	public void setCola(String cola) {
		this.cola = cola;
	}

	/**
	 * Gets the mecanismo.
	 *
	 * @return the mecanismo
	 */
	public String getMecanismo() {
		return mecanismo;
	}

	/**
	 * Sets the mecanismo.
	 *
	 * @param mecanismo the new mecanismo
	 */
	public void setMecanismo(String mecanismo) {
		this.mecanismo = mecanismo;
	}

	/**
	 * Gets the posicion.
	 *
	 * @return the posicion
	 */
	public String getPosicion() {
		return posicion;
	}

	/**
	 * Sets the posicion.
	 *
	 * @param posicion the new posicion
	 */
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	/**
	 * Gets the programa.
	 *
	 * @return the programa
	 */
	public String getPrograma() {
		return programa;
	}

	/**
	 * Sets the programa.
	 *
	 * @param programa the new programa
	 */
	public void setPrograma(String programa) {
		this.programa = programa;
	}
}
