/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMedEntrega.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * @author ooamador
 *
 */
public class BeanMedEntrega implements Serializable {

	/**
	 * Numero de serie
	 */
	private static final long serialVersionUID = 1L;
	
	/** Clave de medio de entrega. */
	private String cveMedioEntrega;
	
	/** Nombre o descripcion del medio de entrega. */
	private String nombreMedioEntrega;

	/**
	 * Devuelve el valor de cveMedioEntrega
	 * @return the cveMedioEntrega
	 */
	public String getCveMedioEntrega() {
		return cveMedioEntrega;
	}

	/**
	 * Modifica el valor de cveMedioEntrega
	 * @param cveMedioEntrega the cveMedioEntrega to set
	 */
	public void setCveMedioEntrega(String cveMedioEntrega) {
		this.cveMedioEntrega = cveMedioEntrega;
	}

	/**
	 * Devuelve el valor de nombreMedioEntrega
	 * @return the nombreMedioEntrega
	 */
	public String getNombreMedioEntrega() {
		return nombreMedioEntrega;
	}

	/**
	 * Modifica el valor de nombreMedioEntrega
	 * @param nombreMedioEntrega the nombreMedioEntrega to set
	 */
	public void setNombreMedioEntrega(String nombreMedioEntrega) {
		this.nombreMedioEntrega = nombreMedioEntrega;
	}


}
