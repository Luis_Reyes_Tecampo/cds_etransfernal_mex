/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanCatalogo.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:30 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
/**
 *Clase la cual se encapsulan los datos que se consultan de catalogos
 * para mostrar en listas
 **/
public class BeanCatalogo implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -3790982911112989113L;
	
	/** Propiedad del tipo String almacena la clave del catalogo */
	private String cve;
	/** Propiedad del tipo String almacena la descripcion del catalogo */
	private String descripcion;
	
	/**
	 * @return el cve
	 */
	public String getCve() {
		return cve;
	}
	/**
	 * @param cve el cve a establecer
	 */
	public void setCve(String cve) {
		this.cve = cve;
	}
	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	

}
