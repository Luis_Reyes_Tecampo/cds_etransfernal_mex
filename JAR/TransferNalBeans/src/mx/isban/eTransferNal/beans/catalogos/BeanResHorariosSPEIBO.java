/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:47 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se van ha insertar en el catalogo 
 * 7x24
 **/
public class BeanResHorariosSPEIBO implements Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -4351800153605836235L;
	
	/** Propiedad del tipo String que indica la clave medio entrega  */
	private String cveMedioEntrega;
	/** Propiedad del tipo String que indica la clave transferencia  */
	private String cveTransferencia;
	/** Propiedad del tipo String que indica la clave de operacion*/
	private String cveOperacion;
	/** Propiedad del tipo  lista */
	private List<BeanCatalogo> listMedioEntrega;
	/** Propiedad del tipo  lista */
	private List<BeanCatalogo> listTrasnfer;
	/** Propiedad del tipo  lista */
	private List<BeanCatalogo> listOperacion;
	/** Propiedad del tipo  lista */
	private List<BeanHorariosSPEI> listHorariosSPEI;
	/** Propiedad del tipo  BeanHorariosSPEI */
	private BeanHorariosSPEI horarioSPEI;
	/** Propiedad del tipo  codError */
	private String codError;
	/** Propiedad del tipo  msgError */
	private String msgError;
	/**Propiedad que almacena la abstraccion de la paginacion*/
	private BeanPaginador paginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**Propiedad privada del tipo de lista a consultar*/
	private String conLista;
	/** Propiedad del tipo mensaje de error */
	private String tipoError;
	
	/**
	 * @return el cveMedioEntrega
	 */
	public String getCveMedioEntrega() {
		return cveMedioEntrega;
	}
	/**
	 * @param cveMedioEntrega el cveMedioEntrega a establecer
	 */
	public void setCveMedioEntrega(String cveMedioEntrega) {
		this.cveMedioEntrega = cveMedioEntrega;
	}
	/**
	 * @return el cveTransferencia
	 */
	public String getCveTransferencia() {
		return cveTransferencia;
	}
	/**
	 * @param cveTransferencia el cveTransferencia a establecer
	 */
	public void setCveTransferencia(String cveTransferencia) {
		this.cveTransferencia = cveTransferencia;
	}
	/**
	 * @return el cveOperacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	/**
	 * @param cveOperacion el cveOperacion a establecer
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	/**
	 * @return el listMedioEntrega
	 */
	public List<BeanCatalogo> getListMedioEntrega() {
		return listMedioEntrega;
	}
	/**
	 * @param listMedioEntrega el listMedioEntrega a establecer
	 */
	public void setListMedioEntrega(List<BeanCatalogo> listMedioEntrega) {
		this.listMedioEntrega = listMedioEntrega;
	}
	/**
	 * @return el listTrasnfer
	 */
	public List<BeanCatalogo> getListTrasnfer() {
		return listTrasnfer;
	}
	/**
	 * @param listTrasnfer el listTrasnfer a establecer
	 */
	public void setListTrasnfer(List<BeanCatalogo> listTrasnfer) {
		this.listTrasnfer = listTrasnfer;
	}
	/**
	 * @return el listOperacion
	 */
	public List<BeanCatalogo> getListOperacion() {
		return listOperacion;
	}
	/**
	 * @param listOperacion el listOperacion a establecer
	 */
	public void setListOperacion(List<BeanCatalogo> listOperacion) {
		this.listOperacion = listOperacion;
	}
	/**
	 * @return el listHorariosSPEI
	 */
	public List<BeanHorariosSPEI> getListHorariosSPEI() {
		return listHorariosSPEI;
	}
	/**
	 * @param listHorariosSPEI el listHorariosSPEI a establecer
	 */
	public void setListHorariosSPEI(List<BeanHorariosSPEI> listHorariosSPEI) {
		this.listHorariosSPEI = listHorariosSPEI;
	}
	/**
	 * @return el horarioSPEI
	 */
	public BeanHorariosSPEI getHorarioSPEI() {
		return horarioSPEI;
	}
	/**
	 * @param horarioSPEI el horarioSPEI a establecer
	 */
	public void setHorarioSPEI(BeanHorariosSPEI horarioSPEI) {
		this.horarioSPEI = horarioSPEI;
	}
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return el msgError
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * @return el paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * @param paginador el paginador a establecer
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el conLista
	 */
	public String getConLista() {
		return conLista;
	}
	/**
	 * @param conLista el conLista a establecer
	 */
	public void setConLista(String conLista) {
		this.conLista = conLista;
	}
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError el tipoError a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	

}
