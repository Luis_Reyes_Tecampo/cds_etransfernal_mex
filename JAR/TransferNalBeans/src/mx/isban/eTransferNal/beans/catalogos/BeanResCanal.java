/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResCanal.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:30 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanCanal;

public class BeanResCanal extends BeanResBase{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 687404622608568851L;
	
	/**
	 * Propiedad del tipo List<BeanResCanal> que almacena el valor de beanResCanalList
	 */
	private List<BeanCanal> beanResCanalList;

	/**
	 * Metodo get que sirve para obtener la propiedad beanResCanalList
	 * @return beanResCanalList Objeto del tipo List<BeanResCanal>
	 */
	public List<BeanCanal> getBeanResCanalList() {
		return beanResCanalList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanResCanalList
	 * @param beanResCanalList del tipo List<BeanResCanal>
	 */
	public void setBeanResCanalList(List<BeanCanal> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}  
	
	

}
