/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanConsultaPayme.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   		By 	      	Company 	 			Description
 * ------- -----------   		----------- ---------- 				----------------------
 * 1.0   07/12/2020 08:23:52 	SNG     	Fabrica SW SNG_Enterprise Creacion
 */


package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el catalogo canales id Unico
**/
public class BeanConsultaPayme extends BeanConsultaPayments implements Serializable {
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;	

	/**
	 * @Metodo verdadero o falso
	 * @return seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	/**
     * Definir el objeto: seleccionado.
     *
     * @param seleccionado El nuevo objeto: seleccionado
     */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	/**
	 * @Metodo getter for tipoOperacion
	 * @return tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @Metodo setter for tipoOperacion
	 * @return tipoOperacion
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
}