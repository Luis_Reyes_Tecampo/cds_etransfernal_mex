/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:53:43 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * bitacora administrativa
 **/

public class BeanResInst implements BeanResultBO,Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

	/**
	 * Propiedad del tipo List<BeanInstitucionPOACOA> que almacena el listado de
	 * registros de la bitacora administrativa
	 */
	private List<BeanInstitucion> listBeanInstitucion;
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador paginador = null;
	/** Propiedad del tipo Integer que almacena el toral de registors */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError = "";
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paginador
	 * 
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 *Modifica el valor de la propiedad paginador
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanInstitucion
	 * @return listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public List<BeanInstitucion> getListBeanInstitucion() {
		return listBeanInstitucion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad listBeanInstitucion
	 * @param listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public void setListBeanInstitucion(List<BeanInstitucion> listBeanInstitucion) {
		this.listBeanInstitucion = listBeanInstitucion;
	}
	
	


}
