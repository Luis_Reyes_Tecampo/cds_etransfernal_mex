/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * The Class BeanReqMantenimientoParametros.
 */
public class BeanReqMantenimientoParametros implements Serializable{

	/** Variable de Seralizacion. */
	private static final long serialVersionUID = -3115264611186569009L;

	/** The parametro. */
	private String parametro;
	
	/** The descripParam. */
	private String descripParam;
	
	/** The tipo parametro. */
	private String tipoParametro;
	
	/** The tipo dato. */
	private String tipoDato;
	
	/** The longitud dato. */
	private String longitudDato;
	
	/** The valor falta. */
	private String valorFalta;
	
	/** The trama. */
	private String trama;
	
	/** The cola. */
	private String cola;
	
	/** The mecanismo. */
	private String mecanismo;
	
	/** The posicion. */
	private String posicion;
	
	/** The parametro falta. */
	private String parametroFalta;
	
	/** The programa. */
	private String programa;
	
	/** Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar. */
    private BeanPaginador paginador = new BeanPaginador();
	
	/** The list selec param. */
	private List<BeanMantenimientoParametros> listSelecParam = new ArrayList<BeanMantenimientoParametros>();
    
	/**
	 * Gets the parametro.
	 *
	 * @return the parametro
	 */
	public String getParametro() {
		return parametro;
	}
	
	/**
	 * Sets the parametro.
	 *
	 * @param parametro the new parametro
	 */
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	
	/**
	 * Gets the descrip param.
	 *
	 * @return the descrip param
	 */
	public String getDescripParam() {
		return descripParam;
	}

	/**
	 * Sets the descrip param.
	 *
	 * @param descripParam the new descrip param
	 */
	public void setDescripParam(String descripParam) {
		this.descripParam = descripParam;
	}

	/**
	 * Gets the tipo parametro.
	 *
	 * @return the tipo parametro
	 */
	public String getTipoParametro() {
		return tipoParametro;
	}
	
	/**
	 * Sets the tipo parametro.
	 *
	 * @param tipoParametro the new tipo parametro
	 */
	public void setTipoParametro(String tipoParametro) {
		this.tipoParametro = tipoParametro;
	}
	
	/**
	 * Gets the tipo dato.
	 *
	 * @return the tipo dato
	 */
	public String getTipoDato() {
		return tipoDato;
	}
	
	/**
	 * Sets the tipo dato.
	 *
	 * @param tipoDato the new tipo dato
	 */
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}
	
	/**
	 * Gets the longitud dato.
	 *
	 * @return the longitud dato
	 */
	public String getLongitudDato() {
		return longitudDato;
	}
	
	/**
	 * Sets the longitud dato.
	 *
	 * @param longitudDato the new longitud dato
	 */
	public void setLongitudDato(String longitudDato) {
		this.longitudDato = longitudDato;
	}
	
	/**
	 * Gets the valor falta.
	 *
	 * @return the valor falta
	 */
	public String getValorFalta() {
		return valorFalta;
	}
	
	/**
	 * Sets the valor falta.
	 *
	 * @param valorFalta the new valor falta
	 */
	public void setValorFalta(String valorFalta) {
		this.valorFalta = valorFalta;
	}

	/**
	 * Gets the posicion.
	 *
	 * @return the posicion
	 */
	public String getPosicion() {
		return posicion;
	}
	
	/**
	 * Sets the posicion.
	 *
	 * @param posicion the new posicion
	 */
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	/**
	 * Gets the trama.
	 *
	 * @return the trama
	 */
	public String getTrama() {
		return trama;
	}
	
	/**
	 * Sets the trama.
	 *
	 * @param trama the new trama
	 */
	public void setTrama(String trama) {
		this.trama = trama;
	}
	
	/**
	 * Gets the mecanismo.
	 *
	 * @return the mecanismo
	 */
	public String getMecanismo() {
		return mecanismo;
	}
	
	/**
	 * Sets the mecanismo.
	 *
	 * @param mecanismo the new mecanismo
	 */
	public void setMecanismo(String mecanismo) {
		this.mecanismo = mecanismo;
	}
	
	/**
	 * Gets the cola.
	 *
	 * @return the cola
	 */
	public String getCola() {
		return cola;
	}
	
	/**
	 * Sets the cola.
	 *
	 * @param cola the new cola
	 */
	public void setCola(String cola) {
		this.cola = cola;
	}
	
	/**
	 * Gets the parametro falta.
	 *
	 * @return the parametro falta
	 */
	public String getParametroFalta() {
		return parametroFalta;
	}
	
	/**
	 * Sets the parametro falta.
	 *
	 * @param parametroFalta the new parametro falta
	 */
	public void setParametroFalta(String parametroFalta) {
		this.parametroFalta = parametroFalta;
	}
	
	/**
	 * Gets the programa.
	 *
	 * @return the programa
	 */
	public String getPrograma() {
		return programa;
	}
	
	/**
	 * Sets the programa.
	 *
	 * @param programa the new programa
	 */
	public void setPrograma(String programa) {
		this.programa = programa;
	}

	/**
	 * Gets the list selec param.
	 *
	 * @return the list selec param
	 */
	public List<BeanMantenimientoParametros> getListSelecParam() {
		return listSelecParam;
	}
	
	/**
	 * Sets the list selec param.
	 *
	 * @param listSelecParam the new list selec param
	 */
	public void setListSelecParam(List<BeanMantenimientoParametros> listSelecParam) {
		this.listSelecParam = listSelecParam;
	}

	/**
	 * Gets the paginador.
	 *
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Sets the paginador.
	 *
	 * @param paginador the new paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
}