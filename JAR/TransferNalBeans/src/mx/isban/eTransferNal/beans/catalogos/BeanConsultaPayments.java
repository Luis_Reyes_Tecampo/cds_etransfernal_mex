package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el catalogo Activar/Desacticar ID unico
**/
public class BeanConsultaPayments  implements Serializable{

	/** La constante serialVersionUID. 
	 * El proceso de serialización asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calculará un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 1L;
	
	/*Varible que contiene el descripcion*/
	@Size(max=30)	
	protected  String descripcion;
	/*Varible que contiene la clave */
	@Size(max=6)
	protected  String clave;
	/*Varible que contiene el act des */
	@Size(max=10)
	protected  String actDes;
	/*Varible que contiene el tipo operacion */
	@Size(max=10)
	protected  String tipoOperacion;
	/*Varible que contiene el detalles */
	@Size(max=40)
	protected  String detalles;
	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	protected boolean seleccionado;
	
	/**
	 * @Metodo getter for detalles
	 * @return detalles
	 */
	public String getDetalles() {
		return detalles;
	}
	
	/**
	 * @Metodo setter for detalles
	 * @return detalles
	 */
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	/**
	 * @Metodo getter for descripcion
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @Metodo setter for descripcion
	 * @return descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @Metodo getter for clave
	 * @return clave
	 */
	public String getClave() {
		return clave;
	}
	/**
	 * @Metodo setter for clave
	 * @return clave
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}
	/**
	 * @Metodo getter for actDes
	 * @return actDes
	 */
	public String getActDes() {
		return actDes;
	}
	/**
	 * @Metodo setter for actDes
	 * @return actDes
	 */
	public void setActDes(String actDes) {
		this.actDes = actDes;
	}
	
	
}
