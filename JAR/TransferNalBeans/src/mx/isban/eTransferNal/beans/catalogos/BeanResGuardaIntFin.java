/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGuardaInst.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:53:43 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResGuardaIntFin implements BeanResultBO,Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 27734982821904716L;
	
	/**
	 * Propiedad del tipo List<BeanInstitucionPOACOA> que almacena el listado de
	 * registros de la bitacora administrativa
	 */
	private List<BeanIntFinanciero> listBeanIntFinanciero;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError = "";
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanInstitucion
	 * @return listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public List<BeanIntFinanciero> getListBeanIntFinanciero() {
		return listBeanIntFinanciero;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanInstitucion
	 * @param listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public void setListBeanIntFinanciero(List<BeanIntFinanciero> listBeanIntFinanciero) {
		this.listBeanIntFinanciero = listBeanIntFinanciero;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad codError
	 * @return codError Objeto de tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad codError
	 * @param codError Objeto de tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad msgError
	 * @return msgError Objeto de tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad msgError
	 * @param msgError Objeto de tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoError
	 * @return tipoError Objeto de tipo @see String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoError
	 * @param tipoError Objeto de tipo @see String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	

}
