/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsReqInst.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:30 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * Class BeanConsReqIntFin.
 * 
 */
public class BeanConsReqIntFin implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8637831584493245028L;
    
    /** Propiedad del tipo @see BeanPaginador que almacena la abstraccion de la paginacion. */
    private BeanPaginador paginador;

    /** La variable que contiene informacion con respecto a: cve interme. */
    private String cveInterme;
    
    /** La variable que contiene informacion con respecto a: tipo interme. */
    private String tipoInterme;
    
    /** La variable que contiene informacion con respecto a: num cecoban. */
    private String numCecoban;
    
    /** La variable que contiene informacion con respecto a: nombre corto. */
    private String nombreCorto;
    
    /** La variable que contiene informacion con respecto a: nombre largo. */
    private String nombreLargo;
    
    /** La variable que contiene informacion con respecto a: num persona. */
    private String numPersona;
    
    /** La variable que contiene informacion con respecto a: fch alta. */
    private String fchAlta;
    
    /** La variable que contiene informacion con respecto a: fch baja. */
    private String fchBaja;
    
    /** La variable que contiene informacion con respecto a: fch ult modif. */
    private String fchUltModif;
    
    /** La variable que contiene informacion con respecto a: usuario modif. */
    private String usuarioModif;
    
    /** La variable que contiene informacion con respecto a: num banxico. */
    private String numBanxico;
    
    /** La variable que contiene informacion con respecto a: cve swift cor. */
    private String cveSwiftCor;
	
	/** Propiedad privada del tipo String que almacena el tipo de selecOpcion. */
	private String selecOpcion;

	/** La variable que contiene informacion con respecto a: operaSpei. */
	private String operaSpei;

	/** La variable que contiene informacion con respecto a: operaSpid. */
	private String operaSpid;	
	
	/** La variable que contiene informacion con respecto a: bandera. */
	private String bandera;
	  
    
	/**
	 * Obtener el objeto: paginador.
	 *
	 * @return El objeto: paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	 * Definir el objeto: paginador.
	 *
	 * @param paginador El nuevo objeto: paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getCveInterme() {
		return cveInterme;
	}
	
	/**
	 * Definir el objeto: cve interme.
	 *
	 * @param cveInterme El nuevo objeto: cve interme
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}
	
	/**
	 * Obtener el objeto: tipo interme.
	 *
	 * @return El objeto: tipo interme
	 */
	public String getTipoInterme() {
		return tipoInterme;
	}
	
	/**
	 * Definir el objeto: tipo interme.
	 *
	 * @param tipoInterme El nuevo objeto: tipo interme
	 */
	public void setTipoInterme(String tipoInterme) {
		this.tipoInterme = tipoInterme;
	}
	
	/**
	 * Obtener el objeto: num cecoban.
	 *
	 * @return El objeto: num cecoban
	 */
	public String getNumCecoban() {
		return numCecoban;
	}
	
	/**
	 * Definir el objeto: num cecoban.
	 *
	 * @param numCecoban El nuevo objeto: num cecoban
	 */
	public void setNumCecoban(String numCecoban) {
		this.numCecoban = numCecoban;
	}
	
	/**
	 * Obtener el objeto: nombre corto.
	 *
	 * @return El objeto: nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}
	
	/**
	 * Definir el objeto: nombre corto.
	 *
	 * @param nombreCorto El nuevo objeto: nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	
	/**
	 * Obtener el objeto: nombre largo.
	 *
	 * @return El objeto: nombre largo
	 */
	public String getNombreLargo() {
		return nombreLargo;
	}
	
	/**
	 * Definir el objeto: nombre largo.
	 *
	 * @param nombreLargo El nuevo objeto: nombre largo
	 */
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
	
	/**
	 * Obtener el objeto: num persona.
	 *
	 * @return El objeto: num persona
	 */
	public String getNumPersona() {
		return numPersona;
	}
	
	/**
	 * Definir el objeto: num persona.
	 *
	 * @param numPersona El nuevo objeto: num persona
	 */
	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}
	
	/**
	 * Obtener el objeto: fch alta.
	 *
	 * @return El objeto: fch alta
	 */
	public String getFchAlta() {
		return fchAlta;
	}
	
	/**
	 * Definir el objeto: fch alta.
	 *
	 * @param fchAlta El nuevo objeto: fch alta
	 */
	public void setFchAlta(String fchAlta) {
		this.fchAlta = fchAlta;
	}
	
	/**
	 * Obtener el objeto: fch baja.
	 *
	 * @return El objeto: fch baja
	 */
	public String getFchBaja() {
		return fchBaja;
	}
	
	/**
	 * Definir el objeto: fch baja.
	 *
	 * @param fchBaja El nuevo objeto: fch baja
	 */
	public void setFchBaja(String fchBaja) {
		this.fchBaja = fchBaja;
	}
	
	/**
	 * Obtener el objeto: fch ult modif.
	 *
	 * @return El objeto: fch ult modif
	 */
	public String getFchUltModif() {
		return fchUltModif;
	}
	
	/**
	 * Definir el objeto: fch ult modif.
	 *
	 * @param fchUltModif El nuevo objeto: fch ult modif
	 */
	public void setFchUltModif(String fchUltModif) {
		this.fchUltModif = fchUltModif;
	}
	
	/**
	 * Obtener el objeto: usuario modif.
	 *
	 * @return El objeto: usuario modif
	 */
	public String getUsuarioModif() {
		return usuarioModif;
	}
	
	/**
	 * Definir el objeto: usuario modif.
	 *
	 * @param usuarioModif El nuevo objeto: usuario modif
	 */
	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}
	
	/**
	 * Obtener el objeto: num banxico.
	 *
	 * @return El objeto: num banxico
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	
	/**
	 * Definir el objeto: num banxico.
	 *
	 * @param numBanxico El nuevo objeto: num banxico
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}
	
	/**
	 * Obtener el objeto: cve swift cor.
	 *
	 * @return El objeto: cve swift cor
	 */
	public String getCveSwiftCor() {
		return cveSwiftCor;
	}
	
	/**
	 * Definir el objeto: cve swift cor.
	 *
	 * @param cveSwiftCor El nuevo objeto: cve swift cor
	 */
	public void setCveSwiftCor(String cveSwiftCor) {
		this.cveSwiftCor = cveSwiftCor;
	}

	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getSelecOpcion() {
		return selecOpcion;
	}

	/**
	 * Definir el objeto: selecOpcion.
	 *
	 * @param cveInterme El nuevo objeto: selecOpcion
	 */
	public void setSelecOpcion(String selecOpcion) {
		this.selecOpcion = selecOpcion;
	}

	/**
	 * Obtener el objeto: OperaSpid.
	 *
	 * @return El objeto: OperaSpid
	 */
	public String getOperaSpid() {
		return operaSpid;
	}

	/**
	 * Definir el objeto: operaSpid.
	 *
	 * @param cveInterme El nuevo objeto: operaSpid
	 */
	public void setOperaSpid(String operaSpid) {
		this.operaSpid = operaSpid;
	}

	/**
	 * Obtener el objeto: OperaSpei.
	 *
	 * @return El objeto: OperaSpei
	 */
	public String getOperaSpei() {
		return operaSpei;
	}

	/**
	 * Definir el objeto: operaSpei.
	 *
	 * @param cveInterme El nuevo objeto: operaSpei
	 */
	public void setOperaSpei(String operaSpei) {
		this.operaSpei = operaSpei;
	}

	/**
	 * Obtener el objeto: bandera.
	 *
	 * @return El objeto: bandera
	 */
	public String getBandera() {
		return bandera;
	}

	/**
	 * Definir el objeto: bandera.
	 *
	 * @param cveInterme El nuevo objeto: bandera
	 */
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	
	
}