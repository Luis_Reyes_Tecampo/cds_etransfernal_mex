/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqInsertBitacora.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:10:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular un registro de la
 * funcionalidad de guardar bitacora 
**/

public class BeanReqEliminarInstPOACOA implements Serializable {
	
	/**
	* Constante privada del Serial version
	*/
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador paginador = null;
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombre
	 */
	private String nombre;

	/**
	 * Propiedad del tipo List<BeanInstitucionPOACOA> que almacena el listado de
	 * registros de la bitacora administrativa
	 */
	private List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA;
	/** Propiedad del tipo Integer que almacena el toral de registors */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError = "";
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanInstitucionPOACOA
	 * @return listBeanInstitucionPOACOA Objeto de tipo @see List<BeanInstitucionPOACOA>
	 */
	public List<BeanInstitucionPOACOA> getListBeanInstitucionPOACOA() {
		return listBeanInstitucionPOACOA;
	}

	/**
	 * Metodo que modifica el valor de la propiedad listBeanInstitucionPOACOA
	 * @param listBeanInstitucionPOACOA Objeto de tipo @see List<BeanInstitucionPOACOA>
	 */
	public void setListBeanInstitucionPOACOA(
			List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA) {
		this.listBeanInstitucionPOACOA = listBeanInstitucionPOACOA;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}



}
