/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanInstitucionPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 12:31:44 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos de la institucion
 * 
**/

public class BeanInstitucionPOACOA  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de participantePOA
	 */
	private String participantePOA;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveInterme
	 */
	private String cveInterme;
	
	/**
	 * Propiedad del tipo String que almacena el valor de validarCuenta
	 */
	private String validarCuenta;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombre
	 */
	private String nombre;

	/**
	 * Propiedad del tipo String que almacena el valor de cveUsuario
	 */
	private String cveUsuario;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveUsuario
	 */
	private String fechaMod;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de seleccionado
	 */
	private boolean seleccionado;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad participantePOA
	 * @return participantePOA Objeto de tipo @see String
	 */
	public String getParticipantePOA() {
		return participantePOA;
	}
	/**
	 * Metodo que modifica el valor de la propiedad participantePOA
	 * @param participantePOA Objeto de tipo @see String
	 */
	public void setParticipantePOA(String participantePOA) {
		this.participantePOA = participantePOA;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad validarCuenta
	 * @return validarCuenta Objeto de tipo @see String
	 */
	public String getValidarCuenta() {
		return validarCuenta;
	}
	/**
	 * Metodo que modifica el valor de la propiedad validarCuenta
	 * @param validarCuenta Objeto de tipo @see String
	 */
	public void setValidarCuenta(String validarCuenta) {
		this.validarCuenta = validarCuenta;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveUsuario
	 * @return cveUsuario Objeto de tipo @see String
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveUsuario
	 * @param cveUsuario Objeto de tipo @see String
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad fechaMod
	 * @return fechaMod Objeto de tipo @see String
	 */
	public String getFechaMod() {
		return fechaMod;
	}
	/**
	 * Metodo que modifica el valor de la propiedad fechaMod
	 * @param fechaMod Objeto de tipo @see String
	 */
	public void setFechaMod(String fechaMod) {
		this.fechaMod = fechaMod;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveInterme
	 * @return cveInterme Objeto de tipo @see String
	 */
	public String getCveInterme() {
		return cveInterme;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveInterme
	 * @param cveInterme Objeto de tipo @see String
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad seleccionado
	 * @return seleccionado Objeto de tipo @see boolean
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	/**
	 * Metodo que modifica el valor de la propiedad seleccionado
	 * @param seleccionado Objeto de tipo @see boolean
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	

}
