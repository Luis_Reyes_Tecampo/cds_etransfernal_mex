/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   24/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * The Class BeanReqMantenimientoParamGuardar.
 */
public class BeanReqMantenimientoParamGuardar extends BeanReqMantenimientoParametros implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -3440731389294867393L;

	/** The key cve param. */
	private String keyCveParam;

    /** The alta parametro. */
    private boolean altaParametro;

	/** The filtro parametro. */
	private String filtroParametro;
	
	/** The filtro descripcion. */
	private String filtroDescripcion;
	
	/** The filtro tipo parametro. */
	private String filtroTipoParametro;
	
	/** The filtro tipo dato. */
	private String filtroTipoDato;
	
	/** The filtro longitud dato. */
	private String filtroLongitudDato;
	
	/** The filtro valor falta. */
	private String filtroValorFalta;
	
	/** The filtro trama. */
	private String filtroTrama;
	
	/** The filtro cola. */
	private String filtroCola;
	
	/** The filtro mecanismo. */
	private String filtroMecanismo;
	
	/** The filtro posicion. */
	private String filtroPosicion;
	
	/** The filtro parametro falta. */
	private String filtroParametroFalta;
	
	/** The filtro programa. */
	private String filtroPrograma;
	
	/** The filtro alta parametro. */
	private String filtroAltaParametro;
    
	/**
	 * Checks if is alta parametro.
	 *
	 * @return true, if is alta parametro
	 */
	public boolean isAltaParametro() {
		return altaParametro;
	}

	/**
	 * Sets the alta parametro.
	 *
	 * @param altaParametro the new alta parametro
	 */
	public void setAltaParametro(boolean altaParametro) {
		this.altaParametro = altaParametro;
	}

	/**
	 * Gets the key cve param.
	 *
	 * @return the key cve param
	 */
	public String getKeyCveParam() {
		return keyCveParam;
	}

	/**
	 * Sets the key cve param.
	 *
	 * @param keyCveParam the new key cve param
	 */
	public void setKeyCveParam(String keyCveParam) {
		this.keyCveParam = keyCveParam;
	}

	/**
	 * Gets the filtro parametro.
	 *
	 * @return the filtro parametro
	 */
	public String getFiltroParametro() {
		return filtroParametro;
	}

	/**
	 * Sets the filtro parametro.
	 *
	 * @param filtroParametro the new filtro parametro
	 */
	public void setFiltroParametro(String filtroParametro) {
		this.filtroParametro = filtroParametro;
	}

	/**
	 * Gets the filtro descripcion.
	 *
	 * @return the filtro descripcion
	 */
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}

	/**
	 * Sets the filtro descripcion.
	 *
	 * @param filtroDescripcion the new filtro descripcion
	 */
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}

	/**
	 * Gets the filtro tipo parametro.
	 *
	 * @return the filtro tipo parametro
	 */
	public String getFiltroTipoParametro() {
		return filtroTipoParametro;
	}

	/**
	 * Sets the filtro tipo parametro.
	 *
	 * @param filtroTipoParametro the new filtro tipo parametro
	 */
	public void setFiltroTipoParametro(String filtroTipoParametro) {
		this.filtroTipoParametro = filtroTipoParametro;
	}

	/**
	 * Gets the filtro tipo dato.
	 *
	 * @return the filtro tipo dato
	 */
	public String getFiltroTipoDato() {
		return filtroTipoDato;
	}

	/**
	 * Sets the filtro tipo dato.
	 *
	 * @param filtroTipoDato the new filtro tipo dato
	 */
	public void setFiltroTipoDato(String filtroTipoDato) {
		this.filtroTipoDato = filtroTipoDato;
	}

	/**
	 * Gets the filtro longitud dato.
	 *
	 * @return the filtro longitud dato
	 */
	public String getFiltroLongitudDato() {
		return filtroLongitudDato;
	}

	/**
	 * Sets the filtro longitud dato.
	 *
	 * @param filtroLongitudDato the new filtro longitud dato
	 */
	public void setFiltroLongitudDato(String filtroLongitudDato) {
		this.filtroLongitudDato = filtroLongitudDato;
	}

	/**
	 * Gets the filtro valor falta.
	 *
	 * @return the filtro valor falta
	 */
	public String getFiltroValorFalta() {
		return filtroValorFalta;
	}

	/**
	 * Sets the filtro valor falta.
	 *
	 * @param filtroValorFalta the new filtro valor falta
	 */
	public void setFiltroValorFalta(String filtroValorFalta) {
		this.filtroValorFalta = filtroValorFalta;
	}

	/**
	 * Gets the filtro trama.
	 *
	 * @return the filtro trama
	 */
	public String getFiltroTrama() {
		return filtroTrama;
	}

	/**
	 * Sets the filtro trama.
	 *
	 * @param filtroTrama the new filtro trama
	 */
	public void setFiltroTrama(String filtroTrama) {
		this.filtroTrama = filtroTrama;
	}

	/**
	 * Gets the filtro cola.
	 *
	 * @return the filtro cola
	 */
	public String getFiltroCola() {
		return filtroCola;
	}

	/**
	 * Sets the filtro cola.
	 *
	 * @param filtroCola the new filtro cola
	 */
	public void setFiltroCola(String filtroCola) {
		this.filtroCola = filtroCola;
	}

	/**
	 * Gets the filtro mecanismo.
	 *
	 * @return the filtro mecanismo
	 */
	public String getFiltroMecanismo() {
		return filtroMecanismo;
	}

	/**
	 * Sets the filtro mecanismo.
	 *
	 * @param filtroMecanismo the new filtro mecanismo
	 */
	public void setFiltroMecanismo(String filtroMecanismo) {
		this.filtroMecanismo = filtroMecanismo;
	}

	/**
	 * Gets the filtro posicion.
	 *
	 * @return the filtro posicion
	 */
	public String getFiltroPosicion() {
		return filtroPosicion;
	}

	/**
	 * Sets the filtro posicion.
	 *
	 * @param filtroPosicion the new filtro posicion
	 */
	public void setFiltroPosicion(String filtroPosicion) {
		this.filtroPosicion = filtroPosicion;
	}

	/**
	 * Gets the filtro parametro falta.
	 *
	 * @return the filtro parametro falta
	 */
	public String getFiltroParametroFalta() {
		return filtroParametroFalta;
	}

	/**
	 * Sets the filtro parametro falta.
	 *
	 * @param filtroParametroFalta the new filtro parametro falta
	 */
	public void setFiltroParametroFalta(String filtroParametroFalta) {
		this.filtroParametroFalta = filtroParametroFalta;
	}

	/**
	 * Gets the filtro programa.
	 *
	 * @return the filtro programa
	 */
	public String getFiltroPrograma() {
		return filtroPrograma;
	}

	/**
	 * Sets the filtro programa.
	 *
	 * @param filtroPrograma the new filtro programa
	 */
	public void setFiltroPrograma(String filtroPrograma) {
		this.filtroPrograma = filtroPrograma;
	}

	/**
	 * Gets the filtro alta parametro.
	 *
	 * @return the filtro alta parametro
	 */
	public String getFiltroAltaParametro() {
		return filtroAltaParametro;
	}

	/**
	 * Sets the filtro alta parametro.
	 *
	 * @param filtroAltaParametro the new filtro alta parametro
	 */
	public void setFiltroAltaParametro(String filtroAltaParametro) {
		this.filtroAltaParametro = filtroAltaParametro;
	}
}