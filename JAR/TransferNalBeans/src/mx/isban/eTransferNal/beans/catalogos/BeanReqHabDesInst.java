/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqHabDesInst.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:10:08 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

public class BeanReqHabDesInst implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1476131946396729040L;
    
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombre
	 */
	private String nombre;
	
	/**
	 * Propiedad del tipo String que almacena el valor de habDesValor
	 */
	private String habDesValor;
	
	/**
	 * Propiedad del tipo String que almacena el valor de habDesNumBanxico
	 */
	private String habDesNumBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de habDesNombre
	 */
	private String habDesNombre;
	
	/**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
    de la paginacion*/
    private BeanPaginador paginador;
	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad habDesNumBanxico
	 * @return habDesNumBanxico Objeto de tipo @see String
	 */
	public String getHabDesNumBanxico() {
		return habDesNumBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad habDesNumBanxico
	 * @param habDesNumBanxico Objeto de tipo @see String
	 */
	public void setHabDesNumBanxico(String habDesNumBanxico) {
		this.habDesNumBanxico = habDesNumBanxico;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad habDesValor
	 * @return habDesValor Objeto de tipo @see String
	 */
	public String getHabDesValor() {
		return habDesValor;
	}
	/**
	 * Metodo que modifica el valor de la propiedad habDesValor
	 * @param habDesValor Objeto de tipo @see String
	 */
	public void setHabDesValor(String habDesValor) {
		this.habDesValor = habDesValor;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad habDesNombre
	 * @return habDesNombre Objeto de tipo @see String
	 */
	public String getHabDesNombre() {
		return habDesNombre;
	}
	/**
	 * Metodo que modifica el valor de la propiedad habDesNombre
	 * @param habDesNombre Objeto de tipo @see String
	 */
	public void setHabDesNombre(String habDesNombre) {
		this.habDesNombre = habDesNombre;
	}
    
    

}
