package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;


/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el histotico de Intemediarios 
**/
public class BeanIntFinancieroHisto extends BeanIntFinancieroHist implements Serializable  {
	
	/** La constante serialVersionUID.
	* El proceso de serializaciÃ³n asocia cada clase serializable a un serialVersionUID.
	* Si la clase no especifica un serialVersionUID el proceso de serializacion  
	* calcularÃ¡ un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 5940161045335898730L;
	/** La variable que contiene informacion con respecto a: tipoIntermeNvo. */
	protected String tipoIntermeNvo;
	/** La variable que contiene informacion con respecto a: numCecobanOri. */
	protected String numCecobanOri;              
	/** La variable que contiene informacion con respecto a: numCecobanNvo. */
	protected String numCecobanNvo;              
	/** La variable que contiene informacion con respecto a: nombreCortoOri. */
	protected String nombreCortoOri;        
	/** La variable que contiene informacion con respecto a: nombreCortoNvo. */
	protected String nombreCortoNvo;
	
	/**
	* Obtener el objeto: tipoIntermeNvo
	*
	* @return El objeto: tipoIntermeNvo
	*/
	public String getTipoIntermeNvo() {
		return tipoIntermeNvo;
	}
	/**
	* Obtener el objeto: tipoIntermeNvo
	*
	* @param El objeto: tipoIntermeNvo
	*/
	public void setTipoIntermeNvo(String tipoIntermeNvo) {
		this.tipoIntermeNvo = tipoIntermeNvo;
	}
	/**
	* Obtener el objeto: numCecobanOri
	*
	* @return El objeto: numCecobanOri
	*/
	public String getNumCecobanOri() {
		return numCecobanOri;
	}
	/**
	* Obtener el objeto: numCecobanOri
	*
	* @param El objeto: numCecobanOri
	*/
	public void setNumCecobanOri(String numCecobanOri) {
		this.numCecobanOri = numCecobanOri;
	}
	/**
	* Obtener el objeto: numCecobanNvo
	*
	* @return El objeto: numCecobanNvo
	*/
	public String getNumCecobanNvo() {
		return numCecobanNvo;
	}
	/**
	* Obtener el objeto: numCecobanNvo
	*
	* @param El objeto: numCecobanNvo
	*/
	public void setNumCecobanNvo(String numCecobanNvo) {
		this.numCecobanNvo = numCecobanNvo;
	}
	/**
	* Obtener el objeto: nombreCortoOri
	*
	* @return El objeto: nombreCortoOri
	*/
	public String getNombreCortoOri() {
		return nombreCortoOri;
	}
	/**
	* Obtener el objeto: nombreCortoOri
	*
	* @param El objeto: nombreCortoOri
	*/
	public void setNombreCortoOri(String nombreCortoOri) {
		this.nombreCortoOri = nombreCortoOri;
	}
	/**
	* Obtener el objeto: nombreCortoNvo
	*
	* @return El objeto: nombreCortoNvo
	*/
	public String getNombreCortoNvo() {
		return nombreCortoNvo;
	}
	/**
	* Obtener el objeto: nombreCortoNvo
	*
	* @param El objeto: nombreCortoNvo
	*/
	public void setNombreCortoNvo(String nombreCortoNvo) {
		this.nombreCortoNvo = nombreCortoNvo;
	}
}