package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanCodErroresDevoluciones.
 * 
 * Contiene los metodos y atributos 
 * para la pantalla de catalogo Activar/Desacticar ID unico
 * 
 * -Contiene una lista del bean: BeanConsultaPayments
 * -Contiene beanPaginador
 * -Contiene beanFilter
 * -Contiene beanError
 *
 * @author FSW-SNG Enterprise
 * @since 07/12/2020
 */
public class BeanConsultasPayments implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2369014286159941981L;
	
	/** La variable que contiene informacion con respecto a: medEntrega. */
	@Valid
	private List<BeanConsultaPayme> medEntrega;
	
	/** La variable que contiene informacion con respecto a: listaCMEnt. */
	@Valid
	private List<BeanConsultaPayme>  listaCMEnt;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	protected BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	protected BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	protected BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	@NotNull
	protected int totalReg = 0;

	/**
	 * Obtener el objeto: bean listaCMEnt.
	 *
	 * @return El objeto: bean listaCMEnt
	 */
	public List<BeanConsultaPayme> getListaCMEnt() {
		return listaCMEnt;
	}
	
	/**
	 * Definir el objeto: listaCMEnt.
	 *
	 * @param listaCanales El nuevo objeto: listaCMEnt
	 */
	
	public void setListaCMEnt(List<BeanConsultaPayme> listaCMEnt) {
		this.listaCMEnt = listaCMEnt;
	}
	
	/**
	 * Obtener el objeto: bean medEntrega.
	 *
	 * @return El objeto: bean medEntrega
	 */
	public List<BeanConsultaPayme> getMedEntrega() {
		return medEntrega;
	}

	/**
	 * Definir el objeto: medEntrega.
	 *
	 * @param medEntrega El nuevo objeto: medEntrega
	 */
	public void setMedEntrega(List<BeanConsultaPayme> medEntrega) {
		this.medEntrega = medEntrega;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}
	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}
}
