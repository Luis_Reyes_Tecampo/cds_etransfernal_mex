/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:53:43 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * bitacora administrativa.
 */

public class BeanResIntFinancieros implements BeanResultBO,Serializable {

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/** Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar. */
	private BeanPaginador beanPaginador = null;
	
	/** La variable que contiene informacion con respecto a: cve interme. */
	private String cveInterme;
	
	/** La variable que contiene informacion con respecto a: tipo interme. */
	private String tipoInterme;
	
	/** La variable que contiene informacion con respecto a: num cecoban. */
	private String numCecoban;
	
	/** La variable que contiene informacion con respecto a: nombre corto. */
	private String nombreCorto;
	
	/** La variable que contiene informacion con respecto a: nombre largo. */
	private String nombreLargo;
	
	/** La variable que contiene informacion con respecto a: num persona. */
	private String numPersona;
	
	/** La variable que contiene informacion con respecto a: fch alta. */
	private String fchAlta;
	
	/** La variable que contiene informacion con respecto a: fch baja. */
	private String fchBaja;
	
	/** La variable que contiene informacion con respecto a: fch ult modif. */
	private String fchUltModif;
	
	/** La variable que contiene informacion con respecto a: usuario modif. */
	private String usuarioModif;
	
	/** La variable que contiene informacion con respecto a: usuario registro. */
	private String usuarioRegistro;
	
	/** La variable que contiene informacion con respecto a: status. */
	private String status;
	
	/** La variable que contiene informacion con respecto a: num banxico. */
	private String numBanxico;
	
	/** La variable que contiene informacion con respecto a: num indeval. */
	private String numIndeval;
	
	/** La variable que contiene informacion con respecto a: id int indeval. */
	private String idIntIndeval;
	
	/** La variable que contiene informacion con respecto a: fol int indeval. */
	private String folIntIndeval;
	
	/** La variable que contiene informacion con respecto a: cve swift cor. */
	private String cveSwiftCor;

	/** Propiedad del tipo List<listBeanIntFinanciero> que almacena el listado de registros de los intervinientes financieros. */
	private List<BeanIntFinanciero> listBeanIntFinanciero;
	
	/**  Propiedad del tipo Integer que almacena el toral de registors. */
	private Integer totalReg = 0;
	
	/**  Propiedad privada del tipo String que almacena el codigo de error. */
	private String codError = "";
	
	/** Propiedad privada del tipo String que almacena el codigo de mensaje de error. */
	private String msgError = "";
	
	/**  Propiedad privada del tipo String que almacena el tipo de error. */
	private String tipoError;

	/** La variable que contiene informacion con respecto a: operaSpid. */
	private String operaSpid;
	
	/** La variable que contiene informacion con respecto a: operaSpei. */
	private String operaSpei;
	
	/**  Propiedad privada del tipo String que almacena el tipo de selecOpcion. */
	private String selecOpcion;
	
	
	/**  Propiedad privada del tipo String que almacena una bandera. */
	private String bandera;
	
	/**
	 * Obtener el objeto: OperaSpid.
	 *
	 * @return El objeto: OperaSpid
	 */
	public String getOperaSpid() {
		return operaSpid;
	}
	/**
	 * Definir el objeto: operaSpid.
	 *
	 * @param cveInterme El nuevo objeto: operaSpid
	 */
	public void setOperaSpid(String operaSpid) {
		this.operaSpid = operaSpid;
	}
	/**
	 * Obtener el objeto: OperaSpei.
	 *
	 * @return El objeto: OperaSpei
	 */
	public String getOperaSpei() {
		return operaSpei;
	}
	/**
	 * Definir el objeto: operaSpei.
	 *
	 * @param cveInterme El nuevo objeto: operaSpei
	 */
	public void setOperaSpei(String operaSpei) {
		this.operaSpei = operaSpei;
	}
	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getSelecOpcion() {
		return selecOpcion;
	}
	/**
	 * Definir el objeto: selecOpcion.
	 *
	 * @param cveInterme El nuevo objeto: selecOpcion
	 */
	public void setSelecOpcion(String selecOpcion) {
		this.selecOpcion = selecOpcion;
	}
	
	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getCveInterme() {
		return cveInterme;
	}

	/**
	 * Definir el objeto: cve interme.
	 *
	 * @param cveInterme El nuevo objeto: cve interme
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}

	/**
	 * Obtener el objeto: tipo interme.
	 *
	 * @return El objeto: tipo interme
	 */
	public String getTipoInterme() {
		return tipoInterme;
	}

	/**
	 * Definir el objeto: tipo interme.
	 *
	 * @param tipoInterme El nuevo objeto: tipo interme
	 */
	public void setTipoInterme(String tipoInterme) {
		this.tipoInterme = tipoInterme;
	}

	/**
	 * Obtener el objeto: num cecoban.
	 *
	 * @return El objeto: num cecoban
	 */
	public String getNumCecoban() {
		return numCecoban;
	}

	/**
	 * Definir el objeto: num cecoban.
	 *
	 * @param numCecoban El nuevo objeto: num cecoban
	 */
	public void setNumCecoban(String numCecoban) {
		this.numCecoban = numCecoban;
	}

	/**
	 * Obtener el objeto: nombre corto.
	 *
	 * @return El objeto: nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * Definir el objeto: nombre corto.
	 *
	 * @param nombreCorto El nuevo objeto: nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * Obtener el objeto: nombre largo.
	 *
	 * @return El objeto: nombre largo
	 */
	public String getNombreLargo() {
		return nombreLargo;
	}

	/**
	 * Definir el objeto: nombre largo.
	 *
	 * @param nombreLargo El nuevo objeto: nombre largo
	 */
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

	/**
	 * Obtener el objeto: num persona.
	 *
	 * @return El objeto: num persona
	 */
	public String getNumPersona() {
		return numPersona;
	}

	/**
	 * Definir el objeto: num persona.
	 *
	 * @param numPersona El nuevo objeto: num persona
	 */
	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}

	/**
	 * Obtener el objeto: fch alta.
	 *
	 * @return El objeto: fch alta
	 */
	public String getFchAlta() {
		return fchAlta;
	}

	/**
	 * Definir el objeto: fch alta.
	 *
	 * @param fchAlta El nuevo objeto: fch alta
	 */
	public void setFchAlta(String fchAlta) {
		this.fchAlta = fchAlta;
	}

	/**
	 * Obtener el objeto: fch baja.
	 *
	 * @return El objeto: fch baja
	 */
	public String getFchBaja() {
		return fchBaja;
	}

	/**
	 * Definir el objeto: fch baja.
	 *
	 * @param fchBaja El nuevo objeto: fch baja
	 */
	public void setFchBaja(String fchBaja) {
		this.fchBaja = fchBaja;
	}

	/**
	 * Obtener el objeto: fch ult modif.
	 *
	 * @return El objeto: fch ult modif
	 */
	public String getFchUltModif() {
		return fchUltModif;
	}

	/**
	 * Definir el objeto: fch ult modif.
	 *
	 * @param fchUltModif El nuevo objeto: fch ult modif
	 */
	public void setFchUltModif(String fchUltModif) {
		this.fchUltModif = fchUltModif;
	}

	/**
	 * Obtener el objeto: usuario modif.
	 *
	 * @return El objeto: usuario modif
	 */
	public String getUsuarioModif() {
		return usuarioModif;
	}

	/**
	 * Definir el objeto: usuario modif.
	 *
	 * @param usuarioModif El nuevo objeto: usuario modif
	 */
	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	/**
	 * Obtener el objeto: usuario registro.
	 *
	 * @return El objeto: usuario registro
	 */
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	/**
	 * Definir el objeto: usuario registro.
	 *
	 * @param usuarioRegistro El nuevo objeto: usuario registro
	 */
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	/**
	 * Obtener el objeto: status.
	 *
	 * @return El objeto: status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Definir el objeto: status.
	 *
	 * @param status El nuevo objeto: status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Obtener el objeto: num indeval.
	 *
	 * @return El objeto: num indeval
	 */
	public String getNumIndeval() {
		return numIndeval;
	}

	/**
	 * Definir el objeto: num indeval.
	 *
	 * @param numIndeval El nuevo objeto: num indeval
	 */
	public void setNumIndeval(String numIndeval) {
		this.numIndeval = numIndeval;
	}

	/**
	 * Obtener el objeto: id int indeval.
	 *
	 * @return El objeto: id int indeval
	 */
	public String getIdIntIndeval() {
		return idIntIndeval;
	}

	/**
	 * Definir el objeto: id int indeval.
	 *
	 * @param idIntIndeval El nuevo objeto: id int indeval
	 */
	public void setIdIntIndeval(String idIntIndeval) {
		this.idIntIndeval = idIntIndeval;
	}

	/**
	 * Obtener el objeto: fol int indeval.
	 *
	 * @return El objeto: fol int indeval
	 */
	public String getFolIntIndeval() {
		return folIntIndeval;
	}

	/**
	 * Definir el objeto: fol int indeval.
	 *
	 * @param folIntIndeval El nuevo objeto: fol int indeval
	 */
	public void setFolIntIndeval(String folIntIndeval) {
		this.folIntIndeval = folIntIndeval;
	}

	/**
	 * Obtener el objeto: cve swift cor.
	 *
	 * @return El objeto: cve swift cor
	 */
	public String getCveSwiftCor() {
		return cveSwiftCor;
	}

	/**
	 * Definir el objeto: cve swift cor.
	 *
	 * @param cveSwiftCor El nuevo objeto: cve swift cor
	 */
	public void setCveSwiftCor(String cveSwiftCor) {
		this.cveSwiftCor = cveSwiftCor;
	}

	/**
	 * Obtener el objeto: list bean int financiero.
	 *
	 * @return El objeto: list bean int financiero
	 */
	public List<BeanIntFinanciero> getListBeanIntFinanciero() {
		return listBeanIntFinanciero;
	}

	/**
	 * Definir el objeto: list bean int financiero.
	 *
	 * @param listBeanIntFinanciero El nuevo objeto: list bean int financiero
	 */
	public void setListBeanIntFinanciero(
			List<BeanIntFinanciero> listBeanIntFinanciero) {
		this.listBeanIntFinanciero = listBeanIntFinanciero;
	}

	/**
	 * Metodo get que obtiene el total de registros.
	 *
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros.
	 *
	 * @param totalReg            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad codError.
	 *
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Modifica el valor de la propiedad codError.
	 *
	 * @param codError            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad msgError.
	 *
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Modifica el valor de la propiedad msgError.
	 *
	 * @param msgError            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Metodo get que sirve para obtener el valor del tipo de error.
	 *
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * Modifica el valor de la propiedad tipoError.
	 *
	 * @param tipoError            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico.
	 *
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numBanxico.
	 *
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad beanPaginador.
	 *
	 * @return beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad beanPaginador.
	 *
	 * @param beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * Metodo que modifica el valor de la propiedad bandera.
	 *
	 * @param bandera Objeto de tipo @see String
	 */
	public String getBandera() {
		return bandera;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad bandera.
	 *
	 * @return bandera Objeto de tipo @see String
	 */
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	
	
}
