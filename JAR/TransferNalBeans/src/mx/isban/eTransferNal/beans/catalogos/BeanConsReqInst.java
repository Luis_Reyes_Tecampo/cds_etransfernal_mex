/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsReqInst.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015 10:30 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


public class BeanConsReqInst implements Serializable {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8637831584493245028L;
    /**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
    de la paginacion*/
    private BeanPaginador paginador;
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String nombre;
	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
