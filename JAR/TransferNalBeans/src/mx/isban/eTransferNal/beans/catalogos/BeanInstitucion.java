/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 12:31:44 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos de la institucion
 * 
**/

public class BeanInstitucion  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveInterme
	 */
	private String cveInterme;
	/**
	 * Propiedad del tipo String que almacena el valor de nombre
	 */
	private String nombre;
	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveInterme
	 * @return cveInterme Objeto de tipo @see String
	 */
	public String getCveInterme() {
		return cveInterme;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveInterme
	 * @param cveInterme Objeto de tipo @see String
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
