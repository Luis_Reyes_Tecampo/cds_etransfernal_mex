/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:53:43 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.util.List;

/**
 * Clase Bean DTO que se encarga de encapsular los parametros necesarios para Intermediarios 
 * 
 */

public class BeanResTiposIntFinancieros extends BeanResIntFinancieros {

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/** Propiedad del tipo List<listBeanIntFinanciero> que almacena el listado de registros de los intervinientes financieros. */
	private List<String> listTiposInterviniente;
	
	/**
	 * Obtener el objeto: list tipos interviniente.
	 *
	 * @return El objeto: list tipos interviniente
	 */
	public List<String> getListTiposInterviniente() {
		return listTiposInterviniente;
	}

	/**
	 * Definir el objeto: list tipos interviniente.
	 *
	 * @param listTiposInterviniente El nuevo objeto: list tipos interviniente
	 */
	public void setListTiposInterviniente(List<String> listTiposInterviniente) {
		this.listTiposInterviniente = listTiposInterviniente;
	}

}
