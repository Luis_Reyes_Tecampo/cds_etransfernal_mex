/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqInsertBitacora.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:10:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

/**
 * Clase Bean DTO que se encarga de encapsular un registro de la
 * funcionalidad de guardar bitacora .
 */

public class BeanReqEliminarIntFin extends BeanResIntFinancieros {
	
	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
}
