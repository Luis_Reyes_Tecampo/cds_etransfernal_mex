/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCertificadoCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   		By 	      	Company 	 			Description
 * ------- -----------   		----------- ---------- 				----------------------
 * 1.0   07/12/2020 08:23:52 	SNG     	Fabrica SW SNG_Enterprise Creacion
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el catalogo canales id Unico
**/
public class BeanConsultasPayme extends BeanConsultasPayments implements Serializable {
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}	
}