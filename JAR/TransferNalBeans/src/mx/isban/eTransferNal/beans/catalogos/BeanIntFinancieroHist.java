package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 *  Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para el el histotico de Intemediarios
**/
public class BeanIntFinancieroHist  implements Serializable {

	/** La constante serialVersionUID.
	* El proceso de serializaciÃ³n asocia cada clase serializable a un serialVersionUID.
	* Si la clase no especifica un serialVersionUID el proceso de serializacion  
	* calcularÃ¡ un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 1816109659975133488L;
	/** La variable que contiene informacion con respecto a: cveInterme. */
	protected String cveInterme;
	/** La variable que contiene informacion con respecto a: fchModif. */
	protected String fchModif;                
	/** La variable que contiene informacion con respecto a: num persona. */
	protected String tipoModif;    
	/** La variable que contiene informacion con respecto a: tipoModif. */
	protected String usuario;
	/** La variable que contiene informacion con respecto a: tipoIntermeOri. */
	protected String tipoIntermeOri;  
	/**
	* Obtener el objeto: cve interme.
	*
	* @return El objeto: cve interme
	*/
	public String getCveInterme() {
		return cveInterme;
	}
	/**
	* Definir el objeto: cve interme.
	*
	* @param cveInterme El nuevo objeto: cve interme
	*/
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}
	/**
	* Obtener el objeto: fchModif.
	*
	* @return El objeto: fchModif
	*/
	public String getFchModif() {
		return fchModif;
	}
	/**
	* Definir el objeto: tipoModif.
	*
	* @param tipoModif El nuevo objeto: tipoModif
	*/
	public void setFchModif(String fchModif) {
		this.fchModif = fchModif;
	}
	/**
	* Definir el objeto: tipoModif.
	*
	* @return tipoModif El nuevo objeto: tipoModif
	*/
	public String getTipoModif() {
		return tipoModif;
	}
	/**
	* Definir el objeto: tipoModif.
	*
	* @param tipoModif El nuevo objeto: tipoModif
	*/
	public void setTipoModif(String tipoModif) {
		this.tipoModif = tipoModif;
	}
	/**
	* Definir el objeto: usuario.
	*
	* @return cveInterme El nuevo objeto: usuario
	*/
	public String getUsuario() {
		return usuario;
	}
	/**
	* Definir el objeto: usuario.
	*
	* @param usuario El nuevo objeto: usuario
	*/
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	* Definir el objeto: tipoIntermeOri.
	*
	* @return tipoIntermeOri El nuevo objeto: tipoIntermeOri
	*/
	public String getTipoIntermeOri() {
		return tipoIntermeOri;
	}
	/**
	* Definir el objeto: tipoIntermeOri.
	*
	* @param tipoIntermeOri El nuevo objeto: tipoIntermeOri
	*/
	public void setTipoIntermeOri(String tipoIntermeOri) {
		this.tipoIntermeOri = tipoIntermeOri;
	}
}
