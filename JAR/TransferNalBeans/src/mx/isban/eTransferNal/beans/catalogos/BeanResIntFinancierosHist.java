/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResIntFinancierosHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanResIntFinancierosHist.
 * 
 * Contiene los metodos y atributos 
 * para la pantalla de consulta Hostorico de Catalogos de Intemediarios
 * 
 * -Contiene una lista del bean: BeanResIntFinancierosHist
 * -Contiene beanPaginador
 * -Contiene beanFilter
 * -Contiene beanError
 *
 * @author FSW-SNG Enterprie
 * @since 17/11/2020
 */
public class BeanResIntFinancierosHist implements Serializable {

	/** La constante serialVersionUID. 
	 * El proceso de serialización asocia cada clase serializable a un serialVersionUID. 
	 * Si la clase no especifica un serialVersionUID el proceso de serializacion  
	 * calculará un serialVersionUID por defecto, basandose en varios aspectos de la clase.
	*/
	private static final long serialVersionUID = 21156601266677242L;

	/** La variable que contiene informacion con respecto a: bean paginador. */
	private List<BeanIntFinancieroHistorico> intFinancieroHistoricos = new ArrayList<BeanIntFinancieroHistorico>();
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	private BeanPaginador beanPaginador;

	/** La variable que contiene informacion con respecto a: bean filter. */
	private BeanFilter beanFilter;

	/** La variable que contiene informacion con respecto a: bean error. */
	private BeanResBase beanError; 
	
	/** La variable que contiene informacion con respecto a: total reg. */
	@NotNull
	private int totalReg = 0;
	

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}
	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}
	
	/**
	 * Definir el objeto: IntFinancieroHistoricos.
	 *
	 * @param historico El nuevo objeto: IntFinancieroHistoricos
	 */
	public List<BeanIntFinancieroHistorico> getIntFinancieroHistoricos() {
		return new ArrayList<BeanIntFinancieroHistorico>(intFinancieroHistoricos);
	}

	/**
	 * Definir el objeto: IntFinancieroHistoricos.
	 *
	 * @param historico El nuevo objeto: IntFinancieroHistoricos
	 */
	
	public void setIntFinancieroHistoricos(List<BeanIntFinancieroHistorico> intFinancieroHistoricos) {
		this.intFinancieroHistoricos = new ArrayList<BeanIntFinancieroHistorico>(intFinancieroHistoricos);
	}
	
	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	
	public BeanResBase getBeanError() {
		return beanError;
	}
	
	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}
	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	
	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
}
