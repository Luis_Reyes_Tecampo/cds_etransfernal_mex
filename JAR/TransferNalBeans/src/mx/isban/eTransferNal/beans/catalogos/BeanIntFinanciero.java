/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanIntFinanciero.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 16:17:44 CST 2016 Juan Jesus Beltran R  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * Clase la cual se encapsulan los datos de Interviniente Financiero.
 *
 * @author Vector SF
 * @version 1.0
 */
public class BeanIntFinanciero implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3409390230874159310L;

	/** La variable que contiene informacion con respecto a: cve interme. */
	private String cveInterme;

	/** La variable que contiene informacion con respecto a: tipo interme. */
	private String tipoInterme;

	/** La variable que contiene informacion con respecto a: num cecoban. */
	private String numCecoban;

	/** La variable que contiene informacion con respecto a: nombre corto. */
	private String nombreCorto;

	/** La variable que contiene informacion con respecto a: nombre largo. */
	private String nombreLargo;

	/** La variable que contiene informacion con respecto a: num persona. */
	private String numPersona;

	/** La variable que contiene informacion con respecto a: fch alta. */
	private String fchAlta;

	/** La variable que contiene informacion con respecto a: fch baja. */
	private String fchBaja;

	/** La variable que contiene informacion con respecto a: fch ult modif. */
	private String fchUltModif;

	/** La variable que contiene informacion con respecto a: usuario modif. */
	private String usuarioModif;

	/** La variable que contiene informacion con respecto a: usuario registro. */
	private String usuarioRegistro;

	/** La variable que contiene informacion con respecto a: status. */
	private String status;

	/** La variable que contiene informacion con respecto a: num banxico. */
	private String numBanxico;

	/** La variable que contiene informacion con respecto a: num indeval. */
	private String numIndeval;

	/** La variable que contiene informacion con respecto a: id int indeval. */
	private String idIntIndeval;

	/** La variable que contiene informacion con respecto a: fol int indeval. */
	private String folIntIndeval;

	/** La variable que contiene informacion con respecto a: cve swift cor. */
	private String cveSwiftCor;

	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;

	/** La variable que contiene informacion con respecto a: operaSpei. */
	private String operaSpei;

	/** La variable que contiene informacion con respecto a: operaSpid. */
	private String operaSpid;

	/** La variable que contiene informacion con respecto a: selecOpcion. */
	private String selecOpcion;

	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getCveInterme() {
		return cveInterme;
	}

	/**
	 * Definir el objeto: cve interme.
	 *
	 * @param cveInterme El nuevo objeto: cve interme
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}

	/**
	 * Obtener el objeto: tipo interme.
	 *
	 * @return El objeto: tipo interme
	 */
	public String getTipoInterme() {
		return tipoInterme;
	}

	/**
	 * Definir el objeto: tipo interme.
	 *
	 * @param tipoInterme El nuevo objeto: tipo interme
	 */
	public void setTipoInterme(String tipoInterme) {
		this.tipoInterme = tipoInterme;
	}

	/**
	 * Obtener el objeto: num cecoban.
	 *
	 * @return El objeto: num cecoban
	 */
	public String getNumCecoban() {
		return numCecoban;
	}

	/**
	 * Definir el objeto: num cecoban.
	 *
	 * @param numCecoban El nuevo objeto: num cecoban
	 */
	public void setNumCecoban(String numCecoban) {
		this.numCecoban = numCecoban;
	}

	/**
	 * Obtener el objeto: nombre corto.
	 *
	 * @return El objeto: nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * Definir el objeto: nombre corto.
	 *
	 * @param nombreCorto El nuevo objeto: nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * Obtener el objeto: nombre largo.
	 *
	 * @return El objeto: nombre largo
	 */
	public String getNombreLargo() {
		return nombreLargo;
	}

	/**
	 * Definir el objeto: nombre largo.
	 *
	 * @param nombreLargo El nuevo objeto: nombre largo
	 */
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

	/**
	 * Obtener el objeto: num persona.
	 *
	 * @return El objeto: num persona
	 */
	public String getNumPersona() {
		return numPersona;
	}

	/**
	 * Definir el objeto: num persona.
	 *
	 * @param numPersona El nuevo objeto: num persona
	 */
	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}

	/**
	 * Obtener el objeto: fch alta.
	 *
	 * @return El objeto: fch alta
	 */
	public String getFchAlta() {
		return fchAlta;
	}

	/**
	 * Definir el objeto: fch alta.
	 *
	 * @param fchAlta El nuevo objeto: fch alta
	 */
	public void setFchAlta(String fchAlta) {
		this.fchAlta = fchAlta;
	}

	/**
	 * Obtener el objeto: fch baja.
	 *
	 * @return El objeto: fch baja
	 */
	public String getFchBaja() {
		return fchBaja;
	}

	/**
	 * Definir el objeto: fch baja.
	 *
	 * @param fchBaja El nuevo objeto: fch baja
	 */
	public void setFchBaja(String fchBaja) {
		this.fchBaja = fchBaja;
	}

	/**
	 * Obtener el objeto: fch ult modif.
	 *
	 * @return El objeto: fch ult modif
	 */
	public String getFchUltModif() {
		return fchUltModif;
	}

	/**
	 * Definir el objeto: fch ult modif.
	 *
	 * @param fchUltModif El nuevo objeto: fch ult modif
	 */
	public void setFchUltModif(String fchUltModif) {
		this.fchUltModif = fchUltModif;
	}

	/**
	 * Obtener el objeto: usuario modif.
	 *
	 * @return El objeto: usuario modif
	 */
	public String getUsuarioModif() {
		return usuarioModif;
	}

	/**
	 * Definir el objeto: usuario modif.
	 *
	 * @param usuarioModif El nuevo objeto: usuario modif
	 */
	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	/**
	 * Obtener el objeto: usuario registro.
	 *
	 * @return El objeto: usuario registro
	 */
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	/**
	 * Definir el objeto: usuario registro.
	 *
	 * @param usuarioRegistro El nuevo objeto: usuario registro
	 */
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	/**
	 * Obtener el objeto: status.
	 *
	 * @return El objeto: status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Definir el objeto: status.
	 *
	 * @param status El nuevo objeto: status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Obtener el objeto: num banxico.
	 *
	 * @return El objeto: num banxico
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * Definir el objeto: num banxico.
	 *
	 * @param numBanxico El nuevo objeto: num banxico
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * Obtener el objeto: num indeval.
	 *
	 * @return El objeto: num indeval
	 */
	public String getNumIndeval() {
		return numIndeval;
	}

	/**
	 * Definir el objeto: num indeval.
	 *
	 * @param numIndeval El nuevo objeto: num indeval
	 */
	public void setNumIndeval(String numIndeval) {
		this.numIndeval = numIndeval;
	}

	/**
	 * Obtener el objeto: id int indeval.
	 *
	 * @return El objeto: id int indeval
	 */
	public String getIdIntIndeval() {
		return idIntIndeval;
	}

	/**
	 * Definir el objeto: id int indeval.
	 *
	 * @param idIntIndeval El nuevo objeto: id int indeval
	 */
	public void setIdIntIndeval(String idIntIndeval) {
		this.idIntIndeval = idIntIndeval;
	}

	/**
	 * Obtener el objeto: fol int indeval.
	 *
	 * @return El objeto: fol int indeval
	 */
	public String getFolIntIndeval() {
		return folIntIndeval;
	}

	/**
	 * Definir el objeto: fol int indeval.
	 *
	 * @param folIntIndeval El nuevo objeto: fol int indeval
	 */
	public void setFolIntIndeval(String folIntIndeval) {
		this.folIntIndeval = folIntIndeval;
	}

	/**
	 * Obtener el objeto: cve swift cor.
	 *
	 * @return El objeto: cve swift cor
	 */
	public String getCveSwiftCor() {
		return cveSwiftCor;
	}

	/**
	 * Definir el objeto: cve swift cor.
	 *
	 * @param cveSwiftCor El nuevo objeto: cve swift cor
	 */
	public void setCveSwiftCor(String cveSwiftCor) {
		this.cveSwiftCor = cveSwiftCor;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Verificar si operaSpei.
	 *
	 * @return true, si operaSpei
	 */
	public String getOperaSpei() {
		return operaSpei;
	}

	/**
	 * Definir el objeto: operaSpei.
	 *
	 * @param seleccionado El nuevo objeto: operaSpei
	 */
	public void setOperaSpei(String operaSpei) {
		this.operaSpei = operaSpei;
	}

	/**
	 * Verificar si selecOpcion.
	 *
	 * @return true, si selecOpcion
	 */
	public String getSelecOpcion() {
		return selecOpcion;
	}

	/**
	 * Definir el objeto: selecOpcion.
	 *
	 * @param seleccionado El nuevo objeto: selecOpcion
	 */
	public void setSelecOpcion(String selecOpcion) {
		this.selecOpcion = selecOpcion;
	}
	
	/**
	 * Verificar si operaSpid.
	 *
	 * @return true, si operaSpid
	 */
	public String getOperaSpid() {
		return operaSpid;
	}

	/**
	 * Definir el objeto: operaSpid.
	 *
	 * @param seleccionado El nuevo objeto: operaSpid
	 */
	public void setOperaSpid(String operaSpid) {
		this.operaSpid = operaSpid;
	}

	

}
