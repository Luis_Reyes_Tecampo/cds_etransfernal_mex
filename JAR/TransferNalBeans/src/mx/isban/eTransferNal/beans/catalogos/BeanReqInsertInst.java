/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqInsertBitacora.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:10:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular un registro de la
 * funcionalidad de guardar bitacora 
**/

public class BeanReqInsertInst implements Serializable {
	
	/**
	* Constante privada del Serial version
	*/
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numBanxico
	 */
	private String numBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombre
	 */
	private String nombre;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveInterme
	 */
	private String cveInterme;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveUsuario
	 */
	private String cveUsuario;

	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador paginador = null;
	/**
	 * Metodo get que obtiene el valor de la propiedad numBanxico
	 * @return numBanxico Objeto de tipo @see String
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numBanxico
	 * @param numBanxico Objeto de tipo @see String
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombre
	 * @return nombre Objeto de tipo @see String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombre
	 * @param nombre Objeto de tipo @see String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveInterme
	 * @return cveInterme Objeto de tipo @see String
	 */
	public String getCveInterme() {
		return cveInterme;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveInterme
	 * @param cveInterme Objeto de tipo @see String
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveUsuario
	 * @return cveUsuario Objeto de tipo @see String
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveUsuario
	 * @param cveUsuario Objeto de tipo @see String
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	
	
}
