/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMedEntrega.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResMedEntrega extends BeanResBase{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7874433626488934523L;
	/**
	 * Propiedad del tipo BeanMedEntrega que almacena el valor de beanMedioEntrega
	 */
	private List<BeanMedEntrega> beanMedioEntregaList;
	/**
	 * Metodo get que sirve para obtener la propiedad beanMedioEntregaList
	 * @return beanMedioEntregaList Objeto del tipo List<BeanMedEntrega>
	 */
	public List<BeanMedEntrega> getBeanMedioEntregaList() {
		return beanMedioEntregaList;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad beanMedioEntregaList
	 * @param beanMedioEntregaList del tipo List<BeanMedEntrega>
	 */
	public void setBeanMedioEntregaList(List<BeanMedEntrega> beanMedioEntregaList) {
		this.beanMedioEntregaList = beanMedioEntregaList;
	}

	

	
	
	

}
