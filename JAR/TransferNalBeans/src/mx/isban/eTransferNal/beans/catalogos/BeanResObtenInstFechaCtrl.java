/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResObtenInstFechaCtrl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResObtenInstFechaCtrl extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 6826948517921136416L;

	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de institucion
	 */
	private String institucion;
	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad institucion
	 * @return institucion Objeto del tipo String
	 */
	public String getInstitucion() {
		return institucion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad institucion
	 * @param institucion del tipo String
	 */
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	
	
}
