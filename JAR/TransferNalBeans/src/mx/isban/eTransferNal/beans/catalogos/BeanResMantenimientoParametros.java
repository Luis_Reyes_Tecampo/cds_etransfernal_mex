/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class BeanResMantenimientoParametros.
 */
public class BeanResMantenimientoParametros extends BeanReqMantenimientoParametros implements Serializable{

	/** Variable de serializacion. */
	private static final long serialVersionUID = 8959884003634961638L;
	
	/** The nombre archivo. */
	private String nombreArchivo;

	/** The cod error. */
	private String codError;
	
	/** The msg error. */
	private String msgError;
	
	/** The tipo error. */
	private String tipoError;
	
	/** The total reg. */
	private int totalReg;
	
	/** The lista parametros. */
	private List<BeanMantenimientoParametros> listaParametros=new ArrayList<BeanMantenimientoParametros>();

	/** The datos parametro. */
	private List<BeanMantenimientoParametros> datosParametro=new ArrayList<BeanMantenimientoParametros>();
	
	/** The opciones tipo param. */
	private List<BeanMantenimientoParametros> opcionesTipoParam=new ArrayList<BeanMantenimientoParametros>();
	
	/** The opciones tipo dato. */
	private List<BeanMantenimientoParametros> opcionesTipoDato=new ArrayList<BeanMantenimientoParametros>();

	/**
	 * Gets the nombre archivo.
	 *
	 * @return the nombre archivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Sets the nombre archivo.
	 *
	 * @param nombreArchivo the new nombre archivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Gets the lista parametros.
	 *
	 * @return the lista parametros
	 */
	public List<BeanMantenimientoParametros> getListaParametros() {
		return listaParametros;
	}

	/**
	 * Sets the lista parametros.
	 *
	 * @param listaParametros the new lista parametros
	 */
	public void setListaParametros(List<BeanMantenimientoParametros> listaParametros) {
		this.listaParametros = listaParametros;
	}

	/**
	 * Gets the datos parametro.
	 *
	 * @return the datos parametro
	 */
	public List<BeanMantenimientoParametros> getDatosParametro() {
		return datosParametro;
	}

	/**
	 * Sets the datos parametro.
	 *
	 * @param datosParametro the new datos parametro
	 */
	public void setDatosParametro(List<BeanMantenimientoParametros> datosParametro) {
		this.datosParametro = datosParametro;
	}

	/**
	 * Gets the opciones tipo param.
	 *
	 * @return the opciones tipo param
	 */
	public List<BeanMantenimientoParametros> getOpcionesTipoParam() {
		return opcionesTipoParam;
	}

	/**
	 * Sets the opciones tipo param.
	 *
	 * @param opcionesTipoParam the new opciones tipo param
	 */
	public void setOpcionesTipoParam(
			List<BeanMantenimientoParametros> opcionesTipoParam) {
		this.opcionesTipoParam = opcionesTipoParam;
	}

	/**
	 * Gets the opciones tipo dato.
	 *
	 * @return the opciones tipo dato
	 */
	public List<BeanMantenimientoParametros> getOpcionesTipoDato() {
		return opcionesTipoDato;
	}

	/**
	 * Sets the opciones tipo dato.
	 *
	 * @param opcionesTipoDato the new opciones tipo dato
	 */
	public void setOpcionesTipoDato(
			List<BeanMantenimientoParametros> opcionesTipoDato) {
		this.opcionesTipoDato = opcionesTipoDato;
	}

	/**
	 * Gets the cod error.
	 *
	 * @return the cod error
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Sets the cod error.
	 *
	 * @param codError the new cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Gets the tipo error.
	 *
	 * @return the tipo error
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * Sets the tipo error.
	 *
	 * @param tipoError the new tipo error
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 * Gets the msg error.
	 *
	 * @return the msg error
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Sets the msg error.
	 *
	 * @param msgError the new msg error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Gets the total reg.
	 *
	 * @return the total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Sets the total reg.
	 *
	 * @param totalReg the new total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
}