package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanTotalesGrafico implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5077860845670054283L;
	/**
	 * Propiedad del tipo String que almacena el valor de montoTotal
	 */
	private String montoTotal;
	
	/**
	 * Propiedad del tipo String que almacena el valor de totalOp
	 */
	private String totalOp;

	/**
	 * Metodo get que sirve para obtener la propiedad montoTotal
	 * @return montoTotal Objeto del tipo String
	 */
	public String getMontoTotal() {
		return montoTotal;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad montoTotal
	 * @param montoTotal del tipo String
	 */
	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalOp
	 * @return totalOp Objeto del tipo String
	 */
	public String getTotalOp() {
		return totalOp;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalOp
	 * @param totalOp del tipo String
	 */
	public void setTotalOp(String totalOp) {
		this.totalOp = totalOp;
	}
	
	
}
