/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMedioEnt.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanMedioEnt implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -4299501402512380343L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMedioEnt
	 */
	private String cveMedioEnt;
	
	/**
	 * Propiedad del tipo String que almacena el valor de descripcion
	 */
	private String descripcion;

	/**
	 * Metodo get que sirve para obtener la propiedad cveMedioEnt
	 * @return cveMedioEnt Objeto del tipo String
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveMedioEnt
	 * @param cveMedioEnt del tipo String
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad descripcion
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad descripcion
	 * @param descripcion del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
