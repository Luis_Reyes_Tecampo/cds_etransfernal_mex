/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResReportePago.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


public class BeanResReportePago extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1901935200646986237L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	/**
	 * Propiedad del tipo List<BeanReportePago> que almacena el valor de reportePagoList
	 */
	private List<BeanReportePago> reportePagoList;
	
	/**
	 * Propiedad del tipo List<BeanTipoPago> que almacena el valor de tipoPagoList
	 */
	private List<BeanTipoPago> tipoPagoList;
	/**
	 * Propiedad del tipo List<BeanMedioEnt> que almacena el valor de medioEntList
	 */
	private List<BeanMedioEnt> medioEntList;
	
	/**
	 * Propiedad del tipo List<BeanEstatus> que almacena el valor de estatusList
	 */
	private List<BeanEstatus> estatusList;
	
	/**
	 * Propiedad del tipo List<String> que almacena el valor de difMinutosList
	 */
	private List<String> difMinutosList;
	
	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador paginador;
	
	/**
	 * Propiedad del tipo String que almacena el valor de totalReg
	 */
	private String totalReg;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nomArchivo
	 */
	private String nomArchivo;
	
	/**
	 * Propiedad del tipo List<BeanRangoTiempo> que almacena el valor de listRangoTiempo
	 */
	private List<BeanRangoTiempo> listRangoTiempo;
	

	/**
	 * Metodo get que sirve para obtener la propiedad reportePagoList
	 * @return reportePagoList Objeto del tipo List<BeanReportePago>
	 */
	public List<BeanReportePago> getReportePagoList() {
		return reportePagoList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad reportePagoList
	 * @param reportePagoList del tipo List<BeanReportePago>
	 */
	public void setReportePagoList(List<BeanReportePago> reportePagoList) {
		this.reportePagoList = reportePagoList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoPagoList
	 * @return tipoPagoList Objeto del tipo List<BeanTipoPago>
	 */
	public List<BeanTipoPago> getTipoPagoList() {
		return tipoPagoList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPagoList
	 * @param tipoPagoList del tipo List<BeanTipoPago>
	 */
	public void setTipoPagoList(List<BeanTipoPago> tipoPagoList) {
		this.tipoPagoList = tipoPagoList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad medioEntList
	 * @return medioEntList Objeto del tipo List<BeanMedioEnt>
	 */
	public List<BeanMedioEnt> getMedioEntList() {
		return medioEntList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad medioEntList
	 * @param medioEntList del tipo List<BeanMedioEnt>
	 */
	public void setMedioEntList(List<BeanMedioEnt> medioEntList) {
		this.medioEntList = medioEntList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad estatusList
	 * @return estatusList Objeto del tipo List<BeanEstatus>
	 */
	public List<BeanEstatus> getEstatusList() {
		return estatusList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad estatusList
	 * @param estatusList del tipo List<BeanEstatus>
	 */
	public void setEstatusList(List<BeanEstatus> estatusList) {
		this.estatusList = estatusList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad difMinutosList
	 * @return difMinutosList Objeto del tipo List<String>
	 */
	public List<String> getDifMinutosList() {
		return difMinutosList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad difMinutosList
	 * @param difMinutosList del tipo List<String>
	 */
	public void setDifMinutosList(List<String> difMinutosList) {
		this.difMinutosList = difMinutosList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad paginador
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad paginador
	 * @param paginador del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo String
	 */
	public String getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg del tipo String
	 */
	public void setTotalReg(String totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad nomArchivo
	 * @return nomArchivo Objeto del tipo String
	 */
	public String getNomArchivo() {
		return nomArchivo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad nomArchivo
	 * @param nomArchivo del tipo String
	 */
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad listRangoTiempo
	 * @return listRangoTiempo Objeto del tipo List<BeanRangoTiempo>
	 */
	public List<BeanRangoTiempo> getListRangoTiempo() {
		return listRangoTiempo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listRangoTiempo
	 * @param listRangoTiempo del tipo List<BeanRangoTiempo>
	 */
	public void setListRangoTiempo(List<BeanRangoTiempo> listRangoTiempo) {
		this.listRangoTiempo = listRangoTiempo;
	}


	
	

}
