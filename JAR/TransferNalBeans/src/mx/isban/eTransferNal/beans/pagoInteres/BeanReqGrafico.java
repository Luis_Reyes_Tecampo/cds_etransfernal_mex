/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqGrafico.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos de busqueda
**/

public class BeanReqGrafico implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -7926822100541889341L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de horaInicio
	 */
	private String horaInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de minutosInicio
	 */
	private String minutoInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de fin
	 */
	private String horaFin;
	/**
	 * Propiedad del tipo String que almacena el valor de minutoFin
	 */
	private String minutoFin;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoPago
	 */
	private String tipoPago;
	
	/**
	 * Metodo get que sirve para obtener la propiedad horaInicio
	 * @return horaInicio Objeto del tipo String
	 */
	public String getHoraInicio() {
		return horaInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaInicio
	 * @param horaInicio del tipo String
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad minutoInicio
	 * @return minutoInicio Objeto del tipo String
	 */
	public String getMinutoInicio() {
		return minutoInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad minutoInicio
	 * @param minutoInicio del tipo String
	 */
	public void setMinutoInicio(String minutoInicio) {
		this.minutoInicio = minutoInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad horaFin
	 * @return horaFin Objeto del tipo String
	 */
	public String getHoraFin() {
		return horaFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaFin
	 * @param horaFin del tipo String
	 */
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad minutoFin
	 * @return minutoFin Objeto del tipo String
	 */
	public String getMinutoFin() {
		return minutoFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad minutoFin
	 * @param minutoFin del tipo String
	 */
	public void setMinutoFin(String minutoFin) {
		this.minutoFin = minutoFin;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoPago
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPago
	 * @param tipoPago del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	

}
