package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanTipoPago implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5728317499974691883L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveTipoPago
	 */
	private String cveTipoPago;
	/**
	 * Propiedad del tipo String que almacena el valor de descripcion
	 */
	private String descripcion;
	/**
	 * Metodo get que sirve para obtener la propiedad cveTipoPago
	 * @return cveTipoPago Objeto del tipo String
	 */
	public String getCveTipoPago() {
		return cveTipoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveTipoPago
	 * @param cveTipoPago del tipo String
	 */
	public void setCveTipoPago(String cveTipoPago) {
		this.cveTipoPago = cveTipoPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad descripcion
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad descripcion
	 * @param descripcion del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
}
