package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanHorario implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6155809765341139139L;
	/**
	 * Propiedad del tipo String que almacena el valor de horaInicio
	 */
	private String horaInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de horaFin
	 */
	private String horaFin;
	
	/**
	 * Propiedad del tipo String que almacena el valor de minutoInicio
	 */
	private String minutoInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de minutoFin
	 */
	private String minutoFin;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fechaInicio
	 */
	private String fechaInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaFin
	 */
	private String fechaFin;
	/**
	 * Metodo get que sirve para obtener la propiedad horaInicio
	 * @return horaInicio Objeto del tipo String
	 */
	public String getHoraInicio() {
		return horaInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaInicio
	 * @param horaInicio del tipo String
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad horaFin
	 * @return horaFin Objeto del tipo String
	 */
	public String getHoraFin() {
		return horaFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaFin
	 * @param horaFin del tipo String
	 */
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad minutoInicio
	 * @return minutoInicio Objeto del tipo String
	 */
	public String getMinutoInicio() {
		return minutoInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad minutoInicio
	 * @param minutoInicio del tipo String
	 */
	public void setMinutoInicio(String minutoInicio) {
		this.minutoInicio = minutoInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad minutoFin
	 * @return minutoFin Objeto del tipo String
	 */
	public String getMinutoFin() {
		return minutoFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad minutoFin
	 * @param minutoFin del tipo String
	 */
	public void setMinutoFin(String minutoFin) {
		this.minutoFin = minutoFin;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fechaInicio
	 * @return fechaInicio Objeto del tipo String
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaInicio
	 * @param fechaInicio del tipo String
	 */
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fechaFin
	 * @return fechaFin Objeto del tipo String
	 */
	public String getFechaFin() {
		return fechaFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaFin
	 * @param fechaFin del tipo String
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
}
