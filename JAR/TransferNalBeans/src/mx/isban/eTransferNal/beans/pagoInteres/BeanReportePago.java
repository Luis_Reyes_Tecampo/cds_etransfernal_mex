/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReportePago.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanReportePago implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8318671271250291081L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de refPago
	 */
	private String refPago;
	/**
	 * Propiedad del tipo String que almacena el valor de refInteres
	 */
	private String refInteres;
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreo;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoPago
	 */
	private String tipoPago;
	/**
	 * Propiedad del tipo String que almacena el valor de medioEnt
	 */
	private String medioEnt;
	/**
	 * Propiedad del tipo String que almacena el valor de importeTransf
	 */
	private String importeTransf;
	/**
	 * Propiedad del tipo String que almacena el valor de importeInteres
	 */
	private String importeInteres;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaInicio
	 */
	private String fechaInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaFin
	 */
	private String fechaFin;
	/**
	 * Propiedad del tipo String que almacena el valor de diferencia
	 */
	private String diferencia;
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	
	/**
	 * Metodo get que sirve para obtener la propiedad refPago
	 * @return refPago Objeto del tipo String
	 */
	public String getRefPago() {
		return refPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad refPago
	 * @param refPago del tipo String
	 */
	public void setRefPago(String refPago) {
		this.refPago = refPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad refInteres
	 * @return refInteres Objeto del tipo String
	 */
	public String getRefInteres() {
		return refInteres;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad refInteres
	 * @param refInteres del tipo String
	 */
	public void setRefInteres(String refInteres) {
		this.refInteres = refInteres;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveRastreo
	 * @return cveRastreo Objeto del tipo String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveRastreo
	 * @param cveRastreo del tipo String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoPago
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPago
	 * @param tipoPago del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad medioEnt
	 * @return medioEnt Objeto del tipo String
	 */
	public String getMedioEnt() {
		return medioEnt;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad medioEnt
	 * @param medioEnt del tipo String
	 */
	public void setMedioEnt(String medioEnt) {
		this.medioEnt = medioEnt;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad importeTransf
	 * @return importeTransf Objeto del tipo String
	 */
	public String getImporteTransf() {
		return importeTransf;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad importeTransf
	 * @param importeTransf del tipo String
	 */
	public void setImporteTransf(String importeTransf) {
		this.importeTransf = importeTransf;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad importeInteres
	 * @return importeInteres Objeto del tipo String
	 */
	public String getImporteInteres() {
		return importeInteres;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad importeInteres
	 * @param importeInteres del tipo String
	 */
	public void setImporteInteres(String importeInteres) {
		this.importeInteres = importeInteres;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fechaInicio
	 * @return fechaInicio Objeto del tipo String
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaInicio
	 * @param fechaInicio del tipo String
	 */
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fechaFin
	 * @return fechaFin Objeto del tipo String
	 */
	public String getFechaFin() {
		return fechaFin;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaFin
	 * @param fechaFin del tipo String
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad diferencia
	 * @return diferencia Objeto del tipo String
	 */
	public String getDiferencia() {
		return diferencia;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad diferencia
	 * @param diferencia del tipo String
	 */
	public void setDiferencia(String diferencia) {
		this.diferencia = diferencia;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad estatus
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad estatus
	 * @param estatus del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

}
