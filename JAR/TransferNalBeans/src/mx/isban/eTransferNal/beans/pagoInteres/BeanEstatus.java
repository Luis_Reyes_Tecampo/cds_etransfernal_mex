/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanEstatus.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanEstatus implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 2997007990430458330L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveEstatus
	 */
	private String cveEstatus;
	/**
	 * Propiedad del tipo String que almacena el valor de descripcion
	 */
	private String descripcion;
	/**
	 * Metodo get que sirve para obtener la propiedad cveEstatus
	 * @return cveEstatus Objeto del tipo String
	 */
	public String getCveEstatus() {
		return cveEstatus;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveEstatus
	 * @param cveEstatus del tipo String
	 */
	public void setCveEstatus(String cveEstatus) {
		this.cveEstatus = cveEstatus;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad descripcion
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad descripcion
	 * @param descripcion del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
