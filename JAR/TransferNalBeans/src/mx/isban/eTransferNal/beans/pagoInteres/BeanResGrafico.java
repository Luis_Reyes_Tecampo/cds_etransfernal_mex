/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGrafico.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResGrafico extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8609468426506215851L;
	
	/**
	 * Propiedad del tipo BeanTotalesGrafico que almacena el valor de totales
	 */
	private BeanTotalesGrafico totales;
	
	/**
	 * Propiedad del tipo Object[] que almacena el valor de devolucionesEnvMov
	 */
	private Object datos[];
		
	/**
	 * Propiedad del tipo String que almacena el valor de leyenda
	 */
	private String leyenda;
	
	/**
	 * Propiedad del tipo int que almacena el valor de rango en minutos
	 */
	private int rango;
	
	/**
	 * Propiedad del tipo String que almacena el valor de leyendaMonto
	 */
	private String leyendaMonto;
	/**
	 * Propiedad del tipo String que almacena el valor de leyendaTotal
	 */
	private String leyendaTotal;
	
	/**
	 * Propiedad del tipo String que almacena el valor de leyendaTipoTransfe
	 */
	private String leyendaTipoTransfe;
	
	/**
	 * Propiedad del tipo List<BeanTipoPago> que almacena el valor de tipoPagoList
	 */
	private List<BeanTipoPago> tipoPagoList;

	/**
	 * Metodo get que sirve para obtener la propiedad totales
	 * @return totales Objeto del tipo BeanTotalesGrafico
	 */
	public BeanTotalesGrafico getTotales() {
		return totales;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totales
	 * @param totales del tipo BeanTotalesGrafico
	 */
	public void setTotales(BeanTotalesGrafico totales) {
		this.totales = totales;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad leyenda
	 * @return leyenda Objeto del tipo String
	 */
	public String getLeyenda() {
		return leyenda;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad leyenda
	 * @param leyenda del tipo String
	 */
	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoPagoList
	 * @return tipoPagoList Objeto del tipo List<BeanTipoPago>
	 */
	public List<BeanTipoPago> getTipoPagoList() {
		return tipoPagoList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPagoList
	 * @param tipoPagoList del tipo List<BeanTipoPago>
	 */
	public void setTipoPagoList(List<BeanTipoPago> tipoPagoList) {
		this.tipoPagoList = tipoPagoList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datos
	 * @return datos Objeto del tipo Object[]
	 */
	public Object[] getDatos() {
		Object obj[] = null;
		if(datos!=null){
			obj = datos.clone();
		}
		return obj;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datos
	 * @param datos del tipo Object[]
	 */
	public void setDatos(Object[] datos) {
		this.datos = datos.clone();
	}

	/**
	 * Metodo get que sirve para obtener la propiedad leyendaMonto
	 * @return leyendaMonto Objeto del tipo String
	 */
	public String getLeyendaMonto() {
		return leyendaMonto;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad leyendaMonto
	 * @param leyendaMonto del tipo String
	 */
	public void setLeyendaMonto(String leyendaMonto) {
		this.leyendaMonto = leyendaMonto;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad leyendaTotal
	 * @return leyendaTotal Objeto del tipo String
	 */
	public String getLeyendaTotal() {
		return leyendaTotal;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad leyendaTotal
	 * @param leyendaTotal del tipo String
	 */
	public void setLeyendaTotal(String leyendaTotal) {
		this.leyendaTotal = leyendaTotal;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad leyendaTipoTransfe
	 * @return leyendaTipoTransfe Objeto del tipo String
	 */
	public String getLeyendaTipoTransfe() {
		return leyendaTipoTransfe;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad leyendaTipoTransfe
	 * @param leyendaTipoTransfe del tipo String
	 */
	public void setLeyendaTipoTransfe(String leyendaTipoTransfe) {
		this.leyendaTipoTransfe = leyendaTipoTransfe;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad rango
	 * @return rango Objeto del tipo int
	 */
	public int getRango() {
		return rango;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad rango
	 * @param rango del tipo int
	 */
	public void setRango(int rango) {
		this.rango = rango;
	}

	
	
	

}
