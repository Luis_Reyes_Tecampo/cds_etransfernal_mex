package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;

public class BeanRangoTiempo implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2454202913216386001L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de epoca
	 */
	private String epoca;
	/**
	 * Propiedad del tipo int que almacena el valor de rango
	 */
	private int rango;

	/**
	 * Metodo get que sirve para obtener la propiedad rango
	 * @return rango Objeto del tipo int
	 */
	public int getRango() {
		return rango;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad rango
	 * @param rango del tipo int
	 */
	public void setRango(int rango) {
		this.rango = rango;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad epoca
	 * @return epoca Objeto del tipo String
	 */
	public String getEpoca() {
		return epoca;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad epoca
	 * @param epoca del tipo String
	 */
	public void setEpoca(String epoca) {
		this.epoca = epoca;
	}
	
}
