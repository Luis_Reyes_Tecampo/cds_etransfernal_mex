/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqReportePago.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

public class BeanReqReportePago implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7029632483370536046L;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoPago
	 */
	private String tipoPago;
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	
	/**
	 * Propiedad del tipo String que almacena el valor de medioEnt
	 */
	private String medioEnt;

	/**
	 * Propiedad del tipo String que almacena el valor de importeInicio
	 */
	private String importeInicio;
	/**
	 * Propiedad del tipo String que almacena el valor de importeFin
	 */
	private String importeFin;
	
	/**
	 * Propiedad del tipo String que almacena el valor de sigDifMinutos
	 */
	private String sigDifMinutos;
	/**
	 * Propiedad del tipo String que almacena el valor de diferenciaMinutos
	 */
	private String difMinutos;
	
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de totalReg
	 */
	private String totalReg;
	
	/**
	 * Propiedad del tipo int que almacena el valor de horaRango
	 */
	private int horaRango;
	/**
	 * Propiedad del tipo int que almacena el valor de diaRango
	 */
	private int diaRango;
	
	/** Propiedad del tipo paginador que almacena la paginador */
	private BeanPaginador paginador;
	
	/**
	 * Propiedad del tipo List<BeanReportePago> que almacena el valor de beanReportePagoList
	 */
	private List<BeanReportePago> beanReportePagoList;
	
	/**
	 * Propiedad del tipo BeanHorario que almacena el valor de horario
	 */
	private BeanHorario horario; 

	/**
	 * Metodo get que sirve para obtener la propiedad tipoPago
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPago
	 * @param tipoPago del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad estatus
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad estatus
	 * @param estatus del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad medioEnt
	 * @return medioEnt Objeto del tipo String
	 */
	public String getMedioEnt() {
		return medioEnt;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad medioEnt
	 * @param medioEnt del tipo String
	 */
	public void setMedioEnt(String medioEnt) {
		this.medioEnt = medioEnt;
	}

	

	/**
	 * Metodo get que sirve para obtener la propiedad importeInicio
	 * @return importeInicio Objeto del tipo String
	 */
	public String getImporteInicio() {
		return importeInicio;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad importeInicio
	 * @param importeInicio del tipo String
	 */
	public void setImporteInicio(String importeInicio) {
		this.importeInicio = importeInicio;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad importeFin
	 * @return importeFin Objeto del tipo String
	 */
	public String getImporteFin() {
		return importeFin;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad importeFin
	 * @param importeFin del tipo String
	 */
	public void setImporteFin(String importeFin) {
		this.importeFin = importeFin;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad sigDifMinutos
	 * @return sigDifMinutos Objeto del tipo String
	 */
	public String getSigDifMinutos() {
		return sigDifMinutos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad sigDifMinutos
	 * @param sigDifMinutos del tipo String
	 */
	public void setSigDifMinutos(String sigDifMinutos) {
		this.sigDifMinutos = sigDifMinutos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad difMinutos
	 * @return difMinutos Objeto del tipo String
	 */
	public String getDifMinutos() {
		return difMinutos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad difMinutos
	 * @param difMinutos del tipo String
	 */
	public void setDifMinutos(String difMinutos) {
		this.difMinutos = difMinutos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad opcion
	 * @return opcion Objeto del tipo String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad opcion
	 * @param opcion del tipo String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad beanReportePagoList
	 * @return beanReportePagoList Objeto del tipo List<BeanReportePago>
	 */
	public List<BeanReportePago> getBeanReportePagoList() {
		return beanReportePagoList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanReportePagoList
	 * @param beanReportePagoList del tipo List<BeanReportePago>
	 */
	public void setBeanReportePagoList(List<BeanReportePago> beanReportePagoList) {
		this.beanReportePagoList = beanReportePagoList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad paginador
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad paginador
	 * @param paginador del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo String
	 */
	public String getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg del tipo String
	 */
	public void setTotalReg(String totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad horario
	 * @return horario Objeto del tipo BeanHorario
	 */
	public BeanHorario getHorario() {
		return horario;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad horario
	 * @param horario del tipo BeanHorario
	 */
	public void setHorario(BeanHorario horario) {
		this.horario = horario;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad horaRango
	 * @return horaRango Objeto del tipo int
	 */
	public int getHoraRango() {
		return horaRango;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad horaRango
	 * @param horaRango del tipo int
	 */
	public void setHoraRango(int horaRango) {
		this.horaRango = horaRango;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad diaRango
	 * @return diaRango Objeto del tipo int
	 */
	public int getDiaRango() {
		return diaRango;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad diaRango
	 * @param diaRango del tipo int
	 */
	public void setDiaRango(int diaRango) {
		this.diaRango = diaRango;
	}




	
	




}
