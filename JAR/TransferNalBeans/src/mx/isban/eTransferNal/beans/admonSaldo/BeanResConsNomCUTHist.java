/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsNomCUTDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/11/2016 12:42:00 IDS		Creacion
 *
 */
package mx.isban.eTransferNal.beans.admonSaldo;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsNomCUTHist extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8874501519881687332L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveInstitucion
	 */
	private String cveInstitucion;
	/**
	 * Propiedad del tipo List<BeanConsNomCUT> que almacena el valor de listBeanConsNomCUT
	 */
	private List<BeanConsNomCUT> listBeanConsNomCUT;

	/**
	 * Metodo get que sirve para obtener la propiedad listBeanConsNomCUT
	 * @return listBeanConsNomCUT Objeto del tipo List<BeanConsNomCUT>
	 */
	public List<BeanConsNomCUT> getListBeanConsNomCUT() {
		return listBeanConsNomCUT;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanConsNomCUT
	 * @param listBeanConsNomCUT del tipo List<BeanConsNomCUT>
	 */
	public void setListBeanConsNomCUT(List<BeanConsNomCUT> listBeanConsNomCUT) {
		this.listBeanConsNomCUT = listBeanConsNomCUT;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cveInstitucion
	 * @return cveInstitucion Objeto del tipo String
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveInstitucion
	 * @param cveInstitucion del tipo String
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}
	
	

	
}
