/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsNomCUT.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   19/11/2016 12:42:00 IDS		Creacion
 *
 */
package mx.isban.eTransferNal.beans.admonSaldo;

import java.io.Serializable;

public class BeanConsNomCUT implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8874501519881687332L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;

	
	/**
	 * Propiedad del tipo String que almacena el valor de cantidadPagos
	 */
	private String cantidadPagos;
	
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private String monto;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cantidadPagosPre
	 */
	private String cantidadPagosPre;
	
	/**
	 * Propiedad del tipo String que almacena el valor de montoPre
	 */
	private String montoPre;

	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cantidadPagosPre
	 * @return cantidadPagosPre Objeto del tipo String
	 */
	public String getCantidadPagosPre() {
		return cantidadPagosPre;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cantidadPagosPre
	 * @param cantidadPagosPre del tipo String
	 */
	public void setCantidadPagosPre(String cantidadPagosPre) {
		this.cantidadPagosPre = cantidadPagosPre;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad montoPre
	 * @return montoPre Objeto del tipo String
	 */
	public String getMontoPre() {
		return montoPre;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad montoPre
	 * @param montoPre del tipo String
	 */
	public void setMontoPre(String montoPre) {
		this.montoPre = montoPre;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cantidadPagos
	 * @return cantidadPagos Objeto del tipo String
	 */
	public String getCantidadPagos() {
		return cantidadPagos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cantidadPagos
	 * @param cantidadPagos del tipo String
	 */
	public void setCantidadPagos(String cantidadPagos) {
		this.cantidadPagos = cantidadPagos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo String
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	

	
}
