package mx.isban.eTransferNal.beans.admonSaldo;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsNomCUTDia extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5943667213086364595L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveInstitucion
	 */
	private String cveInstitucion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cantidadPagos
	 */
	private String cantidadPagos ="0";
	
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private String monto="0";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cantidadPagosPre
	 */
	private String cantidadPagosPre="0";
	
	/**
	 * Propiedad del tipo String que almacena el valor de montoPre
	 */
	private String montoPre="0";

	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cveInstitucion
	 * @return cveInstitucion Objeto del tipo String
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveInstitucion
	 * @param cveInstitucion del tipo String
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cantidadPagos
	 * @return cantidadPagos Objeto del tipo String
	 */
	public String getCantidadPagos() {
		return cantidadPagos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cantidadPagos
	 * @param cantidadPagos del tipo String
	 */
	public void setCantidadPagos(String cantidadPagos) {
		this.cantidadPagos = cantidadPagos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo String
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cantidadPagosPre
	 * @return cantidadPagosPre Objeto del tipo String
	 */
	public String getCantidadPagosPre() {
		return cantidadPagosPre;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cantidadPagosPre
	 * @param cantidadPagosPre del tipo String
	 */
	public void setCantidadPagosPre(String cantidadPagosPre) {
		this.cantidadPagosPre = cantidadPagosPre;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad montoPre
	 * @return montoPre Objeto del tipo String
	 */
	public String getMontoPre() {
		return montoPre;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad montoPre
	 * @param montoPre del tipo String
	 */
	public void setMontoPre(String montoPre) {
		this.montoPre = montoPre;
	}
	
	
}
