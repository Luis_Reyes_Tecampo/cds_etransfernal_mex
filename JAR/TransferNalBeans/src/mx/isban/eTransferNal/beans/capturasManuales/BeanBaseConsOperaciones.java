package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * Clase base para modulo consulta y aprobacion de operaciones
 */
public class BeanBaseConsOperaciones extends BeanBaseConsOperacionesA implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8866922570952835213L;

	/**
	 * Propiedad del tipo @see String que almacena el usuario de captura
	 */
	private String usuarioCaptura;
	
	/**
	 * Propiedad del tipo @see String que almacena el usuario que autoriza
	 */
	private String usuarioAutoriza;
	
	/**
	 * @return the usuarioCaptura
	 */
	public String getUsuarioCaptura() {
		return usuarioCaptura;
	}

	/**
	 * @param usuarioCaptura the usuarioCaptura to set
	 */
	public void setUsuarioCaptura(String usuarioCaptura) {
		this.usuarioCaptura = usuarioCaptura;
	}

	/**
	 * @return the usuarioAutoriza
	 */
	public String getUsuarioAutoriza() {
		return usuarioAutoriza;
	}

	/**
	 * @param usuarioAutoriza the usuarioAutoriza to set
	 */
	public void setUsuarioAutoriza(String usuarioAutoriza) {
		this.usuarioAutoriza = usuarioAutoriza;
	}
}
