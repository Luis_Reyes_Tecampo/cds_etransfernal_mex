/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   30/03/2017 09:55:00 AM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * The Class BeanReqOpePendAutDetalle.
 */
public class BeanReqOpePendAutDetalle implements Serializable {

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -7166411708737731798L;

	/** The template. */
	private String template;
	
	/** The fecha captura. */
	private String fechaCaptura;
	
	/** The cve template. */
	private String cveTemplate;
	
	/** The folio. */
	private String folio;

	/** The paginador. */
	private BeanPaginador paginador = new BeanPaginador();
	
	/** The usr admin. */
	private String usrAdmin;
	
	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 *
	 * @param template the new template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Gets the fecha captura.
	 *
	 * @return the fecha captura
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}

	/**
	 * Sets the fecha captura.
	 *
	 * @param fechaCaptura the new fecha captura
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 * Gets the cve template.
	 *
	 * @return the cve template
	 */
	public String getCveTemplate() {
		return cveTemplate;
	}

	/**
	 * Sets the cve template.
	 *
	 * @param cveTemplate the new cve template
	 */
	public void setCveTemplate(String cveTemplate) {
		this.cveTemplate = cveTemplate;
	}

	/**
	 * Gets the folio.
	 *
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * Sets the folio.
	 *
	 * @param folio the new folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * Gets the paginador.
	 *
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Sets the paginador.
	 *
	 * @param paginador the new paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Gets the usr admin.
	 *
	 * @return the usr admin
	 */
	public String getUsrAdmin() {
		return usrAdmin;
	}

	/**
	 * Sets the usr admin.
	 *
	 * @param usrAdmin the new usr admin
	 */
	public void setUsrAdmin(String usrAdmin) {
		this.usrAdmin = usrAdmin;
	}
}
