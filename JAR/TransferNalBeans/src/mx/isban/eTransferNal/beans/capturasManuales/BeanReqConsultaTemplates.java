/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanReqConsultaTemplates.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanReqConsultaTemplates extends BeanBaseReqResTemplate implements Serializable {

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = 6129555729714239190L;
		
	
	/*DESC. Atributos seucndarios. Eliminacion*/
	/** Propiedad de tipo  que List<BeanTemplate> contiene informacion con respecto a: listaTemplates */
	private List<BeanTemplate> listaTemplates = new ArrayList<BeanTemplate>();
	
	/**
	 * Metodo que permite obtener la lista de templates disponibles para el componente de consulta
	 * @return lista templates
	 */
	public List<BeanTemplate> getListaTemplates() {
		return listaTemplates;
	}


	/**
	 * Metodo que permite establecer la lista de templates
	 * @param listaTemplates disponibles
	 */
	public void setListaTemplates(List<BeanTemplate> listaTemplates) {
		this.listaTemplates = listaTemplates;
	}
	
}
