/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCampoLayout.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanCampoLayout implements Serializable  {

	/** Constante privada del Serial Version */
	private static final long serialVersionUID = 2596839996638821527L;
	
	/** Constante privada del valor true en string */
	private static final String ON="on";
	
	/** Constante privada del valor true en integer */
	private static final String ON_NUMBER="1";
	
	/** Constante privada del valor false en string */
	private static final String OFF="off";
	
	/** Constante privada del valor false en integer */
	private static final String OFF_NUMBER="0";
	
	/*DESC. Atributos principales*/
	
	/** Variable que contiene informacion con respecto a: idLayout */
	private String idLayout;
	
	/** Variable que contiene informacion con respecto a: idCampo */
	private Integer idCampo;
	
	/** Variable que contiene informacion con respecto a: campo */
	private String campo;
	
	/** Variable que contiene informacion con respecto a: descripcion */
	private String descripcion;
	
	/** Variable que contiene informacion con respecto a: obligatorio */
	private String obligatorio = OFF_NUMBER;
	
	/** Variable que contiene informacion con respecto a: constante */
	private String constante;
	
	/** Variable que contiene informacion con respecto a: tipo */
	private String tipo;
	
	/** Variable que contiene informacion con respecto a: longitud */
	private Integer longitud;
	
	/** La variable que contiene informacion con respecto a: combo valores. */
	private List<BeanCombo> comboValores = new ArrayList<BeanCombo>();
	
	/**
	 * Metodo que permite obtener el idCampo
	 * @return idCampo
	 */
	public Integer getIdCampo() {
		return idCampo;
	}
	
	/**
	 * Metodo que permite establecer el valor del idCampo
	 * @param idCampo a establecer
	 */
	public void setIdCampo(Integer idCampo) {
		this.idCampo = idCampo;
	}
	
	/**
	 * Metodo que permite obtener el id del Layout
	 * @return idLayout al que pertenece el campo
	 */
	public String getIdLayout() {
		return idLayout;
	}
	
	/**
	 * Metodo que permite establecer el valor del layout del campo
	 * @param idLayout  al que pertenece el campo
	 */
	public void setIdLayout(String idLayout) {
		this.idLayout = idLayout;
	}
	
	/**
	 * Metodo que permite obtener el valor del campo
	 * @return valor del campo
	 */
	public String getCampo() {
		return campo;
	}
	
	/**
	 * Metodo que permite establecer el valor del campo
	 * @param campo a establecer
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	/**
	 * Metodo que permite obtener la descripcion del campo
	 * @return descripcion del campo
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Meotodo que permite establecer el valor de la descripcion del campo
	 * @param descripcion del campo
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Metodo que permite obtener si el campo es obligatorio
	 * @return string que indica si el campo es obligatorio
	 */
	public String getObligatorio() {
		return obligatorio;
	}
	
	/**
	 * Metodo que permite establecer si un campo es obligatorio
	 * @param obligatorio a establecer
	 */
	public void setObligatorio(String obligatorio) {
		//Procesar la respuesta: on, false, off, etc
		this.obligatorio = convertToBool(obligatorio);
	}
	
	/**
	 * Funcion encargada de transformar el string en un valor valido, antes de settearse
	 * @param input string con el valor
	 * @return 1 o 0
	 */
	public String convertToBool(String input){
		boolean offNumber = false;
		if(input==null){
			offNumber = true;
		}else{
			if(ON.equals(input)){
				return ON_NUMBER;
			}
			if(OFF.equals(input) || input.isEmpty()){
				offNumber = true;
			}
		}
		if(offNumber){
			return OFF_NUMBER;
		}else{
			return input;
		}
	}
	
	/**
	 * Metodo que permite obtener el tipo de campo
	 * @return tipo de campo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Metodo que permite establecer el valor del tipo de campo
	 * @param tipo de campo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Metodo que permite obtener la longitud del campo
	 * @return  longitud del campo
	 */
	public Integer getLongitud() {
		return longitud;
	}
	
	/**
	 * Metodo que permite establecer el valor de la longitud del campo
	 * @param longitud del campo
	 */
	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}
	
	/**
	 * Metodo que permite obtener el valor de la constante de un campo
	 * @return constante del campo
	 */
	public String getConstante() {
		return constante;
	}
	
	/**
	 * Metodo que permite establecer el valor de la constante de un campo
	 * @param constante  del campo
	 */
	public void setConstante(String constante) {
		this.constante = constante;
	}

	/**
	 * @return the comboValores
	 */
	public List<BeanCombo> getComboValores() {
		return comboValores;
	}

	/**
	 * @param comboValores the comboValores to set
	 */
	public void setComboValores(List<BeanCombo> comboValores) {
		this.comboValores = comboValores;
	}
}
