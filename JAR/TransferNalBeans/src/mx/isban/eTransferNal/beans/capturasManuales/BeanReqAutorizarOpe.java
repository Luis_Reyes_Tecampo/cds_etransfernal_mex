/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 12:10:00 AM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanReqAutorizarOpe.
 */
public class BeanReqAutorizarOpe extends BeanResBase implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5517659861446067790L;
	
	/** The template. */
	private String template;
	
	/** The fecha captura. */
	private String fechaCaptura;

	/** Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar. */
    private BeanPaginador paginador = new BeanPaginador();
	
    /** The list operaciones seleccionadas. */
    private List<BeanAutorizarOpe> listOperacionesSeleccionadas = new ArrayList<BeanAutorizarOpe>();
    
    /** The usr admin. */
    private String usrAdmin;
    
	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 *
	 * @param template the new template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Gets the fecha captura.
	 *
	 * @return the fecha captura
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}

	/**
	 * Sets the fecha captura.
	 *
	 * @param fechaCaptura the new fecha captura
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 * Gets the paginador.
	 *
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Sets the paginador.
	 *
	 * @param paginador the new paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Gets the list operaciones seleccionadas.
	 *
	 * @return the list operaciones seleccionadas
	 */
	public List<BeanAutorizarOpe> getListOperacionesSeleccionadas() {
		return listOperacionesSeleccionadas;
	}

	/**
	 * Sets the list operaciones seleccionadas.
	 *
	 * @param listOperacionesSeleccionadas the new list operaciones seleccionadas
	 */
	public void setListOperacionesSeleccionadas(
			List<BeanAutorizarOpe> listOperacionesSeleccionadas) {
		this.listOperacionesSeleccionadas = listOperacionesSeleccionadas;
	}

	/**
	 * Gets the usr admin.
	 *
	 * @return the usr admin
	 */
	public String getUsrAdmin() {
		return usrAdmin;
	}

	/**
	 * Sets the usr admin.
	 *
	 * @param usrAdmin the new usr admin
	 */
	public void setUsrAdmin(String usrAdmin) {
		this.usrAdmin = usrAdmin;
	}
}