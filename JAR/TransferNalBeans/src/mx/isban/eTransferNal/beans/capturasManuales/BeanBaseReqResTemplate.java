/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBaseReqResTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanBaseReqResTemplate  extends BeanBaseReqResTemplateA implements Serializable{

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = -665169223800313947L;
	
	/*DESC. Atributos principales*/	
	/** Variable que contiene informacion con respecto a: filtroEstado */
	private String filtroEstado;
	
	/**
	 * Metodo que permite obtener el filtro estado de consulta
	 * @return filtro estado
	 */
	public String getFiltroEstado() {
		return filtroEstado;
	}
	
	/**
	 * Metodo que permite establecer el valor del filtro estado
	 * @param filtroEstado a establecer
	 */
	public void setFiltroEstado(String filtroEstado) {
		this.filtroEstado = filtroEstado;
	}
}
