package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * The Class BeanResConsOperaciones.
 */
public class BeanResConsOperaciones extends BeanBaseConsOperaciones{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4316003129919165894L;

	/** Propiedad del tipo Integer que almacena la lista de opciones de estatus operacion */
	private String selectedEstatus;
	
	/** Propiedad del tipo Integer que almacena la lista de opciones de estatus operacion */
	private String nombreArchivo;
	
	/** The puede desapartar. */
	private boolean puedeDesapartar;
	
	/** The puede eliminar. */
	private boolean puedeEliminar;
	
	/**
	 * @return the selectedEstatus
	 */
	public String getSelectedEstatus() {
		return selectedEstatus;
	}

	/**
	 * @param selectedEstatus the selectedEstatus to set
	 */
	public void setSelectedEstatus(String selectedEstatus) {
		this.selectedEstatus = selectedEstatus;
	}

	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return the puedeDesapartar
	 */
	public boolean getPuedeDesapartar() {
		return puedeDesapartar;
	}

	/**
	 * @param puedeDesapartar the puedeDesapartar to set
	 */
	public void setPuedeDesapartar(boolean puedeDesapartar) {
		this.puedeDesapartar = puedeDesapartar;
	}

	/**
	 * @return the puedeEliminar
	 */
	public boolean getPuedeEliminar() {
		return puedeEliminar;
	}

	/**
	 * @param puedeEliminar the puedeEliminar to set
	 */
	public void setPuedeEliminar(boolean puedeEliminar) {
		this.puedeEliminar = puedeEliminar;
	}
}
