/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanReqAltaEditTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanReqAltaEditTemplate extends BeanBaseReqResTemplate implements Serializable {

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = 8572683904228905238L;
	
	/*DESC. Datos de principales*/
	/** Variable que contiene informacion con respecto a: tipo de operacion: 0- Inicio de pagina, 1-por
	 *  efecto del selected layout*/	
	private Integer tipoSolicitud=0;

	/**
	 * Metodo que permite obtener el tipo de solicitud del componente
	 * @return tipo de solicitud del compontne
	 */
	public Integer getTipoSolicitud() {
		return tipoSolicitud;
	}

	/**
	 * Metodo que permite establecer el tipo de solicitud del componente
	 * @param tipoSolicitud a establecer
	 */
	public void setTipoSolicitud(Integer tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}	
}
