/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 12:10:00 AM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

// clase hereda de BeanCapturasManuales
/**
 * The Class BeanAutorizarOpe.
 */
public class BeanAutorizarOpe extends BeanCapturasManuales{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = -4584935076979408509L;

	/** The usrs autorizan template. */
	private String usrsAutorizanTemplate;

	/**
	 * Gets the usrs autorizan template.
	 *
	 * @return the usrs autorizan template
	 */
	public String getUsrsAutorizanTemplate() {
		return usrsAutorizanTemplate;
	}

	/**
	 * Sets the usrs autorizan template.
	 *
	 * @param usrsAutorizanTemplate the new usrs autorizan template
	 */
	public void setUsrsAutorizanTemplate(String usrsAutorizanTemplate) {
		this.usrsAutorizanTemplate = usrsAutorizanTemplate;
	}
}
