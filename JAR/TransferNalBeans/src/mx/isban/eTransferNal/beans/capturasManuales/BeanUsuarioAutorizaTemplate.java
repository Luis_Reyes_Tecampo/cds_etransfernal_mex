/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanUsuarioAutorizaTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanUsuarioAutorizaTemplate implements Serializable {
		
	/** Constante privada del Serial Version */
	private static final long serialVersionUID = -1443644987458673693L;
	
	/*DESC. Atributos principales*/
	
	/** Variable que contiene informacion con respecto a: id del Template */
	private String idTemplate;
	
	/** Variable que contiene informacion con respecto a: usrCaptura */
	private String usrCaptura;
	
	/** Variable que contiene informacion con respecto a: usrAutoriza */
	private String usrAutoriza;
		
	/**
	 * Metodo que permite obtener el usuario que captura el template
	 * @return usrCaptura que da de alta el template
	 */
	public String getUsrCaptura() {
		return usrCaptura;
	}
	
	/**
	 * Metodo que permite establecer el valor del usuario que captura el template
	 * @param usrCaptura a establecer
	 */
	public void setUsrCaptura(String usrCaptura) {
		this.usrCaptura = usrCaptura;
	}
	
	/**
	 * Metodo que permite obtener el valor del usuario que autoriza el template
	 * @return usuario que autoriza el template
	 */
	public String getUsrAutoriza() {
		return usrAutoriza;
	}
	
	/**
	 * Metodo que permite establecer el valor del usuario que autoriza el template
	 * @param usrAutoriza  el template
	 */
	public void setUsrAutoriza(String usrAutoriza) {
		this.usrAutoriza = usrAutoriza;
	}	
	
	/**
	 * Metodo que permite obtener el Id del template al que pertenece e objeto
	 * @return id del Template
	 */
	public String getIdTemplate() {
		return idTemplate;
	}
	
	/**
	 * Metodo que permite establecer el valor del template al que pertenece el objeto
	 * @param idTemplate a establecer
	 */
	public void setIdTemplate(String idTemplate) {
		this.idTemplate = idTemplate;
	}
}
