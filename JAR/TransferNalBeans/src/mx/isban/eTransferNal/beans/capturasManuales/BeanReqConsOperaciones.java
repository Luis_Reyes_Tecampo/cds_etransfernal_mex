package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * The Class BeanReqConsOperaciones.
 */
public class BeanReqConsOperaciones extends BeanBaseConsOperaciones{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4316003129919165894L;

	/**
	 * Propiedad del tipo @see String que almacena el folio
	 */
	private String folio;

	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
}
