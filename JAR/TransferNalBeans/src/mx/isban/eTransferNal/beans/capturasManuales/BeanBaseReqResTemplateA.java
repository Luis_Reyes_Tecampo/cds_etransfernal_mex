/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBaseReqResTemplateA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanBaseReqResTemplateA  extends BeanResBase implements Serializable{

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = 1937667199451216942L;

	/** Propiedad que contiene informacion con respecto a: paginador */
	private BeanPaginador paginador=new BeanPaginador();
	
	/** Variable que contiene informacion con respecto a: totalRegistros */
	private Integer totalRegistros = 0;
	
	/** Variable que contiene informacion con respecto a: forward */
	private Boolean forward = false;
	
	/**Variable que contiene informacion con respecto a: modalidad*/
	private Boolean modoEdicion = false;
	
	/**Variable que contiene informacion con respecto a: estados de filtro*/
	private List<String> estados = Arrays.asList(new String[]{"AC", "PD"});
		
	/** Variable que contiene informacion con respecto a: layout seleccionado */
	private String selectedLayout;
	
	/** Variable que contiene informacion con respecto a: listaLayouts */
	private List<String> listaLayouts = new ArrayList<String>();
	
	/** Variable que contiene informacion con respecto a: listaUsuarios */
	private List<String> listaUsuarios = new ArrayList<String>();
	
	/** Propiedad que contiene informacion con respecto a: Campos base de Layout */
	private List<BeanCampoLayout> listaCamposMaster = new ArrayList<BeanCampoLayout>();
	
	/** Propiedad que contiene informacion con respecto a: Template */
	private BeanTemplate template = new BeanTemplate();
	
	/**
	 * Metodo que permite obtener el objeto paginador del componente
	 * @return paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	 * Metodo que permite establecer el valor del paginador del componente.
	 * @param paginador del componente
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Metodo que permite obtener el total de registros del componente
	 * @return total de registros del componente
	 */
	public Integer getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Metodo que permite establecer el total de registros del componentes
	 * @param totalRegistros del componente
	 */
	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	/**
	 * Metodo que permite verificar si se ha generado una peticion de respuesta
	 * @return Boolean de la peticion
	 */
	public Boolean getForward() {
		return forward;
	}
	
	/**
	 * Metodo que permite establecer el valor del forward de la peticion
	 * @param forward  a establecer
	 */
	public void setForward(Boolean forward) {
		this.forward = forward;
	}
	
	/**
	 * Metodo que permite verificar si nos encontramos en modo edicion
	 * @return True | False modo Edicion
	 */
	public Boolean getModoEdicion() {
		return modoEdicion;
	}
	
	/**
	 * Metodo que permite establecer el valor del modo edicion
	 * @param modoEdicion valor Bool a establecer
	 */
	public void setModoEdicion(Boolean modoEdicion) {
		this.modoEdicion = modoEdicion;
	}
	
	/**
	 * Metodo que permite obtener los estados del combo de filtro
	 * @return Lista de filtros
	 */
	public List<String> getEstados() {
		return estados;
	}
	
	/**
	 * Metodo que permite establecer el valor de los estados
	 * @param estados a establecer en la lista
	 */
	public void setEstados(List<String> estados) {
		this.estados = estados;
	}
	
	/**
	 * Metodo que permite obtener el layout seleccionado del componente de altas
	 * @return selected Layout
	 */
	public String getSelectedLayout() {
		return selectedLayout;
	}

	/**
	 * Metodo que permite establecer el layout seleccionado
	 * @param selectedLayout seleccionado
	 */
	public void setSelectedLayout(String selectedLayout) {
		this.selectedLayout = selectedLayout;
	}

	/**
	 * Metodo que permite obtener la lista de layouts disponibles
	 * @return lista de layouts disponibles
	 */
	public List<String> getListaLayouts() {
		return listaLayouts;
	}

	/**
	 * Metodo que permite establecer la lista de layouts
	 * @param listaLayouts a establecer
	 */
	public void setListaLayouts(List<String> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}

	/**
	 * Metodo que permite obtener la lista de usuarios a utilizarse en el componente
	 * @return lista de usuarios disponibles para administrar
	 */
	public List<String> getListaUsuarios() {
		return listaUsuarios;
	}

	/**
	 * Metodo que permite establecer la lista de usuarios disponibles
	 * @param listaUsuarios disponibles para autorizar
	 */
	public void setListaUsuarios(List<String> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	/**
	 * Metodo que permite obtener la definicion especifica de los campos del layout
	 * @return lista de campos de layout
	 */
	public List<BeanCampoLayout> getListaCamposMaster() {
		return listaCamposMaster;
	}

	/**
	 * Metodo que permite establecer la lista de campos del layout principal
	 * @param listaCamposMaster lista de campos del layout
	 */
	public void setListaCamposMaster(List<BeanCampoLayout> listaCamposMaster) {
		this.listaCamposMaster = listaCamposMaster;
	}
	
	/**
	 * Metodo que permite obtener el template a utilizarse para altas.
	 * @return template
	 */
	public BeanTemplate getTemplate() {
		return template;
	}

	/**
	 * Metodo que permite establecer el valor del template
	 * @param template a establecer
	 */
	public void setTemplate(BeanTemplate template) {
		this.template = template;
	}
	
}
