package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * The Class BeanCapturasManuales.
 */
public class BeanCapturasManuales extends BeanCapturasManualesA implements Serializable {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -8532769008875666737L;

	/** Propiedad del tipo @see String que almacena el folio. */
	private String folio;

	/** Propiedad del tipo @see String que almacena el importe. */
	private String importe;

	/** Propiedad del tipo @see String que almacena el template. */
	private String template;

	/** Propiedad del tipo @see String que almacena el usrCaptura. */
	private String usrCaptura;

	/** Propiedad del tipo @see String que almacena el usrAutoriza. */
	private String usrAutoriza;

	/** Propiedad del tipo @see String que almacena el estatus. */
	private String estatus;

	/** Propiedad del tipo @see String que almacena la fechaCaptura. */
	private String fechaCaptura;

	/** Propiedad del tipo @see String que almacena la refTransfer. */
	private String refTransfer;

	/**
	 * Gets the folio.
	 *
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * Sets the folio.
	 *
	 * @param folio
	 *            the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * Sets the importe.
	 *
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 *
	 * @param template
	 *            the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Gets the usr captura.
	 *
	 * @return the usrCaptura
	 */
	public String getUsrCaptura() {
		return usrCaptura;
	}

	/**
	 * Sets the usr captura.
	 *
	 * @param usrCaptura
	 *            the usrCaptura to set
	 */
	public void setUsrCaptura(String usrCaptura) {
		this.usrCaptura = usrCaptura;
	}

	/**
	 * Gets the usr autoriza.
	 *
	 * @return the usrAutoriza
	 */
	public String getUsrAutoriza() {
		return usrAutoriza;
	}

	/**
	 * Sets the usr autoriza.
	 *
	 * @param usrAutoriza
	 *            the usrAutoriza to set
	 */
	public void setUsrAutoriza(String usrAutoriza) {
		this.usrAutoriza = usrAutoriza;
	}

	/**
	 * Gets the estatus.
	 *
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Sets the estatus.
	 *
	 * @param estatus
	 *            the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Gets the fecha captura.
	 *
	 * @return the fechaCaptura
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}

	/**
	 * Sets the fecha captura.
	 *
	 * @param fechaCaptura
	 *            the fechaCaptura to set
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 * Gets the ref transfer.
	 *
	 * @return the refTransfer
	 */
	public String getRefTransfer() {
		return refTransfer;
	}

	/**
	 * Sets the ref transfer.
	 *
	 * @param refTransfer
	 *            the new ref transfer
	 */
	public void setRefTransfer(String refTransfer) {
		this.refTransfer = refTransfer;
	}
}
