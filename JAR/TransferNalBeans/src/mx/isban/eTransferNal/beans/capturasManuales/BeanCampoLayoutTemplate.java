/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCampoLayoutTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanCampoLayoutTemplate implements Serializable {
	
	/** Constante privada del Serial Version */
	private static final long serialVersionUID = -445169223800313947L;
	
	/** Constante privada del valor true en string */
	private static final String ON="on";
	
	/** Constante privada del valor true en integer */
	private static final String ON_NUMBER="1";
	
	/** Constante privada del valor false en string */
	private static final String OFF="off";
	
	/** Constante privada del valor false en integer */
	private static final String OFF_NUMBER="0";
	
	
	/*DESC. Atributos principales*/
	
	/** Variable que contiene informacion con respecto a: id de campo */
	private Integer idCampo;
	
	/** Variable que contiene informacion con respecto a: id del Template */
	private String idTemplate;
	
	/** Variable que contiene informacion con respecto a: campo */
	private String campo;
	
	/** Variable que contiene informacion con respecto a:  1 - Obligatorio, 0-No Obligatorio */
	private String obligatorio=OFF_NUMBER;
	
	/** Variable que contiene informacion con respecto a:  el valor constante del campo */
	private String constante;
	
	
	/**
	 * Metodo que permite obtener el id del campo al que pertenece.
	 * @return id del campo 
	 */
	public Integer getIdCampo() {
		return idCampo;
	}
	
	/**
	 * Metodo que permite establecer el valor del idCampo
	 * @param idCampo a establecer
	 */
	public void setIdCampo(Integer idCampo) {
		this.idCampo = idCampo;
	}
	
	/**
	 * Metodo que permite obtener si el campo es obligatorio 
	 * @return string del campo obligatorio
	 */
	public String getObligatorio() {
		return obligatorio;
	}
	
	/**
	 * Metodo que permite establecer el valor del campo obligatorio
	 * @param obligatorio a establecer
	 */
	public void setObligatorio(String obligatorio) {
		//Procesar la respuesta: on, false, off, etc
		this.obligatorio = convertToBool(obligatorio);
	}
	
	/**
	 * Metodo que permite obtener el valor del campo
	 * @return campo
	 */
	public String getCampo() {
		return campo;
	}
	
	/**
	 * Metodo que permite establecer el valor del campo
	 * @param campo  a establecer
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 * Metodo que permite obtener el id del template
	 * @return idTemplate al que pertenece el campo
	 */
	public String getIdTemplate() {
		return idTemplate;
	}
	
	/**
	 * Metodo que permite establecer el valor del idTemplate
	 * @param idTemplate a establecer
	 */
	public void setIdTemplate(String idTemplate) {
		this.idTemplate = idTemplate;
	}

	/**
	 * Metodo que permite obtener la constante asignada a campo
	 * @return valor de la constante del campo
	 */
	public String getConstante() {
		return constante;
	}
	
	/**
	 * Metodo que permite establecer el valor de la constante.
	 * @param constante  del campo
	 */
	public void setConstante(String constante) {
		this.constante = constante;
	}
	
	/**
	 * Funcion encargada de transformar el string en un valor valido, antes de settearse
	 * @param input string con el valor
	 * @return 1 o 0
	 */
	public String convertToBool(String input){
		boolean isOffNumber = false;
		if(input==null){
			isOffNumber=true;
		}else{
			if(ON.equals(input)){
				return ON_NUMBER;
			}
			if(OFF.equals(input) || input.isEmpty()){
				isOffNumber=true;
			}
		}
		if(isOffNumber){
			return OFF_NUMBER;
		}else{
			return input;
		}
	}
}
