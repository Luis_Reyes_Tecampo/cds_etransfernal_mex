package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanCapturasManualesA.
 */
public class BeanCapturasManualesA extends BeanResBase implements Serializable {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 5922585724770755719L;

	/** Propiedad del tipo @see String que almacena la refTransfer. */
	private String refTransfer;

	/** Propiedad del tipo @see String que almacena el usrApartado. */
	private String usrApartado;

	/** Propiedad del tipo @see String que almacena el seleccionado. */
	private boolean seleccionado;

	/** Propiedad que almacena la propiedad apartadas. */
	private boolean apartado;

	/** The cod error. */
	private String codErrorOpe;

	/** The msg error. */
	private String msgErrorOpe;

	/** Propiedad que almacena la propiedad statusOperacionApartada. */
	private String statusOperacionApartada;

	/** The cuenta ordenante. */
	private String cuentaOrdenante;

	/** The cuenta receptora. */
	private String cuentaReceptora;

	/** The monto. */
	private String monto;

	/**
	 * Gets the ref transfer.
	 *
	 * @return the refTransfer
	 */
	public String getRefTransfer() {
		return refTransfer;
	}

	/**
	 * Sets the ref transfer.
	 *
	 * @param refTransfer
	 *            the new ref transfer
	 */
	public void setRefTransfer(String refTransfer) {
		this.refTransfer = refTransfer;
	}

	/**
	 * Gets the usr apartado.
	 *
	 * @return the usrApartado
	 */
	public String getUsrApartado() {
		return usrApartado;
	}

	/**
	 * Sets the usr apartado.
	 *
	 * @param usrApartado
	 *            the usrApartado to set
	 */
	public void setUsrApartado(String usrApartado) {
		this.usrApartado = usrApartado;
	}

	/**
	 * Checks if is seleccionado.
	 *
	 * @return the seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Sets the seleccionado.
	 *
	 * @param seleccionado
	 *            the seleccionado to set
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Checks if is apartado.
	 *
	 * @return the apartado
	 */
	public boolean isApartado() {
		return apartado;
	}

	/**
	 * Sets the apartado.
	 *
	 * @param apartado
	 *            the apartado to set
	 */
	public void setApartado(boolean apartado) {
		this.apartado = apartado;
	}

	/**
	 * Gets the status operacion apartada.
	 *
	 * @return the statusOperacionApartada
	 */
	public String getStatusOperacionApartada() {
		return statusOperacionApartada;
	}

	/**
	 * Sets the status operacion apartada.
	 *
	 * @param statusOperacionApartada
	 *            the statusOperacionApartada to set
	 */
	public void setStatusOperacionApartada(String statusOperacionApartada) {
		this.statusOperacionApartada = statusOperacionApartada;
	}

	/**
	 * Gets the cod error ope.
	 *
	 * @return the cod error ope
	 */
	public String getCodErrorOpe() {
		return codErrorOpe;
	}

	/**
	 * Sets the cod error ope.
	 *
	 * @param codErrorOpe
	 *            the new cod error ope
	 */
	public void setCodErrorOpe(String codErrorOpe) {
		this.codErrorOpe = codErrorOpe;
	}

	/**
	 * Gets the msg error ope.
	 *
	 * @return the msg error ope
	 */
	public String getMsgErrorOpe() {
		return msgErrorOpe;
	}

	/**
	 * Sets the msg error ope.
	 *
	 * @param msgErrorOpe
	 *            the new msg error ope
	 */
	public void setMsgErrorOpe(String msgErrorOpe) {
		this.msgErrorOpe = msgErrorOpe;
	}

	/**
	 * Gets the cuenta ordenante.
	 *
	 * @return the cuenta ordenante
	 */
	public String getCuentaOrdenante() {
		return cuentaOrdenante;
	}

	/**
	 * Sets the cuenta ordenante.
	 *
	 * @param cuentaOrdenante
	 *            the new cuenta ordenante
	 */
	public void setCuentaOrdenante(String cuentaOrdenante) {
		this.cuentaOrdenante = cuentaOrdenante;
	}

	/**
	 * Gets the cuenta receptora.
	 *
	 * @return the cuenta receptora
	 */
	public String getCuentaReceptora() {
		return cuentaReceptora;
	}

	/**
	 * Sets the cuenta receptora.
	 *
	 * @param cuentaReceptora
	 *            the new cuenta receptora
	 */
	public void setCuentaReceptora(String cuentaReceptora) {
		this.cuentaReceptora = cuentaReceptora;
	}

	/**
	 * Gets the monto.
	 *
	 * @return the monto
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * Sets the monto.
	 *
	 * @param monto
	 *            the new monto
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}

}
