/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 12:10:00 AM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanResAutorizarOpe.
 */
public class BeanResAutorizarOpe extends BeanResBase implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 7348609833487270081L;
	
	/** Propiedad que almacena la lista de registros de Autorizacion Operaciones. */
	private List<BeanAutorizarOpe> listOperacionesPendAutorizar = new ArrayList<BeanAutorizarOpe>();

	/** The operacionesApartadas. */
	private boolean operacionesApartadasPorPagina;
	
	/** The operaciones apartadas usr. */
	private boolean operacionesApartadasUsr;

	/** Propiedad del tipo String que guarda el template. */
	private String template;

	/** Propiedad del tipo String que guarda la fecha de operacion. */
	private String fechaOperacion;
	
	/** Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar. */
    private BeanPaginador paginador = new BeanPaginador();

	/** Propiedad del tipo Integer que guarda el total de registros. */
    private int totalReg=0;
    
    /** The usr admin. */
    private String usrAdmin;
    
	/**
	 * Gets the list operaciones pend autorizar.
	 *
	 * @return the list operaciones pend autorizar
	 */
	public List<BeanAutorizarOpe> getListOperacionesPendAutorizar() {
		return listOperacionesPendAutorizar;
	}

	/**
	 * Sets the list operaciones pend autorizar.
	 *
	 * @param listOperacionesPendAutorizar the new list operaciones pend autorizar
	 */
	public void setListOperacionesPendAutorizar(
			List<BeanAutorizarOpe> listOperacionesPendAutorizar) {
		this.listOperacionesPendAutorizar = listOperacionesPendAutorizar;
	}

	/**
	 * Checks if is operaciones apartadas por pagina.
	 *
	 * @return true, if is operaciones apartadas por pagina
	 */
	public boolean isOperacionesApartadasPorPagina() {
		return operacionesApartadasPorPagina;
	}

	/**
	 * Sets the operaciones apartadas por pagina.
	 *
	 * @param operacionesApartadasPorPagina the new operaciones apartadas por pagina
	 */
	public void setOperacionesApartadasPorPagina(
			boolean operacionesApartadasPorPagina) {
		this.operacionesApartadasPorPagina = operacionesApartadasPorPagina;
	}

	/**
	 * Checks if is operaciones apartadas usr.
	 *
	 * @return true, if is operaciones apartadas usr
	 */
	public boolean isOperacionesApartadasUsr() {
		return operacionesApartadasUsr;
	}

	/**
	 * Sets the operaciones apartadas usr.
	 *
	 * @param operacionesApartadasUsr the new operaciones apartadas usr
	 */
	public void setOperacionesApartadasUsr(boolean operacionesApartadasUsr) {
		this.operacionesApartadasUsr = operacionesApartadasUsr;
	}

	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 *
	 * @param template the new template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Gets the fecha operacion.
	 *
	 * @return the fecha operacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Sets the fecha operacion.
	 *
	 * @param fechaOperacion the new fecha operacion
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Gets the paginador.
	 *
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Sets the paginador.
	 *
	 * @param paginador the new paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Gets the total reg.
	 *
	 * @return the total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Sets the total reg.
	 *
	 * @param totalReg the new total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Gets the usr admin.
	 *
	 * @return the usr admin
	 */
	public String getUsrAdmin() {
		return usrAdmin;
	}

	/**
	 * Sets the usr admin.
	 *
	 * @param usrAdmin the new usr admin
	 */
	public void setUsrAdmin(String usrAdmin) {
		this.usrAdmin = usrAdmin;
	}
}
