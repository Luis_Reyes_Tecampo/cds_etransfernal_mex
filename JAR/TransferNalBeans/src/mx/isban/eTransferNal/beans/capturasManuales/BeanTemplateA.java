/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTemplateA.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanTemplateA implements Serializable  {

	/** Constante privada del Serial Version */
	private static final long serialVersionUID = -230511416103845482L;

	/** Variable que contiene informacion con respecto a: identificador layout */
	private String idLayout;
	
	/** Variable que contiene informacion con respecto a: estado AC, PD */
	private String estatus;
	
	/** Variable que contiene informacion con respecto a: usuario alta */
	private String usrAlta;
	
	/** Variable que contiene informacion con respecto a: usuario autorizo */
	private String usrAutoriza;
	
	/** Variable que contiene informacion con respecto a: fecha captura */
	private String fechaCaptura;
	
	/** Variable que contiene informacion con respecto a: fecha modifica */
	private String fechaModifica;
	
	/** Variable que contiene informacion con respecto a: seleccion */
	private Boolean seleccionado = false;
	
	/** Variable que contiene informacion con respecto a: operaciones asociadas al template */
	private Boolean operacionesAsociadas = false;
	
	/*DESC. Atributos secundarios*/
	
	/** Propiedad de tipo List<BeanLayoutCampoTemplate> que contiene informacion con respecto a: campos del template */
	private List<BeanCampoLayoutTemplate> campos = new ArrayList<BeanCampoLayoutTemplate>();
	
	/** Propiedad de tipo List<BeanUsuarioAutorizaTemplate> que contiene informacion con respecto a: usuarios que autorizan el template */
	private List<BeanUsuarioAutorizaTemplate> usuariosAutorizan = new ArrayList<BeanUsuarioAutorizaTemplate>();

	/**
	 * Metodo que permite obtener el valor del template estatus 
	 * @return estatus del template
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Metodo que permite establecer el valor del template: estatus
	 * @param estatus del template a establecer
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo que permite obtener la fecha de captura del template
	 * @return fecha de captura del template
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	
	/**
	 * Metodo que permite establecer la fecha de captura del template
	 * @param fechaCaptura del template a establecer
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}
	
	/**
	 * Metodo que permite obtener la fecha de modificacion del template
	 * @return fecha de modificacion del template
	 */
	public String getFechaModifica() {
		return fechaModifica;
	}
	
	/**
	 * Metodo que permite establecer la fecha de modificacion de template.
	 * @param fechaModifica del template a establecer
	 */
	public void setFechaModifica(String fechaModifica) {
		this.fechaModifica = fechaModifica;
	}
	
	/**
	 * Metodo que permite obtener los campos del template.
	 * @return lista de campos del template
	 */
	public List<BeanCampoLayoutTemplate> getCampos() {
		return campos;
	}
	
	/**
	 * Metodo que permite establecer la lista de campos del template
	 * @param campos del template
	 */
	public void setCampos(List<BeanCampoLayoutTemplate> campos) {
		this.campos = campos;
	}
	
	/**
	 * Metodo que permite obtener los usuarios que autorizan el template.
	 * @return lista de usuarios que autorizan el template
	 */
	public List<BeanUsuarioAutorizaTemplate> getUsuariosAutorizan() {
		return usuariosAutorizan;
	}
	
	/**
	 * Metodo que permite establecer los usuarios que autorizan el template
	 * @param usuariosAutorizan del template
	 */
	public void setUsuariosAutorizan(List<BeanUsuarioAutorizaTemplate> usuariosAutorizan) {
		this.usuariosAutorizan = usuariosAutorizan;
	}
	
	/**
	 * Metodo que permite obtener el id del Layout.
	 * @return id del Layout del template
	 */
	public String getIdLayout() {
		return idLayout;
	}

	/**
	 * Metodo que permite establecer el id del Layout del template.
	 * @param idLayout a establecer
	 */
	public void setIdLayout(String idLayout) {
		this.idLayout = idLayout;
	}

	/**
	 * Metodo que permite obtener el usuario que creo el template
	 * @return usuario que creo el templae
	 */
	public String getUsrAlta() {
		return usrAlta;
	}

	/**
	 * Metodo que permite establecer el usuario que creo el template
	 * @param usrAlta del template
	 */
	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	/**
	 * Metodo que permite obtener el usuario que autorizo el template
	 * @return usuario que autorizo el template
	 */
	public String getUsrAutoriza() {
		return usrAutoriza;
	}

	/**
	 * Metodo que permite establecer el valor del usuario que autoriza el template
	 * @param usrAutoriza  el template
	 */
	public void setUsrAutoriza(String usrAutoriza) {
		this.usrAutoriza = usrAutoriza;
	}
	
	
	/**
	 * Metodo que permite obtener si el template se encuentra seleccionado
	 * @return bandera indicando si el template esta seleccionado.
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Metodo que permite establecer el valor de la seleccion de un template
	 * @param seleccionado  valor a establecer en la seleccion
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	/**
	 * Metodo a traves del cual se determina si un template tiene operaciones asociadas
	 * @return True | False indicando si tiene operaciones asociadas
	 */
	public Boolean getOperacionesAsociadas() {
		return operacionesAsociadas;
	}
	
	/**
	 * Metodo que permite establecer el valor de las operaciones de un template
	 * @param operacionesAsociadas valor de las operaciones a establecer
	 */
	public void setOperacionesAsociadas(Boolean operacionesAsociadas) {
		this.operacionesAsociadas = operacionesAsociadas;
	}


}
