/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 27 14:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) encargada de administrar las respuestas a la vista
 * solicitada
 *
 */
public class BeanTemplate extends BeanTemplateA implements Serializable  {

	/** Constante privada del Serial Version */
	private static final long serialVersionUID = -7478114182213372498L;
	
	/*DESC. Atributos principales*/
	
	/** Variable que contiene informacion del row generado unicamente para la vista. */
	private Integer idRegistro;
	
	/** Variable que contiene informacion con respecto a: folio de template */
	private String idTemplate;
	
	/**
	 * Metodo que permite obtener el idRegistro del template
	 * @return idRegistro del template
	 */
	public Integer getIdRegistro() {
		return idRegistro;
	}
	
	/**
	 * Metodo que permite establecer el valor del template: idRegistro
	 * @param idRegistro a establecer
	 */
	public void setIdRegistro(Integer idRegistro) {
		this.idRegistro = idRegistro;
	}

	/**
	 * Metodo que permite obtener el id del Template
	 * @return idTemplate
	 */
	public String getIdTemplate() {
		return idTemplate;
	}

	/**
	 * Metodo que permite establecer el id del Template
	 * @param idTemplate a establecer
	 */
	public void setIdTemplate(String idTemplate) {
		this.idTemplate = idTemplate;
	}
}
