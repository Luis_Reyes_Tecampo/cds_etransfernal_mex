package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Clase base para modulo consulta y aprobacion de operaciones
 */
public class BeanBaseConsOperacionesA extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5104797583250496146L;
	
	/**
	 * Propiedad del tipo @see String que almacena el estatus de operacion
	 */
	private String estatusOperacion;
	
	/**
	 * Propiedad del tipo @see String que almacena el template
	 */
	private String template;

	/**
	 * Propiedad del tipo @see String que almacena el layout
	 */
	private String layoutID;
	
	/**
	 * Propiedad del tipo @see String que almacena la fecha de captura
	 */
	private String fechaCaptura;

	/**Propiedad que almacena la lista de registros de la bitacora administrativa*/
	private List<BeanConsOperaciones> listBeanConsOperaciones = new ArrayList<BeanConsOperaciones>();

	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
    private BeanPaginador paginador = new BeanPaginador();
    
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	
	/** The cuenta ordenante. */
	private String cuentaOrdenante;

	/** The monto. */
	private String monto;

	/** The cuenta receptora. */
	private String cuentaReceptora;

	/**
	 * @return the estatusOperacion
	 */
	public String getEstatusOperacion() {
		return estatusOperacion;
	}

	/**
	 * @param estatusOperacion the estatusOperacion to set
	 */
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @return the layoutID
	 */
	public String getLayoutID() {
		return layoutID;
	}

	/**
	 * @param layoutID the layoutID to set
	 */
	public void setLayoutID(String layoutID) {
		this.layoutID = layoutID;
	}
	
	/**
	 * @return the fechaCaptura
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}

	/**
	 * @param fechaCaptura the fechaCaptura to set
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 * @return the listBeanConsOperaciones
	 */
	public List<BeanConsOperaciones> getListBeanConsOperaciones() {
		return listBeanConsOperaciones;
	}

	/**
	 * @param listBeanConsOperaciones the listBeanConsOperaciones to set
	 */
	public void setListBeanConsOperaciones(List<BeanConsOperaciones> listBeanConsOperaciones) {
		this.listBeanConsOperaciones = listBeanConsOperaciones;
	}
	
	/**
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * @param paginador the paginador to set
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Gets the cuenta ordenante.
	 *
	 * @return the cuenta ordenante
	 */
	public String getCuentaOrdenante() {
		return cuentaOrdenante;
	}

	/**
	 * Sets the cuenta ordenante.
	 *
	 * @param cuentaOrdenante the new cuenta ordenante
	 */
	public void setCuentaOrdenante(String cuentaOrdenante) {
		this.cuentaOrdenante = cuentaOrdenante;
	}
	
	/**
	 * Gets the monto.
	 *
	 * @return the monto
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * Sets the monto.
	 *
	 * @param monto the new monto
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	/**
	 * Gets the cuenta receptora.
	 *
	 * @return the cuenta receptora
	 */
	public String getCuentaReceptora() {
		return cuentaReceptora;
	}

	/**
	 * Sets the cuenta receptora.
	 *
	 * @param cuentaReceptora the new cuenta receptora
	 */
	public void setCuentaReceptora(String cuentaReceptora) {
		this.cuentaReceptora = cuentaReceptora;
	}

}
