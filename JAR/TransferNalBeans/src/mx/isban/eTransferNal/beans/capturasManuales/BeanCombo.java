/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCombo.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/03/2017 06:52:03 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * Class BeanCombo.
 *
 * @author FSW-Vector
 * @since 31/03/2017
 */
public class BeanCombo implements Serializable {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3225927782578495697L;
	
	/** La variable que contiene informacion con respecto a: id. */
	private String id;
	
	/** La variable que contiene informacion con respecto a: valor. */
	private String valor;

	/**
	 * Obtener el objeto: id.
	 *
	 * @return El objeto: id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Definir el objeto: id.
	 *
	 * @param id El nuevo objeto: id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Obtener el objeto: valor.
	 *
	 * @return El objeto: valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Definir el objeto: valor.
	 *
	 * @param valor El nuevo objeto: valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

}
