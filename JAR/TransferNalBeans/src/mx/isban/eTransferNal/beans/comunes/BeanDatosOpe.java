package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class BeanDatosOpe implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8730117910553809721L;
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de estatusOperacion
	 */
	private String estatusOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private String monto;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCambio
	 */
	private String tipoCambio;
	/**
	 * Propiedad del tipo String que almacena el valor de referencia
	 */
	private String referencia;
	/**
	 * Propiedad del tipo String que almacena el valor de ctaOrigen
	 */
	private String ctaOrigen;
	/**
	 * Propiedad del tipo String que almacena el valor de ctaDestino
	 */
	private String ctaDestino;

	/**
	 * Propiedad del tipo String que almacena el valor de idOperacion
	 */
	private String idOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de bancoDestino
	 */
	private String bancoDestino;
	/**
	 * Propiedad del tipo String que almacena el valor de codigoErr
	 */
	private String codigoErr;
	
	/**
	 * Propiedad del tipo String que almacena el valor de descOper
	 */
	private String descOper;
	/**
	 * Propiedad del tipo String que almacena el valor de descErr
	 */
	private String descErr;
	/**
	 * Propiedad del tipo String que almacena el valor de comentarios
	 */
	private String comentarios;
	/**
	 * Propiedad del tipo String que almacena el valor de servTran
	 */
	private String servTran;
	/**
	 * Metodo get que sirve para obtener la propiedad fchOperacion
	 * @return fchOperacion Objeto del tipo String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchOperacion
	 * @param fchOperacion del tipo String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad estatusOperacion
	 * @return estatusOperacion Objeto del tipo String
	 */
	public String getEstatusOperacion() {
		return estatusOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad estatusOperacion
	 * @param estatusOperacion del tipo String
	 */
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo String
	 */
	public String getMonto() {
		if(null == monto || StringUtils.EMPTY.equals(monto)) {
			return "0";
		}
		return monto;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoCambio
	 * @return tipoCambio Objeto del tipo String
	 */
	public String getTipoCambio() {
		if(null == tipoCambio || StringUtils.EMPTY.equals(tipoCambio)) {
			return "0";
		}
		return tipoCambio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCambio
	 * @param tipoCambio del tipo String
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad referencia
	 * @return referencia Objeto del tipo String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad referencia
	 * @param referencia del tipo String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ctaOrigen
	 * @return ctaOrigen Objeto del tipo String
	 */
	public String getCtaOrigen() {
		return ctaOrigen;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ctaOrigen
	 * @param ctaOrigen del tipo String
	 */
	public void setCtaOrigen(String ctaOrigen) {
		this.ctaOrigen = ctaOrigen;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ctaDestino
	 * @return ctaDestino Objeto del tipo String
	 */
	public String getCtaDestino() {
		return ctaDestino;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ctaDestino
	 * @param ctaDestino del tipo String
	 */
	public void setCtaDestino(String ctaDestino) {
		this.ctaDestino = ctaDestino;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad idOperacion
	 * @return idOperacion Objeto del tipo String
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad idOperacion
	 * @param idOperacion del tipo String
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad bancoDestino
	 * @return bancoDestino Objeto del tipo String
	 */
	public String getBancoDestino() {
		return bancoDestino;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad bancoDestino
	 * @param bancoDestino del tipo String
	 */
	public void setBancoDestino(String bancoDestino) {
		this.bancoDestino = bancoDestino;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad codigoErr
	 * @return codigoErr Objeto del tipo String
	 */
	public String getCodigoErr() {
		if(codigoErr == null) {
			return null;
		}
		if(codigoErr.trim().length() > 8) {
			return codigoErr.trim().substring(0, 8);
		}
		return codigoErr.trim();
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codigoErr
	 * @param codigoErr del tipo String
	 */
	public void setCodigoErr(String codigoErr) {
		this.codigoErr = codigoErr;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad descOper
	 * @return descOper Objeto del tipo String
	 */
	public String getDescOper() {
		return descOper;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad descOper
	 * @param descOper del tipo String
	 */
	public void setDescOper(String descOper) {
		this.descOper = descOper;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad descErr
	 * @return descErr Objeto del tipo String
	 */
	public String getDescErr() {
		return descErr;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad descErr
	 * @param descErr del tipo String
	 */
	public void setDescErr(String descErr) {
		this.descErr = descErr;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad comentarios
	 * @return comentarios Objeto del tipo String
	 */
	public String getComentarios() {
		return comentarios;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad comentarios
	 * @param comentarios del tipo String
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad servTran
	 * @return servTran Objeto del tipo String
	 */
	public String getServTran() {
		return servTran;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad servTran
	 * @param servTran del tipo String
	 */
	public void setServTran(String servTran) {
		this.servTran = servTran;
	}
	



}
