/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqInsertBitAdminDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

public class BeanReqInsertBitAdminDos implements Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -6504884851212546361L;

	
	
	/**
	 * Campo datoFijo
	 */
	private String datoFijo;
	
	/**
	 * Campo servTran
	 */
	private String servTran;
	
	/**
	 * Campo dirIp
	 */
	private String dirIp;
	
	/**
	 * Campo canApli
	 */
	private String canApli;
	
	/**
	 * Campo instWeb
	 */
	private String instWeb;
	
	/**
	 * Campo hostWeb
	 */
	private String hostWeb;
	
	/**
	 * Campo datoMobi
	 */
	private String datoMobi;
	
	/**
	 * Campo tablaAfec
	 */
	private String tablaAfec;
	
	/**
	 * Campo idSession
	 */
	private String idSession;
	
	/**
	 * Campo codOper
	 */
	private String codOper;

	

	/**
	 * @return the datoFijo
	 */
	public String getDatoFijo() {
		return datoFijo;
	}

	/**
	 * @param datoFijo the datoFijo to set
	 */
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}

	/**
	 * @return the servTran
	 */
	public String getServTran() {
		return servTran;
	}

	/**
	 * @param servTran the servTran to set
	 */
	public void setServTran(String servTran) {
		this.servTran = servTran;
	}

	/**
	 * @return the dirIp
	 */
	public String getDirIp() {
		return dirIp;
	}

	/**
	 * @param dirIp the dirIp to set
	 */
	public void setDirIp(String dirIp) {
		this.dirIp = dirIp;
	}

	/**
	 * @return the canApli
	 */
	public String getCanApli() {
		return canApli;
	}

	/**
	 * @param canApli the canApli to set
	 */
	public void setCanApli(String canApli) {
		this.canApli = canApli;
	}

	/**
	 * @return the instWeb
	 */
	public String getInstWeb() {
		return instWeb;
	}

	/**
	 * @param instWeb the instWeb to set
	 */
	public void setInstWeb(String instWeb) {
		this.instWeb = instWeb;
	}

	/**
	 * @return the hostWeb
	 */
	public String getHostWeb() {
		return hostWeb;
	}

	/**
	 * @param hostWeb the hostWeb to set
	 */
	public void setHostWeb(String hostWeb) {
		this.hostWeb = hostWeb;
	}

	/**
	 * @return the datoMobi
	 */
	public String getDatoMobi() {
		return datoMobi;
	}

	/**
	 * @param datoMobi the datoMobi to set
	 */
	public void setDatoMobi(String datoMobi) {
		this.datoMobi = datoMobi;
	}

	/**
	 * @return the tablaAfec
	 */
	public String getTablaAfec() {
		return tablaAfec;
	}

	/**
	 * @param tablaAfec the tablaAfec to set
	 */
	public void setTablaAfec(String tablaAfec) {
		this.tablaAfec = tablaAfec;
	}

	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}

	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}

	/**
	 * @return the codOper
	 */
	public String getCodOper() {
		return codOper;
	}

	/**
	 * @param codOper the codOper to set
	 */
	public void setCodOper(String codOper) {
		this.codOper = codOper;
	}
}

