/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanSessionSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author ORO000260
 *
 */
@Component
@Scope(value = "session")
public class BeanSessionSPID implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7375225808525674087L;

	/**
	 * ultima fecha de operacion del modulo SPID
	 */
	private String fechaOperacion;
	
	/**
	 * clave Institucion central (Santander)
	 */
	private String cveInstitucion;
	
	/**
	 * @return the fechaOperacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 * @param fechaOperacion the fechaOperacion to set
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 * @return the cveInstitucion
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}
	/**
	 * @param cveInstitucion the cveInstitucion to set
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}
}