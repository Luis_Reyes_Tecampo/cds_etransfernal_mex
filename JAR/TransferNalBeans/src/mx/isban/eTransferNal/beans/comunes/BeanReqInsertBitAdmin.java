/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqInsertBitAdmin.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

public class BeanReqInsertBitAdmin implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1815610150130107096L;

	/**
	 * Campo fecha operacion
	 */
	private String fchOper;
	
	/**
	 * Campo usuario operacion
	 */
	private String userOper;
	
	/**
	 * Campo tipo de operacion
	 */
	private String tipoOper;
	
	/**
	 * Campo token
	 */
	private String idToken;
	
	/**
	 * Campo fecha de actualizacion
	 */
	private String fchAct;
	
	/**
	 * Campo hora de actualizacion
	 */
	private String horaAct;
	
	/**
	 * Campo de valor anterior
	 */
	private String valAnt;
	
	/**
	 * Campo de valor nuevo
	 */
	private String valNvo;
	
	/**
	 * Campo de comentarios
	 */
	private String comentarios;
	
	/**
	 * agrupacion de campos generales
	 */
	private BeanReqInsertBitAdminDos beanReqInsertBitAdminDos;
	
	/**
	 * @return the beanReqInsertBitAdminDos
	 */
	public BeanReqInsertBitAdminDos getBeanReqInsertBitAdminDos() {
		return beanReqInsertBitAdminDos;
	}

	/**
	 * @param beanReqInsertBitAdminDos the beanReqInsertBitAdminDos to set
	 */
	public void setBeanReqInsertBitAdminDos(BeanReqInsertBitAdminDos beanReqInsertBitAdminDos) {
		this.beanReqInsertBitAdminDos = beanReqInsertBitAdminDos;
	}

	/**
	 * @return the fchOper
	 */
	public String getFchOper() {
		return fchOper;
	}

	/**
	 * @param fchOper the fchOper to set
	 */
	public void setFchOper(String fchOper) {
		this.fchOper = fchOper;
	}

	/**
	 * @return the userOper
	 */
	public String getUserOper() {
		return userOper;
	}

	/**
	 * @param userOper the userOper to set
	 */
	public void setUserOper(String userOper) {
		this.userOper = userOper;
	}

	/**
	 * @return the tipoOper
	 */
	public String getTipoOper() {
		return tipoOper;
	}

	/**
	 * @param tipoOper the tipoOper to set
	 */
	public void setTipoOper(String tipoOper) {
		this.tipoOper = tipoOper;
	}

	/**
	 * @return the idToken
	 */
	public String getIdToken() {
		return idToken;
	}

	/**
	 * @param idToken the idToken to set
	 */
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	/**
	 * @return the fchAct
	 */
	public String getFchAct() {
		return fchAct;
	}

	/**
	 * @param fchAct the fchAct to set
	 */
	public void setFchAct(String fchAct) {
		this.fchAct = fchAct;
	}

	/**
	 * @return the horaAct
	 */
	public String getHoraAct() {
		return horaAct;
	}

	/**
	 * @param horaAct the horaAct to set
	 */
	public void setHoraAct(String horaAct) {
		this.horaAct = horaAct;
	}

	/**
	 * @return the valAnt
	 */
	public String getValAnt() {
		return valAnt;
	}

	/**
	 * @param valAnt the valAnt to set
	 */
	public void setValAnt(String valAnt) {
		this.valAnt = valAnt;
	}

	/**
	 * @return the valNvo
	 */
	public String getValNvo() {
		return valNvo;
	}

	/**
	 * @param valNvo the valNvo to set
	 */
	public void setValNvo(String valNvo) {
		this.valNvo = valNvo;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	
}
