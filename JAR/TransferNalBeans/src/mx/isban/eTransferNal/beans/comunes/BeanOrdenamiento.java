/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanOrdenamiento.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

/**
 * @author ORO000260
 *
 */
public class BeanOrdenamiento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 121095156491297113L;

	/**
	 * Nombre del campo para ordenacion
	 */
	private String sortField;
	
	/**
	 * Campo de tipo de ordenacion (ASC, DESC)
	 */
	private String sortType;

	/**
	 * @return the sortField
	 */
	public String getSortField() {
		return sortField;
	}

	/**
	 * @param sortField the sortField to set
	 */
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	/**
	 * @return the sortType
	 */
	public String getSortType() {
		return sortType;
	}

	/**
	 * @param sortType the sortType to set
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
}
