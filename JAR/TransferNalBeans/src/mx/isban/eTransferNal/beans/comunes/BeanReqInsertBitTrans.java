package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

public class BeanReqInsertBitTrans implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2683251085796607962L;
	
	/**
	 * Propiedad del tipo BeanDatosOpe que almacena el valor de datosOpe
	 */
	private BeanDatosOpe datosOpe;
	

	/**
	 * Propiedad del tipo BeanDatosGenerales que almacena el valor de datosGenerales
	 */
	private BeanDatosGenerales datosGenerales;

	public BeanReqInsertBitTrans() {
		super();
		this.datosGenerales = new BeanDatosGenerales();
		this.datosOpe = new BeanDatosOpe();
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datosOpe
	 * @return datosOpe Objeto del tipo BeanDatosOpe
	 */
	public BeanDatosOpe getDatosOpe() {
		return datosOpe;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datosOpe
	 * @param datosOpe del tipo BeanDatosOpe
	 */
	public void setDatosOpe(BeanDatosOpe datosOpe) {
		this.datosOpe = datosOpe;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datosGenerales
	 * @return datosGenerales Objeto del tipo BeanDatosGenerales
	 */
	public BeanDatosGenerales getDatosGenerales() {
		return datosGenerales;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datosGenerales
	 * @param datosGenerales del tipo BeanDatosGenerales
	 */
	public void setDatosGenerales(BeanDatosGenerales datosGenerales) {
		this.datosGenerales = datosGenerales;
	}
	
	
	


}
