package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class BeanDatosGenerales implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5311183460243796358L;
	/**
	 * Propiedad del tipo String que almacena el valor de idToken
	 */
	private String idToken;	
	/**
	 * Propiedad del tipo String que almacena el valor de dirIp
	 */
	private String dirIp;
	/**
	 * Propiedad del tipo String que almacena el valor de aplicacionCanal
	 */
	private String aplicacionCanal;
	/**
	 * Propiedad del tipo String que almacena el valor de instanciaWeb
	 */
	private String instanciaWeb;
	/**
	 * Propiedad del tipo String que almacena el valor de hostWeb
	 */
	private String hostWeb;
	/**
	 * Propiedad del tipo String que almacena el valor de idSesion
	 */
	private String idSesion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de codCliente
	 */
	private String codCliente;
	/**
	 * Propiedad del tipo String que almacena el valor de contrato
	 */
	private String contrato;
	/**
	 * Propiedad del tipo String que almacena el valor de numeroTitulos
	 */
	private String numeroTitulos;
	/**
	 * Propiedad del tipo String que almacena el valor de fchProgramada
	 */
	private String fchProgramada;
	/**
	 * Propiedad del tipo String que almacena el valor de fchAplica
	 */
	private String fchAplica;
	/**
	 * Propiedad del tipo String que almacena el valor de fchAct
	 */
	private String fchAct;
	/**
	 * Propiedad del tipo String que almacena el valor de horaAct
	 */
	private String horaAct;
	/**
	 * Propiedad del tipo String que almacena el valor de nombreArchivo
	 */
	private String nombreArchivo;
	
	/**
	 * Metodo get que sirve para obtener la propiedad idToken
	 * @return idToken Objeto del tipo String
	 */
	public String getIdToken() {
		if(null == idToken || StringUtils.EMPTY.equals(idToken)) {
			return "0";
		}
		return idToken;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad idToken
	 * @param idToken del tipo String
	 */
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad dirIp
	 * @return dirIp Objeto del tipo String
	 */
	public String getDirIp() {
		return dirIp;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad dirIp
	 * @param dirIp del tipo String
	 */
	public void setDirIp(String dirIp) {
		this.dirIp = dirIp;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad aplicacionCanal
	 * @return aplicacionCanal Objeto del tipo String
	 */
	public String getAplicacionCanal() {
		return aplicacionCanal;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad aplicacionCanal
	 * @param aplicacionCanal del tipo String
	 */
	public void setAplicacionCanal(String aplicacionCanal) {
		this.aplicacionCanal = aplicacionCanal;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad instanciaWeb
	 * @return instanciaWeb Objeto del tipo String
	 */
	public String getInstanciaWeb() {
		return instanciaWeb;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad instanciaWeb
	 * @param instanciaWeb del tipo String
	 */
	public void setInstanciaWeb(String instanciaWeb) {
		this.instanciaWeb = instanciaWeb;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad hostWeb
	 * @return hostWeb Objeto del tipo String
	 */
	public String getHostWeb() {
		return hostWeb;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad hostWeb
	 * @param hostWeb del tipo String
	 */
	public void setHostWeb(String hostWeb) {
		this.hostWeb = hostWeb;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad idSesion
	 * @return idSesion Objeto del tipo String
	 */
	public String getIdSesion() {
		return idSesion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad idSesion
	 * @param idSesion del tipo String
	 */
	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad codCliente
	 * @return codCliente Objeto del tipo String
	 */
	public String getCodCliente() {
		return codCliente;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codCliente
	 * @param codCliente del tipo String
	 */
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad contrato
	 * @return contrato Objeto del tipo String
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad contrato
	 * @param contrato del tipo String
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numeroTitulos
	 * @return numeroTitulos Objeto del tipo String
	 */
	public String getNumeroTitulos() {
		if(null == numeroTitulos || StringUtils.EMPTY.equals(numeroTitulos)) {
			return "0";
		}
		return numeroTitulos;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numeroTitulos
	 * @param numeroTitulos del tipo String
	 */
	public void setNumeroTitulos(String numeroTitulos) {
		this.numeroTitulos = numeroTitulos;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchProgramada
	 * @return fchProgramada Objeto del tipo String
	 */
	public String getFchProgramada() {
		return fchProgramada;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchProgramada
	 * @param fchProgramada del tipo String
	 */
	public void setFchProgramada(String fchProgramada) {
		this.fchProgramada = fchProgramada;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchAplica
	 * @return fchAplica Objeto del tipo String
	 */
	public String getFchAplica() {
		return fchAplica;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchAplica
	 * @param fchAplica del tipo String
	 */
	public void setFchAplica(String fchAplica) {
		this.fchAplica = fchAplica;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchAct
	 * @return fchAct Objeto del tipo String
	 */
	public String getFchAct() {
		return fchAct;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchAct
	 * @param fchAct del tipo String
	 */
	public void setFchAct(String fchAct) {
		this.fchAct = fchAct;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad horaAct
	 * @return horaAct Objeto del tipo String
	 */
	public String getHoraAct() {
		return horaAct;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaAct
	 * @param horaAct del tipo String
	 */
	public void setHoraAct(String horaAct) {
		this.horaAct = horaAct;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad nombreArchivo
	 * @return nombreArchivo Objeto del tipo String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad nombreArchivo
	 * @param nombreArchivo del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	
}
