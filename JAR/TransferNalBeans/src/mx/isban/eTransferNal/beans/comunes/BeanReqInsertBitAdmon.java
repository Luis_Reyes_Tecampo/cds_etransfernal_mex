package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

public class BeanReqInsertBitAdmon implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -825054287243776012L;

	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoOperacion
	 */
	private String tipoOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de valAnterior
	 */
	private String valAnterior;
	
	/**
	 * Propiedad del tipo String que almacena el valor de valNuevo
	 */
	private String valNuevo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de comentarios
	 */
	private String comentarios;

	/**
	 * Propiedad del tipo String que almacena el valor de datoFijo
	 */
	private String datoFijo;

	/**
	 * Propiedad del tipo String que almacena el valor de servTran
	 */
	private String servTran;
	
	/**
	 * Propiedad del tipo String que almacena el valor de datoMod
	 */
	private String datoMod;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tablaAfectada
	 */
	private String tablaAfectada;
	
	/**
	 * Propiedad del tipo String que almacena el valor de codOperacion
	 */
	private String codOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de canalAplica
	 */
	private String canalAplica;

	/**
	 * Metodo get que sirve para obtener la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fechaOperacion
	 * @param fechaOperacion del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoOperacion
	 * @return tipoOperacion Objeto del tipo String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoOperacion
	 * @param tipoOperacion del tipo String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad valAnterior
	 * @return valAnterior Objeto del tipo String
	 */
	public String getValAnterior() {
		return valAnterior;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad valAnterior
	 * @param valAnterior del tipo String
	 */
	public void setValAnterior(String valAnterior) {
		this.valAnterior = valAnterior;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad valNuevo
	 * @return valNuevo Objeto del tipo String
	 */
	public String getValNuevo() {
		return valNuevo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad valNuevo
	 * @param valNuevo del tipo String
	 */
	public void setValNuevo(String valNuevo) {
		this.valNuevo = valNuevo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad comentarios
	 * @return comentarios Objeto del tipo String
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad comentarios
	 * @param comentarios del tipo String
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datoFijo
	 * @return datoFijo Objeto del tipo String
	 */
	public String getDatoFijo() {
		return datoFijo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datoFijo
	 * @param datoFijo del tipo String
	 */
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad servTran
	 * @return servTran Objeto del tipo String
	 */
	public String getServTran() {
		return servTran;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad servTran
	 * @param servTran del tipo String
	 */
	public void setServTran(String servTran) {
		this.servTran = servTran;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datoMod
	 * @return datoMod Objeto del tipo String
	 */
	public String getDatoMod() {
		return datoMod;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datoMod
	 * @param datoMod del tipo String
	 */
	public void setDatoMod(String datoMod) {
		this.datoMod = datoMod;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tablaAfectada
	 * @return tablaAfectada Objeto del tipo String
	 */
	public String getTablaAfectada() {
		return tablaAfectada;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tablaAfectada
	 * @param tablaAfectada del tipo String
	 */
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad codOperacion
	 * @return codOperacion Objeto del tipo String
	 */
	public String getCodOperacion() {
		return codOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad codOperacion
	 * @param codOperacion del tipo String
	 */
	public void setCodOperacion(String codOperacion) {
		this.codOperacion = codOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad canalAplica
	 * @return canalAplica Objeto del tipo String
	 */
	public String getCanalAplica() {
		return canalAplica;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad canalAplica
	 * @param canalAplica del tipo String
	 */
	public void setCanalAplica(String canalAplica) {
		this.canalAplica = canalAplica;
	}

}
