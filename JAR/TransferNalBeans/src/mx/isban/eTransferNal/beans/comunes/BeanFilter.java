/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanFilter.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.comunes;

import java.io.Serializable;

/**
 * @author ORO000260
 *
 */
public class BeanFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1804105222958650030L;
	
	/**
	 * Nombre del campo para fltrar
	 */
	private String field;
	
	/**
	 * Valor a utilizar para filtrar en un campo especifico
	 */
	private String fieldValue;

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the fieldValue
	 */
	public String getFieldValue() {
		return fieldValue;
	}

	/**
	 * @param fieldValue the fieldValue to set
	 */
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

}
