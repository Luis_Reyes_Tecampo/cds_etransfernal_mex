/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonCargaArchCanContig.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:09:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los datos
 * de retorno de la funcionalidad del monitor de Carga de Archivos
 * historicos
**/

public class BeanResMonCargaArchCanContig implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad que almacena la abstraccion de una pagina*/
   private BeanPaginador paginador;
   /**Propiedad que almacena la lista de registros del monitor de carga
   de archivos historicos*/
   private List<BeanMonCargaArchCanConting> listBeanMonCargaArchCanConting;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError = "";
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError = "";
   /**Propiedad privada del tipo String que almacena el tipo de error*/
   private String tipoError = "";
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad paginador
   * @return paginador Objeto del tipo BeanPaginador
   */
   public BeanPaginador getPaginador(){
      return paginador;
   }
   /**
   *Modifica el valor de la propiedad paginador
   * @param paginador Objeto del tipo BeanPaginador
   */
   public void setPaginador(BeanPaginador paginador){
      this.paginador=paginador;
   }

   /**
    *Metodo get que sirve para obtener el valor de la propiedad codError
    * @return codError Objeto del tipo String
    */
    public String getCodError(){
       return codError;
    }
    /**
    *Modifica el valor de la propiedad codError
    * @param codError Objeto del tipo String
    */
    public void setCodError(String codError){
       this.codError=codError;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad msgError
    * @return msgError Objeto del tipo String
    */
    public String getMsgError(){
       return msgError;
    }
    /**
    *Modifica el valor de la propiedad msgError
    * @param msgError Objeto del tipo String
    */
    public void setMsgError(String msgError){
       this.msgError=msgError;
    }

   
   /**
    *Metodo get que sirve para obtener el valor del tipo de error
    * @return tipoError Objeto del tipo String
    */
	 public String getTipoError() {
		return tipoError;
	 }
	 /**
   *Modifica el valor de la propiedad tipoError
   * @param tipoError Objeto del tipo String
   */
	 public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	 }
	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanMonCargaArchCanConting
	 * @return List<BeanMonCargaArchCanConting> Objeto de tipo @see List<BeanMonCargaArchCanConting>
	 */
	public List<BeanMonCargaArchCanConting> getListBeanMonCargaArchCanConting() {
		return listBeanMonCargaArchCanConting;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanMonCargaArchCanConting
	 * @param listBeanMonCargaArchCanConting Objeto de tipo @see List<BeanMonCargaArchCanConting>
	 */
	public void setListBeanMonCargaArchCanConting(
			List<BeanMonCargaArchCanConting> listBeanMonCargaArchCanConting) {
		this.listBeanMonCargaArchCanConting = listBeanMonCargaArchCanConting;
	}



}
