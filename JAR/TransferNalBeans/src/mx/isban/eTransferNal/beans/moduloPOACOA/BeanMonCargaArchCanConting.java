/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMonCargaArchCanConting.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos del monitor de
 * carga de archivos
**/

public class BeanMonCargaArchCanConting  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreArchivo
	 */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	/**
	 * Propiedad del tipo String que almacena el valor de numPagos
	 */
	private String numPagos;
	/**
	 * Propiedad del tipo String que almacena el valor de numPagosErr
	 */
	private String numPagosErr;
	/**
	 * Propiedad del tipo String que almacena el valor de totalMonto
	 */
	private String totalMonto;
	/**
	 * Propiedad del tipo String que almacena el valor de totalMontoErr
	 */
	private String totalMontoErr;
	/**
	 * Metodo get que obtiene el valor de la propiedad nombreArchivo
	 * @return String Objeto de tipo @see String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreArchivo
	 * @param nombreArchivo Objeto de tipo @see String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatus
	 * @return String Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatus
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numPagos
	 * @return String Objeto de tipo @see String
	 */
	public String getNumPagos() {
		return numPagos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numPagos
	 * @param numPagos Objeto de tipo @see String
	 */
	public void setNumPagos(String numPagos) {
		this.numPagos = numPagos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numPagosErr
	 * @return String Objeto de tipo @see String
	 */
	public String getNumPagosErr() {
		return numPagosErr;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numPagosErr
	 * @param numPagosErr Objeto de tipo @see String
	 */
	public void setNumPagosErr(String numPagosErr) {
		this.numPagosErr = numPagosErr;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalMonto
	 * @return String Objeto de tipo @see String
	 */
	public String getTotalMonto() {
		return totalMonto;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalMonto
	 * @param totalMonto Objeto de tipo @see String
	 */
	public void setTotalMonto(String totalMonto) {
		this.totalMonto = totalMonto;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalMontoErr
	 * @return String Objeto de tipo @see String
	 */
	public String getTotalMontoErr() {
		return totalMontoErr;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalMontoErr
	 * @param totalMontoErr Objeto de tipo @see String
	 */
	public void setTotalMontoErr(String totalMontoErr) {
		this.totalMontoErr = totalMontoErr;
	}
	

  



}
