package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

public class BeanResConsCanales  implements Serializable {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7039107247448067697L;

	/**Propiedad que almacena la abstraccion de una pagina*/
	private BeanPaginador paginador;
	/**
	 * Propiedad del tipo List<BeanResCanal> que almacena el valor de beanResCanalList
	 */
	private List<BeanCanal> beanResCanalList = null;
	
	/**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError = "";
	/**Propiedad privada del tipo String que almacena el codigo de mensaje
	de error*/
	private String msgError = "";
	/**Propiedad privada del tipo String que almacena el tipo de error*/
	private String tipoError = "";
	     
	/**
	*Metodo get que sirve para obtener el valor de la propiedad paginador
	* @return paginador Objeto del tipo BeanPaginador
	*/
	public BeanPaginador getPaginador(){
		return paginador;
	}
	/**
	*Modifica el valor de la propiedad paginador
	* @param paginador Objeto del tipo BeanPaginador
	*/
	public void setPaginador(BeanPaginador paginador){
		this.paginador=paginador;
	}
	
	/**
	*Metodo get que sirve para obtener el valor de la propiedad codError
	* @return codError Objeto del tipo String
	*/
	public String getCodError(){
	   return codError;
	}
	/**
	*Modifica el valor de la propiedad codError
	* @param codError Objeto del tipo String
	*/
	public void setCodError(String codError){
	   this.codError=codError;
	}
	/**
	*Metodo get que sirve para obtener el valor de la propiedad msgError
	* @return msgError Objeto del tipo String
	*/
	public String getMsgError(){
	   return msgError;
	}
	/**
	*Modifica el valor de la propiedad msgError
	* @param msgError Objeto del tipo String
	*/
	    public void setMsgError(String msgError){
	       this.msgError=msgError;
	    }
	
	   
	   /**
	*Metodo get que sirve para obtener el valor del tipo de error
	* @return tipoError Objeto del tipo String
	*/
	 public String getTipoError() {
		return tipoError;
	 }
	 /**
	   *Modifica el valor de la propiedad tipoError
	   * @param tipoError Objeto del tipo String
	   */
	 public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	 }
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResCanalList
	 * @return List<BeanResCanal> Objeto de tipo @see List<BeanResCanal>
	 */
	public List<BeanCanal> getBeanResCanalList() {
		return beanResCanalList;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResCanalList
	 * @param beanResCanalList Objeto de tipo @see List<BeanResCanal>
	 */
	public void setBeanResCanalList(List<BeanCanal> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}
	 
}
