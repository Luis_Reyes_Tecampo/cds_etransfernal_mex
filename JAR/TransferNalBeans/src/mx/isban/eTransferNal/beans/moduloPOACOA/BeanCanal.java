/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResCanal.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author ooamador
 *
 */
public class BeanCanal extends BeanResBase implements Serializable{

	/**
	 * Serial numero
	 */
	private static final long serialVersionUID = 1L;

	/** Identificador canal. */
	private String idCanal;
	
	/** Nombre Canal*/
	private String nombreCanal;
	
	/** Estatus canal */
	private boolean estatusCanal;
	
	/** bandera renglon */
	private boolean banderaRenglon;

	/**
	 * Devuelve el valor de idCanal
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * Modifica el valor de idCanal
	 * @param idCanal the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * Devuelve el valor de nombreCanal
	 * @return the nombreCanal
	 */
	public String getNombreCanal() {
		return nombreCanal;
	}

	/**
	 * Modifica el valor de nombreCanal
	 * @param nombreCanal the nombreCanal to set
	 */
	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}

	/**
	 * Devuelve el valor de estatusCanal
	 * @return the estatusCanal
	 */
	public boolean isEstatusCanal() {
		return estatusCanal;
	}

	/**
	 * Modifica el valor de estatusCanal
	 * @param estatusCanal the estatusCanal to set
	 */
	public void setEstatusCanal(boolean estatusCanal) {
		this.estatusCanal = estatusCanal;
	}

	/**
	 * Devuelve el valor de banderaRenglon
	 * @return the banderaRenglon
	 */
	public boolean isBanderaRenglon() {
		return banderaRenglon;
	}

	/**
	 * Modifica el valor de banderaRenglon
	 * @param banderaRenglon the banderaRenglon to set
	 */
	public void setBanderaRenglon(boolean banderaRenglon) {
		this.banderaRenglon = banderaRenglon;
	}

}
