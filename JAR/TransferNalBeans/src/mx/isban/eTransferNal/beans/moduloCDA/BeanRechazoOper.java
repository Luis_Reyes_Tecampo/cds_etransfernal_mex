/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanRechazoOper.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2019 04:40:13 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.beans.ArchitechSessionBean;

/**
 * Clase Bean que forma el bean para realizar el rechazo de una operacion,
 * de las cuentas alternas o principales, esto para que no se pasen los parametros por separado.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */

public class  BeanRechazoOper  implements Serializable {

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8584294343325505007L;
	
	/** variable uno *. */
	private BeanReqTranSpeiRec beanReqTranSpeiRec;
	
	/**  variable de sesion*. */
	private ArchitechSessionBean session;
	
	/** historico *. */
	private boolean historico;	
	
	/** lista de datos *. */
	private List<BeanTranSpeiRecOper> listBeanTranSpeiRec;
	
	/** objeto de regreso*. */
	private BeanResConsTranSpeiRec beanResConsTranSpeiRec;
	
	/**
	 * Obtener el objeto: bean req tran spei rec.
	 *
	 * @return the beanReqTranSpeiRec
	 */
	public BeanReqTranSpeiRec getBeanReqTranSpeiRec() {
		return beanReqTranSpeiRec;
	}
	
	/**
	 * Definir el objeto: bean req tran spei rec.
	 *
	 * @param beanReqTranSpeiRec the beanReqTranSpeiRec to set
	 */
	public void setBeanReqTranSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec) {
		this.beanReqTranSpeiRec = beanReqTranSpeiRec;
	}
	
	/**
	 * Obtener el objeto: session.
	 *
	 * @return the session
	 */
	public ArchitechSessionBean getSession() {
		return session;
	}
	
	/**
	 * Definir el objeto: session.
	 *
	 * @param session the session to set
	 */
	public void setSession(ArchitechSessionBean session) {
		this.session = session;
	}
	
	/**
	 * Verificar si historico.
	 *
	 * @return the historico
	 */
	public boolean isHistorico() {
		return historico;
	}
	
	/**
	 * Definir el objeto: historico.
	 *
	 * @param historico the historico to set
	 */
	public void setHistorico(boolean historico) {
		this.historico = historico;
	}
	
	/**
	 * Obtener el objeto: list bean tran spei rec.
	 *
	 * @return the listBeanTranSpeiRec
	 */
	public List<BeanTranSpeiRecOper> getListBeanTranSpeiRec() {
		return listBeanTranSpeiRec;
	}
	
	/**
	 * Definir el objeto: list bean tran spei rec.
	 *
	 * @param listBeanTranSpeiRec the listBeanTranSpeiRec to set
	 */
	public void setListBeanTranSpeiRec(List<BeanTranSpeiRecOper> listBeanTranSpeiRec) {
		this.listBeanTranSpeiRec = listBeanTranSpeiRec;
	}
	
	/**
	 * Obtener el objeto: bean res cons tran spei rec.
	 *
	 * @return the beanResConsTranSpeiRec
	 */
	public BeanResConsTranSpeiRec getBeanResConsTranSpeiRec() {
		return beanResConsTranSpeiRec;
	}
	
	/**
	 * Definir el objeto: bean res cons tran spei rec.
	 *
	 * @param beanResConsTranSpeiRec the beanResConsTranSpeiRec to set
	 */
	public void setBeanResConsTranSpeiRec(BeanResConsTranSpeiRec beanResConsTranSpeiRec) {
		this.beanResConsTranSpeiRec = beanResConsTranSpeiRec;
	}
	
		
	

}
