/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqConsMovCdaFiltrosParteDos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   08/01/201419:35:31 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Consulta de Movimientos CDA
**/
public class BeanReqConsMovCdaFiltrosParteDos implements Serializable{

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 1417936164409679638L;
	/**Propiedad del tipo @see String que almacena la cve de rastreo*/
	   private String cveRastreo;
	   /**Propiedad del tipo @see String que almacena la referencia de
	   transfer*/
	   private String refTransfer;
	   /**Propiedad del tipo @see String que almacena la cuenta del beneficiario*/
	   private String ctaBeneficiario;
	   /**Propiedad del tipo @See String que almacena el monto pago*/
	   private String montoPago;
	   /**Propiedad que indica si se busca operaciones en contingencia*/
	   private boolean opeContingencia;
	   /**Propiedad del tipo @see String que indica si se busca operaciones
	   en contingencia*/
	   private String estatus;
	   
	   /**
	    *Metodo get que sirve para obtener el valor de la propiedad cveRastreo
	    * @return cveRastreo Objeto del tipo String
	    */
	    public String getCveRastreo(){
	       return cveRastreo;
	    }
	    /**
	    *Modifica el valor de la propiedad cveRastreo
	    * @param cveRastreo Objeto del tipo String
	    */
	    public void setCveRastreo(String cveRastreo){
	       this.cveRastreo=cveRastreo;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad refTransfer
	    * @return refTransfer Objeto del tipo String
	    */
	    public String getRefTransfer(){
	       return refTransfer;
	    }
	    /**
	    *Modifica el valor de la propiedad refTransfer
	    * @param refTransfer Objeto del tipo String
	    */
	    public void setRefTransfer(String refTransfer){
	       this.refTransfer=refTransfer;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad ctaBeneficiario
	    * @return ctaBeneficiario Objeto del tipo String
	    */
	    public String getCtaBeneficiario(){
	       return ctaBeneficiario;
	    }
	    /**
	    *Modifica el valor de la propiedad ctaBeneficiario
	    * @param ctaBeneficiario Objeto del tipo String
	    */
	    public void setCtaBeneficiario(String ctaBeneficiario){
	       this.ctaBeneficiario=ctaBeneficiario;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad montoPago
	    * @return montoPago Objeto del tipo String
	    */
	    public String getMontoPago(){
	       return montoPago;
	    }
	    /**
	    *Modifica el valor de la propiedad montoPago
	    * @param montoPago Objeto del tipo String
	    */
	    public void setMontoPago(String montoPago){
	       this.montoPago=montoPago;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad opeContingencia
	    * @return opeContingencia Objeto del tipo String
	    */
	    public boolean getOpeContingencia(){
	       return opeContingencia;
	    }
	    /**
	    *Modifica el valor de la propiedad opeContingencia
	    * @param opeContingencia Objeto del tipo boolean
	    */
	    public void setOpeContingencia(boolean opeContingencia){
	       this.opeContingencia=opeContingencia;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad estatus
	    * @return estatus Objeto del tipo String
	    */
	    public String getEstatus(){
	       return estatus;
	    }
	    /**
	    *Modifica el valor de la propiedad estatus
	    * @param estatus Objeto del tipo String
	    */
	    public void setEstatus(String estatus){
	       this.estatus=estatus;
	    }
}
