/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOperacionBO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015 12:18 INDRA     ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que  forman el detalle
 * de recepcion de operaciones
 **/
public class BeanDetRecepOperacionBO extends BeanDetRecepOperacion implements Serializable  {

   
	     /**  Constante privada del Serial version*/
	private static final long serialVersionUID = -5610635674407185639L;
	
 
	 /**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de folioPaquete
	 */
	private String folioPaquete;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de rfcOrd
	 */
	private String rfcOrd;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de nombreOrd
	 */
	private String nombreOrd;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreo;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de cveRastreoOri
	 */
	private String cveRastreoOri;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de bancoRec
	 */
	private String bancoRec;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec
	 */
	private String numCuentaRec;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec2
	 */
	private String numCuentaRec2;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec
	 */
	private String tipoCuentaRec;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec2
	 */
	private String tipoCuentaRec2;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de rfcRec
	 */
	private String rfcRec;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de rfcRec2
	 */
	private String rfcRec2;
 
	 /**
	 * Propiedad del tipo String que almacena el valor de nombreRec
	 */
	private String nombreRec;
	  
	
	
	/**
		 * Metodo get que sirve para obtener el valor de la propiedad fchOperacion
		 * @return fchOperacion del tipo String
		 */
		public String getFchOperacion() {
			return fchOperacion;
		}
		/**
		 * Metodo set que permite modificar el parametro fchOperacion
		 * @param fchOperacion el fchOperacion a establecer
		 */
		public void setFchOperacion(String fchOperacion) {
			this.fchOperacion = fchOperacion;
		}
	/**
		 * Metodo get que sirve para obtener el valor de la propiedad folioPaquete
		 * @return folioPaquete del tipo String
		 */
		public String getFolioPaquete() {
			return folioPaquete;
		}
		/**
		 * Metodo set que permite modificar el parametro folioPaquete
		 * @param folioPaquete el folioPaquete a establecer
		 */
		public void setFolioPaquete(String folioPaquete) {
			this.folioPaquete = folioPaquete;
		}
	/**
	 * @return el rfcOrd
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}
	/**
	 * @param rfcOrd el rfcOrd a establecer
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}
	/**
	 * @return el nombreOrd
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * @param nombreOrd el nombreOrd a establecer
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}
	/**
	 * @return el cveRastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * @param cveRastreo el cveRastreo a establecer
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * @return el cveRastreoOri
	 */
	public String getCveRastreoOri() {
		return cveRastreoOri;
	}
	/**
	 * @param cveRastreoOri el cveRastreoOri a establecer
	 */
	public void setCveRastreoOri(String cveRastreoOri) {
		this.cveRastreoOri = cveRastreoOri;
	}
	/**
	 * @return el bancoRec
	 */
	public String getBancoRec() {
		return bancoRec;
	}
	/**
	 * @param bancoRec el bancoRec a establecer
	 */
	public void setBancoRec(String bancoRec) {
		this.bancoRec = bancoRec;
	}
	/**
	 * @return el numCuentaRec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}
	/**
	 * @param numCuentaRec el numCuentaRec a establecer
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	/**
	 * @return el numCuentaRec2
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}
	/**
	 * @param numCuentaRec2 el numCuentaRec2 a establecer
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}
	/**
	 * @return el tipoCuentaRec
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}
	/**
	 * @param tipoCuentaRec el tipoCuentaRec a establecer
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	/**
	 * @return el tipoCuentaRec2
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}
	/**
	 * @param tipoCuentaRec2 el tipoCuentaRec2 a establecer
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}
	/**
	 * @return el rfcRec
	 */
	public String getRfcRec() {
		return rfcRec;
	}
	/**
	 * @param rfcRec el rfcRec a establecer
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}
	/**
	 * @return el rfcRec2
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}
	/**
	 * @param rfcRec2 el rfcRec2 a establecer
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}
	/**
	 * @return el nombreRec
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	/**
	 * @param nombreRec el nombreRec a establecer
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}
	
	
	  
}
