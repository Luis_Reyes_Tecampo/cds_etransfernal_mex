/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015 13:32 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * Clase la cual se encapsulan los datos que se utilizan en administracion e saldos.
 */
public class BeanAdministraSaldoBO implements Serializable{

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 4650717870353351900L;
	
	/**  Propiedad del tipo String que almacena estatus de conexion. */
	private String semaforo;
	
	/**  Propiedad del tipo String que almacena el saldo principal. */
	private String saldoPrincipal;
	
	/** La variable que contiene informacion con respecto a: saldo principal count. */
	private String saldoPrincipalCount;
	
	/**  Propiedad del tipo String que almacena el saldo alterno. */
	private String saldoAlterno;
	
	/** La variable que contiene informacion con respecto a: saldo alterno count. */
	private String saldoAlternoCount;
	
	/**  Propiedad del tipo String que almacena el saldo calculado. */
	private String saldoCalculado;
	
	/** La variable que contiene informacion con respecto a: saldo calculado count. */
	private String saldoCalculadoCount;
	
	/**  Propiedad del tipo String que almacena el saldo no reservado. */
	private String saldoNoReservado;
	
	/** La variable que contiene informacion con respecto a: saldo no reservado count. */
	private String saldoNoReservadoCount;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones. */
	private String saldoDevoluciones;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones count. */
	private String saldoDevolucionesCount;
	
	/**  Propiedad del tipo  codError. */
	private String codError;
	
	/**  Propiedad del tipo  msgError. */
	private String msgError;
	
	/**  Propiedad del tipo mensaje de error. */
	private String tipoError;
	
	
	
	/**
	 * Obtener el objeto: semaforo.
	 *
	 * @return el semaforo
	 */
	public String getSemaforo() {
		return semaforo;
	}
	
	/**
	 * Definir el objeto: semaforo.
	 *
	 * @param semaforo el semaforo a establecer
	 */
	public void setSemaforo(String semaforo) {
		this.semaforo = semaforo;
	}
	
	/**
	 * Obtener el objeto: saldo principal.
	 *
	 * @return el saldoPrincipal
	 */
	public String getSaldoPrincipal() {
		return saldoPrincipal;
	}
	
	/**
	 * Definir el objeto: saldo principal.
	 *
	 * @param saldoPrincipal el saldoPrincipal a establecer
	 */
	public void setSaldoPrincipal(String saldoPrincipal) {
		this.saldoPrincipal = saldoPrincipal;
	}
	
	/**
	 * Obtener el objeto: saldo alterno.
	 *
	 * @return el saldoAlterno
	 */
	public String getSaldoAlterno() {
		return saldoAlterno;
	}
	
	/**
	 * Definir el objeto: saldo alterno.
	 *
	 * @param saldoAlterno el saldoAlterno a establecer
	 */
	public void setSaldoAlterno(String saldoAlterno) {
		this.saldoAlterno = saldoAlterno;
	}
	
	/**
	 * Obtener el objeto: saldo calculado.
	 *
	 * @return el saldoCalculado
	 */
	public String getSaldoCalculado() {
		return saldoCalculado;
	}
	
	/**
	 * Definir el objeto: saldo calculado.
	 *
	 * @param saldoCalculado el saldoCalculado a establecer
	 */
	public void setSaldoCalculado(String saldoCalculado) {
		this.saldoCalculado = saldoCalculado;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado.
	 *
	 * @return el saldoNoReservado
	 */
	public String getSaldoNoReservado() {
		return saldoNoReservado;
	}
	
	/**
	 * Definir el objeto: saldo no reservado.
	 *
	 * @param saldoNoReservado el saldoNoReservado a establecer
	 */
	public void setSaldoNoReservado(String saldoNoReservado) {
		this.saldoNoReservado = saldoNoReservado;
	}
	
	/**
	 * Obtener el objeto: cod error.
	 *
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Definir el objeto: cod error.
	 *
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Obtener el objeto: msg error.
	 *
	 * @return el msgError
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * Definir el objeto: msg error.
	 *
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * Obtener el objeto: tipo error.
	 *
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	
	/**
	 * Definir el objeto: tipo error.
	 *
	 * @param tipoError el tipoError a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 * Obtener el objeto: saldo principal count.
	 *
	 * @return El objeto: saldo principal count
	 */
	public String getSaldoPrincipalCount() {
		return saldoPrincipalCount;
	}
	
	/**
	 * Definir el objeto: saldo principal count.
	 *
	 * @param saldoPrincipalCount El nuevo objeto: saldo principal count
	 */
	public void setSaldoPrincipalCount(String saldoPrincipalCount) {
		this.saldoPrincipalCount = saldoPrincipalCount;
	}
	
	/**
	 * Obtener el objeto: saldo alterno count.
	 *
	 * @return El objeto: saldo alterno count
	 */
	public String getSaldoAlternoCount() {
		return saldoAlternoCount;
	}
	
	/**
	 * Definir el objeto: saldo alterno count.
	 *
	 * @param saldoAlternoCount El nuevo objeto: saldo alterno count
	 */
	public void setSaldoAlternoCount(String saldoAlternoCount) {
		this.saldoAlternoCount = saldoAlternoCount;
	}
	
	/**
	 * Obtener el objeto: saldo calculado count.
	 *
	 * @return El objeto: saldo calculado count
	 */
	public String getSaldoCalculadoCount() {
		return saldoCalculadoCount;
	}
	
	/**
	 * Definir el objeto: saldo calculado count.
	 *
	 * @param saldoCalculadoCount El nuevo objeto: saldo calculado count
	 */
	public void setSaldoCalculadoCount(String saldoCalculadoCount) {
		this.saldoCalculadoCount = saldoCalculadoCount;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado count.
	 *
	 * @return El objeto: saldo no reservado count
	 */
	public String getSaldoNoReservadoCount() {
		return saldoNoReservadoCount;
	}
	
	/**
	 * Definir el objeto: saldo no reservado count.
	 *
	 * @param saldoNoReservadoCount El nuevo objeto: saldo no reservado count
	 */
	public void setSaldoNoReservadoCount(String saldoNoReservadoCount) {
		this.saldoNoReservadoCount = saldoNoReservadoCount;
	}
	
	/**
	 * Obtener el objeto: saldo devoluciones.
	 *
	 * @return El objeto: saldo devoluciones
	 */
	public String getSaldoDevoluciones() {
		return saldoDevoluciones;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones.
	 *
	 * @param saldoDevoluciones El nuevo objeto: saldo devoluciones
	 */
	public void setSaldoDevoluciones(String saldoDevoluciones) {
		this.saldoDevoluciones = saldoDevoluciones;
	}
	
	/**
	 * Obtener el objeto: saldo devoluciones count.
	 *
	 * @return El objeto: saldo devoluciones count
	 */
	public String getSaldoDevolucionesCount() {
		return saldoDevolucionesCount;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones count.
	 *
	 * @param saldoDevolucionesCount El nuevo objeto: saldo devoluciones count
	 */
	public void setSaldoDevolucionesCount(String saldoDevolucionesCount) {
		this.saldoDevolucionesCount = saldoDevolucionesCount;
	}
	
	

}
