package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanTranSpeiRecExt extends BeanTranSpeiRecExt2 implements Serializable{


	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2137607502481831845L;

	/**
	 * Propiedad del tipo String que almacena el valor de refeTransfer
	 */
	private String refeTransfer;
	
	/**
	 * Propiedad del tipo String que almacena el valor de agrupacion
	 */
	private String agrupacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveParaPago
	 */
	private String cveParaPago;

	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec2
	 */
	private String numCuentaRec2;
	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec2
	 */
	private String nombreRec2;
	/**
	 * Propiedad del tipo String que almacena el valor de rfcRec2
	 */
	private String rfcRec2;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec2
	 */
	private String tipoCuentaRec2;
	/**
	 * Propiedad del tipo String que almacena el valor de conceptoPago2
	 */
	private String conceptoPago2;

	/**
	 * Propiedad del tipo String que almacena el valor de conceptoPago1
	 */
	private String conceptoPago1;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaOrd
	 */
	private String tipoCuentaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec
	 */
	private String tipoCuentaRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreOrd
	 */
	private String nombreOrd;
	

	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec
	 */
	private String nombreRec;
	

	/**
	 * Metodo get que obtiene el valor de la propiedad refeTransfer
	 * @return refeTransfer Objeto de tipo @see String
	 */
	public String getRefeTransfer() {
		return refeTransfer;
	}
	/**
	 * Metodo que modifica el valor de la propiedad refeTransfer
	 * @param refeTransfer Objeto de tipo @see String
	 */
	public void setRefeTransfer(String refeTransfer) {
		this.refeTransfer = refeTransfer;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad agrupacion
	 * @return agrupacion Objeto de tipo @see String
	 */
	public String getAgrupacion() {
		return agrupacion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad agrupacion
	 * @param agrupacion Objeto de tipo @see String
	 */
	public void setAgrupacion(String agrupacion) {
		this.agrupacion = agrupacion;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveParaPago
	 * @return cveParaPago Objeto de tipo @see String
	 */
	public String getCveParaPago() {
		return cveParaPago;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveParaPago
	 * @param cveParaPago Objeto de tipo @see String
	 */
	public void setCveParaPago(String cveParaPago) {
		this.cveParaPago = cveParaPago;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numCuentaRec2
	 * @return numCuentaRec2 Objeto de tipo @see String
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numCuentaRec2
	 * @param numCuentaRec2 Objeto de tipo @see String
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombreRec2
	 * @return nombreRec2 Objeto de tipo @see String
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreRec2
	 * @param nombreRec2 Objeto de tipo @see String
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad rfcRec2
	 * @return rfcRec2 Objeto de tipo @see String
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad rfcRec2
	 * @param rfcRec2 Objeto de tipo @see String
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaRec2
	 * @return tipoCuentaRec2 Objeto de tipo @see String
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaRec2
	 * @param tipoCuentaRec2 Objeto de tipo @see String
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad conceptoPago2
	 * @return conceptoPago2 Objeto de tipo @see String
	 */
	public String getConceptoPago2() {
		return conceptoPago2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad conceptoPago2
	 * @param conceptoPago2 Objeto de tipo @see String
	 */
	public void setConceptoPago2(String conceptoPago2) {
		this.conceptoPago2 = conceptoPago2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad conceptoPago1
	 * @return conceptoPago1 Objeto de tipo @see String
	 */
	public String getConceptoPago1() {
		return conceptoPago1;
	}
	/**
	 * Metodo que modifica el valor de la propiedad conceptoPago1
	 * @param conceptoPago1 Objeto de tipo @see String
	 */
	public void setConceptoPago1(String conceptoPago1) {
		this.conceptoPago1 = conceptoPago1;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaOrd
	 * @return tipoCuentaOrd Objeto de tipo @see String
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaOrd
	 * @param tipoCuentaOrd Objeto de tipo @see String
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaRec
	 * @return tipoCuentaRec Objeto de tipo @see String
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaRec
	 * @param tipoCuentaRec Objeto de tipo @see String
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad nombreOrd
	 * @return nombreOrd Objeto de tipo @see String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreOrd
	 * @param nombreOrd Objeto de tipo @see String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombreRec
	 * @return nombreRec Objeto de tipo @see String
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreRec
	 * @param nombreRec Objeto de tipo @see String
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	
	
}
