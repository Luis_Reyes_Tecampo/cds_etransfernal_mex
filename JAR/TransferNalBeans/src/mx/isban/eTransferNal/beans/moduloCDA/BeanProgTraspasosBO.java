/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanProgTraspasosBO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   2/09/2015 20:32 INDRA     ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;
/**
 *Clase la cual se encapsulan los datos que se utilizan en programacion de traspasos
 **/
public class BeanProgTraspasosBO  implements Serializable{
	
	/**Constante privada del Serial version */
	private static final long serialVersionUID = -4859675256899973417L;
	/** Propiedad del tipo String para almacenar el importe*/
	private String importeProgTran;
	/** Propiedad del tipo String para almacenar la hora*/
	private String hora;
	/** Propiedad del tipo String para almacenar la descripcion*/
	private String descripcion;
	/** Propiedad del tipo  lista */
	private List<BeanHorarioTransferencia> listHorariosTr;
	/** Propiedad del tipo  codError */
	private String codError;
	/** Propiedad del tipo  msgError */
	private String msgError;
	/** Propiedad del tipo mensaje de error */
	private String tipoError;
	/**Propiedad que almacena la abstraccion de la paginacion*/
	private BeanPaginador paginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**
	 * @return el importeProgTran
	 */
	public String getImporteProgTran() {
		return importeProgTran;
	}
	/**
	 * @param importeProgTran el importeProgTran a establecer
	 */
	public void setImporteProgTran(String importeProgTran) {
		this.importeProgTran = importeProgTran;
	}
	/**
	 * @return el listHorariosTr
	 */
	public List<BeanHorarioTransferencia> getListHorariosTr() {
		return listHorariosTr;
	}
	/**
	 * @param listHorariosTr el listHorariosTr a establecer
	 */
	public void setListHorariosTr(List<BeanHorarioTransferencia> listHorariosTr) {
		this.listHorariosTr = listHorariosTr;
	}
	
	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return el msgError
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError el tipoError a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * @return el paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * @param paginador el paginador a establecer
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora el hora a establecer
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	
}
