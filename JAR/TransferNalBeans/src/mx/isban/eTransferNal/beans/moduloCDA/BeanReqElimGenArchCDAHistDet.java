/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqElimGenArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la funcionalidad de marcar como eliminado los registros
 * Generar Archivo CDA Historico Detalle
**/

public class BeanReqElimGenArchCDAHistDet implements Serializable {

   /**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
   /**Propiedad del tipo @see List<BeanElimGenArchCDAHistDet> que almacena
   el listado de registros a eliminar*/
   private List<BeanElimGenArchCDAHistDet> listBeanElimGenArchCDAHistDet;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanElimGenArchCDAHistDet
   * @return listBeanElimGenArchCDAHistDet Objeto del tipo List<BeanElimGenArchCDAHistDet>
   */
   public List<BeanElimGenArchCDAHistDet> getListBeanElimGenArchCDAHistDet(){
      return listBeanElimGenArchCDAHistDet;
   }
   /**
   *Modifica el valor de la propiedad listBeanElimGenArchCDAHistDet
   * @param listBeanElimGenArchCDAHistDet Objeto del tipo List<BeanElimGenArchCDAHistDet>
   */
   public void setListBeanElimGenArchCDAHistDet(List<BeanElimGenArchCDAHistDet> listBeanElimGenArchCDAHistDet){
      this.listBeanElimGenArchCDAHistDet=listBeanElimGenArchCDAHistDet;
   }



}
