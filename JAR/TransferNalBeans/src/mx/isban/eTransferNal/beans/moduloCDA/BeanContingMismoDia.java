/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResContingMismoDiaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:30:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  de Contingencia CDA Mismo Dia
**/

public class BeanContingMismoDia  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo @see String que almacena el nombre del archvo*/
   private String nomArchivo;
   /**Propiedad del tipo @see String que almacena el nombre del archvo original*/
   private String nomArchivoOrig;
   /**Propiedad del tipo @see String que almacena la fecha y la hora*/
   private String fechaHora;
   /**Propiedad del tipo @see String que almacena el usuario*/
   private String usuario;
   /**Propiedad del tipo @see String que almacena el estatus*/
   private String estatus;
   /**Propiedad del tipo @see String que almacena el numero de
   operaciones generadas*/
   private String numOpGeneradas;
   /**Propiedad del tipo @see String que almacena el numero de operaciones
   por generar*/
   private String numOpGenerar;
 
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomArchivo
   * @return nomArchivo Objeto del tipo String
   */
   public String getNomArchivo(){
      return nomArchivo;
   }
   /**
   *Modifica el valor de la propiedad nomArchivo
   * @param nomArchivo Objeto del tipo String
   */
   public void setNomArchivo(String nomArchivo){
      this.nomArchivo=nomArchivo;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad nomArchivoOrig
    * @return nomArchivoOrig Objeto del tipo String
    */
    public String getNomArchivoOrig(){
       return nomArchivoOrig;
    }
    /**
    *Modifica el valor de la propiedad nomArchivoOrig
    * @param nomArchivoOrig Objeto del tipo String
    */
    public void setNomArchivoOrig(String nomArchivoOrig){
       this.nomArchivoOrig=nomArchivoOrig;
    }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaHora
   * @return fechaHora Objeto del tipo String
   */
   public String getFechaHora(){
      return fechaHora;
   }
   /**
   *Modifica el valor de la propiedad fechaHora
   * @param fechaHora Objeto del tipo String
   */
   public void setFechaHora(String fechaHora){
      this.fechaHora=fechaHora;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad usuario
   * @return usuario Objeto del tipo String
   */
   public String getUsuario(){
      return usuario;
   }
   /**
   *Modifica el valor de la propiedad usuario
   * @param usuario Objeto del tipo String
   */
   public void setUsuario(String usuario){
      this.usuario=usuario;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad estatus
   * @return estatus Objeto del tipo String
   */
   public String getEstatus(){
      return estatus;
   }
   /**
   *Modifica el valor de la propiedad estatus
   * @param estatus Objeto del tipo String
   */
   public void setEstatus(String estatus){
      this.estatus=estatus;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numOpGeneradas
   * @return numOpGeneradas Objeto del tipo String
   */
   public String getNumOpGeneradas(){
      return numOpGeneradas;
   }
   /**
   *Modifica el valor de la propiedad numOpGeneradas
   * @param numOpGeneradas Objeto del tipo String
   */
   public void setNumOpGeneradas(String numOpGeneradas){
      this.numOpGeneradas=numOpGeneradas;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numOpGenerar
   * @return numOpGenerar Objeto del tipo String
   */
   public String getNumOpGenerar(){
      return numOpGenerar;
   }
   /**
   *Modifica el valor de la propiedad numOpGenerar
   * @param numOpGenerar Objeto del tipo String
   */
   public void setNumOpGenerar(String numOpGenerar){
      this.numOpGenerar=numOpGenerar;
   }



}
