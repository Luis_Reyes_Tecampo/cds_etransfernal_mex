/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanGeneralHoras.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/06/2019 07:04:37 PM Uriel Alfredo Botello R. Vector Creacion
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * The Class BeanGeneralHoras que contiene los datos generales de la tabla de horas SPEI.
 */
public class BeanGeneralHoras implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8187204715432526534L;
	
	/** The u version. */
	private String uVersion;
	
	/** The dif acuse aprobacion. */
	private String difAcuseAprobacion;
	
	/** The dif recepcion acuse. */
	private String difRecepcionAcuse;
	
	/** The dif captura recepcion. */
	private String difCapturaRecepcion;
	
	/** The tipo. */
	private String tipo;
	
	/** The tipo pago. */
	private String tipoPago;
	
	/** The sw act his ls. */
	private String swActHisLs;
	
	/** The sw act his en. */
	private String swActHisEn;
	
	/** The sw act his co. */
	private String swActHisCo;
	
	/** The sw calc interes. */
	private String swCalcInteres;
	
	/**
	 * Gets the u version.
	 *
	 * @return the uVersion
	 */
	public String getuVersion() {
		return uVersion;
	}
	
	/**
	 * Sets the u version.
	 *
	 * @param uVersion the uVersion to set
	 */
	public void setuVersion(String uVersion) {
		this.uVersion = uVersion;
	}
	
	/**
	 * Gets the dif acuse aprobacion.
	 *
	 * @return the difAcuseAprobacion
	 */
	public String getDifAcuseAprobacion() {
		return difAcuseAprobacion;
	}
	
	/**
	 * Sets the dif acuse aprobacion.
	 *
	 * @param difAcuseAprobacion the difAcuseAprobacion to set
	 */
	public void setDifAcuseAprobacion(String difAcuseAprobacion) {
		this.difAcuseAprobacion = difAcuseAprobacion;
	}
	
	/**
	 * Gets the dif recepcion acuse.
	 *
	 * @return the difRecepcionAcuse
	 */
	public String getDifRecepcionAcuse() {
		return difRecepcionAcuse;
	}
	
	/**
	 * Sets the dif recepcion acuse.
	 *
	 * @param difRecepcionAcuse the difRecepcionAcuse to set
	 */
	public void setDifRecepcionAcuse(String difRecepcionAcuse) {
		this.difRecepcionAcuse = difRecepcionAcuse;
	}
	
	/**
	 * Gets the dif captura recepcion.
	 *
	 * @return the difCapturaRecepcion
	 */
	public String getDifCapturaRecepcion() {
		return difCapturaRecepcion;
	}
	
	/**
	 * Sets the dif captura recepcion.
	 *
	 * @param difCapturaRecepcion the difCapturaRecepcion to set
	 */
	public void setDifCapturaRecepcion(String difCapturaRecepcion) {
		this.difCapturaRecepcion = difCapturaRecepcion;
	}
	
	/**
	 * Gets the tipo.
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Sets the tipo.
	 *
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Gets the tipo pago.
	 *
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	
	/**
	 * Sets the tipo pago.
	 *
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	/**
	 * Gets the sw act his ls.
	 *
	 * @return the swActHisLs
	 */
	public String getSwActHisLs() {
		return swActHisLs;
	}
	
	/**
	 * Sets the sw act his ls.
	 *
	 * @param swActHisLs the swActHisLs to set
	 */
	public void setSwActHisLs(String swActHisLs) {
		this.swActHisLs = swActHisLs;
	}
	
	/**
	 * Gets the sw act his en.
	 *
	 * @return the swActHisEn
	 */
	public String getSwActHisEn() {
		return swActHisEn;
	}
	
	/**
	 * Sets the sw act his en.
	 *
	 * @param swActHisEn the swActHisEn to set
	 */
	public void setSwActHisEn(String swActHisEn) {
		this.swActHisEn = swActHisEn;
	}
	
	/**
	 * Gets the sw act his co.
	 *
	 * @return the swActHisCo
	 */
	public String getSwActHisCo() {
		return swActHisCo;
	}
	
	/**
	 * Sets the sw act his co.
	 *
	 * @param swActHisCo the swActHisCo to set
	 */
	public void setSwActHisCo(String swActHisCo) {
		this.swActHisCo = swActHisCo;
	}
	
	/**
	 * Gets the sw calc interes.
	 *
	 * @return the swCalcInteres
	 */
	public String getSwCalcInteres() {
		return swCalcInteres;
	}
	
	/**
	 * Sets the sw calc interes.
	 *
	 * @param swCalcInteres the swCalcInteres to set
	 */
	public void setSwCalcInteres(String swCalcInteres) {
		this.swCalcInteres = swCalcInteres;
	}
	
}
