/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanElimGenArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:05:11 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular un registro de la funcionalidad
 * de Generar Archivo CDA Historico Detalle
 **/

public class BeanElimGenArchCDAHistDet implements Serializable {

	/**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = 2784474240120560099L;
	/**
	 * Propiedad del tipo @see String que almacena el idPeticion de la
	 * generacion del archivo
	 */
	private String idPeticion;
	/** Propiedad del tipo @see String que almacena la clave de rastreo */
	private String claveRastreo;
	/**
	 * Propiedad del tipo @see String que almacena el numero de cuenta del
	 * beneficiario
	 */
	private String numCuentaBeneficiario;
	/** Propiedad del tipo @see String que almacena el monto del pago */
	private String montoPago;
	/**
	 * Propiedad del tipo @see String que almacena el nombre de la institucion
	 * emisora
	 */
	private String nomInstEmisora;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad idPeticion
	 * 
	 * @return idPeticion Objeto del tipo String
	 */
	public String getIdPeticion() {
		return idPeticion;
	}

	/**
	 *Modifica el valor de la propiedad idPeticion
	 * 
	 * @param idPeticion
	 *            Objeto del tipo String
	 */
	public void setIdPeticion(String idPeticion) {
		this.idPeticion = idPeticion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveRastreo
	 * 
	 * @return claveRastreo Objeto del tipo String
	 */
	public String getClaveRastreo() {
		return claveRastreo;
	}

	/**
	 *Modifica el valor de la propiedad claveRastreo
	 * 
	 * @param claveRastreo
	 *            Objeto del tipo String
	 */
	public void setClaveRastreo(String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * numCuentaBeneficiario
	 * 
	 * @return numCuentaBeneficiario Objeto del tipo String
	 */
	public String getNumCuentaBeneficiario() {
		return numCuentaBeneficiario;
	}

	/**
	 *Modifica el valor de la propiedad numCuentaBeneficiario
	 * 
	 * @param numCuentaBeneficiario
	 *            Objeto del tipo String
	 */
	public void setNumCuentaBeneficiario(String numCuentaBeneficiario) {
		this.numCuentaBeneficiario = numCuentaBeneficiario;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad montoPago
	 * 
	 * @return montoPago Objeto del tipo String
	 */
	public String getMontoPago() {
		return montoPago;
	}

	/**
	 *Modifica el valor de la propiedad montoPago
	 * 
	 * @param montoPago
	 *            Objeto del tipo String
	 */
	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nomInstEmisora
	 * 
	 * @return nomInstEmisora Objeto del tipo String
	 */
	public String getNomInstEmisora() {
		return nomInstEmisora;
	}

	/**
	 *Modifica el valor de la propiedad nomInstEmisora
	 * 
	 * @param nomInstEmisora
	 *            Objeto del tipo String
	 */
	public void setNomInstEmisora(String nomInstEmisora) {
		this.nomInstEmisora = nomInstEmisora;
	}

}
