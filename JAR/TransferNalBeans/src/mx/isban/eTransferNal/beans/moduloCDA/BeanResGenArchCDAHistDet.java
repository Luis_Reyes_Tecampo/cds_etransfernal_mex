/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGenArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 13:53:51 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular la respuesta de la
 * generacion del archivo CDA historico detalle
**/

public class BeanResGenArchCDAHistDet  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError = "";
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError = "";
   /**Propiedad privada del tipo String que almacena el tipo de error*/
   private String tipoError;
   
   /**
    *Metodo get que sirve para obtener el valor de la propiedad codError
    * @return codError Objeto del tipo String
    */
    public String getCodError(){
       return codError;
    }
    /**
    *Modifica el valor de la propiedad codError
    * @param codError Objeto del tipo String
    */
    public void setCodError(String codError){
       this.codError=codError;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad msgError
    * @return msgError Objeto del tipo String
    */
    public String getMsgError(){
       return msgError;
    }
    /**
    *Modifica el valor de la propiedad msgError
    * @param msgError Objeto del tipo String
    */
    public void setMsgError(String msgError){
       this.msgError=msgError;
    }
    /**
     *Metodo get que sirve para obtener el valor del tipo de error
     * @return tipoError Objeto del tipo String
     */
 	 public String getTipoError() {
 		return tipoError;
 	 }
 	 /**
    *Modifica el valor de la propiedad tipoError
    * @param tipoError Objeto del tipo String
    */
 	 public void setTipoError(String tipoError) {
 		this.tipoError = tipoError;
 	 }

}
