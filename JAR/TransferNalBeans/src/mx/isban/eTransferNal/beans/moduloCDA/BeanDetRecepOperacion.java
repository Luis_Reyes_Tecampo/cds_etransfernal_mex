/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOperacionBO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015 16:48 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos que  forman el detalle
 * de recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 11/02/2019
 */
public class BeanDetRecepOperacion implements Serializable  {
	

	/**  Constante privada del Serial version. */
	private static final long serialVersionUID = -5069824337704957549L;
	
		
	/** La variable que contiene informacion con respecto a: nombre pantalla. */
	@Size(max =40)
	private String nombrePantalla;
	
	/** La variable que contiene informacion con respecto a: id bandera. */
	@NotNull
	private boolean idBandera;
	
	/** La variable que contiene informacion con respecto a: error. */
	@Valid
	private BeanResBase error;
	
	/** La variable que contiene informacion con respecto a: detalle gral. */
	@Valid
	private BeanDetRecepOpera detalleGral;
	
	/** La variable que contiene informacion con respecto a: detalle topo. */
	@Valid
	private BeanDetRecepOperaci detalleTopo;
	
	/** La variable que contiene informacion con respecto a: det estatus. */
	@Valid
	private BeanDetRecepOp detEstatus;
	
	/** La variable que contiene informacion con respecto a: recepcion. */
	@Valid
	private BeanDetRecOpRecep receptor;
	
	/** La variable que contiene informacion con respecto a: ordenante. */
	@Valid
	private BeanDetRecOpOrdent ordenante;
	
	/** La variable que contiene informacion con respecto a: bean cambios. */
	@Valid
	private BeanDetRecepcionOperaCambios beanCambios;
		
	/** La variable que contiene informacion con respecto a: codigo error. */
	@Size(max =8)
	private String codigoError;
	
	/**
	 * Instantiates a new bean det recep operacion.
	 */
	public BeanDetRecepOperacion() {
		super();
		this.beanCambios = new BeanDetRecepcionOperaCambios();
		this.detalleGral = new BeanDetRecepOpera();
		this.detalleTopo = new BeanDetRecepOperaci();
		this.detEstatus = new BeanDetRecepOp();
		this.error = new BeanResBase();
		this.ordenante = new BeanDetRecOpOrdent();
		this.receptor = new BeanDetRecOpRecep();
	}

	/**
	 * Obtener el objeto: error.
	 *
	 * @return El objeto: error
	 */
	public BeanResBase getError() {
		return error;
	}

	/**
	 * Definir el objeto: error.
	 *
	 * @param error El nuevo objeto: error
	 */
	public void setError(BeanResBase error) {
		this.error = error;
	}

	/**
	 * Obtener el objeto: nombre pantalla.
	 *
	 * @return El objeto: nombre pantalla
	 */
	public String getNombrePantalla() {
		return nombrePantalla;
	}

	/**
	 * Definir el objeto: nombre pantalla.
	 *
	 * @param nombrePantalla El nuevo objeto: nombre pantalla
	 */
	public void setNombrePantalla(String nombrePantalla) {
		this.nombrePantalla = nombrePantalla;
	}

	/**
	 * Verificar si id bandera.
	 *
	 * @return true, si id bandera
	 */
	public boolean isIdBandera() {
		return idBandera;
	}

	/**
	 * Definir el objeto: id bandera.
	 *
	 * @param idBandera El nuevo objeto: id bandera
	 */
	public void setIdBandera(boolean idBandera) {
		this.idBandera = idBandera;
	}
	
	
	/**
	 * Obtener el objeto: detalle gral.
	 *
	 * @return El objeto: detalle gral
	 */
	public BeanDetRecepOpera getDetalleGral() {
		return detalleGral;
	}

	/**
	 * Definir el objeto: detalle gral.
	 *
	 * @param detalleGral El nuevo objeto: detalle gral
	 */
	public void setDetalleGral(BeanDetRecepOpera detalleGral) {
		this.detalleGral = detalleGral;
	}

	/**
	 * Obtener el objeto: detalle topo.
	 *
	 * @return El objeto: detalle topo
	 */
	public BeanDetRecepOperaci getDetalleTopo() {
		return detalleTopo;
	}

	/**
	 * Definir el objeto: detalle topo.
	 *
	 * @param detalleTopo El nuevo objeto: detalle topo
	 */
	public void setDetalleTopo(BeanDetRecepOperaci detalleTopo) {
		this.detalleTopo = detalleTopo;
	}

	/**
	 * Obtener el objeto: det estatus.
	 *
	 * @return El objeto: det estatus
	 */
	public BeanDetRecepOp getDetEstatus() {
		return detEstatus;
	}

	/**
	 * Definir el objeto: det estatus.
	 *
	 * @param detEstatus El nuevo objeto: det estatus
	 */
	public void setDetEstatus(BeanDetRecepOp detEstatus) {
		this.detEstatus = detEstatus;
	}

	
	/**
	 * Obtener el objeto: receptor.
	 *
	 * @return El objeto: receptor
	 */
	public BeanDetRecOpRecep getReceptor() {
		return receptor;
	}

	/**
	 * Definir el objeto: receptor.
	 *
	 * @param receptor El nuevo objeto: receptor
	 */
	public void setReceptor(BeanDetRecOpRecep receptor) {
		this.receptor = receptor;
	}

	/**
	 * Obtener el objeto: ordenante.
	 *
	 * @return El objeto: ordenante
	 */
	public BeanDetRecOpOrdent getOrdenante() {
		return ordenante;
	}

	/**
	 * Definir el objeto: ordenante.
	 *
	 * @param ordenante El nuevo objeto: ordenante
	 */
	public void setOrdenante(BeanDetRecOpOrdent ordenante) {
		this.ordenante = ordenante;
	}

	/**
	 * Obtener el objeto: bean cambios.
	 *
	 * @return El objeto: bean cambios
	 */
	public BeanDetRecepcionOperaCambios getBeanCambios() {
		return beanCambios;
	}

	/**
	 * Definir el objeto: bean cambios.
	 *
	 * @param beanCambios El nuevo objeto: bean cambios
	 */
	public void setBeanCambios(BeanDetRecepcionOperaCambios beanCambios) {
		this.beanCambios = beanCambios;
	}
	
	/**
	 * Obtener el objeto: codigo error.
	 *
	 * @return El objeto: codigo error
	 */
	public String getCodigoError() {
		return codigoError;
	}

	/**
	 * Definir el objeto: codigo error.
	 *
	 * @param codigoError El nuevo objeto: codigo error
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	
	

}
