/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version Date/Hour           By       Company Description
 * ------- ------------------- -------- ------- --------------
 * 1.0     09/11/2013 11:33:48  jlleon   INDRA   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Clase del tipo Bean DTO que se encarga de encapsular los datos
 * de retorno de la funcionalidad de monitor CDA
 * 
**/
public class BeanResMonitorCdaHistBO implements BeanResultBO,Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 6710905384333541272L;
	/**Propiedad del tipo  que almacena el bean con los Montos de CDA's */
	private BeanResMonitorCdaHistMontos beanResMonitorCdaMontos;
	/**Propiedad del tipo  que almacena el bean con el volumen de CDA's */
	private BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
	/**Propiedad del tipo  que almacena el codigo de error */
	private String codError;
	/**Propiedad del tipo  que almacena el mensaje de error */
    private String msgError;
    /**Propiedad privada del tipo String que almacena el tipo de error*/
    private String tipoError;    
    
	
    
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResMonitorCdaMontos
	 * @return beanResMonitorCdaMontos Objeto de tipo @see BeanResMonitorCdaHistMontos
	 */
	public BeanResMonitorCdaHistMontos getBeanResMonitorCdaMontos() {
		return beanResMonitorCdaMontos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResMonitorCdaMontos
	 * @param beanResMonitorCdaMontos Objeto de tipo @see BeanResMonitorCdaHistMontos
	 */
	public void setBeanResMonitorCdaMontos(
			BeanResMonitorCdaHistMontos beanResMonitorCdaMontos) {
		this.beanResMonitorCdaMontos = beanResMonitorCdaMontos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResMonitorCdaVolumen
	 * @return beanResMonitorCdaVolumen Objeto de tipo @see BeanResMonitorCdaHistVolumen
	 */
	public BeanResMonitorCdaHistVolumen getBeanResMonitorCdaVolumen() {
		return beanResMonitorCdaVolumen;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResMonitorCdaVolumen
	 * @param beanResMonitorCdaVolumen Objeto de tipo @see BeanResMonitorCdaHistVolumen
	 */
	public void setBeanResMonitorCdaVolumen(
			BeanResMonitorCdaHistVolumen beanResMonitorCdaVolumen) {
		this.beanResMonitorCdaVolumen = beanResMonitorCdaVolumen;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad codError
	 * @return codError Objeto de tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad codError
	 * @param codError Objeto de tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad msgError
	 * @return msgError Objeto de tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad msgError
	 * @param msgError Objeto de tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
    /**
     *Metodo get que sirve para obtener el valor del tipo de error
     * @return tipoError Objeto del tipo String
     */
	 public String getTipoError() {
		return tipoError;
	 }
	 /**
    *Modifica el valor de la propiedad tipoError
    * @param tipoError Objeto del tipo String
    */
	 public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	 }
	/**
	 * Metodo get que obtiene el valor de la propiedad fechaOperacion
	 * @return fechaOperacion Objeto de tipo @see String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad fechaOperacion
	 * @param fechaOperacion Objeto de tipo @see String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	 
}
