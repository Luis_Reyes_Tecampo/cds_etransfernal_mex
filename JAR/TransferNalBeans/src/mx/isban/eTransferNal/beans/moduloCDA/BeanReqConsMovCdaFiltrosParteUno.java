/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqConsMovCdaFiltrosParteUno.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   08/01/201419:30:43 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Consulta de Movimientos CDA
**/
public class BeanReqConsMovCdaFiltrosParteUno implements Serializable{


	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 8011974079135930438L;
	/**Propiedad del tipo @see String que almacena la hora abono de inicio
	   con la que se realiza la busqueda*/
	   private String hrAbonoIniDesde;
	   /**Propiedad del tipo @see String que almacena la hora de abono Fin
	   con la que se realiza la busqueda*/
	   private String hrAbonoFinHasta;
	   /**Propiedad del tipo @see String que almacena la hora Envio inicial
	   con la que se realiza la busqueda*/
	   private String hrEnvioIniDesde;
	   /**Propiedad del tipo @see String que almacena la hora Envio Fin con
	   la que se realiza la busqueda*/
	   private String hrEnvioFinHasta;
	   /**Propiedad del tipo @see String que almacena la cveSpeiOrdenanteAbono*/
	   private String cveSpeiOrdenanteAbono;
	   /**Propiedad del tipo @see String que almacena la descripcion*/
	   private String desc;
	   /**Propiedad del tipo @see String que almacena el tipoPago*/
	   private String tipoPago;

	   /**
	    *Metodo get que sirve para obtener el valor de la propiedad hrAbonoIniDesde
	    * @return hrAbonoIniDesde Objeto del tipo @see String
	    */
	    public String getHrAbonoIniDesde(){
	       return hrAbonoIniDesde;
	    }
	    /**
	    *Modifica el valor de la propiedad hrAbonoIniDesde
	    * @param hrAbonoIniDesde Objeto del tipo @see String
	    */
	    public void setHrAbonoIniDesde(String hrAbonoIniDesde){
	       this.hrAbonoIniDesde=hrAbonoIniDesde;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad hrAbonoFinHasta
	    * @return hrAbonoFinHasta Objeto del tipo String
	    */
	    public String getHrAbonoFinHasta(){
	       return hrAbonoFinHasta;
	    }
	    /**
	    *Modifica el valor de la propiedad hrAbonoFinHasta
	    * @param hrAbonoFinHasta Objeto del tipo String
	    */
	    public void setHrAbonoFinHasta(String hrAbonoFinHasta){
	       this.hrAbonoFinHasta=hrAbonoFinHasta;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad hrEnvioIniDesde
	    * @return hrEnvioIniDesde Objeto del tipo String
	    */
	    public String getHrEnvioIniDesde(){
	       return hrEnvioIniDesde;
	    }
	    /**
	    *Modifica el valor de la propiedad hrEnvioIniDesde
	    * @param hrEnvioIniDesde Objeto del tipo String
	    */
	    public void setHrEnvioIniDesde(String hrEnvioIniDesde){
	       this.hrEnvioIniDesde=hrEnvioIniDesde;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad hrEnvioFinHasta
	    * @return hrEnvioFinHasta Objeto del tipo String
	    */
	    public String getHrEnvioFinHasta(){
	       return hrEnvioFinHasta;
	    }
	    /**
	    *Modifica el valor de la propiedad hrEnvioFinHasta
	    * @param hrEnvioFinHasta Objeto del tipo String
	    */
	    public void setHrEnvioFinHasta(String hrEnvioFinHasta){
	       this.hrEnvioFinHasta=hrEnvioFinHasta;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad cveSpeiOrdenanteAbono
	    * @return cveSpeiOrdenanteAbono Objeto del tipo String
	    */
	    public String getCveSpeiOrdenanteAbono(){
	       return cveSpeiOrdenanteAbono;
	    }
	    /**
	    *Modifica el valor de la propiedad cveSpeiOrdenanteAbono
	    * @param cveSpeiOrdenanteAbono Objeto del tipo String
	    */
	    public void setCveSpeiOrdenanteAbono(String cveSpeiOrdenanteAbono){
	       this.cveSpeiOrdenanteAbono=cveSpeiOrdenanteAbono;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad desc
	    * @return desc Objeto del tipo String
	    */
	    public String getDesc(){
	       return desc;
	    }
	    /**
	    *Modifica el valor de la propiedad desc
	    * @param desc Objeto del tipo String
	    */
	    public void setDesc(String desc){
	       this.desc=desc;
	    }
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad desc
	    * @return tipoPago Objeto del tipo String
	    */
		public String getTipoPago() {
			return tipoPago;
		}
	    /**
	    *Modifica el valor de la propiedad tipoPago
	    * @param tipoPago Objeto del tipo String
	    */
		public void setTipoPago(String tipoPago) {
			this.tipoPago = tipoPago;
		}
	    
	    

}