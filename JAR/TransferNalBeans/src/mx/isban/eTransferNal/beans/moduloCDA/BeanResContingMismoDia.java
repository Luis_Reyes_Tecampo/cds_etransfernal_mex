/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResContingMismoDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:30:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios de la
 * funcionalidad de Contingencia CDA Mismo Dia
 **/

public class BeanResContingMismoDia implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

	/**
	 * Propiedad del tipo List<BeanContingMismoDia> que almacena una lista de
	 * objetos del tipo
	 * 
	 * @see BeanContingMismoDia
	 */
	private List<BeanContingMismoDia> listBeanContingMismoDia = Collections
			.emptyList();
	/** Propiedad del tipo Integer que almacena el toral de registors */
	private Integer totalReg = 0;
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador beanPaginador = null;
	/**
	 * Propiedad del tipo boolean que almacena la bandera para indicar si hay operaciones pendientes
	 */
	private boolean hayOperacionespendientes;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;

	/**
	 * Metodo get que sirve para obtener la lista de objetos BeanContingMismoDia
	 * 
	 * @return List<BeanContingMismoDia> lista de objetos del tipo
	 *         BeanContingMismoDia
	 */
	public List<BeanContingMismoDia> getListBeanContingMismoDia() {
		return listBeanContingMismoDia;
	}

	/**
	 * Metodo set que sirve para setear la lista de objetos BeanContingMismoDia
	 * 
	 * @param listBeanContingMismoDia
	 *            lista de objetos del tipo BeanContingMismoDia
	 */
	public void setListBeanContingMismoDia(
			List<BeanContingMismoDia> listBeanContingMismoDia) {
		this.listBeanContingMismoDia = listBeanContingMismoDia;
	}

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el BeanPaginador
	 * 
	 * @return BeanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 *Modifica el valor de la propiedad BeanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 *Metodo que sirve para obtener el valor de la propiedad hayOperacionespendientes
	 * 
	 * @return hayOperacionespendientes Objeto del tipo boolean
	 */
	public boolean isHayOperacionespendientes() {
		return hayOperacionespendientes;
	}
	/**
	 *Modifica el valor de la propiedad hayOperacionespendientes
	 * @param hayOperacionespendientes
	 *            Objeto del tipo boolean
	 */
	public void setHayOperacionespendientes(boolean hayOperacionespendientes) {
		this.hayOperacionespendientes = hayOperacionespendientes;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

}
