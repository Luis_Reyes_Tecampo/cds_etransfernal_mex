/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqGenArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la funcionalidad de Generar Archivo CDA Historico
**/


public class BeanReqGenArchCDAHist implements Serializable {
   /**
	 * 
	 */
	private static final long serialVersionUID = -7587295815311981672L;
/**Propiedad del tipo @see String que almacena la fecha de operacion*/
   private String fechaOperacion;
   /**Propiedad del tipo @see String que almacena el nombre del archivo*/
   private String nombreArchivo;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
   * @return fechaOperacion Objeto del tipo Date
   */
   public String getFechaOperacion(){
      return fechaOperacion;
   }
   /**
   *Modifica el valor de la propiedad fechaOperacion
   * @param fechaOperacion Objeto del tipo String
   */
   public void setFechaOperacion(String fechaOperacion){
      this.fechaOperacion=fechaOperacion;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
   * @return nombreArchivo Objeto del tipo String
   */
   public String getNombreArchivo(){
      return nombreArchivo;
   }
   /**
   *Modifica el valor de la propiedad nombreArchivo
   * @param nombreArchivo Objeto del tipo String
   */
   public void setNombreArchivo(String nombreArchivo){
      this.nombreArchivo=nombreArchivo;
   }



}
