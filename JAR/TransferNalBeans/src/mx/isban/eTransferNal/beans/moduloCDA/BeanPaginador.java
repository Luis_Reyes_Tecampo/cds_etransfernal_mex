/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqPaginacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la paginacion
**/
public class BeanPaginador  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3135596911537589703L;
	/**Atributo privado que sirve para almacenar la accion de la paginacion SIG (Siguiente), ANT (Anterior), INI (Inicio), y FIN(Fin)*/
	private String accion = null;
	/**Atributo privado que sirve para almacenar la pagina en donde se encuentra por default es la 1*/
	private String pagina="1";
	/**Atributo privado que sirve para almacenar la pagina inicial por default es 1*/
	private String paginaIni="1";
	/**Atributo privado que sirve para almacenar la pagina final*/
	private String paginaFin="0";
	/**Atributo privado que sirve para almacenar la pagina que le sigue al atributo pagina*/
	private String paginaSig;
	/**Atributo privado que sirve para almacenar la pagina anterior al atributo pagina*/
	private String paginaAnt;
	
	/**Variable del tipo String donde se almacenara el rango de la pagina inicial*/
	private String regIni;
	/**Variable del tipo String donde se almacenara el rango de la pagina final*/
	private String regFin;

	/**
	 * Constructor por default de BeanPaginador
	 */
	public BeanPaginador(){
		this.accion = null;
	}

	/**
	 * Constructor del BeanPaginador
	 * @param accion Objeto del tipo String que almacena la accion
	 * @param pagina Objeto del tipo String que almacena la pagina
	 * @param paginaFin Objeto del tipo String que almacena la pagina fin
	 */
	public BeanPaginador(final String accion,final String pagina,
			final String paginaFin){
		super();
		this.accion = accion;
		if(pagina != null && !"".equals(pagina.trim()) && pagina.matches("[0-9]*")){
			this.pagina = pagina;
		}
		if (paginaFin != null){
			this.paginaFin = paginaFin;
		}
	}
	
	/**
	 * Metodo que calcula cual es el valor de la pagina y sus rangos de registros
	 * @param registrosXPagina Objeto del tipo String que almacena el numero de registros por pagina
	 */
	public void paginar(String registrosXPagina){
		if(pagina!=null &&"".equals(pagina)){
			pagina="1";
		}
		int _pagina = 1;
		int _regIni = 1;
		int _regFin = 1;
		paginar();
		int _registrosXPagina = Integer.parseInt(registrosXPagina);
		_pagina = Integer.parseInt(pagina);
		
		_regIni = (_pagina-1)* _registrosXPagina+1;
		_regFin = (_pagina-1)* _registrosXPagina+_registrosXPagina;
		regIni = String.valueOf(_regIni);
		regFin = String.valueOf(_regFin);
	}

	/**
	 * Metodo que calcula cual es el valor de la pagina
	 */
	public void paginar(){
		int _pagina = 1;
		if(accion!=null && !"".equals(accion.trim())){
			_pagina = Integer.parseInt(pagina);
			if(accion!= null && "SIG".equals(accion)){
				_pagina = _pagina+1;
				pagina = String.valueOf(_pagina);
			}else if(accion!= null && "ANT".equals(accion)){
				_pagina = _pagina-1;
				pagina = String.valueOf(_pagina);
			}else if(accion!= null && "FIN".equals(accion)){
				pagina = paginaFin;
			}else if(accion!= null && "INI".equals(accion)){
				pagina = "1";
			}else if(accion!= null && "ACT".equals(accion)){
				pagina = this.getPagina();
			}
		}else if(pagina == null || "".equals(pagina.trim())){
			pagina="1";
		}
	}
	/**
	 * Metodo que sirve para calcular la pagina sig, la pag anterior, y el fin de las pagina
	 * @param totalRegistros Objeto del tipo String que almacena el numero de total de registros
	 * @param registrosXPagina Objeto del tipo String que almacena el numero de registros por pagina
	 */
	public void calculaPaginas(String totalRegistros, String registrosXPagina){
		int _totalRegistros = Integer.parseInt(totalRegistros);
		int _registrosXPagina = Integer.parseInt(registrosXPagina);
		int _totalPaginas = 0;
		int _pagina =Integer.parseInt(pagina);

		int _paginaSig = 0;
		int _paginaAnt = 0;
		int residuo = 0;
		if(_totalRegistros>0){
			_totalPaginas = _totalRegistros/_registrosXPagina;
			residuo = _totalRegistros % _registrosXPagina;
			if(residuo>0){
				_totalPaginas = _totalPaginas + 1;
			}
			paginaFin = String.valueOf(_totalPaginas);
		}
		if( _pagina < _totalPaginas ){
			_paginaSig = _pagina+1;
			paginaSig = String.valueOf(_paginaSig);
		}
		if(_pagina > 0){
			_paginaAnt=_pagina-1;
			paginaAnt = String.valueOf(_paginaAnt);
		}
	}
	
	/**
	 * Metodo que sirve para calcular la pagina sig, la pag anterior, y el fin de las pagina
	 * @param totalRegistros Objeto del tipo Integer que almacena el totalRegistros
	 * @param registrosXPagina Objeto del tipo String que almacena el numero de registros por pagina
	 */
	public void calculaPaginas(Integer totalRegistros, String registrosXPagina){
		int _totalRegistros = 0;
		if(totalRegistros != null){
			_totalRegistros = totalRegistros;
		}
		int _registrosXPagina = Integer.parseInt(registrosXPagina);
		int _totalPaginas = 0;
		int _pagina =Integer.parseInt(pagina);

		int _paginaSig = 0;
		int _paginaAnt = 0;
		int residuo = 0;
		if(_totalRegistros>0){
			_totalPaginas = _totalRegistros/_registrosXPagina;
			residuo = _totalRegistros % _registrosXPagina;
			if(residuo>0){
				_totalPaginas = _totalPaginas + 1;
			}
			paginaFin = String.valueOf(_totalPaginas);
		}else{
			paginaFin="1";
			paginaIni="1";
		}
		if( _pagina < _totalPaginas ){
			_paginaSig = _pagina+1;
			paginaSig = String.valueOf(_paginaSig);
		}
		if(_pagina > 0){
			_paginaAnt=_pagina-1;
			paginaAnt = String.valueOf(_paginaAnt);
		}
	}
	
	/**
	 * Metodo que regresa el numero de la pagina siguiente
	 * @return String con el numero de la pagina siguiente
	 */
	public String getPaginaSig(){
		return paginaSig;
	}
	/**
	 * Metodo que regresa el numero de la pagina anterior
	 * @return String con el numero de la pagina anterior
	 */
	public String getPaginaAnt(){
		return paginaAnt;
	}
	/**
	 * Metodo que regresa la pagina final
	 * @return String con la pagina final
	 */
	public String getPaginaFin(){
		return paginaFin;
	}
	/**
	 * Metodo que setea el numero de la pagina fin
	 * @param paginaFin String con el numero de la pagina fin
	 */
	public void setPaginaFin(String paginaFin){
		this.paginaFin = paginaFin;
	}

	/**
	 * Metodo que regresa la pagina en que se encuentra
	 * @return String con la pagina en que se encuentra
	 */
	public String getPagina(){
		return pagina;
	}
	
	/**
	 * Setea el valor del campo pagina
	 * @param pagina String con la variable pagina
	 */
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	/**
	 * Obtiene la pagina inicial
	 * @return String con la pagina inicial
	 */
	public String getPaginaIni() {
		return paginaIni;
	}
	/**
	 * Metodo que setea el numero de la pagina Ini
	 * @param paginaIni String con el numero de la pagina Ini
	 */
	public void setPaginaIni(String paginaIni){
		this.paginaIni = paginaIni;
	}

	/**
	 * Regresa el valor del campo regIni
	 * @return String Regresa la variable regIni del tipo String
	 */
	public String getRegIni() {
		return this.regIni;
	}

	/**
	 * Regresa el valor del campo regFin
	 * @return String Regresa la variable regFin del tipo String
	 */
	public String getRegFin() {
		return this.regFin;
	}

	/**
	 * Regresa el valor del campo accion
	 * @return String Regresa la variable accion del tipo String
	 */
	public String getAccion() {
		return this.accion;
	}
	
	/**
	 * Setea el valor del campo accion
	 * @param accion String con la variable accion
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}

	/**
	 * Crea un bean paginador para todos los registros de la consulta.
	 * @return BeanPaginador el bean paginador para todos los registros de la consulta.
	 **/
	public static final BeanPaginador crearPaginadorTodosRegistros() {
		final BeanPaginador paginador = new BeanPaginador();
		paginador.regFin = String.valueOf(NumberUtils.INTEGER_ONE);
		paginador.regFin = String.valueOf(Integer.MAX_VALUE);
		return paginador;
	}
	/**
	 * Setea el valor del campo paginaSig
	 * @param paginaSig String con la variable paginaSig
	 */
	public void setPaginaSig(String paginaSig) {
		this.paginaSig = paginaSig;
	}
	/**
	 * Setea el valor del campo paginaAnt
	 * @param paginaAnt String con la variable paginaAnt
	 */
	public void setPaginaAnt(String paginaAnt) {
		this.paginaAnt = paginaAnt;
	}

	/**
	 * Setea el valor del campo regIni
	 * @param regIni String con la variable regIni
	 */
	public void setRegIni(String regIni) {
		this.regIni = regIni;
	}

	/**
	 * Setea el valor del campo regFin
	 * @param regFin String con la variable regFin
	 */
	public void setRegFin(String regFin) {
		this.regFin = regFin;
	}
	
	

	
}
