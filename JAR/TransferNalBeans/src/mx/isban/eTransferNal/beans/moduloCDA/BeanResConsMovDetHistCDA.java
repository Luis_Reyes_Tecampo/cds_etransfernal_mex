package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 * de la funcionalidad  de Consulta Movimiento Detalle Historico CDA
**/
public class BeanResConsMovDetHistCDA implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 5495570339368656004L;
	/**Propiedad del tipo BeanResConsMovCdaDatosGenerales que almacena bean con datos generales*/
	private BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales;
	/**Propiedad del tipo BeanResConsMovCdaBeneficiario que almacena el bean con datos del beneficiario*/
	private BeanResConsMovCdaBeneficiario beanConsMovCdaBeneficiario;
	/**Propiedad del tipo BeanResConsMovCdaOrdenante que almacena el bean con datos ordenante*/
	private BeanResConsMovCdaOrdenante beanConsMovCdaOrdenante;
	 /**Propiedad privada del tipo String que almacena el codigo de error*/
	   private String codError = "";
	   /**Propiedad privada del tipo String que almacena el codigo de mensaje
	   de error*/
	   private String msgError = "";
	   /**Propiedad privada del tipo String que almacena el tipo de error*/
	   private String tipoError;
	
	/**
	 * Metodo que sirve para obtener la propiedad beanMovCdaDatosGenerales
	 * @return beanMovCdaDatosGenerales Objeto de tipo @see BeanResConsMovCdaDatosGenerales
	 */
	public BeanResConsMovCdaDatosGenerales getBeanMovCdaDatosGenerales() {
		return beanMovCdaDatosGenerales;
	}
	/**
	 * Modifica el valor de la propiedad beanMovCdaDatosGenerales
	 * @param beanMovCdaDatosGenerales Objeto de tipo @see BeanResConsMovCdaDatosGenerales
	 */
	public void setBeanMovCdaDatosGenerales(
			BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales) {
		this.beanMovCdaDatosGenerales = beanMovCdaDatosGenerales;
	}
	/**
	 * Metodo que sirve para obtener la propiedad beanConsMovCdaBeneficiario
	 * @return el beanConsMovCdaBeneficiario Objeto de tipo @see BeanResConsMovCdaBeneficiario
	 */
	public BeanResConsMovCdaBeneficiario getBeanConsMovCdaBeneficiario() {
		return beanConsMovCdaBeneficiario;
	}
	/**
	 * Modifica el valor de la propiedad beanMovCdaDatosGenerales beanConsMovCdaBeneficiario
	 * @param beanConsMovCdaBeneficiario Objeto de tipo @see BeanResConsMovCdaBeneficiario
	 */
	public void setBeanConsMovCdaBeneficiario(
			BeanResConsMovCdaBeneficiario beanConsMovCdaBeneficiario) {
		this.beanConsMovCdaBeneficiario = beanConsMovCdaBeneficiario;
	}
	/**
	 *  Metodo que sirve para obtener la propiedad beanMovCdaDatosGenerales
	 * @return beanConsMovCdaOrdenante Objeto de tipo @see BeanResConsMovCdaOrdenante
	 */
	public BeanResConsMovCdaOrdenante getBeanConsMovCdaOrdenante() {
		return beanConsMovCdaOrdenante;
	}
	/**
	 * Modifica el valor de la propiedad beanMovCdaDatosGenerales
	 * @param beanConsMovCdaOrdenante Objeto de tipo @see BeanResConsMovCdaOrdenante
	 */
	public void setBeanConsMovCdaOrdenante(
			BeanResConsMovCdaOrdenante beanConsMovCdaOrdenante) {
		this.beanConsMovCdaOrdenante = beanConsMovCdaOrdenante;
	}
	 /**
    *Metodo get que sirve para obtener el valor de la propiedad codError
    * @return codError Objeto del tipo String
    */
    public String getCodError(){
       return codError;
    }
    /**
    *Modifica el valor de la propiedad codError
    * @param codError Objeto del tipo String
    */
    public void setCodError(String codError){
       this.codError=codError;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad msgError
    * @return msgError Objeto del tipo String
    */
    public String getMsgError(){
       return msgError;
    }
    /**
    *Modifica el valor de la propiedad msgError
    * @param msgError Objeto del tipo String
    */
    public void setMsgError(String msgError){
       this.msgError=msgError;
    }
    /**
     *Metodo get que sirve para obtener el valor del tipo de error
     * @return tipoError Objeto del tipo String
     */
 	 public String getTipoError() {
 		return tipoError;
 	 }
 	 /**
    *Modifica el valor de la propiedad tipoError
    * @param tipoError Objeto del tipo String
    */
 	 public void setTipoError(String tipoError) {
 		this.tipoError = tipoError;
 	 }
}

