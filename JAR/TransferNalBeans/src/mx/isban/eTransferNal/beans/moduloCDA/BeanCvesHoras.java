/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCvesHoras.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/06/2019 07:10:46 PM Uriel Alfredo Botello R. Vector Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * The Class BeanCvesHoras que contiene las claves de la tabla horas SPEI.
 */
public class BeanCvesHoras implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8837595531137012512L;
	
	/** The cve inst ord. */
	private String cveInstOrd;
	
	/** The cve inst benef. */
	private String cveInstBenef;
	
	/** The cve rastreo. */
	private String cveRastreo;
	
	/** The cve rastreo ori. */
	private String cveRastreoOri;
	
	/** The cve transfe. */
	private String cveTransfe;
	
	/** The cve medio ent. */
	private String cveMedioEnt;
	
	/** The folio paquete. */
	private String folioPaquete;
	
	/** The folio pago. */
	private String folioPago;
	
	/** The referencia. */
	private String referencia;
	
	/**
	 * Gets the cve inst ord.
	 *
	 * @return the cveInstOrd
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}
	
	/**
	 * Sets the cve inst ord.
	 *
	 * @param cveInstOrd the cveInstOrd to set
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}
	
	/**
	 * Gets the cve inst benef.
	 *
	 * @return the cveInstBenef
	 */
	public String getCveInstBenef() {
		return cveInstBenef;
	}
	
	/**
	 * Sets the cve inst benef.
	 *
	 * @param cveInstBenef the cveInstBenef to set
	 */
	public void setCveInstBenef(String cveInstBenef) {
		this.cveInstBenef = cveInstBenef;
	}
	
	/**
	 * Gets the cve rastreo.
	 *
	 * @return the cveRastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	
	/**
	 * Sets the cve rastreo.
	 *
	 * @param cveRastreo the cveRastreo to set
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	
	/**
	 * Gets the cve rastreo ori.
	 *
	 * @return the cveRastreoOri
	 */
	public String getCveRastreoOri() {
		return cveRastreoOri;
	}
	
	/**
	 * Sets the cve rastreo ori.
	 *
	 * @param cveRastreoOri the cveRastreoOri to set
	 */
	public void setCveRastreoOri(String cveRastreoOri) {
		this.cveRastreoOri = cveRastreoOri;
	}
	
	/**
	 * Gets the cve transfe.
	 *
	 * @return the cveTransfe
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	
	/**
	 * Sets the cve transfe.
	 *
	 * @param cveTransfe the cveTransfe to set
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	
	/**
	 * Gets the cve medio ent.
	 *
	 * @return the cveMedioEnt
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	
	/**
	 * Sets the cve medio ent.
	 *
	 * @param cveMedioEnt the cveMedioEnt to set
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	
	/**
	 * Gets the folio paquete.
	 *
	 * @return the folioPaquete
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	
	/**
	 * Sets the folio paquete.
	 *
	 * @param folioPaquete the folioPaquete to set
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	
	/**
	 * Gets the folio pago.
	 *
	 * @return the folioPago
	 */
	public String getFolioPago() {
		return folioPago;
	}
	
	/**
	 * Sets the folio pago.
	 *
	 * @param folioPago the folioPago to set
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	
	/**
	 * Gets the referencia.
	 *
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	
	/**
	 * Sets the referencia.
	 *
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
}
