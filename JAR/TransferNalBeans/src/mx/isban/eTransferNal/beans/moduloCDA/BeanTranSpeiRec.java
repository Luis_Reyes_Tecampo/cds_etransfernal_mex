package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.math.BigDecimal;

public class BeanTranSpeiRec extends BeanTranSpeiRecExt implements Serializable{


	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2137607502481831845L;

    /**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
    /**
     * Propiedad del tipo String que almacena el valor de cveMiInstituc
     */
    private String cveMiInstituc;
	/**
	 * Propiedad del tipo String que almacena el valor de cveInstOrd
	 */
	private String cveInstOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPaquete
	 */
	private String folioPaquete;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPago
	 */
	private String folioPago;
    
	/**
	 * Propiedad del tipo String que almacena el valor de topologia
	 */
	private String topologia;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoPago
	 */
	private String tipoPago;
	/**
	 * Propiedad del tipo String que almacena el valor de estatusBanxico
	 */
	private String estatusBanxico;
	/**
	 * Propiedad del tipo String que almacena el valor de estatusTransfer
	 */
	private String estatusTransfer;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaCaptura
	 */
	private String fechaCaptura;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveIntermeOrd
	 */
	private String cveIntermeOrd;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaTran
	 */
	private String numCuentaTran;
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private BigDecimal monto;
	/**
	 * Propiedad del tipo String que almacena el valor de codigoError
	 */
	private String codigoError;
	/**
	 * Propiedad del tipo String que almacena el valor de motivoDevol
	 */
	private String motivoDevol;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad fechaOperacion
	 * @return fechaOperacion Objeto de tipo @see String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad fechaOperacion
	 * @param fechaOperacion Objeto de tipo @see String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto de tipo @see String
	 */
	public String getCveMiInstituc() {
		return cveMiInstituc;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveMiInstituc
	 * @param cveMiInstituc Objeto de tipo @see String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		this.cveMiInstituc = cveMiInstituc;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveInstOrd
	 * @return cveInstOrd Objeto de tipo @see String
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveInstOrd
	 * @param cveInstOrd Objeto de tipo @see String
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad folioPaquete
	 * @return folioPaquete Objeto de tipo @see String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 * Metodo que modifica el valor de la propiedad folioPaquete
	 * @param folioPaquete Objeto de tipo @see String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad folioPago
	 * @return folioPago Objeto de tipo @see String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 * Metodo que modifica el valor de la propiedad folioPago
	 * @param folioPago Objeto de tipo @see String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad topologia
	 * @return topologia Objeto de tipo @see String
	 */
	public String getTopologia() {
		return topologia;
	}
	/**
	 * Metodo que modifica el valor de la propiedad topologia
	 * @param topologia Objeto de tipo @see String
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoPago
	 * @return tipoPago Objeto de tipo @see String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoPago
	 * @param tipoPago Objeto de tipo @see String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatusBanxico
	 * @return estatusBanxico Objeto de tipo @see String
	 */
	public String getEstatusBanxico() {
		return estatusBanxico;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatusBanxico
	 * @param estatusBanxico Objeto de tipo @see String
	 */
	public void setEstatusBanxico(String estatusBanxico) {
		this.estatusBanxico = estatusBanxico;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatusTransfer
	 * @return estatusTransfer Objeto de tipo @see String
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatusTransfer
	 * @param estatusTransfer Objeto de tipo @see String
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad fechaCaptura
	 * @return fechaCaptura Objeto de tipo @see String
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	/**
	 * Metodo que modifica el valor de la propiedad fechaCaptura
	 * @param fechaCaptura Objeto de tipo @see String
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cveIntermeOrd
	 * @return cveIntermeOrd Objeto de tipo @see String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cveIntermeOrd
	 * @param cveIntermeOrd Objeto de tipo @see String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numCuentaTran
	 * @return numCuentaTran Objeto de tipo @see String
	 */
	public String getNumCuentaTran() {
		return numCuentaTran;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numCuentaTran
	 * @param numCuentaTran Objeto de tipo @see String
	 */
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad monto
	 * @return monto Objeto de tipo @see BigDecimal
	 */
	public BigDecimal getMonto() {
		return monto;
	}
	/**
	 * Metodo que modifica el valor de la propiedad monto
	 * @param monto Objeto de tipo @see BigDecimal
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad codigoError
	 * @return codigoError Objeto de tipo @see String
	 */
	public String getCodigoError() {
		return codigoError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad codigoError
	 * @param codigoError Objeto de tipo @see String
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad motivoDevol
	 * @return motivoDevol Objeto de tipo @see String
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}
	/**
	 * Metodo que modifica el valor de la propiedad motivoDevol
	 * @param motivoDevol Objeto de tipo @see String
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	
}
