/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanFechasHoras.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/06/2019 07:06:35 PM Uriel Alfredo Botello R. Vector Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * The Class BeanFechasHoras que contiene las fechas de la tabla horas SPEI.
 */
public class BeanFechasHoras implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 487525241049540749L;
	
	/** The fch operacion. */
	private String fchOperacion;
	
	/** The fch captura. */
	private String fchCaptura;
	
	/** The fch envio. */
	private String fchEnvio;
	
	/** The fch acuse. */
	private String fchAcuse;
	
	/** The fch confirmacion. */
	private String fchConfirmacion;
	
	/** The fch cecepcion. */
	private String fchRecepcion;
	
	/** The fch aprobacion. */
	private String fchAprobacion;
	
	/** The fch captura rec. */
	private String fchCapturaRec;

	/**
	 * Gets the fch operacion.
	 *
	 * @return the fchOperacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Sets the fch operacion.
	 *
	 * @param fchOperacion the fchOperacion to set
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Gets the fch captura.
	 *
	 * @return the fchCaptura
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}

	/**
	 * Sets the fch captura.
	 *
	 * @param fchCaptura the fchCaptura to set
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}

	/**
	 * Gets the fch envio.
	 *
	 * @return the fchEnvio
	 */
	public String getFchEnvio() {
		return fchEnvio;
	}

	/**
	 * Sets the fch envio.
	 *
	 * @param fchEnvio the fchEnvio to set
	 */
	public void setFchEnvio(String fchEnvio) {
		this.fchEnvio = fchEnvio;
	}

	/**
	 * Gets the fch acuse.
	 *
	 * @return the fchAcuse
	 */
	public String getFchAcuse() {
		return fchAcuse;
	}

	/**
	 * Sets the fch acuse.
	 *
	 * @param fchAcuse the fchAcuse to set
	 */
	public void setFchAcuse(String fchAcuse) {
		this.fchAcuse = fchAcuse;
	}

	/**
	 * Gets the fch confirmacion.
	 *
	 * @return the fchConfirmacion
	 */
	public String getFchConfirmacion() {
		return fchConfirmacion;
	}

	/**
	 * Sets the fch confirmacion.
	 *
	 * @param fchConfirmacion the fchConfirmacion to set
	 */
	public void setFchConfirmacion(String fchConfirmacion) {
		this.fchConfirmacion = fchConfirmacion;
	}

	/**
	 * Gets the fch recepcion.
	 *
	 * @return the fchRecepcion
	 */
	public String getFchRecepcion() {
		return fchRecepcion;
	}

	/**
	 * Sets the fch recepcion.
	 *
	 * @param fchRecepcion the fchRecepcion to set
	 */
	public void setFchRecepcion(String fchRecepcion) {
		this.fchRecepcion = fchRecepcion;
	}

	/**
	 * Gets the fch aprobacion.
	 *
	 * @return the fchAprobacion
	 */
	public String getFchAprobacion() {
		return fchAprobacion;
	}

	/**
	 * Sets the fch aprobacion.
	 *
	 * @param fchAprobacion the fchAprobacion to set
	 */
	public void setFchAprobacion(String fchAprobacion) {
		this.fchAprobacion = fchAprobacion;
	}

	/**
	 * Gets the fch captura rec.
	 *
	 * @return the fchCapturaRec
	 */
	public String getFchCapturaRec() {
		return fchCapturaRec;
	}

	/**
	 * Sets the fch captura rec.
	 *
	 * @param fchCapturaRec the fchCapturaRec to set
	 */
	public void setFchCapturaRec(String fchCapturaRec) {
		this.fchCapturaRec = fchCapturaRec;
	}
	
}
