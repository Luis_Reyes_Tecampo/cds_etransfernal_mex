/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsReqBitacoraAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 16 19:25:50 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la bitacora administrativa
**/

public class BeanConsReqBitacoraAdmon implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo @see String que almacena el servicio*/
   private String servicio = "";
   /**Propiedad del tipo @see String que almacena el usuario*/
   private String usuario = "";
   /**Propiedad del tipo @see String que almacena la fechaAccionIni*/
   private String fechaAccionIni;
   /**Propiedad del tipo @see String que almacena la fechaAccionFin*/
   private String fechaAccionFin;
   /**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
   de la paginacion*/
   private BeanPaginador paginador;
   /**Propiedad privada del tipo Integer que almacena el total de registros*/
   private Integer totalReg;
   
   /**Propiedad del tipo @see String que almacena el usuario*/
   private String paramUsuario = "";
   /**Propiedad del tipo @see String que almacena el servicio*/
   private String paramServicio;
   /**Propiedad del tipo @see String que almacena el paramFecha3Antes*/
   private String paramFecha3Antes;
   /**Propiedad del tipo @see String que almacena el paramFechaFin*/
   private String paramFechaFin;
   
   /** Propiedad de tipo boolean que almacena el indicador de menu */
   private boolean esSPID;
   


     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad servicio
   * @return servicio Objeto del tipo String
   */
   public String getServicio(){
      return servicio;
   }
   /**
   *Modifica el valor de la propiedad servicio
   * @param servicio Objeto del tipo String
   */
   public void setServicio(String servicio){
      this.servicio=servicio;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad usuario
   * @return usuario Objeto del tipo String
   */
   public String getUsuario(){
      return usuario;
   }
   /**
   *Modifica el valor de la propiedad usuario
   * @param usuario Objeto del tipo String
   */
   public void setUsuario(String usuario){
      this.usuario=usuario;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaAccionIni
   * @return fechaAccionIni Objeto del tipo Date
   */
   public String getFechaAccionIni(){
      return fechaAccionIni;
   }
   /**
   *Modifica el valor de la propiedad fechaAccionIni
   * @param fechaAccionIni Objeto del tipo String
   */
   public void setFechaAccionIni(String fechaAccionIni){
      this.fechaAccionIni=fechaAccionIni;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaAccionFin
   * @return fechaAccionFin Objeto del tipo String
   */
   public String getFechaAccionFin(){
      return fechaAccionFin;
   }
   /**
   *Modifica el valor de la propiedad fechaAccionFin
   * @param fechaAccionFin Objeto del tipo String
   */
   public void setFechaAccionFin(String fechaAccionFin){
      this.fechaAccionFin=fechaAccionFin;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad paginador
   * @return paginador Objeto del tipo BeanPaginador
   */
   public BeanPaginador getPaginador(){
      return paginador;
   }
   /**
   *Modifica el valor de la propiedad paginador
   * @param paginador Objeto del tipo BeanPaginador
   */
   public void setPaginador(BeanPaginador paginador){
      this.paginador=paginador;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad totalReg
    * @return totalReg Objeto del tipo Integer
    */
    public Integer getTotalReg(){
       return totalReg;
    }
    /**
    *Modifica el valor de la propiedad totalReg
    * @param totalReg Objeto del tipo Integer
    */
    public void setTotalReg(Integer totalReg){
       this.totalReg=totalReg;
    }
    /**
     *Metodo get que sirve para obtener el valor de la propiedad paramUsuario
     * @return paramUsuario Objeto del tipo String
     */
	public String getParamUsuario() {
		return paramUsuario;
	}

    /**
    *Modifica el valor de la propiedad paramUsuario
    * @param paramUsuario Objeto del tipo String
    */
	public void setParamUsuario(String paramUsuario) {
		this.paramUsuario = paramUsuario;
	}
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad paramServicio
	   * @return paramServicio Objeto del tipo String
	   */
	public String getParamServicio() {
		return paramServicio;
	}
    /**
    *Modifica el valor de la propiedad paramServicio
    * @param paramServicio Objeto del tipo String
    */
	public void setParamServicio(String paramServicio) {
		this.paramServicio = paramServicio;
	}
   /**
   *Metodo get que sirve para obtener el valor de la propiedad paramFecha3Antes
   * @return paramFecha3Antes Objeto del tipo String
   */
	public String getParamFecha3Antes() {
		return paramFecha3Antes;
	}
	/**
    *Modifica el valor de la propiedad paramFecha3Antes
    * @param paramFecha3Antes Objeto del tipo String
    */
	public void setParamFecha3Antes(String paramFecha3Antes) {
		this.paramFecha3Antes = paramFecha3Antes;
	}
   /**
   *Metodo get que sirve para obtener el valor de la propiedad paramFechaFin
   * @return paramFechaFin Objeto del tipo String
   */
	public String getParamFechaFin() {
		return paramFechaFin;
	}
	/**
    *Modifica el valor de la propiedad paramFechaFin
    * @param paramFechaFin Objeto del tipo String
    */
	public void setParamFechaFin(String paramFechaFin) {
		this.paramFechaFin = paramFechaFin;
	}

	/**
	 * @return Indica si la consulta es SPID
	 */
	public boolean getEsSPID() {
		return esSPID;
	}

	/**
	 * @param esSPID El valor a asignar al atributo esSPID
	 */
	public void setEsSPID(boolean esSPID) {
		this.esSPID = esSPID;
	}

}
