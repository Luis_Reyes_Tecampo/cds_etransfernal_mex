/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMovimientoCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   12/12/2013 16:16:14 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos de un movimiento CDA
 *
**/
public class BeanMovimientoMonHistCDA implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -1446744051589239170L;
	/**Propiedad de tipo String que almacena la referencia*/
	private String referencia;
	/**Propiedad de tipo String que almacena el nombre de la intitucion emisora*/
	private	String nomInstEmisora;
	/**Propiedad de tipo String que almacena la clave de rastreo*/
	private	String cveRastreo;
	/**Propiedad de tipo String que almacena la cuenta del beneficiario*/
	private	String ctaBenef;
	/**Propiedad de tipo String que almacena el monto*/
	private	String monto;
	/**Propiedad de tipo String que almacena la hora de abono*/
	private	String hrAbono;
	/**Propiedad de tipo String que almacena la hora de envio*/
	private	String hrEnvio;
	/**Propiedad de tipo Date que almacena el estatus CDA*/
	private	String estatusCDA;
	/**Propiedad de tipo String que almacena el codigo de error*/
	private	String codError;
	/**Propiedad de tipo String que almacena la fecha del CDA*/
	private	String fchCDA;
	/**Propiedad de tipo String que almacena la fecha del abono*/
	private	String fechaAbono;
	/**Propiedad de tipo String que almacena el tipo pago*/
	private	String tipoPago;
	/**Objeto bean que almacena las propiedades que conforman la llave*/
	private BeanMovimientoCDALlave beanMovimientoCDALlave = new BeanMovimientoCDALlave();

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad referencia
	 * @return referencia Objeto del tipo @see String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Modifica el valor de la propiedad referencia
	 * @param referencia Objeto del tipo @see String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nomInstEmisora
	 * @return nomInstEmisora Objeto del tipo @see String
	 */
	public String getNomInstEmisora() {
		return nomInstEmisora;
	}
	/**
	 * Modifica el valor de la propiedad nomInstEmisora
	 * @param nomInstEmisora Objeto del tipo @see String
	 */
	public void setNomInstEmisora(String nomInstEmisora) {
		this.nomInstEmisora = nomInstEmisora;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveRastreo
	 * @return cveRastreo Objeto del tipo @see String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Modifica el valor de la propiedad cveRastreo
	 * @param cveRastreo Objeto del tipo @see String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad ctaBenef
	 * @return ctaBenef Objeto del tipo @see String
	 */
	public String getCtaBenef() {
		return ctaBenef;
	}
	/**
	 * Modifica el valor de la propiedad ctaBenef
	 * @param ctaBenef Objeto del tipo @see String
	 */
	public void setCtaBenef(String ctaBenef) {
		this.ctaBenef = ctaBenef;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad monto
	 * @return monto Objeto del tipo @see String
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 * Modifica el valor de la propiedad monto
	 * @param monto Objeto del tipo @see String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad hrAbono
	 * @return hrAbono Objeto del tipo @see String
	 */
	public String getHrAbono() {
		return hrAbono;
	}
	/**
	 * Modifica el valor de la propiedad hrAbono
	 * @param hrAbono Objeto del tipo @see String
	 */
	public void setHrAbono(String hrAbono) {
		this.hrAbono = hrAbono;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad hrEnvio
	 * @return hrEnvio Objeto del tipo @see String
	 */
	public String getHrEnvio() {
		return hrEnvio;
	}
	/**
	 * Modifica el valor de la propiedad hrEnvio
	 * @param hrEnvio Objeto del tipo @see String
	 */
	public void setHrEnvio(String hrEnvio) {
		this.hrEnvio = hrEnvio;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad estatusCDA
	 * @return estatusCDA Objeto del tipo @see String
	 */
	public String getEstatusCDA() {
		return estatusCDA;
	}
	/**
	 * Modifica el valor de la propiedad estatusCDA
	 * @param estatusCDA Objeto del tipo @see String
	 */
	public void setEstatusCDA(String estatusCDA) {
		this.estatusCDA = estatusCDA;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad codError
	 * @return codError Objeto del tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Modifica el valor de la propiedad codError
	 * @param codError Objeto del tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fchCDA
	 * @return fchCDA Objeto del tipo @see String
	 */
	public String getFchCDA() {
		return fchCDA;
	}
	/**
	 * Modifica el valor de la propiedad fchCDA
	 * @param fchCDA Objeto del tipo @see String
	 */
	public void setFchCDA(String fchCDA) {
		this.fchCDA = fchCDA;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaAbono
	 * @return fechaAbono Objeto del tipo @see String
	 */
	public String getFechaAbono() {
		return fechaAbono;
	}
	/**
	 * Modifica el valor de la propiedad fechaAbono
	 * @param fechaAbono Objeto del tipo @see String
	 */
	public void setFechaAbono(String fechaAbono) {
		this.fechaAbono = fechaAbono;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad tipoPago
	 * @return tipoPago Objeto del tipo @see String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Modifica el valor de la propiedad tipoPago
	 * @param tipoPago Objeto del tipo @see String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveSpei
	 * @return cveSpei Objeto del tipo @see String
	 */
	public String getCveSpei() {
		return beanMovimientoCDALlave.getCveSpei();
	}
	/**
	 * Modifica el valor de la propiedad cveSpei
	 * @param cveSpei Objeto del tipo @see String
	 */
	public void setCveSpei(String cveSpei) {
		beanMovimientoCDALlave.setCveSpei(cveSpei);
	}
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaOpe
	 * @return fechaOpe Objeto del tipo @see String
	 */
	public String getFechaOpe() {
		return beanMovimientoCDALlave.getFechaOpe();
	}
	/**
	 * Modifica el valor de la propiedad fechaOpe
	 * @param fechaOpe Objeto del tipo @see String
	 */
	public void setFechaOpe(String fechaOpe) {
		beanMovimientoCDALlave.setFechaOpe(fechaOpe);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPaq
	 * @return folioPaq Objeto del tipo @see String
	 */
	public String getFolioPaq() {
		return beanMovimientoCDALlave.getFolioPaq();
	}
	/**
	 * Modifica el valor de la propiedad folioPaq
	 * @param folioPaq Objeto del tipo @see String
	 */
	public void setFolioPaq(String folioPaq) {
		beanMovimientoCDALlave.setFolioPaq(folioPaq);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPago
	 * @return folioPago Objeto del tipo @see String
	 */
	public String getFolioPago() {
		return beanMovimientoCDALlave.getFolioPago();
	}
	/**
	 * Modifica el valor de la propiedad folioPago
	 * @param folioPago Objeto del tipo @see String
	 */
	public void setFolioPago(String folioPago) {
		beanMovimientoCDALlave.setFolioPago(folioPago);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto del tipo @see String
	 */
	public String getCveMiInstituc() {
		return beanMovimientoCDALlave.getCveMiInstituc();
	}
	/**
	 * Modifica el valor de la propiedad cveMiInstituc
	 * @param cveMiInstituc Objeto del tipo @see String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		beanMovimientoCDALlave.setCveMiInstituc(cveMiInstituc);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad beanMovimientoCDALlave
	 * @return beanMovimientoCDALlave Objeto del tipo @see BeanMovimientoCDALlave
	 */
	public BeanMovimientoCDALlave getBeanMovimientoCDALlave() {
		return beanMovimientoCDALlave;
	}
	/**
	 * Modifica la referencia de la propiedad beanMovimientoCDALlave
	 * @param beanMovimientoCDALlave Objeto del tipo @see BeanMovimientoCDALlave
	 */
	public void setBeanMovimientoCDALlave(
			BeanMovimientoCDALlave beanMovimientoCDALlave) {
		this.beanMovimientoCDALlave = beanMovimientoCDALlave;
	}

	

}