/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResBase.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Carlos Alberto Chong  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de codigo de error y mensaje de error
**/
public class BeanResBase implements BeanResultBO, Serializable {
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 43140784312546891L;
	/**Propiedad del tipo  que almacena el codigo de error */
	@Size(max=40)
	private String codError;
	/**Propiedad del tipo  que almacena el mensaje de error */
	@Size(max=40)
    private String msgError;
	/** Propiedad del tipo mensaje de error */
	@Size(max=40)
	private String tipoError;
	
	/**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
	/**
	 * Metodo get que sirve para obtener la propiedad tipoError
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoError
	 * @param tipoError del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}


}
