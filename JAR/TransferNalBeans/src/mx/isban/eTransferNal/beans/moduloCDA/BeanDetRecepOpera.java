/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOpera.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   30/09/2015 16:48 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos del receptor.
 *
 * @author FSW-Vector
 * @since 27/09/2018
 */
public class BeanDetRecepOpera implements Serializable{

  	/**  serialVersionUID Propiedad del tipo long con el valor de serialVersionUID. */
	private static final long serialVersionUID = 1630253094350246431L;

	/** La variable que contiene informacion con respecto a: fch operacion. */
	@Size(max=10)
	private String fchOperacion;

	/** La variable que contiene informacion con respecto a: refe numerica. */
	@Size(max=7)
	private String refeNumerica;

	/** La variable que contiene informacion con respecto a: cve mi institucion. */
	@Size(max=12)
	private String cveMiInstitucion;

	/** La variable que contiene informacion con respecto a: folio paquete dev. */
	@Size(max=12)
	private String folioPaqueteDev;

	/** La variable que contiene informacion con respecto a: folio pago dev. */
	@Size(max=12)
	private String folioPagoDev;

	/** La variable que contiene informacion con respecto a: folio pago. */
	@Size(max=12)
	private String folioPago;

	/** La variable que contiene informacion con respecto a: folio paquete. */
	@Size(max=12)
	private String folioPaquete;

	/** La variable que contiene informacion con respecto a: cve rastreo. */
	@Size(max=30)
	private String cveRastreo;

	/** La variable que contiene informacion con respecto a: cve rastreo ori. */
	@Size(max=30)
	private String cveRastreoOri;

	/** La variable que contiene informacion con respecto a: motivo devol. */
	@Size(max=12)
	private String cveInstOrd;

	/** La variable que contiene informacion con respecto a: estatus Ope. */
	@Size(max=2)
	private String estatusOpe;

	/** La variable que contiene informacion con respecto a: cve Transfer. */
	@Size(max=3)
	private String cveTransfer;

	/** La variable que contiene informacion con respecto a: des Cve Transf. */
	@Size(max=60)
	private String desCveTransf;

	/** La variable que contiene informacion con respecto a: cve operacion. */
	@Size(max=8)
	private String cveOperacion;

	/** La variable que contiene informacion con respecto a: fch operacion. */
	@Size(max=10)
	private String fchCaptura;

	/**
	 * Obtener el objeto: fchCaptura.
	 *
	 * @return El objeto: fchCaptura
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}

	/**
	 * Definir el objeto: fchCaptura.
	 *
	 * @param cveOperacion El nuevo objeto: cve operacion
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}
	/**
	 * Obtener el objeto: cve operacion.
	 *
	 * @return El objeto: cve operacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}

	/**
	 * Definir el objeto: cve operacion.
	 *
	 * @param cveOperacion El nuevo objeto: cve operacion
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}

	/**
	 * Obtener el objeto: fch operacion.
	 *
	 * @return El objeto: fch operacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Definir el objeto: fch operacion.
	 *
	 * @param fchOperacion El nuevo objeto: fch operacion
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Obtener el objeto: refe numerica.
	 *
	 * @return El objeto: refe numerica
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}

	/**
	 * Definir el objeto: refe numerica.
	 *
	 * @param refeNumerica El nuevo objeto: refe numerica
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}

	/**
	 * Obtener el objeto: cve mi institucion.
	 *
	 * @return El objeto: cve mi institucion
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Definir el objeto: cve mi institucion.
	 *
	 * @param cveMiInstitucion El nuevo objeto: cve mi institucion
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Obtener el objeto: folio paquete dev.
	 *
	 * @return El objeto: folio paquete dev
	 */
	public String getFolioPaqueteDev() {
		return folioPaqueteDev;
	}

	/**
	 * Definir el objeto: folio paquete dev.
	 *
	 * @param folioPaqueteDev El nuevo objeto: folio paquete dev
	 */
	public void setFolioPaqueteDev(String folioPaqueteDev) {
		this.folioPaqueteDev = folioPaqueteDev;
	}

	/**
	 * Obtener el objeto: folio pago dev.
	 *
	 * @return El objeto: folio pago dev
	 */
	public String getFolioPagoDev() {
		return folioPagoDev;
	}

	/**
	 * Definir el objeto: folio pago dev.
	 *
	 * @param folioPagoDev El nuevo objeto: folio pago dev
	 */
	public void setFolioPagoDev(String folioPagoDev) {
		this.folioPagoDev = folioPagoDev;
	}

	/**
	 * Obtener el objeto: folio pago.
	 *
	 * @return El objeto: folio pago
	 */
	public String getFolioPago() {
		return folioPago;
	}

	/**
	 * Definir el objeto: folio pago.
	 *
	 * @param folioPago El nuevo objeto: folio pago
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}

	/**
	 * Obtener el objeto: folio paquete.
	 *
	 * @return El objeto: folio paquete
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}

	/**
	 * Definir el objeto: folio paquete.
	 *
	 * @param folioPaquete El nuevo objeto: folio paquete
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}

	/**
	 * Obtener el objeto: cve rastreo.
	 *
	 * @return El objeto: cve rastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}

	/**
	 * Definir el objeto: cve rastreo.
	 *
	 * @param cveRastreo El nuevo objeto: cve rastreo
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}

	/**
	 * Obtener el objeto: cve rastreo ori.
	 *
	 * @return El objeto: cve rastreo ori
	 */
	public String getCveRastreoOri() {
		return cveRastreoOri;
	}

	/**
	 * Definir el objeto: cve rastreo ori.
	 *
	 * @param cveRastreoOri El nuevo objeto: cve rastreo ori
	 */
	public void setCveRastreoOri(String cveRastreoOri) {
		this.cveRastreoOri = cveRastreoOri;
	}


	/**
	 * Obtener el objeto: cve inst ord.
	 *
	 * @return El objeto: cve inst ord
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}

	/**
	 * Definir el objeto: cve inst ord.
	 *
	 * @param cveInstOrd El nuevo objeto: cve inst ord
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}

	/**
	 * Obtener el objeto: estatus Ope.
	 *
	 * @return El objeto: estatus Ope
	 */
	public String getEstatusOpe() {
		return estatusOpe;
	}

	/**
	 * Definir el objeto: estatus Ope.
	 *
	 * @param cveInstOrd El nuevo objeto: estatus Ope
	 */
	public void setEstatusOpe(String estatusOpe) {
		this.estatusOpe = estatusOpe;
	}

	/**
	 * Obtener el objeto: cve Transfer.
	 *
	 * @return El objeto: cve Transfer
	 */
	public String getCveTransfer() {
		return cveTransfer;
	}

	/**
	 * Definir el objeto: cve Transfer.
	 *
	 * @param cveInstOrd El nuevo objeto: cve Transfer
	 */
	public void setCveTransfer(String cveTransfer) {
		this.cveTransfer = cveTransfer;
	}

	/**
	 * Obtener el objeto: des Cve Transf.
	 *
	 * @return El objeto: des Cve Transf
	 */
	public String getDesCveTransf() {
		return desCveTransf;
	}

	/**
	 * Definir el objeto: des Cve Transf.
	 *
	 * @param desCveTransf El nuevo objeto: des Cve Transf
	 */
	public void setDesCveTransf(String desCveTransf) {
		this.desCveTransf = desCveTransf;
	}



}