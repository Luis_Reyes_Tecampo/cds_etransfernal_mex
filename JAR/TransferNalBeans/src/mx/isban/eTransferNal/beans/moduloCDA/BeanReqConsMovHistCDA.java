/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqConsMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Consulta de Movimientos HistoricosCDAs
 **/

public class BeanReqConsMovHistCDA implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -2660903540449877117L;
	
	/**Bean que mapea los parametros con fechas*/
	private BeanReqConsMovHistCDAFechaHora beanReqConsMovHistCDAFechaHora = 
		new BeanReqConsMovHistCDAFechaHora();
	
	/**
	 * Propiedad del tipo String que almacena la clave Spei del Ordenante de
	 * Abono
	 */
	private String cveSpeiOrdenanteAbono;
	/**
	 * Propiedad del tipo String que almacena el nombre de la institucion
	 * emisora
	 */
	private String nombreInstEmisora;
	
	/** Propiedad del tipo String que almacena la clave de rastreo */
	private String cveRastreo;
	/**
	 * Propiedad del tipo String que almacena el numero de cuenta del
	 * beneficiario
	 */
	private String ctaBeneficiario;
	/** Propiedad del tipo String que almacena el monto de Pago */
	private String montoPago;
	/**
	 * Propiedad del tipo String que almacena si la operacion se ha realizado en
	 * contingencia
	 */
	private boolean opeContingencia;
	/**
	 * Propiedad del tipo String que almacena el numero de referencia
	 */
	private String referencia;
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
    private BeanPaginador paginador = new BeanPaginador();
    
    /**Propiedad privada del tipo Integer que almacena el total de registros*/
    private Integer totalReg;
	/**
	 * Propiedad del tipo String que almacena el tipo pago
	 */
    private String tipoPago;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOpeInicio
	 *
	 * @return fechaOpeInicio Objeto del tipo String
	 */
	public String getFechaOpeInicio() {
		return beanReqConsMovHistCDAFechaHora.getFechaOpeInicio();
	}

	/**
	 *Modifica el valor de la propiedad fechaOpeInicio
	 *
	 * @param fechaOpeInicio
	 *            Objeto del tipo String
	 */
	public void setFechaOpeInicio(String fechaOpeInicio) {
		beanReqConsMovHistCDAFechaHora.setFechaOpeInicio(fechaOpeInicio);
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOpeFin
	 *
	 * @return fechaOpeFin Objeto del tipo String
	 */
	public String getFechaOpeFin() {
		return beanReqConsMovHistCDAFechaHora.getFechaOpeFin();
	}

	/**
	 *Modifica el valor de la propiedad fechaOpeFin
	 *
	 * @param fechaOpeFin
	 *            Objeto del tipo String
	 */
	public void setFechaOpeFin(String fechaOpeFin) {
		beanReqConsMovHistCDAFechaHora.setFechaOpeFin(fechaOpeFin);
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hrAbonoIni
	 *
	 * @return hrAbonoIni Objeto del tipo String
	 */
	public String getHrAbonoIni() {
		return beanReqConsMovHistCDAFechaHora.getHrAbonoIni();
	}

	/**
	 *Modifica el valor de la propiedad hrAbonoIni
	 *
	 * @param hrAbonoIni
	 *            Objeto del tipo String
	 */
	public void setHrAbonoIni(String hrAbonoIni) {
		beanReqConsMovHistCDAFechaHora.setHrAbonoIni(hrAbonoIni);
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hrAbonoFin
	 *
	 * @return hrAbonoFin Objeto del tipo String
	 */
	public String getHrAbonoFin() {
		return beanReqConsMovHistCDAFechaHora.getHrAbonoFin();
	}

	/**
	 *Modifica el valor de la propiedad hrAbonoFin
	 *
	 * @param hrAbonoFin
	 *            Objeto del tipo String
	 */
	public void setHrAbonoFin(String hrAbonoFin) {
		beanReqConsMovHistCDAFechaHora.setHrAbonoFin(hrAbonoFin);
	}

	/**
	 * Metodo que sirve para obtener la propiedad hrEnvioIni
	 * @return el hrEnvioIni Objeto del tipo @see String
	 */
	public String getHrEnvioIni() {
		return beanReqConsMovHistCDAFechaHora.getHrEnvioIni();
	}

	/**
	 * Modifica el valor de la propiedad hrEnvioIni
	 * @param hrEnvioIni
	 * el hrEnvioIni a establecer
	 */
	public void setHrEnvioIni(String hrEnvioIni) {
		beanReqConsMovHistCDAFechaHora.setHrEnvioIni(hrEnvioIni);
	}

	/**
	 * Metodo para obtener el valor de la propiedad hrEnvioFin
	 * @return el hrEnvioFin Objeto del tipo @see String
	 */
	public String getHrEnvioFin() {
		return beanReqConsMovHistCDAFechaHora.getHrEnvioFin();
	}

	/**
	 * Modifica el valor de la propiedad hrEnvioFin
	 * @param hrEnvioFin
	 *            el hrEnvioFin a establecer
	 */
	public void setHrEnvioFin(String hrEnvioFin) {
		beanReqConsMovHistCDAFechaHora.setHrEnvioFin(hrEnvioFin);
	}

	/**
	 * Metodo que obtiene el valor de la propiedad cveSpeiOrdenanteAbono
	 * @return el cveSpeiOrdenanteAbono Objeto del tipo @see String
	 */
	public String getCveSpeiOrdenanteAbono() {
		return cveSpeiOrdenanteAbono;
	}

	/**
	 * Modifica el valor de la propiedad cveSpeiOrdenanteAbono
	 * @param cveSpeiOrdenanteAbono
	 *            el cveSpeiOrdenanteAbono a establecer
	 */
	public void setCveSpeiOrdenanteAbono(String cveSpeiOrdenanteAbono) {
		this.cveSpeiOrdenanteAbono = cveSpeiOrdenanteAbono;
	}

	/**
	 * Metodo que obtiene el valor de la propiedad nombreInstEmisora
	 * @return el nombreInstEmisora Objeto del tipo @see String
	 */
	public String getNombreInstEmisora() {
		return nombreInstEmisora;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombreInstEmisora
	 * @param nombreInstEmisora
	 *            el nombreInstEmisora a establecer
	 */
	public void setNombreInstEmisora(String nombreInstEmisora) {
		this.nombreInstEmisora = nombreInstEmisora;
	}

	/**
	 * Metodo que obtiene el valor de la propiedad cveRastreo
	 * @return el cveRastreo Objeto del tipo @see String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveRastreo
	 * @param cveRastreo
	 *            el cveRastreo a establecer
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}

	/**
	 * Metodo que obtiene el valor de la propiedad ctaBeneficiario
	 * @return el ctaBeneficiario Objeto del tipo @see String
	 */
	public String getCtaBeneficiario() {
		return ctaBeneficiario;
	}

	/**
	 * Metodo que modifica el valor de la propiedad ctaBeneficiario
	 * @param ctaBeneficiario
	 *            el ctaBeneficiario a establecer
	 */
	public void setCtaBeneficiario(String ctaBeneficiario) {
		this.ctaBeneficiario = ctaBeneficiario;
	}

	/**
	 * Metodo que obtiene el valor de la propiedad montoPago
	 * @return el montoPago Objeto del tipo @see String
	 */
	public String getMontoPago() {
		return montoPago;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoPago
	 * @param montoPago
	 *            el montoPago a establecer
	 */
	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}

	/**opeContingencia
	 * Metodo que obtiene el valor de la propiedad opeContingencia
	 * @return opeContingencia Objeto del tipo @see String
	 */
	public boolean getOpeContingencia() {
		return opeContingencia;
	}

	/**
	 * Metodo que modifica el valor de la propiedad opeContingencia
	 * @param opeContingencia
	 *            el opeContingencia a establecer
	 */
	public void setOpeContingencia(boolean opeContingencia) {
		this.opeContingencia = opeContingencia;
	}

	/**
	 * Metodo que modifica el valor de la propiedad referencia
	 * @param referencia Objeto del tipo @see String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * Metodo que obtiene el valor de la propiedad referencia
	 * @return referencia  Objeto del tipo @see String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad paginador
	   * @return paginador Objeto del tipo BeanPaginador
	   */
	   public BeanPaginador getPaginador(){
	      return paginador;
	   }
	   /**
	   *Modifica el valor de la propiedad paginador
	   * @param paginador Objeto del tipo BeanPaginador
	   */
	   public void setPaginador(BeanPaginador paginador){
	      this.paginador=paginador;
	   }
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad totalReg
	   * @return totalReg Objeto del tipo Integer
	   */
	   public Integer getTotalReg(){
	      return totalReg;
	   }
	   /**
	   *Modifica el valor de la propiedad totalReg
	   * @param totalReg Objeto del tipo Integer
	   */
	   public void setTotalReg(Integer totalReg){
	      this.totalReg=totalReg;
	   }
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad tipoPago
	   * @return tipoPago Objeto del tipo String
	   */
	   public String getTipoPago() {
		  return tipoPago;
	   }
	   /**
	   *Modifica el valor de la propiedad tipoPago
	   * @param tipoPago Objeto del tipo String
	   */
	   public void setTipoPago(String tipoPago) {
		   this.tipoPago = tipoPago;
	   }
	   /**
		*Metodo get que sirve para obtener el valor de la propiedad beanReqConsMovHistCDAFechaHora
		* @return beanReqConsMovHistCDAFechaHora Objeto del tipo BeanReqConsMovHistCDAFechaHora
		*/
	   public BeanReqConsMovHistCDAFechaHora getBeanReqConsMovHistCDAFechaHora() {
		   return beanReqConsMovHistCDAFechaHora;
	   }
	   /**
	   * Modifica el valor de la propiedad beanReqConsMovHistCDAFechaHora
	   * @param beanReqConsMovHistCDAFechaHora Objeto del tipo BeanReqConsMovHistCDAFechaHora
	   */
	   public void setBeanReqConsMovHistCDAFechaHora(
			BeanReqConsMovHistCDAFechaHora beanReqConsMovHistCDAFechaHora) {
		   this.beanReqConsMovHistCDAFechaHora = beanReqConsMovHistCDAFechaHora;
	   }


	   
	    
}