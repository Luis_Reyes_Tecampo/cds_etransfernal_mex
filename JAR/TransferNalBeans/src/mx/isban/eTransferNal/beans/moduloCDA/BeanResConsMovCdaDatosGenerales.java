/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovCdaDatosGenerales.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   31/12/2013 16:45:28 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 * de la funcionalidad  de Consulta de Detalle Movimiento CDA
**/
public class BeanResConsMovCdaDatosGenerales implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 8221148340137326256L;

	/**Propiedad del tipo String que almacena el numero de referencia de la operacion*/
	private String referencia;
	/**Propiedad del tipo String que almacena la clave de rastreo*/
	private String cveRastreo;
	/**Propiedad del tipo String que almacena la fecha de operacion*/
	private String fechaOpe;
	/**Propiedad del tipo String que almacena la hora de abono*/
	private String hrAbono;
	/**Propiedad del tipo String que almacena la hora de envio*/
	private String hrEnvio;
	/**Propiedad del tipo String que almacena el estatusCDA*/
	private String estatusCDA;
	/**Propiedad del tipo String que almacena el tipo de pago*/
	private String tipoPago;
	/**Propiedad del tipo String que almacena la fecha de abono*/
	private String fechaAbono;
	
	/**
	 * Metodo que sirve para obtener la propiedad referencia
	 * @return referencia Objeto de tipo @see String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Modifica el valor de la propiedad referencia
	 * @param referencia Objeto de tipo @see String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * Metodo que sirve para obtener la propiedad fechaOpe
	 * @return fechaOpe Objeto de tipo @see String
	 */
	public String getFechaOpe() {
		return fechaOpe;
	}
	/**
	 * Modifica el valor de la propiedad fechaOpe
	 * @param fechaOpe Objeto de tipo @see String
	 */ 
	public void setFechaOpe(String fechaOpe) {
		this.fechaOpe = fechaOpe;
	}
	/**
	 * Metodo que sirve para obtener la propiedad cveRastreo
	 * @return cveRastreo Objeto de tipo @see String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Modifica el valor de la propiedad cveRastreo
	 * @param cveRastreo Objeto de tipo @see String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * Metodo que sirve para obtener la propiedad hrAbono
	 * @return hrAbono Objeto de tipo @see String
	 */
	public String getHrAbono() {
		return hrAbono;
	}
	/**
	 * Modifica el valor de la propiedad hrAbono
	 * @param hrAbono Objeto de tipo @see String
	 */
	public void setHrAbono(String hrAbono) {
		this.hrAbono = hrAbono;
	}
	/**
	 * Metodo que sirve para obtener la propiedad hrEnvio
	 * @return hrEnvio Objeto de tipo @see String
	 */
	public String getHrEnvio() {
		return hrEnvio;
	}
	/**
	 * Modifica el valor de la propiedad hrEnvio
	 * @param hrEnvio Objeto de tipo @see String
	 */
	public void setHrEnvio(String hrEnvio) {
		this.hrEnvio = hrEnvio;
	}
	/**
	 * Metodo que sirve para obtener la propiedad estatusCDA
	 * @return estatusCDA Objeto de tipo @see String
	 */
	public String getEstatusCDA() {
		return estatusCDA;
	}
	/**
	 * Modifica el valor de la propiedad estatusCDA
	 * @param estatusCDA Objeto de tipo @see String
	 */
	public void setEstatusCDA(String estatusCDA) {
		this.estatusCDA = estatusCDA;
	}
	/**
	 * Metodo que sirve para obtener la propiedad tipoPago
	 * @return tipoPago Objeto de tipo @see String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Modifica el valor de la propiedad tipoPago
	 * @param tipoPago Objeto de tipo @see String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * Metodo que sirve para obtener la propiedad fechaAbono
	 * @return fechaAbono Objeto de tipo @see String
	 */
	public String getFechaAbono() {
		return fechaAbono;
	}
	/**
	 * Modifica el valor de la propiedad fechaAbono
	 * @param fechaAbono Objeto de tipo @see String
	 */
	public void setFechaAbono(String fechaAbono) {
		this.fechaAbono = fechaAbono;
	}
	
}
