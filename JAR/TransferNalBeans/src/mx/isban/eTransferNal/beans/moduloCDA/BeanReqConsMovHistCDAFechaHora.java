package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**Clase bean que almacena los parametros de busqueda que tienen que ver con fecha y horario*/
public class BeanReqConsMovHistCDAFechaHora implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7698991644727416649L;
	/**
	 * Propiedad del tipo @see String que almacena la fecha de operacion de inicio
	 */
	private String fechaOpeInicio;
	/**
	 * Propiedad del tipo @see String que almacena la fecha de operacion fin
	 */
	private String fechaOpeFin;
	/**
	 * Propiedad del tipo @see String que almacena la hora abono de inicio con la
	 * que se realiza la busqueda
	 */
	private String hrAbonoIni;
	/**
	 * Propiedad del tipo @see String que almacena la hora de abono Fin con la que
	 * se realiza la busqueda
	 */
	private String hrAbonoFin;
	/**
	 * Propiedad del tipo @see String que almacena la hora envio de inicio con la
	 * que se realiza la busqueda
	 */
	private String hrEnvioIni;
	/**
	 * Propiedad del tipo @see String que almacena la hora envio fin con la que se
	 * realiza la busqueda
	 */
	private String hrEnvioFin;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOpeInicio
	 *
	 * @return fechaOpeInicio Objeto del tipo String
	 */
	public String getFechaOpeInicio() {
		return fechaOpeInicio;
	}

	/**
	 *Modifica el valor de la propiedad fechaOpeInicio
	 *
	 * @param fechaOpeInicio
	 *            Objeto del tipo String
	 */
	public void setFechaOpeInicio(String fechaOpeInicio) {
		this.fechaOpeInicio = fechaOpeInicio;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOpeFin
	 *
	 * @return fechaOpeFin Objeto del tipo String
	 */
	public String getFechaOpeFin() {
		return fechaOpeFin;
	}

	/**
	 *Modifica el valor de la propiedad fechaOpeFin
	 *
	 * @param fechaOpeFin
	 *            Objeto del tipo String
	 */
	public void setFechaOpeFin(String fechaOpeFin) {
		this.fechaOpeFin = fechaOpeFin;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hrAbonoIni
	 *
	 * @return hrAbonoIni Objeto del tipo String
	 */
	public String getHrAbonoIni() {
		return hrAbonoIni;
	}

	/**
	 *Modifica el valor de la propiedad hrAbonoIni
	 *
	 * @param hrAbonoIni
	 *            Objeto del tipo String
	 */
	public void setHrAbonoIni(String hrAbonoIni) {
		this.hrAbonoIni = hrAbonoIni;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hrAbonoFin
	 *
	 * @return hrAbonoFin Objeto del tipo String
	 */
	public String getHrAbonoFin() {
		return hrAbonoFin;
	}

	/**
	 *Modifica el valor de la propiedad hrAbonoFin
	 *
	 * @param hrAbonoFin
	 *            Objeto del tipo String
	 */
	public void setHrAbonoFin(String hrAbonoFin) {
		this.hrAbonoFin = hrAbonoFin;
	}

	/**
	 * Metodo que sirve para obtener la propiedad hrEnvioIni
	 * @return el hrEnvioIni Objeto del tipo @see String
	 */
	public String getHrEnvioIni() {
		return hrEnvioIni;
	}

	/**
	 * Modifica el valor de la propiedad hrEnvioIni
	 * @param hrEnvioIni
	 * el hrEnvioIni a establecer
	 */
	public void setHrEnvioIni(String hrEnvioIni) {
		this.hrEnvioIni = hrEnvioIni;
	}

	/**
	 * Metodo para obtener el valor de la propiedad hrEnvioFin
	 * @return el hrEnvioFin Objeto del tipo @see String
	 */
	public String getHrEnvioFin() {
		return hrEnvioFin;
	}

	/**
	 * Modifica el valor de la propiedad hrEnvioFin
	 * @param hrEnvioFin
	 *            el hrEnvioFin a establecer
	 */
	public void setHrEnvioFin(String hrEnvioFin) {
		this.hrEnvioFin = hrEnvioFin;
	}


}
