/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResTranSpeiRec.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/02/2019 01:29:42 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Class BeanResConsTranSpeiRec.
 *
 * @author FSW-Vector
 * @since 17/09/2018
 */
public class BeanResTranSpeiRec  implements  Serializable{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 602526456577457337L;
	
	/** Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de beanTranSpeiRecList. */
	private List<BeanTranSpeiRecOper> listBeanTranSpeiRec = null;
	
	/** Propiedad del tipo List<BeanMotDevolucion> que almacena el valor de beanMotDevolucionList. */
	private List<BeanMotDevolucion> beanMotDevolucionList;
	
	/** Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de listBeanTranSpeiRecOK. */
	private List<BeanTranSpeiRecOper> listBeanTranSpeiRecOK = Collections.emptyList();
	
	/** Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de listBeanTranSpeiRecNOK. */
	private List<BeanTranSpeiRecOper> listBeanTranSpeiRecNOK = Collections.emptyList();
	
	/** La variable que contiene informacion con respecto a: bean comun. */
	private BeanTranSpeiRecComun beanComun;
	
	/** La variable que contiene informacion con respecto a: bean res cons opcion. */
	private BeanResTranSpeiRecOpcion beanResConsOpcion;
	
	/** La variable que contiene informacion con respecto a: bean res cons ext. */
	private BeanResTranSpeiRecExt beanResConsExt;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	private BeanResBase beanError;
	
	/** Propiedad del tipo BeanPaginador que almacena el valor de paginador. */
	private BeanPaginador beanPaginador;
		
	/** La variable que contiene informacion con respecto a: bean busqueda list. */
	private List<BeanMotDevolucion> beanBusquedaList;
	
	/** Constructor del objeto BeanResTranSpeiRec */
	public BeanResTranSpeiRec() {
		super();
		this.beanComun = new BeanTranSpeiRecComun();
		this.beanResConsOpcion = new BeanResTranSpeiRecOpcion();
		this.beanResConsExt = new BeanResTranSpeiRecExt();
		this.beanError = new BeanResBase();
		this.beanPaginador = new BeanPaginador();
	}

	/** La variable que contiene informacion con respecto a: bean busqueda list. */
	private List<BeanMotDevolucion> beanListUsuarios;
	

	/** La variable que contiene informacion con respecto a: bean list desc tipo pago. */
	private List<BeanMotDevolucion> beanListDescTipoPago;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad beanMotDevolucionList.
	 *
	 * @return beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public List<BeanMotDevolucion> getBeanMotDevolucionList() {
		return beanMotDevolucionList;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad beanMotDevolucionList.
	 *
	 * @param beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public void setBeanMotDevolucionList(
			List<BeanMotDevolucion> beanMotDevolucionList) {
		this.beanMotDevolucionList = beanMotDevolucionList;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listBeanTranSpeiRecOK.
	 *
	 * @return listBeanTranSpeiRecOK Objeto del tipo List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRecOper> getListBeanTranSpeiRecOK() {
		return listBeanTranSpeiRecOK;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanTranSpeiRecOK.
	 *
	 * @param listBeanTranSpeiRecOK del tipo List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRecOK(List<BeanTranSpeiRecOper> listBeanTranSpeiRecOK) {
		this.listBeanTranSpeiRecOK = listBeanTranSpeiRecOK;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listBeanTranSpeiRecNOK.
	 *
	 * @return listBeanTranSpeiRecNOK Objeto del tipo List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRecOper> getListBeanTranSpeiRecNOK() {
		return listBeanTranSpeiRecNOK;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanTranSpeiRecNOK.
	 *
	 * @param listBeanTranSpeiRecNOK del tipo List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRecNOK(
			List<BeanTranSpeiRecOper> listBeanTranSpeiRecNOK) {
		this.listBeanTranSpeiRecNOK = listBeanTranSpeiRecNOK;
	}

	/**
	 * Obtener el objeto: list bean tran spei rec.
	 *
	 * @return El objeto: list bean tran spei rec
	 */
	public List<BeanTranSpeiRecOper> getListBeanTranSpeiRec() {
		return listBeanTranSpeiRec;
	}

	/**
	 * Definir el objeto: list bean tran spei rec.
	 *
	 * @param listBeanTranSpeiRec El nuevo objeto: list bean tran spei rec
	 */
	public void setListBeanTranSpeiRec(List<BeanTranSpeiRecOper> listBeanTranSpeiRec) {
		this.listBeanTranSpeiRec = listBeanTranSpeiRec;
	}

	/**
	 * Obtener el objeto: bean comun.
	 *
	 * @return El objeto: bean comun
	 */
	public BeanTranSpeiRecComun getBeanComun() {
		return beanComun;
	}

	/**
	 * Definir el objeto: bean comun.
	 *
	 * @param beanComun El nuevo objeto: bean comun
	 */
	public void setBeanComun(BeanTranSpeiRecComun beanComun) {
		this.beanComun = beanComun;
	}

	
	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: bean res cons opcion.
	 *
	 * @return El objeto: bean res cons opcion
	 */
	public BeanResTranSpeiRecOpcion getBeanResConsOpcion() {
		return beanResConsOpcion;
	}

	/**
	 * Definir el objeto: bean res cons opcion.
	 *
	 * @param beanResConsOpcion El nuevo objeto: bean res cons opcion
	 */
	public void setBeanResConsOpcion(BeanResTranSpeiRecOpcion beanResConsOpcion) {
		this.beanResConsOpcion = beanResConsOpcion;
	}

	/**
	 * Obtener el objeto: bean res cons ext.
	 *
	 * @return El objeto: bean res cons ext
	 */
	public BeanResTranSpeiRecExt getBeanResConsExt() {
		return beanResConsExt;
	}

	/**
	 * Definir el objeto: bean res cons ext.
	 *
	 * @param beanResConsExt El nuevo objeto: bean res cons ext
	 */
	public void setBeanResConsExt(BeanResTranSpeiRecExt beanResConsExt) {
		this.beanResConsExt = beanResConsExt;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Obtener el objeto: bean busqueda list.
	 *
	 * @return El objeto: bean busqueda list
	 */
	public List<BeanMotDevolucion> getBeanBusquedaList() {
		return beanBusquedaList;
	}

	/**
	 * Definir el objeto: bean busqueda list.
	 *
	 * @param beanBusquedaList El nuevo objeto: bean busqueda list
	 */
	public void setBeanBusquedaList(List<BeanMotDevolucion> beanBusquedaList) {
		this.beanBusquedaList = beanBusquedaList;
	}

	/**
	 * Obtener el objeto: bean list usuarios.
	 *
	 * @return El objeto: bean list usuarios
	 */
	public List<BeanMotDevolucion> getBeanListUsuarios() {
		return beanListUsuarios;
	}

	/**
	 * Definir el objeto: bean list usuarios.
	 *
	 * @param beanListUsuarios El nuevo objeto: bean list usuarios
	 */
	public void setBeanListUsuarios(List<BeanMotDevolucion> beanListUsuarios) {
		this.beanListUsuarios = beanListUsuarios;
	}
	
	/**
	 * Obtener el objeto: bean list desc tipo pago.
	 *
	 * @return El objeto: bean list desc tipo pago.
	 */
	public List<BeanMotDevolucion> getBeanListDescTipoPago() {
		return beanListDescTipoPago;
	}
	
	/**
	 * Definir el objeto: bean list desc tipo pago..
	 *
	 * @param beanListDescTipoPago El nuevo objeto: bean list desc tipo pago.
	 */
	public void setBeanListDescTipoPago(List<BeanMotDevolucion> beanListDescTipoPago) {
		this.beanListDescTipoPago = beanListDescTipoPago;
	}
	
	

}
