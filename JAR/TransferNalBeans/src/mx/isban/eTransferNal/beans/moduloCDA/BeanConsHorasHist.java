/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanConsHorasHist.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/06/2019 07:13:13 PM Uriel Alfredo Botello R. Vector Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * The Class BeanConsHorasHist que contiene todos los datos de la tabla horas SPEI.
 */
public class BeanConsHorasHist implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6650201344124189393L;

	/** The bean general horas. */
	private BeanGeneralHoras beanGeneralHoras;

	/** The bean cves horas. */
	private BeanCvesHoras beanCvesHoras;

	/** The bean fechas horas. */
	private BeanFechasHoras beanFechasHoras;
	
	/** The bean errors. */
	private BeanResBase beanErrors;

	/**
	 * Instantiates a new bean cons horas hist.
	 */
	public BeanConsHorasHist() {
		super();
		this.beanCvesHoras = new BeanCvesHoras();
		this.beanFechasHoras = new BeanFechasHoras();
		this.beanGeneralHoras = new BeanGeneralHoras();
		this.beanErrors = new BeanResBase();
	}

	/**
	 * Gets the bean general horas.
	 *
	 * @return the beanGeneralHoras
	 */
	public BeanGeneralHoras getBeanGeneralHoras() {
		return beanGeneralHoras;
	}

	/**
	 * Sets the bean general horas.
	 *
	 * @param beanGeneralHoras the beanGeneralHoras to set
	 */
	public void setBeanGeneralHoras(BeanGeneralHoras beanGeneralHoras) {
		this.beanGeneralHoras = beanGeneralHoras;
	}

	/**
	 * Gets the bean cves horas.
	 *
	 * @return the beanCvesHoras
	 */
	public BeanCvesHoras getBeanCvesHoras() {
		return beanCvesHoras;
	}

	/**
	 * Sets the bean cves horas.
	 *
	 * @param beanCvesHoras the beanCvesHoras to set
	 */
	public void setBeanCvesHoras(BeanCvesHoras beanCvesHoras) {
		this.beanCvesHoras = beanCvesHoras;
	}

	/**
	 * Gets the bean fechas horas.
	 *
	 * @return the beanFechasHoras
	 */
	public BeanFechasHoras getBeanFechasHoras() {
		return beanFechasHoras;
	}

	/**
	 * Sets the bean fechas horas.
	 *
	 * @param beanFechasHoras the beanFechasHoras to set
	 */
	public void setBeanFechasHoras(BeanFechasHoras beanFechasHoras) {
		this.beanFechasHoras = beanFechasHoras;
	}

	/**
	 * Gets the bean errors.
	 *
	 * @return the beanErrors
	 */
	public BeanResBase getBeanErrors() {
		return beanErrors;
	}

	/**
	 * Sets the bean errors.
	 *
	 * @param beanErrors the beanErrors to set
	 */
	public void setBeanErrors(BeanResBase beanErrors) {
		this.beanErrors = beanErrors;
	}
	
	
	
	
}
