/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanGenArchHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 15 21:49:36 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos de Generar Archivo
 * CDA Historico Detalle
**/

public class BeanGenArchHistDet  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo @see String que almacena el id de la peticion*/
   private String idPeticion;
   /**Propiedad del tipo @see String que almacena la clave de rastreo*/
   private String claveRastreo;
   /**Propiedad del tipo @see String que almacena el numero de cuenta
   del beneficiario*/
   private String numCuentaBeneficiario;
   /**Propiedad del tipo @see String que almacena el monto del pago*/
   private String montoPago;
   /**Propiedad del tipo @see String que almacena el nombre de la institucion
   emisora*/
   private String nomInstEmisora;
   /**Propiedad del tipo @see String que almacena la fecha de operacion*/
   private String fchOperacion;
   /**Propiedad del tipo @see String que almacena la cveMiInstitucion*/
   private String cveMiInstitucion;
   /**Propiedad del tipo @see String que almacena la cveInstOrd*/
   private String cveInstOrd;
   /**Propiedad del tipo @see String que almacena la folioPaquete*/
   private String folioPaquete;
   /**Propiedad del tipo @see String que almacena la folioPago*/
   private String folioPago;
   
   
   /**Propiedad del tipo boolean que indica si fue marcado para ser
   eliminado*/
   private boolean seleccionado;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad idPeticion
   * @return idPeticion Objeto del tipo String
   */
   public String getIdPeticion(){
      return idPeticion;
   }
   /**
   *Modifica el valor de la propiedad idPeticion
   * @param idPeticion Objeto del tipo String
   */
   public void setIdPeticion(String idPeticion){
      this.idPeticion=idPeticion;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad claveRastreo
   * @return claveRastreo Objeto del tipo String
   */
   public String getClaveRastreo(){
      return claveRastreo;
   }
   /**
   *Modifica el valor de la propiedad claveRastreo
   * @param claveRastreo Objeto del tipo String
   */
   public void setClaveRastreo(String claveRastreo){
      this.claveRastreo=claveRastreo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numCuentaBeneficiario
   * @return numCuentaBeneficiario Objeto del tipo String
   */
   public String getNumCuentaBeneficiario(){
      return numCuentaBeneficiario;
   }
   /**
   *Modifica el valor de la propiedad numCuentaBeneficiario
   * @param numCuentaBeneficiario Objeto del tipo String
   */
   public void setNumCuentaBeneficiario(String numCuentaBeneficiario){
      this.numCuentaBeneficiario=numCuentaBeneficiario;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad montoPago
   * @return montoPago Objeto del tipo String
   */
   public String getMontoPago(){
      return montoPago;
   }
   /**
   *Modifica el valor de la propiedad montoPago
   * @param montoPago Objeto del tipo String
   */
   public void setMontoPago(String montoPago){
      this.montoPago=montoPago;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomInstEmisora
   * @return nomInstEmisora Objeto del tipo String
   */
   public String getNomInstEmisora(){
      return nomInstEmisora;
   }
   /**
   *Modifica el valor de la propiedad nomInstEmisora
   * @param nomInstEmisora Objeto del tipo String
   */
   public void setNomInstEmisora(String nomInstEmisora){
      this.nomInstEmisora=nomInstEmisora;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad seleccionado
   * @return seleccionado Objeto del tipo boolean
   */
   public boolean getSeleccionado(){
      return seleccionado;
   }
   /**
   *Modifica el valor de la propiedad seleccionado
   * @param seleccionado Objeto del tipo boolean
   */
   public void setSeleccionado(boolean seleccionado){
      this.seleccionado=seleccionado;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad FchOperacion
    * @return FchOperacion Objeto del tipo String
    */   
   public String getFchOperacion() {
	   return fchOperacion;
   }
   /**
    *Modifica el valor de la propiedad fchOperacion
    * @param fchOperacion Objeto del tipo String
    */
   public void setFchOperacion(String fchOperacion) {
       this.fchOperacion = fchOperacion;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad cveMiInstitucion
    * @return cveMiInstitucion Objeto del tipo String
    */
   public String getCveMiInstitucion() {
	   return cveMiInstitucion;
   }
   /**
    *Modifica el valor de la propiedad cveMiInstitucion
    * @param cveMiInstitucion Objeto del tipo String
    */
   public void setCveMiInstitucion(String cveMiInstitucion) {
	   this.cveMiInstitucion = cveMiInstitucion;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad cveInstOrd
    * @return cveInstOrd Objeto del tipo String
    */
   public String getCveInstOrd() {
	   return cveInstOrd;
   }
   /**
    *Modifica el valor de la propiedad cveInstOrd
    * @param cveInstOrd Objeto del tipo String
    */
   public void setCveInstOrd(String cveInstOrd) {
	   this.cveInstOrd = cveInstOrd;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad folioPaquete
    * @return folioPaquete Objeto del tipo String
    */
   public String getFolioPaquete() {
	   return folioPaquete;
   }
   /**
    *Modifica el valor de la propiedad folioPaquete
    * @param folioPaquete Objeto del tipo String
    */
   public void setFolioPaquete(String folioPaquete) {
	   this.folioPaquete = folioPaquete;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad folioPago
    * @return folioPago Objeto del tipo String
    */
   public String getFolioPago() {
	   return folioPago;
   }
   /**
    *Modifica el valor de la propiedad folioPago
    * @param folioPago Objeto del tipo String
    */
   public void setFolioPago(String folioPago) {
	   this.folioPago = folioPago;
   }

}
