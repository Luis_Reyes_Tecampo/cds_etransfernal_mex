/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepcionOperaCambios.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   06/04/2018   SALVADOR MEZA  VECTOR 		Creacion de Bean
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos 
 * que  forman el detalle de recepcion de operaciones, nuevos campos agregados a la pantalla del detalle
 * apartado: Cambios.
 *
 * @author FSW-Vector
 * @since 11/02/2019
 */
public class BeanDetRecepcionOperaCambios implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4432936196193666204L;
	
	/** La variable que contiene informacion con respecto a: buc cliente. */
	@Size(max=8)
	private String bucCliente;
	
	/** La variable que contiene informacion con respecto a: cta receptora ori. */
	@Size(max=20)
	private String ctaReceptoraOri;
	
	/** La variable que contiene informacion con respecto a: cta receptora int. */
	@Size(max=20)
	private String ctaReceptoraInt;	
	
	/** La variable que contiene informacion con respecto a: aplicativo. */
	@Size(max=40)
	private String aplicativo;
	
	/** La variable que contiene informacion con respecto a: version. */
	@Size(max=40)
	private String version;
	
	/** La variable que contiene informacion con respecto a: long rastreo. */
	@Size(max=30)
	private String longRastreo;
			
	/** La variable que contiene informacion con respecto a: fch captura. */
	@Size(max=10)
	private String fchCaptura;
	
	/** La variable que contiene informacion con respecto a: motivo devol. */
	@Size(max=5)
	private String motivoDevol;
	
	/** La variable que contiene informacion con respecto a: referencia numerica. */
	@Size(max=7)
	private String refeNumerica;
	
	/** La variable que contiene informacion con respecto a: otros. */
	@Valid
	private BeanDetRecOperaOtros otros;
	
	/** La variable que contiene informacion con respecto a: motivo devol desc. */
	@Size(max=60)
	private String motivoDevolDesc;
	
	/**
	 * Obtener el objeto: buc cliente.
	 *
	 * @return El objeto: buc cliente
	 */
	public String getBucCliente() {
		return bucCliente;
	}

	
	/**
	 * Definir el objeto: buc cliente.
	 *
	 * @param bucCliente El nuevo objeto: buc cliente
	 */
	public void setBucCliente(String bucCliente) {
		this.bucCliente = bucCliente;
	}

	/**
	 * Obtener el objeto: cta receptora ori.
	 *
	 * @return El objeto: cta receptora ori
	 */
	public String getCtaReceptoraOri() {
		return ctaReceptoraOri;
	}

	/**
	 * Definir el objeto: cta receptora ori.
	 *
	 * @param ctaReceptoraOri El nuevo objeto: cta receptora ori
	 */
	public void setCtaReceptoraOri(String ctaReceptoraOri) {
		this.ctaReceptoraOri = ctaReceptoraOri;
	}

	/**
	 * Obtener el objeto: cta receptora int.
	 *
	 * @return El objeto: cta receptora int
	 */
	public String getCtaReceptoraInt() {
		return ctaReceptoraInt;
	}

	/**
	 * Definir el objeto: cta receptora int.
	 *
	 * @param ctaReceptoraInt El nuevo objeto: cta receptora int
	 */
	public void setCtaReceptoraInt(String ctaReceptoraInt) {
		this.ctaReceptoraInt = ctaReceptoraInt;
	}

	/**
	 * Obtener el objeto: aplicativo.
	 *
	 * @return El objeto: aplicativo
	 */
	public String getAplicativo() {
		return aplicativo;
	}

	/**
	 * Definir el objeto: aplicativo.
	 *
	 * @param aplicativo El nuevo objeto: aplicativo
	 */
	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}

	/**
	 * Obtener el objeto: version.
	 *
	 * @return El objeto: version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Definir el objeto: version.
	 *
	 * @param version El nuevo objeto: version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Obtener el objeto: long rastreo.
	 *
	 * @return El objeto: long rastreo
	 */
	public String getLongRastreo() {
		return longRastreo;
	}

	/**
	 * Definir el objeto: long rastreo.
	 *
	 * @param longRastreo El nuevo objeto: long rastreo
	 */
	public void setLongRastreo(String longRastreo) {
		this.longRastreo = longRastreo;
	}

	
	/**
	 * Obtener el objeto: fch captura.
	 *
	 * @return El objeto: fch captura
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}

	/**
	 * Definir el objeto: fch captura.
	 *
	 * @param fchCaptura El nuevo objeto: fch captura
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}
	
	/**
	 * Obtener el objeto: motivo devol.
	 *
	 * @return El objeto: motivo devol
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}

	/**
	 * Definir el objeto: motivo devol.
	 *
	 * @param motivoDevol El nuevo objeto: motivo devol
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	
	/**
	 * Obtener el objeto: refe numerica.
	 *
	 * @return El objeto: refe numerica
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}

	/**
	 * Definir el objeto: refe numerica.
	 *
	 * @param refeNumerica El nuevo objeto: refe numerica
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}
	
	/**
	 * Obtener el objeto: otros.
	 *
	 * @return El objeto: otros
	 */
	public BeanDetRecOperaOtros getOtros() {
		return otros;
	}

	/**
	 * Definir el objeto: otros.
	 *
	 * @param otros El nuevo objeto: otros
	 */
	public void setOtros(BeanDetRecOperaOtros otros) {
		this.otros = otros;
	}
	
	/**
	 * Obtener el objeto: motivo devol desc.
	 *
	 * @return El objeto: motivo devol desc
	 */
	public String getMotivoDevolDesc() {
		return motivoDevolDesc;
	}

	/**
	 * Definir el objeto: motivo devol desc.
	 *
	 * @param motivoDevol El nuevo objeto: motivo devol desc
	 */
	public void setMotivoDevolDesc(String motivoDevolDesc) {
		this.motivoDevolDesc = motivoDevolDesc;
	}
	
}