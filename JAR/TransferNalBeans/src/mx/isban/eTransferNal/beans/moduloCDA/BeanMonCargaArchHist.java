/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMonCargaArchHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de os campos del monitor de
 * carga de archivos historicos
**/

public class BeanMonCargaArchHist  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad que almacena el idPeticion*/
   private String idPeticion;
   /**Propiedad que almacena el nombre del archivo*/
   private String nomArchivo;
   /**Propiedad que almacena el estatus de carga del archivo*/
   private String estatusCargaArch;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad idPeticion
   * @return idPeticion Objeto del tipo String
   */
   public String getIdPeticion(){
      return idPeticion;
   }
   /**
   *Modifica el valor de la propiedad idPeticion
   * @param idPeticion Objeto del tipo String
   */
   public void setIdPeticion(String idPeticion){
      this.idPeticion=idPeticion;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomArchivo
   * @return nomArchivo Objeto del tipo String
   */
   public String getNomArchivo(){
      return nomArchivo;
   }
   /**
   *Modifica el valor de la propiedad nomArchivo
   * @param nomArchivo Objeto del tipo String
   */
   public void setNomArchivo(String nomArchivo){
      this.nomArchivo=nomArchivo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad estatusCargaArch
   * @return estatusCargaArch Objeto del tipo String
   */
   public String getEstatusCargaArch(){
      return estatusCargaArch;
   }
   /**
   *Modifica el valor de la propiedad estatusCargaArch
   * @param estatusCargaArch Objeto del tipo String
   */
   public void setEstatusCargaArch(String estatusCargaArch){
      this.estatusCargaArch=estatusCargaArch;
   }



}
