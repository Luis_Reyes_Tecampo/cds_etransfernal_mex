/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanReqTranSpeiRec.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/02/2019 01:32:00 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;


/**
 * Clase de tipo bean que contiene el resquest de la pantalla de recepcion de operaciones
 * de cuenta alterna y principal, es la sumatoria de la tabla.
 *
 * @author FSW-Vector
 * @since 17/09/2018
 */
public class BeanReqTranSpeiRec implements Serializable{
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 4791157012036732559L;
	
	/** Propiedad del tipo BeanPaginador que almacena el valor de paginador. */
	private BeanPaginador paginador;
	
	/** Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de beanTranSpeiRecList. */
	private List<BeanTranSpeiRecOper> listBeanTranSpeiRec = null;
	
	/** La variable que contiene informacion con respecto a: bean comun. */
	private BeanTranSpeiRecComun beanComun;
	
	
	

	/**
	 * Metodo get que obtiene el valor de la propiedad paginador.
	 *
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paginador.
	 *
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	
	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanTranSpeiRec.
	 *
	 * @return listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRecOper> getListBeanTranSpeiRec() {
		return listBeanTranSpeiRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad listBeanTranSpeiRec.
	 *
	 * @param listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRec(List<BeanTranSpeiRecOper> listBeanTranSpeiRec) {
		this.listBeanTranSpeiRec = listBeanTranSpeiRec;
	}

	/**
	 * Obtener el objeto: bean comun.
	 *
	 * @return El objeto: bean comun
	 */
	public BeanTranSpeiRecComun getBeanComun() {
		return beanComun;
	}

	/**
	 * Definir el objeto: bean comun.
	 *
	 * @param beanComun El nuevo objeto: bean comun
	 */
	public void setBeanComun(BeanTranSpeiRecComun beanComun) {
		this.beanComun = beanComun;
	}

	
	
}