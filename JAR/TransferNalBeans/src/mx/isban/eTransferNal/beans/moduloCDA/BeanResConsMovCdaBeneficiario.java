/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovCdaBeneficiario.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   31/12/2013 16:45:28 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 * de la funcionalidad  de Consulta de Detalle Movimiento CDA
**/
public class BeanResConsMovCdaBeneficiario implements Serializable{
	
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 3466762442220291970L;
	/**Propiedad del tipo String que almacena el numero de cta del beneficiario*/
	private String ctaBenef;
	/**Propiedad del tipo String que almacena el monto de la operacion*/
	private String monto;
	/**Propiedad del tipo String que almacena el RFC/CURP del beneficiario*/
	private String rfcCurpBeneficiario;
	/**Propiedad del tipo String que almacena el nombre de la institucion receptora*/
	private String nomInstRec;
	/**Propiedad del tipo String que almacena el tipo de cuenta Beneficiario*/
	private String tipoCuentaBened;
	/**Propiedad del tipo String que almacena el nombre del beneficiario*/
	private String nombreBened;
	/**Propiedad del tipo String que almacena el concepto de pago*/
	private String conceptoPago;
	/**Propiedad del tipo String que almacena el importe de IVA*/
	private String importeIVA;
	
	/**
	 * Metodo que sirve para obtener la propiedad ctaBenef
	 * @return ctaBenef Objeto de tipo @see String
	 */ 
	public String getCtaBenef() {
		return ctaBenef;
	}
	/**
	 * Modifica el valor de la propiedad ctaBenef
	 * @param ctaBenef Objeto de tipo @see String
	 */
	public void setCtaBenef(String ctaBenef) {
		this.ctaBenef = ctaBenef;
	}
	/**
	 * Metodo que sirve para obtener la propiedad monto
	 * @return monto Objeto de tipo @see String
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 * Modifica el valor de la propiedad monto
	 * @param monto Objeto de tipo @see String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	/**
	 * Metodo que sirve para obtener la propiedad nomInstRec
	 * @return nomInstRec Objeto de tipo @see String
	 */
	public String getNomInstRec() {
		return nomInstRec;
	}
	/**
	 * Modifica el valor de la propiedad nomInstRec
	 * @param nomInstRec Objeto de tipo @see String
	 */
	public void setNomInstRec(String nomInstRec) {
		this.nomInstRec = nomInstRec;
	}
	/**
	 * Metodo que sirve para obtener la propiedad tipoCuentaBened
	 * @return tipoCuentaBened Objeto de tipo @see String
	 */
	public String getTipoCuentaBened() {
		return tipoCuentaBened;
	}
	/**
	 * Modifica el valor de la propiedad tipoCuentaBened
	 * @param tipoCuentaBened Objeto de tipo @see String
	 */
	public void setTipoCuentaBened(String tipoCuentaBened) {
		this.tipoCuentaBened = tipoCuentaBened;
	}
	/**
	 * Metodo que sirve para obtener la propiedad nombreBened
	 * @return nombreBened Objeto de tipo @see String
	 */ 
	public String getNombreBened() {
		return nombreBened;
	}
	/**
	 * Modifica el valor de la propiedad nombreBened
	 * @param nombreBened Objeto de tipo @see String
	 */
	public void setNombreBened(String nombreBened) {
		this.nombreBened = nombreBened;
	}
	
	/**
	 * Metodo que sirve para obtener la propiedad rfcCurpBeneficiario
	 * @return rfcCurpBeneficiario Objeto de tipo @see String
	 */
	public String getRfcCurpBeneficiario() {
		return rfcCurpBeneficiario;
	}
	/**
	 * Modifica el valor de la propiedad rfcCurpBeneficiario
	 * @param rfcCurpBeneficiario Objeto de tipo @see String
	 */
	public void setRfcCurpBeneficiario(String rfcCurpBeneficiario) {
		this.rfcCurpBeneficiario = rfcCurpBeneficiario;
	}
	/**
	 * Metodo que sirve para obtener la propiedad conceptoPago
	 * @return conceptoPago Objeto de tipo @see String
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	/**
	 * Modifica el valor de la propiedad conceptoPago
	 * @param conceptoPago Objeto de tipo @see String
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	/**
	 * Metodo que sirve para obtener la propiedad importeIVA
	 * @return importeIVA Objeto de tipo @see String
	 */
	public String getImporteIVA() {
		return importeIVA;
	}
	/**
	 * Modifica el valor de la propiedad importeIVA
	 * @param importeIVA Objeto de tipo @see String
	 */
	public void setImporteIVA(String importeIVA) {
		this.importeIVA = importeIVA;
	}
}
