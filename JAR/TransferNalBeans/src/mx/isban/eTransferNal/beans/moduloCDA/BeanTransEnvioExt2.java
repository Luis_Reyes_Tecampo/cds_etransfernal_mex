package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanTransEnvioExt2 implements Serializable{
   	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -4168124229012298738L;

	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec2
	 */
	private String nombreRec2 = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de rfcRec2
	 */
	private String rfcRec2 = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec2
	 */
	private String tipoCuentaRec2 = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de conceptoPago2
	 */
	private String conceptoPago2 = "";

	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaOrd
	 */
	private String tipoCuentaOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec
	 */
	private String tipoCuentaRec = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de comentario1
	 */
	private String comentario1;
	
	/**
	 * Propiedad del tipo String que almacena el valor de comentario2
	 */
	private String comentario2;
	
	/**
	 * Propiedad del tipo String que almacena el valor de comentario3
	 */
	private String comentario3;
	/**
	 * Propiedad del tipo String que almacena el valor de comentario3
	 */
	private String comentario4;
	
	/**
	 * Propiedad del tipo String que almacena el valor de servicio
	 */
	private String servicio;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad nombreRec2
	 * @return nombreRec2 Objeto de tipo @see String
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreRec2
	 * @param nombreRec2 Objeto de tipo @see String
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad rfcRec2
	 * @return rfcRec2 Objeto de tipo @see String
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad rfcRec2
	 * @param rfcRec2 Objeto de tipo @see String
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaRec2
	 * @return tipoCuentaRec2 Objeto de tipo @see String
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaRec2
	 * @param tipoCuentaRec2 Objeto de tipo @see String
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad conceptoPago2
	 * @return conceptoPago2 Objeto de tipo @see String
	 */
	public String getConceptoPago2() {
		return conceptoPago2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad conceptoPago2
	 * @param conceptoPago2 Objeto de tipo @see String
	 */
	public void setConceptoPago2(String conceptoPago2) {
		this.conceptoPago2 = conceptoPago2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaOrd
	 * @return tipoCuentaOrd Objeto de tipo @see String
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaOrd
	 * @param tipoCuentaOrd Objeto de tipo @see String
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad tipoCuentaRec
	 * @return tipoCuentaRec Objeto de tipo @see String
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}
	/**
	 * Metodo que modifica el valor de la propiedad tipoCuentaRec
	 * @param tipoCuentaRec Objeto de tipo @see String
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad comentario1
	 * @return comentario1 Objeto de tipo @see String
	 */
	public String getComentario1() {
		return comentario1;
	}
	/**
	 * Metodo que modifica el valor de la propiedad comentario1
	 * @param comentario1 Objeto de tipo @see String
	 */
	public void setComentario1(String comentario1) {
		this.comentario1 = comentario1;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad comentario2
	 * @return comentario2 Objeto de tipo @see String
	 */
	public String getComentario2() {
		return comentario2;
	}
	/**
	 * Metodo que modifica el valor de la propiedad comentario2
	 * @param comentario2 Objeto de tipo @see String
	 */
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad comentario3
	 * @return comentario3 Objeto de tipo @see String
	 */
	public String getComentario3() {
		return comentario3;
	}
	/**
	 * Metodo que modifica el valor de la propiedad comentario3
	 * @param comentario3 Objeto de tipo @see String
	 */
	public void setComentario3(String comentario3) {
		this.comentario3 = comentario3;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad comentario4
	 * @return comentario4 Objeto de tipo @see String
	 */
	public String getComentario4() {
		return comentario4;
	}
	/**
	 * Metodo que modifica el valor de la propiedad comentario4
	 * @param comentario4 Objeto de tipo @see String
	 */
	public void setComentario4(String comentario4) {
		this.comentario4 = comentario4;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad servicio
	 * @return servicio Objeto de tipo @see String
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * Metodo que modifica el valor de la propiedad servicio
	 * @param servicio Objeto de tipo @see String
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	

}
