/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResTranSpeiRecOpcion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/02/2019 01:25:55 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;


/**
 * Clase de tipo bean que contiene el responce de la pantalla de recepcion de operaciones
 * de cuenta alterna y principal, es la sumatoria de la tabla.
 *
 * @author FSW-Vector
 * @since 17/09/2018
 */
public class BeanResTranSpeiRecOpcion  implements  Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5032951777890599460L;


	/** Propiedad del tipo String que almacena el valor de strMontoTopoVLiquidadas. */
	private String strMontoTopoVLiquidadas;
	
	
	/** Propiedad del tipo String que almacena el valor de strVolumenTopoVLiquidadas. */
	private String strVolumenTopoVLiquidadas;
	

	/** Propiedad del tipo String que almacena el valor de montoTopoTLiquidadas. */
	private String strMontoTopoTLiquidadas;
	
	
	/** Propiedad del tipo String que almacena el valor de strVolumenTopoTLiquidadas. */
	private String strVolumenTopoTLiquidadas;
	
	/** Propiedad del tipo String que almacena el valor de montoTopoTXLiquidar. */
	private String strMontoTopoTXLiquidar;
	
	/** Propiedad del tipo String que almacena el valor de strVolumenTopoTXLiquidar. */
	private String strVolumenTopoTXLiquidar;
	
	/** Propiedad del tipo String que almacena el valor de strMontoRechazos. */
	private String strMontoRechazos;	

	/** Propiedad del tipo String que almacena el valor de strVolumenRechazos. */
	private String strVolumenRechazos;
	/** La variable que contiene informacion con respecto a: nombre pantalla. */
	private String eliminarTodo;
	
	
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoVLiquidadas.
	 *
	 * @return strMontoTopoVLiquidadas Objeto de tipo @see String
	 */
	public String getStrMontoTopoVLiquidadas() {
		return strMontoTopoVLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoVLiquidadas.
	 *
	 * @param strMontoTopoVLiquidadas Objeto de tipo @see String
	 */
	public void setStrMontoTopoVLiquidadas(String strMontoTopoVLiquidadas) {
		this.strMontoTopoVLiquidadas = strMontoTopoVLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoVLiquidadas.
	 *
	 * @return strVolumenTopoVLiquidadas Objeto de tipo @see String
	 */
	public String getStrVolumenTopoVLiquidadas() {
		return strVolumenTopoVLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoVLiquidadas.
	 *
	 * @param strVolumenTopoVLiquidadas Objeto de tipo @see String
	 */
	public void setStrVolumenTopoVLiquidadas(String strVolumenTopoVLiquidadas) {
		this.strVolumenTopoVLiquidadas = strVolumenTopoVLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoTLiquidadas.
	 *
	 * @return strMontoTopoTLiquidadas Objeto de tipo @see String
	 */
	public String getStrMontoTopoTLiquidadas() {
		return strMontoTopoTLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoTLiquidadas.
	 *
	 * @param strMontoTopoTLiquidadas Objeto de tipo @see String
	 */
	public void setStrMontoTopoTLiquidadas(String strMontoTopoTLiquidadas) {
		this.strMontoTopoTLiquidadas = strMontoTopoTLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoTLiquidadas.
	 *
	 * @return strVolumenTopoTLiquidadas Objeto de tipo @see String
	 */
	public String getStrVolumenTopoTLiquidadas() {
		return strVolumenTopoTLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoTLiquidadas.
	 *
	 * @param strVolumenTopoTLiquidadas Objeto de tipo @see String
	 */
	public void setStrVolumenTopoTLiquidadas(String strVolumenTopoTLiquidadas) {
		this.strVolumenTopoTLiquidadas = strVolumenTopoTLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoTXLiquidar.
	 *
	 * @return strMontoTopoTXLiquidar Objeto de tipo @see String
	 */
	public String getStrMontoTopoTXLiquidar() {
		return strMontoTopoTXLiquidar;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoTXLiquidar.
	 *
	 * @param strMontoTopoTXLiquidar Objeto de tipo @see String
	 */
	public void setStrMontoTopoTXLiquidar(String strMontoTopoTXLiquidar) {
		this.strMontoTopoTXLiquidar = strMontoTopoTXLiquidar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoTXLiquidar.
	 *
	 * @return strVolumenTopoTXLiquidar Objeto de tipo @see String
	 */
	public String getStrVolumenTopoTXLiquidar() {
		return strVolumenTopoTXLiquidar;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoTXLiquidar.
	 *
	 * @param strVolumenTopoTXLiquidar Objeto de tipo @see String
	 */
	public void setStrVolumenTopoTXLiquidar(String strVolumenTopoTXLiquidar) {
		this.strVolumenTopoTXLiquidar = strVolumenTopoTXLiquidar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoRechazos.
	 *
	 * @return strMontoRechazos Objeto de tipo @see String
	 */
	public String getStrMontoRechazos() {
		return strMontoRechazos;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strMontoRechazos.
	 *
	 * @param strMontoRechazos Objeto de tipo @see String
	 */
	public void setStrMontoRechazos(String strMontoRechazos) {
		this.strMontoRechazos = strMontoRechazos;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenRechazos.
	 *
	 * @return strVolumenRechazos Objeto de tipo @see String
	 */
	public String getStrVolumenRechazos() {
		return strVolumenRechazos;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenRechazos.
	 *
	 * @param strVolumenRechazos Objeto de tipo @see String
	 */
	public void setStrVolumenRechazos(String strVolumenRechazos) {
		this.strVolumenRechazos = strVolumenRechazos;
	}

	/**
	 * getEliminarTodo de tipo String.
	 * @author FSW-Vector
	 * @return eliminarTodo de tipo String
	 */
	public String getEliminarTodo() {
		return eliminarTodo;
	}

	/**
	 * setEliminarTodo para asignar valor a eliminarTodo.
	 * @author FSW-Vector
	 * @param eliminarTodo de tipo String
	 */
	public void setEliminarTodo(String eliminarTodo) {
		this.eliminarTodo = eliminarTodo;
	}
	
	
	

}
