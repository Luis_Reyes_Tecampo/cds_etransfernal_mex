package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanMotDevolucion implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -4833705161774219291L;

	
	/**
	 * Propiedad del tipo String que almacena el valor de secuencial
	 */
	private String secuencial;
	/**
	 * Propiedad del tipo String que almacena el valor de descripcion
	 */
	private String descripcion;
	/**
	 * Metodo get que obtiene el valor de la propiedad secuencial
	 * @return secuencial Objeto de tipo @see String
	 */
	public String getSecuencial() {
		return secuencial;
	}
	/**
	 * Metodo que modifica el valor de la propiedad secuencial
	 * @param secuencial Objeto de tipo @see String
	 */
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad descripcion
	 * @return descripcion Objeto de tipo @see String
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad descripcion
	 * @param descripcion Objeto de tipo @see String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
