package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

public class BeanReqConsTranSpeiRec implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4791157012036732559L;
	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador paginador;
	/**
	 * Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de beanTranSpeiRecList
	 */
	private List<BeanTranSpeiRec> listBeanTranSpeiRec = null;
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;

	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveMiInstitucion
	 * @return cveMiInstitucion Objeto de tipo @see String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion Objeto de tipo @see String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad opcion
	 * @return opcion Objeto de tipo @see String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad opcion
	 * @param opcion Objeto de tipo @see String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanTranSpeiRec
	 * @return listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRec> getListBeanTranSpeiRec() {
		return listBeanTranSpeiRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad listBeanTranSpeiRec
	 * @param listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRec(List<BeanTranSpeiRec> listBeanTranSpeiRec) {
		this.listBeanTranSpeiRec = listBeanTranSpeiRec;
	}
	
	
	
}
