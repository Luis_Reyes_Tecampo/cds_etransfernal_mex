/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* PerfilDTO.java
*
* Control de versiones:
*
* Version Date/Hour 	By 		Company 	Description
* ------- --------------- ----------- -------- -----------------------------------------------------------------
* 1.0 	30/06/2011 12:59:47 	MGMS	 STEFANINI 	 Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanPerfil implements Serializable{

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 7044155944382269516L;
	/**Objeto del tipo String que almacena la cve de perfil*/
	private String cvePerfil;
	/**Objeto del tipo String que almacena la cve de modulo*/
	private String cveModulo;
	/**Objeto del tipo String que almacena el perfil sam*/
	private String txtPerfilSam;
	/**Objeto del tipo String que almacena el perfil trfp*/
	private String txtPerfilTrfp;
	
	/**
	 * Metodo get de la propiedad Cve Perfil
	 * @return cvePerfil Cadena con la cve del perfil
	 */
	public String getCvePerfil() {
		return cvePerfil;
	}
	/**
	 * Metodo set de la propiedad Cve Perfil
	 * @param cvePerfil Cadena con la cve del perfil
	 */
	public void setCvePerfil(String cvePerfil) {
		this.cvePerfil = cvePerfil;
	}
	/**
	 * Metodo get de la propiedad Cve modulo
	 * @return cveModulo Cadena con la cve del modulo
	 */
	public String getCveModulo() {
		return cveModulo;
	}
	/**
	 * Metodo set de la propiedad cveModulo
	 * @param cveModulo Cadena con la cveModulo
	 */
	public void setCveModulo(String cveModulo) {
		this.cveModulo = cveModulo;
	}
	/**
	 * Metodo get de la propiedad TxtPerfilSam
	 * @return txtPerfilSam Cadena con el valor de txtPerfilSam
	 */
	public String getTxtPerfilSam() {
		return txtPerfilSam;
	}
	/**
	 * Metodo set de la propiedad txtPerfilSam
	 * @param txtPerfilSam Cadena con el valor de txtPerfilSam
	 */
	public void setTxtPerfilSam(String txtPerfilSam) {
		this.txtPerfilSam = txtPerfilSam;
	}
	/**
	 * Metodo get de la propiedad TxtPerfilSam
	 * @return txtPerfilSam Cadena con el valor de txtPerfilSam
	 */
	public String getTxtPerfilTrfp() {
		return txtPerfilTrfp;
	}
	/**
	 * Metodo set de la propiedad txtPerfilTrfp
	 * @param txtPerfilTrfp Cadena con el valor de txtPerfilTrfp
	 */
	public void setTxtPerfilTrfp(String txtPerfilTrfp) {
		this.txtPerfilTrfp = txtPerfilTrfp;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cvePerfil == null) ? 0 : cvePerfil.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		BeanPerfil other = (BeanPerfil) obj;
		if (cvePerfil == null) {
			if (other.cvePerfil != null){
				return false;
			}
		} else if (!cvePerfil.equals(other.cvePerfil)){
			return false;
		}
		return true;
	}
		
}
