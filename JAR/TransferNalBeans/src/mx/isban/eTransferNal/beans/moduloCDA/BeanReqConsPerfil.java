package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanReqConsPerfil implements Serializable {

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = 4281095329357190939L;
	
	/**
	 * Propiedad del tipo String que almacena el perfil
	 */
	private String perfil;
	
	/**
	 * Propiedad del tipo String que almacena el perfil
	 */
	private String servicio;

	/**
	 * Metodo get que sirve para obtener el perfil
	 * @return perfil Objeto del tipo String con el perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	/**
	 * Metodo set que sirve para modificar el perfil
	 * @param perfil Objeto del tipo String con el perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	/**
	 * Metodo get que sirve para obtener el servicio
	 * @return servicio Objeto del tipo String
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * Metodo set para cambiar el valor del servicio
	 * @param servicio Objeto del tipo String
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	
	
	
}
