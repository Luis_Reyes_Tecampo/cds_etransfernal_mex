package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResConsTranSpeiRec extends BeanResConsTranSpeiRecExt implements BeanResultBO, Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 602526456577457337L;
	
	/**
	 * Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de beanTranSpeiRecList
	 */
	private List<BeanTranSpeiRec> listBeanTranSpeiRec = null;
	/**
	 * Propiedad del tipo List<BeanMotDevolucion> que almacena el valor de beanMotDevolucionList
	 */
	private List<BeanMotDevolucion> beanMotDevolucionList;
	
	/**
	 * Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de listBeanTranSpeiRecOK
	 */
	private List<BeanTranSpeiRec> listBeanTranSpeiRecOK = Collections.emptyList();
	/**
	 * Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de listBeanTranSpeiRecNOK
	 */
	private List<BeanTranSpeiRec> listBeanTranSpeiRecNOK = Collections.emptyList();
	

	/**
	 * Propiedad del tipo String que almacena el valor de strMontoTopoVLiquidadas
	 */
	private String strMontoTopoVLiquidadas;
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de strVolumenTopoVLiquidadas
	 */
	private String strVolumenTopoVLiquidadas;
	

	/**
	 * Propiedad del tipo String que almacena el valor de montoTopoTLiquidadas
	 */
	private String strMontoTopoTLiquidadas;
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de strVolumenTopoTLiquidadas
	 */
	private String strVolumenTopoTLiquidadas;
	
	/**
	 * Propiedad del tipo String que almacena el valor de montoTopoTXLiquidar
	 */
	private String strMontoTopoTXLiquidar;
	
	/**
	 * Propiedad del tipo String que almacena el valor de strVolumenTopoTXLiquidar
	 */
	private String strVolumenTopoTXLiquidar;
	
	/**
	 * Propiedad del tipo String que almacena el valor de strMontoRechazos
	 */
	private String strMontoRechazos;	

	/**
	 * Propiedad del tipo String que almacena el valor de strVolumenRechazos
	 */
	private String strVolumenRechazos;

	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanTranSpeiRec
	 * @return listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRec> getListBeanTranSpeiRec() {
		return listBeanTranSpeiRec;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanTranSpeiRec
	 * @param listBeanTranSpeiRec Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRec(List<BeanTranSpeiRec> listBeanTranSpeiRec) {
		this.listBeanTranSpeiRec = listBeanTranSpeiRec;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalReg
	 * @return totalReg Objeto de tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalReg
	 * @param totalReg Objeto de tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoVLiquidadas
	 * @return strMontoTopoVLiquidadas Objeto de tipo @see String
	 */
	public String getStrMontoTopoVLiquidadas() {
		return strMontoTopoVLiquidadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoVLiquidadas
	 * @param strMontoTopoVLiquidadas Objeto de tipo @see String
	 */
	public void setStrMontoTopoVLiquidadas(String strMontoTopoVLiquidadas) {
		this.strMontoTopoVLiquidadas = strMontoTopoVLiquidadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoVLiquidadas
	 * @return strVolumenTopoVLiquidadas Objeto de tipo @see String
	 */
	public String getStrVolumenTopoVLiquidadas() {
		return strVolumenTopoVLiquidadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoVLiquidadas
	 * @param strVolumenTopoVLiquidadas Objeto de tipo @see String
	 */
	public void setStrVolumenTopoVLiquidadas(String strVolumenTopoVLiquidadas) {
		this.strVolumenTopoVLiquidadas = strVolumenTopoVLiquidadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoTLiquidadas
	 * @return strMontoTopoTLiquidadas Objeto de tipo @see String
	 */
	public String getStrMontoTopoTLiquidadas() {
		return strMontoTopoTLiquidadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoTLiquidadas
	 * @param strMontoTopoTLiquidadas Objeto de tipo @see String
	 */
	public void setStrMontoTopoTLiquidadas(String strMontoTopoTLiquidadas) {
		this.strMontoTopoTLiquidadas = strMontoTopoTLiquidadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoTLiquidadas
	 * @return strVolumenTopoTLiquidadas Objeto de tipo @see String
	 */
	public String getStrVolumenTopoTLiquidadas() {
		return strVolumenTopoTLiquidadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoTLiquidadas
	 * @param strVolumenTopoTLiquidadas Objeto de tipo @see String
	 */
	public void setStrVolumenTopoTLiquidadas(String strVolumenTopoTLiquidadas) {
		this.strVolumenTopoTLiquidadas = strVolumenTopoTLiquidadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoTopoTXLiquidar
	 * @return strMontoTopoTXLiquidar Objeto de tipo @see String
	 */
	public String getStrMontoTopoTXLiquidar() {
		return strMontoTopoTXLiquidar;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strMontoTopoTXLiquidar
	 * @param strMontoTopoTXLiquidar Objeto de tipo @see String
	 */
	public void setStrMontoTopoTXLiquidar(String strMontoTopoTXLiquidar) {
		this.strMontoTopoTXLiquidar = strMontoTopoTXLiquidar;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenTopoTXLiquidar
	 * @return strVolumenTopoTXLiquidar Objeto de tipo @see String
	 */
	public String getStrVolumenTopoTXLiquidar() {
		return strVolumenTopoTXLiquidar;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenTopoTXLiquidar
	 * @param strVolumenTopoTXLiquidar Objeto de tipo @see String
	 */
	public void setStrVolumenTopoTXLiquidar(String strVolumenTopoTXLiquidar) {
		this.strVolumenTopoTXLiquidar = strVolumenTopoTXLiquidar;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strMontoRechazos
	 * @return strMontoRechazos Objeto de tipo @see String
	 */
	public String getStrMontoRechazos() {
		return strMontoRechazos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strMontoRechazos
	 * @param strMontoRechazos Objeto de tipo @see String
	 */
	public void setStrMontoRechazos(String strMontoRechazos) {
		this.strMontoRechazos = strMontoRechazos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad strVolumenRechazos
	 * @return strVolumenRechazos Objeto de tipo @see String
	 */
	public String getStrVolumenRechazos() {
		return strVolumenRechazos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad strVolumenRechazos
	 * @param strVolumenRechazos Objeto de tipo @see String
	 */
	public void setStrVolumenRechazos(String strVolumenRechazos) {
		this.strVolumenRechazos = strVolumenRechazos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad opcion
	 * @return opcion Objeto de tipo @see String
	 */
	public String getOpcion() {
		return opcion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad opcion
	 * @param opcion Objeto de tipo @see String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad beanMotDevolucionList
	 * @return beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public List<BeanMotDevolucion> getBeanMotDevolucionList() {
		return beanMotDevolucionList;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanMotDevolucionList
	 * @param beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public void setBeanMotDevolucionList(
			List<BeanMotDevolucion> beanMotDevolucionList) {
		this.beanMotDevolucionList = beanMotDevolucionList;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad listBeanTranSpeiRecOK
	 * @return listBeanTranSpeiRecOK Objeto del tipo List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRec> getListBeanTranSpeiRecOK() {
		return listBeanTranSpeiRecOK;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanTranSpeiRecOK
	 * @param listBeanTranSpeiRecOK del tipo List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRecOK(List<BeanTranSpeiRec> listBeanTranSpeiRecOK) {
		this.listBeanTranSpeiRecOK = listBeanTranSpeiRecOK;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad listBeanTranSpeiRecNOK
	 * @return listBeanTranSpeiRecNOK Objeto del tipo List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRec> getListBeanTranSpeiRecNOK() {
		return listBeanTranSpeiRecNOK;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanTranSpeiRecNOK
	 * @param listBeanTranSpeiRecNOK del tipo List<BeanTranSpeiRec>
	 */
	public void setListBeanTranSpeiRecNOK(
			List<BeanTranSpeiRec> listBeanTranSpeiRecNOK) {
		this.listBeanTranSpeiRecNOK = listBeanTranSpeiRecNOK;
	}	

}
