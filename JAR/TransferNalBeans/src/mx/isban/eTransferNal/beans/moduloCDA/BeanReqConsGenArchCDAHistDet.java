/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqConsGenArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 15 13:47:04 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la funcionalidad de Generar Archivo CDA Historico Detalle
**/

public class BeanReqConsGenArchCDAHistDet implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo @see Integer que almacena el idPeticion*/
   private String idPeticion;
   
   /**Propiedad que almacena la abstraccion de una pagina*/
   private BeanPaginador paginador;
   /**Propiedad que almacena la lista de registros de la funcionalidad
   de Generacion de archivos historico detallle*/
   private List<BeanGenArchHistDet> listBeanGenArchHistDet;
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad idPeticion
   * @return idPeticion Objeto del tipo String
   */
   public String getIdPeticion(){
      return idPeticion;
   }
   /**
   *Modifica el valor de la propiedad idPeticion
   * @param idPeticion Objeto del tipo String
   */
   public void setIdPeticion(String idPeticion){
      this.idPeticion=idPeticion;
   }

   /**
    *Metodo get que sirve para obtener el valor de la propiedad paginador
    * @return paginador Objeto del tipo BeanPaginador
    */
    public BeanPaginador getPaginador(){
       return paginador;
    }
    /**
    *Modifica el valor de la propiedad paginador
    * @param paginador Objeto del tipo BeanPaginador
    */
    public void setPaginador(BeanPaginador paginador){
       this.paginador=paginador;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad listBeanGenArchHistDet
    * @return listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
    */
    public List<BeanGenArchHistDet> getListBeanGenArchHistDet(){
       return listBeanGenArchHistDet;
    }
    /**
    *Modifica el valor de la propiedad listBeanGenArchHistDet
    * @param listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
    */
    public void setListBeanGenArchHistDet(List<BeanGenArchHistDet> listBeanGenArchHistDet){
       this.listBeanGenArchHistDet=listBeanGenArchHistDet;
    }


}
