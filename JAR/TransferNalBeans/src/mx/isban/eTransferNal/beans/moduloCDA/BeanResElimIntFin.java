/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResElimSelecArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 17:39:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import mx.isban.eTransferNal.beans.catalogos.BeanResTiposIntFinancieros;

/**
 * Clase Bean DTO que se encarga de encapsular la respuesta en la opcion de
 * eliminar en la funcionalidad de Intermediarios.
 */

public class BeanResElimIntFin extends BeanResTiposIntFinancieros {

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/** Propiedad del tipo String que almacena el valor de eliminadosErroneos. */
	private String eliminadosErroneos;
	
	/** Propiedad del tipo String que almacena el valor de eliminadosCorrectos. */
	private String eliminadosCorrectos;

	/**
	 * Metodo get que obtiene el valor de la propiedad eliminadosErroneos.
	 *
	 * @return String Objeto de tipo @see String
	 */
	public String getEliminadosErroneos() {
		return eliminadosErroneos;
	}

	/**
	 * Metodo que modifica el valor de la propiedad eliminadosErroneos.
	 *
	 * @param eliminadosErroneos Objeto de tipo @see String
	 */
	public void setEliminadosErroneos(String eliminadosErroneos) {
		this.eliminadosErroneos = eliminadosErroneos;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad eliminadosCorrectos.
	 *
	 * @return String Objeto de tipo @see String
	 */
	public String getEliminadosCorrectos() {
		return eliminadosCorrectos;
	}

	/**
	 * Metodo que modifica el valor de la propiedad eliminadosCorrectos.
	 *
	 * @param eliminadosCorrectos Objeto de tipo @see String
	 */
	public void setEliminadosCorrectos(String eliminadosCorrectos) {
		this.eliminadosCorrectos = eliminadosCorrectos;
	}
}
