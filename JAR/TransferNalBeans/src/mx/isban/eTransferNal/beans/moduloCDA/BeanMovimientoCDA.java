/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMovimientoCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   12/12/2013 16:16:14 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos de un movimiento CDA
 *
**/
public class BeanMovimientoCDA implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -1446744051589239170L;
	/**Propiedad de tipo String que almacena la referencia*/
	private String referencia;
	/**Propiedad de tipo String que almacena el nombre de la intitucion emisora*/
	private	String nomInstEmisora;
	/**Propiedad de tipo String que almacena la clave de rastreo*/
	private	String cveRastreo;
	/**Propiedad de tipo String que almacena la cuenta del beneficiario*/
	private	String ctaBenef;
	/**Propiedad de tipo String que almacena el monto*/
	private	String monto;
	/**Propiedad de tipo String que almacena la hora de abono*/
	private	String hrAbono;
	/**Propiedad de tipo String que almacena la hora de envio*/
	private	String hrEnvio;
	/**Propiedad de tipo Date que almacena el estatus CDA*/
	private	String estatusCDA;
	/**Propiedad de tipo String que almacena el codigo de error*/
	private	String codError;
	/**Propiedad de tipo String que almacena la fecha del CDA*/
	private	String fchCDA;
	/**Propiedad de tipo String que almacena la fecha del abono*/
	private	String fechaAbono;
	/**Propiedad de tipo String que almacena el tipo pago*/
	private	String tipoPago;
	/**Propiedad de tipo int que almacena el folio de paquete cda*/
	private String folioPaqCda;
	/**Propiedad de tipo int que almacena el folio cda*/
	private String folioCda;
	/**Propiedad de tipo string que almacena la modalidad*/
	private String modalidad;
	/** Propiedad de tipo String que almacena el nombre del emisor*/
	private String nombreOrd;
	/**Propiedad de tipo String que almacena el tipo de cuenta del emisor*/
	private String tipoCuentaOrd;
	/**Propieda de tipo String que almacena el numero de cuenta del emisor*/
	private String numCuentaOrd;
	/**Propiedad de tipo String que alamcena el rfc del emisor*/
	private String rfcOrd;
	/**Propiedad de tipo String que almacena el nombre del banco receptor*/
	private String nombreBcoRec;
	/**Propiedad de tipo String que alamcena el nombre del receptor*/
	private String nombreRec;
	/**Propiedad de tipo String que almacena el tipo de cuenta del receptor*/
	private String tipoCuentaRec;
	/**Propiedad de tipo String que almacena el rfc del receptor*/
	private String rfcRec;
	/**Propiedad de tipo String que almacena el concepto de pago*/
	private String conceptoPago;
	/**Propiedad de tipo int que almacena el IVA*/
	private String iva;
	/**Propiedad de tipo String que almacena el numero de serie certificado*/
	private String numSerieCertif;
	/**Propiedad de tipo String que almacena el nombre del recepto2r*/
	private String nombreRec2;
	/**Propiedad de tipo String que almacena el rfc del receptor2*/
	private String rfcRec2;
	/**Propiedad de tipo String que almacena el tipo de cuenta del receptor2*/
	private String tipoCuentaRec2;
	/**Propiedad de tipo String que almacena el numero de cuenta del receptor2*/
	private String numCuentaRec2;
	/**Propiedad de tipo String que almacena el folio codi*/
	private String folioCodi;
	/**Propiedad de tipo String que almacena el pago de comision*/
	private String pagoComision;
	/**Propiedad de tipo int que almacena el monto de comision*/
	private String montoComision;
	/**Propiedad de tipo String que almacena el numero celular del emisor*/
	private String numCelOrd;
	/**Propiedad de tipo String que almacena el digito de verificacion del emisor*/
	private String digVerifiOrd;
	/**Propiedad de tipo String que almacena el numero celular del receptor*/
	private String numCelRec;
	/**Propiedad de tipo String que almacena el digito de verificacion del receptor*/
	private String digVerifiRec;
	/**Propiedad de tipo String que almacena el Sello digital*/
	private String selloDigital;
	
	
	/**Objeto bean que almacena las propiedades que conforman la llave*/
	private BeanMovimientoCDALlave beanMovimientoCDALlave = new BeanMovimientoCDALlave();

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad referencia
	 * @return referencia Objeto del tipo @see String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Modifica el valor de la propiedad referencia
	 * @param referencia Objeto del tipo @see String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nomInstEmisora
	 * @return nomInstEmisora Objeto del tipo @see String
	 */
	public String getNomInstEmisora() {
		return nomInstEmisora;
	}
	/**
	 * Modifica el valor de la propiedad nomInstEmisora
	 * @param nomInstEmisora Objeto del tipo @see String
	 */
	public void setNomInstEmisora(String nomInstEmisora) {
		this.nomInstEmisora = nomInstEmisora;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveRastreo
	 * @return cveRastreo Objeto del tipo @see String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Modifica el valor de la propiedad cveRastreo
	 * @param cveRastreo Objeto del tipo @see String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad ctaBenef
	 * @return ctaBenef Objeto del tipo @see String
	 */
	public String getCtaBenef() {
		return ctaBenef;
	}
	/**
	 * Modifica el valor de la propiedad ctaBenef
	 * @param ctaBenef Objeto del tipo @see String
	 */
	public void setCtaBenef(String ctaBenef) {
		this.ctaBenef = ctaBenef;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad monto
	 * @return monto Objeto del tipo @see String
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 * Modifica el valor de la propiedad monto
	 * @param monto Objeto del tipo @see String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad hrAbono
	 * @return hrAbono Objeto del tipo @see String
	 */
	public String getHrAbono() {
		return hrAbono;
	}
	/**
	 * Modifica el valor de la propiedad hrAbono
	 * @param hrAbono Objeto del tipo @see String
	 */
	public void setHrAbono(String hrAbono) {
		this.hrAbono = hrAbono;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad hrEnvio
	 * @return hrEnvio Objeto del tipo @see String
	 */
	public String getHrEnvio() {
		return hrEnvio;
	}
	/**
	 * Modifica el valor de la propiedad hrEnvio
	 * @param hrEnvio Objeto del tipo @see String
	 */
	public void setHrEnvio(String hrEnvio) {
		this.hrEnvio = hrEnvio;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad estatusCDA
	 * @return estatusCDA Objeto del tipo @see String
	 */
	public String getEstatusCDA() {
		return estatusCDA;
	}
	/**
	 * Modifica el valor de la propiedad estatusCDA
	 * @param estatusCDA Objeto del tipo @see String
	 */
	public void setEstatusCDA(String estatusCDA) {
		this.estatusCDA = estatusCDA;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad codError
	 * @return codError Objeto del tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Modifica el valor de la propiedad codError
	 * @param codError Objeto del tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fchCDA
	 * @return fchCDA Objeto del tipo @see String
	 */
	public String getFchCDA() {
		return fchCDA;
	}
	/**
	 * Modifica el valor de la propiedad fchCDA
	 * @param fchCDA Objeto del tipo @see String
	 */
	public void setFchCDA(String fchCDA) {
		this.fchCDA = fchCDA;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaAbono
	 * @return fechaAbono Objeto del tipo @see String
	 */
	public String getFechaAbono() {
		return fechaAbono;
	}
	/**
	 * Modifica el valor de la propiedad fechaAbono
	 * @param fechaAbono Objeto del tipo @see String
	 */
	public void setFechaAbono(String fechaAbono) {
		this.fechaAbono = fechaAbono;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad tipoPago
	 * @return tipoPago Objeto del tipo @see String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Modifica el valor de la propiedad tipoPago
	 * @param tipoPago Objeto del tipo @see String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveSpei
	 * @return cveSpei Objeto del tipo @see String
	 */
	public String getCveSpei() {
		return beanMovimientoCDALlave.getCveSpei();
	}
	/**
	 * Modifica el valor de la propiedad cveSpei
	 * @param cveSpei Objeto del tipo @see String
	 */
	public void setCveSpei(String cveSpei) {
		beanMovimientoCDALlave.setCveSpei(cveSpei);
	}
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaOpe
	 * @return fechaOpe Objeto del tipo @see String
	 */
	public String getFechaOpe() {
		return beanMovimientoCDALlave.getFechaOpe();
	}
	/**
	 * Modifica el valor de la propiedad fechaOpe
	 * @param fechaOpe Objeto del tipo @see String
	 */
	public void setFechaOpe(String fechaOpe) {
		beanMovimientoCDALlave.setFechaOpe(fechaOpe);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPaq
	 * @return folioPaq Objeto del tipo @see String
	 */
	public String getFolioPaq() {
		return beanMovimientoCDALlave.getFolioPaq();
	}
	/**
	 * Modifica el valor de la propiedad folioPaq
	 * @param folioPaq Objeto del tipo @see String
	 */
	public void setFolioPaq(String folioPaq) {
		beanMovimientoCDALlave.setFolioPaq(folioPaq);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPago
	 * @return folioPago Objeto del tipo @see String
	 */
	public String getFolioPago() {
		return beanMovimientoCDALlave.getFolioPago();
	}
	/**
	 * Modifica el valor de la propiedad folioPago
	 * @param folioPago Objeto del tipo @see String
	 */
	public void setFolioPago(String folioPago) {
		beanMovimientoCDALlave.setFolioPago(folioPago);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto del tipo @see String
	 */
	public String getCveMiInstituc() {
		return beanMovimientoCDALlave.getCveMiInstituc();
	}
	/**
	 * Modifica el valor de la propiedad cveMiInstituc
	 * @param cveMiInstituc Objeto del tipo @see String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		beanMovimientoCDALlave.setCveMiInstituc(cveMiInstituc);
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad beanMovimientoCDALlave
	 * @return beanMovimientoCDALlave Objeto del tipo @see BeanMovimientoCDALlave
	 */
	public BeanMovimientoCDALlave getBeanMovimientoCDALlave() {
		return beanMovimientoCDALlave;
	}
	/**
	 * Modifica la referencia de la propiedad beanMovimientoCDALlave
	 * @param beanMovimientoCDALlave Objeto del tipo @see BeanMovimientoCDALlave
	 */
	public void setBeanMovimientoCDALlave(
			BeanMovimientoCDALlave beanMovimientoCDALlave) {
		this.beanMovimientoCDALlave = beanMovimientoCDALlave;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPaqCda
	 * @return folioPaqCda Objeto del tipo @see int
	 */
	public String getFolioPaqCda() {
		return folioPaqCda;
	}
	/**
	 * Modifica el valor de la propiedad folioPaqCda
	 * @param folioPaqCda Objeto del tipo @see int
	 */
	public void setFolioPaqCda(String folioPaqCda) {
		this.folioPaqCda = folioPaqCda;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioCda
	 * @return folioCda Objeto del tipo @see int
	 */
	public String getFolioCda() {
		return folioCda;
	}
	/**
	 * Modifica el valor de la propiedad folioCda
	 * @param folioCda Objeto del tipo @see int
	 */
	public void setFolioCda(String folioCda) {
		this.folioCda = folioCda;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad modalidad
	 * @return modalidad Objeto del tipo @see String
	 */
	public String getModalidad() {
		return modalidad;
	}
	/**
	 * Modifica el valor de la propiedad modalidad
	 * @param modalidad Objeto del tipo @see String
	 */
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nombreOrd
	 * @return nombreOrd Objeto del tipo @see String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * Modifica el valor de la propiedad nombreOrd
	 * @param nombreOrd Objeto del tipo @see String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad tipoCuentaOrd
	 * @return tipoCuentaOrd Objeto del tipo @see String
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	/**
	 * Modifica el valor de la propiedad tipoCuentaOrd
	 * @param tipoCuentaOrd Objeto del tipo @see String
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad numCuentaOrd
	 * @return numCuentaOrd Objeto del tipo @see String
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}
	/**
	 * Modifica el valor de la propiedad numCuentaOrd
	 * @param numCuentaOrd Objeto del tipo @see String
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad rfcOrd
	 * @return rfcOrd Objeto del tipo @see String
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}
	/**
	 * Modifica el valor de la propiedad rfcOrd
	 * @param rfcOrd Objeto del tipo @see String
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nombreBcoRec
	 * @return nombreBcoRec Objeto del tipo @see String
	 */
	public String getNombreBcoRec() {
		return nombreBcoRec;
	}
	/**
	 * Modifica el valor de la propiedad nombreBcoRed
	 * @param nombreBcoRec Objeto del tipo @see String
	 */
	public void setNombreBcoRec(String nombreBcoRec) {
		this.nombreBcoRec = nombreBcoRec;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nombreRec
	 * @return nombreRec Objeto del tipo @see String
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	/**
	 * Modifica el valor de la propiedad nombreRec
	 * @param nombreRec Objeto del tipo @see String
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad tipoCuentaRec
	 * @return tipoCuentaRec Objeto del tipo @see String
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}
	/**
	 * Modifica el valor de la propiedad tipoCuentaRec
	 * @param tipoCuentaRec Objeto del tipo @see String
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad rfcRec
	 * @return rfcRec Objeto del tipo @see String
	 */
	public String getRfcRec() {
		return rfcRec;
	}
	/**
	 * Modifica el valor de la propiedad rfcRec
	 * @param rfcRec Objeto del tipo @see String
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad conceptoPago
	 * @return conceptoPago Objeto del tipo @see String
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	/**
	 * Modifica el valor de la propiedad conceptoPago
	 * @param conceptoPago Objeto del tipo @see String
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad iva
	 * @return iva Objeto del tipo @see int
	 */
	public String getIva() {
		return iva;
	}
	/**
	 * Modifica el valor de la propiedad iva
	 * @param iva Objeto del tipo @see int
	 */
	public void setIva(String iva) {
		this.iva = iva;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad numSerieCertif
	 * @return numSerieCertif Objeto del tipo @see String
	 */
	public String getNumSerieCertif() {
		return numSerieCertif;
	}
	/**
	 * Modifica el valor de la propiedad numSerieCertif
	 * @param numserieCertif Objeto del tipo @see String
	 */
	public void setNumSerieCertif(String numSerieCertif) {
		this.numSerieCertif = numSerieCertif;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nombreRec2
	 * @return nombreRec2 Objeto del tipo @see String
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}
	/**
	 * Modifica el valor de la propiedad nombreRec2
	 * @param nombreRec2 Objeto del tipo @see String
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad rfcRec2
	 * @return rfcRec2 Objeto del tipo @see String
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}
	/**
	 * Modifica el valor de la propiedad rfcRec2
	 * @param rfcRec2 Objeto del tipo @see String
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad tipoCuentaRec2
	 * @return tipoCuentaRec2 Objeto del tipo @see String
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}
	/**
	 * Modifica el valor de la propiedad tipoCuentaRec2
	 * @param tipoCuentaRec2 Objeto del tipo @see String
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad numCuentaRec2
	 * @return numCuentaRec2 Objeto del tipo @see String
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}
	/**
	 * Modifica el valor de la propiedad numCuentaRec2
	 * @param numCuentaRec2 Objeto del tipo @see String
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioCodi
	 * @return folioCodi Objeto del tipo @see String
	 */
	public String getFolioCodi() {
		return folioCodi;
	}
	/**
	 * Modifica el valor de la propiedad folioCodi
	 * @param folioCodi Objeto del tipo @see String
	 */
	public void setFolioCodi(String folioCodi) {
		this.folioCodi = folioCodi;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad pagoComision
	 * @return pagoComision Objeto del tipo @see String
	 */
	public String getPagoComision() {
		return pagoComision;
	}
	/**
	 * Modifica el valor de la propiedad pagoComision
	 * @param pagoComision Objeto del tipo @see String
	 */
	public void setPagoComision(String pagoComision) {
		this.pagoComision = pagoComision;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad montoComision
	 * @return montoComision Objeto del tipo @see int
	 */
	public String getMontoComision() {
		return montoComision;
	}
	/**
	 * Modifica el valor de la propiedad montoComision
	 * @param montoComision Objeto del tipo @see int
	 */
	public void setMontoComision(String montoComision) {
		this.montoComision = montoComision;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad numCelOrd
	 * @return numCelOrd Objeto del tipo @see String
	 */
	public String getNumCelOrd() {
		return numCelOrd;
	}
	/**
	 * Modifica el valor de la propiedad numCelOrd
	 * @param numCelOrd Objeto del tipo @see String
	 */
	public void setNumCelOrd(String numCelOrd) {
		this.numCelOrd = numCelOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad digVerifiOrd
	 * @return digVerifiOrd Objeto del tipo @see String
	 */
	public String getDigVerifiOrd() {
		return digVerifiOrd;
	}
	/**
	 * Modifica el valor de la propiedad digVerifiOrd
	 * @param digVerifiOrd Objeto del tipo @see String
	 */
	public void setDigVerifiOrd(String digVerifiOrd) {
		this.digVerifiOrd = digVerifiOrd;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad numCelRec
	 * @return numCelRec Objeto del tipo @see String
	 */
	public String getNumCelRec() {
		return numCelRec;
	}
	/**
	 * Modifica el valor de la propiedad numCelRec
	 * @param numCelRec Objeto del tipo @see String
	 */
	public void setNumCelRec(String numCelRec) {
		this.numCelRec = numCelRec;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad digVerifiRec
	 * @return digVerifiRec Objeto del tipo @see String
	 */
	public String getDigVerifiRec() {
		return digVerifiRec;
	}
	/**
	 * Modifica el valor de la propiedad digVerifiRec
	 * @param digVerifiRec Objeto del tipo @see String
	 */
	public void setDigVerifiRec(String digVerifiRec) {
		this.digVerifiRec = digVerifiRec;
	}
	public String getSelloDigital() {
		return selloDigital;
	}
	public void setSelloDigital(String selloDigital) {
		this.selloDigital = selloDigital;
	}
	

}