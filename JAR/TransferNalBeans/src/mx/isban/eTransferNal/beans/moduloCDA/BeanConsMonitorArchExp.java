/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsMonitorArchExp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 18:19:23 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean que encapsula cada uno de los campos de  los registros
 * del monitor de archivos exportar
**/

public class BeanConsMonitorArchExp  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo String almacena el modulo*/
   private String modulo;
   /**Propiedad del tipo String almacena el subModulo*/
   private String subModulo;
   /**Propiedad del tipo String almacena el nombre del archivo*/
   private String nomArchivo;
   /**Propiedad del tipo String almacena el estatus*/
   private String estatus;
   /**Propiedad del tipo String almacena el numero de registros*/
   private String numRegistros;
   /**Propiedad del tipo String almacena la fecha de carga*/
   private String fechaCarga;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad modulo
   * @return modulo Objeto del tipo String
   */
   public String getModulo(){
      return modulo;
   }
   /**
   *Modifica el valor de la propiedad modulo
   * @param modulo Objeto del tipo String
   */
   public void setModulo(String modulo){
      this.modulo=modulo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad subModulo
   * @return subModulo Objeto del tipo String
   */
   public String getSubModulo(){
      return subModulo;
   }
   /**
   *Modifica el valor de la propiedad subModulo
   * @param subModulo Objeto del tipo String
   */
   public void setSubModulo(String subModulo){
      this.subModulo=subModulo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomArchivo
   * @return nomArchivo Objeto del tipo String
   */
   public String getNomArchivo(){
      return nomArchivo;
   }
   /**
   *Modifica el valor de la propiedad nomArchivo
   * @param nomArchivo Objeto del tipo String
   */
   public void setNomArchivo(String nomArchivo){
      this.nomArchivo=nomArchivo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad estatus
   * @return estatus Objeto del tipo String
   */
   public String getEstatus(){
      return estatus;
   }
   /**
   *Modifica el valor de la propiedad estatus
   * @param estatus Objeto del tipo String
   */
   public void setEstatus(String estatus){
      this.estatus=estatus;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numRegistros
   * @return numRegistros Objeto del tipo String
   */
   public String getNumRegistros(){
      return numRegistros;
   }
   /**
   *Modifica el valor de la propiedad numRegistros
   * @param numRegistros Objeto del tipo String
   */
   public void setNumRegistros(String numRegistros){
      this.numRegistros=numRegistros;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaCarga
   * @return fechaCarga Objeto del tipo String
   */
   public String getFechaCarga(){
      return fechaCarga;
   }
   /**
   *Modifica el valor de la propiedad fechaCarga
   * @param fechaCarga Objeto del tipo String
   */
   public void setFechaCarga(String fechaCarga){
      this.fechaCarga=fechaCarga;
   }



}
