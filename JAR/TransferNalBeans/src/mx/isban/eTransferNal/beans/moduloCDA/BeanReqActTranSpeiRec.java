/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqActTranSpeiRec.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;


/**
 * @author cchong
 *
 */
public class BeanReqActTranSpeiRec implements Serializable{

    /**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 1347292116999256422L;
	/**
	 * Propiedad del tipo String que almacena el valor de fechaOperacion
	 */
	private String fechaOperacion;
    /**
     * Propiedad del tipo String que almacena el valor de cveMiInstituc
     */
    private String cveMiInstituc;
	/**
	 * Propiedad del tipo String que almacena el valor de cveInstOrd
	 */
	private String cveInstOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPaquete
	 */
	private String folioPaquete;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPago
	 */
	private String folioPago;
	
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	/**
	 * Propiedad del tipo String que almacena el valor de motivoDevol
	 */
	private String motivoDevol;
	
	/**
	 * Propiedad del tipo String que almacena el valor de codigoError
	 */
	private String codigoError;
	
	/**
	 * Propiedad del tipo String que almacena el valor de referenciaTransfer
	 */
	private String referenciaTransfer;
	/**
	 * Propiedad del tipo String que almacena el valor de la fchCaptura
	 */
	private String fchCaptura;
	
	/**
	 * Propiedad del tipo String que almacena el valor de refeTransfer
	 */
	private String refeTransfer;
	/**
	 * Metodo get que obtiene el valor de la propiedad fechaOperacion
	 * @return fechaOperacion Objeto de tipo @see String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fechaOperacion
	 * @param fechaOperacion Objeto de tipo @see String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto de tipo @see String
	 */
	public String getCveMiInstituc() {
		return cveMiInstituc;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveMiInstituc
	 * @param cveMiInstituc Objeto de tipo @see String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		this.cveMiInstituc = cveMiInstituc;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveInstOrd
	 * @return cveInstOrd Objeto de tipo @see String
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveInstOrd
	 * @param cveInstOrd Objeto de tipo @see String
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad folioPaquete
	 * @return folioPaquete Objeto de tipo @see String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}

	/**
	 * Metodo que modifica el valor de la propiedad folioPaquete
	 * @param folioPaquete Objeto de tipo @see String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad folioPago
	 * @return folioPago Objeto de tipo @see String
	 */
	public String getFolioPago() {
		return folioPago;
	}

	/**
	 * Metodo que modifica el valor de la propiedad folioPago
	 * @param folioPago Objeto de tipo @see String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad estatus
	 * @return estatus Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Metodo que modifica el valor de la propiedad estatus
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad motivoDevol
	 * @return motivoDevol Objeto de tipo @see String
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}

	/**
	 * Metodo que modifica el valor de la propiedad motivoDevol
	 * @param motivoDevol Objeto de tipo @see String
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad codigoError
	 * @return codigoError Objeto de tipo @see String
	 */
	public String getCodigoError() {
		return codigoError;
	}

	/**
	 * Metodo que modifica el valor de la propiedad codigoError
	 * @param codigoError Objeto de tipo @see String
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad referenciaTransfer
	 * @return referenciaTransfer Objeto de tipo @see String
	 */
	public String getReferenciaTransfer() {
		return referenciaTransfer;
	}

	/**
	 * Metodo que modifica el valor de la propiedad referenciaTransfer
	 * @param referenciaTransfer Objeto de tipo @see String
	 */
	public void setReferenciaTransfer(String referenciaTransfer) {
		this.referenciaTransfer = referenciaTransfer;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad fchCaptura
	 * @return fchCaptura Objeto de tipo @see String
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fchCaptura
	 * @param fchCaptura Objeto de tipo @see String
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad refeTransfer
	 * @return String Objeto de tipo @see String
	 */
	public String getRefeTransfer() {
		return refeTransfer;
	}

	/**
	 * Metodo que modifica el valor de la propiedad refeTransfer
	 * @param refeTransfer Objeto de tipo @see String
	 */
	public void setRefeTransfer(String refeTransfer) {
		this.refeTransfer = refeTransfer;
	}
	
	
	
}
