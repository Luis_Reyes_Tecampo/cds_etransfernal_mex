/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:53:43 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * bitacora administrativa
 **/

public class BeanResConsBitAdmon implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

	/**
	 * Propiedad del tipo List<BeanConsBitAdmon> que almacena el listado de
	 * registros de la bitacora administrativa
	 */
	private List<BeanConsBitAdmon> listBeanConsBitAdmon;
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador paginador = null;
	/** Propiedad del tipo Integer que almacena el toral de registors */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError = "";
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;

    /**
     * Propiedad del tipo String que almacena el nombre del archivo excel
     */
    private String nomArchivo;
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listBeanConsBitAdmon
	 * 
	 * @return listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
	 */
	public List<BeanConsBitAdmon> getListBeanConsBitAdmon() {
		return listBeanConsBitAdmon;
	}

	/**
	 *Modifica el valor de la propiedad listBeanConsBitAdmon
	 * 
	 * @param listBeanConsBitAdmon
	 *            Objeto del tipo List<BeanConsBitAdmon>
	 */
	public void setListBeanConsBitAdmon(
			List<BeanConsBitAdmon> listBeanConsBitAdmon) {
		this.listBeanConsBitAdmon = listBeanConsBitAdmon;
	}

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paginador
	 * 
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 *Modifica el valor de la propiedad paginador
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
   /**
    *Metodo get que sirve para obtener el valor de la propiedad nombre de archivo
    * @return nomArchivo Objeto del tipo String
    */
   public String getNomArchivo() {
	   return nomArchivo;
   }
   /**
    *Metodo set que sirve para modificar el valor de la propiedad nombre de archivo
    * @param nomArchivo Objeto del tipo String
    */
   public void setNomArchivo(String nomArchivo) {
	  this.nomArchivo = nomArchivo;
   }


}
