package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanTranSpeiRecExt2 extends BeanResBase implements Serializable{


	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2137607502481831845L;

	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String refeNumerica;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreoOri;
	
	/**
	 * Propiedad del tipo String que almacena el valor de prioriodad
	 */
	private String prioriodad;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec
	 */
	private String numCuentaRec;
	/**
	 * Propiedad del tipo boolean que almacena el valor de seleccionado
	 */
	private boolean seleccionado;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de seleccionado
	 */
	private boolean selecDevolucion;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de selecRecuperar
	 */
	private boolean selecRecuperar;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoOperacion
	 */
	private String tipoOperacion;

	/**
	 * Propiedad del tipo String que almacena el valor de motivoDevolAnt
	 */
	private String motivoDevolAnt;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaOrd
	 */
	private String numCuentaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de cde
	 */
	private String cde;
	
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private String montoStr;

	
	/**
	 * Metodo get que obtiene el valor de la propiedad cveRastreo
	 * @return cveRastreo Objeto de tipo @see String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveRastreo
	 * @param cveRastreo Objeto de tipo @see String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad refeNumerica
	 * @return refeNumerica Objeto de tipo @see String
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}

	/**
	 * Metodo que modifica el valor de la propiedad refeNumerica
	 * @param refeNumerica Objeto de tipo @see String
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveRastreoOri
	 * @return cveRastreoOri Objeto de tipo @see String
	 */
	public String getCveRastreoOri() {
		return cveRastreoOri;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveRastreoOri
	 * @param cveRastreoOri Objeto de tipo @see String
	 */
	public void setCveRastreoOri(String cveRastreoOri) {
		this.cveRastreoOri = cveRastreoOri;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad prioriodad
	 * @return prioriodad Objeto de tipo @see String
	 */
	public String getPrioriodad() {
		return prioriodad;
	}

	/**
	 * Metodo que modifica el valor de la propiedad prioriodad
	 * @param prioriodad Objeto de tipo @see String
	 */
	public void setPrioriodad(String prioriodad) {
		this.prioriodad = prioriodad;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad numCuentaRec
	 * @return numCuentaRec Objeto de tipo @see String
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numCuentaRec
	 * @param numCuentaRec Objeto de tipo @see String
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad seleccionado
	 * @return seleccionado Objeto de tipo @see boolean
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Metodo que modifica el valor de la propiedad seleccionado
	 * @param seleccionado Objeto de tipo @see boolean
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad tipoOperacion
	 * @return tipoOperacion Objeto de tipo @see String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad tipoOperacion
	 * @param tipoOperacion Objeto de tipo @see String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad selecDevolucion
	 * @return selecDevolucion Objeto de tipo @see boolean
	 */
	public boolean isSelecDevolucion() {
		return selecDevolucion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad selecDevolucion
	 * @param selecDevolucion Objeto de tipo @see boolean
	 */
	public void setSelecDevolucion(boolean selecDevolucion) {
		this.selecDevolucion = selecDevolucion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad selecRecuperar
	 * @return selecRecuperar Objeto de tipo @see boolean
	 */
	public boolean isSelecRecuperar() {
		return selecRecuperar;
	}

	/**
	 * Metodo que modifica el valor de la propiedad selecRecuperar
	 * @param selecRecuperar Objeto de tipo @see boolean
	 */
	public void setSelecRecuperar(boolean selecRecuperar) {
		this.selecRecuperar = selecRecuperar;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad motivoDevolAnt
	 * @return motivoDevolAnt Objeto de tipo @see String
	 */
	public String getMotivoDevolAnt() {
		return motivoDevolAnt;
	}

	/**
	 * Metodo que modifica el valor de la propiedad motivoDevolAnt
	 * @param motivoDevolAnt Objeto de tipo @see String
	 */
	public void setMotivoDevolAnt(String motivoDevolAnt) {
		this.motivoDevolAnt = motivoDevolAnt;
	}	
	
	/**
	 * Metodo get que obtiene el valor de la propiedad numCuentaOrd
	 * @return numCuentaOrd Objeto de tipo @see String
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numCuentaOrd
	 * @param numCuentaOrd Objeto de tipo @see String
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad cde
	 * @return cde Objeto de tipo @see String
	 */
	public String getCde() {
		return cde;
	}
	/**
	 * Metodo que modifica el valor de la propiedad cde
	 * @param cde Objeto de tipo @see String
	 */
	public void setCde(String cde) {
		this.cde = cde;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoStr
	 * @return montoStr Objeto de tipo @see String
	 */
	public String getMontoStr() {
		return montoStr;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoStr
	 * @param montoStr Objeto de tipo @see String
	 */
	public void setMontoStr(String montoStr) {
		this.montoStr = montoStr;
	}
}
