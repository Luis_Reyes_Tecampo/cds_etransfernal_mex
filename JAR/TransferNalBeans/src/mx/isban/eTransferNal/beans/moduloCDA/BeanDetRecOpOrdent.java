/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  30/09/2015 16:46 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos del ordenante.
 *
 * @author FSW-Vector
 * @since 11/02/2019
 */
public class BeanDetRecOpOrdent implements Serializable{

	/**  serialVersionUID Propiedad del tipo long con el valor de serialVersionUID. */
	private static final long serialVersionUID = 5338010741824794732L;
	
	/** La variable que contiene informacion con respecto a: nombre ord. */
	@Size(max=40)
	private String nombreOrd;
	
	/** La variable que contiene informacion con respecto a: rfc ord. */
	@Size(max=18)
	private String rfcOrd;
		
	/** La variable que contiene informacion con respecto a: banco ordenante. */
	@Size(max=12)
	private String bancoOrdenante;
	
	/** La variable que contiene informacion con respecto a: folio paquete ord. */
	@Size(max=12)
	private String folioPaqueteOrd;
	
	/** La variable que contiene informacion con respecto a: monto. */
	@Size(max=19)
	private String monto;
	
	/** La variable que contiene informacion con respecto a: iva. */
	@Size(max=8)
	private String iva;
	
	/** La variable que contiene informacion con respecto a: num cuenta ord. */
	@Size(max=20)
	private String numCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta ord. */
	@Size(max=2)
	private String tipoCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: clave intermedi ord. */
	@Size(max=5)
	private String cveIntermeOrd;

	/** La variable que contiene informacion con respecto a: importe cargo. */
	@Size(max=20)
	private String importeCargo;

	/**
	 * Obtener el objeto: importe cargo.
	 *
	 * @return El objeto: importe cargo
	 */
	public String getImporteCargo() {
		return importeCargo;
	}

	/**
	 * Definir el objeto: importe cargo.
	 *
	 * @param importeCargo El nuevo objeto: importe cargo
	 */
	public void setImporteCargo(String importeCargo) {
		this.importeCargo = importeCargo;
	}

	/**
	 * Obtener el objeto: nombre ord.
	 *
	 * @return El objeto: nombre ord
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}

	/**
	 * Definir el objeto: nombre ord.
	 *
	 * @param nombreOrd El nuevo objeto: nombre ord
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Obtener el objeto: rfc ord.
	 *
	 * @return El objeto: rfc ord
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}

	/**
	 * Definir el objeto: rfc ord.
	 *
	 * @param rfcOrd El nuevo objeto: rfc ord
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}
	
	/**
	 * Obtener el objeto: banco ordenante.
	 *
	 * @return El objeto: banco ordenante
	 */
	public String getBancoOrdenante() {
		return bancoOrdenante;
	}

	/**
	 * Definir el objeto: banco ordenante.
	 *
	 * @param bancoOrdenante El nuevo objeto: banco ordenante
	 */
	public void setBancoOrdenante(String bancoOrdenante) {
		this.bancoOrdenante = bancoOrdenante;
	}

	/**
	 * Obtener el objeto: folio paquete ord.
	 *
	 * @return El objeto: folio paquete ord
	 */
	public String getFolioPaqueteOrd() {
		return folioPaqueteOrd;
	}

	/**
	 * Definir el objeto: folio paquete ord.
	 *
	 * @param folioPaqueteOrd El nuevo objeto: folio paquete ord
	 */
	public void setFolioPaqueteOrd(String folioPaqueteOrd) {
		this.folioPaqueteOrd = folioPaqueteOrd;
	}

	/**
	 * Obtener el objeto: monto.
	 *
	 * @return El objeto: monto
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * Definir el objeto: monto.
	 *
	 * @param monto El nuevo objeto: monto
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}

	/**
	 * Obtener el objeto: iva.
	 *
	 * @return El objeto: iva
	 */
	public String getIva() {
		return iva;
	}

	/**
	 * Definir el objeto: iva.
	 *
	 * @param iva El nuevo objeto: iva
	 */
	public void setIva(String iva) {
		this.iva = iva;
	}

	/**
	 * Obtener el objeto: num cuenta ord.
	 *
	 * @return El objeto: num cuenta ord
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}

	/**
	 * Definir el objeto: num cuenta ord.
	 *
	 * @param numCuentaOrd El nuevo objeto: num cuenta ord
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}

	/**
	 * Obtener el objeto: tipo cuenta ord.
	 *
	 * @return El objeto: tipo cuenta ord
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	
	
	/**
	 * Definir el objeto: tipo cuenta ord.
	 *
	 * @param tipoCuentaOrd El nuevo objeto: tipo cuenta ord
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	
	/**
	 * Obtener el objeto: cve interme ord.
	 *
	 * @return El objeto: cve interme ord
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}

	/**
	 * Definir el objeto: cve interme ord.
	 *
	 * @param cveIntermeOrd El nuevo objeto: cve interme ord
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	
	
	
}
