/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  30/09/2015 16:46 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;



/**
 * Clase la cual se encapsulan los datos que  forman el detalle
 * de recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 29/09/2018
 */
public class BeanDetRecepOperaci extends BeanResBase implements Serializable{
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5722909917517188218L;
	
	/** La variable que contiene informacion con respecto a: cde. */
	@Size(max=50)
	private String cde;
	
	/** La variable que contiene informacion con respecto a: cda. */
	@Size(max=1)
	private String cda;
	
	/** La variable que contiene informacion con respecto a: topologia. */
	@Size(max=1)
	private String topologia;
	
	/** La variable que contiene informacion con respecto a: prioridad. */
	@Size(max=7)
	private String prioridad;
	
	/** La variable que contiene informacion con respecto a: descripcion prioridad. */
	@Size(max=40)
	private String descripcionPrioridad;
	
	/** La variable que contiene informacion con respecto a: tipo pago. */
	@Size(max=7)
	private String tipoPago;
	
	/** La variable que contiene informacion con respecto a: tipo operacion. */
	@Size(max=2)
	private String tipoOperacion;
	
	/** La variable que contiene informacion con respecto a: desc tipo pago. */
	@Size(max=40)
	private String descTipoPago;
	
	/** La variable que contiene informacion con respecto a: desc tipo operacion. */
	@Size(max=40)
	private String descTipoOperacion;
	
	/** La variable que contiene informacion con respecto a: cuenta modif. */
	@Size(max=40)
	private String cuentaModif;

	/**
	 * Obtener el objeto: cde.
	 *
	 * @return El objeto: cde
	 */
	public String getCde() {
		return cde;
	}

	/**
	 * Definir el objeto: cde.
	 *
	 * @param cde El nuevo objeto: cde
	 */
	public void setCde(String cde) {
		this.cde = cde;
	}

	/**
	 * Obtener el objeto: cda.
	 *
	 * @return El objeto: cda
	 */
	public String getCda() {
		return cda;
	}

	/**
	 * Definir el objeto: cda.
	 *
	 * @param cda El nuevo objeto: cda
	 */
	public void setCda(String cda) {
		this.cda = cda;
	}

	/**
	 * Obtener el objeto: topologia.
	 *
	 * @return El objeto: topologia
	 */
	public String getTopologia() {
		return topologia;
	}

	/**
	 * Definir el objeto: topologia.
	 *
	 * @param topologia El nuevo objeto: topologia
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}

	/**
	 * Obtener el objeto: prioridad.
	 *
	 * @return El objeto: prioridad
	 */
	public String getPrioridad() {
		return prioridad;
	}

	/**
	 * Definir el objeto: prioridad.
	 *
	 * @param prioridad El nuevo objeto: prioridad
	 */
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	/**
	 * Obtener el objeto: descripcion prioridad.
	 *
	 * @return El objeto: descripcion prioridad
	 */
	public String getDescripcionPrioridad() {
		return descripcionPrioridad;
	}

	/**
	 * Definir el objeto: descripcion prioridad.
	 *
	 * @param descripcionPrioridad El nuevo objeto: descripcion prioridad
	 */
	public void setDescripcionPrioridad(String descripcionPrioridad) {
		this.descripcionPrioridad = descripcionPrioridad;
	}

	/**
	 * Obtener el objeto: tipo pago.
	 *
	 * @return El objeto: tipo pago
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * Definir el objeto: tipo pago.
	 *
	 * @param tipoPago El nuevo objeto: tipo pago
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	/**
	 * Obtener el objeto: tipo operacion.
	 *
	 * @return El objeto: tipo operacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * Definir el objeto: tipo operacion.
	 *
	 * @param tipoOperacion El nuevo objeto: tipo operacion
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * Obtener el objeto: desc tipo pago.
	 *
	 * @return El objeto: desc tipo pago
	 */
	public String getDescTipoPago() {
		return descTipoPago;
	}

	/**
	 * Definir el objeto: desc tipo pago.
	 *
	 * @param descTipoPago El nuevo objeto: desc tipo pago
	 */
	public void setDescTipoPago(String descTipoPago) {
		this.descTipoPago = descTipoPago;
	}

	/**
	 * Obtener el objeto: desc tipo operacion.
	 *
	 * @return El objeto: desc tipo operacion
	 */
	public String getDescTipoOperacion() {
		return descTipoOperacion;
	}

	/**
	 * Definir el objeto: desc tipo operacion.
	 *
	 * @param descTipoOperacion El nuevo objeto: desc tipo operacion
	 */
	public void setDescTipoOperacion(String descTipoOperacion) {
		this.descTipoOperacion = descTipoOperacion;
	}

	/**
	 * Obtener el objeto: cuenta modif.
	 *
	 * @return El objeto: cuenta modif
	 */
	public String getCuentaModif() {
		return cuentaModif;
	}

	/**
	 * Definir el objeto: cuenta modif.
	 *
	 * @param cuentaModif El nuevo objeto: cuenta modif
	 */
	public void setCuentaModif(String cuentaModif) {
		this.cuentaModif = cuentaModif;
	}
	
	

}
