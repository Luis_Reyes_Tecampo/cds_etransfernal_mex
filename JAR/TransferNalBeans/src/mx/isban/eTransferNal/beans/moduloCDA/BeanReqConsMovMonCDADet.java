package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanReqConsMovMonCDADet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1118584901705851857L;
	/**
	 * Propiedad del tipo String que almacena el numero de referencia
	 */
	private String referencia;
	/**
	 * Propiedad del tipo String que almacena la fecha de operacion
	 */
	private String fechaOpeLink;
	/**
	 * Propiedad del tipo String que almacena la cve de mi institucion
	 */
	private String cveMiInstitucLink;
	/**
	 * Propiedad del tipo String que almacena la cve spei ordenante
	 */
	private String cveSpeiLink;
	/**
	 * Propiedad del tipo String que almacena el folio del paquete
	 */
	private String folioPaqLink;
	/**
	 * Propiedad del tipo String que almacena el folio del pago
	 */
	private String folioPagoLink;
	/**
	 * Propiedad del tipo String que almacena la fecha del cda
	 */
	private String fchCDALink;
	
	/**
	 * Propiedad del tipo String que almacena el estatus del CDA
	 */
	private String estatusCDALink;
	
	/**
	 * Metodo get que sirve para obtener la referencia
	 * @return referencia Objeto string que almacena la referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Metodo set que sirve para modificar la referencia
	 * @param referencia Objeto string que almacena la referencia
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * Metodo get que sirve para obtener la fecha de operacion
	 * @return fechaOpeLink Objeto string que almacena la fecha de operacion
	 */
	public String getFechaOpeLink() {
		return fechaOpeLink;
	}
	/**
	 * Metodo set que sirve para modificar la fecha de operacion
	 * @param fechaOpeLink Objeto string que almacena la fecha de operacion
	 */
	public void setFechaOpeLink(String fechaOpeLink) {
		this.fechaOpeLink = fechaOpeLink;
	}
	/**
	 * Metodo get que sirve para modificar la cve de la institucion
	 * @return cveMiInstitucLink Objeto string que almacena la cve de la institucion
	 */
	public String getCveMiInstitucLink() {
		return cveMiInstitucLink;
	}
	/**
	 * Metodo set que sirve para modificar la cve de la institucion
	 * @param cveMiInstitucLink Objeto string que almacena la cve de la institucion
	 */

	public void setCveMiInstitucLink(String cveMiInstitucLink) {
		this.cveMiInstitucLink = cveMiInstitucLink;
	}
	/**
	 * Metodo get que sirve para modificar la cve spei
	 * @return cveSpeiLink Objeto string que almacena la cve spei
	 */
	public String getCveSpeiLink() {
		return cveSpeiLink;
	}
	/**
	 * Metodo set que sirve para modificar la cve spei
	 * @param cveSpeiLink Objeto string que almacena la cve spei
	 */
	public void setCveSpeiLink(String cveSpeiLink) {
		this.cveSpeiLink = cveSpeiLink;
	}
	/**
	 * Metodo get que sirve para modificar el folio paq
	 * @return folioPaqLink Objeto string que almacena el folio del paquete
	 */
	public String getFolioPaqLink() {
		return folioPaqLink;
	}
	/**
	 * Metodo set que sirve para modificar el folio paq
	 * @param folioPaqLink Objeto string que almacena el folio del paquete
	 */
	public void setFolioPaqLink(String folioPaqLink) {
		this.folioPaqLink = folioPaqLink;
	}
	/**
	 * Metodo get que sirve para obtener el folio del pago
	 * @return folioPagoLink Objeto string que almacena el folio del pago
	 */
	public String getFolioPagoLink() {
		return folioPagoLink;
	}
	/**
	 * Metodo set que sirve para modificar el folio pago
	 * @param folioPagoLink Objeto string que almacena el folio del pago
	 */
	public void setFolioPagoLink(String folioPagoLink) {
		this.folioPagoLink = folioPagoLink;
	}
	/**
	 * Metodo get que sirve para obtener la fecha cda
	 * @return fchCDALink Objeto string que almacena la fecha cda
	 */
	public String getFchCDALink() {
		return fchCDALink;
	}
	/**
	 * Metodo set que sirve para modificar la fecha cda
	 * @param fchCDALink Objeto string que almacena la fecha cda
	 */
	public void setFchCDALink(String fchCDALink) {
		this.fchCDALink = fchCDALink;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatusCDALink
	 * @return estatusCDALink Objeto de tipo @see String
	 */
	public String getEstatusCDALink() {
		return estatusCDALink;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatusCDALink
	 * @param estatusCDALink Objeto de tipo @see String
	 */
	public void setEstatusCDALink(String estatusCDALink) {
		this.estatusCDALink = estatusCDALink;
	}
	
	
}
