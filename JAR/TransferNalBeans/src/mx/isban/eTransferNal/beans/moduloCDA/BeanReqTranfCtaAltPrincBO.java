package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanReqTranfCtaAltPrincBO implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -7021368790362869527L;

	/**
	 * Propiedad del tipo String que almacena el valor de importe
	 */
	private String importe;
	
	/**
	 * Propiedad del tipo String que almacena el valor de concepto
	 */
	private String concepto;
	
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;

	/**
	 * Metodo get que obtiene el valor de la propiedad importe
	 * @return importe Objeto de tipo @see String
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * Metodo que modifica el valor de la propiedad importe
	 * @param importe Objeto de tipo @see String
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad concepto
	 * @return concepto Objeto de tipo @see String
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * Metodo que modifica el valor de la propiedad concepto
	 * @param concepto Objeto de tipo @see String
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad opcion
	 * @return opcion Objeto de tipo @see String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad opcion
	 * @param opcion Objeto de tipo @see String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	

}
