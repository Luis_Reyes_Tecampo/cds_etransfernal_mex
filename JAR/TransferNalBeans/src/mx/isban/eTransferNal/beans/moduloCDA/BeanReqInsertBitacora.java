/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqInsertBitacora.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:10:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Date;

/**
 *Clase Bean DTO que se encarga de encapsular un registro de la
 * funcionalidad de guardar bitacora 
**/

public class BeanReqInsertBitacora implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo String almacena el numero de registros*/
   private String numeroRegistros;
   /**Propiedad del tipo String almacena la descripcion de la operacion*/
   private String descOperacion;
   /**Propiedad del tipo String que almacena el codOperacion*/
   private String codOperacion;
   /**Propiedad del tipo Date que almacena la fecha y hora*/
   private Date fechaHora;
   /**Propiedad del tipo String que almacena el estatus de la operacion*/
   private String estatusOperacion;
   /**Propiedad del tipo String almacena el datoFijo (el nombre del
   campo llave)*/
   private String datoFijo;
   /**Propiedad del tipo String que indica el servicio*/
   private String servicio;
   /**Propiedad del tipo String almacena el nombre del archivo*/
   private String nomArchivo;
   /**Propiedad del tipo String que almacena el dato modificado*/
   private String datoModificado;
   /**Propiedad del tipo String que almacena la tabla afectada*/
   private String tablaAfectada;
   /**Propiedad del tipo String que almacena el valor nuevo*/
   private String valNuevo;
   /**Propiedad del tipo String que almacena el valor anterior*/
   private String valAnt;
   /**Propiedad del tipo String almacena el codigo de error*/
   private String codError;
   /**Propiedad del tipo String almacena la descripcion del error*/
   private String descCodError;

  
   /**
   *Metodo get que sirve para obtener el valor de la propiedad descOperacion
   * @return descOperacion Objeto del tipo String
   */
   public String getDescOperacion(){
      return descOperacion;
   }
   /**
   *Modifica el valor de la propiedad descOperacion
   * @param descOperacion Objeto del tipo String
   */
   public void setDescOperacion(String descOperacion){
      this.descOperacion=descOperacion;
   }

   /**
   *Metodo get que sirve para obtener el valor de la propiedad datoFijo
   * @return datoFijo Objeto del tipo String
   */
   public String getDatoFijo(){
      return datoFijo;
   }
   /**
   *Modifica el valor de la propiedad datoFijo
   * @param datoFijo Objeto del tipo String
   */
   public void setDatoFijo(String datoFijo){
      this.datoFijo=datoFijo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad servicio
   * @return servicio Objeto del tipo String
   */
   public String getServicio(){
      return servicio;
   }
   /**
   *Modifica el valor de la propiedad servicio
   * @param servicio Objeto del tipo String
   */
   public void setServicio(String servicio){
      this.servicio=servicio;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomArchivo
   * @return nomArchivo Objeto del tipo String
   */
   public String getNomArchivo(){
      return nomArchivo;
   }
   /**
   *Modifica el valor de la propiedad nomArchivo
   * @param nomArchivo Objeto del tipo String
   */
   public void setNomArchivo(String nomArchivo){
      this.nomArchivo=nomArchivo;
   }

   /**
   *Metodo get que sirve para obtener el valor de la propiedad datoModificado
   * @return datoModificado Objeto del tipo String
   */
   public String getDatoModificado(){
      return datoModificado;
   }
   /**
   *Modifica el valor de la propiedad datoModificado
   * @param datoModificado Objeto del tipo String
   */
   public void setDatoModificado(String datoModificado){
      this.datoModificado=datoModificado;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad tablaAfectada
   * @return tablaAfectada Objeto del tipo String
   */
   public String getTablaAfectada(){
      return tablaAfectada;
   }
   /**
   *Modifica el valor de la propiedad tablaAfectada
   * @param tablaAfectada Objeto del tipo String
   */
   public void setTablaAfectada(String tablaAfectada){
      this.tablaAfectada=tablaAfectada;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad descCodError
   * @return descCodError Objeto del tipo String
   */
   public String getDescCodError(){
      return descCodError;
   }
   /**
   *Modifica el valor de la propiedad descCodError
   * @param descCodError Objeto del tipo String
   */
   public void setDescCodError(String descCodError){
      this.descCodError=descCodError;
   }
   
   /**
    *Metodo get que sirve para obtener el valor del codigo de operacion
    * @return codOperacion Objeto del tipo String
    */
	public String getCodOperacion() {
		return codOperacion;
	}
   /**
    *Metodo set que sirve para poner el valor del codigo de operacion
    * @param codOperacion Objeto del tipo String
    */
	public void setCodOperacion(String codOperacion) {
		this.codOperacion = codOperacion;
	}
   /**
    *Metodo get que sirve para obtener el valor de la fecha hora
    * @return fechaHora Objeto del tipo Date
    */	
	public Date getFechaHora() {
		return fechaHora;
	}
   /**
    *Metodo set que sirve para poner el valor de la fecha hora
    * @param fechaHora Objeto del tipo Date
    */	
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
   /**
    *Metodo get que sirve para obtener el valor del estatus de la operacion
    * @return estatusOperacion Objeto del tipo String
    */	
	public String getEstatusOperacion() {
		return estatusOperacion;
	}
   /**
    *Metodo set que sirve para poner el valor del estatus de la operacion
    *@param estatusOperacion Objeto del tipo String
    */	
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numeroRegistros
   * @return numeroRegistros Objeto del tipo String
   */
   public String getNumeroRegistros(){
      return numeroRegistros;
   }
   /**
   *Modifica el valor de la propiedad numeroRegistros
   * @param numeroRegistros Objeto del tipo String
   */
   public void setNumeroRegistros(String numeroRegistros){
      this.numeroRegistros=numeroRegistros;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad valNuevo
    * @return valNuevo Objeto del tipo String
    */
	public String getValNuevo() {
		return valNuevo;
	}
   /**
   *Metodo set que sirve para modificar el valor de la propiedad valNuevo
   * @param valNuevo Objeto del tipo String
   */
	public void setValNuevo(String valNuevo) {
		this.valNuevo = valNuevo;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad valAnt
	 * @return valAnt Objeto de tipo @see String
	 */
	public String getValAnt() {
		return valAnt;
	}
	/**
	 * Metodo que modifica el valor de la propiedad valAnt
	 * @param valAnt Objeto de tipo @see String
	 */
	public void setValAnt(String valAnt) {
		this.valAnt = valAnt;
	}





}
