package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;


public class BeanServiciosTareas implements Serializable{
	/**
	 * Constate del Serial version
	 */
	private static final long serialVersionUID = -3836896087640095896L;
	/**
	 * Propiedad del tipo String que almacena el perfil
	 */
	private String perfil;
	/**
	 * Propiedad del tipo String que almacena el modulo
	 */
	private String modulo;
	/**
	 * Propiedad del tipo String que almacena el servicio
	 */
	private String servicio;
	/**
	 * Propiedad del tipo String que almacena la tarea
	 */
	private String tarea;
	/**
	 * Metodo get que sirve para obtener el perfil
	 * @return perfil Objeto del tipo String que almacena el perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	/**
	 * Metodo set que sirve para almacenar el perfil
	 * @param perfil Objeto del tipo String que almacena el perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	/**
	 * Metodo get que sirve para obtener el modulo
	 * @return modulo Objeto del tipo String que almacena el modulo
	 */
	public String getModulo() {
		return modulo;
	}
	/**
	 * Metodo set que sirve para almacenar el modulo
	 * @param modulo Objeto del tipo String que almacena el modulo
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	/**
	 * Metodo get que sirve para obtener el servicio
	 * @return modulo Objeto del tipo String que almacena el servicio
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * Metodo set que sirve para almacenar el servicio
	 * @param servicio Objeto del tipo String que almacena el servicio
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	/**
	 * Metodo get que sirve para obtener la tarea
	 * @return modulo Objeto del tipo String que almacena la tarea
	 */
	public String getTarea() {
		return tarea;
	}
	/**
	 * Metodo set que sirve para almacenar la tarea
	 * @param tarea Objeto del tipo String que almacena la tarea
	 */
	public void setTarea(String tarea) {
		this.tarea = tarea;
	}
	
	
	

}

