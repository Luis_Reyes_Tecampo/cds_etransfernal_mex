/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqConsMovCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *   2.0   30/12/2016                   Adrian Ricardo Martinez       VECTOR SF     Se agrega modulo CDA SPID
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Consulta de Movimientos CDA
**/

public class BeanReqConsMovCDA implements Serializable {

   /**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 4292159788948203164L;
   
	 /**Propiedad del tipo @see BeanReqConsMovCdaFiltrosParteUno que almacena los filtros de busqueda*/
	private BeanReqConsMovCdaFiltrosParteUno beanReqConsMovCdaFiltrosParteUno;
	/**Propiedad del tipo @see BeanReqConsMovCdaFiltrosParteDos que almacena los filtros de busqueda*/
	private BeanReqConsMovCdaFiltrosParteDos beanReqConsMovCdaFiltrosParteDos;
   /**Propiedad del tipo @see String que almacena la referencia para
   ver el detalle de la referencia*/
   private String referencia;
   /**Propiedad del tipo @see String que almacena la fecha de operacion*/
   private String fechaOperacion;
   /**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
   de la paginacion*/
   private BeanPaginador paginador;
   /**Propiedad privada del tipo Integer que almacena el total de registros*/
   private Integer totalReg;
   
   /** Indicador si la consulta es SPID */
   private boolean esSPID;
   
   /**
   * Metodo get que sirve para obtener el valor de la propiedad referencia
   * @return referencia Objeto del tipo String
   */
   public String getReferencia(){
      return referencia;
   }
   /**
   * Modifica el valor de la propiedad referencia
   * @param referencia Objeto del tipo String
   */
   public void setReferencia(String referencia){
      this.referencia=referencia;
   }
	/**
	 * Modifica el valor de la propiedad fechaOperacion
	 * @param fechaOperacion Objeto del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
   *Metodo get que sirve para obtener el valor de la propiedad paginador
   * @return paginador Objeto del tipo BeanPaginador
   */
   public BeanPaginador getPaginador(){
      return paginador;
   }
   /**
   *Modifica el valor de la propiedad paginador
   * @param paginador Objeto del tipo BeanPaginador
   */
   public void setPaginador(BeanPaginador paginador){
      this.paginador=paginador;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad totalReg
    * @return totalReg Objeto del tipo Integer
    */
    public Integer getTotalReg(){
       return totalReg;
    }
    /**
    * Modifica el valor de la propiedad totalReg
    * @param totalReg Objeto del tipo Integer
    */
    public void setTotalReg(Integer totalReg){
       this.totalReg=totalReg;
    }
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad
	 * @return  beanReqConsMovCdaFiltrosParteUno objeto del tipo @see beanReqConsMovCdaFiltrosParteUno 
	 */
	public BeanReqConsMovCdaFiltrosParteUno getBeanReqConsMovCdaFiltrosParteUno() {
		return beanReqConsMovCdaFiltrosParteUno;
	}
	/**
	 * Modifica el valor de la propiedad beanReqConsMovCdaFiltrosParteUno
	 * @param beanReqConsMovCdaFiltrosParteUno objeto del tipo @see beanReqConsMovCdaFiltrosParteUno 
	 */
	public void setBeanReqConsMovCdaFiltrosParteUno(
			BeanReqConsMovCdaFiltrosParteUno beanReqConsMovCdaFiltrosParteUno) {
		this.beanReqConsMovCdaFiltrosParteUno = beanReqConsMovCdaFiltrosParteUno;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad
	 * @return  beanReqConsMovCdaFiltrosParteDos objeto del tipo @see BeanReqConsMovCdaFiltrosParteDos
	 */
	public BeanReqConsMovCdaFiltrosParteDos getBeanReqConsMovCdaFiltrosParteDos() {
		return beanReqConsMovCdaFiltrosParteDos;
	}
	/**
	 * Modifica el valor de la propiedad beanReqConsMovCdaFiltrosParteDos
	 * @param beanReqConsMovCdaFiltrosParteDos objeto del tipo @see BeanReqConsMovCdaFiltrosParteDos
	 */
	public void setBeanReqConsMovCdaFiltrosParteDos(
			BeanReqConsMovCdaFiltrosParteDos beanReqConsMovCdaFiltrosParteDos) {
		this.beanReqConsMovCdaFiltrosParteDos = beanReqConsMovCdaFiltrosParteDos;
	}

	/**
	 * @return Indica si la consulta es SPID
	 */
	public boolean getEsSPID() {
		return esSPID;
	}

	/**
	 * @param esSPID El valor a asignar al atributo esSPID
	 */
	public void setEsSPID(boolean esSPID) {
		this.esSPID = esSPID;
	}
	
	
}
