package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanTransEnvioExt extends BeanTransEnvioExt2 implements Serializable{
   	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3989579016244209728L;

	/**
	 * Propiedad del tipo String que almacena el valor de nombreOrd
	 */
	private String nombreOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveIntermeRec
	 */
	private String cveIntermeRec = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cuentaRec
	 */
	private String cuentaRec = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de plazaBanxico
	 */
	private String plazaBanxico;
	
	/**
	 * Propiedad del tipo String que almacena el valor de ptoVtaRec
	 */
	private String ptoVtaRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de divisaRec
	 */
	private String divisaRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec
	 */
	private String nombreRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de importeCargo
	 */
	private String importeCargo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de importeAbono
	 */
	private String importeAbono;
	
	/**
	 * Propiedad del tipo String que almacena el valor de importeDolares
	 */
	private String importeDolares;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreBcoRec
	 */
	private String nombreBcoRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de sucBcoRe
	 */
	private String sucBcoRe;
	
	/**
	 * Propiedad del tipo String que almacena el valor de paisBcoRec
	 */
	private String paisBcoRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de ciudadBcoRe
	 */
	private String ciudadBcoRe;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveABA
	 */
	private String cveABA;
	




	/**
	 * Metodo get que obtiene el valor de la propiedad nombreOrd
	 * @return nombreOrd Objeto de tipo @see String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombreOrd
	 * @param nombreOrd Objeto de tipo @see String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveIntermeRec
	 * @return cveIntermeRec Objeto de tipo @see String
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveIntermeRec
	 * @param cveIntermeRec Objeto de tipo @see String
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cuentaRec
	 * @return cuentaRec Objeto de tipo @see String
	 */
	public String getCuentaRec() {
		return cuentaRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cuentaRec
	 * @param cuentaRec Objeto de tipo @see String
	 */
	public void setCuentaRec(String cuentaRec) {
		this.cuentaRec = cuentaRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad plazaBanxico
	 * @return plazaBanxico Objeto de tipo @see String
	 */
	public String getPlazaBanxico() {
		return plazaBanxico;
	}

	/**
	 * Metodo que modifica el valor de la propiedad plazaBanxico
	 * @param plazaBanxico Objeto de tipo @see String
	 */
	public void setPlazaBanxico(String plazaBanxico) {
		this.plazaBanxico = plazaBanxico;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad ptoVtaRec
	 * @return ptoVtaRec Objeto de tipo @see String
	 */
	public String getPtoVtaRec() {
		return ptoVtaRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad ptoVtaRec
	 * @param ptoVtaRec Objeto de tipo @see String
	 */
	public void setPtoVtaRec(String ptoVtaRec) {
		this.ptoVtaRec = ptoVtaRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad divisaRec
	 * @return divisaRec Objeto de tipo @see String
	 */
	public String getDivisaRec() {
		return divisaRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad divisaRec
	 * @param divisaRec Objeto de tipo @see String
	 */
	public void setDivisaRec(String divisaRec) {
		this.divisaRec = divisaRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombreRec
	 * @return nombreRec Objeto de tipo @see String
	 */
	public String getNombreRec() {
		return nombreRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombreRec
	 * @param nombreRec Objeto de tipo @see String
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad importeCargo
	 * @return importeCargo Objeto de tipo @see String
	 */
	public String getImporteCargo() {
		return importeCargo;
	}

	/**
	 * Metodo que modifica el valor de la propiedad importeCargo
	 * @param importeCargo Objeto de tipo @see String
	 */
	public void setImporteCargo(String importeCargo) {
		this.importeCargo = importeCargo;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad importeAbono
	 * @return importeAbono Objeto de tipo @see String
	 */
	public String getImporteAbono() {
		return importeAbono;
	}

	/**
	 * Metodo que modifica el valor de la propiedad importeAbono
	 * @param importeAbono Objeto de tipo @see String
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad importeDolares
	 * @return importeDolares Objeto de tipo @see String
	 */
	public String getImporteDolares() {
		return importeDolares;
	}

	/**
	 * Metodo que modifica el valor de la propiedad importeDolares
	 * @param importeDolares Objeto de tipo @see String
	 */
	public void setImporteDolares(String importeDolares) {
		this.importeDolares = importeDolares;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombreBcoRec
	 * @return nombreBcoRec Objeto de tipo @see String
	 */
	public String getNombreBcoRec() {
		return nombreBcoRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombreBcoRec
	 * @param nombreBcoRec Objeto de tipo @see String
	 */
	public void setNombreBcoRec(String nombreBcoRec) {
		this.nombreBcoRec = nombreBcoRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad sucBcoRe
	 * @return sucBcoRe Objeto de tipo @see String
	 */
	public String getSucBcoRe() {
		return sucBcoRe;
	}

	/**
	 * Metodo que modifica el valor de la propiedad sucBcoRe
	 * @param sucBcoRe Objeto de tipo @see String
	 */
	public void setSucBcoRe(String sucBcoRe) {
		this.sucBcoRe = sucBcoRe;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad paisBcoRec
	 * @return paisBcoRec Objeto de tipo @see String
	 */
	public String getPaisBcoRec() {
		return paisBcoRec;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paisBcoRec
	 * @param paisBcoRec Objeto de tipo @see String
	 */
	public void setPaisBcoRec(String paisBcoRec) {
		this.paisBcoRec = paisBcoRec;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad ciudadBcoRe
	 * @return ciudadBcoRe Objeto de tipo @see String
	 */
	public String getCiudadBcoRe() {
		return ciudadBcoRe;
	}

	/**
	 * Metodo que modifica el valor de la propiedad ciudadBcoRe
	 * @param ciudadBcoRe Objeto de tipo @see String
	 */
	public void setCiudadBcoRe(String ciudadBcoRe) {
		this.ciudadBcoRe = ciudadBcoRe;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveABA
	 * @return cveABA Objeto de tipo @see String
	 */
	public String getCveABA() {
		return cveABA;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveABA
	 * @param cveABA Objeto de tipo @see String
	 */
	public void setCveABA(String cveABA) {
		this.cveABA = cveABA;
	}

}
