/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 12:31:44 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que se van ha consultar
 * en la bitacora administrativa
**/

public class BeanConsBitAdmon2  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	/**Objeto del tipo String que almacena el folio de operacion*/
	private String folioOper;
	/**Objeto del tipo String que almacena el idToken*/
	private String idToken;
	/**Objeto del tipo String que almacena el idOperacion*/
	private String idOperacion;
	/**Objeto del tipo String que almacena el estatusOperacion*/
	private String estatusOperacion;
	/**Objeto del tipo String que almacena el codOperacion*/
	private String codOperacion;
	/**Objeto del tipo String que almacena el idSesion*/
	private String idSesion;
	/**Objeto del tipo String que almacena la tabla afectada*/
	private String tablaAfectada;
	/**Objeto del tipo String que almacena la instancia Web*/
	private String instanciaWeb;
	/**Objeto del tipo String que almacena el host web*/
	private String hostWeb;
	/**Objeto del tipo String que almacena el dato fijo*/
	private String datoFijo;
	/**
	 * Metodo get que almacena el folio de operacion
	 * @return folioOper Objeto del tipo String que almacena el folio de operacion
	 */
	public String getFolioOper() {
		return folioOper;
	}
	/**
	 * Metodo set que almacena el folio de operacion
	 * @param folioOper Objeto del tipo String que almacena el folio de operacion
	 */
	public void setFolioOper(String folioOper) {
		this.folioOper = folioOper;
	}
	/**
	 * Metodo get que almacena el idtoken
	 * @return idToken Objeto del tipo String que almacena el idToken
	 */
	public String getIdToken() {
		return idToken;
	}
	/**
	 * Metodo set que almacena el token
	 * @param idToken Objeto del tipo String que almacena el idtoken
	 */
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	/**
	 * Metodo get que almacena el id de la operacion
	 * @return idOperacion Objeto del tipo String que almacena el folio de operacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	/**
	 * Metodo set que almacena el id Operacion
	 * @param idOperacion Objeto del tipo String que almacena el idOperacion
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
	/**
	 * Metodo get que almacena el estatus de la operacion
	 * @return estatusOperacion Objeto del tipo String que almacena el estatus de la operacion
	 */
	public String getEstatusOperacion() {
		return estatusOperacion;
	}
	/**
	 * Metodo set que almacena el estatus de la operacion
	 * @param estatusOperacion Objeto del tipo String que almacena el estatus de la operacion
	 */
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}
	/**
	 * Metodo get que almacena el codigo de operacion
	 * @return codOperacion Objeto del tipo String que almacena el codigo de operacion
	 */
	public String getCodOperacion() {
		return codOperacion;
	}
	/**
	 * Metodo set que almacena el codigo de operacion
	 * @param codOperacion Objeto del tipo String que almacena el codigo de operacion
	 */
	public void setCodOperacion(String codOperacion) {
		this.codOperacion = codOperacion;
	}
	/**
	 * Metodo get que almacena el id sesion
	 * @return idSesion Objeto del tipo String que almacena el id sesion
	 */
	public String getIdSesion() {
		return idSesion;
	}
	/**
	 * Metodo set que almacena el id sesion
	 * @param idSesion Objeto del tipo String que almacena el id sesion
	 */
	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	/**
	 * Metodo get que almacena la tabla afectada
	 * @return tablaAfectada Objeto del tipo String que almacena la tabla afectada
	 */
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	/**
	 * Metodo set que almacena la tabla afectada
	 * @param tablaAfectada Objeto del tipo String que almacena la tabla afectada
	 */
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	/**
	 * Metodo get que almacena la instancia web
	 * @return instanciaWeb Objeto del tipo String que almacena la instancia web
	 */
	public String getInstanciaWeb() {
		return instanciaWeb;
	}
	/**
	 * Metodo set que almacena la instancia web
	 * @param instanciaWeb Objeto del tipo String que almacena la instanciaWeb
	 */
	public void setInstanciaWeb(String instanciaWeb) {
		this.instanciaWeb = instanciaWeb;
	}
	/**
	 * Metodo get que almacena el host web
	 * @return hostWeb Objeto del tipo String que almacena el host web
	 */
	public String getHostWeb() {
		return hostWeb;
	}
	/**
	 * Metodo set que almacena el host web
	 * @param hostWeb Objeto del tipo String que almacena el host web
	 */
	public void setHostWeb(String hostWeb) {
		this.hostWeb = hostWeb;
	}
	/**
	 * Metodo get que almacena el dato fijo
	 * @return datoFijo Objeto del tipo String que almacena el dato fijo
	 */
	public String getDatoFijo() {
		return datoFijo;
	}
	/**
	 * Metodo set que almacena el dato fijo
	 * @param datoFijo Objeto del tipo String que almacena el dato fijo
	 */	
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}


}
