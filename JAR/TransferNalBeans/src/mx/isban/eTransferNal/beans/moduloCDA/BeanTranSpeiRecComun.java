/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTranSpeiRecComun.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/02/2019 01:18:04 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;


/**
 * Class BeanResConsTranSpeiRec.
 *
 * @author FSW-Vector
 * @since 17/09/2018
 */
public class BeanTranSpeiRecComun  implements Serializable{
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2460957237542230252L;

	/** Propiedad del tipo String que almacena el valor de opcion. */
	private String opcion;
	
	/** Propiedad del tipo String que almacena el valor de cveMiInstitucion. */
	private String cveMiInstitucion;
	
	/** Propiedad del tipo String que almacena el valor de fechaOperacion. */
	private String fechaOperacion;
	
	/** Propiedad del tipo String que almacena el valor de fechaOperacion. */
	private String fechaActual;
	
	/** Propiedad del tipo Integer que almacena el toral de registors. */
	private Integer totalReg = 0;
	
	/** La variable que contiene informacion con respecto a: pagina. */
	private String nombrePantalla; 

	/** Propiedad del tipo String que almacena el valor de referenciaTransfer. */
	private String referenciaTransfer;
	

	/** Propiedad del tipo String que almacena el valor de estatus. */
	private String estatus;
	
	/** Propiedad del tipo String que almacena el valor del numero de cuenta receptora. */
	private String numCuentaRec;
	
	/** La variable que contiene informacion con respecto a: nombre pantalla. */
	private String eliminarTodo;

	
	/**
	 * Metodo get que obtiene el valor de la propiedad totalReg.
	 *
	 * @return totalReg Objeto de tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad totalReg.
	 *
	 * @param totalReg Objeto de tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	

	/**
	 * Metodo get que obtiene el valor de la propiedad opcion.
	 *
	 * @return opcion Objeto de tipo @see String
	 */
	public String getOpcion() {
		return opcion;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad opcion.
	 *
	 * @param opcion Objeto de tipo @see String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	
	
	/**
	 * Obtener el objeto: cve mi institucion.
	 *
	 * @return the cveMiInstitucion
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}
	
	/**
	 * Definir el objeto: cve mi institucion.
	 *
	 * @param cveMiInstitucion the cveMiInstitucion to set
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}
	
	/**
	 * Obtener el objeto: fecha operacion.
	 *
	 * @return the fechaOperacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	
	/**
	 * Definir el objeto: fecha operacion.
	 *
	 * @param fechaOperacion the fechaOperacion to set
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	/**
	 * Obtener el objeto: fecha actual.
	 *
	 * @return the fechaActual
	 */
	public String getFechaActual() {
		return fechaActual;
	}
	
	/**
	 * Definir el objeto: fecha actual.
	 *
	 * @param fechaActual the fechaActual to set
	 */
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	/**
	 * Obtener el objeto: nombre pantalla.
	 *
	 * @return El objeto: nombre pantalla
	 */
	public String getNombrePantalla() {
		return nombrePantalla;
	}

	/**
	 * Definir el objeto: nombre pantalla.
	 *
	 * @param nombrePantalla El nuevo objeto: nombre pantalla
	 */
	public void setNombrePantalla(String nombrePantalla) {
		this.nombrePantalla = nombrePantalla;
	}

	/**
	 * Obtener el objeto: referencia transfer.
	 *
	 * @return El objeto: referencia transfer
	 */
	public String getReferenciaTransfer() {
		return referenciaTransfer;
	}

	/**
	 * Definir el objeto: referencia transfer.
	 *
	 * @param referenciaTransfer El nuevo objeto: referencia transfer
	 */
	public void setReferenciaTransfer(String referenciaTransfer) {
		this.referenciaTransfer = referenciaTransfer;
	}

	/**
	 * Obtener el objeto: estatus.
	 *
	 * @return El objeto: estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Definir el objeto: estatus.
	 *
	 * @param estatus El nuevo objeto: estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Obtener el objeto: num cuenta rec.
	 *
	 * @return El objeto: num cuenta rec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Definir el objeto: num cuenta rec.
	 *
	 * @param numCuentaRec El nuevo objeto: num cuenta rec
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Obtener el objeto: eliminar .
	 *
	 * @return El objeto: eliminar 
	 */
	public String getEliminarTodo() {
		return eliminarTodo;
	}

	/**
	 * Definir el objeto: eliminar .
	 *
	 * @param eliminarTodo El nuevo objeto: eliminar 
	 */
	public void setEliminarTodo(String eliminarTodo) {
		this.eliminarTodo = eliminarTodo;
	}
	
	
	

}
