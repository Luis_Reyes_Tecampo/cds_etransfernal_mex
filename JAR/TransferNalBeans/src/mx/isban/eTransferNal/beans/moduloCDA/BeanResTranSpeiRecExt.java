/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResTranSpeiRecExt.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/02/2019 01:28:40 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * Clase de tipo bean que contiene el responce de la pantalla de recepcion de operaciones
 * de cuenta alterna y principal, es la sumatoria de la tabla.
 *
 * @author FSW
 * @since 3/10/2018
 */
public class BeanResTranSpeiRecExt  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -194424289351107833L;
	/*** Propiedad del tipo BigDecimal que almacena la suma de Topologia V*/
	private String sumaTopoV = "";
	  /*** Propiedad del tipo String que almacena la suma de Volumen Topologia V*/
	private Integer sumaVolV = 0;
	
	/*** Propiedad del tipo String que almacena la suma de Topologia T*/
	private String sumaTopoT = "";
	  /*** Propiedad del tipo String que almacena la suma de Volumen de Topologia T*/
	private Integer sumaVolT = 0;
	
	/*** Propiedad del tipo String que almacena la suma de Topologia por liberar*/
	private String sumaTopoTxL = "";
	  /*** Propiedad del tipo String que almacena la suma de volumen por liberar*/
	private Integer sumaVolTxL = 0;
	
	/*** Propiedad del tipo String que almacena la suma de topologia rechazar*/
	private String sumaTopoRech = "";
	  /*** Propiedad del tipo String que almacena la suma de volumen rechazar*/
	private Integer sumaVolRech = 0;
	
	
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoV
	 * @return sumaTopoV Objeto de tipo @see String
	 */
	public String getSumaTopoV() {
		return sumaTopoV;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoV
	 * @param sumaTopoV Objeto de tipo @see String
	 */
	public void setSumaTopoV(String sumaTopoV) {
		this.sumaTopoV = sumaTopoV;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolV
	 * @return sumaVolV Objeto de tipo @see Integer
	 */
	public Integer getSumaVolV() {
		return sumaVolV;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolV
	 * @param sumaVolV Objeto de tipo @see Integer
	 */
	public void setSumaVolV(Integer sumaVolV) {
		this.sumaVolV = sumaVolV;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoT
	 * @return sumaTopoT Objeto de tipo @see String
	 */
	public String getSumaTopoT() {
		return sumaTopoT;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoT
	 * @param sumaTopoT Objeto de tipo @see String
	 */
	public void setSumaTopoT(String sumaTopoT) {
		this.sumaTopoT = sumaTopoT;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolT
	 * @return sumaVolT Objeto de tipo @see Integer
	 */
	public Integer getSumaVolT() {
		return sumaVolT;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolT
	 * @param sumaVolT Objeto de tipo @see Integer
	 */
	public void setSumaVolT(Integer sumaVolT) {
		this.sumaVolT = sumaVolT;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoTxL
	 * @return sumaTopoTxL Objeto de tipo @see String
	 */
	public String getSumaTopoTxL() {
		return sumaTopoTxL;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoTxL
	 * @param sumaTopoTxL Objeto de tipo @see String
	 */
	public void setSumaTopoTxL(String sumaTopoTxL) {
		this.sumaTopoTxL = sumaTopoTxL;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolTxL
	 * @return sumaVolTxL Objeto de tipo @see Integer
	 */
	public Integer getSumaVolTxL() {
		return sumaVolTxL;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolTxL
	 * @param sumaVolTxL Objeto de tipo @see Integer
	 */
	public void setSumaVolTxL(Integer sumaVolTxL) {
		this.sumaVolTxL = sumaVolTxL;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoRech
	 * @return sumaTopoRech Objeto de tipo @see String
	 */
	public String getSumaTopoRech() {
		return sumaTopoRech;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoRech
	 * @param sumaTopoRech Objeto de tipo @see String
	 */
	public void setSumaTopoRech(String sumaTopoRech) {
		this.sumaTopoRech = sumaTopoRech;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolRech
	 * @return sumaVolRech Objeto de tipo @see Integer
	 */
	public Integer getSumaVolRech() {
		return sumaVolRech;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolRech
	 * @param sumaVolRech Objeto de tipo @see Integer
	 */
	public void setSumaVolRech(Integer sumaVolRech) {
		this.sumaVolRech = sumaVolRech;
	}
	


	

}
