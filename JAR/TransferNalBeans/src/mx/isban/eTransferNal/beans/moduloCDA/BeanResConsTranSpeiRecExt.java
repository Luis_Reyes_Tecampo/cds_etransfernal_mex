package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanResConsTranSpeiRecExt extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 602526456577457337L;
	
	
	/*** Propiedad del tipo BigDecimal que almacena la suma de Topologia V*/
	private String sumaTopoV = "";
	  /*** Propiedad del tipo String que almacena la suma de Volumen Topologia V*/
	private Integer sumaVolV = 0;
	
	/*** Propiedad del tipo String que almacena la suma de Topologia T*/
	private String sumaTopoT = "";
	  /*** Propiedad del tipo String que almacena la suma de Volumen de Topologia T*/
	private Integer sumaVolT = 0;
	
	/*** Propiedad del tipo String que almacena la suma de Topologia por liberar*/
	private String sumaTopoTxL = "";
	  /*** Propiedad del tipo String que almacena la suma de volumen por liberar*/
	private Integer sumaVolTxL = 0;
	
	/*** Propiedad del tipo String que almacena la suma de topologia rechazar*/
	private String sumaTopoRech = "";
	  /*** Propiedad del tipo String que almacena la suma de volumen rechazar*/
	private Integer sumaVolRech = 0;
	
	/**
	 * Propiedad del tipo String que almacena el valor de referenciaTransfer
	 */
	private String referenciaTransfer;
	

	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	
	/**
	 * Propiedad del tipo String que almacena el valor del numero de cuenta receptora
	 */
	private String numCuentaRec;
	
	

	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador beanPaginador;
	/**
	 * Metodo get que obtiene el valor de la propiedad beanPaginador
	 * @return beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanPaginador
	 * @param beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoV
	 * @return sumaTopoV Objeto de tipo @see String
	 */
	public String getSumaTopoV() {
		return sumaTopoV;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoV
	 * @param sumaTopoV Objeto de tipo @see String
	 */
	public void setSumaTopoV(String sumaTopoV) {
		this.sumaTopoV = sumaTopoV;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolV
	 * @return sumaVolV Objeto de tipo @see Integer
	 */
	public Integer getSumaVolV() {
		return sumaVolV;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolV
	 * @param sumaVolV Objeto de tipo @see Integer
	 */
	public void setSumaVolV(Integer sumaVolV) {
		this.sumaVolV = sumaVolV;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoT
	 * @return sumaTopoT Objeto de tipo @see String
	 */
	public String getSumaTopoT() {
		return sumaTopoT;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoT
	 * @param sumaTopoT Objeto de tipo @see String
	 */
	public void setSumaTopoT(String sumaTopoT) {
		this.sumaTopoT = sumaTopoT;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolT
	 * @return sumaVolT Objeto de tipo @see Integer
	 */
	public Integer getSumaVolT() {
		return sumaVolT;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolT
	 * @param sumaVolT Objeto de tipo @see Integer
	 */
	public void setSumaVolT(Integer sumaVolT) {
		this.sumaVolT = sumaVolT;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoTxL
	 * @return sumaTopoTxL Objeto de tipo @see String
	 */
	public String getSumaTopoTxL() {
		return sumaTopoTxL;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoTxL
	 * @param sumaTopoTxL Objeto de tipo @see String
	 */
	public void setSumaTopoTxL(String sumaTopoTxL) {
		this.sumaTopoTxL = sumaTopoTxL;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolTxL
	 * @return sumaVolTxL Objeto de tipo @see Integer
	 */
	public Integer getSumaVolTxL() {
		return sumaVolTxL;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolTxL
	 * @param sumaVolTxL Objeto de tipo @see Integer
	 */
	public void setSumaVolTxL(Integer sumaVolTxL) {
		this.sumaVolTxL = sumaVolTxL;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaTopoRech
	 * @return sumaTopoRech Objeto de tipo @see String
	 */
	public String getSumaTopoRech() {
		return sumaTopoRech;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaTopoRech
	 * @param sumaTopoRech Objeto de tipo @see String
	 */
	public void setSumaTopoRech(String sumaTopoRech) {
		this.sumaTopoRech = sumaTopoRech;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad sumaVolRech
	 * @return sumaVolRech Objeto de tipo @see Integer
	 */
	public Integer getSumaVolRech() {
		return sumaVolRech;
	}
	/**
	 * Metodo que modifica el valor de la propiedad sumaVolRech
	 * @param sumaVolRech Objeto de tipo @see Integer
	 */
	public void setSumaVolRech(Integer sumaVolRech) {
		this.sumaVolRech = sumaVolRech;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad referenciaTransfer
	 * @return referenciaTransfer Objeto de tipo @see String
	 */
	public String getReferenciaTransfer() {
		return referenciaTransfer;
	}
	/**
	 * Metodo que modifica el valor de la propiedad referenciaTransfer
	 * @param referenciaTransfer Objeto de tipo @see String
	 */
	public void setReferenciaTransfer(String referenciaTransfer) {
		this.referenciaTransfer = referenciaTransfer;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatus
	 * @return estatus Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatus
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaRec
	 * @return numCuentaRec Objeto del tipo String
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaRec
	 * @param numCuentaRec del tipo String
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	


	

}
