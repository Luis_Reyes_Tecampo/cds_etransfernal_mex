/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovCdaOrdenante.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   31/12/2013 16:45:28 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 * de la funcionalidad  de Consulta de Detalle Movimiento CDA
**/
public class BeanResConsMovCdaOrdenante implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -2532677393774316754L;
	/**Propiedad del tipo String que almacena la clave SPEI*/
	private String cveSpei;
	/**Propiedad del tipo String que almacena el nombre de la institucion emisora*/
	private String nomInstEmisora;
	/**Propiedad del tipo String que almacena el numero de cuenta del beneficiario*/
	/**Propiedad del tipo String que almacena el nombre Ordenante*/
	private String nombreOrd;
	/**Propiedad del tipo String que almacena el numero cuenta ordenante*/
	private String ctaOrd;
	/**Propiedad del tipo String que almacena el tipo de cuenta Ordenante*/
	private String tipoCuentaOrd;
	/**Propiedad del tipo String que almacena el RFC/CURP del ordenante*/
	private String rfcCurpOrdenante;
	
	/**
	 * Metodo que sirve para obtener la propiedad cveSpei
	 * @return cveSpei Objeto de tipo @see String
	 */
	public String getCveSpei() {
		return cveSpei;
	}
	/**
	 * Modifica el valor de la propiedad cveSpei
	 * @param cveSpei Objeto de tipo @see String
	 */
	public void setCveSpei(String cveSpei) {
		this.cveSpei = cveSpei;
	}
	/**
	 * Metodo que sirve para obtener la propiedad nomInstEmisora
	 * @return nomInstEmisora Objeto de tipo @see String
	 */
	public String getNomInstEmisora() {
		return nomInstEmisora;
	}
	/**
	 * Modifica el valor de la propiedad nomInstEmisora
	 * @param nomInstEmisora Objeto de tipo @see String
	 */
	public void setNomInstEmisora(String nomInstEmisora) {
		this.nomInstEmisora = nomInstEmisora;
	}
	/**
	 * Metodo que sirve para obtener la propiedad nombreOrd
	 * @return nombreOrd Objeto de tipo @see String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * Modifica el valor de la propiedad nombreOrd
	 * @param nombreOrd Objeto de tipo @see String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}
	/**
	 * Metodo que sirve para obtener la propiedad ctaOrd
	 * @return ctaOrd Objeto de tipo @see String
	 */
	public String getCtaOrd() {
		return ctaOrd;
	}
	/**
	 * Modifica el valor de la propiedad ctaOrd
	 * @param ctaOrd Objeto de tipo @see String
	 */
	public void setCtaOrd(String ctaOrd) {
		this.ctaOrd = ctaOrd;
	}
	/**
	 * Metodo que sirve para obtener la propiedad tipoCuentaOrd
	 * @return tipoCuentaOrd Objeto de tipo @see String
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	/**
	 * Modifica el valor de la propiedad tipoCuentaOrd
	 * @param tipoCuentaOrd Objeto de tipo @see String
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	/**
	 * Metodo que sirve para obtener la propiedad rfcCurp Ordenante
	 * @return rfcCurpOrdenante Objeto de tipo @see String
	 */
	public String getRfcCurpOrdenante() {
		return rfcCurpOrdenante;
	}
	/**
	 * Modifica el valor de la propiedad rfcCurp
	 * @param rfcCurpOrdenante Objeto de tipo @see String
	 */
	public void setRfcCurpOrdenante(String rfcCurpOrdenante) {
		this.rfcCurpOrdenante = rfcCurpOrdenante;
	}
}
