/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqGenArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la funcionalidad de Generar Archivo CDA Historico Detalle
**/

public class BeanReqGenArchCDAHistDet implements Serializable {

   /**
	 * Constante del Serial version
	 */
	private static final long serialVersionUID = 1179355862569121285L;

   /**Propiedad del tipo @see String que almacena el idPeticion*/
   private String idPeticion;
   /**Propiedad del tipo @see String que almacena el numero de registros*/
   private String numRegArchivo;
   
   /**Propiedad del tipo @see BeanPaginador que almacena la abstraccion de la paginacion*/
   private BeanPaginador paginador= null;
     
   
   /**
   *Metodo get que sirve para obtener el valor de la propiedad idPeticion
   * @return idPeticion Objeto del tipo String
   */
   public String getIdPeticion(){
      return idPeticion;
   }
   /**
   *Modifica el valor de la propiedad idPeticion
   * @param idPeticion Objeto del tipo String
   */
   public void setIdPeticion(String idPeticion){
      this.idPeticion=idPeticion;
   }
   /**
    * Metodo get que sirve para obtener la referencia al Paginador
    * @return paginador Objeto del tipo Paginador
    */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * Metodo set que almacena la referencia del paginador
	 * @param paginador  Objeto del tipo Paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
    * Metodo get que sirve para obtener el numero de registros en el archivo
    * @return numRegArchivo Objeto del tipo String
    */
	public String getNumRegArchivo() {
		return numRegArchivo;
	}
	/**
    * Metodo set que sirve para modificar el numero de registros en el archivo
    * @param numRegArchivo Objeto del tipo String
    */
	public void setNumRegArchivo(String numRegArchivo) {
		this.numRegArchivo = numRegArchivo;
	}



}
