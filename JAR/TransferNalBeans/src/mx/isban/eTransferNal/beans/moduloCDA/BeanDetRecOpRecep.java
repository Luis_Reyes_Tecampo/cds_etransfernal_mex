/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOpera.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   30/09/2015 16:48 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos del receptor
 *
 * @author FSW-Vector
 * @since 27/09/2018
 */
public class BeanDetRecOpRecep implements Serializable{
	  
  	/**  serialVersionUID Propiedad del tipo long con el valor de serialVersionUID. */
	private static final long serialVersionUID = 1630253094350246431L;
	 

	/** La variable que contiene informacion con respecto a: banco rec. */
	@Size(max=12)
	private String bancoRec;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec. */
	@Size(max=20)
	private String numCuentaRec;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec 2. */
	@Size(max=35)
	private String numCuentaRec2;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec. */
	@Size(max=2)
	private String tipoCuentaRec;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec 2. */
	@Size(max=2)
	private String tipoCuentaRec2;
	
	/** La variable que contiene informacion con respecto a: nombre rec 2. */
	@Size(max=40)
	private String nombreRec2;
	
	/** La variable que contiene informacion con respecto a: rfc rec. */
	@Size(max=18)
	private String rfcRec;
	
	/** La variable que contiene informacion con respecto a: rfc rec 2. */
	@Size(max=18)
	private String rfcRec2;
	
	/** La variable que contiene informacion con respecto a: nombre rec. */
	@Size(max=40)
	private String nombreRec;

	/** La variable que contiene informacion con respecto a: importe dls. */
	@Size(max=20)
	private String importeDls;
	
	/** La variable que contiene informacion con respecto a: importe abono. */
	@Size(max=20)
	private String importeAbono;
	
	

	/**
	 * Obtener el objeto: importe dls.
	 *
	 * @return El objeto: importe dls
	 */
	public String getImporteDls() {
		return importeDls;
	}

	/**
	 * Definir el objeto: importe dls.
	 *
	 * @param importeDls El nuevo objeto: importe dls
	 */
	public void setImporteDls(String importeDls) {
		this.importeDls = importeDls;
	}

	/**
	 * Obtener el objeto: importe abono.
	 *
	 * @return El objeto: importe abono
	 */
	public String getImporteAbono() {
		return importeAbono;
	}

	/**
	 * Definir el objeto: importe abono.
	 *
	 * @param importeAbono El nuevo objeto: importe abono
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}

	/**
	 * Obtener el objeto: banco rec.
	 *
	 * @return El objeto: banco rec
	 */
	public String getBancoRec() {
		return bancoRec;
	}

	/**
	 * Definir el objeto: banco rec.
	 *
	 * @param bancoRec El nuevo objeto: banco rec
	 */
	public void setBancoRec(String bancoRec) {
		this.bancoRec = bancoRec;
	}

	/**
	 * Obtener el objeto: num cuenta rec.
	 *
	 * @return El objeto: num cuenta rec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Definir el objeto: num cuenta rec.
	 *
	 * @param numCuentaRec El nuevo objeto: num cuenta rec
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Obtener el objeto: num cuenta rec 2.
	 *
	 * @return El objeto: num cuenta rec 2
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}

	/**
	 * Definir el objeto: num cuenta rec 2.
	 *
	 * @param numCuentaRec2 El nuevo objeto: num cuenta rec 2
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec.
	 *
	 * @return El objeto: tipo cuenta rec
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}

	/**
	 * Definir el objeto: tipo cuenta rec.
	 *
	 * @param tipoCuentaRec El nuevo objeto: tipo cuenta rec
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec 2.
	 *
	 * @return El objeto: tipo cuenta rec 2
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}

	/**
	 * Definir el objeto: tipo cuenta rec 2.
	 *
	 * @param tipoCuentaRec2 El nuevo objeto: tipo cuenta rec 2
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}

	/**
	 * Obtener el objeto: nombre rec 2.
	 *
	 * @return El objeto: nombre rec 2
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}

	/**
	 * Definir el objeto: nombre rec 2.
	 *
	 * @param nombreRec2 El nuevo objeto: nombre rec 2
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}

	/**
	 * Obtener el objeto: rfc rec.
	 *
	 * @return El objeto: rfc rec
	 */
	public String getRfcRec() {
		return rfcRec;
	}

	/**
	 * Definir el objeto: rfc rec.
	 *
	 * @param rfcRec El nuevo objeto: rfc rec
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}

	/**
	 * Obtener el objeto: rfc rec 2.
	 *
	 * @return El objeto: rfc rec 2
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}

	/**
	 * Definir el objeto: rfc rec 2.
	 *
	 * @param rfcRec2 El nuevo objeto: rfc rec 2
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}

	/**
	 * Obtener el objeto: nombre rec.
	 *
	 * @return El objeto: nombre rec
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	
	/**
	 * Definir el objeto: nombre rec.
	 *
	 * @param nombreRec El nuevo objeto: nombre rec
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}
	
	
	
}
