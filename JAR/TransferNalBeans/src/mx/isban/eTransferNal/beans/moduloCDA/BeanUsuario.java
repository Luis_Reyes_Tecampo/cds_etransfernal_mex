package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class BeanUsuario implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -475656242062298400L;
	/**Propiedad privada que almacena la cve del usuario*/
	private String cveUsuario;
	/**Propiedad privada que almacena el estatus del usuario*/
	private String estatusUsuario;
	/**Propiedad privada que almacena la fecha de ultimo acceso*/
	private String fechaUltimoAcceso;
	/**Propiedad privada que almacena la fecha de actualizacion*/
	private String fechaActualizacion;
	/**Propiedad privada que almacena el servicio*/
	private String servicio;
	/**Propiedad privada que almacena cveUsuarioUltimaOperacion*/
	private String cveUsuarioUltimaOperacion;
	/**Propiedad privada que almacena flagBloqueoSam*/
	private String flagBloqueoSam;
	/**Propiedad privada que almacena flagEliminaSam*/
	private String flagEliminaSam;
	/**Propiedad privada que almacena la lista de perfiles*/
	private List<BeanPerfil> listPerfiles = Collections.emptyList();
	
	/**
	 * Metodo get que obtiene la propiedad de cve del usuario
	 * @return cveUsuario Objeto del tipo String
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}
	/**
	 * Metodo set que modifica la propiedad de cve del usuario
	 * @param cveUsuario Objeto del tipo String
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
	/**
	 * Metodo get que obtiene la propiedad del estatus del usuario
	 * @return estatusUsuario Objeto del tipo String
	 */
	public String getEstatusUsuario() {
		return estatusUsuario;
	}
	/**
	 * Metodo set que modifica la propiedad del estatus del usuario
	 * @param estatusUsuario Objeto del tipo String
	 */
	public void setEstatusUsuario(String estatusUsuario) {
		this.estatusUsuario = estatusUsuario;
	}
	/**
	 * Metodo get que obtiene la propiedad de la fecha del ultimo acceso
	 * @return fechaUltimoAcceso Objeto del tipo String
	 */
	public String getFechaUltimoAcceso() {
		return fechaUltimoAcceso;
	}
	/**
	 * Metodo set que modifica la propiedad de la fecha del ultimo acceso
	 * @param fechaUltimoAcceso Objeto del tipo String
	 */
	public void setFechaUltimoAcceso(String fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}
	/**
	 * Metodo get que obtiene la propiedad de la fecha de actualizacion
	 * @return fechaActualizacion Objeto del tipo String
	 */
	public String getFechaActualizacion() {
		return fechaActualizacion;
	}
	/**
	 * Metodo set que modifica la propiedad de la fecha de actualizacion
	 * @param fechaActualizacion Objeto del tipo String
	 */
	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/**
	 * Metodo get que obtiene la propiedad del servicio
	 * @return servicio Objeto del tipo String
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * Metodo set que modifica la propiedad del servicio
	 * @param servicio Objeto del tipo String
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	/**
	 * Metodo get que obtiene la propiedad de la cveUsuarioUltimaOperacion
	 * @return cveUsuarioUltimaOperacion Objeto del tipo String
	 */
	public String getCveUsuarioUltimaOperacion() {
		return cveUsuarioUltimaOperacion;
	}
	/**
	 * Metodo set que modifica la propiedad de la cveUsuarioUltimaOperacion
	 * @param cveUsuarioUltimaOperacion Objeto del tipo String
	 */
	public void setCveUsuarioUltimaOperacion(String cveUsuarioUltimaOperacion) {
		this.cveUsuarioUltimaOperacion = cveUsuarioUltimaOperacion;
	}
	/**
	 * Metodo get que obtiene la propiedad de la flagBloqueoSam
	 * @return flagBloqueoSam Objeto del tipo String
	 */
	public String getFlagBloqueoSam() {
		return flagBloqueoSam;
	}
	/**
	 * Metodo set que modifica la propiedad de la flagBloqueoSam
	 * @param flagBloqueoSam Objeto del tipo String
	 */
	public void setFlagBloqueoSam(String flagBloqueoSam) {
		this.flagBloqueoSam = flagBloqueoSam;
	}
	/**
	 * Metodo get que obtiene la propiedad de la flagEliminaSam
	 * @return flagEliminaSam Objeto del tipo String
	 */
	public String getFlagEliminaSam() {
		return flagEliminaSam;
	}
	/**
	 * Metodo set que obtiene la propiedad de la flagEliminaSam
	 * @param flagEliminaSam Objeto del tipo String
	 */
	public void setFlagEliminaSam(String flagEliminaSam) {
		this.flagEliminaSam = flagEliminaSam;
	}
	/**
	 * Metodo get que obtiene la propiedad listPerfiles del tipo List<BeanPerfil>
	 * @return listPerfiles Objeto del tipo List<BeanPerfil>
	 */
	public List<BeanPerfil> getListPerfiles() {
		return listPerfiles;
	}
	/**
	 * Metodo set que modifica la propiedad listPerfiles del tipo List<BeanPerfil>
	 * @param listPerfiles Objeto del tipo List<BeanPerfil>
	 */
	public void setListPerfiles(List<BeanPerfil> listPerfiles) {
		this.listPerfiles = listPerfiles;
	}
	
	
	

}
