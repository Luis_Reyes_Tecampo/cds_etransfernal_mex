/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version Date/Hour           By       Company Description
 * ------- ------------------- -------- ------- --------------
 * 1.0     09/11/2013 11:33:48  jlleon   INDRA   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * Clase del tipo Bean DTO que se encarga de encapsular los datos
 * de retorno de la funcionalidad de monitor CDA
 * 
**/
public class BeanResMonitorCdaBO implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 6710905384333541272L;
	/**Propiedad del tipo  que almacena el bean con los Montos de CDA's */
	private BeanResMonitorCdaMontos beanResMonitorCdaMontos;
	/**Propiedad del tipo  que almacena el bean con el volumen de CDA's */
	private BeanResMonitorCdaVolumen beanResMonitorCdaVolumen;
	/**Propiedad del tipo  que almacena si existe conexion con Banco de Mexico */
	private boolean conectado;
	/**Propiedad del tipo  que almacena si existe contingencia */
	private boolean contingencia;
	/**Propiedad del tipo String  que almacena el color del semaforo*/
	private String colorSemaforo;
	/**Propiedad del tipo  que almacena el codigo de error */
	private String codError;
	/**Propiedad del tipo  que almacena el mensaje de error */
    private String msgError;
    /**Propiedad privada del tipo String que almacena el tipo de error*/
    private String tipoError;    
	
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResMonitorCdaMontos
	 * @return el beanResMonitorCdaMontos Objeto de tipo @see BeanResMonitorCdaMontos
	 */
	public BeanResMonitorCdaMontos getBeanResMonitorCdaMontos() {
		return beanResMonitorCdaMontos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResMonitorCdaMontos
	 * @param beanResMonitorCdaMontos Objeto de tipo @see BeanResMonitorCdaMontos
	 */
	public void setBeanResMonitorCdaMontos(
			BeanResMonitorCdaMontos beanResMonitorCdaMontos) {
		this.beanResMonitorCdaMontos = beanResMonitorCdaMontos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResMonitorCdaVolumen
	 * @return el beanResMonitorCdaVolumen Objeto de tipo @see BeanResMonitorCdaVolumen
	 */
	public BeanResMonitorCdaVolumen getBeanResMonitorCdaVolumen() {
		return beanResMonitorCdaVolumen;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResMonitorCdaVolumen
	 * @param beanResMonitorCdaVolumen Objeto de tipo @see BeanResMonitorCdaVolumen
	 */
	public void setBeanResMonitorCdaVolumen(
			BeanResMonitorCdaVolumen beanResMonitorCdaVolumen) {
		this.beanResMonitorCdaVolumen = beanResMonitorCdaVolumen;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad conectado
	 * @return conectado valor de tipo boolean
	 */
	public boolean isConectado() {
		return conectado;
	}
	/**
	 * Metodo que modifica el valor de la propiedad conectado
	 * @param conectado valor de tipo boolean
	 */
	public void setConectado(boolean conectado) {
		this.conectado = conectado;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad contingencia
	 * @return contingencia valor de tipo boolean
	 */
	public boolean isContingencia() {
		return contingencia;
	}
	/**
	 * Metodo que modifica el valor de la propiedad contingencia
	 * @param contingencia valor de tipo boolean
	 */
	public void setContingencia(boolean contingencia) {
		this.contingencia = contingencia;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad colorSemaforo
	 * @return colorSemaforo valor de tipo boolean
	 */
	public String getColorSemaforo() {
		return colorSemaforo;
	}
	/**
	 * Metodo que modifica el valor de la propiedad colorSemaforo
	 * @param colorSemaforo valor de tipo boolean
	 */
	public void setColorSemaforo(String colorSemaforo) {
		this.colorSemaforo = colorSemaforo;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad codError
	 * @return codError Objeto de tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad codError
	 * @param codError Objeto de tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad msgError
	 * @return msgError Objeto de tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad msgError
	 * @param msgError Objeto de tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
    /**
     *Metodo get que sirve para obtener el valor del tipo de error
     * @return tipoError Objeto del tipo String
     */
	 public String getTipoError() {
		return tipoError;
	 }
	 /**
    *Modifica el valor de la propiedad tipoError
    * @param tipoError Objeto del tipo String
    */
	 public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	 }
}
