package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



/**
 * Clase de tipo Bean que contiene algunos los atributos de una recepcion de operacion.
 *
 * @author FSW
 * @since 3/10/2018
 */
public class BeanTranSpeiRecOper implements Serializable{
	

    /** La constante serialVersionUID. */
	private static final long serialVersionUID = -6569038760548677638L;
	
	/** La variable que contiene informacion con respecto a: bean detalle. */
	@Valid
	private BeanDetRecepOperacion beanDetalle;
	
	/** Propiedad del tipo String que almacena el valor de numCuentaTran. */
	@Size(max=20)
	private String numCuentaTran;
		
	/** Propiedad del tipo String que almacena el valor de usuario. */
	@Size(max=40)
	private String usuario;
	
	/** Propiedad del tipo String que almacena el valor de agrupacion. */
	@Size(max=40)
	private String agrupacion;
	
	/** Propiedad del tipo boolean que almacena el valor de seleccionado. */
	@NotNull
	private boolean seleccionado;
	
	/** Propiedad del tipo boolean que almacena el valor de seleccionado. */
	@NotNull
	private boolean selecDevolucion;
	
	/** Propiedad del tipo boolean que almacena el valor de selecRecuperar. */
	@NotNull
	private boolean selecRecuperar;
	
	/** Propiedad del tipo boolean que almacena el valor de motivo devolucion Ant. */
	@Size(max=40)
	private String motivoDevolAnt;
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	@Size(max=40)
	private String estatus;
	
	/**
	 * Propiedad del tipo String que almacena el valor de referenciaTransfer
	 */
	@Size(max=12)
	private String referenciaTransfer;
	
	
	
	public BeanTranSpeiRecOper() {
		super();
		beanDetalle = new BeanDetRecepOperacion();
		beanDetalle.setBeanCambios(new BeanDetRecepcionOperaCambios());
		beanDetalle.setError(new BeanResBase());
		beanDetalle.setDetalleGral(new BeanDetRecepOpera());
		beanDetalle.setDetalleTopo(new BeanDetRecepOperaci());
		beanDetalle.setDetEstatus(new BeanDetRecepOp());
		beanDetalle.setReceptor(new BeanDetRecOpRecep());
		beanDetalle.setOrdenante(new BeanDetRecOpOrdent());
	}

	/**
	 * Obtener el objeto: bean detalle.
	 *
	 * @return El objeto: bean detalle
	 */
	public BeanDetRecepOperacion getBeanDetalle() {
		return beanDetalle;
	}

	/**
	 * Definir el objeto: bean detalle.
	 *
	 * @param beanDetalle El nuevo objeto: bean detalle
	 */
	public void setBeanDetalle(BeanDetRecepOperacion beanDetalle) {
		this.beanDetalle = beanDetalle;
	}

	/**
	 * Obtener el objeto: num cuenta tran.
	 *
	 * @return El objeto: num cuenta tran
	 */
	public String getNumCuentaTran() {
		return numCuentaTran;
	}

	/**
	 * Definir el objeto: num cuenta tran.
	 *
	 * @param numCuentaTran El nuevo objeto: num cuenta tran
	 */
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}

	/**
	 * Obtener el objeto: usuario.
	 *
	 * @return El objeto: usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Definir el objeto: usuario.
	 *
	 * @param usuario El nuevo objeto: usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Obtener el objeto: agrupacion.
	 *
	 * @return El objeto: agrupacion
	 */
	public String getAgrupacion() {
		return agrupacion;
	}

	/**
	 * Definir el objeto: agrupacion.
	 *
	 * @param agrupacion El nuevo objeto: agrupacion
	 */
	public void setAgrupacion(String agrupacion) {
		this.agrupacion = agrupacion;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Verificar si selec devolucion.
	 *
	 * @return true, si selec devolucion
	 */
	public boolean isSelecDevolucion() {
		return selecDevolucion;
	}

	/**
	 * Definir el objeto: selec devolucion.
	 *
	 * @param selecDevolucion El nuevo objeto: selec devolucion
	 */
	public void setSelecDevolucion(boolean selecDevolucion) {
		this.selecDevolucion = selecDevolucion;
	}

	/**
	 * Verificar si selec recuperar.
	 *
	 * @return true, si selec recuperar
	 */
	public boolean isSelecRecuperar() {
		return selecRecuperar;
	}

	/**
	 * Definir el objeto: selec recuperar.
	 *
	 * @param selecRecuperar El nuevo objeto: selec recuperar
	 */
	public void setSelecRecuperar(boolean selecRecuperar) {
		this.selecRecuperar = selecRecuperar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad estatus
	 * @return estatus Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Metodo que modifica el valor de la propiedad estatus
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	

	/**
	 * Metodo get que obtiene el valor de la propiedad referenciaTransfer
	 * @return referenciaTransfer Objeto de tipo @see String
	 */
	public String getReferenciaTransfer() {
		return referenciaTransfer;
	}

	/**
	 * Metodo que modifica el valor de la propiedad referenciaTransfer
	 * @param referenciaTransfer Objeto de tipo @see String
	 */
	public void setReferenciaTransfer(String referenciaTransfer) {
		this.referenciaTransfer = referenciaTransfer;
	}

	public String getMotivoDevolAnt() {
		return motivoDevolAnt;
	}

	public void setMotivoDevolAnt(String motivoDevolAnt) {
		this.motivoDevolAnt = motivoDevolAnt;
	}
	
	
	
}
