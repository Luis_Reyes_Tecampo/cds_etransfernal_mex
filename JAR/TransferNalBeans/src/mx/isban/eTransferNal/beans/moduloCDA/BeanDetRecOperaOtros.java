/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepcionOperaCambios.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   06/04/2018   SALVADOR MEZA  VECTOR 		Creacion de Bean
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;


import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos 
 * que  forman el detalle de recepcion de operaciones, nuevos campos agregados a la pantalla del detalle
 * apartado: Cambios.
 *
 * @author FSW-Vector
 * @since 11/02/2019
 */
public class BeanDetRecOperaOtros implements Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8402430527544979110L;
	/** The Constant serialVersionUID. */
	@Size(max=36)
	private String swift;
	
	/** La variable que contiene informacion con respecto a: swift uno. */
	@Size(max=40)
	private String swiftUno;
	
	/** La variable que contiene informacion con respecto a: swift dos. */
	@Size(max=40)
	private String swiftDos;
	
	/** La variable que contiene informacion con respecto a: refe adi. */
	@Size(max=40)
	private String refeAdi;
	
	/** La variable que contiene informacion con respecto a: ind benef rec. */
	@Size(max=40)
	private String indBenefRec;
	
	/** La variable que contiene informacion con respecto a: Monto pgo ori. */
	@Size(max=19)
	private String montoPgoOri;
	
	/** La variable que contiene informacion con respecto a: fch oper ori. */
	@Size(max=10)
	private String fchOperOri;
	
	/** La variable que contiene informacion con respecto a: fch oper ori. */
	@Size(max=40)
	private String desError;
	
	/** La variable que contiene informacion con respecto a: desc tipo pgo. */
	@Size(max=40)
	private String descTipoPgo;
	
	/** La variable que contiene informacion con respecto a: numero Cuenta Clave. */
	private String numCuentClv;
	
	/**
	 * Obtener el objeto: numCuentClv
	 *
	 * @return El objeto: numCuentClv
	 */
	public String getNumCuentClv() {
		return numCuentClv;
	}


	/**
	 * Definir el objeto: numCuentClv
	 *
	 * @param bucCliente El nuevo objeto: numCuentClv
	 */
	public void setNumCuentClv(String numCuentClv) {
		this.numCuentClv = numCuentClv;
	}
	
	/**
	 * Obtener el objeto: swift.
	 *
	 * @return El objeto: swift
	 */
	public String getSwift() {
		return swift;
	}
	
	/**
	 * Definir el objeto: swift.
	 *
	 * @param swift El nuevo objeto: swift
	 */
	public void setSwift(String swift) {
		this.swift = swift;
	}
	
	/**
	 * Obtener el objeto: swift uno.
	 *
	 * @return El objeto: swift uno
	 */
	public String getSwiftUno() {
		return swiftUno;
	}
	
	/**
	 * Definir el objeto: swift uno.
	 *
	 * @param swiftUno El nuevo objeto: swift uno
	 */
	public void setSwiftUno(String swiftUno) {
		this.swiftUno = swiftUno;
	}
	
	/**
	 * Obtener el objeto: swift dos.
	 *
	 * @return El objeto: swift dos
	 */
	public String getSwiftDos() {
		return swiftDos;
	}
	
	/**
	 * Definir el objeto: swift dos.
	 *
	 * @param swiftDos El nuevo objeto: swift dos
	 */
	public void setSwiftDos(String swiftDos) {
		this.swiftDos = swiftDos;
	}
	
	/**
	 * Obtener el objeto: refe adi.
	 *
	 * @return El objeto: refe adi
	 */
	public String getRefeAdi() {
		return refeAdi;
	}
	
	/**
	 * Definir el objeto: refe adi.
	 *
	 * @param refeAdi El nuevo objeto: refe adi
	 */
	public void setRefeAdi(String refeAdi) {
		this.refeAdi = refeAdi;
	}
	
	/**
	 * Obtener el objeto: ind benef rec.
	 *
	 * @return El objeto: ind benef rec
	 */
	public String getIndBenefRec() {
		return indBenefRec;
	}
	
	/**
	 * Definir el objeto: ind benef rec.
	 *
	 * @param indBenefRec El nuevo objeto: ind benef rec
	 */
	public void setIndBenefRec(String indBenefRec) {
		this.indBenefRec = indBenefRec;
	}
	
	
	
	/**
	 * Obtener el objeto: monto pgo ori.
	 *
	 * @return El objeto: monto pgo ori
	 */
	public String getMontoPgoOri() {
		return montoPgoOri;
	}

	/**
	 * Definir el objeto: monto pgo ori.
	 *
	 * @param montoPgoOri El nuevo objeto: monto pgo ori
	 */
	public void setMontoPgoOri(String montoPgoOri) {
		this.montoPgoOri = montoPgoOri;
	}

	/**
	 * Obtener el objeto: fch oper ori.
	 *
	 * @return El objeto: fch oper ori
	 */
	public String getFchOperOri() {
		return fchOperOri;
	}
	
	/**
	 * Definir el objeto: fch oper ori.
	 *
	 * @param fchOperOri El nuevo objeto: fch oper ori
	 */
	public void setFchOperOri(String fchOperOri) {
		this.fchOperOri = fchOperOri;
	}
	
	
	/**
	 * Obtener el objeto: des error.
	 *
	 * @return El objeto: des error
	 */
	public String getDesError() {
		return desError;
	}

	/**
	 * Definir el objeto: des error.
	 *
	 * @param desError El nuevo objeto: des error
	 */
	public void setDesError(String desError) {
		this.desError = desError;
	}

	/**
	 * Obtener el objeto: desc tipo pgo.
	 *
	 * @return El objeto: desc tipo pgo
	 */
	public String getDescTipoPgo() {
		return descTipoPgo;
	}

	/**
	 * Definir el objeto: desc tipo pgo.
	 *
	 * @param descTipoPgo El nuevo objeto: desc tipo pgo
	 */
	public void setDescTipoPgo(String descTipoPgo) {
		this.descTipoPgo = descTipoPgo;
	}
	
	
	
}