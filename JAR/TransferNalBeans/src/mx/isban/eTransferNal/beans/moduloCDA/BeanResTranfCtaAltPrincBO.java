package mx.isban.eTransferNal.beans.moduloCDA;

import java.util.Collections;
import java.util.Map;

/**
 * Class BeanResTranfCtaAltPrincBO.
 */
public class BeanResTranfCtaAltPrincBO extends BeanResBase {
	
	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = -7021368790362869527L;
	
	/**  Propiedad del tipo String que almacena estatus de conexion. */
	private String semaforo;
	
	/**  Propiedad del tipo String que almacena el saldo principal. */
	private String saldoPrincipal;
	
	/** La variable que contiene informacion con respecto a: saldo principal count. */
	private String saldoPrincipalCount;
	
	/**  Propiedad del tipo String que almacena el saldo alterno. */
	private String saldoAlterno;
	
	/** La variable que contiene informacion con respecto a: saldo alterno count. */
	private String saldoAlternoCount;
	
	/**  Propiedad del tipo String que almacena el saldo calculado. */
	private String saldoCalculado;
	
	/** La variable que contiene informacion con respecto a: saldo calculado count. */
	private String saldoCalculadoCount;
	
	/**  Propiedad del tipo String que almacena el saldo no reservado. */
	private String saldoNoReservado;
	
	/** La variable que contiene informacion con respecto a: saldo no reservado count. */
	private String saldoNoReservadoCount;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones. */
	private String saldoDevoluciones;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones count. */
	private String saldoDevolucionesCount;
	
	/** Propiedad del tipo String que almacena el valor de estatus. */
	private String estatus;
	
	/** Propiedad del tipo String que almacena el valor de referencia. */
	private String referencia;
	
	/** Variable del tipo String que almacena el store procedure que marco el error. */
	private String storeProcedure ="";
	
	/** Propiedad del tipo String que almacena el valor de tramaRespuesta. */
	private String tramaRespuesta = "";
	
	/** Propiedad del tipo Map<String,Object> que almacena el valor de resultados. */
	private Map<String,Object> resultados = Collections.emptyMap();

	/**
	 * Regresa el valor del campo storeProcedure.
	 *
	 * @return Regresa la variable storeProcedure del tipo String
	 */
	
	public String getStoreProcedure() {
		return this.storeProcedure;
	}
	
	/**
	 * Almacena el parametro storeProcedure en el campo storeProcedure.
	 *
	 * @param storeProcedure del tipo String
	 */
	
	public void setStoreProcedure(String storeProcedure) {
		this.storeProcedure = storeProcedure;
	}
	
	/**
	 * Regresa el valor del campo resultados.
	 *
	 * @return Regresa la variable resultados del tipo List<Map<String,Map<String,String>>>
	 */
	
	public Map<String, Object> getResultados() {
		return this.resultados;
	}
	
	/**
	 * Almacena el parametro resultados en el campo resultados.
	 *
	 * @param resultados del tipo List<Map<String,Map<String,String>>>
	 */
	
	public void setResultados(Map<String, Object> resultados) {
		this.resultados = resultados;
	}
	
	/**
	 * Obtener el objeto: trama respuesta.
	 *
	 * @return el tramaRespuesta
	 */
	public String getTramaRespuesta() {
		return this.tramaRespuesta;
	}
	
	/**
	 * Definir el objeto: trama respuesta.
	 *
	 * @param tramaRespuesta el tramaRespuesta a establecer
	 */
	public void setTramaRespuesta(String tramaRespuesta) {
		this.tramaRespuesta = tramaRespuesta;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad semaforo.
	 *
	 * @return semaforo Objeto de tipo @see String
	 */
	public String getSemaforo() {
		return semaforo;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad semaforo.
	 *
	 * @param semaforo Objeto de tipo @see String
	 */
	public void setSemaforo(String semaforo) {
		this.semaforo = semaforo;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad saldoPrincipal.
	 *
	 * @return saldoPrincipal Objeto de tipo @see String
	 */
	public String getSaldoPrincipal() {
		return saldoPrincipal;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad saldoPrincipal.
	 *
	 * @param saldoPrincipal Objeto de tipo @see String
	 */
	public void setSaldoPrincipal(String saldoPrincipal) {
		this.saldoPrincipal = saldoPrincipal;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad saldoAlterno.
	 *
	 * @return saldoAlterno Objeto de tipo @see String
	 */
	public String getSaldoAlterno() {
		return saldoAlterno;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad saldoAlterno.
	 *
	 * @param saldoAlterno Objeto de tipo @see String
	 */
	public void setSaldoAlterno(String saldoAlterno) {
		this.saldoAlterno = saldoAlterno;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad saldoCalculado.
	 *
	 * @return saldoCalculado Objeto de tipo @see String
	 */
	public String getSaldoCalculado() {
		return saldoCalculado;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad saldoCalculado.
	 *
	 * @param saldoCalculado Objeto de tipo @see String
	 */
	public void setSaldoCalculado(String saldoCalculado) {
		this.saldoCalculado = saldoCalculado;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad saldoNoReservado.
	 *
	 * @return saldoNoReservado Objeto de tipo @see String
	 */
	public String getSaldoNoReservado() {
		return saldoNoReservado;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad saldoNoReservado.
	 *
	 * @param saldoNoReservado Objeto de tipo @see String
	 */
	public void setSaldoNoReservado(String saldoNoReservado) {
		this.saldoNoReservado = saldoNoReservado;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad estatus.
	 *
	 * @return estatus Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad estatus.
	 *
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad referencia.
	 *
	 * @return referencia Objeto de tipo @see String
	 */
	public String getReferencia() {
		return referencia;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad referencia.
	 *
	 * @param referencia Objeto de tipo @see String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	/**
	 * Obtener el objeto: saldo principal count.
	 *
	 * @return El objeto: saldo principal count
	 */
	public String getSaldoPrincipalCount() {
		return saldoPrincipalCount;
	}
	
	/**
	 * Definir el objeto: saldo principal count.
	 *
	 * @param saldoPrincipalCount El nuevo objeto: saldo principal count
	 */
	public void setSaldoPrincipalCount(String saldoPrincipalCount) {
		this.saldoPrincipalCount = saldoPrincipalCount;
	}
	
	/**
	 * Obtener el objeto: saldo alterno count.
	 *
	 * @return El objeto: saldo alterno count
	 */
	public String getSaldoAlternoCount() {
		return saldoAlternoCount;
	}
	
	/**
	 * Definir el objeto: saldo alterno count.
	 *
	 * @param saldoAlternoCount El nuevo objeto: saldo alterno count
	 */
	public void setSaldoAlternoCount(String saldoAlternoCount) {
		this.saldoAlternoCount = saldoAlternoCount;
	}
	
	/**
	 * Obtener el objeto: saldo calculado count.
	 *
	 * @return El objeto: saldo calculado count
	 */
	public String getSaldoCalculadoCount() {
		return saldoCalculadoCount;
	}
	
	/**
	 * Definir el objeto: saldo calculado count.
	 *
	 * @param saldoCalculadoCount El nuevo objeto: saldo calculado count
	 */
	public void setSaldoCalculadoCount(String saldoCalculadoCount) {
		this.saldoCalculadoCount = saldoCalculadoCount;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado count.
	 *
	 * @return El objeto: saldo no reservado count
	 */
	public String getSaldoNoReservadoCount() {
		return saldoNoReservadoCount;
	}
	
	/**
	 * Definir el objeto: saldo no reservado count.
	 *
	 * @param saldoNoReservadoCount El nuevo objeto: saldo no reservado count
	 */
	public void setSaldoNoReservadoCount(String saldoNoReservadoCount) {
		this.saldoNoReservadoCount = saldoNoReservadoCount;
	}	
	
	/**
	 * Obtener el objeto: saldo devoluciones.
	 *
	 * @return El objeto: saldo devoluciones
	 */
	public String getSaldoDevoluciones() {
		return saldoDevoluciones;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones.
	 *
	 * @param saldoDevoluciones El nuevo objeto: saldo devoluciones
	 */
	public void setSaldoDevoluciones(String saldoDevoluciones) {
		this.saldoDevoluciones = saldoDevoluciones;
	}
	
	/**
	 * Obtener el objeto: saldo devoluciones count.
	 *
	 * @return El objeto: saldo devoluciones count
	 */
	public String getSaldoDevolucionesCount() {
		return saldoDevolucionesCount;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones count.
	 *
	 * @param saldoDevolucionesCount El nuevo objeto: saldo devoluciones count
	 */
	public void setSaldoDevolucionesCount(String saldoDevolucionesCount) {
		this.saldoDevolucionesCount = saldoDevolucionesCount;
	}
	

}
