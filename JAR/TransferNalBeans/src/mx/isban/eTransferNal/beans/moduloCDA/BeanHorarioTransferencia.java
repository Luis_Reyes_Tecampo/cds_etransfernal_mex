/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanHorarioTransferencia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   20/09/2015 20:44 INDRA     ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que  forman la configuracion
 * de horarios de transferencias
 **/
public class BeanHorarioTransferencia implements Serializable{
	
	/** Constante privada del Serial version */
	private static final long serialVersionUID = -819977006235251599L;
	/** Propiedad del tipo String almacena el dia habil */
	private String hora;
	/** Propiedad del tipo String almacena descripcion */
	private String descripcion;
	/** Propiedad del tipo String almacena si esta seleccionado o no en la lista */
	private boolean seleccionado;
	
	/**
	 * @return el hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora el hora a establecer
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return el seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado el seleccionado a establecer
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
}
