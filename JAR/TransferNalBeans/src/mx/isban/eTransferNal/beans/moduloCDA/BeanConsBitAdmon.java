/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsBitAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 12:31:44 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que se van ha insertar
 * en la bitacora administrativa
**/

public class BeanConsBitAdmon  implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad del tipo String que indica el servicio*/
   private String servicio;
   /**Propiedad del tipo String almacena la fecha de operacion*/
   private String fechaOpe;
   /**Propiedad del tipo String almacena la hora de la operacion*/
   private String hrOpe;
   /**Propiedad del tipo String almacena la ip*/
   private String ip;
   /**Propiedad del tipo String almacena el modulo, canal, operacion*/
   private String moduloCanalOp;
   /**Propiedad del tipo String almacena el usuario*/
   private String usuario;
   /**Propiedad del tipo String almacena el dato afectado*/
   private String datoAfectado;
   /**Propiedad del tipo String almacena el valor anterior*/
   private String valAnterior;
   /**Propiedad del tipo String almacena el valor nuevo*/
   private String valNuevo;
   /**Propiedad del tipo String almacena la descripcion de la operacion*/
   private String descOperacion;
   /**Propiedad del tipo String almacena el numero de registros*/
   private String numeroRegistros;
   /**Propiedad del tipo String almacena el nombre del archivo*/
   private String nomArchivo;
   /**Propiedad del tipo String almacena el codigo de error*/
   private String codError;
   /**Propiedad del tipo String almacena la descripcion del error*/
   private String descCodError;
   /**Propiedad del tipo BeanConsBitAdmon almacena las propiedades de las bitacoras*/
   private BeanConsBitAdmon2 beanConsBitAdmon2 = null;
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad servicio
   * @return servicio Objeto del tipo String
   */
   public String getServicio(){
      return servicio;
   }
   /**
   *Modifica el valor de la propiedad servicio
   * @param servicio Objeto del tipo String
   */
   public void setServicio(String servicio){
      this.servicio=servicio;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaOpe
   * @return fechaOpe Objeto del tipo String
   */
   public String getFechaOpe(){
      return fechaOpe;
   }
   /**
   *Modifica el valor de la propiedad fechaOpe
   * @param fechaOpe Objeto del tipo String
   */
   public void setFechaOpe(String fechaOpe){
      this.fechaOpe=fechaOpe;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad hrOpe
   * @return hrOpe Objeto del tipo String
   */
   public String getHrOpe(){
      return hrOpe;
   }
   /**
   *Modifica el valor de la propiedad hrOpe
   * @param hrOpe Objeto del tipo String
   */
   public void setHrOpe(String hrOpe){
      this.hrOpe=hrOpe;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad ip
   * @return ip Objeto del tipo String
   */
   public String getIp(){
      return ip;
   }
   /**
   *Modifica el valor de la propiedad ip
   * @param ip Objeto del tipo String
   */
   public void setIp(String ip){
      this.ip=ip;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad moduloCanalOp
   * @return moduloCanalOp Objeto del tipo String
   */
   public String getModuloCanalOp(){
      return moduloCanalOp;
   }
   /**
   *Modifica el valor de la propiedad moduloCanalOp
   * @param moduloCanalOp Objeto del tipo String
   */
   public void setModuloCanalOp(String moduloCanalOp){
      this.moduloCanalOp=moduloCanalOp;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad usuario
   * @return usuario Objeto del tipo String
   */
   public String getUsuario(){
      return usuario;
   }
   /**
   *Modifica el valor de la propiedad usuario
   * @param usuario Objeto del tipo String
   */
   public void setUsuario(String usuario){
      this.usuario=usuario;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad datoAfectado
   * @return datoAfectado Objeto del tipo String
   */
   public String getDatoAfectado(){
      return datoAfectado;
   }
   /**
   *Modifica el valor de la propiedad datoAfectado
   * @param datoAfectado Objeto del tipo String
   */
   public void setDatoAfectado(String datoAfectado){
      this.datoAfectado=datoAfectado;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad valAnterior
   * @return valAnterior Objeto del tipo String
   */
   public String getValAnterior(){
      return valAnterior;
   }
   /**
   *Modifica el valor de la propiedad valAnterior
   * @param valAnterior Objeto del tipo String
   */
   public void setValAnterior(String valAnterior){
      this.valAnterior=valAnterior;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad valNuevo
   * @return valNuevo Objeto del tipo String
   */
   public String getValNuevo(){
      return valNuevo;
   }
   /**
   *Modifica el valor de la propiedad valNuevo
   * @param valNuevo Objeto del tipo String
   */
   public void setValNuevo(String valNuevo){
      this.valNuevo=valNuevo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad descOperacion
   * @return descOperacion Objeto del tipo String
   */
   public String getDescOperacion(){
      return descOperacion;
   }
   /**
   *Modifica el valor de la propiedad descOperacion
   * @param descOperacion Objeto del tipo String
   */
   public void setDescOperacion(String descOperacion){
      this.descOperacion=descOperacion;
   }

   /**
   *Metodo get que sirve para obtener el valor de la propiedad numeroRegistros
   * @return numeroRegistros Objeto del tipo String
   */
   public String getNumeroRegistros(){
      return numeroRegistros;
   }
   /**
   *Modifica el valor de la propiedad numeroRegistros
   * @param numeroRegistros Objeto del tipo String
   */
   public void setNumeroRegistros(String numeroRegistros){
      this.numeroRegistros=numeroRegistros;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad nomArchivo
   * @return nomArchivo Objeto del tipo String
   */
   public String getNomArchivo(){
      return nomArchivo;
   }
   /**
   *Modifica el valor de la propiedad nomArchivo
   * @param nomArchivo Objeto del tipo String
   */
   public void setNomArchivo(String nomArchivo){
      this.nomArchivo=nomArchivo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad descCodError
   * @return descCodError Objeto del tipo String
   */
   public String getDescCodError(){
      return descCodError;
   }
   /**
   *Modifica el valor de la propiedad descCodError
   * @param descCodError Objeto del tipo String
   */
   public void setDescCodError(String descCodError){
      this.descCodError=descCodError;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad beanConsBitAdmon2
    * @return beanConsBitAdmon2 Objeto del tipo BeanConsBitAdmon2
    */
	public BeanConsBitAdmon2 getBeanConsBitAdmon2() {
		return beanConsBitAdmon2;
	}
   /**
   *Modifica el valor de la propiedad beanConsBitAdmon2
   * @param beanConsBitAdmon2 Objeto del tipo BeanConsBitAdmon2
   */
	public void setBeanConsBitAdmon2(BeanConsBitAdmon2 beanConsBitAdmon2) {
		this.beanConsBitAdmon2 = beanConsBitAdmon2;
	}
	   



}
