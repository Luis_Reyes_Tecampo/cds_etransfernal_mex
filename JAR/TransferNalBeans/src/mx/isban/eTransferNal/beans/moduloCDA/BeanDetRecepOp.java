/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  30/09/2015 16:46 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * Clase la cual se encapsulan los datos del ordenante.
 *
 * @author FSW-Vector
 * @since 11/02/2019
 */
public class BeanDetRecepOp implements Serializable{

	/**  serialVersionUID Propiedad del tipo long con el valor de serialVersionUID. */
	private static final long serialVersionUID = 5338010741824794732L;
	
	/** La variable que contiene informacion con respecto a: estatus banxico. */
	@Size(max=2)
	private String estatusBanxico;
	
	/** La variable que contiene informacion con respecto a: descripcion banxico. */
	@Size(max=40)
	private String descripcionBanxico;
	
	/** La variable que contiene informacion con respecto a: otro banxico. */
	@Size(max=2)
	private String otroBanxico;
	
	/** La variable que contiene informacion con respecto a: refe transfer. */
	@Size(max=12)
	private String refeTransfer;
	
	/** La variable que contiene informacion con respecto a: estatus transfer. */
	@Size(max=5)
	private String estatusTransfer;
	
	/** La variable que contiene informacion con respecto a: descripcion trasnfer. */
	@Size(max=40)
	private String descripcionTrasnfer;
	
	/** La variable que contiene informacion con respecto a: cve pago. */
	@Size(max=10)
	private String cvePago;
	
	/** La variable que contiene informacion con respecto a: refe cobranza. */
	@Size(max=40)
	private String refeCobranza;
	
	/** La variable que contiene informacion con respecto a: concepto pago 1. */
	@Size(max=210)
	private String conceptoPago1;
	
	/** La variable que contiene informacion con respecto a: concepto pago 2. */
	@Size(max=40)
	private String conceptoPago2;

	/**
	 * Obtener el objeto: estatus banxico.
	 *
	 * @return El objeto: estatus banxico
	 */
	public String getEstatusBanxico() {
		return estatusBanxico;
	}

	/**
	 * Definir el objeto: estatus banxico.
	 *
	 * @param estatusBanxico El nuevo objeto: estatus banxico
	 */
	public void setEstatusBanxico(String estatusBanxico) {
		this.estatusBanxico = estatusBanxico;
	}

	/**
	 * Obtener el objeto: descripcion banxico.
	 *
	 * @return El objeto: descripcion banxico
	 */
	public String getDescripcionBanxico() {
		return descripcionBanxico;
	}

	/**
	 * Definir el objeto: descripcion banxico.
	 *
	 * @param descripcionBanxico El nuevo objeto: descripcion banxico
	 */
	public void setDescripcionBanxico(String descripcionBanxico) {
		this.descripcionBanxico = descripcionBanxico;
	}

	/**
	 * Obtener el objeto: otro banxico.
	 *
	 * @return El objeto: otro banxico
	 */
	public String getOtroBanxico() {
		return otroBanxico;
	}

	/**
	 * Definir el objeto: otro banxico.
	 *
	 * @param otroBanxico El nuevo objeto: otro banxico
	 */
	public void setOtroBanxico(String otroBanxico) {
		this.otroBanxico = otroBanxico;
	}

	/**
	 * Obtener el objeto: refe transfer.
	 *
	 * @return El objeto: refe transfer
	 */
	public String getRefeTransfer() {
		return refeTransfer;
	}

	/**
	 * Definir el objeto: refe transfer.
	 *
	 * @param refeTransfer El nuevo objeto: refe transfer
	 */
	public void setRefeTransfer(String refeTransfer) {
		this.refeTransfer = refeTransfer;
	}

	/**
	 * Obtener el objeto: estatus transfer.
	 *
	 * @return El objeto: estatus transfer
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}

	/**
	 * Definir el objeto: estatus transfer.
	 *
	 * @param estatusTransfer El nuevo objeto: estatus transfer
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}

	/**
	 * Obtener el objeto: descripcion trasnfer.
	 *
	 * @return El objeto: descripcion trasnfer
	 */
	public String getDescripcionTrasnfer() {
		return descripcionTrasnfer;
	}
	
	/**
	 * Definir el objeto: descripcion trasnfer.
	 *
	 * @param descripcionTrasnfer El nuevo objeto: descripcion trasnfer
	 */
	public void setDescripcionTrasnfer(String descripcionTrasnfer) {
		this.descripcionTrasnfer = descripcionTrasnfer;
	}

	/**
	 * Obtener el objeto: cve pago.
	 *
	 * @return El objeto: cve pago
	 */
	public String getCvePago() {
		return cvePago;
	}

	/**
	 * Definir el objeto: cve pago.
	 *
	 * @param cvePago El nuevo objeto: cve pago
	 */
	public void setCvePago(String cvePago) {
		this.cvePago = cvePago;
	}

	/**
	 * Obtener el objeto: refe cobranza.
	 *
	 * @return El objeto: refe cobranza
	 */
	public String getRefeCobranza() {
		return refeCobranza;
	}

	/**
	 * Definir el objeto: refe cobranza.
	 *
	 * @param refeCobranza El nuevo objeto: refe cobranza
	 */
	public void setRefeCobranza(String refeCobranza) {
		this.refeCobranza = refeCobranza;
	}

	/**
	 * Obtener el objeto: concepto pago 1.
	 *
	 * @return El objeto: concepto pago 1
	 */
	public String getConceptoPago1() {
		return conceptoPago1;
	}

	/**
	 * Definir el objeto: concepto pago 1.
	 *
	 * @param conceptoPago1 El nuevo objeto: concepto pago 1
	 */
	public void setConceptoPago1(String conceptoPago1) {
		this.conceptoPago1 = conceptoPago1;
	}

	/**
	 * Obtener el objeto: concepto pago 2.
	 *
	 * @return El objeto: concepto pago 2
	 */
	public String getConceptoPago2() {
		return conceptoPago2;
	}

	/**
	 * Definir el objeto: concepto pago 2.
	 *
	 * @param conceptoPago2 El nuevo objeto: concepto pago 2
	 */
	public void setConceptoPago2(String conceptoPago2) {
		this.conceptoPago2 = conceptoPago2;
	}
	
	
}
