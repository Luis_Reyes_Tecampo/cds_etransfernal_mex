package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * @author cchong
 *
 */
public class BeanTransEnvio extends BeanTransEnvioExt implements Serializable{
   	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3989579016244209728L;

	/**
	 * Propiedad del tipo String que almacena el valor de cveEmpresa
	 */
	private String cveEmpresa = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ptoVta
	 */
	private String ptoVta = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de medioEnt
	 */
	private String medioEnt = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de referenciaMed
	 */
	private String referenciaMed = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de usuarioCap
	 */
	private String usuarioCap = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de usuarioSol
	 */
	private String usuarioSol = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveTransfe
	 */
	private String cveTransfe = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveOperacion
	 */
	private String cveOperacion = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de formaLiq
	 */
	private String formaLiq = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveIntermeOrd
	 */
	private String cveIntermeOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de cuentaOrd
	 */
	private String cuentaOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de ptoVtaOrd
	 */
	private String ptoVtaOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de divisaOrd
	 */
	private String divisaOrd = "";
	    	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreOrd
	 */
	private String nombreOrd = "";
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec2
	 */
	private String numCuentaRec2 = "";
	
	
	/**
	 * Metodo get que obtiene el valor de la propiedad cveEmpresa
	 * @return cveEmpresa Objeto de tipo @see String
	 */
	public String getCveEmpresa() {
		return cveEmpresa;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveEmpresa
	 * @param cveEmpresa Objeto de tipo @see String
	 */
	public void setCveEmpresa(String cveEmpresa) {
		this.cveEmpresa = cveEmpresa;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad ptoVta
	 * @return ptoVta Objeto de tipo @see String
	 */
	public String getPtoVta() {
		return ptoVta;
	}

	/**
	 * Metodo que modifica el valor de la propiedad ptoVta
	 * @param ptoVta Objeto de tipo @see String
	 */
	public void setPtoVta(String ptoVta) {
		this.ptoVta = ptoVta;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad medioEnt
	 * @return medioEnt Objeto de tipo @see String
	 */
	public String getMedioEnt() {
		return medioEnt;
	}

	/**
	 * Metodo que modifica el valor de la propiedad medioEnt
	 * @param medioEnt Objeto de tipo @see String
	 */
	public void setMedioEnt(String medioEnt) {
		this.medioEnt = medioEnt;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad referenciaMed
	 * @return referenciaMed Objeto de tipo @see String
	 */
	public String getReferenciaMed() {
		return referenciaMed;
	}

	/**
	 * Metodo que modifica el valor de la propiedad referenciaMed
	 * @param referenciaMed Objeto de tipo @see String
	 */
	public void setReferenciaMed(String referenciaMed) {
		this.referenciaMed = referenciaMed;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad usuarioCap
	 * @return usuarioCap Objeto de tipo @see String
	 */
	public String getUsuarioCap() {
		return usuarioCap;
	}

	/**
	 * Metodo que modifica el valor de la propiedad usuarioCap
	 * @param usuarioCap Objeto de tipo @see String
	 */
	public void setUsuarioCap(String usuarioCap) {
		this.usuarioCap = usuarioCap;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad usuarioSol
	 * @return usuarioSol Objeto de tipo @see String
	 */
	public String getUsuarioSol() {
		return usuarioSol;
	}

	/**
	 * Metodo que modifica el valor de la propiedad usuarioSol
	 * @param usuarioSol Objeto de tipo @see String
	 */
	public void setUsuarioSol(String usuarioSol) {
		this.usuarioSol = usuarioSol;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveTransfe
	 * @return cveTransfe Objeto de tipo @see String
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveTransfe
	 * @param cveTransfe Objeto de tipo @see String
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveOperacion
	 * @return cveOperacion Objeto de tipo @see String
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveOperacion
	 * @param cveOperacion Objeto de tipo @see String
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad formaLiq
	 * @return formaLiq Objeto de tipo @see String
	 */
	public String getFormaLiq() {
		return formaLiq;
	}

	/**
	 * Metodo que modifica el valor de la propiedad formaLiq
	 * @param formaLiq Objeto de tipo @see String
	 */
	public void setFormaLiq(String formaLiq) {
		this.formaLiq = formaLiq;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveIntermeOrd
	 * @return cveIntermeOrd Objeto de tipo @see String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveIntermeOrd
	 * @param cveIntermeOrd Objeto de tipo @see String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cuentaOrd
	 * @return cuentaOrd Objeto de tipo @see String
	 */
	public String getCuentaOrd() {
		return cuentaOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cuentaOrd
	 * @param cuentaOrd Objeto de tipo @see String
	 */
	public void setCuentaOrd(String cuentaOrd) {
		this.cuentaOrd = cuentaOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad ptoVtaOrd
	 * @return ptoVtaOrd Objeto de tipo @see String
	 */
	public String getPtoVtaOrd() {
		return ptoVtaOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad ptoVtaOrd
	 * @param ptoVtaOrd Objeto de tipo @see String
	 */
	public void setPtoVtaOrd(String ptoVtaOrd) {
		this.ptoVtaOrd = ptoVtaOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad divisaOrd
	 * @return divisaOrd Objeto de tipo @see String
	 */
	public String getDivisaOrd() {
		return divisaOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad divisaOrd
	 * @param divisaOrd Objeto de tipo @see String
	 */
	public void setDivisaOrd(String divisaOrd) {
		this.divisaOrd = divisaOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad nombreOrd
	 * @return nombreOrd Objeto de tipo @see String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}

	/**
	 * Metodo que modifica el valor de la propiedad nombreOrd
	 * @param nombreOrd Objeto de tipo @see String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad numCuentaRec2
	 * @return numCuentaRec2 Objeto de tipo @see String
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numCuentaRec2
	 * @param numCuentaRec2 Objeto de tipo @see String
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}

}
