/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResElimSelecArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 17:39:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

/**
 *Clase Bean DTO que se encarga de encapsular la respuesta en la opcion de
 * eliminar en la funcionalidad de Generar Archivo CDA Historico Detalle
 **/

public class BeanResElimSelecArchCDAHistDet implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

	/** Propiedad que almacena la abstraccion de una pagina */
	private BeanPaginador paginador;
	/**
	 * Propiedad que almacena la lista de registros que seran marcados como
	 * eliminados
	 */
	private List<BeanGenArchHistDet> listBeanGenArchHistDet;

	/**
	 * Propiedad privada del tipo String que almacena el numero de registros en
	 * el archivo
	 */
	private String numRegArchivo;
	/**
	 * Propiedad privada del tipo String que almacena el numero de registros
	 * encontrados
	 */
	private String numRegEncontrados;
	/** Propiedad privada del tipo String que almacena el codigo de error */
	private String codError = "";
	/**
	 * Propiedad privada del tipo String que almacena el codigo de mensaje de
	 * error
	 */
	private String msgError = "";
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listBeanGenArchHistDet
	 * 
	 * @return listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
	 */
	public List<BeanGenArchHistDet> getListBeanGenArchHistDet() {
		return listBeanGenArchHistDet;
	}

	/**
	 *Modifica el valor de la propiedad listBeanGenArchHistDet
	 * 
	 * @param listBeanGenArchHistDet
	 *            Objeto del tipo List<BeanGenArchHistDet>
	 */
	public void setListBeanGenArchHistDet(
			List<BeanGenArchHistDet> listBeanGenArchHistDet) {
		this.listBeanGenArchHistDet = listBeanGenArchHistDet;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paginador
	 * 
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 *Modifica el valor de la propiedad paginador
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numRegArchivo
	 * 
	 * @return numRegArchivo Objeto del tipo String
	 */
	public String getNumRegArchivo() {
		return numRegArchivo;
	}

	/**
	 *Modifica el valor de la propiedad numRegArchivo
	 * 
	 * @param numRegArchivo
	 *            Objeto del tipo String
	 */
	public void setNumRegArchivo(String numRegArchivo) {
		this.numRegArchivo = numRegArchivo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * numRegEncontrados
	 * 
	 * @return numRegEncontrados Objeto del tipo String
	 */
	public String getNumRegEncontrados() {
		return numRegEncontrados;
	}

	/**
	 *Modifica el valor de la propiedad numRegEncontrados
	 * 
	 * @param numRegEncontrados
	 *            Objeto del tipo String
	 */
	public void setNumRegEncontrados(String numRegEncontrados) {
		this.numRegEncontrados = numRegEncontrados;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

}
