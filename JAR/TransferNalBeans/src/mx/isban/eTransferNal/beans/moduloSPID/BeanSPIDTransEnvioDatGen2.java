package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDTransEnvioDatGen2 implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -4169777603380102215L;

	/**
	 * Propiedad del tipo String que almacena el valor de importeCargo
	 */
	private String importeCargo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de importeAbono
	 */
	private String importeAbono;

	/**
	 * Propiedad del tipo String que almacena el valor de motivoDevol
	 */
	private String motivoDevol;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPaquete
	 */
	private String folioPaquete;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPago
	 */
	private String folioPago;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCambio
	 */
	private String tipoCambio;
	/**
	 * Propiedad del tipo String que almacena el valor de comentario2
	 */
	private String comentario2;
	/**
	 * Propiedad del tipo String que almacena el valor de comentario3
	 */
	private String comentario3;
	/**
	 * Propiedad del tipo String que almacena el valor de conceptoPago
	 */
	private String conceptoPago;
	/**
	 * Metodo get que sirve para obtener la propiedad motivoDevol
	 * @return motivoDevol Objeto del tipo String
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad motivoDevol
	 * @param motivoDevol del tipo String
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPaquete
	 * @return folioPaquete Objeto del tipo String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPaquete
	 * @param folioPaquete del tipo String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPago
	 * @return folioPago Objeto del tipo String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPago
	 * @param folioPago del tipo String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoCambio
	 * @return tipoCambio Objeto del tipo String
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCambio
	 * @param tipoCambio del tipo String
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad comentario2
	 * @return comentario2 Objeto del tipo String
	 */
	public String getComentario2() {
		return comentario2;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad comentario2
	 * @param comentario2 del tipo String
	 */
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad comentario3
	 * @return comentario3 Objeto del tipo String
	 */
	public String getComentario3() {
		return comentario3;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad comentario3
	 * @param comentario3 del tipo String
	 */
	public void setComentario3(String comentario3) {
		this.comentario3 = comentario3;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad conceptoPago
	 * @return conceptoPago Objeto del tipo String
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad conceptoPago
	 * @param conceptoPago del tipo String
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad importeCargo
	 * @return importeCargo Objeto del tipo String
	 */
	public String getImporteCargo() {
		return importeCargo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad importeCargo
	 * @param importeCargo del tipo String
	 */
	public void setImporteCargo(String importeCargo) {
		this.importeCargo = importeCargo;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad importeAbono
	 * @return importeAbono Objeto del tipo String
	 */
	public String getImporteAbono() {
		return importeAbono;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad importeAbono
	 * @param importeAbono del tipo String
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}
	

}
