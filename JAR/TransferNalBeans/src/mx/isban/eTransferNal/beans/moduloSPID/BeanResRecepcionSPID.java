/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResRecepcionSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResRecepcionSPID implements BeanResultBO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -896986258536610206L;
	
	/**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
	/**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**
	 * 
	 */
	private BeanReceptoresSPID beanReceptorSPID;

	/**
	 * @return the beanReceptorSPID
	 */
	public BeanReceptoresSPID getBeanReceptorSPID() {
		return beanReceptorSPID;
	}

	/**
	 * @param beanReceptorSPID the beanReceptorSPID to set
	 */
	public void setBeanReceptorSPID(BeanReceptoresSPID beanReceptorSPID) {
		this.beanReceptorSPID = beanReceptorSPID;
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the msgError
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError the msgError to set
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * @return the tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * @param tipoError the tipoError to set
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
}
