/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsultaRecepcionesDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 *Clase la cual se encapsulan los datos que se utilizan en consulta recepciones
 **/
public class BeanConsultaRecepcionesDos implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8022023654101480512L;
	/** Propiedad del tipo String que almacena la topologia */
	private String topologia;
	/** Propiedad del tipo String que almacena el tipo pago */
	private Integer tipoPago;
	/** Propiedad del tipo String que almacena estatus baxico */
	private String estatusBanxico;
	/** Propiedad del tipo String que almacena estatus transfer */
	private String estatusTransfer;
	/** Propiedad del tipo String que almacena fecha operacion */
	private String fechOperacion;
	/** Propiedad del tipo String que almacena fecha captura */
	private String fechCaptura;
	/** Propiedad del tipo String que almacena clave ins ord */
	private String cveInsOrd;
	/** Propiedad del tipo String que almacena clave interme ord */
	private String cveIntermeOrd;
	/** Propiedad del tipo String que almacena folio paquete */
	private String folioPaquete;
	/** Propiedad del tipo String que almacena folio pago */
	private String folioPago;
	/** Propiedad del tipo String que almacena num cuenta rec */
	private String numCuentaRec;
	/** Propiedad del tipo String que almacena num cuenta tran */
	private String numCuentaTran;
	/** Propiedad del tipo String que almacena monto */
	private String monto;

	/** Propiedad del tipo String que almacena motivo devolucion*/
	private String motivoDevol;

	
	
	
	/**
	 * @return el estatusBanxico
	 */
	public String getEstatusBanxico() {
		return estatusBanxico;
	}
	
	/**
	 * @param estatusBanxico el estatusBanxico a establecer
	 */
	public void setEstatusBanxico(String estatusBanxico) {
		this.estatusBanxico = estatusBanxico;
	}
	/**
	 * @return el topologia
	 */
	public String getTopologia() {
		return topologia;
	}
	/**
	 * @param topologia la topologia a establecer
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	/**
	 * @return el tipoPago
	 */
	public Integer getTipoPago() {
		return tipoPago;
	}
	/**
	 * @param tipoPago el tipoPago a establecer
	 */
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * @return el estatusTransfer
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}
	
	/**
	 * @param estatusTransfer el estatusTransfer a establecer
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}
	/**
	 * @return el fechOperacion
	 */
	public String getFechOperacion() {
		return fechOperacion;
	}
	/**
	 * @param fechOperacion la fechOperacion a establecer
	 */
	public void setFechOperacion(String fechOperacion) {
		this.fechOperacion = fechOperacion;
	}
	/**
	 * @return el fechCaptura
	 */
	public String getFechCaptura() {
		return fechCaptura;
	}
	/**
	 * @param fechCaptura la fechCaptura a establecer
	 */
	public void setFechCaptura(String fechCaptura) {
		this.fechCaptura = fechCaptura;
	}
	/**
	 * @return el cveInsOrd
	 */
	public String getCveInsOrd() {
		return cveInsOrd;
	}
	/**
	 * @param cveInsOrd la cveInsOrd a establecer
	 */
	public void setCveInsOrd(String cveInsOrd) {
		this.cveInsOrd = cveInsOrd;
	}
	/**
	 * @return el folioPaquete
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 * @param folioPaquete el folioPaquete a establecer
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 * @return el folioPago
	 */
	public String getFolioPago() {
		return folioPago;
	}
	
	/**
	 * @param folioPago el folioPago a establecer
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	/**
	 * @return el numCuentaTran
	 */
	public String getNumCuentaTran() {
		return numCuentaTran;
	}
	
	/**
	 * @param numCuentaTran el numCuentaTran a establecer
	 */
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}
	/**
	 * @return el monto
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 * @param monto el monto a establecer
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	/**
	 * @return el motivoDevol
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}
	
	/**
	 * @param motivoDevol el motivoDevol a establecer
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	/**
	 * @return el cveIntermeOrd
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 * @param cveIntermeOrd el cveIntermeOrd a establecer
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	
	/**
	 * @return el numCuentaRec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}
	/**
	 * @param numCuentaRec el numCuentaRec a establecer
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	
	

}
