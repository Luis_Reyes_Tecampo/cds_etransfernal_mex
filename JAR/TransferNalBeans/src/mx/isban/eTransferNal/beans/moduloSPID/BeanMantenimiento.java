/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanMantenimiento.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que contiene los datos necesario que seran mostrados en el mantenimiento de certificados
 **/
public class BeanMantenimiento implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 6160464307503702086L;
	
	/** Propiedad del tipo String que almacena el nombre*/
	private String nombre;
	/** Propiedad del tipo String que almacena la descripción */
	private String descripcion;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nombre
	 * 
	 * @return nombre Objeto del tipo String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 *Modifica el valor de la propiedad nombre
	 * 
	 * @param nombre
	 *            Objeto del tipo String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad descripcion
	 * 
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 *Modifica el valor de la propiedad descripcion
	 * 
	 * @param descripcion
	 *            Objeto del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
