/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSaldosVolumen.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSaldosVolumen implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3546703802098639264L;
	

	/**
	 * Propiedad del tipo String que almacena el valor de devoluciones Env Conf
	 */
	private String devolucionesEnvConf;


	/**
	 * Metodo get que sirve para obtener la propiedad devolucionesEnvConf
	 * @return devolucionesEnvConf Objeto del tipo String
	 */
	public String getDevolucionesEnvConf() {
		return devolucionesEnvConf;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad devolucionesEnvConf
	 * @param devolucionesEnvConf del tipo String
	 */
	public void setDevolucionesEnvConf(String devolucionesEnvConf) {
		this.devolucionesEnvConf = devolucionesEnvConf;
	}
	


}
