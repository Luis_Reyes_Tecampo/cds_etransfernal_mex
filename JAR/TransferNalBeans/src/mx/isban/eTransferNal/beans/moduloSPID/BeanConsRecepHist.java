/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsRecepHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se van a consultar de los receptores historicos
 **/
public class BeanConsRecepHist implements Serializable {
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 4567679260543107807L;

	/** Propiedad del tipo String que indica la fechaOperacion */
	private String fechaOperacion;

	/**
	 * cve de la institucion
	 */
	private String cveMiInstituc;
	
	/**
	 * usuario de bloqueo
	 */
	private String usuario;

	/** Propiedad del tipo String que indica codigo error */
	private String codError; 

	/** Propiedad del tipo Boolean que indica seleccionado */
	private Boolean seleccionado= false;
	/** Propiedad del tipo BeanPaginador que indica beanPaginador */
	private BeanPaginador beanPaginador;
	
	/** Propiedad del tipo BeanConsRecepHistDos que indica beanConsRecepHistDos */
	private BeanConsRecepHistDos beanConsRecepHistDos;
	
	
	
	
	
	/**
	 * @return the beanConsRecepHistDos
	 */
	public BeanConsRecepHistDos getBeanConsRecepHistDos() {
		return beanConsRecepHistDos;
	}
	/**
	 * @param beanConsRecepHistDos the beanConsRecepHistDos to set
	 */
	public void setBeanConsRecepHistDos(BeanConsRecepHistDos beanConsRecepHistDos) {
		this.beanConsRecepHistDos = beanConsRecepHistDos;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPagiandor
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
	 * 
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 *Modifica el valor de la propiedad fechaOperacion
	 * 
	 * @param fechaOperacion
	 *            Objeto del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad seleccionado
	 * 
	 * @return seleccionado Objeto del tipo Boolean
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 *Modifica el valor de la propiedad seleccionado
	 * 
	 * @param seleccionado
	 *            Objeto del tipo boolean
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
	 * 
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getfechaOperacion() {
		return fechaOperacion;
	}
	/**
	 *Modifica el valor de la propiedad fechaOperacion
	 * 
	 * @param fechaOperacion
	 *            Objeto del tipo String
	 */
	public void setfechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	
	/**
	 * @return the cveMiInstituc
	 */
	public String getCveMiInstituc() {
		return cveMiInstituc;
	}
	/**
	 * @param cveMiInstituc the cveMiInstituc to set
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		this.cveMiInstituc = cveMiInstituc;
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
