/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanMantenimientoCertificadoDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos necesarios que seran mostrados en el mantenimiento de certificados
 **/
public class BeanMantenimientoCertificadoDos implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8855548383378405614L;

	/**Propiedad del tipo String que contiene el valor de numeroCertificado*/
	private String numeroCertificado;
	
	/**Propiedad del tipo String que contiene el valor de estado*/
	private String estado;
	
	/**Propiedad del tipo String que contiene el valor de palabraClave*/
	private String palabraClave;
	
	/**Propiedad del tipo String que contiene el valor de usuario*/
	private String usuario;
	
	/**Propiedad del tipo String que contiene el valor de usuarioModifica*/
	private String usuarioModifica;
	
	/**Propiedad del tipo String que contiene el valor de fechaAlta*/
	private String fechaAlta;
	
	/**Propiedad del tipo String que contiene el valor de fechaModificacion*/
	private String fechaModificacion;
	
	/**Propiedad del tipo String que contiene el valor de mensaje*/
	private String mensaje;
	
	/**Propiedad del tipo String que contiene el valor de mantenimiento*/
	private String mantenimiento;
	
	
	/**
	 * Metodo get que sirve para obtener la propiedad numeroCertificado
	 * @return numeroCertificado Objeto del tipo @see String
	 */
	public String getNumeroCertificado() {
		return numeroCertificado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad numeroCertificado
	 * @param numeroCertificado Objeto del tipo @see String
	 */
	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad estado
	 * @return estado Objeto del tipo @see String
	 */
	public String getEstado() {
		return estado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad estado
	 * @param estado Objeto del tipo @see String
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad palabraClave
	 * @return palabraClave Objeto del tipo @see String
	 */
	public String getPalabraClave() {
		return palabraClave;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad palabraClave
	 * @param palabraClave Objeto del tipo @see String
	 */
	public void setPalabraClave(String palabraClave) {
		this.palabraClave = palabraClave;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad usuario
	 * @return usuario Objeto del tipo @see String
	 */
	public String getUsuario() {
		return usuario;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad usuario
	 * @param usuario Objeto del tipo @see String
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad usuarioModifica
	 * @return usuarioModifica Objeto del tipo @see String
	 */
	public String getUsuarioModifica() {
		return usuarioModifica;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad usuarioModifica
	 * @param usuarioModifica Objeto del tipo @see String
	 */
	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad fechaAlta
	 * @return fechaAlta Objeto del tipo @see String
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaAlta
	 * @param fechaAlta Objeto del tipo @see String
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad fechaModificacion
	 * @return fechaModificacion Objeto del tipo @see String
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaModificacion
	 * @param fechaModificacion Objeto del tipo @see String
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad mensaje
	 * @return mensaje Objeto del tipo @see String
	 */
	public String getMensaje() {
		return mensaje;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad mensaje
	 * @param mensaje Objeto del tipo @see String
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad mantenimiento
	 * @return mantenimiento Objeto del tipo @see String
	 */
	public String getMantenimiento() {
		return mantenimiento;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad mantenimiento
	 * @param mantenimiento Objeto del tipo @see String
	 */
	public void setMantenimiento(String mantenimiento) {
		this.mantenimiento = mantenimiento;
	}
	
	
	
	
}
