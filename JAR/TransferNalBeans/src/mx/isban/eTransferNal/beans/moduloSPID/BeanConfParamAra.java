/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfParamAra.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en la configuración de parámetros
 **/
public class BeanConfParamAra implements Serializable {
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -4278279264272843197L;
	
	/** Propiedad del tipo String que almacena el araDestinatario */
	private String araDestinatario;
	/** Propiedad del tipo String que almacena el araCertificado */
	private String araCertificado;
	/** Propiedad del tipo String que almacena el araDireccionIP */
	private String araDireccionIP;
	/** Propiedad del tipo String que almacena el araInicial */
	private String araInicial;
	/** Propiedad del tipo String que almacena el araFinal */
	private String araFinal;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad araDestinatario
	 * 
	 * @return araDestinatario Objeto del tipo String
	 */
	public String getAraDestinatario() {
		return araDestinatario;
	}
	/**
	 *Modifica el valor de la propiedad araDestinatario
	 * 
	 * @param araDestinatario
	 *            Objeto del tipo String
	 */
	public void setAraDestinatario(String araDestinatario) {
		this.araDestinatario = araDestinatario;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad araCertificado
	 * 
	 * @return araCertificado Objeto del tipo String
	 */
	public String getAraCertificado() {
		return araCertificado;
	}
	/**
	 *Modifica el valor de la propiedad araCertificado
	 * 
	 * @param araCertificado
	 *            Objeto del tipo String
	 */
	public void setAraCertificado(String araCertificado) {
		this.araCertificado = araCertificado;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad araDireccionIP
	 * 
	 * @return araDireccionIP Objeto del tipo String
	 */
	public String getAraDireccionIP() {
		return araDireccionIP;
	}
	/**
	 *Modifica el valor de la propiedad araDireccionIP
	 * 
	 * @param araDireccionIP
	 *            Objeto del tipo String
	 */
	public void setAraDireccionIP(String araDireccionIP) {
		this.araDireccionIP = araDireccionIP;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad araInicial
	 * 
	 * @return araInicial Objeto del tipo String
	 */
	public String getAraInicial() {
		return araInicial;
	}
	/**
	 *Modifica el valor de la propiedad araInicial
	 * 
	 * @param araInicial
	 *            Objeto del tipo String
	 */
	public void setAraInicial(String araInicial) {
		this.araInicial = araInicial;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad araFinal
	 * 
	 * @return araFinal Objeto del tipo String
	 */
	public String getAraFinal() {
		return araFinal;
	}
	/**
	 *Modifica el valor de la propiedad araFinal
	 * 
	 * @param araFinal
	 *            Objeto del tipo String
	 */
	public void setAraFinal(String araFinal) {
		this.araFinal = araFinal;
	}
}
