/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqMantenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad del mantenimiento de certificados
**/
public class BeanReqMantenimientoCertificado implements Serializable {

	/**Propiedad de tipo long que mantiene el estado del onjeto*/
	private static final long serialVersionUID = -2539573143713531279L;
	
	/**Propiedad del tipo BeanPaginador que contiene el valor de paginador*/
	private BeanPaginador paginador;

	/**Propiedad del tipo Integer que contiene el valor de totalReg*/
	private Integer totalReg;
	
	/**Propiedad del tipo List<BeanMantenimientoCertificado> que contiene el valor de listaMantenimientoCertificado*/
	private List<BeanMantenimientoCertificado> listaMantenimientoCertificado;
	
	/**
	 * Metodo get que sirve para obtener la propiedad paginador
	 * @return paginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad paginador
	 * @param paginador Objeto del tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad claveMedio
	 * @param totalReg Objeto del tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaMantenimientoCertificado
	 * @return listaMantenimientoCertificado Objeto del tipo @see List<BeanMantenimientoCertificado>
	 */
	public List<BeanMantenimientoCertificado> getListaMantenimientoCertificado() {
		return listaMantenimientoCertificado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaMantenimientoCertificado
	 * @param listaMantenimientoCertificado Objeto del tipo @see List<BeanMantenimientoCertificado>
	 */
	public void setListaMantenimientoCertificado(List<BeanMantenimientoCertificado> listaMantenimientoCertificado) {
		this.listaMantenimientoCertificado = listaMantenimientoCertificado;
	}
	
	
}
