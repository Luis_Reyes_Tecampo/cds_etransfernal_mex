package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDBloqueo implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6798337296490814127L;
	/**
	 * Propiedad del tipo String que almacena el valor de usuario
	 */
	private String usuario;
	/**
	 * Propiedad del tipo boolean que almacena el valor de bloqueado
	 */
	private boolean bloqueado;
	/**
	 * Metodo get que sirve para obtener la propiedad usuario
	 * @return usuario Objeto del tipo String
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad usuario
	 * @param usuario del tipo String
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad bloqueado
	 * @return bloqueado Objeto del tipo boolean
	 */
	public boolean isBloqueado() {
		return bloqueado;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad bloqueado
	 * @param bloqueado del tipo boolean
	 */
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	

}
