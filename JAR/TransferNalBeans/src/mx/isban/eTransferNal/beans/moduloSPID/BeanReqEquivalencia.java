/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de la configuración de equivalencias
**/
public class BeanReqEquivalencia implements Serializable {

	/**
	 * Propiedad para mantener el estado del objeto
	 */
	private static final long serialVersionUID = 3491460710890158966L;
	
	/**Propiedad con la lista de equivalencias*/
	private List<BeanEquivalencia> listaEquivalencias;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaEquivalencias
	 * 
	 * @return listaEquivalencias Objeto del tipo List<BeanEquivalencia>
	 */
	public List<BeanEquivalencia> getListaEquivalencias() {
		return listaEquivalencias;
	}
	/**
	 *Modifica el valor de la propiedad listaEquivalencias
	 * 
	 * @param listaEquivalencias
	 *            Objeto del tipo List<BeanEquivalencia>
	 */
	public void setListaEquivalencias(List<BeanEquivalencia> listaEquivalencias) {
		this.listaEquivalencias = listaEquivalencias;
	}

	/**
	    *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	    * @return serialVersionUID Objeto del tipo long
	    */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
