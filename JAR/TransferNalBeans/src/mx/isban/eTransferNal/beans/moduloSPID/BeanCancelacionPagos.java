/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 * Clase la cual se encapsulan los datos de la cancelación de pagos
 * @author OPENROAD
 *
 */
public class BeanCancelacionPagos implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7849566207876741354L;
	/**
	 * 
	 */
	
	/**Propiedad privada del tipo String que almacena la fecha de operacion*/
	private String sFechaOperacion;
	/**Propiedad privada del tipo String que almacena la clave de instituci�n*/
	private String sCveInstitucion;
	
	/** Propiedad del tipo Strinbg que almacena los registros seleccionados */
	private boolean seleccionado;
	/** Propiedad del tipo Strinbg que almacena el total de registrso */
	private Integer totalReg;
	/** Propiedad del tipo BeanCancelacionPagosDos que almacena los campos */
	private BeanCancelacionPagosDos beanCancelacionPagosDos;

	
	
	
	/**
	 * @return the beanCancelacionPagosDos
	 */
	public BeanCancelacionPagosDos getBeanCancelacionPagosDos() {
		return beanCancelacionPagosDos;
	}

	/**
	 * @param beanCancelacionPagosDos the beanCancelacionPagosDos to set
	 */
	public void setBeanCancelacionPagosDos(BeanCancelacionPagosDos beanCancelacionPagosDos) {
		this.beanCancelacionPagosDos = beanCancelacionPagosDos;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la sFechaOperacion
	 * 
	 * @return sFechaOperacion Objeto del tipo String
	 */
	public String getSFechaOperacion() {
		return sFechaOperacion;
	}

	/**
	 *Modifica el valor de la propiedad sFechaOperacion
	 * 
	 * @param sFechaOperacion Objeto del tipo String
	 */
	public void setSFechaOperacion(String sFechaOperacion) {
		this.sFechaOperacion = sFechaOperacion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la sCveInstitucion
	 * 
	 * @return sCveInstitucion Objeto del tipo String
	 */
	public String getSCveInstitucion() {
		return sCveInstitucion;
	}

	/**
	 *Modifica el valor de la propiedad sCveInstitucion
	 * 
	 * @param sCveInstitucion Objeto del tipo String
	 */
	public void setSCveInstitucion(String sCveInstitucion) {
		this.sCveInstitucion = sCveInstitucion;
	}


	
	/**
	 *Metodo get que sirve para obtener el valor de totalReg
	 * 
	 * @return totalReg Objeto del tipo int
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 *Modifica el valor de la propiedad referencia
	 * 
	 * @param totalReg Objeto del tipo String
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}



	/**
	 *Metodo get que sirve para obtener el valor de seleccionado
	 * 
	 * @return seleccionado Objeto del tipo String
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 *Modifica el valor de la propiedad seleccionado
	 * 
	 * @param seleccionado Objeto del tipo String
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	


}
