/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Historial Mensajes
**/
public class BeanReqHistorialMensajes implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -1111397441208770295L;
	/**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
	   de la paginacion*/
	private BeanPaginador paginador;	
	/**Propiedad pribada de tipo String que almacena la referencia*/
	private String ref;
	
	/**
	 * 
	 */
	private String sortField;
	
	/**
	 * 
	 */
	private String sortType;
	
	
	/**
	 * @return the sortField
	 */
	public String getSortField() {
		return sortField;
	}

	/**
	 * @param sortField the sortField to set
	 */
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	/**
	 * @return the sortType
	 */
	public String getSortType() {
		return sortType;
	}

	/**
	 * @param sortType the sortType to set
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	/**
	   *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	   * @return beanPaginador Objeto del tipo BeanPaginador
	   */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	   *Modifica el valor de la propiedad beanPaginador
	   * @param paginador Objeto del tipo BeanPaginador
	   */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad referencia
	   * @return ref Objeto del tipo String
	   */
	public String getRef() {
		return ref;
	}
	

	/**
	   *Modifica el valor de la propiedad ref
	   * @param ref Objeto del tipo String
	   */
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	
}

