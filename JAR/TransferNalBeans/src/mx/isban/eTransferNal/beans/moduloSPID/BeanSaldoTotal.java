package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSaldoTotal implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6479528390217701146L;
	/**
	 * Propiedad del tipo String que almacena el valor de topoVLiquidadas
	 */
	private String topoVLiquidadas;
	/**
	 * Propiedad del tipo String que almacena el valor de topoTLiquidadas
	 */
	private String topoTLiquidadas;
	/**
	 * Propiedad del tipo String que almacena el valor de topoTPorLiquidar
	 */
	private String topoTPorLiquidar;
	/**
	 * Propiedad del tipo String que almacena el valor de rechazos
	 */
	private String rechazos;
	/**
	 * Metodo get que sirve para obtener la propiedad topoVLiquidadas
	 * @return topoVLiquidadas Objeto del tipo String
	 */
	public String getTopoVLiquidadas() {
		return topoVLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoVLiquidadas
	 * @param topoVLiquidadas del tipo String
	 */
	public void setTopoVLiquidadas(String topoVLiquidadas) {
		this.topoVLiquidadas = topoVLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTLiquidadas
	 * @return topoTLiquidadas Objeto del tipo String
	 */
	public String getTopoTLiquidadas() {
		return topoTLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTLiquidadas
	 * @param topoTLiquidadas del tipo String
	 */
	public void setTopoTLiquidadas(String topoTLiquidadas) {
		this.topoTLiquidadas = topoTLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTPorLiquidar
	 * @return topoTPorLiquidar Objeto del tipo String
	 */
	public String getTopoTPorLiquidar() {
		return topoTPorLiquidar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTPorLiquidar
	 * @param topoTPorLiquidar del tipo String
	 */
	public void setTopoTPorLiquidar(String topoTPorLiquidar) {
		this.topoTPorLiquidar = topoTPorLiquidar;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rechazos
	 * @return rechazos Objeto del tipo String
	 */
	public String getRechazos() {
		return rechazos;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rechazos
	 * @param rechazos del tipo String
	 */
	public void setRechazos(String rechazos) {
		this.rechazos = rechazos;
	}

}
