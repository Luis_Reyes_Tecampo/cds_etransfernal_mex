/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 *Clase la cual se encapsulan los datos que se utilizan en consulta recepciones
 **/
public class BeanConsultaRecepciones implements Serializable{
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 5978095462088566928L;
	
	
	/** Propiedad del tipo String que almacena codigo error */
	private String codigoError;

	/** Propiedad del tipo String que almacena mesaje error */
	private String mensajeError;
	/** Propiedad del tipo BeanPaginador que almacena la paginacion */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena total registros */
	private Integer totalReg;
	
	/** Propiedad del tipo Boolean que almacena si fue seleccionado */
	private Boolean seleccionado = false;
	
	/**Propiedad del tipo @see String que almacena el usuario*/
	private String paramUsuario = "";
	/**Propiedad del tipo @see String que almacena el servicio*/
	private String paramServicio;
	/**Propiedad del tipo @see BeanConsultaRecepcionesDos que almacena el beanConsultaRecepcionesDos*/
	private BeanConsultaRecepcionesDos beanConsultaRecepcionesDos;
	
	
	
	
	/**
	 * @return the beanConsultaRecepcionesDos
	 */
	public BeanConsultaRecepcionesDos getBeanConsultaRecepcionesDos() {
		return beanConsultaRecepcionesDos;
	}
	/**
	 * @param beanConsultaRecepcionesDos the beanConsultaRecepcionesDos to set
	 */
	public void setBeanConsultaRecepcionesDos(BeanConsultaRecepcionesDos beanConsultaRecepcionesDos) {
		this.beanConsultaRecepcionesDos = beanConsultaRecepcionesDos;
	}
	/**
	 * @return el codigoError
	 */
	public String getCodigoError() {
		return codigoError;
	}
	/**
	 * @param codigoError el codigoError a establecer
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	
	/**
	 * @return el mensajeError
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 * @param mensajeError el mensajeError a establecer
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	/**
	 * @return el beanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador el beanPaginador a establecer
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el seleccionado
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado el seleccionado a establecer
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	/**
	 * @return el paramUsuario
	 */
	public String getParamUsuario() {
		return paramUsuario;
	}
	/**
	 * @param paramUsuario el paramUsuario a establecer
	 */
	public void setParamUsuario(String paramUsuario) {
		this.paramUsuario = paramUsuario;
	}
	/**
	 * @return el paramServicio
	 */
	public String getParamServicio() {
		return paramServicio;
	}
	/**
	 * @param paramServicio el paramServicio a establecer
	 */
	public void setParamServicio(String paramServicio) {
		this.paramServicio = paramServicio;
	}
	

}
