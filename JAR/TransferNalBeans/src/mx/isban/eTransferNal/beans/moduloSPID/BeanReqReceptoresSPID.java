package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

public class BeanReqReceptoresSPID implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4791157012036732559L;
	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador paginador;
	/**
	 * Propiedad del tipo List<BeanReceptoresSPID> que almacena el valor de listBeanReceptoresSPID
	 */
	private List<BeanReceptoresSPID> listBeanReceptoresSPID = null;
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de servicio
	 */
	private String servicio;
	
	/**
	 * Propiedad del tipo String que almacena el valor de importe
	 */
	private String importeBusqueda;
	

	/**
	 * Metodo get que obtiene el valor de la propiedad paginador
	 * @return paginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad paginador
	 * @param paginador Objeto de tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad cveMiInstitucion
	 * @return cveMiInstitucion Objeto de tipo @see String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion Objeto de tipo @see String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad opcion
	 * @return opcion Objeto de tipo @see String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad opcion
	 * @param opcion Objeto de tipo @see String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad listBeanReceptoresSPID
	 * @return listBeanReceptoresSPID Objeto del tipo List<BeanReceptoresSPID>
	 */
	public List<BeanReceptoresSPID> getListBeanReceptoresSPID() {
		return listBeanReceptoresSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanReceptoresSPID
	 * @param listBeanReceptoresSPID del tipo List<BeanReceptoresSPID>
	 */
	public void setListBeanReceptoresSPID(
			List<BeanReceptoresSPID> listBeanReceptoresSPID) {
		this.listBeanReceptoresSPID = listBeanReceptoresSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad servicio
	 * @return servicio Objeto del tipo String
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad servicio
	 * @param servicio del tipo String
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad importeBusqueda
	 * @return importeBusqueda Objeto del tipo String
	 */
	public String getImporteBusqueda() {
		return importeBusqueda;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad importeBusqueda
	 * @param importeBusqueda del tipo String
	 */
	public void setImporteBusqueda(String importeBusqueda) {
		this.importeBusqueda = importeBusqueda;
	}








	
	
	
}
