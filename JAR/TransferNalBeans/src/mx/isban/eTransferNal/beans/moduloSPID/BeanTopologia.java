/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 *configuraci�n de topologias
 **/
public class BeanTopologia implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 541993864266366801L;
	
	/**Propiedad que contendra el valor de claveMedio*/
	private String claveMedio;
	
	/**Propiedad que contendra el valor de claveOperacion*/
	private String claveOperacion;
	
	/**Propiedad que contendra el valor de topologia*/
	private String topologia;
	
	/**Propiedad que contendra el valor de prioridad*/
	private String prioridad;
	
	/**Propiedad que contendra el valor de importeMinimo*/
	private String importeMinimo;
	
	/**Propiedad que contendra el valor de si un registro es seleccionado*/
	private boolean seleccionado;
	
	/**
	 * Metodo get que sirve para obtener la propiedad claveMedio
	 * @return claveMedio Objeto del tipo @see BeanClaveMedio
	 */
	public String getClaveMedio() {
		return claveMedio;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad referencia
	 * @param claveMedio Objeto del tipo @see BeanClaveMedio
	 */
	public void setClaveMedio(String claveMedio) {
		this.claveMedio = claveMedio;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad claveOperacion
	 * @return claveOperacion Objeto del tipo @see BeanClaveOperacion
	 */
	public String getClaveOperacion() {
		return claveOperacion;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad claveOperacion
	 * @param claveOperacion Objeto del tipo @see BeanClaveOperacion
	 */
	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad topologia
	 * @return topologia Objeto del tipo @see String
	 */
	public String getTopologia() {
		return topologia;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad topologia
	 * @param topologia Objeto del tipo @see String
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad prioridad
	 * @return prioridad Objeto del tipo @see String
	 */
	public String getPrioridad() {
		return prioridad;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad prioridad
	 * @param prioridad Objeto del tipo @see String
	 */
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad importeMinimo
	 * @return importeMinimo Objeto del tipo @see String
	 */
	public String getImporteMinimo() {
		return importeMinimo;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad importeMinimo
	 * @param importeMinimo Objeto del tipo @see String
	 */
	public void setImporteMinimo(String importeMinimo) {
		this.importeMinimo = importeMinimo;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad seleccionado
	 * @return seleccionado Objeto del tipo @see boolean	
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad seleccionado
	 * @param seleccionado Objeto del tipo @see boolean
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
	
}
