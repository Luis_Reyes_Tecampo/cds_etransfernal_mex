/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanOrdenReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 *Clase que encapsula los datos que se mostraran en la orden de reparación
 **/
public class BeanOrdenReparacion implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 1917495041616379641L;
	
	
	/** Propiedad del tipo String que almacena el mensajeError */
	private String mensajeError;
	/** Propiedad del tipo BeanPaginador que almacena el beanPaginador */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo boolean que almacena el seleccionado */
	private boolean seleccionado;
	
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**Propiedad del tipo @see String que almacena el usuario*/
	private String paramUsuario = "";
	
	/**Propiedad del tipo @see String que almacena el servicio*/
	private String paramServicio;
	
	/**Propiedad del tipo @see BeanOrdenReparacionDos que almacena el beanOrdenReparacionDos*/
	private BeanOrdenReparacionDos beanOrdenReparacionDos;

	
	
	
	/**
	 * @return the beanOrdenReparacionDos
	 */
	public BeanOrdenReparacionDos getBeanOrdenReparacionDos() {
		return beanOrdenReparacionDos;
	}
	/**
	 * @param beanOrdenReparacionDos the beanOrdenReparacionDos to set
	 */
	public void setBeanOrdenReparacionDos(BeanOrdenReparacionDos beanOrdenReparacionDos) {
		this.beanOrdenReparacionDos = beanOrdenReparacionDos;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad mensajeError
	 * 
	 * @return mensajeError Objeto del tipo String
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 *Modifica el valor de la propiedad mensajeError
	 * 
	 * @param mensajeError
	 *            Objeto del tipo String
	 */
	/**
	 * Metodo set que modifica la referencia de la propiedad mensajeError
	 * @param mensajeError Objeto del tipo @see String
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg Objeto del tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoDeMensaje
	 * 
	 * @return tipoDeMensaje Objeto del tipo String
	 */
	public String getParamUsuario() {
		return paramUsuario;
	}
	/**
	 *Modifica el valor de la propiedad tipoDeMensaje
	 * 
	 * @param tipoDeMensaje
	 *            Objeto del tipo String
	 */
	/**
	 * Metodo set que modifica la referencia de la propiedad paramUsuario
	 * @param paramUsuario Objeto del tipo @see String
	 */
	public void setParamUsuario(String paramUsuario) {
		this.paramUsuario = paramUsuario;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoDeMensaje
	 * 
	 * @return tipoDeMensaje Objeto del tipo String
	 */
	public String getParamServicio() {
		return paramServicio;
	}
	/**
	 *Modifica el valor de la propiedad paramServicio
	 * 
	 * @param paramServicio
	 *            Objeto del tipo String
	 */
	/**
	 * Metodo set que modifica la referencia de la propiedad paramServicio
	 * @param paramServicio Objeto del tipo @see String
	 */
	public void setParamServicio(String paramServicio) {
		this.paramServicio = paramServicio;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad seleccionado
	 * 
	 * @return seleccionado Objeto del tipo boolean
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	/**
	 *Modifica el valor de la propiedad seleccionado
	 * 
	 * @param isChecked
	 *            Objeto del tipo boolean
	 */
	/**
	 * Metodo get que sirve para obtener la propiedad seleccionado
	 * @param isChecked objeto del tipo boolean
	 */
	public void setSeleccionado(boolean isChecked) {
		this.seleccionado = isChecked;
	}
}
