/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;


import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * consulta de recepciones
 **/
public class BeanResConsultaRecepciones implements BeanResultBO, Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -1937756274068088666L;

	/** Propiedad del tipo BeanPaginador que almacena el paginador */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el totalReg */
	private Integer totalReg = 0;
	/** Propiedad de tipo Integer que almacena el total de registros del rango completo */
	private String importeTotal = "";
	
	/** Propiedad del tipo String que almacena el codError */
	private String codError;
	/** Propiedad del tipo String que almacena el msgError */
	private String msgError;
	/** Propiedad del tipo String que almacena el tipoError */
	private String tipoError;
	/** Propiedad del tipo BeanConsultaRecepciones que almacena el listaConsultaRecepciones */
	private List<BeanConsultaRecepciones> listaConsultaRecepciones;
	/** Propiedad del tipo String que almacena el nombreArchivo */
	private String nombreArchivo; 
	/** Propiedad del tipo String que almacena el tipoOrden */
	private String tipoOrden;
	/** Propiedad del tipo String que almacena el titulo */
	private String titulo;
	
	
	/**
	 * @return el beanPaginador a establecer
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador el paginador a establecer
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * @return el totalReg a mostrar
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return el msgError
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
 * @param msgError el msgError a establecer
 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError el tipoError a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * @return la listaConsultaRecepciones
	 */
	public List<BeanConsultaRecepciones> getListaConsultaRecepciones() {
		return listaConsultaRecepciones;
	}
	/**
	 * @param listaConsultaRecepciones la listaConsultaRecepciones a establecer
	 */
	public void setListaConsultaRecepciones(List<BeanConsultaRecepciones> listaConsultaRecepciones) {
		this.listaConsultaRecepciones = listaConsultaRecepciones;
	}
	/**
	 * @return el nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo el nombreArchivo a establecer
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return el tipoOrden
	 */
	public String getTipoOrden() {
		return tipoOrden;
	}
	/**
	 * @param tipoOrden el tipoOrden a establecer
	 */
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}
	/**
	 * @return el titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo el titulo a establecer
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	/**
	 * @return the importeTotal
	 */
	public String getImporteTotal() {
		return importeTotal;
	}
	/**
	 * @param importeTotal the importeTotal to set
	 */
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	
}
