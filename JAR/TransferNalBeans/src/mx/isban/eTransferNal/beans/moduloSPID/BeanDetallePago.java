/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanDetallePago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTranMensaje;
/**
 * Clase que encapsula los datos que se mostraran en el detalle de cada pago.
 *
 * @author ISBAN
 * @since 18/03/2016
 */
public class BeanDetallePago implements Serializable{

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = -8421007578080435130L;
	
	/**  Propiedad del tipo String que almacena el estatus. */
	private String estatus;
	
	/**  Propiedad del tipo BeanDatosGeneralesPago que almacena el beanDatosGeneralesPago. */
	private BeanDatosGeneralesPago beanDatosGeneralesPago;
	
	/**  Propiedad del tipo BeanOrdenantePago que almacena el beanOrdenantePago. */
	private BeanOrdenantePago beanOrdenantePago;
	
	/**  Propiedad del tipo BeanReceptorPago que almacena el beanReceptorPago. */
	private BeanReceptorPago beanReceptorPago;
	
	/**  Propiedad del tipo String que almacena el codigo de error. */
	private String codError;
	
	/** La variable que contiene informacion con respecto a: desc error. */
	private String descError;
	
	private BeanTranMensaje beanTranMensaje;
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad estatus.
	 *
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Modifica el valor de la propiedad estatus.
	 *
	 * @param estatus            Objeto del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad serialVersionUID.
	 *
	 * @return serialVersionUID Objeto del tipo Long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad beanDatosGeneralesPago.
	 *
	 * @return beanDatosGeneralesPago Objeto del tipo BeanDatosGeneralesPago
	 */
	public BeanDatosGeneralesPago getBeanDatosGeneralesPago() {
		return beanDatosGeneralesPago;
	}
	
	/**
	 * Modifica el valor de la propiedad beanDatosGeneralesPago.
	 *
	 * @param beanDatosGeneralesPago            Objeto del tipo BeanDatosGeneralesPago
	 */
	public void setBeanDatosGeneralesPago(BeanDatosGeneralesPago beanDatosGeneralesPago) {
		this.beanDatosGeneralesPago = beanDatosGeneralesPago;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad beanReceptorPago.
	 *
	 * @return beanReceptorPago Objeto del tipo BeanReceptorPago
	 */
	public BeanReceptorPago getBeanReceptorPago() {
		return beanReceptorPago;
	}
	
	/**
	 * Modifica el valor de la propiedad beanReceptorPago.
	 *
	 * @param beanReceptorPago            Objeto del tipo BeanReceptorPago
	 */
	public void setBeanReceptorPago(BeanReceptorPago beanReceptorPago) {
		this.beanReceptorPago = beanReceptorPago;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad beanOrdenantePago.
	 *
	 * @return beanOrdenantePago Objeto del tipo BeanOrdenantePago
	 */
	public BeanOrdenantePago getBeanOrdenantePago() {
		return beanOrdenantePago;
	}
	
	/**
	 * Modifica el valor de la propiedad beanOrdenantePago.
	 *
	 * @param beanOrdenantePago            Objeto del tipo BeanOrdenantePago
	 */
	public void setBeanOrdenantePago(BeanOrdenantePago beanOrdenantePago) {
		this.beanOrdenantePago = beanOrdenantePago;
	}

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad codError.
	 *
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Modifica el valor de la propiedad codError.
	 *
	 * @param codError            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Obtener el objeto: desc error.
	 *
	 * @return El objeto: desc error
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * Definir el objeto: desc error.
	 *
	 * @param descError El nuevo objeto: desc error
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
	
	public void setBeanTranMsj(BeanTranMensaje beanTranMesaje) {
		this.beanTranMensaje = beanTranMensaje;
	}
	
	public BeanTranMensaje getBeanTranMsj() {
		return beanTranMensaje;
	}

}
