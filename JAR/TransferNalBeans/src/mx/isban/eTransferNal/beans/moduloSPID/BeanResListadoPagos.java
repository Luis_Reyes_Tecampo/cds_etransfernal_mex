/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResListadoPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para el
 * listado de pagos
 **/
public class BeanResListadoPagos implements BeanResultBO, Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -1757750456912759704L;
	
	/**Propiedad del tipo BeanPaginador que almacena el beanPaginador*/
	private BeanPaginador beanPaginador;
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/** Propiedad privada del tipo String que almacena el nombre de archivo */
	private String nombreArchivo;
	/** Propiedad privada del tipo List<BeanPago> que almacena el listadoPagos */
	private List<BeanPago> listadoPagos;
	/**
	 * Importe total sin considerar la paginacion
	 */
	private String importeTotal;
	
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoError
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listadoPagos
	 * 
	 * @return listadoPagos Objeto del tipo List<BeanPago>
	 */
	public List<BeanPago> getListadoPagos() {
		return listadoPagos;
	}
	/**
	 *Modifica el valor de la propiedad listadoPagos
	 * 
	 * @param listadoPagos
	 *            Objeto del tipo List<BeanPago>
	 */
	public void setListadoPagos(
			List<BeanPago> listadoPagos) {
		this.listadoPagos = listadoPagos;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
	 * 
	 * @return nombreArchivo Objeto del tipo String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 *Modifica el valor de la propiedad nombreArchivo
	 * 
	 * @param nombreArchivo
	 *            Objeto del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return the importeTotal
	 */
	public String getImporteTotal() {
		return importeTotal;
	}

	/**
	 * @param importeTotal the importeTotal to set
	 */
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}	
}
