/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsRecepHistDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que se van a consultar de los receptores historicos
 **/
public class BeanConsRecepHistDos implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -970175024939000759L;
	/** Propiedad del tipo String que indica la topologia */
	private String topologia;
	/** Propiedad del tipo String que indica la tipopago */
	private String tipoPago;
	/** Propiedad del tipo String que indica el estatusBanxico */
	private String estatusBanxico;
	/** Propiedad del tipo String que indica el estatusTransfer */
	private String estatusTransfer;

	/** Propiedad del tipo String que indica la hora */
	private String hora;

	/** Propiedad del tipo String que indica la cveInstOrd */
	private String cveInstOrd;
	/** Propiedad del tipo String que indica la cveIntermeOrd */
	private String cveIntermeOrd;
	/** Propiedad del tipo String que indica el folio paquete */
	private String folioPaquete;
	/** Propiedad del tipo String que indica el folio pago */
	private String folioPago;
	/** Propiedad del tipo String que indica la numerocuentatran */
	private String numCuentaTran;
	/** Propiedad del tipo String que indica el monto */
	private String monto;
 
	/** Propiedad del tipo String que indica motivodevol */
	private String motivoDevol;
	/** Propiedad del tipo String que indica motivodevolDesc */
	private String motivoDevolDesc;

	/** Propiedad del tipo String que indica fecha Captura */
	private String fechaCaptura;

	
	
	

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad topologia
	 * 
	 * @return topologia Objeto del tipo String
	 */
	public String getTopologia() {
		return topologia;
	}
	/**
	 *Modifica el valor de la propiedad topologia
	 * 
	 * @param topologia
	 *            Objeto del tipo String
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoPago
	 * 
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 *Modifica el valor de la propiedad tipoPago
	 * 
	 * @param tipoPago
	 *            Objeto del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad estatusBanxico
	 * 
	 * @return estatusBanxico Objeto del tipo String
	 */
	public String getEstatusBanxico() {
		return estatusBanxico;
	}
	/**
	 *Modifica el valor de la propiedad estatusBanxico
	 * 
	 * @param estatusBanxico
	 *            Objeto del tipo String
	 */
	public void setEstatusBanxico(String estatusBanxico) {
		this.estatusBanxico = estatusBanxico;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad estatusTransfer
	 * 
	 * @return estatusTransfer Objeto del tipo String
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}
	/**
	 *Modifica el valor de la propiedad estatusTransfer
	 * 
	 * @param estatusTransfer
	 *            Objeto del tipo String
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hora
	 * 
	 * @return hora Objeto del tipo String
	 */
	public String getHora() {
		return hora;
	}
	/**
	 *Modifica el valor de la propiedad hora
	 * 
	 * @param hora
	 *            Objeto del tipo String
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveInstOrd
	 * 
	 * @return cveInstOrd Objeto del tipo String
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}
	/**
	 *Modifica el valor de la propiedad cveInstOrd
	 * 
	 * @param cveInstOrd
	 *            Objeto del tipo String
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad getCveIntermeOrd
	 * 
	 * @return cveIntermeOrd Objeto del tipo String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 *Modifica el valor de la propiedad cveIntermeOrd
	 * 
	 * @param cveIntermeOrd
	 *            Objeto del tipo String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioPaquete
	 * 
	 * @return folioPaquete Objeto del tipo String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 *Modifica el valor de la propiedad folioPaquete
	 * 
	 * @param folioPaquete
	 *            Objeto del tipo String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioPago
	 * 
	 * @return folioPago Objeto del tipo String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 *Modifica el valor de la propiedad folioPago
	 * 
	 * @param folioPago
	 *            Objeto del tipo String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numcuentatran
	 * 
	 * @return numCuentaTran Objeto del tipo String
	 */
	public String getNumCuentaTran() {
		return numCuentaTran;
	}
	/**
	 *Modifica el valor de la propiedad numCuentaTran
	 * 
	 * @param numCuentaTran
	 *            Objeto del tipo String
	 */
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad monto
	 * 
	 * @return monto Objeto del tipo String
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 *Modifica el valor de la propiedad monto
	 * 
	 * @param monto
	 *            Objeto del tipo String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad motivoDevol
	 * 
	 * @return motivoDevol Objeto del tipo String
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}
	/**
	 *Modifica el valor de la propiedad motivoDevol
	 * 
	 * @param motivoDevol
	 *            Objeto del tipo String
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	
	
	/**
	 * @return the motivoDevolDesc
	 */
	public String getMotivoDevolDesc() {
		return motivoDevolDesc;
	}
	/**
	 * @param motivoDevolDesc the motivoDevolDesc to set
	 */
	public void setMotivoDevolDesc(String motivoDevolDesc) {
		this.motivoDevolDesc = motivoDevolDesc;
	}
	/**
	 * @return the fechaCaptura
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	/**
	 * @param fechaCaptura the fechaCaptura to set
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}
	
}
