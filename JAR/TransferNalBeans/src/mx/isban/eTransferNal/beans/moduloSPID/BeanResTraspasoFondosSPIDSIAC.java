package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResTraspasoFondosSPIDSIAC extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7998848308924472715L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de referencia
	 */
	private String referencia;
	
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tramaRespuesta
	 */
	private String tramaRespuesta;
	/**
	 * Metodo get que sirve para obtener la propiedad cveMiInstitucion
	 * @return cveMiInstitucion Objeto del tipo String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad referencia
	 * @return referencia Objeto del tipo String
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad referencia
	 * @param referencia del tipo String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad estatus
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad estatus
	 * @param estatus del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion del tipo String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchOperacion
	 * @return fchOperacion Objeto del tipo String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchOperacion
	 * @param fchOperacion del tipo String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tramaRespuesta
	 * @return tramaRespuesta Objeto del tipo String
	 */
	public String getTramaRespuesta() {
		return tramaRespuesta;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tramaRespuesta
	 * @param tramaRespuesta del tipo String
	 */
	public void setTramaRespuesta(String tramaRespuesta) {
		this.tramaRespuesta = tramaRespuesta;
	}
	
	
}
