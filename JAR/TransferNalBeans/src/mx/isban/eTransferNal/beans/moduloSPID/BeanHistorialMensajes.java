/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 *Clase que encapsula los datos que se mostraran en la historia del mensaje
 **/
public class BeanHistorialMensajes implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 7827253903562182659L;
	
	/** Propiedad del tipo String que almacena el del*/
	private String del;
	/** Propiedad del tipo String que almacena el estatus*/
	private String estatus;
	/** Propiedad del tipo String que almacena el al*/
	private String al;
	/** Propiedad del tipo String que almacena el estatus 2*/
	private String estatus2;
	/** Propiedad del tipo String que almacena el usuario*/
	private String usuario;
	/** Propiedad del tipo String que almacena la fecha*/
	private String fecha;
	
	/**
	 *Metodo get que sirve para obtener el valor de del
	 * 
	 * @return del Objeto del tipo String
	 */
	public String getDel() {
		return del;
	}
	
	/**
	 *Modifica el valor de la propiedad del
	 * 
	 * @param del Objeto del tipo String
	 */
	public void setDel(String del) {
		this.del = del;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de estatus
	 * 
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 *Modifica el valor de la propiedad estatus
	 * 
	 * @param estatus Objeto del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de al
	 * 
	 * @return al Objeto del tipo String
	 */
	public String getAl() {
		return al;
	}

	/**
	 *Modifica el valor de la propiedad al
	 * 
	 * @param al Objeto del tipo String
	 */
	public void setAl(String al) {
		this.al = al;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de estatus2
	 * 
	 * @return estatus2 Objeto del tipo String
	 */
	public String getEstatus2() {
		return estatus2;
	}
	
	/**
	 *Modifica el valor de la propiedad estatus2
	 * 
	 * @param estatus2 Objeto del tipo String
	 */
	public void setEstatus2(String estatus2) {
		this.estatus2 = estatus2;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de usuario
	 * 
	 * @return usuario Objeto del tipo String
	 */
	public String getUsuario() {
		return usuario;
	}
	
	/**
	 *Modifica el valor de la propiedad usuario
	 * 
	 * @param usuario Objeto del tipo String
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/**
	 *Modifica el valor de la propiedad fecha
	 * 
	 * @return fecha Objeto del tipo String
	 */
	public String getFecha() {
		return fecha;
	}
	
	/**
	 *Modifica el valor de la propiedad fecha
	 * 
	 * @param fecha Objeto del tipo String
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	

}
