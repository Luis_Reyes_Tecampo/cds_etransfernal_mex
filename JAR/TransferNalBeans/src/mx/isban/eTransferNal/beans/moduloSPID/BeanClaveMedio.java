/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanClaveMedio.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Clase que encapsula los datos que se mostraran en la configuraci�n de topolog�as
 *
 */
public class BeanClaveMedio implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 6053148647097203299L;

	/** Propiedad del tipo String que almacena la claveMedio */
	private String claveMedio;
	
	/** Propiedad del tipo String que almacena la descripci�n */
	private String descripcion;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveMedio
	 * 
	 * @return claveMedio Objeto del tipo String
	 */
	public String getClaveMedio() {
		return claveMedio;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad claveMedio
	 * @param claveMedio Objeto del tipo @see String
	 */
	public void setClaveMedio(String claveMedio) {
		this.claveMedio = claveMedio;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad descripcion
	 * 
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad claveOperacion
	 * @param descripcion Objeto del tipo @see String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 *Metodo que sirve para concatenar los valores de las propiedades claveMedio y descripcion
	 * 
	 * @return claveMedio +" - "+descripcion Objeto del tipo String
	 */
	/**
	 * Metodo que devuelve toString() del Objeto 
	 * @return String
	 */
	@Override
	public String toString() {
		return claveMedio +" - "+descripcion;
	}
}
