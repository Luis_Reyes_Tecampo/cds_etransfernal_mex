/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanMonitorBancos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se utilizan en monitor bancos
 **/
public class BeanMonitorBancos implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8093185039957044416L;
	/** Propiedad del tipo String que almacena clave banco */
	private String cveBanco;
	/** Propiedad del tipo String que almacena clave interme */
	private String cveInterme;
	/** Propiedad del tipo String que almacena nombre banco */
	private String nombreBanco;
	/** Propiedad del tipo String que almacena estado recep */
	private String estadoRecep;
	
	/** Propiedad del tipo String que almacena estado instit*/
	private String estadoInstit;
	/** Propiedad del tipo String que almacena intermeNoReg a establecer*/
	private String intermeNoReg;
	/** Propiedad del tipo String que almacena mesaje error */
	private String mensajeError;
	/** Propiedad del tipo BeanPaginador que almacena la paginacion */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena total registros */
	private Integer totalReg;
	/** Propiedad del tipo Boolean que almacena si fue seleccionado */
	private Boolean seleccionado = false;
	/**
	 * @return el cveBanco
	 */
	public String getCveBanco() {
		return cveBanco;
	}
	/**
	 * @param cveBanco el cveBanco a establecer
	 */
	public void setCveBanco(String cveBanco) {
		this.cveBanco = cveBanco;
	}
	/**
	 * @return el cveInterme
	 */
	public String getCveInterme() {
		return cveInterme;
	}
	/**
	 * @param cveinterme el cveInterme a establecer
	 */
	public void setCveInterme(String cveinterme) {
		this.cveInterme = cveinterme;
	}
	/**
	 * @return el nombreBanco
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}
	/**
	 * @param nombreBanco el nombreBanco a establecer
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	/**
	 * @return el estadoRecep
	 */
	public String getEstadoRecep() {
		return estadoRecep;
	}
	/**
	 * @param estadoRecep el estadoRecep a establecer
	 */
	public void setEstadoRecep(String estadoRecep) {
		this.estadoRecep = estadoRecep;
	}
	/**
	 * @return el estadoInstit
	 */
	public String getEstadoInstit() {
		return estadoInstit;
	}
	/**
	 * @param estadoInstit el estadoInstit a establecer
	 */
	public void setEstadoInstit(String estadoInstit) {
		this.estadoInstit = estadoInstit;
	}
	
	/**
	 * @return el intermeNoReg
	 */
	public String getIntermeNoReg() {
		return intermeNoReg;
	}
	/**
	 * @param intermeNoReg el intermeNoReg a establecer
	 */
	public void setIntermeNoReg(String intermeNoReg) {
		this.intermeNoReg = intermeNoReg;
	}
	/**
	 * @return el mensajeError
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 * @param mensajeError el mensajeError a establecer
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	/**
	 * @return el beanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador el beanPaginador a establecer
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el seleccionado
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado el seleccionado a establecer
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	

}
