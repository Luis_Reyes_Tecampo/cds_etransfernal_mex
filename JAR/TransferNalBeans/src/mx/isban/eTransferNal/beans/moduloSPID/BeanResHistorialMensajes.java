/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * historia del mensaje
 **/
public class BeanResHistorialMensajes  implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8652290926009604520L;

	
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/** Propiedad privada del tipo String que almacena La referencia */
	private String ref;
	
	/**
	 * Propiedad del tipo List<BeanHistorialMensajes> que almacena el listado de
	 * registros de Historial Mensajes
	 */
	private List<BeanHistorialMensajes> listaHisMsj;

	/**
	 * Metodo get que obtiene la referencia
	 * 
	 * @return String objeto con la referencia
	 */
	public String getRef() {
		return ref;
	}

	/**
	 * Metodo set que sirve para setear la referencia
	 * 
	 * @param ref objeto para setear la referencia
	 *           
	 */
	public void setRef(String ref) {
		this.ref = ref;
	}	

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaHisMsj
	 * 
	 * @return listaHisMsj Objeto del tipo List<BeanHistorialMensajes>
	 */
	public List<BeanHistorialMensajes> getListaHisMsj() {
		return listaHisMsj;
	}

	/**
	 *Modifica el valor de la propiedad listaHisMsj
	 * 
	 * @param listaHisMsj
	 *            Objeto del tipo List<BeanHistorialMensajes>
	 */
	public void setListaHisMsj(List<BeanHistorialMensajes> listaHisMsj) {
		this.listaHisMsj = listaHisMsj;
	}
}
