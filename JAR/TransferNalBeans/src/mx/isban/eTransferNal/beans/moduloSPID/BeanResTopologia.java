/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * configuración de topologias
 **/
public class BeanResTopologia implements BeanResultBO,  Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -3908309832263794336L;
	
	/**Propiedad que contendra los resultado que se mostraran en la lista*/
	private List<BeanTopologia> listaTopologias;
	
	/**Propiedad que contendra los select de los combos cuando se cree una topologia*/
	private List<BeanClaveMedio> listaCveMedio;
	
	/**Propiedad que contendra los select de los combos cuando se cree una topologia*/
	private List<BeanClaveOperacion> listaCveOperacion;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**Propiedad que contendra las propiedades del paginador en la tabla*/
	private BeanPaginador beanPaginador;
    
	/**
	 * Importe total sin considerar la paginacion
	 */
	private String importeTotal;
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaTopologias
	 * @return listaTopologias Objeto del tipo @see List
	 */
	public List<BeanTopologia> getListaTopologias() {
		return listaTopologias;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaTopologias
	 * @param listaTopologias Objeto del tipo @see List
	 */
	public void setListaTopologias(List<BeanTopologia> listaTopologias) {
		this.listaTopologias = listaTopologias;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaCveMedio
	 * @return listaCveMedio Objeto del tipo @see List
	 */
	public List<BeanClaveMedio> getListaCveMedio() {
		return listaCveMedio;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaCveMedio
	 * @param listaCveMedio Objeto del tipo @see List
	 */
	public void setListaCveMedio(List<BeanClaveMedio> listaCveMedio) {
		this.listaCveMedio = listaCveMedio;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad claveMedio
	 * @return claveMedio Objeto del tipo @see BeanClaveMedio
	 */
	public List<BeanClaveOperacion> getListaCveOperacion() {
		return listaCveOperacion;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaCveOperacion
	 * @param listaCveOperacion Objeto del tipo @see List
	 */
	public void setListaCveOperacion(List<BeanClaveOperacion> listaCveOperacion) {
		this.listaCveOperacion = listaCveOperacion;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaCveOperacion
	 * @return listaCveOperacion Objeto del tipo @see List
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg Objeto del tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad codError
	 * @return codError Objeto del tipo @see Integer
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad codError
	 * @param codError Objeto del tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad msgError
	 * @return msgError Objeto del tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad msgError
	 * @param msgError Objeto del tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad tipoError
	 * @return tipoError Objeto del tipo @see String
	 */
	public String getTipoError() {
		return tipoError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoError
	 * @param tipoError Objeto del tipo @see String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad beanPaginador
	 * @return beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * 
	 * @return String
	 */
	public String getImporteTotal() {
		return importeTotal;
	}

	/**
	 * 
	 * @param importeTotal String
	 */
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	
	
}
