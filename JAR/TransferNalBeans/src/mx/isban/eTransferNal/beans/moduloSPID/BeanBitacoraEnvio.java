/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanBitacoraEnvio.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase que encapsula los datos que se mostraran en la Bitácora de Envío
 **/
public class BeanBitacoraEnvio implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 3171786175177117663L;
	
	/** Propiedad del tipo Long que almacena el folio de la solicitud */
	private Long folioSolicitud;
	/** Propiedad del tipo String que almacena el tipo de mensaje */
	private String tipoDeMensaje;
	/** Propiedad del tipo String que almacena la fecha y la hora de envío */
	private String fechaHoraEnvio;
	/** Propiedad del tipo String que almacena el número de pagos */
	private String numeroPagos;
	/** Propiedad del tipo String que almacena el importe de abono */
	private String importeAbono;
	/** Propiedad del tipo String que almacena el código del banco */
	private String codBanco;
	/** Propiedad del tipo String que almacena el número de reprocesos */
	private String numReprocesos;
	/** Propiedad del tipo String que almacena el acuse de recibo */
	private String acuseRecibo;
	/** Propiedad del tipo String que almacena los bytes acumulados */
	private String bytesAcumulados;
	/** Propiedad del tipo String que almacena la operación de certificación */
	private String operacionCert;
	/** Propiedad del tipo String que almacena el número de certificado */
	private String numCertificado;
	/** Propiedad del tipo BeanPaginador que contiene las funciones de paginación */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg;

	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioSolicitud
	 * 
	 * @return folioSolicitud Objeto del tipo Long
	 */
	public Long getFolioSolicitud() {
		return folioSolicitud;
	}
	/**
	 *Modifica el valor de la propiedad folioSolicitud
	 * 
	 * @param folioSolicitud
	 *            Objeto del tipo Long
	 */
	public void setFolioSolicitud(long folioSolicitud) {
		this.folioSolicitud = folioSolicitud;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoDeMensaje
	 * 
	 * @return tipoDeMensaje Objeto del tipo String
	 */
	public String getTipoDeMensaje() {
		return tipoDeMensaje;
	}
	/**
	 *Modifica el valor de la propiedad tipoMensaje
	 * 
	 * @param tipoDeMensaje
	 *            Objeto del tipo String
	 */
	public void setTipoDeMensaje(String tipoDeMensaje) {
		this.tipoDeMensaje = tipoDeMensaje;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaHoraEnvio
	 * 
	 * @return fechaHoraEnvio Objeto del tipo String
	 */
	public String getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}
	/**
	 *Modifica el valor de la propiedad fechaHoraEnvio
	 * 
	 * @param fechaHoraEnvio
	 *            Objeto del tipo String
	 */
	public void setFechaHoraEnvio(String fechaHoraEnvio) {
		this.fechaHoraEnvio = fechaHoraEnvio;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numeroPagos
	 * 
	 * @return numeroPagos Objeto del tipo String
	 */
	public String getNumeroPagos() {
		return numeroPagos;
	}
	/**
	 *Modifica el valor de la propiedad numeroPagos
	 * 
	 * @param numeroPagos
	 *            Objeto del tipo String
	 */
	public void setNumeroPagos(String numeroPagos) {
		this.numeroPagos = numeroPagos;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeAbono
	 * 
	 * @return importeAbono Objeto del tipo String
	 */
	public String getImporteAbono() {
		return importeAbono;
	}
	/**
	 *Modifica el valor de la propiedad importeAbono
	 * 
	 * @param importeAbono
	 *            Objeto del tipo String
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codBanco
	 * 
	 * @return codBanco Objeto del tipo String
	 */
	public String getCodBanco() {
		return codBanco;
	}
	/**
	 *Modifica el valor de la propiedad codBanco
	 * 
	 * @param codBanco
	 *            Objeto del tipo String
	 */
	public void setCodBanco(String codBanco) {
		this.codBanco = codBanco;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numReprocesos
	 * 
	 * @return numReprocesos Objeto del tipo String
	 */
	public String getNumReprocesos() {
		return numReprocesos;
	}
	/**
	 *Modifica el valor de la propiedad numReprocesos
	 * 
	 * @param numReprocesos
	 *            Objeto del tipo String
	 */
	public void setNumReprocesos(String numReprocesos) {
		this.numReprocesos = numReprocesos;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad acuseRecibo
	 * 
	 * @return acuseRecibo Objeto del tipo String
	 */
	public String getAcuseRecibo() {
		return acuseRecibo;
	}
	/**
	 *Modifica el valor de la propiedad acuseRecibo
	 * 
	 * @param acuseRecibo
	 *            Objeto del tipo String
	 */
	public void setAcuseRecibo(String acuseRecibo) {
		this.acuseRecibo = acuseRecibo;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad bytesAcumulados
	 * 
	 * @return bytesAcumulados Objeto del tipo String
	 */
	public String getBytesAcumulados() {
		return bytesAcumulados;
	}
	/**
	 *Modifica el valor de la propiedad bytesAcumulados
	 * 
	 * @param bytesAcumulados
	 *            Objeto del tipo String
	 */
	public void setBytesAcumulados(String bytesAcumulados) {
		this.bytesAcumulados = bytesAcumulados;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad operacionCert
	 * 
	 * @return operacionCert Objeto del tipo String
	 */
	public String getOperacionCert() {
		return operacionCert;
	}
	/**
	 *Modifica el valor de la propiedad operacionCert
	 * 
	 * @param operacionCert
	 *            Objeto del tipo String
	 */
	public void setOperacionCert(String operacionCert) {
		this.operacionCert = operacionCert;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numCertificado
	 * 
	 * @return numCertificado Objeto del tipo String
	 */
	public String getNumCertificado() {
		return numCertificado;
	}
	/**
	 *Modifica el valor de la propiedad numCertificado
	 * 
	 * @param numCertificado
	 *            Objeto del tipo String
	 */
	public void setNumCertificado(String numCertificado) {
		this.numCertificado = numCertificado;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
}
