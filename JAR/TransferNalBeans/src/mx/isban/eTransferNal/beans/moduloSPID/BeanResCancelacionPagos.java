/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * Cancelacion Pagos
 **/
public class BeanResCancelacionPagos  implements BeanResultBO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6814944304889673763L;

	/**
	 * 
	 */
	

	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros  en pagina*/
	private Integer totalReg = 0;
	/** Propiedad de tipo Integer que almacena el total de registros del rango completo */
	private String importeTotal = "";
	

	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/**
	 * Propiedad del tipo List<BeanBitacoraEnvio> que almacena el listado de
	 * registros de la Bitácora de Envío
	 */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo List<BeanCancelacionPagos> que almacena el listado de
	 * registros de cancelacion pagos
	 */
	private List<BeanCancelacionPagos> listaCancelacionPagos;
	
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad nombre de archivo
	    * @return nombreArchivo Objeto del tipo String
	    */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	    *Metodo set que sirve para modificar el valor de la propiedad nombre de archivo
	    * @param nombreArchivo Objeto del tipo String
	    */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

    /**
     * Metodo get que sirve para obtener el valor del importe Total
     * @return importeTotal Objeto de tipo Integer
     */
	public String getImporteTotal() {
		return importeTotal;
	}

	/**
	 * Modifica el valor de la propiedad ImporteTotal
	 * @param importeTotal
	 * 			objeto de tipo entero
	 */
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaCancelacionPagos
	 * 
	 * @return listaCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	 */
	public List<BeanCancelacionPagos> getListaCancelacionPagos() {
		return listaCancelacionPagos;
	}

	/**
	 *Modifica el valor de la propiedad listaCancelacionPagos
	 * 
	 * @param listaCancelacionPagos
	 *            Objeto del tipo List<BeanCancelacionPagos>
	 */
	public void setListaCancelacionPagos(List<BeanCancelacionPagos> listaCancelacionPagos) {
		this.listaCancelacionPagos = listaCancelacionPagos;
	}

}
