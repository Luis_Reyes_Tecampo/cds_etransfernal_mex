/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqMonitorBancos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se utilizan en monitorbancos
 **/
public class BeanReqMonitorBancos implements Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -6390546768458242720L;
	
	/** Propiedad del tipo BeanPaginador que almacena el paginador */
	private BeanPaginador paginador;
	/** Lista del tipo BeanMonitorBancos que almacena los bancos */
	private List<BeanMonitorBancos> listaMonitorBancos = null;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg;
	/**
	 * @return el paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * @param paginador el paginador a establecer
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * @return la listaMonitorBancos
	 */
	public List<BeanMonitorBancos> getListaMonitorBancos() {
		return listaMonitorBancos;
	}
	/**
	 * @param listaMonitorBancos la listaMonitorBancos a establecer
	 */
	public void setListaMonitorBancos(List<BeanMonitorBancos> listaMonitorBancos) {
		this.listaMonitorBancos = listaMonitorBancos;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

}
