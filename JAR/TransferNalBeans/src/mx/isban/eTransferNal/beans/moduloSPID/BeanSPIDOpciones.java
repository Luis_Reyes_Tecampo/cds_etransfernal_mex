package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDOpciones implements Serializable{
	

	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2445737350815251971L;
	/**
	 * Propiedad del tipo boolean que almacena el valor de seleccionado
	 */
	private boolean seleccionado;
	/**
	 * Propiedad del tipo boolean que almacena el valor de selecDevolucion
	 */
	private boolean selecDevolucion;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de selecRecuperar
	 */
	private boolean selecRecuperar;
	
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String servicio;

	/**
	 * Metodo get que sirve para obtener la propiedad seleccionado
	 * @return seleccionado Objeto del tipo boolean
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad seleccionado
	 * @param seleccionado del tipo boolean
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad selecDevolucion
	 * @return selecDevolucion Objeto del tipo boolean
	 */
	public boolean isSelecDevolucion() {
		return selecDevolucion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad selecDevolucion
	 * @param selecDevolucion del tipo boolean
	 */
	public void setSelecDevolucion(boolean selecDevolucion) {
		this.selecDevolucion = selecDevolucion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad selecRecuperar
	 * @return selecRecuperar Objeto del tipo boolean
	 */
	public boolean isSelecRecuperar() {
		return selecRecuperar;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad selecRecuperar
	 * @param selecRecuperar del tipo boolean
	 */
	public void setSelecRecuperar(boolean selecRecuperar) {
		this.selecRecuperar = selecRecuperar;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad opcion
	 * @return opcion Objeto del tipo String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad opcion
	 * @param opcion del tipo String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad servicio
	 * @return servicio Objeto del tipo String
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad servicio
	 * @param servicio del tipo String
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

}
