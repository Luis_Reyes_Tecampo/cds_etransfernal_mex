/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que muestra los datos que seren mostrados en la configuración de equivalencias
 **/
public class BeanEquivalencia implements Serializable{
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 4532658171685631429L;

	/** Propiedad del tipo String que almacena la clave de operación */
	private String claveOper;
	/** Propiedad del tipo String que almacena el tipo de pago */
	private String tipoPago;
	/** Propiedad del tipo String que almacena la clasificación */
	private String clasificacion;
	/** Propiedad del tipo String que almacena la descripción */
	private String descripcion;
	/** Propiedad del tipo String que almacena el horario */
	private String horario;
	/** Propiedad del tipo boolean que almacena si esta seleccionado o no */
	private boolean seleccionado;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveOper
	 * 
	 * @return claveOper Objeto del tipo String
	 */
	public String getClaveOper() {
		return claveOper;
	}
	/**
	 *Modifica el valor de la propiedad claveOper
	 * 
	 * @param claveOper
	 *            Objeto del tipo String
	 */
	public void setClaveOper(String claveOper) {
		this.claveOper = claveOper;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoPago
	 * 
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 *Modifica el valor de la propiedad tipoPago
	 * 
	 * @param tipoPago
	 *            Objeto del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad clasificacion
	 * 
	 * @return clasificacion Objeto del tipo String
	 */
	public String getClasificacion() {
		return clasificacion;
	}
	/**
	 *Modifica el valor de la propiedad clasificacion
	 * 
	 * @param clasificacion
	 *            Objeto del tipo String
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad descripcion
	 * 
	 * @return descripcion Objeto del tipo String
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 *Modifica el valor de la propiedad descripcion
	 * 
	 * @param descripcion
	 *            Objeto del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad horario
	 * 
	 * @return horario Objeto del tipo String
	 */
	public String getHorario() {
		return horario;
	}
	/**
	 *Modifica el valor de la propiedad horario
	 * 
	 * @param horario
	 *            Objeto del tipo String
	 */
	public void setHorario(String horario) {
		this.horario = horario;
	}

	/**
	 *Metodo is que sirve para obtener el valor de la propiedad seleccionado
	 * 
	 * @return seleccionado Objeto del tipo boolean
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	/**
	 *Modifica el valor de la propiedad seleccionado
	 * 
	 * @param seleccionado
	 *            Objeto del tipo boolean
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	 * 
	 * @return serialVersionUID Objeto del tipo long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
