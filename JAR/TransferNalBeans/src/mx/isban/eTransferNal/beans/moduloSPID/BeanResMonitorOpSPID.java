/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ResBeanMonitorOperativoSpid.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResMonitorOpSPID extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8524443029677605287L;
	
	/**
	 * Propiedad del tipo BeanOperLiquidadasSaldo que almacena el valor de operLiquidadasSaldo
	 */
	private BeanOperLiquidadasSaldo operLiquidadasSaldo;
	/**
	 * Propiedad del tipo BeanOperLiquidadasVolumen que almacena el valor de operLiquidadasVolumen
	 */
	private BeanOperLiquidadasVolumen operLiquidadasVolumen;

	/**
	 * Propiedad del tipo BeanOperNoConfirmadasSaldo que almacena el valor de operNoConfirmadasSaldo
	 */
	private BeanOperNoConfirmadasSaldo operNoConfirmadasSaldo;
	/**
	 * Propiedad del tipo BeanOperNoConfirmadasVolumen que almacena el valor de operNoConfirmadasVolumen
	 */
	private BeanOperNoConfirmadasVolumen operNoConfirmadasVolumen;
	
	/**
	 * Propiedad del tipo saldos que almacena el valor de beanSaldos
	 */
	private BeanSaldos saldos;
	/**
	 * Propiedad del tipo BeanSaldosVolumen que almacena el valor de beanSaldosVolumen
	 */
	private BeanSaldosVolumen saldosVolumen;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;
	/**
	 * Metodo get que sirve para obtener la propiedad operLiquidadasSaldo
	 * @return operLiquidadasSaldo Objeto del tipo BeanOperLiquidadasSaldo
	 */
	public BeanOperLiquidadasSaldo getOperLiquidadasSaldo() {
		return operLiquidadasSaldo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad operLiquidadasSaldo
	 * @param operLiquidadasSaldo del tipo BeanOperLiquidadasSaldo
	 */
	public void setOperLiquidadasSaldo(BeanOperLiquidadasSaldo operLiquidadasSaldo) {
		this.operLiquidadasSaldo = operLiquidadasSaldo;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad operLiquidadasVolumen
	 * @return operLiquidadasVolumen Objeto del tipo BeanOperLiquidadasVolumen
	 */
	public BeanOperLiquidadasVolumen getOperLiquidadasVolumen() {
		return operLiquidadasVolumen;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad operLiquidadasVolumen
	 * @param operLiquidadasVolumen del tipo BeanOperLiquidadasVolumen
	 */
	public void setOperLiquidadasVolumen(
			BeanOperLiquidadasVolumen operLiquidadasVolumen) {
		this.operLiquidadasVolumen = operLiquidadasVolumen;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad operNoConfirmadasSaldo
	 * @return operNoConfirmadasSaldo Objeto del tipo BeanOperNoConfirmadasSaldo
	 */
	public BeanOperNoConfirmadasSaldo getOperNoConfirmadasSaldo() {
		return operNoConfirmadasSaldo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad operNoConfirmadasSaldo
	 * @param operNoConfirmadasSaldo del tipo BeanOperNoConfirmadasSaldo
	 */
	public void setOperNoConfirmadasSaldo(
			BeanOperNoConfirmadasSaldo operNoConfirmadasSaldo) {
		this.operNoConfirmadasSaldo = operNoConfirmadasSaldo;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad operNoConfirmadasVolumen
	 * @return operNoConfirmadasVolumen Objeto del tipo BeanOperNoConfirmadasVolumen
	 */
	public BeanOperNoConfirmadasVolumen getOperNoConfirmadasVolumen() {
		return operNoConfirmadasVolumen;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad operNoConfirmadasVolumen
	 * @param operNoConfirmadasVolumen del tipo BeanOperNoConfirmadasVolumen
	 */
	public void setOperNoConfirmadasVolumen(
			BeanOperNoConfirmadasVolumen operNoConfirmadasVolumen) {
		this.operNoConfirmadasVolumen = operNoConfirmadasVolumen;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad saldos
	 * @return saldos Objeto del tipo BeanSaldos
	 */
	public BeanSaldos getSaldos() {
		return saldos;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldos
	 * @param saldos del tipo BeanSaldos
	 */
	public void setSaldos(BeanSaldos saldos) {
		this.saldos = saldos;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad saldosVolumen
	 * @return saldosVolumen Objeto del tipo BeanSaldosVolumen
	 */
	public BeanSaldosVolumen getSaldosVolumen() {
		return saldosVolumen;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldosVolumen
	 * @param saldosVolumen del tipo BeanSaldosVolumen
	 */
	public void setSaldosVolumen(BeanSaldosVolumen saldosVolumen) {
		this.saldosVolumen = saldosVolumen;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchOperacion
	 * @return fchOperacion Objeto del tipo String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchOperacion
	 * @param fchOperacion del tipo String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveMiInstitucion
	 * @return cveMiInstitucion Objeto del tipo String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion del tipo String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	
	

}
