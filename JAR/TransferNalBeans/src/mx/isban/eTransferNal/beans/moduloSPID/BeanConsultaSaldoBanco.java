/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsultaSaldoBanco.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase que encapsula los datos que se mostraran en la Consulta de Saldos por Banco
 **/
public class BeanConsultaSaldoBanco implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 6760879725178040698L;
	
	/** Propiedad del tipo String que almacena la clave del banco */
	private String claveBanco;
	/** Propiedad del tipo String que almacena el nombre del banco */
	private String nombreBanco;
	/** Propiedad del tipo Integer que almacena el volumen de operaciones enviadas */
	private Integer volumenOperacionesEnviadas;
	/** Propiedad del tipo BigDecimal que almacena el importe de operaciones enviadas */
	private BigDecimal importeOperacionesEnviadas;
	/** Propiedad del tipo Integer que almacena el volumen de operaciones recibidas */
	private Integer volumenOperacionesRecibidas;
	/** Propiedad del tipo BigDecimal que almacena el importe de operaciones recibidas */
	private BigDecimal importeOperacionesRecibidas;
	/** Propiedad del tipo Long que almacena el saldo */
	private String saldo;
	/** Propiedad del tipo BeanPaginador que contiene las funciones de paginación */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg;
	/** Propiedad del tipo String que almacena el usuario */
	private String paramUsuario;
	/** Propiedad del tipo String que almacena el servicio */
	private String paramServicio;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveBanco
	 * 
	 * @return claveBanco Objeto del tipo String
	 */
	public String getClaveBanco() {
		return claveBanco;
	}
	/**
	 *Modifica el valor de la propiedad claveBanco
	 * 
	 * @param claveBanco
	 *            Objeto del tipo String
	 */
	public void setClaveBanco(String claveBanco) {
		this.claveBanco = claveBanco;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nombreBanco
	 * 
	 * @return nombreBanco Objeto del tipo String
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}
	/**
	 *Modifica el valor de la propiedad nombreBanco
	 * 
	 * @param nombreBanco
	 *            Objeto del tipo String
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad volumenOperacionesEnviadas
	 * 
	 * @return volumenOperacionesEnviadas Objeto del tipo Integer
	 */
	public Integer getVolumenOperacionesEnviadas() {
		return volumenOperacionesEnviadas;
	}
	/**
	 *Modifica el valor de la propiedad volumenOperacionesEnviadas
	 * 
	 * @param volumenOperacionesEnviadas
	 *            Objeto del tipo Integer
	 */
	public void setVolumenOperacionesEnviadas(Integer volumenOperacionesEnviadas) {
		this.volumenOperacionesEnviadas = volumenOperacionesEnviadas;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeOperacionesEnviadas
	 * 
	 * @return importeOperacionesEnviadas Objeto del tipo BigDecimal
	 */
	public BigDecimal getImporteOperacionesEnviadas() {
		return importeOperacionesEnviadas;
	}
	/**
	 *Modifica el valor de la propiedad importeOperacionesEnviadas
	 * 
	 * @param importeOperacionesEnviadas
	 *            Objeto del tipo BigDecimal
	 */
	public void setImporteOperacionesEnviadas(BigDecimal importeOperacionesEnviadas) {
		this.importeOperacionesEnviadas = importeOperacionesEnviadas;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad volumenOperacionesRecibidas
	 * 
	 * @return volumenOperacionesRecibidas Objeto del tipo Integer
	 */
	public Integer getVolumenOperacionesRecibidas() {
		return volumenOperacionesRecibidas;
	}
	/**
	 *Modifica el valor de la propiedad volumenOperacionesRecibidas
	 * 
	 * @param volumenOperacionesRecibidas
	 *            Objeto del tipo Integer
	 */
	public void setVolumenOperacionesRecibidas(Integer volumenOperacionesRecibidas) {
		this.volumenOperacionesRecibidas = volumenOperacionesRecibidas;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeOperacionesRecibidas
	 * 
	 * @return importeOperacionesRecibidas Objeto del tipo BigDecimal
	 */
	public BigDecimal getImporteOperacionesRecibidas() {
		return importeOperacionesRecibidas;
	}
	/**
	 *Modifica el valor de la propiedad importeOperacionesRecibidas
	 * 
	 * @param importeOperacionesRecibidas
	 *            Objeto del tipo BigDecimal
	 */
	public void setImporteOperacionesRecibidas(BigDecimal importeOperacionesRecibidas) {
		this.importeOperacionesRecibidas = importeOperacionesRecibidas;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad saldo
	 * 
	 * @return saldo Objeto del tipo Long
	 */
	public String getSaldo() {
		return saldo;
	}
	/**
	 *Modifica el valor de la propiedad saldo
	 * 
	 * @param saldo
	 *            Objeto del tipo Long
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paramUsuario
	 * 
	 * @return paramUsuario Objeto del tipo String
	 */
	public String getParamUsuario() {
		return paramUsuario;
	}
	/**
	 *Modifica el valor de la propiedad paramUsuario
	 * 
	 * @param paramUsuario
	 *            Objeto del tipo String
	 */
	public void setParamUsuario(String paramUsuario) {
		this.paramUsuario = paramUsuario;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paramServicio
	 * 
	 * @return paramServicio Objeto del tipo String
	 */
	public String getParamServicio() {
		return paramServicio;
	}
	/**
	 *Modifica el valor de la propiedad paramServicio
	 * 
	 * @param paramServicio
	 *            Objeto del tipo String
	 */
	public void setParamServicio(String paramServicio) {
		this.paramServicio = paramServicio;
	}
	
	
}
