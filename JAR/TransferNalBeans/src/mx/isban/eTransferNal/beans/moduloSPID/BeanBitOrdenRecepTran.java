/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBitOrdenRecep.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/01/2019 11:30:14 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Class BeanBitOrdenRecep.
 * Bean que contiene los atributos de Transaccion para el grabado de Bitacora de una Orden de Recepcion
 *
 * @author FSW-Vector
 * @since 31/01/2019
 */
public class BeanBitOrdenRecepTran implements Serializable{
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2366512928389485232L;
	
	/** La variable que contiene informacion con respecto a: fch operacion. */
	private String fchOperacion;
	
	/** La variable que contiene informacion con respecto a: referencia. */
	private String referencia;
	
	/** La variable que contiene informacion con respecto a: num cuenta ord. */
	private String numCuentaOrd; 
	
	/** La variable que contiene informacion con respecto a: num cuenta rec. */
	private String numCuentaRec;
	
	/** La variable que contiene informacion con respecto a: monto. */
	private String monto;
	
	/**
	 * Obtener el objeto: fch operacion.
	 *
	 * @return El objeto: fch operacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}
	
	/**
	 * Definir el objeto: fch operacion.
	 *
	 * @param fchOperacion El nuevo objeto: fch operacion
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}
	
	/**
	 * Obtener el objeto: referencia.
	 *
	 * @return El objeto: referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	
	/**
	 * Definir el objeto: referencia.
	 *
	 * @param referencia El nuevo objeto: referencia
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	/**
	 * Obtener el objeto: num cuenta ord.
	 *
	 * @return El objeto: num cuenta ord
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}
	
	/**
	 * Definir el objeto: num cuenta ord.
	 *
	 * @param numCuentaOrd El nuevo objeto: num cuenta ord
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}
	
	/**
	 * Obtener el objeto: num cuenta rec.
	 *
	 * @return El objeto: num cuenta rec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}
	
	/**
	 * Definir el objeto: num cuenta rec.
	 *
	 * @param numCuentaRec El nuevo objeto: num cuenta rec
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	
	/**
	 * Obtener el objeto: monto.
	 *
	 * @return El objeto: monto
	 */
	public String getMonto() {
		return monto;
	}
	
	/**
	 * Definir el objeto: monto.
	 *
	 * @param monto El nuevo objeto: monto
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	
}

