/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSPIDEstatus.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDEstatus implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 7229377667496058035L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de estatusBanxico
	 */
	private String estatusBanxico;
	/**
	 * Propiedad del tipo String que almacena el valor de estatusTransfer
	 */
	private String estatusTransfer;
	
	/**
	 * Propiedad del tipo String que almacena el valor de codigoError
	 */
	private String codigoError;
	
	/**
	 * Metodo get que sirve para obtener la propiedad estatusBanxico
	 * @return estatusBanxico Objeto del tipo String
	 */
	public String getEstatusBanxico() {
		return estatusBanxico;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad estatusBanxico
	 * @param estatusBanxico del tipo String
	 */
	public void setEstatusBanxico(String estatusBanxico) {
		this.estatusBanxico = estatusBanxico;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad estatusTransfer
	 * @return estatusTransfer Objeto del tipo String
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad estatusTransfer
	 * @param estatusTransfer del tipo String
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad codigoError
	 * @return codigoError Objeto del tipo String
	 */
	public String getCodigoError() {
		return codigoError;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codigoError
	 * @param codigoError del tipo String
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	
}
