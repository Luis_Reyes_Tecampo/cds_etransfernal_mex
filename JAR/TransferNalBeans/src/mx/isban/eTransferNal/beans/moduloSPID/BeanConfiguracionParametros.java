/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfiguracionParametros.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en la configuración de parámetros
 **/
public class BeanConfiguracionParametros implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -5936622981267916710L;
	
	/** Propiedad del tipo long que almacena la cveCESIF */
	private long cveCESIF;
	/** Propiedad del tipo String que almacena el nomInstit */
	private String nomInstit;
	/** Propiedad del tipo String que almacena la ordPago */
	private String ordPago;
	/** Propiedad del tipo String que almacena las devoluciones */
	private String devoluciones;
	/** Propiedad del tipo String que almacena el trasSPIDSIAC */
	private String trasSPIDSIAC;
	/** Propiedad del tipo BeanConfParamFec que almacena el fec */
	private BeanConfParamFec fec;
	/** Propiedad del tipo BeanConfParamAra que almacena el ara */
	private BeanConfParamAra ara;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad ara
	 * 
	 * @return ara Objeto del tipo BeanConfParamAra
	 */
	public BeanConfParamAra getAra() {
		return ara;
	}
	/**
	 *Modifica el valor de la propiedad ara
	 * 
	 * @param ara
	 *            Objeto del tipo BeanConfParamAra
	 */
	public void setAra(BeanConfParamAra ara) {
		this.ara = ara;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fec
	 * 
	 * @return fec Objeto del tipo BeanConfParamFec
	 */
	public BeanConfParamFec getFec() {
		return fec;
	}
	/**
	 *Modifica el valor de la propiedad fec
	 * 
	 * @param fec
	 *            Objeto del tipo BeanConfParamFec
	 */
	public void setFec(BeanConfParamFec fec) {
		this.fec = fec;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveCESIF
	 * 
	 * @return cveCESIF Objeto del tipo Long
	 */
	public long getCveCESIF() {
		return cveCESIF;
	}
	/**
	 *Modifica el valor de la propiedad cveCESIF
	 * 
	 * @param cveCESIF
	 *            Objeto del tipo Long
	 */
	public void setCveCESIF(long cveCESIF) {
		this.cveCESIF = cveCESIF;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nomInstit
	 * 
	 * @return nomInstit Objeto del tipo String
	 */
	public String getNomInstit() {
		return nomInstit;
	}
	/**
	 *Modifica el valor de la propiedad nomInstit
	 * 
	 * @param nomInstit
	 *            Objeto del tipo String
	 */
	public void setNomInstit(String nomInstit) {
		this.nomInstit = nomInstit;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad ordPago
	 * 
	 * @return ordPago Objeto del tipo String
	 */
	public String getOrdPago() {
		return ordPago;
	}
	/**
	 *Modifica el valor de la propiedad ordPago
	 * 
	 * @param ordPago
	 *            Objeto del tipo String
	 */
	public void setOrdPago(String ordPago) {
		this.ordPago = ordPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad devoluciones
	 * 
	 * @return devoluciones Objeto del tipo String
	 */
	public String getDevoluciones() {
		return devoluciones;
	}
	/**
	 *Modifica el valor de la propiedad devoluciones
	 * 
	 * @param devoluciones
	 *            Objeto del tipo String
	 */
	public void setDevoluciones(String devoluciones) {
		this.devoluciones = devoluciones;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad trasSPIDSIAC
	 * 
	 * @return trasSPIDSIAC Objeto del tipo String
	 */
	public String getTrasSPIDSIAC() {
		return trasSPIDSIAC;
	}
	/**
	 *Modifica el valor de la propiedad trasSPIDSIAC
	 * 
	 * @param trasSPIDSIAC
	 *            Objeto del tipo String
	 */
	public void setTrasSPIDSIAC(String trasSPIDSIAC) {
		this.trasSPIDSIAC = trasSPIDSIAC;
	}

		
}
