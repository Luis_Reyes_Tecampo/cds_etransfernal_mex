package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDFchPagos implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5975790055224989458L;

	/**
	 * Propiedad del tipo String que almacena el valor de fchInstrucPago
	 */
	private String fchInstrucPago;
	
	/**
	 * Propiedad del tipo String que almacena el valor de horaInstrucPago
	 */
	private String horaInstrucPago;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fchAceptPago
	 */
	private String fchAceptPago;
	
	/**
	 * Propiedad del tipo String que almacena el valor de horaAceptPago
	 */
	private String horaAceptPago;

	/**
	 * Metodo get que sirve para obtener la propiedad fchInstrucPago
	 * @return fchInstrucPago Objeto del tipo String
	 */
	public String getFchInstrucPago() {
		return fchInstrucPago;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchInstrucPago
	 * @param fchInstrucPago del tipo String
	 */
	public void setFchInstrucPago(String fchInstrucPago) {
		this.fchInstrucPago = fchInstrucPago;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad horaInstrucPago
	 * @return horaInstrucPago Objeto del tipo String
	 */
	public String getHoraInstrucPago() {
		return horaInstrucPago;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad horaInstrucPago
	 * @param horaInstrucPago del tipo String
	 */
	public void setHoraInstrucPago(String horaInstrucPago) {
		this.horaInstrucPago = horaInstrucPago;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchAceptPago
	 * @return fchAceptPago Objeto del tipo String
	 */
	public String getFchAceptPago() {
		return fchAceptPago;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchAceptPago
	 * @param fchAceptPago del tipo String
	 */
	public void setFchAceptPago(String fchAceptPago) {
		this.fchAceptPago = fchAceptPago;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad horaAceptPago
	 * @return horaAceptPago Objeto del tipo String
	 */
	public String getHoraAceptPago() {
		return horaAceptPago;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad horaAceptPago
	 * @param horaAceptPago del tipo String
	 */
	public void setHoraAceptPago(String horaAceptPago) {
		this.horaAceptPago = horaAceptPago;
	}
	

}
