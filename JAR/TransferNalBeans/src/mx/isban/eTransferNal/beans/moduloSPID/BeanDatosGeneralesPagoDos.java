/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanDatosGeneralesPagoDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en el detalle de cada pago
 **/
public class BeanDatosGeneralesPagoDos implements Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1743124218447115235L;
	
	/** Propiedad del tipo String que almacena la referencia del mecanismo */
	private String refeMecanismo;
	/** Propiedad del tipo String que almacena la clave de devolución */
	private String claveDevolucion;
	/** Propiedad del tipo String que almacena el motivo de Devolución */
	private String motivoDevolucion;
	/** Propiedad del tipo String que almacena la fecha de instrucción pago */
	private String fechaInstruccionPago;
	/** Propiedad del tipo String que almacena la hora de instrucción pago */
	private String horaInstruccionPago;
	/** Propiedad del tipo String que almacena la referncia numérica */
	private String refeNumerica;
	/** Propiedad del tipo String que almacena el folio de paquete */
	private String folioPaquete;
	/** Propiedad del tipo String que almacena el folio de pago */
	private String folioPago;
	/** Propiedad del tipo String que almacena el envConf */
	private String envConf;
	/** Propiedad del tipo String que almacena la clave de rastreo */
	private String claveRastreo;
	/** Propiedad del tipo String que almacena la dirección IP */
	private String direccionIP;
	/** Propiedad del tipo String que almacena el estatus */
	private String estatus;
	
	
	
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad refeMecanismo
	 * 
	 * @return refeMecanismo Objeto del tipo String
	 */
	public String getRefeMecanismo() {
		return refeMecanismo;
	}
	/**
	 *Modifica el valor de la propiedad refeMecanismo
	 * 
	 * @param refeMecanismo
	 *            Objeto del tipo String
	 */
	public void setRefeMecanismo(String refeMecanismo) {
		this.refeMecanismo = refeMecanismo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveDevolucion
	 * 
	 * @return claveDevolucion Objeto del tipo String
	 */
	public String getClaveDevolucion() {
		return claveDevolucion;
	}
	/**
	 *Modifica el valor de la propiedad claveDevolucion
	 * 
	 * @param claveDevolucion
	 *            Objeto del tipo String
	 */
	public void setClaveDevolucion(String claveDevolucion) {
		this.claveDevolucion = claveDevolucion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad motivoDevolucion
	 * 
	 * @return motivoDevolucion Objeto del tipo String
	 */
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}
	/**
	 *Modifica el valor de la propiedad motivoDevolucion
	 * 
	 * @param motivoDevolucion
	 *            Objeto del tipo String
	 */
	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaInstruccionPago
	 * 
	 * @return fechaInstruccionPago Objeto del tipo String
	 */
	public String getFechaInstruccionPago() {
		return fechaInstruccionPago;
	}
	/**
	 *Modifica el valor de la propiedad fechaInstruccionPago
	 * 
	 * @param fechaInstruccionPago
	 *            Objeto del tipo String
	 */
	public void setFechaInstruccionPago(String fechaInstruccionPago) {
		this.fechaInstruccionPago = fechaInstruccionPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad horaInstruccionPago
	 * 
	 * @return horaInstruccionPago Objeto del tipo String
	 */
	public String getHoraInstruccionPago() {
		return horaInstruccionPago;
	}
	/**
	 *Modifica el valor de la propiedad horaInstruccionPago
	 * 
	 * @param horaInstruccionPago
	 *            Objeto del tipo String
	 */
	public void setHoraInstruccionPago(String horaInstruccionPago) {
		this.horaInstruccionPago = horaInstruccionPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad refeNumerica
	 * 
	 * @return refeNumerica Objeto del tipo String
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}
	/**
	 *Modifica el valor de la propiedad refeNumerica
	 * 
	 * @param refeNumerica
	 *            Objeto del tipo String
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioPaquete
	 * 
	 * @return folioPaquete Objeto del tipo String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 *Modifica el valor de la propiedad folioPaquete
	 * 
	 * @param folioPaquete
	 *            Objeto del tipo String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioPago
	 * 
	 * @return folioPago Objeto del tipo String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 *Modifica el valor de la propiedad folioPago
	 * 
	 * @param folioPago
	 *            Objeto del tipo String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad envConf
	 * 
	 * @return envConf Objeto del tipo String
	 */
	public String getEnvConf() {
		return envConf;
	}
	/**
	 *Modifica el valor de la propiedad envConf
	 * 
	 * @param envConf
	 *            Objeto del tipo String
	 */
	public void setEnvConf(String envConf) {
		this.envConf = envConf;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveRastreo
	 * 
	 * @return claveRastreo Objeto del tipo String
	 */
	public String getClaveRastreo() {
		return claveRastreo;
	}
	/**
	 *Modifica el valor de la propiedad claveRastreo
	 * 
	 * @param claveRastreo
	 *            Objeto del tipo String
	 */
	public void setClaveRastreo(String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad direccionIP
	 * 
	 * @return direccionIP Objeto del tipo String
	 */
	public String getDireccionIP() {
		return direccionIP;
	}
	/**
	 *Modifica el valor de la propiedad direccionIP
	 * 
	 * @param direccionIP
	 *            Objeto del tipo String
	 */
	public void setDireccionIP(String direccionIP) {
		this.direccionIP = direccionIP;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	 * 
	 * @return serialVersionUID Objeto del tipo String
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
