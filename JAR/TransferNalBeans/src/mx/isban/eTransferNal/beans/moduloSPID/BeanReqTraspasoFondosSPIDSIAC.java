package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;

public class BeanReqTraspasoFondosSPIDSIAC implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -618089089698325407L;
	/**
	 * Propiedad del tipo String que almacena el valor de importe
	 */
	private String importe;
	
	/**
	 * Propiedad del tipo String que almacena el valor de comentario
	 */
	private String comentario;

	

	/**
	 * Metodo get que sirve para obtener la propiedad importe
	 * @return importe Objeto del tipo String
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad importe
	 * @param importe del tipo String
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad comentario
	 * @return comentario Objeto del tipo String
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad comentario
	 * @param comentario del tipo String
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	
}
