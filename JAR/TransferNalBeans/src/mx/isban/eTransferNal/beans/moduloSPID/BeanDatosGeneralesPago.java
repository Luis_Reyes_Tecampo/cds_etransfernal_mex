/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanDatosGeneralesPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en el detalle de cada pago
 **/
public class BeanDatosGeneralesPago implements Serializable{
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -400161749978108778L;

	/** Propiedad del tipo String que almacena la referencia */
	private String referencia;
	/** Propiedad del tipo String que almacena la clave de transgferencia */
	private String claveTransferencia;
	/** Propiedad del tipo String que almacena la forma de liquidación */
	private String formaLiquidacion;
	/** Propiedad del tipo String que almacena la empresa */
	private String empresa;
	/** Propiedad del tipo String que almacena el punto de venta */
	private String puntoVenta;
	/** Propiedad del tipo String que almacena la clave de cola */
	private String cola;
	/** Propiedad del tipo String que almacena el medio de entrega */
	private String medioEntrega;
	/** Propiedad del tipo String que almacena la referencia medio */
	private String referenciaMedio;
	/** Propiedad del tipo String que almacena la clave de operación */
	private String claveOperacion;
	/** Propiedad del tipo String que almacena el usuario capturo */
	private String usuarioCapturo;
	/** Propiedad del tipo String que almacena la fecha captura */
    private String fechaCaptura;
    /** Propiedad del tipo String que almacena el usuario que solicito */
	private String usuarioSolicito;
	/** Propiedad del tipo String que almacena la refeConcent */
	private String refeConcent;
	
	/** Propiedad del tipo BeanDatosGeneralesPagoDos que almacena la beanDatosGeneralesPagoDos */
	private BeanDatosGeneralesPagoDos beanDatosGeneralesPagoDos;
	
	
	
	
	/**
	 * @return the beanDatosGeneralesPagoDos
	 */
	public BeanDatosGeneralesPagoDos getBeanDatosGeneralesPagoDos() {
		return beanDatosGeneralesPagoDos;
	}
	/**
	 * @param beanDatosGeneralesPagoDos the beanDatosGeneralesPagoDos to set
	 */
	public void setBeanDatosGeneralesPagoDos(BeanDatosGeneralesPagoDos beanDatosGeneralesPagoDos) {
		this.beanDatosGeneralesPagoDos = beanDatosGeneralesPagoDos;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad referencia
	 * 
	 * @return referencia Objeto del tipo String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 *Modifica el valor de la propiedad referencia
	 * 
	 * @param referencia
	 *            Objeto del tipo String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveTransferencia
	 * 
	 * @return claveTransferencia Objeto del tipo String
	 */
	public String getClaveTransferencia() {
		return claveTransferencia;
	}
	/**
	 *Modifica el valor de la propiedad claveTransferencia
	 * 
	 * @param claveTransparencia
	 *            Objeto del tipo String
	 */
	public void setClaveTransferencia(String claveTransparencia) {
		this.claveTransferencia = claveTransparencia;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad formaLiquidacion
	 * 
	 * @return formaLiquidacion Objeto del tipo String
	 */
	public String getFormaLiquidacion() {
		return formaLiquidacion;
	}
	/**
	 *Modifica el valor de la propiedad formaLiquidacion
	 * 
	 * @param formaLiquidacion
	 *            Objeto del tipo String
	 */
	public void setFormaLiquidacion(String formaLiquidacion) {
		this.formaLiquidacion = formaLiquidacion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad empresa
	 * 
	 * @return empresa Objeto del tipo String
	 */
	public String getEmpresa() {
		return empresa;
	}
	/**
	 *Modifica el valor de la propiedad empresa
	 * 
	 * @param empresa
	 *            Objeto del tipo String
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad puntoVenta
	 * 
	 * @return puntoVenta Objeto del tipo String
	 */
	public String getPuntoVenta() {
		return puntoVenta;
	}
	/**
	 *Modifica el valor de la propiedad puntoVenta
	 * 
	 * @param puntoVenta
	 *            Objeto del tipo String
	 */
	public void setPuntoVenta(String puntoVenta) {
		this.puntoVenta = puntoVenta;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cola
	 * 
	 * @return cola Objeto del tipo String
	 */
	public String getCola() {
		return cola;
	}
	/**
	 *Modifica el valor de la propiedad cola
	 * 
	 * @param cola
	 *            Objeto del tipo String
	 */
	public void setCola(String cola) {
		this.cola = cola;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad medioEntrega
	 * 
	 * @return medioEntrega Objeto del tipo String
	 */
	public String getMedioEntrega() {
		return medioEntrega;
	}
	/**
	 *Modifica el valor de la propiedad medioEntrega
	 * 
	 * @param medioEntrega
	 *            Objeto del tipo String
	 */
	public void setMedioEntrega(String medioEntrega) {
		this.medioEntrega = medioEntrega;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad referenciaMedio
	 * 
	 * @return referenciaMedio Objeto del tipo String
	 */
	public String getReferenciaMedio() {
		return referenciaMedio;
	}
	/**
	 *Modifica el valor de la propiedad referenciaMedio
	 * 
	 * @param referenciaMedio
	 *            Objeto del tipo String
	 */
	public void setReferenciaMedio(String referenciaMedio) {
		this.referenciaMedio = referenciaMedio;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveOperacion
	 * 
	 * @return claveOperacion Objeto del tipo String
	 */
	public String getClaveOperacion() {
		return claveOperacion;
	}
	/**
	 *Modifica el valor de la propiedad claveOperacion
	 * 
	 * @param claveOperacion
	 *            Objeto del tipo String
	 */
	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad usuarioCapturo
	 * 
	 * @return usuarioCapturo Objeto del tipo String
	 */
	public String getUsuarioCapturo() {
		return usuarioCapturo;
	}
	/**
	 *Modifica el valor de la propiedad usuarioCapturo
	 * 
	 * @param usuarioCapturo
	 *            Objeto del tipo String
	 */
	public void setUsuarioCapturo(String usuarioCapturo) {
		this.usuarioCapturo = usuarioCapturo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaCaptura
	 * 
	 * @return fechaCaptura Objeto del tipo String
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	/**
	 *Modifica el valor de la propiedad fechaCaptura
	 * 
	 * @param fechaCaptura
	 *            Objeto del tipo String
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad usuarioSolicito
	 * 
	 * @return usuarioSolicito Objeto del tipo String
	 */
	public String getUsuarioSolicito() {
		return usuarioSolicito;
	}
	/**
	 *Modifica el valor de la propiedad usuarioSolicito
	 * 
	 * @param usuarioSolicito
	 *            Objeto del tipo String
	 */
	public void setUsuarioSolicito(String usuarioSolicito) {
		this.usuarioSolicito = usuarioSolicito;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad refeConcent
	 * 
	 * @return refeConcent Objeto del tipo String
	 */
	public String getRefeConcent() {
		return refeConcent;
	}
	/**
	 *Modifica el valor de la propiedad refeConcent
	 * 
	 * @param refeConcent
	 *            Objeto del tipo String
	 */
	public void setRefeConcent(String refeConcent) {
		this.refeConcent = refeConcent;
	}

}
