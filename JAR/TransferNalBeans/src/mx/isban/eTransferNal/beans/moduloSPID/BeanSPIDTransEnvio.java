package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * @author cchong
 *
 */
public class BeanSPIDTransEnvio implements Serializable{
   	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1693577431574691228L;
	/**
	 * Propiedad del tipo BeanSPIDTransEnvioDatGen que almacena el valor de beanSPIDTransEnvioDatGen
	 */
	private BeanSPIDTransEnvioDatGen beanSPIDTransEnvioDatGen = null;
	/**
	 * Propiedad del tipo BeanSPIDTransEnvioDatGen2 que almacena el valor de beanSPIDTransEnvioDatGen2
	 */
	private BeanSPIDTransEnvioDatGen2 beanSPIDTransEnvioDatGen2 = null;
	/**
	 * Propiedad del tipo BeanSPIDTransEnvioOrd que almacena el valor de beanSPIDTransEnvioOrd
	 */
	private BeanSPIDTransEnvioOrd beanSPIDTransEnvioOrd = null;
	/**
	 * Propiedad del tipo BeanSPIDTransEnvioRec que almacena el valor de beanSPIDTransEnvioRec
	 */
	private BeanSPIDTransEnvioRec beanSPIDTransEnvioRec = null;
	/**
	 * Metodo get que sirve para obtener la propiedad beanSPIDTransEnvioDatGen
	 * @return beanSPIDTransEnvioDatGen Objeto del tipo BeanSPIDTransEnvioDatGen
	 */
	public BeanSPIDTransEnvioDatGen getBeanSPIDTransEnvioDatGen() {
		return beanSPIDTransEnvioDatGen;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad beanSPIDTransEnvioDatGen
	 * @param beanSPIDTransEnvioDatGen del tipo BeanSPIDTransEnvioDatGen
	 */
	public void setBeanSPIDTransEnvioDatGen(
			BeanSPIDTransEnvioDatGen beanSPIDTransEnvioDatGen) {
		this.beanSPIDTransEnvioDatGen = beanSPIDTransEnvioDatGen;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad beanSPIDTransEnvioDatGen2
	 * @return beanSPIDTransEnvioDatGen2 Objeto del tipo BeanSPIDTransEnvioDatGen2
	 */
	public BeanSPIDTransEnvioDatGen2 getBeanSPIDTransEnvioDatGen2() {
		return beanSPIDTransEnvioDatGen2;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad beanSPIDTransEnvioDatGen2
	 * @param beanSPIDTransEnvioDatGen2 del tipo BeanSPIDTransEnvioDatGen2
	 */
	public void setBeanSPIDTransEnvioDatGen2(
			BeanSPIDTransEnvioDatGen2 beanSPIDTransEnvioDatGen2) {
		this.beanSPIDTransEnvioDatGen2 = beanSPIDTransEnvioDatGen2;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad beanSPIDTransEnvioOrd
	 * @return beanSPIDTransEnvioOrd Objeto del tipo BeanSPIDTransEnvioOrd
	 */
	public BeanSPIDTransEnvioOrd getBeanSPIDTransEnvioOrd() {
		return beanSPIDTransEnvioOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad beanSPIDTransEnvioOrd
	 * @param beanSPIDTransEnvioOrd del tipo BeanSPIDTransEnvioOrd
	 */
	public void setBeanSPIDTransEnvioOrd(BeanSPIDTransEnvioOrd beanSPIDTransEnvioOrd) {
		this.beanSPIDTransEnvioOrd = beanSPIDTransEnvioOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad beanSPIDTransEnvioRec
	 * @return beanSPIDTransEnvioRec Objeto del tipo BeanSPIDTransEnvioRec
	 */
	public BeanSPIDTransEnvioRec getBeanSPIDTransEnvioRec() {
		return beanSPIDTransEnvioRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad beanSPIDTransEnvioRec
	 * @param beanSPIDTransEnvioRec del tipo BeanSPIDTransEnvioRec
	 */
	public void setBeanSPIDTransEnvioRec(BeanSPIDTransEnvioRec beanSPIDTransEnvioRec) {
		this.beanSPIDTransEnvioRec = beanSPIDTransEnvioRec;
	}
	
	

}
