/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanCfgTramaSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    10/02/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanCfgTramaSPID implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 603415562640103644L;


	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreCampo
	 */
	private String nombreCampo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCampo
	 */
	private String tipoCampo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tamano
	 */
	private int tamano;
	
	/**
	 * Propiedad del tipo String que almacena el valor de justificado
	 */
	private String justificado;
	
	/**
	 * Constructor 
	 * @param nombreCampo String con el nombre del campo
	 * @param tipoCampo String con el tipo del campo
	 * @param tamano int con la longitud 
	 * @param justificado String
	 */
	public BeanCfgTramaSPID(String nombreCampo,String tipoCampo,
			int tamano, String justificado){
		this.nombreCampo = nombreCampo;
		this.tipoCampo = tipoCampo;
		this.tamano = tamano; 
		this.justificado = justificado;
			
	}

	/**
	 * Metodo get que sirve para obtener la propiedad nombreCampo
	 * @return nombreCampo Objeto del tipo String
	 */
	public String getNombreCampo() {
		return nombreCampo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad nombreCampo
	 * @param nombreCampo del tipo String
	 */
	public void setNombreCampo(String nombreCampo) {
		this.nombreCampo = nombreCampo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoCampo
	 * @return tipoCampo Objeto del tipo String
	 */
	public String getTipoCampo() {
		return tipoCampo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCampo
	 * @param tipoCampo del tipo String
	 */
	public void setTipoCampo(String tipoCampo) {
		this.tipoCampo = tipoCampo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tamano
	 * @return tamano Objeto del tipo int
	 */
	public int getTamano() {
		return tamano;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tamano
	 * @param tamano del tipo int
	 */
	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad justificado
	 * @return justificado Objeto del tipo String
	 */
	public String getJustificado() {
		return justificado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad justificado
	 * @param justificado del tipo String
	 */
	public void setJustificado(String justificado) {
		this.justificado = justificado;
	}


	
}
