package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDTransEnvioDatGen implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 673962684028772494L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveEmpresa
	 */
	/**
	 * Propiedad del tipo String que almacena el valor de cveEmpresa
	 */
	private String cveEmpresa;
	/**
	 * Propiedad del tipo String que almacena el valor de cvePtoVta
	 */
	private String cvePtoVta;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMedioEnt
	 */
	private String cveMedioEnt;
	/**
	 * Propiedad del tipo String que almacena el valor de referenciaMed
	 */
	private String referenciaMed;
	/**
	 * Propiedad del tipo String que almacena el valor de cveUsuarioCap
	 */
	private String cveUsuarioCap;
	/**
	 * Propiedad del tipo String que almacena el valor de cveUsuarioSol
	 */
	private String cveUsuarioSol;
	/**
	 * Propiedad del tipo String que almacena el valor de cveTransfe
	 */
	private String cveTransfe;
	/**
	 * Propiedad del tipo String que almacena el valor de cveOperacion
	 */
	private String cveOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de refeNumerica
	 */
	private String refeNumerica;
	/**
	 * Propiedad del tipo String que almacena el valor de direccionIp
	 */
	private String direccionIp;
	/**
	 * Propiedad del tipo String que almacena el valor de fchInstrucPago
	 */
	private String fchInstrucPago;
	/**
	 * Propiedad del tipo String que almacena el valor de horaInstrucPago
	 */
	private String horaInstrucPago;
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreo;
	/**
	 * Metodo get que sirve para obtener la propiedad cveEmpresa
	 * @return cveEmpresa Objeto del tipo String
	 */
	public String getCveEmpresa() {
		return cveEmpresa;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveEmpresa
	 * @param cveEmpresa del tipo String
	 */
	public void setCveEmpresa(String cveEmpresa) {
		this.cveEmpresa = cveEmpresa;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cvePtoVta
	 * @return cvePtoVta Objeto del tipo String
	 */
	public String getCvePtoVta() {
		return cvePtoVta;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cvePtoVta
	 * @param cvePtoVta del tipo String
	 */
	public void setCvePtoVta(String cvePtoVta) {
		this.cvePtoVta = cvePtoVta;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveMedioEnt
	 * @return cveMedioEnt Objeto del tipo String
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveMedioEnt
	 * @param cveMedioEnt del tipo String
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad referenciaMed
	 * @return referenciaMed Objeto del tipo String
	 */
	public String getReferenciaMed() {
		return referenciaMed;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad referenciaMed
	 * @param referenciaMed del tipo String
	 */
	public void setReferenciaMed(String referenciaMed) {
		this.referenciaMed = referenciaMed;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveUsuarioCap
	 * @return cveUsuarioCap Objeto del tipo String
	 */
	public String getCveUsuarioCap() {
		return cveUsuarioCap;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveUsuarioCap
	 * @param cveUsuarioCap del tipo String
	 */
	public void setCveUsuarioCap(String cveUsuarioCap) {
		this.cveUsuarioCap = cveUsuarioCap;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveUsuarioSol
	 * @return cveUsuarioSol Objeto del tipo String
	 */
	public String getCveUsuarioSol() {
		return cveUsuarioSol;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveUsuarioSol
	 * @param cveUsuarioSol del tipo String
	 */
	public void setCveUsuarioSol(String cveUsuarioSol) {
		this.cveUsuarioSol = cveUsuarioSol;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveTransfe
	 * @return cveTransfe Objeto del tipo String
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveTransfe
	 * @param cveTransfe del tipo String
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveOperacion
	 * @return cveOperacion Objeto del tipo String
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveOperacion
	 * @param cveOperacion del tipo String
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad refeNumerica
	 * @return refeNumerica Objeto del tipo String
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad refeNumerica
	 * @param refeNumerica del tipo String
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad direccionIp
	 * @return direccionIp Objeto del tipo String
	 */
	public String getDireccionIp() {
		return direccionIp;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad direccionIp
	 * @param direccionIp del tipo String
	 */
	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchInstrucPago
	 * @return fchInstrucPago Objeto del tipo String
	 */
	public String getFchInstrucPago() {
		return fchInstrucPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchInstrucPago
	 * @param fchInstrucPago del tipo String
	 */
	public void setFchInstrucPago(String fchInstrucPago) {
		this.fchInstrucPago = fchInstrucPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad horaInstrucPago
	 * @return horaInstrucPago Objeto del tipo String
	 */
	public String getHoraInstrucPago() {
		return horaInstrucPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaInstrucPago
	 * @param horaInstrucPago del tipo String
	 */
	public void setHoraInstrucPago(String horaInstrucPago) {
		this.horaInstrucPago = horaInstrucPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveRastreo
	 * @return cveRastreo Objeto del tipo String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveRastreo
	 * @param cveRastreo del tipo String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}


}
