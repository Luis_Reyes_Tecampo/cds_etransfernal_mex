/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResMonitorBancos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para el
 * monitor de bancos
 **/
public class BeanResMonitorBancos implements BeanResultBO, Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 5274355864409461096L;
	
	/** Propiedad del tipo BeanPaginador que almacena el paginador */
	private BeanPaginador beanPaginador;
	
	/** Propiedad del tipo Integer que almacena el totalReg */
	private Integer totalReg = 0;
	/** Propiedad del String  que almacena el codError */
	private String codError;
	/** Propiedad del String  que almacena el msgError */
	private String msgError;
	/** Propiedad del String  que almacena el tipoError */
	private String tipoError;
	/** Propiedad del String  que almacena el nombreArchivo */
	private String nombreArchivo; 
	/** Lista del BeanMonitorBancos que almacena la listaMonitorBancos */
	private List<BeanMonitorBancos> listaMonitorBancos;

	/**
	 * @return el paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador el beanPaginador a establecer
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return el msgError
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError el tipoError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError la listaMonitorBancos a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * @return la listaMonitorBancos
	 */
	public List<BeanMonitorBancos> getListaMonitorBancos() {
		return listaMonitorBancos;
	}
	/**
	 * @param listaMonitorBancos el nombreArchivo a establecer
	 */
	public void setListaMonitorBancos(List<BeanMonitorBancos> listaMonitorBancos) {
		this.listaMonitorBancos = listaMonitorBancos;
	}
	/**
	 * @return el nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo el nombreArchivo a establecer
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


}
