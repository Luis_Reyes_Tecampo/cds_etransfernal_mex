/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanTipoPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que contiene los datos que se mostraran en el listado de pagos
 **/
public class BeanTipoPago implements Serializable{

	/**
	 * Constante privada del serial version
	 */
	private static final long serialVersionUID = -728202836641528245L;
	
	/** Propiedad del tipo int que almacena la clave del tipo de pago */
	private int claveTipoPago;
	/** Propiedad del tipo String que almacena el tipo de pago */
	private String tipoPago;
	
	
	/**
	 * M�todo que asigna los valores a los parametros
	 * @param claveTipoPago  Objeto del tipo String
	 * @param tipoPago       Objeto del tipo String
	 */
	public BeanTipoPago(int claveTipoPago, String tipoPago) {
		this.claveTipoPago = claveTipoPago;
		this.tipoPago = tipoPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveTipoPago
	 * 
	 * @return claveTipoPago Objeto del tipo int
	 */
	public int getClaveTipoPago() {
		return claveTipoPago;
	}
	/**
	 *Modifica el valor de la propiedad claveTipoPago
	 * 
	 * @param claveTipoPago
	 *            Objeto del tipo int
	 */
	public void setClaveTipoPago(int claveTipoPago) {
		this.claveTipoPago = claveTipoPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoPago
	 * 
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 *Modifica el valor de la propiedad tipoPago
	 * 
	 * @param tipoPago
	 *            Objeto del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
}
