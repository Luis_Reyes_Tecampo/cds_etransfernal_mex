/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSPIDLlave.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDLlave implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2942235116803095526L;
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstituc
	 */
	private String cveMiInstituc;
	/**
	 * Propiedad del tipo String que almacena el valor de cveInstOrd
	 */
	private String cveInstOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPaquete
	 */
	private String folioPaquete;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPago
	 */
	private String folioPago;
	/**
	 * Metodo get que sirve para obtener la propiedad fchOperacion
	 * @return fchOperacion Objeto del tipo String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchOperacion
	 * @param fchOperacion del tipo String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto del tipo String
	 */
	public String getCveMiInstituc() {
		return cveMiInstituc;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveMiInstituc
	 * @param cveMiInstituc del tipo String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		this.cveMiInstituc = cveMiInstituc;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveInstOrd
	 * @return cveInstOrd Objeto del tipo String
	 */
	public String getCveInstOrd() {
		return cveInstOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveInstOrd
	 * @param cveInstOrd del tipo String
	 */
	public void setCveInstOrd(String cveInstOrd) {
		this.cveInstOrd = cveInstOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPaquete
	 * @return folioPaquete Objeto del tipo String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPaquete
	 * @param folioPaquete del tipo String
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPago
	 * @return folioPago Objeto del tipo String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPago
	 * @param folioPago del tipo String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}

}
