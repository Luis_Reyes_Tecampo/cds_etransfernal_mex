/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase que encapsula los datos que se mostraran en la consulta de pagos
 **/
public class BeanPago implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 1917495041616379642L;
	
	/** Propiedad del tipo Long que almacena la referencia */
	private Long referencia;
	/** Propiedad del tipo String que almacena la cveCola */
	private String cveCola;
	/** Propiedad del tipo String que almacena la cveTran */
	private String cveTran;
	/** Propiedad del tipo String que almacena la cveMecanismo */
	private String cveMecanismo;
	/** Propiedad del tipo String que almacena la cveMedioEnt */
	private String cveMedioEnt;
	/** Propiedad del tipo String que almacena la fechaCaptura */
	private String fechaCaptura;
	/** Propiedad del tipo String que almacena la cveIntermeOrd */
	private String cveIntermeOrd;
	/** Propiedad del tipo String que almacena la cveIntermeRec */
	private String cveIntermeRec;
	/** Propiedad del tipo String que almacena el importeAbono */
	private String importeAbono;
	/** Propiedad del tipo String que almacena el estatus */
	private String estatus;
	/** Propiedad del tipo String que almacena el folioPaquete */
	private String folioPaquete;
	/** Propiedad del tipo String que almacena el foloPago */
	private String foloPago;
	/** Propiedad del tipo String que almacena el mensajeError */
	private String mensajeError;
	/** Propiedad del tipo BeanPaginador que almacena el beanPaginador */
	private BeanPaginador beanPaginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad referencia
	 * 
	 * @return referencia Objeto del tipo Long
	 */
	public Long getReferencia() {
		return referencia;
	}
	/**
	 *Modifica el valor de la propiedad referencia
	 * 
	 * @param referencia
	 *            Objeto del tipo Long
	 */
	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveCola
	 * 
	 * @return cveCola Objeto del tipo String
	 */
	public String getCveCola() {
		return cveCola;
	}
	/**
	 *Modifica el valor de la propiedad cveCola
	 * 
	 * @param cveCola
	 *            Objeto del tipo String
	 */
	public void setCveCola(String cveCola) {
		this.cveCola = cveCola;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveTran
	 * 
	 * @return cveTran Objeto del tipo String
	 */
	public String getCveTran() {
		return cveTran;
	}
	/**
	 *Modifica el valor de la propiedad cveTran
	 * 
	 * @param cveTran
	 *            Objeto del tipo String
	 */
	public void setCveTran(String cveTran) {
		this.cveTran = cveTran;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveMecanismo
	 * 
	 * @return cveMecanismo Objeto del tipo String
	 */
	public String getCveMecanismo() {
		return cveMecanismo;
	}
	/**
	 *Modifica el valor de la propiedad cveMecanismo
	 * 
	 * @param cveMecanismo
	 *            Objeto del tipo String
	 */
	public void setCveMecanismo(String cveMecanismo) {
		this.cveMecanismo = cveMecanismo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveMedioEnt
	 * 
	 * @return cveMedioEnt Objeto del tipo String
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	/**
	 *Modifica el valor de la propiedad cveMedioEnt
	 * 
	 * @param cveMedioEnt
	 *            Objeto del tipo String
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaCaptura
	 * 
	 * @return fechaCaptura Objeto del tipo String
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	/**
	 *Modifica el valor de la propiedad fechaCaptura
	 * 
	 * @param fechaCaptura
	 *            Objeto del tipo String
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveIntermeOrd
	 * 
	 * @return cveIntermeOrd Objeto del tipo String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 *Modifica el valor de la propiedad cveIntermeOrd
	 * 
	 * @param cveIntermeOrd
	 *            Objeto del tipo String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveIntermeRec
	 * 
	 * @return cveIntermeRec Objeto del tipo String
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}
	/**
	 *Modifica el valor de la propiedad cveIntermeRec
	 * 
	 * @param cveIntermeRec
	 *            Objeto del tipo String
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeAbono
	 * 
	 * @return importeAbono Objeto del tipo String
	 */
	public String getImporteAbono() {
		return importeAbono;
	}
	/**
	 *Modifica el valor de la propiedad importeAbono
	 * 
	 * @param importeAbono
	 *            Objeto del tipo String
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad estatus
	 * 
	 * @return estatus Objeto del tipo String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 *Modifica el valor de la propiedad estatus
	 * 
	 * @param estatus
	 *            Objeto del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folioPaquete
	 * 
	 * @return folioPaquete Objeto del tipo String
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 *Modifica el valor de la propiedad folioPaquete
	 * 
	 * @param string
	 *            Objeto del tipo String para folioPaquete
	 */
	public void setFolioPaquete(String string) {
		this.folioPaquete = string;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad foloPago
	 * 
	 * @return foloPago Objeto del tipo String
	 */
	public String getFoloPago() {
		return foloPago;
	}
	/**
	 *Modifica el valor de la propiedad foloPago
	 * 
	 * @param foloPago
	 *            Objeto del tipo String
	 */
	public void setFoloPago(String foloPago) {
		this.foloPago = foloPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad mensajeError
	 * 
	 * @return mensajeError Objeto del tipo String
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 *Modifica el valor de la propiedad mensajeError
	 * 
	 * @param mensajeError
	 *            Objeto del tipo String
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
}
