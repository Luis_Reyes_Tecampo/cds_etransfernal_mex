/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTranMensajeComp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   29/01/2019 01:50:59 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Class BeanTranMensajeComp.
 *
 * @author FSW-Vector
 * @since 29/01/2019
 */
public class BeanTranMensaje implements Serializable {

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2271336316047833101L;

	/** La variable que contiene informacion con respecto a: buc cliente. */
	private String bucCliente;
	
	/** La variable que contiene informacion con respecto a: aplicativo. */
	private String aplicativo;
	
	/** La variable que contiene informacion con respecto a: tipo id ord. */
	private String tipoIdOrd;
	
	/** La variable que contiene informacion con respecto a: tipo id rec. */
	private String tipoIdRec;
	
	/** La variable que contiene informacion con respecto a: id ord. */
	private String idOrd;
	
	/** La variable que contiene informacion con respecto a: id rec. */
	private String idRec;
	
	/** La variable que contiene informacion con respecto a: tipo id rec dos. */
	private String tipoIdRecDos;
	
	/** La variable que contiene informacion con respecto a: importe iva. */
	private Integer importeIva;
	
	/** La variable que contiene informacion con respecto a: num cta cve. */
	private String numCtaCve;
	
	/** La variable que contiene informacion con respecto a: dir ip gen. */	
	private BeanTranMensajeComp transMsjComp;
	
	/**
	 * Obtener el objeto: buc cliente.
	 *
	 * @return El objeto: buc cliente
	 */
	public String getBucCliente() {
		return bucCliente;
	}
	
	/**
	 * Definir el objeto: buc cliente.
	 *
	 * @param bucCliente El nuevo objeto: buc cliente
	 */
	public void setBucCliente(String bucCliente) {
		this.bucCliente = bucCliente;
	}
	
	/**
	 * Obtener el objeto: aplicativo.
	 *
	 * @return El objeto: aplicativo
	 */
	public String getAplicativo() {
		return aplicativo;
	}
	
	/**
	 * Definir el objeto: aplicativo.
	 *
	 * @param aplicativo El nuevo objeto: aplicativo
	 */
	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}
	
	/**
	 * Obtener el objeto: tipo id ord.
	 *
	 * @return El objeto: tipo id ord
	 */
	public String getTipoIdOrd() {
		return tipoIdOrd;
	}
	
	/**
	 * Definir el objeto: tipo id ord.
	 *
	 * @param tipoIdOrd El nuevo objeto: tipo id ord
	 */
	public void setTipoIdOrd(String tipoIdOrd) {
		this.tipoIdOrd = tipoIdOrd;
	}
	
	/**
	 * Obtener el objeto: tipo id rec.
	 *
	 * @return El objeto: tipo id rec
	 */
	public String getTipoIdRec() {
		return tipoIdRec;
	}
	
	/**
	 * Definir el objeto: tipo id rec.
	 *
	 * @param tipoIdRec El nuevo objeto: tipo id rec
	 */
	public void setTipoIdRec(String tipoIdRec) {
		this.tipoIdRec = tipoIdRec;
	}
	
	/**
	 * Obtener el objeto: id ord.
	 *
	 * @return El objeto: id ord
	 */
	public String getIdOrd() {
		return idOrd;
	}
	
	/**
	 * Definir el objeto: id ord.
	 *
	 * @param idOrd El nuevo objeto: id ord
	 */
	public void setIdOrd(String idOrd) {
		this.idOrd = idOrd;
	}
	
	/**
	 * Obtener el objeto: id rec.
	 *
	 * @return El objeto: id rec
	 */
	public String getIdRec() {
		return idRec;
	}
	
	/**
	 * Definir el objeto: id rec.
	 *
	 * @param idRec El nuevo objeto: id rec
	 */
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	
	/**
	 * Obtener el objeto: tipo id rec dos.
	 *
	 * @return El objeto: tipo id rec dos
	 */
	public String getTipoIdRecDos() {
		return tipoIdRecDos;
	}
	
	/**
	 * Definir el objeto: tipo id rec dos.
	 *
	 * @param tipoIdRecDos El nuevo objeto: tipo id rec dos
	 */
	public void setTipoIdRecDos(String tipoIdRecDos) {
		this.tipoIdRecDos = tipoIdRecDos;
	}
	
	/**
	 * Obtener el objeto: importe iva.
	 *
	 * @return El objeto: importe iva
	 */
	public Integer getImporteIva() {
		return importeIva;
	}
	
	/**
	 * Definir el objeto: importe iva.
	 *
	 * @param importeIva El nuevo objeto: importe iva
	 */
	public void setImporteIva(Integer importeIva) {
		this.importeIva = importeIva;
	}
	
	/**
	 * Obtener el objeto: num cta cve.
	 *
	 * @return El objeto: num cta cve
	 */
	public String getNumCtaCve() {
		return numCtaCve;
	}
	
	/**
	 * Definir el objeto: num cta cve.
	 *
	 * @param numCtaCve El nuevo objeto: num cta cve
	 */
	public void setNumCtaCve(String numCtaCve) {
		this.numCtaCve = numCtaCve;
	}

	/**
	 * Obtener el objeto: trans msj comp.
	 *
	 * @return El objeto: trans msj comp
	 */
	public BeanTranMensajeComp getTransMsjComp() {
		return transMsjComp;
	}

	/**
	 * Definir el objeto: trans msj comp.
	 *
	 * @param transMsjComp El nuevo objeto: trans msj comp
	 */
	public void setTransMsjComp(BeanTranMensajeComp transMsjComp) {
		this.transMsjComp = transMsjComp;
	}
	
	
}
