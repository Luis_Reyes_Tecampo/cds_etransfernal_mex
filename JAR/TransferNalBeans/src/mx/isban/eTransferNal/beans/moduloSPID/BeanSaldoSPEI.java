/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanSaldoSPEI.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/01/2019 11:55:47 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class BeanSaldoSPEI.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
public class BeanSaldoSPEI implements Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3978084451153317007L;
	
	/** La variable que contiene informacion con respecto a: saldo T. */
	private String saldoT;
	
	/** La variable que contiene informacion con respecto a: cuadre baxico. */
	private Date cuadreBaxico;
	
	/** La variable que contiene informacion con respecto a: nom pantalla. */
	private String nomPantalla;

	/**
	 * Obtener el objeto: saldo T.
	 *
	 * @return El objeto: saldo T
	 */
	public String getSaldoT() {
		return saldoT;
	}

	/**
	 * Definir el objeto: saldo T.
	 *
	 * @param saldoT El nuevo objeto: saldo T
	 */
	public void setSaldoT(String saldoT) {
		this.saldoT = saldoT;
	}

	/**
	 * Obtener el objeto: cuadre baxico.
	 *
	 * @return El objeto: cuadre baxico
	 */
	public Date getCuadreBaxico() {
		Date aux = new Date();
		aux = cuadreBaxico;
		return aux;
	}

	/**
	 * Definir el objeto: cuadre baxico.
	 *
	 * @param cuadreBaxico El nuevo objeto: cuadre baxico
	 */
	public void setCuadreBaxico(Date cuadreBaxico) {
		Date aux = new Date();
		aux = cuadreBaxico;
		this.cuadreBaxico = aux;
	}

	/**
	 * Obtener el objeto: nom pantalla.
	 *
	 * @return El objeto: nom pantalla
	 */
	public String getNomPantalla() {
		return nomPantalla;
	}

	/**
	 * Definir el objeto: nom pantalla.
	 *
	 * @param nomPantalla El nuevo objeto: nom pantalla
	 */
	public void setNomPantalla(String nomPantalla) {
		this.nomPantalla = nomPantalla;
	} 
	
	
}
