/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanCancelacionPagosDos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 * Clase la cual se encapsulan los datos de la cancelación de pagos
 * @author OPENROAD
 *
 */
public class BeanCancelacionPagosDos implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7849566207876741354L;
	/**
	 * 
	 */
	
	/** Propiedad del tipo Strinbg que almacena la referencia */
	private String referencia;
	/** Propiedad del tipo Strinbg que almacena la clave de cola */
	private String cola;
	/** Propiedad del tipo Strinbg que almacena la clave de transferencia */
	private String cvnTran;
	/** Propiedad del tipo Strinbg que almacena la la cave de mecanismo */
	private String mecan;
	/** Propiedad del tipo Strinbg que almacena el medio de entrada */
	private String medioEnt;
	/** Propiedad del tipo Strinbg que almacena la clave de orden */
	private String ord;
	/** Propiedad del tipo Strinbg que almacena la clave rec */
	private String rec;
	/** Propiedad del tipo Strinbg que almacenael imprte abono */
	private String importeAbono;
	/** Propiedad del tipo Strinbg que almacena la clave paquete */
	private String paq;
	/** Propiedad del tipo Strinbg que almacena el folio de pago */
	private String pago;
	/** Propiedad del tipo Strinbg que almacena el estatus */
	private String est;
	/** Propiedad del tipo Strinbg que almacena la topologia */
	private String topo;
	/** Propiedad del tipo Strinbg que almacena la prioridad */
	private String pri;
	
	


	/**
	 *Metodo get que sirve para obtener el valor de la referencia
	 * 
	 * @return referencia Objeto del tipo String
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 *Modifica el valor de la propiedad referencia
	 * 
	 * @param referencia Objeto del tipo String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	



	/**
	 *Metodo get que sirve para obtener el valor de cola
	 * 
	 * @return cola Objeto del tipo String
	 */
	public String getCola() {
		return cola;
	}

	/**
	 *Modifica el valor de la propiedad cola
	 * 
	 * @param cola Objeto del tipo String
	 */
	public void setCola(String cola) {
		this.cola = cola;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de cvnTran
	 * 
	 * @return cvnTran Objeto del tipo String
	 */
	public String getCvnTran() {
		return cvnTran;
	}

	/**
	 *Modifica el valor de la propiedad cvnTran
	 * 
	 * @param cvnTran Objeto del tipo String
	 */
	public void setCvnTran(String cvnTran) {
		this.cvnTran = cvnTran;
	}

	/**
	 *Metodo get que sirve para obtener el valor de mecan
	 * 
	 * @return mecan Objeto del tipo String
	 */
	public String getMecan() {
		return mecan;
	}
	/**
	 *Modifica el valor de la propiedad mecan
	 * 
	 * @param mecan Objeto del tipo String
	 */
	public void setMecan(String mecan) {
		this.mecan = mecan;
	}

	/**
	 *Metodo get que sirve para obtener el valor de medioEnt
	 * 
	 * @return medioEnt Objeto del tipo String
	 */
	public String getMedioEnt() {
		return medioEnt;
	}

	/**
	 *Modifica el valor de la propiedad medioEnt
	 * 
	 * @param medioEnt Objeto del tipo String
	 */
	public void setMedioEnt(String medioEnt) {
		this.medioEnt = medioEnt;
	}

	/**
	 *Metodo get que sirve para obtener el valor de ord
	 * 
	 * @return ord Objeto del tipo String
	 */
	public String getOrd() {
		return ord;
	}
	
	/**
	 *Modifica el valor de la propiedad ord
	 * 
	 * @param ord Objeto del tipo String
	 */
	public void setOrd(String ord) {
		this.ord = ord;
	}

	/**
	 *Metodo get que sirve para obtener el valor de rec
	 * 
	 * @return rec Objeto del tipo String
	 */
	public String getRec() {
		return rec;
	}

	/**
	 *Modifica el valor de la propiedad rec
	 * 
	 * @param rec Objeto del tipo String
	 */
	public void setRec(String rec) {
		this.rec = rec;
	}

	/**
	 *Metodo get que sirve para obtener el valor de importeAbono
	 * 
	 * @return importeAbono Objeto del tipo String
	 */
	public String getImporteAbono() {
		return importeAbono;
	}

	/**
	 *Modifica el valor de la propiedad importeAbono
	 * 
	 * @param importeAbono Objeto del tipo String
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}

	/**
	 *Metodo get que sirve para obtener el valor de paq
	 * 
	 * @return paq Objeto del tipo String
	 */
	public String getPaq() {
		return paq;
	}

	/**
	 *Modifica el valor de la propiedad paq
	 * 
	 * @param paq Objeto del tipo String
	 */
	public void setPaq(String paq) {
		this.paq = paq;
	}

	/**
	 *Metodo get que sirve para obtener el valor de pago
	 * 
	 * @return pago Objeto del tipo String
	 */
	public String getPago() {
		return pago;
	}

	/**
	 *Modifica el valor de la propiedad pago
	 * 
	 * @param pago Objeto del tipo String
	 */
	public void setPago(String pago) {
		this.pago = pago;
	}


	/**
	 *Metodo get que sirve para obtener el valor de est
	 * 
	 * @return est Objeto del tipo String
	 */
	public String getEst() {
		return est;
	}

	/**
	 *Modifica el valor de la propiedad est
	 * 
	 * @param est Objeto del tipo String
	 */
	public void setEst(String est) {
		this.est = est;
	}

	/**
	 *Metodo get que sirve para obtener el valor de topo
	 * 
	 * @return topo Objeto del tipo String
	 */
	public String getTopo() {
		return topo;
	}

	/**
	 *Modifica el valor de la propiedad topo
	 * 
	 * @param topo Objeto del tipo String
	 */
	public void setTopo(String topo) {
		this.topo = topo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de pri
	 * 
	 * @return pri Objeto del tipo String
	 */
	public String getPri() {
		return pri;
	}

	/**
	 *Modifica el valor de la propiedad pri
	 * 
	 * @param pri Objeto del tipo String
	 */
	public void setPri(String pri) {
		this.pri = pri;
	}


	
	


}
