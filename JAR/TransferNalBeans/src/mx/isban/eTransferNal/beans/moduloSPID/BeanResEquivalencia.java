/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * configuración de equivalencias
 **/
public class BeanResEquivalencia implements Serializable{

	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -2590795410071352178L;
	
	/**Propiedad para lista de claves de equivalencias*/
	private List<BeanEquivalencia> beanEquivalencias;
	
	/**Propiedad para catalogo de claves de operacion*/
	private List<BeanClaveOperacion> listaClaveOperacion;
	
	/**Propiedad para la lista de tipo de pago*/
	private List<BeanTipoPago> listaTipoPago;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**Propiedad que contendra las propiedades del paginador en la tabla*/
	private BeanPaginador beanPaginador;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanEquivalencias
	 * 
	 * @return beanEquivalencias Objeto del tipo List<BeanEquivalencia>
	 */
	public List<BeanEquivalencia> getBeanEquivalencias() {
		return beanEquivalencias;
	}
	/**
	 *Modifica el valor de la propiedad beanEquivalencias
	 * 
	 * @param beanEquivalencias
	 *            Objeto del tipo List<BeanEquivalencia>
	 */
	public void setBeanEquivalencias(List<BeanEquivalencia> beanEquivalencias) {
		this.beanEquivalencias = beanEquivalencias;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listaClaveOperacion
	 * 
	 * @return listaClaveOperacion Objeto del tipo List<BeanClaveOperacion>
	 */
	public List<BeanClaveOperacion> getListaClaveOperacion() {
		return listaClaveOperacion;
	}
	/**
	 *Modifica el valor de la propiedad listaClaveOperacion
	 * 
	 * @param listaClaveOperacion
	 *            Objeto del tipo List<BeanClaveOperacion>
	 */
	public void setListaClaveOperacion(List<BeanClaveOperacion> listaClaveOperacion) {
		this.listaClaveOperacion = listaClaveOperacion;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listaTipoPago
	 * 
	 * @return listaTipoPago Objeto del tipo List<BeanTipoPago>
	 */
	public List<BeanTipoPago> getListaTipoPago() {
		return listaTipoPago;
	}
	/**
	 *Modifica el valor de la propiedad listaTipoPago
	 * 
	 * @param listaTipoPago
	 *            Objeto del tipo List<BeanTipoPago>
	 */
	public void setListaTipoPago(List<BeanTipoPago> listaTipoPago) {
		this.listaTipoPago = listaTipoPago;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoError
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	 * 
	 * @return serialVersionUID Objeto del tipo long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
