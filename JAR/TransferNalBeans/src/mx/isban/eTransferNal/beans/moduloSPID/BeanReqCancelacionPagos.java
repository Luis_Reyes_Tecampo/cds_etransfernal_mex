/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Cancelacion Pagos
**/
public class BeanReqCancelacionPagos implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4643500096889571705L;
	/**
	 * 
	 */
	
	
	/**Propiedad privada del tipo String que almacena la fecha de operacion*/
	private String sFechaOperacion;
	/**Propiedad privada del tipo String que almacena la clave de instituci�n*/
	private String sCveInstitucion;
	
	/**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
	   de la paginacion*/
	private BeanPaginador paginador;
	
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	
	/**
	 * Propiedad del tipo List<BeanCancelacionPagos> que almacena el listado de
	 * registros de la Cancelacion Pagos
	 */
	private List<BeanCancelacionPagos> listaCancelacionPagos =null;
	/**Propiedad privada del tipo trignS que almacena el filtro*/
	private String filtro;
	


	/**
	    *Metodo get que sirve para obtener el valor de la propiedad sFechaOperacion
	    * @return sFechaOperacion Objeto del tipo String
	    */
	public String getSFechaOperacion() {
		return sFechaOperacion;
	}

	/**
	   *Modifica el valor de la propiedad sFechaOperacion
	   * @param sFechaOperacion Objeto del tipo String
	   */
	public void setSFechaOperacion(String sFechaOperacion) {
		this.sFechaOperacion = sFechaOperacion;
	}

	/**
	    *Metodo get que sirve para obtener el valor de la propiedad sCveInstitucion
	    * @return sCveInstitucion Objeto del tipo String
	    */
	public String getSCveInstitucion() {
		return sCveInstitucion;
	}

	/**
	   *Modifica el valor de la propiedad sCveInstitucion
	   * @param sCveInstitucion Objeto del tipo String
	   */
	public void setSCveInstitucion(String sCveInstitucion) {
		this.sCveInstitucion = sCveInstitucion;
	}

	/**
	    *Metodo get que sirve para obtener el valor de la propiedad filtro
	    * @return filtro Objeto del tipo String
	    */
	public String getFiltro() {
		return filtro;
	}
	
	/**
	   *Modifica el valor de la propiedad filtro
	   * @param filtro Objeto del tipo String
	   */
	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad totalReg
	    * @return totalReg Objeto del tipo Int
	    */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	   *Modifica el valor de la propiedad totalReg
	   * @param totalReg Objeto del tipo Int
	   */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	   * @return beanPaginador Objeto del tipo BeanPaginador
	   */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	   *Modifica el valor de la propiedad beanPaginador
	   * @param paginador Objeto del tipo BeanPaginador
	   */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaCancelacionPagos
	 * 
	 * @return listaCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	 */
	public List<BeanCancelacionPagos> getListaCancelacionPagos() {
		return listaCancelacionPagos;
	}
	
	/**
	 *Modifica el valor de la propiedad listaCancelacionPagos
	 * 
	 * @param listaCancelacionPagos
	 *            Objeto del tipo List<BeanCancelacionPagos>
	 */
	public void setListaCancelacionPagos(List<BeanCancelacionPagos> listaCancelacionPagos) {
		this.listaCancelacionPagos = listaCancelacionPagos;
	}
}
