/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqAvisoTraspasos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se va a usar en el req de la consulta receptores historicos
 **/
public class BeanReqAvisoTraspasos implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7221569731548433530L;

	/**Propiedad del tipo BeanPaginador que almacena el beanPaginador*/
	private BeanPaginador paginador;
	/**Propiedad del tipo Integer que almacena el totalReg*/
	private Integer totalReg;
	/**Propiedad del tipo List que almacena la lista de datos*/
	private List<BeanAvisoTraspasos> listaConAviso;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad paginador
	 * 
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 *Modifica el valor de la propiedad paginador
	 * 
	 * @param paginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listaConAviso
	 * 
	 * @return listaConAviso Objeto del tipo List
	 */
	public List<BeanAvisoTraspasos> getListaConAviso() {
		return listaConAviso;
	}
	/**
	 *Modifica el valor de la propiedad listaConAviso
	 * 
	 * @param listaConAviso
	 *            Objeto del tipo List
	 */
	public void setListaConAviso(List<BeanAvisoTraspasos> listaConAviso) {
		this.listaConAviso = listaConAviso;
	}
}
