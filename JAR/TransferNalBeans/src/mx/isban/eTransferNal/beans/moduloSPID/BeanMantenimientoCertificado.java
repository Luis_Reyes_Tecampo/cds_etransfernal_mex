/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanMantenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos necesarios que seran mostrados en el mantenimiento de certificados
 **/
public class BeanMantenimientoCertificado implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 4032851432582491201L;
	
	/**Propiedad del tipo String que contiene el valor de claveIns*/
	private String claveIns;
	

	
	/**Propiedad del tipo boolean que contiene el valor de facultadFirma*/
	private boolean facultadFirma = false;
	
	/**Propiedad del tipo boolean que contiene el valor de facultadMant*/
	private boolean facultadMant = false;
	
	/**Propiedad del tipo boolean que contiene el valor de defaultFirma*/
	private boolean defaultFirma = false;
	
	/**Propiedad del tipo boolean que contiene el valor de defaultMant*/
	private boolean defaultMant = false;
	
	/**Propiedad del tipo boolean que contiene el valor de seleccionado*/
	private boolean seleccionado = false;
	
	/**Propiedad del tipo String que contiene el valor de sFacultadFirma*/
	private String sFacultadFirma;
	
	/**Propiedad del tipo String que contiene el valor de sFacultadMant*/
	private String sFacultadMant;
	
	/**Propiedad del tipo String que contiene el valor de sDefaultFirma*/
	private String sDefaultFirma;
	
	/**Propiedad del tipo String que contiene el valor de sDefaultMant*/
	private String sDefaultMant;
	
	/**Propiedad del tipo BeanMantenimientoCertificadoDos que contiene el valor de beanMantenimientoCertificadoDos*/
	private BeanMantenimientoCertificadoDos beanMantenimientoCertificadoDos;
	
	
	
	/**
	 * @return the beanMantenimientoCertificadoDos
	 */
	public BeanMantenimientoCertificadoDos getBeanMantenimientoCertificadoDos() {
		return beanMantenimientoCertificadoDos;
	}

	/**
	 * @param beanMantenimientoCertificadoDos the beanMantenimientoCertificadoDos to set
	 */
	public void setBeanMantenimientoCertificadoDos(BeanMantenimientoCertificadoDos beanMantenimientoCertificadoDos) {
		this.beanMantenimientoCertificadoDos = beanMantenimientoCertificadoDos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad claveMedio
	 * @return claveMedio Objeto del tipo @see String
	 */
	public String getClaveIns() {
		return claveIns;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad claveIns
	 * @param claveIns Objeto del tipo @see String
	 */


	/**
	 * @param claveIns Objeto del tipo @see String
	 */

	public void setClaveIns(String claveIns) {
		this.claveIns = claveIns;
	}
	
	
	
	/**
	 * Metodo get que sirve para obtener la propiedad facultadFirma
	 * @return facultadFirma Objeto del tipo @see String
	 */
	public boolean isFacultadFirma() {
		return facultadFirma;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad facultadFirma
	 * @param facultadFirma Objeto del tipo @see String
	 */
	public void setFacultadFirma(boolean facultadFirma) {
		this.facultadFirma = facultadFirma;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad facultadMant
	 * @return facultadMant Objeto del tipo @see String
	 */
	public boolean isFacultadMant() {
		return facultadMant;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad facultadMant
	 * @param facultadMant Objeto del tipo @see String
	 */
	public void setFacultadMant(boolean facultadMant) {
		this.facultadMant = facultadMant;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad defaultFirma
	 * @return defaultFirma Objeto del tipo @see String
	 */
	public boolean isDefaultFirma() {
		return defaultFirma;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad defaultFirma
	 * @param defaultFirma Objeto del tipo @see String
	 */
	public void setDefaultFirma(boolean defaultFirma) {
		this.defaultFirma = defaultFirma;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad defaultMant
	 * @return defaultMant Objeto del tipo @see String
	 */
	public boolean isDefaultMant() {
		return defaultMant;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad defaultMant
	 * @param defaultMant Objeto del tipo @see String
	 */
	public void setDefaultMant(boolean defaultMant) {
		this.defaultMant = defaultMant;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad seleccionado
	 * @return seleccionado Objeto del tipo @see String
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad seleccionado
	 * @param seleccionado Objeto del tipo @see String
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
	/**
	 * @return the sFacultadFirma
	 */
	public String getSFacultadFirma() {
		return sFacultadFirma;
	}
	/**
	 * @param sFacultadFirma the String to set
	 */
	public void setSFacultadFirma(String sFacultadFirma) {
		this.sFacultadFirma = sFacultadFirma;
	}
	/**
	 * @return the sFacultadMant
	 */
	public String getSFacultadMant() {
		return sFacultadMant;
	}
	/**
	 * @param sFacultadMant the String to set
	 */
	public void setSFacultadMant(String sFacultadMant) {
		this.sFacultadMant = sFacultadMant;
	}
	/**
	 * @return the sDefaultFirma
	 */
	public String getSDefaultFirma() {
		return sDefaultFirma;
	}
	/**
	 * @param sDefaultFirma the String to set
	 */
	public void setSDefaultFirma(String sDefaultFirma) {
		this.sDefaultFirma = sDefaultFirma;
	}
	/**
	 * @return the sDefaultMant
	 */
	public String getSDefaultMant() {
		return sDefaultMant;
	}
	/**
	 * @param sDefaultMant the String to set
	 */
	public void setSDefaultMant(String sDefaultMant) {
		this.sDefaultMant = sDefaultMant;
	}
	
	
}
