/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTranMensajeComp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   29/01/2019 01:50:59 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Class BeanTranMensajeComp.
 *
 * @author FSW-Vector
 * @since 29/01/2019
 */
public class BeanTranMensajeComp implements Serializable {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5975584836419917223L;
	
	/** La variable que contiene informacion con respecto a: dir ip gen. */
	private String dirIpGen;
	
	/** La variable que contiene informacion con respecto a: dir ip firma. */
	private String dirIpFirma;
	
	/** La variable que contiene informacion con respecto a: timeTamp. */
	private String timeTamp;
	
	/** La variable que contiene informacion con respecto a: canal firma. */
	private String canalFirma;
	
	/** La variable que contiene informacion con respecto a: swift. */
	private String swift;
	
	/** La variable que contiene informacion con respecto a: swift uno. */
	private String swiftUno;
	
	/** La variable que contiene informacion con respecto a: swift dos. */
	private String swiftDos;
	
	/** La variable que contiene informacion con respecto a: refe adicional. */
	private String refeAdicional;
	
	/** La variable que contiene informacion con respecto a: fchAplicion. */
	private String fchAplicion;
	
	/** La variable que contiene informacion con respecto a: buffer. */
	private String buffer;
			
	/**
	 * Obtener el objeto: dir ip firma.
	 *
	 * @return El objeto: dir ip firma
	 */
	public String getDirIpFirma() {
		return dirIpFirma;
	}
	
	/**
	 * Definir el objeto: dir ip firma.
	 *
	 * @param dirIpFirma El nuevo objeto: dir ip firma
	 */
	public void setDirIpFirma(String dirIpFirma) {
		this.dirIpFirma = dirIpFirma;
	}
		
	/**
	 * Obtener el objeto: canal firma.
	 *
	 * @return El objeto: canal firma
	 */
	public String getCanalFirma() {
		return canalFirma;
	}
	
	/**
	 * Definir el objeto: canal firma.
	 *
	 * @param canalFirma El nuevo objeto: canal firma
	 */
	public void setCanalFirma(String canalFirma) {
		this.canalFirma = canalFirma;
	}
	
	/**
	 * Obtener el objeto: swift.
	 *
	 * @return El objeto: swift
	 */
	public String getSwift() {
		return swift;
	}
	
	/**
	 * Definir el objeto: swift.
	 *
	 * @param swift El nuevo objeto: swift
	 */
	public void setSwift(String swift) {
		this.swift = swift;
	}
	
	/**
	 * Obtener el objeto: swift uno.
	 *
	 * @return El objeto: swift uno
	 */
	public String getSwiftUno() {
		return swiftUno;
	}
	
	/**
	 * Definir el objeto: swift uno.
	 *
	 * @param swiftUno El nuevo objeto: swift uno
	 */
	public void setSwiftUno(String swiftUno) {
		this.swiftUno = swiftUno;
	}
	
	/**
	 * Obtener el objeto: swift dos.
	 *
	 * @return El objeto: swift dos
	 */
	public String getSwiftDos() {
		return swiftDos;
	}
	
	/**
	 * Definir el objeto: swift dos.
	 *
	 * @param swiftDos El nuevo objeto: swift dos
	 */
	public void setSwiftDos(String swiftDos) {
		this.swiftDos = swiftDos;
	}
	
	/**
	 * Obtener el objeto: refe adicional.
	 *
	 * @return El objeto: refe adicional
	 */
	public String getRefeAdicional() {
		return refeAdicional;
	}
	
	/**
	 * Definir el objeto: refe adicional.
	 *
	 * @param refeAdicional El nuevo objeto: refe adicional
	 */
	public void setRefeAdicional(String refeAdicional) {
		this.refeAdicional = refeAdicional;
	}

	/**
	 * Obtener el objeto: dir ip gen.
	 *
	 * @return El objeto: dir ip gen
	 */
	public String getDirIpGen() {
		return dirIpGen;
	}

	/**
	 * Definir el objeto: dir ip gen.
	 *
	 * @param dirIpGen El nuevo objeto: dir ip gen
	 */
	public void setDirIpGen(String dirIpGen) {
		this.dirIpGen = dirIpGen;
	}

	/**
	 * Obtener el objeto: time tamp.
	 *
	 * @return El objeto: time tamp
	 */
	public String getTimeTamp() {
		return timeTamp;
	}

	/**
	 * Definir el objeto: time tamp.
	 *
	 * @param timeTamp El nuevo objeto: time tamp
	 */
	public void setTimeTamp(String timeTamp) {
		this.timeTamp = timeTamp;
	}
	
	/**
	 * Obtener el objeto: fch aplicion.
	 *
	 * @return El objeto: fch aplicion
	 */
	public String getFchAplicion() {
		return fchAplicion;
	}

	/**
	 * Definir el objeto: fch aplicion.
	 *
	 * @param fchAplicion El nuevo objeto: fch aplicion
	 */
	public void setFchAplicion(String fchAplicion) {
		this.fchAplicion = fchAplicion;
	}

	/**
	 * Obtener el objeto: buffer.
	 *
	 * @return El objeto: buffer
	 */
	public String getBuffer() {
		return buffer;
	}

	/**
	 * Definir el objeto: buffer.
	 *
	 * @param buffer El nuevo objeto: buffer
	 */
	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}
	
	
	
}
