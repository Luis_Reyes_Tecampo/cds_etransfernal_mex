/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResMantenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para el
 *mantenimiento de certificados
 **/
public class BeanResMantenimientoCertificado implements Serializable {
	
	/**Propiedad de tipo long que mantiene el estado del onjeto*/
	private static final long serialVersionUID = -306783086076846812L;
	
	/**Propiedad del tipo List<BeanMantenimientoCertificado> que contiene el valor de listaMantenimientoCertificado*/
	private List<BeanMantenimientoCertificado> listaMantenimientoCertificado;
	
	/**Propiedad del tipo List<BeanMantenimiento> que contiene el valor de listaMantenimiento*/
	private List<BeanMantenimiento> listaMantenimiento;
	
	/**Propiedad del tipo Integer que contiene el valor de totalReg*/
	private Integer totalReg = 0;
	
	/**Propiedad del tipo String que contiene el valor de codError*/
	private String codError;
	
	/**Propiedad del tipo String que contiene el valor de msgError*/
	private String msgError;
	
	/**Propiedad del tipo String que contiene el valor de tipoError*/
	private String tipoError;
	
	/**Propiedad del tipo BeanPaginador que contiene el valor de paginador*/
	private BeanPaginador beanPaginador;
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaMantenimientoCertificado
	 * @return listaMantenimientoCertificado Objeto del tipo @see List<BeanMantenimientoCertificado>
	 */
	public List<BeanMantenimientoCertificado> getListaMantenimientoCertificado() {
		return listaMantenimientoCertificado;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaMantenimientoCertificado
	 * @param listaMantenimientoCertificado Objeto del tipo @see List<BeanMantenimientoCertificado>
	 */
	public void setListaMantenimientoCertificado(List<BeanMantenimientoCertificado> listaMantenimientoCertificado) {
		this.listaMantenimientoCertificado = listaMantenimientoCertificado;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaMantenimiento
	 * @return listaMantenimiento Objeto del tipo @see List<BeanMantenimiento>
	 */
	public List<BeanMantenimiento> getListaMantenimiento() {
		return listaMantenimiento;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaMantenimiento
	 * @param listaMantenimiento Objeto del tipo @see List<BeanMantenimiento>
	 */
	public void setListaMantenimiento(List<BeanMantenimiento> listaMantenimiento) {
		this.listaMantenimiento = listaMantenimiento;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad claveMedio
	 * @param totalReg Objeto del tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad codError
	 * @return codError Objeto del tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad codError
	 * @param codError Objeto del tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad msgError
	 * @return msgError Objeto del tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad msgError
	 * @param msgError Objeto del tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad tipoError
	 * @return tipoError Objeto del tipo @see String
	 */
	public String getTipoError() {
		return tipoError;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoError
	 * @param tipoError Objeto del tipo @see String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad beanPaginador
	 * @return beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}	
}
