package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDTransEnvioRec implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2567618665228754681L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveDivisaRec
	 */
	private String cveDivisaRec;
	/**
	 * Propiedad del tipo String que almacena el valor de cvePtoVtaRec
	 */
	private String cvePtoVtaRec;
	/**
	 * Propiedad del tipo String que almacena el valor de cveIntermeRec
	 */
	private String cveIntermeRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec
	 */
	private String tipoCuentaRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec
	 */
	private String numCuentaRec;
	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec
	 */
	private String nombreRec;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreRec
	 */
	private String rfcRec;

	/**
	 * Metodo get que sirve para obtener la propiedad cveDivisaRec
	 * @return cveDivisaRec Objeto del tipo String
	 */
	public String getCveDivisaRec() {
		return cveDivisaRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveDivisaRec
	 * @param cveDivisaRec del tipo String
	 */
	public void setCveDivisaRec(String cveDivisaRec) {
		this.cveDivisaRec = cveDivisaRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cvePtoVtaRec
	 * @return cvePtoVtaRec Objeto del tipo String
	 */
	public String getCvePtoVtaRec() {
		return cvePtoVtaRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cvePtoVtaRec
	 * @param cvePtoVtaRec del tipo String
	 */
	public void setCvePtoVtaRec(String cvePtoVtaRec) {
		this.cvePtoVtaRec = cvePtoVtaRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cveIntermeRec
	 * @return cveIntermeRec Objeto del tipo String
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveIntermeRec
	 * @param cveIntermeRec del tipo String
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoCuentaRec
	 * @return tipoCuentaRec Objeto del tipo String
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCuentaRec
	 * @param tipoCuentaRec del tipo String
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaRec
	 * @return numCuentaRec Objeto del tipo String
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaRec
	 * @param numCuentaRec del tipo String
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad nombreRec
	 * @return nombreRec Objeto del tipo String
	 */
	public String getNombreRec() {
		return nombreRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad nombreRec
	 * @param nombreRec del tipo String
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad rfcRec
	 * @return rfcRec Objeto del tipo String
	 */
	public String getRfcRec() {
		return rfcRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad rfcRec
	 * @param rfcRec del tipo String
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}


}
