/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResOrdenReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * orden de reparaciones
 **/
public class BeanResOrdenReparacion implements BeanResultBO, Serializable{

	/**Propiedad del tipo long que conserva el estado del objeto*/
	private static final long serialVersionUID = -1757750456912759703L;
	
	/**Propiedad del tipo BeanPaginador que almacena la paginacion de registros*/
	private BeanPaginador beanPaginador;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**Propiedad del tipo String que almacena el nombre del Archivo */
	private String nombreArchivo; 
	
	/**Propiedad del tipo List<BeanOrdenReparacion> que almacena las ordenes obtenidas */
	private List<BeanOrdenReparacion> listaOrdenesReparacion;
	
	/**
	 * Metodo get que sirve para obtener la propiedad beanPaginador
	 * @return beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return beanPaginador Objeto del tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg Objeto del tipo @see String
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad codError
	 * @return codError Objeto del tipo @see String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad codError
	 * @param codError Objeto del tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad msgError
	 * @return msgError Objeto del tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad msgError
	 * @param msgError Objeto del tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoError
	 * @return tipoError Objeto del tipo @see BeanPaginador
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param tipoError Objeto del tipo @see String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad listaOrdenesReparacion
	 * @return listaOrdenesReparacion Objeto del tipo @see List
	 */
	public List<BeanOrdenReparacion> getListaOrdenesReparacion() {
		return listaOrdenesReparacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listaOrdenesReparacion
	 * @param listaOrdenesReparacion Objeto del tipo @see List
	 */
	public void setListaOrdenesReparacion(
			List<BeanOrdenReparacion> listaOrdenesReparacion) {
		this.listaOrdenesReparacion = listaOrdenesReparacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad nombreArchivo
	 * @return nombreArchivo Objeto del tipo @see String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad nombreArchivo
	 * @param nombreArchivo Objeto del tipo @see String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
}
