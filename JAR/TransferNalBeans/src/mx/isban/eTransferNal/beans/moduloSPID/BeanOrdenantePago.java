/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanOrdenantePago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en el detalle de cada pago
 **/
public class BeanOrdenantePago implements Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 4933933084028829120L;
	
	/** Propiedad del tipo String que almacena el intermediario */
	private String intermediario;
	/** Propiedad del tipo String que almacena el nombre */
	private String nombre;
	/** Propiedad del tipo String que almacena el numeroCuenta */
	private String numeroCuenta;
	/** Propiedad del tipo String que almacena el puntoVenta */
	private String puntoVenta;
	/** Propiedad del tipo String que almacena la divisa */
	private String divisa;
	/** Propiedad del tipo String que almacena el importeCargo */
	private String importeCargo;
	/** Propiedad del tipo String que almacena el tipoCuenta */
	private String tipoCuenta;
	/** Propiedad del tipo String que almacena el codigoPostal */
	private String codigoPostal;
	/** Propiedad del tipo String que almacena la fechaConstit */
	private String fechaConstit;
	/** Propiedad del tipo String que almacena el rfc */
	private String rfc;
	/** Propiedad del tipo String que almacena el domicilio */
	private String domicilio;

	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad intermediario
	 * 
	 * @return intermediario Objeto del tipo String
	 */
	public String getIntermediario() {
		return intermediario;
	}
	/**
	 *Modifica el valor de la propiedad intermediario
	 * 
	 * @param intermediario
	 *            Objeto del tipo String
	 */
	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nombre
	 * 
	 * @return nombre Objeto del tipo String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 *Modifica el valor de la propiedad nombre
	 * 
	 * @param nombre
	 *            Objeto del tipo String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numeroCuenta
	 * 
	 * @return numeroCuenta Objeto del tipo String
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**
	 *Modifica el valor de la propiedad numeroCuenta
	 * 
	 * @param numeroCuenta
	 *            Objeto del tipo String
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad puntoVenta
	 * 
	 * @return puntoVenta Objeto del tipo String
	 */
	public String getPuntoVenta() {
		return puntoVenta;
	}
	/**
	 *Modifica el valor de la propiedad puntoVenta
	 * 
	 * @param puntoVenta
	 *            Objeto del tipo String
	 */
	public void setPuntoVenta(String puntoVenta) {
		this.puntoVenta = puntoVenta;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad divisa
	 * 
	 * @return divisa Objeto del tipo String
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 *Modifica el valor de la propiedad divisa
	 * 
	 * @param divisa
	 *            Objeto del tipo String
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeCargo
	 * 
	 * @return importeCargo Objeto del tipo String
	 */
	public String getImporteCargo() {
		return importeCargo;
	}
	/**
	 *Modifica el valor de la propiedad importeCargo
	 * 
	 * @param importeCargo
	 *            Objeto del tipo String
	 */
	public void setImporteCargo(String importeCargo) {
		this.importeCargo = importeCargo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoCuenta
	 * 
	 * @return tipoCuenta Objeto del tipo String
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 *Modifica el valor de la propiedad tipoCuenta
	 * 
	 * @param tipoCuenta
	 *            Objeto del tipo String
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codigoPostal
	 * 
	 * @return codigoPostal Objeto del tipo String
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 *Modifica el valor de la propiedad codigoPostal
	 * 
	 * @param codigoPostal
	 *            Objeto del tipo String
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaConstit
	 * 
	 * @return fechaConstit Objeto del tipo String
	 */
	public String getFechaConstit() {
		return fechaConstit;
	}
	/**
	 *Modifica el valor de la propiedad fechaConstit
	 * 
	 * @param fechaConstit
	 *            Objeto del tipo String
	 */
	public void setFechaConstit(String fechaConstit) {
		this.fechaConstit = fechaConstit;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad rfc
	 * 
	 * @return rfc Objeto del tipo String
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 *Modifica el valor de la propiedad rfc
	 * 
	 * @param rfc
	 *            Objeto del tipo String
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad domicilio
	 * 
	 * @return domicilio Objeto del tipo String
	 */
	public String getDomicilio() {
		return domicilio;
	}
	/**
	 *Modifica el valor de la propiedad domicilio
	 * 
	 * @param domicilio
	 *            Objeto del tipo String
	 */
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	 * 
	 * @return serialVersionUID Objeto del tipo long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
