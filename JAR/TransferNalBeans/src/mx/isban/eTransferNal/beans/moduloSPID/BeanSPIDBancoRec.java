/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSPIDBancoRec.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDBancoRec implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -319354440458371924L;
	/**
	 * Propiedad del tipo String que almacena el valor de bancoRec
	 */
	private String nombreRec;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaRec
	 */
	private String tipoCuentaRec;
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaRec
	 */
	private String numCuentaRec;
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaTran
	 */
	private String numCuentaTran;
	/**
	 * Propiedad del tipo String que almacena el valor de rfcRec
	 */
	private String rfcRec;
	
	/**
	 * Metodo get que sirve para obtener la propiedad nombreRec
	 * @return nombreRec Objeto del tipo String
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad nombreRec
	 * @param nombreRec del tipo String
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad tipoCuentaRec
	 * @return tipoCuentaRec Objeto del tipo String
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCuentaRec
	 * @param tipoCuentaRec del tipo String
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaRec
	 * @return numCuentaRec Objeto del tipo String
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaRec
	 * @param numCuentaRec del tipo String
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rfcRec
	 * @return rfcRec Objeto del tipo String
	 */
	public String getRfcRec() {
		return rfcRec;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rfcRec
	 * @param rfcRec del tipo String
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaTran
	 * @return numCuentaTran Objeto del tipo String
	 */
	public String getNumCuentaTran() {
		return numCuentaTran;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaTran
	 * @param numCuentaTran del tipo String
	 */
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}
	
}
