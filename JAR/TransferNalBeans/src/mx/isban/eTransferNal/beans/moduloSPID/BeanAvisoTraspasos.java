/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanAvisoTraspasos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase la cual se encapsulan los datos que se van a consultar de los traspasos
 **/
/**
 * @author OPENROAD
 *
 */
public class BeanAvisoTraspasos implements Serializable{
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -486753272617360771L;
	/** Propiedad del tipo String que indica la hora */
	private String hora;
	/** Propiedad del tipo String que indica el folio */
	private String folio;
	/** Propiedad del tipo String que indica el tipo traspaso */
	private String typTrasp;
	/** Propiedad del tipo String que indica el descripcion */
	private String typDsc;
	/** Propiedad del tipo Integer que indica el total de registros */
	private Integer totalReg;
	/** Propiedad del tipo String que indica el importe */
	private String importe;
	/** Propiedad del tipo String que indica el sIAC */
	private String sIAC;
	/** Propiedad del tipo String que indica el sIDV */
	private String sIDV;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad hora
	 * 
	 * @return hora Objeto del tipo String
	 */
	public String getHora() {
		return hora;
	}
	/**
	 *Modifica el valor de la propiedad hora
	 * 
	 * @param hora
	 *            Objeto del tipo String
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad folio
	 * 
	 * @return folio Objeto del tipo String
	 */
	public String getFolio() {
		return folio;
	}
	/**
	 *Modifica el valor de la propiedad folio
	 * 
	 * @param folio
	 *            Objeto del tipo String
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad typtrasp
	 * 
	 * @return typTrasp Objeto del tipo String
	 */
	public String getTypTrasp() {
		return typTrasp;
	}
	/**
	 *Modifica el valor de la propiedad typtrasp
	 * 
	 * @param typTrasp
	 *            Objeto del tipo String
	 */
	public void setTypTrasp(String typTrasp) {
		this.typTrasp = typTrasp;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad descripcion
	 * 
	 * @return typDsc Objeto del tipo String
	 */
	public String getTypDsc() {
		return typDsc;
	}
	/**
	 *Modifica el valor de la propiedad typdsc
	 * 
	 * @param typDsc
	 *            Objeto del tipo String
	 */
	public void setTypDsc(String typDsc) {
		this.typDsc = typDsc;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importe
	 * 
	 * @return importe Objeto del tipo String
	 */
	public String getImporte() {
		return importe;
	}
	/**
	 *Modifica el valor de la propiedad importe
	 * 
	 * @param importe
	 *            Objeto del tipo String
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad sIAC
	 * 
	 * @return sIAC Objeto del tipo String
	 */
	public String getSIAC() {
		return sIAC;
	}
	
	/**
	 * Modifica el valor de la propiedad sIAC
	 * @param sIAC Objeto del tipo String
	 */
	public void setSIAC(String sIAC) {
		this.sIAC = sIAC;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad sIDV
	 * 
	 * @return sIDV Objeto del tipo String
	 */
	public String getSIDV() {
		return sIDV;
	}
	/**
	 *Modifica el valor de la propiedad sIDV
	 * 
	 * @param sIDV
	 *            Objeto del tipo String
	 */
	public void setSIDV(String sIDV) {
		this.sIDV = sIDV;
	}
	

}
