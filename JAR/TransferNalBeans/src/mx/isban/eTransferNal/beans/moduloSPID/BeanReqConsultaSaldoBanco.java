/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqConsultaSaldoBanco.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Consulta de Saldos por Banco
**/
public class BeanReqConsultaSaldoBanco implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -8310044394257905915L;
	
	/**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
	   de la paginacion*/
	private BeanPaginador paginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**
	 * Propiedad del tipo List<BeanConsultaSaldoBanco> que almacena el listado de
	 * registros de la Bitácora de Envío
	 */
	private List<BeanConsultaSaldoBanco> listaConsultaSaldoBanco = null;
	
	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	   * @return beanPaginador Objeto del tipo BeanPaginador
	   */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	   *Modifica el valor de la propiedad beanPaginador
	   * @param paginador Objeto del tipo BeanPaginador
	   */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad totalReg
	    * @return totalReg Objeto del tipo Integer
	    */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	    * Modifica el valor de la propiedad totalReg
	    * @param totalReg Objeto del tipo Integer
	    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaConsultaSaldoBanco
	 * 
	 * @return listaConsultaSaldoBanco Objeto del tipo List<BeanConsultaSaldoBanco>
	 */
	public List<BeanConsultaSaldoBanco> getListaConsultaSaldoBanco() {
		return listaConsultaSaldoBanco;
	}
	/**
	 *Modifica el valor de la propiedad listaConsultaSaldoBanco
	 * 
	 * @param listaConsultaSaldoBanco
	 *            Objeto del tipo List<BeanConsultaSaldoBanco>
	 */
	public void setListaConsultaSaldoBanco(List<BeanConsultaSaldoBanco> listaConsultaSaldoBanco) {
		this.listaConsultaSaldoBanco = listaConsultaSaldoBanco;
	}

}
