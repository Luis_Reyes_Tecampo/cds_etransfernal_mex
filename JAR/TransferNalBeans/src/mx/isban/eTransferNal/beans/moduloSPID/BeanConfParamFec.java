/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfParamFec.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en la configuración de parámetros
 **/
public class BeanConfParamFec implements Serializable {
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7217152845094251594L;
	
	/** Propiedad del tipo String que almacena el fecLogin */
	private String fecLogin;
	/** Propiedad del tipo String que almacena el fecPassword */
	private String fecPassword;
	/** Propiedad del tipo String que almacena el fecDestinatario */
	private String fecDestinatario;
	/** Propiedad del tipo String que almacena el fecDireccionIP */
	private String fecDireccionIP;
	/** Propiedad del tipo String que almacena el fecInicial */
	private String fecInicial;
	/** Propiedad del tipo String que almacena el fecFinal */
	private String fecFinal;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecLogin
	 * 
	 * @return fecLogin Objeto del tipo String
	 */
	public String getFecLogin() {
		return fecLogin;
	}
	/**
	 *Modifica el valor de la propiedad fecLogin
	 * 
	 * @param fecLogin
	 *            Objeto del tipo String
	 */
	public void setFecLogin(String fecLogin) {
		this.fecLogin = fecLogin;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecPassword
	 * 
	 * @return fecPassword Objeto del tipo String
	 */
	public String getFecPassword() {
		return fecPassword;
	}
	/**
	 *Modifica el valor de la propiedad fecPassword
	 * 
	 * @param fecPassword
	 *            Objeto del tipo String
	 */
	public void setFecPassword(String fecPassword) {
		this.fecPassword = fecPassword;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecDestinatario
	 * 
	 * @return fecDestinatario Objeto del tipo String
	 */
	public String getFecDestinatario() {
		return fecDestinatario;
	}
	/**
	 *Modifica el valor de la propiedad fecDestinatario
	 * 
	 * @param fecDestinatario
	 *            Objeto del tipo String
	 */
	public void setFecDestinatario(String fecDestinatario) {
		this.fecDestinatario = fecDestinatario;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecDireccionIP
	 * 
	 * @return fecDireccionIP Objeto del tipo String
	 */
	public String getFecDireccionIP() {
		return fecDireccionIP;
	}
	/**
	 *Modifica el valor de la propiedad fecDireccionIP
	 * 
	 * @param fecDireccionIP
	 *            Objeto del tipo String
	 */
	public void setFecDireccionIP(String fecDireccionIP) {
		this.fecDireccionIP = fecDireccionIP;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecInicial
	 * 
	 * @return fecInicial Objeto del tipo String
	 */
	public String getFecInicial() {
		return fecInicial;
	}
	/**
	 *Modifica el valor de la propiedad fecInicial
	 * 
	 * @param fecInicial
	 *            Objeto del tipo String
	 */
	public void setFecInicial(String fecInicial) {
		this.fecInicial = fecInicial;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecFinal
	 * 
	 * @return fecFinal Objeto del tipo String
	 */
	public String getFecFinal() {
		return fecFinal;
	}
	/**
	 *Modifica el valor de la propiedad fecFinal
	 * 
	 * @param fecFinal
	 *            Objeto del tipo String
	 */
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
}
