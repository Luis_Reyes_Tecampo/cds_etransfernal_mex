/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqConsReceptHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se van a usar en el req de los receptores historicos
 **/
public class BeanReqConsReceptHist implements Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 52547583410504366L;

	/**Propiedad del tipo BeanPaginador que almacena el beanPaginador*/
	private BeanPaginador beanPaginador;
	
	/**Propiedad del tipo Integer que almacena el toral de registros*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/** Propiedad privada del tipo String que almacena la opcion */
	private String opcion="";
	
	/** Propiedad privada del tipo String que almacena la fecha */
	private String fecha="";
	
	/** Propiedad privada del tipo List que almacena la lista de datos */
	private List<BeanConsRecepHist> listaDatos;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad opcion
	 * 
	 * @return opcion Objeto del tipo String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 *Modifica el valor de la propiedad opcion
	 * 
	 * @param opcion
	 *            Objeto del tipo String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPagiandor
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoError
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listaDatos
	 * 
	 * @return listaDatos Objeto del tipo List
	 */
	public List<BeanConsRecepHist> getListaDatos() {
		return listaDatos;
	}

	/**
	 *Modifica el valor de la propiedad listaDatos
	 * 
	 * @param listaDatos
	 *            Objeto del tipo List
	 */
	public void setListaDatos(
			List<BeanConsRecepHist> listaDatos) {
		this.listaDatos = listaDatos;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fecha
	 * 
	 * @return fecha Objeto del tipo String
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 *Modifica el valor de la propiedad fecha
	 * 
	 * @param fecha
	 *            Objeto del tipo String
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
