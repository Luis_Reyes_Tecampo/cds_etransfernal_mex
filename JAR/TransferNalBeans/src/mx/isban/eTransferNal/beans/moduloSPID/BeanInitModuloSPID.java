/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanInitModuloSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 *Clase que encapsula los datos que se mostraran en el inicio del módula SPID
 **/
public class BeanInitModuloSPID implements Serializable {
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -2522483497202670243L;
	
	/** Propiedad del tipo String que almacena la fechaOperacion */
	private String fechaOperacion;
	/** Propiedad del tipo String que almacena la cveInstitucion */
	private String cveInstitucion;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
	 * 
	 * @return fechaOperacion Objeto del tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 *Modifica el valor de la propiedad fechaOperacion
	 * 
	 * @param fechaOperacion
	 *            Objeto del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad cveInstitucion
	 * 
	 * @return cveInstitucion Objeto del tipo String
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}
	/**
	 *Modifica el valor de la propiedad cveInstitucion
	 * 
	 * @param cveInstitucion
	 *            Objeto del tipo String
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}
}
