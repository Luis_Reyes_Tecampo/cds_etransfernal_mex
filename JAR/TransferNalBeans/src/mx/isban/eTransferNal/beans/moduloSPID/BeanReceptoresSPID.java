/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReceptoresSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanReceptoresSPID extends BeanResBase implements Serializable {
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5949636965158443784L;

	/**
	 * Propiedad del tipo BeanSPIDLlave que almacena el valor de llave
	 */
	private BeanSPIDLlave llave;
	
	/**
	 * Propiedad del tipo BeanSPIDRastreo que almacena el valor de rastreo
	 */
	private BeanSPIDRastreo rastreo;
	
	/**
	 * Propiedad del tipo BeanSPIDDatGenerales que almacena el valor de datGenerales
	 */
	private BeanSPIDDatGenerales datGenerales;
	
	/**
	 * Propiedad del tipo BeanSPIDEstatus que almacena el valor de estatus
	 */
	private BeanSPIDEstatus estatus;
	
	/**
	 * Propiedad del tipo BeanSPIDBancoOrd que almacena el valor de bancoOrd
	 */
	private BeanSPIDBancoOrd bancoOrd;
	/**
	 * Propiedad del tipo BeanSPIDBancoRec que almacena el valor de bancoRec
	 */
	private BeanSPIDBancoRec bancoRec;
	
	/**
	 * Propiedad del tipo BeanSPIDEnvDev que almacena el valor de envDev
	 */
	private BeanSPIDEnvDev envDev;
	
	/**
	 * Propiedad del tipo BeanSPIDFchPagos que almacena el valor de fchPagos
	 */
	private BeanSPIDFchPagos fchPagos;
	

	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador paginador;
	
	/**
	 * Propiedad del tipo BeanSPIDOpciones que almacena el valor de opciones
	 */
	private BeanSPIDOpciones opciones;
	
	/**
	 * Propiedad del tipo BeanSPIDBloqueo que almacena el valor de bloqueo
	 */
	private BeanSPIDBloqueo bloqueo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de agrupacion
	 */
	private String agrupacion;
	
	/**
	 * Metodo get que sirve para obtener la propiedad bloqueo
	 * @return bloqueo Objeto del tipo BeanSPIDBloqueo
	 */
	public BeanSPIDBloqueo getBloqueo() {
		return bloqueo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bloqueo
	 * @param bloqueo del tipo BeanSPIDBloqueo
	 */
	public void setBloqueo(BeanSPIDBloqueo bloqueo) {
		this.bloqueo = bloqueo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad llave
	 * @return llave Objeto del tipo BeanSPIDLlave
	 */
	public BeanSPIDLlave getLlave() {
		return llave;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad llave
	 * @param llave del tipo BeanSPIDLlave
	 */
	public void setLlave(BeanSPIDLlave llave) {
		this.llave = llave;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datGenerales
	 * @return datGenerales Objeto del tipo BeanSPIDDatGenerales
	 */
	public BeanSPIDDatGenerales getDatGenerales() {
		return datGenerales;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datGenerales
	 * @param datGenerales del tipo BeanSPIDDatGenerales
	 */
	public void setDatGenerales(BeanSPIDDatGenerales datGenerales) {
		this.datGenerales = datGenerales;
	}
  
	/**
	 * Metodo get que sirve para obtener la propiedad estatus
	 * @return estatus Objeto del tipo BeanSPIDEstatus
	 */
	public BeanSPIDEstatus getEstatus() {
		return estatus;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad estatus
	 * @param estatus del tipo BeanSPIDEstatus
	 */
	public void setEstatus(BeanSPIDEstatus estatus) {
		this.estatus = estatus;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bancoOrd
	 * @return bancoOrd Objeto del tipo BeanSPIDBancoOrd
	 */
	public BeanSPIDBancoOrd getBancoOrd() {
		return bancoOrd;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bancoOrd
	 * @param bancoOrd del tipo BeanSPIDBancoOrd
	 */
	public void setBancoOrd(BeanSPIDBancoOrd bancoOrd) {
		this.bancoOrd = bancoOrd;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bancoRec
	 * @return bancoRec Objeto del tipo BeanSPIDBancoRec
	 */
	public BeanSPIDBancoRec getBancoRec() {
		return bancoRec;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bancoRec
	 * @param bancoRec del tipo BeanSPIDBancoRec
	 */
	public void setBancoRec(BeanSPIDBancoRec bancoRec) {
		this.bancoRec = bancoRec;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad envDev
	 * @return envDev Objeto del tipo BeanSPIDEnvDev
	 */
	public BeanSPIDEnvDev getEnvDev() {
		return envDev;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad envDev
	 * @param envDev del tipo BeanSPIDEnvDev
	 */
	public void setEnvDev(BeanSPIDEnvDev envDev) {
		this.envDev = envDev;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad rastreo
	 * @return rastreo Objeto del tipo BeanSPIDRastreo
	 */
	public BeanSPIDRastreo getRastreo() {
		return rastreo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad rastreo
	 * @param rastreo del tipo BeanSPIDRastreo
	 */
	public void setRastreo(BeanSPIDRastreo rastreo) {
		this.rastreo = rastreo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchPagos
	 * @return fchPagos Objeto del tipo BeanSPIDFchPagos
	 */
	public BeanSPIDFchPagos getFchPagos() {
		return fchPagos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchPagos
	 * @param fchPagos del tipo BeanSPIDFchPagos
	 */
	public void setFchPagos(BeanSPIDFchPagos fchPagos) {
		this.fchPagos = fchPagos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad paginador
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad paginador
	 * @param paginador del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad opciones
	 * @return opciones Objeto del tipo BeanSPIDOpciones
	 */
	public BeanSPIDOpciones getOpciones() {
		return opciones;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad opciones
	 * @param opciones del tipo BeanSPIDOpciones
	 */
	public void setOpciones(BeanSPIDOpciones opciones) {
		this.opciones = opciones;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad agrupacion
	 * @return agrupacion Objeto del tipo String
	 */
	public String getAgrupacion() {
		return agrupacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad agrupacion
	 * @param agrupacion del tipo String
	 */
	public void setAgrupacion(String agrupacion) {
		this.agrupacion = agrupacion;
	}


}
