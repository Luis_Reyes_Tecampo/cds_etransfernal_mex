package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDRastreo implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 13033541845027552L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreo
	 */
	private String cveRastreo;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreoOri
	 */
	private String cveRastreoOri;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveRastreoOri
	 */
	private String longRastreo;
	/**
	 * Propiedad del tipo String que almacena el valor de direccionIp
	 */
	private String direccionIp;
	

	/**
	 * Metodo get que sirve para obtener la propiedad cveRastreo
	 * @return cveRastreo Objeto del tipo String
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveRastreo
	 * @param cveRastreo del tipo String
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cveRastreoOri
	 * @return cveRastreoOri Objeto del tipo String
	 */
	public String getCveRastreoOri() {
		return cveRastreoOri;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveRastreoOri
	 * @param cveRastreoOri del tipo String
	 */
	public void setCveRastreoOri(String cveRastreoOri) {
		this.cveRastreoOri = cveRastreoOri;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad longRastreo
	 * @return longRastreo Objeto del tipo String
	 */
	public String getLongRastreo() {
		return longRastreo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad longRastreo
	 * @param longRastreo del tipo String
	 */
	public void setLongRastreo(String longRastreo) {
		this.longRastreo = longRastreo;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad direccionIp
	 * @return direccionIp Objeto del tipo String
	 */
	public String getDireccionIp() {
		return direccionIp;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad direccionIp
	 * @param direccionIp del tipo String
	 */
	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}


	
}
