/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de la configuración de topologias
**/
public class BeanReqTopologia implements Serializable {
	
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -8367152274680929734L;
	
	/**Propiedad que contendra las propiedades del paginador en la tabla*/
	private BeanPaginador paginador;
	
	/** Propiedad que se encarga de mostrar el total de registros obetenidos*/
	private Integer totalReg;
	
	/**Propiedad que contendra las propiedades del paginador en la tabla*/
	private List<BeanTopologia> listaTopologias;
	
	/**
	 * Metodo get que sirve para obtener la propiedad beanPaginador
	 * @return beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param paginador Objeto del tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg Objeto del tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad listaTopologias
	 * @return listaTopologias Objeto del tipo @see List
	 */
	public List<BeanTopologia> getListaTopologias() {
		return listaTopologias;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaTopologias
	 * @param listaTopologias Objeto del tipo @see List
	 */
	public void setListaTopologias(List<BeanTopologia> listaTopologias) {
		this.listaTopologias = listaTopologias;
	}
	
	
}
