/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanOperNoConfirmadasVolumen.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanOperNoConfirmadasVolumen implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 942353207102651834L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de traspasoSPIDSIACEspera
	 */
	private String traspasoSPIDSIACEspera;
	/**
	 * Propiedad del tipo String que almacena el valor de traspasoSPIDSIACEnv
	 */
	private String traspasoSPIDSIACEnv;
	/**
	 * Propiedad del tipo String que almacena el valor de traspasoSPIDSIACReparar
	 */
	private String traspasoSPIDSIACReparar;
	/**
	 * Propiedad del tipo String que almacena el valor de ordEspera
	 */
	private String ordEspera;
	/**
	 * Propiedad del tipo String que almacena el valor de ordEnviadas
	 */
	private String ordEnviadas;
	/**
	 * Propiedad del tipo String que almacena el valor de ordPorReparar
	 */
	private String ordPorReparar;

	/**
	 * Metodo get que sirve para obtener la propiedad traspasoSPIDSIACEspera
	 * @return traspasoSPIDSIACEspera Objeto del tipo String
	 */
	public String getTraspasoSPIDSIACEspera() {
		return traspasoSPIDSIACEspera;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad traspasoSPIDSIACEspera
	 * @param traspasoSPIDSIACEspera del tipo String
	 */
	public void setTraspasoSPIDSIACEspera(String traspasoSPIDSIACEspera) {
		this.traspasoSPIDSIACEspera = traspasoSPIDSIACEspera;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad traspasoSPIDSIACEnv
	 * @return traspasoSPIDSIACEnv Objeto del tipo String
	 */
	public String getTraspasoSPIDSIACEnv() {
		return traspasoSPIDSIACEnv;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad traspasoSPIDSIACEnv
	 * @param traspasoSPIDSIACEnv del tipo String
	 */
	public void setTraspasoSPIDSIACEnv(String traspasoSPIDSIACEnv) {
		this.traspasoSPIDSIACEnv = traspasoSPIDSIACEnv;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad traspasoSPIDSIACReparar
	 * @return traspasoSPIDSIACReparar Objeto del tipo String
	 */
	public String getTraspasoSPIDSIACReparar() {
		return traspasoSPIDSIACReparar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad traspasoSPIDSIACReparar
	 * @param traspasoSPIDSIACReparar del tipo String
	 */
	public void setTraspasoSPIDSIACReparar(String traspasoSPIDSIACReparar) {
		this.traspasoSPIDSIACReparar = traspasoSPIDSIACReparar;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordEspera
	 * @return ordEspera Objeto del tipo String
	 */
	public String getOrdEspera() {
		return ordEspera;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordEspera
	 * @param ordEspera del tipo String
	 */
	public void setOrdEspera(String ordEspera) {
		this.ordEspera = ordEspera;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordEnviadas
	 * @return ordEnviadas Objeto del tipo String
	 */
	public String getOrdEnviadas() {
		return ordEnviadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordEnviadas
	 * @param ordEnviadas del tipo String
	 */
	public void setOrdEnviadas(String ordEnviadas) {
		this.ordEnviadas = ordEnviadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordPorReparar
	 * @return ordPorReparar Objeto del tipo String
	 */
	public String getOrdPorReparar() {
		return ordPorReparar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordPorReparar
	 * @param ordPorReparar del tipo String
	 */
	public void setOrdPorReparar(String ordPorReparar) {
		this.ordPorReparar = ordPorReparar;
	}


}
