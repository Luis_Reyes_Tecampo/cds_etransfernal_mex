/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSPIDEnvDev.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDEnvDev implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -9101652679331472713L;

	/**
	 * Propiedad del tipo String que almacena el valor de motivoDev
	 */
	private String motivoDev;
	
	/**
	 * 
	 */
	private String motivoDevDesc;
	/**
	 * Propiedad del tipo String que almacena el valor de motivoDevAnt
	 */
	private String motivoDevAnt;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPaqDev
	 */
	private String folioPaqDev;
	/**
	 * Propiedad del tipo String que almacena el valor de folioPagoDev
	 */
	private String folioPagoDev;
	/**
	 * Metodo get que sirve para obtener la propiedad motivoDev
	 * @return motivoDev Objeto del tipo String
	 */
	public String getMotivoDev() {
		return motivoDev;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad motivoDev
	 * @param motivoDev del tipo String
	 */
	public void setMotivoDev(String motivoDev) {
		this.motivoDev = motivoDev;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPaqDev
	 * @return folioPaqDev Objeto del tipo String
	 */
	public String getFolioPaqDev() {
		return folioPaqDev;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPaqDev
	 * @param folioPaqDev del tipo String
	 */
	public void setFolioPaqDev(String folioPaqDev) {
		this.folioPaqDev = folioPaqDev;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad folioPagoDev
	 * @return folioPagoDev Objeto del tipo String
	 */
	public String getFolioPagoDev() {
		return folioPagoDev;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad folioPagoDev
	 * @param folioPagoDev del tipo String
	 */
	public void setFolioPagoDev(String folioPagoDev) {
		this.folioPagoDev = folioPagoDev;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad motivoDevAnt
	 * @return motivoDevAnt Objeto del tipo String
	 */
	public String getMotivoDevAnt() {
		return motivoDevAnt;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad motivoDevAnt
	 * @param motivoDevAnt del tipo String
	 */
	public void setMotivoDevAnt(String motivoDevAnt) {
		this.motivoDevAnt = motivoDevAnt;
	}
	/**
	 * @return the motivoDevDesc
	 */
	public String getMotivoDevDesc() {
		return motivoDevDesc;
	}
	/**
	 * @param motivoDevDesc the motivoDevDesc to set
	 */
	public void setMotivoDevDesc(String motivoDevDesc) {
		this.motivoDevDesc = motivoDevDesc;
	}
}
