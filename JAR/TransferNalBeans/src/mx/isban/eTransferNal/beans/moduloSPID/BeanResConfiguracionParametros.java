/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConfiguracionParametros.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;


/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de la configuración de parametros
**/
public class BeanResConfiguracionParametros implements BeanResultBO, Serializable{
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 6051740646388984855L;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**Propiedad del tipo String que almacena el nombre de archivo*/
	private String nombreArchivo;
	
	/**Propiedad del tipo BeanConfiguracionParametros que almacena la configuracionParametros*/
	private BeanConfiguracionParametros configuracionParametros;
	

	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
	   * @return nombreArchivo Objeto del tipo String
	   */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	   *Modifica el valor de la propiedad nombreArchivo
	   * @param nombreArchivo Objeto del tipo String
	   */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad codError
	   * @return codError Objeto del tipo String
	   */
	public String getCodError() {
		return codError;
	}
	/**
	   *Modifica el valor de la propiedad codError
	   * @param codError Objeto del tipo String
	   */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	   *Metodo get que sirve para obtener el valor de la propiedad msgError
	   * @return msgError Objeto del tipo String
	   */
	public String getMsgError() {
		return msgError;
	}
	/**
	   *Modifica el valor de la propiedad msgError
	   * @param msgError Objeto del tipo String
	   */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	   *Metodo get que sirve para obtener el valor de la propiedad tipoError
	   * @return tipoError Objeto del tipo String
	   */
	public String getTipoError() {
		return tipoError;
	}
	/**
	   *Modifica el valor de la propiedad tipoError
	   * @param tipoError Objeto del tipo String
	   */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	   *Metodo get que sirve para obtener el valor de la propiedad configuracionParametros
	   * @return configuracionParametros Objeto del tipo BeanConfiguracionParametros
	   */
	public BeanConfiguracionParametros getConfiguracionParametros() {
		return configuracionParametros;
	}
	/**
	   *Modifica el valor de la propiedad configuracionParametros
	   * @param configuracionParametros Objeto del tipo BeanConfiguracionParametros
	   */
	public void setConfiguracionParametros(
			BeanConfiguracionParametros configuracionParametros) {
		this.configuracionParametros = configuracionParametros;
	}
	
	
}
