/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConsReceptHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Lun Abr 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * @author IDS
 *
 */
public class BeanConsReceptHistSum implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4047257488938589568L;

	/**
	 * Topo V Liquidadas - Num Registros
	 */
	private String topoVLiqCount;
	
	/**
	 * Topo T Liquidadas - Num Registros
	 */
	private String topoTLiqCount;
	
	/**
	 * Topo T Por Liquidar - Num Registros
	 */
	private String topoTPorLiqCount;
	
	/**
	 * Rechazadas - Num Registros
	 */
	private String rechazadasCount;
	
	/**
	 * Topo V Liquidadas - Importe Total
	 */
	private String topoVLiqSum;
	
	/**
	 * Topo T Liquidadas - Importe Total
	 */
	private String topoTLiqSum;
	
	/**
	 * Topo T Por Liquidar - Importe Total
	 */
	private String topoTPorLiqSum;
	
	/**
	 * Rechazadas - Importe Total
	 */
	private String rechazadasSum;

	/**
	 * @return the topoVLiqCount
	 */
	public String getTopoVLiqCount() {
		return topoVLiqCount;
	}

	/**
	 * @param topoVLiqCount the topoVLiqCount to set
	 */
	public void setTopoVLiqCount(String topoVLiqCount) {
		this.topoVLiqCount = topoVLiqCount;
	}

	/**
	 * @return the topoTLiqCount
	 */
	public String getTopoTLiqCount() {
		return topoTLiqCount;
	}

	/**
	 * @param topoTLiqCount the topoTLiqCount to set
	 */
	public void setTopoTLiqCount(String topoTLiqCount) {
		this.topoTLiqCount = topoTLiqCount;
	}

	/**
	 * @return the topoTPorLiqCount
	 */
	public String getTopoTPorLiqCount() {
		return topoTPorLiqCount;
	}

	/**
	 * @param topoTPorLiqCount the topoTPorLiqCount to set
	 */
	public void setTopoTPorLiqCount(String topoTPorLiqCount) {
		this.topoTPorLiqCount = topoTPorLiqCount;
	}

	/**
	 * @return the rechazadasCount
	 */
	public String getRechazadasCount() {
		return rechazadasCount;
	}

	/**
	 * @param rechazadasCount the rechazadasCount to set
	 */
	public void setRechazadasCount(String rechazadasCount) {
		this.rechazadasCount = rechazadasCount;
	}

	/**
	 * @return the topoVLiqSum
	 */
	public String getTopoVLiqSum() {
		return topoVLiqSum;
	}

	/**
	 * @param topoVLiqSum the topoVLiqSum to set
	 */
	public void setTopoVLiqSum(String topoVLiqSum) {
		this.topoVLiqSum = topoVLiqSum;
	}

	/**
	 * @return the topoTLiqSum
	 */
	public String getTopoTLiqSum() {
		return topoTLiqSum;
	}

	/**
	 * @param topoTLiqSum the topoTLiqSum to set
	 */
	public void setTopoTLiqSum(String topoTLiqSum) {
		this.topoTLiqSum = topoTLiqSum;
	}

	/**
	 * @return the topoTPorLiqSum
	 */
	public String getTopoTPorLiqSum() {
		return topoTPorLiqSum;
	}

	/**
	 * @param topoTPorLiqSum the topoTPorLiqSum to set
	 */
	public void setTopoTPorLiqSum(String topoTPorLiqSum) {
		this.topoTPorLiqSum = topoTPorLiqSum;
	}

	/**
	 * @return the rechazadasSum
	 */
	public String getRechazadasSum() {
		return rechazadasSum;
	}

	/**
	 * @param rechazadasSum the rechazadasSum to set
	 */
	public void setRechazadasSum(String rechazadasSum) {
		this.rechazadasSum = rechazadasSum;
	}
}
