/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos que se utilizan en consulta recepciones
 **/
public class BeanReqConsultaRecepciones implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -5060954250903198935L;
	
	/** Propiedad del tipo BeanPaginador que almacena el paginador */
	private BeanPaginador paginador;
	/** Lista del tipo BeanConsultaRecepciones que almacena las recepciones */
	private List<BeanConsultaRecepciones> listaConsultaRecepciones = null;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg;
	/** Propiedad del tipo String que almacena el tipo orden */
	private String tipoOrden;
	/** Propiedad del tipo String que almacena el titulo */
	private String titulo;
	
	/**
	 * 
	 */
	private String sortField;
	
	/**
	 * 
	 */
	private String sortType;
	
	/**
	 * @return the sortField
	 */
	public String getSortField() {
		return sortField;
	}
	/**
	 * @param sortField the sortField to set
	 */
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	/**
	 * @return the sortType
	 */
	public String getSortType() {
		return sortType;
	}
	/**
	 * @param sortType the sortType to set
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	/**
	 * @return el paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * @param paginador el paginador a establecer
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * @return el listaConsultaRecepciones
	 */
	public List<BeanConsultaRecepciones> getListaConsultaRecepciones() {
		return listaConsultaRecepciones;
	}
	/**
	 * @param listaConsultaRecepciones la listaConsultaRecepciones a establecer
	 */
	public void setListaConsultaRecepciones(List<BeanConsultaRecepciones> listaConsultaRecepciones) {
		this.listaConsultaRecepciones = listaConsultaRecepciones;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return el tipoOrden
	 */
	public String getTipoOrden() {
		return tipoOrden;
	}
	/**
	 * @param tipoOrden el tipoOrden a establecer
	 */
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}
	/**
	 * @return el titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo el titulo a establecer
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	private String topologia;
	
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	public String getTopologia() {
		return this.topologia;
	}
	
	private String tipoPago;
	
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getTipoPago() {
		return this.tipoPago;
	}

	private String cveInsOrd;
	
	public void setCveInsOrd(String cveInsOrd) {
		this.cveInsOrd = cveInsOrd;
	}
	public String getCveInsOrd() {
		return this.cveInsOrd;
	}

	private String cveIntermeOrd;
	
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	public String getCveIntermeOrd() {
		return this.cveIntermeOrd;
	}
	

	private String folioPaquetes;
	
	public void setFolioPaquetes(String folioPaquetes) {
		this.folioPaquetes = folioPaquetes;
	}
	public String getFolioPaquetes() {
		return this.folioPaquetes;
	}
	

	private String numCuentaRec;
	
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}
	public String getNumCuentaRec() {
		return this.numCuentaRec;
	}
	
	

	private String numCuentaTran;
	
	public void setNumCuentaTran(String numCuentaTran) {
		this.numCuentaTran = numCuentaTran;
	}
	public String getNumCuentaTran() {
		return this.numCuentaTran;
	}
	
	private String monto;
	
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getMonto() {
		return this.monto;
	}
	
	private String codigoError;
	
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public String getCodigoError() {
		return this.codigoError;
	}

	private String motivoDevol;
	
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}
	public String getMotivoDevol() {
		return this.motivoDevol;
	}
	
	private String folioPagos;
	
	public void setFolioPagos(String folioPagos) {
		this.folioPagos = folioPagos;
	}
	public String getFolioPagos() {
		return this.folioPagos;
	}
	
	

}
