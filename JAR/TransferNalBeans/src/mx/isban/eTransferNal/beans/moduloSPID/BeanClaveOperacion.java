/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanClaveOperacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Clase que encapsula los datos que se mostraran en la configuraci�n de topolog�as
 *
 */
public class BeanClaveOperacion implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 2921710970312138442L;

	/** Propiedad del tipo String que almacena la claveOperacion */
	private String claveOperacion;
	
	/** Propiedad del tipo String que almacena la descripci�n */
	private String descripcion;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveOperacion
	 * 
	 * @return claveOperacion Objeto del tipo String
	 */
	public String getClaveOperacion() {
		return claveOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad claveOperacion
	 * @param claveOperacion Objeto del tipo @see String
	 */
	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad descripcion
	 * @return descripcion Objeto del tipo @see String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad descripcion
	 * @param descripcion Objeto del tipo @see String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 *Metodo que sirve para concatenar los valores de las propiedades claveOperacion y descripcion
	 * 
	 * @return claveOperacion +" - "+descripcion Objeto del tipo String
	 */
	@Override
	public String toString() {
		return claveOperacion +" - "+descripcion;
	}
}
