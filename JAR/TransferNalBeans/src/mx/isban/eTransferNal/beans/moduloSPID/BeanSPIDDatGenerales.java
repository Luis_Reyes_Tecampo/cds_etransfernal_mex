/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSPIDDatGenerales.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

public class BeanSPIDDatGenerales implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4896502535329873226L;

	/**
	 * Propiedad del tipo String que almacena el valor de topologia
	 */
	private String topologia;

	/**
	 * Propiedad del tipo String que almacena el valor de cda
	 */
	private String cda;

	/**
	 * Propiedad del tipo String que almacena el valor de enviar
	 */
	private String enviar;

	/**
	 * Propiedad del tipo String que almacena el valor de clasifOperacion
	 */
	private String clasifOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoOperacion
	 */
	private String tipoOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de prioridad
	 */
	private String prioridad;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoPago
	 */
	private String tipoPago;

	/**
	 * Propiedad del tipo String que almacena el valor de refeTransfer
	 */
	private String refeTransfer;

	/**
	 * Propiedad del tipo String que almacena el valor de conceptoPago
	 */
	private String conceptoPago;

	/**
	 * Propiedad del tipo String que almacena el valor de fchCaptura
	 */
	private String fchCaptura;

	/**
	 * Propiedad del tipo String que almacena el valor de cde
	 */
	private String cde;

	/**
	 * Propiedad del tipo BigDecimal que almacena el valor de monto
	 */
	private BigDecimal monto;

	/**
	 * Propiedad del tipo String que almacena el valor de fchaCaptura
	 */
	private String fechaCaptura;
		
	/**
	 * Metodo get que sirve para obtener la propiedad topologia
	 * @return topologia Objeto del tipo String
	 */
	public String getTopologia() {
		return topologia;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topologia
	 * @param topologia del tipo String
	 */
	public void setTopologia(String topologia) {
		this.topologia = topologia;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad prioridad
	 * @return prioridad Objeto del tipo String
	 */
	public String getPrioridad() {
		return prioridad;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad prioridad
	 * @param prioridad del tipo String
	 */
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoPago
	 * @return tipoPago Objeto del tipo String
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPago
	 * @param tipoPago del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoOperacion
	 * @return tipoOperacion Objeto del tipo String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoOperacion
	 * @param tipoOperacion del tipo String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad refeTransfer
	 * @return refeTransfer Objeto del tipo String
	 */
	public String getRefeTransfer() {
		return refeTransfer;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad refeTransfer
	 * @param refeTransfer del tipo String
	 */
	public void setRefeTransfer(String refeTransfer) {
		this.refeTransfer = refeTransfer;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad conceptoPago
	 * @return conceptoPago Objeto del tipo String
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad conceptoPago
	 * @param conceptoPago del tipo String
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchCaptura
	 * @return fchCaptura Objeto del tipo String
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchCaptura
	 * @param fchCaptura del tipo String
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cde
	 * @return cde Objeto del tipo String
	 */
	public String getCde() {
		return cde;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cde
	 * @param cde del tipo String
	 */
	public void setCde(String cde) {
		this.cde = cde;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cda
	 * @return cda Objeto del tipo String
	 */
	public String getCda() {
		return cda;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cda
	 * @param cda del tipo String
	 */
	public void setCda(String cda) {
		this.cda = cda;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad enviar
	 * @return enviar Objeto del tipo String
	 */
	public String getEnviar() {
		return enviar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad enviar
	 * @param enviar del tipo String
	 */
	public void setEnviar(String enviar) {
		this.enviar = enviar;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad clasifOperacion
	 * @return clasifOperacion Objeto del tipo String
	 */
	public String getClasifOperacion() {
		return clasifOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad clasifOperacion
	 * @param clasifOperacion del tipo String
	 */
	public void setClasifOperacion(String clasifOperacion) {
		this.clasifOperacion = clasifOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo BigDecimal
	 */
	public BigDecimal getMonto() {
		return monto;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo BigDecimal
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	/**
	 * Metodo get que sirve para obtener el monto formateado
	 * @return String con el monto formateado
	 */
	public String getStrMonto() {
		Utilerias utilerias = Utilerias.getUtilerias();
		return utilerias.formateaDecimales(monto, Utilerias.FORMATO_DECIMAL_NUMBER_LARGO_DOS);
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fechaCaptura
	 * @return fechaCaptura Objeto del tipo String
	 */
	public String getFechaCaptura() {
		return fechaCaptura;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad fechaCaptura
	 * @param fechaCaptura del tipo String
	 */
	public void setFechaCaptura(String fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}
}