/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqOrdeReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de la orden de reparación
**/
public class BeanReqOrdeReparacion implements Serializable {
	
	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = -3657295147516579689L;
	
	/**Propiedad del tipo BeanPaginador que almacena la paginacion de registros*/
	private BeanPaginador paginador;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg;
	
	/**Propiedad del tipo List<BeanOrdenReparacion> que almacena las ordenes obtenidas */
	private List<BeanOrdenReparacion> listaOrdenesReparacion = null;
	
	/**
	 * Metodo get que sirve para obtener la propiedad beanPaginador
	 * @return beanPaginador Objeto del tipo @see BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad beanPaginador
	 * @param paginador Objeto del tipo @see BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return beanPaginador Objeto del tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg Objeto del tipo @see String
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad listaOrdenesReparacion
	 * @return listaOrdenesReparacion Objeto del tipo @see List
	 */
	public List<BeanOrdenReparacion> getListaOrdenesReparacion() {
		return listaOrdenesReparacion;
	}
	
	/**
	 * Metodo set que modifica la referencia de la propiedad listaOrdenesReparacion
	 * @param listaOrdenesReparacion Objeto del tipo @see List
	 */
	public void setListaOrdenesReparacion(
			List<BeanOrdenReparacion> listaOrdenesReparacion) {
		this.listaOrdenesReparacion = listaOrdenesReparacion;
	}
}
