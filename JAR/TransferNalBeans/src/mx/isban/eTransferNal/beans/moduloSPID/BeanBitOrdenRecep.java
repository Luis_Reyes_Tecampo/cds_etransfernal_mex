/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBitOrdenRecep.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/01/2019 11:30:14 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 * 1.1   26/11/2020 12:00:00 AM VectorFSW 	Modificacion
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Class BeanBitOrdenRecep.
 * Bean que contiene los atributos para el grabado de Bitacora de una Orden de Recepcion
 *
 * @author FSW-Vector
 * @since 31/01/2019
 */
public class BeanBitOrdenRecep implements Serializable{
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2366512928389485232L;
	
	/** La variable que contiene informacion con respecto a: id operacion. */
	private String idOperacion; 
	
	/** La variable que contiene informacion con respecto a: serv tran. */
	private String servTran; 
	
	/** La variable que contiene informacion con respecto a: nombre archivo. */
	private String nombreArchivo;
	
	/** La variable que contiene informacion con respecto a: datos tran. */
	private BeanBitOrdenRecepTran datosTran = new BeanBitOrdenRecepTran();
	
	/** La variable que contiene informacion con respecto a: datos comp. */
	private BeanBitOrdenRecepComp datosComp = new BeanBitOrdenRecepComp();

	/**
	 * Obtener el objeto: id operacion.
	 *
	 * @return El objeto: id operacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	
	/**
	 * Definir el objeto: id operacion.
	 *
	 * @param idOperacion El nuevo objeto: id operacion
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
	
	/**
	 * Obtener el objeto: serv tran.
	 *
	 * @return El objeto: serv tran
	 */
	public String getServTran() {
		return servTran;
	}
	
	/**
	 * Definir el objeto: serv tran.
	 *
	 * @param servTran El nuevo objeto: serv tran
	 */
	public void setServTran(String servTran) {
		this.servTran = servTran;
	}
	
	/**
	 * Obtener el objeto: nombre archivo.
	 *
	 * @return El objeto: nombre archivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	/**
	 * Definir el objeto: nombre archivo.
	 *
	 * @param nombreArchivo El nuevo objeto: nombre archivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Obtener el objeto: datos tran.
	 *
	 * @return El objeto: datos tran
	 */
	public BeanBitOrdenRecepTran getDatosTran() {
		return datosTran;
	}

	/**
	 * Definir el objeto: datos tran.
	 *
	 * @param datosTran El nuevo objeto: datos tran
	 */
	public void setDatosTran(BeanBitOrdenRecepTran datosTran) {
		this.datosTran = datosTran;
	}

	/**
	 * Obtener el objeto: datos comp.
	 *
	 * @return El objeto: datos comp
	 */
	public BeanBitOrdenRecepComp getDatosComp() {
		return datosComp;
	}

	/**
	 * Definir el objeto: datos comp.
	 *
	 * @param datosComp El nuevo objeto: datos comp
	 */
	public void setDatosComp(BeanBitOrdenRecepComp datosComp) {
		this.datosComp = datosComp;
	}
	
}

