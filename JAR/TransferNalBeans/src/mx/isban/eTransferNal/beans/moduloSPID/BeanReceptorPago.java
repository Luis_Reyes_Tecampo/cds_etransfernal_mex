/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReceptorPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *Clase que encapsula los datos que se mostraran en el detalle de cada pago
 **/
public class BeanReceptorPago implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7029272953805002651L;
	
	/** Propiedad del tipo String que almacena el intermediario */
	private String intermediario;
	/** Propiedad del tipo String que almacena la sucursal */
	private String sucursal;
	/** Propiedad del tipo String que almacena el nombre */
	private String nombre;
	/** Propiedad del tipo String que almacena la divisa */
	private String divisa;
	/** Propiedad del tipo String que almacena la claveSwiftCorres */
	private String claveSwiftCorres;
	/** Propiedad del tipo BigDecimal que almacena el importeAbono */
	private BigDecimal importeAbono;
	/** Propiedad del tipo String que almacena el tipoCuenta */
	private String tipoCuenta;
	/** Propiedad del tipo String que almacena el rfc */
	private String rfc;
	/** Propiedad del tipo String que almacena el puntoVentaReceptor */
	private String puntoVentaReceptor;
	/** Propiedad del tipo String que almacena el numeroCuentaReceptor */
	private String numeroCuentaReceptor;
	/** Propiedad del tipo String que almacena el conceptoPago */
	private String conceptoPago;
	/** Propiedad del tipo String que almacena el comentario1 */
	private String comentario1;
	/** Propiedad del tipo String que almacena el comentario2 */
	private String comentario2;
	/** Propiedad del tipo String que almacena el comentario3 */
	private String comentario3;
	
	

	/**
	 * @return the conceptoPago objeto del tipo String
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	/**
	 * @param conceptoPago the conceptoPago to set
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	/**
	 * @return the comentario1 objeto del tipo String
	 */
	public String getComentario1() {
		return comentario1;
	}
	/**
	 * @param comentario1 the comentario1 to set
	 */
	public void setComentario1(String comentario1) {
		this.comentario1 = comentario1;
	}
	/**
	 * @return the comentario2 objeto del tipo String
	 */
	public String getComentario2() {
		return comentario2;
	}
	/**
	 * @param comentario2 the comentario2 to set
	 */
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	/**
	 * @return the comentario3 objeto del tipo String
	 */
	public String getComentario3() {
		return comentario3;
	}
	/**
	 * @param comentario3 the comentario3 to set
	 */
	public void setComentario3(String comentario3) {
		this.comentario3 = comentario3;
	}
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad intermediario
	 * 
	 * @return intermediario Objeto del tipo String
	 */
	public String getIntermediario() {
		return intermediario;
	}
	/**
	 *Modifica el valor de la propiedad intermediario
	 * 
	 * @param intermediario
	 *            Objeto del tipo String
	 */
	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad sucursal
	 * 
	 * @return sucursal Objeto del tipo String
	 */
	public String getSucursal() {
		return sucursal;
	}
	/**
	 *Modifica el valor de la propiedad sucursal
	 * 
	 * @param sucursal
	 *            Objeto del tipo String
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad nombre
	 * 
	 * @return nombre Objeto del tipo String
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 *Modifica el valor de la propiedad nombre
	 * 
	 * @param nombre
	 *            Objeto del tipo String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad divisa
	 * 
	 * @return divisa Objeto del tipo String
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 *Modifica el valor de la propiedad divisa
	 * 
	 * @param divisa
	 *            Objeto del tipo String
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad claveSwiftCorres
	 * 
	 * @return claveSwiftCorres Objeto del tipo String
	 */
	public String getClaveSwiftCorres() {
		return claveSwiftCorres;
	}
	/**
	 *Modifica el valor de la propiedad claveSwiftCorres
	 * 
	 * @param claveSwiftCorres
	 *            Objeto del tipo String
	 */
	public void setClaveSwiftCorres(String claveSwiftCorres) {
		this.claveSwiftCorres = claveSwiftCorres;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad importeAbono
	 * 
	 * @return importeAbono Objeto del tipo BigDecimal
	 */
	public BigDecimal getImporteAbono() {
		return importeAbono;
	}
	/**
	 *Modifica el valor de la propiedad importeAbono
	 * 
	 * @param importeAbono
	 *            Objeto del tipo BigDecimal
	 */
	public void setImporteAbono(BigDecimal importeAbono) {
		this.importeAbono = importeAbono;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoCuenta
	 * 
	 * @return tipoCuenta Objeto del tipo String
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 *Modifica el valor de la propiedad tipoCuenta
	 * 
	 * @param tipoCuenta
	 *            Objeto del tipo String
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad rfc
	 * 
	 * @return rfc Objeto del tipo String
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 *Modifica el valor de la propiedad rfc
	 * 
	 * @param rfc
	 *            Objeto del tipo String
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad serialVersionUID
	 * 
	 * @return serialVersionUID Objeto del tipo long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad puntoVentaReceptor
	 * 
	 * @return puntoVentaReceptor Objeto del tipo String
	 */
	public String getPuntoVentaReceptor() {
		return puntoVentaReceptor;
	}
	/**
	 *Modifica el valor de la propiedad puntoVentaReceptor
	 * 
	 * @param puntoVentaReceptor
	 *            Objeto del tipo String
	 */
	public void setPuntoVentaReceptor(String puntoVentaReceptor) {
		this.puntoVentaReceptor = puntoVentaReceptor;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numeroCuentaReceptor
	 * 
	 * @return numeroCuentaReceptor Objeto del tipo String
	 */
	public String getNumeroCuentaReceptor() {
		return numeroCuentaReceptor;
	}
	/**
	 *Modifica el valor de la propiedad numeroCuentaReceptor
	 * 
	 * @param numeroCuentaReceptor
	 *            Objeto del tipo String
	 */
	public void setNumeroCuentaReceptor(String numeroCuentaReceptor) {
		this.numeroCuentaReceptor = numeroCuentaReceptor;
	}
	
	

}
