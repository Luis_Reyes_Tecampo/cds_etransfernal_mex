/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanOperLiquidadasSaldo.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanOperLiquidadasSaldo implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1715410616069231044L;
	/**
	 * Propiedad del tipo String que almacena el saldo Inicial
	 */
	private String saldoInicial;
	/**
	 * Propiedad del tipo String que almacena el saldo traspaso SIAC SPID
	 */
	private String traspasoSIACSPID;
	/**
	 * Propiedad del tipo String que almacena el saldo ord Recibidas Por Aplic
	 */
	private String ordRecibidasPorAplic;
	/**
	 * Propiedad del tipo String que almacena el saldo ord Recibidas Aplic
	 */
	private String ordRecibidasAplic;
	/**
	 * Propiedad del tipo String que almacena el saldo ord Recibidas Rechazadas
	 */
	private String ordRecibidasRechazadas;
	/**
	 * Propiedad del tipo String que almacena el saldo ord Recibidas Por Dev
	 */
	private String ordRecibidasPorDev;
	/**
	 * Propiedad del tipo String que almacena el saldo traspaso SPID SIAC
	 */
	private String traspasoSPIDSIAC;
	/**
	 * Propiedad del tipo String que almacena el saldo ord Enviadas Conf
	 */
	private String ordEnviadasConf;
	/**
	 * Metodo get que sirve para obtener la propiedad saldoInicial
	 * @return saldoInicial Objeto del tipo String
	 */
	public String getSaldoInicial() {
		return saldoInicial;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldoInicial
	 * @param saldoInicial del tipo String
	 */
	public void setSaldoInicial(String saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad traspasoSIACSPID
	 * @return traspasoSIACSPID Objeto del tipo String
	 */
	public String getTraspasoSIACSPID() {
		return traspasoSIACSPID;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad traspasoSIACSPID
	 * @param traspasoSIACSPID del tipo String
	 */
	public void setTraspasoSIACSPID(String traspasoSIACSPID) {
		this.traspasoSIACSPID = traspasoSIACSPID;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordRecibidasPorAplic
	 * @return ordRecibidasPorAplic Objeto del tipo String
	 */
	public String getOrdRecibidasPorAplic() {
		return ordRecibidasPorAplic;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordRecibidasPorAplic
	 * @param ordRecibidasPorAplic del tipo String
	 */
	public void setOrdRecibidasPorAplic(String ordRecibidasPorAplic) {
		this.ordRecibidasPorAplic = ordRecibidasPorAplic;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordRecibidasAplic
	 * @return ordRecibidasAplic Objeto del tipo String
	 */
	public String getOrdRecibidasAplic() {
		return ordRecibidasAplic;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordRecibidasAplic
	 * @param ordRecibidasAplic del tipo String
	 */
	public void setOrdRecibidasAplic(String ordRecibidasAplic) {
		this.ordRecibidasAplic = ordRecibidasAplic;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordRecibidasRechazadas
	 * @return ordRecibidasRechazadas Objeto del tipo String
	 */
	public String getOrdRecibidasRechazadas() {
		return ordRecibidasRechazadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordRecibidasRechazadas
	 * @param ordRecibidasRechazadas del tipo String
	 */
	public void setOrdRecibidasRechazadas(String ordRecibidasRechazadas) {
		this.ordRecibidasRechazadas = ordRecibidasRechazadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordRecibidasPorDev
	 * @return ordRecibidasPorDev Objeto del tipo String
	 */
	public String getOrdRecibidasPorDev() {
		return ordRecibidasPorDev;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordRecibidasPorDev
	 * @param ordRecibidasPorDev del tipo String
	 */
	public void setOrdRecibidasPorDev(String ordRecibidasPorDev) {
		this.ordRecibidasPorDev = ordRecibidasPorDev;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad traspasoSPIDSIAC
	 * @return traspasoSPIDSIAC Objeto del tipo String
	 */
	public String getTraspasoSPIDSIAC() {
		return traspasoSPIDSIAC;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad traspasoSPIDSIAC
	 * @param traspasoSPIDSIAC del tipo String
	 */
	public void setTraspasoSPIDSIAC(String traspasoSPIDSIAC) {
		this.traspasoSPIDSIAC = traspasoSPIDSIAC;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad ordEnviadasConf
	 * @return ordEnviadasConf Objeto del tipo String
	 */
	public String getOrdEnviadasConf() {
		return ordEnviadasConf;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad ordEnviadasConf
	 * @param ordEnviadasConf del tipo String
	 */
	public void setOrdEnviadasConf(String ordEnviadasConf) {
		this.ordEnviadasConf = ordEnviadasConf;
	}



}
