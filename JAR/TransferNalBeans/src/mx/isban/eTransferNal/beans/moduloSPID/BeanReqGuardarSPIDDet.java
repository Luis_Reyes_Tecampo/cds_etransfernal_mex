package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanReqGuardarSPIDDet implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -2783382725219186222L;
	
	/**
	 * Propiedad del tipo BeanReceptoresSPID que almacena el valor de beanReq
	 */
	private BeanReceptoresSPID beanReq;
	
	/**
	 * Propiedad del tipo BeanReceptoresSPID que almacena el valor de beanReqAnt
	 */
	private BeanReceptoresSPID beanReqAnt;

	/**
	 * Metodo get que sirve para obtener la propiedad beanReq
	 * @return beanReq Objeto del tipo BeanReceptoresSPID
	 */
	public BeanReceptoresSPID getBeanReq() {
		return beanReq;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanReq
	 * @param beanReq del tipo BeanReceptoresSPID
	 */
	public void setBeanReq(BeanReceptoresSPID beanReq) {
		this.beanReq = beanReq;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad beanReqAnt
	 * @return beanReqAnt Objeto del tipo BeanReceptoresSPID
	 */
	public BeanReceptoresSPID getBeanReqAnt() {
		return beanReqAnt;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanReqAnt
	 * @param beanReqAnt del tipo BeanReceptoresSPID
	 */
	public void setBeanReqAnt(BeanReceptoresSPID beanReqAnt) {
		this.beanReqAnt = beanReqAnt;
	}
	
	

}
