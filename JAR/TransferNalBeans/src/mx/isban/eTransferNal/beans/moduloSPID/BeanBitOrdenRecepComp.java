/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBitOrdenRecepComp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/11/2020 06:46:21 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Class BeanBitOrdenRecepComp.
 * Bean que contiene los atributos complementarios para el grabado de Bitacora de una Orden de Recepcion
 *
 * @author FSW-Vector
 * @since 26/11/2020
 */
public class BeanBitOrdenRecepComp implements Serializable{
	
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2366512928389485232L;
	
	/** La variable que contiene informacion con respecto a: cod error. */
	private String codError; 
	
	/** La variable que contiene informacion con respecto a: desc err. */
	private String descErr; 
	
	/** La variable que contiene informacion con respecto a: desc oper. */
	private String descOper; 
	
	/** La variable que contiene informacion con respecto a: comentarios. */
	private String comentarios; 
	
	/** La variable que contiene informacion con respecto a: estatus. */
	private String estatus;
	
	/**
	 * Obtener el objeto: cod error.
	 *
	 * @return El objeto: cod error
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Definir el objeto: cod error.
	 *
	 * @param codError El nuevo objeto: cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Obtener el objeto: desc err.
	 *
	 * @return El objeto: desc err
	 */
	public String getDescErr() {
		return descErr;
	}
	
	/**
	 * Definir el objeto: desc err.
	 *
	 * @param descErr El nuevo objeto: desc err
	 */
	public void setDescErr(String descErr) {
		this.descErr = descErr;
	}
	
	/**
	 * Obtener el objeto: desc oper.
	 *
	 * @return El objeto: desc oper
	 */
	public String getDescOper() {
		return descOper;
	}
	
	/**
	 * Definir el objeto: desc oper.
	 *
	 * @param descOper El nuevo objeto: desc oper
	 */
	public void setDescOper(String descOper) {
		this.descOper = descOper;
	}
	
	/**
	 * Obtener el objeto: comentarios.
	 *
	 * @return El objeto: comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}
	
	/**
	 * Definir el objeto: comentarios.
	 *
	 * @param comentarios El nuevo objeto: comentarios
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	/**
	 * Obtener el objeto: estatus.
	 *
	 * @return El objeto: estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Definir el objeto: estatus.
	 *
	 * @param estatus El nuevo objeto: estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
		
}

