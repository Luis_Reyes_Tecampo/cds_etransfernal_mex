/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanReqBitacoraEnvio.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los parametros
 * necesarios de la funcionalidad de Bitácora de Envío
**/
public class BeanReqBitacoraEnvio implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 1322795069365430093L;
	
	/**Propiedad del tipo @see BeanPaginador que almacena la abstraccion
	   de la paginacion*/
	private BeanPaginador paginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**
	 * Propiedad del tipo List<BeanBitacoraEnvio> que almacena el listado de
	 * registros de la Bitácora de Envío
	 */
	private List<BeanBitacoraEnvio> listaBitacoraEnvio = null;
	
	/**
	   *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	   * @return beanPaginador Objeto del tipo BeanPaginador
	   */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	   *Modifica el valor de la propiedad beanPaginador
	   * @param paginador Objeto del tipo BeanPaginador
	   */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad totalReg
	    * @return totalReg Objeto del tipo Integer
	    */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	    * Modifica el valor de la propiedad totalReg
	    * @param totalReg Objeto del tipo Integer
	    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaBitacoraEnvio
	 * 
	 * @return listaBitacoraEnvio Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public List<BeanBitacoraEnvio> getListaBitacoraEnvio() {
		return listaBitacoraEnvio;
	}
	/**
	 *Modifica el valor de la propiedad listaBitacoraEnvio
	 * 
	 * @param listaBitacoraEnvio
	 *            Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public void setListaBitacoraEnvio(List<BeanBitacoraEnvio> listaBitacoraEnvio) {
		this.listaBitacoraEnvio = listaBitacoraEnvio;
	}

}
