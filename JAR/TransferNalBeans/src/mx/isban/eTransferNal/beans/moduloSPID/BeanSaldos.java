/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanSaldos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    30/01/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSaldos implements Serializable{
	
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3546703802098639264L;
	
	/**
	 * Propiedad del tipo String que almacena el valor del saldo Calculado
	 */
	private String saldoCalculado;
	/**
	 * Propiedad del tipo String que almacena el valor de saldo No Reservado
	 */
	private String saldoNoReservado;
	/**
	 * Propiedad del tipo String que almacena el valor de saldo Reservado
	 */
	private String saldoReservado;
	/**
	 * Propiedad del tipo String que almacena el valor de diferencia
	 */
	private String diferencia;
	/**
	 * Propiedad del tipo String que almacena el valor de devoluciones Env Conf
	 */
	private String devolucionesEnvConf;
	/**
	 * Metodo get que sirve para obtener la propiedad saldoCalculado
	 * @return saldoCalculado Objeto del tipo String
	 */
	public String getSaldoCalculado() {
		return saldoCalculado;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldoCalculado
	 * @param saldoCalculado del tipo String
	 */
	public void setSaldoCalculado(String saldoCalculado) {
		this.saldoCalculado = saldoCalculado;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad saldoNoReservado
	 * @return saldoNoReservado Objeto del tipo String
	 */
	public String getSaldoNoReservado() {
		return saldoNoReservado;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldoNoReservado
	 * @param saldoNoReservado del tipo String
	 */
	public void setSaldoNoReservado(String saldoNoReservado) {
		this.saldoNoReservado = saldoNoReservado;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad saldoReservado
	 * @return saldoReservado Objeto del tipo String
	 */
	public String getSaldoReservado() {
		return saldoReservado;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad saldoReservado
	 * @param saldoReservado del tipo String
	 */
	public void setSaldoReservado(String saldoReservado) {
		this.saldoReservado = saldoReservado;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad diferencia
	 * @return diferencia Objeto del tipo String
	 */
	public String getDiferencia() {
		return diferencia;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad diferencia
	 * @param diferencia del tipo String
	 */
	public void setDiferencia(String diferencia) {
		this.diferencia = diferencia;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad devolucionesEnvConf
	 * @return devolucionesEnvConf Objeto del tipo String
	 */
	public String getDevolucionesEnvConf() {
		return devolucionesEnvConf;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad devolucionesEnvConf
	 * @param devolucionesEnvConf del tipo String
	 */
	public void setDevolucionesEnvConf(String devolucionesEnvConf) {
		this.devolucionesEnvConf = devolucionesEnvConf;
	}
	
	


}
