/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResBitacoraEnvio.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * Bitácora de Envío
 **/
public class BeanResBitacoraEnvio implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -435712064690814428L;
	
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/**
     * Propiedad del tipo String que almacena el nombre del archivo excel
     */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo List<BeanBitacoraEnvio> que almacena el listado de
	 * registros de la Bitácora de Envío
	 */
	private List<BeanBitacoraEnvio> listaBitacoraEnvio;
	
	/**
	 * Importe total sin considerar la paginacion
	 */
	private String importeTotal;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad nombre de archivo
	    * @return nombreArchivo Objeto del tipo String
	    */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	    *Metodo set que sirve para modificar el valor de la propiedad nombre de archivo
	    * @param nombreArchivo Objeto del tipo String
	    */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaBitacoraEnvio
	 * 
	 * @return listaBitacoraEnvio Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public List<BeanBitacoraEnvio> getListaBitacoraEnvio() {
		return listaBitacoraEnvio;
	}
	/**
	 *Modifica el valor de la propiedad listaBitacoraEnvio
	 * 
	 * @param listaBitacoraEnvio
	 *            Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public void setListaBitacoraEnvio(List<BeanBitacoraEnvio> listaBitacoraEnvio) {
		this.listaBitacoraEnvio = listaBitacoraEnvio;
	}
	
	/**
	 * @return the importeTotal
	 */
	public String getImporteTotal() {
		return importeTotal;
	}

	/**
	 * @param importeTotal the importeTotal to set
	 */
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
}