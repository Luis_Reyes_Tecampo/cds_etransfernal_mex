package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

public class BeanSPIDTransEnvioOrd implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6105862108715710378L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveDivisaOrd
	 */
	private String cveDivisaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de cvePtoVtaOrd
	 */
	private String cvePtoVtaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de cveIntermeOrd
	 */
	private String cveIntermeOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de tipoCuentaOrd
	 */
	private String tipoCuentaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaOrd
	 */
	private String numCuentaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de nombreOrd
	 */
	private String nombreOrd;
	
	/**
	 * Propiedad del tipo String que almacena el valor de rfcOrd
	 */
	private String rfcOrd;
	
	/**
	 * Propiedad del tipo String que almacena el valor de domicilioOrd
	 */
	private String domicilioOrd;
	
	/**
	 * Propiedad del tipo String que almacena el valor de codPostalOrd
	 */
	private String codPostalOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de fchConstitOrd
	 */
	private String fchConstitOrd;
	/**
	 * Metodo get que sirve para obtener la propiedad cveDivisaOrd
	 * @return cveDivisaOrd Objeto del tipo String
	 */
	public String getCveDivisaOrd() {
		return cveDivisaOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveDivisaOrd
	 * @param cveDivisaOrd del tipo String
	 */
	public void setCveDivisaOrd(String cveDivisaOrd) {
		this.cveDivisaOrd = cveDivisaOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cvePtoVtaOrd
	 * @return cvePtoVtaOrd Objeto del tipo String
	 */
	public String getCvePtoVtaOrd() {
		return cvePtoVtaOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cvePtoVtaOrd
	 * @param cvePtoVtaOrd del tipo String
	 */
	public void setCvePtoVtaOrd(String cvePtoVtaOrd) {
		this.cvePtoVtaOrd = cvePtoVtaOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveIntermeOrd
	 * @return cveIntermeOrd Objeto del tipo String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveIntermeOrd
	 * @param cveIntermeOrd del tipo String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad tipoCuentaOrd
	 * @return tipoCuentaOrd Objeto del tipo String
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad tipoCuentaOrd
	 * @param tipoCuentaOrd del tipo String
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaOrd
	 * @return numCuentaOrd Objeto del tipo String
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaOrd
	 * @param numCuentaOrd del tipo String
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad nombreOrd
	 * @return nombreOrd Objeto del tipo String
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad nombreOrd
	 * @param nombreOrd del tipo String
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rfcOrd
	 * @return rfcOrd Objeto del tipo String
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rfcOrd
	 * @param rfcOrd del tipo String
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad domicilioOrd
	 * @return domicilioOrd Objeto del tipo String
	 */
	public String getDomicilioOrd() {
		return domicilioOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad domicilioOrd
	 * @param domicilioOrd del tipo String
	 */
	public void setDomicilioOrd(String domicilioOrd) {
		this.domicilioOrd = domicilioOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad codPostalOrd
	 * @return codPostalOrd Objeto del tipo String
	 */
	public String getCodPostalOrd() {
		return codPostalOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codPostalOrd
	 * @param codPostalOrd del tipo String
	 */
	public void setCodPostalOrd(String codPostalOrd) {
		this.codPostalOrd = codPostalOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad fchConstitOrd
	 * @return fchConstitOrd Objeto del tipo String
	 */
	public String getFchConstitOrd() {
		return fchConstitOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad fchConstitOrd
	 * @param fchConstitOrd del tipo String
	 */
	public void setFchConstitOrd(String fchConstitOrd) {
		this.fchConstitOrd = fchConstitOrd;
	}
	

}
