package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResDesencripta implements Serializable, BeanResultBO{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1411784956907962808L;
	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/**Propiedad que tiene el valor de la cadena DesEncriptada*/
	private String cadDesencriptada;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cadDesencriptada
	 * @return cadDesencriptada Objeto del tipo String
	 */
	public String getCadDesencriptada() {
		return cadDesencriptada;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cadDesencriptada
	 * @param cadDesencriptada del tipo String
	 */
	public void setCadDesencriptada(String cadDesencriptada) {
		this.cadDesencriptada = cadDesencriptada;
	}

	
}
