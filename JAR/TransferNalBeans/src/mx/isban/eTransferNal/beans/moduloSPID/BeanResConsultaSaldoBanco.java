/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConsultaSaldoBanco.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios para la
 * Consiulta de Saldos or Banco
 **/
public class BeanResConsultaSaldoBanco implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -716044159718868732L;
	
	/**
	 * Propiedad del tipo BeanPaginador que encapsula la funcionalidad de
	 * paginar
	 */
	private BeanPaginador beanPaginador;
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	/**
     * Propiedad del tipo String que almacena el nombre del archivo excel
     */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo List<BeanConsultaSaldoBanco> que almacena el listado de
	 * registros de la Consulta de saldos por banco
	 */
	private List<BeanConsultaSaldoBanco> listaConsultaSaldoBanco;
	/** Propiedad privada del tipo Integer que almacena el total de volumen de operaciones enviadas */
	private Integer totalVolumenEnv;
	/** Propiedad privada del tipo Integer que almacena el total de volumen de operaciones recibidas */
	private Integer totalVolumenRec;
	/** Propiedad privada del tipo BigDecimal que almacena el total de importe de operaciones enviadas */
	private BigDecimal totalImporteEnv;
	/** Propiedad privada del tipo BigDecimal que almacena el total de importe de operaciones recibidas */
	private BigDecimal totalImporteRec;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPaginador
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor del tipo de error
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad nombre de archivo
	    * @return nombreArchivo Objeto del tipo String
	    */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	    *Metodo set que sirve para modificar el valor de la propiedad nombre de archivo
	    * @param nombreArchivo Objeto del tipo String
	    */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listaConsultaSaldoBanco
	 * 
	 * @return listaConsultaSaldoBanco Objeto del tipo List<BeanConsultaSaldoBanco>
	 */
	public List<BeanConsultaSaldoBanco> getListaConsultaSaldoBanco() {
		return listaConsultaSaldoBanco;
	}
	/**
	 *Modifica el valor de la propiedad listaConsultaSaldoBanco
	 * 
	 * @param listaConsultaSaldoBanco
	 *            Objeto del tipo List<BeanConsultaSaldoBanco>
	 */
	public void setListaConsultaSaldoBanco(List<BeanConsultaSaldoBanco> listaConsultaSaldoBanco) {
		this.listaConsultaSaldoBanco = listaConsultaSaldoBanco;
	}
	
	/**
	 *Metodo get que sirve para calcular el total de volumen de operaciones enviadas
	 * 
	 * @return totalVolumenEnv Objeto del tipo Integer
	 */
	public Integer getTotalVolumenEnv() {
		totalVolumenEnv = 0;
		for(BeanConsultaSaldoBanco beanConsultaSaldoBanco : listaConsultaSaldoBanco){
			totalVolumenEnv += beanConsultaSaldoBanco.getVolumenOperacionesEnviadas();
		}
		return totalVolumenEnv;
	}

	/**
	 * @param totalVolumenEnv the totalVolumenEnv to set
	 */
	public void setTotalVolumenEnv(Integer totalVolumenEnv) {
		this.totalVolumenEnv = totalVolumenEnv;
	}
	
	
	
	/**
	 *Metodo get que sirve para calcular el total de volumen de operaciones recibidas
	 * 
	 * @return totalVolumenRec Objeto del tipo Integer
	 */
	public Integer getTotalVolumenRec() {
		totalVolumenRec = 0;
		for (BeanConsultaSaldoBanco beanConsultaSaldoBanco : listaConsultaSaldoBanco){
			totalVolumenRec += beanConsultaSaldoBanco.getVolumenOperacionesRecibidas();
		}
		return totalVolumenRec;
	}
	/**
	 * @param totalVolumenRec the totalVolumenRec to set
	 */
	public void setTotalVolumenRec(Integer totalVolumenRec) {
		this.totalVolumenRec = totalVolumenRec;
	}

	/**
	 *Metodo get que sirve para calcular el total de importe de operaciones enviadas
	 * 
	 * @return totalImporteEnv Objeto del tipo BigDecimal
	 */
	public BigDecimal getTotalImporteEnv() {
		totalImporteEnv = new BigDecimal(0);
		for (BeanConsultaSaldoBanco beanConsultaSaldoBanco : listaConsultaSaldoBanco){
			totalImporteEnv = totalImporteEnv.add(beanConsultaSaldoBanco.getImporteOperacionesEnviadas());
		}
		return totalImporteEnv;
	}
	/**
	 * @param totalImporteEnv the totalImporteEnv to set
	 */
	public void setTotalImporteEnv(BigDecimal totalImporteEnv) {
		this.totalImporteEnv = totalImporteEnv;
	}

	/**
	 *Metodo get que sirve para calcular el total de importe de operaciones recibidas
	 * 
	 * @return totalImporteRec Objeto del tipo BigDecimal
	 */
	public BigDecimal getTotalImporteRec() {
		totalImporteRec = new BigDecimal(0);
		for (BeanConsultaSaldoBanco beanConsultaSaldoBanco : listaConsultaSaldoBanco){
			totalImporteRec = totalImporteRec.add(beanConsultaSaldoBanco.getImporteOperacionesRecibidas());
		}
		return totalImporteRec;
	}
	/**
	 * @param totalImporteRec the totalImporteRec to set
	 */
	public void setTotalImporteRec(BigDecimal totalImporteRec) {
		this.totalImporteRec = totalImporteRec;
	}

	
}
