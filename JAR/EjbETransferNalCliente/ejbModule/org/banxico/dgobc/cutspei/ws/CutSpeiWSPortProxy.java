package org.banxico.dgobc.cutspei.ws;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import java.util.List;

public class CutSpeiWSPortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private org.banxico.dgobc.cutspei.ws.CutSpeiWSService _service = null;
        private org.banxico.dgobc.cutspei.ws.CutSpeiWS _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new org.banxico.dgobc.cutspei.ws.CutSpeiWSService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            _service = new org.banxico.dgobc.cutspei.ws.CutSpeiWSService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getCutSpeiWSPort();
        }

        public org.banxico.dgobc.cutspei.ws.CutSpeiWS getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://ws.cutspei.dgobc.banxico.org/", "cutSpeiWSPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

    }

    public CutSpeiWSPortProxy() {
        _descriptor = new Descriptor();
    }

    public CutSpeiWSPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public String obtSalt(String usuario) {
        return _getDescriptor().getProxy().obtSalt(usuario);
    }

    public List<Object> obtMontoPagosNominaTesofe(String usuario, String contrasena, int fecha) {
        return _getDescriptor().getProxy().obtMontoPagosNominaTesofe(usuario,contrasena,fecha);
    }

    public List<Object> obtMontoPagosNominaTesofeStr(String usuario, String contrasena, int fechaConsulta) {
        return _getDescriptor().getProxy().obtMontoPagosNominaTesofeStr(usuario,contrasena,fechaConsulta);
    }

}