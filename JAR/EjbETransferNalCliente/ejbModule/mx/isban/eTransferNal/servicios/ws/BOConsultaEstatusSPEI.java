/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOConsultaEstatusSPEI.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:40:27 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface BOConsultaEstatusSPEI.
 * Define los metodos para el Servicio de Consulta de Estatus BO
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@Remote
public interface BOConsultaEstatusSPEI {

	/**
	 * Consulta estatus SPEI.
	 *
	 * @param session El objeto: session
	 * @return Objeto res entrada string json DTO
	 * @throws BusinessException La business exception
	 */
	ResEntradaStringJsonDTO consultaEstatusSPEI();
}
