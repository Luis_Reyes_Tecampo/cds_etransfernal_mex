/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOConsultaDetallada.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2017 11:28:50 AM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface BOConsultaDetallada.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@Remote
public interface BOConsultaDetallada {

	
	/**
	 * Consulta detallada.
	 *
	 * @param reqConsultaDetallada El objeto: req consulta detallada
	 * @return Objeto res entrada string json dto
	 */
	ResEntradaStringJsonDTO consultaDetallada(EntradaStringJsonDTO reqConsultaDetallada);
}
