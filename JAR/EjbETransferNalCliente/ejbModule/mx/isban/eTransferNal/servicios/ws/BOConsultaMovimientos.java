/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovimientosImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;


/**
 * @author mcamarillo
 *
 */
@Remote
public interface BOConsultaMovimientos{
	
	/**
	 * @param entradaStringJsonDTO Bean con el json de entrada
	 * @return ResEntradaStringJsonDTO Bean de resouesta con el json de salida
	 * 			
	 */
	ResEntradaStringJsonDTO getConsultaMovimientos(EntradaStringJsonDTO entradaStringJsonDTO);


}
