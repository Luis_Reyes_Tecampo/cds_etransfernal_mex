/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaImporteSpei.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/10/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;


@Remote
public interface BOConsultaImporteSpei {
	/**
	 * Metodo que consulta los datos de una operacion
	 * @param param bean con los parametros de la consulta
	 * @return
	 * 			ResEntradaStringJsonDTO bean de respuesta con los datos de la operacion
	 */
	ResEntradaStringJsonDTO consultaImporteSpei(final EntradaStringJsonDTO param);
}
