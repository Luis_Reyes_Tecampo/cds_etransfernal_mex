/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BOConsultaBancoSPID.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 11:24:45 AM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.ws.BeanResponseConsultaBancoSPID;

/**
 * The Interface BOConsultaBancoSPID.
 * @author agarcia
 *
 */
@Remote
public interface BOConsultaBancoSPID {

	/**
	 * Consulta bancos SPID.
	 * @return the bean response consulta banco SPID
	 */
	BeanResponseConsultaBancoSPID consultaBancosSPID();
}