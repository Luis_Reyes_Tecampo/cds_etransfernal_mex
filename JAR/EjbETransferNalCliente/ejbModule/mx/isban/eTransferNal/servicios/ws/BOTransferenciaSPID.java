/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOTransferenciaSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface de negocio que tiene la funcionalidad de transferenica SPID
 * @author cchong
 *
 */
@Remote
public interface BOTransferenciaSPID {
	/**
	 * @param transferenciaJsonDTO Bean con el json de entrada
	 * @return ResEntradaStringJsonDTO Bean de resouesta con el json de salida
	 * 			
	 */
	ResEntradaStringJsonDTO transferencia(EntradaStringJsonDTO transferenciaJsonDTO);

}
