/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOTransferCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-09-2018 10:51:47 AM Juan Jesus Beltran R. VECTOR SWF Creacion
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface BOTransferCifrado.
 *
 * @author FSW-Vector
 * @since 08-09-2018
 */
@Remote
public interface BOTransferCifrado{

	/**
	 * Obtener el objeto: response service.
	 *
	 * @param entradaStringJsonDTO El objeto: entrada string json DTO
	 * @param mqService El objeto: mq service
	 * @return El objeto: response service
	 */
	ResEntradaStringJsonDTO getResponseService(EntradaStringJsonDTO entradaStringJsonDTO, String mqService);

}
