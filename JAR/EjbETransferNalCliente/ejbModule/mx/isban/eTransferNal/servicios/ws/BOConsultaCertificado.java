package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificado;

@Remote
public interface BOConsultaCertificado{
	
	/**
	 * Metodo que consulta los datos de un certificado a partir de numero de serie del certificado
	 * @param reqBeanConsultaCertificado bean con el numero de certificado
	 * @return
	 * 			ResBeanConsultaCertificado ResBeanConsultaCertificado con los datos del certificado
	 */
	 BeanResConsultaCertificado consultaCertificado(final BeanReqConsultaCertificado reqBeanConsultaCertificado);
}
