/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORConsultaBancoEJB.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.ws.ResBeanConsultaBanco;

/**
 * Interface de negocio que tiene la funcionalidad de consulta
 * @author cchong
 *
 */
@Remote
public interface BORConsultaBancoEJB {
	
	/**
	 * Metodo que permite realizar al consulta del banco por prefijo Cuenta clabe
	 * @param prefijoCuentaClabe String con el prefijo de la cuenta clabe
	 * @param architechSessionBean Objeto de la arquitectura
	 * @return ResBeanConsultaBanco bean que almacena la informacion del banco
	 */
	ResBeanConsultaBanco consultaBanco(String prefijoCuentaClabe,ArchitechSessionBean architechSessionBean);
}
