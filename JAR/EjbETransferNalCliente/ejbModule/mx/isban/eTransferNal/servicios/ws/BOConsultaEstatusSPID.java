/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaEstatusSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.servicios.ws;

import javax.ejb.Remote;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;
import mx.isban.eTransferNal.beans.ws.ResEntradaStringJsonDTO;

/**
 * Interface de negocio que tiene la funcionalidad de consultar el estatus de SPID
 * @author cchong
 *
 */
@Remote
public interface BOConsultaEstatusSPID {

	/**
	 * Metodo que permite consultar el estatus de las operaciones SPID
	 * @param reqConsultaEstatusDet Objeto del tipo EntradaStringJsonDTO
	 * @return ResEntradaStringJsonDTO Objeto de respuesta
	 */
	ResEntradaStringJsonDTO consultaEstatusDetSPID(EntradaStringJsonDTO reqConsultaEstatusDet);
}
