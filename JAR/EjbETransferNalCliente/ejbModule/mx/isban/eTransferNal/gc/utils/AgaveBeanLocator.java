/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanLocator.java
 *
 * Control de versiones:
 *
 * Version  Date                    By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		12/02/2015			    Stefanini			Stefanini	Creacion
 */
package mx.isban.eTransferNal.gc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



/**
 * The Class BeanLocator.
 */
public final class AgaveBeanLocator  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 552951218417001818L;
	
	/**Referencia constante al Log4j*/
	private static final Logger LOG = Logger.getLogger(AgaveBeanLocator.class);

	/** The Constant CONFIGURACION_AMBIENTE_EJB_PROPERTIES. */
	private static final String CONFIGURACION_AMBIENTE_EJB_PROPERTIES = "/Configuracion/AmbienteEJB.properties";
	
	/**
	 * Propiedad que almacena el valor de RUTA_ARQ
	 */
	private static final String RUTA_ARQ = "/arquitecturaAgave/DistV1";

	/** The cluster name. */
	private static String clusterName = loadClusterName();

	/**
	 * Instantiates a new agave bean locator.
	 */
	private AgaveBeanLocator() {

	}

	/**
	 * Load cluster name.
	 * 
	 * @return the string
	 */
	private static String loadClusterName() {
		String rutaBaseArquitectura = RUTA_ARQ;
		String archivoAmbiente = rutaBaseArquitectura
				+ CONFIGURACION_AMBIENTE_EJB_PROPERTIES;
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(new File(archivoAmbiente)));
		} catch (FileNotFoundException e) {
			LOG.error("No se encontro el archivo de configuracion...",e);
		} catch (IOException e) {
			LOG.error("No se puede cargar el archivo de configuracion...",e);
		}
		return StringUtils.defaultString(p.getProperty("CLUSTEREJB"));
	}

	/**
	 * Sobrecarga de metodo.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @return the t
	 */
	public static <T> T lookup(Class<T> clazz) {
		return lookup(clazz, clazz.getCanonicalName());
	}

	/**
	 * Lookup.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param jndiName
	 *            the jndi name
	 * @return the t
	 */
	public static <T> T lookup(Class<T> clazz, String jndiName) {
		Object bean = lookup(jndiName);
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
	}

	/**
	 * Lookup.
	 * 
	 * @param jndiName
	 *            the jndi name
	 * @return the object
	 */
	public static Object lookup(String jndiName) {
		Context context = null;
		Object obj = null;
		try {
			context = new InitialContext();
			obj =  context.lookup(clusterName + jndiName);
			context.close();
			context = null;
		} catch (NamingException ex) {
			throw new IllegalStateException("Cannot connect to bean: "
					+ clusterName + jndiName + " Reason: " + ex, ex.getCause());
		} finally {
			try {
				if(context!= null){
					context.close();
					context = null;
				}
				
			} catch (NamingException ex) {
				LOG.error("Cannot close InitialContext. Reason: " + ex,ex.getCause());
			}
		}
		return obj;
	}
}
