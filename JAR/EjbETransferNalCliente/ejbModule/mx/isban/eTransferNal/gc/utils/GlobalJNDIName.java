/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * GlobalJNDIName.java
 *
 * Control de versiones:
 *
 * Version  Date                    By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		14/02/2015			    Stefanini			Stefanini	Creacion
 */
package mx.isban.eTransferNal.gc.utils;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.Stateful;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

/**
 * The Class GlobalJNDIName.
 */
public class GlobalJNDIName{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1240394226622335582L;

	/**Referencia constante al Log4j*/
	private static final Logger LOG = Logger.getLogger(GlobalJNDIName.class);
	/** The Constant PREFIX. */
	private static final String PREFIX = "";

	/** The Constant SEPARATOR. */
	private static final String SEPARATOR = "";

	/** The Constant PROPERTY_FILE. */
	private static final String PROPERTY_FILE = "/global.jndi";

	/** The Constant MODULE_NAME_KEY. */
	private static final String MODULE_NAME_KEY = "module.name";

	/** The Constant APP_NAME_KEY. */
	private static final String APP_NAME_KEY = "application.name";

	/** The builder. */
	private transient final StringBuilder builder;

	/** The app name. */
	private transient String appName;

	/** The module name. */
	private transient String moduleName;

	/** The bean name. */
	private transient String beanName;

	/** The business interface name. */
	private String businessInterfaceName;

	/**
	 * Propiedad que almacena el valor de cargaProperties
	 */
	private boolean cargaProperties;

	/**
	 * Instantiates a new global jndi name.
	 */
	public GlobalJNDIName() {
		this.builder = new StringBuilder();
		this.builder.append(PREFIX).append(SEPARATOR);

		cargaProperties = false;

		if (cargaProperties) {
			Properties globalConfiguration = new Properties();
			try {
				globalConfiguration.load(this.getClass().getResourceAsStream(
						PROPERTY_FILE));
			} catch (IOException e) {
				LOG.error("Error al cargar archivo de propiedades:"
						+ e.getMessage());
			}
			this.appName = globalConfiguration.getProperty(APP_NAME_KEY);
			this.moduleName = globalConfiguration.getProperty(MODULE_NAME_KEY);
		}
	}

	/**
	 * With app name.
	 * 
	 * @param appName
	 *            the app name
	 * @return the global jndi name
	 */
	public GlobalJNDIName withAppName(String appName) {
		this.appName = appName;
		return this;
	}

	/**
	 * With module name.
	 * 
	 * @param moduleName
	 *            the module name
	 * @return the global jndi name
	 */
	public GlobalJNDIName withModuleName(String moduleName) {
		this.moduleName = moduleName;
		return this;
	}

	/**
	 * With bean name.
	 * 
	 * @param beanName
	 *            the bean name
	 * @return the global jndi name
	 */
	public GlobalJNDIName withBeanName(String beanName) {
		this.beanName = beanName;
		return this;
	}

	/**
	 * With bean name.
	 * 
	 * @param beanClass
	 *            the bean class
	 * @return the global jndi name
	 */
	public GlobalJNDIName withBeanName(Class beanClass) {
		return withBeanName(computeBeanName(beanClass));
	}

	/**
	 * With business interface.
	 * 
	 * @param interfaze
	 *            the interfaze
	 * @return the global jndi name
	 */
	public GlobalJNDIName withBusinessInterface(Class interfaze) {
		this.businessInterfaceName = interfaze.getName();
		return this;
	}

	/**
	 * Compute bean name.
	 * 
	 * @param beanClass
	 *            the bean class
	 * @return the string
	 */
	String computeBeanName(Class beanClass) {
		Stateless stateless = (Stateless) beanClass
				.getAnnotation(Stateless.class);
		if (stateless != null && isNotEmpty(stateless.name())) {
			return stateless.name();
		}
		Stateful stateful = (Stateful) beanClass.getAnnotation(Stateful.class);
		if (stateful != null && isNotEmpty(stateful.name())) {
			return stateful.name();
		}
		return beanClass.getSimpleName();
	}

	/**
	 * Checks if is not empty.
	 * 
	 * @param name
	 *            the name
	 * @return true, if is not empty
	 */
	private boolean isNotEmpty(String name) {
		return name != null && !name.isEmpty();
	}

	/**
	 * As string.
	 * 
	 * @return the string
	 */
	public String asString() {
		if (businessInterfaceName != null) {
			this.builder.append(businessInterfaceName);
		}
		return this.builder.toString();
	}

	/**
	 * Locate.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @return the t
	 */
	public <T> T locate(Class<T> clazz) {
		return AgaveBeanLocator.lookup(clazz, asString());
	}

	/**
	 * Locate.
	 * 
	 * @return the object
	 */
	public Object locate() {
		return AgaveBeanLocator.lookup(asString());
	}

	/**
	 *Metodo que regresa el valor de la propiedad appName
	 *@return appName del tipo String regresa el valor de appName
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad appName
	 * @param appName del tipo String que sustituye el valor de la propiedad appName
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 *Metodo que regresa el valor de la propiedad moduleName
	 *@return moduleName del tipo String regresa el valor de moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad moduleName
	 * @param moduleName del tipo String que sustituye el valor de la propiedad moduleName
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 *Metodo que regresa el valor de la propiedad beanName
	 *@return beanName del tipo String regresa el valor de beanName
	 */
	public String getBeanName() {
		return beanName;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad beanName
	 * @param beanName del tipo String que sustituye el valor de la propiedad beanName
	 */
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	/**
	 *Metodo que regresa el valor de la propiedad businessInterfaceName
	 *@return businessInterfaceName del tipo String regresa el valor de businessInterfaceName
	 */
	public String getBusinessInterfaceName() {
		return businessInterfaceName;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad businessInterfaceName
	 * @param businessInterfaceName del tipo String que sustituye el valor de la propiedad businessInterfaceName
	 */
	public void setBusinessInterfaceName(String businessInterfaceName) {
		this.businessInterfaceName = businessInterfaceName;
	}

	/**
	 *Metodo que regresa el valor de la propiedad cargaProperties
	 *@return cargaProperties del tipo boolean regresa el valor de cargaProperties
	 */
	public boolean isCargaProperties() {
		return cargaProperties;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad cargaProperties
	 * @param cargaProperties del tipo boolean que sustituye el valor de la propiedad cargaProperties
	 */
	public void setCargaProperties(boolean cargaProperties) {
		this.cargaProperties = cargaProperties;
	}
	
	
}