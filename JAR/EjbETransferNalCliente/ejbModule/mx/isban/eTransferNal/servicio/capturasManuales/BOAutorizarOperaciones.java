/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaOperaciones.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   22/03/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.capturasManuales;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpe;

/**
 * The Interface BOAutorizarOperaciones.
 */
@Remote
public interface BOAutorizarOperaciones {

	/**
	 * Consultar operaciones pendientes.
	 *
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @param req the req
	 * @return BeanResAutorizarOpe bean con los datos de respuesta
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe consultarOperacionesPendientes(ArchitechSessionBean sessionBean, BeanReqAutorizarOpe req)throws BusinessException;

	/**
	 * Apartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param listRegistrosSeleccionados the list registros seleccionados
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe apartarOPeraciones(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listRegistrosSeleccionados)throws BusinessException;

	/**
	 * Desapartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param req the req
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe desapartarOPeraciones(ArchitechSessionBean sessionBean, BeanReqAutorizarOpe req)throws BusinessException;
	
	/**
	 * Liberar operacion autorizada.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @param listOpeSelec the list ope selec
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	void liberarOperacionAutorizada(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listOpeSelec)throws BusinessException;
	
	/**
	 * Autorizar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe autorizarOperaciones(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException;
	
	/**
	 * Reparar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe repararOperaciones(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException;
	
	/**
	 * Cancelar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param listOperacionesSeleccionadas the list operaciones seleccionadas
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe cancelarOperaciones(ArchitechSessionBean sessionBean, List<BeanAutorizarOpe> listOperacionesSeleccionadas)throws BusinessException;
	
	/**
	 * Consultar usr admin.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res autorizar ope
	 * @throws BusinessException the business exception
	 */
	BeanResAutorizarOpe consultarUsrAdmin(ArchitechSessionBean sessionBean)throws BusinessException;
}