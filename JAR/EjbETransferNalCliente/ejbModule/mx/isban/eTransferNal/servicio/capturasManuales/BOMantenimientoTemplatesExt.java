/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMantenimientoTemplatesExt.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Apr 07 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.servicio.capturasManuales;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;

/**
 * Interfaz Service BO  que actua como firma de la capa de negocios de servicios externos del template.
 */
public interface BOMantenimientoTemplatesExt {
	
	/**
	 * Metodo que permite obtener la lista de layouts disponibles para el componente de altas.
	 *
	 * @param req Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la Lista de layouts para el componente de altas
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate obtenerListaLayouts(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metodo que permite obtener la lista de usuarios disponibles para asignarlos como autorizados.
	 *
	 * @param req Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la Lista de usuarios disponibles para para asignarlos a autorizados
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate obtenerListaUsuarios(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metodo que permite obtener los campos a utilizarse en la alta de templates.
	 *
	 * @param req Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la Lista de campos  del layout a utilizarse en la alta
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate obtenerCamposLayout(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
}
