/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   21/03/2017 04:29:24 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.servicio.capturasManuales;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOCapturaCentral.
 *
 * @author FSW-Vector
 * @since 21/03/2017
 */
@Remote
public interface BOCapturaCentral {

	/**
	 * Consulta todos los templates.
	 *
	 * @param request El objeto: request
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean captura central response
	 * @throws BusinessException 
	 */
	BeanCapturaCentralResponse consultaTodosTemplates(
			BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Consulta template.
	 *
	 * @param request El objeto: request
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean captura central response
	 * @throws BusinessException 
	 */
	BeanCapturaCentralResponse consultaTemplate(
			BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Alta captura manual spid.
	 *
	 * @param operacion El objeto: operacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res base
	 * @throws BusinessException 
	 */
	BeanResBase altaCapturaManualSPID(BeanOperacion operacion,
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
}
