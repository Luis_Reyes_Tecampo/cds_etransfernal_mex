/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMantenimientoTemplates.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 28 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.servicio.capturasManuales;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResPrincipalTemplate;

/**
 * Interfaz Service BO  que actua como firma de la capa de negocios.
 */
@Remote
public interface BOMantenimientoTemplates {
	
	/**
	 * Metodo que permite obtener la lista de templates disponibles para el mantenimiento .
	 *
	 * @param req Objeto con los parametros de consulta
	 * @param session Objeto general de sesion @see ArchitechSessionBean
	 * @return Objeto que expone la lista de templates disponibles
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate consultarTemplates(BeanReqConsultaTemplates req, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metodo a traves del cual se invoca la accion de guardar el template en la BD.
	 *
	 * @param req Objeto con las propiedades del template
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de guardar informacion de template
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate guardarTemplate(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metodo a traves del cual se invoca la accion de eliminar templates de la BD.
	 *
	 * @param req Objeto con la lista de templates a eliminar
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de eliminar informacion del template
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate eliminarTemplates(BeanReqConsultaTemplates req, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metodo a traves del cual se invoca la accion de obtener template de la BD.
	 *
	 * @param req Objeto con las propiedades a buscar del template
	 * @param session  Objeto general de session @see ArchitechSessionBean
	 * @return  Objeto con la respuesta del proceso de obtener informacion de template
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate obtenerTemplate(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
	
	/**
	 * Metodo a traves del cual s einvoca la accion de actualizar el template en la BD.
	 *
	 * @param req Objeto con las propiedades del template a actualizar
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de actualizar informacion de template
	 * @throws BusinessException the business exception
	 */
	BeanResPrincipalTemplate actualizarTemplate(BeanReqAltaEditTemplate req, ArchitechSessionBean session) throws BusinessException;
	
}
