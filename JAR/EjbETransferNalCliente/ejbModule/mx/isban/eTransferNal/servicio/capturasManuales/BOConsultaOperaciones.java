/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaOperaciones.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/03/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.capturasManuales;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *Clase del tipo BO que se encarga  del negocio de la consulta de operaciones de capturas manuales
**/
@Remote
public interface BOConsultaOperaciones {
	
	
	/**
	 * Consultar operaciones.
	 *
	 * @param session the session
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param nextPage the next page
	 * @return BeanResConsNomCUTDia bean con los datos de respuesta
	 * @throws BusinessException 
	 */
	BeanResConsOperaciones consultarOperaciones(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones, boolean nextPage) throws BusinessException;
	
	/**
	 * Apartar.
	 *
	 * @param session the session
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @return the bean res base
	 * @throws BusinessException 
	 */
	BeanResBase apartar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException;
	
	/**
	 * Desapartar.
	 *
	 * @param session the session
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @return the bean res base
	 * @throws BusinessException 
	 */
	BeanResBase desapartar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException;

	/**
	 * Eliminar.
	 *
	 * @param session the session
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @return the bean res base
	 * @throws BusinessException 
	 */
	BeanResBase eliminar(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException;
	
	/**
	 * Exportar las operaciones.
	 *
	 * @param session the session
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @return the bean res cons operaciones
	 * @throws BusinessException 
	 */
	BeanResConsOperaciones exportarTodo(ArchitechSessionBean session, BeanReqConsOperaciones beanReqConsOperaciones) throws BusinessException;
}
