/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaOperacionesDetalle.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   31/03/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.capturasManuales;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


/**
 *Clase del tipo BO que se encarga  del negocio de la consulta de operaciones de capturas manuales
**/
@Remote
public interface BOConsultaOperacionesDetalle {
	
	/**
	 * Obtener template.
	 *
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param architechSessionBean the architech session bean
	 * @return the bean res cons operaciones
	 * @throws BusinessException 
	 */
	BeanResConsOperaciones obtenerTemplate(
			BeanReqConsOperaciones beanReqConsOperaciones,
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Consulta permiso apartar.
	 *
	 * @param architechSessionBean the architech session bean
	 * @param folio the folio
	 * @return the bean res base
	 * @throws BusinessException 
	 */
	BeanResBase consultaPermisoApartar(ArchitechSessionBean architechSessionBean, String folio) throws BusinessException;
	
	/**
	 * Consulta permiso modificar.
	 *
	 * @param architechSessionBean the architech session bean
	 * @param folio the folio
	 * @return the bean res base
	 * @throws BusinessException 
	 */
	BeanResBase consultaPermisoModificar(ArchitechSessionBean architechSessionBean, String folio) throws BusinessException;
	
	/**
	 * Consulta permiso modificar.
	 *
	 * @param architechSessionBean the architech session bean
	 * @param operacion the operacion
	 * @param camposPistas the campos pistas
	 * @param folio the folio
	 * @return the bean res base
	 */
	BeanResBase guardarOperacion(ArchitechSessionBean architechSessionBean, BeanOperacion operacion, List<Map<String, String>> camposPistas, BeanResConsOperaciones folio);
}