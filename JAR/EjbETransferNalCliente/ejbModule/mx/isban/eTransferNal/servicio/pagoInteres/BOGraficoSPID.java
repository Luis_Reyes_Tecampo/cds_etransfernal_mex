/**
  * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGraficoSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.pagoInteres;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGrafico;

/**
 * Interfaz del tipo BO que se encarga de obtener los datos para la graficación y totales
**/
@Remote
public interface BOGraficoSPID {
	/**
	 * @param req Bean con los parametros del tipo BeanReqGrafico
	 * @param sessionBean Objeto de session de la arquitectura
	 * @return BeanResGrafico bean de respuesta del tipo BeanResGrafico
	 */
	BeanResGrafico consultaDatosSPID(BeanReqGrafico req,ArchitechSessionBean sessionBean);
	
	
	/**
	 * Metodo que consulta los catalogos
	 * @param sesion ArchitechSessionBean Objeto de la arquitectura
	 * @return BeanResGrafico Bean con los datos de respuesta
	 */
	BeanResGrafico consultaCatalogos(ArchitechSessionBean sesion);
	
	 /**
	 * Metodo que permite consultar el rango 
	 * @param sessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResGrafico bean con el rango en minutos
	 */
	BeanResGrafico consultaRango(ArchitechSessionBean sessionBean);

	
}
