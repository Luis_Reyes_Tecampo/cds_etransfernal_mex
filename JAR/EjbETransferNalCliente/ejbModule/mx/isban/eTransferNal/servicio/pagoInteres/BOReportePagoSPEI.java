/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOReportePagoSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/09/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.pagoInteres;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePago;

/**
 * Interfaz del tipo BO que se encarga de obtener los datos para reporte de pago SPID
**/
@Remote
public interface BOReportePagoSPEI {
	
	/**
	 * Metodo que consulta los catalogos
	 * @param sesion ArchitechSessionBean Objeto de la arquitectura
	 * @return BeanResReportePago Bean con los datos de respuesta
	 */
	BeanResReportePago consultaCatalogos(ArchitechSessionBean sesion);
	
	/**
	 * Metodo para realizar la consulta del reporte de pago de interes
	 * @param beanReqReportePago Bean con los parametros para realizar la consulta
	 * @param sesion ArchitechSessionBean Objeto de la arquitectura
	 * @return BeanResReportePago Bean con los datos de respuesta
	 */
	BeanResReportePago consultaReportePago(BeanReqReportePago beanReqReportePago, ArchitechSessionBean sesion);
	
	/**
	 * Metodo para realizar el export del reporte de pago de interes
	 * @param beanReqReportePago Bean con los parametros para realizar la consulta
	 * @param sesion ArchitechSessionBean Objeto de la arquitectura
	 * @return BeanResReportePago Bean con los datos de respuesta
	 */
	BeanResReportePago exportarTodoReportePago(BeanReqReportePago beanReqReportePago, ArchitechSessionBean sesion);
	
}
