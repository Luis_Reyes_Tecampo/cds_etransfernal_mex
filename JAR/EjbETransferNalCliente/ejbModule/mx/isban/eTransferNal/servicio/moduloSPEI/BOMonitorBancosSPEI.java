/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorBancosSPEI.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 11:12:32	    1396920: Moises Navarro                 TCS         Creacion de BOMonitorBancosSPEI.java
 */
package mx.isban.eTransferNal.servicio.moduloSPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.global.BeanNuevoPaginador;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanResponseMonBancSPEIBO;

/**
 * Descripcion:Interface que define los metodos de la capata de negocio para el monitor de bancos SPEI 
 * BOMonitorBancosSPEI.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BOMonitorBancosSPEI.java
*/
@Remote
public interface BOMonitorBancosSPEI {

	/**
	 * Descripcion   : Metodo que realiza la logica de negocio para la consulta del monitor de bancos SPEI
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 19/03/2020
	 * @param session sesion de agave
	 * @param paginador bean de paginacion
	 * @return BeanResponseMonBancSPEIBO bean con la respuesta de la consulta
	 * @throws BusinessException exepcion de negocio
	 */
	BeanResponseMonBancSPEIBO consultaMonitorBancos(ArchitechSessionBean session, BeanNuevoPaginador paginador)
			throws BusinessException;
}
