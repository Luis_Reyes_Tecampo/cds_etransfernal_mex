/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOEmisionRep.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	     By 	     Company 	   Description
 * ------- -----------   -----------    ----------  ----------------------
 * 1.0      18/04/2018 12:05:20 PM Moises Trejo Hernandez. Isban Creacion
 * 2.0      25/04/2018   Salvador Meza     VECTOR       Creacion
 */
package mx.isban.eTransferNal.servicio.SPEICero;

/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */
import javax.ejb.Remote;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;

/**
 * Interface BOEmisionRep.
 *
 * @author mtrejo
 */
@Remote
public interface BOEmisionRep {


	/**
	 * Procedimiento para obtener lo siguiente:
	 *  - Estatus de Operacion 
	 *  - Hora Inicio SPEI CERO
	 *  - Horario cambio fecha operacion .
	 *
	 * @param architechSessionBean the architech session bean
	 * @param indicador the indicador
	 * @param historico
	 * @return the res emision reporte
	 */
	BeanResSpeiCero getResEmisionReporte(ArchitechSessionBean architechSessionBean, int indicador,boolean historico) throws BusinessException;
	
	/**
	 * funcion que realiza la busqueda
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */	
	BeanResSpeiCero buscarDatos(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) throws BusinessException;

	/**
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	BeanResSpeiCero exportarTodo(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) throws BusinessException;

	/**
	 * funcion para exportar edo de cuenta
	 * 
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	BeanResSpeiCero generarEdoCta(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) throws BusinessException;

	
}