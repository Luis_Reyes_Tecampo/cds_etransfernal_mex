/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOAvisoTraspasos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;

/**
 *Clase del tipo BO que se encarga  del negocio para aviso traspasos
**/
@Remote
public interface BOAvisoTraspasos  {

	
	/**
	 * Metodo para obtener aviso de traspasos
	 * @param paginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @param cveInst Objeto del tipo @see String
	 * @param fch Objeto del tipo @see String
	 * @return fch Objeto del tipo  String
	 * @throws BusinessException exception
	 */
	public BeanResAvisoTraspasos obtenerAviso(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType, String cveInst, String fch) 
			throws BusinessException;
	
	
	
	/**
	 * @param beanReqAvisoTraspasos BeanReqAvisoTraspasos
	 * @param architechBean ArchitechSessionBean
	 * @param cveInst String
	 * @param fch String
	 * @return BeanResAvisoTraspasos
	 * @throws BusinessException exception
	 */
	public BeanResAvisoTraspasos exportarTodoAvisoTraspasos(
			BeanReqAvisoTraspasos beanReqAvisoTraspasos, ArchitechSessionBean architechBean, String cveInst, String fch) throws BusinessException;
}
