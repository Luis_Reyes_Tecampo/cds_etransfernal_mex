/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BORecepcion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * Interfaz para el BoRecepcion
 * @author IDS
 *
 */
@Remote
public interface BORecepcion {
	
	/**
	 * @param beanLlave Objeto del tipo BeanSPIDLlave
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResRecepcionSPID
	 * @throws BusinessException exception
	 */
	public BeanResRecepcionSPID obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean) throws BusinessException;
}
