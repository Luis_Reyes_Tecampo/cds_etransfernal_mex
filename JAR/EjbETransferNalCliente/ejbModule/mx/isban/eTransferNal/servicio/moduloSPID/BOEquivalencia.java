/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqEquivalencia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalencia;

/**
 * Interfaz para la equivalencia de datos
 * @author IDS
 *
 */
@Remote
public interface BOEquivalencia {
	
	/**
	 * Metodo para listar las equivalencias
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResEquivalencia listarEquivalencia(BeanPaginador beanPaginador, 
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;
	
	/**
	 * Metodo para guardar equivalencia
	 * @param beanEquivalencia  Objeto del tipo @see BeanEquivalencia
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @return beanPaginador Objeto del tipo  BeanPaginador
	 * @throws BusinessException exception
	 */
	public BeanResEquivalencia guardarEquivalencia(BeanEquivalencia beanEquivalencia, 
			ArchitechSessionBean architechSessionBean, BeanPaginador beanPaginador) throws BusinessException;
	
	/**
	 * Metodo para eliminar equivalencia
	 * @param beanReqEquivalencia Objeto del tipo @see BeanReqEquivalencia
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @return beanPaginador Objeto del tipo  BeanPaginador
	 * @throws BusinessException exception
	 */
	public BeanResEquivalencia eliminarEquivalencia(BeanReqEquivalencia beanReqEquivalencia,
			ArchitechSessionBean architechSessionBean, BeanPaginador beanPaginador) throws BusinessException;
	
	/**
	 * Metodo para la generacion de combos de equivalencia
	 * @return generarCombosEquivalencia Objeto del tipo  BeanResEquivalencia
	 */
	public BeanResEquivalencia generarCombosEquivalencia();

	
	/**
	 * Metodo para guardar para la edicion de equivalencia
	 * @param beanEquivalencia Objeto del tipo @see BeanEquivalencia
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param cveOperacion Objeto del tipo @see String
	 * @param tipoPago Objeto del tipo @see String
	 * @param clasificacion Objeto del tipo @see String
	 * @return clasificacion Objeto del tipo  String
	 * @throws BusinessException exception
	 */
	BeanResEquivalencia guardarEdicionEquivalencia(BeanEquivalencia beanEquivalencia,
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador, String cveOperacion, String tipoPago,
			String clasificacion) throws BusinessException;
}
