package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTraspasoFondosSPIDSIAC;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTraspasoFondosSPIDSIAC;

@Remote
public interface BOTraspasoFondosSPIDSIAC {
	
    /**
     * Metodo que se encarga de realizar la transferencia
     * @param req Objeto del tipo @see BeanReqTraspasoFondosSPIDSIAC
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResTraspasoFondosSPIDSIAC Objeto del tipo @see BeanResTraspasoFondosSPIDSIAC
     */
    public BeanResTraspasoFondosSPIDSIAC traspasoFondosSPIDSIAC(BeanReqTraspasoFondosSPIDSIAC req,ArchitechSessionBean sessionBean);
}
