package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorOpSPID;

@Remote
public interface BORMonitorOpSPID {
	/**
	 * Metodo que sirve para consultar la informacion del monitor operativo SPID
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorOpSPID Objeto del tipo @see BeanResMonitorOpSPID
	 */
	BeanResMonitorOpSPID obtenerDatosMonitorSPID(
			ArchitechSessionBean architectSessionBean);
}
