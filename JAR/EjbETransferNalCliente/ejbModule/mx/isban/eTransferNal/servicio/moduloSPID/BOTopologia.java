/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologia;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTopologia;

/**
 * Interfaz  de  BOTopologia
 * @author IDS
 *
 */
@Remote
public interface BOTopologia {
	/**
	 * Metodo para obtener topologias
	 * @param beanPaginador Objeto del tipo  BeanPaginador
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @param sortField Objeto del tipo  String
	 * @param sortType Objeto del tipo  String
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResTopologia obtenerTopologias(BeanPaginador beanPaginador, 
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;

	/**
	 * Metodo para obtener combosTopologia
	 * @param beanReqTopologia Objeto del tipo  BeanReqTopologia
	 * @param architechBean Objeto del tipo  ArchitechSessionBean
	 * @return architechBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResTopologia obtenerCombosTopologia(ArchitechSessionBean architechBean) throws BusinessException;

	/**
	 * Metodo para guardar la topologia
	 * @param beanTopologia Objeto del tipo  BeanTopologia
	 * @param architechBean Objeto del tipo  ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo  BeanPaginador
	 * @return  beanPaginador Objeto del tipo  BeanPaginador
	 * @throws BusinessException exception
	 */
	public BeanResTopologia guardarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador) throws BusinessException;

	/**
	 * Metodo para eliminar topologias
	 * @param beanReqTopologia Objeto del tipo BeanReqTopologia
	 * @param architechBean Objeto del tipo ArchitechSessionBean
	 * @return architechBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResTopologia eliminarTopologia(BeanReqTopologia beanReqTopologia, 
			ArchitechSessionBean architechBean) throws BusinessException;

	/**
	 * Metodo para guardar ediciones de topologia
	 * @param beanTopologia Objeto del tipo BeanTopologia
	 * @param architechBean Objeto del tipo ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param cveMedio  Objeto del tipo String
	 * @param cveOperacion  Objeto del tipo String
	 * @param toPri  Objeto del tipo String
	 * @return toPri  Objeto del tipo String
	 * @throws BusinessException exception
	 */
	public BeanResTopologia guardarEdicionTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador, 
			String cveMedio, String cveOperacion, String toPri) throws BusinessException;
}
