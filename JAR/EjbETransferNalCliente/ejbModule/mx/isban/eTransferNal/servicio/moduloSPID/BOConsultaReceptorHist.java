/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaReceptorHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsReceptHist;

/**
 *Clase del tipo BO que se encarga  del negocio del flujo consulta receptor historico
**/
@Remote
public interface BOConsultaReceptorHist {
	
	/**Metodo que sirve para consultar los receptores historicos
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsReceptHist Objeto del tipo BeanResConsReceptHist
	   */
	public BeanResConsReceptHist obtenerDatosReceptHist(BeanPaginador beanPaginador,BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para consultar los receptores Ini
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanResConsReceptHist Objeto del tipo BeanResConsReceptHist
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResConsReceptHist obtenerDatosReceptIni(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean)  throws BusinessException;
}
