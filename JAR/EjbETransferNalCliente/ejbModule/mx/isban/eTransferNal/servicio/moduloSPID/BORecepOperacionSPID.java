/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BORecepOperacionSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones
**/
@Remote
public interface BORecepOperacionSPID {

	/**
	 * @param sessionBean Objeto de la arquitectura
	 * @return Bean con la fecha y cve mi institucion
	 */
	public BeanResReceptoresSPID consultaCveMiInstFchOp(ArchitechSessionBean sessionBean);
	/**
     * Metodo que se encarga de realizar la consulta de las operaciones
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo @see BeanResConsMovCdaDAO
     */
    public BeanResReceptoresSPID consultaTodasTransf(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    
    /**
     * Metodo que se encarga de realizar la transferencia
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo @see BeanResConsTranSpeiRec
     */
    public BeanResReceptoresSPID transferirSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    
    
    /**
     * Metodo que se encarga de rechazar la operacion
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo @see BeanResReceptoresSPID
     */
    public BeanResReceptoresSPID rechazarTransfSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    /**
     * Metodo que se encarga de devolver la operacion
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo @see BeanResReceptoresSPID
     */
    public BeanResReceptoresSPID devolverTransferSpeiRec(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    
    /**
     * @param beanReqReceptoresSPID Objeto bean del tipo BeanReqReceptoresSPID
     * @param sessionBean Objeto de la arquitectura ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo BeanResReceptoresSPID
     */
    public BeanResReceptoresSPID recuperarTransfSpeiRec(
    		BeanReqReceptoresSPID beanReqReceptoresSPID,
			ArchitechSessionBean sessionBean);
    
    /**
     * @param beanReqReceptoresSPID Objeto bean del tipo BeanReqReceptoresSPID
     * @param sessionBean Objeto de la arquitectura ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo BeanResReceptoresSPID
     */
    public BeanResReceptoresSPID actMotRechazo(
    		BeanReqReceptoresSPID beanReqReceptoresSPID,
			ArchitechSessionBean sessionBean);
    
    /**Metodo que permite apartar una operacion
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo BeanResReceptoresSPID
     **/
    BeanResReceptoresSPID apartar(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    
    
    /**Metodo que permite desapartar una operacion
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPID Objeto del tipo BeanResReceptoresSPID
     **/
    BeanResReceptoresSPID desApartar(BeanReqReceptoresSPID beanReqReceptoresSPID,ArchitechSessionBean sessionBean);
    
    
}
