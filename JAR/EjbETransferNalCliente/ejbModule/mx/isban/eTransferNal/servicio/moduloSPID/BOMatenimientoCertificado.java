/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOMatenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificado;

/**
 * Interfaz para mantenimiento de certificado
 * @author IDS
 *
 */
@Remote
public interface BOMatenimientoCertificado {

	/**
	 * Metodo que obtiene los certificados registrados.
	 *
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException exception
	 */
	BeanResMantenimientoCertificado obtenerMantenimientoCertificados(BeanPaginador beanPaginador,
			ArchitechSessionBean architechBean, String nomPantalla) throws BusinessException;

	/**
	 * Metodo que elimina un certificado.
	 *
	 * @param beanReqMantenimientoCertificado Objeto del tipo @see BeanReqMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @param beanPaginador El objeto: bean paginador
	 * @return BeanResMantenimientoCertificado
	 */
	BeanResMantenimientoCertificado eliminarTopologia(BeanReqMantenimientoCertificado beanReqMantenimientoCertificado,
			ArchitechSessionBean architechBean, String nomPantalla, BeanPaginador beanPaginador) throws BusinessException;

	/**
	 * Metodo que guarda un ceritifcado .
	 *
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException exception
	 */
	BeanResMantenimientoCertificado guardarCertificado(BeanMantenimientoCertificado beanMantenimientoCertificado,
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador, String nomPantalla) throws BusinessException;
	
	/**
	 * Metodo que guarda la edicion de un certificado.
	 *
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param numeroCertOld Objeto del tipo @see String
	 * @param cveInsOld  Objeto del tipo @see String
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificado
	 * @throws BusinessException exception
	 */
	BeanResMantenimientoCertificado guardarEdicionMantenimiento(BeanMantenimientoCertificado beanMantenimientoCertificado, 
			ArchitechSessionBean architechBean, BeanPaginador beanPaginador, 
			String numeroCertOld, String cveInsOld, String nomPantalla) throws BusinessException;
	
	/**
	 * M�todo que inserta el nombre de la tabla dependiendo del parametro 
	 * que se est� recibiendo para la ejecuci�n de los querys.
	 *
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return TABLA
	 */
	String asignaTablaDB(String nomPantalla);


}
