/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Remote;




import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;

/**
 * Interfaz para el BoPago
 * @author IDS
 *
 */
@Remote
public interface BOPago {
	/**
	 * Metodo para obtener listado de pagos
	 * @param tipoOrden Objeto del tipo String
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResListadoPagos obtenerListadoPagos(
			String tipoOrden, BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean) throws BusinessException;
	/**
	 * Metodo para obtener listado de pagos
	 * @param tipoOrden Objeto del tipo String
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BigDecimal obtenerListadoPagosMontos(
			String tipoOrden, ArchitechSessionBean architechSessionBean) throws BusinessException;
	

	/**
	 * Metodo para exportar todos los listados de pagos
	 * @param tipoOrden Objeto del tipo String
	 * @param beanReqListadoPago Objeto del tipo  BeanReqListadoPagos
	 * @param architechBean  Objeto del tipo  ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResListadoPagos exportarTodoListadoPagos(
			String tipoOrden, BeanReqListadoPagos beanReqListadoPago, ArchitechSessionBean architechBean) throws BusinessException;
	
	/**
	 * Metodo para consultar el detalle del pago
	 * @param referencia Objeto del tipo  String
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanDetallePago consultaDetallePago(String referencia, ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Metodo para buscar pagos
	 * @param field  Objeto del tipo  String
	 * @param valor Objeto del tipo  String
	 * @param tipoOrden Objeto del tipo String
	 * @param paginador Objeto del tipo  BeanPaginador
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @return  architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResListadoPagos buscarPagos(String field, String valor, String tipoOrden, BeanPaginador paginador, ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	
	/**
	 * Metodo para buscar pagos
	 * @param field  Objeto del tipo  String
	 * @param valor Objeto del tipo  String
	 * @param tipoOrden Objeto del tipo String
	 * @param paginador Objeto del tipo  BeanPaginador
	 * @param architechSessionBean Objeto del tipo  ArchitechSessionBean
	 * @return  BigDecimal Sumatoria
	 * @throws BusinessException exception
	 */
	public BigDecimal buscarPagosMonto(String field, String valor, String tipoOrden, ArchitechSessionBean architechSessionBean) throws BusinessException;
}
