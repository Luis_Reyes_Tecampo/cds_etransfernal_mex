/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * Interfaz para la consulta de recepciones
 * @author IDS
 *
 */
@Remote
public interface BOConsultaRecepciones {
	
	  /**Metodo que sirve para consultar las recepciones
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param beanReqConsultaRecepciones Objeto del tipo @see BeanReqConsultaRecepciones
	   * @param cveInst cadena con la clave institucion
	   * @param fch cadena con la fecha operacion
	   * @return BeanResConsultaRecepciones Objeto del tipo BeanResConsultaRecepciones
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResConsultaRecepciones obtenerConsultaRecepciones(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch) throws BusinessException;

	/**
	 * @param beanLlave Objeto del tipo BeanSPIDLlave
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResRecepcionSPID
	 * @throws BusinessException Exception
	 */
	public BeanResRecepcionSPID obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	  /**Metodo que sirve para exportar todo de las recepciones
	   * @param beanReqConsultaRecepciones Objeto del tipo @see BeanReqConsultaRecepciones
	   * @param architechBean Objeto del tipo @see ArchitechSessionBean
	   * @param cveInst  Objeto del tipo @see String
	   * @param fch Objeto del tipo @see String
	   * @return BeanResConsultaRecepciones Objeto del tipo BeanResConsultaRecepciones
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResConsultaRecepciones exportarTodoConsultaRecepciones(
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, ArchitechSessionBean architechBean, String cveInst, String fch) throws BusinessException;
	
	
	  /**Metodo que sirve para exportar todo de las recepciones
	   * @param beanReqConsultaRecepciones Objeto del tipo @see BeanReqConsultaRecepciones
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param cveInst cadena con la clave institucion
	   * @param fch cadena con la fecha operacion
	   * @return BeanResConsultaRecepciones Objeto del tipo BeanResConsultaRecepciones
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResConsultaRecepciones enviarConsultaRecepciones(
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, ArchitechSessionBean architechSessionBean, String cveInst, String fch) throws BusinessException;

}
