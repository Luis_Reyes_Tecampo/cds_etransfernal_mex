/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOInitModuloSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanInitModuloSPID;

/**
 * Interfaz para el modulo de SPID.
 *
 * @author IDS
 */
@Remote
public interface BOInitModuloSPID {
	
	/**
	 * Metodo para la obtencion de los header.
	 *
	 * @param tipo El objeto: tipo de modulo SPEI o SPID
	 * @return BeanInitModuloSPID Objeto del tipo obtenerInformacionHeader
	 * @throws BusinessException exception
	 */
	BeanInitModuloSPID obtenerInformacionHeader(String tipo)  throws BusinessException ;
	
	/**
	 * El metodo reinicia la session en caso de no existir.
	 *
	 * @return BeanSessionSPID
	 * @throws BusinessException exception
	 */
	BeanSessionSPID beanCheckSessionSPID() throws BusinessException;
	
	/**
	 * Bean check session SPEI.
	 * 
	 * Metodo para validar la existencia de la sessuin SPEI
	 * en caso de no existir 
	 *
	 * @return Objeto bean session SPID
	 * @throws BusinessException La business exception
	 */
	BeanSessionSPID beanCheckSessionSPEI() throws BusinessException;
}
