/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;

/**
 * Interfaz para la obtencion del historial de mensajes
 * @author IDS
 *
 */
@Remote
public interface BOHistorialMensajes {

	
	/**
	 * Metodo para obtener el historial de los mensajes
	 * @param paginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanReqHistorialMensajes Objeto del tipo @see BeanReqHistorialMensajes
	 * @return beanReqHistorialMensajes Objeto del tipo  BeanReqHistorialMensajes
	 */
	public BeanResHistorialMensajes obtenerHistorial(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes);

}
