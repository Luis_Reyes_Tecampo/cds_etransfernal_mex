/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConfiguracionParametros.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametros;

/**
 * Interfaz de configuracion de parametros
 * @author IDS
 *
 */
@Remote
public interface BOConfiguracionParametros {
	
	/**
	 * Metodo para obtener configuracion de parametros.
	 *
	 * @param cveInstitucion Objeto del tipo Long
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	BeanResConfiguracionParametros obtenerConfiguracionParametros(
			Long cveInstitucion, ArchitechSessionBean architechSessionBean, String nomPantalla) throws BusinessException;
	
	/**
	 * Metodo para guardar configuracion de parametros.
	 *
	 * @param configuracionParametros Objeto del tipo BeanConfiguracionParametros
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	BeanResConfiguracionParametros guardarConfiguracionParametros(
			BeanConfiguracionParametros configuracionParametros, ArchitechSessionBean architechSessionBean, String nomPantalla) throws BusinessException;


}
