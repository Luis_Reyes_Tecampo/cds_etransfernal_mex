/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOOrdenReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;

/**
 * Interfaz para la ordenes de reparacion
 * @author IDS
 *
 */
@Remote
public interface BOOrdenReparacion {
	/**
	 * Metodo para  la obtencion de ordenes de reparacion
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResOrdenReparacion obtenerOrdenesReparacion(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;
	
	/**
	 * Metodo para  la obtencion de ordenes de reparacion Montos
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BigDecimal obtenerOrdenesReparacionMontos(
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;

	/**
	 * Metodo para exportar todas las ordenes de reparacion
	 * @param beanReqOrdeReparacion  Objeto del tipo @see BeanReqOrdeReparacion
	 * @param architechBean  Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResOrdenReparacion exportarTodoOrdenesReparacion(
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechBean) throws BusinessException;
	
	/**
	 * Metodo para el envio de ordenes de reparacion
	 * @param beanReqOrdeReparacion Objeto del tipo @see BeanReqOrdeReparacion
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @throws BusinessException exception
	 */
	public BeanResOrdenReparacion enviarOrdenesReparacion(
			BeanReqOrdeReparacion beanReqOrdeReparacion, ArchitechSessionBean architechSessionBean) throws BusinessException;
}
