/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BODetRecepOpSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqGuardarSPIDDet;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPID;

@Remote
public interface BODetRecepOpSPID {
	
	/**
	 * @param beanReqGuardarSPIDDet del tipo BeanReqGuardarSPIDDet bean con los datos modificados y anteriores
	 * @param sessionBean Objeto de la ArchitechSessionBean 
	 * @return BeanResReceptoresSPID bean de respuesta
	 */
	BeanResReceptoresSPID guardarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet, ArchitechSessionBean sessionBean);
	
	/**
	 * @param beanReqGuardarSPIDDet Bean con los datos de consulta
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResReceptoresSPID Bean de respuesta
	 */
	BeanResReceptoresSPID consultarDetRecepOp(BeanReqGuardarSPIDDet beanReqGuardarSPIDDet, ArchitechSessionBean sessionBean);
	
	

}
