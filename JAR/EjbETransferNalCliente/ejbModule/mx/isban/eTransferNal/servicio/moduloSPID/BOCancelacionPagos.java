/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResCancelacionPagos;

/**
 * Interfaz  de cancelcion de pagos
 * @author IDS
 *
 */
@Remote
public interface BOCancelacionPagos {
	
	/**Metodo que sirve para consultar cancelacion de apagos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResCancelacionPagos obtenerCancelacionPagos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)  throws BusinessException;
	/**Metodo que sirve para consultar cancelacion de pagos filtrados por traspasos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResCancelacionPagos obtenerCancelacionPagosTraspasos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)  throws BusinessException;
	/**Metodo que sirve para consultarla cancelacion de pagos filtrados por orden
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */

	public BeanResCancelacionPagos obtenerCancelacionPagosOrden(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType)  throws BusinessException;
	/**Sirve para exportar la informacion
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo @see architechSessionBean
	   * @param opcion Objeto del tipo String
	   * @return beanResCancelacionPagos Objeto del tipo BeanResCancelacionPagos
	   * @throws BusinessException exception
	   */
	
	public BeanResCancelacionPagos exportarTodoCancelacionPagos(BeanReqCancelacionPagos beanReqCancelacionPagos, ArchitechSessionBean architechSessionBean, String opcion) throws BusinessException ;

	/**Metodo que sirve para actualizar una cancelacion de pago
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResCancelacionPagos cancelar(
			BeanReqCancelacionPagos beanReqCancelacionPagos, ArchitechSessionBean architechSessionBean) throws BusinessException;
	
}
