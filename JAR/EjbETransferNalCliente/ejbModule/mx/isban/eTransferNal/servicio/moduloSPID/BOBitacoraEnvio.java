/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOBitacoraEnvio.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvio;

/**
 *Clase del tipo BO que se encarga  del negocio del flujo de la bitacora de envío
**/
@Remote
public interface BOBitacoraEnvio {
	
	/**Metodo que sirve para consultar los envios
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResBitacoraEnvio obtenerBitacoraEnvio(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;

	/**Metodo que sirve para exportar todos los envios
	   * @param beanReqBitacoraEnvio Objeto del tipo @see BeanReqBitacoraEnvio
	   * @param architechBean Objeto del tipo @see ArchitechSessionBean
	   * @return architechBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResBitacoraEnvio exportarTodoBitacoraEnvio(
			BeanReqBitacoraEnvio beanReqBitacoraEnvio, ArchitechSessionBean architechBean) throws BusinessException;
	
}
