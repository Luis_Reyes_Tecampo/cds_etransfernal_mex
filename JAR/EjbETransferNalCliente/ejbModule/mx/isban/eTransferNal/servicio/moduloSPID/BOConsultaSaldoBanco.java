/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOConsultaSaldoBanco.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBanco;

/**
 *Clase del tipo BO que se encarga  del negocio del flujo de la consulta de saldos por banco
**/
@Remote
public interface BOConsultaSaldoBanco {
	
	/**Metodo que sirve para consultar los saldos
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResConsultaSaldoBanco obtenerConsultaSaldoBanco(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;
	
	
	/**Metodo que sirve para consultar los saldos Total
	   
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BigDecimal obtenerConsultaSaldoBancoMontos(
			ArchitechSessionBean architechSessionBean, String sortField, String sortType) throws BusinessException;
	
	/**Metodo que sirve para exportar todos los saldos
	   * @param beanReqConsultaSaldoBanco Objeto del tipo @see BeanReqConsultaSaldoBanco
	   * @param architechBean Objeto del tipo @see ArchitechSessionBean
	   * @return architechBean Objeto del tipo ArchitechSessionBean
	   * @throws BusinessException exception
	   */
	public BeanResConsultaSaldoBanco exportarTodoConsultaSaldoBanco(
			BeanReqConsultaSaldoBanco beanReqConsultaSaldoBanco, ArchitechSessionBean architechBean) throws BusinessException;
}
