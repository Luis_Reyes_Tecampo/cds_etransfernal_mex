/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 16:08:14 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico
**/

public interface BOGenerarArchCDAHist{

   /**Metodo que sirve para consultar la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean)
       throws BusinessException;
   /**Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAHist Objeto del tipo BeanResProcGenArchCDAHist
    * @exception BusinessExceptionException de negocio
    */
    public BeanResProcGenArchCDAHist procesarGenArchCDAHist(BeanReqGenArchCDAHist beanReqGenArchCDAHist, 
    		ArchitechSessionBean architechSessionBean)
        throws BusinessException;

   


}
