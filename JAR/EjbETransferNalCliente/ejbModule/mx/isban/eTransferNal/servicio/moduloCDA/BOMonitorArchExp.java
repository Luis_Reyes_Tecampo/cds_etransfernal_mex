/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorArchExp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 17:42:48 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExp;


/**
 *Clase del tipo BO que se encarga  del negocio del Monitor de
 * archivos a exportar
**/

public interface BOMonitorArchExp{

   /**Metodo que permite consultar el monitor de archivos exportar
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsMonitorArchExp Objeto del tipo BeanResConsMonitorArchExp
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsMonitorArchExp consultarMonitorArchExp(BeanPaginador beanPaginador, 
		   ArchitechSessionBean architechSessionBean)
       throws BusinessException;



}
