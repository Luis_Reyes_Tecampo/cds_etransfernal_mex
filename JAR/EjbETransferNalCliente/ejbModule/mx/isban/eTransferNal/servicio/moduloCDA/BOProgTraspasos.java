/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOProgTraspasos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosBO;

/**
 *Clase del tipo BO que se encarga  del negocio de la
 *programacion de transferencias
**/
@Remote
public interface BOProgTraspasos {
	
	 /**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios  de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanProgTraspasosBO consultaHorariosTransferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanProgTraspasosBO agregaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios de transferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosBO Objeto del tipo BeanProgTraspasosBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanProgTraspasosBO eliminaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;

}
