/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOContingMismoDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:32:53 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDia;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDia;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Contingencia CDA Mismo Dia
**/
public interface BOContingMismoDia{

   /**Metodo de negocio que calcula la pagina y que hace el llamado
   * al dao para obtener la informacion para la funcionalidad de Contingencia
   * CDA mismo dia
   * @param beanPaginacion Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResContingMismoDia Objeto del tipo BeanResContingMismoDia
   * @exception BusinessExceptionException de negocio
   */
   public BeanResContingMismoDia consultaContingMismoDia(final BeanPaginador beanPaginacion, 
		   final ArchitechSessionBean architechSessionBean)
       throws BusinessException;

   /**Metodo que sirve para indicar que se debe de generar el archivo
    * de contingencia mismo dia
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchContingMismoDia Objeto del tipo BeanResGenArchContingMismoDia
    * @exception BusinessExceptionException de negocio
    */
    public BeanResGenArchContingMismoDia genArchContigMismoDia(ArchitechSessionBean architechSessionBean)
        throws BusinessException;

	/**
	 * Metodo DAO para obtener la informacion para la funcionalidad de
	 * Contingencia CDA mismo dia
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResContingMismoDiaDAO Objeto del tipo
	 *         BeanResContingMismoDiaDAO
	 * @exception BusinessExceptionException de negocio
	 */
	public BeanResContingMismoDia consultaPendientesMismoDia(
			ArchitechSessionBean architechSessionBean) throws BusinessException;

}
