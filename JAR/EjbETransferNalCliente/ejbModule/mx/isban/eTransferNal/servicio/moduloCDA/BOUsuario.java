/**
* ISBAN M�xico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BORUsuarioEJB.java
*
* Control de versiones:
*
* Version Date/Hour           By       Company Description
* ------- ------------------- -------- ------- --------------
* 1.0     23/06/2011 12:07:22 O Acosta ISBAN   Creacion
*
*/
package mx.isban.eTransferNal.servicio.moduloCDA;
 
import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

/**
 * Objeto de negocio para el modulo de usuarios con acceso a
 * Transfer.
 *
 */
@Remote
public interface BOUsuario {

    /**
     * Identifica los datos en sesion del usuario en el sistema, para
     * despues realizar las operaciones pertinentes.
     * @param sessionBean la sesion del usuario.
     * @throws BusinessException si ocurre algun error.
     **/
    public void guardarUsuario(ArchitechSessionBean sessionBean)
            throws BusinessException;

  
}
