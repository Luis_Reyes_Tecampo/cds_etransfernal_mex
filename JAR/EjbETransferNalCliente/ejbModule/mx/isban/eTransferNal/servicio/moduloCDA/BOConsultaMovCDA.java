/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   12/12/2013 16:58:43 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCDA;

/**
 * Interfaz del tipo BO que se encarga del negocio para la consulta de
 * Movimientos CDAs
 * 
 */
@Remote
public interface BOConsultaMovCDA {

	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConsMovCDA Objeto del tipo @see BeanResConsMovCDA
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovCDA consultarMovCDA(
			BeanReqConsMovCDA beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean)throws BusinessException;

	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConsMovCDA Objeto del tipo @see BeanResConsMovCDA
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovCDA consultaMovCDAExpTodos(
			BeanReqConsMovCDA beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean)throws BusinessException;

	/**
	 * Metodo que sirve para solicitar el detalle de un movimiento CDA
	 * @param beanReqConsMovCDADet Objeto del tipo @see BeanReqConsMovCDADet
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetCDA Objeto del tipo @see BeanResConsMovDetCDA
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovDetCDA consMovDetCDA(
			BeanReqConsMovCDADet beanReqConsMovCDADet,
			ArchitechSessionBean architechSessionBean)throws BusinessException;
	
	/**
	 * Metodo para consultar la fecha de operacion de Banco de Mexico
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param esSPID Indica si es SPID
	 * @return BeanResConsMovCDA Objeto del tipo @see BeanResConsFechaOp
	 * @throws BusinessException Excepcion de negocio
	 */
	public BeanResConsMovCDA consultarFechaOperacion(ArchitechSessionBean sessionBean, boolean esSPID)
	throws BusinessException;	

}
