/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOHorariosSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;

/**
 * Clase del tipo BO que se encarga  del negocio del detalle
 * de recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
@Remote
public interface BODetRecepOperacion {
	
	/**
	 * Metodo Update que se encarga de actualizar la cuenta Receptora.
	 *
	 * @param architechSessionBean El objeto: architech session bean
	 * @param bean El objeto: bean
	 * @param historico El objeto: historico
	 * @param ctaRecepAnt El objeto: cta recep ant
	 * @return Objeto bean det recep operacion DAO
	 * @throws BusinessException La business exception
	 */
	BeanDetRecepOperacionDAO updateCtaReceptora(ArchitechSessionBean architechSessionBean, BeanDetRecepOperacionDAO bean, boolean historico,String ctaRecepAnt) throws BusinessException;
	
}
