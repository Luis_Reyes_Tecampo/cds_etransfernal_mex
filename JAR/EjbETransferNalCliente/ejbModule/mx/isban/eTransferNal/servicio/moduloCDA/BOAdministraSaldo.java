/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOAdministraSaldo.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranfCtaAltPrincBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranfCtaAltPrincBO;

/**
 *Clase del tipo BO que se encarga  del negocio del flujo de administracion de saldos
**/
@Remote
public interface BOAdministraSaldo {


  /**Metodo que sirve para consultar los saldos
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
   */
	public BeanAdministraSaldoBO obtenerSaldos(ArchitechSessionBean architechSessionBean);
	
	 /**Metodo que sirve para enviar monto desde la cuenta alterna
	   * @param beanReqTranfCtaAltPrincBO Objeto del tipo @see BeanReqTranfCtaAltPrincBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResTranfCtaAltPrincBO Objeto del tipo BeanResTranfCtaAltPrincBO
	   */
	public BeanResTranfCtaAltPrincBO envioAlternaPrincipal(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,ArchitechSessionBean architechSessionBean);
	
	 /**Metodo que sirve para enviar monto desde la cuenta principal 
	   * @param beanReqTranfCtaAltPrincBO Objeto del tipo @see BeanReqTranfCtaAltPrincBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResTranfCtaAltPrincBO Objeto del tipo BeanResTranfCtaAltPrincBO
	   */
	public BeanResTranfCtaAltPrincBO envioPrincipalAlterna(BeanReqTranfCtaAltPrincBO beanReqTranfCtaAltPrincBO,ArchitechSessionBean architechSessionBean);

}
