/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     5/09/2019 06:39:46 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

// TODO: Auto-generated Javadoc
/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
@Remote
public interface BORecepOperacion {

	
	 /**
 	 * Consulta todas transf.
 	 *
 	 * @param beanResTranSpeiRec the bean res tran spei rec
 	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
 	 * @param sessionBean El objeto: session bean
 	 * @param historico El objeto: historico
 	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
 	 * @return Objeto bean res cons tran spei rec
 	 * @throws BusinessException La business exception
 	 */
 	BeanResTranSpeiRec consultaTodasTransf(BeanResTranSpeiRec beanResTranSpeiRec, BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico,BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException;

	/**
	 * Consulta listas.
	 *
	 * @param beanResConsTranSpeiRec El objeto: bean res cons tran spei rec
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param session El objeto: session
	 * @param historico El objeto: historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec consultaListas(BeanResTranSpeiRec beanResConsTranSpeiRec, BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean session, boolean historico, BeanTranSpeiRecOper beanTranSpeiRecOper)
			throws BusinessException;
    
  	
 	
   
    
    
}
