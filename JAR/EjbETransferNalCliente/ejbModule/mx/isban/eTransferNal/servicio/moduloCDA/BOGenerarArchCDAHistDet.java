/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 17:25:07 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDet;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico Detalle
**/

public interface BOGenerarArchCDAHistDet{

	   /**Metodo que sirve para consultar la informacion de la generacion
	   * de archivos CDA historico detalle
	   * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResConsGenArchHistDet Objeto del tipo BeanResConsGenArchHistDet
	   * @exception BusinessExceptionException de negocio
	   */
	   public BeanResConsGenArchHistDet consultaGenArchHistDet(BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet, 
			   BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean)
	       throws BusinessException;

   /**Metodo que sirve para marcar los registros como eliminados
   * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResElimSelecArchCDAHistDet Objeto del tipo BeanResElimSelecArchCDAHistDet
   * @exception BusinessExceptionException de negocio
   */
   public BeanResElimSelecArchCDAHistDet elimSelGenArchCDAHistDet(BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet, 
		   ArchitechSessionBean architechSessionBean)
       throws BusinessException;

   /**Metodo que permite generar el archivo CDA Historico detalle
    * @param beanReqGenArchCDAHistDet Objeto del tipo @see BeanReqGenArchCDAHistDet
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDet Objeto del tipo BeanResGenArchCDAHistDet
    * @exception BusinessExceptionException de negocio
    */
    public BeanResGenArchCDAHistDet generarArchCDAHistDet(BeanReqGenArchCDAHistDet beanReqGenArchCDAHistDet, 
    		ArchitechSessionBean architechSessionBean)
        throws BusinessException;


}
