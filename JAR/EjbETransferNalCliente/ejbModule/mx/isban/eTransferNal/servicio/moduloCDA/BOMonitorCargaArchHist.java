/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonitorCargaArchHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:09:46 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHist;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * del Monitor de carga de archivos historico
**/

public interface BOMonitorCargaArchHist{

   /**Metodo que sirve para consultar la informacion del monitor de
   * carga de archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchHist Objeto del tipo BeanResMonCargaArchHist
   * @exception BusinessExceptionException de negocio
   */
   public BeanResMonCargaArchHist consultarMonitorCargaArchHist(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean)
       throws BusinessException;



}
