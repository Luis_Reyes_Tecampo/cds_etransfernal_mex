package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfil;

/**
 * Interfaz del tipo BO que se encarga  del negocio del perfilamiento
**/
public interface BOPerfilamiento {
	
	/**
	 * Metodo que permite consultar los servicios y tareas que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfil Objeto del tipo BeanResConsPerfil
	 * @exception BusinessException exception de negocio
	 */
	public BeanResConsPerfil consultaServicio(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Metodo que permite consultar las tareas que contiene un perfil en un servicio
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfil Objeto del tipo BeanResConsPerfil
	 * @exception BusinessException exception de negocio
	 */
	public BeanResConsPerfil consultaServicioTarea(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean) throws BusinessException;

}
