/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOBitacoraAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 09:55:49 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsReqBitacoraAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmon;


/**
 *Clase del tipo BO que se encarga  del negocio de la bitacora
 * administrativa
**/
public interface BOBitacoraAdmon{

   /**Metodo que sirve para consultar la informacion de la bitacora
   * administrativa
   * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsBitAdmon Objeto del tipo BeanResConsBitAdmon
   * @exception BusinessExceptionException de negocio
   */
   public BeanResConsBitAdmon consultaBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean)
       throws BusinessException;

   /**Metodo que sirve para realizar el exportar todo
    * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResConsBitAdmon Objeto del tipo BeanResConsBitAdmon
    * @exception BusinessExceptionException de negocio
    */
    public BeanResConsBitAdmon expTodosBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean)
    throws BusinessException;


}
