package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqMonCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;

/**
 * Interfaz del tipo BO que se encarga  del negocio del monitor CDA
**/
@Remote
public interface BOMonitorCDAHist {
	
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param beanReqMonCDAHist Objeto del tipo @see BeanReqMonCDAHist
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaHistBO Objeto del tipo @see BeanResMonitorCdaHistBO
	 * @exception BusinessException Excepcion de negocio
	 */
	public BeanResMonitorCdaHistBO obtenerDatosMonitorCDA(BeanReqMonCDAHist beanReqMonCDAHist,ArchitechSessionBean architectSessionBean) 
	throws BusinessException;


}
