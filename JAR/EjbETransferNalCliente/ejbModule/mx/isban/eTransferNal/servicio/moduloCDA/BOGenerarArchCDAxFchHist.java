/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 16:08:14 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;


/**
 * Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico.
 *
 * @author FSW-Vector
 * @since 14/01/2019
 */
@Remote
public interface BOGenerarArchCDAxFchHist{

   /**
    * Metodo que sirve para consultar la fecha de operacion.
    *
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
    * @throws BusinessException La business exception
    */
   BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean)
       throws BusinessException;
   
   /**
    * Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico.
    *
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAxFchHist Objeto del tipo BeanResProcGenArchCDAxFchHist
    * @throws BusinessException La business exception
    */
    BeanResProcGenArchCDAxFchHist procesarGenArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, 
    		ArchitechSessionBean architechSessionBean)
        throws BusinessException;

}
