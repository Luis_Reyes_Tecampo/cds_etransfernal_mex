/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BODetalleOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/10/2018 05:14:56 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.servicio.contingencia;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.contingencia.BeanResTranCanales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


/**
 * Interface BODetalleOperacion.
 * 
 * Interface que define los metodos utilizados por el controller
 * 
 * @author FSW-Vector
 * @since 10/10/2018
 */
@Remote
public interface BODetalleOperacion {

		
	
	/**
	 * Consultar.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param beanFilter El objeto: bean filter
	 * @param beanPaginador El objeto: bean paginador
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @return Objeto bean res tran canales
	 * @throws BusinessException La business exception
	 */
	BeanResTranCanales consultar(ArchitechSessionBean session, String nombreArchivo, BeanFilter beanFilter, BeanPaginador beanPaginador, String nombrePantalla)
			throws BusinessException;
	
	/**
	 * Exportar todos los registros.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param beanFilter El objeto: bean filter
	 * @param totalRegistros El objeto: total registros
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session, String nombreArchivo, BeanFilter beanFilter, int totalRegistros,String nomPantalla)
			throws BusinessException;
}
