/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOCargaArchivoRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.contingencia;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.contingencia.BeanConsultaOperContingentes;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanales;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *  BO para la carga de nombre de archivos respuesta.
 *
 * @author FSW-Vector
 * @since 22/10/2018
 */
@Remote
public interface BOConsultaOperContingentes {
	
	/**
	 * Consultar oper contingentes.
	 *
	 * @param architectSessionBean El objeto: architect session bean
	 * @param beanPaginador El objeto: bean paginador
	 * @param bean El objeto: bean
	 * @return Objeto bean res consulta oper contingente
	 * @throws BusinessException La business exception
	 */
	BeanResConsultaOperContingente consultarOperContingentes(ArchitechSessionBean architectSessionBean,BeanPaginador beanPaginador,BeanReqConsultaOperContingente bean )  throws BusinessException ;

	/**
	 * Carga archivo.
	 *
	 * @param beanResCanales El objeto: bean res canales
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res cons canales
	 * @throws BusinessException La business exception
	 */
	BeanResConsCanales cargaArchivo(BeanConsultaOperContingentes beanResCanales, ArchitechSessionBean session,BeanReqConsultaOperContingente bean)  throws BusinessException ;
	
	/**
	 * Actualiza estatus.
	 *
	 * @param beanResCanales El objeto: bean res canales
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res cons canales
	 * @throws BusinessException La business exception
	 */
	BeanResConsCanales actualizaEstatus(BeanConsultaOperContingentes beanResCanales, ArchitechSessionBean session,BeanReqConsultaOperContingente bean)  throws BusinessException ;
	
	
	/**
	 * Actualizar nombre arch procesar.
	 *
	 * @param beanResCanales El objeto: bean res canales
	 * @param session El objeto: session
	 * @param nomPantalla El objeto: nom pantalla
	 * @return Objeto bean res cons canales
	 * @throws BusinessException La business exception
	 */
	BeanResConsCanales actualizarNombreArchProcesar(BeanConsultaOperContingentes beanResCanales, ArchitechSessionBean session,String nomPantalla)  throws BusinessException ;

	/**
	 * Obtener medio entrega.
	 *
	 * @param session El objeto: session
	 * @return Objeto bean res cons canales
	 * @throws BusinessException La business exception
	 */
	BeanResConsCanales obtenerMedioEntrega(ArchitechSessionBean session)  throws BusinessException ;	

}
