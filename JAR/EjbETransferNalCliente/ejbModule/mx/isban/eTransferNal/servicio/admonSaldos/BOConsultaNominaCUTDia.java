/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaNominaCUTDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   20/11/2016     IDS 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.admonSaldos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTDia;

/**
 *Clase del tipo BO que se encarga  del negocio de la consulta de la nomina cut del dia
**/
@Remote
public interface BOConsultaNominaCUTDia {
	
	
	/**
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResConsNomCUTDia bean con los datos de respuesta
	 */
	BeanResConsNomCUTDia consultar(ArchitechSessionBean sessionBean);
	
	/**
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResConsNomCUTDia bean con los datos de respuesta
	 */
	BeanResConsNomCUTDia consultarNomina(ArchitechSessionBean sessionBean);
}
