/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaNominaCUTDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   20/11/2016     IDS 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.admonSaldos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldo.BeanReqConsNominaCUT;
import mx.isban.eTransferNal.beans.admonSaldo.BeanResConsNomCUTHist;

/**
 *Clase del tipo BO que se encarga  del negocio de la consulta de la nomina cut del dia
**/
@Remote
public interface BOConsultaNominaCUTHist {
	
	/**
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResConsNomCUTDia bean con los datos de respuesta
	 */
	BeanResConsNomCUTHist consultar(ArchitechSessionBean sessionBean);
	
	/**
	 * @param req Bean con los datos de entrada
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResConsNomCUTDia bean con los datos de respuesta
	 */
	BeanResConsNomCUTHist consultarNomina(BeanReqConsNominaCUT req,ArchitechSessionBean sessionBean);
}
