/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepOperacionExportarTodo.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   3/10/2018 05:48:56 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;


/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Remote
public interface BORecepOperacionExportarTodo {
	
	
	/**
	 * Exportar el total de registros.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param recepOperaAdmonSaldos El objeto: recep opera admon saldos
	 * @param beanReqConsTranSpei El objeto: bean req cons tran spei
	 * @param historico El objeto: historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec exportarTodo(ArchitechSessionBean sessionBean,BeanResTranSpeiRec recepOperaAdmonSaldos,
			BeanReqTranSpeiRec beanReqConsTranSpei,boolean historico,BeanTranSpeiRecOper beanTranSpeiRecOper) throws BusinessException;

   
    
    
}
