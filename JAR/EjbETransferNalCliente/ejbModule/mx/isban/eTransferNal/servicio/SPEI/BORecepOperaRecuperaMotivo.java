package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
@Remote
public interface BORecepOperaRecuperaMotivo {

    
  	/**
	   * Recuperar transf spei rec.
	   *
	   * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	   * @param sessionBean El objeto: session bean
	   * @param historico El objeto: historico
	   * @return Objeto bean res cons tran spei rec
	   * @throws BusinessException La business exception
	   */
	  BeanResTranSpeiRec recuperarTransfSpeiRec(
				BeanReqTranSpeiRec beanReqTranSpeiRec,
				ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;
    
    
    /**
     * Act mot rechazo.
     *
     * @param beanReqTranSpeiRec Objeto bean del tipo BeanReqConsTranSpeiRec
     * @param sessionBean Objeto de la arquitectura ArchitechSessionBean
     * @param historico the historico
     * @return BeanResConsTranSpeiRec Objeto del tipo BeanResConsTranSpeiRec
     * @throws BusinessException La business exception
     */
	 BeanResTranSpeiRec actMotRechazo(
			BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico)  throws BusinessException;

	 
	
    

   
    
    
}
