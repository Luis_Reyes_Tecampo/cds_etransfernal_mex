/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepOperacionDevolver.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   24/10/2018 11:19:16 AM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 24/10/2018
 */
@Remote
public interface BORecepOperacionDevolver {

	
     /**
      * Devolver transfer spei rec.
      *
      * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
      * @param sessionBean El objeto: session bean
      * @param historico El objeto: historico
      * @return Objeto bean res cons tran spei rec
      * @throws BusinessException La business exception
      */
     BeanResTranSpeiRec devolverTransferSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;
     
    
	 /**
 	 * Envia.
 	 *
 	 * @param esDevolucion El objeto: es devolucion
 	 * @param datos El objeto: datos
 	 * @param sessionBean El objeto: session bean
 	 * @param historico El objeto: historico
 	 * @param nombrePant El objeto: nombre pant
 	 * @param beanTranSpei El objeto: bean tran spei
 	 * @return Objeto bean tran spei rec
 	 * @throws BusinessException La business exception
 	 */
 	BeanTranSpeiRecOper envia(boolean esDevolucion, String[] datos, ArchitechSessionBean sessionBean, boolean historico,
			String nombrePant, BeanTranSpeiRecOper beanTranSpei) throws BusinessException;
	 
	
     
 	
	
	/**
	 * Insertar pistas bitacora.
	 *
	 * @param datos El objeto: datos
	 * @param envio El objeto: envio
	 * @param historico El objeto: historico
	 * @param sessionBean El objeto: session bean
	 * @throws BusinessException La business exception
	 */
	void insertarPistasBitacora(String datos, Boolean envio, boolean historico, ArchitechSessionBean sessionBean)
			throws BusinessException;
	
	
}
