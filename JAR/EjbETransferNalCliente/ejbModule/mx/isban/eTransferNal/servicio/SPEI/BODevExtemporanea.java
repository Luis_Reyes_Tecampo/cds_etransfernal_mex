package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;


/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 5/10/2018
 */
@Remote
public interface BODevExtemporanea {

	
    
   
	/**
	 * Devolucion extemporanea.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec devolucionExtemporanea(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;
	
	
	
	/**
	 * Nuevo registro devo.
	 *
	 * @param historico El objeto: historico
	 * @param sessionBean El objeto: session bean
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @param beanTranSpei El objeto: bean tran spei
	 * @param pundoDo El objeto: pundo do
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanDetRecepOperacionDAO nuevoRegistroDevo(boolean historico, ArchitechSessionBean sessionBean,String nombrePantalla,BeanTranSpeiRecOper beanTranSpei, String pundoDo, BeanReqTranSpeiRec beanReqTranSpeiRec) throws BusinessException;
	
	
	/**
	 * Enviar HTO.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec enviarHTO(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;
}
