/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepOpApartada.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   20/09/2018 06:08:52 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;


/**
 * Interface BORecepOpApartada.
 *
 * @author FSW-Vector
 * @since 20/09/2018
 */
@Remote
public interface BORecepOpApartada {

	/**
	 * Aparta operaciones.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec apartaOperaciones(BeanReqTranSpeiRec beanReqTranSpeiRec, ArchitechSessionBean sessionBean,
			boolean historico) throws BusinessException;

	/**
	 * Aparta operaciones eliminar.
	 *
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res cons tran spei rec
	 * @throws BusinessException La business exception
	 */
	BeanResTranSpeiRec apartaOperacionesEliminar(BeanReqTranSpeiRec beanReqTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;

	

}
