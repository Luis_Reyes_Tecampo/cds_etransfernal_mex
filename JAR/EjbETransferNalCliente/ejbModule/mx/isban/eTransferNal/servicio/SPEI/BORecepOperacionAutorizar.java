package mx.isban.eTransferNal.servicio.SPEI;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;

/**
 * Interfaz del tipo BO que se encarga  del negocio de la recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 7/02/2019
 */
@Remote
public interface BORecepOperacionAutorizar {

	
    /**
     * Transferir spei rec.
     *
     * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
     * @param sessionBean El objeto: session bean
     * @param historico El objeto: historico
     * @return Objeto bean ry ques cons tran spei rec
     * @throws BusinessException La business exception
     */
	BeanResTranSpeiRec transferirSpeiRec(BeanReqTranSpeiRec beanReqTranSpeiRec,ArchitechSessionBean sessionBean, boolean historico) throws BusinessException;
  
}
