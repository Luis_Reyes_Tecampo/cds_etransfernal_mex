/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOCargaArchivoRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;

/**
 *  BO para la carga de nombre de archivos respuesta.
 *
 * @author FSW-Vector
 * @since 11/01/2019
 */
@Remote
public interface BOCargaArchivoRespuesta {

	
	/**
	 * Metodo que valida si esta iniciado POACOA.
	 *
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Objeto de la Arquitectura
	 * @return BeanResValInicioPOACOA Bean de Respuesta
	 */
	BeanResValInicioPOACOA validaInicioPOACOA(String modulo, ArchitechSessionBean architectSessionBean);
	
	/**
	 * Agrega nombre del archivo para su posterior proceso.
	 *
	 * @param nombreArchivo Nombre del archivo
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Objeto bean de respuesta
	 */
	BeanResValInicioPOACOA agregarNombreArchResp(String nombreArchivo, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza nombre del archivo para su posterior proceso.
	 *
	 * @param nombreArchivo Nombre del archivo
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Objeto bean de respuesta
	 */
	BeanResValInicioPOACOA actualizarNombreArchResp(String nombreArchivo, String modulo,
			ArchitechSessionBean architectSessionBean);
}
