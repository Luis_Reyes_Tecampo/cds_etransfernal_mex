/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOReparacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/08/2019 01:32:51 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;

/**
 * Interface BOReparacion.
 *
 * Interface que declara metodos necesarios 
 * para realizar los flujos de la
 * pantalla de recepciones.
 * 
 * @author FSW-Vector
 * @since 8/08/2019
 */
@Remote
public interface BOReparacion {

	/**
	 * Obtener ordenes reparacion.
	 *
	 * Consultar los registros disponibles
	 * 
	 * @param modulo        El objeto: modulo SPID o SPEI
	 * @param beanPaginador El objeto: bean paginador
	 * @param session       El objeto: session
	 * @param sortField     El objeto: sort field
	 * @param sortType      El objeto: sort type
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	BeanResOrdenReparacion obtenerOrdenesReparacion(BeanModulo modulo, BeanPaginador beanPaginador,
			ArchitechSessionBean session, String sortField, String sortType) throws BusinessException;

	/**
	 * Enviar ordenes reparacion.
	 *
	 * Envia los registros a la reparacion mediante un cambio de estatus.
	 * 
	 * @param modulo                El objeto: modulo SPID o SPEI
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param architechSessionBean  El objeto: architech session bean
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	BeanResOrdenReparacion enviarOrdenesReparacion(BeanModulo modulo, BeanReqOrdeReparacion beanReqOrdeReparacion,
			ArchitechSessionBean architechSessionBean) throws BusinessException;
	
	/**
	 * Obtener ordenes reparacion montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo    El objeto: modulo SPID o SPEI
	 * @param session   El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType  El objeto: sort type
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	BigDecimal obtenerOrdenesReparacionMontos(BeanModulo modulo, ArchitechSessionBean session, String sortField,
			String sortType) throws BusinessException;
}
