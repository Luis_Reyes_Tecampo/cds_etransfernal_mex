/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOCargaArchivoRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqArchPago;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanales;

/** BO para la carga de nombre de archivos respuesta */
@Remote
public interface BOCargaArchivoEnvio {

	/**
	 * Agrega nombre del archivo para pagos
	 * @param beanReqArchPago con los datos del archivo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	BeanResConsCanales agregarNombreArchProcesar(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza nombre del archivo para pagos
	 * @param beanReqArchPago con los datos del archivo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	BeanResConsCanales actualizarNombreArchProcesar(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Obtiene los medios de entrega para el combo de nombre de archivos de pagos.
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResConsCanales Bean de respuesta con la consulta de canales
	 */
	BeanResConsCanales obtenerMediosEntrega(
			ArchitechSessionBean architectSessionBean);
}
