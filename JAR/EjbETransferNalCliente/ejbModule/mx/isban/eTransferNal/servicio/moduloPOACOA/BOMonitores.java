/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMonitores.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:39:13 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;

/**
 * Interface BOMonitores.
 *
 * Interface que declara metodos necesarios para realizar la consulta del
 * monitor.
 * 
 * @author FSW-Vector
 * @since 6/08/2019
 */
@Remote
public interface BOMonitores {

	/**
	 * Obtener monitor.
	 *
	 * Obtener la informacion actual del monitor
	 * 
	 * @param modulo  El objeto: modulo SPEI o SPID
	 * @param session El objeto: session
	 * @return Objeto bean monitor
	 * @throws BusinessException La business exception
	 */
	BeanMonitor obtenerMonitor(BeanModulo modulo, ArchitechSessionBean session) throws BusinessException;
}
