/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMonCanCargaArchConting.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:09:46 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContig;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * del Monitor de carga de archivos contingencia canales
**/
@Remote
public interface BOMonCanCargaArchConting{

   /**Metodo que sirve para consultar la informacion del monitor de
   * carga de archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchCanContig Objeto del tipo BeanResMonCargaArchCanContig
   */
   BeanResMonCargaArchCanContig consultarMonCanCargaArchConting(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);



}
