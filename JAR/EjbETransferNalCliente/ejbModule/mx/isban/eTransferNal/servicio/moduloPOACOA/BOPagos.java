/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 10:40:24 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagos;

/**
 * Interface BOPagos.
 *
 * Interface que declara metodos necesarios para realizar los flujos de la
 * pantalla de pagos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Remote
public interface BOPagos {

	/**
	 * Obtener listado pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param tipoOrden            El objeto: tipo orden
	 * @param beanPaginador        El objeto: bean paginador
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	BeanResListadoPagos obtenerListadoPagos(BeanModulo modulo, String tipoOrden, BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**
	 * Buscar pagos.
	 *
	 * Buscar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param field                El objeto: field
	 * @param valor                El objeto: valor
	 * @param tipoOrden            El objeto: tipo orden
	 * @param paginador            El objeto: paginador
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	BeanResListadoPagos buscarPagos(BeanModulo modulo, String field, String valor, String tipoOrden,
			BeanPaginador paginador, ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**
	 * Obtener listado pagos montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param tipoOrden            El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	BigDecimal obtenerListadoPagosMontos(BeanModulo modulo, String tipoOrden, ArchitechSessionBean architechSessionBean)
			throws BusinessException;

	/**
	 * Buscar pagos monto.
	 *
	 * Buscar los importes
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param field                El objeto: field
	 * @param valor                El objeto: valor
	 * @param tipoOrden            El objeto: tipo orden
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto big decimal
	 * @throws BusinessException La business exception
	 */
	BigDecimal buscarPagosMonto(BeanModulo modulo, String field, String valor, String tipoOrden,
			ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**
	 * Exportar todos listado pagos.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 * 
	 * @param modulo             El objeto: modulo SPID o SPEI
	 * @param nombre             El objeto: nombre
	 * @param tipoOrden          El objeto: tipo orden
	 * @param beanReqListadoPago El objeto: bean req listado pago
	 * @param architechBean      El objeto: architech bean
	 * @return Objeto bean res listado pagos
	 * @throws BusinessException La business exception
	 */
	BeanResListadoPagos exportarTodoListadoPagos(BeanModulo modulo, String nombre, String tipoOrden,
			BeanReqListadoPagos beanReqListadoPago, ArchitechSessionBean architechBean) throws BusinessException;

	/**
	 * Consulta detalle pago.
	 *
	 * Consulta el detalle de pago
	 * 
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param referencia           El objeto: referencia
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean detalle pago
	 * @throws BusinessException La business exception
	 */
	BeanDetallePago consultaDetallePago(BeanModulo modulo, String referencia, ArchitechSessionBean architechSessionBean)
			throws BusinessException;

}
