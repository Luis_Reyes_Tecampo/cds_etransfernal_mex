/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 11:26:48 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * Interface BORecepciones.
 *
 * Interface que declara metodos necesarios para realizar los flujos de la
 * pantalla de recepciones.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Remote
public interface BORecepciones {

	/**
	 * Obtener consulta recepciones.
	 *
	 * Consultar los registros disponibles 
	 * 
	 * @param beanPaginador              El objeto: bean paginador
	 * @param modulo                     El objeto: modulo SPID o SPEI
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param cveInst                    El objeto: cve inst
	 * @param fch                        El objeto: fch
	 * @param architechSessionBean       El objeto: architech session bean
	 * @return Objeto bean res consulta recepciones
	 * @throws BusinessException La business exception
	 */
	BeanResConsultaRecepciones obtenerConsultaRecepciones(BeanPaginador beanPaginador, BeanModulo modulo,
			BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch,
			ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**
	 * Exportar todos.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 *  
	 * @param beanResConsultaRecepciones El objeto: bean res consulta recepciones
	 * @param modulo                     El objeto: modulo SPID o SPEI
	 * @param reqSession                 El objeto: req session
	 * @param nombre                     El objeto: nombre
	 * @param session                    El objeto: session
	 * @return Objeto bean res consulta recepciones
	 * @throws BusinessException La business exception
	 */
	BeanResConsultaRecepciones exportarTodo(BeanResConsultaRecepciones beanResConsultaRecepciones, BeanModulo modulo,
			BeanSessionSPID reqSession, String nombre, ArchitechSessionBean session) throws BusinessException;

	/**
	 * Obtener detalle recepcion.
	 *
	 * Consultar del detalle de la
	 * recpecion seleccionada.
	 * 
	 * @param beanLlave            El objeto: bean llave
	 * @param modulo               El objeto: modulo SPID o SPEI
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res recepcion SPID
	 * @throws BusinessException La business exception
	 */
	BeanResRecepcionSPID obtenerDetalleRecepcion(BeanSPIDLlave beanLlave, BeanModulo modulo,
			ArchitechSessionBean architechSessionBean) throws BusinessException;

}
