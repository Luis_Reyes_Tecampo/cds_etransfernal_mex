/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOAvisos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 10:34:38 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasos;

/**
 * Interface BOAvisos.
 * 
 * Interface que declara metodos necesarios para realizar los flujos de la
 * pantalla de avisos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Remote
public interface BOAvisos {

	/**
	 * Obtener avisos.
	 *
	 * Consultar la lista de los avisos disponibles.
	 * 
	 * @param paginador            El objeto: paginador
	 * @param modulo               El objeto: modulo
	 * @param sortField            El objeto: sort field
	 * @param sortType             El objeto: sort type
	 * @param cveInst              El objeto: cve inst
	 * @param fch                  El objeto: fch
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res aviso traspasos
	 * @throws BusinessException La business exception
	 */
	BeanResAvisoTraspasos obtenerAvisos(BeanPaginador paginador, BeanModulo modulo, String sortField, String sortType,
			String cveInst, String fch, ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**
	 * Exportar todos.
	 *
	 * Exportar todos los registros para el proceso batch.
	 * 
	 * @param beanAvisos El objeto: bean avisos
	 * @param modulo     El objeto: modulo
	 * @param reqSession El objeto: req session
	 * @param session    El objeto: session
	 * @return Objeto bean res aviso traspasos
	 * @throws BusinessException La business exception
	 */
	BeanResAvisoTraspasos exportarTodo(BeanResAvisoTraspasos beanAvisos, BeanModulo modulo, BeanSessionSPID reqSession,
			ArchitechSessionBean session) throws BusinessException;

}
