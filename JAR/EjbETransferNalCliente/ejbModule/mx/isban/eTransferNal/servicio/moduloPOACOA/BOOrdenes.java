/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOOrdenes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 10:38:10 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacion;

/**
 * Interface BOOrdenes.
 *
 * Interface que declara metodos necesarios para realizar los flujos de la
 * pantalla de ordenes de reparacion.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Remote
public interface BOOrdenes {


	/**
	 * Exportar todos ordenes reparacion.
	 *
	 * Realizar la exportacion de los registros para el procesamiento batch.
	 * 
	 * @param modulo                El objeto: modulo
	 * @param beanReqOrdeReparacion El objeto: bean req orde reparacion
	 * @param architechBean         El objeto: architech bean
	 * @return Objeto bean res orden reparacion
	 * @throws BusinessException La business exception
	 */
	BeanResOrdenReparacion exportarTodoOrdenesReparacion(BeanModulo modulo, BeanReqOrdeReparacion beanReqOrdeReparacion,
			ArchitechSessionBean architechBean) throws BusinessException;

}
