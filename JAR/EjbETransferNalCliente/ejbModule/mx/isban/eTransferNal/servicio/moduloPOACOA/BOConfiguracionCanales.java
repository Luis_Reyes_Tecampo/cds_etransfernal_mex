/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConfiguracionCanales.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanal;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaCanales;

/** BO para al configuracion de canales en POA-COA*/
@Remote
public interface BOConfiguracionCanales {
	
	/**
	 * Obtiene los medios de entrega para el combo de alta canales.
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResMedEntrega Bean de respuesta
	 */
	BeanResMedEntrega obtenerMediosEntrega(
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Consulta los canales del catalogo
	 * 
	 * @param architectSessionBean Objeto de sesion
	 * @return List<BeanMedEntrega> Lista de beans
	 */
	BeanResCanal obtenerCanales(ArchitechSessionBean architectSessionBean);
	
	/**
	 * Consulta los canales del catalogo con paginacion
	 * @param reqCanal de tipo BeanReqCanal
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResConsultaCanales bean de respuesta
	 */
	BeanResConsultaCanales obtenerCanalesPag(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Agrega canal al catalogo
	 * @param reqCanal de tipo BeanReqCanal
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResConsultaCanales bean de respuesta
	 */
	BeanResConsultaCanales agregarCanal(BeanReqCanal reqCanal, ArchitechSessionBean architectSessionBean);
	
	/**
	 * Activar desactivar canal en el catalogo
	 * @param reqCanal de tipo BeanReqCanal
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResConsultaCanales activarDesactivarCanal(BeanReqCanal reqCanal, ArchitechSessionBean architectSessionBean);
	
	/**
	 * Eliminar canal del catalogo
	 * @param reqCanal de tipo BeanReqCanal
	 * @param architectSessionBean Objeto de sesion
	 * @return BeanResConsultaCanales bean de respuesta
	 */
	BeanResConsultaCanales borrarCanal(BeanReqCanal reqCanal, ArchitechSessionBean architectSessionBean);

}
