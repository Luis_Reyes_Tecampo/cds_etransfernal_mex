/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultarArchivos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaArchRespuesta;

/**
 * Interface BOConsultarArchivos.
 *
 * @author ooamador
 */
@Remote
public interface BOConsultarArchivos {
	
	/**
	 * Consultar archivos proc.
	 *
	 * @param beanPaginador el bean para paginar
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean El objeto: architect session bean
	 * @return BeanResConsultaArchRespuesta Bean de respuesta
	 */
	BeanResConsultaArchRespuesta consultarArchivosProc( BeanPaginador beanPaginador, String modulo,
			ArchitechSessionBean architectSessionBean);

}
