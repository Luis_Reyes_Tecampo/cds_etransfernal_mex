/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOHistorialMsj.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 01:01:58 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.moduloPOACOA;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResHistorialMensajes;

/**
 * Interface BOHistorialMsj.
 *
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Remote
public interface BOHistorialMsj {

	/**
	 * Obtener historial.
	 *
	 * Consultar el historial de los  pagos.
	 * 
	 * @param modulo                   El objeto: modulo
	 * @param paginador                El objeto: paginador
	 * @param architechSessionBean     El objeto: architech session bean
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @return Objeto bean res historial mensajes
	 * @throws BusinessException La business exception
	 */
	BeanResHistorialMensajes obtenerHistorial(BeanModulo modulo, BeanPaginador paginador,
			ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes)
			throws BusinessException;
}
