/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/09/2019 01:43:19 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCtaFideicomiso;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepCtasFideicomiso;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Remote
public interface BOExcepCtasFideicomiso {

	/**
	 * Consulta de cuentas (todas y por filtrado).
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param beanCtasFideico El objeto: bean ctas fideico --> Objeto de filtrado
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepCtasFideicomiso consultaCuentas(ArchitechSessionBean sessionBean, BeanExcepCtasFideicomiso beanCtasFideico) throws BusinessException;
	
	/**
	 * Alta de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param cuenta El objeto: cuenta --> Valor del registro nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepCtasFideicomiso altaCuentas(ArchitechSessionBean sessionBean, String cuenta) throws BusinessException;
	
	/**
	 * Baja de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param listaCuentas El objeto: lista cuentas --> Lista de registros a eliminar
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepCtasFideicomiso bajaCuentas(ArchitechSessionBean sessionBean, List<BeanCtaFideicomiso> listaCuentas) throws BusinessException;
	
	/**
	 * Modificacion de cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param ctaOld El objeto: cta old --> Valor del registro anterior
	 * @param ctaNew El objeto: cta new --> Valor del registro nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepCtasFideicomiso modificacionCuentas(ArchitechSessionBean sessionBean, String ctaOld, String ctaNew) throws BusinessException;
	
	/**
	 * Exportar todos los registros.
	 *
	 * @param session El objeto: session --> Objeto de session
	 * @return Objeto bean res base --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session) throws BusinessException;
	
	
	
}
