/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCertificadosCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-25-2018 04:07:35 PM Juan Jesus Beltran R. Vector Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOCertificadosCifrado.
 * 
 * Expone las firmas de los metodos que se consumen 
 * desde el COntroller 
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
@Remote
public interface BOCertificadosCifrado {

	/**
	 * Consultar certificados.
	 *
	 * @param session El objeto: session
	 * @param request El objeto: request
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto bean certificados cifrado
	 * @throws BusinessException La business exception
	 */
	BeanCertificadosCifrado consultar(ArchitechSessionBean session, BeanCertificadoCifrado request, BeanPaginador beanPaginador)
			throws BusinessException;

	/**
	 * Consultar combos.
	 *
	 * @param session El objeto: session
	 * @return Objeto bean certificados
	 * @throws BusinessException La business exception
	 */
	BeanCombosCertificadoCifrado consultarCombos(ArchitechSessionBean session) throws BusinessException;

	/**
	 * Agregar certificado.
	 *
	 * @param session El objeto: session
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase agregarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado)
			throws BusinessException;
	
	/**
	 * Eliminar certificados.
	 *
	 * @param session El objeto: session
	 * @param beanCertificados El objeto: bean certificados
	 * @return Objeto bean eliminar response
	 * @throws BusinessException La business exception
	 */
	BeanEliminarResponse eliminarCertificados(ArchitechSessionBean session, BeanCertificadosCifrado beanCertificados)
			throws BusinessException;
	
	/**
	 * Editar certificado.
	 *
	 * @param session El objeto: session
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase editarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado)
			throws BusinessException;
	
	/**
	 * Exportar todo.
	 *
	 * @param session El objeto: session
	 * @param request El objeto: request
	 * @param totalRegistros El objeto: total registros
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session, BeanCertificadoCifrado request, int totalRegistros)
			throws BusinessException;
	
}
