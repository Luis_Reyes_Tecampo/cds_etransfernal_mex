/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase --> BONombresFideico.java
 * 
 * Control de versiones -->
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11 -->56 -->21 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;
import mx.isban.eTransferNal.beans.catalogos.BeanNombresFideico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BONombresFideico.
 * 
 * Interfaz que declara los metodos utilizados por la capa de negocio 
 * para los flujos del catalogo de identificacion de nombres de
 * fideicomiso.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Remote
public interface BONombresFideico {

	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las nombres de fideicomiso
	 *
	 * @param beanNombre --> objeto que almacena las los nombres de fideicomiso
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador --> bean que permite paginar
	 * @return the beanNombresFideico --> Resultado de la operacion
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanNombresFideico consultar(BeanNombreFideico beanNombre, BeanPaginador beanPaginador, ArchitechSessionBean session) 
		throws BusinessException;
	
	/**
	 * Agregar registros.
	 *
	 * Metodo publico utilizado para agregar registros
	 * al catalogo.
	 * 
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNombre --> bean que obtiene el nombre
	 * @return the beanNombresFideico --> Resultado de la operacion
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 * 
	 */
	BeanResBase agregar(BeanNombreFideico beanNombre, ArchitechSessionBean session)
		throws BusinessException;
	
	/**
	 * Editar registros.
	 *
	 * Metodo publico utilizado para editar registros
	 * del catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 *@param anterior --> objeto de tipo  BeanNombreFideico que trae los nmbres de usuarios
	 * @param beanNombre --> bean que obtiene el nombre de usuario
	 * @return the beanNombresFideico --> Resultado de la operacion
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 * 
	 */
	BeanResBase editar(BeanNombreFideico beanNombre, BeanNombreFideico anterior, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Eliminar registros.
	 *
	 * Metodo publico utilizado para eliminar registros
	 * en el catalogo.
	 * 
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNombres --> bean que obtiene el nombre
	 * @return the beanNombresFideico --> Resultado de la operacion
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 * 
	 */ 
	BeanEliminarResponse eliminar(BeanNombresFideico beanNombres, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Exportar todos.
	 * 
	 * Realiza la inserccion para el proceso batch de bitacora.
	 *
	 * @param beanNombre --> objeto que almacena los registros
	 * @param totalRegistros --> variable de tipo int y que muestra el total de registros
	 * @param session --> objeto de tipo ArchitechSessionBean que administra la sesion del usuario
	 * @return the beanNombresFideico --> Resultado de la operacion
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanResBase exportarTodo(BeanNombreFideico beanNombre, int totalRegistros, ArchitechSessionBean session)
		throws BusinessException;
}
