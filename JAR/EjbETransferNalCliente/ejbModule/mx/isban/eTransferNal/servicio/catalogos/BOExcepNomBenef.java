/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/09/2019 01:43:19 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepNomBenef;
import mx.isban.eTransferNal.beans.catalogos.BeanNomBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Remote
public interface BOExcepNomBenef {

	/**
	 * Consulta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Objeto de filtrado
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepNomBenef consultaNombres(ArchitechSessionBean sessionBean, BeanExcepNomBenef beanExcepNomBenef) throws BusinessException;
	
	/**
	 * Alta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param nombre El objeto: nombre --> Valor del registro nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepNomBenef altaNombres(ArchitechSessionBean sessionBean, String nombre) throws BusinessException;
	
	/**
	 * Baja nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param listaNombres El objeto: lista nombres --> Lista de registros a eliminar
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepNomBenef bajaNombres(ArchitechSessionBean sessionBean, List<BeanNomBeneficiario> listaNombres) throws BusinessException;
	
	/**
	 * Modificacion nombres.
	 *
	 * @param sessionBean El objeto: session bean --> Objeto de session
	 * @param nomOld El objeto: nom old --> Valor del registro anterior
	 * @param nomNew El objeto: nom new --> Valor del registro nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanExcepNomBenef modificacionNombres(ArchitechSessionBean sessionBean, String nomOld, String nomNew) throws BusinessException;
	
	/**
	 * Exportar todos los nombres.
	 *
	 * @param session El objeto: session --> Objeto de session
	 * @return Objeto bean res base --> Respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error durante este flujo
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session) throws BusinessException;
	
}
