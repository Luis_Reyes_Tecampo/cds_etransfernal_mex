/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BONotificador.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:20:59 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificaciones;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesCombo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BONotificador.
 *
 * @author FSW-Vector
 * @since 26/07/2019
 */
@Remote
public interface BONotificador {

	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanNotificacion -->  objeto que almacena las notificaciones
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador -->  bean que permite paginar
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanNotificaciones consultar(BeanNotificacion beanNotificacion, BeanPaginador beanPaginador, ArchitechSessionBean session) 
			throws BusinessException;
	
	/**
	 * consultar listados de notificaciones
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param tipo -->  variable de tipo int que trae el tipo de listado
	 * @param filter -->  objeto de tipo BeanNotificacion que trae el filtro
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return listas -->  regresa la lista de los registros
	 * @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanNotificacionesCombo consultarListas(int tipo, BeanNotificacion filter, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * agregar notificacion
	 * 
	 * Metodo publico utilizado para agregar notificaciones
	 *
	 *@param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion -->  bean que obtiene la notificacion
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanResBase agregar(BeanNotificacion beanNotificacion, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * editar notificacion
	 * 
	 * Metodo publico utilizado para editar notificaciones
	 *
	 * @param beanNotificacion -->  objeto que trae notificaciones 	
	 * @param anterior -->  objeto que trae los registros anteriores
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanResBase editar(BeanNotificacion beanNotificacion, BeanNotificacion anterior, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session -->  objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificaciones -->  bean que obtiene notificaciones
	 * @return response -->  regresa la respuesta en base a los parámetros obtenidos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanEliminarResponse eliminar(BeanNotificaciones beanNotificaciones, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Exportar registros.
	 *beanNotificaciones
	 * @param beanNotificacion -->  trae las notificaciones
	 * @param totalRegistros -->  variable de tipo int 	y que obtiene el numero de registros
	 * @param session -->  objeto de tipo ArchitechSessionBean
	 * @return response -->  obtiene el mensaje de respuesta de la base de datos.
	 *  @throws BusinessException realiza excepciones en caso de presentarse ciertos casos 
	 */
	BeanResBase exportarTodo(BeanNotificacion beanNotificacion, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException;
}
