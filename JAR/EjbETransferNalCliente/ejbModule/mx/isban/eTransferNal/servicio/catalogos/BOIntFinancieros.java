/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOInstitucionesPCCda.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancieros;
import mx.isban.eTransferNal.beans.catalogos.BeanResTiposIntFinancieros;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimIntFin;

@Remote
public interface BOIntFinancieros {

	  /**Metodo que sirve para consultar la informacion de Intermediarios
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResInst Objeto del tipo BeanResInst
	   */
	public BeanResTiposIntFinancieros consultaTiposInterviniente(BeanResIntFinancieros beanReqInsertInst,
			ArchitechSessionBean architechSessionBean);
    
	  /**Metodo que sirve para consultar la informacion de Intermediarios
	   * @param beanConsReqInst Objeto del tipo @see BeanConsReqInst
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResInstPOACOA Objeto del tipo BeanResInstPOACOA
	   */
	public BeanResTiposIntFinancieros buscarIntFinancierosFiltro(BeanConsReqIntFin beanConsReqIntFin, 
			ArchitechSessionBean architechSessionBean);
	
   /**
    * Elim cat int financieros.
    *
    * @param beanReqEliminarIntFin El objeto: bean req eliminar Intermediarios
    * @param architechSessionBean El objeto: architech session bean
    * @return Objeto bean res elim int fin
    */
   public BeanResElimIntFin elimCatIntFinancieros(BeanReqEliminarIntFin beanReqEliminarIntFin, 
		   ArchitechSessionBean architechSessionBean);
   
   /**Metodo que sirve para insertar la informacion de las Intermediarios
   * @param beanReqInsertInst Objeto del tipo @see BeanReqInsertInst
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResGuardaInst Objeto del tipo BeanResGuardaInst
   */ 
   public BeanResTiposIntFinancieros guardaIntFin(BeanReqInsertarIntFin beanReqInsertIntFin, 
		   ArchitechSessionBean architechSessionBean);
   
   /**
    * Actualiza int financiero.
    *
    * @param beanReqInsertIntFin El objeto: bean req insert Intermediarios
    * @param architechSessionBean El objeto: architech session bean
    * @return Objeto bean res guarda int fin
    */
   public BeanResIntFinancieros actualizaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin, 
			ArchitechSessionBean architechSessionBean);
	
   /**
    * Actualiza int financiero.
    * @param session El objeto: architech session bean
    * @param beanConsReqIntFin El objeto: bean que consulta Cat Intermediarios
    * @param totalRegistros parametro que contiene el numero de registros
    * @return Objeto BeanResBase 
    * @throws BusinessException se lanza una ecepcion del tipo businessException
    */
    BeanResBase exportarTodo(ArchitechSessionBean session, BeanConsReqIntFin beanConsReqIntFin, int totalRegistros)
			throws BusinessException;
}
