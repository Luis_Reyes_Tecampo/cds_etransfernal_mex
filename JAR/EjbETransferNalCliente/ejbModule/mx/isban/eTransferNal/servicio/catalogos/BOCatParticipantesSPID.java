/**
 * Isban Mexico
 * Clase: BOCatParticipantesSPID.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 09/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanReqParticipantes;
import mx.isban.eTransferNal.beans.catalogos.BeanResParticipantesSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
@Remote
public interface BOCatParticipantesSPID {
	
	/**
	 * Consulta el catalogo de participantes SPID
	 * 
	 * @param cveIntermediario EL parametro de busqueda cve intermediario
	 * @param architectSessionBean EL bean de session
	 * @return BeanResParticipantesSPID
	 */
	BeanResParticipantesSPID consultaParticipantes(BeanReqParticipantes cveIntermediario, ArchitechSessionBean architectSessionBean);

	/**
	 * Consulta el catalogo de participantes SPID
	 * 
	 * @param cveIntermediario EL parametro de busqueda cve intermediario
	 * @param architectSessionBean EL bean de session
	 * @return BeanResParticipantesSPID
	 */
	BeanResParticipantesSPID consultaPartiAlta(BeanReqParticipantes cveIntermediario, ArchitechSessionBean architectSessionBean);
		
	/**
	 * Guarda catalogo participantes
	 * 
	 * @param request El request para guardar
	 * @param architectSessionBean
	 * @return
	 */
	BeanResBase guardarParticipante(BeanReqParticipantes request, ArchitechSessionBean architectSessionBean);
	
	/**
	 * elimina participantes
	 * 
	 * @param request El request para guardar
	 * @param architectSessionBean
	 * @return
	 */
	BeanResBase eliminaParticipante(BeanReqParticipantes request, ArchitechSessionBean architectSessionBean);

}
