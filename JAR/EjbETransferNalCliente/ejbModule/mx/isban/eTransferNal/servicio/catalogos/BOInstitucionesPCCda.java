/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOInstitucionesPCCda.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqEliminarInstPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqHabDesInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimInstPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResHabDesInstParticipante;

@Remote
public interface BOInstitucionesPCCda {

	  /**Metodo que sirve para consultar la informacion de las instituciones
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResInst Objeto del tipo BeanResInst
	   */
	   public BeanResInst consultaInst(ArchitechSessionBean architechSessionBean);
    
	  /**Metodo que sirve para consultar la informacion de las instituciones
	   * @param beanConsReqInst Objeto del tipo @see BeanConsReqInst
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResInstPOACOA Objeto del tipo BeanResInstPOACOA
	   */
	   public BeanResInstPOACOA buscarInstitucionesPOACOA(BeanConsReqInst beanConsReqInst, ArchitechSessionBean architechSessionBean);
	   
	   /**Metodo que sirve para insertar la informacion de las instituciones
	   * @param beanReqInsertInst Objeto del tipo @see BeanReqInsertInst
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResGuardaInst Objeto del tipo BeanResGuardaInst
	   */ 
	   public BeanResGuardaInst guardaInstitucionPOACOA(BeanReqInsertInst beanReqInsertInst, ArchitechSessionBean architechSessionBean);
	   
	   /**Metodo que sirve para eliminar la informacion de las instituciones
	   * @param beanReqEliminarInstPOACOA Objeto del tipo @see BeanReqEliminarInstPOACOA
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResElimInstPOACOA Objeto del tipo BeanResGuardaInst
	   */
	   public BeanResElimInstPOACOA elimCatInstFinancieras(
			   BeanReqEliminarInstPOACOA beanReqEliminarInstPOACOA, ArchitechSessionBean architechSessionBean);
	   
	   /**Metodo que sirve para insertar la informacion de las instituciones
	   * @param beanReqHabDesInst Objeto del tipo @see BeanReqHabDesInst
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHabDesInstParticipante Objeto del tipo BeanResHabDesInstParticipante
	   */
	   public BeanResHabDesInstParticipante habDesPOACOA(BeanReqHabDesInst beanReqHabDesInst, ArchitechSessionBean architechSessionBean);
	   
	   /**Metodo que sirve para insertar la informacion de las instituciones
		   * @param beanReqHabDesInst Objeto del tipo @see BeanReqHabDesInst
		   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
		   * @return BeanResHabDesInstParticipante Objeto del tipo BeanResHabDesInstParticipante
		   */
	   public BeanResHabDesInstParticipante habDesValidacionCuenta(BeanReqHabDesInst beanReqHabDesInst, ArchitechSessionBean architechSessionBean);
}
