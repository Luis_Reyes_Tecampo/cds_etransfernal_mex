package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancierosHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Declaracion de la interfaz BOIntFinancierosHis
 */
@Remote
public interface BOIntFinancierosHis {

	/**
	 * ConsultaIntFinaHist, se consultan todos los registrso historicos.
	 * @param session El objeto session, se obtiene la sesion 
	 * @param beanIntFinancieroHistorico de tipo  BeanIntFinancieroHisto
	 * @param beanPaginador objeto BeanPaginador
	 * @return Objeto BeanCodErroresDevoluciones  
	 * @throws BusinessException exception lanzada al momento de un error de logica
	 */	
	BeanResIntFinancierosHist consultaIntFinaHist (ArchitechSessionBean session,BeanIntFinancieroHistorico beanIntFinancieroHistorico,BeanPaginador beanPaginador) throws BusinessException;
	
	/**
	 * Exportar excel, metodo que exporta a un archivo los registros presentados en pantalla 
	 * para el caso de historicos 
     * @param architectSessionBean El objeto session, se obtiene la sesion 
	 * @param beanIntFinancieroHisto el objeto BeanIntFinancieroHisto
	 * @param totalRegistros recibe el total de registros
	 * @return objeto a regresar BeanResBase
     * @throws BusinessException exception lanzada al momento de un error de logica
	 */
	BeanResBase exportarTodo(ArchitechSessionBean architectSessionBean, BeanIntFinancieroHistorico beanIntFinancieroHisto,int totalRegistros) 
			throws BusinessException;
}