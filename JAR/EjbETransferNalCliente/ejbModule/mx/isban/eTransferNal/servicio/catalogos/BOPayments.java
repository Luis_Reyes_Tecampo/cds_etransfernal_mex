package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultasPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Declaracion de la interfaz BOPaymentsImpl
 */
@Remote
public interface BOPayments {

	/**
    * consultasPayments, se consultan todos los registros, id de canal unico y su descripcion.
    * @param architectSessionBean El objeto session, se obtiene la sesion 
    * @param requestPayments de tipo  BeanConsultaPayments
    * @param beanPaginador objeto BeanPaginador
    * @return Objeto BeanConsultasPayme  
    * @throws BusinessException exception lanzada al momento de un error de logica
    */
	BeanConsultasPayme consultasPayments(ArchitechSessionBean architectSessionBean, BeanConsultaPayme requestPayments, BeanPaginador beanPaginador)
			throws BusinessException;
	
	/**
    * editarconsultasPayments, metodo para acualizae la datos correpondientes a un mensaje de errro.
    * @param session El objeto session, se obtiene la sesion 
    * @param beanConsultaPayments se obtiene los datos de la vista
    * @return regresa el objeto BeanResBase
    * @throws BusinessException exception lanzada al momento de un error de logica
    */
	BeanResBase editarconsultasPayments(ArchitechSessionBean session, BeanConsultaPayme beanConsultaPayments)
				throws BusinessException;
	
	/**
	 * Exportar excel, metodo que exporta a un archivo los registros presentados en pantalla 
	 * para el caso de cales de id �nico activos/desactivos
     * @param session El objeto session, se obtiene la sesion 
	 * @param request el objeto BeanCodErrorDevoluciones
	 * @param totalRegistros recibe el total de registros
	 * @return objeto a regresar BeanResBase
     * @throws BusinessException exception lanzada al momento de un error de logica
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session, BeanConsultaPayme request, int totalRegistros)
			throws BusinessException;
	
	/**
    * Consultar metodo que carga informacion a los combos de laa vista alta.
    * @param session El objeto session, se obtiene la sesion 
    * @param beanConsultaPayments de tipo  BeanConsultaPayments
    * @param beanPaginador objeto BeanPaginador
    * @return BeanConsultasPayme
    * @throws BusinessException exception lanzada al momento de un error de logica
    */
	BeanConsultasPayme consultasCombos(ArchitechSessionBean session,  BeanConsultaPayme beanConsultaPayments, BeanPaginador beanPaginador) 
			throws BusinessException;


	
}
