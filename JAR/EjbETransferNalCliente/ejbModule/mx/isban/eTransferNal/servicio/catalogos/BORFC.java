/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BORFC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:55:06 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;
import mx.isban.eTransferNal.beans.catalogos.BeanResRFC;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BORFC.
 * 
 * Interfaz que declara los metodos utilizados por la capa de negocio 
 * para los flujos del catalogo de excepcion de RFC.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Remote
public interface BORFC {

	/**
	 * Consultar.
	 *
	 * @param beanRFC the beanRFC --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador the beanPaginador  --> Objeto para realizar el paginado
	 * @param session El objeto: session
	 * @return the beanResRFC --> El objeto session
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanResRFC consultar(BeanRFC beanRFC, BeanPaginador beanPaginador, ArchitechSessionBean session) 
		throws BusinessException;
	
	/**
	 * Agregar.
	 *
	 * @param beanRFC the beanRFC --> Objeto con los datos del nuevo registro
	 * @param session El objeto: session
	 * @return the beanResBase --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase agregar(BeanRFC beanRFC, ArchitechSessionBean session)
		throws BusinessException;
	
	/**
	 * Editar.
	 *
	 * @param beanRFC the bean RFC --> Objeto con los datos del nuevo registro
	 * @param anterior El objeto: anterior
	 * @param session El objeto: session
	 * @return the beanResBase --> Resultado de la operacion
	 * @throws BusinessException the business exception--> Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase editar(BeanRFC beanRFC, BeanRFC anterior, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Eliminar.
	 *
	 * @param beanResRFC the bean res RFC --> Registros a eliminar
	 * @param session El objeto: session
	 * @return the beanEliminarResponse --> Resultado de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanEliminarResponse eliminar(BeanResRFC beanResRFC, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Exportar todos.
	 *
	 * @param beanRFC the beanRFCa --> Objeto de filtrado de registros
	 * @param totalRegistros the totalRegistros --> Parametro con el total de registros actual
	 * @param session El objeto: session
	 * @return the beanResBase --> Objeto de respuesta de la operacion
	 * @throws BusinessException the business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase exportarTodo(BeanRFC beanRFC, int totalRegistros, ArchitechSessionBean session)
		throws BusinessException;
}
