/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMttoMediosEntrega.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/09/2018 10:25:47 AM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOMttoMediosEntrega.
 *
 * @author FSW-Vector
 * @since 26/09/2018
 */
@Remote
public interface BOMttoMediosEntrega {

	/**
	 * Consultar Mantenimientos Entrega.
	 *
	 * @param session El objeto: session
	 * @param request El objeto: request
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto bean mantenimientos entrega
	 * @throws BusinessException La business exception
	 */
	BeanMediosEntrega consultar(ArchitechSessionBean session, BeanMedioEntrega beanMedio, BeanPaginador beanPaginador)
			throws BusinessException;
	
	/**
	 * Agregar medio entrega.
	 *
	 * @param session El objeto: session
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase agregarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio)
			throws BusinessException;
	
	/**
	 * Eliminar medios.
	 *
	 * @param session El objeto: session
	 * @param beanMedios El objeto: bean medios
	 * @return Objeto bean eliminar response
	 * @throws BusinessException La business exception
	 */
	BeanEliminarResponse eliminarMedios(ArchitechSessionBean session, BeanMediosEntrega beanMedios)
			throws BusinessException;
	
	/**
	 * Editar medio.
	 *
	 * @param session El objeto: session
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase editarMedio(ArchitechSessionBean session, BeanMedioEntrega beanMedio)
			throws BusinessException;
	
	/**
	 * Exportar todo.
	 *
	 * @param session El objeto: session
	 * @param request El objeto: request
	 * @param totalRegistros El objeto: total registros
	 * @return Objeto bean res base
	 * @throws BusinessException La business exception
	 */
	BeanResBase exportarTodo(ArchitechSessionBean session, BeanMedioEntrega request, int totalRegistros)
			throws BusinessException;
}
