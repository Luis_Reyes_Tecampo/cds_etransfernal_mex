/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOParametrosPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOParametrosPOACOA.
 *
 * @author ooamador
 */
@Remote
public interface BOParametrosPOACOA {
	
	/**
	 * Activa o desactiva el proceso POA .
	 *
	 * @param esActivacion True cuando se trata de acrivacion del POA
	 * @param esActivoCOA true cuando existe un proceso COA activado
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase objeto de respuesta
	 */
	BeanResBase actDesPOA(boolean esActivacion, boolean esActivoCOA, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Activa o desactiva el proceso COA .
	 *
	 * @param esActivacion true cuando se trata de acrivacion del COA
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase objeto de respuesta
	 */
	BeanResBase actDescCOA(boolean esActivacion, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Activa las diferentes fases del proceso.
	 *
	 * @param fase Fase acrual del proceso
	 * @param liqFinal String liquidacion final
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase objeto de respuesta
	 */
	BeanResBase activaFases(String fase,String liqFinal, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Generara Archivo.
	 *
	 * @param tipo String con el Tipo de archivo a generar
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase objeto de respuesta
	 */
	BeanResBase generarArchivo(String tipo, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Activa o desactiva la firma de archivos.
	 *
	 * @param cifrado Si se firman los archivos a procesar
	 * @param architectSessionBean  datos de sesion
	 * @return BeanResBase objeto de respuesta
	 */
	BeanResBase actDesFirmas(String cifrado,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Obtiene los parametros del POACOA.
	 *
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean datos de sesion
	 * @return BeanResPOACOA bean con los parametros POA COA
	 */
	BeanResPOACOA obtenParametrosPOACOA(String modulo, ArchitechSessionBean architectSessionBean);

}
