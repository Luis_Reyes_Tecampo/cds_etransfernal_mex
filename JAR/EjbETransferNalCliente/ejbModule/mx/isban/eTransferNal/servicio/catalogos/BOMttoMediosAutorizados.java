/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOMttoMediosAutorizados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/09/2018 10:25:47 AM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.servicio.catalogos;


import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;



/**
 * Interface BOMttoMediosAutorizados.
 *
 * Declaracion del flujo de negocio 
 * para el catalogo de Medios Autorizados.
 * 
 * @author FSW-Vector
 * @since 26/09/2018
 */
@Remote
public interface BOMttoMediosAutorizados {
	
	/**
	 * Consulta tabla MA.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean the session bean --> El objeto session
	 * @param beanAuto El objeto: bean auto --> Objeto de entrada
	 * @return the list --> Objeto de respuesta de la consulta principal
	 * @throws BusinessException the business exception --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes consultaTablaMA(BeanMttoMediosAutoBusqueda bean, ArchitechSessionBean sessionBean,BeanMttoMediosAutorizados beanAuto) throws BusinessException;
	
	/**
	 * Llena listas.
	 *
	 * @param sesion the sesion  --> El objeto session
	 * @return the bean mtto medios autorizados res --> Objeto de respuesta con la informacion de listas
	 * @throws BusinessException the business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes llenaListas(ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Adds the mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req --> Objeto de entrada con los datos del nuevo registro
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes guardarNuevoMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Del mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes delMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Mod mtt medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req --> Objeto de entrada conla informacion del registro
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes modificarMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * mostrarRegistroEditarMttMedios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req  --> Objeto de entrada conla informacion de los registros
	 * @param sesion the sesion --> El objeto session
	 * @param accion El objeto: accion --> Parametro de tipo de operacion
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes mostrarRegistroEditarMttMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, ArchitechSessionBean sesion,String accion) throws BusinessException;
	
	/**
	 * Obtener datos.
	 *
	 * @param beanPaginador the bean paginador --> Objeto con informacion del paginado
	 * @param sesion the sesion --> El objeto session
	 * @param sortField the sort field -->Parametro de ordenamiento
	 * @param sortType the sort type  -->Parametro de tipo ordenamiento 
	 * @param beanAuto El objeto: bean auto --> Objeto de informacion de entrada
	 * @return the bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException the business exception  --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes obtenerDatos(BeanPaginador beanPaginador, ArchitechSessionBean sesion, String sortField, 
			String sortType,BeanMttoMediosAutorizados beanAuto)
	 throws BusinessException;
	
	/**
	 * Exportar todos.
	 *
	 * @param sessionBean El objeto: session bean
	 * @param beanMttoMediosAutorizadosReq El objeto: bean mtto medios autorizados req
	 * @param total El objeto: total --> Parametro con el total de registros obtenidos
	 * @return Objeto bean mtto medios autorizados res --> OBjeto de salida con la respuesta de la operacion
	 * @throws BusinessException --> Lanzada cuando ocurre una excepcion dentro del flujo
	 */
	BeanMttoMediosAutorizadosRes exportarTodo(ArchitechSessionBean sessionBean,
			BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, int total) throws BusinessException;
}

