/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOAfectacionBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:14:14 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.catalogos.BeanBloques;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface BOAfectacionBloques.
 *
 * Interfaz que declara los metodos utilizados por la capa de negocio 
 * para los flujos del catalogo de afectacion de bloques.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
@Remote
public interface BOAfectacionBloques {

	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanBloque: objeto que bloques
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador: bean que permite paginar
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException the business exception
	 */
	BeanBloques consultar(BeanBloque beanBloque, BeanPaginador beanPaginador, ArchitechSessionBean session) 
		throws BusinessException;
	
	/**
	 * agregar bloque
	 * 
	 * Metodo publico utilizado para agregar bloques.
	 *
	 * @param beanBloque El objeto: bean bloque
	 * @param session El objeto: session
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException the business exception
	 */
	BeanResBase agregar(BeanBloque beanBloque, ArchitechSessionBean session)
		throws BusinessException;
	
	/**
	 * editar bloque
	 * 
	 * Metodo publico utilizado para editar bloques
	 *
	 * @param beanBloque: objeto que trae bloques	
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException the business exception
	 */
	BeanResBase editar(BeanBloque beanBloque, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session: objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloques: bean que obtiene bloques
	 * @return response: regresa la respuesta en base a los parámetros obtenidos.
	 * @throws BusinessException the business exception
	 */
	BeanEliminarResponse eliminar(BeanBloques beanBloques, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Exportar todo.
	 *
	 * @param beanBloque: trae los bloques
	 * @param totalRegistros: variable de tipo int 	y que obtiene el numero de registros
	 * @param session: objeto de tipo ArchitechSessionBean
	 * @return response: obtiene el mensaje de respuesta de la base de datos.
	 * @throws BusinessException the business exception
	 */
	BeanResBase exportarTodo(BeanBloque beanBloque, int totalRegistros, ArchitechSessionBean session)
		throws BusinessException;
}
