/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOHorariosSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   20/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIBO;


/**
 *Clase del tipo BO que se encarga  del negocio del catalogo
 *de horarios para el envio de pagos SPEI
**/
@Remote
public interface BOHorariosSPEI {
	
	  /**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO consultaHorariosSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO agregaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	
	/**Metodo que sirve para modificar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO modificaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO eliminaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO consultaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean)
		throws BusinessException;
	
	/**Metodo que sirve para consultar la informacion los catalogos para la pantalla
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   * @exception BusinessExceptionException de negocio
	   */
	public BeanResHorariosSPEIBO llenaListas(ArchitechSessionBean architechSessionBean)
		throws BusinessException;

}
