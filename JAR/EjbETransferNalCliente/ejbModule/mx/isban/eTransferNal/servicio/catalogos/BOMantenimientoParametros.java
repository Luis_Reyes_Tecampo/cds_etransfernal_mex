/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.servicio.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;

/**
 * The Interface BOMantenimientoParametros.
 */
@Remote
public interface BOMantenimientoParametros {
	
	/**
	 * Consulta opciones combos.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros consultaOpcionesCombos(ArchitechSessionBean sessionBean) throws BusinessException;
	
	/**
	 * Consultar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @param exportarTodo the exportar todo
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros consultarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request) throws BusinessException;
	
	/**
	 * Alta parametro nuevo.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros altaParametroNuevo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) throws BusinessException;
	
	/**
	 * Consultar datos parametros.
	 *
	 * @param sessionBean the session bean
	 * @param claveParametro the clave parametro
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros consultarDatosParametro(ArchitechSessionBean sessionBean, String claveParametro) throws BusinessException;
	
	/**
	 * Editar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros editarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request) throws BusinessException;
	
	/**
	 * Eliminar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @param listParametrosSeleccionados the list parametros seleccionados
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros eliminarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request, List<BeanMantenimientoParametros> listParametrosSeleccionados) throws BusinessException;

	/**
	 * Exportar todos.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 * @throws BusinessException the business exception
	 */
	BeanResMantenimientoParametros exportarTodo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request) throws BusinessException;
}
