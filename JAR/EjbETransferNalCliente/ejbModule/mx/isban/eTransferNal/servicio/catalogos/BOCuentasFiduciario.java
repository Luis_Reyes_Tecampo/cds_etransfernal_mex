/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOCuentasFiduciario.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     17/09/2019 01:33:11 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.servicio.catalogos;

import javax.ejb.Remote;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * 
 * interface BOCuentasFiduciario 
 * 
 * Interfaz que declara los metodos utilizados por la capa de negocio 
 * para los flujos del catalogo de cuentas fiduciario.
 *
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Remote
public interface BOCuentasFiduciario {

	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador El objeto: beanPaginador --> Objeto para realizar el paginado
	 * @param session objeto: bean sesion --> El objeto session
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador, ArchitechSessionBean session) 
		throws BusinessException;
	
	/**
	 * Exportar todos.
	 * 
	 * Realiza la inserccion para el proceso batch de bitacora.
	 *
	 * @param beanCuenta El objeto: bean cuenta --> Objeto de filtrado de registros
	 * @param totalRegistros El objeto: total registros --> Parametro con el totla de registros actual
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto bean res base --> Objeto de respuesta de la operacion
	 * @throws BusinessException La business exception --> Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase exportarTodo(BeanCuenta beanCuenta, int totalRegistros, ArchitechSessionBean session)
		throws BusinessException;
}
