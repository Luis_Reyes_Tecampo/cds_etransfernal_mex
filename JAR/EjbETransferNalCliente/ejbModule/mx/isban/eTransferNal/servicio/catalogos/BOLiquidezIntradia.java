/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOLiquidezIntradia.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/10/2019 12:05:03 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.servicio.catalogos;
import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.catalogos.BeanEliminarResponse;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidacionesCombo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaReq;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

 /**
 * Interface BOLiquidezIntradia.
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de Liquidez Intradia
 *
 * @author FSW-Vector
 * @since 8/10/2019 
 */

@Remote
public interface BOLiquidezIntradia {

	/**
	 * Consultar.
	 *
	 * @param beanFilter objeto bean filter - contiene los campos para realizar el filtro
	 * @param canal objeto  canal - Contiene el canal al que se realiza la consulta
	 * @param beanPaginador objeto bean paginador - Contiene las variables de paginacion
	 * @param session objeto  session - Contiene atributos de la sesion
	 * @return bean liquidez intradia req - Contiene el resultado de la consulta
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanLiquidezIntradiaReq consultar(BeanLiquidezIntradia beanFilter,String canal, BeanPaginador beanPaginador, ArchitechSessionBean session) 
			throws BusinessException;
	
	/**
	 * Consultar listas.
	 *
	 * @param tipo objeto tipo - Objeto que define el tipo de consulta
	 * @param beanFilter objeto bean filter  - contiene los campos para realizar el filtro
	 * @param session objeto session - Contiene atributos de la sesion
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @return objeto bean liquidaciones combo  - Contiene el resultado de la consulta
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanLiquidacionesCombo consultarListas(int tipo,BeanLiquidezIntradia beanFilter, ArchitechSessionBean session, String canal)
			throws BusinessException;
	
	/**
	 * Agregar.
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a agregar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase agregar(BeanLiquidezIntradia liquidacion,String canal, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Editar.
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a editar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param anterior objeto anterior - Contiene los atributos del objeto anterior
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase editar(BeanLiquidezIntradia liquidacion,String canal, BeanLiquidezIntradia anterior, ArchitechSessionBean session)
			throws BusinessException;
	
	/**
	 * Eliminar.
	 *
	 * @param liquidaciones objeto liquidaciones - Contiene los atributos a eliminar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean eliminar response - Objeto que retorna resultado de la eliminacion
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanEliminarResponse eliminar(BeanLiquidezIntradiaReq liquidaciones,String canal, ArchitechSessionBean session)
			throws BusinessException;

	/**
	 * Exportar t-o-d-o.
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a consultar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param totalRegistros objeto total registros - Numero de registros consultados
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - - Objeto que retorna resultado de la eliminacion
	 * @throws BusinessException objeto business exception - Lanzada cuando se produce un error en el flujo
	 */
	BeanResBase exportarTodo(BeanLiquidezIntradia liquidacion,String canal, int totalRegistros, ArchitechSessionBean session)
			throws BusinessException;
}
