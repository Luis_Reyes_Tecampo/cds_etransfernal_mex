/**
 * Clase: BO para la capa de negocio para la pantalla
 * Monitor Carga Archivos Historicos Detalle CDASPID
 * 
 * @author Vector
 * @version 1.0
 * @since Enero 2017
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;

/**
 * The Interface BOCargaArchivosHistoricosDetalleCDASPID.
 */
public interface BOCargaArchivosHistoricosDetalleCDASPID {
	
	/**
	 * Metodo para consultar los detalles del archivo
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param paginador El parametro paginador
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 * @throws BusinessException 
	 */
	BeanResMonitorCargaArchHistCADSPID consutarDetallesArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean) throws BusinessException;
	
	/**
	 * Metodo para generar el archivo historico
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 * @throws BusinessException 
	 */
	BeanResMonitorCargaArchHistCADSPID generarArchivoHistorico(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean) throws BusinessException;
	
	/**
	 * Metodo para eleminar la seleccion del archivo
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 * @throws BusinessException 
	 */
	BeanResMonitorCargaArchHistCADSPID eliminarSelecionArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean) throws BusinessException;
	
	
	
}
