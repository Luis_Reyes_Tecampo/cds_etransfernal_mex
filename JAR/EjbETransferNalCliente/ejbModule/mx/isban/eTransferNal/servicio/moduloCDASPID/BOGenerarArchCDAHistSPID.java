/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0    Dic 28 17:30:00 CST 2016 	Alan Garcia Villagran   Vector 	Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAHist;

/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico SPID
**/
public interface BOGenerarArchCDAHistSPID {

	/**Metodo que sirve para consultar la fecha de operacion
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
	 * @throws BusinessException 
	   */
	BeanResConsFechaOp consultaFechaOperacionSPID(ArchitechSessionBean architechSessionBean) throws BusinessException;

	/**Metodo que sirve para indicar que se debe de procesar el archivo
	    * de CDA Historico SPID
	    * @param beanReqGenArchCDAHistSPID Objeto del tipo @see BeanReqGenArchCDAHist
	    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	    * @return BeanResProcGenArchCDAHist Objeto del tipo BeanResProcGenArchCDAHist
	 * @throws BusinessException 
	    */
	BeanResProcGenArchCDAHist procesarGenArchHisSPID(BeanReqGenArchCDAHist beanReqGenArchCDAHistSPID, 
    		ArchitechSessionBean architechSessionBean) throws BusinessException;
}
