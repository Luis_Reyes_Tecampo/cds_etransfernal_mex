package mx.isban.eTransferNal.servicio.moduloCDASPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistBO;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonCDAHistSPID;

/**
 * Interfaz del tipo BO que se encarga  del negocio del monitor CDA
**/
@Remote
public interface BOMonitorCDAHistSPID {
	
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param beanReqMonCDAHist Objeto del tipo @see BeanReqMonCDAHist
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaHistBO Objeto del tipo @see BeanResMonitorCdaHistBO
	 * @exception BusinessException Excepcion de negocio
	 */
	BeanResMonitorCdaHistBO obtenerDatosMonitorCDASPID(BeanReqMonCDAHistSPID beanReqMonCDAHist,ArchitechSessionBean architectSessionBean) 
	throws BusinessException;

}
