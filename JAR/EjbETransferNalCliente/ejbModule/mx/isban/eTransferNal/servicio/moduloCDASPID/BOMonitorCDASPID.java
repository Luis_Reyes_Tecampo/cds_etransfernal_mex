package mx.isban.eTransferNal.servicio.moduloCDASPID;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaBO;

/**
 * Interfaz del tipo BO que se encarga  del negocio del monitor CDA
**/
public interface BOMonitorCDASPID {
	
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @exception BusinessException Excepcion de negocio
	 */
	public BeanResMonitorCdaBO obtenerDatosMonitorCDA(ArchitechSessionBean architectSessionBean) 
	throws BusinessException;

	/**
	 * Metodo que sirve para solicitar la detencion del servicio CDA
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaBO Objeto del tipo @see BeanResMonitorCdaBO
	 * @exception BusinessException Excepcion de negocio
	 */
	public BeanResMonitorCdaBO detenerServicioCDA(ArchitechSessionBean architectSessionBean) 
	throws BusinessException;


}
