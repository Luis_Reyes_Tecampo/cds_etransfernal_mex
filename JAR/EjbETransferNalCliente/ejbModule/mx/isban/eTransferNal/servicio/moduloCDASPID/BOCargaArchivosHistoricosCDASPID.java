/**
 * Clase: BO para la capa de negocio para la pantalla
 * Monitor Carga Archivos Historicos CDASPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre 2016
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;

/**
 * The Interface BOCargaArchivosHistoricosCDASPID.
 */
public interface BOCargaArchivosHistoricosCDASPID {
	
	/**
	 * Metodo que define la carga de estatus del archivo
	 * 
	 * @param paginador La variable para la paginacion
	 * @param sessionBean La variable para la session 
	 * @return La lista de respuesta de estatus de archivo
	 * @throws BusinessException 
	 */
	BeanResMonitorCargaArchHistCADSPID obtenerEstatusArchivo(
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean) throws BusinessException;
	
	
}
