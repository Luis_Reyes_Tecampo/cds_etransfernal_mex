/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/12/2013 0:26:12 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.servicio.moduloCDASPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonCDADetSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqConsMovMonHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCDASPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCDASPID;

/**
 * Interfaz del tipo BO que se encarga del negocio para la consulta de
 * Movimientos Historicos CDAs
 * 
 */
@Remote
public interface BOConsultaMovMonHistCDASPID {

	/**
	 * Metodo que se encarga de hacer el llamado al dao para obtener
	 * la informacion de los Movimientos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovHistCDA Objeto del tipo @see BeanResConsMovHistCDA
	 * @throws BusinessException Exception de negocio
	 */
	BeanResConsMovHistCDASPID consultarMovHistCDASPID(
			BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA,
			ArchitechSessionBean architechSessionBean)throws BusinessException;

	 /**
	 * Metodo que se encarga de solicitar la insercion de la peticion de
	 * Exportar Todos de los Movimientos Historicos CDA
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return beanResConMovHistCDA  Objeto del tipo @see BeanResConsMovHistCDA 
	 * @throws BusinessExceptionException de negocio
	 */
	BeanResConsMovHistCDASPID consultaMovHistCDAExpTodosSPID(
			BeanReqConsMovMonHistCDASPID beanReqConsMovHistCDA,
			ArchitechSessionBean architechSessionBean)throws BusinessException;

	 /**
	 * Metodo que sirve para solicitar el detalle de un movimiento Historico CDA
	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovMonCDADet
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovDetHistCDA Objeto del tipo @see BeanResConsMovDetHistCDA
	 * @throws BusinessExceptionException de negocio
	 */
	BeanResConsMovDetHistCDASPID consMovHistDetCDASPID(
			BeanReqConsMovMonCDADetSPID beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean)throws BusinessException;

}