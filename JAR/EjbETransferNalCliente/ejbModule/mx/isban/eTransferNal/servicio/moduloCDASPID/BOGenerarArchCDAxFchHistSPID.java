/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAxFchHistSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Jan 04 18:24:31 CST 2017 jjbeltran  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.servicio.moduloCDASPID;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOp;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResProcGenArchCDAxFchHist;


/**
 *Clase del tipo BO que se encarga  del negocio para la funcionalidad
 * de Generar Archivo CDA Historico
**/
@Remote
public interface BOGenerarArchCDAxFchHistSPID{

   /**Metodo que sirve para consultar la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOp Objeto del tipo BeanResConsFechaOp
 * @throws BusinessException 
   */
   BeanResConsFechaOp consultaFechaOperacion(ArchitechSessionBean architechSessionBean) throws BusinessException;
   /**Metodo que sirve para indicar que se debe de procesar el archivo
    * de CDA Historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResProcGenArchCDAxFchHist Objeto del tipo BeanResProcGenArchCDAxFchHist
 * @throws BusinessException 
    */
    BeanResProcGenArchCDAxFchHist procesarGenArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist, 
    		ArchitechSessionBean architechSessionBean) throws BusinessException;

}
