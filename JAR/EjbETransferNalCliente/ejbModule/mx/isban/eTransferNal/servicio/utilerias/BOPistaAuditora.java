/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BOPistaAuditora.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/08/2018 06:29:23 PM Moises Trejo Hernandez. Isban Creacion
 */

package mx.isban.eTransferNal.servicio.utilerias;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.utilerias.BeanPistaAuditora;
/**
 * Interface BOPistaAuditora.
 *
 * @author mtrejo
 */
@Remote
public interface BOPistaAuditora {
	
	/**
	 * Procedimiento para el llenado de datos de la Pista Audiora Catalogos.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 * @return Objeto bean res guarda bitacora DAO
	 */
	BeanResGuardaBitacoraDAO llenaPistaAuditora(BeanPistaAuditora beanPista, ArchitechSessionBean sesion);
	
	
	
	
	/**
	 * Llena pista auditora admon.
	 *
	 * @param beanPista El objeto: bean pista
	 * @param sesion El objeto: sesion
	 * @return Objeto bean res guarda bitacora DAO
	 * @throws BusinessException La business exception
	 */
	BeanResGuardaBitacoraDAO llenaPistaAuditoraAdmon(BeanPistaAuditora beanPista, ArchitechSessionBean sesion)throws BusinessException; 
}
