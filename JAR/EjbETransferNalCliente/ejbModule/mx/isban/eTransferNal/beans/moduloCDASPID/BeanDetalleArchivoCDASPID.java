package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

/**
 * The Class BeanDetalleArchivoCDASPID.
 */
public class BeanDetalleArchivoCDASPID implements Serializable {

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = -8279054421457984910L;
	
	/**
	 * La variable idArchivo
	 */
	private String idArchivo;
	
	/**
	 * La variable nombreArchivo
	 */
	private String nombreArchivo;
	
	/**
	 * La variable estatusArchivo
	 */
	private String estatusArchivo;
	
	/**
	 * La variable de claveRastreo
	 */
	private String claveRastreo;
	
	/**
	 * La variable cuentaBeneficiario
	 */
	private String cuentaBeneficiario;
	
	/**
	 * La variable montoPago
	 */
	private String montoPago;
	
	/**
	 * La variable institucionEmisora
	 */
	private String institucionEmisora;
	
	/**
	 * La variable fechaOprecion
	 */
	private String fechaOperacion;
	
	/**
	 * La variable claveInstitucion
	 */
	private String claveInstitucion;
	
	/**
	 * La variable institucionOrdinaria
	 */
	private String institucionOrdinaria;
	
	/**
	 * La variable folioPaquete
	 */
	private String folioPaquete;
	
	/**
	 * La variable folioPago
	 */
	private String folioPago;
	
	/**
	 * La variable seleccionado
	 */
	private boolean seleccionado;
	
	/**
	 * Metodo para obtener la variable nombreArchivo
	 * 
	 * @return La variable nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Metodo para establecer la variable nombreArchivo
	 * 
	 * @param nombreArchivo La variable nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 * Metodo para obtener la variable idArchivo
	 * 
	 * @return La variable idArchivo
	 */
	public String getIdArchivo() {
		return idArchivo;
	}

	/**
	 * Metodo para establecer la variable idArchivo
	 * 
	 * @param idArchivo La variable idArchivo
	 */
	public void setIdArchivo(String idArchivo) {
		this.idArchivo = idArchivo;
	}

	/**
	 * Metodo para obtener la variable estatusArchivo
	 * 
	 * @return La variable estatusArchivo
	 */
	public String getEstatusArchivo() {
		return estatusArchivo;
	}

	/**
	 * Metodo para establecer la variable estatusArchivo
	 * 
	 * @param estatusArchivo La variable estatusArchivo 
	 */
	public void setEstatusArchivo(String estatusArchivo) {
		this.estatusArchivo = estatusArchivo;
	}

	/**
	 * Metodo para obtener la variable calveRasteo
	 * 
	 * @return La variable claveRasteo
	 */
	public String getClaveRastreo() {
		return claveRastreo;
	}

	/**
	 * Metodo para establecer la variable claveRastreo
	 * 
	 * @param claveRastreo La variable claveRastreo
	 */
	public void setClaveRastreo(String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}

	/**
	 * Metodo para obtener la variable cuentaBeneficiario
	 * 
	 * @return La variable cuentaBeneficiario
	 */
	public String getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	/**
	 * Metodo para obtener la variable cuentaBeneficiario
	 * 
	 * @param cuentaBeneficiario La variable cuentaBeneficiario
	 */
	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	/**
	 * Metodo para obtener la variable montoPago
	 * 
	 * @return La variable montoPago
	 */
	public String getMontoPago() {
		return montoPago;
	}

	/**
	 * Metodo para establecer la variable montoPago
	 * 
	 * @param montoPago La variable montoPago
	 */
	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}

	/**
	 * Metodo para obtener la variable institucionEmisora
	 * 
	 * @return La variable institucionEmisora
	 */
	public String getInstitucionEmisora() {
		return institucionEmisora;
	}

	/**
	 * Metodo para establecer la variable institucionEmisora
	 * 
	 * @param institucionEmisora La variable institucionEmisora
	 */
	public void setInstitucionEmisora(String institucionEmisora) {
		this.institucionEmisora = institucionEmisora;
	}

	/**
	 * Metodo para obtener la variable fechaOperacion
	 * 
	 * @return La variable fechaOperacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo para establecer la variable idArchivo
	 * 
	 * @param fechaOperacion La variable fechaOperacion
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * Metodo para obtener la variable claveInstitucion
	 * 
	 * @return La variable claveInstitucion
	 */
	public String getClaveInstitucion() {
		return claveInstitucion;
	}

	/**
	 * Metodo para establecer la variable claveInstitucion
	 * 
	 * @param claveInstitucion La variable claveInstitucion
	 */
	public void setClaveInstitucion(String claveInstitucion) {
		this.claveInstitucion = claveInstitucion;
	}

	/**
	 * Metodo para obtener la variable institucionOrdinaria
	 * 
	 * @return La variable institucionOrdinaria
	 */
	public String getInstitucionOrdinaria() {
		return institucionOrdinaria;
	}

	/**
	 * Metodo para obtener la variable institucionOrdinaria
	 * 
	 * @param institucionOrdinaria La variable institucionOrdinaria
	 */
	public void setInstitucionOrdinaria(String institucionOrdinaria) {
		this.institucionOrdinaria = institucionOrdinaria;
	}

	/**
	 * Metodo para obtener la variable folioPaquete
	 * 
	 * @return La variable folioPaquete
	 */
	public String getFolioPaquete() {
		return folioPaquete;
	}

	/**
	 * Metodo para establecer la variable folioPaquete
	 * 
	 * @param folioPaquete La variable folioPaquete
	 */
	public void setFolioPaquete(String folioPaquete) {
		this.folioPaquete = folioPaquete;
	}

	/**
	 * Metodo para obtener la variable folioPago
	 * 
	 * @return La variable folioPago
	 */
	public String getFolioPago() {
		return folioPago;
	}

	/**
	 * Metodo para establecer la variable folioPago
	 * 
	 * @param folioPago La variable folioPago
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}

	/**
	 * MMetodo para establecer la variable seleccionado 
	 * 
	 * @return La variable seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Metodo para establecer la variable seleccionado
	 * 
	 * @param seleccionado La variable seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

}
