/**
 * Clase: Bean que transporta la lista de archivos de la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre del 2016 
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * The Class BeanRespuestaGenericoCADSPID.
 */
public class BeanRespuestaGenericoCADSPID implements BeanResultBO, Serializable {

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = -1652843298896503285L;
	
	
	/**
	 * La variable paginador
	 */
	private BeanPaginador paginador;
	
	/**
	 * La variable codigo de error
	 */
	private String codigoError;
	
	/**
	 * La variable mensaje de error
	 */
	private String mensajeError;
	
	/**
	 * La variable registro archivo
	 */
	private String registrosArchivo;
	
	/**
	 * La variable resgistro encontrados
	 */
	private String registrosEncontrados;
	
	/**
	 * Metodo para acceder a la variable paginador
	 * 
	 * @return El paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo para establecer la variable paginador
	 * 
	 * @param paginador La variable paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Metodo para acceder a la variable codigoError
	 *  
	 * @return la variable codigoError
	 */
	public String getCodigoError() {
		return codigoError;
	}

	/**
	 * Metodo para establecer la variable codigoError
	 * 
	 * @param codigoError la variable codigoError
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	/**
	 * Metodo para acceder a la variable mensajeError
	 * 
	 * @return la variable mensajeError
	 */
	public String getMensajeError() {
		return mensajeError;
	}

	/**
	 * Metodo para establecer la variable mensajeError
	 * 
	 * @param mensajeError la variable mensajeError
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	/**
	 * Metodo para obtener la variable registrosArchivo
	 * 
	 * @return La variable registrosArchivo
	 */
	public String getRegistrosArchivo() {
		return registrosArchivo;
	}

	/**
	 * Metodo para establecer la variable registrosArchivo
	 * 
	 * @param registrosArchivo La variable registrosArchivo
	 */
	public void setRegistrosArchivo(String registrosArchivo) {
		this.registrosArchivo = registrosArchivo;
	}

	/**
	 * Metodo para establecer la variable registrosEncontrados
	 * 
	 * @return La variable registrosEncontrados
	 */
	public String getRegistrosEncontrados() {
		return registrosEncontrados;
	}

	/**
	 * Metodo para establecer la variable registroEncontrados
	 * 
	 * @param registrosEncontrados La variable registrosEncontrados
	 */
	public void setRegistrosEncontrados(String registrosEncontrados) {
		this.registrosEncontrados = registrosEncontrados;
	}

	@Override
	public String getCodError() {
		return this.codigoError;
	}

	@Override
	public String getMsgError() {
		return this.mensajeError;
	}

	@Override
	public void setCodError(String codigoError) {
		this.codigoError = codigoError;
	}

	@Override
	public void setMsgError(String mensajeError) {
		this.mensajeError = mensajeError;	
	}
	

}
