/**
 * Clase: Bean que transporta la lista de archivos de la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre del 2016 
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 * The Class BeanResMonitorCargaArchHistCADSPID.
 */
public class BeanResMonitorCargaArchHistCADSPID extends BeanRespuestaGenericoCADSPID {

	/** La variable de serializacion. */
	private static final long serialVersionUID = -1652843298896503285L;
	
	/** La variable paginador. */
	private BeanPaginador paginador;
	
	/** La variable lista de resultado. */
	private List<BeanMonitorCargaArchHistCADSPID> listaResultado;
	
	/** La variable lista de detalle. */
	private List<BeanDetalleArchivoCDASPID> listaDetalle;
	
	/** La variable registros totales. */
	private Integer registrosTotales;
	
	/**
	 * Metodo para acceder la variable paginador.
	 *
	 * @return paginador La variable paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo para acceder la variable paginador.
	 *
	 * @param paginador La variable paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	
	/**
	 * Matodo para establecer la variable listaResultado.
	 *
	 * @return la variable listaResultado
	 */
	public List<BeanMonitorCargaArchHistCADSPID> getListaResultado() {
		return listaResultado;
	}

	/**
	 * Metodo para establecer la variable listaResultado.
	 *
	 * @param listaResultado La variable
	 */
	public void setListaResultado(List<BeanMonitorCargaArchHistCADSPID> listaResultado) {
		this.listaResultado = listaResultado;
	}

	/**
	 * Metodo para obtener la variable listaDetalle.
	 *
	 * @return La variable listaDetalle
	 */
	public List<BeanDetalleArchivoCDASPID> getListaDetalle() {
		return listaDetalle;
	}

	/**
	 * Metodo para establecer la variable listaDetalle .
	 *
	 * @param listaDetalle La variable listaDetalle
	 */
	public void setListaDetalle(List<BeanDetalleArchivoCDASPID> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	/**
	 * Metodo para obtener los regristros totales.
	 *
	 * @return Los registros totales
	 */
	public Integer getRegistrosTotales() {
		return registrosTotales;
	}
	
	/**
	 * Metodo para establecer los registros totales.
	 *
	 * @param registrosTotales La variable registrosTotales
	 */
	public void setRegistrosTotales(Integer registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
}
