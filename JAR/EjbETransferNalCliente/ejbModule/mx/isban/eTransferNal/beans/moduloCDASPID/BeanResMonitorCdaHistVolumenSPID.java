/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version Date/Hour           By       Company Description
 * ------- ------------------- -------- ------- --------------
 * 1.0     09/11/2013 11:33:48  jlleon   INDRA   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

/**
 * Clase del tipo Bean DTO que se encarga de encapsular los datos de retorno de
 * la funcionalidad de monitor CDA
 * 
 **/
public class BeanResMonitorCdaHistVolumenSPID implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 6710905384333541272L;

	/** Propiedad del tipo que almacena el volumen de CDA's recibidas aplicadas */
	private String volumenCDAOrdRecAplicadas;
	/**
	 * Propiedad del tipo que almacena el volumen de CDA's Pendientes por enviar
	 */
	private String volumenCDAPendEnviar;
	/** Propiedad del tipo que almacena el volumen de CDA's enviadas */
	private String volumenCDAEnviadas;
	/** Propiedad del tipo que almacena el volumen de CDA's confirmadas */
	private String volumenCDAConfirmadas;
	/**
	 * Propiedad del tipo que almacena el volumen de CDA's realizadas en
	 * contingencia
	 */
	private String volumenCDAContingencia;
	/** Propiedad del tipo que almacena el volumen de CDA's rechazadas */
	private String volumenCDARechazadas;

	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAOrdRecAplicadas
	 * 
	 * @return volumenCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public String getVolumenCDAOrdRecAplicadas() {
		return volumenCDAOrdRecAplicadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAPendEnviar
	 * 
	 * @param volumenCDAPendEnviar
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDAPendEnviar(String volumenCDAPendEnviar) {
		this.volumenCDAPendEnviar = volumenCDAPendEnviar;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAEnviadas
	 * 
	 * @return volumenCDAEnviadas Objeto del tipo @see String
	 */
	public String getVolumenCDAEnviadas() {
		return volumenCDAEnviadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAOrdRecAplicadas
	 * 
	 * @param volumenCDAOrdRecAplicadas
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDAOrdRecAplicadas(String volumenCDAOrdRecAplicadas) {
		this.volumenCDAOrdRecAplicadas = volumenCDAOrdRecAplicadas;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAPendEnviar
	 * 
	 * @return volumenCDAPendEnviar Objeto del tipo @see String
	 */
	public String getVolumenCDAPendEnviar() {
		return volumenCDAPendEnviar;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAEnviadas
	 * 
	 * @param volumenCDAEnviadas
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDAEnviadas(String volumenCDAEnviadas) {
		this.volumenCDAEnviadas = volumenCDAEnviadas;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAContingencia
	 * 
	 * @return volumenCDAContingencia Objeto del tipo @see String
	 */
	public String getVolumenCDAContingencia() {
		return volumenCDAContingencia;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAContingencia
	 * 
	 * @param volumenCDAContingencia
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDAContingencia(String volumenCDAContingencia) {
		this.volumenCDAContingencia = volumenCDAContingencia;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAConfirmadas
	 * 
	 * @return volumenCDAConfirmadas Objeto del tipo @see String
	 */
	public String getVolumenCDAConfirmadas() {
		return volumenCDAConfirmadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAConfirmadas
	 * 
	 * @param volumenCDAConfirmadas
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDAConfirmadas(String volumenCDAConfirmadas) {
		this.volumenCDAConfirmadas = volumenCDAConfirmadas;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDARechazadas
	 * 
	 * @return volumenCDARechazadas Objeto del tipo @see String
	 */
	public String getVolumenCDARechazadas() {
		return volumenCDARechazadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad volumenCDARechazadas
	 * 
	 * @param volumenCDARechazadas
	 *            Objeto del tipo @see String
	 */
	public void setVolumenCDARechazadas(String volumenCDARechazadas) {
		this.volumenCDARechazadas = volumenCDARechazadas;
	}
}
