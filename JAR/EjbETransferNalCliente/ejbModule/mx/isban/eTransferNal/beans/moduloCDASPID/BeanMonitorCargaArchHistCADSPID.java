/**
 * Clase: Bean para transportar datos de la pantalla
 * Monitor Carga Achivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0 
 * @since Diciembre del 2016
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

/**
 * The Class BeanMonitorCargaArchHistCADSPID.
 */
public class BeanMonitorCargaArchHistCADSPID implements Serializable{

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = 2502799359013808278L;
	
	/**
	 * La variable idArchivo
	 */
	private String idArchivo;
	
	/**
	 * La variable para el nombre del archivo
	 */
	private String nombreArchivo;
	
	/**
	 * La variable para el estatus del archivo
	 */
	private String estatusArchivo;
	
	/**
	 * Metodo para obtener la variable idArchivo
	 * 
	 * @return La variable idArchivo
	 */
	public String getIdArchivo() {
		return idArchivo;
	}

	/**
	 * Metodo para establecer la variable idArchivo
	 * 
	 * @param idArchivo La variable idArchivo
	 */
	public void setIdArchivo(String idArchivo) {
		this.idArchivo = idArchivo;
	}

	/**
	 * Metodo para acceder a la variable nombreArchivo
	 * 
	 * @return  nombreArchivo La variable nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Metodo para establecer la variable nombreArchivo 
	 * 
	 * @param nombreArchivo La variable nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Metodo para acceder a la variable estatusArchivo
	 * 
	 * @return estatusArchivo La variable estatusArchivo
	 */
	public String getEstatusArchivo() {
		return estatusArchivo;
	}

	/**
	 * Metodo para establecer la variable estatusArchivo
	 * 
	 * @param estatusArchivo El parametro estatusArchivo
	 */
	public void setEstatusArchivo(String estatusArchivo) {
		this.estatusArchivo = estatusArchivo;
	}

}
