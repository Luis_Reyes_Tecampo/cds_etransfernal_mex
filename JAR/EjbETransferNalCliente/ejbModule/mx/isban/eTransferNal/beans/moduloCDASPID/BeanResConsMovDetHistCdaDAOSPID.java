/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovDetHistCdaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaBeneficiario;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDatosGenerales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaOrdenante;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  Consulta Detalle de Movimiento CDA
**/
public class BeanResConsMovDetHistCdaDAOSPID implements BeanResultBO, Serializable{

		/**
		 * Constante privada del Serial version
		 */		
		private static final long serialVersionUID = 6303826664918569687L;
		/**Propiedad del tipo BeanResConsMovCdaDatosGenerales que almacena bean con datos generales*/
		private BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales;
		/**Propiedad del tipo BeanResConsMovCdaBeneficiario que almacena el bean con datos del beneficiario*/
		private BeanResConsMovCdaBeneficiario beanConsMovCdaBeneficiario;
		/**Propiedad del tipo BeanResConsMovCdaOrdenante que almacena el bean con datos ordenante*/
		private BeanResConsMovCdaOrdenante beanConsMovCdaOrdenante;
		/**Propiedad privada del tipo String que almacena el codigo de mensaje de error*/
		private String msgError;
		/**Propiedad privada del tipo String que almacena el codigo de error*/
		private String codError;
		

			/**
			 *  Metodo que sirve para obtener la propiedad beanMovCdaDatosGenerales
			 * @return beanConsMovCdaOrdenante Objeto de tipo @see BeanResConsMovCdaOrdenante
			 */
			public BeanResConsMovCdaOrdenante getBeanConsMovCdaOrdenante() {
				return beanConsMovCdaOrdenante;
			}
			/**
			 * Modifica el valor de la propiedad beanMovCdaDatosGenerales
			 * @param beanConsMovCdaOrdenante Objeto de tipo @see BeanResConsMovCdaOrdenante
			 */
			public void setBeanConsMovCdaOrdenante(
					BeanResConsMovCdaOrdenante beanConsMovCdaOrdenante) {
				this.beanConsMovCdaOrdenante = beanConsMovCdaOrdenante;
			}
		
			/**
			 * Metodo que sirve para obtener la propiedad beaConsMovCdaBeneficiario
			 * @return el beanConsMovCdaBeneficiario Objeto de tipo @see BeanResConsMovCdaBeneficiario
			 */
			public BeanResConsMovCdaBeneficiario getBeanConsMovCdaBeneficiario() {
				return beanConsMovCdaBeneficiario;
			}
			/**
			 * Modifica el valor de la propiedad beanMovCdaDatosGenerales beaConsMovCdaBeneficiario
			 * @param beanConsMovCdaBeneficiario Objeto de tipo @see BeanResConsMovCdaBeneficiario
			 */
			public void setBeanConsMovCdaBeneficiario(
					BeanResConsMovCdaBeneficiario beanConsMovCdaBeneficiario) {
				this.beanConsMovCdaBeneficiario = beanConsMovCdaBeneficiario;
			}
			
			/**
			 * Metodo que sirve para obtener la propiedad beanMovCdaDatosGenerales
			 * @return beanMovCdaDatosGenerales Objeto de tipo @see BeanResConsMovCdaDatosGenerales
			 */
			public BeanResConsMovCdaDatosGenerales getBeanMovCdaDatosGenerales() {
				return beanMovCdaDatosGenerales;
			}
			/**
			 * Modifica el valor de la propiedad beanMovCdaDatosGenerales
			 * @param beanMovCdaDatosGenerales Objeto de tipo @see BeanResConsMovCdaDatosGenerales
			 */
			public void setBeanMovCdaDatosGenerales(
					BeanResConsMovCdaDatosGenerales beanMovCdaDatosGenerales) {
				this.beanMovCdaDatosGenerales = beanMovCdaDatosGenerales;
			}

		   /**
		   *Metodo get que sirve para obtener el valor de la propiedad msgError
		   * @return msgError Objeto del tipo String
		   */
		   public String getMsgError(){
		      return msgError;
		   }
		   /**
		   *Modifica el valor de la propiedad msgError
		   * @param msgError Objeto del tipo String
		   */
		   public void setMsgError(String msgError){
		      this.msgError=msgError;
		   }
		   /**
		   *Metodo get que sirve para obtener el valor de la propiedad codError
		   * @return codError Objeto del tipo String
		   */
		   public String getCodError(){
		      return codError;
		   }
		   /**
		   *Modifica el valor de la propiedad codError
		   * @param codError Objeto del tipo String
		   */
		   public void setCodError(String codError){
		      this.codError=codError;
		   }
}
