/**
 * Clase: Bean que transporta la lista de archivos de la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre del 2016 
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * The Class BeanReqMonitorCargaArchHistCADSPID.
 */
public class BeanReqMonitorCargaArchHistCADSPID implements Serializable {

	/**
	 * La variable de serializacion
	 */
	private static final long serialVersionUID = -1652843298896503285L;
	
	/**
	 * La variable listaDetalle
	 */
	private List<BeanDetalleArchivoCDASPID> listaDetalle = new ArrayList<BeanDetalleArchivoCDASPID>();
	
	/**
	 * La variable de paginador
	 */
	private BeanPaginador paginador = new BeanPaginador();
	
	/**
	 * La variable de idArchivo
	 */
	private String idArchivo;
	
	/**
	 * La variable nombreArchivo
	 */
	private String nombreArchivo;
	
	/**
	 * La variable estatusArchivo
	 */
	private String estatusArchivo;
	
	/**
	 * Metodo para acceder a la variable listaDetalle
	 * 
	 * @return La variable listaDetalle
	 */
	public List<BeanDetalleArchivoCDASPID> getListaDetalle() {
		return listaDetalle;
	}
	
	/**
	 * Metodo para establecer la variable listaDetalle
	 * 
	 * @param listaDetalle La variable listaDetalle
	 */
	public void setListaDetalle(List<BeanDetalleArchivoCDASPID> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	/**
	 * Metodo para obtener la variable idArchivo
	 * 
	 * @return La variable idArchivo
	 */
	public String getIdArchivo() {
		return idArchivo;
	}

	/**
	 * Metodo para establecer la variable id Archivo
	 * 
	 * @param idArchivo La variable idArchivo
	 */
	public void setIdArchivo(String idArchivo) {
		this.idArchivo = idArchivo;
	}	

	/**
	 * Metodo para obtener la variable estatusArchivo
	 * 
	 * @return La variable estatusArchivo
	 */
	public String getEstatusArchivo() {
		return estatusArchivo;
	}

	/**
	 * Metodo para establecer la variable estatusArchivo
	 * 
	 * @param estatusArchivo La variable estatusArchivo
	 */
	public void setEstatusArchivo(String estatusArchivo) {
		this.estatusArchivo = estatusArchivo;
	}

	/**
	 * Metodo para obtener la variable nombreArchivo
	 * 
	 * @return La variable nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Metodo para establecer la variable nombreArchivo
	 * 
	 * @param nombreArchivo La variable nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Metodo para acceder a la variable paginador
	 * 
	 * @return paginador La variable paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo para establecer la variable paginador
	 * 
	 * @param paginador La variable paginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

}
