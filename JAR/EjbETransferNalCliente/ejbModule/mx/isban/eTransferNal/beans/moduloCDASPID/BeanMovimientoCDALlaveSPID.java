/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanMovimientoCDALlave.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Mon Dec 09 10:02:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;
/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 *  para la funcionalidad de Bean Movimiento
**/
public class BeanMovimientoCDALlaveSPID implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1995795233722402993L;
	/**Propiedad de tipo String que almacena la fecha de Operacion*/
	private	String fechaOpe;
	/**Propiedad de tipo String que almacena la cve de mi Instituc*/
	private	String cveMiInstituc;
	/**Propiedad de tipo String que almacena la clave SPEI*/
	private	String cveSpei;
	/**Propiedad de tipo String que almacena el folio de Paquete*/
	private	String folioPaq;
	/**Propiedad de tipo String que almacena el folio de Pago*/
	private	String folioPago;
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveSpei
	 * @return cveSpei Objeto del tipo @see String
	 */
	public String getCveSpei() {
		return cveSpei;
	}
	/**
	 * Modifica el valor de la propiedad cveSpei
	 * @param cveSpei Objeto del tipo @see String
	 */
	public void setCveSpei(String cveSpei) {
		this.cveSpei = cveSpei;
	}
	
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad fechaOpe
	 * @return fechaOpe Objeto del tipo @see String
	 */
	public String getFechaOpe() {
		return fechaOpe;
	}
	/**
	 * Modifica el valor de la propiedad fechaOpe
	 * @param fechaOpe Objeto del tipo @see String
	 */
	public void setFechaOpe(String fechaOpe) {
		this.fechaOpe = fechaOpe;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPaq
	 * @return folioPaq Objeto del tipo @see String
	 */
	public String getFolioPaq() {
		return folioPaq;
	}
	/**
	 * Modifica el valor de la propiedad folioPaq
	 * @param folioPaq Objeto del tipo @see String
	 */
	public void setFolioPaq(String folioPaq) {
		this.folioPaq = folioPaq;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad folioPago
	 * @return folioPago Objeto del tipo @see String
	 */
	public String getFolioPago() {
		return folioPago;
	}
	/**
	 * Modifica el valor de la propiedad folioPago
	 * @param folioPago Objeto del tipo @see String
	 */
	public void setFolioPago(String folioPago) {
		this.folioPago = folioPago;
	}
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad cveMiInstituc
	 * @return cveMiInstituc Objeto del tipo @see String
	 */
	public String getCveMiInstituc() {
		return cveMiInstituc;
	}
	/**
	 * Modifica el valor de la propiedad cveMiInstituc
	 * @param cveMiInstituc Objeto del tipo @see String
	 */
	public void setCveMiInstituc(String cveMiInstituc) {
		this.cveMiInstituc = cveMiInstituc;
	}

}
