/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 19:44:43 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase Bean DTO que se encarga de encapsular los parametros necesarios
 * de la funcionalidad  de Consulta de Movimientos Historicos CDA
**/
public class BeanResConsMovHistCDASPID implements Serializable{

	/**
	 *  Constante privada del Serial version
	 */
	private static final long serialVersionUID = 4278699478193021963L;
	
	/**Propiedad del tipo List<BeanContingMismoDia> que almacena una lista de objetos del tipo
	 * @see BeanContingMismoDia*/
	private List<BeanMovimientoCDASPID> listBeanMovimientoCDA = Collections.emptyList();
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador beanPaginador = null;
	/** Propiedad del tipo String que almacena nombre de archivo generado */
	private String nombreArchivo = "";
	 /**Propiedad privada del tipo String que almacena el codigo de error*/
	   private String codError = "";
	   /**Propiedad privada del tipo String que almacena el codigo de mensaje
	   de error*/
	   private String msgError = "";
	   /**Propiedad privada del tipo String que almacena el tipo de error*/
	   private String tipoError;
	   
		
	
	 /**
	    * Metodo get que sirve para obtener la lista de objetos BeanMovimientoCDA
	    * @return listBeanMovimientoCDA lista de objetos del tipo BeanMovimientoCDA
	    */
	   public List<BeanMovimientoCDASPID> getListBeanMovimientoCDA() {
		return listBeanMovimientoCDA;
	   }
	   
	   /**
	    * Metodo set que sirve para setear la lista de objetos BeanMovimientoCDA
	    * @param listBeanMovimientoCDA lista de objetos del tipo BeanMovimientoCDA
	    */
	   public void setListBeanMovimientoCDA(
			List<BeanMovimientoCDASPID> listBeanMovimientoCDA) {
		this.listBeanMovimientoCDA = listBeanMovimientoCDA;
	   }

	   /**
	    * Metodo get que obtiene el total de registros
	    * @return Integer objeto con el numero total de registros
	    */   
	   public Integer getTotalReg() {
		return totalReg;
	   }

	   /**
	    * Metodo set que sirve para setear el total de registros
	    * @param totalReg Objeto del tipo Integer
	    */
		public void setTotalReg(Integer totalReg) {
			this.totalReg = totalReg;
		}
	   /**
	    *Metodo get que sirve para obtener el BeanPaginador
	    * @return BeanPaginador Objeto del tipo BeanPaginador
	    */   
	   public BeanPaginador getBeanPaginador() {
		   return beanPaginador;
	   }
	   /**
	    *Modifica el valor de la propiedad BeanPaginador
	    * @param beanPaginador Objeto del tipo BeanPaginador
	    */
	   public void setBeanPaginador(BeanPaginador beanPaginador) {
		    this.beanPaginador = beanPaginador;
	   }
	   
	    /**
	    *Metodo get que sirve para obtener el valor de la propiedad msgError
	    * @return msgError Objeto del tipo String
	    */
	    public String getMsgError(){
	       return msgError;
	    }
	    
	    /**
	    *Modifica el valor de la propiedad msgError
	    * @param msgError Objeto del tipo String
	    */
	    public void setMsgError(String msgError){
	       this.msgError=msgError;
	    }
		   
	   /**
	    *Metodo get que sirve para obtener el valor de la propiedad codError
	    * @return codError Objeto del tipo String
	    */
	    public String getCodError(){
	       return codError;
	    }
	    
	    /**
	    *Modifica el valor de la propiedad codError
	    * @param codError Objeto del tipo String
	    */
	    public void setCodError(String codError){
	       this.codError=codError;
	    }
		    
	    /**
	     *Metodo get que sirve para obtener el valor del tipo de error
	     * @return tipoError Objeto del tipo String
	     */
	 	 public String getTipoError() {
	 		return tipoError;
	 	 }
	 	 
	 	 /**
	    *Modifica el valor de la propiedad tipoError
	    * @param tipoError Objeto del tipo String
	    */
	 	 public void setTipoError(String tipoError) {
	 		this.tipoError = tipoError;
	 	 }
	 	
	 	 /**
	 	 * Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
	 	 * @param nombreArchivo Objeto del tipo String 
	 	 */
	 	public void setNombreArchivo(String nombreArchivo) {
	 		this.nombreArchivo = nombreArchivo;
	 	}

	 	/**
	 	 * Modifica el valor de la propiedad nombreArchivo
	 	 * @return nombreArchivo Objeto del tipo String
	 	 */
	 	public String getNombreArchivo() {
	 		return nombreArchivo;
	 	}
}