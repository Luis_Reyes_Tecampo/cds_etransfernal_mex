/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovHistCdaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanMovimientoCDA;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  Consulta de Movimientos Historicos CDA
**/
public class BeanResConsMovHistCdaDAOSPID implements BeanResultBO, Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 1717982834779019437L;
	/**
	 * Propiedad del tipo List<BeanMovimientoCDA> que almacena una lista de
	 * objetos del tipo	 BeanMovimientoCDA 
	 * @see BeanMovimientoCDA
	 */
	private List<BeanMovimientoCDASPID> listBeanMovimientoCDA = Collections
			.emptyList();
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	/** Propiedad del tipo String que almacena nombre de archivo generado */
	private String nombreArchivo = "";
	/** Propiedad del tipo @see String que almacena el codigo de error */
	private String codError;
	/** Propiedad del tipo @see String que almacena el mensaje de error */
	private String msgError;

	
	/**
	 * Metodo que devuelve una lista de Movimientos CDA
	 * @return listBeanMovimientoCDA Objeto del tipo @see List<BeanMovimientoCDA>
	 */
	public List<BeanMovimientoCDASPID> getListBeanMovimientoCDA() {
		return listBeanMovimientoCDA;
	}

	/**
	 * Metodo que modifica la lista de movimientos CDA
	 * @param listBeanMovimientoCDA el listBeanMovimientoCDA a establecer
	 */
	public void setListBeanMovimientoCDA(
			List<BeanMovimientoCDASPID> listBeanMovimientoCDA) {
		this.listBeanMovimientoCDA = listBeanMovimientoCDA;
	}

	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad msgError
	   * @return msgError Objeto del tipo String
	   */
	   public String getMsgError(){
	      return msgError;
	   }
	   
	   /**
	   *Modifica el valor de la propiedad msgError
	   * @param msgError Objeto del tipo String
	   */
	   public void setMsgError(String msgError){
	      this.msgError=msgError;
	   }

	/**
	   *Metodo get que sirve para obtener el valor de la propiedad codError
	   * @return codError Objeto del tipo String
	   */
	   public String getCodError(){
	      return codError;
	   }
	   
	   /**
	   *Modifica el valor de la propiedad codError
	   * @param codError Objeto del tipo String
	   */
	   public void setCodError(String codError){
	      this.codError=codError;
	   }
	   
	   /**
		 * Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
		 * @param nombreArchivo Objeto del tipo String 
		 */
		public void setNombreArchivo(String nombreArchivo) {
			this.nombreArchivo = nombreArchivo;
		}

		/**
		 * Modifica el valor de la propiedad nombreArchivo
		 * @return nombreArchivo Objeto del tipo String
		 */
		public String getNombreArchivo() {
			return nombreArchivo;
		}

		/**
		 * Metodo que obtiene el total de registros
		 * @return el totalReg Objeto del tipo @see Integer
		 */
		public Integer getTotalReg() {
			return totalReg;
		}

		/**
		 * Metodo que modifica el valor del total de registros
		 * @param totalReg el totalReg a establecer
		 */
		public void setTotalReg(Integer totalReg) {
			this.totalReg = totalReg;
		}
}
