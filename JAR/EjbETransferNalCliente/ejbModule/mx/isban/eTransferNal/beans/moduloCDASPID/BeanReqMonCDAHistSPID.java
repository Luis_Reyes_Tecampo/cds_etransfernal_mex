package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;


/**
 * The Class BeanReqMonCDAHistSPID.
 */
public class BeanReqMonCDAHistSPID implements Serializable{

	/**
	 * Constante del Serial Version
	 */
	private static final long serialVersionUID = -6662896370230581432L;
	

	/**
	 * Propiedad del tipo String que almacena el valor de la fechaOperacion
	 */
	private String fechaOperacion = "";

	/**
	 * Metodo get que obtiene el valor de la propiedad fechaOperacion
	 * @return fechaOperacion Objeto de tipo @see String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fechaOperacion
	 * @param fechaOperacion Objeto de tipo @see String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	
}
