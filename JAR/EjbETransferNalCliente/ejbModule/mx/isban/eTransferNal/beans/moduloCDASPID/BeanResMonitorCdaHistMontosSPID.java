/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version Date/Hour           By       Company Description
 * ------- ------------------- -------- ------- --------------
 * 1.0     09/11/2013 11:33:48  jlleon   INDRA   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDASPID;

import java.io.Serializable;

/**
 * Clase del tipo Bean DTO que se encarga de encapsular los datos de retorno de
 * la funcionalidad de monitor CDA
 * 
 **/
public class BeanResMonitorCdaHistMontosSPID implements Serializable {

	/**
	 * Constante del serial version
	 */
	private static final long serialVersionUID = 6710905384333541272L;
	/** Propiedad del tipo que almacena el numero de CDA's recibidas aplicadas */
	private String montoCDAOrdRecAplicadas;
	/** Propiedad del tipo que almacena el numero de CDA's Pendientes por enviar */
	private String montoCDAPendEnviar;
	/** Propiedad del tipo que almacena el numero de CDA's enviadas */
	private String montoCDAEnviadas;
	/** Propiedad del tipo que almacena el numero de CDA's confirmadas */
	private String montoCDAConfirmadas;
	/**
	 * Propiedad del tipo que almacena el numero de CDA's realizadas en
	 * contingencia
	 */
	private String montoCDAContingencia;
	/** Propiedad del tipo que almacena el numero de CDA's rechazadas */
	private String montoCDARechazadas;

	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAOrdRecAplicadas
	 * 
	 * @return montoCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public String getMontoCDAOrdRecAplicadas() {
		return montoCDAOrdRecAplicadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoCDAOrdRecAplicadas
	 * 
	 * @param montoCDAOrdRecAplicadas
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDAOrdRecAplicadas(String montoCDAOrdRecAplicadas) {
		this.montoCDAOrdRecAplicadas = montoCDAOrdRecAplicadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoCDAPendEnviar
	 * 
	 * @param montoCDAPendEnviar
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDAPendEnviar(String montoCDAPendEnviar) {
		this.montoCDAPendEnviar = montoCDAPendEnviar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAPendEnviar
	 * 
	 * @return montoCDAPendEnviar Objeto del tipo @see String
	 */
	public String getMontoCDAPendEnviar() {
		return montoCDAPendEnviar;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAEnviadas
	 * 
	 * @return montoCDAEnviadas Objeto del tipo @see String
	 */
	public String getMontoCDAEnviadas() {
		return montoCDAEnviadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAConfirmadas
	 * 
	 * @return montoCDAConfirmadas Objeto del tipo @see String
	 */
	public String getMontoCDAConfirmadas() {
		return montoCDAConfirmadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoCDAContingencia
	 * 
	 * @param montoCDAContingencia
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDAContingencia(String montoCDAContingencia) {
		this.montoCDAContingencia = montoCDAContingencia;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoCDARechazadas
	 * 
	 * @param montoCDARechazadas
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDARechazadas(String montoCDARechazadas) {
		this.montoCDARechazadas = montoCDARechazadas;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAContingencia
	 * 
	 * @return montoCDAContingencia Objeto del tipo @see String
	 */
	public String getMontoCDAContingencia() {
		return montoCDAContingencia;
	}


	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDARechazadas
	 * 
	 * @return montoCDARechazadas Objeto del tipo @see String
	 */
	public String getMontoCDARechazadas() {
		return montoCDARechazadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAEnviadas
	 * 
	 * @param montoCDAEnviadas
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDAEnviadas(String montoCDAEnviadas) {
		this.montoCDAEnviadas = montoCDAEnviadas;
	}

	/**
	 * Metodo que modifica el valor de la propiedad montoCDAConfirmadas
	 * 
	 * @param montoCDAConfirmadas
	 *            Objeto del tipo @see String
	 */
	public void setMontoCDAConfirmadas(String montoCDAConfirmadas) {
		this.montoCDAConfirmadas = montoCDAConfirmadas;
	}
}
