/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsGenArchHistDetDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 12:22:37 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * para la funcionalidad de Generar Archivo CDA Historico Detalle
**/

public class BeanResConsGenArchHistDetDAO  implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad que almacena la lista de registros que seran marcados
   como eliminados*/
   private List<BeanGenArchHistDet> listBeanGenArchHistDet;
   /**Propiedad privada del tipo Integer que almacena el total de registros*/
   private Integer totalReg;
   /**Propiedad privada del tipo String que almacena el numero de registros
   en el archivo*/
   private String numRegArchivo;
   /**Propiedad privada del tipo String que almacena el numero de registros
   encontrados*/
   private String numRegEncontrados;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanGenArchHistDet
   * @return listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
   */
   public List<BeanGenArchHistDet> getListBeanGenArchHistDet(){
      return listBeanGenArchHistDet;
   }
   /**
   *Modifica el valor de la propiedad listBeanGenArchHistDet
   * @param listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
   */
   public void setListBeanGenArchHistDet(List<BeanGenArchHistDet> listBeanGenArchHistDet){
      this.listBeanGenArchHistDet=listBeanGenArchHistDet;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad totalReg
   * @return totalReg Objeto del tipo Integer
   */
   public Integer getTotalReg(){
      return totalReg;
   }
   /**
   *Modifica el valor de la propiedad totalReg
   * @param totalReg Objeto del tipo Integer
   */
   public void setTotalReg(Integer totalReg){
      this.totalReg=totalReg;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numRegArchivo
   * @return numRegArchivo Objeto del tipo String
   */
   public String getNumRegArchivo(){
      return numRegArchivo;
   }
   /**
   *Modifica el valor de la propiedad numRegArchivo
   * @param numRegArchivo Objeto del tipo String
   */
   public void setNumRegArchivo(String numRegArchivo){
      this.numRegArchivo=numRegArchivo;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad numRegEncontrados
   * @return numRegEncontrados Objeto del tipo String
   */
   public String getNumRegEncontrados(){
      return numRegEncontrados;
   }
   /**
   *Modifica el valor de la propiedad numRegEncontrados
   * @param numRegEncontrados Objeto del tipo String
   */
   public void setNumRegEncontrados(String numRegEncontrados){
      this.numRegEncontrados=numRegEncontrados;
   }



}
