package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class BeanResUsuarioPerfilesDAO extends BeanResBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5444955081010604702L;
	/**Atributo privado que almacena la lista de perfiles*/
	private List<BeanPerfil> listPerfiles = Collections.emptyList();
	
	/**
	 * Metodo get que obtiene la lista de perfiles
	 * @return listPerfiles Objeto del tipo List<BeanPerfil>
	 */
	public List<BeanPerfil> getListPerfiles() {
		return listPerfiles;
	}
	/**
	 * Metodo set que modifica la lista de perfiles
	 * @param listPerfiles Objeto del tipo List<BeanPerfil>
	 */
	public void setListPerfiles(List<BeanPerfil> listPerfiles) {
		this.listPerfiles = listPerfiles;
	}

}
