package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

public class ResBeanEjecTranDAO extends BeanResBase implements Serializable{


	/**Codigo de error de TransacciOn exitosa*/
	public static final String COD_EXITO = "OK000003";	
	/**Codigo de error para identificar que hay un error desconocido al tratar de ejecutar la consulta en la base de datos*/
	public static final String COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_BD  = "EB00010D";
	/**Codigo de error para identificar que no se encontarron datos*/
	public static final String COD_ERROR_DATOS_NO_ENCONTRADOS                   = "ED00010V";
	
	/**
	 * Propiedad del tipo String que almacena el valor de COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS
	 */
	public static final String COD_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS      = "EC00030D";
	/**
	 * Propiedad del tipo String que almacena el valor de DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS
	 */
	public static final String DESC_ERROR_AL_EJECUTAR_UNA_SENTENCIA_EN_CICS     = "No se pudo establecer comunicación con los servicios.";

	/**
	 * Propiedad del tipo String que almacena el valor de COD_ERROR_CONFIGURACION_NO_ENCONTRADA
	 */
	public static final String COD_ERROR_CONFIGURACION_NO_ENCONTRADA             = "ED00440V";
	/**
	 * Propiedad del tipo String que almacena el valor de DESC_ERROR_CONFIGURACION_NO_ENCONTRADA
	 */
	public static final String DESC_ERROR_CONFIGURACION_NO_ENCONTRADA            = "La configuración de la transacción no se encuentra registrada en la BD.";
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2191400636706944621L;
	/**Atributo privado del tipo String que almacena el codigo de error*/
	private String codError = "";
	/**Atributo privado del tipo String que almacena el mensaje de error*/
	private String msgError = "";
	/**Variable del tipo String que almacena el tipo de error jAlert, jInfo, jError*/
	private String tipoError ="";
	/**Variable del tipo String que almacena el store procedure que marco el error */
	private String storeProcedure ="";
	
	/**
	 * Propiedad del tipo String que almacena el valor de tramaRespuesta
	 */
	private String tramaRespuesta = "";
	
	/**
	 * Propiedad del tipo Map<String,Object> que almacena el valor de resultados
	 */
	private Map<String,Object> resultados = Collections.emptyMap();
	
	/**
	 * Regresa el valor del campo codError
	 * @return Regresa la variable codError del tipo String
	 */
	
	public String getCodError() {
		return this.codError;
	}
	/**
	 * Almacena el parametro codError en el campo codError
	 * @param codError del tipo String
	 */
	
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Regresa el valor del campo msgError
	 * @return Regresa la variable msgError del tipo String
	 */
	
	public String getMsgError() {
		return this.msgError;
	}
	/**
	 * Almacena el parametro msgError en el campo msgError
	 * @param msgError del tipo String
	 */
	
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * Regresa el valor del campo tipoError
	 * @return Regresa la variable tipoError del tipo String
	 */
	
	public String getTipoError() {
		return this.tipoError;
	}
	/**
	 * Almacena el parametro tipoError en el campo tipoError
	 * @param tipoError del tipo String
	 */
	
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * Regresa el valor del campo storeProcedure
	 * @return Regresa la variable storeProcedure del tipo String
	 */
	
	public String getStoreProcedure() {
		return this.storeProcedure;
	}
	/**
	 * Almacena el parametro storeProcedure en el campo storeProcedure
	 * @param storeProcedure del tipo String
	 */
	
	public void setStoreProcedure(String storeProcedure) {
		this.storeProcedure = storeProcedure;
	}
	/**
	 * Regresa el valor del campo resultados
	 * @return Regresa la variable resultados del tipo List<Map<String,Map<String,String>>>
	 */
	
	public Map<String, Object> getResultados() {
		return this.resultados;
	}
	/**
	 * Almacena el parametro resultados en el campo resultados
	 * @param resultados del tipo List<Map<String,Map<String,String>>>
	 */
	
	public void setResultados(Map<String, Object> resultados) {
		this.resultados = resultados;
	}
	/**
	 * @return el tramaRespuesta
	 */
	public String getTramaRespuesta() {
		return this.tramaRespuesta;
	}
	/**
	 * @param tramaRespuesta el tramaRespuesta a establecer
	 */
	public void setTramaRespuesta(String tramaRespuesta) {
		this.tramaRespuesta = tramaRespuesta;
	}
}
