/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsBitAdmonDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 10:39:58 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * para la bitacora administrativa
**/

public class BeanResConsBitAdmonDAO  implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**Propiedad que almacena la lista de registros de la bitacora administrativa*/
	private List<BeanConsBitAdmon> listBeanConsBitAdmon;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanConsBitAdmon
   * @return listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
   */
   public List<BeanConsBitAdmon> getListBeanConsBitAdmon(){
      return listBeanConsBitAdmon;
   }
   /**
   *Modifica el valor de la propiedad listBeanConsBitAdmon
   * @param listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
   */
   public void setListBeanConsBitAdmon(List<BeanConsBitAdmon> listBeanConsBitAdmon){
      this.listBeanConsBitAdmon=listBeanConsBitAdmon;
   }
   
   /**
    * Metodo get que obtiene el total de registros
    * @return Integer objeto con el numero total de registros
    */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}



}
