/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsMovCdaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   12/12/2013 16:39:56 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  Consulta de Movimientos CDA
**/
public class BeanResConsMovCdaDAO implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 5935853577548891029L;
	/**
	 * Propiedad del tipo List<BeanMovimientoCDA> que almacena una lista de
	 * objetos del tipo	 * 
	 * @see BeanMovimientoCDA
	 */
	private List<BeanMovimientoCDA> listBeanMovimientoCDA = Collections
			.emptyList();
	/** Propiedad del tipo Integer que almacena el toral de registros */
	private Integer totalReg = 0;
	/** Propiedad del tipo String que almacena nombre de archivo generado */
	private String nombreArchivo = "";
	/** Propiedad del tipo @see String que almacena el codigo de error */
	private String codError;
	/** Propiedad del tipo @see String que almacena el mensaje de error */
	private String msgError;

	
	/**
	 * Metodo que sirve para obtener la lista de movimientos CDA
	 * @return listBeanMovimientoCDA
	 */
	public List<BeanMovimientoCDA> getListBeanMovimientoCDA() {
		return listBeanMovimientoCDA;
	}

	/**
	 * Metodo para actualizar la lista de movimientos CDA
	 * @param listBeanMovimientoCDA el listBeanMovimientoCDA a establecer
	 */
	public void setListBeanMovimientoCDA(
			List<BeanMovimientoCDA> listBeanMovimientoCDA) {
		this.listBeanMovimientoCDA = listBeanMovimientoCDA;
	}

	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	   * Metodo get que sirve para obtener el valor de la propiedad codError
	   * @return codError Objeto del tipo String
	   */
	   public String getCodError(){
	      return codError;
	   }
	   /**
	   *Modifica el valor de la propiedad codError
	   * @param codError Objeto del tipo String
	   */
	   public void setCodError(String codError){
	      this.codError=codError;
	   }
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad msgError
	   * @return msgError Objeto del tipo String
	   */
	   public String getMsgError(){
	      return msgError;
	   }
	   /**
	   * Modifica el valor de la propiedad msgError
	   * @param msgError Objeto del tipo String
	   */
	   public void setMsgError(String msgError){
	      this.msgError=msgError;
	   }

	/**
	 * Metodo get que sirve para obtener el valor de la propiedad nombreArchivo
	 * @param nombreArchivo Objeto del tipo String 
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Modifica el valor de la propiedad nombreArchivo
	 * @return nombreArchivo Objeto del tipo String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
}
