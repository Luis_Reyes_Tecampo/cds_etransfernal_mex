package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;

/**
 * La clase BeanResConsTranSpeiRecExtDAO.
 */
public class BeanResConsTranSpeiRecExtDAO implements Serializable{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 602526456577457337L;
	
	/** Propiedad del tipo List<BeanTranSpeiRec> que almacena el valor de beanTranSpeiRecList. */
	private List<BeanTranSpeiRecOper> beanTranSpeiRecList = null;
		
	/** Propiedad del tipo BigDecimal que almacena el valor de montoTopoVLiquidadas. */
	private BigDecimal montoTopoVLiquidadas = BigDecimal.ZERO;
	
	/** Propiedad del tipo BigInteger que almacena el valor de volumenTopoVLiquidadas. */
	private Integer volumenTopoVLiquidadas = Constantes.INT_CERO;
	
	/** Propiedad del tipo BigDecimal que almacena el valor de montoTopoVLiquidadas. */
	private BigDecimal montoTopoTLiquidadas = BigDecimal.ZERO;
	
	/** Propiedad del tipo BigInteger que almacena el valor de volumenTopoVLiquidadas. */
	private Integer volumenTopoTLiquidadas = Constantes.INT_CERO;
	
	/** Propiedad del tipo BigDecimal que almacena el valor de montoTopoVLiquidadas. */
	private BigDecimal montoTopoTXLiquidar = BigDecimal.ZERO;
	
	/** Propiedad del tipo Integer que almacena el valor de volumenTopoVLiquidadas. */
	private Integer volumenTopoTXLiquidar = Constantes.INT_CERO;
	
	/** Propiedad del tipo BigDecimal que almacena el valor de montoTopoVLiquidadas. */
	private BigDecimal montoRechazos = BigDecimal.ZERO;
	
	/** Propiedad del tipo BigInteger que almacena el valor de volumenTopoVLiquidadas. */
	private Integer volumenRechazos = Constantes.INT_CERO;
	
	/**
	 * |
	 * Metodo get que obtiene el valor de la propiedad beanTranSpeiRecList.
	 *
	 * @return beanTranSpeiRecList Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public List<BeanTranSpeiRecOper> getBeanTranSpeiRecList() {
		return beanTranSpeiRecList;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad beanTranSpeiRecList.
	 *
	 * @param beanTranSpeiRecList Objeto de tipo @see List<BeanTranSpeiRec>
	 */
	public void setBeanTranSpeiRecList(List<BeanTranSpeiRecOper> beanTranSpeiRecList) {
		this.beanTranSpeiRecList = beanTranSpeiRecList;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad montoTopoVLiquidadas.
	 *
	 * @return montoTopoVLiquidadas Objeto de tipo @see BigDecimal
	 */
	public BigDecimal getMontoTopoVLiquidadas() {
		return montoTopoVLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad montoTopoVLiquidadas.
	 *
	 * @param montoTopoVLiquidadas Objeto de tipo @see BigDecimal
	 */
	public void setMontoTopoVLiquidadas(BigDecimal montoTopoVLiquidadas) {
		this.montoTopoVLiquidadas = montoTopoVLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenTopoVLiquidadas.
	 *
	 * @return volumenTopoVLiquidadas Objeto de tipo @see Integer
	 */
	public Integer getVolumenTopoVLiquidadas() {
		return volumenTopoVLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad volumenTopoVLiquidadas.
	 *
	 * @param volumenTopoVLiquidadas Objeto de tipo @see Integer
	 */
	public void setVolumenTopoVLiquidadas(Integer volumenTopoVLiquidadas) {
		this.volumenTopoVLiquidadas = volumenTopoVLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad montoTopoTLiquidadas.
	 *
	 * @return montoTopoTLiquidadas Objeto de tipo @see BigDecimal
	 */
	public BigDecimal getMontoTopoTLiquidadas() {
		return montoTopoTLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad montoTopoTLiquidadas.
	 *
	 * @param montoTopoTLiquidadas Objeto de tipo @see BigDecimal
	 */
	public void setMontoTopoTLiquidadas(BigDecimal montoTopoTLiquidadas) {
		this.montoTopoTLiquidadas = montoTopoTLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenTopoTLiquidadas.
	 *
	 * @return volumenTopoTLiquidadas Objeto de tipo @see Integer
	 */
	public Integer getVolumenTopoTLiquidadas() {
		return volumenTopoTLiquidadas;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad volumenTopoTLiquidadas.
	 *
	 * @param volumenTopoTLiquidadas Objeto de tipo @see Integer
	 */
	public void setVolumenTopoTLiquidadas(Integer volumenTopoTLiquidadas) {
		this.volumenTopoTLiquidadas = volumenTopoTLiquidadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad montoTopoTXLiquidar.
	 *
	 * @return montoTopoTXLiquidar Objeto de tipo @see BigDecimal
	 */
	public BigDecimal getMontoTopoTXLiquidar() {
		return montoTopoTXLiquidar;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad montoTopoTXLiquidar.
	 *
	 * @param montoTopoTXLiquidar Objeto de tipo @see BigDecimal
	 */
	public void setMontoTopoTXLiquidar(BigDecimal montoTopoTXLiquidar) {
		this.montoTopoTXLiquidar = montoTopoTXLiquidar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenTopoTXLiquidar.
	 *
	 * @return volumenTopoTXLiquidar Objeto de tipo @see Integer
	 */
	public Integer getVolumenTopoTXLiquidar() {
		return volumenTopoTXLiquidar;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad volumenTopoTXLiquidar.
	 *
	 * @param volumenTopoTXLiquidar Objeto de tipo @see Integer
	 */
	public void setVolumenTopoTXLiquidar(Integer volumenTopoTXLiquidar) {
		this.volumenTopoTXLiquidar = volumenTopoTXLiquidar;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad montoRechazos.
	 *
	 * @return montoRechazos Objeto de tipo @see BigDecimal
	 */
	public BigDecimal getMontoRechazos() {
		return montoRechazos;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad montoRechazos.
	 *
	 * @param montoRechazos Objeto de tipo @see BigDecimal
	 */
	public void setMontoRechazos(BigDecimal montoRechazos) {
		this.montoRechazos = montoRechazos;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenRechazos.
	 *
	 * @return volumenRechazos Objeto de tipo @see Integer
	 */
	public Integer getVolumenRechazos() {
		return volumenRechazos;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad volumenRechazos.
	 *
	 * @param volumenRechazos Objeto de tipo @see Integer
	 */
	public void setVolumenRechazos(Integer volumenRechazos) {
		this.volumenRechazos = volumenRechazos;
	}
}
