package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResConsTranSpeiRecDAO extends BeanResConsTranSpeiRecExtDAO implements BeanResultBO, Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 602526456577457337L;
		
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    /*** Propiedad del tipo BigDecimal que almacena la suma de Topologia V*/
	private BigDecimal sumaTopoV = BigDecimal.ZERO;
	  /*** Propiedad del tipo BigDecimal que almacena la suma de Volumen de Topologia V*/
	private Integer sumaVolV = 0;
	
	/*** Propiedad del tipo BigDecimal que almacena la suma de Topologia T*/
	private BigDecimal sumaTopoT = BigDecimal.ZERO;
	  /*** Propiedad del tipo BigDecimal que almacena la suma de*/
	private Integer sumaVolT = 0;
	
	/*** Propiedad del tipo BigDecimal que almacena la suma de  Topologia T por liberar*/
	private BigDecimal sumaTopoTxL = BigDecimal.ZERO;
	  /*** Propiedad del tipo BigDecimal que almacena la suma de Topologia T por liberar*/
	private Integer sumaVolTxL = 0;
	
	/*** Propiedad del tipo BigDecimal que almacena la suma de Topologoa rechazados*/
	private BigDecimal sumaTopoRech = BigDecimal.ZERO;
	  /*** Propiedad del tipo BigDecimal que almacena la suma de volumen de topologia rechazados*/
	private Integer sumaVolRech = 0;

	/**
	 * Metodo get que obtiene el valor de la propiedad totalReg
	 * @return totalReg Objeto de tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalReg
	 * @param totalReg Objeto de tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad codError
	 * @return codError Objeto de tipo @see String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad codError
	 * @param codError Objeto de tipo @see String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad msgError
	 * @return msgError Objeto de tipo @see String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * Metodo que modifica el valor de la propiedad msgError
	 * @param msgError Objeto de tipo @see String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	

	/**
	 * @return el sumaTopoV
	 */
	public BigDecimal getSumaTopoV() {
		return sumaTopoV;
	}
	/**
	 * @param sumaTopoV el sumaTopoV a establecer
	 */
	public void setSumaTopoV(BigDecimal sumaTopoV) {
		this.sumaTopoV = sumaTopoV;
	}
	/**
	 * @return el sumaVolV
	 */
	public Integer getSumaVolV() {
		return sumaVolV;
	}
	/**
	 * @param sumaVolV el sumaVolV a establecer
	 */
	public void setSumaVolV(Integer sumaVolV) {
		this.sumaVolV = sumaVolV;
	}
	/**
	 * @return el sumaTopoT
	 */
	public BigDecimal getSumaTopoT() {
		return sumaTopoT;
	}
	/**
	 * @param sumaTopoT el sumaTopoT a establecer
	 */
	public void setSumaTopoT(BigDecimal sumaTopoT) {
		this.sumaTopoT = sumaTopoT;
	}
	/**
	 * @return el sumaVolT
	 */
	public Integer getSumaVolT() {
		return sumaVolT;
	}
	/**
	 * @param sumaVolT el sumaVolT a establecer
	 */
	public void setSumaVolT(Integer sumaVolT) {
		this.sumaVolT = sumaVolT;
	}
	/**
	 * @return el sumaTopoTxL
	 */
	public BigDecimal getSumaTopoTxL() {
		return sumaTopoTxL;
	}
	/**
	 * @param sumaTopoTxL el sumaTopoTxL a establecer
	 */
	public void setSumaTopoTxL(BigDecimal sumaTopoTxL) {
		this.sumaTopoTxL = sumaTopoTxL;
	}
	/**
	 * @return el sumaVolTxL
	 */
	public Integer getSumaVolTxL() {
		return sumaVolTxL;
	}
	/**
	 * @param sumaVolTxL el sumaVolTxL a establecer
	 */
	public void setSumaVolTxL(Integer sumaVolTxL) {
		this.sumaVolTxL = sumaVolTxL;
	}
	/**
	 * @return el sumaTopoRech
	 */
	public BigDecimal getSumaTopoRech() {
		return sumaTopoRech;
	}
	/**
	 * @param sumaTopoRech el sumaTopoRech a establecer
	 */
	public void setSumaTopoRech(BigDecimal sumaTopoRech) {
		this.sumaTopoRech = sumaTopoRech;
	}
	/**
	 * @return el sumaVolRech
	 */
	public Integer getSumaVolRech() {
		return sumaVolRech;
	}
	/**
	 * @param sumaVolRech el sumaVolRech a establecer
	 */
	public void setSumaVolRech(Integer sumaVolRech) {
		this.sumaVolRech = sumaVolRech;
	}
	
    
	

}
