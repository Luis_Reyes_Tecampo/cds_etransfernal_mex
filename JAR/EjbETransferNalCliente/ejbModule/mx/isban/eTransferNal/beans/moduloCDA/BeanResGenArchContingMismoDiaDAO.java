/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGenArchContingMismoDiaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 11:15:02 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que encapsula la respuesta del insert a la base
 * de datos
**/

public class BeanResGenArchContingMismoDiaDAO  implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
   /**Propiedad del tipo @see String que almacena el numero de
   operaciones generadas*/
   private String numOpGeneradas;
   /**Propiedad del tipo @see String que almacena el numero de operaciones
   por generar*/
   private String numOpGenerar;
   /**Propiedad privada del tipo String que almacena el id de la peticion*/
   private String idPeticion;
   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad numOpGeneradas
    * @return numOpGeneradas Objeto del tipo String
    */
    public String getNumOpGeneradas(){
       return numOpGeneradas;
    }
    /**
    *Modifica el valor de la propiedad numOpGeneradas
    * @param numOpGeneradas Objeto del tipo String
    */
    public void setNumOpGeneradas(String numOpGeneradas){
       this.numOpGeneradas=numOpGeneradas;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad numOpGenerar
    * @return numOpGenerar Objeto del tipo String
    */
    public String getNumOpGenerar(){
       return numOpGenerar;
    }
    /**
    *Modifica el valor de la propiedad numOpGenerar
    * @param numOpGenerar Objeto del tipo String
    */
    public void setNumOpGenerar(String numOpGenerar){
       this.numOpGenerar=numOpGenerar;
    }
    
    /**
     *Metodo get que sirve para obtener el valor de la propiedad idPeticion
     * @return idPeticion Objeto del tipo String
     */
 	public String getIdPeticion() {
 		return idPeticion;
 	}
    /**
     *Metodo set que sirve para modificar el valor de la propiedad idPeticion
     * @param idPeticion Objeto del tipo String
     */
 	public void setIdPeticion(String idPeticion) {
 		this.idPeticion = idPeticion;
 	}




}
