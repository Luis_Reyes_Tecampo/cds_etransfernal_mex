/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResEstadoConexionDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 10:06:58 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase del tipo Bean DTO que se encarga de encapsular los valores
 * de regreso de  la consulta de estado de conexion
**/

public class BeanResEstadoConexionDAO  implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad privada del tipo Integer que almacena los bytes recibidos
   si el valor es -1 significa que se desconecto*/
   private Integer bytesRecibidos;
   
   /**Propiedad privada del tipo Integer que almacena -1 si esta en contingencia y 1 
   si esta normal*/
   private Integer contingencia;
   
   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad bytesRecibidos
   * @return bytesRecibidos Objeto del tipo Integer
   */
   public Integer getBytesRecibidos(){
      return bytesRecibidos;
   }
   /**
   *Modifica el valor de la propiedad bytesRecibidos
   * @param bytesRecibidos Objeto del tipo Integer
   */
   public void setBytesRecibidos(Integer bytesRecibidos){
      this.bytesRecibidos=bytesRecibidos;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad contingencia
    * @return contingencia Objeto del tipo Integer
    */
   public Integer getContingencia() {
	return contingencia;
   }
   /**
    *Modifica el valor de la propiedad contingencia
    * @param contingencia Objeto del tipo Integer
    */
   public void setContingencia(Integer contingencia) {
		this.contingencia = contingencia;
   }
  /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }



}
