/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResContingMismoDiaDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:30:47 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  de Contingencia CDA Mismo Dia
**/

public class BeanResContingMismoDiaDAO implements BeanResultBO, Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo List<BeanContingMismoDia> que almacena una lista de objetos del tipo
	 * @see BeanContingMismoDia*/
	private List<BeanContingMismoDia> listBeanContingMismoDia = Collections.emptyList();
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/**Propiedad del tipo Integer que almacena las operaciones pendientes*/
	private Integer operacionesPendientes = 0;
	
   /**Propiedad del tipo @see String que almacena el codigo de error*/
   private String codError;
   /**Propiedad del tipo @see String que almacena el mensaje de error*/
   private String msgError;
   
   
   /**
    * Metodo get que sirve para obtener la lista de objetos BeanContingMismoDia
    * @return List<BeanContingMismoDia> lista de objetos del tipo BeanContingMismoDia
    */
   public List<BeanContingMismoDia> getListBeanContingMismoDia() {
	return listBeanContingMismoDia;
   }
   
   /**
    * Metodo set que sirve para setear la lista de objetos BeanContingMismoDia
    * @param listBeanContingMismoDia lista de objetos del tipo BeanContingMismoDia
    */
   public void setListBeanContingMismoDia(
		List<BeanContingMismoDia> listBeanContingMismoDia) {
	this.listBeanContingMismoDia = listBeanContingMismoDia;
   }

   /**
    * Metodo get que obtiene el total de registros
    * @return Integer objeto con el numero total de registros
    */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

/**
    *Metodo get que sirve para obtener el valor de la propiedad codError
    * @return codError Objeto del tipo String
    */
    public String getCodError(){
       return codError;
    }
    /**
    *Modifica el valor de la propiedad codError
    * @param codError Objeto del tipo String
    */
    public void setCodError(String codError){
       this.codError=codError;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad msgError
    * @return msgError Objeto del tipo String
    */
    public String getMsgError(){
       return msgError;
    }
    /**
    *Modifica el valor de la propiedad msgError
    * @param msgError Objeto del tipo String
    */
    public void setMsgError(String msgError){
       this.msgError=msgError;
    }
    /**
     *Metodo get que sirve para obtener el valor de la propiedad operacionesPendientes
     * @return operacionesPendientes Objeto del tipo Integer
     */
	public Integer getOperacionesPendientes() {
		return operacionesPendientes;
	}
    /**
    *Metodo set que sirve para obtener el valor de la propiedad operacionesPendientes
    * @param operacionesPendientes Objeto del tipo Integer
    */
	public void setOperacionesPendientes(Integer operacionesPendientes) {
		this.operacionesPendientes = operacionesPendientes;
	}
    

}
