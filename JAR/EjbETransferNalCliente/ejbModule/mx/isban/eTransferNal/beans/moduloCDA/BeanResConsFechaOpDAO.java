/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsFechaOpDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 18:24:31 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que encapsula la fecha de operacion y la respuesta
 * en la base de datos
**/

public class BeanResConsFechaOpDAO implements BeanResultBO, Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad privada del tipo String que almacena la fecha de operacion*/
   private String fechaOperacion;
   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad fechaOperacion
   * @return fechaOperacion Objeto del tipo String
   */
   public String getFechaOperacion(){
      return fechaOperacion;
   }
   /**
   *Modifica el valor de la propiedad fechaOperacion
   * @param fechaOperacion Objeto del tipo String
   */
   public void setFechaOperacion(String fechaOperacion){
      this.fechaOperacion=fechaOperacion;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }



}
