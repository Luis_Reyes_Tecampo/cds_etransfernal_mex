package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

public class BeanResConsMiInstDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 6775898423330352235L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de miInstitucion
	 */
	private String miInstitucion;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad miInstitucion
	 * @return miInstitucion Objeto de tipo @see String
	 */
	public String getMiInstitucion() {
		return miInstitucion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad miInstitucion
	 * @param miInstitucion Objeto de tipo @see String
	 */
	public void setMiInstitucion(String miInstitucion) {
		this.miInstitucion = miInstitucion;
	}
	
}
