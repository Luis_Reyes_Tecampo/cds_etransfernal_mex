/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResMonCargaArchHistDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:53:14 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * para la funcionalidad del Monitor de Carga de Archivos
**/

public class BeanResMonCargaArchHistDAO  implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad que almacena la lista de registros del monitor de carga
   de archivos historicos*/
   private List<BeanMonCargaArchHist> listBeanMonCargaArchHist;
   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;
   /**Propiedad privada del tipo Integer que almacena el total de registros*/
   private Integer totalReg;

     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanMonCargaArchHist
   * @return listBeanMonCargaArchHist Objeto del tipo List<BeanMonCargaArchHist>
   */
   public List<BeanMonCargaArchHist> getListBeanMonCargaArchHist(){
      return listBeanMonCargaArchHist;
   }
   /**
   *Modifica el valor de la propiedad listBeanMonCargaArchHist
   * @param listBeanMonCargaArchHist Objeto del tipo List<BeanMonCargaArchHist>
   */
   public void setListBeanMonCargaArchHist(List<BeanMonCargaArchHist> listBeanMonCargaArchHist){
      this.listBeanMonCargaArchHist=listBeanMonCargaArchHist;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad totalReg
   * @return totalReg Objeto del tipo Integer
   */
   public Integer getTotalReg(){
      return totalReg;
   }
   /**
   *Modifica el valor de la propiedad totalReg
   * @param totalReg Objeto del tipo Integer
   */
   public void setTotalReg(Integer totalReg){
      this.totalReg=totalReg;
   }



}
