/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanProgTraspasosDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

/**
 *Clase Bean DTO que se encarga del catalogo
 *de horarios para el envio de pagos SPEI
**/
public class BeanProgTraspasosDAO extends BeanResBase implements Serializable{

	/** Constante privada del Serial version */
	private static final long serialVersionUID = 5700019141288117342L;
	/** Propiedad del tipo String para almacenar el importe*/
	private String importeProgTran;
	/** Propiedad del tipo  lista */
	private List<BeanHorarioTransferencia> listHorariosTr;
	/**Propiedad que almacena la abstraccion de la paginacion*/
	private BeanPaginador paginador;
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	/**
	 * @return el importeProgTran
	 */
	public String getImporteProgTran() {
		return importeProgTran;
	}
	/**
	 * @param importeProgTran el importeProgTran a establecer
	 */
	public void setImporteProgTran(String importeProgTran) {
		this.importeProgTran = importeProgTran;
	}
	/**
	 * @return el listHorariosTr
	 */
	public List<BeanHorarioTransferencia> getListHorariosTr() {
		return listHorariosTr;
	}
	/**
	 * @param listHorariosTr el listHorariosTr a establecer
	 */
	public void setListHorariosTr(List<BeanHorarioTransferencia> listHorariosTr) {
		this.listHorariosTr = listHorariosTr;
	}
	/**
	 * @return el paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}
	/**
	 * @param paginador el paginador a establecer
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}
	/**
	 * @return el totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
}
