/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanAdministraSaldoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * Clase Bean DTO que se encarga de la administracion de saldos.
 */
public class BeanAdministraSaldoDAO extends BeanResBase implements Serializable {

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = -2712276333777033085L;
	
	/**  Propiedad del tipo String que almacena el saldo principal. */
	private String saldoPrincipal;
	
	/** La variable que contiene informacion con respecto a: saldo principal count. */
	private String saldoPrincipalCount;
	
	/**  Propiedad del tipo String que almacena el saldo alterno. */
	private String saldoAlterno;
	
	/** La variable que contiene informacion con respecto a: saldo alterno count. */
	private String saldoAlternoCount;
	
	/**  Propiedad del tipo String que almacena el saldo calculado. */
	private String saldoCalculado;
	
	/** La variable que contiene informacion con respecto a: saldo calculado count. */
	private String saldoCalculadoCount;
	
	/**  Propiedad del tipo String que almacena el saldo no reservado. */
	private String saldoNoReservado;
	
	/** La variable que contiene informacion con respecto a: saldo no reservado count. */
	private String saldoNoReservadoCount;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones. */
	private String saldoDevoluciones;
	
	/** La variable que contiene informacion con respecto a: saldo devoluciones count. */
	private String saldoDevolucionesCount;
	
	/**
	 * Obtener el objeto: saldo principal.
	 *
	 * @return El objeto: saldo principal
	 */
	public String getSaldoPrincipal() {
		return saldoPrincipal;
	}
	
	/**
	 * Definir el objeto: saldo principal.
	 *
	 * @param saldoPrincipal El nuevo objeto: saldo principal
	 */
	public void setSaldoPrincipal(String saldoPrincipal) {
		this.saldoPrincipal = saldoPrincipal;
	}
	
	/**
	 * Obtener el objeto: saldo principal count.
	 *
	 * @return El objeto: saldo principal count
	 */
	public String getSaldoPrincipalCount() {
		return saldoPrincipalCount;
	}
	
	/**
	 * Definir el objeto: saldo principal count.
	 *
	 * @param saldoPrincipalCount El nuevo objeto: saldo principal count
	 */
	public void setSaldoPrincipalCount(String saldoPrincipalCount) {
		this.saldoPrincipalCount = saldoPrincipalCount;
	}
	
	/**
	 * Obtener el objeto: saldo alterno.
	 *
	 * @return El objeto: saldo alterno
	 */
	public String getSaldoAlterno() {
		return saldoAlterno;
	}
	
	/**
	 * Definir el objeto: saldo alterno.
	 *
	 * @param saldoAlterno El nuevo objeto: saldo alterno
	 */
	public void setSaldoAlterno(String saldoAlterno) {
		this.saldoAlterno = saldoAlterno;
	}
	
	/**
	 * Obtener el objeto: saldo alterno count.
	 *
	 * @return El objeto: saldo alterno count
	 */
	public String getSaldoAlternoCount() {
		return saldoAlternoCount;
	}
	
	/**
	 * Definir el objeto: saldo alterno count.
	 *
	 * @param saldoAlternoCount El nuevo objeto: saldo alterno count
	 */
	public void setSaldoAlternoCount(String saldoAlternoCount) {
		this.saldoAlternoCount = saldoAlternoCount;
	}
	
	/**
	 * Obtener el objeto: saldo calculado.
	 *
	 * @return El objeto: saldo calculado
	 */
	public String getSaldoCalculado() {
		return saldoCalculado;
	}
	
	/**
	 * Definir el objeto: saldo calculado.
	 *
	 * @param saldoCalculado El nuevo objeto: saldo calculado
	 */
	public void setSaldoCalculado(String saldoCalculado) {
		this.saldoCalculado = saldoCalculado;
	}
	
	/**
	 * Obtener el objeto: saldo calculado count.
	 *
	 * @return El objeto: saldo calculado count
	 */
	public String getSaldoCalculadoCount() {
		return saldoCalculadoCount;
	}
	
	/**
	 * Definir el objeto: saldo calculado count.
	 *
	 * @param saldoCalculadoCount El nuevo objeto: saldo calculado count
	 */
	public void setSaldoCalculadoCount(String saldoCalculadoCount) {
		this.saldoCalculadoCount = saldoCalculadoCount;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado.
	 *
	 * @return El objeto: saldo no reservado
	 */
	public String getSaldoNoReservado() {
		return saldoNoReservado;
	}
	
	/**
	 * Definir el objeto: saldo no reservado.
	 *
	 * @param saldoNoReservado El nuevo objeto: saldo no reservado
	 */
	public void setSaldoNoReservado(String saldoNoReservado) {
		this.saldoNoReservado = saldoNoReservado;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado count.
	 *
	 * @return El objeto: saldo no reservado count
	 */
	public String getSaldoNoReservadoCount() {
		return saldoNoReservadoCount;
	}
	
	/**
	 * Definir el objeto: saldo no reservado count.
	 *
	 * @param saldoNoReservadoCount El nuevo objeto: saldo no reservado count
	 */
	public void setSaldoNoReservadoCount(String saldoNoReservadoCount) {
		this.saldoNoReservadoCount = saldoNoReservadoCount;
	}
	
	/**
	 * Obtener el objeto: saldo devoluciones.
	 *
	 * @return El objeto: saldo devoluciones
	 */
	public String getSaldoDevoluciones() {
		return saldoDevoluciones;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones.
	 *
	 * @param saldoDevoluciones El nuevo objeto: saldo devoluciones
	 */
	public void setSaldoDevoluciones(String saldoDevoluciones) {
		this.saldoDevoluciones = saldoDevoluciones;
	}
	
	/**
	 * Obtener el objeto: saldo devoluciones count.
	 *
	 * @return El objeto: saldo devoluciones count
	 */
	public String getSaldoDevolucionesCount() {
		return saldoDevolucionesCount;
	}
	
	/**
	 * Definir el objeto: saldo devoluciones count.
	 *
	 * @param saldoDevolucionesCount El nuevo objeto: saldo devoluciones count
	 */
	public void setSaldoDevolucionesCount(String saldoDevolucionesCount) {
		this.saldoDevolucionesCount = saldoDevolucionesCount;
	}
	
	

}
