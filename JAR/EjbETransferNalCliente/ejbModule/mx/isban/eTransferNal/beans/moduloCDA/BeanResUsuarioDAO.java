/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * UsuarioDTO.java
 *
 * Control de versiones:
 *
 * Version Date/Hour           By       Company Description
 * ------- ------------------- -------- ------- --------------
 * 1.0     22/06/2011 11:06:44 O Acosta ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * Objeto de transferencia de datos para los <i>Usuarios</i> con
 * acceso a <b>Transfer</b>.
 */
public class BeanResUsuarioDAO extends BeanResBase implements Serializable{

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = 443313972795253308L;
    /**Propiedad privada que almacena el usuario*/
    private BeanUsuario usuario;
	/**
	 * Metodo get que obtiene un objeto del tipo BeanUsuario
	 * @return usuario Objeto del tipo BeanUsuario
	 */
    public BeanUsuario getUsuario() {
		return usuario;
	}
	/**
	 * Metodo set que modifica un objeto del tipo BeanUsuario
	 * @param usuario Objeto del tipo BeanUsuario
	 */    
	public void setUsuario(BeanUsuario usuario) {
		this.usuario = usuario;
	}
    
 

}
