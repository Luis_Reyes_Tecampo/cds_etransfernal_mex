package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResConsPerfilDAO implements BeanResultBO,Serializable {
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo List<BeanContingMismoDia> que almacena una lista de objetos del tipo
	 * @see BeanContingMismoDia*/
	private List<BeanServiciosTareas> listBeanServiciosTareas = Collections.emptyList();
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
   /**Propiedad del tipo @see String que almacena el codigo de error*/
   private String codError;
   /**Propiedad del tipo @see String que almacena el mensaje de error*/
   private String msgError;
   
   
   /**
    * Metodo get que sirve para obtener la lista de objetos listBeanServiciosTareas
    * @return List<BeanServiciosTareas> lista de objetos del tipo listBeanServiciosTareas
    */
   public List<BeanServiciosTareas> getListBeanServiciosTareas() {
	return listBeanServiciosTareas;
   }
   
   /**
    * Metodo set que sirve para setear la lista de objetos listBeanServiciosTareas
    * @param listBeanServiciosTareas lista de objetos del tipo listBeanServiciosTareas
    */
   public void setListBeanServiciosTareas(
		List<BeanServiciosTareas> listBeanServiciosTareas) {
	this.listBeanServiciosTareas = listBeanServiciosTareas;
   }

   /**
    * Metodo get que obtiene el total de registros
    * @return Integer objeto con el numero total de registros
    */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

/**
    *Metodo get que sirve para obtener el valor de la propiedad codError
    * @return codError Objeto del tipo String
    */
    public String getCodError(){
       return codError;
    }
    /**
    *Modifica el valor de la propiedad codError
    * @param codError Objeto del tipo String
    */
    public void setCodError(String codError){
       this.codError=codError;
    }
    /**
    *Metodo get que sirve para obtener el valor de la propiedad msgError
    * @return msgError Objeto del tipo String
    */
    public String getMsgError(){
       return msgError;
    }
    /**
    *Modifica el valor de la propiedad msgError
    * @param msgError Objeto del tipo String
    */
    public void setMsgError(String msgError){
       this.msgError=msgError;
    }

}

