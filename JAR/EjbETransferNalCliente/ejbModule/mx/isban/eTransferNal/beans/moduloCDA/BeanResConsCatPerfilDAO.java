package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;


/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * para la consulta del catalogo del perfil
 **/
public class BeanResConsCatPerfilDAO extends BeanResBase implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7161286554485958961L;
	
	/**Lista de registros del catalogo de perfiles*/
	private List<BeanPerfil> perfilList;

	/**
	 * Metodo get que obtiene un objeto del tipo List<BeanPerfil>
	 * @return perfilList Objeto del tipo List<BeanPerfil>
	 */
	public List<BeanPerfil> getPerfilList() {
		return perfilList;
	}
	/**
	 * Metodo set que modifcia un objeto del tipo List<BeanPerfil>
	 * @param perfilList Objeto del tipo List<BeanPerfil>
	 */
	public void setPerfilList(List<BeanPerfil> perfilList) {
		this.perfilList = perfilList;
	}
	
	
}
