/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * Jessica.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/12/2013 23:53:02 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de la funcionalidad  Consulta del Monitor CDA
**/
public class BeanResMonitorCdaDAO extends BeanResBase implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8088199161830225722L;
	
	/**Propiedad del tipo  que almacena el numero de CDA's recibidas aplicadas */
	private String montoCDAOrdRecAplicadas;
	/**Propiedad del tipo  que almacena el numero de CDA's Pendientes por enviar */
	private String montoCDAPendEnviar;
	/**Propiedad del tipo  que almacena el numero de CDA's enviadas */
	private String montoCDAEnviadas;
	/**Propiedad del tipo  que almacena el numero de CDA's confirmadas */
	private String montoCDAConfirmadas;
	/**Propiedad del tipo  que almacena el numero de CDA's realizadas en contingencia */
	private String montoCDAContingencia;
	/**Propiedad del tipo  que almacena el numero de CDA's rechazadas */
	private String montoCDARechazadas;
	/**Propiedad del tipo  que almacena el volumen de CDA's recibidas aplicadas */
	private String volumenCDAOrdRecAplicadas;
	/**Propiedad del tipo  que almacena el volumen de CDA's Pendientes por enviar */
	private String volumenCDAPendEnviar;
	/**Propiedad del tipo  que almacena el volumen de CDA's enviadas */
	private String volumenCDAEnviadas;
	/**Propiedad del tipo  que almacena el volumen de CDA's confirmadas */
	private String volumenCDAConfirmadas;
	/**Propiedad del tipo  que almacena el volumen de CDA's realizadas en contingencia*/
	private String volumenCDAContingencia;
	/**Propiedad del tipo  que almacena el volumen de CDA's rechazadas */
	private String volumenCDARechazadas;
	/**Propiedad del tipo  que almacena si existe conexion con Banco de Mexico */
	private boolean existeConexion;
	/**Propiedad del tipo  que almacena si existe contingencia*/
	private boolean existeContingencia;
	
	/** La variable que contiene informacion con respecto a: monto CDA conf contingencia. */
	private String montoCDAConfContingencia;	
	/** La variable que contiene informacion con respecto a: volumen CDA conf contingencia. */
	private String volumenCDAConfContingencia;
	/** La variable que contiene informacion con respecto a: monto CDA env contingencia. */
	private String montoCDAEnvContingencia;	
	/** La variable que contiene informacion con respecto a: volumen CDA env contingencia. */
	private String volumenCDAEnvContingencia;
	/** La variable que contiene informacion con respecto a: monto CDA procesadas. */
	private String montoCDAProcesadas;	
	/** La variable que contiene informacion con respecto a: volumen CDA procesadas. */
	private String volumenCDAProcesadas;

    /**
     * Metodo get que obtiene el valor de la propiedad montoCDAOrdRecAplicadas
	 * @return montoCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public String getMontoCDAOrdRecAplicadas() {
		return montoCDAOrdRecAplicadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAOrdRecAplicadas
	 * @param montoCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public void setMontoCDAOrdRecAplicadas(String montoCDAOrdRecAplicadas) {
		this.montoCDAOrdRecAplicadas = montoCDAOrdRecAplicadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAPendEnviar
	 * @return montoCDAPendEnviar Objeto del tipo @see String
	 */
	public String getMontoCDAPendEnviar() {
		return montoCDAPendEnviar;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAPendEnviar
	 * @param montoCDAPendEnviar Objeto del tipo @see String
	 */
	public void setMontoCDAPendEnviar(String montoCDAPendEnviar) {
		this.montoCDAPendEnviar = montoCDAPendEnviar;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAEnviadas
	 * @return montoCDAEnviadas Objeto del tipo @see String
	 */
	public String getMontoCDAEnviadas() {
		return montoCDAEnviadas; 
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAEnviadas
	 * @param montoCDAEnviadas Objeto del tipo @see String
	 */
	public void setMontoCDAEnviadas(String montoCDAEnviadas) {
		this.montoCDAEnviadas = montoCDAEnviadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAConfirmadas
	 * @return montoCDAConfirmadas Objeto del tipo @see String
	 */
	public String getMontoCDAConfirmadas() {
		return montoCDAConfirmadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAConfirmadas
	 * @param montoCDAConfirmadas Objeto del tipo @see String
	 */
	public void setMontoCDAConfirmadas(String montoCDAConfirmadas) {
		this.montoCDAConfirmadas = montoCDAConfirmadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDAContingencia
	 * @return montoCDAContingencia Objeto del tipo @see String
	 */
	public String getMontoCDAContingencia() {
		return montoCDAContingencia;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDAContingencia
	 * @param montoCDAContingencia Objeto del tipo @see String
	 */
	public void setMontoCDAContingencia(String montoCDAContingencia) {
		this.montoCDAContingencia = montoCDAContingencia;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad montoCDARechazadas
	 * @return montoCDARechazadas Objeto del tipo @see String
	 */
	public String getMontoCDARechazadas() {
		return montoCDARechazadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad montoCDARechazadas
	 * @param montoCDARechazadas Objeto del tipo @see String
	 */
	public void setMontoCDARechazadas(String montoCDARechazadas) {
		this.montoCDARechazadas = montoCDARechazadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAOrdRecAplicadas
	 * @return volumenCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public String getVolumenCDAOrdRecAplicadas() {
		return volumenCDAOrdRecAplicadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAOrdRecAplicadas
	 * @param volumenCDAOrdRecAplicadas Objeto del tipo @see String
	 */
	public void setVolumenCDAOrdRecAplicadas(String volumenCDAOrdRecAplicadas) {
		this.volumenCDAOrdRecAplicadas = volumenCDAOrdRecAplicadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAPendEnviar
	 * @return volumenCDAPendEnviar Objeto del tipo @see String
	 */
	public String getVolumenCDAPendEnviar() {
		return volumenCDAPendEnviar;
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAPendEnviar
	 * @param volumenCDAPendEnviar Objeto del tipo @see String
	 */
	public void setVolumenCDAPendEnviar(String volumenCDAPendEnviar) {
		this.volumenCDAPendEnviar = volumenCDAPendEnviar;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAEnviadas
	 * @return volumenCDAEnviadas Objeto del tipo @see String
	 */
	public String getVolumenCDAEnviadas() {
		return volumenCDAEnviadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAEnviadas
	 * @param volumenCDAEnviadas Objeto del tipo @see String
	 */
	public void setVolumenCDAEnviadas(String volumenCDAEnviadas) {
		this.volumenCDAEnviadas = volumenCDAEnviadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAConfirmadas
	 * @return volumenCDAConfirmadas Objeto del tipo @see String
	 */
	public String getVolumenCDAConfirmadas() {
		return volumenCDAConfirmadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAConfirmadas
	 * @param volumenCDAConfirmadas Objeto del tipo @see String
	 */
	public void setVolumenCDAConfirmadas(String volumenCDAConfirmadas) {
		this.volumenCDAConfirmadas = volumenCDAConfirmadas;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDAContingencia
	 * @return volumenCDAContingencia Objeto del tipo @see String
	 */
	public String getVolumenCDAContingencia() {
		return volumenCDAContingencia; 
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDAContingencia
	 * @param volumenCDAContingencia Objeto del tipo @see String
	 */
	public void setVolumenCDAContingencia(String volumenCDAContingencia) {
		this.volumenCDAContingencia = volumenCDAContingencia;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad volumenCDARechazadas
	 * @return volumenCDARechazadas Objeto del tipo @see String
	 */
	public String getVolumenCDARechazadas() {
		return volumenCDARechazadas;
	}
	/**
	 * Metodo que modifica el valor de la propiedad volumenCDARechazadas
	 * @param volumenCDARechazadas Objeto del tipo @see String
	 */
	public void setVolumenCDARechazadas(String volumenCDARechazadas) {
		this.volumenCDARechazadas = volumenCDARechazadas;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad existeConexion
	 * @return existeConexion Objeto del tipo boolean
	 */
	public boolean isExisteConexion() {
		return existeConexion;
	}
	/**
	 * Modifica el valor de la propiedad existeConexion
	 * @param existeConexion Objeto del tipo boolean
	 */
	public void setExisteConexion(boolean existeConexion) {
		this.existeConexion = existeConexion;
	}

   /**
   * Metodo get que obtiene el valor de la propiedad existeContingencia
   * @return existeContingencia Objeto del tipo boolean
   */
   public boolean isExisteContingencia() {
	   return existeContingencia;
   }
	/**
	 * Modifica el valor de la propiedad existeContingencia
	 * @param existeContingencia Objeto del tipo boolean
	 */
   public void setExisteContingencia(boolean existeContingencia) {
	   this.existeContingencia = existeContingencia;
   }
	
	/**
	 * Obtener el objeto: monto CDA conf contingencia.
	 *
	 * @return El objeto: monto CDA conf contingencia
	 */
	public String getMontoCDAConfContingencia() {
		return montoCDAConfContingencia;
	}
	
	/**
	 * Definir el objeto: monto CDA conf contingencia.
	 *
	 * @param montoCDAConfContingencia El nuevo objeto: monto CDA conf contingencia
	 */
	public void setMontoCDAConfContingencia(String montoCDAConfContingencia) {
		this.montoCDAConfContingencia = montoCDAConfContingencia;
	}
	
	/**
	 * Obtener el objeto: volumen CDA conf contingencia.
	 *
	 * @return El objeto: volumen CDA conf contingencia
	 */
	public String getVolumenCDAConfContingencia() {
		return volumenCDAConfContingencia;
	}
	
	/**
	 * Definir el objeto: volumen CDA conf contingencia.
	 *
	 * @param volumenCDAConfContingencia El nuevo objeto: volumen CDA conf contingencia
	 */
	public void setVolumenCDAConfContingencia(String volumenCDAConfContingencia) {
		this.volumenCDAConfContingencia = volumenCDAConfContingencia;
	}
	
	/**
	 * Obtener el objeto: monto CDA env contingencia.
	 *
	 * @return El objeto: monto CDA env contingencia
	 */
	public String getMontoCDAEnvContingencia() {
		return montoCDAEnvContingencia;
	}
	
	/**
	 * Definir el objeto: monto CDA env contingencia.
	 *
	 * @param montoCDAEnvContingencia El nuevo objeto: monto CDA env contingencia
	 */
	public void setMontoCDAEnvContingencia(String montoCDAEnvContingencia) {
		this.montoCDAEnvContingencia = montoCDAEnvContingencia;
	}
	
	/**
	 * Obtener el objeto: volumen CDA env contingencia.
	 *
	 * @return El objeto: volumen CDA env contingencia
	 */
	public String getVolumenCDAEnvContingencia() {
		return volumenCDAEnvContingencia;
	}
	
	/**
	 * Definir el objeto: volumen CDA env contingencia.
	 *
	 * @param volumenCDAEnvContingencia El nuevo objeto: volumen CDA env contingencia
	 */
	public void setVolumenCDAEnvContingencia(String volumenCDAEnvContingencia) {
		this.volumenCDAEnvContingencia = volumenCDAEnvContingencia;
	}
	
	/**
	 * Obtener el objeto: monto CDA procesadas.
	 *
	 * @return El objeto: monto CDA procesadas
	 */
	public String getMontoCDAProcesadas() {
		return montoCDAProcesadas;
	}
	
	/**
	 * Definir el objeto: monto CDA procesadas.
	 *
	 * @param montoCDAProcesadas El nuevo objeto: monto CDA procesadas
	 */
	public void setMontoCDAProcesadas(String montoCDAProcesadas) {
		this.montoCDAProcesadas = montoCDAProcesadas;
	}
	
	/**
	 * Obtener el objeto: volumen CDA procesadas.
	 *
	 * @return El objeto: volumen CDA procesadas
	 */
	public String getVolumenCDAProcesadas() {
		return volumenCDAProcesadas;
	}
	
	/**
	 * Definir el objeto: volumen CDA procesadas.
	 *
	 * @param volumenCDAProcesadas El nuevo objeto: volumen CDA procesadas
	 */
	public void setVolumenCDAProcesadas(String volumenCDAProcesadas) {
		this.volumenCDAProcesadas = volumenCDAProcesadas;
	}
	   
	  
}
