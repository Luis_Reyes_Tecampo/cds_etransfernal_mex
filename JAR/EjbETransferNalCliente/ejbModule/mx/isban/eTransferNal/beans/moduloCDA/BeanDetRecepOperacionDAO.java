/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanDetRecepOperacionBO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015 16:02 INDRA     ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

/**
 * clase de tipo bean que contiene atributos para mostrar el detalle sw una operacion.
 *
 * @author INDRA
 * @sice 29/09/2015
 */
public class BeanDetRecepOperacionDAO  implements Serializable{

	/** La variable que contiene informacion con respecto a: parametro. */
	private String parametro;
	
	/**   Constante privada del Serial version. */
	private static final long serialVersionUID = 4403539345829770293L;
	
	/** La variable que contiene informacion con respecto a: bean det recep operacion. */
	private BeanDetRecepOperacion beanDetRecepOperacion;
	
	/**
	 * Instantiates a new bean det recep operacion DAO.
	 */
	public BeanDetRecepOperacionDAO() {
		super();
		this.beanDetRecepOperacion = new BeanDetRecepOperacion();
	}

	/**
	 * Obtener el objeto: bean det recep operacion.
	 *
	 * @return El objeto: bean det recep operacion
	 */
	public BeanDetRecepOperacion getBeanDetRecepOperacion() {
		return beanDetRecepOperacion;
	}

	/**
	 * Definir el objeto: bean det recep operacion.
	 *
	 * @param beanDetRecepOperacion El nuevo objeto: bean det recep operacion
	 */
	public void setBeanDetRecepOperacion(BeanDetRecepOperacion beanDetRecepOperacion) {
		this.beanDetRecepOperacion = beanDetRecepOperacion;
	}
	
	/**
	 * Obtener el objeto: parametro.
	 *
	 * @return El objeto: parametro
	 */
	public String getParametro() {
		return parametro;
	}

	/**
	 * Definir el objeto: parametro.
	 *
	 * @param parametro El nuevo objeto: parametro
	 */
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
}
