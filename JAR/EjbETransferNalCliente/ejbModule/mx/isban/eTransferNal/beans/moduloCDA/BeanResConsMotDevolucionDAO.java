package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;
import java.util.List;

public class BeanResConsMotDevolucionDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 6674338589729315053L;
	
	/**
	 * Propiedad del tipo List<BeanMotDevolucion> que almacena el valor de beanMotDevolucionList
	 */
	private List<BeanMotDevolucion> beanMotDevolucionList;

	/**
	 * Metodo get que obtiene el valor de la propiedad beanMotDevolucionList
	 * @return beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public List<BeanMotDevolucion> getBeanMotDevolucionList() {
		return beanMotDevolucionList;
	}

	/**
	 * Metodo que modifica el valor de la propiedad beanMotDevolucionList
	 * @param beanMotDevolucionList Objeto de tipo @see List<BeanMotDevolucion>
	 */
	public void setBeanMotDevolucionList(
			List<BeanMotDevolucion> beanMotDevolucionList) {
		this.beanMotDevolucionList = beanMotDevolucionList;
	}	

}
