/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGenArchCDAHistDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu Dec 12 11:52:02 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.moduloCDA;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * para la funcionalidad de Generar Archivo CDA Historico
**/

public class BeanResGenArchCDAHistDAO implements BeanResultBO,Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;
   /**Propiedad privada del tipo String que almacena el id de la peticion*/
   private String idPeticion;
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
    *Metodo get que sirve para obtener el valor de la propiedad idPeticion
    * @return idPeticion Objeto del tipo String
    */
	public String getIdPeticion() {
		return idPeticion;
	}
   /**
    *Metodo set que sirve para modificar el valor de la propiedad idPeticion
    * @param idPeticion Objeto del tipo String
    */
	public void setIdPeticion(String idPeticion) {
		this.idPeticion = idPeticion;
	}



}
