package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;
/**
 * The Class BeanLiquidacionesCombo.
 */
public class BeanLiquidacionesCombo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1125606840400006265L;

	/** The trasferencias. */
	@Size(min=0, max=3)
	private List<BeanCatalogo> trasferencias;
	
	/** The operaciones. */
	@Size(min=0, max=8)
	private List<BeanCatalogo> operaciones;
	
	/** The estatus. */
	@Size(min=0, max=2)
	private List<BeanCatalogo> estatus;
	
	/** The mecanismos. */
	@Size(min=0, max=8)
	private List<BeanCatalogo> mecanismos;
	
	/**
	 * Gets the trasferencias.
	 *
	 * @return the trasferencias
	 */
	public List<BeanCatalogo> getTrasferencias() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.trasferencias;
		return listAux;
	}
	
	/**
	 * Sets the trasferencias.
	 *
	 * @param trasferencias the new trasferencias
	 */
	public void setTrasferencias(List<BeanCatalogo> trasferencias) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = trasferencias;
		this.trasferencias = listAux;
	}
	
	/**
	 * Gets the operaciones.
	 *
	 * @return the operaciones
	 */
	public List<BeanCatalogo> getOperaciones() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.operaciones;
		return listAux;
	}
	
	/**
	 * Sets the operaciones.
	 *
	 * @param operaciones the new operaciones
	 */
	public void setOperaciones(List<BeanCatalogo> operaciones) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = operaciones;
		this.operaciones = listAux;
	}
	
	/**
	 * Gets the estatus.
	 *
	 * @return the estatus
	 */
	public List<BeanCatalogo> getEstatus() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.estatus;
		return listAux;
	}
	
	/**
	 * Sets the estatus.
	 *
	 * @param estatus the new estatus
	 */
	public void setEstatus(List<BeanCatalogo> estatus) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = estatus;
		this.estatus = listAux;
	}
	
	/**
	 * Gets the mecanismos.
	 *
	 * @return the mecanismos
	 */
	public List<BeanCatalogo> getMecanismos() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.mecanismos;
		return listAux;
	}
	
	/**
	 * Sets the mecanismos.
	 *
	 * @param mecanismos the new mecanismos
	 */
	public void setMecanismos(List<BeanCatalogo> mecanismos) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = mecanismos;
		this.mecanismos = listAux;
	}
}
