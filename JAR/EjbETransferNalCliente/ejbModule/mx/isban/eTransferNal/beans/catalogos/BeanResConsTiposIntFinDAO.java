/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsInstDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Sep 18 10:39:58 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de los Tipos Int Financieros.
 */

public class BeanResConsTiposIntFinDAO extends BeanResBase implements Serializable {
	
	 /** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/** Propiedad del tipo Integer que almacena el toral de registors. */
	private Integer totalReg = 0;

	/** La variable que contiene informacion con respecto a: list tipos int fin. */
	private List<String> listTiposIntFin;
	
	/** La variable que contiene informacion con respecto a: cve interme. */
	private String cveInterme;
    
    /** La variable que contiene informacion con respecto a: num indeval. */
    private String numIndeval;
    
    /** La variable que contiene informacion con respecto a: tipo interme. */
    private String tipoInterme;
    
    /** La variable que contiene informacion con respecto a: id int indeval. */
    private String idIntIndeval;
    
    /** La variable que contiene informacion con respecto a: num cecoban. */
    private String numCecoban;
    
    /** La variable que contiene informacion con respecto a: fol int indeval. */
    private String folIntIndeval;
    
    /** La variable que contiene informacion con respecto a: nombre corto. */
    private String nombreCorto;
    
    /** La variable que contiene informacion con respecto a: nombre largo. */
    private String nombreLargo;
    
    /** La variable que contiene informacion con respecto a: num persona. */
    private String numPersona;
    
    /** La variable que contiene informacion con respecto a: status. */
    private String status;
    
    /** La variable que contiene informacion con respecto a: num banxico. */
    private String numBanxico;
    
    /** La variable que contiene informacion con respecto a: cve usuario. */
    private String cveUsuario;
    
    /** La variable que contiene informacion con respecto a: cve swift cor. */
    private String cveSwiftCor;
    
    /** La variable que contiene informacion con respecto a: fch baja. */
    private String fchBaja;
	
	/**
	 * Obtener el objeto: list tipos int fin.
	 *
	 * @return El objeto: list tipos int fin
	 */
	public List<String> getListTiposIntFin() {
		return listTiposIntFin;
	}

	/**
	 * Definir el objeto: list tipos int fin.
	 *
	 * @param listTiposIntFin El nuevo objeto: list tipos int fin
	 */
	public void setListTiposIntFin(List<String> listTiposIntFin) {
		this.listTiposIntFin = listTiposIntFin;
	}

	/**
	 * Metodo get que obtiene el total de registros.
	 *
	 * @return Integer objeto con el numero total de registros
	 */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros.
    *
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Obtener el objeto: cve interme.
	 *
	 * @return El objeto: cve interme
	 */
	public String getCveInterme() {
		return cveInterme;
	}
	
	/**
	 * Definir el objeto: cve interme.
	 *
	 * @param cveInterme El nuevo objeto: cve interme
	 */
	public void setCveInterme(String cveInterme) {
		this.cveInterme = cveInterme;
	}
	
	/**
	 * Obtener el objeto: num indeval.
	 *
	 * @return El objeto: num indeval
	 */
	public String getNumIndeval() {
		return numIndeval;
	}
	
	/**
	 * Definir el objeto: num indeval.
	 *
	 * @param numIndeval El nuevo objeto: num indeval
	 */
	public void setNumIndeval(String numIndeval) {
		this.numIndeval = numIndeval;
	}
	
	/**
	 * Obtener el objeto: tipo interme.
	 *
	 * @return El objeto: tipo interme
	 */
	public String getTipoInterme() {
		return tipoInterme;
	}
	
	/**
	 * Definir el objeto: tipo interme.
	 *
	 * @param tipoInterme El nuevo objeto: tipo interme
	 */
	public void setTipoInterme(String tipoInterme) {
		this.tipoInterme = tipoInterme;
	}
	
	/**
	 * Obtener el objeto: id int indeval.
	 *
	 * @return El objeto: id int indeval
	 */
	public String getIdIntIndeval() {
		return idIntIndeval;
	}
	
	/**
	 * Definir el objeto: id int indeval.
	 *
	 * @param idIntIndeval El nuevo objeto: id int indeval
	 */
	public void setIdIntIndeval(String idIntIndeval) {
		this.idIntIndeval = idIntIndeval;
	}
	
	/**
	 * Obtener el objeto: num cecoban.
	 *
	 * @return El objeto: num cecoban
	 */
	public String getNumCecoban() {
		return numCecoban;
	}
	
	/**
	 * Definir el objeto: num cecoban.
	 *
	 * @param numCecoban El nuevo objeto: num cecoban
	 */
	public void setNumCecoban(String numCecoban) {
		this.numCecoban = numCecoban;
	}
	
	/**
	 * Obtener el objeto: fol int indeval.
	 *
	 * @return El objeto: fol int indeval
	 */
	public String getFolIntIndeval() {
		return folIntIndeval;
	}
	
	/**
	 * Definir el objeto: fol int indeval.
	 *
	 * @param folIntIndeval El nuevo objeto: fol int indeval
	 */
	public void setFolIntIndeval(String folIntIndeval) {
		this.folIntIndeval = folIntIndeval;
	}
	
	/**
	 * Obtener el objeto: nombre corto.
	 *
	 * @return El objeto: nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}
	
	/**
	 * Definir el objeto: nombre corto.
	 *
	 * @param nombreCorto El nuevo objeto: nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	
	/**
	 * Obtener el objeto: nombre largo.
	 *
	 * @return El objeto: nombre largo
	 */
	public String getNombreLargo() {
		return nombreLargo;
	}
	
	/**
	 * Definir el objeto: nombre largo.
	 *
	 * @param nombreLargo El nuevo objeto: nombre largo
	 */
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
	
	/**
	 * Obtener el objeto: num persona.
	 *
	 * @return El objeto: num persona
	 */
	public String getNumPersona() {
		return numPersona;
	}
	
	/**
	 * Definir el objeto: num persona.
	 *
	 * @param numPersona El nuevo objeto: num persona
	 */
	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}
	
	/**
	 * Obtener el objeto: status.
	 *
	 * @return El objeto: status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Definir el objeto: status.
	 *
	 * @param status El nuevo objeto: status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Obtener el objeto: num banxico.
	 *
	 * @return El objeto: num banxico
	 */
	public String getNumBanxico() {
		return numBanxico;
	}
	
	/**
	 * Definir el objeto: num banxico.
	 *
	 * @param numBanxico El nuevo objeto: num banxico
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}
	
	/**
	 * Obtener el objeto: cve usuario.
	 *
	 * @return El objeto: cve usuario
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}
	
	/**
	 * Definir el objeto: cve usuario.
	 *
	 * @param cveUsuario El nuevo objeto: cve usuario
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	/**
	 * Obtener el objeto: cve swift cor.
	 *
	 * @return El objeto: cve swift cor
	 */
	public String getCveSwiftCor() {
		return cveSwiftCor;
	}

	/**
	 * Definir el objeto: cve swift cor.
	 *
	 * @param cveSwiftCor El nuevo objeto: cve swift cor
	 */
	public void setCveSwiftCor(String cveSwiftCor) {
		this.cveSwiftCor = cveSwiftCor;
	}

	/**
	 * Obtener el objeto: fch baja.
	 *
	 * @return El objeto: fch baja
	 */
	public String getFchBaja() {
		return fchBaja;
	}

	/**
	 * Definir el objeto: fch baja.
	 *
	 * @param fchBaja El nuevo objeto: fch baja
	 */
	public void setFchBaja(String fchBaja) {
		this.fchBaja = fchBaja;
	}

}
