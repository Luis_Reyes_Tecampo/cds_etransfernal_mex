/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResElimSelecArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 17:39:30 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanGenArchHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *Clase Bean DTO que se encarga de encapsular la respuesta en la opcion de
 * eliminar en la funcionalidad de Generar Archivo CDA Historico Detalle
 **/

public class BeanResElimIntFinDAO extends BeanResBase implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

	/** Propiedad que almacena la abstraccion de una pagina */
	private BeanPaginador beanPaginador;
	/**
	 * Propiedad que almacena la lista de registros que seran marcados como
	 * eliminados
	 */
	private List<BeanGenArchHistDet> listBeanGenArchHistDet;

	/**
	 * Propiedad privada del tipo String que almacena el numero de registros en
	 * el archivo
	 */
	private String numRegArchivo;
	/**
	 * Propiedad privada del tipo String que almacena el numero de registros
	 * encontrados
	 */
	private String numRegEncontrados;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listBeanGenArchHistDet
	 * 
	 * @return listBeanGenArchHistDet Objeto del tipo List<BeanGenArchHistDet>
	 */
	public List<BeanGenArchHistDet> getListBeanGenArchHistDet() {
		return listBeanGenArchHistDet;
	}

	/**
	 *Modifica el valor de la propiedad listBeanGenArchHistDet
	 * 
	 * @param listBeanGenArchHistDet
	 *            Objeto del tipo List<BeanGenArchHistDet>
	 */
	public void setListBeanGenArchHistDet(
			List<BeanGenArchHistDet> listBeanGenArchHistDet) {
		this.listBeanGenArchHistDet = listBeanGenArchHistDet;
	}



	/**
	 * Metodo get que obtiene el valor de la propiedad beanPaginador
	 * @return beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad beanPaginador
	 * @param beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad numRegArchivo
	 * 
	 * @return numRegArchivo Objeto del tipo String
	 */
	public String getNumRegArchivo() {
		return numRegArchivo;
	}

	/**
	 *Modifica el valor de la propiedad numRegArchivo
	 * 
	 * @param numRegArchivo
	 *            Objeto del tipo String
	 */
	public void setNumRegArchivo(String numRegArchivo) {
		this.numRegArchivo = numRegArchivo;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * numRegEncontrados
	 * 
	 * @return numRegEncontrados Objeto del tipo String
	 */
	public String getNumRegEncontrados() {
		return numRegEncontrados;
	}

	/**
	 *Modifica el valor de la propiedad numRegEncontrados
	 * 
	 * @param numRegEncontrados
	 *            Objeto del tipo String
	 */
	public void setNumRegEncontrados(String numRegEncontrados) {
		this.numRegEncontrados = numRegEncontrados;
	}


}
