/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCuentas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 12:39:36 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanCuentas.
 *
 * Clase tipo Bean que almacena los atributos
 * que se utilizaran en los flujos de este
 * catalogo.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
public class BeanCuentas implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4267119163844978382L;

	/** La variable que contiene informacion con respecto a: cuentas. */
	@Size(min=20,max=20)
	private List<BeanCuenta> cuentas;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	@NotNull
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	@NotNull
	private BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	@NotNull
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	@NotNull
	private int totalReg = 0;

	/**
	 * Obtener el objeto: cuentas.
	 *
	 * @return El objeto: cuentas
	 */
	public List<BeanCuenta> getCuentas() {
		List<BeanCuenta> listAux = new ArrayList<BeanCuenta>();
		listAux = this.cuentas;
		return listAux;
	}

	/**
	 * Definir el objeto: cuentas.
	 *
	 * @param cuentas El nuevo objeto: cuentas
	 */
	public void setCuentas(List<BeanCuenta> cuentas) {
		List<BeanCuenta> listAux = new ArrayList<BeanCuenta>();
		listAux = cuentas;
		this.cuentas = listAux;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
	
}
