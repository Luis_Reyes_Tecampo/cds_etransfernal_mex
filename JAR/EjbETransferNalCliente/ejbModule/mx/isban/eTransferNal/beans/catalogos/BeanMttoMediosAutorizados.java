/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMttoMediosAutorizados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   21/09/2018 04:37:15 PM ALFONSO HERNANDEZ. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;



/**
 * Class BeanMttoMediosAutorizados.
 *
 * @author FSW-Vector
 * @since 21/09/2018
 */
public class BeanMttoMediosAutorizados extends BeanMttoMediosAReq implements Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4895369192417027187L;
	
	/** La variable que contiene informacion con respecto a: cve medio ent. */
	private String cveMedioEnt;
	
	/** La variable que contiene informacion con respecto a: cve transfe. */
	private String cveTransfe;
	
	/** La variable que contiene informacion con respecto a: descripcion. */
	private String desCveMed;
	
	/** La variable que contiene informacion con respecto a: cve operacion. */
	private String cveOperacion;
	
	/** La variable que contiene informacion con respecto a: cve CLACON. */
	private String cveCLACON;
	
	/** La variable que contiene informacion con respecto a: suc operante. */
	private String sucOperante;
	
	/** La variable que contiene informacion con respecto a: hora inicio. */
	private String horaInicio;
	
	/** La variable que contiene informacion con respecto a: hora cierre. */
	private String horaCierre;
	
	/** La variable que contiene informacion con respecto a: horario estatus. */
	private String horarioEstatus;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;
	
	/**
	 * Obtener el objeto: cve medio ent.
	 *
	 * @return El objeto: cve medio ent
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	
	/**
	 * Definir el objeto: cve medio ent.
	 *
	 * @param cveMedioEnt El nuevo objeto: cve medio ent
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	
	/**
	 * Obtener el objeto: cve transfe.
	 *
	 * @return El objeto: cve transfe
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	
	/**
	 * Definir el objeto: cve transfe.
	 *
	 * @param cveTransfe El nuevo objeto: cve transfe
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	
	
	/**
	 * Gets the des cve med.
	 *
	 * @return the des cve med
	 */
	public String getDesCveMed() {
		return desCveMed;
	}

	/**
	 * Sets the des cve med.
	 *
	 * @param desCveMed the new des cve med
	 */
	public void setDesCveMed(String desCveMed) {
		this.desCveMed = desCveMed;
	}

	/**
	 * Obtener el objeto: cve operacion.
	 *
	 * @return El objeto: cve operacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	
	/**
	 * Definir el objeto: cve operacion.
	 *
	 * @param cveOperacion El nuevo objeto: cve operacion
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	
	/**
	 * Obtener el objeto: cve CLACON.
	 *
	 * @return El objeto: cve CLACON
	 */
	public String getCveCLACON() {
		return cveCLACON;
	}
	
	/**
	 * Definir el objeto: cve CLACON.
	 *
	 * @param cveCLACON El nuevo objeto: cve CLACON
	 */
	public void setCveCLACON(String cveCLACON) {
		this.cveCLACON = cveCLACON;
	}
	
	
	
	/**
	 * getSucOperante de tipo String.
	 * @author FSW-Vector
	 * @return sucOperante de tipo String
	 */
	public String getSucOperante() {
		return sucOperante;
	}

	/**
	 * setSucOperante para asignar valor a sucOperante.
	 * @author FSW-Vector
	 * @param sucOperante de tipo String
	 */
	public void setSucOperante(String sucOperante) {
		this.sucOperante = sucOperante;
	}

	/**
	 * Obtener el objeto: hora inicio.
	 *
	 * @return El objeto: hora inicio
	 */
	public String getHoraInicio() {
		return horaInicio;
	}
	
	/**
	 * Definir el objeto: hora inicio.
	 *
	 * @param horaInicio El nuevo objeto: hora inicio
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	/**
	 * Obtener el objeto: hora cierre.
	 *
	 * @return El objeto: hora cierre
	 */
	public String getHoraCierre() {
		return horaCierre;
	}
	
	/**
	 * Definir el objeto: hora cierre.
	 *
	 * @param horaCierre El nuevo objeto: hora cierre
	 */
	public void setHoraCierre(String horaCierre) {
		this.horaCierre = horaCierre;
	}
	
	/**
	 * Obtener el objeto: horario estatus.
	 *
	 * @return El objeto: horario estatus
	 */
	public String getHorarioEstatus() {
		return horarioEstatus;
	}
	
	/**
	 * Definir el objeto: horario estatus.
	 *
	 * @param horarioEstatus El nuevo objeto: horario estatus
	 */
	public void setHorarioEstatus(String horarioEstatus) {
		this.horarioEstatus = horarioEstatus;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
}
