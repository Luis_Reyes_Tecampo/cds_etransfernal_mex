	/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMedioEntrega.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   28/09/2018 04:22:34 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * Class BeanMedioEntrega.
 * 
 * Mapea los datos de la pantalla de alta de medio de entrega
 *
 * @author FSW-Vector
 * @since 28/09/2018
 */
public class BeanMedioEntrega implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1208031376622308600L;
	
	/** La variable que contiene informacion con respecto a: cve medio entrega. */
	private String cveMedioEntrega;
	
	/** La variable que contiene informacion con respecto a: version. */
	private String version;
	
	/** La variable que contiene informacion con respecto a: desc. */
	private String desc;
	
	/** La variable que contiene informacion con respecto a: com anual PF. */
	private String comAnualPF;
	
	/** La variable que contiene informacion con respecto a: com anual PM. */
	private String comAnualPM;
	
	/** La variable que contiene informacion con respecto a: seguridad. */
	private String seguridad;
	
	/** La variable que contiene informacion con respecto a: canal. */
	private String canal;
	
	/** La variable que contiene informacion con respecto a: importe maximo para operar SPEI despues de las 18 hrs. */
	private String importeMax;
		
	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;

	/**
	 * Obtener el objeto: cve medio entrega.
	 *
	 * @return El objeto: cve medio entrega
	 */
	public String getCveMedioEntrega() {
		return cveMedioEntrega;
	}

	/**
	 * Definir el objeto: cve medio entrega.
	 *
	 * @param cveMedioEntrega El nuevo objeto: cve medio entrega
	 */
	public void setCveMedioEntrega(String cveMedioEntrega) {
		this.cveMedioEntrega = cveMedioEntrega;
	}

	/**
	 * Obtener el objeto: version.
	 *
	 * @return El objeto: version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Definir el objeto: version.
	 *
	 * @param version El nuevo objeto: version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Obtener el objeto: desc.
	 *
	 * @return El objeto: desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Definir el objeto: desc.
	 *
	 * @param desc El nuevo objeto: desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Obtener el objeto: com anual PF.
	 *
	 * @return El objeto: com anual PF
	 */
	public String getComAnualPF() {
		return comAnualPF;
	}

	/**
	 * Definir el objeto: com anual PF.
	 *
	 * @param comAnualPF El nuevo objeto: com anual PF
	 */
	public void setComAnualPF(String comAnualPF) {
		this.comAnualPF = comAnualPF;
	}

	/**
	 * Obtener el objeto: com anual PM.
	 *
	 * @return El objeto: com anual PM
	 */
	public String getComAnualPM() {
		return comAnualPM;
	}

	/**
	 * Definir el objeto: com anual PM.
	 *
	 * @param comAnualPM El nuevo objeto: com anual PM
	 */
	public void setComAnualPM(String comAnualPM) {
		this.comAnualPM = comAnualPM;
	}

	/**
	 * Obtener el objeto: seguridad.
	 *
	 * @return El objeto: seguridad
	 */
	public String getSeguridad() {
		return seguridad;
	}

	/**
	 * Definir el objeto: seguridad.
	 *
	 * @param seguridad El nuevo objeto: seguridad
	 */
	public void setSeguridad(String seguridad) {
		this.seguridad = seguridad;
	}

	/**
	 * Obtener el objeto: canal.
	 *
	 * @return El objeto: canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * Definir el objeto: canal.
	 *
	 * @param canal El nuevo objeto: canal
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}
	
	/**
	 * Obtener el objeto: canal.
	 *
	 * @return El objeto: canal
	 */
	public String getImporteMax() {
		return importeMax;
	}

	/**
	 * Definir el objeto: canal.
	 *
	 * @param canal El nuevo objeto: canal
	 */
	public void setImporteMax (String importeMax) {
		this.importeMax = importeMax;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	

}
