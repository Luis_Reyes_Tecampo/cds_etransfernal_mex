package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

/**
 * The Class BeanMttoMediosAutorizadosReq.
 *
 * @author ahernandez
 */
public class BeanMttoMediosAutorizadosReq  extends BeanMttoMediosAutorizados implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4895369192417027187L;
	
	/**private **/
	private List<BeanMttoMediosAutorizados> listaBeanMttoMedAuto;
	
	/** variable para busqueda */
	private String dtosBusqueda;
	
	
	/**
	 * @return the dtosBusqueda
	 */
	public String getDtosBusqueda() {
		return dtosBusqueda;
	}

	/**
	 * @param dtosBusqueda the dtosBusqueda to set
	 */
	public void setDtosBusqueda(String dtosBusqueda) {
		this.dtosBusqueda = dtosBusqueda;
	}

	/**
	 * @return the listaBeanMttoMedAuto
	 */
	public List<BeanMttoMediosAutorizados> getListaBeanMttoMedAuto() {
		return listaBeanMttoMedAuto;
	}

	/**
	 * @param listaBeanMttoMedAuto the listaBeanMttoMedAuto to set
	 */
	public void setListaBeanMttoMedAuto(List<BeanMttoMediosAutorizados> listaBeanMttoMedAuto) {
		this.listaBeanMttoMedAuto = listaBeanMttoMedAuto;
	}
	
	
}
