/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanNotificacionesExportar.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:36:12 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Class BeanNotificacionesExportar.
 *
 * Clase que contiene los atributos y metodos de acceso para 
 * el objeto que contiene los datos utilizados por los flujos.
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
public class BeanNotificacionesExportar implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -870950181963430203L;
	
	/** La variable que contiene informacion con respecto a: notificaciones E. */
	@NotNull
	private List<BeanNotificacion> notificacionesE;
	
	/**
	 * Obtener el objeto: notificaciones.
	 *
	 * @return El objeto: notificaciones
	 */
	@NotNull
	public List<BeanNotificacion> getNotificacionesE() {
		List<BeanNotificacion> listAux = new ArrayList<BeanNotificacion>();
		listAux = this.notificacionesE;
		return listAux;
	}

	/**
	 * Definir el objeto: notificaciones.
	 *
	 * @param notificaciones El nuevo objeto: notificaciones
	 */
	public void setNotificacionesE(List<BeanNotificacion> notificacionesE) {
		List<BeanNotificacion> listAux = new ArrayList<BeanNotificacion>();
		listAux = notificacionesE;
		this.notificacionesE = listAux;
	}
	
}
