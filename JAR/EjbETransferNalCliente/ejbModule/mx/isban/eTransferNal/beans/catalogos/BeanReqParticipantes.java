/**
 * Isban Mexico
 * Clase: BeanReqParticipantes.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 13/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
public class BeanReqParticipantes implements Serializable{

	/**  Propiedad del tipo long que almacena el valor de serialVersionUID*/
	private static final long serialVersionUID = 4640429982534429938L;

	/**  Propiedad del tipo String que almacena el valor de cveIntermediario*/
	private String cveIntermediario;
	
	
	/**  Propiedad del tipo String que almacena el valor de numBanxico*/
	private String numBanxico;
	
	/**  Propiedad del tipo String que almacena el valor de nombre*/
	private String nombre;
	
	/**  Propiedad del tipo String que almacena el valor de fecha*/
	private String fecha;

	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
    private BeanPaginador paginador = null;
    
    /**Propiedad privada del tipo Integer que almacena el total de registros*/
    private Integer totalReg;
    
	/**
	 * @return Valor del atributo: cveIntermediario
	 */
	public String getCveIntermediario() {
		return cveIntermediario;
	}

	/**
	 * @param cveIntermediario Valor a establecer en atributo: cveIntermediario
	 */
	public void setCveIntermediario(String cveIntermediario) {
		this.cveIntermediario = cveIntermediario;
	}

	/**
	 * @return Valor del atributo: numBanxico
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * @param numBanxico Valor a establecer en atributo: numBanxico
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * @return Valor del atributo: nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre Valor a establecer en atributo: nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return Valor del atributo: fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha Valor a establecer en atributo: fecha
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	/**
	 * Devuelve el valor de paginador
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Modifica el valor de paginador
	 * @param paginador the paginador to set
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Devuelve el valor de totalReg
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Modifica el valor de totalReg
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}	
}
