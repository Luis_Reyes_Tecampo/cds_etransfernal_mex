/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanBloque.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:10:22 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
/**
 * Class BeanBloque.
 *
 * Clase que contiene los atributos y metodos de acceso para el objeto
 * bloque.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
public class BeanBloque implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 928135725367020176L;

	/** La variable que contiene informacion con respecto a: num bloque. */
	@Pattern(regexp = "^[a-zA-Z0-9ÁÉÍÓÚÑáéíóúñ\\s\\&\\.\\,\\#]*$", message="idioma not valid.")
	private String numBloque;
	
	/** La variable que contiene informacion con respecto a: valor. */
	@Pattern(regexp = "^[a-zA-Z0-9ÁÉÍÓÚÑáéíóúñ\\s\\&\\.\\,\\#]*$", message="idioma not valid.")
	private String valor;
	
	/** La variable que contiene informacion con respecto a: descripcion. */
	@Pattern(regexp = "^[a-zA-Z0-9ÁÉÍÓÚÑáéíóúñ\\s\\&\\.\\,\\#]*$", message="idioma not valid.")
	private String descripcion;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	private boolean seleccionado;
	
	/**
	 * Obtener el objeto: num bloque.
	 *
	 * @return El objeto: num bloque
	 */
	public String getNumBloque() {
		return numBloque;
	}
	
	/**
	 * Definir el objeto: num bloque.
	 *
	 * @param numBloque El nuevo objeto: num bloque
	 */
	public void setNumBloque(String numBloque) {
		this.numBloque = numBloque;
	}
	
	/**
	 * Obtener el objeto: valor.
	 *
	 * @return El objeto: valor
	 */
	public String getValor() {
		return valor;
	}
	
	/**
	 * Definir el objeto: valor.
	 *
	 * @param valor El nuevo objeto: valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	/**
	 * Obtener el objeto: descripcion.
	 *
	 * @return El objeto: descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Definir el objeto: descripcion.
	 *
	 * @param descripcion El nuevo objeto: descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
