/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/09/2019 02:02:59 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


/**
 * Class BeanExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 18/09/2019
 */
public class BeanExcepCtasFideicomiso implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4633082158560827256L;
	
	/** La variable que contiene informacion con respecto a: lista cuentas. */
	@NotNull
	private List<BeanCtaFideicomiso> listaCuentas;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	@NotNull
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	@NotNull
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	@NotNull
	private BeanCtaFideicomiso beanFilter;
	
	/**
	 * Nueva instancia bean excep ctas fideicomiso.
	 */
	public BeanExcepCtasFideicomiso() {
		super();
		this.beanError = new BeanResBase();
		this.beanPaginador = new BeanPaginador();
		this.beanFilter = new BeanCtaFideicomiso();
	}

	/**
	 * Obtener el objeto: lista cuentas.
	 *
	 * @return El objeto: lista cuentas
	 */
	public List<BeanCtaFideicomiso> getListaCuentas() {
		List<BeanCtaFideicomiso> listAux = new ArrayList<BeanCtaFideicomiso>();
		listAux = this.listaCuentas;
		return listAux;
	}

	/**
	 * Definir el objeto: lista cuentas.
	 *
	 * @param listaCuentas El nuevo objeto: lista cuentas
	 */
	public void setListaCuentas(List<BeanCtaFideicomiso> listaCuentas) {
		List<BeanCtaFideicomiso> listAux = new ArrayList<BeanCtaFideicomiso>();
		listAux = listaCuentas;
		this.listaCuentas = listAux;
	}

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	
	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanCtaFideicomiso getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanCtaFideicomiso beanFilter) {
		this.beanFilter = beanFilter;
	}
	

}
