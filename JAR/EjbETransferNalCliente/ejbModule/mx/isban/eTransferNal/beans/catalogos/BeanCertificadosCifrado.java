/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCertificadosCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-25-2018 04:14:48 PM Juan Jesus Beltran R. Vector Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanCertificadosCifrado.
 * 
 * Contiene los metodos y atributos 
 * para la pantalla de consulta de Certificados
 * 
 * -Contiene una lista del bean: BeanCertificadoCifrado
 * -Contiene beanPaginador
 * -Contiene beanFilter
 * -Contiene beanError
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanCertificadosCifrado implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4680823372443227433L;

	/** La variable que contiene informacion con respecto a: certificados. */
	private List<BeanCertificadoCifrado> certificados;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	private BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	private int totalReg = 0;

	/**
	 * Obtener el objeto: certificados.
	 *
	 * @return El objeto: certificados
	 */
	public List<BeanCertificadoCifrado> getCertificados() {
		return certificados;
	}

	/**
	 * Definir el objeto: certificados.
	 *
	 * @param certificados El nuevo objeto: certificados
	 */
	public void setCertificados(List<BeanCertificadoCifrado> certificados) {
		this.certificados = certificados;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
	
}
