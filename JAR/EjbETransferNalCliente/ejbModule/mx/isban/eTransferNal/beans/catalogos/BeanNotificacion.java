/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanNotificacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:35:33 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * Class BeanNotificacion.
 *
 * Clase que contiene los atributos y metodos de acceso para el objeto
 * notificador.
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
public class BeanNotificacion implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8338961611166995329L;

	/** La variable que contiene informacion con respecto a: tipo notif. */
	@NotNull
	private String tipoNotif;
	
	/** La variable que contiene informacion con respecto a: cve transfer. */
	@NotNull
	private String cveTransfer;
	
	/** La variable que contiene informacion con respecto a: medio entrega. */
	@NotNull
	private String medioEntrega;
	
	/** La variable que contiene informacion con respecto a: estatus transfer. */
	@NotNull
	private String estatusTransfer;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;
	
	/**
	 * Obtener el objeto: tipo notif.
	 *
	 * @return El objeto: tipo notif
	 */
	public String getTipoNotif() {
		return tipoNotif;
	}
	
	/**
	 * Definir el objeto: tipo notif.
	 *
	 * @param tipoNotif El nuevo objeto: tipo notif
	 */
	public void setTipoNotif(String tipoNotif) {
		this.tipoNotif = tipoNotif;
	}
	
	/**
	 * Obtener el objeto: cve transfer.
	 *
	 * @return El objeto: cve transfer
	 */
	public String getCveTransfer() {
		return cveTransfer;
	}
	
	/**
	 * Definir el objeto: cve transfer.
	 *
	 * @param cveTransfer El nuevo objeto: cve transfer
	 */
	public void setCveTransfer(String cveTransfer) {
		this.cveTransfer = cveTransfer;
	}
	
	/**
	 * Obtener el objeto: medio entrega.
	 *
	 * @return El objeto: medio entrega
	 */
	public String getMedioEntrega() {
		return medioEntrega;
	}
	
	/**
	 * Definir el objeto: medio entrega.
	 *
	 * @param medioEntrega El nuevo objeto: medio entrega
	 */
	public void setMedioEntrega(String medioEntrega) {
		this.medioEntrega = medioEntrega;
	}
	
	/**
	 * Obtener el objeto: estatus transfer.
	 *
	 * @return El objeto: estatus transfer
	 */
	public String getEstatusTransfer() {
		return estatusTransfer;
	}
	
	/**
	 * Definir el objeto: estatus transfer.
	 *
	 * @param estatusTransfer El nuevo objeto: estatus transfer
	 */
	public void setEstatusTransfer(String estatusTransfer) {
		this.estatusTransfer = estatusTransfer;
	}
	
	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
