/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanProcesosPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author ooamador
 *
 */
public class BeanResPOACOA extends BeanResBase implements Serializable {
	
	/**
	 * Version
	 */
	private static final long serialVersionUID = 1L;

	/**Clave del proceso.*/
	private String activacion;
	
	/** Fase de la operacion */
	private String fase;
	
	/**Campo no hay registro. */
	private String noHayRetorno;
	
	/**
	 * Propiedad del tipo String que almacena el valor de generar
	 */
	private String generar;
	
	/**Campo cifrado*/
	private String cifrado;

	/**
	 * Devuelve el valor de activacion
	 * @return the activacion
	 */
	public String getActivacion() {
		return activacion;
	}

	/**
	 * Modifica el valor de activacion
	 * @param activacion the activacion to set
	 */
	public void setActivacion(String activacion) {
		this.activacion = activacion;
	}

	/**
	 * Devuelve el valor de fase
	 * @return the fase
	 */
	public String getFase() {
		return fase;
	}

	/**
	 * Modifica el valor de fase
	 * @param fase the fase to set
	 */
	public void setFase(String fase) {
		this.fase = fase;
	}

	/**
	 * Devuelve el valor de noHayRetorno
	 * @return the noHayRetorno
	 */
	public String getNoHayRetorno() {
		return noHayRetorno;
	}

	/**
	 * Modifica el valor de noHayRetorno
	 * @param noHayRetorno the noHayRetorno to set
	 */
	public void setNoHayRetorno(String noHayRetorno) {
		this.noHayRetorno = noHayRetorno;
	}

	/**
	 * Devuelve el valor de cifrado
	 * @return the cifrado
	 */
	public String getCifrado() {
		return cifrado;
	}

	/**
	 * Modifica el valor de cifrado
	 * @param cifrado the cifrado to set
	 */
	public void setCifrado(String cifrado) {
		this.cifrado = cifrado;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad generar
	 * @return String Objeto de tipo @see String
	 */
	public String getGenerar() {
		return generar;
	}

	/**
	 * Metodo que modifica el valor de la propiedad generar
	 * @param generar Objeto de tipo @see String
	 */
	public void setGenerar(String generar) {
		this.generar = generar;
	}
	
	
}
