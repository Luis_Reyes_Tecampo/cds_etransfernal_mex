/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanListasMttoMedios.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:37:20 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */


package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;


/**
 * Class BeanListasMttoMedios.
 * Clase que mapea los datos de los combos cuando se da de alta un nuevo mantenimiento de medios
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanListasMttoMedios implements Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8210496583100814289L;
	
	
	/** La variable que contiene informacion con respecto a: list medios entrega. */
	private List<BeanCatalogo> listMediosEntrega;
	
	/** La variable que contiene informacion con respecto a: list transferencia. */
	private List<BeanCatalogo> listTransferencia;
	
	
	/** La variable que contiene informacion con respecto a: list operaciones. */
	private List<BeanCatalogo> listOperaciones;
	
	
	/** La variable que contiene informacion con respecto a: list mecanismo. */
	private List<BeanCatalogo> listMecanismo;
	
	
	/** La variable que contiene informacion con respecto a: list horario. */
	private List<BeanCatalogo> listHorario;
	
	
	/** La variable que contiene informacion con respecto a: list in hab. */
	private List<BeanCatalogo> listInHab;
	
	
	/** La variable que contiene informacion con respecto a: list canal. */
	private List<BeanCatalogo> listCanal;


	/**
	 * Obtener el objeto: list medios entrega.
	 *
	 * @return El objeto: list medios entrega
	 */
	public List<BeanCatalogo> getListMediosEntrega() {
		return listMediosEntrega;
	}


	/**
	 * Definir el objeto: list medios entrega.
	 *
	 * @param listMediosEntrega El nuevo objeto: list medios entrega
	 */
	public void setListMediosEntrega(List<BeanCatalogo> listMediosEntrega) {
		this.listMediosEntrega = listMediosEntrega;
	}


	/**
	 * Obtener el objeto: list transferencia.
	 *
	 * @return El objeto: list transferencia
	 */
	public List<BeanCatalogo> getListTransferencia() {
		return listTransferencia;
	}


	/**
	 * Definir el objeto: list transferencia.
	 *
	 * @param listTransferencia El nuevo objeto: list transferencia
	 */
	public void setListTransferencia(List<BeanCatalogo> listTransferencia) {
		this.listTransferencia = listTransferencia;
	}


	/**
	 * Obtener el objeto: list operaciones.
	 *
	 * @return El objeto: list operaciones
	 */
	public List<BeanCatalogo> getListOperaciones() {
		return listOperaciones;
	}


	/**
	 * Definir el objeto: list operaciones.
	 *
	 * @param listOperaciones El nuevo objeto: list operaciones
	 */
	public void setListOperaciones(List<BeanCatalogo> listOperaciones) {
		this.listOperaciones = listOperaciones;
	}


	/**
	 * Obtener el objeto: list mecanismo.
	 *
	 * @return El objeto: list mecanismo
	 */
	public List<BeanCatalogo> getListMecanismo() {
		return listMecanismo;
	}


	/**
	 * Definir el objeto: list mecanismo.
	 *
	 * @param listMecanismo El nuevo objeto: list mecanismo
	 */
	public void setListMecanismo(List<BeanCatalogo> listMecanismo) {
		this.listMecanismo = listMecanismo;
	}


	/**
	 * Obtener el objeto: list horario.
	 *
	 * @return El objeto: list horario
	 */
	public List<BeanCatalogo> getListHorario() {
		return listHorario;
	}


	/**
	 * Definir el objeto: list horario.
	 *
	 * @param listHorario El nuevo objeto: list horario
	 */
	public void setListHorario(List<BeanCatalogo> listHorario) {
		this.listHorario = listHorario;
	}


	/**
	 * Obtener el objeto: list in hab.
	 *
	 * @return El objeto: list in hab
	 */
	public List<BeanCatalogo> getListInHab() {
		return listInHab;
	}


	/**
	 * Definir el objeto: list in hab.
	 *
	 * @param listInHab El nuevo objeto: list in hab
	 */
	public void setListInHab(List<BeanCatalogo> listInHab) {
		this.listInHab = listInHab;
	}


	/**
	 * Obtener el objeto: list canal.
	 *
	 * @return El objeto: list canal
	 */
	public List<BeanCatalogo> getListCanal() {
		return listCanal;
	}


	/**
	 * Definir el objeto: list canal.
	 *
	 * @param listCanal El nuevo objeto: list canal
	 */
	public void setListCanal(List<BeanCatalogo> listCanal) {
		this.listCanal = listCanal;
	}


	/**
	 * Obtener el objeto: serialversionuid.
	 *
	 * @return El objeto: serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
