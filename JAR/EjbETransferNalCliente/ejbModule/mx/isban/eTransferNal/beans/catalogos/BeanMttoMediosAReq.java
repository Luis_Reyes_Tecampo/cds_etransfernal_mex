/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMttoMediosAReq.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:29:48 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * Class BeanMttoMediosAReq.
 *clase que mapea los tado principales de un manteniiento de medios
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanMttoMediosAReq  implements Serializable{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 262787872260464006L;

	/** La variable que contiene informacion con respecto a: flg in hab. */
	private String flgInHab;
	
	/** La variable que contiene informacion con respecto a: cve CLACONTEF. */
	private String cveCLACONTEF;
	
	/** La variable que contiene informacion con respecto a: cve mecanismo. */
	private String cveMecanismo;
	
	/** La variable que contiene informacion con respecto a: limite importe. */
	private String limiteImporte;
	
	/** La variable que contiene informacion con respecto a: canal. */
	private String canal;
	
	/** La variable que contiene informacion con respecto a: desc operacion. */
	private String descOperacion;
	
	/** La variable que contiene informacion con respecto a: desc trasferencia. */
	private String descTrasferencia;
	
	/** La variable que contiene informacion con respecto a: version. */
	private String version;
	
	/** La variable que contiene informacion con respecto a: accion. */
	private String accion;
	
	
	
	

	
	/**
	 * Obtener el objeto: cve CLACONTEF.
	 *
	 * @return El objeto: cve CLACONTEF
	 */
	public String getCveCLACONTEF() {
		return cveCLACONTEF;
	}

	
	/**
	 * Definir el objeto: cve CLACONTEF.
	 *
	 * @param cveCLACONTEF El nuevo objeto: cve CLACONTEF
	 */
	public void setCveCLACONTEF(String cveCLACONTEF) {
		this.cveCLACONTEF = cveCLACONTEF;
	}

	
	/**
	 * Obtener el objeto: cve mecanismo.
	 *
	 * @return El objeto: cve mecanismo
	 */
	public String getCveMecanismo() {
		return cveMecanismo;
	}

	
	/**
	 * Definir el objeto: cve mecanismo.
	 *
	 * @param cveMecanismo El nuevo objeto: cve mecanismo
	 */
	public void setCveMecanismo(String cveMecanismo) {
		this.cveMecanismo = cveMecanismo;
	}

	
	

	
	/**
	 * Obtener el objeto: canal.
	 *
	 * @return El objeto: canal
	 */
	public String getCanal() {
		return canal;
	}

	
	/**
	 * Definir el objeto: canal.
	 *
	 * @param canal El nuevo objeto: canal
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	
	/**
	 * Obtener el objeto: desc operacion.
	 *
	 * @return El objeto: desc operacion
	 */
	public String getDescOperacion() {
		return descOperacion;
	}

	
	/**
	 * Definir el objeto: desc operacion.
	 *
	 * @param descOperacion El nuevo objeto: desc operacion
	 */
	public void setDescOperacion(String descOperacion) {
		this.descOperacion = descOperacion;
	}

	/**
	 * Obtener el objeto: desc trasferencia.
	 *
	 * @return El objeto: desc trasferencia
	 */
	public String getDescTrasferencia() {
		return descTrasferencia;
	}

	
	/**
	 * Definir el objeto: desc trasferencia.
	 *
	 * @param descTrasferencia El nuevo objeto: desc trasferencia
	 */
	public void setDescTrasferencia(String descTrasferencia) {
		this.descTrasferencia = descTrasferencia;
	}

	
	/**
	 * Obtener el objeto: version.
	 *
	 * @return El objeto: version
	 */
	public String getVersion() {
		return version;
	}

	
	/**
	 * Definir el objeto: version.
	 *
	 * @param version El nuevo objeto: version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Obtener el objeto: accion.
	 *
	 * @return El objeto: accion
	 */
	public String getAccion() {
		return accion;
	}

	
	/**
	 * Definir el objeto: accion.
	 *
	 * @param accion El nuevo objeto: accion
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}


	/**
	 * getFlgInHab de tipo String.
	 * @author FSW-Vector
	 * @return flgInHab de tipo String
	 */
	public String getFlgInHab() {
		return flgInHab;
	}


	/**
	 * setFlgInHab para asignar valor a flgInHab.
	 * @author FSW-Vector
	 * @param flgInHab de tipo String
	 */
	public void setFlgInHab(String flgInHab) {
		this.flgInHab = flgInHab;
	}


	/**
	 * getLimiteImporte de tipo String.
	 * @author FSW-Vector
	 * @return limiteImporte de tipo String
	 */
	public String getLimiteImporte() {
		return limiteImporte;
	}


	/**
	 * setLimiteImporte para asignar valor a limiteImporte.
	 * @author FSW-Vector
	 * @param limiteImporte de tipo String
	 */
	public void setLimiteImporte(String limiteImporte) {
		this.limiteImporte = limiteImporte;
	}


	/**
	 * getSerialversionuid de tipo long.
	 * @author FSW-Vector
	 * @return serialversionuid de tipo long
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	
	
	
}
