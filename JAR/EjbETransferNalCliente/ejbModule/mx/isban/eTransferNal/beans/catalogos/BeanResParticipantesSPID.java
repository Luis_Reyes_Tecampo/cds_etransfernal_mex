/**
 * Isban Mexico
 * Clase: BeanResParticipantesSPID.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 09/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
public class BeanResParticipantesSPID extends BeanResBase implements Serializable{

	/**  Propiedad del tipo long que almacena el valor de serialVersionUID*/
	private static final long serialVersionUID = 3360228784658757368L;
	
	/**  Propiedad del tipo List<BeanParticipantesSPID> que almacena el valor de listParticipantes*/
	private List<BeanParticipantesSPID> listParticipantes;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador beanPaginador = null;
	
	/**
	 * @return Valor del atributo: listParticipantes
	 */
	public List<BeanParticipantesSPID> getListParticipantes() {
		return listParticipantes;
	}

	/**
	 * @param listParticipantes Valor a establecer en atributo: listParticipantes
	 */
	public void setListParticipantes(List<BeanParticipantesSPID> listParticipantes) {
		this.listParticipantes = listParticipantes;
	}
	
	/**
	 * Metodo get que obtiene el total de registros
	 * @return Integer objeto con el numero total de registros
	 */   
	public Integer getTotalReg() {
		return totalReg;
	}

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad beanPaginador
	 * @return beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Metodo que modifica el valor de la propiedad beanPaginador
	 * @param beanPaginador Objeto de tipo @see BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	
}
