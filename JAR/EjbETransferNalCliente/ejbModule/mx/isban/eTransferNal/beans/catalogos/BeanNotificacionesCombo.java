/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanNotificacionesCombo.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 02:31:12 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Class BeanNotificacionesCombo.
 *
 * Clase que almacena los atributos de las listas que
 * seran utilizadas por los combos de alta y edicion.
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
public class BeanNotificacionesCombo implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2773259093990771464L;

	/** La variable que contiene informacion con respecto a: notificadores. */
	@NotNull
	private List<BeanCatalogo> notificadores;
	
	/** La variable que contiene informacion con respecto a: trasferencias. */
	@NotNull
	private List<BeanCatalogo> trasferencias;
	
	/** La variable que contiene informacion con respecto a: medios entrega. */
	@NotNull
	private List<BeanCatalogo> mediosEntrega;
	
	/** La variable que contiene informacion con respecto a: estatus. */
	@NotNull
	private List<BeanCatalogo> estatus;
	
	/**
	 * Obtener el objeto: notificadores.
	 *
	 * @return El objeto: notificadores
	 */
	public List<BeanCatalogo> getNotificadores() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.notificadores;
		return listAux;
	}
	
	/**
	 * Definir el objeto: notificadores.
	 *
	 * @param notificadores El nuevo objeto: notificadores
	 */
	public void setNotificadores(List<BeanCatalogo> notificadores) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = notificadores;
		this.notificadores = listAux;
	}
	
	/**
	 * Obtener el objeto: trasferencias.
	 *
	 * @return El objeto: trasferencias
	 */
	public List<BeanCatalogo> getTrasferencias() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.trasferencias;
		return listAux;
	}
	
	/**
	 * Definir el objeto: trasferencias.
	 *
	 * @param trasferencias El nuevo objeto: trasferencias
	 */
	public void setTrasferencias(List<BeanCatalogo> trasferencias) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = trasferencias;
		this.trasferencias = listAux;
	}
	
	/**
	 * Obtener el objeto: medios entrega.
	 *
	 * @return El objeto: medios entrega
	 */
	public List<BeanCatalogo> getMediosEntrega() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.mediosEntrega;
		return listAux;
	}
	
	/**
	 * Definir el objeto: medios entrega.
	 *
	 * @param mediosEntrega El nuevo objeto: medios entrega
	 */
	public void setMediosEntrega(List<BeanCatalogo> mediosEntrega) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = mediosEntrega;
		this.mediosEntrega = listAux;
	}
	
	/**
	 * Obtener el objeto: estatus.
	 *
	 * @return El objeto: estatus
	 */
	public List<BeanCatalogo> getEstatus() {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = this.estatus;
		return listAux;
	}
	
	/**
	 * Definir el objeto: estatus.
	 *
	 * @param estatus El nuevo objeto: estatus
	 */
	public void setEstatus(List<BeanCatalogo> estatus) {
		List<BeanCatalogo> listAux = new ArrayList<BeanCatalogo>();
		listAux = estatus;
		this.estatus = listAux;
	}
	
}
