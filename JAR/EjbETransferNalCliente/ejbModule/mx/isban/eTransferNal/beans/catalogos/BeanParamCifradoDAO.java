package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanParamCifradoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -7572496502385060696L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cifrado
	 */
	private String cifrado;

	/**
	 * Metodo get que sirve para obtener la propiedad cifrado
	 * @return cifrado Objeto del tipo String
	 */
	public String getCifrado() {
		return cifrado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cifrado
	 * @param cifrado del tipo String
	 */
	public void setCifrado(String cifrado) {
		this.cifrado = cifrado;
	}
	
	

}
