/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanNotificaciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:36:12 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanNotificaciones.
 *
 * Clase que contiene los atributos y metodos de acceso para 
 * el objeto que contiene los datos utilizados por los flujos.
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
public class BeanNotificaciones implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -870950181963430203L;

	/** La variable que contiene informacion con respecto a: notificaciones. */
	@NotNull
	private List<BeanNotificacion> notificaciones;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	@NotNull
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	@NotNull
	private BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	@NotNull
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	private int totalReg = 0;

	/**
	 * Obtener el objeto: notificaciones.
	 *
	 * @return El objeto: notificaciones
	 */
	public List<BeanNotificacion> getNotificaciones() {
		List<BeanNotificacion> listAux = new ArrayList<BeanNotificacion>();
		listAux = this.notificaciones;
		return listAux;
	}

	/**
	 * Definir el objeto: notificaciones.
	 *
	 * @param notificaciones El nuevo objeto: notificaciones
	 */
	public void setNotificaciones(List<BeanNotificacion> notificaciones) {
		List<BeanNotificacion> listAux = new ArrayList<BeanNotificacion>();
		listAux = notificaciones;
		this.notificaciones = listAux;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
	
	
}
