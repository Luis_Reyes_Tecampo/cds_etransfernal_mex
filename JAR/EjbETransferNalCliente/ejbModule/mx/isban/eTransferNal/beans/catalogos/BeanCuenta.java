/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCuenta.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 12:36:37 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 * Class BeanCuenta.
 *
 * Clase tipo Bean que almacena los atributos
 * que se utilizaran en los flujos del catalogo.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
public class BeanCuenta implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8929194745294384261L;

	/** La variable que contiene informacion con respecto a: cuenta. */
	@Size(min=20,max=20)
	private String cuenta;

	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	private boolean seleccionado;
	/**
	 * Nueva instancia bean cuenta.
	 */
	public BeanCuenta() {
		super();
	}

	/**
	 * Obtener el objeto: cuenta.
	 *
	 * @return El objeto: cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * Definir el objeto: cuenta.
	 *
	 * @param cuenta El nuevo objeto: cuenta
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
