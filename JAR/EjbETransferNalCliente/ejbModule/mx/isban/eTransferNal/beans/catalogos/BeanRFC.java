/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanRFC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:39:27 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



/**
 * Class BeanRFC.
 *
 * Clase tipo Bean que almacena los atributos
 * que se utilizaran en los flujos de este
 * catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public class BeanRFC implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1795640752506436224L;
	
	/** La variable que contiene informacion con respecto a: rfc. */
	@NotNull
	@Size(min=18)
	private String rfc;

	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;
	/**
	 * Nueva instancia bean RFC.
	 */
	public BeanRFC() {
		super();
	}

	/**
	 * Obtener el objeto: rfc.
	 *
	 * @return El objeto: rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * Definir el objeto: rfc.
	 *
	 * @param rfc El nuevo objeto: rfc
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
}
