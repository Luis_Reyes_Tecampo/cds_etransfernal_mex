/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsInstDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Sep 18 10:39:58 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de las instituciones
**/

public class BeanResConsInstDAO extends BeanResBase implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**Propiedad que almacena la lista de registros de instituciones*/
	private List<BeanInstitucion> listBeanInstitucion;

	 /**
	 * Metodo get que obtiene el valor de la propiedad listBeanInstitucion
	 * @return listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public List<BeanInstitucion> getListBeanInstitucion() {
		return listBeanInstitucion;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanInstitucion
	 * @param listBeanInstitucion Objeto de tipo @see List<BeanInstitucion>
	 */
	public void setListBeanInstitucion(List<BeanInstitucion> listBeanInstitucion) {
		this.listBeanInstitucion = listBeanInstitucion;
	}
	/**
   * Metodo get que obtiene el total de registros
   * @return Integer objeto con el numero total de registros
   */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}



}
