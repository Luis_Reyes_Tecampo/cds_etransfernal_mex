/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCtaFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   14/09/2019 02:01:59 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
/**
 * Class BeanCtaFideicomiso.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
public class BeanCtaFideicomiso implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1795640752506436224L;
	
	/** La variable que contiene informacion con respecto a: rfc. */
	@NotNull
	private String cuenta;

	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	private boolean seleccionado;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	@NotNull
	private int totalReg = 0;
	
	/**
	 * Nueva instancia bean cta fideicomiso.
	 */
	public BeanCtaFideicomiso() {
		super();
	}
	
	/**
	 * Obtener el objeto: cuenta.
	 *
	 * @return El objeto: cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	
	/**
	 * Definir el objeto: cuenta.
	 *
	 * @param cuenta El nuevo objeto: cuenta
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}

	
	
	
}
