package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

 /**
 * The Class BeanLiquidezIntradiaExportar.
 */
public class BeanLiquidezIntradiaExportar implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6862274506296527144L;
	/** The liquidez exportar. */
	@Size(min=0,max=120)
	private List<BeanLiquidezIntradia> liquidezExportarE;

	/**
	 * Gets the liquidez exportar E.
	 *
	 * @return the liquidez exportar E
	 */
	public List<BeanLiquidezIntradia> getLiquidezExportarE() {
		List<BeanLiquidezIntradia> listAux = new ArrayList<BeanLiquidezIntradia>();
		listAux = this.liquidezExportarE;
		return listAux;
	}

	/**
	 * Sets the liquidez exportar E.
	 *
	 * @param liquidezExportarE the new liquidez exportar E
	 */
	public void setLiquidezExportarE(List<BeanLiquidezIntradia> liquidezExportarE) {
		List<BeanLiquidezIntradia> listAux = new ArrayList<BeanLiquidezIntradia>();
		listAux = liquidezExportarE;
		this.liquidezExportarE = listAux;
	}
 
	
	

}
