/**
 * 
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
/**
 * The Class BeanLiquidezIntradia.
 *
 * @author vector
 */
public class BeanLiquidezIntradia implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6161679009542913666L;

	/** The cve trans fe. */
	@NotNull
	private String cveTransFe;
	
	/** The estatus. */
	@NotNull
	private String estatus;
	
	/** The cve mecanismo. */
	@NotNull
	private String cveMecanismo;
	
	/** The cve operacion. */
	@NotNull
	private String cveOperacion;

	/** The tipo accion. */
	@NotNull
	private String tipoAccion;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	private boolean seleccionado;
	
	
	/**
	 * Checks if is seleccionado.
	 *
	 * @return true, if is seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Sets the seleccionado.
	 *
	 * @param seleccionado the new seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	/**
	 * Gets the tipo accion.
	 *
	 * @return the tipo accion
	 */
	public String getTipoAccion() {
		return tipoAccion;
	}

	/**
	 * Sets the tipo accion.
	 *
	 * @param tipoAccion the new tipo accion
	 */
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	/**
	 * Gets the cve trans fe.
	 *
	 * @return the cve trans fe
	 */
	public String getCveTransFe() {
		return cveTransFe;
	}

	/**
	 * Sets the cve trans fe.
	 *
	 * @param cveTransFe the new cve trans fe
	 */
	public void setCveTransFe(String cveTransFe) {
		this.cveTransFe = cveTransFe;
	}

	/**
	 * Gets the estatus.
	 *
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Sets the estatus.
	 *
	 * @param estatus the new estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Gets the cve mecanismo.
	 *
	 * @return the cve mecanismo
	 */
	public String getCveMecanismo() {
		return cveMecanismo;
	}

	/**
	 * Sets the cve mecanismo.
	 *
	 * @param cveMecanismo the new cve mecanismo
	 */
	public void setCveMecanismo(String cveMecanismo) {
		this.cveMecanismo = cveMecanismo;
	}

	/**
	 * Gets the cve operacion.
	 *
	 * @return the cve operacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}

	/**
	 * Sets the cve operacion.
	 *
	 * @param cveOperacion the new cve operacion
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	
}
