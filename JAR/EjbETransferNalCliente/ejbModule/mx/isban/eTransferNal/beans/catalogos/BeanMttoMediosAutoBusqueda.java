/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMttoMediosAutoBusqueda.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:32:02 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.comunes.BeanOrdenamiento;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 * Class BeanMttoMediosAutoBusqueda.
 * clase que guarda el paginador y los filtros para  la busqueda para la pantalla de mantemientos de medios
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanMttoMediosAutoBusqueda implements Serializable{

	
	

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: sort field. */
	private String sortField;
	
	/** La variable que contiene informacion con respecto a: sort type. */
	private String sortType;
	
	/** La variable que contiene informacion con respecto a: filtro. */
	private String filtro;
	
	/** La variable que contiene informacion con respecto a: dto. */
	private String dto;	
	
	/** La variable que contiene informacion con respecto a: bean ordenamiento. */
	private BeanOrdenamiento beanOrdenamiento;
	
	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	
	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
	/**
	 * Obtener el objeto: sort field.
	 *
	 * @return El objeto: sort field
	 */
	public String getSortField() {
		return sortField;
	}
	
	/**
	 * Definir el objeto: sort field.
	 *
	 * @param sortField El nuevo objeto: sort field
	 */
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	
	/**
	 * Obtener el objeto: sort type.
	 *
	 * @return El objeto: sort type
	 */
	public String getSortType() {
		return sortType;
	}
	
	/**
	 * Definir el objeto: sort type.
	 *
	 * @param sortType El nuevo objeto: sort type
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	
	/**
	 * Obtener el objeto: filtro.
	 *
	 * @return El objeto: filtro
	 */
	public String getFiltro() {
		return filtro;
	}
	
	/**
	 * Definir el objeto: filtro.
	 *
	 * @param filtro El nuevo objeto: filtro
	 */
	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}
	
	/**
	 * Obtener el objeto: dto.
	 *
	 * @return El objeto: dto
	 */
	public String getDto() {
		return dto;
	}
	
	/**
	 * Definir el objeto: dto.
	 *
	 * @param dto El nuevo objeto: dto
	 */
	public void setDto(String dto) {
		this.dto = dto;
	}
	
	/**
	 * Obtener el objeto: bean ordenamiento.
	 *
	 * @return El objeto: bean ordenamiento
	 */
	public BeanOrdenamiento getBeanOrdenamiento() {
		return beanOrdenamiento;
	}
	
	/**
	 * Definir el objeto: bean ordenamiento.
	 *
	 * @param beanOrdenamiento El nuevo objeto: bean ordenamiento
	 */
	public void setBeanOrdenamiento(BeanOrdenamiento beanOrdenamiento) {
		this.beanOrdenamiento = beanOrdenamiento;
	}
	
	
	
	
}
