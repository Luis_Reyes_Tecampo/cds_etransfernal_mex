/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCombosCertificadoCifrado.java
 * 
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 12:09:55 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanCombosCertificadoCifrado.
 * 
 * Se utiliza para el llenado de los cobos en la patalla de 
 * alta de certificados y mantenimiento
 * 
 * Mapea los catalogos utilizados
 *
 * @author FSW-Vector
 * @since 12/09/2018
 */
public class BeanCombosCertificadoCifrado implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3752238076727671155L;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	private BeanResBase beanError;

	/** La variable que contiene informacion con respecto a: lista canales. */
	private List<BeanCatalogo> listaCanales;
	
	/** La variable que contiene informacion con respecto a: lista firmas. */
	private List<BeanCatalogo> listaFirmas;
	
	/** La variable que contiene informacion con respecto a: lista cifrados. */
	private List<BeanCatalogo> listaCifrados;
	
	/** La variable que contiene informacion con respecto a: lista tecnologias. */
	private List<BeanCatalogo> listaTecnologias;

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: lista canales.
	 *
	 * @return El objeto: lista canales
	 */
	public List<BeanCatalogo> getListaCanales() {
		return listaCanales;
	}

	/**
	 * Definir el objeto: lista canales.
	 *
	 * @param listaCanales El nuevo objeto: lista canales
	 */
	public void setListaCanales(List<BeanCatalogo> listaCanales) {
		this.listaCanales = listaCanales;
	}

	/**
	 * Obtener el objeto: lista firmas.
	 *
	 * @return El objeto: lista firmas
	 */
	public List<BeanCatalogo> getListaFirmas() {
		return listaFirmas;
	}

	/**
	 * Definir el objeto: lista firmas.
	 *
	 * @param listaFirmas El nuevo objeto: lista firmas
	 */
	public void setListaFirmas(List<BeanCatalogo> listaFirmas) {
		this.listaFirmas = listaFirmas;
	}

	/**
	 * Obtener el objeto: lista cifrados.
	 *
	 * @return El objeto: lista cifrados
	 */
	public List<BeanCatalogo> getListaCifrados() {
		return listaCifrados;
	}

	/**
	 * Definir el objeto: lista cifrados.
	 *
	 * @param listaCifrados El nuevo objeto: lista cifrados
	 */
	public void setListaCifrados(List<BeanCatalogo> listaCifrados) {
		this.listaCifrados = listaCifrados;
	}

	/**
	 * Obtener el objeto: lista tecnologias.
	 *
	 * @return El objeto: lista tecnologias
	 */
	public List<BeanCatalogo> getListaTecnologias() {
		return listaTecnologias;
	}

	/**
	 * Definir el objeto: lista tecnologias.
	 *
	 * @param listaTecnologias El nuevo objeto: lista tecnologias
	 */
	public void setListaTecnologias(List<BeanCatalogo> listaTecnologias) {
		this.listaTecnologias = listaTecnologias;
	}

}
