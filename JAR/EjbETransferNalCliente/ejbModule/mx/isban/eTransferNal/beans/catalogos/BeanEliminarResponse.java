/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanEliminarResponse.java
 * 
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   12/09/2018 12:08:50 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanEliminarResponse.
 * 
 * Se utiliza para la respuesta de eliminacion
 * 
 * Se informan los registros eliminados de manera correcat
 * e incorrecta
 *
 * @author FSW-Vector
 * @since 12/09/2018
 */
public class BeanEliminarResponse extends BeanResBase {

	/** Constante privada del Serial version. */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/** Propiedad del tipo String que almacena el valor de eliminadosErroneos. */
	private String eliminadosErroneos;
	
	/** Propiedad del tipo String que almacena el valor de eliminadosCorrectos. */
	private String eliminadosCorrectos;

	/**
	 * Metodo get que obtiene el valor de la propiedad eliminadosErroneos.
	 *
	 * @return String Objeto de tipo @see String
	 */
	public String getEliminadosErroneos() {
		return eliminadosErroneos;
	}

	/**
	 * Metodo que modifica el valor de la propiedad eliminadosErroneos.
	 *
	 * @param eliminadosErroneos Objeto de tipo @see String
	 */
	public void setEliminadosErroneos(String eliminadosErroneos) {
		this.eliminadosErroneos = eliminadosErroneos;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad eliminadosCorrectos.
	 *
	 * @return String Objeto de tipo @see String
	 */
	public String getEliminadosCorrectos() {
		return eliminadosCorrectos;
	}

	/**
	 * Metodo que modifica el valor de la propiedad eliminadosCorrectos.
	 *
	 * @param eliminadosCorrectos Objeto de tipo @see String
	 */
	public void setEliminadosCorrectos(String eliminadosCorrectos) {
		this.eliminadosCorrectos = eliminadosCorrectos;
	}
}
