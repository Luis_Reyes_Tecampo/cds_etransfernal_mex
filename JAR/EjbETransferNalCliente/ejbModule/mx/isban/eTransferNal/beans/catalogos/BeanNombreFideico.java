/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanNombreFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:41:29 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * Class BeanNombreFideico.
 *
 * Clase tipo Bean que almacena los atributos
 * que se utilizaran en los flujos de este
 * catalogo.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
public class BeanNombreFideico implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5361558616733303419L;
	
	/** La variable que contiene informacion con respecto a: nombre. */
	@NotNull
	private String nombre;

	/** La variable que contiene informacion con respecto a: seleccionado. */
	@NotNull
	private boolean seleccionado;
	
	/**
	 * Nueva instancia bean nombre fideico.
	 */
	public BeanNombreFideico() {
		super();
	}

	/**
	 * Obtener el objeto: nombre.
	 *
	 * @return El objeto: nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Definir el objeto: nombre.
	 *
	 * @param nombre El nuevo objeto: nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
