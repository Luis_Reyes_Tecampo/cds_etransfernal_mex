/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanProcesosPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * @author ooamador
 *
 */
public class BeanProcesosPOACOA implements Serializable {
	
	/**
	 * Version
	 */
	private static final long serialVersionUID = 1L;

	/**Fecha de operacion.*/
	private String fchOperacion;
	
	/**Clave de institucion.*/
	private String cveInstitucion; 
	
	/**Clave del proceso.*/
	private String cveProceso;
	
	/** Fase de la operacion */
	private String fase;
	
	/**
	 * Propiedad del tipo String que almacena el valor de generar
	 */
	private String generar;
	
	/**
	 * Propiedad del tipo String que almacena el valor de liqFinal
	 */
	private String liqFinal;

	/**
	 * Devuelve el valor de fchOperacion
	 * @return the fchOperacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Modifica el valor de fchOperacion
	 * @param fchOperacion the fchOperacion to set
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Devuelve el valor de cveInstitucion
	 * @return the cveInstitucion
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}

	/**
	 * Modifica el valor de cveInstitucion
	 * @param cveInstitucion the cveInstitucion to set
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}

	/**
	 * Devuelve el valor de cveProceso
	 * @return the cveProceso
	 */
	public String getCveProceso() {
		return cveProceso;
	}

	/**
	 * Modifica el valor de cveProceso
	 * @param cveProceso the cveProceso to set
	 */
	public void setCveProceso(String cveProceso) {
		this.cveProceso = cveProceso;
	}

	/**
	 * Devuelve el valor de fase
	 * @return the fase
	 */
	public String getFase() {
		return fase;
	}

	/**
	 * Modifica el valor de fase
	 * @param fase the fase to set
	 */
	public void setFase(String fase) {
		this.fase = fase;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad generar
	 * @return String Objeto de tipo @see String
	 */
	public String getGenerar() {
		return generar;
	}

	/**
	 * Metodo que modifica el valor de la propiedad generar
	 * @param generar Objeto de tipo @see String
	 */
	public void setGenerar(String generar) {
		this.generar = generar;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad liqFinal
	 * @return String Objeto de tipo @see String
	 */
	public String getLiqFinal() {
		return liqFinal;
	}

	/**
	 * Metodo que modifica el valor de la propiedad liqFinal
	 * @param liqFinal Objeto de tipo @see String
	 */
	public void setLiqFinal(String liqFinal) {
		this.liqFinal = liqFinal;
	}

}
