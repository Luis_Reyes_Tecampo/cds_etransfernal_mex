/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCertificadoCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:15:52 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

/**
 * Class BeanCertificadoCifrado.
 * Mapea los datos de la pantalla de alta de certificados
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanCertificadoCifrado implements Serializable {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5380843947967534181L;

	/** La variable que contiene informacion con respecto a: canal. */
	private String canal;
	
	/** La variable que contiene informacion con respecto a: alias. */
	private String alias;
	
	/** La variable que contiene informacion con respecto a: num certificado. */
	private String numCertificado;
	
	/** La variable que contiene informacion con respecto a: fch vencimiento. */
	private String fchVencimiento;
	
	/** La variable que contiene informacion con respecto a: alg digest. */
	private String algDigest;
	
	/** La variable que contiene informacion con respecto a: alg simetrico. */
	private String algSimetrico;
	
	/** La variable que contiene informacion con respecto a: tec origen. */
	private String tecOrigen;
	
	/** La variable que contiene informacion con respecto a: estatus. */
	private String estatus;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;

	/**
	 * Obtener el objeto: canal.
	 *
	 * @return El objeto: canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * Definir el objeto: canal.
	 *
	 * @param canal El nuevo objeto: canal
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * Obtener el objeto: alias.
	 *
	 * @return El objeto: alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Definir el objeto: alias.
	 *
	 * @param alias El nuevo objeto: alias
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Obtener el objeto: num certificado.
	 *
	 * @return El objeto: num certificado
	 */
	public String getNumCertificado() {
		return numCertificado;
	}

	/**
	 * Definir el objeto: num certificado.
	 *
	 * @param numCertificado El nuevo objeto: num certificado
	 */
	public void setNumCertificado(String numCertificado) {
		this.numCertificado = numCertificado;
	}

	/**
	 * Obtener el objeto: fch vencimiento.
	 *
	 * @return El objeto: fch vencimiento
	 */
	public String getFchVencimiento() {
		return fchVencimiento;
	}

	/**
	 * Definir el objeto: fch vencimiento.
	 *
	 * @param fchVencimiento El nuevo objeto: fch vencimiento
	 */
	public void setFchVencimiento(String fchVencimiento) {
		this.fchVencimiento = fchVencimiento;
	}

	/**
	 * Obtener el objeto: alg digest.
	 *
	 * @return El objeto: alg digest
	 */
	public String getAlgDigest() {
		return algDigest;
	}

	/**
	 * Definir el objeto: alg digest.
	 *
	 * @param algDigest El nuevo objeto: alg digest
	 */
	public void setAlgDigest(String algDigest) {
		this.algDigest = algDigest;
	}

	/**
	 * Obtener el objeto: alg simetrico.
	 *
	 * @return El objeto: alg simetrico
	 */
	public String getAlgSimetrico() {
		return algSimetrico;
	}

	/**
	 * Definir el objeto: alg simetrico.
	 *
	 * @param algSimetrico El nuevo objeto: alg simetrico
	 */
	public void setAlgSimetrico(String algSimetrico) {
		this.algSimetrico = algSimetrico;
	}

	/**
	 * Obtener el objeto: tec origen.
	 *
	 * @return El objeto: tec origen
	 */
	public String getTecOrigen() {
		return tecOrigen;
	}

	/**
	 * Definir el objeto: tec origen.
	 *
	 * @param tecOrigen El nuevo objeto: tec origen
	 */
	public void setTecOrigen(String tecOrigen) {
		this.tecOrigen = tecOrigen;
	}

	/**
	 * Obtener el objeto: estatus.
	 *
	 * @return El objeto: estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Definir el objeto: estatus.
	 *
	 * @param estatus El nuevo objeto: estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
