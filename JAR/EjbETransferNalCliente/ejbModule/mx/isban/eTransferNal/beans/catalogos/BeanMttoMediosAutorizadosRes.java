/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMttoMediosAutorizadosRes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   11/09/2018 06:33:01 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;




/**
 * Class BeanMttoMediosAutorizadosRes.
 * Clase de respuesta de la pantalla de manteimiento de medios
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
public class BeanMttoMediosAutorizadosRes extends BeanListasMttoMedios implements BeanResultBO, Serializable{
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -4113966076482447249L;
	
	/** La variable que contiene informacion con respecto a: lista bean mtto med auto. */
	private List<BeanMttoMediosAutorizadosReq> listaBeanMttoMedAuto;
	
	/** La variable que contiene informacion con respecto a: cod error. */
	private String codError;
	
	/** La variable que contiene informacion con respecto a: msg error. */
	private String msgError;
	
	/** La variable que contiene informacion con respecto a: tipo error. */
	private String tipoError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	private Integer totalReg = 0;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: existe. */
	private boolean existe;
	
	/** La variable que contiene informacion con respecto a: filtros. */
	private boolean filtros;
	
	/** La variable que contiene informacion con respecto a: nombre archivo. */
	private String nombreArchivo;
	
	
	/** La variable que contiene informacion con respecto a: bean mtto medios auto req. */
	private BeanMttoMediosAutorizadosReq beanMttoMediosAutoReq;
	
	/**
	 * Obtener el objeto: nombre archivo.
	 *
	 * @return El objeto: nombre archivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Definir el objeto: nombre archivo.
	 *
	 * @param nombreArchivo El nuevo objeto: nombre archivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Obtener el objeto: lista bean mtto med auto.
	 *
	 * @return El objeto: lista bean mtto med auto
	 */
	public List<BeanMttoMediosAutorizadosReq> getListaBeanMttoMedAuto() {
		return listaBeanMttoMedAuto;
	}

	/**
	 * Definir el objeto: lista bean mtto med auto.
	 *
	 * @param listaBeanMttoMedAuto El nuevo objeto: lista bean mtto med auto
	 */
	public void setListaBeanMttoMedAuto(List<BeanMttoMediosAutorizadosReq> listaBeanMttoMedAuto) {
		this.listaBeanMttoMedAuto = listaBeanMttoMedAuto;
	}

	/* (sin Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getCodError()
	 */
	public String getCodError() {
		return codError;
	}

	/* (sin Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setCodError(java.lang.String)
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/* (sin Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getMsgError()
	 */
	public String getMsgError() {
		return msgError;
	}

	/* (sin Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setMsgError(java.lang.String)
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Obtener el objeto: tipo error.
	 *
	 * @return El objeto: tipo error
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * Definir el objeto: tipo error.
	 *
	 * @param tipoError El nuevo objeto: tipo error
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Verificar si existe.
	 *
	 * @return true, si existe
	 */
	public boolean isExiste() {
		return existe;
	}

	/**
	 * Definir el objeto: existe.
	 *
	 * @param existe El nuevo objeto: existe
	 */
	public void setExiste(boolean existe) {
		this.existe = existe;
	}

	/**
	 * Verificar si filtros.
	 *
	 * @return true, si filtros
	 */
	public boolean isFiltros() {
		return filtros;
	}

	/**
	 * Definir el objeto: filtros.
	 *
	 * @param filtros El nuevo objeto: filtros
	 */
	public void setFiltros(boolean filtros) {
		this.filtros = filtros;
	}

	/**
	 * Obtener el objeto: bean mtto medios auto req.
	 *
	 * @return El objeto: bean mtto medios auto req
	 */
	public BeanMttoMediosAutorizadosReq getBeanMttoMediosAutoReq() {
		return beanMttoMediosAutoReq;
	}

	
	/**
	 * Definir el objeto: bean mtto medios auto req.
	 *
	 * @param beanMttoMediosAutoReq El nuevo objeto: bean mtto medios auto req
	 */
	public void setBeanMttoMediosAutoReq(BeanMttoMediosAutorizadosReq beanMttoMediosAutoReq) {
		this.beanMttoMediosAutoReq = beanMttoMediosAutoReq;
	}

	
	
	
	
}
