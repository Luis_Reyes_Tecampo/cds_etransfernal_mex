/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanExcepNomBenef.java
 * 
 * Control de versiones:
 * Version      Date/Hour 				 	  By 	              Company 	   Description
 * ------- ------------------------   -------------------------- ---------- ----------------------
 * 1.0      19/09/2019 11:25:47 PM     Uriel Alfredo Botello R.    VSF        Creacion
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


/**
 * Class BeanExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 18/09/2019
 */
public class BeanExcepNomBenef implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4633082158560827256L;
	
	/** La variable que contiene informacion con respecto a: lista cuentas. */
	@Size(min=0,max=120)
	private List<BeanNomBeneficiario> listaNombres;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	@NotNull
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	@NotNull
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	@NotNull
	private BeanNomBeneficiario beanFilter;
	
	/**
	 * Nueva instancia bean excep nom benef.
	 */
	public BeanExcepNomBenef() {
		super();
		this.beanError = new BeanResBase();
		this.beanPaginador = new BeanPaginador();
		this.beanFilter = new BeanNomBeneficiario();
	}



	/**
	 * Obtener el objeto: lista nombres.
	 *
	 * @return El objeto: lista nombres
	 */
	public List<BeanNomBeneficiario> getListaNombres() {
		List<BeanNomBeneficiario> listAux = new ArrayList<BeanNomBeneficiario>();
		listAux = this.listaNombres;
		return listAux;
	}

	/**
	 * Definir el objeto: lista nombres.
	 *
	 * @param listaNombres El nuevo objeto: lista nombres
	 */
	public void setListaNombres(List<BeanNomBeneficiario> listaNombres) {
		List<BeanNomBeneficiario> listAux = new ArrayList<BeanNomBeneficiario>();
		listAux = listaNombres;
		this.listaNombres = listAux;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanNomBeneficiario beanFilter) {
		this.beanFilter = beanFilter;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanNomBeneficiario getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}


}
