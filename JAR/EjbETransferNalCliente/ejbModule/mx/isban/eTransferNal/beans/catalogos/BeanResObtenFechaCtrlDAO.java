package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResObtenFechaCtrlDAO extends BeanResBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1652878698116391995L;

	/**
	 * Propiedad del tipo String que almacena el valor de fecha
	 */
	private String fecha;

	/**
	 * Metodo get que sirve para obtener la propiedad fecha
	 * @return fecha Objeto del tipo String
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fecha
	 * @param fecha del tipo String
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
