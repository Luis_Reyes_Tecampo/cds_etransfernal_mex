/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGuardaBitacoraDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 22 23:20:04 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *Clase Bean DTO que se encarga de encapsular la respuesta de guardar
 * la bitacora
**/

public class BeanResGuardaInstDAO extends BeanResBase implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;

}
