/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsInstPOACOADAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Sep 18 10:39:58 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 *Clase Bean DTO que se encarga de encapsular los valores de regreso
 * de las instituciones
**/

public class BeanResConsInstPOACOADAO extends BeanResBase implements Serializable {
	
	 /**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 8798833859305882110L;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**Propiedad que almacena la lista de registros de instituciones*/
	private List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA;

   /**
	 * Metodo get que obtiene el valor de la propiedad listBeanInstitucionPOACOA
	 * @return listBeanInstitucionPOACOA Objeto de tipo @see List<BeanInstitucionPOACOA>
	 */
	public List<BeanInstitucionPOACOA> getListBeanInstitucionPOACOA() {
		return listBeanInstitucionPOACOA;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanInstitucionPOACOA
	 * @param listBeanInstitucionPOACOA Objeto de tipo @see List<BeanInstitucionPOACOA>
	 */
	public void setListBeanInstitucionPOACOA(
			List<BeanInstitucionPOACOA> listBeanInstitucionPOACOA) {
		this.listBeanInstitucionPOACOA = listBeanInstitucionPOACOA;
	}
  /**
   * Metodo get que obtiene el total de registros
   * @return Integer objeto con el numero total de registros
   */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}



}
