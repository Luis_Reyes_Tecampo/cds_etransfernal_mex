/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOMantenimientoParametros.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   18/05/2017     Vector 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanResMantenimientoParametrosDAO.
 */
public class BeanResMantenimientoParametrosDAO extends BeanResBase implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 5731568632592379840L;
	
	/** The total reg. */
	private int totalReg=0;

	/** The lista parametros. */
	private List<BeanMantenimientoParametros> listaParametros = Collections.emptyList();

	/** The datos parametro. */
	private List<BeanMantenimientoParametros> datosParametro = Collections.emptyList();
	
	/** The lista parametros. */
	private List<BeanMantenimientoParametros> opcionesTipoParam = Collections.emptyList();
	
	/** The lista parametros. */
	private List<BeanMantenimientoParametros> opcionesTipoDato = Collections.emptyList();

	/**
	 * Gets the total reg.
	 *
	 * @return the total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Sets the total reg.
	 *
	 * @param totalReg the new total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Gets the lista parametros.
	 *
	 * @return the lista parametros
	 */
	public List<BeanMantenimientoParametros> getListaParametros() {
		return listaParametros;
	}

	/**
	 * Sets the lista parametros.
	 *
	 * @param listaParametros the new lista parametros
	 */
	public void setListaParametros(List<BeanMantenimientoParametros> listaParametros) {
		this.listaParametros = listaParametros;
	}
	
	/**
	 * Gets the datos parametro.
	 *
	 * @return the datos parametro
	 */
	public List<BeanMantenimientoParametros> getDatosParametro() {
		return datosParametro;
	}

	/**
	 * Sets the datos parametro.
	 *
	 * @param datosParametro the new datos parametro
	 */
	public void setDatosParametro(List<BeanMantenimientoParametros> datosParametro) {
		this.datosParametro = datosParametro;
	}

	/**
	 * Gets the opciones tipo param.
	 *
	 * @return the opciones tipo param
	 */
	public List<BeanMantenimientoParametros> getOpcionesTipoParam() {
		return opcionesTipoParam;
	}

	/**
	 * Sets the opciones tipo param.
	 *
	 * @param opcionesTipoParam the new opciones tipo param
	 */
	public void setOpcionesTipoParam(
			List<BeanMantenimientoParametros> opcionesTipoParam) {
		this.opcionesTipoParam = opcionesTipoParam;
	}

	/**
	 * Gets the opciones tipo dato.
	 *
	 * @return the opciones tipo dato
	 */
	public List<BeanMantenimientoParametros> getOpcionesTipoDato() {
		return opcionesTipoDato;
	}

	/**
	 * Sets the opciones tipo dato.
	 *
	 * @param opcionesTipoDato the new opciones tipo dato
	 */
	public void setOpcionesTipoDato(
			List<BeanMantenimientoParametros> opcionesTipoDato) {
		this.opcionesTipoDato = opcionesTipoDato;
	}
}