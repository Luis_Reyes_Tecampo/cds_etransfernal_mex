/**
 * 
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanLiquidezIntradiaReq.
 *
 * @author vector
 */
public class BeanLiquidezIntradiaReq implements Serializable {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7863556973564692261L;

	/** The lista bean liquidez intradia. */
	@Size(min=0,max=120)
	private List<BeanLiquidezIntradia> listaBeanLiquidezIntradia;

	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	@NotNull
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	@NotNull
	private BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	@NotNull
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	@NotNull
	private int totalReg = 0;

	/**
	 * Gets the lista bean liquidez intradia.
	 *
	 * @return the lista bean liquidez intradia
	 */
	public List<BeanLiquidezIntradia> getListaBeanLiquidezIntradia() {
		List<BeanLiquidezIntradia> listAux = new ArrayList<BeanLiquidezIntradia>();
		listAux = this.listaBeanLiquidezIntradia;
		return listAux;
	}

	/**
	 * Sets the lista bean liquidez intradia.
	 *
	 * @param listaBeanLiquidezIntradia the new lista bean liquidez intradia
	 */
	public void setListaBeanLiquidezIntradia(List<BeanLiquidezIntradia> listaBeanLiquidezIntradia) {
		List<BeanLiquidezIntradia> listAux = new ArrayList<BeanLiquidezIntradia>();
		listAux = listaBeanLiquidezIntradia;
		this.listaBeanLiquidezIntradia = listAux;
	}

	

	/**
	 * Gets the bean paginador.
	 *
	 * @return the bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Sets the bean paginador.
	 *
	 * @param beanPaginador the new bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Gets the bean filter.
	 *
	 * @return the bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Sets the bean filter.
	 *
	 * @param beanFilter the new bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}

	/**
	 * Gets the bean error.
	 *
	 * @return the bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Sets the bean error.
	 *
	 * @param beanError the new bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}

	/**
	 * Gets the total reg.
	 *
	 * @return the total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Sets the total reg.
	 *
	 * @param totalReg the new total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}
	
}
