/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanLlaveCarga.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     12/09/2019 12:24:41 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * Class BeanLlaveCarga.
 * 
 * Clase para guardar el id del catalogo actual
 * 
 * @author FSW-Vector
 * @since 12/09/2019
 */
public class BeanLlaveCarga implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1963664280050456990L;

	/** La variable que contiene informacion con respecto a: id cat. */
	@NotNull
	private Integer idCat;
	
	/** La variable que contiene informacion con respecto a: ruta. */
	@NotNull
	private String ruta;
	
	/** La variable que contiene informacion con respecto a: nombre. */
	@NotNull
	private String nombreCat;
	
	
	
	/**
	 * Obtener el objeto: id cat.
	 *
	 * @return El objeto: id cat
	 */
	public Integer getIdCat() {
		return idCat;
	}

	/**
	 * Definir el objeto: id cat.
	 *
	 * @param idCat El nuevo objeto: id cat
	 */
	public void setIdCat(Integer idCat) {
		this.idCat = idCat;
	}

	/**
	 * Obtener el objeto: ruta.
	 *
	 * @return El objeto: ruta
	 */
	public String getRuta() {
		return ruta;
	}
	
	/**
	 * Definir el objeto: ruta.
	 *
	 * @param ruta El nuevo objeto: ruta
	 */
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	/**
	 * Obtener el objeto: nombre cat.
	 *
	 * @return El objeto: nombre cat
	 */
	public String getNombreCat() {
		return nombreCat;
	}

	/**
	 * Definir el objeto: nombre cat.
	 *
	 * @param nombreCat El nuevo objeto: nombre cat
	 */
	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}

}
