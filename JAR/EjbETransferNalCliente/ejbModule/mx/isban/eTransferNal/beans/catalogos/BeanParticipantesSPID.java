/**
 * Isban Mexico
 * Clase: BeanParticipantesSPID.java
 * Descripcion: 
 * 
 * Control de Cambios
 * 1.0 09/12/2016 Rosa Martinez Rivera.
 */
package mx.isban.eTransferNal.beans.catalogos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author Vector SF
 * @version 1.0
 *
 */
public class BeanParticipantesSPID extends BeanResBase{

	/** Propiedad del tipo long que almacena el valor de serialVersionUID */
	private static final long serialVersionUID = -5394488617985219183L;
	
	
	/**  Propiedad del tipo String que almacena el valor de cveIntermediario*/
	private String cveIntermediario;
	
	
	/**  Propiedad del tipo String que almacena el valor de numBanxico*/
	private String numBanxico;
	
	/**  Propiedad del tipo String que almacena el valor de nombre*/
	private String nombre;
	
	/**  Propiedad del tipo String que almacena el valor de fecha*/
	private String fecha;

	/**
	 * @return Valor del atributo: cveIntermediario
	 */
	public String getCveIntermediario() {
		return cveIntermediario;
	}

	/**
	 * @param cveIntermediario Valor a establecer en atributo: cveIntermediario
	 */
	public void setCveIntermediario(String cveIntermediario) {
		this.cveIntermediario = cveIntermediario;
	}

	/**
	 * @return Valor del atributo: numBanxico
	 */
	public String getNumBanxico() {
		return numBanxico;
	}

	/**
	 * @param numBanxico Valor a establecer en atributo: numBanxico
	 */
	public void setNumBanxico(String numBanxico) {
		this.numBanxico = numBanxico;
	}

	/**
	 * @return Valor del atributo: nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre Valor a establecer en atributo: nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return Valor del atributo: fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha Valor a establecer en atributo: fecha
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	

}
