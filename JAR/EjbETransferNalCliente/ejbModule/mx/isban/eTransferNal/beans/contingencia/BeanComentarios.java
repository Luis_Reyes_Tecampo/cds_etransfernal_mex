/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanComentarios.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/10/2018 06:37:47 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeanComentarios.
 * 
 * Clase que contiene los atributos de comentarios
 *
 * @author FSW-Vector
 * @since 10/10/2018
 */
public class BeanComentarios implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3766288948508614655L;

	/** La variable que contiene informacion con respecto a: comentario 1. */
	private String comentario1;
	
	/** La variable que contiene informacion con respecto a: comentario 2. */
	private String comentario2;
	
	/** La variable que contiene informacion con respecto a: comentario 3. */
	private String comentario3;
	
	/** La variable que contiene informacion con respecto a: comentario 4. */
	private String comentario4;
	
	/**
	 * Obtener el objeto: comentario 3.
	 *
	 * @return El objeto: comentario 3
	 */
	public String getComentario3() {
		return comentario3;
	}
	
	/**
	 * Definir el objeto: comentario 3.
	 *
	 * @param comentario3 El nuevo objeto: comentario 3
	 */
	public void setComentario3(String comentario3) {
		this.comentario3 = comentario3;
	}
	
	/**
	 * Obtener el objeto: comentario 1.
	 *
	 * @return El objeto: comentario 1
	 */
	public String getComentario1() {
		return comentario1;
	}
	
	/**
	 * Definir el objeto: comentario 1.
	 *
	 * @param comentario1 El nuevo objeto: comentario 1
	 */
	public void setComentario1(String comentario1) {
		this.comentario1 = comentario1;
	}
	
	/**
	 * Obtener el objeto: comentario 4.
	 *
	 * @return El objeto: comentario 4
	 */
	public String getComentario4() {
		return comentario4;
	}
	
	/**
	 * Definir el objeto: comentario 4.
	 *
	 * @param comentario4 El nuevo objeto: comentario 4
	 */
	public void setComentario4(String comentario4) {
		this.comentario4 = comentario4;
	}
	/**
	 * Obtener el objeto: comentario 2.
	 *
	 * @return El objeto: comentario 2
	 */
	public String getComentario2() {
		return comentario2;
	}
	
	/**
	 * Definir el objeto: comentario 2.
	 *
	 * @param comentario2 El nuevo objeto: comentario 2
	 */
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	
}
