/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeandDatosGenerales.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 02:01:11 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeandDatosGenerales.
 *
 * Clase que alamcena los atributos generales de la tabla TRAN_MENSAJE_CANALES.	
 * @author FSW-Vector
 * @since 9/10/2018
 */
public class BeanDatosGenerales implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1739875718308352543L;
	
	/** La variable que contiene informacion con respecto a: nombre arch. */
	private String nombreArch;
	
	/** La variable que contiene informacion con respecto a: secuencia. */
	private String secuencia;
	
	/** La variable que contiene informacion con respecto a: referencia med. */
	private String referenciaMed;
	
	/** La variable que contiene informacion con respecto a: importe. */
	private String importe;
	
	/** La variable que contiene informacion con respecto a: concepto pago 2. */
	private String conceptoPago2;
	
	/** La variable que contiene informacion con respecto a: fch cap transfer. */
	private String fchCapTransfer;
	
	/** La variable que contiene informacion con respecto a: refe transfer. */
	private String refeTransfer;
	
	/** La variable que contiene informacion con respecto a: bean datos generales ext. */
	private BeanDatosGeneralesExt beanDatosGeneralesExt;
	
	/**
	 * Obtener el objeto: nombre arch.
	 *
	 * @return El objeto: nombre arch
	 */
	public String getNombreArch() {
		return nombreArch;
	}
	
	/**
	 * Definir el objeto: nombre arch.
	 *
	 * @param nombreArch El nuevo objeto: nombre arch
	 */
	public void setNombreArch(String nombreArch) {
		this.nombreArch = nombreArch;
	}
	
	/**
	 * Obtener el objeto: secuencia.
	 *
	 * @return El objeto: secuencia
	 */
	public String getSecuencia() {
		return secuencia;
	}
	
	/**
	 * Definir el objeto: secuencia.
	 *
	 * @param secuencia El nuevo objeto: secuencia
	 */
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	
	/**
	 * Obtener el objeto: referencia med.
	 *
	 * @return El objeto: referencia med
	 */
	public String getReferenciaMed() {
		return referenciaMed;
	}
	
	/**
	 * Definir el objeto: referencia med.
	 *
	 * @param referenciaMed El nuevo objeto: referencia med
	 */
	public void setReferenciaMed(String referenciaMed) {
		this.referenciaMed = referenciaMed;
	}
	
	/**
	 * Obtener el objeto: importe.
	 *
	 * @return El objeto: importe
	 */
	public String getImporte() {
		return importe;
	}
	
	/**
	 * Definir el objeto: importe.
	 *
	 * @param importe El nuevo objeto: importe
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
	
	/**
	 * Obtener el objeto: concepto pago 2.
	 *
	 * @return El objeto: concepto pago 2
	 */
	public String getConceptoPago2() {
		return conceptoPago2;
	}
	
	/**
	 * Definir el objeto: concepto pago 2.
	 *
	 * @param conceptoPago2 El nuevo objeto: concepto pago 2
	 */
	public void setConceptoPago2(String conceptoPago2) {
		this.conceptoPago2 = conceptoPago2;
	}
	
	/**
	 * Obtener el objeto: fch cap transfer.
	 *
	 * @return El objeto: fch cap transfer
	 */
	public String getFchCapTransfer() {
		return fchCapTransfer;
	}
	
	/**
	 * Definir el objeto: fch cap transfer.
	 *
	 * @param fchCapTransfer El nuevo objeto: fch cap transfer
	 */
	public void setFchCapTransfer(String fchCapTransfer) {
		this.fchCapTransfer = fchCapTransfer;
	}
	
	/**
	 * Obtener el objeto: refe transfer.
	 *
	 * @return El objeto: refe transfer
	 */
	public String getRefeTransfer() {
		return refeTransfer;
	}
	
	/**
	 * Definir el objeto: refe transfer.
	 *
	 * @param refeTransfer El nuevo objeto: refe transfer
	 */
	public void setRefeTransfer(String refeTransfer) {
		this.refeTransfer = refeTransfer;
	}

	/**
	 * Obtener el objeto: bean datos generales ext.
	 *
	 * @return El objeto: bean datos generales ext
	 */
	public BeanDatosGeneralesExt getBeanDatosGeneralesExt() {
		return beanDatosGeneralesExt;
	}

	/**
	 * Definir el objeto: bean datos generales ext.
	 *
	 * @param beanDatosGeneralesExt El nuevo objeto: bean datos generales ext
	 */
	public void setBeanDatosGeneralesExt(BeanDatosGeneralesExt beanDatosGeneralesExt) {
		this.beanDatosGeneralesExt = beanDatosGeneralesExt;
	}
	
}
