/**
 * 
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;


/**
 * The Class BeanResConsCanales.
 *
 * @author vector
 */
public class BeanResConsCanalesD extends BeanResBase implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The bean res canal list. */
	private List<BeanCanales> beanResCanalList = null;

	/**
	 * Gets the bean res canal list.
	 *
	 * @return the bean res canal list
	 */
	public List<BeanCanales> getBeanResCanalList() {
		return beanResCanalList;
	}

	/**
	 * Sets the bean res canal list.
	 *
	 * @param beanResCanalList the new bean res canal list
	 */
	public void setBeanResCanalList(List<BeanCanales> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}
	
}
