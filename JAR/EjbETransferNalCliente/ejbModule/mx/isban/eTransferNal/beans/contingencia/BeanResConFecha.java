
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanResConFecha.
 */
public class BeanResConFecha extends BeanResBase implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	   /** Propiedad privada del tipo String que almacena la fecha de operacion. */
	   private String fechaOperacion;
	
	   
	   
	/**
	 * Gets the fecha operacion.
	 *
	 * @return the fecha operacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	
	/**
	 * Sets the fecha operacion.
	 *
	 * @param fechaOperacion the new fecha operacion
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	
	
}
