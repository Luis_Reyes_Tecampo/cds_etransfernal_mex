/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTranMensajeContig.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 02:03:23 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;


/**
 * Class BeanTranMensajeContig.
 * 
 * Clase que almacena los Bean que contienen los todos los atributos de la tabla TRAN_MENSAJE_CONTIG.
 * 
 * @author FSW-Vector
 * @since 9/10/2018
 */
public class BeanTranMensajeCanales implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6338334506481091555L;

	/** La variable que contiene informacion con respecto a: bean datos ordenante. */
	private BeanDatosOrdenante beanDatosOrdenante;
	
	/** La variable que contiene informacion con respecto a: bean datos receptor. */
	private BeanDatosReceptor beanDatosReceptor;
	
	/** La variable que contiene informacion con respecto a: bean claves. */
	private BeanClaves beanClaves;
	
	/** La variable que contiene informacion con respecto a: bean comentarios. */
	private BeanComentarios beanComentarios;
	
	/** La variable que contiene informacion con respecto a: bean datos operacion. */
	private BeanDatosOperacion beanDatosOperacion;
	
	/** La variable que contiene informacion con respecto a: bean datos generales. */
	private BeanDatosGenerales beanDatosGenerales;
	
	/**
	 * Obtener el objeto: bean datos ordenante.
	 *
	 * @return El objeto: bean datos ordenante
	 */
	public BeanDatosOrdenante getBeanDatosOrdenante() {
		return beanDatosOrdenante;
	}
	
	/**
	 * Definir el objeto: bean datos ordenante.
	 *
	 * @param beanDatosOrdenante El nuevo objeto: bean datos ordenante
	 */
	public void setBeanDatosOrdenante(BeanDatosOrdenante beanDatosOrdenante) {
		this.beanDatosOrdenante = beanDatosOrdenante;
	}
	
	/**
	 * Obtener el objeto: bean datos receptor.
	 *
	 * @return El objeto: bean datos receptor
	 */
	public BeanDatosReceptor getBeanDatosReceptor() {
		return beanDatosReceptor;
	}
	
	/**
	 * Definir el objeto: bean datos receptor.
	 *
	 * @param beanDatosReceptor El nuevo objeto: bean datos receptor
	 */
	public void setBeanDatosReceptor(BeanDatosReceptor beanDatosReceptor) {
		this.beanDatosReceptor = beanDatosReceptor;
	}
	
	/**
	 * Obtener el objeto: bean claves.
	 *
	 * @return El objeto: bean claves
	 */
	public BeanClaves getBeanClaves() {
		return beanClaves;
	}
	
	/**
	 * Definir el objeto: bean claves.
	 *
	 * @param beanClaves El nuevo objeto: bean claves
	 */
	public void setBeanClaves(BeanClaves beanClaves) {
		this.beanClaves = beanClaves;
	}
	
	/**
	 * Obtener el objeto: bean comentarios.
	 *
	 * @return El objeto: bean comentarios
	 */
	public BeanComentarios getBeanComentarios() {
		return beanComentarios;
	}
	
	/**
	 * Definir el objeto: bean comentarios.
	 *
	 * @param beanComentarios El nuevo objeto: bean comentarios
	 */
	public void setBeanComentarios(BeanComentarios beanComentarios) {
		this.beanComentarios = beanComentarios;
	}
	
	/**
	 * Obtener el objeto: bean datos operacion.
	 *
	 * @return El objeto: bean datos operacion
	 */
	public BeanDatosOperacion getBeanDatosOperacion() {
		return beanDatosOperacion;
	}
	
	/**
	 * Definir el objeto: bean datos operacion.
	 *
	 * @param beanDatosOperacion El nuevo objeto: bean datos operacion
	 */
	public void setBeanDatosOperacion(BeanDatosOperacion beanDatosOperacion) {
		this.beanDatosOperacion = beanDatosOperacion;
	}
	
	/**
	 * Obtener el objeto: bean datos generales.
	 *
	 * @return El objeto: bean datos generales
	 */
	public BeanDatosGenerales getBeanDatosGenerales() {
		return beanDatosGenerales;
	}
	
	/**
	 * Definir el objeto: bean datos generales.
	 *
	 * @param beanDatosGenerales El nuevo objeto: bean datos generales
	 */
	public void setBeanDatosGenerales(BeanDatosGenerales beanDatosGenerales) {
		this.beanDatosGenerales = beanDatosGenerales;
	}
	
}
