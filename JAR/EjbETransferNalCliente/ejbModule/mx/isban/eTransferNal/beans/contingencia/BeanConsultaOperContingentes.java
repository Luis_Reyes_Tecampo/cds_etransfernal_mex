package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanConsultaOperContingentes.
 *
 * @author FSW-Vector
 * @since 25/10/2018
 */
public class BeanConsultaOperContingentes extends BeanResBase implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8107695201132434038L;

	/** The nombre archivo. */
	private String nombreArchivo;
	
	/** The estatus. */
	private String estatus;
	
	/** The num pagos. */
	private String numPagos;
	
	/** The num pagos err. */
	private String numPagosErr;
	
	/** The total monto. */
	private String totalMonto;
	
	/** The total monto err. */
	private String totalMontoErr;
	
	/** The cve usuario alta. */
	private String cveUsuarioAlta;
	
	/** The fch captura. */
	private String fchCaptura;
	
	/** The fch proceso. */
	private String fchProceso;
	
	/** The mensaje. */
	private String mensaje;
	
	/** The cve medio ent. */
	private String cveMedioEnt;
	
	/** The descripcion. */
	private String descripcion;

	/** The fch operacion. */
	private String fchOperacion;
	
	/** La variable que contiene informacion con respecto a: seleccionado. */
	private boolean seleccionado;

	/**
	 * Gets the fch operacion.
	 *
	 * @return the fch operacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Sets the fch operacion.
	 *
	 * @param fchOperacion the new fch operacion
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Gets the nombre archivo.
	 *
	 * @return the nombre archivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Sets the nombre archivo.
	 *
	 * @param nombreArchivo the new nombre archivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Gets the estatus.
	 *
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Sets the estatus.
	 *
	 * @param estatus the new estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Gets the num pagos.
	 *
	 * @return the num pagos
	 */
	public String getNumPagos() {
		return numPagos;
	}

	/**
	 * Sets the num pagos.
	 *
	 * @param numPagos the new num pagos
	 */
	public void setNumPagos(String numPagos) {
		this.numPagos = numPagos;
	}

	/**
	 * Gets the num pagos err.
	 *
	 * @return the num pagos err
	 */
	public String getNumPagosErr() {
		return numPagosErr;
	}

	/**
	 * Sets the num pagos err.
	 *
	 * @param numPagosErr the new num pagos err
	 */
	public void setNumPagosErr(String numPagosErr) {
		this.numPagosErr = numPagosErr;
	}

	/**
	 * Gets the total monto.
	 *
	 * @return the total monto
	 */
	public String getTotalMonto() {
		return totalMonto;
	}

	/**
	 * Sets the total monto.
	 *
	 * @param totalMonto the new total monto
	 */
	public void setTotalMonto(String totalMonto) {
		this.totalMonto = totalMonto;
	}

	/**
	 * Gets the total monto err.
	 *
	 * @return the total monto err
	 */
	public String getTotalMontoErr() {
		return totalMontoErr;
	}

	/**
	 * Sets the total monto err.
	 *
	 * @param totalMontoErr the new total monto err
	 */
	public void setTotalMontoErr(String totalMontoErr) {
		this.totalMontoErr = totalMontoErr;
	}

	/**
	 * Gets the cve usuario alta.
	 *
	 * @return the cve usuario alta
	 */
	public String getCveUsuarioAlta() {
		return cveUsuarioAlta;
	}

	/**
	 * Sets the cve usuario alta.
	 *
	 * @param cveUsuarioAlta the new cve usuario alta
	 */
	public void setCveUsuarioAlta(String cveUsuarioAlta) {
		this.cveUsuarioAlta = cveUsuarioAlta;
	}

	/**
	 * Gets the fch captura.
	 *
	 * @return the fch captura
	 */
	public String getFchCaptura() {
		return fchCaptura;
	}

	/**
	 * Sets the fch captura.
	 *
	 * @param fchCaptura the new fch captura
	 */
	public void setFchCaptura(String fchCaptura) {
		this.fchCaptura = fchCaptura;
	}

	/**
	 * Gets the fch proceso.
	 *
	 * @return the fch proceso
	 */
	public String getFchProceso() {
		return fchProceso;
	}

	/**
	 * Sets the fch proceso.
	 *
	 * @param fchProceso the new fch proceso
	 */
	public void setFchProceso(String fchProceso) {
		this.fchProceso = fchProceso;
	}

	/**
	 * Gets the mensaje.
	 *
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Sets the mensaje.
	 *
	 * @param mensaje the new mensaje
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * Gets the cve medio ent.
	 *
	 * @return the cve medio ent
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}

	/**
	 * Sets the cve medio ent.
	 *
	 * @param cveMedioEnt the new cve medio ent
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Verificar si seleccionado.
	 *
	 * @return true, si seleccionado
	 */
	public boolean isSeleccionado() {
		return seleccionado;
	}

	/**
	 * Definir el objeto: seleccionado.
	 *
	 * @param seleccionado El nuevo objeto: seleccionado
	 */
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
}
