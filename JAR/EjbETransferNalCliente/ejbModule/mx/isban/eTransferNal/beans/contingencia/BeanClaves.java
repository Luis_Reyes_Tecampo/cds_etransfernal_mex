/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanClaves.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 01:47:06 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeanClaves.
 * 
 * Clase que contiene los atributos de Clave utilizados en las operaciones para la tabla TRAN_MENSAJE_CONTIG.
 * 
 * @author FSW-Vector
 * @since 9/10/2018
 */
public class BeanClaves implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8944062778847118559L;

	/** La variable que contiene informacion con respecto a: cve empresa. */
	private String cveEmpresa;
	
	/** La variable que contiene informacion con respecto a: cve pto vta. */
	private String cvePtoVta;
	
	/** La variable que contiene informacion con respecto a: cve medio ent. */
	private String cveMedioEnt;
	
	/** La variable que contiene informacion con respecto a: cve transfe. */
	private String cveTransfe;
	
	/** La variable que contiene informacion con respecto a: cve operacion. */
	private String cveOperacion;
	
	/** La variable que contiene informacion con respecto a: cve divisa com. */
	private String cveDivisa;
	
	/** La variable que contiene informacion con respecto a: cve usuario cap. */
	private String cveUsuarioCap;
	
	/** La variable que contiene informacion con respecto a: cve usuario sol. */
	private String cveUsuarioSol;
	
	/**
	 * Obtener el objeto: cve empresa.
	 *
	 * @return El objeto: cve empresa
	 */
	public String getCveEmpresa() {
		return cveEmpresa;
	}
	
	/**
	 * Definir el objeto: cve empresa.
	 *
	 * @param cveEmpresa El nuevo objeto: cve empresa
	 */
	public void setCveEmpresa(String cveEmpresa) {
		this.cveEmpresa = cveEmpresa;
	}
	
	/**
	 * Obtener el objeto: cve pto vta.
	 *
	 * @return El objeto: cve pto vta
	 */
	public String getCvePtoVta() {
		return cvePtoVta;
	}
	
	/**
	 * Definir el objeto: cve pto vta.
	 *
	 * @param cvePtoVta El nuevo objeto: cve pto vta
	 */
	public void setCvePtoVta(String cvePtoVta) {
		this.cvePtoVta = cvePtoVta;
	}
	
	/**
	 * Obtener el objeto: cve medio ent.
	 *
	 * @return El objeto: cve medio ent
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	
	/**
	 * Definir el objeto: cve medio ent.
	 *
	 * @param cveMedioEnt El nuevo objeto: cve medio ent
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	
	/**
	 * Obtener el objeto: cve transfe.
	 *
	 * @return El objeto: cve transfe
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	
	/**
	 * Definir el objeto: cve transfe.
	 *
	 * @param cveTransfe El nuevo objeto: cve transfe
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	
	/**
	 * Obtener el objeto: cve operacion.
	 *
	 * @return El objeto: cve operacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	
	/**
	 * Definir el objeto: cve operacion.
	 *
	 * @param cveOperacion El nuevo objeto: cve operacion
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	
	/**
	 * Obtener el objeto: cve divisa.
	 *
	 * @return El objeto: cve divisa
	 */
	public String getCveDivisa() {
		return cveDivisa;
	}
	
	/**
	 * Definir el objeto: cve divisa.
	 *
	 * @param cveDivisa El nuevo objeto: cve divisa
	 */
	public void setCveDivisa(String cveDivisa) {
		this.cveDivisa = cveDivisa;
	}
	
	/**
	 * Obtener el objeto: cve usuario cap.
	 *
	 * @return El objeto: cve usuario cap
	 */
	public String getCveUsuarioCap() {
		return cveUsuarioCap;
	}
	
	/**
	 * Definir el objeto: cve usuario cap.
	 *
	 * @param cveUsuarioCap El nuevo objeto: cve usuario cap
	 */
	public void setCveUsuarioCap(String cveUsuarioCap) {
		this.cveUsuarioCap = cveUsuarioCap;
	}
	
	/**
	 * Obtener el objeto: cve usuario sol.
	 *
	 * @return El objeto: cve usuario sol
	 */
	public String getCveUsuarioSol() {
		return cveUsuarioSol;
	}
	
	/**
	 * Definir el objeto: cve usuario sol.
	 *
	 * @param cveUsuarioSol El nuevo objeto: cve usuario sol
	 */
	public void setCveUsuarioSol(String cveUsuarioSol) {
		this.cveUsuarioSol = cveUsuarioSol;
	}
	
}
