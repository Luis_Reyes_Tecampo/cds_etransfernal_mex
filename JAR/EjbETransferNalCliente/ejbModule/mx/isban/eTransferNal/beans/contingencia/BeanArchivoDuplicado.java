package mx.isban.eTransferNal.beans.contingencia;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanArchivoDuplicado.
 *
 * @author FSW-Vector
 * @since 25/10/2018
 */
public class BeanArchivoDuplicado extends BeanResBase{

	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La variable que contiene informacion con respecto a: es duplicado. */
	private boolean esDuplicado;

	/**
	 * Metodo get que obtiene el valor de la propiedad esDuplicado.
	 *
	 * @return boolean Objeto de tipo @see boolean
	 */
	public boolean isEsDuplicado() {
		return esDuplicado;
	}

	/**
	 * Metodo que modifica el valor de la propiedad esDuplicado.
	 *
	 * @param esDuplicado Objeto de tipo @see boolean
	 */
	public void setEsDuplicado(boolean esDuplicado) {
		this.esDuplicado = esDuplicado;
	}
}
