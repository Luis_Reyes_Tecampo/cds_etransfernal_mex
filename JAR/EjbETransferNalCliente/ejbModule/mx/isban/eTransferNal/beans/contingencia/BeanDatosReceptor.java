/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanDatosReceptor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 01:22:29 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeanDatosReceptor.
 * 
 * Clase que contiene los atributos para el Receptor.
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
public class BeanDatosReceptor implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3103813240794267651L;

	/** La variable que contiene informacion con respecto a: cve interme rec. */
	private String cveIntermeRec;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec. */
	private String numCuentaRec;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec 2. */
	private String numCuentaRec2;
	
	/** La variable que contiene informacion con respecto a: nombre rec. */
	private String nombreRec;
	
	/** La variable que contiene informacion con respecto a: nombre rec 2. */
	private String nombreRec2;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec. */
	private String tipoCuentaRec;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec 2. */
	private String tipoCuentaRec2;
	
	/** La variable que contiene informacion con respecto a: rfc rec 2. */
	private String rfcRec2;
	
	/** La variable que contiene informacion con respecto a: rfc rec. */
	private String rfcRec;

	/**
	 * Obtener el objeto: cve interme rec.
	 *
	 * @return El objeto: cve interme rec
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}

	/**
	 * Definir el objeto: cve interme rec.
	 *
	 * @param cveIntermeRec El nuevo objeto: cve interme rec
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}

	/**
	 * Obtener el objeto: num cuenta rec.
	 *
	 * @return El objeto: num cuenta rec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Definir el objeto: num cuenta rec.
	 *
	 * @param numCuentaRec El nuevo objeto: num cuenta rec
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Obtener el objeto: num cuenta rec 2.
	 *
	 * @return El objeto: num cuenta rec 2
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}

	/**
	 * Definir el objeto: num cuenta rec 2.
	 *
	 * @param numCuentaRec2 El nuevo objeto: num cuenta rec 2
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}

	/**
	 * Obtener el objeto: nombre rec.
	 *
	 * @return El objeto: nombre rec
	 */
	public String getNombreRec() {
		return nombreRec;
	}

	/**
	 * Definir el objeto: nombre rec.
	 *
	 * @param nombreRec El nuevo objeto: nombre rec
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	/**
	 * Obtener el objeto: nombre rec 2.
	 *
	 * @return El objeto: nombre rec 2
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}

	/**
	 * Definir el objeto: nombre rec 2.
	 *
	 * @param nombreRec2 El nuevo objeto: nombre rec 2
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec.
	 *
	 * @return El objeto: tipo cuenta rec
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}

	/**
	 * Definir el objeto: tipo cuenta rec.
	 *
	 * @param tipoCuentaRec El nuevo objeto: tipo cuenta rec
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec 2.
	 *
	 * @return El objeto: tipo cuenta rec 2
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}

	/**
	 * Definir el objeto: tipo cuenta rec 2.
	 *
	 * @param tipoCuentaRec2 El nuevo objeto: tipo cuenta rec 2
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}

	/**
	 * Obtener el objeto: rfc rec 2.
	 *
	 * @return El objeto: rfc rec 2
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}

	/**
	 * Definir el objeto: rfc rec 2.
	 *
	 * @param rfcRec2 El nuevo objeto: rfc rec 2
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}

	/**
	 * Obtener el objeto: rfc rec.
	 *
	 * @return El objeto: rfc rec
	 */
	public String getRfcRec() {
		return rfcRec;
	}

	/**
	 * Definir el objeto: rfc rec.
	 *
	 * @param rfcRec El nuevo objeto: rfc rec
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}

}
