/**
 * 
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Class BeanCanales.
 *
 * @author vector
 */
public class BeanCanales extends BeanResBase implements Serializable{
	
	/** Serial numero. */
	private static final long serialVersionUID = 1L;

	/** Identificador canal. */
	private String idCanal;
	
	/**  Nombre Canal. */
	private String nombreCanal;
	
	/**  Estatus canal. */
	private boolean estatusCanal;
	
	/**  bandera renglon. */
	private boolean banderaRenglon;

	/**
	 * Gets the id canal.
	 *
	 * @return the id canal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * Sets the id canal.
	 *
	 * @param idCanal the new id canal
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * Gets the nombre canal.
	 *
	 * @return the nombre canal
	 */
	public String getNombreCanal() {
		return nombreCanal;
	}

	/**
	 * Sets the nombre canal.
	 *
	 * @param nombreCanal the new nombre canal
	 */
	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}

	/**
	 * Checks if is estatus canal.
	 *
	 * @return true, if is estatus canal
	 */
	public boolean isEstatusCanal() {
		return estatusCanal;
	}

	/**
	 * Sets the estatus canal.
	 *
	 * @param estatusCanal the new estatus canal
	 */
	public void setEstatusCanal(boolean estatusCanal) {
		this.estatusCanal = estatusCanal;
	}

	/**
	 * Checks if is bandera renglon.
	 *
	 * @return true, if is bandera renglon
	 */
	public boolean isBanderaRenglon() {
		return banderaRenglon;
	}

	/**
	 * Sets the bandera renglon.
	 *
	 * @param banderaRenglon the new bandera renglon
	 */
	public void setBanderaRenglon(boolean banderaRenglon) {
		this.banderaRenglon = banderaRenglon;
	}

	
	
	
}
