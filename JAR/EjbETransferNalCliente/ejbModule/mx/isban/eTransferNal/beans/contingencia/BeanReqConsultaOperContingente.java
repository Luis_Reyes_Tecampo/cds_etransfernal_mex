/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanReqConsultaOperContingente.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/10/2018 12:24:33 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanReqConsultaOperContingente.
 *
 * @author FSW-Vector
 * @since 22/10/2018
 */
public class BeanReqConsultaOperContingente implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** La variable que contiene informacion con respecto a: lista bean res cons oper. */
	private List<BeanConsultaOperContingentes>listaBeanResConsOper ;
	
	/** Propiedad del tipo Integer que almacena el toral de registors. */
	private Integer totalReg = 0;
	
	/** Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar. */
	private BeanPaginador beanPaginador = null;

	/** La variable que contiene informacion con respecto a: beanbase. */
	private BeanResBase beanbase;
	
	/** variable que guarda el nombre de la pantalla*. */
	private String nomPantalla;
	
	/** La variable que contiene informacion con respecto a: fecha operacion. */
	private String fechaOperacion;
	
	/** La variable que contiene informacion con respecto a: historico. */
	private boolean historico;


	/**
	 * Obtener el objeto: lista bean res cons oper.
	 *
	 * @return El objeto: lista bean res cons oper
	 */
	public List<BeanConsultaOperContingentes> getListaBeanResConsOper() {
		return listaBeanResConsOper;
	}

	/**
	 * Definir el objeto: lista bean res cons oper.
	 *
	 * @param listaBeanResConsOper El nuevo objeto: lista bean res cons oper
	 */
	public void setListaBeanResConsOper(List<BeanConsultaOperContingentes> listaBeanResConsOper) {
		this.listaBeanResConsOper = listaBeanResConsOper;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * getNomPantalla de tipo String.
	 * @author FSW-Vector
	 * @return nomPantalla de tipo String
	 */
	public String getNomPantalla() {
		return nomPantalla;
	}

	/**
	 * setNomPantalla para asignar valor a nomPantalla.
	 * @author FSW-Vector
	 * @param nomPantalla de tipo String
	 */
	public void setNomPantalla(String nomPantalla) {
		this.nomPantalla = nomPantalla;
	}
	
	/**
	 * Obtener el objeto: beanbase.
	 *
	 * @return El objeto: beanbase
	 */
	public BeanResBase getBeanbase() {
		return beanbase;
	}

	/**
	 * Definir el objeto: beanbase.
	 *
	 * @param beanbase El nuevo objeto: beanbase
	 */
	public void setBeanbase(BeanResBase beanbase) {
		this.beanbase = beanbase;
	}

	/**
	 * getFechaOperacion de tipo String.
	 * @author FSW-Vector
	 * @return fechaOperacion de tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * setFechaOperacion para asignar valor a fechaOperacion.
	 * @author FSW-Vector
	 * @param fechaOperacion de tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * isHistorico de tipo boolean.
	 * @author FSW-Vector
	 * @return historico de tipo boolean
	 */
	public boolean isHistorico() {
		return historico;
	}

	/**
	 * setHistorico para asignar valor a historico.
	 * @author FSW-Vector
	 * @param historico de tipo boolean
	 */
	public void setHistorico(boolean historico) {
		this.historico = historico;
	}
	
	

}
