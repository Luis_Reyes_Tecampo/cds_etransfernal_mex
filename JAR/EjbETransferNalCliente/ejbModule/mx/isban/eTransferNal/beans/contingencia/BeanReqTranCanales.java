/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResTranMensaje.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/10/2018 01:06:26 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.isban.eTransferNal.beans.comunes.BeanFilter;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanResTranMensaje.
 *
 * Clase que se utilizara para realizar el flujo y alamcenar los valores necesarios
 * 
 * @author FSW-Vector
 * @since 10/10/2018
 */
public class BeanReqTranCanales implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7996704590474304420L;

	/** La variable que contiene informacion con respecto a: registros. */
	private List<BeanTranMensajeCanales> registros;
	
	/** La variable que contiene informacion con respecto a: bean paginador. */
	private BeanPaginador beanPaginador;
	
	/** La variable que contiene informacion con respecto a: bean filter. */
	private BeanFilter beanFilter;
	
	/** La variable que contiene informacion con respecto a: bean error. */
	private BeanResBase beanError;
	
	/** La variable que contiene informacion con respecto a: total reg. */
	private int totalReg = 0;
	
	/** La variable que contiene informacion con respecto a: importe total actual. */
	private BigDecimal importeTotalActual;
	
	/** variable que guarda el nombre de la pantalla*. */
	private String nomPantalla;
	
	/** Propiedad privada del tipo String que almacena la fecha de operacion. */
	private String fechaOperacion;
	   
	/**
	 * Obtener el objeto: registros.
	 *
	 * @return El objeto: registros
	 */
	public List<BeanTranMensajeCanales> getRegistros() {
		return registros;
	}

	/**
	 * Definir el objeto: registros.
	 *
	 * @param registros El nuevo objeto: registros
	 */
	public void setRegistros(List<BeanTranMensajeCanales> registros) {
		this.registros = registros;
	}

	/**
	 * Obtener el objeto: total reg.
	 *
	 * @return El objeto: total reg
	 */
	public int getTotalReg() {
		return totalReg;
	}

	/**
	 * Definir el objeto: total reg.
	 *
	 * @param totalReg El nuevo objeto: total reg
	 */
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Obtener el objeto: bean filter.
	 *
	 * @return El objeto: bean filter
	 */
	public BeanFilter getBeanFilter() {
		return beanFilter;
	}

	/**
	 * Definir el objeto: bean filter.
	 *
	 * @param beanFilter El nuevo objeto: bean filter
	 */
	public void setBeanFilter(BeanFilter beanFilter) {
		this.beanFilter = beanFilter;
	}
	/**
	 * Obtener el objeto: bean paginador.
	 *
	 * @return El objeto: bean paginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Definir el objeto: bean paginador.
	 *
	 * @param beanPaginador El nuevo objeto: bean paginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * Obtener el objeto: bean error.
	 *
	 * @return El objeto: bean error
	 */
	public BeanResBase getBeanError() {
		return beanError;
	}

	/**
	 * Definir el objeto: bean error.
	 *
	 * @param beanError El nuevo objeto: bean error
	 */
	public void setBeanError(BeanResBase beanError) {
		this.beanError = beanError;
	}


	/**
	 * Obtener el objeto: importe total actual.
	 *
	 * @return El objeto: importe total actual
	 */
	public BigDecimal getImporteTotalActual() {
		return importeTotalActual;
	}

	/**
	 * Definir el objeto: importe total actual.
	 *
	 * @param importeTotalActual El nuevo objeto: importe total actual
	 */
	public void setImporteTotalActual(BigDecimal importeTotalActual) {
		this.importeTotalActual = importeTotalActual;
	}

	/**
	 * getNomPantalla de tipo String.
	 * @author FSW-Vector
	 * @return nomPantalla de tipo String
	 */
	public String getNomPantalla() {
		return nomPantalla;
	}

	/**
	 * setNomPantalla para asignar valor a nomPantalla.
	 * @author FSW-Vector
	 * @param nomPantalla de tipo String
	 */
	public void setNomPantalla(String nomPantalla) {
		this.nomPantalla = nomPantalla;
	}

	/**
	 * getFechaOperacion de tipo String.
	 * @author FSW-Vector
	 * @return fechaOperacion de tipo String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * setFechaOperacion para asignar valor a fechaOperacion.
	 * @author FSW-Vector
	 * @param fechaOperacion de tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	
	
}
