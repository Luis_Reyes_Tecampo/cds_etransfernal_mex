/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanDatosOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/10/2018 06:44:39 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeanDatosOperacion.
 * 
 * Clase que contiene los atributos usados para alamcenar los resultados de la oepracion realizada
 *
 * @author FSW-Vector
 * @since 10/10/2018
 */
public class BeanDatosOperacion implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -1249772701390876942L;

	/** La variable que contiene informacion con respecto a: cod error. */
	private String codError;
	
	/** La variable que contiene informacion con respecto a: estatus inicial. */
	private String estatusInicial;
	
	/** La variable que contiene informacion con respecto a: mensaje. */
	private String mensaje;
	
	/**
	 * Obtener el objeto: cod error.
	 *
	 * @return El objeto: cod error
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Definir el objeto: cod error.
	 *
	 * @param codError El nuevo objeto: cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Obtener el objeto: estatus inicial.
	 *
	 * @return El objeto: estatus inicial
	 */
	public String getEstatusInicial() {
		return estatusInicial;
	}
	
	/**
	 * Definir el objeto: estatus inicial.
	 *
	 * @param estatusInicial El nuevo objeto: estatus inicial
	 */
	public void setEstatusInicial(String estatusInicial) {
		this.estatusInicial = estatusInicial;
	}
	
	/**
	 * Obtener el objeto: mensaje.
	 *
	 * @return El objeto: mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}
	
	/**
	 * Definir el objeto: mensaje.
	 *
	 * @param mensaje El nuevo objeto: mensaje
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
