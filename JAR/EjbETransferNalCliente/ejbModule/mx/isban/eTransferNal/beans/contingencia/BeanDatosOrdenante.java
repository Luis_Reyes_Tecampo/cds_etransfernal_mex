/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanDatosOrdenante.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 12:49:28 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;

/**
 * Class BeanDatosOrdenante.
 * 
 * Clase que contiene los atributos necesarios del Ordenante.
 * @author FSW-Vector
 * @since 9/10/2018
 */
public class BeanDatosOrdenante implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 8588130682904787441L;

	/** La variable que contiene informacion con respecto a: cve interme ord. */
	private String cveIntermeOrd;
	
	/** La variable que contiene informacion con respecto a: num cuenta ord. */
	private String numCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: cve pto vta ord. */
	private String cvePtoVtaOrd;
	
	/** La variable que contiene informacion con respecto a: nombre ord. */
	private String nombreOrd;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta ord. */
	private String tipoCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: rfc ord. */
	private String rfcOrd;

	/**
	 * Obtener el objeto: cve interme ord.
	 *
	 * @return El objeto: cve interme ord
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}

	/**
	 * Definir el objeto: cve interme ord.
	 *
	 * @param cveIntermeOrd El nuevo objeto: cve interme ord
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}

	/**
	 * Obtener el objeto: num cuenta ord.
	 *
	 * @return El objeto: num cuenta ord
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}

	/**
	 * Definir el objeto: num cuenta ord.
	 *
	 * @param numCuentaOrd El nuevo objeto: num cuenta ord
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}

	/**
	 * Obtener el objeto: cve pto vta ord.
	 *
	 * @return El objeto: cve pto vta ord
	 */
	public String getCvePtoVtaOrd() {
		return cvePtoVtaOrd;
	}

	/**
	 * Definir el objeto: cve pto vta ord.
	 *
	 * @param cvePtoVtaOrd El nuevo objeto: cve pto vta ord
	 */
	public void setCvePtoVtaOrd(String cvePtoVtaOrd) {
		this.cvePtoVtaOrd = cvePtoVtaOrd;
	}

	/**
	 * Obtener el objeto: nombre ord.
	 *
	 * @return El objeto: nombre ord
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}

	/**
	 * Definir el objeto: nombre ord.
	 *
	 * @param nombreOrd El nuevo objeto: nombre ord
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Obtener el objeto: tipo cuenta ord.
	 *
	 * @return El objeto: tipo cuenta ord
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}

	/**
	 * Definir el objeto: tipo cuenta ord.
	 *
	 * @param tipoCuentaOrd El nuevo objeto: tipo cuenta ord
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}

	/**
	 * Obtener el objeto: rfc ord.
	 *
	 * @return El objeto: rfc ord
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}

	/**
	 * Definir el objeto: rfc ord.
	 *
	 * @param rfcOrd El nuevo objeto: rfc ord
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}
	
}
