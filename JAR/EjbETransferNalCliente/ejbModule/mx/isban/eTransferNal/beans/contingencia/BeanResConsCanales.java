package mx.isban.eTransferNal.beans.contingencia;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanResConsCanales.
 *
 * @author FSW-Vector
 * @since 25/10/2018
 */
public class BeanResConsCanales  implements Serializable {

	/** Propiedad del tipo long que almacena el valor de serialVersionUID. */
	private static final long serialVersionUID = 7039107247448067697L;

	/** Propiedad que almacena la abstraccion de una pagina. */
	private BeanPaginador paginador;
	
	/** Propiedad del tipo List<BeanResCanal> que almacena el valor de beanResCanalList. */
	private List<BeanCanales> beanResCanalList = null;
	
	/** La variable que contiene informacion con respecto a: bean base error. */
	private BeanResBase beanBaseError;
	     
	/**
	 * Metodo get que sirve para obtener el valor de la propiedad paginador.
	 *
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador(){
		return paginador;
	}
	
	/**
	 * Modifica el valor de la propiedad paginador.
	 *
	 * @param paginador Objeto del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador){
		this.paginador=paginador;
	}
	

	/**
	 * Obtener el objeto: bean base error.
	 *
	 * @return El objeto: bean base error
	 */
	public BeanResBase getBeanBaseError() {
		return beanBaseError;
	}
	
	/**
	 * Definir el objeto: bean base error.
	 *
	 * @param beanBaseError El nuevo objeto: bean base error
	 */
	public void setBeanBaseError(BeanResBase beanBaseError) {
		this.beanBaseError = beanBaseError;
	}
	
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResCanalList.
	 *
	 * @return List<BeanResCanal> Objeto de tipo @see List<BeanResCanal>
	 */
	public List<BeanCanales> getBeanResCanalList() {
		return beanResCanalList;
	}
	
	/**
	 * Metodo que modifica el valor de la propiedad beanResCanalList.
	 *
	 * @param beanResCanalList Objeto de tipo @see List<BeanResCanal>
	 */
	public void setBeanResCanalList(List<BeanCanales> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}
	 
}
