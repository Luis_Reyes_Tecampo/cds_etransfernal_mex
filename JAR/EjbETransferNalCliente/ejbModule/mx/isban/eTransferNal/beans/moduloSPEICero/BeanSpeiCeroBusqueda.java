package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;

/**
 * 
 * @author vector
 * Bean para los datos de busqueda de SPEICERO
 *
 */
public class BeanSpeiCeroBusqueda  extends BeanFechaHorario  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5613497830687272516L;
	
	/** The insti principal. */
	
	private boolean instiPrincipal;	
	/** The insti alterna. */
	private boolean instiAlterna;
	
	/** The prev spei cero. */
	private boolean prevSpeiCero;
	
	/** The duran spei cero. */
	private boolean duranSpeiCero;
	
	/** The estatus opera. */
	private String estatusOpera;	
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;	
	/**
	 * Propiedad del tipo String que almacena el valor de opcionCero
	 */
	private String opcionCero;	
	
	/** propiedad que indica si es historico */
	private String historico;
	
	/** propiedad que indica si es exportarTodo */
	private String exportar;
	
	/** The duran spei cero. */
	private String rptTodo;
	
	
	/**
	 * @return the historico
	 */
	public String getHistorico() {
		return historico;
	}
	/**
	 * @param historico the historico to set
	 */
	public void setHistorico(String historico) {
		this.historico = historico;
	}
		
	/**
	 * @return the exportar
	 */
	public String getExportar() {
		return exportar;
	}
	/**
	 * @param exportar the exportar to set
	 */
	public void setExportar(String exportar) {
		this.exportar = exportar;
	}
	/**
	 * @return the instiPrincipal
	 */
	public boolean isInstiPrincipal() {
		return instiPrincipal;
	}
	/**
	 * @param instiPrincipal the instiPrincipal to set
	 */
	public void setInstiPrincipal(boolean instiPrincipal) {
		this.instiPrincipal = instiPrincipal;
	}
	/**
	 * @return the instiAlterna
	 */
	public boolean isInstiAlterna() {
		return instiAlterna;
	}
	/**
	 * @param instiAlterna the instiAlterna to set
	 */
	public void setInstiAlterna(boolean instiAlterna) {
		this.instiAlterna = instiAlterna;
	}
	/**
	 * @return the prevSpeiCero
	 */
	public boolean isPrevSpeiCero() {
		return prevSpeiCero;
	}
	/**
	 * @param prevSpeiCero the prevSpeiCero to set
	 */
	public void setPrevSpeiCero(boolean prevSpeiCero) {
		this.prevSpeiCero = prevSpeiCero;
	}
	
	/**
	 * @return the duranSpeiCero
	 */
	public boolean isDuranSpeiCero() {
		return duranSpeiCero;
	}
	/**
	 * @param duranSpeiCero the duranSpeiCero to set
	 */
	public void setDuranSpeiCero(boolean duranSpeiCero) {
		this.duranSpeiCero = duranSpeiCero;
	}
	/**
	 * @return the estatusOpera
	 */
	public String getEstatusOpera() {
		return estatusOpera;
	}
	/**
	 * @param estatusOpera the estatusOpera to set
	 */
	public void setEstatusOpera(String estatusOpera) {
		this.estatusOpera = estatusOpera;
	}
	/**
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}
	/**
	 * @param opcion the opcion to set
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	/**
	 * @return the opcionCero
	 */
	public String getOpcionCero() {
		return opcionCero;
	}
	/**
	 * @param opcionCero the opcionCero to set
	 */
	public void setOpcionCero(String opcionCero) {
		this.opcionCero = opcionCero;
	}
	/**
	 * @return the rptTodo
	 */
	public String getRptTodo() {
		return rptTodo;
	}
	/**
	 * @param rptTodo the rptTodo to set
	 */
	public void setRptTodo(String rptTodo) {
		this.rptTodo = rptTodo;
	}	
	
	
	
	
	
	
}
