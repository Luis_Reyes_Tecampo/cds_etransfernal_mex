/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanFechaHorario.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   24/04/2018   SALVADOR MEZA  VECTOR 		Creacion de Bean
 *
 */
package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Clase la cual se encapsulan los datos 
 * que  forman la entrada de la consulta de Emision Reporte del dia
 * Modulo: SPEI Cero.
 */
public class BeanFechaHorario extends BeanSPEIListaMinHrs  implements BeanResultBO,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3005340084151195495L;
	
	
	/** The hora ini spei cero. */
	private String horaIniSpeiCero;
	
	/** The hora fec operacion. */
	private String horaFecOperacion;
	
	/** The hora inicio. */
	private String horaInicio;
	
	/** The hora fin. */
	private String horaFin;
	
	/** The hora inicio. */
	private String minInicio;
	
	/** The hora fin. */
	private String minFin;
	
	/** The hora inicio. */
	private String hrInicio;
	
	/** The hora fin. */
	private String hrFin;
	
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;
	
	/**
	 * Gets the hora ini spei cero.
	 *
	 * @return the hora ini spei cero
	 */
	public String getHoraIniSpeiCero() {
		return horaIniSpeiCero;
	}

	/**
	 * Sets the hora ini spei cero.
	 *
	 * @param horaIniSpeiCero the new hora ini spei cero
	 */
	public void setHoraIniSpeiCero(String horaIniSpeiCero) {
		this.horaIniSpeiCero = horaIniSpeiCero;
	}

	/**
	 * Gets the hora fec operacion.
	 *
	 * @return the hora fec operacion
	 */
	public String getHoraFecOperacion() {
		return horaFecOperacion;
	}

	/**
	 * Sets the hora fec operacion.
	 *
	 * @param horaFecOperacion the new hora fec operacion
	 */
	public void setHoraFecOperacion(String horaFecOperacion) {
		this.horaFecOperacion = horaFecOperacion;
	}

	/**
	 * Gets the hora inicio.
	 *
	 * @return the hora inicio
	 */
	public String getHoraInicio() {
		return horaInicio;
	}

	/**
	 * Sets the hora inicio.
	 *
	 * @param horaInicio the new hora inicio
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * Gets the hora fin.
	 *
	 * @return the hora fin
	 */
	public String getHoraFin() {
		return horaFin;
	}

	/**
	 * Sets the hora fin.
	 *
	 * @param horaFin the new hora fin
	 */
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	/**
	 * @return the minInicio
	 */
	public String getMinInicio() {
		return minInicio;
	}

	/**
	 * @param minInicio the minInicio to set
	 */
	public void setMinInicio(String minInicio) {
		this.minInicio = minInicio;
	}

	/**
	 * @return the minFin
	 */
	public String getMinFin() {
		return minFin;
	}

	/**
	 * @param minFin the minFin to set
	 */
	public void setMinFin(String minFin) {
		this.minFin = minFin;
	}

	/**
	 * @return the hrInicio
	 */
	public String getHrInicio() {
		return hrInicio;
	}

	/**
	 * @param hrInicio the hrInicio to set
	 */
	public void setHrInicio(String hrInicio) {
		this.hrInicio = hrInicio;
	}

	/**
	 * @return the hrFin
	 */
	public String getHrFin() {
		return hrFin;
	}

	/**
	 * @param hrFin the hrFin to set
	 */
	public void setHrFin(String hrFin) {
		this.hrFin = hrFin;
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the msgError
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError the msgError to set
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	

	
}