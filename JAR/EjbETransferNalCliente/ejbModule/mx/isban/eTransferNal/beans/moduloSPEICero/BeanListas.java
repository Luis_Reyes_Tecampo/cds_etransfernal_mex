/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanEstatusOpera.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/04/2018   SALVADOR MEZA  VECTOR 		Creacion de Bean
 *
 */
package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;

/**
 * Clase la cual encapsulan los datos 
 * que forman parte de la consulta del Estatus de Operacion
 * Modulo: SPEI Cero.
 */
public class BeanListas implements Serializable{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7906281053917335297L;
	
	/** The enviadas. */
	private String valor;
	
	/** The confirmadas. */
	private String clave;

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * Gets the clave.
	 *
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Sets the clave.
	 *
	 * @param clave the new clave
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}
}