/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsMiInstitucion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	         Company 	 Description
 * ------- -----------   -----------    ---------- ----------------------
 *   1.0   26/04/2018   SALVADOR MEZA     VECTOR     Creacion de Bean
 *
 */
package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Clase la cual se encapsulan los datos 
 * que  forman la salida de la consulta de Emision Reporte del dia
 * Modulo: SPEI Cero.
 */
public class BeanConsMiInstitucion implements BeanResultBO,Serializable{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8375130057950660520L;
	
	/** The mi institucion. */
	private String miInstitucion;
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;

	/**
	 * Gets the mi institucion.
	 *
	 * @return the mi institucion
	 */
	public String getMiInstitucion() {
		return miInstitucion;
	}

	/**
	 * Sets the mi institucion.
	 *
	 * @param miInstitucion the new mi institucion
	 */
	public void setMiInstitucion(String miInstitucion) {
		this.miInstitucion = miInstitucion;
	}
	
	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the msgError
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError the msgError to set
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	
}
