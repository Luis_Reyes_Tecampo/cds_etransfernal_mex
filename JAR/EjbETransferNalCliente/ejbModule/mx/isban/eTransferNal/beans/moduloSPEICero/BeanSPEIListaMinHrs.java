package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * @author vector
 * Bean para los datos de busqueda de SPEICERO
 *
 */
public class BeanSPEIListaMinHrs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8563266595249696533L;
	
	/** The lista listaMinInicio. */
	private List<BeanListas> hInicio1;
	
	/** The lista listaMinFin. */
	private List<BeanListas> hFin1;
	
	/** The lista listaHraInicio. */
	private List<BeanListas> hInicio2;
	
	/** The lista listaHraFin. */
	private List<BeanListas> hFin2;

	
	/**
	 * @return the hInicio1
	 */
	public List<BeanListas> gethInicio1() {
		return hInicio1;
	}

	/**
	 * @param hInicio1 the hInicio1 to set
	 */
	public void sethInicio1(List<BeanListas> hInicio1) {
		this.hInicio1 = hInicio1;
	}

	/**
	 * @return the hFin1
	 */
	public List<BeanListas> gethFin1() {
		return hFin1;
	}

	/**
	 * @param hFin1 the hFin1 to set
	 */
	public void sethFin1(List<BeanListas> hFin1) {
		this.hFin1 = hFin1;
	}

	/**
	 * @return the hInicio2
	 */
	public List<BeanListas> gethInicio2() {
		return hInicio2;
	}

	/**
	 * @param hInicio2 the hInicio2 to set
	 */
	public void sethInicio2(List<BeanListas> hInicio2) {
		this.hInicio2 = hInicio2;
	}

	/**
	 * @return the hFin2
	 */
	public List<BeanListas> gethFin2() {
		return hFin2;
	}

	/**
	 * @param hFin2 the hFin2 to set
	 */
	public void sethFin2(List<BeanListas> hFin2) {
		this.hFin2 = hFin2;
	}

	
	
	
}
