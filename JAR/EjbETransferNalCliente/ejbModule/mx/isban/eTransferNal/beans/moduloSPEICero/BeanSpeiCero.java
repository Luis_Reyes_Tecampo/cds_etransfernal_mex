package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;
/**
 * 
 * @author vector
 *
 */
public class BeanSpeiCero  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6881911041985609725L;
	
	/**variable para referencia	**/
	private String referencia;
	/**variable para tipoOperacion	**/
	private String tipoOperacion;
	/**variable para descripcion	**/
	private String descripcion;
	/**variable para cveTransferencia	**/
	private String cveTransferencia;
	/**variable para cveOperacion	**/
	private String cveOperacion;
	/**variable para importeAbono	**/
	private String importeAbono;
	/**variable para nombreOrd	**/
	private String nombreOrd;
	/**variable para nombreRec	**/
	private String nombreRec;
	/**variable para estatusSer	**/
	private String estatusSer;
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the cveTransferencia
	 */
	public String getCveTransferencia() {
		return cveTransferencia;
	}
	/**
	 * @param cveTransferencia the cveTransferencia to set
	 */
	public void setCveTransferencia(String cveTransferencia) {
		this.cveTransferencia = cveTransferencia;
	}
	/**
	 * @return the cveOperacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	/**
	 * @param cveOperacion the cveOperacion to set
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	/**
	 * @return the importeAbono
	 */
	public String getImporteAbono() {
		return importeAbono;
	}
	/**
	 * @param importeAbono the importeAbono to set
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}
	/**
	 * @return the nombreOrd
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}
	/**
	 * @param nombreOrd the nombreOrd to set
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}
	/**
	 * @return the nombreRec
	 */
	public String getNombreRec() {
		return nombreRec;
	}
	/**
	 * @param nombreRec the nombreRec to set
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}
	/**
	 * @return the estatusSer
	 */
	public String getEstatusSer() {
		return estatusSer;
	}
	/**
	 * @param estatusSer the estatusSer to set
	 */
	public void setEstatusSer(String estatusSer) {
		this.estatusSer = estatusSer;
	}
	
	
}
