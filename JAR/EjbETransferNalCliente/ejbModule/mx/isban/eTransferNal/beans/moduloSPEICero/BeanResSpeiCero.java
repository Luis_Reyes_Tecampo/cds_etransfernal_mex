package mx.isban.eTransferNal.beans.moduloSPEICero;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
/**
 * 
 * @author vector 31/05/2018
 *
 */
public class BeanResSpeiCero extends BeanSpeiCeroBusqueda implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3044200254552910353L;
	
	/** Propiedad del tipo BeanPaginador que almacena el valor de paginador. */
	private BeanPaginador beanPaginador;
	
	/**
	 * variable para la lista de los objetos
	 */
	private List<BeanSpeiCero> listaSpeiCero;

	/** The lista estatus opera. */
	private List<BeanListas> listaEstatusOpera;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/** The hora fin. */
	private String mInstitucion;
	/** The hora . */
	private boolean todo;
	
	/** The fechaOperacion */
	private String fchOperacion;
	/** The tipoError */
	private String tipoError;
	
	/**
	 * @return the beanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador the beanPaginador to set
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}	
	
	/**
	 * @return the listaSpeiCero
	 */
	public List<BeanSpeiCero> getListaSpeiCero() {
		return listaSpeiCero;
	}
	/**
	 * @param listaSpeiCero the listaSpeiCero to set
	 */
	public void setListaSpeiCero(List<BeanSpeiCero> listaSpeiCero) {
		this.listaSpeiCero = listaSpeiCero;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the listaEstatusOpera
	 */
	public List<BeanListas> getListaEstatusOpera() {
		return listaEstatusOpera;
	}
	/**
	 * @param listaEstatusOpera the listaEstatusOpera to set
	 */
	public void setListaEstatusOpera(List<BeanListas> listaEstatusOpera) {
		this.listaEstatusOpera = listaEstatusOpera;
	}
	
	/**
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * @return the mInstitucion
	 */
	public String getmInstitucion() {
		return mInstitucion;
	}
	/**
	 * @param mInstitucion the mInstitucion to set
	 */
	public void setmInstitucion(String mInstitucion) {
		this.mInstitucion = mInstitucion;
	}
	/**
	 * @return the 
	 */
	public boolean isTodo() {
		return todo;
	}
	/**
	 * @param the  to set
	 */
	public void setTodo(boolean todo) {
		this.todo = todo;
	}
	/**
	 * @return the fchOperacion
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}
	/**
	 * @param fchOperacion the fchOperacion to set
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}
	/**
	 * @return the tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError the tipoError to set
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	
	
			
}
