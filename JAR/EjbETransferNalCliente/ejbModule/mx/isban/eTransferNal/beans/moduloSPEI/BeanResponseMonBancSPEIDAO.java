/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResponseMonBancSPEIDAO.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 11:00:05	    1396920: Moises Navarro                 TCS         Creacion de BeanResponseMonBancSPEIDAO.java
 */
package mx.isban.eTransferNal.beans.moduloSPEI;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.utils.UtilsBean;

/**
 * Descripcion:Bean utilizado para transportar la respuesta del monitor de bancos SPEI del DAO al BO
 * BeanResponseMonBancSPEIDAO.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BeanResponseMonBancSPEIDAO.java
*/
public class BeanResponseMonBancSPEIDAO implements BeanResultBO, Serializable{
	
	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = 4301224669492366476L;

	/** Atributo que representa la variable codError del tipo String */
	private String codError;

    /** Atributo que representa la variable msgError del tipo String */
    private String msgError;
    
    /** Atributo que representa la variable listaBancos del tipo List<BeanEstatusBancoSPEI> */
    private List<BeanEstatusBancoSPEI> listaBancos;
    
    /** Atributo que representa la variable consolidadoEstatus del tipo BeanConsolEstatBancoSPEI */
    private BeanConsolEstatBancoSPEI consolidadoEstatus;

    /* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getCodError()
	 */
	@Override
	public String getCodError() {
		return this.codError;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getMsgError()
	 */
	@Override
	public String getMsgError() {
		return this.msgError;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setCodError(java.lang.String)
	 */
	@Override
	public void setCodError(String arg0) {
		this.codError=arg0;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setMsgError(java.lang.String)
	 */
	@Override
	public void setMsgError(String arg0) {
		this.msgError=arg0;
	}

	/**
	* Descripcion   : Obtiene el valor del atributo listaBancos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return listaBancos
	*/
	public List<BeanEstatusBancoSPEI> getListaBancos() {
		return UtilsBean.clone(listaBancos);
	}

	/**
	* Descripcion   : Establece el valor del atributo listaBancos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param listaBancos valor del listaBancos a establecer
	*/
	public void setListaBancos(List<BeanEstatusBancoSPEI> listaBancos) {
		this.listaBancos = UtilsBean.clone(listaBancos);
	}

	/**
	* Descripcion   : Obtiene el valor del atributo consolidadoEstatus
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return consolidadoEstatus
	*/
	public BeanConsolEstatBancoSPEI getConsolidadoEstatus() {
		return consolidadoEstatus;
	}

	/**
	* Descripcion   : Establece el valor del atributo consolidadoEstatus
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param consolidadoEstatus valor del consolidadoEstatus a establecer
	*/
	public void setConsolidadoEstatus(BeanConsolEstatBancoSPEI consolidadoEstatus) {
		this.consolidadoEstatus = consolidadoEstatus;
	}

}
