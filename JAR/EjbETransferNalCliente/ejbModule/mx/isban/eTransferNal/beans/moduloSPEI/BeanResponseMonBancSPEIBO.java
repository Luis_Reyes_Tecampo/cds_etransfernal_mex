/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResponseMonBancSPEIBO.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 10:50:39	    1396920: Moises Navarro                 TCS         Creacion de BeanResponseMonBancSPEIBO.java
 */
package mx.isban.eTransferNal.beans.moduloSPEI;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.global.BeanNuevoPaginador;
import mx.isban.eTransferNal.utils.UtilsBean;

/**
 * Descripcion:Bean utilizado para transportar la respuesta del monitor de Bancos SPEI del BO al Controller
 * BeanResponseMonBancSPEIBO.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BeanResponseMonBancSPEIBO.java
*/
public class BeanResponseMonBancSPEIBO implements Serializable{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = -7799270752669921131L;
	
	/** Atributo que representa la variable listadoBancos del tipo List<BeanEstatusBancoSPEI> */
	private List<BeanEstatusBancoSPEI> listadoBancos;
	/** Atributo que representa la variable consolidado del tipo BeanConsolEstatBancoSPEI */
	private BeanConsolEstatBancoSPEI consolidado;
	
	/** Atributo que representa la variable beanPaginador del tipo BeanPaginador */
	private BeanNuevoPaginador beanPaginador;
	
	
	/**
	* Descripcion   : Obtiene el valor del atributo listadoBancos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return listadoBancos
	*/
	public List<BeanEstatusBancoSPEI> getListadoBancos() {
		return UtilsBean.clone(listadoBancos);
	}
	/**
	* Descripcion   : Establece el valor del atributo listadoBancos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param listadoBancos valor del listadoBancos a establecer
	*/
	public void setListadoBancos(List<BeanEstatusBancoSPEI> listadoBancos) {
		this.listadoBancos = UtilsBean.clone(listadoBancos);
	}
	/**
	* Descripcion   : Obtiene el valor del atributo consolidado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return consolidado
	*/
	public BeanConsolEstatBancoSPEI getConsolidado() {
		return consolidado;
	}
	/**
	* Descripcion   : Establece el valor del atributo consolidado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param consolidado valor del consolidado a establecer
	*/
	public void setConsolidado(BeanConsolEstatBancoSPEI consolidado) {
		this.consolidado = consolidado;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo beanPaginador
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return beanPaginador
	*/
	public BeanNuevoPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	* Descripcion   : Establece el valor del atributo beanPaginador
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param beanPaginador valor del beanPaginador a establecer
	*/
	public void setBeanPaginador(BeanNuevoPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	
}
