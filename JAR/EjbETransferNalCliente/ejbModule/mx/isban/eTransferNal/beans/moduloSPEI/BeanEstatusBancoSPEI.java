/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanEstatusBancoSPEI.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 10:51:28	    1396920: Moises Navarro                 TCS         Creacion de BeanEstatusBancoSPEI.java
 */
package mx.isban.eTransferNal.beans.moduloSPEI;

import java.io.Serializable;

/**
 * Descripcion:Bean utilizado para contener la informacion de un banco del monitor de bancos SPEI
 * BeanEstatusBancoSPEI.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BeanEstatusBancoSPEI.java
*/
public class BeanEstatusBancoSPEI implements Serializable{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = -603689973409702533L;
	
	/** Atributo que representa la variable banco del tipo String */
	private String banco;
	/** Atributo que representa la variable intermediario del tipo String */
	private String intermediario;
	/** Atributo que representa la variable nombre del tipo String */
	private String nombre;
	/** Atributo que representa la variable conectado del tipo String */
	private String conectado;
	/** Atributo que representa la variable descripcionConectado del tipo String */
	private String descripcionConectado;
	/** Atributo que representa la variable receptivo del tipo String */
	private String receptivo;
	/** Atributo que representa la variable descripcionReceptivo del tipo String */
	private String descripcionReceptivo;
	/** Atributo que representa la variable semaforo del tipo String */
	private String semaforoConectado;
	/** Atributo que representa la variable semaforoReceptivo del tipo String */
	private String semaforoReceptivo;
	/**
	* Descripcion   : Obtiene el valor del atributo banco
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return banco
	*/
	public String getBanco() {
		return banco;
	}
	/**
	* Descripcion   : Establece el valor del atributo banco
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param banco valor del banco a establecer
	*/
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo intermediario
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return intermediario
	*/
	public String getIntermediario() {
		return intermediario;
	}
	/**
	* Descripcion   : Establece el valor del atributo intermediario
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param intermediario valor del intermediario a establecer
	*/
	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo nombre
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return nombre
	*/
	public String getNombre() {
		return nombre;
	}
	/**
	* Descripcion   : Establece el valor del atributo nombre
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param nombre valor del nombre a establecer
	*/
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo conectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return conectado
	*/
	public String getConectado() {
		return conectado;
	}
	/**
	* Descripcion   : Establece el valor del atributo conectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param conectado valor del conectado a establecer
	*/
	public void setConectado(String conectado) {
		this.conectado = conectado;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo descripcionConectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return descripcionConectado
	*/
	public String getDescripcionConectado() {
		return descripcionConectado;
	}
	/**
	* Descripcion   : Establece el valor del atributo descripcionConectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param descripcionConectado valor del descripcionConectado a establecer
	*/
	public void setDescripcionConectado(String descripcionConectado) {
		this.descripcionConectado = descripcionConectado;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo receptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return receptivo
	*/
	public String getReceptivo() {
		return receptivo;
	}
	/**
	* Descripcion   : Establece el valor del atributo receptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param receptivo valor del receptivo a establecer
	*/
	public void setReceptivo(String receptivo) {
		this.receptivo = receptivo;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo descripcionReceptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return descripcionReceptivo
	*/
	public String getDescripcionReceptivo() {
		return descripcionReceptivo;
	}
	/**
	* Descripcion   : Establece el valor del atributo descripcionReceptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param descripcionReceptivo valor del descripcionReceptivo a establecer
	*/
	public void setDescripcionReceptivo(String descripcionReceptivo) {
		this.descripcionReceptivo = descripcionReceptivo;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo semaforoConectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return semaforoConectado
	*/
	public String getSemaforoConectado() {
		return semaforoConectado;
	}
	/**
	* Descripcion   : Establece el valor del atributo semaforoConectado
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param semaforoConectado valor del semaforoConectado a establecer
	*/
	public void setSemaforoConectado(String semaforoConectado) {
		this.semaforoConectado = semaforoConectado;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo semaforoReceptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return semaforoReceptivo
	*/
	public String getSemaforoReceptivo() {
		return semaforoReceptivo;
	}
	/**
	* Descripcion   : Establece el valor del atributo semaforoReceptivo
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param semaforoReceptivo valor del semaforoReceptivo a establecer
	*/
	public void setSemaforoReceptivo(String semaforoReceptivo) {
		this.semaforoReceptivo = semaforoReceptivo;
	}
}
