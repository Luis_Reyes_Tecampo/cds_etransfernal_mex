/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsolEstatBancoSPEI.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 10:55:30	    1396920: Moises Navarro                 TCS         Creacion de BeanConsolEstatBancoSPEI.java
 */
package mx.isban.eTransferNal.beans.moduloSPEI;

import java.io.Serializable;

/**
 * Descripcion:Bean utilizado para almacenar el consolidado de los estaus de los bancos del monitor SPEI
 * BeanConsolEstatBancoSPEI.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BeanConsolEstatBancoSPEI.java
*/
public class BeanConsolEstatBancoSPEI implements Serializable{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = 7862326377564879683L;
	/** Atributo que representa la variable conectados del tipo int */
	private int conectados=0;
	/** Atributo que representa la variable receptivos del tipo int */
	private int receptivos=0;
	/** Atributo que representa la variable desconectados del tipo int */
	private int desconectados=0;
	/** Atributo que representa la variable noReceptivos del tipo int */
	private int noReceptivos=0;
	/** Atributo que representa la variable bloqueados del tipo int */
	private int bloqueados=0;
	/** Atributo que representa la variable bajas del tipo int */
	private int bajas=0;
	/** Atributo que representa la variable intermediarioNoReg del tipo int */
	private int intermediarioNoReg=0;
	/** Atributo que representa la variable total del tipo int */
	private int total=0;
	/**
	* Descripcion   : Obtiene el valor del atributo intermediarioNoReg
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return intermediarioNoReg
	*/
	public int getIntermediarioNoReg() {
		return intermediarioNoReg;
	}
	/**
	* Descripcion   : Establece el valor del atributo intermediarioNoReg
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param intermediarioNoReg valor del intermediarioNoReg a establecer
	*/
	public void setIntermediarioNoReg(int intermediarioNoReg) {
		this.intermediarioNoReg = intermediarioNoReg;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo total
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return total
	*/
	public int getTotal() {
		return total;
	}
	/**
	* Descripcion   : Establece el valor del atributo total
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param total valor del total a establecer
	*/
	public void setTotal(int total) {
		this.total = total;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo conectados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return conectados
	*/
	public int getConectados() {
		return conectados;
	}
	/**
	* Descripcion   : Establece el valor del atributo conectados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param conectados valor del conectados a establecer
	*/
	public void setConectados(int conectados) {
		this.conectados = conectados;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo receptivos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return receptivos
	*/
	public int getReceptivos() {
		return receptivos;
	}
	/**
	* Descripcion   : Establece el valor del atributo receptivos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param receptivos valor del receptivos a establecer
	*/
	public void setReceptivos(int receptivos) {
		this.receptivos = receptivos;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo desconectados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return desconectados
	*/
	public int getDesconectados() {
		return desconectados;
	}
	/**
	* Descripcion   : Establece el valor del atributo desconectados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param desconectados valor del desconectados a establecer
	*/
	public void setDesconectados(int desconectados) {
		this.desconectados = desconectados;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo noReceptivos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return noReceptivos
	*/
	public int getNoReceptivos() {
		return noReceptivos;
	}
	/**
	* Descripcion   : Establece el valor del atributo noReceptivos
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param noReceptivos valor del noReceptivos a establecer
	*/
	public void setNoReceptivos(int noReceptivos) {
		this.noReceptivos = noReceptivos;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo bloqueados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return bloqueados
	*/
	public int getBloqueados() {
		return bloqueados;
	}
	/**
	* Descripcion   : Establece el valor del atributo bloqueados
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param bloqueados valor del bloqueados a establecer
	*/
	public void setBloqueados(int bloqueados) {
		this.bloqueados = bloqueados;
	}
	/**
	* Descripcion   : Obtiene el valor del atributo bajas
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @return bajas
	*/
	public int getBajas() {
		return bajas;
	}
	/**
	* Descripcion   : Establece el valor del atributo bajas
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 19/03/2020
	* @param bajas valor del bajas a establecer
	*/
	public void setBajas(int bajas) {
		this.bajas = bajas;
	}
	
}
