package mx.isban.eTransferNal.beans.exportar;

import java.io.Serializable;
/**
 * 
 * @author vector 31/05/2018
 *
 */
public class BeanExportarTodo   implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7666890364590036836L;
	
	/**Modulo al que pertenece**/
	private String modulo;
	/**submodulo**/
	private String subModulo;
	/**consulta a exportar**/
	private String consultaExportar;
	/**comlumnas que tendra el reporte **/
	private String columnasReporte;
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private String totalReg;
	/**estatus del reporte **/
	private String estatus;
	/**nombre del reporte**/
	private String nombreRpt;
	/**
	 * @return the modulo
	 */
	public String getModulo() {
		return modulo;
	}
	/**
	 * @param modulo the modulo to set
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	/**
	 * @return the subModulo
	 */
	public String getSubModulo() {
		return subModulo;
	}
	/**
	 * @param subModulo the subModulo to set
	 */
	public void setSubModulo(String subModulo) {
		this.subModulo = subModulo;
	}
	/**
	 * @return the consultaExportar
	 */
	public String getConsultaExportar() {
		return consultaExportar;
	}
	/**
	 * @param consultaExportar the consultaExportar to set
	 */
	public void setConsultaExportar(String consultaExportar) {
		this.consultaExportar = consultaExportar;
	}
	/**
	 * @return the columnasReporte
	 */
	public String getColumnasReporte() {
		return columnasReporte;
	}
	/**
	 * @param columnasReporte the columnasReporte to set
	 */
	public void setColumnasReporte(String columnasReporte) {
		this.columnasReporte = columnasReporte;
	}
	/**
	 * @return the totalReg
	 */
	public String getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(String totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the nombreRpt
	 */
	public String getNombreRpt() {
		return nombreRpt;
	}
	/**
	 * @param nombreRpt the nombreRpt to set
	 */
	public void setNombreRpt(String nombreRpt) {
		this.nombreRpt = nombreRpt;
	}
		
	
}
