/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanDatosIp.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   4/10/2018 12:32:34 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * Class BeanDatosIp.
 * 
 * Clase que contiene los atributos necesarios para almacenar los nuevos campos de las idrecciones Ip.
 * 
 * @author FSW-Vector
 * @since 04/10/2018
 */
public class BeanDatosIp implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6965611205235389947L;
	
	/** La variable que contiene informacion con respecto a: dir ip gen tran. */
	private String dirIpGenTran;
	
	/** La variable que contiene informacion con respecto a: dir ip firma. */
	private String dirIpFirma;
	
	/**
	 * Obtener el objeto: dir ip gen tran.
	 *
	 * @return El objeto: dir ip gen tran
	 */
	public String getDirIpGenTran() {
		return dirIpGenTran;
	}
	
	/**
	 * Definir el objeto: dir ip gen tran.
	 *
	 * @param dirIpGenTran El nuevo objeto: dir ip gen tran
	 */
	public void setDirIpGenTran(String dirIpGenTran) {
		this.dirIpGenTran = dirIpGenTran;
	}
	
	/**
	 * Obtener el objeto: dir ip firma.
	 *
	 * @return El objeto: dir ip firma
	 */
	public String getDirIpFirma() {
		return dirIpFirma;
	}
	
	/**
	 * Definir el objeto: dir ip firma.
	 *
	 * @param dirIpFirma El nuevo objeto: dir ip firma
	 */
	public void setDirIpFirma(String dirIpFirma) {
		this.dirIpFirma = dirIpFirma;
	}
}
