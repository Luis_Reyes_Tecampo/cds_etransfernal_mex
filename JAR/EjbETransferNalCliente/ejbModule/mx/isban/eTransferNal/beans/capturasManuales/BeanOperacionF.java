/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionF.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   01/06/2017 07:00:16 PM Omar Zuniga Lagunas Vector Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;

/**
 * Class BeanOperacionF.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionF implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 2552738903330576965L;

	/** La variable que contiene informacion con respecto a: nombre bco rec. */
	private String nombreBcoRec;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec2. */
	private String numCuentaRec2;
	
	/** La variable que contiene informacion con respecto a: plaza banxico. */
	private String plazaBanxico;
	
	/** La variable que contiene informacion con respecto a: sucursal bco rec. */
	private String sucursalBcoRec;
	
	/** La variable que contiene informacion con respecto a: bean datos generales. */
	private BeanDatosGenerales beanDatosGenerales;
	
	/** La variable que contiene informacion con respecto a: bean datos ip. */
	private BeanDatosIp beanDatosIp;
	
	/**
	 * Obtener el objeto: nombre bco rec.
	 *
	 * @return El objeto: nombre bco rec
	 */
	public String getNombreBcoRec() {
		return nombreBcoRec;
	}
	
	/**
	 * Definir el objeto: nombre bco rec.
	 *
	 * @param nombreBcoRec El nuevo objeto: nombre bco rec
	 */
	public void setNombreBcoRec(String nombreBcoRec) {
		this.nombreBcoRec = nombreBcoRec;
	}
	
	/**
	 * Obtener el objeto: num cuenta rec2.
	 *
	 * @return El objeto: num cuenta rec2
	 */
	public String getNumCuentaRec2() {
		return numCuentaRec2;
	}
	
	/**
	 * Definir el objeto: num cuenta rec2.
	 *
	 * @param numCuentaRec2 El nuevo objeto: num cuenta rec2
	 */
	public void setNumCuentaRec2(String numCuentaRec2) {
		this.numCuentaRec2 = numCuentaRec2;
	}
	
	/**
	 * Obtener el objeto: plaza banxico.
	 *
	 * @return El objeto: plaza banxico
	 */
	public String getPlazaBanxico() {
		return plazaBanxico;
	}
	
	/**
	 * Definir el objeto: plaza banxico.
	 *
	 * @param plazaBanxico El nuevo objeto: plaza banxico
	 */
	public void setPlazaBanxico(String plazaBanxico) {
		this.plazaBanxico = plazaBanxico;
	}
	
	/**
	 * Obtener el objeto: sucursal bco rec.
	 *
	 * @return El objeto: sucursal bco rec
	 */
	public String getSucursalBcoRec() {
		return sucursalBcoRec;
	}
	
	/**
	 * Definir el objeto: sucursal bco rec.
	 *
	 * @param sucursalBcoRec El nuevo objeto: sucursal bco rec
	 */
	public void setSucursalBcoRec(String sucursalBcoRec) {
		this.sucursalBcoRec = sucursalBcoRec;
	}

	public BeanDatosGenerales getBeanDatosGenerales() {
		return beanDatosGenerales;
	}

	public void setBeanDatosGenerales(BeanDatosGenerales beanDatosGenerales) {
		this.beanDatosGenerales = beanDatosGenerales;
	}

	public BeanDatosIp getBeanDatosIp() {
		return beanDatosIp;
	}

	public void setBeanDatosIp(BeanDatosIp beanDatosIp) {
		this.beanDatosIp = beanDatosIp;
	}
	
}
