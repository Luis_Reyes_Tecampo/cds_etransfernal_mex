/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResConsultaTemplatesDAO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) - DAO encargada de administrar las respuestas al servicio
 * solicitado
 *
 */
public class BeanResConsultaTemplatesDAO extends BeanResBase implements Serializable {

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = -5524106243924104364L;
	
	/*DESC. Atributos principales*/
	
	/** Propiedad de tipo  que List<BeanTemplate> contiene informacion con respecto a: listaTemplates */
	private List<BeanTemplate> listaTemplates = new ArrayList<BeanTemplate>();
	
	/** Variable que contiene informacion con respecto a: totalRegistros */
	private Integer totalRegistros =0;
	
	/**
	 * Metodo que permite obtener la lista de templates disponibles para el componente de consulta
	 * @return lista templates
	 */
	public List<BeanTemplate> getListaTemplates() {
		return listaTemplates;
	}
	
	/**
	 * Metodo que permite establecer la lista de templates
	 * @param listaTemplates disponibles
	 */
	public void setListaTemplates(List<BeanTemplate> listaTemplates) {
		this.listaTemplates = listaTemplates;
	}
	

	/**
	 * Metodo que permite obtener el total de registros del componente
	 * @return total de registros del componente
	 */
	public Integer getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Metodo que permite establecer el total de registros del componentes
	 * @param totalRegistros del componente
	 */
	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	
}
