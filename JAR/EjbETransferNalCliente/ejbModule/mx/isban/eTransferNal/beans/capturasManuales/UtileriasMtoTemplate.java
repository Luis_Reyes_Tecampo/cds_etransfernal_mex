/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: UtileriasMtoTemplate.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Thu Apr 06 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * Clase Utileria para uso de pistas de auditoria
 * 
 *
 */
public class UtileriasMtoTemplate {
	
	/**
	 * Metodo que permite obtener los atributos de la clase template con sus valores
	 * @param template objeto con las propiedades
	 * @return String con la informacion del template
	 */
	public String obtenerDatosTemplate(BeanTemplate template){
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		
		mapa.put("TEMP.", template.getIdTemplate());
		mapa.put("LAY.", template.getIdLayout());
		mapa.put("EST.", template.getEstatus());
		/*mapa.put("USR_ALTA", template.getUsrAlta());
		mapa.put("USR_AUTORIZA", template.getUsrAutoriza());*/
		
		/*mapa.put("FEC_CAPTURA", template.getFechaCaptura());
		mapa.put("FEC_MODIFICA", template.getFechaModifica());*/
		
		return convertirString(mapa);
	}
	
	/**
	 * Metodo que permite comparar la informacion de la tabla de campos, y devuelve un string de diferencias
	 * @param listaCamposOrigen con los atributos d eorigen
	 * @param listaCamposFinal con los atributos finales
	 * @return HashMap<String, String>  con las diferencias finales
	 */
	public HashMap<String, Object> compararCamposObtenerDiferentes(List<BeanCampoLayoutTemplate> listaCamposBD,
			List<BeanCampoLayoutTemplate> listaCamposFinal){
		
		HashMap<String, Object> temp = new HashMap<String, Object>();
		HashMap<String, HashMap<String, Object>> inicialMap = new HashMap<String, HashMap<String, Object>>();
		HashMap<String, HashMap<String, Object>> finalMap = new HashMap<String, HashMap<String, Object>>();
		
		HashMap<String, Object> resultado = new HashMap<String, Object>();
		Boolean cambios = false;
		String tempString1="", tempString2="";
		Integer index=1;
		
		//Inicializar map de resultados.
		resultado.put("inicial", "");
		resultado.put("final", "");
		
		//A. Convertir a HashMap la primer lista
		for (BeanCampoLayoutTemplate campo : listaCamposBD) {
			temp = new HashMap<String, Object>();
			//temp.put("ID_TEMPLATE",campo.getIdTemplate());
			//temp.put("ID_CAMPO", campo.getIdCampo());
			temp.put("CAM", campo.getCampo());
			temp.put("OB", campo.getObligatorio());
			temp.put("CONS", campo.getConstante());	
						
			inicialMap.put(campo.getCampo(), temp);
		}
		
		//B. Convertir a HashMap segunda lista
		for (BeanCampoLayoutTemplate campo : listaCamposFinal) {
			temp = new HashMap<String, Object>();
			//temp.put("ID_TEMPLATE",campo.getIdTemplate());
			//temp.put("ID_CAMPO", campo.getIdCampo());
			temp.put("CAM", campo.getCampo());
			
			//Definir los campos manipulables
			temp.put("OB", '0');
			temp.put("CONS", "");
			
			if(campo.getObligatorio()!=null){
				temp.put("OB", campo.getObligatorio());
			}
			if(campo.getConstante()!=null){
				temp.put("CONS", campo.getConstante());
			}
			
			finalMap.put(campo.getCampo(), temp);
		}
		
		//Iterar y comparar datos. CONSTANTE VS OBLIGATORIO
		for(Map.Entry<String, HashMap<String, Object>> entry : inicialMap.entrySet()){
			String key= entry.getKey();
			HashMap<String, Object> filaBD = entry.getValue();
			HashMap<String, Object> filaView = finalMap.get(key);
			
			if(!filaBD.get("CONS").equals(filaView.get("CONS")) || 
					!filaBD.get("OB").equals(filaView.get("OB"))){
				//Cambio en constante
				cambios=true;
				tempString1 = resultado.get("inicial") + convertirString(filaBD);
				tempString2 = resultado.get("final") + convertirString(filaView);
				
				if(index<inicialMap.size()){
					tempString1 += "|";
					tempString2 += "|";
				}
				
				resultado.put("inicial", tempString1);
				resultado.put("final", tempString2);
			}
			
			index++;
		}
		
		//Actualizar los cambios
		resultado.put("cambios", cambios);
		
		return resultado;
	}
	
	
	/**
	 * Metodo que permite obtener informacion de los usuarios asociados al template
	 * @param listaUsuarios con la informacion del template
	 * @return String con la informacion de los usuarios que autorizan el template
	 */
	public String obtenerDatosUsuariosTemplate(List<BeanUsuarioAutorizaTemplate> listaUsuarios){
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String resultado="";
		Integer index=1;
		
		for(BeanUsuarioAutorizaTemplate usuario: listaUsuarios){
			//mapa.put("ID_TEMPLATE",usuario.getIdTemplate());
			//mapa.put("USR_CAPTURA", usuario.getUsrCaptura());
			mapa.put("AUT", usuario.getUsrAutoriza());
			resultado+=convertirString(mapa);
			if(index<listaUsuarios.size()){
				resultado += "|"; 
			}
			index++;
		}
		
		return resultado;
	}
	
	
	/**
	 * Metodo que permite obtener los campos de un template
	 * @param listaCampos que contienen la informacion de los campos
	 * @return String con la informacion de los campos
	 */
	public String obtenerDatosCamposTemplate(List<BeanCampoLayoutTemplate> listaCampos){
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String resultado="";
		Integer index=1;
		
		for(BeanCampoLayoutTemplate campo: listaCampos){
			//mapa.put("ID_TEMPLATE",campo.getIdTemplate());
			//mapa.put("ID_CAMPO", campo.getIdCampo());
			mapa.put("CAM", campo.getCampo());
			mapa.put("OB", campo.getObligatorio());
			mapa.put("CONS", campo.getConstante());			
			resultado+=convertirString(mapa);
			if(index<listaCampos.size()){
				resultado += "|"; 
			}
			index++;
		}
		
		return resultado;
		
	}
	
	
	
	/**
	 * Metodo que permite convertir una estructura Hash a string con key, value
	 * @param map con los valores a convertir
	 * @return String con los atributos y valores
	 */
	public String convertirString(HashMap<String, Object> map){
		String resultado="";
		Integer index= 1;
		
		for(Map.Entry<String, Object> entry : map.entrySet()){
			resultado+= entry.getKey() + "=" + entry.getValue();
			//Separador de atributos
			if(index<map.size()){
				resultado+=",";
			}
			index++;
		}
		
		return resultado;
	}
}
