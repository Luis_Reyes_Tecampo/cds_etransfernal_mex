/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   27/03/2017 07:01:52 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.math.BigDecimal;

/**
 * Class BeanOperacionC.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionC extends BeanOperacionD {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5320518012077649357L;

	/** La variable que contiene informacion con respecto a: cve usuario sol. */
	private String cveUsuarioSol;
	
	/** La variable que contiene informacion con respecto a: direccion ip. */
	private String direccionIp;
	
	/** La variable que contiene informacion con respecto a: domicilio ord. */
	private String domicilioOrd;
	
	/** La variable que contiene informacion con respecto a: fch constit ord. */
	private String fchConstitOrd;
	
	/** La variable que contiene informacion con respecto a: fch instruc pago. */
	private String fchInstrucPago;
	
	/** La variable que contiene informacion con respecto a: folio pago. */
	private Long folioPago;
	
	/** La variable que contiene informacion con respecto a: folio paquete. */
	private Long folioPaquete;
	
	/** La variable que contiene informacion con respecto a: hora instruc pago. */
	private String horaInstrucPago;
	
	/** La variable que contiene informacion con respecto a: importe abono. */
	private BigDecimal importeAbono;
	
	/** La variable que contiene informacion con respecto a: importe cargo. */
	private BigDecimal importeCargo;
	
	/**
	 * Obtener el objeto: cve usuario sol.
	 *
	 * @return El objeto: cve usuario sol
	 */
	public String getCveUsuarioSol() {
		return cveUsuarioSol;
	}
	
	/**
	 * Definir el objeto: cve usuario sol.
	 *
	 * @param cveUsuarioSol El nuevo objeto: cve usuario sol
	 */
	public void setCveUsuarioSol(String cveUsuarioSol) {
		this.cveUsuarioSol = cveUsuarioSol;
	}
	
	/**
	 * Obtener el objeto: direccion ip.
	 *
	 * @return El objeto: direccion ip
	 */
	public String getDireccionIp() {
		return direccionIp;
	}
	
	/**
	 * Definir el objeto: direccion ip.
	 *
	 * @param direccionIp El nuevo objeto: direccion ip
	 */
	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}
	
	/**
	 * Obtener el objeto: domicilio ord.
	 *
	 * @return El objeto: domicilio ord
	 */
	public String getDomicilioOrd() {
		return domicilioOrd;
	}
	
	/**
	 * Definir el objeto: domicilio ord.
	 *
	 * @param domicilioOrd El nuevo objeto: domicilio ord
	 */
	public void setDomicilioOrd(String domicilioOrd) {
		this.domicilioOrd = domicilioOrd;
	}
	
	/**
	 * Obtener el objeto: fch constit ord.
	 *
	 * @return El objeto: fch constit ord
	 */
	public String getFchConstitOrd() {
		return fchConstitOrd;
	}
	
	/**
	 * Definir el objeto: fch constit ord.
	 *
	 * @param fchConstitOrd El nuevo objeto: fch constit ord
	 */
	public void setFchConstitOrd(String fchConstitOrd) {
		this.fchConstitOrd = fchConstitOrd;
	}
	
	/**
	 * Obtener el objeto: fch instruc pago.
	 *
	 * @return El objeto: fch instruc pago
	 */
	public String getFchInstrucPago() {
		return fchInstrucPago;
	}
	
	/**
	 * Definir el objeto: fch instruc pago.
	 *
	 * @param fchInstrucPago El nuevo objeto: fch instruc pago
	 */
	public void setFchInstrucPago(String fchInstrucPago) {
		this.fchInstrucPago = fchInstrucPago;
	}
	
	/**
	 * Obtener el objeto: folio pago.
	 *
	 * @return El objeto: folio pago
	 */
	public Long getFolioPago() {
		return folioPago;
	}
	
	/**
	 * Definir el objeto: folio pago.
	 *
	 * @param folioPago El nuevo objeto: folio pago
	 */
	public void setFolioPago(Long folioPago) {
		this.folioPago = folioPago;
	}
	
	/**
	 * Obtener el objeto: folio paquete.
	 *
	 * @return El objeto: folio paquete
	 */
	public Long getFolioPaquete() {
		return folioPaquete;
	}
	
	/**
	 * Definir el objeto: folio paquete.
	 *
	 * @param folioPaquete El nuevo objeto: folio paquete
	 */
	public void setFolioPaquete(Long folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	
	/**
	 * Obtener el objeto: hora instruc pago.
	 *
	 * @return El objeto: hora instruc pago
	 */
	public String getHoraInstrucPago() {
		return horaInstrucPago;
	}
	
	/**
	 * Definir el objeto: hora instruc pago.
	 *
	 * @param horaInstrucPago El nuevo objeto: hora instruc pago
	 */
	public void setHoraInstrucPago(String horaInstrucPago) {
		this.horaInstrucPago = horaInstrucPago;
	}
	
	/**
	 * Obtener el objeto: importe abono.
	 *
	 * @return El objeto: importe abono
	 */
	public BigDecimal getImporteAbono() {
		return importeAbono;
	}
	
	/**
	 * Definir el objeto: importe abono.
	 *
	 * @param importeAbono El nuevo objeto: importe abono
	 */
	public void setImporteAbono(BigDecimal importeAbono) {
		this.importeAbono = importeAbono;
	}
	
	/**
	 * Obtener el objeto: importe cargo.
	 *
	 * @return El objeto: importe cargo
	 */
	public BigDecimal getImporteCargo() {
		return importeCargo;
	}
	
	/**
	 * Definir el objeto: importe cargo.
	 *
	 * @param importeCargo El nuevo objeto: importe cargo
	 */
	public void setImporteCargo(BigDecimal importeCargo) {
		this.importeCargo = importeCargo;
	}
}
