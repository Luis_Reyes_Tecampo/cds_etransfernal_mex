/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionD.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   27/03/2017 07:00:16 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * Class BeanOperacionD.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionD extends BeanOperacionE {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7719833130047971679L;

	/** La variable que contiene informacion con respecto a: motivo devol. */
	private String motivoDevol;
	
	/** La variable que contiene informacion con respecto a: nombre ord. */
	private String nombreOrd;
	
	/** La variable que contiene informacion con respecto a: nombre rec. */
	private String nombreRec;
	
	/** La variable que contiene informacion con respecto a: nombre rec2. */
	private String nombreRec2;
	
	/** La variable que contiene informacion con respecto a: num cuenta ord. */
	private String numCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: num cuenta rec. */
	private String numCuentaRec;
	
	/** La variable que contiene informacion con respecto a: refe numerica. */
	private String refeNumerica;
	
	/** La variable que contiene informacion con respecto a: referencia med. */
	private Long referenciaMed;
	
	/** La variable que contiene informacion con respecto a: rfc ord. */
	private String rfcOrd;
	
	/** La variable que contiene informacion con respecto a: rfc rec. */
	private String rfcRec;

	/**
	 * Obtener el objeto: motivo devol.
	 *
	 * @return El objeto: motivo devol
	 */
	public String getMotivoDevol() {
		return motivoDevol;
	}

	/**
	 * Definir el objeto: motivo devol.
	 *
	 * @param motivoDevol El nuevo objeto: motivo devol
	 */
	public void setMotivoDevol(String motivoDevol) {
		this.motivoDevol = motivoDevol;
	}

	/**
	 * Obtener el objeto: nombre ord.
	 *
	 * @return El objeto: nombre ord
	 */
	public String getNombreOrd() {
		return nombreOrd;
	}

	/**
	 * Definir el objeto: nombre ord.
	 *
	 * @param nombreOrd El nuevo objeto: nombre ord
	 */
	public void setNombreOrd(String nombreOrd) {
		this.nombreOrd = nombreOrd;
	}

	/**
	 * Obtener el objeto: nombre rec.
	 *
	 * @return El objeto: nombre rec
	 */
	public String getNombreRec() {
		return nombreRec;
	}

	/**
	 * Definir el objeto: nombre rec.
	 *
	 * @param nombreRec El nuevo objeto: nombre rec
	 */
	public void setNombreRec(String nombreRec) {
		this.nombreRec = nombreRec;
	}

	/**
	 * Obtener el objeto: nombre rec2.
	 *
	 * @return El objeto: nombre rec2
	 */
	public String getNombreRec2() {
		return nombreRec2;
	}

	/**
	 * Definir el objeto: nombre rec2.
	 *
	 * @param nombreRec2 El nuevo objeto: nombre rec2
	 */
	public void setNombreRec2(String nombreRec2) {
		this.nombreRec2 = nombreRec2;
	}

	/**
	 * Obtener el objeto: num cuenta ord.
	 *
	 * @return El objeto: num cuenta ord
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}

	/**
	 * Definir el objeto: num cuenta ord.
	 *
	 * @param numCuentaOrd El nuevo objeto: num cuenta ord
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}

	/**
	 * Obtener el objeto: num cuenta rec.
	 *
	 * @return El objeto: num cuenta rec
	 */
	public String getNumCuentaRec() {
		return numCuentaRec;
	}

	/**
	 * Definir el objeto: num cuenta rec.
	 *
	 * @param numCuentaRec El nuevo objeto: num cuenta rec
	 */
	public void setNumCuentaRec(String numCuentaRec) {
		this.numCuentaRec = numCuentaRec;
	}

	/**
	 * Obtener el objeto: refe numerica.
	 *
	 * @return El objeto: refe numerica
	 */
	public String getRefeNumerica() {
		return refeNumerica;
	}

	/**
	 * Definir el objeto: refe numerica.
	 *
	 * @param refeNumerica El nuevo objeto: refe numerica
	 */
	public void setRefeNumerica(String refeNumerica) {
		this.refeNumerica = refeNumerica;
	}

	/**
	 * Obtener el objeto: referencia med.
	 *
	 * @return El objeto: referencia med
	 */
	public Long getReferenciaMed() {
		return referenciaMed;
	}

	/**
	 * Definir el objeto: referencia med.
	 *
	 * @param referenciaMed El nuevo objeto: referencia med
	 */
	public void setReferenciaMed(Long referenciaMed) {
		this.referenciaMed = referenciaMed;
	}

	/**
	 * Obtener el objeto: rfc ord.
	 *
	 * @return El objeto: rfc ord
	 */
	public String getRfcOrd() {
		return rfcOrd;
	}

	/**
	 * Definir el objeto: rfc ord.
	 *
	 * @param rfcOrd El nuevo objeto: rfc ord
	 */
	public void setRfcOrd(String rfcOrd) {
		this.rfcOrd = rfcOrd;
	}

	/**
	 * Obtener el objeto: rfc rec.
	 *
	 * @return El objeto: rfc rec
	 */
	public String getRfcRec() {
		return rfcRec;
	}

	/**
	 * Definir el objeto: rfc rec.
	 *
	 * @param rfcRec El nuevo objeto: rfc rec
	 */
	public void setRfcRec(String rfcRec) {
		this.rfcRec = rfcRec;
	}
}
