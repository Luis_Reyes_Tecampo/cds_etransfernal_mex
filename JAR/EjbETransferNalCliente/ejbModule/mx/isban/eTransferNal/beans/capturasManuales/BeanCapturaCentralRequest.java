/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCapturaCentralRequest.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   3/04/2017 05:46:30 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class BeanCapturaCentralRequest.
 *
 * @author FSW-Vector
 * @since 3/04/2017
 */
public class BeanCapturaCentralRequest implements Serializable {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7077132828055545004L;
	
	/** La variable que contiene informacion con respecto a: template. */
	private String template;
	
	/** La variable que contiene informacion con respecto a: step. */
	private String step;
	
	/** La variable que contiene informacion con respecto a: id folio. */
	private String idFolio;
	
	/** La variable que contiene informacion con respecto a: puede editar. */
	private boolean editable;

	/** La variable que contiene informacion con respecto a: listTemplates. */
	private List<String> listTemplates = new ArrayList<String>();

	/** La variable que contiene informacion con respecto a: listLayouts. */
	private List<String> listLayouts = new ArrayList<String>();
	
	/**
	 * Obtener el objeto: template.
	 *
	 * @return El objeto: template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Definir el objeto: template.
	 *
	 * @param template El nuevo objeto: template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Obtener el objeto: step.
	 *
	 * @return El objeto: step
	 */
	public String getStep() {
		return step;
	}

	/**
	 * Definir el objeto: step.
	 *
	 * @param step El nuevo objeto: step
	 */
	public void setStep(String step) {
		this.step = step;
	}

	/**
	 * Obtener el objeto: id folio.
	 *
	 * @return El objeto: id folio
	 */
	public String getIdFolio() {
		return idFolio;
	}

	/**
	 * Definir el objeto: id folio.
	 *
	 * @param idFolio El nuevo objeto: id folio
	 */
	public void setIdFolio(String idFolio) {
		this.idFolio = idFolio;
	}

	/**
	 * Verificar si editable.
	 *
	 * @return true, si editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Definir el objeto: editable.
	 *
	 * @param editable El nuevo objeto: editable
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the listTemplates
	 */
	public List<String> getListTemplates() {
		return listTemplates;
	}

	/**
	 * @param listTemplates the listTemplates to set
	 */
	public void setListTemplates(List<String> listTemplates) {
		this.listTemplates = listTemplates;
	}

	/**
	 * @return the listLayouts
	 */
	public List<String> getListLayouts() {
		return listLayouts;
	}

	/**
	 * @param listLayouts the listLayouts to set
	 */
	public void setListLayouts(List<String> listLayouts) {
		this.listLayouts = listLayouts;
	}
}
