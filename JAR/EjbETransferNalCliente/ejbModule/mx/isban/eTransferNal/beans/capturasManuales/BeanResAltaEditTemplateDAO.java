/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanResAltaEditTemplateDAO.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 29 10:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * 
 * Clase Bean tipo Data Transfer Object (DTO) - DAO encargada de administrar las respuestas al servicio
 * solicitado
 *
 */
public class BeanResAltaEditTemplateDAO extends BeanResBase implements Serializable {

	/** Constante privada del Serial Version */	
	private static final long serialVersionUID = -5524106243924104364L;
	
	/*DESC. Atributos principales*/
	
	/** Variable que contiene informacion con respecto a: listaLayouts */	
	private List<String> listaLayouts = new ArrayList<String>();
	
	/** Variable que contiene informacion con respecto a: listaUsuarios */
	private List<String> listaUsuarios = new ArrayList<String>();
	
	/** Propiedad que contiene informacion con respecto a: Campos base de Layout */
	private List<BeanCampoLayout> listaCamposMaster = new ArrayList<BeanCampoLayout>();
	
	/** Propiedad que contiene informacion con respecto a: Template */
	private BeanTemplate template = new BeanTemplate();
	
	
	/**
	 * Metodo que permite obtener la lista de layouts disponibles
	 * @return lista de layouts disponibles
	 */
	public List<String> getListaLayouts() {
		return listaLayouts;
	}
	
	/**
	 * Metodo que permite establecer la lista de layouts
	 * @param listaLayouts a establecer
	 */
	public void setListaLayouts(List<String> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}
	
	/**
	 * Metodo que permite obtener la lista de usuarios a utilizarse en el componente
	 * @return lista de usuarios disponibles para administrar
	 */
	public List<String> getListaUsuarios() {
		return listaUsuarios;
	}
	
	/**
	 * Metodo que permite establecer la lista de usuarios disponibles
	 * @param listaUsuarios disponibles para autorizar
	 */
	public void setListaUsuarios(List<String> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	
	/**
	 * Metodo que permite obtener la definicion especifica de los campos del layout
	 * @return lista de campos de layout
	 */
	public List<BeanCampoLayout> getListaCamposMaster() {
		return listaCamposMaster;
	}
	
	/**
	 * Metodo que permite establecer la lista de campos del layout principal
	 * @param listaCamposMaster lista de campos del layout
	 */
	public void setListaCamposMaster(List<BeanCampoLayout> listaCamposMaster) {
		this.listaCamposMaster = listaCamposMaster;
	}
	
	/**
	 * Metodo que permite obtener el template a utilizarse para altas.
	 * @return template
	 */
	public BeanTemplate getTemplate() {
		return template;
	}
	
	/**
	 * Metodo que permite establecer el valor del template
	 * @param template a establecer
	 */
	public void setTemplate(BeanTemplate template) {
		this.template = template;
	}
}
