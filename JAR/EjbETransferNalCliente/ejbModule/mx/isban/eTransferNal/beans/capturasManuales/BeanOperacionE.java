/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionE.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   01/06/2017 07:00:16 PM Omar Zuniga Lagunas Vector Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.math.BigDecimal;

/**
 * Class BeanOperacionD.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionE extends BeanOperacionF {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 7219494395276966070L;
	
	/** La variable que contiene informacion con respecto a: rfc rec2. */
	private String rfcRec2;
	
	/** La variable que contiene informacion con respecto a: tipo cambio. */
	private BigDecimal tipoCambio;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta ord. */
	private String tipoCuentaOrd;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec. */
	private String tipoCuentaRec;
	
	/** La variable que contiene informacion con respecto a: tipo cuenta rec2. */
	private String tipoCuentaRec2;

	/** La variable que contiene informacion con respecto a: ciudad bco rec. */
	private String ciudadBcoRec;
	
	/** La variable que contiene informacion con respecto a: cve aba. */
	private String cveAba;
	
	/** La variable que contiene informacion con respecto a: cve pais bco rec. */
	private String cvePaisBcoRec;
	
	/** La variable que contiene informacion con respecto a: forma liq. */
	private String formaLiq;
	
	/** La variable que contiene informacion con respecto a: importe dls. */
	private BigDecimal importeDls;
	
	/**
	 * Obtener el objeto: rfc rec2.
	 *
	 * @return El objeto: rfc rec2
	 */
	public String getRfcRec2() {
		return rfcRec2;
	}

	/**
	 * Definir el objeto: rfc rec2.
	 *
	 * @param rfcRec2 El nuevo objeto: rfc rec2
	 */
	public void setRfcRec2(String rfcRec2) {
		this.rfcRec2 = rfcRec2;
	}

	/**
	 * Obtener el objeto: tipo cambio.
	 *
	 * @return El objeto: tipo cambio
	 */
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * Definir el objeto: tipo cambio.
	 *
	 * @param tipoCambio El nuevo objeto: tipo cambio
	 */
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * Obtener el objeto: tipo cuenta ord.
	 *
	 * @return El objeto: tipo cuenta ord
	 */
	public String getTipoCuentaOrd() {
		return tipoCuentaOrd;
	}

	/**
	 * Definir el objeto: tipo cuenta ord.
	 *
	 * @param tipoCuentaOrd El nuevo objeto: tipo cuenta ord
	 */
	public void setTipoCuentaOrd(String tipoCuentaOrd) {
		this.tipoCuentaOrd = tipoCuentaOrd;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec.
	 *
	 * @return El objeto: tipo cuenta rec
	 */
	public String getTipoCuentaRec() {
		return tipoCuentaRec;
	}

	/**
	 * Definir el objeto: tipo cuenta rec.
	 *
	 * @param tipoCuentaRec El nuevo objeto: tipo cuenta rec
	 */
	public void setTipoCuentaRec(String tipoCuentaRec) {
		this.tipoCuentaRec = tipoCuentaRec;
	}
	
	/**
	 * Obtener el objeto: ciudad bco rec.
	 *
	 * @return El objeto: ciudad bco rec
	 */
	public String getCiudadBcoRec() {
		return ciudadBcoRec;
	}
	
	/**
	 * Definir el objeto: ciudad bco rec.
	 *
	 * @param ciudadBcoRec El nuevo objeto: ciudad bco rec
	 */
	public void setCiudadBcoRec(String ciudadBcoRec) {
		this.ciudadBcoRec = ciudadBcoRec;
	}
	
	/**
	 * Obtener el objeto: cve aba.
	 *
	 * @return El objeto: cve aba
	 */
	public String getCveAba() {
		return cveAba;
	}
	
	/**
	 * Definir el objeto: cve aba.
	 *
	 * @param cveAba El nuevo objeto: cve aba
	 */
	public void setCveAba(String cveAba) {
		this.cveAba = cveAba;
	}
	
	/**
	 * Obtener el objeto: cve pais bco rec.
	 *
	 * @return El objeto: cve pais bco rec
	 */
	public String getCvePaisBcoRec() {
		return cvePaisBcoRec;
	}
	
	/**
	 * Definir el objeto: cve pais bco rec.
	 *
	 * @param cvePaisBcoRec El nuevo objeto: cve pais bco rec
	 */
	public void setCvePaisBcoRec(String cvePaisBcoRec) {
		this.cvePaisBcoRec = cvePaisBcoRec;
	}
	
	/**
	 * Obtener el objeto: forma liq.
	 *
	 * @return El objeto: forma liq
	 */
	public String getFormaLiq() {
		return formaLiq;
	}
	
	/**
	 * Definir el objeto: forma liq.
	 *
	 * @param formaLiq El nuevo objeto: forma liq
	 */
	public void setFormaLiq(String formaLiq) {
		this.formaLiq = formaLiq;
	}
	
	/**
	 * Obtener el objeto: importe dls.
	 *
	 * @return El objeto: importe dls
	 */
	public BigDecimal getImporteDls() {
		return importeDls;
	}
	
	/**
	 * Definir el objeto: importe dls.
	 *
	 * @param importeDls El nuevo objeto: importe dls
	 */
	public void setImporteDls(BigDecimal importeDls) {
		this.importeDls = importeDls;
	}

	/**
	 * Obtener el objeto: tipo cuenta rec2.
	 *
	 * @return El objeto: tipo cuenta rec2
	 */
	public String getTipoCuentaRec2() {
		return tipoCuentaRec2;
	}

	/**
	 * Definir el objeto: tipo cuenta rec2.
	 *
	 * @param tipoCuentaRec2 El nuevo objeto: tipo cuenta rec2
	 */
	public void setTipoCuentaRec2(String tipoCuentaRec2) {
		this.tipoCuentaRec2 = tipoCuentaRec2;
	}
}