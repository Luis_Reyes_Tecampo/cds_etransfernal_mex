/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   27/03/2017 07:04:45 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * Class BeanOperacion.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacion extends BeanOperacionA {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -5864177144609191046L;
	
	/** La variable que contiene informacion con respecto a: id folio. */
	private String idFolio;
	
	/** La variable que contiene informacion con respecto a: estatus. */
	private String estatus;
	
	/** La variable que contiene informacion con respecto a: cod error. */
	private String codError;
	
	/** La variable que contiene informacion con respecto a: msg error. */
	private String msgError;
	
	/** La variable que contiene informacion con respecto a: id template. */
	private String idTemplate;
	
	/** La variable que contiene informacion con respecto a: id layout. */
	private String idLayout;
	
	/** La variable que contiene informacion con respecto a: usr autoriza. */
	private String usrAutoriza;
	
	/** La variable que contiene informacion con respecto a: usr modifica. */
	private String usrModifica;
	
	/** La variable que contiene informacion con respecto a: operacion apartada. */
	private String operacionApartada;
	
	/** La variable que contiene informacion con respecto a: cod postal ord. */
	private String codPostalOrd;
	
	/**
	 * Obtener el objeto: id folio.
	 *
	 * @return El objeto: id folio
	 */
	public String getIdFolio() {
		return idFolio;
	}
	
	/**
	 * Definir el objeto: id folio.
	 *
	 * @param idFolio El nuevo objeto: id folio
	 */
	public void setIdFolio(String idFolio) {
		this.idFolio = idFolio;
	}
	
	/**
	 * Obtener el objeto: estatus.
	 *
	 * @return El objeto: estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Definir el objeto: estatus.
	 *
	 * @param estatus El nuevo objeto: estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * Obtener el objeto: cod error.
	 *
	 * @return El objeto: cod error
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * Definir el objeto: cod error.
	 *
	 * @param codError El nuevo objeto: cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * Obtener el objeto: msg error.
	 *
	 * @return El objeto: msg error
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * Definir el objeto: msg error.
	 *
	 * @param msgError El nuevo objeto: msg error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * Obtener el objeto: id template.
	 *
	 * @return El objeto: id template
	 */
	public String getIdTemplate() {
		return idTemplate;
	}
	
	/**
	 * Definir el objeto: id template.
	 *
	 * @param idTemplate El nuevo objeto: id template
	 */
	public void setIdTemplate(String idTemplate) {
		this.idTemplate = idTemplate;
	}
	
	/**
	 * Obtener el objeto: id layout.
	 *
	 * @return El objeto: id layout
	 */
	public String getIdLayout() {
		return idLayout;
	}
	
	/**
	 * Definir el objeto: id layout.
	 *
	 * @param idLayout El nuevo objeto: id layout
	 */
	public void setIdLayout(String idLayout) {
		this.idLayout = idLayout;
	}
	
	/**
	 * Obtener el objeto: usr autoriza.
	 *
	 * @return El objeto: usr autoriza
	 */
	public String getUsrAutoriza() {
		return usrAutoriza;
	}
	
	/**
	 * Definir el objeto: usr autoriza.
	 *
	 * @param usrAutoriza El nuevo objeto: usr autoriza
	 */
	public void setUsrAutoriza(String usrAutoriza) {
		this.usrAutoriza = usrAutoriza;
	}
	
	/**
	 * Obtener el objeto: usr modifica.
	 *
	 * @return El objeto: usr modifica
	 */
	public String getUsrModifica() {
		return usrModifica;
	}
	
	/**
	 * Definir el objeto: usr modifica.
	 *
	 * @param usrModifica El nuevo objeto: usr modifica
	 */
	public void setUsrModifica(String usrModifica) {
		this.usrModifica = usrModifica;
	}
	
	/**
	 * Obtener el objeto: operacion apartada.
	 *
	 * @return El objeto: operacion apartada
	 */
	public String getOperacionApartada() {
		return operacionApartada;
	}
	
	/**
	 * Definir el objeto: operacion apartada.
	 *
	 * @param operacionApartada El nuevo objeto: operacion apartada
	 */
	public void setOperacionApartada(String operacionApartada) {
		this.operacionApartada = operacionApartada;
	}
	
	/**
	 * Obtener el objeto: cod postal ord.
	 *
	 * @return El objeto: cod postal ord
	 */
	public String getCodPostalOrd() {
		return codPostalOrd;
	}
	
	/**
	 * Definir el objeto: cod postal ord.
	 *
	 * @param codPostalOrd El nuevo objeto: cod postal ord
	 */
	public void setCodPostalOrd(String codPostalOrd) {
		this.codPostalOrd = codPostalOrd;
	}
}
