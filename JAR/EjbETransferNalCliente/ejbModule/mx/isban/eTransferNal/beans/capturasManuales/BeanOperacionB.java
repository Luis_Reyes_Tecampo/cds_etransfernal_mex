/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionB.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   27/03/2017 07:02:52 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * Class BeanOperacionB.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionB extends BeanOperacionC {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5605052392468549052L;
	
	/** La variable que contiene informacion con respecto a: cve interme ord. */
	private String cveIntermeOrd;
	
	/** La variable que contiene informacion con respecto a: cve interme rec. */
	private String cveIntermeRec;
	
	/** La variable que contiene informacion con respecto a: cve medio ent. */
	private String cveMedioEnt;
	
	/** La variable que contiene informacion con respecto a: cve operacion. */
	private String cveOperacion;
	
	/** La variable que contiene informacion con respecto a: cve pto vta. */
	private String cvePtoVta;
	
	/** La variable que contiene informacion con respecto a: cve pto vta ord. */
	private String cvePtoVtaOrd;
	
	/** La variable que contiene informacion con respecto a: cve pto vta rec. */
	private String cvePtoVtaRec;
	
	/** La variable que contiene informacion con respecto a: cve rastreo. */
	private String cveRastreo;
	
	/** La variable que contiene informacion con respecto a: cve transfe. */
	private String cveTransfe;
	
	/** La variable que contiene informacion con respecto a: cve usuario cap. */
	private String cveUsuarioCap;
	
	/**
	 * Obtener el objeto: cve interme ord.
	 *
	 * @return El objeto: cve interme ord
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	
	/**
	 * Definir el objeto: cve interme ord.
	 *
	 * @param cveIntermeOrd El nuevo objeto: cve interme ord
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	
	/**
	 * Obtener el objeto: cve interme rec.
	 *
	 * @return El objeto: cve interme rec
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}
	
	/**
	 * Definir el objeto: cve interme rec.
	 *
	 * @param cveIntermeRec El nuevo objeto: cve interme rec
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}
	
	/**
	 * Obtener el objeto: cve medio ent.
	 *
	 * @return El objeto: cve medio ent
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	
	/**
	 * Definir el objeto: cve medio ent.
	 *
	 * @param cveMedioEnt El nuevo objeto: cve medio ent
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	
	/**
	 * Obtener el objeto: cve operacion.
	 *
	 * @return El objeto: cve operacion
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	
	/**
	 * Definir el objeto: cve operacion.
	 *
	 * @param cveOperacion El nuevo objeto: cve operacion
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	
	/**
	 * Obtener el objeto: cve pto vta.
	 *
	 * @return El objeto: cve pto vta
	 */
	public String getCvePtoVta() {
		return cvePtoVta;
	}
	
	/**
	 * Definir el objeto: cve pto vta.
	 *
	 * @param cvePtoVta El nuevo objeto: cve pto vta
	 */
	public void setCvePtoVta(String cvePtoVta) {
		this.cvePtoVta = cvePtoVta;
	}
	
	/**
	 * Obtener el objeto: cve pto vta ord.
	 *
	 * @return El objeto: cve pto vta ord
	 */
	public String getCvePtoVtaOrd() {
		return cvePtoVtaOrd;
	}
	
	/**
	 * Definir el objeto: cve pto vta ord.
	 *
	 * @param cvePtoVtaOrd El nuevo objeto: cve pto vta ord
	 */
	public void setCvePtoVtaOrd(String cvePtoVtaOrd) {
		this.cvePtoVtaOrd = cvePtoVtaOrd;
	}
	
	/**
	 * Obtener el objeto: cve pto vta rec.
	 *
	 * @return El objeto: cve pto vta rec
	 */
	public String getCvePtoVtaRec() {
		return cvePtoVtaRec;
	}
	
	/**
	 * Definir el objeto: cve pto vta rec.
	 *
	 * @param cvePtoVtaRec El nuevo objeto: cve pto vta rec
	 */
	public void setCvePtoVtaRec(String cvePtoVtaRec) {
		this.cvePtoVtaRec = cvePtoVtaRec;
	}

	/**
	 * Obtener el objeto: cve rastreo.
	 *
	 * @return El objeto: cve rastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	
	/**
	 * Definir el objeto: cve rastreo.
	 *
	 * @param cveRastreo El nuevo objeto: cve rastreo
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}
	
	/**
	 * Obtener el objeto: cve transfe.
	 *
	 * @return El objeto: cve transfe
	 */
	public String getCveTransfe() {
		return cveTransfe;
	}
	
	/**
	 * Definir el objeto: cve transfe.
	 *
	 * @param cveTransfe El nuevo objeto: cve transfe
	 */
	public void setCveTransfe(String cveTransfe) {
		this.cveTransfe = cveTransfe;
	}
	
	/**
	 * Obtener el objeto: cve usuario cap.
	 *
	 * @return El objeto: cve usuario cap
	 */
	public String getCveUsuarioCap() {
		return cveUsuarioCap;
	}
	
	/**
	 * Definir el objeto: cve usuario cap.
	 *
	 * @param cveUsuarioCap El nuevo objeto: cve usuario cap
	 */
	public void setCveUsuarioCap(String cveUsuarioCap) {
		this.cveUsuarioCap = cveUsuarioCap;
	}
}
