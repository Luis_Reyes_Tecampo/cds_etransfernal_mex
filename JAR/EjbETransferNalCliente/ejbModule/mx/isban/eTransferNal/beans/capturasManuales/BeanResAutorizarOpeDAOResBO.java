package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * The Class BeanResAutorizarOpeDAOResBO.
 */
public class BeanResAutorizarOpeDAOResBO extends BeanBaseConsOperaciones{

	/**  Propiedad del tipo Integer que almacena el total de registros. */
	private Integer totalReg = 0;
	
	/** Variable de Serializacion. */
	private static final long serialVersionUID = -7996397952143397727L;

	/**
	 * Gets the total reg.
	 *
	 * @return the total reg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Sets the total reg.
	 *
	 * @param totalReg the new total reg
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
}
