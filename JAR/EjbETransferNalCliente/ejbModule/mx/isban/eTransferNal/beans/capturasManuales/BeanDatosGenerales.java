/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanDatosGenerales.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   4/10/2018 12:33:03 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class BeanDatosGenerales.
 *
 * Clase que contiene los atributos necesarios para almacenar los nuevos campos y que no hay alguna relacion.
 * 
 * @author FSW-Vector
 * @since 04/10/2018
 */
public class BeanDatosGenerales implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -3107718278472942304L;
	
	/** La variable que contiene informacion con respecto a: buc cliente. */
	private String  bucCliente;
	
	/** La variable que contiene informacion con respecto a: uetr swift. */
	private String uetrSwift;
	
	/** La variable que contiene informacion con respecto a: campo swift 1. */
	private String campoSwift1;
	
	/** La variable que contiene informacion con respecto a: campo swift 2. */
	private String campoSwift2;
	
	/** La variable que contiene informacion con respecto a: importe iva. */
	private BigDecimal importeIva;
	
	/** La variable que contiene informacion con respecto a: refe adicional 1. */
	private String refeAdicional1;
		
	/**
	 * Obtener el objeto: buc cliente.
	 *
	 * @return El objeto: buc cliente
	 */
	public String getBucCliente() {
		return bucCliente;
	}
	
	/**
	 * Definir el objeto: buc cliente.
	 *
	 * @param bucCliente El nuevo objeto: buc cliente
	 */
	public void setBucCliente(String bucCliente) {
		this.bucCliente = bucCliente;
	}
	
	/**
	 * Obtener el objeto: uetr swift.
	 *
	 * @return El objeto: uetr swift
	 */
	public String getUetrSwift() {
		return uetrSwift;
	}
	
	/**
	 * Definir el objeto: uetr swift.
	 *
	 * @param uetrSwift El nuevo objeto: uetr swift
	 */
	public void setUetrSwift(String uetrSwift) {
		this.uetrSwift = uetrSwift;
	}
	
	/**
	 * Obtener el objeto: campo swift 1.
	 *
	 * @return El objeto: campo swift 1
	 */
	public String getCampoSwift1() {
		return campoSwift1;
	}
	
	/**
	 * Definir el objeto: campo swift 1.
	 *
	 * @param campoSwift1 El nuevo objeto: campo swift 1
	 */
	public void setCampoSwift1(String campoSwift1) {
		this.campoSwift1 = campoSwift1;
	}
	
	/**
	 * Obtener el objeto: campos swift 2.
	 *
	 * @return El objeto: campos swift 2
	 */
	public String getCampoSwift2() {
		return campoSwift2;
	}
	
	/**
	 * Definir el objeto: campos swift 2.
	 *
	 * @param campoSwift2 El nuevo objeto: campos swift 2
	 */
	public void setCampoSwift2(String campoSwift2) {
		this.campoSwift2 = campoSwift2;
	}
	
	/**
	 * Obtener el objeto: importe iva.
	 *
	 * @return El objeto: importe iva
	 */
	public BigDecimal getImporteIva() {
		return importeIva;
	}
	
	/**
	 * Definir el objeto: importe iva.
	 *
	 * @param importeIva El nuevo objeto: importe iva
	 */
	public void setImporteMiva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}

	/**
	 * Obtener el objeto: refe adicional 1.
	 *
	 * @return El objeto: refe adicional 1
	 */
	public String getRefeAdicional1() {
		return refeAdicional1;
	}

	/**
	 * Definir el objeto: refe adicional 1.
	 *
	 * @param refeAdicional1 El nuevo objeto: refe adicional 1
	 */
	public void setRefeAdicional1(String refeAdicional1) {
		this.refeAdicional1 = refeAdicional1;
	}
	
}
