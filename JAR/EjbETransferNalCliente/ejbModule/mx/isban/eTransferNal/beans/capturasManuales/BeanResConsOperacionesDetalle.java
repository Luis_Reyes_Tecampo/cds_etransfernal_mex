package mx.isban.eTransferNal.beans.capturasManuales;

import java.util.ArrayList;
import java.util.List;

/**
 * BeanResConsOperacionesDetalle.
 */
public class BeanResConsOperacionesDetalle extends BeanBaseConsOperaciones{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8880569424529695343L;
	
	/** The id layout. */
	private List<BeanLayout> layout = new ArrayList<BeanLayout>();

	/** The editando. */
	private boolean editando;
	
	/** The permiso modicar. */
	private boolean permisoModicar;
	
	/** The folio. */
	private String folio;
	
	/**
	 * @return the layout
	 */
	public List<BeanLayout> getLayout() {
		return layout;
	}

	/**
	 * @param layout the layout to set
	 */
	public void setLayout(List<BeanLayout> layout) {
		this.layout = layout;
	}

	/**
	 * @return the editando
	 */
	public boolean isEditando() {
		return editando;
	}

	/**
	 * @param editando the editando to set
	 */
	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	/**
	 * @return the permisoModicar
	 */
	public boolean isPermisoModicar() {
		return permisoModicar;
	}

	/**
	 * @param permisoModicar the permisoModicar to set
	 */
	public void setPermisoModicar(boolean permisoModicar) {
		this.permisoModicar = permisoModicar;
	}

	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
}
