/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanLayout.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   31/03/2017 06:52:10 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class BeanLayout.
 *
 * @author FSW-Vector
 * @since 31/03/2017
 */
public class BeanLayout implements Serializable {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 654156572556745776L;
	
	/** La variable que contiene informacion con respecto a: tipo servicio. */
	private String tipoServicio;
	
	/** La variable que contiene informacion con respecto a: obligatorio. */
	private int obligatorio;
	
	/** La variable que contiene informacion con respecto a: constante. */
	private String constante;
	
	/** La variable que contiene informacion con respecto a: campo. */
	private String campo;
	
	/** La variable que contiene informacion con respecto a: query. */
	private String query;
	
	/** La variable que contiene informacion con respecto a: combo valores. */
	private List<BeanCombo> comboValores = new ArrayList<BeanCombo>();
	
	/** La variable que contiene informacion con respecto a: valor actual. */
	private String valorActual;
	
	/** La variable que contiene informacion con respecto a: etiqueta. */
	private String etiqueta;
	
	/**
	 * Obtener el objeto: tipo servicio.
	 *
	 * @return El objeto: tipo servicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	
	/**
	 * Definir el objeto: tipo servicio.
	 *
	 * @param tipoServicio El nuevo objeto: tipo servicio
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	
	/**
	 * Obtener el objeto: obligatorio.
	 *
	 * @return El objeto: obligatorio
	 */
	public int getObligatorio() {
		return obligatorio;
	}
	
	/**
	 * Definir el objeto: obligatorio.
	 *
	 * @param obligatorio El nuevo objeto: obligatorio
	 */
	public void setObligatorio(int obligatorio) {
		this.obligatorio = obligatorio;
	}
	
	/**
	 * Obtener el objeto: constante.
	 *
	 * @return El objeto: constante
	 */
	public String getConstante() {
		return constante;
	}
	
	/**
	 * Definir el objeto: constante.
	 *
	 * @param constante El nuevo objeto: constante
	 */
	public void setConstante(String constante) {
		this.constante = constante;
	}
	
	/**
	 * Obtener el objeto: campo.
	 *
	 * @return El objeto: campo
	 */
	public String getCampo() {
		return campo;
	}
	
	/**
	 * Definir el objeto: campo.
	 *
	 * @param campo El nuevo objeto: campo
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	/**
	 * Obtener el objeto: query.
	 *
	 * @return El objeto: query
	 */
	public String getQuery() {
		return query;
	}
	
	/**
	 * Definir el objeto: query.
	 *
	 * @param query El nuevo objeto: query
	 */
	public void setQuery(String query) {
		this.query = query;
	}
	
	/**
	 * Obtener el objeto: valor actual.
	 *
	 * @return El objeto: valor actual
	 */
	public String getValorActual() {
		return valorActual;
	}
	
	/**
	 * Definir el objeto: valor actual.
	 *
	 * @param valorActual El nuevo objeto: valor actual
	 */
	public void setValorActual(String valorActual) {
		this.valorActual = valorActual;
	}
	
	/**
	 * Obtener el objeto: etiqueta.
	 *
	 * @return El objeto: etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}
	
	/**
	 * Definir el objeto: etiqueta.
	 *
	 * @param etiqueta El nuevo objeto: etiqueta
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * Obtener el objeto: combo valores.
	 *
	 * @return El objeto: combo valores
	 */
	public List<BeanCombo> getComboValores() {
		return comboValores;
	}

	/**
	 * Definir el objeto: combo valores.
	 *
	 * @param comboValores El nuevo objeto: combo valores
	 */
	public void setComboValores(List<BeanCombo> comboValores) {
		this.comboValores = comboValores;
	}
	
	
}
