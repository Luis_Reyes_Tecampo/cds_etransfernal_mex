package mx.isban.eTransferNal.beans.capturasManuales;

/**
 * Class BeanOperacionA.
 *
 * @author FSW-Vector
 * @since 27/03/2017
 */
public class BeanOperacionA extends BeanOperacionB {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2918927430531626273L;
	
	/** La variable que contiene informacion con respecto a: comentario1. */
	private String comentario1;
	
	/** La variable que contiene informacion con respecto a: comentario2. */
	private String comentario2;
	
	/** La variable que contiene informacion con respecto a: comentario3. */
	private String comentario3;
	
	/** La variable que contiene informacion con respecto a: comentario4. */
	private String comentario4;
	
	/** La variable que contiene informacion con respecto a: concepto pago. */
	private String conceptoPago;
	
	/** La variable que contiene informacion con respecto a: concepto pago2. */
	private String conceptoPago2;
	
	/** La variable que contiene informacion con respecto a: cve divisa ord. */
	private String cveDivisaOrd;
	
	/** La variable que contiene informacion con respecto a: cve divisa rec. */
	private String cveDivisaRec;
	
	/** La variable que contiene informacion con respecto a: cve empresa. */
	private String cveEmpresa;
	
	/**
	 * Obtener el objeto: comentario1.
	 *
	 * @return El objeto: comentario1
	 */
	public String getComentario1() {
		return comentario1;
	}
	
	/**
	 * Definir el objeto: comentario1.
	 *
	 * @param comentario1 El nuevo objeto: comentario1
	 */
	public void setComentario1(String comentario1) {
		this.comentario1 = comentario1;
	}
	
	/**
	 * Obtener el objeto: comentario2.
	 *
	 * @return El objeto: comentario2
	 */
	public String getComentario2() {
		return comentario2;
	}
	
	/**
	 * Definir el objeto: comentario2.
	 *
	 * @param comentario2 El nuevo objeto: comentario2
	 */
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	
	/**
	 * Obtener el objeto: comentario3.
	 *
	 * @return El objeto: comentario3
	 */
	public String getComentario3() {
		return comentario3;
	}
	
	/**
	 * Definir el objeto: comentario3.
	 *
	 * @param comentario3 El nuevo objeto: comentario3
	 */
	public void setComentario3(String comentario3) {
		this.comentario3 = comentario3;
	}
	
	/**
	 * Obtener el objeto: comentario4.
	 *
	 * @return El objeto: comentario4
	 */
	public String getComentario4() {
		return comentario4;
	}
	
	/**
	 * Definir el objeto: comentario4.
	 *
	 * @param comentario4 El nuevo objeto: comentario4
	 */
	public void setComentario4(String comentario4) {
		this.comentario4 = comentario4;
	}
	
	/**
	 * Obtener el objeto: concepto pago.
	 *
	 * @return El objeto: concepto pago
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	
	/**
	 * Definir el objeto: concepto pago.
	 *
	 * @param conceptoPago El nuevo objeto: concepto pago
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	
	/**
	 * Obtener el objeto: concepto pago2.
	 *
	 * @return El objeto: concepto pago2
	 */
	public String getConceptoPago2() {
		return conceptoPago2;
	}
	
	/**
	 * Definir el objeto: concepto pago2.
	 *
	 * @param conceptoPago2 El nuevo objeto: concepto pago2
	 */
	public void setConceptoPago2(String conceptoPago2) {
		this.conceptoPago2 = conceptoPago2;
	}
	
	/**
	 * Obtener el objeto: cve divisa ord.
	 *
	 * @return El objeto: cve divisa ord
	 */
	public String getCveDivisaOrd() {
		return cveDivisaOrd;
	}
	
	/**
	 * Definir el objeto: cve divisa ord.
	 *
	 * @param cveDivisaOrd El nuevo objeto: cve divisa ord
	 */
	public void setCveDivisaOrd(String cveDivisaOrd) {
		this.cveDivisaOrd = cveDivisaOrd;
	}
	
	/**
	 * Obtener el objeto: cve divisa rec.
	 *
	 * @return El objeto: cve divisa rec
	 */
	public String getCveDivisaRec() {
		return cveDivisaRec;
	}
	
	/**
	 * Definir el objeto: cve divisa rec.
	 *
	 * @param cveDivisaRec El nuevo objeto: cve divisa rec
	 */
	public void setCveDivisaRec(String cveDivisaRec) {
		this.cveDivisaRec = cveDivisaRec;
	}
	
	/**
	 * Obtener el objeto: cve empresa.
	 *
	 * @return El objeto: cve empresa
	 */
	public String getCveEmpresa() {
		return cveEmpresa;
	}
	
	/**
	 * Definir el objeto: cve empresa.
	 *
	 * @param cveEmpresa El nuevo objeto: cve empresa
	 */
	public void setCveEmpresa(String cveEmpresa) {
		this.cveEmpresa = cveEmpresa;
	}	
}
