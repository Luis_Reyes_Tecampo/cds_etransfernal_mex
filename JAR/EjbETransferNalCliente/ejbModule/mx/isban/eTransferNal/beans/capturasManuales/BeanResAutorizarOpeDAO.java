/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 01:10:00 PM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * The Class BeanResAutorizarOpeDAO.
 */
public class BeanResAutorizarOpeDAO extends BeanResAutorizarOpeDAOResBO implements Serializable{

	/** Variable de Serializacion. */
	private static final long serialVersionUID = 1426366180598811627L;

	/** The list operaciones pend autorizar. */
	private List<BeanAutorizarOpe> listOperacionesPendAutorizar = Collections.emptyList();
	
	/** The list operaciones pend autorizar. */
	private List<BeanAutorizarOpe> listOperacionesApartadas = Collections.emptyList();
	
	/** The operaciones apartadas. */
	private boolean operacionesApartadas;
	
	/** The usr admin. */
	private String usrAdmin;

	/**
	 * Gets the list operaciones pend autorizar.
	 *
	 * @return the list operaciones pend autorizar
	 */
	public List<BeanAutorizarOpe> getListOperacionesPendAutorizar() {
		return listOperacionesPendAutorizar;
	}

	/**
	 * Sets the list operaciones pend autorizar.
	 *
	 * @param listOperacionesPendAutorizar the new list operaciones pend autorizar
	 */
	public void setListOperacionesPendAutorizar(
			List<BeanAutorizarOpe> listOperacionesPendAutorizar) {
		this.listOperacionesPendAutorizar = listOperacionesPendAutorizar;
	}

	/**
	 * Gets the list operaciones apartadas.
	 *
	 * @return the list operaciones apartadas
	 */
	public List<BeanAutorizarOpe> getListOperacionesApartadas() {
		return listOperacionesApartadas;
	}

	/**
	 * Sets the list operaciones apartadas.
	 *
	 * @param listOperacionesApartadas the new list operaciones apartadas
	 */
	public void setListOperacionesApartadas(
			List<BeanAutorizarOpe> listOperacionesApartadas) {
		this.listOperacionesApartadas = listOperacionesApartadas;
	}

	/**
	 * Checks if is operaciones apartadas.
	 *
	 * @return true, if is operaciones apartadas
	 */
	public boolean isOperacionesApartadas() {
		return operacionesApartadas;
	}

	/**
	 * Sets the operaciones apartadas.
	 *
	 * @param operacionesApartadas the new operaciones apartadas
	 */
	public void setOperacionesApartadas(boolean operacionesApartadas) {
		this.operacionesApartadas = operacionesApartadas;
	}

	/**
	 * Gets the usr admin.
	 *
	 * @return the usr admin
	 */
	public String getUsrAdmin() {
		return usrAdmin;
	}

	/**
	 * Sets the usr admin.
	 *
	 * @param usrAdmin the new usr admin
	 */
	public void setUsrAdmin(String usrAdmin) {
		this.usrAdmin = usrAdmin;
	}
}
