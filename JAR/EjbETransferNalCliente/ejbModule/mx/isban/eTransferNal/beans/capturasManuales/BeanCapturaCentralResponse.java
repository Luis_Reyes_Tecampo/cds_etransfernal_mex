/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanCapturaCentralResponse.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   3/04/2017 05:47:56 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.beans.capturasManuales;

import java.util.ArrayList;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanCapturaCentralResponse.
 *
 * @author FSW-Vector
 * @since 3/04/2017
 */
public class BeanCapturaCentralResponse extends BeanResBase {
	
	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 654156572556745776L;
	
	/** La variable que contiene informacion con respecto a: template. */
	private String template;
	
	/** La variable que contiene informacion con respecto a: id layout. */
	private String idLayout;
	
	/** La variable que contiene informacion con respecto a: usr captura. */
	private String usrCaptura;
	
	/** La variable que contiene informacion con respecto a: usr modifica. */
	private String usrModifica;
	
	/** La variable que contiene informacion con respecto a: step. */
	private String step;
	
	/** La variable que contiene informacion con respecto a: folio. */
	private String folio;
	
	/** La variable que contiene informacion con respecto a: layout. */
	private List<BeanLayout> layout = new ArrayList<BeanLayout>();

	/** La variable que contiene informacion con respecto a: listTemplates. */
	private List<String> listTemplates = new ArrayList<String>();

	/** La variable que contiene informacion con respecto a: listLayouts. */
	private List<String> listLayouts = new ArrayList<String>();
	
	/**
	 * Obtener el objeto: template.
	 *
	 * @return El objeto: template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Definir el objeto: template.
	 *
	 * @param template El nuevo objeto: template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Obtener el objeto: id layout.
	 *
	 * @return El objeto: id layout
	 */
	public String getIdLayout() {
		return idLayout;
	}

	/**
	 * Definir el objeto: id layout.
	 *
	 * @param idLayout El nuevo objeto: id layout
	 */
	public void setIdLayout(String idLayout) {
		this.idLayout = idLayout;
	}

	/**
	 * Obtener el objeto: usr captura.
	 *
	 * @return El objeto: usr captura
	 */
	public String getUsrCaptura() {
		return usrCaptura;
	}

	/**
	 * Definir el objeto: usr captura.
	 *
	 * @param usrCaptura El nuevo objeto: usr captura
	 */
	public void setUsrCaptura(String usrCaptura) {
		this.usrCaptura = usrCaptura;
	}

	/**
	 * Obtener el objeto: usr modifica.
	 *
	 * @return El objeto: usr modifica
	 */
	public String getUsrModifica() {
		return usrModifica;
	}

	/**
	 * Definir el objeto: usr modifica.
	 *
	 * @param usrModifica El nuevo objeto: usr modifica
	 */
	public void setUsrModifica(String usrModifica) {
		this.usrModifica = usrModifica;
	}

	/**
	 * Obtener el objeto: step.
	 *
	 * @return El objeto: step
	 */
	public String getStep() {
		return step;
	}

	/**
	 * Definir el objeto: step.
	 *
	 * @param step El nuevo objeto: step
	 */
	public void setStep(String step) {
		this.step = step;
	}

	/**
	 * Obtener el objeto: layout.
	 *
	 * @return El objeto: layout
	 */
	public List<BeanLayout> getLayout() {
		return layout;
	}

	/**
	 * Definir el objeto: layout.
	 *
	 * @param layout El nuevo objeto: layout
	 */
	public void setLayout(List<BeanLayout> layout) {
		this.layout = layout;
	}

	/**
	 * Obtener el objeto: folio.
	 *
	 * @return El objeto: folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * Definir el objeto: folio.
	 *
	 * @param folio El nuevo objeto: folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the listTemplates
	 */
	public List<String> getListTemplates() {
		return listTemplates;
	}

	/**
	 * @param listTemplates the listTemplates to set
	 */
	public void setListTemplates(List<String> listTemplates) {
		this.listTemplates = listTemplates;
	}

	/**
	 * @return the listLayouts
	 */
	public List<String> getListLayouts() {
		return listLayouts;
	}

	/**
	 * @param listLayouts the listLayouts to set
	 */
	public void setListLayouts(List<String> listLayouts) {
		this.listLayouts = listLayouts;
	}
}
