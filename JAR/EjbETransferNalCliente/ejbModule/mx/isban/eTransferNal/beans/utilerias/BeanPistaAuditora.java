/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanPistaAuditora.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/08/2018 06:06:19 PM Moises Trejo Hernandez. Isban Creacion
 */

package mx.isban.eTransferNal.beans.utilerias;

import java.io.Serializable;

/**
 * Class BeanPistaAuditora.
 *
 * @author mtrejo
 */
public class BeanPistaAuditora implements Serializable {
	
	/**   Serial Generado por el UID. */
	private static final long serialVersionUID = -2551720084339429702L;

	/**  Operacion a realizar SQL. */
	private String operacion;
	
	/**  Url del Controlador de acceso al form. */
	private String urlController;
	
	/**  Nombre de la tabla modificada y en operaciones. */
	private String nomTabla;

	/**  Dato a modificar. */
	private String datoModi;
	
	/**  Dato FIjo. */
	private String datoFijo;
	
	/**  Comentarios. */
	private String comentarios;
	
	/**  Estatus de la operacion realizada SQL. */
	private String estatus;
	
	/** The dto anterior. */
	private String dtoAnterior;
	
	/** The dto anterior. */
	private String dtoNuevo;
	
	/** The dto codigoError. */
	private String codigoError;
	
	/**
	 * Gets the operacion.
	 *
	 * @return the operacion
	 */
	public String getOperacion() {
		return operacion;
	}
	
	/**
	 * Sets the operacion.
	 *
	 * @param operacion the operacion to set
	 */
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	/**
	 * Gets the url controller.
	 *
	 * @return the urlController
	 */
	public String getUrlController() {
		return urlController;
	}
	
	/**
	 * Sets the url controller.
	 *
	 * @param urlController the urlController to set
	 */
	public void setUrlController(String urlController) {
		this.urlController = urlController;
	}

	/**
	 * Gets the nom tabla.
	 *
	 * @return the nomTabla
	 */
	public String getNomTabla() {
		return nomTabla;
	}
	
	/**
	 * Sets the nom tabla.
	 *
	 * @param nomTabla the nomTabla to set
	 */
	public void setNomTabla(String nomTabla) {
		this.nomTabla = nomTabla;
	}

	/**
	 * Gets the dato modi.
	 *
	 * @return the datoModi
	 */
	public String getDatoModi() {
		return datoModi;
	}
	
	/**
	 * Sets the dato modi.
	 *
	 * @param datoModi the datoModi to set
	 */
	public void setDatoModi(String datoModi) {
		this.datoModi = datoModi;
	}

	/**
	 * Gets the dato fijo.
	 *
	 * @return the datoFijo
	 */
	public String getDatoFijo() {
		return datoFijo;
	}
	
	/**
	 * Sets the dato fijo.
	 *
	 * @param datoFijo the datoFijo to set
	 */
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}

	/**
	 * Gets the comentarios.
	 *
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}
	
	/**
	 * Sets the comentarios.
	 *
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Gets the estatus.
	 *
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Sets the estatus.
	 *
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * Gets the dto anterior.
	 *
	 * @return the dto anterior
	 */
	public String getDtoAnterior() {
		return dtoAnterior;
	}
	
	/**
	 * Sets the dto anterior.
	 *
	 * @param dtoAnterior the new dto anterior
	 */
	public void setDtoAnterior(String dtoAnterior) {
		this.dtoAnterior = dtoAnterior;
	}
	
	/**
	 * Gets the dto nuevo.
	 *
	 * @return the dto nuevo
	 */
	public String getDtoNuevo() {
		return dtoNuevo;
	}

	/**
	 * Sets the dto nuevo.
	 *
	 * @param dtoNuevo the new dto nuevo
	 */
	public void setDtoNuevo(String dtoNuevo) {
		this.dtoNuevo = dtoNuevo;
	}
	
	/**
	 * Obtener el objeto: codigo error.
	 *
	 * @return El objeto: codigo error
	 */
	public String getCodigoError() {
		return codigoError;
	}

	/**
	 * Definir el objeto: codigo error.
	 *
	 * @param codigoError El nuevo objeto: codigo error
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	
	
	
}
