/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanNuevoPaginador.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      31/03/2020 22:05:17	    1396920: Moises Navarro                 TCS         Creacion de BeanNuevoPaginador.java
 */
package mx.isban.eTransferNal.beans.global;

import java.io.Serializable;

/**
 * Descripcion:Bean de paginacion con validaciones JSR303
 * BeanNuevoPaginador.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    BeanNuevoPaginador.java
*/
public class BeanNuevoPaginador implements Serializable{

	/** Atributo que representa la variable serialVersionUID del tipo long */
	private static final long serialVersionUID = 5317930618170532698L;
	
	private String accion = null;
	/**Atributo privado que sirve para almacenar la pagina en donde se encuentra por default es la 1*/
	private int pagina=1;
	/**Atributo privado que sirve para almacenar la pagina inicial por default es 1*/
	private int paginaIni=1;
	/**Atributo privado que sirve para almacenar la pagina final*/
	private int paginaFin=0;
	/**Atributo privado que sirve para almacenar la pagina que le sigue al atributo pagina*/
	private int paginaSig;
	/**Atributo privado que sirve para almacenar la pagina anterior al atributo pagina*/
	private int paginaAnt;
	
	/**Variable del tipo String donde se almacenara el rango de la pagina inicial*/
	private int regIni;
	/**Variable del tipo String donde se almacenara el rango de la pagina final*/
	private int regFin;

	
	/**
	 * Descripcion   : Contructor de clase
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 31/03/2020
	 */
	public BeanNuevoPaginador(){
		this.accion = null;
	}

	
	/**
	 * Descripcion   : Contructor de clase
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 31/03/2020
	 * @param accion accion a realizar
	 * @param pagina pagina inicial
	 * @param paginaFin pagina final
	 */
	public BeanNuevoPaginador(final String accion,final int pagina,final int paginaFin){
		super();
		this.accion = accion;
		this.pagina = pagina;
		this.paginaFin = paginaFin;
	}

	/**
	 * Metodo que calcula cual es el valor de la pagina
	 */
	public void paginar(){
		this.regIni = 1;
		this.regFin = 1;
		int registrosXPagina = 20;
		if(accion!=null && !"".equals(accion.trim())){
			if("SIG".equals(accion)){
				pagina = pagina+1;
			}else if("ANT".equals(accion)){
				pagina = pagina-1;
			}else if("FIN".equals(accion)){
				pagina = paginaFin;
			}else if("INI".equals(accion)){
				pagina = 1;
			}else if("ACT".equals(accion)){
				pagina = this.getPagina();
			}
		}else {
			pagina=1;
		}
		regIni = (pagina-1)* registrosXPagina+1;
		regFin = (pagina-1)* registrosXPagina+registrosXPagina;
	}
	/**
	 * Metodo que sirve para calcular la pagina sig, la pag anterior, y el fin de las pagina
	 * @param totalRegistros Objeto del tipo String que almacena el numero de total de registros
	 * @param registrosXPagina Objeto del tipo String que almacena el numero de registros por pagina
	 */
	public void calculaPaginas(int totalRegistros){
		int registrosXPagina = 20;
		int totalPaginas = 0;

		paginaSig = 0;
		paginaAnt = 0;
		int residuo = 0;
		if(totalRegistros>0){
			totalPaginas = totalRegistros/registrosXPagina;
			residuo = totalRegistros % registrosXPagina;
			if(residuo>0){
				totalPaginas = totalPaginas + 1;
				if(pagina==paginaFin){
					regFin=regIni-1+residuo;
				}
			}
			paginaFin = totalPaginas;
		}
		if( pagina < totalPaginas ){
			paginaSig = pagina+1;
		}
		if(pagina > 0){
			paginaAnt=pagina-1;
		}
	}


	/**
	* Descripcion   : Obtiene el valor del atributo accion
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return accion
	*/
	public String getAccion() {
		return accion;
	}


	/**
	* Descripcion   : Establece el valor del atributo accion
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param accion valor del accion a establecer
	*/
	public void setAccion(String accion) {
		this.accion = accion;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo pagina
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return pagina
	*/
	public int getPagina() {
		return pagina;
	}


	/**
	* Descripcion   : Establece el valor del atributo pagina
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param pagina valor del pagina a establecer
	*/
	public void setPagina(int pagina) {
		this.pagina = pagina;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo paginaIni
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return paginaIni
	*/
	public int getPaginaIni() {
		return paginaIni;
	}


	/**
	* Descripcion   : Establece el valor del atributo paginaIni
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param paginaIni valor del paginaIni a establecer
	*/
	public void setPaginaIni(int paginaIni) {
		this.paginaIni = paginaIni;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo paginaFin
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return paginaFin
	*/
	public int getPaginaFin() {
		return paginaFin;
	}


	/**
	* Descripcion   : Establece el valor del atributo paginaFin
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param paginaFin valor del paginaFin a establecer
	*/
	public void setPaginaFin(int paginaFin) {
		this.paginaFin = paginaFin;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo paginaSig
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return paginaSig
	*/
	public int getPaginaSig() {
		return paginaSig;
	}


	/**
	* Descripcion   : Establece el valor del atributo paginaSig
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param paginaSig valor del paginaSig a establecer
	*/
	public void setPaginaSig(int paginaSig) {
		this.paginaSig = paginaSig;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo paginaAnt
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return paginaAnt
	*/
	public int getPaginaAnt() {
		return paginaAnt;
	}


	/**
	* Descripcion   : Establece el valor del atributo paginaAnt
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param paginaAnt valor del paginaAnt a establecer
	*/
	public void setPaginaAnt(int paginaAnt) {
		this.paginaAnt = paginaAnt;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo regIni
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return regIni
	*/
	public int getRegIni() {
		return regIni;
	}


	/**
	* Descripcion   : Establece el valor del atributo regIni
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param regIni valor del regIni a establecer
	*/
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}


	/**
	* Descripcion   : Obtiene el valor del atributo regFin
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @return regFin
	*/
	public int getRegFin() {
		return regFin;
	}


	/**
	* Descripcion   : Establece el valor del atributo regFin
	* Creado por    : 1396920: Moises Navarro
	* Fecha Creacion: 31/03/2020
	* @param regFin valor del regFin a establecer
	*/
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}

}
