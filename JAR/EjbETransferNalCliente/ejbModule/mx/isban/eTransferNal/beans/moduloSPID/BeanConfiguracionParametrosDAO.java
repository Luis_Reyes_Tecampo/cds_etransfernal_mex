/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfiguracionParametrosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * Clase para la configuracion de parametros de DAO implements de tipo publico
 * @author ISBAN
 *
 */
public class BeanConfiguracionParametrosDAO implements Serializable {
		
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7398571712456098191L;
	
	/**Propiedad del tipo long que almacena la cveCESIF*/
	private long cveCESIF;
	/**Propiedad del tipo String que almacena el nomInstit*/
	private String nomInstit;
	/**Propiedad del tipo String que almacena la ordPago*/
	private String ordPago;
	/**Propiedad del tipo String que almacena las devoluciones*/
	private String devoluciones;
	/**Propiedad del tipo String que almacena los trasSPIDSIAC*/
	private String trasSPIDSIAC;
	/**Propiedad del tipo BeanConfParamAraDAO que almacena el ara*/
	private BeanConfParamAraDAO ara;
	/**Propiedad del tipo BeanConfParamFecDAO que almacena el fec*/
	private BeanConfParamFecDAO fec;
	/**Propiedad del tipo String que almacena el mensajeError*/
	private String mensajeError;
	/** Propiedad del tipo @see String que almacena el codigo de error4 */
	private String codError;
	/** Propiedad del tipo @see String que almacena el mensaje de error4 */
	private String msgError;
	
	/**
	 * @return BeanConfParamAraDAO
	 */
	public BeanConfParamAraDAO getAra() {
		return ara;
	}
	/**
	 * @param ara Objeto del tipo BeanConfParamAraDAO
	 */
	public void setAra(BeanConfParamAraDAO ara) {
		this.ara = ara;
	}
	/**
	 * @return BeanConfParamFecDAO
	 */
	public BeanConfParamFecDAO getFec() {
		return fec;
	}
	/**
	 * @param fec Objeto del tipo BeanConfParamFecDAO
	 */
	public void setFec(BeanConfParamFecDAO fec) {
		this.fec = fec;
	}
	
	/**
	 * @return long
	 */
	public long getCveCESIF() {
		return cveCESIF;
	}
	/**
	 * @param cveCESIF Objeto del tipo long
	 */
	public void setCveCESIF(long cveCESIF) {
		this.cveCESIF = cveCESIF;
	}
	/**
	 * @return String
	 */
	public String getNomInstit() {
		return nomInstit;
	}
	/**
	 * @param nomInstit Objeto del tipo String
	 */
	public void setNomInstit(String nomInstit) {
		this.nomInstit = nomInstit;
	}
	/**
	 * @return String
	 */
	public String getOrdPago() {
		return ordPago;
	}
	/**
	 * @param ordPago Objeto del tipo String
	 */
	public void setOrdPago(String ordPago) {
		this.ordPago = ordPago;
	}
	/**
	 * @return String
	 */
	public String getDevoluciones() {
		return devoluciones;
	}
	/**
	 * @param devoluciones Objeto del tipo String
	 */
	public void setDevoluciones(String devoluciones) {
		this.devoluciones = devoluciones;
	}
	/**
	 * @return String
	 */
	public String getTrasSPIDSIAC() {
		return trasSPIDSIAC;
	}
	/**
	 * @param trasSPIDSIAC Objeto del tipo String
	 */
	public void setTrasSPIDSIAC(String trasSPIDSIAC) {
		this.trasSPIDSIAC = trasSPIDSIAC;
	}	
	/**
	 * @return String
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 * @param mensajeError Objeto del tipo String
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	} 
}