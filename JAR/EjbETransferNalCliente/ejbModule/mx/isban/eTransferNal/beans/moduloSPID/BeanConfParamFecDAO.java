/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfParamFecDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 * Clase para la confirmacion de parametros y la emplementacion de Serializable
 * @author ISBAN
 *
 */
public class BeanConfParamFecDAO implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 5471014771157028511L;
	
	/**Propiedad del tipo String que almacena la fecLogin*/
	private String fecLogin;
	/**Propiedad del tipo String que almacena la fecPassword*/
	private String fecPassword;
	/**Propiedad del tipo String que almacena la fecDestinatario*/
	private String fecDestinatario;
	/**Propiedad del tipo String que almacena la fecDireccionIP*/
	private String fecDireccionIP;
	/**Propiedad del tipo long que almacena la fecInicial*/
	private long fecInicial;
	/**Propiedad del tipo long que almacena la fecFinal*/
	private long fecFinal;
	
	/**
	 * @return String
	 */
	public String getFecLogin() {
		return fecLogin;
	}
	/**
	 * @param fecLogin Objeto del tipo String
	 */
	public void setFecLogin(String fecLogin) {
		this.fecLogin = fecLogin;
	}
	/**
	 * @return String
	 */
	public String getFecPassword() {
		return fecPassword;
	}
	/**
	 * @param fecPassword Objeto del tipo String
	 */
	public void setFecPassword(String fecPassword) {
		this.fecPassword = fecPassword;
	}
	/**
	 * @return String
	 */
	public String getFecDestinatario() {
		return fecDestinatario;
	}
	/**
	 * @param fecDestinatario Objeto del tipo String
	 */
	public void setFecDestinatario(String fecDestinatario) {
		this.fecDestinatario = fecDestinatario;
	}
	/**
	 * @return String
	 */
	public String getFecDireccionIP() {
		return fecDireccionIP;
	}
	/**
	 * @param fecDireccionIP Objeto del tipo String
	 */
	public void setFecDireccionIP(String fecDireccionIP) {
		this.fecDireccionIP = fecDireccionIP;
	}
	/**
	 * @return long
	 */
	public long getFecInicial() {
		return fecInicial;
	}
	/**
	 * @param fecInicial Objeto del tipo long
	 */
	public void setFecInicial(long fecInicial) {
		this.fecInicial = fecInicial;
	}
	/**
	 * @return long
	 */
	public long getFecFinal() {
		return fecFinal;
	}
	/**
	 * @param fecFinal Objeto del tipo long
	 */
	public void setFecFinal(long fecFinal) {
		this.fecFinal = fecFinal;
	}
}
