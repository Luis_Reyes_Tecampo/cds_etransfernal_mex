/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConsultaRecepcionesDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Clase para la consulta de recepciones en DAO con sus implementaciones
 * @author ISBAN
 *
 */
public class BeanResConsultaRecepcionesDAO implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -6351674292634632868L;
	
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	
	/** Propiedad del tipo String que almacena el codigo de error */
	private String codError;
	
	/** Propiedad del tipo String que almacena el mensaje de error */
	private String msgError;
	
	/** Propiedad del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/** Propiedad del tipo String que almacena el titulo */
	private String titulo;
	
	/** Lista  del tipo BeanConsultaRecepciones */
	private List<BeanConsultaRecepciones> listaConsultaRecepciones;
	
	/** Propiedad del tipo String que almacena el nombre del archivo */
	private String nombreArchivo;
	
	private String importeTotal;
	
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getImporteTotal() {
		return this.importeTotal;
	}
	
	

	/**
	 * @return el totalRegError
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * @param totalReg el totalReg a estableser
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * @return el MsgError
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	
	/**
	 * @param tipoError el tipoError a estableser el dato
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * @return la lista ConsultaRecepciones
	 */
	public List<BeanConsultaRecepciones> getListaConsultaRecepciones() {
		return listaConsultaRecepciones;
	}

	/**
	 * @param listaConsultaRecepciones la lista BeanConsultaRecepciones a establecer
	 */
	public void setListaConsultaRecepciones(List<BeanConsultaRecepciones> listaConsultaRecepciones) {
		this.listaConsultaRecepciones = listaConsultaRecepciones;
	}
	
	/**
	 * @return el nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	/**
	 * @param nombreArchivo el nombreArchivo a establecer
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 * @return el titulo a asignar
	 */
	public String getTitulo() {
		return titulo;
	}
	
	/**
	 * @param titulo el titulo a establecer
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	
	
	
}
