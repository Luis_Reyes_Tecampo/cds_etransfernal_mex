/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanCancelacionPagosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Metodo para la cancelacion de pagos implementando el BeanResultBO y Serializable de la clase
 * @author ISBAN
 *
 */
public class BeanCancelacionPagosDAO  implements BeanResultBO, Serializable{


	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -3161892344613971752L;

	/**Propiedad del tipo List<BeanCancelacionPagos> que almacena el beanCancelacionPagos*/
	private List<BeanCancelacionPagos>  beanCancelacionPagos;
	
	/**Propiedad del tipo Integer que almacena el totalReg*/
	private Integer totalReg = 0;
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;
	
	/**Propiedad del tipo String que almacena el nombreArchivo*/
	private String nombreArchivo;
	
	
	/**
	 * @return List<BeanCancelacionPagos>
	 */
	public List<BeanCancelacionPagos> getBeanCancelacionPagos() {
		return beanCancelacionPagos;
	}
	/**
	 * @param beanCancelacionPagos Objeto del tipo List<BeanCancelacionPagos>
	 */
	public void setBeanCancelacionPagos(List<BeanCancelacionPagos> beanCancelacionPagos) {
		this.beanCancelacionPagos = beanCancelacionPagos;
	}	
	
	/**
	 * @return String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo Objeto del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
}
