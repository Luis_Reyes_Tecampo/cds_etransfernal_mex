/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConfParamAraDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;


/**
 * Clase para confirmacion de parametros implementando  Serializable
 * @author ISBAN
 *
 */

public class BeanConfParamAraDAO implements Serializable {

	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 353513979432670935L;
	
	/**Propiedad del tipo String que almacena el araDestinatario*/
	private String araDestinatario;
	/**Propiedad del tipo String que almacena el araCertificado*/
	private String araCertificado;
	/**Propiedad del tipo String que almacena la araDireccionIP*/
	private String araDireccionIP;
	/**Propiedad del tipo long que almacena la araInicial*/
	private long araInicial;
	/**Propiedad del tipo long que almacena la araFinal*/
	private long araFinal;
	
	/**
	 * @return String
	 */
	public String getAraDestinatario() {
		return araDestinatario;
	}
	/**
	 * @param araDestinatario Objeto del tipo String
	 */
	public void setAraDestinatario(String araDestinatario) {
		this.araDestinatario = araDestinatario;
	}
	/**
	 * @return String
	 */
	public String getAraCertificado() {
		return araCertificado;
	}
	/**
	 * @param araCertificado Objeto del tipo String
	 */
	public void setAraCertificado(String araCertificado) {
		this.araCertificado = araCertificado;
	}
	/**
	 * @return String
	 */
	public String getAraDireccionIP() {
		return araDireccionIP;
	}
	/**
	 * @param araDireccionIP Objeto del tipo String
	 */
	public void setAraDireccionIP(String araDireccionIP) {
		this.araDireccionIP = araDireccionIP;
	}
	/**
	 * @return long
	 */
	public long getAraInicial() {
		return araInicial;
	}
	/**
	 * @param araInicial Objeto del tipo long
	 */
	public void setAraInicial(long araInicial) {
		this.araInicial = araInicial;
	}
	/**
	 * @return long
	 */
	public long getAraFinal() {
		return araFinal;
	}
	/**
	 * @param araFinal Objeto del tipo long
	 */
	public void setAraFinal(long araFinal) {
		this.araFinal = araFinal;
	}
}
