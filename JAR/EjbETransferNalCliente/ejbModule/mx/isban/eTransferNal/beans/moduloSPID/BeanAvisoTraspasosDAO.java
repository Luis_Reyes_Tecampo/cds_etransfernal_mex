/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanAvisoTraspasosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 *Clase la cual se encapsulan los datos para almancear la respuesta de la consulta lineal
 **/
public class BeanAvisoTraspasosDAO implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -2057342738295871365L;
	
	/**Propiedad del tipo String que almacena el Hora*/
	private String hora;
	
	/**Propiedad del tipo String que almacena el folio*/
	private String folio;
	
	/**Propiedad del tipo BeanPaginador que almacena el beanPaginador*/
	private BeanPaginador beanPaginador;

	/**Propiedad del tipo Integer que almacena el toral de registros*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad beanPagiandor
	 * 
	 * @return beanPaginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 *Modifica el valor de la propiedad beanPaginador
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad tipoError
	 * 
	 * @return tipoError Objeto del tipo String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 *Modifica el valor de la propiedad tipoError
	 * 
	 * @param tipoError
	 *            Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad Hora
	 * 
	 * @return hora Objeto del tipo String
	 */
	public String getHora() {
		return hora;
	}

	/**
	 *Modifica el valor de la propiedad Hora
	 * 
	 * @param hora Objeto del tipo String
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 *Modifica el valor de la propiedad Folio
	 * 
	 * @param folio Objeto del tipo String
	 */
	public void setOpcion(String folio) {
		this.folio = folio;
	}
	
}
