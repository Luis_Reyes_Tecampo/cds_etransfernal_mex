/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanListadoPagosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BeanListadoPagosDAO implements Serializable {
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -3717293609834211403L;
	
	/**Propiedad del tipo Long que almacena la referencia*/
	private Long referencia;
	/**Propiedad del tipo String que almacena la cveCola*/
	private String cveCola;
	/**Propiedad del tipo String que almacena la cveTran*/
	private String cveTran;
	/**Propiedad del tipo String que almacena la cveMecanismo*/
	private String cveMecanismo;
	/**Propiedad del tipo String que almacena la cveMedioEnt*/
	private String cveMedioEnt;
	/**Propiedad del tipo Date que almacena la fechaCaptura*/
	private Date fechaCaptura;
	/**Propiedad del tipo String que almacena la cveIntermeOrd*/
	private String cveIntermeOrd;
	/**Propiedad del tipo String que almacena la cveIntermeRec*/
	private String cveIntermeRec;
	/**Propiedad del tipo BigDecimal que almacena el importeAbono*/
	private BigDecimal importeAbono;
	/**Propiedad del tipo String que almacena el estatus*/
	private String estatus;
	/**Propiedad del tipo Long que almacena el folioPaquete*/
	private Long folioPaquete;
	/**Propiedad del tipo Long que almacena el foloPago*/
	private Long foloPago;
	/**Propiedad del tipo String que almacena el mensajeError*/
	private String mensajeError;
	/** Propiedad del tipo @see String que almacena el codigo de error */
	private String codError;
	/** Propiedad del tipo @see String que almacena el mensaje de error */
	private String msgError;
	
	/**
	 * @return Long
	 */
	public Long getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia Objeto del tipo Long
	 */
	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return String
	 */
	public String getCveCola() {
		return cveCola;
	}
	/**
	 * @param cveCola Objeto del tipo String
	 */
	public void setCveCola(String cveCola) {
		this.cveCola = cveCola;
	}
	/**
	 * @return String
	 */
	public String getCveTran() {
		return cveTran;
	}
	/**
	 * @param cveTran Objeto del tipo String
	 */
	public void setCveTran(String cveTran) {
		this.cveTran = cveTran;
	}
	/**
	 * @return String
	 */
	public String getCveMecanismo() {
		return cveMecanismo;
	}
	/**
	 * @param cveMecanismo Objeto del tipo String
	 */
	public void setCveMecanismo(String cveMecanismo) {
		this.cveMecanismo = cveMecanismo;
	}
	/**
	 * @return String
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	/**
	 * @param cveMedioEnt Objeto del tipo String
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	/**
	 * @return Date
	 */
	public Date getFechaCaptura() {
		return fechaCaptura;
	}
	/**
	 * @param fechaCaptura Objeto del tipo Date
	 */
	public void setFechaCaptura(Date fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}
	/**
	 * @return String
	 */
	public String getCveIntermeOrd() {
		return cveIntermeOrd;
	}
	/**
	 * @param cveIntermeOrd Objeto del tipo String
	 */
	public void setCveIntermeOrd(String cveIntermeOrd) {
		this.cveIntermeOrd = cveIntermeOrd;
	}
	/**
	 * @return String
	 */
	public String getCveIntermeRec() {
		return cveIntermeRec;
	}
	/**
	 * @param cveIntermeRec objeto del tipo String
	 */
	public void setCveIntermeRec(String cveIntermeRec) {
		this.cveIntermeRec = cveIntermeRec;
	}
	/**
	 * @return BigDecimal
	 */
	public BigDecimal getImporteAbono() {
		return importeAbono;
	}
	/**
	 * @param importeAbono Objeto del tipo BigDecimal
	 */
	public void setImporteAbono(BigDecimal importeAbono) {
		this.importeAbono = importeAbono;
	}
	/**
	 * @return String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus Objeto del tipo String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return Long
	 */
	public Long getFolioPaquete() {
		return folioPaquete;
	}
	/**
	 * @param folioPaquete Objeto del tipo Long
	 */
	public void setFolioPaquete(Long folioPaquete) {
		this.folioPaquete = folioPaquete;
	}
	/**
	 * @return Long
	 */
	public Long getFoloPago() {
		return foloPago;
	}
	/**
	 * @param foloPago Objeto del tipo Long
	 */
	public void setFoloPago(Long foloPago) {
		this.foloPago = foloPago;
	}
	/**
	 * @return String
	 */
	public String getMensajeError() {
		return mensajeError;
	}
	/**
	 * @param mensajeError Objeto del tipo String
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
}
