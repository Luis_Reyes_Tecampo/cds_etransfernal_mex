/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResConfiguracionParametrosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;

/**
 * Clase para la configuracion de parametros DAO con BeanResultBO Serializable
 * @author ISBAN
 *
 */
public class BeanResConfiguracionParametrosDAO implements BeanResultBO, Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4991875446290261103L;

	/**Propiedad que almacena la lista de registros de la bitacora administrativa*/
	private BeanConfiguracionParametros configuracionParametros;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError = msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanConsBitAdmon
   * @return listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
   */
   public BeanConfiguracionParametros getConfiguracionParametros(){
      return configuracionParametros;
   }
   /**
   *Modifica el valor de la propiedad
   * @param configuracionParametros Objeto del tipo BeanConfiguracionParametros
   */
   public void setConfiguracionParametros(BeanConfiguracionParametros configuracionParametros){
      this.configuracionParametros=configuracionParametros;
   }
}
