/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResEquivalenciaDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;

/**
 * Clase para la equivalencia de resultado de dao
 * @author ISBAN
 *
 */
public class BeanResEquivalenciaDAO implements Serializable{
	
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -961859206044648193L;

	/** Propiedad del tipo String que almacena la claveOper */
    private String claveOper;
    /** Propiedad del tipo String que almacena el tipoPago */
	private String tipoPago;
	/** Propiedad del tipo String que almacena la clasificacion */
	private String clasificacion;
	/** Propiedad del tipo String que almacena la descripcion */
	private String descripcion;
	/** Propiedad del tipo String que almacena el horario */
	private String horario;
	
	/** Propiedad del tipo List<BeanEquivalencia> que almacena el beanEquivalencias */
	private List<BeanEquivalencia> beanEquivalencias;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error*/
    private String msgError;
    
    /** Propiedad privada del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/**Propiedad que contendra las propiedades del paginador en la tabla*/
	private BeanPaginador beanPaginador;

	/**
	 * @return String
	 */
	public String getClaveOper() {
		return claveOper;
	}

	/**
	 * @param claveOper Objeto del tipo String
	 */
	public void setClaveOper(String claveOper) {
		this.claveOper = claveOper;
	}

	/**
	 * @return String
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago Objeto del tipo String
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * @return String
	 */
	public String getClasificacion() {
		return clasificacion;
	}

	/**
	 * @param clasificacion Objeto del tipo String
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	/**
	 * @return String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion Objeto del tipo String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return String
	 */
	public String getHorario() {
		return horario;
	}

	/**
	 * @param horario Objeto del tipo String
	 */
	public void setHorario(String horario) {
		this.horario = horario;
	}

	/**
	 * @return List<BeanEquivalencia>
	 */
	public List<BeanEquivalencia> getBeanEquivalencias() {
		return beanEquivalencias;
	}

	/**
	 * @param beanEquivalencias Objeto del tipo List<BeanEquivalencia>
	 */
	public void setBeanEquivalencias(List<BeanEquivalencia> beanEquivalencias) {
		this.beanEquivalencias = beanEquivalencias;
	}

	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * @param totalReg Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * @return String
	 */
	public String getTipoError() {
		return tipoError;
	}

	/**
	 * @param tipoError Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	/**
	 * @return BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
}
