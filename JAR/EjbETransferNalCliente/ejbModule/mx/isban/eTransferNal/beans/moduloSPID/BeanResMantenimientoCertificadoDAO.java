/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResMantenimientoCertificadoDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResMantenimientoCertificadoDAO implements Serializable, BeanResultBO {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -3986284261754774564L;
	/**Propiedad del tipo Integer que almacena el totalReg*/
	private Integer totalReg = 0;
	/**Propiedad del tipo List<BeanMantenimientoCertificado> que almacena el listaMantenimientoCertificado*/
	private List<BeanMantenimientoCertificado> listaMantenimientoCertificado;
	/**Propiedad del tipo List<BeanMantenimiento> que almacena el listaMantenimiento*/
	private List<BeanMantenimiento> listaMantenimiento;
	/**Propiedad del tipo List<BeanClaveOperacion> que almacena el listaCveOperacion*/
	private List<BeanClaveOperacion> listaCveOperacion;
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;
	
	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return List<BeanMantenimientoCertificado>
	 */
	public List<BeanMantenimientoCertificado> getListaMantenimientoCertificado() {
		return listaMantenimientoCertificado;
	}
	/**
	 * @param listaMantenimientoCertificado Objeto del tipo List<BeanMantenimientoCertificado>
	 */
	public void setListaMantenimientoCertificado(List<BeanMantenimientoCertificado> listaMantenimientoCertificado) {
		this.listaMantenimientoCertificado = listaMantenimientoCertificado;
	}
	/**
	 * @return List<BeanMantenimiento>
	 */
	public List<BeanMantenimiento> getListaMantenimiento() {
		return listaMantenimiento;
	}
	/**
	 * @param listaMantenimiento Objeto del tipo List<BeanMantenimiento>
	 */
	public void setListaMantenimiento(List<BeanMantenimiento> listaMantenimiento) {
		this.listaMantenimiento = listaMantenimiento;
	}
	/**
	 * @return List<BeanClaveOperacion>
	 */
	public List<BeanClaveOperacion> getListaCveOperacion() {
		return listaCveOperacion;
	}
	/**
	 * @param listaCveOperacion Objeto del tipo List<BeanClaveOperacion>
	 */
	public void setListaCveOperacion(List<BeanClaveOperacion> listaCveOperacion) {
		this.listaCveOperacion = listaCveOperacion;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	
}
