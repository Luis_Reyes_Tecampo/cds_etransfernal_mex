/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanPantalla.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     10/03/2020 01:56:34 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class BeanPantalla.
 *
 * @author FSW-Vector
 * @since 10/03/2020
 */
public class BeanPantalla implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3871521268625915957L;
	
	/** La variable que contiene informacion con respecto a: nom pantalla. */
	@NotNull
	@Size(min=1,max=30)
	private String nomPantalla;

	/**
	 * Obtener el objeto: nom pantalla.
	 *
	 * @return El objeto: nom pantalla
	 */
	public String getNomPantalla() {
		return nomPantalla;
	}

	/**
	 * Definir el objeto: nom pantalla.
	 *
	 * @param nomPantalla El nuevo objeto: nom pantalla
	 */
	public void setNomPantalla(String nomPantalla) {
		this.nomPantalla = nomPantalla;
	}
	
	

}
