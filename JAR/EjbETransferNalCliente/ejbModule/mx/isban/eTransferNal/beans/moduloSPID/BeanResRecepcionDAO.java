/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResRecepcionDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResRecepcionDAO implements BeanResultBO, Serializable{

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 598989871700534230L;

	/**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError;
	
	/**Propiedad privada del tipo String que almacena el codigo de mensaje
   	de error*/
	private String msgError;
	
	/**Propiedad privada del tipo BeanReceptoresSPID que almacena el beanReceptorSPID*/
	private BeanReceptoresSPID beanReceptorSPID;
	
	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the msgError
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError the msgError to set
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * @return the beanReceptorSPID
	 */
	public BeanReceptoresSPID getBeanReceptorSPID() {
		return beanReceptorSPID;
	}

	/**
	 * @param beanReceptorSPID the beanReceptorSPID to set
	 */
	public void setBeanReceptorSPID(BeanReceptoresSPID beanReceptorSPID) {
		this.beanReceptorSPID = beanReceptorSPID;
	}
}
