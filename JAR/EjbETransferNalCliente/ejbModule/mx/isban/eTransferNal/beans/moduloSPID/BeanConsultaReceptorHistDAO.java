/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsultaReceptorHistDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsRecepHist;

/**
 *Clase la cual se encapsulan los datos para almancear la respuesta de la consulta de Hist DAO
 **/
public class BeanConsultaReceptorHistDAO implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -1678004648875017958L;

	//Tabla TRAN_SPID_REC_HIS
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
    /**Propiedad del tipo @see String que almacena el codigo de error3*/
    private String codError;
    
    /**Propiedad del tipo @see String que almacena el mensaje de error4*/
    private String msgError;
	
    /** Propiedad privada del tipo List que almacena la lista de datos */
	private List<BeanConsRecepHist> listaDatos;

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad totalReg
	 * 
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 *Modifica el valor de la propiedad totalReg
	 * 
	 * @param totalReg
	 *            Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	

	/**
	 *Metodo get que sirve para obtener el valor de la propiedad listaDatos
	 * 
	 * @return listadatos Objeto del tipo List
	 */
	public List<BeanConsRecepHist> getListaDatos() {
		return listaDatos;
	}

	/**
	 *Modifica el valor de la propiedad listaDatos
	 * 
	 * @param listaDatos
	 *            Objeto del tipo List
	 */
	public void setListaDatos(List<BeanConsRecepHist> listaDatos) {
		this.listaDatos = listaDatos;
	}
	
	
}
