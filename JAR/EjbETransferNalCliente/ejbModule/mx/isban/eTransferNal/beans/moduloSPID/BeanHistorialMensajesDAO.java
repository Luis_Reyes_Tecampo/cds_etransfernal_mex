/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanHistorialMensajesDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

/**
 * Metodo para ver el historial de los mensajes con ben y implements
 * @author ISBAN
 *
 */
public class BeanHistorialMensajesDAO implements Serializable{

	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 1804285458314238634L;
	
	/**Propiedad del tipo List<BeanHistorialMensajes> que almacena la beanHistorialMensajes*/
	private List<BeanHistorialMensajes>  beanHistorialMensajes;
	
	
	/**Propiedad del tipo Integer que almacena la totalReg*/
	private Integer totalReg = 0;
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;	
	
	/**
	 * @return List<BeanHistorialMensajes>
	 */
	public List<BeanHistorialMensajes> getBeanHistorialMensajes() {
		return beanHistorialMensajes;
	}
	/**
	 * @param beanHistorialMensajes Objeto del tipo List<BeanHistorialMensajes>
	 */
	public void setBeanHistorialMensajes(List<BeanHistorialMensajes> beanHistorialMensajes) {
		this.beanHistorialMensajes = beanHistorialMensajes;
	}
	
	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	

}
