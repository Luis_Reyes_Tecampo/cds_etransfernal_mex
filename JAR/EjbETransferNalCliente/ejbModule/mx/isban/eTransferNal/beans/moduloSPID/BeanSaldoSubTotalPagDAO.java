package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;

public class BeanSaldoSubTotalPagDAO implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -787897332063441432L;
	/**
	 * Propiedad del tipo String que almacena el valor de topoVLiquidadas
	 */
	private BigDecimal topoVLiquidadas = BigDecimal.ZERO;
	/**
	 * Propiedad del tipo String que almacena el valor de topoTLiquidadas
	 */
	private BigDecimal topoTLiquidadas = BigDecimal.ZERO;
	/**
	 * Propiedad del tipo String que almacena el valor de topoTPorLiquidar
	 */
	private BigDecimal topoTPorLiquidar = BigDecimal.ZERO;
	/**
	 * Propiedad del tipo String que almacena el valor de rechazos
	 */
	private BigDecimal rechazos = BigDecimal.ZERO;
	/**
	 * Metodo get que sirve para obtener la propiedad topoVLiquidadas
	 * @return topoVLiquidadas Objeto del tipo BigDecimal
	 */
	public BigDecimal getTopoVLiquidadas() {
		return topoVLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoVLiquidadas
	 * @param topoVLiquidadas del tipo BigDecimal
	 */
	public void setTopoVLiquidadas(BigDecimal topoVLiquidadas) {
		this.topoVLiquidadas = topoVLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTLiquidadas
	 * @return topoTLiquidadas Objeto del tipo BigDecimal
	 */
	public BigDecimal getTopoTLiquidadas() {
		return topoTLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTLiquidadas
	 * @param topoTLiquidadas del tipo BigDecimal
	 */
	public void setTopoTLiquidadas(BigDecimal topoTLiquidadas) {
		this.topoTLiquidadas = topoTLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTPorLiquidar
	 * @return topoTPorLiquidar Objeto del tipo BigDecimal
	 */
	public BigDecimal getTopoTPorLiquidar() {
		return topoTPorLiquidar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTPorLiquidar
	 * @param topoTPorLiquidar del tipo BigDecimal
	 */
	public void setTopoTPorLiquidar(BigDecimal topoTPorLiquidar) {
		this.topoTPorLiquidar = topoTPorLiquidar;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rechazos
	 * @return rechazos Objeto del tipo BigDecimal
	 */
	public BigDecimal getRechazos() {
		return rechazos;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rechazos
	 * @param rechazos del tipo BigDecimal
	 */
	public void setRechazos(BigDecimal rechazos) {
		this.rechazos = rechazos;
	}


}
