/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResTopologiaDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;

public class BeanResTopologiaDAO implements Serializable, BeanResultBO {

	/**Propiedad que conserva el estado del objeto a traves de las capas*/
	private static final long serialVersionUID = -3889965704893751824L;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**Propiedad que almacena la lista de registros de la bitacora administrativa*/
	private List<BeanTopologia> listaTopologias;
	
	/**Propiedad que contendra los select de los combos cuando se cree una topologia*/
	private List<BeanClaveMedio> listaCveMedio;
	
	/**Propiedad que contendra los select de los combos cuando se cree una topologia*/
	private List<BeanClaveOperacion> listaCveOperacion;

	/**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError;
	
	/**Propiedad privada del tipo String que almacena el codigo de mensaje
   	de error*/
	private String msgError;

   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanConsBitAdmon
   * @return listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
   */
   public List<BeanTopologia> getListaTopologias(){
      return listaTopologias;
   }
   /**
   *Modifica el valor de la propiedad listBeanConsBitAdmon
   * @param listaTopologias Objeto del tipo List<BeanTopologia>
   */
   public void setListaTopologias(List<BeanTopologia> listaTopologias){
      this.listaTopologias=listaTopologias;
   }
   
   /**
    * Metodo get que obtiene el total de registros
    * @return Integer objeto con el numero total de registros
    */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return List<BeanClaveMedio>
	 */
	public List<BeanClaveMedio> getListaCveMedio() {
		return listaCveMedio;
	}
	/**
	 * @param listaCveMedio Objeto del tipo List<BeanClaveMedio>
	 */
	public void setListaCveMedio(List<BeanClaveMedio> listaCveMedio) {
		this.listaCveMedio = listaCveMedio;
	}
	/**
	 * @return List<BeanClaveOperacion>
	 */
	public List<BeanClaveOperacion> getListaCveOperacion() {
		return listaCveOperacion;
	}
	/**
	 * @param listaCveOperacion Objeto del tipo List<BeanClaveOperacion>
	 */
	public void setListaCveOperacion(List<BeanClaveOperacion> listaCveOperacion) {
		this.listaCveOperacion = listaCveOperacion;
	}
}
