/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanBitacoraEnvioDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Clase para la botacora de envio en DAO
 * @author ISBAN
 *
 */
public class BeanBitacoraEnvioDAO implements Serializable {

	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 7500222321631622033L;
	
	/**Propiedad del tipo Long que almacena el folioSolicitud*/
	private Long folioSolicitud;
	/**Propiedad del tipo String que almacena el tipoDeMensaje*/
	private String tipoDeMensaje;
	/**Propiedad del tipo Date que almacena la fechaHoraEnvio*/
	private Date fechaHoraEnvio;
	/**Propiedad del tipo Integer que almacena el numeroPagos*/
	private Integer numeroPagos;
	/**Propiedad del tipo BigDecimal que almacena el importeAbono*/
	private BigDecimal importeAbono;
	/**Propiedad del tipo Integer que almacena el codBanco*/
	private Integer codBanco;
	/**Propiedad del tipo Integer que almacena el numReprocesos*/
	private Integer numReprocesos;
	/**Propiedad del tipo String que almacena el acuseRecibo*/
	private String acuseRecibo;
	/**Propiedad del tipo Integer que almacena los bytesAcumulados*/
	private Integer bytesAcumulados;
	/**Propiedad del tipo String que almacena la operacionCert*/
	private String operacionCert;
	/**Propiedad del tipo String que almacena el numCertificado*/
	private String numCertificado;
	
	/**
	 * @return Long
	 */
	public Long getFolioSolicitud() {
		return folioSolicitud;
	}
	/**
	 * @param folioSolicitud Objeto del tipo Long
	 */
	public void setFolioSolicitud(Long folioSolicitud) {
		this.folioSolicitud = folioSolicitud;
	}
	/**
	 * @return String
	 */
	public String getTipoDeMensaje() {
		return tipoDeMensaje;
	}
	/**
	 * @param tipoDeMensaje Objeto del tipo String
	 */
	public void setTipoDeMensaje(String tipoDeMensaje) {
		this.tipoDeMensaje = tipoDeMensaje;
	}
	/**
	 * @return Date
	 */
	public Date getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}
	/**
	 * @param fechaHoraEnvio Objeto del tipo Date
	 */
	public void setFechaHoraEnvio(Date fechaHoraEnvio) {
		this.fechaHoraEnvio = fechaHoraEnvio;
	}
	/**
	 * @return Integer
	 */
	public Integer getNumeroPagos() {
		return numeroPagos;
	}
	/**
	 * @param numeroPagos Objeto del tipo Integer
	 */
	public void setNumeroPagos(Integer numeroPagos) {
		this.numeroPagos = numeroPagos;
	}
	/**
	 * @return BigDecimal
	 */
	public BigDecimal getImporteAbono() {
		return importeAbono;
	}
	/**
	 * @param importeAbono Objeto del tipo BigDecimal
	 */
	public void setImporteAbono(BigDecimal importeAbono) {
		this.importeAbono = importeAbono;
	}
	/**
	 * @return Integer
	 */
	public Integer getCodBanco() {
		return codBanco;
	}
	/**
	 * @param codBanco Objeto del tipo Integer
	 */
	public void setCodBanco(Integer codBanco) {
		this.codBanco = codBanco;
	}
	/**
	 * @return Integer
	 */
	public Integer getNumReprocesos() {
		return numReprocesos;
	}
	/**
	 * @param numReprocesos Objeto del tipo Integer
	 */
	public void setNumReprocesos(Integer numReprocesos) {
		this.numReprocesos = numReprocesos;
	}
	/**
	 * @return String
	 */
	public String getAcuseRecibo() {
		return acuseRecibo;
	}
	/**
	 * @param acuseRecibo Objeto del tipo String
	 */
	public void setAcuseRecibo(String acuseRecibo) {
		this.acuseRecibo = acuseRecibo;
	}
	/**
	 * @return Integer
	 */
	public Integer getBytesAcumulados() {
		return bytesAcumulados;
	}
	/**
	 * @param bytesAcumulados Objeto del tipo Integer
	 */
	public void setBytesAcumulados(Integer bytesAcumulados) {
		this.bytesAcumulados = bytesAcumulados;
	}
	/**
	 * @return String
	 */
	public String getOperacionCert() {
		return operacionCert;
	}
	/**
	 * @param operacionCert Objeto del tipo String
	 */
	public void setOperacionCert(String operacionCert) {
		this.operacionCert = operacionCert;
	}
	/**
	 * @return String
	 */
	public String getNumCertificado() {
		return numCertificado;
	}
	/**
	 * @param numCertificado Objeto del tipo String
	 */
	public void setNumCertificado(String numCertificado) {
		this.numCertificado = numCertificado;
	}
	
	

}
