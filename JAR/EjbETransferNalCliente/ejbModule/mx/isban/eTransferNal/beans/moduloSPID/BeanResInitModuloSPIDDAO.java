/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResInitModuloSPIDDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;


/**
 * Clase para el modulo de SPIDDAO
 * @author ISBAN
 *
 */
public class BeanResInitModuloSPIDDAO {
	
	/**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError;
	/**Propiedad privada del tipo String que almacena el codigo de mensaje
   	de error*/
	private String msgError;
	/**Propiedad privada del tipo String que almacena la fechaOperacion*/
	private String fechaOperacion;
	/**Propiedad privada del tipo String que almacena la cveInstitucion*/
	private String cveInstitucion;
	
	/**
	 * @return String
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	/**
	 * @param fechaOperacion Objeto del tipo String
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	/**
	 * @return String
	 */
	public String getCveInstitucion() {
		return cveInstitucion;
	}
	/**
	 * @param cveInstitucion Objeto del tipo String
	 */
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}

	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	
}
