/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResBitacoraEnvioDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanBitacoraEnvio;;

/**
 *Clase Bean DTO que se encarga de la bitacora de envio
**/
public class BeanResBitacoraEnvioDAO implements BeanResultBO, Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 3094782337923348258L;
	
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	/**
	 * Propiedad del tipo List<BeanBitacoraEnvio> que almacena el listado de
	 * registros de la Bitácora de Envío
	 */
	private List<BeanBitacoraEnvio> listBeanBitacoraEnvio;
	/** Propiedad privada del tipo String que almacena el código de error */
	private String codError;
	/**
	 * Propiedad privada del tipo String que almacena el código de mensaje de
	 * error
	 */
	private String msgError;
	/**
     * Propiedad del tipo String que almacena el nombre del archivo excel
     */
	private String nombreArchivo;
	
	/**
	 * Metodo get que obtiene el total de registros
	 * 
	 * @return Integer objeto con el numero total de registros
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo set que sirve para setear el total de registros
	 * 
	 * @param totalReg
	 *            el total de registros
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad
	 * listBeanBitacoraEnvio
	 * 
	 * @return listBeanBitacoraEnvio Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public List<BeanBitacoraEnvio> getListBeanBitacoraEnvio() {
		return listBeanBitacoraEnvio;
	}
	/**
	 *Modifica el valor de la propiedad listBeanBitacoraEnvio
	 * 
	 * @param listBeanBitacoraEnvio
	 *            Objeto del tipo List<BeanBitacoraEnvio>
	 */
	public void setListBeanBitacoraEnvio(List<BeanBitacoraEnvio> listBeanBitacoraEnvio) {
		this.listBeanBitacoraEnvio = listBeanBitacoraEnvio;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad codError1
	 * 
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 *Modifica el valor de la propiedad codError
	 * 
	 * @param codError
	 *            Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 *Metodo get que sirve para obtener el valor de la propiedad msgError2
	 * 
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 *Modifica el valor de la propiedad msgError
	 * 
	 * @param msgError
	 *            Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	    *Metodo get que sirve para obtener el valor de la propiedad nombre de archivo
	    * @return nombreArchivo Objeto del tipo String
	    */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	    *Metodo set que sirve para modificar el valor de la propiedad nombre de archivo
	    * @param nombreArchivo Objeto del tipo String
	    */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	

}
