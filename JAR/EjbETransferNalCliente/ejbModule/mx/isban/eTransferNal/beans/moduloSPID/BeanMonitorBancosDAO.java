/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanMonitorBancosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

/**
 * Clase para el monitoreo de bancos y implementacion DAO
 * @author ISBAN
 *
 */
public class BeanMonitorBancosDAO implements Serializable {

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 9133150525848379524L;
	
	/** Propiedad del tipo Integer que almacena el total de registros */
	private Integer totalReg = 0;
	
	/** Propiedad del tipo String que almacena el codigo de error */
	private String codError;
	
	/** Propiedad del tipo String que almacena el mensaje de error */
	private String msgError;
	
	/** Propiedad del tipo String que almacena el tipo de error */
	private String tipoError;
	
	/** Propiedad del tipo String que almacena el nombre del archivo */
	private String nombreArchivo;
	
	/** Lista  del tipo BeanMonitorBancos */
	private List<BeanMonitorBancos> listaMonitorBancos;

	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	
	/**
	 * @param totalReg el totalReg a establecer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * @return el MsgError
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * @param msgError el msgError a establecer
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * @return el tipoError
	 */
	public String getTipoError() {
		return tipoError;
	}
	
	/**
	 * @param tipoError el tipoError a establecer
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	/**
	 * @return la lista MonitorBancos
	 */
	public List<BeanMonitorBancos> getListaMonitorBancos() {
		return listaMonitorBancos;
	}
	
	/**
	 * @param listaMonitorBancos la lista BeanMonitorBancos a establecer
	 */
	public void setListaMonitorBancos(List<BeanMonitorBancos> listaMonitorBancos) {
		this.listaMonitorBancos = listaMonitorBancos;
	}
	
	/**
	 * @return el nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	/**
	 * @param nombreArchivo el nombreArchivo a establecer
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
}
