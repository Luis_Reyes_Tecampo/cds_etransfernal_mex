package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResReceptoresSPIDDAO extends BeanResBase implements Serializable {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6671368819215275020L;
	
	/**
	 * Propiedad del tipo BeanSaldoSubTotalPag que almacena el valor de saldoSubTotalPag
	 */
	private BeanSaldoSubTotalPagDAO saldoSubTotalPag;
	
	/**
	 * Propiedad del tipo BeanVolumenSubTotalPag que almacena el valor de volumenSubTotalPag
	 */
	private BeanVolumenSubTotalPagDAO volumenSubTotalPag;
	
	/**
	 * Propiedad del tipo BeanVolumenTotal que almacena el valor de volumenTotal
	 */
	private BeanVolumenTotalDAO volumenTotal;
	
	/**
	 * Propiedad del tipo BeanSaldoTotal que almacena el valor de saldoTotal
	 */
	private BeanSaldoTotalDAO saldoTotal;
	
	/**
	 * Propiedad del tipo List<BeanReceptoresSPID> que almacena el valor de listReceptoresSPID
	 */
	private List<BeanReceptoresSPID> listReceptoresSPID;
	
	/**
	 * Propiedad del tipo BeanPaginador que almacena el valor de paginador
	 */
	private BeanPaginador paginador;
	/**
	 * Propiedad del tipo String que almacena el valor de opcion
	 */
	private String opcion;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de bloqueado
	 */
	private boolean bloqueado;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;
	
	/**
	 * Propiedad del tipo BeanReceptoresSPID que almacena el valor de beanReceptoresSPID
	 */
	private BeanReceptoresSPID beanReceptoresSPID;	
	
	
	/**
	 * Metodo get que sirve para obtener la propiedad saldoSubTotalPag
	 * @return saldoSubTotalPag Objeto del tipo BeanSaldoSubTotalPagDAO
	 */
	public BeanSaldoSubTotalPagDAO getSaldoSubTotalPag() {
		return saldoSubTotalPag;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad saldoSubTotalPag
	 * @param saldoSubTotalPag del tipo BeanSaldoSubTotalPagDAO
	 */
	public void setSaldoSubTotalPag(BeanSaldoSubTotalPagDAO saldoSubTotalPag) {
		this.saldoSubTotalPag = saldoSubTotalPag;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad volumenSubTotalPag
	 * @return volumenSubTotalPag Objeto del tipo BeanVolumenSubTotalPagDAO
	 */
	public BeanVolumenSubTotalPagDAO getVolumenSubTotalPag() {
		return volumenSubTotalPag;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad volumenSubTotalPag
	 * @param volumenSubTotalPag del tipo BeanVolumenSubTotalPagDAO
	 */
	public void setVolumenSubTotalPag(BeanVolumenSubTotalPagDAO volumenSubTotalPag) {
		this.volumenSubTotalPag = volumenSubTotalPag;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad volumenTotal
	 * @return volumenTotal Objeto del tipo BeanVolumenTotalDAO
	 */
	public BeanVolumenTotalDAO getVolumenTotal() {
		return volumenTotal;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad volumenTotal
	 * @param volumenTotal del tipo BeanVolumenTotalDAO
	 */
	public void setVolumenTotal(BeanVolumenTotalDAO volumenTotal) {
		this.volumenTotal = volumenTotal;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad saldoTotal
	 * @return saldoTotal Objeto del tipo BeanSaldoTotalDAO
	 */
	public BeanSaldoTotalDAO getSaldoTotal() {
		return saldoTotal;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad saldoTotal
	 * @param saldoTotal del tipo BeanSaldoTotalDAO
	 */
	public void setSaldoTotal(BeanSaldoTotalDAO saldoTotal) {
		this.saldoTotal = saldoTotal;
	}

	

	/**
	 * Metodo get que sirve para obtener la propiedad listReceptoresSPID
	 * @return listReceptoresSPID Objeto del tipo List<BeanReceptoresSPID>
	 */
	public List<BeanReceptoresSPID> getListReceptoresSPID() {
		return listReceptoresSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listReceptoresSPID
	 * @param listReceptoresSPID del tipo List<BeanReceptoresSPID>
	 */
	public void setListReceptoresSPID(
			List<BeanReceptoresSPID> listReceptoresSPID) {
		this.listReceptoresSPID = listReceptoresSPID;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad paginador
	 * @return paginador Objeto del tipo BeanPaginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad paginador
	 * @param paginador del tipo BeanPaginador
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad opcion
	 * @return opcion Objeto del tipo String
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad opcion
	 * @param opcion del tipo String
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad bloqueado
	 * @return bloqueado Objeto del tipo boolean
	 */
	public boolean isBloqueado() {
		return bloqueado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad bloqueado
	 * @param bloqueado del tipo boolean
	 */
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchOperacion
	 * @return fchOperacion Objeto del tipo String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchOperacion
	 * @param fchOperacion del tipo String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cveMiInstitucion
	 * @return cveMiInstitucion Objeto del tipo String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion del tipo String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad beanReceptoresSPID
	 * @return beanReceptoresSPID Objeto del tipo BeanReceptoresSPID
	 */
	public BeanReceptoresSPID getBeanReceptoresSPID() {
		return beanReceptoresSPID;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanReceptoresSPID
	 * @param beanReceptoresSPID del tipo BeanReceptoresSPID
	 */
	public void setBeanReceptoresSPID(BeanReceptoresSPID beanReceptoresSPID) {
		this.beanReceptoresSPID = beanReceptoresSPID;
	}



}
