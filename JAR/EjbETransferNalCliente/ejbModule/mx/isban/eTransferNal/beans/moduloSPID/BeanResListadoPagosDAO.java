/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResListadoPagosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanPago;

public class BeanResListadoPagosDAO implements BeanResultBO, Serializable{
	
	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = 7322323207179138223L;

	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;

	/**Propiedad que almacena la lista de registros de la bitacora administrativa*/
	private List<BeanPago> listBeanPago;

   /**Propiedad privada del tipo String que almacena el codigo de error*/
   private String codError;
   /**Propiedad privada del tipo String que almacena el codigo de mensaje
   de error*/
   private String msgError;

   /**Propiedad privada del tipo String que almacena el nombreArchivo*/
	private String nombreArchivo;
     
   /**
   *Metodo get que sirve para obtener el valor de la propiedad codError
   * @return codError Objeto del tipo String
   */
   public String getCodError(){
      return codError;
   }
   /**
   *Modifica el valor de la propiedad codError
   * @param codError Objeto del tipo String
   */
   public void setCodError(String codError){
      this.codError=codError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad msgError
   * @return msgError Objeto del tipo String
   */
   public String getMsgError(){
      return msgError;
   }
   /**
   *Modifica el valor de la propiedad msgError
   * @param msgError Objeto del tipo String
   */
   public void setMsgError(String msgError){
      this.msgError=msgError;
   }
   /**
   *Metodo get que sirve para obtener el valor de la propiedad listBeanConsBitAdmon
   * @return listBeanConsBitAdmon Objeto del tipo List<BeanConsBitAdmon>
   */
   public List<BeanPago> getListBeanPago(){
      return listBeanPago;
   }
   /**
   *Modifica el valor de la propiedad
   * @param listBeanPago Objeto del tipo List<BeanPago>
   */
   public void setListBeanPago(List<BeanPago> listBeanPago){
      this.listBeanPago=listBeanPago;
   }
   
   /**
    * Metodo get que obtiene el total de registros
    * @return Integer objeto con el numero total de registros
    */   
   public Integer getTotalReg() {
	return totalReg;
   }

   /**
    * Metodo set que sirve para setear el total de registros
    * @param totalReg el total de registros
    */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	/**
	 * @return String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	/**
	 * @param nombreArchivo Objeto del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
}
