/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanResAvisoTraspasosDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;
import java.util.List;
import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * Clase para el aviso de los traspasos de con su debida implementacion
 * @author ISBAN
 *
 */
public class BeanResAvisoTraspasosDAO implements BeanResultBO, Serializable{
	

	/**
	 * Constante privada del Serial version
	 */
	private static final long serialVersionUID = -7093034169214928484L;
	/**Propiedad del tipo BeanPaginador que almacena el beanPaginador*/
	private BeanPaginador beanPaginador;
	/**Propiedad del tipo Integer que almacena el totalReg*/
	private Integer totalReg = 0;
	/**Propiedad del tipo String que almacena el codError*/
	private String codError;
	/**Propiedad del tipo String que almacena el msgError*/
	private String msgError;
	/**Propiedad del tipo String que almacena el tipoError*/
	private String tipoError;
	/**Propiedad del tipo String que almacena el nombreArchivo*/
	private String nombreArchivo;
	/**Propiedad del tipo List<BeanAvisoTraspasos> que almacena el listaConAviso*/
	private List<BeanAvisoTraspasos> listaConAviso;
	
	
	/**
	 * @return String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo Objeto del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 * @return BeanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}
	/**
	 * @param beanPaginador Objeto del tipo BeanPaginador
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}
	/**
	 * @return Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * @param totalReg Objeto del tipo Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	/**
	 * @return String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError Objeto del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * @return String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * @param msgError Objeto del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * @return String
	 */
	public String getTipoError() {
		return tipoError;
	}
	/**
	 * @param tipoError Objeto del tipo String
	 */
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	/**
	 * @return List<BeanAvisoTraspasos>
	 */
	public List<BeanAvisoTraspasos> getListaConAviso() {
		return listaConAviso;
	}
	/**
	 * @param listaConAviso Objeto del tipo List<BeanAvisoTraspasos>
	 */
	public void setListaConAviso(List<BeanAvisoTraspasos> listaConAviso) {
		this.listaConAviso = listaConAviso;
	}
}
