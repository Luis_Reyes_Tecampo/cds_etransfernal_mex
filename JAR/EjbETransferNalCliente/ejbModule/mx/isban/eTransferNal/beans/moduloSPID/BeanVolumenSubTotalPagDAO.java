package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

import mx.isban.eTransferNal.constantes.moduloCDA.Constantes;

public class BeanVolumenSubTotalPagDAO implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6479528390217701146L;
	
	/**
	 * Propiedad del tipo Integer que almacena el valor de topoVLiquidadas
	 */
	private Integer topoVLiquidadas = Constantes.INT_CERO;
	
	/**
	 * Propiedad del tipo Integer que almacena el valor de topoTLiquidadas
	 */
	private Integer topoTLiquidadas = Constantes.INT_CERO;
	
	/**
	 * Propiedad del tipo Integer que almacena el valor de topoTPorLiquidar
	 */
	private Integer topoTPorLiquidar = Constantes.INT_CERO;
	/**
	 * Propiedad del tipo Integer que almacena el valor de rechazos
	 */
	private Integer rechazos = Constantes.INT_CERO;
	/**
	 * Metodo get que sirve para obtener la propiedad topoVLiquidadas
	 * @return topoVLiquidadas Objeto del tipo Integer
	 */
	public Integer getTopoVLiquidadas() {
		return topoVLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoVLiquidadas
	 * @param topoVLiquidadas del tipo Integer
	 */
	public void setTopoVLiquidadas(Integer topoVLiquidadas) {
		this.topoVLiquidadas = topoVLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTLiquidadas
	 * @return topoTLiquidadas Objeto del tipo Integer
	 */
	public Integer getTopoTLiquidadas() {
		return topoTLiquidadas;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTLiquidadas
	 * @param topoTLiquidadas del tipo Integer
	 */
	public void setTopoTLiquidadas(Integer topoTLiquidadas) {
		this.topoTLiquidadas = topoTLiquidadas;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad topoTPorLiquidar
	 * @return topoTPorLiquidar Objeto del tipo Integer
	 */
	public Integer getTopoTPorLiquidar() {
		return topoTPorLiquidar;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad topoTPorLiquidar
	 * @param topoTPorLiquidar del tipo Integer
	 */
	public void setTopoTPorLiquidar(Integer topoTPorLiquidar) {
		this.topoTPorLiquidar = topoTPorLiquidar;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rechazos
	 * @return rechazos Objeto del tipo Integer
	 */
	public Integer getRechazos() {
		return rechazos;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rechazos
	 * @param rechazos del tipo Integer
	 */
	public void setRechazos(Integer rechazos) {
		this.rechazos = rechazos;
	}

}
