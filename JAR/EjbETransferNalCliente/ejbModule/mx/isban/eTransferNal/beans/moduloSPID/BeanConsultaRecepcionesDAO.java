/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanConsultaRecepcionesDAO.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.beans.moduloSPID;

import java.io.Serializable;

/**
 * clase para consultar la recepciones DAO
 * @author ISBAN
 *
 */
public class  BeanConsultaRecepcionesDAO implements Serializable {
	
	/**
	 * 
	 */
	public static final long serialVersionUID = 7459618104145261850L;
	
}
