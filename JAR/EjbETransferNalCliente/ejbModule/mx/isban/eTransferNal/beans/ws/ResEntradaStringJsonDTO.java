package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

public class ResEntradaStringJsonDTO implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 2387576439723045419L;
	/**
	 * Propiedad del tipo String que almacena el valor de json
	 */
	private String json;


	
	/**
	 * Metodo get que sirve para obtener la propiedad json
	 * @return json Objeto del tipo String
	 */
	public String getJson() {
		return json;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad json
	 * @param json del tipo String
	 */
	public void setJson(String json) {
		this.json = json;
	}
}
