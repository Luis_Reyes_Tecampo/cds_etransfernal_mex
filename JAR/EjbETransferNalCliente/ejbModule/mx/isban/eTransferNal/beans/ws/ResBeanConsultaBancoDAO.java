/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ResBeanConsultaBancoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * Bean que almacena la informacion de respuesta de la consulta del banco
 * @author cchong
 */
public class ResBeanConsultaBancoDAO implements BeanResultBO,Serializable{

	/** Propiedad que almacena el valor de serialVersionUID */
	private static final long serialVersionUID = 8407465984721578801L;
	
	/** Propiedad que almacena el valor de prefijoCuentaClabe */
	private String prefijoCuentaClabe;
	
	/** Propiedad que almacena el valor de cveBanco */
	private String cveBanco;
	
	/** Propiedad que almacena el valor de nombreBanco */
	private String nombreBanco;
	
	/**Propiedad privada del tipo String que almacena el codigo de error*/
	private String codError;
	
	/**Propiedad privada del tipo String que almacena el codigo de mensaje de error*/
	private String msgError;

	/**
	 *Metodo que regresa el valor de la propiedad prefijoCuentaClabe
	 *@return prefijoCuentaClabe del tipo String regresa el valor de prefijoCuentaClabe
	 */
	public String getPrefijoCuentaClabe() {
		return prefijoCuentaClabe;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad prefijoCuentaClabe
	 * @param prefijoCuentaClabe del tipo String que sustituye el valor de la propiedad prefijoCuentaClabe
	 */
	public void setPrefijoCuentaClabe(String prefijoCuentaClabe) {
		this.prefijoCuentaClabe = prefijoCuentaClabe;
	}

	/**
	 *Metodo que regresa el valor de la propiedad cveBanco
	 *@return cveBanco del tipo String regresa el valor de cveBanco
	 */
	public String getCveBanco() {
		return cveBanco;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad cveBanco
	 * @param cveBanco del tipo String que sustituye el valor de la propiedad cveBanco
	 */
	public void setCveBanco(String cveBanco) {
		this.cveBanco = cveBanco;
	}

	/**
	 *Metodo que regresa el valor de la propiedad nombreBanco
	 *@return nombreBanco del tipo String regresa el valor de nombreBanco
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}

	/**
	 * Metodo que modifica la referencia de la propiedad nombreBanco
	 * @param nombreBanco del tipo String que sustituye el valor de la propiedad nombreBanco
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	
	     
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad codError
	   * @return codError Objeto del tipo String
	   */
	   public String getCodError(){
	      return codError;
	   }
	   /**
	   *Modifica el valor de la propiedad codError
	   * @param codError Objeto del tipo String
	   */
	   public void setCodError(String codError){
	      this.codError=codError;
	   }
	   /**
	   *Metodo get que sirve para obtener el valor de la propiedad msgError
	   * @return msgError Objeto del tipo String
	   */
	   public String getMsgError(){
	      return msgError;
	   }
	   /**
	   *Modifica el valor de la propiedad msgError
	   * @param msgError Objeto del tipo String
	   */
	   public void setMsgError(String msgError){
	      this.msgError=msgError;
	   }
	
	
}
