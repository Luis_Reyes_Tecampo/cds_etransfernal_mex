package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanReqConsultaImporteSpei implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 2306144629204627037L;
	/**
	 * Propiedad del tipo String que almacena el valor de cveOperacion
	 */
	private String cveOperacion;
	/**
	 * Propiedad del tipo String que almacena el valor de cveTransferencia
	 */
	private String cveTransferencia;
	/**
	 * Propiedad del tipo String que almacena el valor de numCuentaOrd
	 */
	private String numCuentaOrd;
	/**
	 * Propiedad del tipo String que almacena el valor de cveMedioEnt
	 */
	private String cveMedioEnt;
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private String monto;
	/**
	 * Propiedad del tipo String que almacena el valor de horaFututra
	 */
	private String horaFutura;
	/**
	 * Propiedad del tipo String que almacena el valor de esInhabil
	 */
	private String esInhabil;
	/**
	 * Metodo get que sirve para obtener la propiedad cveOperacion
	 * @return cveOperacion Objeto del tipo String
	 */
	public String getCveOperacion() {
		return cveOperacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveOperacion
	 * @param cveOperacion del tipo String
	 */
	public void setCveOperacion(String cveOperacion) {
		this.cveOperacion = cveOperacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveTransferencia
	 * @return cveTransferencia Objeto del tipo String
	 */
	public String getCveTransferencia() {
		return cveTransferencia;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveTransferencia
	 * @param cveTransferencia del tipo String
	 */
	public void setCveTransferencia(String cveTransferencia) {
		this.cveTransferencia = cveTransferencia;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numCuentaOrd
	 * @return numCuentaOrd Objeto del tipo String
	 */
	public String getNumCuentaOrd() {
		return numCuentaOrd;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numCuentaOrd
	 * @param numCuentaOrd del tipo String
	 */
	public void setNumCuentaOrd(String numCuentaOrd) {
		this.numCuentaOrd = numCuentaOrd;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad cveMedioEnt
	 * @return cveMedioEnt Objeto del tipo String
	 */
	public String getCveMedioEnt() {
		return cveMedioEnt;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad cveMedioEnt
	 * @param cveMedioEnt del tipo String
	 */
	public void setCveMedioEnt(String cveMedioEnt) {
		this.cveMedioEnt = cveMedioEnt;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo String
	 */
	public String getMonto() {
		return monto;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo String
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad horaFutura
	 * @return horaFutura Objeto del tipo String
	 */
	public String getHoraFutura() {
		return horaFutura;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad horaFutura
	 * @param horaFutura del tipo String
	 */
	public void setHoraFutura(String horaFutura) {
		this.horaFutura = horaFutura;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad esInhabil
	 * @return esInhabil Objeto del tipo String
	 */
	public String getEsInhabil() {
		return esInhabil;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad esInhabil
	 * @param esInhabil del tipo String
	 */
	public void setEsInhabil(String esInhabil) {
		this.esInhabil = esInhabil;
	}
	


}
