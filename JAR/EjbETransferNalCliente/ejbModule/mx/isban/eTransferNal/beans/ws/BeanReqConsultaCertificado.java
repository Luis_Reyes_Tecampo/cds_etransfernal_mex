package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

public class BeanReqConsultaCertificado implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -6785659414100365589L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numCertificado
	 */
	private String numCertificado;

	/**
	 * Metodo get que sirve para obtener la propiedad numCertificado
	 * @return numCertificado Objeto del tipo String
	 */
	public String getNumCertificado() {
		return numCertificado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad numCertificado
	 * @param numCertificado del tipo String
	 */
	public void setNumCertificado(String numCertificado) {
		this.numCertificado = numCertificado;
	}
	

}
