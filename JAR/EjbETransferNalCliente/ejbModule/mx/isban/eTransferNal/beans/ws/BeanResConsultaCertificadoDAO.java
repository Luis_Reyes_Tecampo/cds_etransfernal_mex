package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsultaCertificadoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5914689252568686693L;
	
	/**
	 * Propiedad del tipo List<String> que almacena el valor de certResponse
	 */
	private List<String> certResponse;

	/**
	 * Metodo get que sirve para obtener la propiedad certResponse
	 * @return certResponse Objeto del tipo List<String>
	 */
	public List<String> getCertResponse() {
		return certResponse;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad certResponse
	 * @param certResponse del tipo List<String>
	 */
	public void setCertResponse(List<String> certResponse) {
		this.certResponse = certResponse;
	}
	
	
	
	
}
