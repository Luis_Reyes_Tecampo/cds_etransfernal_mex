package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsultaImporteSpei implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 4037398604040894531L;
	
	/**Propiedad del tipo  que almacena el codigo de error */
	private String codError;
	/**Propiedad del tipo  que almacena el mensaje de error */
    private String msgError;
	/**
	 * Metodo get que sirve para obtener la propiedad codError
	 * @return codError Objeto del tipo String
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codError
	 * @param codError del tipo String
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad msgError
	 * @return msgError Objeto del tipo String
	 */
	public String getMsgError() {
		return msgError;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad msgError
	 * @param msgError del tipo String
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad serialVersionUID
	 * @return serialVersionUID Objeto del tipo long
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
    
    

}
