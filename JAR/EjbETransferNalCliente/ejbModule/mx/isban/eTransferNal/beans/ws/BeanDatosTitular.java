package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

public class BeanDatosTitular implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 9174836186364650478L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de pais
	 */
	private String pais = "";
	/**
	 * Propiedad del tipo String que almacena el valor de estado
	 */
	private String estado = "";
	/**
	 * Propiedad del tipo String que almacena el valor de localidad
	 */
	private String localidad = "";
	/**
	 * Propiedad del tipo String que almacena el valor de nombreOrganizacion
	 */
	private String nombreOrganizacion = "";
	/**
	 * Propiedad del tipo String que almacena el valor de nombreTitular
	 */
	private String nombreTitular = "";
	/**
	 * Propiedad del tipo String que almacena el valor de rfc
	 */
	private String rfc = "";
	/**
	 * Propiedad del tipo String que almacena el valor de codPostal
	 */
	private String codPostal = "";
	/**
	 * Propiedad del tipo String que almacena el valor de direccion
	 */
	private String direccion = "";
	/**
	 * Propiedad del tipo String que almacena el valor de curp
	 */
	private String curp = "";
	/**
	 * Propiedad del tipo String que almacena el valor de numPasaporteIfe
	 */
	private String numPasaporteIfe = "";
	/**
	 * Propiedad del tipo String que almacena el valor de email
	 */
	private String email = "";
	/**
	 * Metodo get que sirve para obtener la propiedad pais
	 * @return pais Objeto del tipo String
	 */
	public String getPais() {
		return pais;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad pais
	 * @param pais del tipo String
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad estado
	 * @return estado Objeto del tipo String
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad estado
	 * @param estado del tipo String
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad localidad
	 * @return localidad Objeto del tipo String
	 */
	public String getLocalidad() {
		return localidad;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad localidad
	 * @param localidad del tipo String
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad nombreOrganizacion
	 * @return nombreOrganizacion Objeto del tipo String
	 */
	public String getNombreOrganizacion() {
		return nombreOrganizacion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad nombreOrganizacion
	 * @param nombreOrganizacion del tipo String
	 */
	public void setNombreOrganizacion(String nombreOrganizacion) {
		this.nombreOrganizacion = nombreOrganizacion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad nombreTitular
	 * @return nombreTitular Objeto del tipo String
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad nombreTitular
	 * @param nombreTitular del tipo String
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad rfc
	 * @return rfc Objeto del tipo String
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad rfc
	 * @param rfc del tipo String
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad codPostal
	 * @return codPostal Objeto del tipo String
	 */
	public String getCodPostal() {
		return codPostal;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad codPostal
	 * @param codPostal del tipo String
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad direccion
	 * @return direccion Objeto del tipo String
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad direccion
	 * @param direccion del tipo String
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad curp
	 * @return curp Objeto del tipo String
	 */
	public String getCurp() {
		return curp;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad curp
	 * @param curp del tipo String
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad numPasaporteIfe
	 * @return numPasaporteIfe Objeto del tipo String
	 */
	public String getNumPasaporteIfe() {
		return numPasaporteIfe;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad numPasaporteIfe
	 * @param numPasaporteIfe del tipo String
	 */
	public void setNumPasaporteIfe(String numPasaporteIfe) {
		this.numPasaporteIfe = numPasaporteIfe;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad email
	 * @return email Objeto del tipo String
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad email
	 * @param email del tipo String
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	


}
