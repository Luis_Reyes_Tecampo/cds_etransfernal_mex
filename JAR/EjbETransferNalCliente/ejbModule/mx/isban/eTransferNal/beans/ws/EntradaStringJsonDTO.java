/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * EntradaStringJsonDTO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

/**
 * @author mcamarillo
 *
 */
public class EntradaStringJsonDTO implements Serializable {

	/**
	 * version Serializable
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * string en forma de json
	 */
	private String cadenaJson;

	/**
	 * Metodo get que sirve para obtener la propiedad cadenaJson
	 * @return cadenaJson Objeto del tipo String
	 */
	public String getCadenaJson() {
		return cadenaJson;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cadenaJson
	 * @param cadenaJson del tipo String
	 */
	public void setCadenaJson(String cadenaJson) {
		this.cadenaJson = cadenaJson;
	}
}