/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Clase: BeanResConsultaBancoSPIDDAO.java
 *
 * Control de versiones:
 * Version  Date/Hour 	   			By 	      			   Company 	    Description
 * ------- ----------------------   ---------------------  ---------    --------------------
 * 1.0      6/10/2017 01:20:45 PM 	Alan Garcia Villagran. Isban 		Creacion
 */
package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;
import java.util.List;

import mx.isban.agave.commons.interfaces.BeanResultBO;
import mx.isban.eTransferNal.ws.BeanConsultaBancoSPID;

/**
 * The Class BeanResConsultaBancoSPIDDAO.
 */
public class BeanResConsultaBancoSPIDDAO implements BeanResultBO, Serializable{

	/** Variable de serializacion. */
	private static final long serialVersionUID = 417534100868863713L;

	/** The lista bancos spid. */
	private List<BeanConsultaBancoSPID> listaBancosSPID;

	/** The cod error. */
	private String codError;

	/** The msg error. */
	private String msgError;

	/**
	 * Gets the lista bancos spid.
	 *
	 * @return the lista bancos spid
	 */
	public List<BeanConsultaBancoSPID> getListaBancosSPID() {
		return listaBancosSPID;
	}

	/**
	 * Sets the lista bancos spid.
	 *
	 * @param listaBancosSPID the new lista bancos spid
	 */
	public void setListaBancosSPID(List<BeanConsultaBancoSPID> listaBancosSPID) {
		this.listaBancosSPID = listaBancosSPID;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getCodError()
	 */
	@Override
	public String getCodError() {
		return codError;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#getMsgError()
	 */
	@Override
	public String getMsgError() {
		return msgError;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setCodError(java.lang.String)
	 */
	@Override
	public void setCodError(String codEr) {
		codError = codEr;
	}

	/* (non-Javadoc)
	 * @see mx.isban.agave.commons.interfaces.BeanResultBO#setMsgError(java.lang.String)
	 */
	@Override
	public void setMsgError(String msgEr) {
		msgError = msgEr;
	}
}