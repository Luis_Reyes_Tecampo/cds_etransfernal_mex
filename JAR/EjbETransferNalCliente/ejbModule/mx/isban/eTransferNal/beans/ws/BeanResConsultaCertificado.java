package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;

public class BeanResConsultaCertificado implements Serializable {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 8608289562790172205L;
	/**
	 * Propiedad del tipo String que almacena el valor de codRespuesta
	 */
	private String codRespuesta ="";
	/**
	 * Propiedad del tipo String que almacena el valor de mensaje
	 */
	private String mensaje="";
	/**
	 * Propiedad del tipo String que almacena el valor de numSerie
	 */
	private String numSerie="";
	/**
	 * Propiedad del tipo String que almacena el valor de fchEmision
	 */
	private String fchEmision="";
	/**
	 * Propiedad del tipo String que almacena el valor de fchExpiracion
	 */
	private String fchExpiracion="";
	/**
	 * Propiedad del tipo String que almacena el valor de estadoCertificado
	 */
	private String estadoCertificado="";
	
	/**
	 * Propiedad del tipo BeanDatosTitular que almacena el valor de datosTitular
	 */
	private BeanDatosTitular datosTitular;

	/**
	 * Metodo get que sirve para obtener la propiedad codRespuesta
	 * @return codRespuesta Objeto del tipo String
	 */
	public String getCodRespuesta() {
		return codRespuesta;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad codRespuesta
	 * @param codRespuesta del tipo String
	 */
	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad mensaje
	 * @return mensaje Objeto del tipo String
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad mensaje
	 * @param mensaje del tipo String
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad numSerie
	 * @return numSerie Objeto del tipo String
	 */
	public String getNumSerie() {
		return numSerie;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad numSerie
	 * @param numSerie del tipo String
	 */
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchEmision
	 * @return fchEmision Objeto del tipo String
	 */
	public String getFchEmision() {
		return fchEmision;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchEmision
	 * @param fchEmision del tipo String
	 */
	public void setFchEmision(String fchEmision) {
		this.fchEmision = fchEmision;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad fchExpiracion
	 * @return fchExpiracion Objeto del tipo String
	 */
	public String getFchExpiracion() {
		return fchExpiracion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad fchExpiracion
	 * @param fchExpiracion del tipo String
	 */
	public void setFchExpiracion(String fchExpiracion) {
		this.fchExpiracion = fchExpiracion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad estadoCertificado
	 * @return estadoCertificado Objeto del tipo String
	 */
	public String getEstadoCertificado() {
		return estadoCertificado;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad estadoCertificado
	 * @param estadoCertificado del tipo String
	 */
	public void setEstadoCertificado(String estadoCertificado) {
		this.estadoCertificado = estadoCertificado;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datosTitular
	 * @return datosTitular Objeto del tipo BeanDatosTitular
	 */
	public BeanDatosTitular getDatosTitular() {
		return datosTitular;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datosTitular
	 * @param datosTitular del tipo BeanDatosTitular
	 */
	public void setDatosTitular(BeanDatosTitular datosTitular) {
		this.datosTitular = datosTitular;
	}
	



}
