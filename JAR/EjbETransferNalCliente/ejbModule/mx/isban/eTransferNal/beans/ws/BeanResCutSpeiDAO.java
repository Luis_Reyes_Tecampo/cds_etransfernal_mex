package mx.isban.eTransferNal.beans.ws;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResCutSpeiDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5386550218741874325L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de salt
	 */
	private String salt;
	
	/**
	 * Propiedad del tipo String que almacena el valor de monto
	 */
	private BigDecimal monto;

	/**
	 * Propiedad del tipo String que almacena el valor de cantidad
	 */
	private int cantidad;
	/**
	 * Metodo get que sirve para obtener la propiedad salt
	 * @return salt Objeto del tipo String
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad salt
	 * @param salt del tipo String
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad monto
	 * @return monto Objeto del tipo BigDecimal
	 */
	public BigDecimal getMonto() {
		return monto;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad monto
	 * @param monto del tipo BigDecimal
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cantidad
	 * @return cantidad Objeto del tipo int
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cantidad
	 * @param cantidad del tipo int
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
