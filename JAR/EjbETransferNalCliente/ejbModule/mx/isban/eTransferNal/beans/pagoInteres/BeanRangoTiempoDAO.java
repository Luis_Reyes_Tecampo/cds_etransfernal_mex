package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanRangoTiempoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1610121715030155884L;
	
	/**
	 * Propiedad del tipo List<BeanRangoTiempo> que almacena el valor de listRangoTiempo
	 */
	private List<BeanRangoTiempo> listRangoTiempo;

	/**
	 * Metodo get que sirve para obtener la propiedad listRangoTiempo
	 * @return listRangoTiempo Objeto del tipo List<BeanRangoTiempo>
	 */
	public List<BeanRangoTiempo> getListRangoTiempo() {
		return listRangoTiempo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listRangoTiempo
	 * @param listRangoTiempo del tipo List<BeanRangoTiempo>
	 */
	public void setListRangoTiempo(List<BeanRangoTiempo> listRangoTiempo) {
		this.listRangoTiempo = listRangoTiempo;
	}
	

}
