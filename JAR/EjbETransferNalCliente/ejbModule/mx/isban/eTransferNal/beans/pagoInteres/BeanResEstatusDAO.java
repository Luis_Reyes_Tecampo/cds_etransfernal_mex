/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResTipoPagoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResEstatusDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5473977725319213591L;
	

	/**
	 * Propiedad del tipo List<BeanEstatus> que almacena el valor de estatusList
	 */
	private List<BeanEstatus> estatusList;


	/**
	 * Metodo get que sirve para obtener la propiedad estatusList
	 * @return estatusList Objeto del tipo List<BeanEstatus>
	 */
	public List<BeanEstatus> getEstatusList() {
		return estatusList;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad estatusList
	 * @param estatusList del tipo List<BeanEstatus>
	 */
	public void setEstatusList(List<BeanEstatus> estatusList) {
		this.estatusList = estatusList;
	}


	

}
