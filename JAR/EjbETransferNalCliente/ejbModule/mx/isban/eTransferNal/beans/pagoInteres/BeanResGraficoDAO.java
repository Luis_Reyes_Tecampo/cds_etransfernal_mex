/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResGraficoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResGraficoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -8609468426506215851L;
	
	/**
	 * Propiedad del tipo BeanGrafico que almacena el valor de beanGrafico
	 */
	private BeanTotalesGraficoDAO beanTotalesGraficoDAO;
	
	/**
	 * Propiedad del tipo int que almacena el valor de paginacion
	 */
	private int paginacion =0;
	
	/**
	 * Propiedad del tipo int que almacena el valor de rango en minutos
	 */
	private int rango;
	/**
	 * Propiedad del tipo List<String[]> que almacena el valor de envios
	 */
	private List<Object[]> datos;
	

	/**
	 * Metodo get que sirve para obtener la propiedad beanTotalesGraficoDAO
	 * @return beanTotalesGraficoDAO Objeto del tipo BeanTotalesGraficoDAO
	 */
	public BeanTotalesGraficoDAO getBeanTotalesGraficoDAO() {
		return beanTotalesGraficoDAO;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanTotalesGraficoDAO
	 * @param beanTotalesGraficoDAO del tipo BeanTotalesGraficoDAO
	 */
	public void setBeanTotalesGraficoDAO(BeanTotalesGraficoDAO beanTotalesGraficoDAO) {
		this.beanTotalesGraficoDAO = beanTotalesGraficoDAO;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad datos
	 * @return datos Objeto del tipo List<Object[]>
	 */
	public List<Object[]> getDatos() {
		return datos;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad datos
	 * @param datos del tipo List<Object[]>
	 */
	public void setDatos(List<Object[]> datos) {
		this.datos = datos;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad paginacion
	 * @return paginacion Objeto del tipo int
	 */
	public int getPaginacion() {
		return paginacion;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad paginacion
	 * @param paginacion del tipo int
	 */
	public void setPaginacion(int paginacion) {
		this.paginacion = paginacion;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad rango
	 * @return rango Objeto del tipo int
	 */
	public int getRango() {
		return rango;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad rango
	 * @param rango del tipo int
	 */
	public void setRango(int rango) {
		this.rango = rango;
	}

	
}
