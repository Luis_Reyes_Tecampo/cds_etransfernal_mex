/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResTipoPagoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResMedioEntDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5473977725319213591L;
	

	/**
	 * Propiedad del tipo List<BeanMedioEnt> que almacena el valor de medioEntList
	 */
	private List<BeanMedioEnt> medioEntList;


	/**
	 * Metodo get que sirve para obtener la propiedad medioEntList
	 * @return medioEntList Objeto del tipo List<BeanMedioEnt>
	 */
	public List<BeanMedioEnt> getMedioEntList() {
		return medioEntList;
	}


	/**
	 * Metodo set que modifica la referencia de la propiedad medioEntList
	 * @param medioEntList del tipo List<BeanMedioEnt>
	 */
	public void setMedioEntList(List<BeanMedioEnt> medioEntList) {
		this.medioEntList = medioEntList;
	}
	
	

}
