/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResTipoPagoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResTipoPagoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 5473977725319213591L;
	

	
	/**
	 * Propiedad del tipo List<BeanTipoPago> que almacena el valor de tipoPagoList
	 */
	private List<BeanTipoPago> tipoPagoList;



	/**
	 * Metodo get que sirve para obtener la propiedad tipoPagoList
	 * @return tipoPagoList Objeto del tipo List<BeanTipoPago>
	 */
	public List<BeanTipoPago> getTipoPagoList() {
		return tipoPagoList;
	}



	/**
	 * Metodo set que modifica la referencia de la propiedad tipoPagoList
	 * @param tipoPagoList del tipo List<BeanTipoPago>
	 */
	public void setTipoPagoList(List<BeanTipoPago> tipoPagoList) {
		this.tipoPagoList = tipoPagoList;
	}




	
	

}
