/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanGrafico.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 *Clase Bean que encapsula cada uno de los campos de respuesta 
**/
public class BeanTotalesGraficoDAO implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -5077860845670054283L;
	/**
	 * Propiedad del tipo BigDecimal que almacena el valor de montoTotal
	 */
	private BigDecimal montoTotal = BigDecimal.ZERO;
	/**
	 * Propiedad del tipo BigDecimal que almacena el valor de totalOpEnv
	 */
	private BigDecimal totalOp = BigDecimal.ZERO;
	/**
	 * Metodo get que sirve para obtener la propiedad montoTotal
	 * @return montoTotal Objeto del tipo BigDecimal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad montoTotal
	 * @param montoTotal del tipo BigDecimal
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	/**
	 * Metodo get que sirve para obtener la propiedad totalOp
	 * @return totalOp Objeto del tipo BigDecimal
	 */
	public BigDecimal getTotalOp() {
		return totalOp;
	}
	/**
	 * Metodo set que modifica la referencia de la propiedad totalOp
	 * @param totalOp del tipo BigDecimal
	 */
	public void setTotalOp(BigDecimal totalOp) {
		this.totalOp = totalOp;
	}
	
}
