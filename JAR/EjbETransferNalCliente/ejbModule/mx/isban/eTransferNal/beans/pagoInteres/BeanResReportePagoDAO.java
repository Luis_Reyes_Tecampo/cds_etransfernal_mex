/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResReportePagoDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0  09/09/2016 10:42:08 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.pagoInteres;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResReportePagoDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1901935200646986237L;
	/**
	 * Propiedad del tipo String que almacena el valor de nombreArchivo
	 */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo List<BeanReportePago> que almacena el valor de reportePagoList
	 */
	private List<BeanReportePago> reportePagoList;
	
	/**
	 * Propiedad del tipo String que almacena el valor de totalReg
	 */
	private String totalReg;

	/**
	 * Metodo get que sirve para obtener la propiedad reportePagoList
	 * @return reportePagoList Objeto del tipo List<BeanReportePago>
	 */
	public List<BeanReportePago> getReportePagoList() {
		return reportePagoList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad reportePagoList
	 * @param reportePagoList del tipo List<BeanReportePago>
	 */
	public void setReportePagoList(List<BeanReportePago> reportePagoList) {
		this.reportePagoList = reportePagoList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad totalReg
	 * @return totalReg Objeto del tipo String
	 */
	public String getTotalReg() {
		return totalReg;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad totalReg
	 * @param totalReg del tipo String
	 */
	public void setTotalReg(String totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad nombreArchivo
	 * @return nombreArchivo Objeto del tipo String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad nombreArchivo
	 * @param nombreArchivo del tipo String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	

}
