package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

/**
 * Clase Error.
 */
public class BeanError implements Serializable {

	/**
	 * Excepciones de negocio como token no valido, cuenta invalida, etc.
	 */
	public static final String TIPO_BUSSINESS = "BUSSINESS";
	/**
	 * Excepciones de ejecucion. Ej, de comunicacion, etc.
	 */
	public static final String SEVERIDAD_ALTA = "3";

	/** Constante SEVERIDAD_MEDIA. */
	public static final String SEVERIDAD_MEDIA = "2";

	/** Constante SEVERIDAD_BAJA. */
	public static final String SEVERIDAD_BAJA = "1";

	/** Constante TIPO_EXECUTION. */
	public static final String TIPO_EXECUTION = "EXECUTION";

	/** Constante CLAVE_OK. */
	public static final String CLAVE_OK = "OK";
	
	/** Constante serialVersionUID. */
	private static final long serialVersionUID = -6632379895415188364L;

	/** Variable codigo. */
	private Integer codigo;

	/** Variable clave. */
	private String clave;

	/** Variable severity. */
	private String severity;

	/** Variable message. */
	private String message;

	/** Variable tipo. */
	private String tipo;

	/**
	 * Constructor
	 */
	public BeanError() {
		super();
	}

	/**
	 * Instancia nuevo error
	 * 
	 * @param clave
	 *            the clave
	 * @param severity
	 *            the severity
	 * @param message
	 *            the message
	 */
	public BeanError(String clave, String severity, String message) {
		this.clave = clave;
		this.message = message;
		this.severity = severity;
	}

	/**
	 * Obtiene el codigo.
	 * 
	 * @return el codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Modifica el codigo.
	 * 
	 * @param codigo
	 *            el codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Obtiene el clave.
	 * 
	 * @return el clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Modifica el clave.
	 * 
	 * @param clave
	 *            el clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Obtiene el severity.
	 * 
	 * @return el severity
	 */
	public String obtieneeverity() {
		return severity;
	}

	/**
	 * Modifica el severity.
	 * 
	 * @param severity
	 *            el severity to set
	 */
	public void modificaeverity(String severity) {
		this.severity = severity;
	}

	/**
	 * Obtiene el message.
	 * 
	 * @return el message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Modifica el message.
	 * 
	 * @param message
	 *            el message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * Devuelve el valor de tipo
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Modifica el valor de tipo
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Modifica el tipo.
	 * 
	 * @return el int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + codigo;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result
				+ ((severity == null) ? 0 : severity.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	/**
	 * Modifica el tipo.
	 * 
	 * @param obj
	 *            the obj
	 * @return the int
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BeanError other = (BeanError) obj;
		if (clave == null) {
			if (other.clave != null) {
				return false;
			}
		} else if (!clave.equals(other.clave)) {
			return false;
		}
		if (codigo != other.codigo) {
			return false;
		}
		if (message == null) {
			if (other.message != null) {
				return false;
			}
		} else if (!message.equals(other.message)) {
			return false;
		}
		if (severity == null) {
			if (other.severity != null) {
				return false;
			}
		} else if (!severity.equals(other.severity)) {
			return false;
		}
		if (tipo == null) {
			if (other.tipo != null) {
				return false;
			}
		} else if (!tipo.equals(other.tipo)) {
			return false;
		}
		return true;
	}

	/**
	 * Devuelve el valor de severity
	 * @return the severity
	 */
	public String getSeverity() {
		return severity;
	}

	/**
	 * Modifica el valor de severity
	 * @param severity the severity to set
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}

}
