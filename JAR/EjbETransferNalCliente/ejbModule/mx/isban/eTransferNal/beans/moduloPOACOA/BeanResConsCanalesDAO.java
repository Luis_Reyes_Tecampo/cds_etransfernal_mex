package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsCanalesDAO extends BeanResBase implements Serializable{
	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -1034464154126610388L;

	
	/**
	 * Propiedad del tipo List<BeanResCanal> que almacena el valor de beanResCanalList
	 */
	private List<BeanCanal> beanResCanalList = null;
	/**
	 * Metodo get que obtiene el valor de la propiedad beanResCanalList
	 * @return List<BeanResCanal> Objeto de tipo @see List<BeanResCanal>
	 */
	public List<BeanCanal> getBeanResCanalList() {
		return beanResCanalList;
	}
	/**
	 * Metodo que modifica el valor de la propiedad beanResCanalList
	 * @param beanResCanalList Objeto de tipo @see List<BeanResCanal>
	 */
	public void setBeanResCanalList(List<BeanCanal> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}
	
	
}
