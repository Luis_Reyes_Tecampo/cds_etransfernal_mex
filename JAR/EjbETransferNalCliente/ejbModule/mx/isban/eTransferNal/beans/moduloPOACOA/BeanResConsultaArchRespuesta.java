/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResConsultaArchRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    26/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author ooamador
 *
 */
public class BeanResConsultaArchRespuesta extends BeanResBase implements Serializable {


	/**
	 * Numero serial
	 */
	private static final long serialVersionUID = 1L;
	
	/**Lista de resultados de la consulta*/
	private List<BeanResArchRespuesta> listaBeanResArchProc = Collections.emptyList();
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador beanPaginador = null;
	/**
	 * Devuelve el valor de totalReg
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Modifica el valor de totalReg
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Devuelve el valor de beanPaginador
	 * @return the beanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Modifica el valor de beanPaginador
	 * @param beanPaginador the beanPaginador to set
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Devuelve el valor de listaBeanResArchProc
	 * @return the listaBeanResArchProc
	 */
	public List<BeanResArchRespuesta> getListaBeanResArchProc() {
		return listaBeanResArchProc;
	}

	/**
	 * Modifica el valor de listaBeanResArchProc
	 * @param listaBeanResArchProc the listaBeanResArchProc to set
	 */
	public void setListaBeanResArchProc(List<BeanResArchRespuesta> listaBeanResArchProc) {
		this.listaBeanResArchProc = listaBeanResArchProc;
	}
	
}
