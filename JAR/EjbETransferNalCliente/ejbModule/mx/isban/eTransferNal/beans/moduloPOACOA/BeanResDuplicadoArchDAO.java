package mx.isban.eTransferNal.beans.moduloPOACOA;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResDuplicadoArchDAO extends BeanResBase{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -4312147771251782980L;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de esDuplicado
	 */
	private boolean esDuplicado;

	/**
	 * Metodo get que obtiene el valor de la propiedad esDuplicado
	 * @return boolean Objeto de tipo @see boolean
	 */
	public boolean isEsDuplicado() {
		return esDuplicado;
	}

	/**
	 * Metodo que modifica el valor de la propiedad esDuplicado
	 * @param esDuplicado Objeto de tipo @see boolean
	 */
	public void setEsDuplicado(boolean esDuplicado) {
		this.esDuplicado = esDuplicado;
	}
	
	
	
	

}
