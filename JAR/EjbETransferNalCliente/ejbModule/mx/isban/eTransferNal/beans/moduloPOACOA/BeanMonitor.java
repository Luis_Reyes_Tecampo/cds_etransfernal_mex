/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanMonitor.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:52:08 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Class BeanMonitor.
 *
 * Clase tidpo Bean que almacena los objetos
 * utilizados por lo monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanMonitor implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 4996916791400504205L;

	/** La variable que contiene informacion con respecto a: operaciones liquidadas. */
	@NotNull
	@Valid
	private BeanOperacionesLiquidadas operacionesLiquidadas;
	
	/** La variable que contiene informacion con respecto a: saldos. */
	@NotNull
	@Valid
	private BeanSaldos saldos;
	
	/** La variable que contiene informacion con respecto a: operaciones no confirmadas. */
	@NotNull
	@Valid
	private BeanOperacionesNoConfirmadas operacionesNoConfirmadas;
	
	/** La variable que contiene informacion con respecto a: error. */
	@NotNull
	@Valid
	private BeanResBase error;
	
	/** La variable que contiene informacion con respecto a: session. */
	@NotNull
	@Valid
	private BeanSessionSPID session;
	
	/**
	 * Obtener el objeto: operaciones liquidadas.
	 *
	 * @return El objeto: operaciones liquidadas
	 */
	public BeanOperacionesLiquidadas getOperacionesLiquidadas() {
		return operacionesLiquidadas;
	}
	
	/**
	 * Definir el objeto: operaciones liquidadas.
	 *
	 * @param operacionesLiquidadas El nuevo objeto: operaciones liquidadas
	 */
	public void setOperacionesLiquidadas(BeanOperacionesLiquidadas operacionesLiquidadas) {
		this.operacionesLiquidadas = operacionesLiquidadas;
	}
	
	/**
	 * Obtener el objeto: saldos.
	 *
	 * @return El objeto: saldos
	 */
	public BeanSaldos getSaldos() {
		return saldos;
	}
	
	/**
	 * Definir el objeto: saldos.
	 *
	 * @param saldos El nuevo objeto: saldos
	 */
	public void setSaldos(BeanSaldos saldos) {
		this.saldos = saldos;
	}
	
	/**
	 * Obtener el objeto: operaciones no confirmadas.
	 *
	 * @return El objeto: operaciones no confirmadas
	 */
	public BeanOperacionesNoConfirmadas getOperacionesNoConfirmadas() {
		return operacionesNoConfirmadas;
	}
	
	/**
	 * Definir el objeto: operaciones no confirmadas.
	 *
	 * @param operacionesNoConfirmadas El nuevo objeto: operaciones no confirmadas
	 */
	public void setOperacionesNoConfirmadas(BeanOperacionesNoConfirmadas operacionesNoConfirmadas) {
		this.operacionesNoConfirmadas = operacionesNoConfirmadas;
	}


	/**
	 * Obtener el objeto: error.
	 *
	 * @return El objeto: error
	 */
	public BeanResBase getError() {
		return error;
	}


	/**
	 * Definir el objeto: error.
	 *
	 * @param error El nuevo objeto: error
	 */
	public void setError(BeanResBase error) {
		this.error = error;
	}


	public BeanSessionSPID getSession() {
		return session;
	}


	public void setSession(BeanSessionSPID session) {
		this.session = session;
	}
	
}
