/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqCanal.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * @author ooamador
 *
 */
public class BeanReqCanal implements Serializable {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cmbCanal
	 */
	private String cmbCanal;
	
	/** Identificador canal */
	private String idCanal;
	
	/** Estatus del canal activado/desactivado. */
	private boolean activoCanal;
	
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
    private BeanPaginador paginador = null;
    
    /**Propiedad privada del tipo Integer que almacena el total de registros*/
    private Integer totalReg;

	/**
	 * Devuelve el valor de idCanal
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * Modifica el valor de idCanal
	 * @param idCanal the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * Devuelve el valor de activoCanal
	 * @return the activoCanal
	 */
	public boolean isActivoCanal() {
		return activoCanal;
	}

	/**
	 * Modifica el valor de activoCanal
	 * @param activoCanal the activoCanal to set
	 */
	public void setActivoCanal(boolean activoCanal) {
		this.activoCanal = activoCanal;
	}

	/**
	 * Devuelve el valor de paginador
	 * @return the paginador
	 */
	public BeanPaginador getPaginador() {
		return paginador;
	}

	/**
	 * Modifica el valor de paginador
	 * @param paginador the paginador to set
	 */
	public void setPaginador(BeanPaginador paginador) {
		this.paginador = paginador;
	}

	/**
	 * Devuelve el valor de totalReg
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Modifica el valor de totalReg
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad cmbCanal
	 * @return cmbCanal Objeto del tipo String
	 */
	public String getCmbCanal() {
		return cmbCanal;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad cmbCanal
	 * @param cmbCanal del tipo String
	 */
	public void setCmbCanal(String cmbCanal) {
		this.cmbCanal = cmbCanal;
	}


}
