/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionesNoConfirmadas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:50:06 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Class BeanOperacionesNoConfirmadas.
 *
 * Bean que contiene los atributos para almacenar la informacion
 * respecto al panel de Operaciones No confirmadas de los monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanOperacionesNoConfirmadas implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -6077821239346799415L;

	/** La variable que contiene informacion con respecto a: traspasos. */
	@NotNull
	@Valid
	private BeanTraspasos traspasos;
	
	/** La variable que contiene informacion con respecto a: ordenes. */
	@NotNull
	@Valid
	private BeanOrdenes ordenes;
	
	/**
	 * Obtener el objeto: traspasos.
	 *
	 * @return El objeto: traspasos
	 */
	public BeanTraspasos getTraspasos() {
		return traspasos;
	}
	
	/**
	 * Definir el objeto: traspasos.
	 *
	 * @param traspasos El nuevo objeto: traspasos
	 */
	public void setTraspasos(BeanTraspasos traspasos) {
		this.traspasos = traspasos;
	}
	
	/**
	 * Obtener el objeto: ordenes.
	 *
	 * @return El objeto: ordenes
	 */
	public BeanOrdenes getOrdenes() {
		return ordenes;
	}
	
	/**
	 * Definir el objeto: ordenes.
	 *
	 * @param ordenes El nuevo objeto: ordenes
	 */
	public void setOrdenes(BeanOrdenes ordenes) {
		this.ordenes = ordenes;
	}
}
