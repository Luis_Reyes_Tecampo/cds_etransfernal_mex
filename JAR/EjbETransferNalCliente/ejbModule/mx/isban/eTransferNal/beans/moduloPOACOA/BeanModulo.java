/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanModulo.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 05:03:04 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * Class BeanModulo.
 *
 * Clase tipo bean para guardar el tipo 
 * de monitor que se esta utilizando
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanModulo implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5219060494039004758L;

	/** La variable que contiene informacion con respecto a: modulo. */
	@NotNull
	private String modulo;
	
	/** La variable que contiene informacion con respecto a: tipo. */
	@NotNull
	private String tipo;

	/** La variable que contiene informacion con respecto a: path. */
	@NotNull
	private String path;
	/**
	 * Obtener el objeto: modulo.
	 *
	 * @return El objeto: modulo
	 */
	public String getModulo() {
		return modulo;
	}

	/**
	 * Definir el objeto: modulo.
	 *
	 * @param modulo El nuevo objeto: modulo
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	/**
	 * Obtener el objeto: tipo.
	 *
	 * @return El objeto: tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Definir el objeto: tipo.
	 *
	 * @param tipo El nuevo objeto: tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
}
