/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanReqArchPago.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

/**
 * @author ooamador
 *
 */
public class BeanReqArchPago implements Serializable {

	/**
	 * Numero serial
	 */
	private static final long serialVersionUID = 1L;
	
	/** Nombre de archivo */
	private String nombreArchivo;
	
	/** Clave medio de entrega */
	private String cveMedioEntrega;
	

	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;

	/**
	 * Devuelve el valor de nombreArchivo
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Modifica el valor de nombreArchivo
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Devuelve el valor de cveMedioEntrega
	 * @return the cveMedioEntrega
	 */
	public String getCveMedioEntrega() {
		return cveMedioEntrega;
	}

	/**
	 * Modifica el valor de cveMedioEntrega
	 * @param cveMedioEntrega the cveMedioEntrega to set
	 */
	public void setCveMedioEntrega(String cveMedioEntrega) {
		this.cveMedioEntrega = cveMedioEntrega;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad fchOperacion
	 * @return String Objeto de tipo @see String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fchOperacion
	 * @param fchOperacion Objeto de tipo @see String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

}
