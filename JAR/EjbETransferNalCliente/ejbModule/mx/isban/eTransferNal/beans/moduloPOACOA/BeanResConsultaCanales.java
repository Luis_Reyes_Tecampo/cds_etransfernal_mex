/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanConsultaCanales.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    26/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import mx.isban.eTransferNal.beans.catalogos.BeanMedEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * @author ooamador
 *
 */
public class BeanResConsultaCanales extends BeanResBase implements Serializable {


	/**
	 * Numero serial
	 */
	private static final long serialVersionUID = 1L;

	/**Lista de resultados de la consulta*/
	private List<BeanCanal> listaBeanResCanal = Collections.emptyList();
	
	/**
	 * Propiedad del tipo List<BeanResCanal> que almacena el valor de beanResCanalList
	 */
	private List<BeanCanal> beanResCanalList = Collections.emptyList();
	
	/**
	 * Propiedad del tipo BeanMedEntrega que almacena el valor de beanMedioEntrega
	 */
	private List<BeanMedEntrega> beanMedioEntregaList;
	
	/**Propiedad del tipo Integer que almacena el toral de registors*/
	private Integer totalReg = 0;
	
	/**Propiedad del tipo BeanPaginador que encapsula la funcionalidad de paginar*/
	private BeanPaginador beanPaginador = null;

	/**
	 * Devuelve el valor de listaBeanResCanal
	 * @return the listaBeanResCanal
	 */
	public List<BeanCanal> getListaBeanResCanal() {
		return listaBeanResCanal;
	}

	/**
	 * Modifica el valor de listaBeanResCanal
	 * @param listaBeanResCanal the listaBeanResCanal to set
	 */
	public void setListaBeanResCanal(List<BeanCanal> listaBeanResCanal) {
		this.listaBeanResCanal = listaBeanResCanal;
	}

	/**
	 * Devuelve el valor de totalReg
	 * @return the totalReg
	 */
	public Integer getTotalReg() {
		return totalReg;
	}

	/**
	 * Modifica el valor de totalReg
	 * @param totalReg the totalReg to set
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}

	/**
	 * Devuelve el valor de beanPaginador
	 * @return the beanPaginador
	 */
	public BeanPaginador getBeanPaginador() {
		return beanPaginador;
	}

	/**
	 * Modifica el valor de beanPaginador
	 * @param beanPaginador the beanPaginador to set
	 */
	public void setBeanPaginador(BeanPaginador beanPaginador) {
		this.beanPaginador = beanPaginador;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad beanResCanalList
	 * @return beanResCanalList Objeto del tipo List<BeanCanal>
	 */
	public List<BeanCanal> getBeanResCanalList() {
		return beanResCanalList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanResCanalList
	 * @param beanResCanalList del tipo List<BeanCanal>
	 */
	public void setBeanResCanalList(List<BeanCanal> beanResCanalList) {
		this.beanResCanalList = beanResCanalList;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad beanMedioEntregaList
	 * @return beanMedioEntregaList Objeto del tipo List<BeanMedEntrega>
	 */
	public List<BeanMedEntrega> getBeanMedioEntregaList() {
		return beanMedioEntregaList;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad beanMedioEntregaList
	 * @param beanMedioEntregaList del tipo List<BeanMedEntrega>
	 */
	public void setBeanMedioEntregaList(List<BeanMedEntrega> beanMedioEntregaList) {
		this.beanMedioEntregaList = beanMedioEntregaList;
	}
	
	
}
