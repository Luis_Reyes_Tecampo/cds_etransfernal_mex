/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanRecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:41:05 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Class BeanRecepciones.
 *
 * Clase tipo Bean que contiene los atributos de las 
 * recpeciones mostradas en el monitor POACOA 
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanRecepciones implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -7876396289222705848L;

	/** La variable que contiene informacion con respecto a: ordenes por aplicar. */
	@NotNull
	@Valid
	private BeanSaldo ordenesPorAplicar;
	
	/** La variable que contiene informacion con respecto a: ordenes aplicadas. */
	@NotNull
	@Valid
	private BeanSaldo ordenesAplicadas;
	
	/** La variable que contiene informacion con respecto a: ordenes rechazadas. */
	@NotNull
	@Valid
	private BeanSaldo ordenesRechazadas;
	
	/**
	 * Obtener el objeto: ordenes por aplicar.
	 *
	 * @return El objeto: ordenes por aplicar
	 */
	public BeanSaldo getOrdenesPorAplicar() {
		return ordenesPorAplicar;
	}
	
	/**
	 * Definir el objeto: ordenes por aplicar.
	 *
	 * @param ordenesPorAplicar El nuevo objeto: ordenes por aplicar
	 */
	public void setOrdenesPorAplicar(BeanSaldo ordenesPorAplicar) {
		this.ordenesPorAplicar = ordenesPorAplicar;
	}
	
	/**
	 * Obtener el objeto: ordenes aplicadas.
	 *
	 * @return El objeto: ordenes aplicadas
	 */
	public BeanSaldo getOrdenesAplicadas() {
		return ordenesAplicadas;
	}
	
	/**
	 * Definir el objeto: ordenes aplicadas.
	 *
	 * @param ordenesAplicadas El nuevo objeto: ordenes aplicadas
	 */
	public void setOrdenesAplicadas(BeanSaldo ordenesAplicadas) {
		this.ordenesAplicadas = ordenesAplicadas;
	}
	
	/**
	 * Obtener el objeto: ordenes rechazadas.
	 *
	 * @return El objeto: ordenes rechazadas
	 */
	public BeanSaldo getOrdenesRechazadas() {
		return ordenesRechazadas;
	}
	
	/**
	 * Definir el objeto: ordenes rechazadas.
	 *
	 * @param ordenesRechazadas El nuevo objeto: ordenes rechazadas
	 */
	public void setOrdenesRechazadas(BeanSaldo ordenesRechazadas) {
		this.ordenesRechazadas = ordenesRechazadas;
	}
	
}
