/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanSaldos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:36:14 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class BeanSaldos.
 *
 * Clase tipo Bean que alamacena los atributos de 
 * la seccion saldos para los monitores PAOCOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanSaldos implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 9098257375759589909L;

	/** La variable que contiene informacion con respecto a: saldo calculado. */
	@NotNull
	@Size(min=1)
	private String saldoCalculado;
	
	/** La variable que contiene informacion con respecto a: saldo no reservado. */
	@NotNull
	@Size(min=1)
	private String saldoNoReservado;
	
	/** La variable que contiene informacion con respecto a: saldo reservado. */
	@NotNull
	@Size(min=1)
	private String saldoReservado;
	
	/** La variable que contiene informacion con respecto a: diferencia. */
	@NotNull
	@Size(min=1)
	private String diferencia;
	
	/** La variable que contiene informacion con respecto a: devoluciones envidadas. */
	@NotNull
	@Valid
	private BeanSaldo devolucionesEnvidadas;
	
	/**
	 * Obtener el objeto: saldo calculado.
	 *
	 * @return El objeto: saldo calculado
	 */
	public String getSaldoCalculado() {
		return saldoCalculado;
	}
	
	/**
	 * Definir el objeto: saldo calculado.
	 *
	 * @param saldoCalculado El nuevo objeto: saldo calculado
	 */
	public void setSaldoCalculado(String saldoCalculado) {
		this.saldoCalculado = saldoCalculado;
	}
	
	/**
	 * Obtener el objeto: saldo no reservado.
	 *
	 * @return El objeto: saldo no reservado
	 */
	public String getSaldoNoReservado() {
		return saldoNoReservado;
	}
	
	/**
	 * Definir el objeto: saldo no reservado.
	 *
	 * @param saldoNoReservado El nuevo objeto: saldo no reservado
	 */
	public void setSaldoNoReservado(String saldoNoReservado) {
		this.saldoNoReservado = saldoNoReservado;
	}
	
	/**
	 * Obtener el objeto: saldo reservado.
	 *
	 * @return El objeto: saldo reservado
	 */
	public String getSaldoReservado() {
		return saldoReservado;
	}
	
	/**
	 * Definir el objeto: saldo reservado.
	 *
	 * @param saldoReservado El nuevo objeto: saldo reservado
	 */
	public void setSaldoReservado(String saldoReservado) {
		this.saldoReservado = saldoReservado;
	}
	
	/**
	 * Obtener el objeto: diferencia.
	 *
	 * @return El objeto: diferencia
	 */
	public String getDiferencia() {
		return diferencia;
	}
	
	/**
	 * Definir el objeto: diferencia.
	 *
	 * @param diferencia El nuevo objeto: diferencia
	 */
	public void setDiferencia(String diferencia) {
		this.diferencia = diferencia;
	}
	
	/**
	 * Obtener el objeto: devoluciones envidadas.
	 *
	 * @return El objeto: devoluciones envidadas
	 */
	public BeanSaldo getDevolucionesEnvidadas() {
		return devolucionesEnvidadas;
	}
	
	/**
	 * Definir el objeto: devoluciones envidadas.
	 *
	 * @param devolucionesEnvidadas El nuevo objeto: devoluciones envidadas
	 */
	public void setDevolucionesEnvidadas(BeanSaldo devolucionesEnvidadas) {
		this.devolucionesEnvidadas = devolucionesEnvidadas;
	}
	
}
