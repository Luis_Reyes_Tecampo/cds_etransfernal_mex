/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BeanResArchProc.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

/**
 * @author ooamador
 *
 */
public class BeanResArchRespuesta implements Serializable {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 650168329008225123L;

	/** Nombre del archivo de respuesta */
	private String nombreArchivo;
	
	/** estatus del archivo de respuesta */
	private String estatus;
	
	/** descripcion del archivo de respuesta */
	private String descripcion;

	/**
	 * Propiedad del tipo String que almacena el valor de totalMonto
	 */
	private String totalMonto;
	
	/**
	 * Propiedad del tipo String que almacena el valor de numPagos
	 */
	private String numPagos;
	
	/** bandera renglon */
	private boolean banderaRenglon;

	/**
	 * Devuelve el valor de nombreArchivo
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * Modifica el valor de nombreArchivo
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * Devuelve el valor de estatus
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * Modifica el valor de estatus
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * Devuelve el valor de descripcion
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modifica el valor de descripcion
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Devuelve el valor de banderaRenglon
	 * @return the banderaRenglon
	 */
	public boolean isBanderaRenglon() {
		return banderaRenglon;
	}

	/**
	 * Modifica el valor de banderaRenglon
	 * @param banderaRenglon the banderaRenglon to set
	 */
	public void setBanderaRenglon(boolean banderaRenglon) {
		this.banderaRenglon = banderaRenglon;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad totalMonto
	 * @return String Objeto de tipo @see String
	 */
	public String getTotalMonto() {
		return totalMonto;
	}

	/**
	 * Metodo que modifica el valor de la propiedad totalMonto
	 * @param totalMonto Objeto de tipo @see String
	 */
	public void setTotalMonto(String totalMonto) {
		this.totalMonto = totalMonto;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad numPagos
	 * @return String Objeto de tipo @see String
	 */
	public String getNumPagos() {
		return numPagos;
	}

	/**
	 * Metodo que modifica el valor de la propiedad numPagos
	 * @param numPagos Objeto de tipo @see String
	 */
	public void setNumPagos(String numPagos) {
		this.numPagos = numPagos;
	}

}
