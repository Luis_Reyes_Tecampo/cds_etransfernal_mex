/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOrdenes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:33:41 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Class BeanOrdenes.
 *
 * Clase que almacena los campos para las ordenes
 * que se muestran en los monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanOrdenes implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1658465206072272308L;

	/** La variable que contiene informacion con respecto a: ordenes espera. */
	@NotNull
	@Valid
	private BeanSaldo ordenesEspera;
	
	/** La variable que contiene informacion con respecto a: ordenes enviadas. */
	@NotNull
	@Valid
	private BeanSaldo ordenesEnviadas;
	
	/** La variable que contiene informacion con respecto a: ordenes por reparar. */
	@NotNull
	@Valid
	private BeanSaldo ordenesPorReparar;
	
	/**
	 * Obtener el objeto: ordenes espera.
	 *
	 * @return El objeto: ordenes espera
	 */
	public BeanSaldo getOrdenesEspera() {
		return ordenesEspera;
	}
	
	/**
	 * Definir el objeto: ordenes espera.
	 *
	 * @param ordenesEspera El nuevo objeto: ordenes espera
	 */
	public void setOrdenesEspera(BeanSaldo ordenesEspera) {
		this.ordenesEspera = ordenesEspera;
	}
	
	/**
	 * Obtener el objeto: ordenes enviadas.
	 *
	 * @return El objeto: ordenes enviadas
	 */
	public BeanSaldo getOrdenesEnviadas() {
		return ordenesEnviadas;
	}
	
	/**
	 * Definir el objeto: ordenes enviadas.
	 *
	 * @param ordenesEnviadas El nuevo objeto: ordenes enviadas
	 */
	public void setOrdenesEnviadas(BeanSaldo ordenesEnviadas) {
		this.ordenesEnviadas = ordenesEnviadas;
	}
	
	/**
	 * Obtener el objeto: ordenes por reparar.
	 *
	 * @return El objeto: ordenes por reparar
	 */
	public BeanSaldo getOrdenesPorReparar() {
		return ordenesPorReparar;
	}
	
	/**
	 * Definir el objeto: ordenes por reparar.
	 *
	 * @param ordenesPorReparar El nuevo objeto: ordenes por reparar
	 */
	public void setOrdenesPorReparar(BeanSaldo ordenesPorReparar) {
		this.ordenesPorReparar = ordenesPorReparar;
	}
	
}
