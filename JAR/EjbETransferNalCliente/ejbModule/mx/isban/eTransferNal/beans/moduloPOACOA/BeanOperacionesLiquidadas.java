/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanOperacionesLiquidadas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:47:22 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class BeanOperacionesLiquidadas.
 *
 * Bean que contiene los atributos para almacenar la informacion
 * respecto al panel de Operaciones Liquidadas de los monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanOperacionesLiquidadas implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -2064030882466883876L;

	/** La variable que contiene informacion con respecto a: saldo. */
	@NotNull
	@Size(min=1)
	private String saldo;
	
	/** La variable que contiene informacion con respecto a: recepciones. */
	@NotNull
	@Valid
	private BeanRecepciones recepciones;
	
	/** La variable que contiene informacion con respecto a: traspaso orden. */
	@NotNull
	@Valid
	private BeanTraspasoOrden traspasoOrden;
	
	/**
	 * Obtener el objeto: saldo.
	 *
	 * @return El objeto: saldo
	 */
	public String getSaldo() {
		return saldo;
	}
	
	/**
	 * Definir el objeto: saldo.
	 *
	 * @param saldo El nuevo objeto: saldo
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
	/**
	 * Obtener el objeto: recepciones.
	 *
	 * @return El objeto: recepciones
	 */
	public BeanRecepciones getRecepciones() {
		return recepciones;
	}
	
	/**
	 * Definir el objeto: recepciones.
	 *
	 * @param recepciones El nuevo objeto: recepciones
	 */
	public void setRecepciones(BeanRecepciones recepciones) {
		this.recepciones = recepciones;
	}
	
	/**
	 * Obtener el objeto: traspaso orden.
	 *
	 * @return El objeto: traspaso orden
	 */
	public BeanTraspasoOrden getTraspasoOrden() {
		return traspasoOrden;
	}
	
	/**
	 * Definir el objeto: traspaso orden.
	 *
	 * @param traspasoOrden El nuevo objeto: traspaso orden
	 */
	public void setTraspasoOrden(BeanTraspasoOrden traspasoOrden) {
		this.traspasoOrden = traspasoOrden;
	}
	
}
