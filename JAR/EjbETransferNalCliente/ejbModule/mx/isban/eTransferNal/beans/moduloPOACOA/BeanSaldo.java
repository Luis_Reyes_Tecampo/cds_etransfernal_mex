/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanSaldo.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:27:02 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class BeanSaldo.
 *
 * Clase tipo Bean que almacena saldo y volumen
 * los cuales se usaran en los objetos genericos
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanSaldo implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8354721716809125367L;

	/** La variable que contiene informacion con respecto a: saldo. */
	@NotNull
	@Size(min=1)
	private String saldo;
	
	/** La variable que contiene informacion con respecto a: volumen. */
	@NotNull
	@Size(min=1)
	private String volumen;
	
	/**
	 * Obtener el objeto: saldo.
	 *
	 * @return El objeto: saldo
	 */
	public String getSaldo() {
		return saldo;
	}
	
	/**
	 * Definir el objeto: saldo.
	 *
	 * @param saldo El nuevo objeto: saldo
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
	/**
	 * Obtener el objeto: volumen.
	 *
	 * @return El objeto: volumen
	 */
	public String getVolumen() {
		return volumen;
	}
	
	/**
	 * Definir el objeto: volumen.
	 *
	 * @param volumen El nuevo objeto: volumen
	 */
	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}
	
}
