/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTraspasos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:31:57 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Class BeanTraspasos.
 *
 * Clase que almacena los campos para los traspasos
 * que se muestran en los monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanTraspasos implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -475422332286903306L;
	
	/** La variable que contiene informacion con respecto a: traspasos espera. */
	@NotNull
	@Valid
	private BeanSaldo traspasosEspera;
	
	/** La variable que contiene informacion con respecto a: traspasos enviados. */
	@NotNull
	@Valid
	private BeanSaldo traspasosEnviados;
	
	/** La variable que contiene informacion con respecto a: traspasos por reparar. */
	@NotNull
	@Valid
	private BeanSaldo traspasosPorReparar;
	
	/**
	 * Obtener el objeto: traspasos espera.
	 *
	 * @return El objeto: traspasos espera
	 */
	public BeanSaldo getTraspasosEspera() {
		return traspasosEspera;
	}
	
	/**
	 * Definir el objeto: traspasos espera.
	 *
	 * @param traspasosEspera El nuevo objeto: traspasos espera
	 */
	public void setTraspasosEspera(BeanSaldo traspasosEspera) {
		this.traspasosEspera = traspasosEspera;
	}
	
	/**
	 * Obtener el objeto: traspasos enviados.
	 *
	 * @return El objeto: traspasos enviados
	 */
	public BeanSaldo getTraspasosEnviados() {
		return traspasosEnviados;
	}
	
	/**
	 * Definir el objeto: traspasos enviados.
	 *
	 * @param traspasosEnviados El nuevo objeto: traspasos enviados
	 */
	public void setTraspasosEnviados(BeanSaldo traspasosEnviados) {
		this.traspasosEnviados = traspasosEnviados;
	}
	
	/**
	 * Obtener el objeto: traspasos por reparar.
	 *
	 * @return El objeto: traspasos por reparar
	 */
	public BeanSaldo getTraspasosPorReparar() {
		return traspasosPorReparar;
	}
	
	/**
	 * Definir el objeto: traspasos por reparar.
	 *
	 * @param traspasosPorReparar El nuevo objeto: traspasos por reparar
	 */
	public void setTraspasosPorReparar(BeanSaldo traspasosPorReparar) {
		this.traspasosPorReparar = traspasosPorReparar;
	}
	
}
