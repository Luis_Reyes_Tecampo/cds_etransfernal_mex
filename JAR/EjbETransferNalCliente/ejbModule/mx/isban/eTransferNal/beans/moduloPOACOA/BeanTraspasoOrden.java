/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: BeanTraspasoOrden.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     29/07/2019 11:44:36 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Class BeanTraspasoOrden.
 *
 * Clase tipo bean que contiene atributos para las operaciones
 * y traspasos por tipo de los monitores POACOA
 * 
 * @author FSW-Vector
 * @since 29/07/2019
 */
public class BeanTraspasoOrden implements Serializable{

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 3424147560537082788L;

	/** La variable que contiene informacion con respecto a: traspaso SIAC tipo. */
	@NotNull
	@Valid
	private BeanSaldo traspasoSIACTipo;
	
	/** La variable que contiene informacion con respecto a: ordenes por devolver. */
	@NotNull
	@Valid
	private BeanSaldo ordenesPorDevolver;
	
	/** La variable que contiene informacion con respecto a: traspaso tipo SIAC. */
	@NotNull
	@Valid
	private BeanSaldo traspasoTipoSIAC;
	
	/** La variable que contiene informacion con respecto a: ordenes envidas confirmadas. */
	@NotNull
	@Valid
	private BeanSaldo ordenesEnvidasConfirmadas;
	
	/**
	 * Obtener el objeto: traspaso SIAC tipo.
	 *
	 * @return El objeto: traspaso SIAC tipo
	 */
	public BeanSaldo getTraspasoSIACTipo() {
		return traspasoSIACTipo;
	}
	
	/**
	 * Definir el objeto: traspaso SIAC tipo.
	 *
	 * @param traspasoSIACTipo El nuevo objeto: traspaso SIAC tipo
	 */
	public void setTraspasoSIACTipo(BeanSaldo traspasoSIACTipo) {
		this.traspasoSIACTipo = traspasoSIACTipo;
	}
	
	/**
	 * Obtener el objeto: ordenes por devolver.
	 *
	 * @return El objeto: ordenes por devolver
	 */
	public BeanSaldo getOrdenesPorDevolver() {
		return ordenesPorDevolver;
	}
	
	/**
	 * Definir el objeto: ordenes por devolver.
	 *
	 * @param ordenesPorDevolver El nuevo objeto: ordenes por devolver
	 */
	public void setOrdenesPorDevolver(BeanSaldo ordenesPorDevolver) {
		this.ordenesPorDevolver = ordenesPorDevolver;
	}
	
	/**
	 * Obtener el objeto: traspaso tipo SIAC.
	 *
	 * @return El objeto: traspaso tipo SIAC
	 */
	public BeanSaldo getTraspasoTipoSIAC() {
		return traspasoTipoSIAC;
	}
	
	/**
	 * Definir el objeto: traspaso tipo SIAC.
	 *
	 * @param traspasoTipoSIAC El nuevo objeto: traspaso tipo SIAC
	 */
	public void setTraspasoTipoSIAC(BeanSaldo traspasoTipoSIAC) {
		this.traspasoTipoSIAC = traspasoTipoSIAC;
	}
	
	/**
	 * Obtener el objeto: ordenes envidas confirmadas.
	 *
	 * @return El objeto: ordenes envidas confirmadas
	 */
	public BeanSaldo getOrdenesEnvidasConfirmadas() {
		return ordenesEnvidasConfirmadas;
	}
	
	/**
	 * Definir el objeto: ordenes envidas confirmadas.
	 *
	 * @param ordenesEnvidasConfirmadas El nuevo objeto: ordenes envidas confirmadas
	 */
	public void setOrdenesEnvidasConfirmadas(BeanSaldo ordenesEnvidasConfirmadas) {
		this.ordenesEnvidasConfirmadas = ordenesEnvidasConfirmadas;
	}
	
}
