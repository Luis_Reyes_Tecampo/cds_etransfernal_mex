package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResMonCargaArchCanContingDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 6637194798071823752L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de nombreArchivo
	 */
	private String nombreArchivo;
	/**
	 * Propiedad del tipo String que almacena el valor de estatus
	 */
	private String estatus;
	/**
	 * Propiedad del tipo String que almacena el valor de numPagos
	 */
	private String numPagos;
	/**
	 * Propiedad del tipo String que almacena el valor de numPagosErr
	 */
	private String numPagosErr;
	/**
	 * Propiedad del tipo String que almacena el valor de totalMonto
	 */
	private String totalMonto;
	/**
	 * Propiedad del tipo String que almacena el valor de totalMontoErr
	 */
	private String totalMontoErr;
	
	/**Propiedad privada del tipo Integer que almacena el total de registros*/
	private Integer totalReg;
	
	/**
	 * Propiedad del tipo List<BeanMonCargaArchCanConting> que almacena el valor de listBeanMonCargaArchCanContig
	 */
	private List<BeanMonCargaArchCanConting> listBeanMonCargaArchCanContig;
	
	/**
	 * Metodo get que obtiene el valor de la propiedad nombreArchivo
	 * @return String Objeto de tipo @see String
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * Metodo que modifica el valor de la propiedad nombreArchivo
	 * @param nombreArchivo Objeto de tipo @see String
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad estatus
	 * @return String Objeto de tipo @see String
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Metodo que modifica el valor de la propiedad estatus
	 * @param estatus Objeto de tipo @see String
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numPagos
	 * @return String Objeto de tipo @see String
	 */
	public String getNumPagos() {
		return numPagos;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numPagos
	 * @param numPagos Objeto de tipo @see String
	 */
	public void setNumPagos(String numPagos) {
		this.numPagos = numPagos;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad numPagosErr
	 * @return String Objeto de tipo @see String
	 */
	public String getNumPagosErr() {
		return numPagosErr;
	}
	/**
	 * Metodo que modifica el valor de la propiedad numPagosErr
	 * @param numPagosErr Objeto de tipo @see String
	 */
	public void setNumPagosErr(String numPagosErr) {
		this.numPagosErr = numPagosErr;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalMonto
	 * @return String Objeto de tipo @see String
	 */
	public String getTotalMonto() {
		return totalMonto;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalMonto
	 * @param totalMonto Objeto de tipo @see String
	 */
	public void setTotalMonto(String totalMonto) {
		this.totalMonto = totalMonto;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalMontoErr
	 * @return String Objeto de tipo @see String
	 */
	public String getTotalMontoErr() {
		return totalMontoErr;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalMontoErr
	 * @param totalMontoErr Objeto de tipo @see String
	 */
	public void setTotalMontoErr(String totalMontoErr) {
		this.totalMontoErr = totalMontoErr;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad listBeanMonCargaArchCanContig
	 * @return List<BeanMonCargaArchCanConting> Objeto de tipo @see List<BeanMonCargaArchCanConting>
	 */
	public List<BeanMonCargaArchCanConting> getListBeanMonCargaArchCanContig() {
		return listBeanMonCargaArchCanContig;
	}
	/**
	 * Metodo que modifica el valor de la propiedad listBeanMonCargaArchCanContig
	 * @param listBeanMonCargaArchCanContig Objeto de tipo @see List<BeanMonCargaArchCanConting>
	 */
	public void setListBeanMonCargaArchCanContig(
			List<BeanMonCargaArchCanConting> listBeanMonCargaArchCanContig) {
		this.listBeanMonCargaArchCanContig = listBeanMonCargaArchCanContig;
	}
	/**
	 * Metodo get que obtiene el valor de la propiedad totalReg
	 * @return Integer Objeto de tipo @see Integer
	 */
	public Integer getTotalReg() {
		return totalReg;
	}
	/**
	 * Metodo que modifica el valor de la propiedad totalReg
	 * @param totalReg Objeto de tipo @see Integer
	 */
	public void setTotalReg(Integer totalReg) {
		this.totalReg = totalReg;
	}
	
	

}
