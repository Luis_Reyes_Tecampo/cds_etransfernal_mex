package mx.isban.eTransferNal.beans.moduloPOACOA;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResValInicioPOACOA extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = -7566135153241883030L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de cveMiInstitucion
	 */
	private String cveMiInstitucion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de noHayRetorno
	 */
	private String noHayRetorno;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fchOperacion
	 */
	private String fchOperacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de activacion
	 */
	private String activacion;
	
	/**
	 * Propiedad del tipo String que almacena el valor de fase
	 */
	private String fase;
	/**
	 * Propiedad del tipo String que almacena el valor de generar
	 */
	private String generar;
	
	/**
	 * Propiedad del tipo boolean que almacena el valor de isActivo
	 */
	private boolean activoPOACOA;


	
	/**
	 * Metodo get que obtiene el valor de la propiedad cveMiInstitucion
	 * @return String Objeto de tipo @see String
	 */
	public String getCveMiInstitucion() {
		return cveMiInstitucion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad cveMiInstitucion
	 * @param cveMiInstitucion Objeto de tipo @see String
	 */
	public void setCveMiInstitucion(String cveMiInstitucion) {
		this.cveMiInstitucion = cveMiInstitucion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad noHayRetorno
	 * @return String Objeto de tipo @see String
	 */
	public String getNoHayRetorno() {
		return noHayRetorno;
	}

	/**
	 * Metodo que modifica el valor de la propiedad noHayRetorno
	 * @param noHayRetorno Objeto de tipo @see String
	 */
	public void setNoHayRetorno(String noHayRetorno) {
		this.noHayRetorno = noHayRetorno;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad fchOperacion
	 * @return String Objeto de tipo @see String
	 */
	public String getFchOperacion() {
		return fchOperacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fchOperacion
	 * @param fchOperacion Objeto de tipo @see String
	 */
	public void setFchOperacion(String fchOperacion) {
		this.fchOperacion = fchOperacion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad activacion
	 * @return String Objeto de tipo @see String
	 */
	public String getActivacion() {
		return activacion;
	}

	/**
	 * Metodo que modifica el valor de la propiedad activacion
	 * @param activacion Objeto de tipo @see String
	 */
	public void setActivacion(String activacion) {
		this.activacion = activacion;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad fase
	 * @return String Objeto de tipo @see String
	 */
	public String getFase() {
		return fase;
	}

	/**
	 * Metodo que modifica el valor de la propiedad fase
	 * @param fase Objeto de tipo @see String
	 */
	public void setFase(String fase) {
		this.fase = fase;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad generar
	 * @return String Objeto de tipo @see String
	 */
	public String getGenerar() {
		return generar;
	}

	/**
	 * Metodo que modifica el valor de la propiedad generar
	 * @param generar Objeto de tipo @see String
	 */
	public void setGenerar(String generar) {
		this.generar = generar;
	}

	/**
	 * Metodo get que obtiene el valor de la propiedad isActivoPOACOA
	 * @return boolean Objeto de tipo @see boolean
	 */
	public boolean isActivoPOACOA() {
		return activoPOACOA;
	}

	/**
	 * Metodo que modifica el valor de la propiedad isActivoPOACOA
	 * @param activoPOACOA Objeto de tipo @see boolean
	 */
	public void setActivoPOACOA(boolean activoPOACOA) {
		this.activoPOACOA = activoPOACOA;
	}	

}
