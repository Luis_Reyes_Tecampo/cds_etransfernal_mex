package mx.isban.eTransferNal.beans.admonSaldos;

import java.io.Serializable;
import java.util.List;

import mx.isban.eTransferNal.beans.admonSaldo.BeanConsNomCUT;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResConsNomCUTHistDAO extends BeanResBase implements Serializable{

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 3704479056787179009L;
	
	/**
	 * Propiedad del tipo List<BeanConsNomCUT> que almacena el valor de listBeanConsNomCUT
	 */
	private List<BeanConsNomCUT> listBeanConsNomCUT;

	/**
	 * Metodo get que sirve para obtener la propiedad listBeanConsNomCUT
	 * @return listBeanConsNomCUT Objeto del tipo List<BeanConsNomCUT>
	 */
	public List<BeanConsNomCUT> getListBeanConsNomCUT() {
		return listBeanConsNomCUT;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad listBeanConsNomCUT
	 * @param listBeanConsNomCUT del tipo List<BeanConsNomCUT>
	 */
	public void setListBeanConsNomCUT(List<BeanConsNomCUT> listBeanConsNomCUT) {
		this.listBeanConsNomCUT = listBeanConsNomCUT;
	}
	

}
