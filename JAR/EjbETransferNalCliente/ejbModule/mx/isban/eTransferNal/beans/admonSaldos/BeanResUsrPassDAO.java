package mx.isban.eTransferNal.beans.admonSaldos;

import java.io.Serializable;

import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

public class BeanResUsrPassDAO extends BeanResBase implements Serializable {

	/**
	 * Propiedad del tipo long que almacena el valor de serialVersionUID
	 */
	private static final long serialVersionUID = 1851803744386568417L;
	
	/**
	 * Propiedad del tipo String que almacena el valor de usuario
	 */
	private String usuario;
	
	/**
	 * Propiedad del tipo String que almacena el valor de contrasena
	 */
	private String contrasena;

	/**
	 * Metodo get que sirve para obtener la propiedad usuario
	 * @return usuario Objeto del tipo String
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad usuario
	 * @param usuario del tipo String
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Metodo get que sirve para obtener la propiedad contrasena
	 * @return contrasena Objeto del tipo String
	 */
	public String getContrasena() {
		return contrasena;
	}

	/**
	 * Metodo set que modifica la referencia de la propiedad contrasena
	 * @param contrasena del tipo String
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
	

}
