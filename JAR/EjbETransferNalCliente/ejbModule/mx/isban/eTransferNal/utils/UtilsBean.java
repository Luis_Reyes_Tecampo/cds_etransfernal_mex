package mx.isban.eTransferNal.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Descripcion:Clase para clonar Listados
 * UtilsBean.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    UtilsBean.java
*/ 
public final class UtilsBean {
	
	/**
	 * Constructor para evitar instancias
	 */
	private UtilsBean() {
		// evita instancias
	}

	/**
	 * Clona un array
	 * @param array Arreglo de String
	 * @return Devuelve un arreglo
	 */
	public static String[] clone(String[] array){
		if (array!=null){
			return array.clone();
		} else {
			return new String[]{};
		}
	}
	
	/**
	 * Descripcion   : Metodo que la copia de un Properties
	 * Creado por    : 1396929 - [Moises Navarro Galvan]
	 * Fecha Creacion: 24/01/2019
	 * @param properties Properties a clonar
	 * @return Properties a clonado
	 */
	public static Properties clone(Properties properties){
		if (properties!=null){
			Properties p = new Properties();
			p.putAll(properties);
			return p;
		} else {
			return new Properties();
		}
	}
	   /**
     * Clona un array
     * @param array Arreglo de Double
     * @return Devuelve un arreglo
     */
    public static double[] clone(double[] array){
        if (array!=null){
            return array.clone();
        } else {
            return new double[]{};
        }
    }

	/**
	 * Clona una lista
	 * @param list Listado generico
	 * @return Regresa una lista generica
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> clone(List<T> list){
		if (list!=null){
			return list;
		} else {
			return (List<T>)(new ArrayList<T>()).clone();
		}
	}

	/**
	 * Descripcion   : Metodo que evalua si una cadena esta vacia y retorna null.
	 * Creado por    : 1396929 - [Moises Navarro Galvan]
	 * Fecha Creacion: 21/09/2018
	 * @param value Cadena a evaluar
	 * @return Null si la cadena esta vacia, si no regresa la cadena original.
	 */
	public static Object emptyToNull(Object value) {
		if (value instanceof String && value.toString().isEmpty()) {
			return null;
		}
		return value;
	}
	
	/**
	  * Clona un array
	  * @param array Arreglo de int
	  * @return Devuelve un arreglo
	  */
	 public static int[] clone(int[] array){
	     if (array!=null){
	         return array.clone();
	     } else {
	         return new int[]{};
	     }
	 }
	 
	 /**
	 * Descripcion   : Metodo que realiza la copia de un objeto fecha
	 * Creado por    : 1396929 - [Moises Navarro Galvan]
	 * Fecha Creacion: 24/01/2019
	 * @param fecha Fecha a clonar
	 * @return Fecha clonada
	 */
	public static Date clone(Date fecha) {
		 if (fecha!=null) {
			 return new Date(fecha.getTime());
		 }
		 return null;
	 }

}