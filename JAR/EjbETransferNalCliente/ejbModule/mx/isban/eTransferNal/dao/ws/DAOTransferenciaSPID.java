/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOTransferenciaSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   14/03/2016 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;

/**
 * @author cchong
 *
 */
@Local
public interface DAOTransferenciaSPID {
	
	/**
	 * @param trama
	 * 			json convertido a string
	 * @return
	 * 			ResBeanEjecTranDAO Objeto Bean de respuesta
	 */
	ResBeanEjecTranDAO ejecutar(String trama);

}
