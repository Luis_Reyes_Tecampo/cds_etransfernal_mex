/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOConsultaEstatusSPEI.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   9/10/2018 05:40:47 PM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.ws.EntradaStringJsonDTO;

/**
 * Interface DAOConsultaEstatusSPEI.
 * Define los metodos para el Servicio de Consulta de Estatus BO
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */
@Local
public interface DAOConsultaEstatusSPEI {
	
	/**
	 * Consulta estatus SPEI.
	 *
	 * @param session El objeto: session
	 * @return Objeto res entrada string json DTO
	 */
	EntradaStringJsonDTO consultaEstatusSPEI();

}