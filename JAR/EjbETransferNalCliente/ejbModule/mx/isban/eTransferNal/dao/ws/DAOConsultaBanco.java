/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaBanco.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/07/2015 Carlos Alberto Chong Antonio  ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.ws.ResBeanConsultaBancoDAO;

/**
 * 
 * Interface del tipo DAO que tiene la firma de los metodos para el flujo de consulta de bancos
 */
@Local
public interface DAOConsultaBanco {

	/**
	 * Metodo que permite realizar al consulta del banco por prefijo Cuenta clabe
	 * @param prefijoCuentaClabe String con el prefijo de la cuenta clabe
	 * @param architechSessionBean Objeto de la Arquitectura
	 * @return ResBeanConsultaBancoDAO bean que almacena la informacion del banco
	 */
	ResBeanConsultaBancoDAO consultaBanco(String prefijoCuentaClabe, ArchitechSessionBean architechSessionBean);
}
