/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaImporteSpei.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   02/10/2016     Indra FSW   ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;
@Local
public interface DAOConsultaImporteSpei {
	/**
	 * Metodo que consulta los datos de una operacion
	 * @param trama String con los parametros necesarios
	 * @return
	 * 			ResBeanEjecTranDAO bean de respuesta con los datos de la operacion
	 */
	ResBeanEjecTranDAO consultaImporteSpei(final String trama);
}
