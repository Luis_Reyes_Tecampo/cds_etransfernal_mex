package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.ws.BeanReqCutSpei;
import mx.isban.eTransferNal.beans.ws.BeanResCutSpeiDAO;

@Local
public interface DAOConsultaCutSpeiWSService {
	
	/**
	 * @param beanReqCutSpei Bean con los parametros del WS
	 * @param sessionBean Objeto de la arquitectura
	 * @return BeanResCutSpeiDAO bean con la respuesta de la salt
	 */
	BeanResCutSpeiDAO obtSalt(BeanReqCutSpei beanReqCutSpei, ArchitechSessionBean sessionBean);
	
	
	/**
	 * @param beanReqCutSpei Bean con los parametros del WS
	 * @param sessionBean Objeto de la arquitectura
	 * @return BeanResCutSpeiDAO bean con la respuesta del monto pago nomina tesofe 
	 */
	BeanResCutSpeiDAO obtMontoPagosNominaTesofe(BeanReqCutSpei beanReqCutSpei,
			ArchitechSessionBean sessionBean);
}
