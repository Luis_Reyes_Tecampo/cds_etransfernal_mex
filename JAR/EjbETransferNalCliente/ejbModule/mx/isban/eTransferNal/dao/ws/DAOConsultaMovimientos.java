/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovimientos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   15/12/2015 INDRA FSW ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;

/**
 * @author mcamarillo
 *
 */
@Local
public interface DAOConsultaMovimientos{
	
	/**
	 * @param trama
	 * 			json convertido a string
	 * @return
	 * 			ResBeanEjecTranDAO Objeto Bean de respuesta
	 */
	ResBeanEjecTranDAO ejecutar(String trama);

}
