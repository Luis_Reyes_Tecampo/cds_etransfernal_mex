/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOConsulaDetallada.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   7/02/2017 05:03:19 PM Juan Jesus Beltran R. Isban Creacion
 */

package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.moduloCDA.ResBeanEjecTranDAO;

/**
 * Interface DAOConsulaDetallada.
 *
 * @author FSW-Vector
 * @since 7/02/2017
 */
@Local
public interface DAOConsulaDetallada {
	
	/**
	 * Consulta detalle.
	 *
	 * @param trama 			json convertido a string
	 * @return 			ResBeanEjecTranDAO Objeto Bean de respuesta
	 */
	ResBeanEjecTranDAO consultaDetalle(String trama);

}
