/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaCertificado.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   16/02/2016     Indra FSW   ISBAN Creacion
 *
 */
package mx.isban.eTransferNal.dao.ws;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.ws.BeanReqConsultaCertificado;
import mx.isban.eTransferNal.beans.ws.BeanResConsultaCertificadoDAO;
@Local
public interface DAOConsultaCertificado {
	
	/**
	 * Metodo que consulta el WebService del Banco de Mexico
	 * @param req BeanReqConsultaCertificado con los parametros necesarios para llamar al WS
	 * @return BeanResConsultaCertificadoDAO bean de respuesta
	 */
	BeanResConsultaCertificadoDAO consultaCertificado(BeanReqConsultaCertificado req);

}
