/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorBancosSPEI.java
 * Control de versiones:
 * Version  Date/Hour               By                  					Company     Description
 * -------  -------------------     --------------------------------    	--------    -----------------------------------------------------------------
 * 1.0      19/03/2020 11:15:31	    1396920: Moises Navarro                 TCS         Creacion de DAOMonitorBancosSPEI.java
 */
package mx.isban.eTransferNal.dao.moduloSPEI;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.global.BeanNuevoPaginador;
import mx.isban.eTransferNal.beans.moduloSPEI.BeanResponseMonBancSPEIDAO;

/**
 * Descripcion:Interface que define los metodos de acceso a datos para el monitor de bancos SPEI
 * DAOMonitorBancosSPEI.java
 * @author 1396920: Moises Navarro
 * @Version 1.0                             TCS     Creacion de    DAOMonitorBancosSPEI.java
*/
@Local
public interface DAOMonitorBancosSPEI {

	/**
	 * Descripcion   : Metodo que realiza la consulta de los estatus de los bancos para el monitor de bancos SPEI
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 19/03/2020
	 * @param architech sesion de agave
	 * @param paginador bean de paginacion
	 * @return BeanResponseMonBancSPEIDAO bean con la respuesta de la consulta
	 */
	BeanResponseMonBancSPEIDAO consultaMonitorBancosSPEI(ArchitechSessionBean architech, BeanNuevoPaginador paginador);
	
	/**
	 * Descripcion   : Metodo que realiza la consulta del resumen de bancos para el monitor de bancos SPEI
	 * Creado por    : 1396920: Moises Navarro
	 * Fecha Creacion: 19/03/2020
	 * @param architech sesion de agave
	 * @return BeanResponseMonBancSPEIDAO bean con la respuesta de la consulta
	 */
	BeanResponseMonBancSPEIDAO resumenMonitorBancosSPEI(ArchitechSessionBean architech);
}
