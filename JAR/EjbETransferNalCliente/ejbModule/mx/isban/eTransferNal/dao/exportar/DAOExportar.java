/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOEmisionReporte.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.exportar;

/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */
import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.exportar.BeanExportarTodo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion siguiente:
 *  - Estatus Operacion
 *  - Hora Inicio SPEI CERO 
 *  - Horario cambio fecha operacion.
 */
@Local
public interface DAOExportar {
	
	/**
	 * Funcion para exportar.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @return Objeto bean res base
	 */
	BeanResBase exportarTodo(BeanExportarTodo bean, ArchitechSessionBean sessionBean);
	
	/**
	 * Funcion para exportar.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return Objeto bean res base
	 */
	BeanResBase exportarTodos(BeanExportarTodo bean, ArchitechSessionBean sessionBean,BeanTranSpeiRecOper beanTranSpeiRecOper);
	
}
