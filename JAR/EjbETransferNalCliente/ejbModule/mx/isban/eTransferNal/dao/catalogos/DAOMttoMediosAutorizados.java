/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMttoMediosAutorizados.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     11/09/2018 12:11:04 PM   Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;



import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutoBusqueda;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizados;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosReq;
import mx.isban.eTransferNal.beans.catalogos.BeanMttoMediosAutorizadosRes;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 * Interface DAOMttoMediosAutorizados.
 *
 * Declaracion del flujo de acceso a los datos 
 * para el catalogo de Medios Autorizados.
 * 
 * @author FSW-Vector
 * @since 11/09/2018
 */
@Local
public interface DAOMttoMediosAutorizados {
	
	/**
	 * Consulta tabla MA.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean the session bean --> El objeto session
	 * @param beanAuto El objeto: bean auto --> Objeto de entrada
	 * @return the list --> Objeto de respuesta de la consulta principal
	 */
	BeanMttoMediosAutorizadosRes consultaTablaMA(BeanMttoMediosAutoBusqueda bean, ArchitechSessionBean sessionBean,BeanMttoMediosAutorizados beanAuto);
	
	/**
	 * Consulta listas.
	 *
	 * @param beanMttoMediosAutorizadosRes El objeto: bean mtto medios autorizados res --> Objeto con informacion de consulta
	 * @param consulta El objeto: consulta --> Parametro de tipo de consulta para llenado de lista
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> Respuesta obtenida
	 */
	BeanMttoMediosAutorizadosRes consultaListas(BeanMttoMediosAutorizadosRes beanMttoMediosAutorizadosRes, String consulta, ArchitechSessionBean sesion);
	
	
	/**
	 * Operaciones mtto medios.
	 *
	 * @param beanMttoMediosAutorizadosReq the bean mtto medios autorizados req --> Objeto con datos para el flujo
	 * @param tipoOper the tipo oper --> Tipo de operacion ejecutada
	 * @param sesion the sesion --> El objeto session
	 * @return the bean mtto medios autorizados res --> Respuesta
	 */
	BeanMttoMediosAutorizadosRes operacionesMttoMedios(BeanMttoMediosAutorizadosReq beanMttoMediosAutorizadosReq, String tipoOper, ArchitechSessionBean sesion);
	
	
	
	/**
	 * Mostrar registro seleccionado.
	 *
	 * @param beanMtto the bean mtto --> Objeto con informacion del registro
	 * @param sesion the sesion --> El objeto session
	 * @param accion El objeto: accion --> Parametro de accion al mostrar el registro
	 * @return the bean mtto medios autorizados res --> Respuesta obtenida
	 */
	BeanMttoMediosAutorizadosRes mostrarRegistro(BeanMttoMediosAutorizadosReq beanMtto, ArchitechSessionBean sesion,String accion);
	
	/**
	 * Obtener datos.
	 *
	 * @param beanPaginador El objeto: bean paginador --> Objeto con informacion del paginado
	 * @param architechSessionBean El objeto: architech session bean
	 * @param sortField El objeto: sort field  -->Parametro de ordenamiento
	 * @param sortType El objeto: sort type  -->Parametro de tipo ordenamiento 
	 * @param beanAuto El objeto: bean auto --> Objeto de  entrada auxiliar
	 * @return Objeto bean mtto medios autorizados resOBjeto de salida con la respuesta de la operacion
	 */
	BeanMttoMediosAutorizadosRes obtenerDatos(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean,
			String sortField, String sortType,BeanMttoMediosAutorizados beanAuto);

	
}
