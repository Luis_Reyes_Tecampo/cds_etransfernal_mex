/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOInstitucionesPCCda.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Thu May 18 18:20:20 CST 2017 Alan Garcia Villagran  ISBAN 	Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParamGuardar;
import mx.isban.eTransferNal.beans.catalogos.BeanReqMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametros;
import mx.isban.eTransferNal.beans.catalogos.BeanResMantenimientoParametrosDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Interface DAOMantenimientoParametros.
 */
@Local
public interface DAOMantenimientoParametros {

	/**
	 * Consulta opciones tipo param.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res mantenimiento parametros dao
	 */
	BeanResMantenimientoParametrosDAO consultaOpcionesTipoParam(ArchitechSessionBean sessionBean);

	/**
	 * Consulta opciones tipo dato.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res mantenimiento parametros dao
	 */
	BeanResMantenimientoParametrosDAO consultaOpcionesTipoDato(ArchitechSessionBean sessionBean);
	
	/**
	 * Consultar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 */
	BeanResMantenimientoParametrosDAO consultarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request);
	
	/**
	 * Alta parametro nuevo.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 */
	BeanResMantenimientoParametrosDAO altaParametroNuevo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request);
	
	/**
	 * Editar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @return the bean res mantenimiento parametros
	 */
	BeanResMantenimientoParametrosDAO editarParametros(ArchitechSessionBean sessionBean, BeanReqMantenimientoParamGuardar request);
	
	/**
	 * Eliminar parametros.
	 *
	 * @param sessionBean the session bean
	 * @param claveParametro the clave parametro
	 * @return the bean res mantenimiento parametros
	 */
	BeanResMantenimientoParametrosDAO eliminarParametros(ArchitechSessionBean sessionBean, String claveParametro);

	/**
	 * Exportar todos.
	 *
	 * @param sessionBean the session bean
	 * @param request the request
	 * @param responseConsulParams the response consul params
	 * @return the bean res base
	 */
	BeanResBase exportarTodo(ArchitechSessionBean sessionBean, BeanReqMantenimientoParametros request, BeanResMantenimientoParametros responseConsulParams);
	
	/**
	 * Consultar datos parametro.
	 *
	 * @param sessionBean the session bean
	 * @param claveParametro the clave parametro
	 * @return the bean res mantenimiento parametros dao
	 */
	BeanResMantenimientoParametrosDAO consultarDatosParametro(ArchitechSessionBean sessionBean, String claveParametro);
}
