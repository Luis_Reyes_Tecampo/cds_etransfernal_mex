/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAONombresFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:54:08 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanNombreFideico;
import mx.isban.eTransferNal.beans.catalogos.BeanNombresFideico;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAONombresFideico.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a la
 * informacion para los flujos del catalogo de identificacion de nombres de
 * fideicomiso en SPID.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Local
public interface DAONombresFideico {

	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar los nombres de fideicomiso
	 *
	 * @param beanNombre --> objeto que almacena los nombres de fideicomiso
	 * @param beanPaginador --> bean que permite paginar
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return Objeto bean nombres fideico
	 */
	BeanNombresFideico consultar(BeanNombreFideico beanNombre, BeanPaginador beanPaginador,
			ArchitechSessionBean session);

	/**
	 * Agregar registros.
	 * 
	 * Metodo publico utilizado para agregar registros
	 * al catalogo.
	 *
	 * @param beanNombre --> bean que obtiene el nombre
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return Objeto bean res base
	 */
	BeanResBase agregar(BeanNombreFideico beanNombre, ArchitechSessionBean session);

	/**
	 * Editar registros.
	 * 
	 * Metodo publico utilizado para editar registros
	 * del catalogo.
	 *
	 * @param beanNombre --> bean que obtiene el nombre de usuario
	 * @param anterior --> objeto de tipo  BeanNombreFideico que trae los nmbres de usuarios
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return Objeto bean res base
	 */
	BeanResBase editar(BeanNombreFideico beanNombre, BeanNombreFideico anterior, ArchitechSessionBean session);


/**
 * Eliminar registros.
 * 
 * Metodo publico utilizado para eliminar registros
 * en el catalogo.
 *
 * @param beanNombre --> bean que obtiene el nombre
 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
 * @return Objeto bean res base
 */ 
	BeanResBase eliminar(BeanNombreFideico beanNombre, ArchitechSessionBean session);
}
