/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOInstitucionesPCCda.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinanciero;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertarIntFin;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsTiposIntFinDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimIntFinDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOIntFinancieros.
 */
@Local
public interface DAOIntFinancieros {

	
//	/**
//	 * Metodo que permite consultar la tabla comu_interme_fin.
//	 *
//	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
//	 * @return BeanResConsInstDAO Objeto del tipo BeanResConsInstDAO
//	 */
//	public BeanResConsIntFinDAO consultaIntFin(ArchitechSessionBean architechSessionBean);

	/**
	 * Metodo que permite consultar el catalogo de instituciones.
	 *
	 * @param beanConsReqIntFin El objeto: bean cons req int fin
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsInstPOACOADAO Objeto del tipo BeanResConsInstDAO
	 */
	public BeanResConsIntFinDAO buscarIntFinancieros(BeanConsReqIntFin beanConsReqIntFin, 
			   ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que sirve para marcar los registros como eliminados.
	 *
	 * @param beanIntFinanciero El objeto: bean int financiero
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResElimInstPOACOADAO Objeto del tipo
	 *         BeanResElimInstPOACOADAO
	 */
	public BeanResElimIntFinDAO elimIntFinanciero(BeanIntFinanciero beanIntFinanciero,
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Guarda int financiero.
	 *
	 * @param beanReqInsertIntFin El objeto: bean req insert int fin
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res base
	 */
	public BeanResBase guardaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin, 
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Consulta tipos int fin.
	 *
	 * @param beanReqInsertInst El objeto: bean req insert inst
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res cons tipos int fin dao
	 */
	public BeanResConsTiposIntFinDAO consultaTiposIntFin(BeanReqInsertarIntFin beanReqInsertInst,
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Actualiza int financiero.
	 *
	 * @param beanReqInsertIntFin El objeto: bean req insert int fin
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res base
	 */
	public BeanResBase actualizaIntFinanciero(BeanReqInsertarIntFin beanReqInsertIntFin, 
			ArchitechSessionBean architechSessionBean);
	
}
