/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOLiquidezIntradia.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     30/08/2019 05:53:39 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidacionesCombo;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradia;
import mx.isban.eTransferNal.beans.catalogos.BeanLiquidezIntradiaReq;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOLiquidezIntradia.
 *
 *
 * Clase que contiene los metodos de la capa de servicio para realizar
 * los flujos de catalogo de Liquidez Intradia
 *
 * @author FSW-Vector
 * @since 8/10/2019
 */
@Local
public interface DAOLiquidezIntradia {

	/**
	 * Consultar.
	 * 
	 * Consultar registros del catalgo.
	 *
	 * @param beanFilter El objeto: bean filter - contiene los campos para realizar el filtro
	 * @param canal the canal - Contiene el canal al que se realiza la consulta
	 * @param beanPaginador El objeto: bean paginador - Contiene las variables de paginacion
	 * @param session El objeto: session - Contiene atributos de la sesion
	 * @return bean liquidez intradia req - Contiene el resultado de la consulta
	 */
	BeanLiquidezIntradiaReq consultar(BeanLiquidezIntradia beanFilter, String canal,BeanPaginador beanPaginador, ArchitechSessionBean session); 
	
	/**
	 * Consultar listas.
	 * 
	 * Consutar las listas para los combos
	 *
	 * @param tipo objeto tipo - Objeto que define el tipo de consulta
	 * @param beanFilter objeto bean filter  - contiene los campos para realizar el filtro
	 * @param session objeto session - Contiene atributos de la sesion
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @return objeto bean liquidaciones combo  - Contiene el resultado de la consulta
	 */
	BeanLiquidacionesCombo consultarListas(int tipo, BeanLiquidezIntradia beanFilter,ArchitechSessionBean session, String canal);
	
	/**
	 * Agregar.
	 * 
	 * Agrega un registro al catalogo.
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a agregar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 */
	BeanResBase agregar(BeanLiquidezIntradia liquidacion, String canal,ArchitechSessionBean session);
	
	/**
	 * Editar.
	 *  
	 * Modifica un registro al catalogo.
	 *
	 * @param liquidacion objeto liquidacion - Contiene los atributos a editar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param anterior objeto anterior - Contiene los atributos del objeto anterior
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean res base - Objeto que retorna resultado de la operacion
	 */
	BeanResBase editar(BeanLiquidezIntradia liquidacion, BeanLiquidezIntradia anterior,String canal, ArchitechSessionBean session);
	
	/**
	 * Eliminar.
	 * 
	 * Elimina un registro del catalogo
	 *
	 * @param liquidaciones objeto liquidaciones - Contiene los atributos a eliminar
	 * @param canal objeto canal - Contiene el canal al que se realiza la consulta
	 * @param session objeto session - Contiene atributos de la sesion
	 * @return objeto bean eliminar response - Objeto que retorna resultado de la eliminacion
	 */
	BeanResBase eliminar(BeanLiquidezIntradia liquidaciones, String canal,ArchitechSessionBean session);
	
}
