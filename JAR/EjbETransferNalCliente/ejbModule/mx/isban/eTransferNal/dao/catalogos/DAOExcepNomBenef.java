/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   8/09/2019 10:15:53 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepNomBenef;

/**
 * Interface DAOExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Local
public interface DAOExcepNomBenef {

	/**
	 * Consulta nombres.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param beanExcepNomBenef El objeto: bean excep nom benef --> Valor de filtros
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	BeanExcepNomBenef consultaNombres(ArchitechSessionBean sessionBean, BeanExcepNomBenef beanExcepNomBenef);
	
	/**
	 * Alta nombre.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nombre El objeto: nombre  --> Valor del registro
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	BeanExcepNomBenef altaNombre(ArchitechSessionBean sessionBean, String nombre);
	
	/**
	 * Baja nombre.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nombre El objeto: nombre --> Valor del registro
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	BeanExcepNomBenef bajaNombre(ArchitechSessionBean sessionBean, String nombre);
	
	/**
	 * Total registros.
	 *
	 * @param nombre El objeto: nombre --> Valor buscado
	 * @return Objeto int --> total de registros obtenidos
	 */
	int totalRegistros(String nombre);
	
	/**
	 * Modificacionnombres.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param nomOld El objeto: nom old --> Valor actual
	 * @param nomNew El objeto: nom new --> Valor nuevo
	 * @return Objeto bean excep nom benef --> Respuesta de la operacion
	 */
	BeanExcepNomBenef modificacionNombres(ArchitechSessionBean sessionBean, String nomOld, String nomNew);
	
	/**
	 * Buscar registro.
	 * 
	 * * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param nomNew El objeto: nom new --> Valor a buscar
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto int --> total de registros obtenidos
	 */
	int buscarRegistro(String nomNew, ArchitechSessionBean session);
}
