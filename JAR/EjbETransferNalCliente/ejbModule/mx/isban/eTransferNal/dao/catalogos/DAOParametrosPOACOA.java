/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOParametrosPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanProcesosPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOParametrosPOACOA.
 *
 * @author ooamador
 */
@Local
public interface DAOParametrosPOACOA {
	
	/**
	 * Inserta el registro para activar el proceso de COA o POA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para insertar BD
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase insertaActivacionPOACOA(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza el registro para desactivar o activar el proceso de COA o POA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para actualizar BD
	 * @param modulo El objeto: modulo
	 * @param esDesactivacion boolean que indica si se desactiva el POACOA
	 * @param nuevaCveOper Clave de operacion
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase actualizaProcesoPOACOA(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
                boolean esDesactivacion, String nuevaCveOper,
                ArchitechSessionBean architectSessionBean);

	
	/**
	 * Actualiza del proceso COAPOA.
	 *
	 * @param beanProcesosPOACOA Bean con los datos para actualizar BD
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase actualizaFase(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Inserta parametro de cifrado.
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param architectSessionBean  Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase insertaParametroFirma(String cveInstitucion,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza parametro de cifrado.
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param cifrado 0 no firmado 1 firmado
	 * @param architectSessionBean  Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase actualizaParametroFirma(String cveInstitucion, String cifrado,
			ArchitechSessionBean architectSessionBean);
	

}
