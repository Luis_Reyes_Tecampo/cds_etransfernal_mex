/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMttoMediosEntrega.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   26/09/2018 10:18:24 AM Juan Jesus Beltran R. VectorFSW Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanMedioEntrega;
import mx.isban.eTransferNal.beans.catalogos.BeanMediosEntrega;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOMttoMediosEntrega.
 *
 * @author FSW-Vector
 * @since 26/09/2018
 */
@Local
public interface DAOMttoMediosEntrega {
	
	/**
	 * Consultar.
	 *
	 * @param session El objeto: session
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto bean mantenimiento entrega
	 */
	BeanMediosEntrega consultar(ArchitechSessionBean session, BeanMedioEntrega beanMedio,
			BeanPaginador beanPaginador);
	
	/**
	 * Agregar medio entrega.
	 *
	 * @param session El objeto: session
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto bean res base
	 */
	BeanResBase agregarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio);
	
	/**
	 * Eliminar Medios de Entrega.
	 *
	 * @param session El objeto: session
	 * @param beanMedio El objeto: bean medio
	 * @return Objeto bean res base
	 */
	BeanResBase eliminarMediosEntrega(ArchitechSessionBean session, BeanMedioEntrega beanMedio);
	
	/**
	 * Editar Medio de Entrega.
	 *
	 * @param session El objeto: session
	 * @param requestMedio El objeto: request medio
	 * @return Objeto bean res base
	 */
	BeanResBase editarMedioEntrega(ArchitechSessionBean session, BeanMedioEntrega requestMedio);
	
}
