/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCuentasFideico.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     13/09/2019 12:58:44 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOCuentasFideico.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a la
 * informacion para los flujos del catalogo de identificacion de cuentas de
 * fideicomiso en SPID.
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Local
public interface DAOCuentasFideico {

	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param session objeto --> bean sesion --> El objeto session
	 * @param beanPaginador objeto --> paginador --> Objeto para realizar el paginado
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 */
	BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador,
			ArchitechSessionBean session);

	/**
	 * Agregar registro.
	 * 
	 * Metodo publico utilizado para agregar un registro
	 * en el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos del nuevo registro
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	BeanResBase agregar(BeanCuenta beanCuenta, ArchitechSessionBean session);

	/**
	 * Editar registro.
	 * 
	 * Metodo publico utilizado para editar un registro
	 * del catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos del nuevo registro
	 * @param anterior El objeto --> anterior --> Objeto con los datos del registro a editar
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	BeanResBase editar(BeanCuenta beanCuenta, BeanCuenta anterior, ArchitechSessionBean session);

	/**
	 * Eliminar registro.
	 * 
	 * Metodo publico utilizado para eliminar un registro
	 * del catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto con los datos registro a eliminar
	 * @param session objeto --> bean sesion  --> El objeto session
	 * @return Objeto BeanResBase --> Resultado de la operacion
	 */
	BeanResBase eliminar(BeanCuenta beanCuenta, ArchitechSessionBean session);
}
