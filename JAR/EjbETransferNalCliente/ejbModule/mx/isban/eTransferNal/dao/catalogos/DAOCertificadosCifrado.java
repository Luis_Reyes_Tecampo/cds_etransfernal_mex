/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCertificadosCifrado.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   08-25-2018 04:44:42 PM Juan Jesus Beltran R. Vector Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCombosCertificadoCifrado;
import mx.isban.eTransferNal.beans.catalogos.BeanCertificadosCifrado;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOCertificadosCifrado.
 * 
 * Define la firma de los metodos a consumirse por la capa de 
 * Negocio
 *
 * @author FSW-Vector
 * @since 11/09/2018
 */
@Local
public interface DAOCertificadosCifrado {
	
	/**
	 * Consultar.
	 *
	 * @param session El objeto: session
	 * @param beanFilter El objeto: bean filter
	 * @param beanPaginador El objeto: bean paginador
	 * @return Objeto bean certificados cifrado
	 */
	BeanCertificadosCifrado consultar(ArchitechSessionBean session, BeanCertificadoCifrado beanFilter,
			BeanPaginador beanPaginador);
	
	/**
	 * Consultar combo.
	 *
	 * @param session El objeto: session
	 * @param beanCertificados El objeto: bean certificados
	 * @param consulta El objeto: consulta
	 * @return Objeto bean certificados
	 */
	BeanCombosCertificadoCifrado consultarCombo(ArchitechSessionBean session, BeanCombosCertificadoCifrado beanCertificados, String consulta);
	
	/**
	 * Agregar certificado.
	 *
	 * @param session El objeto: session
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto bean res base
	 */
	BeanResBase agregarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado);
	
	/**
	 * Eliminar certificados.
	 *
	 * @param session El objeto: session
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto bean res base
	 */
	BeanResBase eliminarCertificados(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado);
	
	/**
	 * Editar certificado.
	 *
	 * @param session El objeto: session
	 * @param beanCertificado El objeto: bean certificado
	 * @return Objeto bean res base
	 */
	BeanResBase editarCertificado(ArchitechSessionBean session, BeanCertificadoCifrado beanCertificado);
	
}
