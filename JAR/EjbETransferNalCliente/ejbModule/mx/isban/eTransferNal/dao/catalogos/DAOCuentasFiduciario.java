/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCuentasFiduciario.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     17/09/2019 01:35:55 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */

package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanCuenta;
import mx.isban.eTransferNal.beans.catalogos.BeanCuentas;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * Interface DAOCuentasFiduciario.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a la
 * informacion para los flujos del catalogo de cuentas fiduciario
 * 
 * @author FSW-Vector
 * @since 13/09/2019
 */
@Local
public interface DAOCuentasFiduciario {

	/**
	 * consultar los registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar
	 * el catalogo.
	 *
	 * @param beanCuenta El objeto --> bean cuenta --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador El objeto --> bean paginador
	 * @param session objeto --> bean sesion --> El objeto session
	 * @return Objeto BeanCuentas --> Resultado obtenido en la consulta
	 */
	BeanCuentas consultar(BeanCuenta beanCuenta, BeanPaginador beanPaginador,
			ArchitechSessionBean session);
	
}
