/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOInstitucionesPCCda.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsReqInst;
import mx.isban.eTransferNal.beans.catalogos.BeanHabDesParticipanteDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanInstitucionPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanReqHabDesInst;
import mx.isban.eTransferNal.beans.catalogos.BeanReqInsertInst;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResConsInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResElimInstPOACOADAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;

@Local
public interface DAOInstitucionesPCCda {
	
	/**Metodo que permite consultar la tabla comu_interme_fin
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsInstDAO Objeto del tipo BeanResConsInstDAO
	 **/
	public BeanResConsInstDAO consultaInst(ArchitechSessionBean architechSessionBean);

	/**Metodo que permite consultar el catalogo de instituciones
	 * @param beanConsReqInst Objeto del tipo @see BeanConsReqInst
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsInstPOACOADAO Objeto del tipo BeanResConsInstDAO
	 **/
	public BeanResConsInstPOACOADAO buscarInstitucionesPOACOA(BeanConsReqInst beanConsReqInst, 
			   ArchitechSessionBean architechSessionBean);
	
	/**Metodo que permite guardar una institucion en el catalogo
     * @param beanReqInsertInst Objeto del tipo @see BeanReqInsertInst
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaInstDAO Objeto del tipo BeanResGuardaInstDAO
     **/
     public BeanResGuardaInstDAO guardaInstitucionPOACOA(BeanReqInsertInst beanReqInsertInst, ArchitechSessionBean architechSessionBean);
     
  	/**
  	 * Metodo que sirve para marcar los registros como eliminados
  	 * 
  	 * @param beanInstitucionPOACOA
  	 *            Objeto del tipo @see BeanInstitucionPOACOA
  	 * @param architechSessionBean
  	 *            Objeto del tipo @see ArchitechSessionBean
  	 * @return BeanResElimInstPOACOADAO Objeto del tipo
  	 *         BeanResElimInstPOACOADAO
  	 * */
  	public BeanResElimInstPOACOADAO elimCatInstFinancieras(
  			BeanInstitucionPOACOA beanInstitucionPOACOA,
  			ArchitechSessionBean architechSessionBean);
  	
    
    /**
     * Metodo que permite habilitar y deshabilitar la participacion POA
     * @param beanReqHabDesInst Objeto del tipo BeanReqHabDesInst
     * @param architechSessionBean Objeto de la arquitectura del tipo @see ArchitechSessionBean
     * @return BeanHabDesParticipanteDAO Objeto del tipo BeanHabDesParticipanteDAO
     */
    public BeanHabDesParticipanteDAO habDesParticipantePOA(BeanReqHabDesInst beanReqHabDesInst, 
   		 ArchitechSessionBean architechSessionBean);
    
    /**
     * Metodo que permite habilitar y deshabilitar la participacion POA
     * @param beanReqHabDesInst Objeto del tipo BeanReqHabDesInst
     * @param architechSessionBean Objeto de la arquitectura del tipo @see ArchitechSessionBean
     * @return BeanHabDesParticipanteDAO Objeto del tipo BeanHabDesParticipanteDAO
     */
    public BeanHabDesParticipanteDAO habDesValidacionCuenta(BeanReqHabDesInst beanReqHabDesInst, 
   		 ArchitechSessionBean architechSessionBean);
}
