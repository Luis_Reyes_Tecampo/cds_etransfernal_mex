/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOParametrosPOACOA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    29/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanParamCifradoDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanProcesosPOACOA;
import mx.isban.eTransferNal.beans.catalogos.BeanResObtenFechaCtrlDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResPOACOA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;

/**
 * Interface DAOParametrosPOACOA2.
 *
 * @author ooamador
 */
@Local
public interface DAOParametrosPOACOA2 {
	
	
	
	/**
	 * Obtiene la institucion .
	 *
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResConsMiInstDAO con la respuesta de la institucion
	 */
	BeanResConsMiInstDAO consultaMiInstitucion(String modulo, ArchitechSessionBean sessionBean);
	
	/**
	 * Obtiene la fecha de operacion de la tabla TRAN_CTRL_FECHAS .
	 *
	 * @param esLiqFinal true si es el campo de Liquidacion final
	 * @param sessionBean Datos de sesion
	 * @return BeanResObtenFechaCtrlDAO bean con la fecha de control
	 */
	BeanResObtenFechaCtrlDAO obtenFechaCTRLFECHAS(boolean esLiqFinal,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Obtiene la fecha de operacion de la tabla TRAN_SPEI_CTRL .
	 *
	 * @param cveInstitucion Clave de la institucion
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return fecha de operacion tipo String
	 */
	BeanResObtenFechaCtrlDAO obtenFechaSPEI(String cveInstitucion, String modulo,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Obtiene el parametro de cifrado.
	 * @param cveInstitucion Clave de la institucion 
	 * @param sessionBean Datos de sesion
	 * @return BeanParamCifradoDAO Bean de respuesta para el parametro de cifrado
	 */
	BeanParamCifradoDAO obtenParametroCifrado(String cveInstitucion,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Obten params POACOA.
	 *
	 * @param beanProcesosPOACOA Datos para la consulta.
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResPOACOA bean con los datos del resultado de la consulta
	 */
	BeanResPOACOA obtenParamsPOACOA(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Actualiza generar.
	 *
	 * @param beanProcesosPOACOA Datos para la consulta.
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta del tipo BeanResBase
	 */
	BeanResBase actualizaGenerar(BeanProcesosPOACOA beanProcesosPOACOA, String modulo,
			ArchitechSessionBean architectSessionBean);

}
