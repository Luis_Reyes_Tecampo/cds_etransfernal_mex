/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOAfectacionBloques.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     23/07/2019 12:12:13 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanBloque;
import mx.isban.eTransferNal.beans.catalogos.BeanBloques;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOAfectacionBloques.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a
 * la informacion para los flujos del catalogo de afectacion de bloques.
 * 
 * @author FSW-Vector
 * @since 23/07/2019
 */
@Local
public interface DAOAfectacionBloques {

	/**
	 * Consulta registros.
	 *
	 * Metodo publico utilizado mostra los registros del catalogo
	 * 
	 * @param beanPaginador --> objeto que permite paginar
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	BeanBloques consultar(BeanBloque beanBloque, BeanPaginador beanPaginador, ArchitechSessionBean session);
		
	/**
	 * Agregar registros.
	 *
	 * Metodo publico utilizado para agregar registros
	 * al catalogo.
	 * 
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanBloque --> bean que obtiene el bloque
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
		BeanResBase agregar(BeanBloque beanBloque, ArchitechSessionBean session);
		
		/**
		 * Editar registros.
		 *
		 * Metodo publico utilizado para editar registros
		 * del catalogo.
		 * 
		 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
		 * @param beanBloque --> bean que obtiene el bloque
		 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
		 * 
		 */
		BeanResBase editar(BeanBloque beanBloque, ArchitechSessionBean session);
		
		/**
		 * Eliminar registros.
		 *
		 * Metodo publico utilizado para eliminar registros
		 * en el catalogo.
		 * 
		 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
		 * @param beanBloque --> bean que obtiene el bloque
		 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
		 * 
		 */
		BeanResBase eliminar(BeanBloque beanBloque, ArchitechSessionBean session);
		
}
