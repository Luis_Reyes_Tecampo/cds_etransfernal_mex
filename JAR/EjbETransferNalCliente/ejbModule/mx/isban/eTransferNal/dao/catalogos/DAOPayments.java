package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultaPayme;
import mx.isban.eTransferNal.beans.catalogos.BeanConsultasPayme;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Declaracion de la interfaz DAOPaymentsImpl
 */
@Local
public interface DAOPayments {
	
	
	

		/**
		 * Consultar, se consultan la tabla COMU_MEDIO_ENT que contienen los datos de la clave medios entrega
		 * descripcion, duplicados. 
		 * @param session El objeto session, se obtiene la sesion 
		 * @param beanFilter en este objeto se utiliza para obtener los datos de la vista (JSP).
		 * @param beanPaginador este objeto se utiliza para la paginacion. 
		 * @return BeanConsultasPayme Bean de respuesta
		 */
		BeanConsultasPayme consultarPayments(ArchitechSessionBean session, BeanConsultaPayme beanFilter,
				BeanPaginador beanPaginador);
		
		
		/**
		 * Editar, se editan las tabla Tcomu_error y tran_cod_error
		 * @param session El objeto session, se obtiene la sesion 
		 * @param beanConsultaPayments El objeto: BeanConsultaPayments
		 * @return BeanResBase Bean de respuesta
		 */
		BeanResBase editarPayments(ArchitechSessionBean session, BeanConsultaPayme beanConsultaPayments);
		
		
		/**
		 * Consultar, se consultan la lista del combo COMU_MEDIO_ENT que contienen los datos de la clave medios entreg. 
		 * @param session El objeto session, se obtiene la sesion 
		 * @param beanFilter en este objeto se utiliza para obtener los datos de la vista (JSP).
		 * @param beanPaginador este objeto se utiliza para la paginacion. 
		 * @return BeanConsultasPayme Bean de respuesta
		 */
		BeanConsultasPayme consultarCombo(ArchitechSessionBean session, BeanConsultaPayme beanFilter, BeanPaginador beanPaginador);
		
		

}
