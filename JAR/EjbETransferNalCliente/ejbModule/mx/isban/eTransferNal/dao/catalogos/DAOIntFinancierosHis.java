/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOIntFinancierosHis.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Sept 20 09:55:49 CST 2015   ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanIntFinancieroHistorico;
import mx.isban.eTransferNal.beans.catalogos.BeanResIntFinancierosHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;

/**
 * Interface DAOIntFinancieros.
 */
@Local
public interface DAOIntFinancierosHis {

	/**
	 * Consultar, se consultan el historico de catalogos  
	 * consultan los campos error y su descripcion. 
	 * @param session El objeto session, se btiene la sesion 
	 * @param beanIntFinancieroHistorico en este objeto se utiliza para obtener los datos de la vista (JSP).
	 * @param beanPaginador este objeto se utiliza para la paginacion. 
	 * @return beanIntFinancieroHistorico Bean de respuesta
	 */
	 BeanResIntFinancierosHist consultar(ArchitechSessionBean session, BeanIntFinancieroHistorico beanIntFinancieroHistorico,
			BeanPaginador beanPaginador);  
	
}
