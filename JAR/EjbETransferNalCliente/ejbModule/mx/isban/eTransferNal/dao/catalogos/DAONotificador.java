/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAONotificador.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     26/07/2019 11:25:26 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacion;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificaciones;
import mx.isban.eTransferNal.beans.catalogos.BeanNotificacionesCombo;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAONotificador.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a
 * la informacion para los flujos del catalogo notificador de estatus.
 * 
 * 
 * @author FSW-Vector
 * @since 26/07/2019
 */
@Local
public interface DAONotificador {

	/**
	 * consultar registros del catalogo.
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param beanNotificacion --> objeto que almacena las notificaciones
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanPaginador --> bean que permite paginar
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	BeanNotificaciones consultar(BeanNotificacion beanNotificacion, BeanPaginador beanPaginador, ArchitechSessionBean session);
	
	/**
	 * consultar listados de notificaciones
	 * 
	 * Metodo publico utilizado para consultar las notificaciones
	 *
	 * @param tipo --> variable de tipo int que trae el tipo de listado
	 * @param filter --> objeto de tipo BeanNotificacion que trae el filtro
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return listas --> regresa la lista de los registros
	 */
	BeanNotificacionesCombo consultarListas(int tipo, BeanNotificacion filter, ArchitechSessionBean session);
	
	/**
	 * agregar notificacion
	 * 
	 * Metodo publico utilizado para agregar notificaciones
	 *
	 *@param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion --> bean que obtiene la notificacion
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 */
	BeanResBase agregar(BeanNotificacion beanNotificacion, ArchitechSessionBean session);
	
	/**
	 * editar notificacion
	 * 
	 * Metodo publico utilizado para editar notificaciones
	 *
	 * @param beanNotificacion --> objeto que trae notificaciones 	
	 * @param anterior --> objeto que trae los registros anteriores
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	BeanResBase editar(BeanNotificacion beanNotificacion, BeanNotificacion anterior, ArchitechSessionBean session);
	
	/**
	 * eliminar notificacion
	 * 
	 * Metodo publico utilizado para eliminar notificaciones
	 *
	 * @param session --> objeto ArchitechSessionBean que mantiene iniciada la sesion del usuario
	 * @param beanNotificacion --> bean que obtiene notificaciones
	 * @return response --> regresa la respuesta en base a los parámetros obtenidos.
	 * 
	 */
	BeanResBase eliminar(BeanNotificacion beanNotificacion, ArchitechSessionBean session);
}
