/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOHorariosSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   21/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanHorariosSPEI;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIBO;
import mx.isban.eTransferNal.beans.catalogos.BeanResHorariosSPEIDAO;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el catalogo de horarios para envio de pagos por SPEI
**/
@Local
public interface DAOHorariosSPEI {
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO consultaHorariosSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO agregaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean);
	
	
	/**Metodo que sirve para modificar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO modificaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO eliminaHorarioSPEI(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanHorariosSPEI
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIDAO consultaHorarioSPEI(BeanHorariosSPEI bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios para envio de pagos por SPEI
	   * @param bean Objeto del tipo @see BeanResHorariosSPEIBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResHorariosSPEIBO Objeto del tipo BeanResHorariosSPEIBO
	   */
	public BeanResHorariosSPEIBO consultaListas(BeanResHorariosSPEIBO bean,ArchitechSessionBean architechSessionBean);
	
	
}
