/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOExcepCtasFideicomiso.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   8/09/2019 10:15:53 PM Uriel Alfredo Botello R. VSF Creacion
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanExcepCtasFideicomiso;

/**
 * Interface DAOExcepCtasFideicomiso.
 *
 * @author FSW-Vector
 * @since 19/09/2019
 */
@Local
public interface DAOExcepCtasFideicomiso {

	/**
	 * Consulta cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param beanCtasFideico El objeto: bean ctas fideico --> Objeto con datos de filtrado
	 * @return Objeto bean excep ctas fideicomiso --> Objeto de respuesta
	 */
	BeanExcepCtasFideicomiso consultaCuentas(ArchitechSessionBean sessionBean, BeanExcepCtasFideicomiso beanCtasFideico);
	
	/**
	 * Alta cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param cuenta El objeto: cuenta --> Valor del registro
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	BeanExcepCtasFideicomiso altaCuentas(ArchitechSessionBean sessionBean, String cuenta);
	
	/**
	 * Baja cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param cuenta El objeto: cuenta --> Valor del registro
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	BeanExcepCtasFideicomiso bajaCuentas(ArchitechSessionBean sessionBean, String cuenta);
	
	/**
	 * Consulta total registros.
	 *
	 * @param cuenta El objeto: cuenta --> Valor buscado
	 * @return Objeto int --> total de registros obtenidos
	 */
	int totalRegistros(String cuenta);
	
	/**
	 * Modificacion cuentas.
	 *
	 * @param sessionBean El objeto: session bean --> El objeto session
	 * @param ctaOld El objeto: cta old --> Valor actual
	 * @param ctaNew El objeto: cta new --> Valor nuevo
	 * @return Objeto bean excep ctas fideicomiso --> Respuesta de la operacion
	 */
	BeanExcepCtasFideicomiso modificacionCuentas(ArchitechSessionBean sessionBean, String ctaOld, String ctaNew);

	/**
	 * Buscar registro.
	 * 
	 * * Metodo privado utilizado para validar la existencia de un registro 
	 * en el catalogo.
	 *
	 * @param ctaNew El objeto: cta new --> Valor buscado
	 * @param session El objeto: session --> El objeto session
	 * @return Objeto int --> Respuesta entera de la busqueda
	 */
	int buscarRegistro(String ctaNew, ArchitechSessionBean session);

}
