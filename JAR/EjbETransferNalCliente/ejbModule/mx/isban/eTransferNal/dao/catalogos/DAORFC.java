/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORFC.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     8/09/2019 11:53:00 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.catalogos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanRFC;
import mx.isban.eTransferNal.beans.catalogos.BeanResRFC;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAORFC.
 *
 * Interfaz que declara los metodos utilizados por la capa de acceso a
 * la informacion para los flujos del catalogo de excepcion de RFC.
 * 
 * @author FSW-Vector
 * @since 8/09/2019
 */
@Local
public interface DAORFC {
	
	/**
	 * Consultar.
	 * 
	 * Consultar los registros del catalogo.
	 *
	 * @param beanRFC El objeto: bean RFC  --> Objeto que contiene campos para realizar los filtros
	 * @param beanPaginador El objeto: bean paginador  --> Objeto para realizar el paginado
	 * @param session El objeto: session  --> El objeto session
	 * @return Objeto bean bloques  --> Resultado obtenido en la consulta
	 */
	BeanResRFC consultar(BeanRFC beanRFC, BeanPaginador beanPaginador, ArchitechSessionBean session);
		
		/**
		 * Agregar.
		 * 
		 * Agrega un nuevo registro al catalogo.
		 * 
		 * @param beanRFC El objeto: bean RFC  --> Objeto con los datos del nuevo registro
		 * @param session El objeto: session   --> El objeto session
		 * @return Objeto bean res base  --> Resultado de la operacion
		 */
		BeanResBase agregar(BeanRFC beanRFC, ArchitechSessionBean session);
		
		/**
		 * Editar.
		 * 
		 * Mofifica un registro del catalogo.
		 *
		 * @param beanRFC El objeto: bean RFC  --> Objeto con los datos del nuevo registro
		 * @param anterior El objeto: anterior  --> Objeto con los datos del registro a editar
		 * @param session El objeto: session   --> El objeto session
		 * @return Objeto bean res base --> Resultado de la operacion
		 */
		BeanResBase editar(BeanRFC beanRFC, BeanRFC anterior, ArchitechSessionBean session);
		
		/**
		 * Eliminar.
		 * 
		 * Elimina un registro del catalogo.
		 *
		 * @param beanRFC El objeto: bean RFC --> Objeto con los datos registro a eliminar
		 * @param session El objeto: session   --> El objeto session
		 * @return Objeto bean res base --> Resultado de la operacion
		 */
		BeanResBase eliminar(BeanRFC beanRFC, ArchitechSessionBean session);
}
