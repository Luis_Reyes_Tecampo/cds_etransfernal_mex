/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOBitAdministrativa.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.comun;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;

@Local
public interface DAOBitAdministrativa {
	
	/**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitacora Objeto del tipo @see BeanReqInsertBitacora
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitAdmin beanReqInsertBitacora, ArchitechSessionBean architechSessionBean);

}
