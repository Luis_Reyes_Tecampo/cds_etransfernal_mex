package mx.isban.eTransferNal.dao.comun;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;

@Local
public interface DAOBitAdmon {

	/**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitAdmon Objeto del tipo @see BeanReqInsertBitAdmon
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitAdmon beanReqInsertBitAdmon, ArchitechSessionBean architechSessionBean);
}
