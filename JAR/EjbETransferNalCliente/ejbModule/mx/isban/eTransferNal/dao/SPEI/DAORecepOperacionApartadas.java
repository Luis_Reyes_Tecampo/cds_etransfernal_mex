/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORecepOperacionApartadas.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   19/09/2018 06:08:34 PM Lily Marlenne Angeles Bravo. VectorFSW Creacion
 */

package mx.isban.eTransferNal.dao.SPEI;


import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

/**
 * Interface DAORecepOperacionApartadas.
 *
 * @author FSW-Vector
 * @since 19/09/2018
 */
@Local
public interface DAORecepOperacionApartadas {

	
	
	/**
	 * Aparta operaciones.
	 *
	 * @param beanTranSpeiRec El objeto: list bean tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param pantalla El objeto: pantalla
	 * @return Objeto bean res cons tran spei rec DAO
	 */
	BeanResTranSpeiRec apartaOperaciones(BeanTranSpeiRecOper beanTranSpeiRec, ArchitechSessionBean sessionBean,
			boolean historico,String pantalla);
	
	
	/**
	 * Aparta operaciones eliminar.
	 *
	 * @param beanTranSpeiRec El objeto: bean tran spei rec
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @param pantalla El objeto: pantalla
	 * @return Objeto bean res cons tran spei rec
	 */
	BeanResTranSpeiRec apartaOperacionesEliminar(BeanTranSpeiRecOper beanTranSpeiRec,
			ArchitechSessionBean sessionBean, boolean historico, String pantalla);

	
	/**
	 * Existe operacion.
	 *
	 * @param bean El objeto: bean
	 * @param sessionBean El objeto: session bean
	 * @param historico El objeto: historico
	 * @return Objeto bean res base
	 */
	BeanResBase existeOperacion(BeanTranSpeiRecOper bean, ArchitechSessionBean sessionBean, boolean historico);
	
	
}