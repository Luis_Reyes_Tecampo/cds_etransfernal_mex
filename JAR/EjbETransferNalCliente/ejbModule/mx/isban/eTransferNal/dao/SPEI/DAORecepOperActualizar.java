package mx.isban.eTransferNal.dao.SPEI;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanResGuardaInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion para
 * el  recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 3/10/2018
 */
@Local	    
public interface DAORecepOperActualizar {

	
    
    /**
     * Actualiza trans spei rec.
     *
     * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
     * @param architechSessionBean El objeto: architech session bean
     * @param historico El objeto: historico
     * @return Objeto bean BeanResBase
     */
	BeanResBase actualizaTransSpeiRec(BeanTranSpeiRecOper beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean,boolean historico);
    
    
    
      
      /**
       * Actualiza motivo rec.
       *
       * @param beanReqActTranSpeiRec El objeto: BeanTranSpeiRec
       * @param architechSessionBean El objeto: architech session bean
       * @param pantalla El objeto: pantalla
       * @return Objeto bean res act BeanResBase
       */
	BeanResBase actualizaMotivoRec(BeanTranSpeiRecOper beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean, String pantalla);
    
    /**
     * Guarda tran spei horas man.
     *
     * @param beanReqActTranSpeiRec bean del tipo BeanReqActTranSpeiRec
     * @param architechSessionBean Objeto de ArchitechSessionBean
     * @return Objeto bean res guarda inst DAO
     */
      BeanResGuardaInstDAO guardaTranSpeiHorasMan(BeanTranSpeiRecOper beanReqActTranSpeiRec, 
      		 ArchitechSessionBean architechSessionBean);
    
    
      /**
       * Rechazar.
       *
       * @param beanReqActTranSpeiRec El objeto: bean req act tran spei rec
       * @param architechSessionBean El objeto: architech session bean
       * @param historico El objeto: historico
       * @return Objeto beanResBase
       */
      BeanResBase rechazar(BeanTranSpeiRecOper beanReqActTranSpeiRec,
     		 ArchitechSessionBean architechSessionBean,boolean historico);
    
      
      /**
  	 * Consulta error.
  	 *
  	 * @param architechSessionBean El objeto: architech session bean
  	 * @param codigoError El objeto: codigo error
  	 * @return Objeto bean res base
  	 */
  	 BeanResBase consultaError(ArchitechSessionBean architechSessionBean, String  codigoError);
    
}
