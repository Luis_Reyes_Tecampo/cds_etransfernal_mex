/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOContingMismoDia.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 10 13:02:05 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResContingMismoDiaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchContingMismoDiaDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Contingencia CDA Mismo Dia
**/
@Local
public interface DAOContingMismoDiaSPID{


	   /**Metodo DAO para obtener la informacion para la funcionalidad
	   * de Contingencia CDA mismo dia
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResContingMismoDiaDAO Objeto del tipo BeanResContingMismoDiaDAO
	   */
	   BeanResContingMismoDiaDAO consultaContingMismoDia(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);
	   /**Metodo DAO para guardar la informacion que se requiere para generar
	    * los archivos de contingencia cda mismo dia
	    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	    * @return BeanResGenArchContingMismoDiaDAO Objeto del tipo BeanResGenArchContingMismoDiaDAO
	    */
	    BeanResGenArchContingMismoDiaDAO genArchContingMismoDia(ArchitechSessionBean architechSessionBean);
	    
		/**
		 * Metodo DAO para obtener la informacion para la funcionalidad de
		 * Contingencia CDA mismo dia
		 * @param architechSessionBean
		 *            Objeto del tipo @see ArchitechSessionBean
		 * @return BeanResContingMismoDiaDAO Objeto del tipo
		 *         BeanResContingMismoDiaDAO
		 * 
		 */
		BeanResContingMismoDiaDAO consultaPendientesMismoDia(
				ArchitechSessionBean architechSessionBean);
}
