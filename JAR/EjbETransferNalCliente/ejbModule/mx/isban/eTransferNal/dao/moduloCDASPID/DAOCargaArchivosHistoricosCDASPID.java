/**
 * Clase: DAO-Interface que define los metodos para la pantalla
 * Monitor Carga Archivos Historicos CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Diciembre 2016
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;

/**
 * The Interface DAOCargaArchivosHistoricosCDASPID.
 */
@Local
public interface DAOCargaArchivosHistoricosCDASPID {

	/**
	 * Metodo para obtener el estatus de los archivos
	 * 
	 * @param sessionBean El parametro de la session
	 * @param paginador La variable paginador
	 * @return La respuesta obtenida
	 */
	BeanResMonitorCargaArchHistCADSPID obtenerEstatusArchivo(
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean);
	

}
