/**
 * Clase: DAO-Interface que define los metodos para la pantalla
 * Monitor Carga Archivos Historicos Detalle CADSPID
 * 
 * @author Vector
 * @version 1.0
 * @since Enero 2016
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonitorCargaArchHistCADSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCargaArchHistCADSPID;

/**
 * The Interface DAOCargaArchivosHistoricosDetalleCDASPID.
 */
@Local
public interface DAOCargaArchivosHistoricosDetalleCDASPID {

	/**
	 * Metodo para consultar los detalles del archivo
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param paginador El parametro paginador
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 */
	BeanResMonitorCargaArchHistCADSPID consutarDetallesArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			BeanPaginador paginador,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Metodo para generar el archivo historico
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 */
	BeanResMonitorCargaArchHistCADSPID generarArchivoHistorico(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean);
	
	/**
	 * Metodo para eleminar la seleccion del archivo
	 * 
	 * @param datosEntranda El parametro datosEntranda
	 * @param sessionBean El parametro sessionBean
	 * @return El resultado de la consulta
	 */
	BeanResMonitorCargaArchHistCADSPID eliminarSelecionArchivo(
			BeanReqMonitorCargaArchHistCADSPID datosEntranda,
			ArchitechSessionBean sessionBean);
	

}
