/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchxFchCDAHistSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Jan 04 18:24:31 CST 2017 jjbeltran  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico
**/
@Local
public interface DAOGenerarArchxFchCDAHistSPID{

   /**Metodo DAO para obtener la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   */
   BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean);
   
   /**Metodo DAO para registrar el archivo a generar del CDA historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDA
    */
    BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist,
    		ArchitechSessionBean architechSessionBean);



}
