/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * BOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0    Dic 29 11:00:00 CST 2016 	Alan Garcia Villagran   Vector 	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico
**/
@Local
public interface DAOGenerarArchCDAHistSPID {

	/**Metodo DAO para obtener la fecha de operacion
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanResConsFechaOpDAOSPID Objeto del tipo BeanResConsFechaOpDAO
	   */
	BeanResConsFechaOpDAO consultaFechaOperacionSPID(ArchitechSessionBean architechSessionBean);

	/**Metodo DAO para registrar el archivo a generar del CDA historico
	    * @param beanReqGenArchCDSPIDAHist Objeto del tipo @see BeanReqGenArchCDAHist
	    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDA
	    */
	BeanResGenArchCDAHistDAO generarArchHistCDASPID(BeanReqGenArchCDAHist beanReqGenArchCDSPIDAHist, ArchitechSessionBean architechSessionBean);
}
