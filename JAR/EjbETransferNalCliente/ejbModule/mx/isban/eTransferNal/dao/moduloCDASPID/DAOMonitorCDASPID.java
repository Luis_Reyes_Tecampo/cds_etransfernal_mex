/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 09:59:16 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResEstadoConexionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el monitor CDA
**/
@Local
public interface DAOMonitorCDASPID{

   /**
    * Metodo que se encarga para saber si esta en contingencia o no
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResEstadoConexionDAO Objeto del tipo BeanResEstadoConexionDAO
   */
   public BeanResEstadoConexionDAO consultaEstadoConexion(ArchitechSessionBean architechSessionBean);

   /**
    * Metodo que sirve para consultar los movimientos CDA agrupados por monto y volumen
    *  @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    *  @return BeanResMonitorCdaDAO Objeto del tipo @see BeanResMonitorCdaDAO
    */
	public BeanResMonitorCdaDAO consultarCDAAgrupadas(
			final ArchitechSessionBean architechSessionBean);

	/**
	 * Metodo que sirve para actualiza la tabla de parametros para solicitar
	 * la detencion del servicio CDA.
	 *  @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 *  @return BeanResMonitorCdaDAO Objeto del tipo @see BeanResMonitorCdaDAO
	 */
	public BeanResMonitorCdaDAO detenerServicioCDA(
			final ArchitechSessionBean architechSessionBean);

}
