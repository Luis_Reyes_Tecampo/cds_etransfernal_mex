/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/12/2013 0:17:59 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovHistCDA;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovDetHistCdaDAOSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResConsMovHistCdaDAOSPID;

/**
 * Interfaz del tipo DAO que se encarga obtener la informacion para la consulta
 * de Movimientos Historicos CDA's.
 */
@Local
public interface DAOConsultaMovHistCDASPID {

	/**
	 * Metodo que se encarga de realizar la consulta de movimientos historicos CDA.
	 *
	 * @param beanConsMovHistCDA  Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
	 */
	BeanResConsMovHistCdaDAOSPID consultarMovHistCDASPID(
			BeanReqConsMovHistCDA beanConsMovHistCDA,
			ArchitechSessionBean sessionBean);

	/**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	BeanResConsMovHistCdaDAOSPID guardarConsultaMovHistExpTodoSPID(
			BeanReqConsMovHistCDA beanReqConsMovHistCDA,
			ArchitechSessionBean sessionBean);

	 /**
 	 * Metodo encargado de consultar el detalle de un movimiento CDA.
 	 *
 	 * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDADet
 	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanResConsMovDetHistCdaDAO Objeto del tipo @see BeanResConsMovDetHistCdaDAO
 	 */
	BeanResConsMovDetHistCdaDAOSPID consultarMovDetHistCDASPID(BeanReqConsMovCDADet beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para generar la consulta de exportar todo.
	 *
	 * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovHistCDA
	 * @param architechSessionBean the architech session bean
	 * @return String Objeto del tipo @see String
	 */
    String generarConsultaExportarTodoSPID(BeanReqConsMovHistCDA beanReqConsMovHistCDA, ArchitechSessionBean architechSessionBean);
}
