/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 09:59:16 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDASPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanReqMonCDAHistSPID;
import mx.isban.eTransferNal.beans.moduloCDASPID.BeanResMonitorCdaHistDAOSPID;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el monitor CDA
**/
@Local
public interface DAOMonitorCDAHistSPID{

 
	/**
	 * Metodo que sirve para consultar la informacion de los CDA's
	 * @param beanReqMonCDAHist Objeto del tipo @see BeanReqMonCDAHist
	 * @param architectSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResMonitorCdaHistBO Objeto del tipo @see BeanResMonitorCdaHistBO
	 * @exception BusinessException Excepcion de negocio
	 */
	BeanResMonitorCdaHistDAOSPID consultarCDAAgrupadasSPID(BeanReqMonCDAHistSPID beanReqMonCDAHist,ArchitechSessionBean architectSessionBean) 
	throws BusinessException;

}
