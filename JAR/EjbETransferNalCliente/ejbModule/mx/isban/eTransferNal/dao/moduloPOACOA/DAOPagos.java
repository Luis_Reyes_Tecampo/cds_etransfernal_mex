/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOPagos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 11:37:34 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;

/**
 * Interface DAOPagos.
 *
 * Interface que declara metodos necesarios para realizar los flujos
 * de consulta a BD para la 
 * pantalla de pagos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Local
public interface DAOPagos {

	/**
	 * Obtener listado pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo El objeto: modulo
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param beanPaginador El objeto: bean paginador
	 * @param session El objeto: session
	 * @return Objeto bean res listado pagos DAO
	 */
	BeanResListadoPagosDAO obtenerListadoPagos(BeanModulo modulo,
			String cveMecanismo, String estatus, BeanPaginador beanPaginador, ArchitechSessionBean session);
	
	/**
	 * Buscar pagos.
	 *
	 * Consultar los registros de 
	 * pagos disponibles.
	 * 
	 * @param modulo El objeto: modulo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param paginador El objeto: paginador
	 * @param session El objeto: session
	 * @return Objeto bean res listado pagos DAO
	 */
	BeanResListadoPagosDAO buscarPagos(BeanModulo modulo, String field, String valor, String cveMecanismo, String estatus, BeanPaginador paginador, ArchitechSessionBean session);
	
	/**
	 * Obtener listado pagos montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	BigDecimal obtenerListadoPagosMontos(BeanModulo modulo,
			String cveMecanismo, String estatus, ArchitechSessionBean session);
	
	/**
	 * Buscar pagos montos.
	 *
	 * Buscar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param cveMecanismo El objeto: cve mecanismo
	 * @param estatus El objeto: estatus
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	BigDecimal buscarPagosMontos(BeanModulo modulo, String field, String valor, String cveMecanismo, String estatus,ArchitechSessionBean session);

	
	/**
	 * Consultar detalle pago.
	 *
	 * Consulta el detalle de pago 
	 *
	 * @param referencia El objeto: referencia
	 * @param modulo El objeto: modulo
	 * @param session El objeto: session
	 * @return Objeto bean detalle pago
	 */
	BeanDetallePago consultarDetallePago(String referencia, BeanModulo modulo, ArchitechSessionBean session);
	
}
