/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOOrdenes.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 11:36:07 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;

/**
 * Interface DAOOrdenes.
 *
 * Interface que declara metodos necesarios para realizar los flujos
 * de consulta a BD para la 
 * pantalla de Ordenes de reparacion.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Local
public interface DAOOrdenes {
	
	/**
	 * Obtener ordenes reparacion.
	 *
	 * Consultar los registros disponibles
	 * 
	 * @param modulo El objeto: modulo
	 * @param beanPaginador El objeto: bean paginador
	 * @param session El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @return Objeto bean res orden reparacion DAO
	 */
	BeanResOrdenReparacionDAO obtenerOrdenesReparacion(BeanModulo modulo,
			BeanPaginador beanPaginador, ArchitechSessionBean session, String sortField, String sortType);
	
	/**
	 * Obtener ordenes reparacion montos.
	 *
	 * Consultar los importes
	 * 
	 * @param modulo El objeto: modulo
	 * @param session El objeto: session
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @return Objeto big decimal
	 */
	BigDecimal obtenerOrdenesReparacionMontos(BeanModulo modulo,
			 ArchitechSessionBean session, String sortField, String sortType);
	
	/**
	 * Actualizar odenes reparacion.
	 *
	 * Envia el registro para actualizarlo. 
	 * 
	 * @param modulo El objeto: modulo
	 * @param beanOrdenReparacion El objeto: bean orden reparacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res orden reparacion DAO
	 */
	BeanResOrdenReparacionDAO actualizarOdenesReparacion(BeanModulo modulo,
			BeanOrdenReparacion beanOrdenReparacion, ArchitechSessionBean architechSessionBean);
}
