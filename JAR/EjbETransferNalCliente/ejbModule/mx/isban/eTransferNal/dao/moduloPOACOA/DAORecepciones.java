/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAORecepciones.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 11:46:22 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * Interface DAORecepciones.
 *
 * Interface que declara metodos necesarios para realizar los flujos
 * de consulta a BD para la 
 * pantalla de Recepciones
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Local
public interface DAORecepciones {

	/**
	 * Obtener consulta recepciones.
	 *
	 * Consultar los registros disponibles. 
	 * 
	 * @param beanPaginador El objeto: bean paginador
	 * @param modulo El objeto: modulo
	 * @param beanReqConsultaRecepciones El objeto: bean req consulta recepciones
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto bean res consulta recepciones DAO
	 */
	BeanResConsultaRecepcionesDAO obtenerConsultaRecepciones(
			BeanPaginador beanPaginador, BeanModulo modulo, BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch,
			ArchitechSessionBean session);

	/**
	 * Obtener importe total.
	 *
	 * Consultar el monto total.
	 * 
	 * @param modulo El objeto: modulo
	 * @param tipoOrden El objeto: tipo orden
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto big decimal
	 */
	BigDecimal obtenerImporteTotal(BeanModulo modulo, String tipoOrden, String cveInst, String fch, ArchitechSessionBean session) ;
	
	/**
	 * Obtener det recepcion.
	 *
	 * Consultar detalle de la recepcion
	 * 
	 * @param beanLlave El objeto: bean llave
	 * @param modulo El objeto: modulo
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res recepcion DAO
	 */
	BeanResRecepcionDAO obtenerDetRecepcion(BeanSPIDLlave beanLlave, BeanModulo modulo,
			ArchitechSessionBean architechSessionBean);
}
