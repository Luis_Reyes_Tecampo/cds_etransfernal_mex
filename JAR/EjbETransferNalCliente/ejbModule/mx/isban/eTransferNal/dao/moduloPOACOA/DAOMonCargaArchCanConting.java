/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCargaArchConting.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:31:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResMonCargaArchCanContingDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad del Monitor de carga de archivos historico
**/
@Local
public interface DAOMonCargaArchCanConting{

   /**Metodo DAO para obtener la informacion del monitor de carga de
   * archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param fchOperacion String con la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchCanContingDAO Objeto del tipo BeanResMonCargaArchCanContingDAO
   * 
   */
   BeanResMonCargaArchCanContingDAO consultarMonCargaArchCanConting(BeanPaginador beanPaginador, String fchOperacion,
		   ArchitechSessionBean architechSessionBean);



}
