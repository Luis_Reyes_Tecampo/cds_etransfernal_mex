/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOArchivosRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqArchPago;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsCanalesDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;

/**
 * @author ooamador
 *
 */
@Local
public interface DAOArchivosOrdenPago {
	
	/**
	 * Inserta el nombre del archivo en Base de Datos.
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	BeanResBase registarNombreArchivo(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza el nombre del archivo en Base de Datos.
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */	
	BeanResBase actualizarNombreArchivo(BeanReqArchPago beanReqArchPago,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Consulta en BD los medios de entrega.
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResConsCanalesDAO bean de respuesta con los canales
	 */
	BeanResConsCanalesDAO consultarCanales(
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Retir
	 * @param beanReqArchPago Bean con los parametros
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO Bean de respuesta
	 */
	 BeanResDuplicadoArchDAO consultaDuplicidadNombre(BeanReqArchPago beanReqArchPago,
			final ArchitechSessionBean architectSessionBean);

}
