/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOHistorialMsj.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 12:39:17 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;

/**
 * Interface DAOHistorialMsj.
 *
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Local
public interface DAOHistorialMsj {

	/**
	 * Obtener historial.
	 *
	 * Consultar el historial de los  pagos.
	 * 
	 * @param modulo El objeto: modulo
	 * @param paginador El objeto: paginador
	 * @param session El objeto: session
	 * @param beanReqHistorialMensajes El objeto: bean req historial mensajes
	 * @return Objeto bean historial mensajes DAO
	 */
	BeanHistorialMensajesDAO obtenerHistorial(BeanModulo modulo, BeanPaginador paginador, ArchitechSessionBean session, BeanReqHistorialMensajes beanReqHistorialMensajes);
}
