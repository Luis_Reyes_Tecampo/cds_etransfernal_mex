/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOAvisos.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     25/09/2019 11:33:57 AM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;

/**
 * Interface DAOAvisos.
 *
 * Interface que declara metodos necesarios para realizar los flujos
 * de consulta a BD para la 
 * pantalla de pagos.
 * 
 * @author FSW-Vector
 * @since 25/09/2019
 */
@Local
public interface DAOAvisos {

	/**
	 * Obtener aviso.
	 *
	 * Consultar la lista de los avisos disponibles.
	 * 
	 * @param beanPaginador El objeto: bean paginador
	 * @param modulo El objeto: modulo
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @param session El objeto: session
	 * @return Objeto bean res aviso traspasos DAO
	 */
	BeanResAvisoTraspasosDAO obtenerAviso(BeanPaginador beanPaginador, BeanModulo modulo,
			String sortField, String sortType, String cveInst, String fch, ArchitechSessionBean session);
	
	/**
	 * Obtener importe total.
	 *
	 * Obetner el importe total para mostrar.
	 * 
	 * @param session El objeto: session
	 * @param modulo El objeto: modulo
	 * @param sortField El objeto: sort field
	 * @param sortType El objeto: sort type
	 * @param cveInst El objeto: cve inst
	 * @param fch El objeto: fch
	 * @return Objeto big decimal
	 */
	BigDecimal obtenerImporteTotal(
			ArchitechSessionBean session, BeanModulo modulo, String sortField,
			String sortType, String cveInst, String fch);
}
