/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConfiguracionCanales.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    23/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.catalogos.BeanResCanalDAO;
import mx.isban.eTransferNal.beans.catalogos.BeanResMedEntregaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanReqCanal;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaCanales;

/**
 * @author ooamador
 *
 */
@Local
public interface DAOConfiguracionCanales {

	/**
	 * Consulta en BD los medios de entrega.
	 * @param architechSessionBean Datos de sesion
	 * @return BeanResMedEntregaDAO Bean de respuesta
	 */
	  BeanResMedEntregaDAO consultaMediosEntrega(ArchitechSessionBean architechSessionBean);


	/**
	 * Consulta los canales del catalogo
	 * @param architectSessionBean Datos de sesion 
	 * @return BeanResCanalDAO Bean de respuesta con el listado de canales
	 **/
	  BeanResCanalDAO consultarCanales(
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Consulta los canales del catalogo con paginacion
	 * @param reqCanal DTO con los datos para la consulta
	 * @param architectSessionBean Datos de sesion 
	 * @return BeanResConsultaCanales Bean de respuesta
	 **/
	BeanResConsultaCanales consultarCanalesPag(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean);

	/**
	 * Actualiza si el canal esta activado/desactivado 
	 * para el proceso de contingencia
	 * @param reqCanal DTO con los datos para la actualizacion
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	BeanResBase actualizarEstatusCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean) ;

	/**
	 * Elimina el canal del catalogo.
	 * @param reqCanal DTO con los datos para eliminar el canal
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	BeanResBase eliminarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean);

	/**
	 * Metodo para insertar un canal
	 * @param reqCanal DTO con los datos para dar de alta el canal
	 * @param architectSessionBean datos de sesion
	 * @return BeanResBase Bean de respuesta
	 */
	BeanResBase insertarCanal(BeanReqCanal reqCanal,
			ArchitechSessionBean architectSessionBean);

}
