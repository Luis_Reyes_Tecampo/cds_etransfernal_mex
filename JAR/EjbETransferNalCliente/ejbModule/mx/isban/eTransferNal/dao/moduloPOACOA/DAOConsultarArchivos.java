/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultarArchivos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    28/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsultaArchRespuesta;

/**
 * Interface DAOConsultarArchivos.
 *
 * @author ooamador
 */
@Local
public interface DAOConsultarArchivos {
	
	/**
	 * Consulta de archivos a procesar.
	 *
	 * @param beanPaginador Bean para realizar la paginacion.
	 * @param fchOperacion String con la fecha de operacion
	 * @param modulo El objeto: modulo
	 * @param architechSessionBean Datos de session
	 * @return BeanResConsultaArchRespuesta Bean de respuesta
	 */
	BeanResConsultaArchRespuesta consultarArchivosProc(
			BeanPaginador beanPaginador,String fchOperacion, String modulo,
			ArchitechSessionBean architechSessionBean);


}
