/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOArchivosRespuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    27/09/2015 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResDuplicadoArchDAO;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanResValInicioPOACOA;

/**
 * Interface DAOArchivosRespuesta.
 *
 * @author ooamador
 */
@Local
public interface DAOArchivosRespuesta {
	
	/**
	 * Inserta el nombre del archivo en Base de Datos.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param fchOperacion String con la fecha de operacion
	 * @param institucion Institucion financiera
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase registarNombreArchivo(String nombreArchivo, String fchOperacion, String institucion, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Actualiza el nombre del archivo en Base de Datos.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param institucion Institucion financiera
	 * @param fchOperacion String con la fecha de operacion
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResBase bean de respuesta
	 */
	BeanResBase actualizarNombreArchivo(String nombreArchivo, String institucion,String fchOperacion, String modulo,
			ArchitechSessionBean architectSessionBean);
	
	/**
	 * Obtiene la institucion .
	 *
	 * @param modulo El objeto: modulo
	 * @param sessionBean Datos de sesion
	 * @return BeanResConsMiInstDAO
	 */
	BeanResConsMiInstDAO consultaMiInstitucion(String modulo, ArchitechSessionBean sessionBean);
	
	/**
	 * Metodo que se encarga de consultar para saber si hay dup�licados.
	 *
	 * @param nombreArchivo Nombre del archivo a insertar
	 * @param institucion Institucion financiera
	 * @param fchOperacion String fchOperacion
	 * @param modulo El objeto: modulo
	 * @param architectSessionBean Datos de sesion
	 * @return BeanResDuplicadoArchDAO bean de respuesta
	 */
	BeanResDuplicadoArchDAO consultaDuplicidadNombre(String nombreArchivo, String institucion, String fchOperacion, String modulo,
			final ArchitechSessionBean architectSessionBean);
	
  /**
   * Metodo DAO para obtener la fecha de operacion.
   *
   * @param modulo El objeto: modulo
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   */
   BeanResConsFechaOpDAO consultaFechaOperacion(String modulo, ArchitechSessionBean architechSessionBean);
   
   /**
    * Metodo que valida el inicio del POA/COA .
    *
    * @param cveInstitucion String con la cve de la institucion
    * @param fecha String con la fecha de operacion
    * @param modulo El objeto: modulo
    * @param architectSessionBean Objeto de la arquitectura
    * @return BeanResValInicioPOACOA Bean de respuesta del tipo BeanResValInicioPOACOA
    */
	BeanResValInicioPOACOA validaInicioPOACOA(String cveInstitucion, String fecha, String modulo,
				ArchitechSessionBean architectSessionBean);

}
