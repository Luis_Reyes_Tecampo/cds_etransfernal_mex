/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMonitores.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	            By 	                      Company 	   Description
 * ------- ----------------------   ------------------------  -----------  ----------------------
 * 1.0     6/08/2019 04:39:08 PM   Alfonso Hernandez Anaya.  Isban        Creacion de la clase
 */
package mx.isban.eTransferNal.dao.moduloPOACOA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.comunes.BeanSessionSPID;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanModulo;
import mx.isban.eTransferNal.beans.moduloPOACOA.BeanMonitor;

/**
 * Interface DAOMonitores.
 *
 * @author FSW-Vector
 * @since 6/08/2019
 */
@Local
public interface DAOMonitores {

	/**
	 * Obtener monitor.
	 *
	 * Obtener la informacion del DAO
	 *  
	 * @param modulo El objeto: modulo
	 * @param reqSession El objeto: req session
	 * @param session El objeto: session
	 * @return Objeto bean monitor
	 */
	BeanMonitor obtenerMonitor(BeanModulo modulo, BeanSessionSPID reqSession, ArchitechSessionBean session);
}
