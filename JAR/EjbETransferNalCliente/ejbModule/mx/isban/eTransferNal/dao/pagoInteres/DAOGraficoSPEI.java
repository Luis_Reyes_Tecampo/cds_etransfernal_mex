/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGraficoSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    31/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.pagoInteres;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqGrafico;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResGraficoDAO;

@Local
public interface DAOGraficoSPEI {
	
	/**
	 * Metodo que regresa los datos de SPEI
	 * @param req Bean del tipo BeanReqGrafico
	 * @param sessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResGraficoDAO Bean de Respuesta
	 */
	BeanResGraficoDAO consultaDatosSPEI(BeanReqGrafico req,ArchitechSessionBean sessionBean);
	
	 /**
	 * Metodo que permite lanzar la consulta a la bd 
	 * @param sessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResGraficoDAO bean con el rango en minutos
	 */
	BeanResGraficoDAO consultaRango(ArchitechSessionBean sessionBean);
}
