/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORepPagoInteresCompSPEI.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    31/08/2016 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.pagoInteres;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.pagoInteres.BeanRangoTiempoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanReqReportePago;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResEstatusDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResMedioEntDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResReportePagoDAO;
import mx.isban.eTransferNal.beans.pagoInteres.BeanResTipoPagoDAO;

@Local
public interface DAORepPagoInteresCompSPEI {
	
	/**
	 * Metodo que sirve para obtener el catalogo de tipos de pago
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanResTipoPagoDAO Bean con los datos del catalogo
	 */
	BeanResTipoPagoDAO consultaTipoPago(ArchitechSessionBean session);
	
	/**
	 * Metodo que sirve para obtener el catalogo de Estatus
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanResEstatusDAO Bean con los datos del catalogo
	 */
	BeanResEstatusDAO consultaEstatus(ArchitechSessionBean session);
	
	/**
	 * Metodo que sirve para obtener el catalogo de Medios de entrega
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanResMedioEntDAO Bean con los datos del catalogo
	 */
	BeanResMedioEntDAO consultaMedioEnt(ArchitechSessionBean session);

	/**
	 * Metodo que sirve para realizar la consulta sobre la tabla TRAN_SPEI_INTERES
	 * @param req Objeto del tipo BeanReqReportePago que almacena los parametros para la consulta
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanResReportePagoDAO Bean con los datos de la tabla de TRAN_SPEI_INTERES
	 */
	BeanResReportePagoDAO consultaReportePago(BeanReqReportePago req, ArchitechSessionBean session);
	
	/**
	 * Metodo que sirve para realizar la consulta sobre la tabla TRAN_SPEI_INTERES
	 * @param req Objeto del tipo BeanReqReportePago que almacena los parametros para la consulta
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanResReportePagoDAO Bean con los datos de la tabla de TRAN_SPEI_INTERES
	 */
	BeanResReportePagoDAO expTodoReportePago(BeanReqReportePago req,ArchitechSessionBean session);

	/**
	 * Metodo que sirve para obtener el rango de tiempo
	 * @param session Objeto de la arquitectura ArchitechSessionBean 
	 * @return BeanRangoTiempoDAO Bean con los datos del rango de tiempo
	 */
	BeanRangoTiempoDAO consultaRango(ArchitechSessionBean session);

}
