/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAODetalleOperacion.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   10/10/2018 05:11:28 PM Alfonso Hernandez Anaya. Isban Creacion
 */
package mx.isban.eTransferNal.dao.contingencia;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.contingencia.BeanResTranCanales;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;


/**
 * Interface DAODetalleOperacion.
 * 
 * Define los metodos que se utilizan en la capa de negocio
 *
 * @author FSW-Vector
 * @since 10/10/2018
 */
@Local
public interface DAODetalleOperacion {

	
	/**
	 * Consultar.
	 *
	 * @param session El objeto: session
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param field El objeto: field
	 * @param valor El objeto: valor
	 * @param beanPaginador El objeto: bean paginador
	 * @param nombrePantalla El objeto: nombre pantalla
	 * @return Objeto bean res tran canales
	 */
	BeanResTranCanales consultar(ArchitechSessionBean session, String nombreArchivo, String field, String valor, 
			BeanPaginador beanPaginador,String nombrePantalla);
}
