package mx.isban.eTransferNal.dao.contingencia;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.contingencia.BeanArchivoDuplicado;
import mx.isban.eTransferNal.beans.contingencia.BeanConsultaOperContingentes;
import mx.isban.eTransferNal.beans.contingencia.BeanReqConsultaOperContingente;
import mx.isban.eTransferNal.beans.contingencia.BeanResConFecha;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsCanalesD;
import mx.isban.eTransferNal.beans.contingencia.BeanResConsultaOperContingente;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;



/**
 * The Interface DAOConsultaOperContingentes.
 *
 * @author FSW-Vector
 * @since 22/10/2018
 */
public interface DAOConsultaOperContingentes {
	
	/**
	 * Consulta oper contingentes.
	 *
	 * @param arquite El objeto: arquite
	 * @param beanPaginador El objeto: bean paginador
	 * @param bean El objeto: bean
	 * @return Objeto bean res consulta oper contingente
	 */
	BeanResConsultaOperContingente consultaOperContingentes(ArchitechSessionBean arquite,BeanPaginador beanPaginador,BeanReqConsultaOperContingente bean );
	
	/**
	 * Consultar canal.
	 *
	 * @param session El objeto: session
	 * @return Objeto bean res cons canales D
	 */
	BeanResConsCanalesD consultarCanal(ArchitechSessionBean session);
	
	/**
	 * Consulta fecha operacion.
	 *
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res con fecha
	 */
	BeanResConFecha consultaFechaOperacion(ArchitechSessionBean architechSessionBean);
 	
 	/**
	  * Valida archivo dupl.
	  *
	  * @param beanArchivo El objeto: bean archivo
	  * @param session El objeto: session
	  * @param bean El objeto: bean
	  * @return Objeto bean archivo duplicado
	  */
	 BeanArchivoDuplicado validaArchivoDupl(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session, BeanReqConsultaOperContingente bean);
	 
	/**
	 * Registra archivo.
	 *
	 * @param beanArchivo El objeto: bean archivo
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res base
	 */
	BeanResBase registraArchivo(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session,BeanReqConsultaOperContingente bean);
	 
	/**
	 * Actualiza estatus.
	 *
	 * @param beanArchivo El objeto: bean archivo
	 * @param session El objeto: session
	 * @param bean El objeto: bean
	 * @return Objeto bean res base
	 */
	BeanResBase actualizaEstatus(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session,BeanReqConsultaOperContingente bean);

    /**
     * Actualiza nombre.
     *
     * @param beanArchivo El objeto: bean archivo
     * @param session El objeto: session
     * @return Objeto bean res base
     */
    BeanResBase actualizaNombre(BeanConsultaOperContingentes beanArchivo, ArchitechSessionBean session);


}
