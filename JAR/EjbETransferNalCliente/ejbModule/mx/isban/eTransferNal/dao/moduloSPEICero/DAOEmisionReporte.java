/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOEmisionReporte.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/04/2018      SMEZA      VECTOR    	Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPEICero;

/**
 * Anexo de Imports para la funcionalidad del Modulo SPEI Cero
 * 
 * @author FSW-Vector
 * @sice 26 Abril 2018
 *
 */
import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanConsMiInstitucion;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanFechaHorario;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanReqSpeiCero;
import mx.isban.eTransferNal.beans.moduloSPEICero.BeanResSpeiCero;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion siguiente:
 *  - Estatus Operacion
 *  - Hora Inicio SPEI CERO 
 *  - Horario cambio fecha operacion.
 */
@Local
public interface DAOEmisionReporte {
	
	/**
	 * Cons mi institucion.
	 *
	 * @param clave the clave
	 * @param sessionBean the session bean
	 * @return the bean cons mi institucion
	 */	
	BeanConsMiInstitucion consMiInstitucion(String clave, ArchitechSessionBean sessionBean);
	
	
	

	
	/**
	 * Cons horario cambio.
	 *
	 * @param sessionBean the session bean
	 * @param entrada the entrada
	 * @return the bean res emi reporte
	 */
	BeanFechaHorario consHorarioCambio(ArchitechSessionBean sessionBean, BeanConsMiInstitucion entrada);
	
	
	/**
	 * Cons estatus opera.
	 *
	 * @param sessionBean the session bean
	 * @param entrada the entrada
	 * @param historico
	 * @return the bean BeanResSpeiCero
	 */
	BeanResSpeiCero incializa(ArchitechSessionBean sessionBean, BeanConsMiInstitucion entrada,boolean historico) ;
	
	/**
	 * funcion que realiza la busqueda
	 * @param sessionBean
	 * @param beanReqSpeiCero
	 * @return
	 */
	BeanResSpeiCero buscarDatos(ArchitechSessionBean sessionBean, BeanReqSpeiCero beanReqSpeiCero) ;
	
	/**
	 * funcion que arma la consulta del reporte
	 * @param beanReqSpeiCero
	 * @param sessionBean
	 * @return
	 */
	 String consultaRptExportar(BeanReqSpeiCero beanReqSpeiCero,ArchitechSessionBean sessionBean);
	 
	 /**
	  * funcion para generar un edo de cuenta
	  * @param bean
	  * @param sessionBean
	  * @return
	  */
	 BeanResBase generarEdoCta(BeanReqSpeiCero bean,ArchitechSessionBean sessionBean);
}
