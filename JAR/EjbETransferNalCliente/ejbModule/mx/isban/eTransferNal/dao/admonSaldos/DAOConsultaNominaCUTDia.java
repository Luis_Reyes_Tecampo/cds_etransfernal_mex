package mx.isban.eTransferNal.dao.admonSaldos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResUsrPassDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.ws.BeanResCutSpeiDAO;

@Local
public interface DAOConsultaNominaCUTDia {
	/**
	 * @param sessionBean Objeto de arquitectura
	 * @return BeanResUsrPassDAO Bean con el usuario y password
	 */
	BeanResUsrPassDAO consultaUsrPass(ArchitechSessionBean sessionBean);
	/**
	 * @param sessionBean Objeto de arquitectura
	 * @return BeanResCutSpeiDAO Bean con los datos de respuesta
	 */
	BeanResCutSpeiDAO consultaNominaCUT(ArchitechSessionBean sessionBean, BeanResConsFechaOpDAO beanResConsFechaOpDAO);

}
