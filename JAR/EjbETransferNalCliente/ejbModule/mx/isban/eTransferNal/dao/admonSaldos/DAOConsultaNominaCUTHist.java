package mx.isban.eTransferNal.dao.admonSaldos;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.admonSaldo.BeanReqConsNominaCUT;
import mx.isban.eTransferNal.beans.admonSaldos.BeanResConsNomCUTHistDAO;

@Local
public interface DAOConsultaNominaCUTHist {
	
	/**
	 * @param req Bean con los datos de entrada
	 * @param sessionBean Objeto de sesion de la arquitectura
	 * @return BeanResConsNomCUTHistDAO bean con los datos de respuesta
	 */
	BeanResConsNomCUTHistDAO consultarNomina(BeanReqConsNominaCUT req,ArchitechSessionBean sessionBean);

}
