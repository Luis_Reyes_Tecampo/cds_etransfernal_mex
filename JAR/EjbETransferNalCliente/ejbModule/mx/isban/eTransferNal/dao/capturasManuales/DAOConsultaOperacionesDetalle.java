package mx.isban.eTransferNal.dao.capturasManuales;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Interface DAOConsultaOperacionesDetalle.
 */
@Local
public interface DAOConsultaOperacionesDetalle {
	
	/**
	 * Obtener template.
	 *
	 * @param beanReqConsOperaciones the bean req cons operaciones
	 * @param architechSessionBean the architech session bean
	 * @return the bean res cons operaciones
	 */
	BeanResConsOperaciones obtenerTemplate(BeanReqConsOperaciones beanReqConsOperaciones,ArchitechSessionBean architechSessionBean);
	
	/**
	 * Permisos apartar.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res base
	 */
	BeanResBase permisosApartar(ArchitechSessionBean sessionBean, String folio);
	
	/**
	 * Permisos modificar.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res base
	 */
	BeanResBase permisosModificar(ArchitechSessionBean sessionBean, String folio);
	

	/**
	 * Guardar operacion.
	 *
	 * @param sessionBean the session bean
	 * @param operacion the operacion
	 * @return the bean res base
	 */
	BeanResBase guardarOperacion(ArchitechSessionBean sessionBean, BeanOperacion operacion);
}
