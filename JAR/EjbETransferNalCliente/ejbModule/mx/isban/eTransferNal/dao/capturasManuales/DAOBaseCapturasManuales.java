/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOBaseCapturasManuales.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   13/06/2017 04:35:43 Omar Zuniga Lagunas Vector Creacion
 */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCombo;

/**
 * Interface DAOBaseCapturasManuales.
 *
 * @author FSW-Vector
 * @since 21/03/2017
 */
@Local
public interface DAOBaseCapturasManuales {
	
	/**
	 * Obtener el objeto: combo valores.
	 *
	 * @param query El objeto: query
	 * @param session the session
	 * @return El objeto: combo valores
	 */
	//Metodo que obtiene los valores cuando hay combos
	List<BeanCombo> getComboValores(String query, ArchitechSessionBean  session);

}
