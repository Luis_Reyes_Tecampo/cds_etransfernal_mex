/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   21/03/2017 04:35:43 PM Juan Jesus Beltran R. Isban Creacion
 */
package mx.isban.eTransferNal.dao.capturasManuales;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralRequest;
import mx.isban.eTransferNal.beans.capturasManuales.BeanCapturaCentralResponse;
import mx.isban.eTransferNal.beans.capturasManuales.BeanOperacion;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * Interface DAOCapturaCentral.
 *
 * @author FSW-Vector
 * @since 21/03/2017
 */
@Local
public interface DAOCapturaCentral {
	
	/**
	 * Consulta todos los templates.
	 *
	 * @param request El objeto: request
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean captura central response
	 */
	BeanCapturaCentralResponse consultaTodosTemplates(
			BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Consulta template.
	 *
	 * @param request El objeto: request
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean captura central response
	 */
	BeanCapturaCentralResponse consultaTemplate(
			BeanCapturaCentralRequest request,
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Alta captura manual spid.
	 *
	 * @param operacion El objeto: operacion
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean res base
	 */
	BeanResBase altaCapturaManualSPID(BeanOperacion operacion,
			ArchitechSessionBean architechSessionBean);

}
