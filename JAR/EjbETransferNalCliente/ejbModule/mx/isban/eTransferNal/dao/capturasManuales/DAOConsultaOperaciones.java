package mx.isban.eTransferNal.dao.capturasManuales;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsOperaciones;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsOperaciones;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * The Interface DAOConsultaOperaciones.
 */
@Local
public interface DAOConsultaOperaciones {
	
	/**
	 * Consultar operaciones.
	 *
	 * @param session the session
	 * @param req the req
	 * @return BeanResponseConsOperacionesDAO Bean con los datos de la consulta
	 */
	BeanResConsOperaciones consultarOperaciones(ArchitechSessionBean session,BeanReqConsOperaciones req);
	
	/**
	 * Consultar folios apartados.
	 *
	 * @param session the session
	 * @param req the req
	 * @return the bean res cons operaciones
	 */
	BeanResConsOperaciones consultarFoliosApartados(ArchitechSessionBean session, BeanReqConsOperaciones req);
	
	/**
	 * Apartar desapartar folio.
	 *
	 * @param sessionBean the session bean
	 * @param beanConsOperaciones the bean cons operaciones
	 * @param apartando the apartando
	 * @return the bean res base
	 */
	BeanResBase apartarDesapartarFolio(ArchitechSessionBean sessionBean, BeanConsOperaciones beanConsOperaciones, Boolean apartando);
		
	/**
	 * Eliminar folio.
	 *
	 * @param sessionBean the session bean
	 * @param beanConsOperaciones the bean cons operaciones
	 * @return the bean res base
	 */
	BeanResBase eliminarFolio(ArchitechSessionBean sessionBean, BeanConsOperaciones beanConsOperaciones);
	
	/**
	 * Exportar las operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param req the req
	 * @param beanResConsOperaciones the bean res cons operaciones
	 * @return the bean res base
	 */
	BeanResBase exportarTodo( ArchitechSessionBean sessionBean, BeanReqConsOperaciones req, BeanResConsOperaciones beanResConsOperaciones);
}
