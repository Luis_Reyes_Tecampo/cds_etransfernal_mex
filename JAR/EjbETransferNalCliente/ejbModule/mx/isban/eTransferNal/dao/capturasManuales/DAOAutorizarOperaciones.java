/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOCapturaCentral.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0   22/03/2017 01:10:00 PM Alan Garcia Villagran R. Isban Creacion
 */
package mx.isban.eTransferNal.dao.capturasManuales;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAutorizarOpe;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAutorizarOpeDAO;

/**
 * The Interface DAOAutorizarOperaciones.
 */
@Local
public interface DAOAutorizarOperaciones {

	/**
	 * Consultar operaciones pendientes.
	 *
	 * @param sessionBean the session bean
	 * @param req the req
	 * @return the bean res autorizar ope dao
	 */
	BeanResAutorizarOpeDAO consultarOperacionesPendientes(ArchitechSessionBean sessionBean, BeanReqAutorizarOpe req);

	/**
	 * Apartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope
	 */
	BeanResAutorizarOpeDAO apartarOPeraciones(ArchitechSessionBean sessionBean, String folio);
	
	/**
	 * Desapartar o peraciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope
	 */
	BeanResAutorizarOpeDAO desapartarOPeraciones(ArchitechSessionBean sessionBean, String folio) ;
	
	/**
	 * Autorizar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope
	 */
	BeanResAutorizarOpeDAO autorizarOperaciones(ArchitechSessionBean sessionBean, String folio);
	
	/**
	 * Reparar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope
	 */
	BeanResAutorizarOpeDAO repararOperaciones(ArchitechSessionBean sessionBean, String folio) ;
	
	/**
	 * Cancelar operaciones.
	 *
	 * @param sessionBean the session bean
	 * @param folio the folio
	 * @return the bean res autorizar ope
	 */
	BeanResAutorizarOpeDAO cancelarOperaciones(ArchitechSessionBean sessionBean, String folio) ;

	/**
	 * Consultar usr admin.
	 *
	 * @param sessionBean the session bean
	 * @return the bean res autorizar ope DAO
	 */
	BeanResAutorizarOpeDAO consultarUsrAdmin(ArchitechSessionBean sessionBean);
	
	/**
	 * Consulta operaciones apartadas.
	 *
	 * @param req the req
	 * @param params the params
	 * @param sessionBean the session bean
	 * @return the BeanResAutorizarOpeDAO
	 */
	BeanResAutorizarOpeDAO consultaOperacionesApartadas(BeanReqAutorizarOpe req, List<Object> params, ArchitechSessionBean sessionBean);
	
	/**
	 * Consulta operaciones apartadas por pagina.
	 *
	 * @param req the req
	 * @param params the params
	 * @param sessionBean the session bean
	 * @return the BeanResAutorizarOpeDAO
	 */
	BeanResAutorizarOpeDAO consultaOperacionesApartadasPorPagina(BeanReqAutorizarOpe req, List<Object> params, ArchitechSessionBean sessionBean);
	
}
