/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMantenimientoTemplates.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Mar 28 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */
package mx.isban.eTransferNal.dao.capturasManuales;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqConsultaTemplates;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResConsultaTemplatesDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;

/**
 * 
 * Interfaz DAO que actua como firma de la capa de persistencia
 *
 */
@Local
public interface DAOMantenimientoTemplates {
	
	/**
	 * Metodo que permite obtener la lista de templates disponibles para el mantenimiento
	 * @param beanReq  Objeto con los parametros de consulta
	 * @param session  Objeto general de sesion @see ArchitechSessionBean
	 * @return Objeto que expone la lista de templates disponibles
	 */
	BeanResConsultaTemplatesDAO consultarTemplates(BeanReqConsultaTemplates beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo a traves del cual se invoca la accion de guardar el template en la BD
	 * @param beanReq  Objeto con las propiedades del template
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de guardar informacion de template
	 */
	BeanResBase guardarTemplate(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo a traves del cual se invoca la eliminacion de templates en la BD
	 * @param template template a eliminar
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de eliminar templates
	 */
	BeanResBase eliminarTemplate(BeanTemplate template, ArchitechSessionBean session);
	
	/**
	 * Metodo a traves del cual se invoca la accion de actualizar el template en la BD
	 * @param beanReq  Objeto con las propiedades del template
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return  Objeto con la respuesta del proceso de actualizar informacion de template
	 */
	BeanResBase actualizarFechaTemplate(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite guardar un usuario para un template especifico
	 * @param beanReq Objeto con la informacion del usuario a guardar
	 * @param session Objeto genereal de session @see ArchitechSessionBean 
	 * @return Objeto con la respuesta del proceso de guardar usuario del template
	 */
	BeanResBase actualizarUsuarios(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite verificar si un usuario existe en el template
	 * @param beanUsuario Objeto que contiene la informacion del usuario a verificar
	 * @param session Objeto genereal de session @see ArchitechSessionBean 
	 * @return Objeto con la respuesta del proceso de verificar usuario
	 */
	BeanResBase verificarExistenciaUsuario(BeanUsuarioAutorizaTemplate beanUsuario, ArchitechSessionBean session);
	
}
