/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * 
 * Clase: DAOMantenimientoTemplatesExt.java
 * 
 * Control de versiones:
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 * 1.0	Tue Apr 07 11:00:00 GTM 2017	Hermenegildo Fernandez Santos	Nexsol	Creacion 
 * */

package mx.isban.eTransferNal.dao.capturasManuales;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.capturasManuales.BeanResAltaEditTemplateDAO;
import mx.isban.eTransferNal.beans.capturasManuales.BeanReqAltaEditTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanTemplate;
import mx.isban.eTransferNal.beans.capturasManuales.BeanUsuarioAutorizaTemplate;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
/**
 * 
 * Interfaz DAO que actua como firma de la capa de persistencia de servicios externos del template
 *
 */
public interface DAOMantenimientoTemplatesExt {
	
	/**
	 * Metodo que permite obtener la lista de layouts disponibles para el componente de altas
	 * @param beanReq Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Lista de layouts para el componente de altas
	 */
	BeanResAltaEditTemplateDAO obtenerLayouts(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite obtener la lista de usuarios disponibles para asignarlos como autorizados
	 * @param beanReq Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Lista de usuarios disponibles para para asignarlos a autorizados
	 */
	BeanResAltaEditTemplateDAO obtenerUsuarios(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite obtener los campos a utilizarse en la alta de templates
	 * @param beanReq Objeto con los parametros de consulta
	 * @param session Objeto general de session @see ArchitechSessionBean
	 * @return Lista de campos  del layout a utilizarse en la alta
	 */
	BeanResAltaEditTemplateDAO obtenerCamposLayout(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo a traves del cual se verifica que un template candidato a eliminar no tenga operaciones asociadas
	 * @param template candidato a eliminar
	 * @param session  Objeto genereal de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de verificar operaciones
	 */
	BeanResBase verificarOperaciones(BeanTemplate template, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite verificar si un usuario ha sido asignado para autorizar el template en la tabla operaciones
	 * @param beanUsuario Objeto que contiene la informacion del usuario a verificar
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return con la respuesta del rpoceso de verificar usuario en operaciones
	 */
	BeanResBase verificarExistenciaOperacionesUsuarioTemplate(BeanUsuarioAutorizaTemplate beanUsuario, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite actualizar un campo del template especifico
	 * @param beanReq Objeto con la informacion del campo a actualizar
	 * @param session Objeto genereal de session @see ArchitechSessionBean
	 * @return Objeto con la respuesta del proceso de actualizar campo del template
	 */
	BeanResBase actualizarCampos(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
	/**
	 * Metodo que permite obtener los datos de un template:  template, usuarios, campos
	 * @param beanReq Objeto con las propiedades del template
	 * @param session Objeto genereal de session @see ArchitechSessionBean 
	 * @return Objeto con la respuesta de busqueda del template
	 */
	BeanResAltaEditTemplateDAO obtenerTemplate(BeanReqAltaEditTemplate beanReq, ArchitechSessionBean session);
	
}
