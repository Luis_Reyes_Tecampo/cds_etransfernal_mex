/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   12/12/2013 17:08:32 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetCdaDAO;


/*
 * Interfaz del tipo DAO que se encarga obtener la informacion para la consulta
 * de Movimientos CDAs
 */
@Local
public interface DAOConsultaMovCDA {

	/**
     * Metodo que se encarga de realizar la consulta de los movimientos CDA
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	BeanResConsMovCdaDAO consultarMovCDA(
			BeanReqConsMovCDA beanReqConsMovCDA,
			ArchitechSessionBean sessionBean);

	/**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	BeanResConsMovCdaDAO guardarConsultaMovExpTodo(
			BeanReqConsMovCDA beanReqConsMovCDA,
			ArchitechSessionBean sessionBean);

	/**
     * Metodo encargado de consultar el detalle de un movimiento CDA
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovCDADet
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean 
     * @return BeanResConsMovDetCdaDAO Objeto del tipo @see BeanResConsMovDetCdaDAO
     */
	BeanResConsMovDetCdaDAO consultarMovDetCDA(BeanReqConsMovCDADet beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean);

}
