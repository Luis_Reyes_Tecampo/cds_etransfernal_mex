/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAODetRecepOperacion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   29/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanDetRecepOperacionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResBase;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion para
 * el detalle de recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 1/10/2018
 */
@Local
public interface DAODetRecepOperacion {
		 
	 /**
 	 * Generar autorizacion Historica
 	 *
 	 * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
 	 * @param architechSessionBean El objeto: architech session bean
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto con la respuesta
 	 */
	 BeanDetRecepOperacionDAO generarAutorizacionHTO(BeanDetRecepOperacionDAO bean,ArchitechSessionBean architechSessionBean, boolean historico, BeanReqTranSpeiRec beanReqTranSpeiRec);
 	
 	
 	/**
	  * Valida si Existe operacion.
	  *
	  * @param architechSessionBean El objeto: architech session bean de la arquitectura
	  * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
	  * @return Objeto la respuesta basica
	  */
	 BeanResBase existeOperacion(ArchitechSessionBean architechSessionBean, BeanTranSpeiRecOper bean);
	 
	 /**
 	 * Realiza la actualizacion de la 
 	 * Cuenta Receptora
 	 *
 	 * @param architechSessionBean El objeto: architech session bean de la arquitectura
 	 * @param bean El objeto: el objeto ocn la informacion del requistro a actualizar
 	 * @param parametros El objeto: los parametros para ejecutar el query
 	 * @param pantalla El objeto: el identificador para validar el modulo (Alterna / Principal)
 	 * @return Objeto de respuesta
 	 */
	 BeanDetRecepOperacionDAO updateCtaReceptora(ArchitechSessionBean architechSessionBean, BeanDetRecepOperacionDAO bean, List<Object> parametros,String pantalla) ;

	 
	
	 /**
 	 * Eliminar registro.
 	 *
 	 * @param architechSessionBean El objeto: architech session bean de la aarquitectura
 	 * @param bean El objeto: el objeto con la informacion de la operacion a eliminar
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto bean de respuesta
 	 */
 	BeanTranSpeiRecOper eliminarRegistro(ArchitechSessionBean architechSessionBean, BeanTranSpeiRecOper bean,boolean historico);
 	
 	
 	/**
	  * Metodo que valida si Existe la operacion
	  *
	  * @param bean El objeto: el objeto con la infromacion de la oppperacion
	  * @param historico El objeto: la bandera que identifica si es historico
	  * @return Objeto bean de respuesta
	  */
	 BeanTranSpeiRecOper existeBeanOper(BeanTranSpeiRecOper bean, boolean historico);
	 
	 
	 /**
 	 * Nuevo registro historico.
 	 *
 	 * @param bean El objeto: El objeto con la informacion d ela operacion
 	 * @param architechSessionBean El objeto: architech session bean de la arquitectura
 	 * @param historico El objeto: la bandera que identifica si es historico
 	 * @return Objeto bean con el contenido de la operacion actualizada
 	 */
	 BeanTranSpeiRecOper nuevoRegistroHTO(BeanTranSpeiRecOper bean,ArchitechSessionBean architechSessionBean, boolean historico);


	/**
	 * Consulta detalle operacion.
	 *
	 * Metodo para consultar los datos del nuevo registro de 
	 * operacion y mostrarlos en pantalla.
	 * 
	 * @param bean El objeto: bean, con la informacion de la operacion de la que se requiere ver el detalle 
	 * @param architechSessionBean El objeto: architech session bean de la aquiterctura
	 * @return Objeto bean detalle operacion (Contiene los datos del detalle de la operacion)
	 */
	BeanDetRecepOperacionDAO consultaDetRecepcionOp(BeanDetRecepOperacionDAO bean,
			ArchitechSessionBean architechSessionBean);

}
