/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 18:24:31 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico
**/
@Local
public interface DAOGenerarArchCDAHist{

   /**Metodo DAO para obtener la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @param esSPID Indica si es SPID
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   */
   public BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean, boolean esSPID);
   
   /**Metodo DAO para registrar el archivo a generar del CDA historico
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDA
    */
    public BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchCDAHist beanReqGenArchCDAHist,
    		ArchitechSessionBean architechSessionBean);



}
