package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;

@Local	    
public interface DAORecepOperacion2 {
    /**
     * Metodo que actualiza
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO actualizaTransSpeiRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean);
    
    
    /**
     * Metodo que actualiza el motivo de rechazo
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO actualizaMotivoRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean);
    
    /**
     * @param beanReqActTranSpeiRec bean del tipo BeanReqActTranSpeiRec
     * @param architechSessionBean Objeto de ArchitechSessionBean
     */
    public void guardaTranSpeiHorasMan(BeanReqActTranSpeiRec beanReqActTranSpeiRec, 
      		 ArchitechSessionBean architechSessionBean);
    /**
     * Metodo que rechaza una operacion
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO rechazar(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
     		 ArchitechSessionBean architechSessionBean);

}
