/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHistDet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Sun Dec 15 13:47:03 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchCDAHistDet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsGenArchHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResElimSelecArchCDAHistDetDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDetDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico Detalle
**/
@Local
public interface DAOGenerarArchCDAHistDet{

  /**Metodo DAO que sirve para consultar la informacion de la generacion
   * de archivos CDA historico detalle
   * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsGenArchHistDetDAO Objeto del tipo BeanResConsGenArchHistDetDAO
   */
  public BeanResConsGenArchHistDetDAO consultaGenArchHistDet(
		  BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet, 
			   BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);   
  /**Metodo que sirve para marcar los registros como eliminados
   * @param beanReqConsGenArchCDAHistDet Objeto del tipo @see BeanReqConsGenArchCDAHistDet
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResElimSelecArchCDAHistDetDAO Objeto del tipo BeanResElimSelecArchCDAHistDetDAO
   * 
   */
   public BeanResElimSelecArchCDAHistDetDAO elimSelGenArchCDAHistDet(
		   BeanReqConsGenArchCDAHistDet beanReqConsGenArchCDAHistDet,
		   ArchitechSessionBean architechSessionBean);

   /**Metodo que sirve para mandar a generar el archivo de CDA historico
    * detalle
    * @param beanReqGenArchCDAHistDet Objeto del tipo @see BeanReqGenArchCDAHistDet
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDetDAO Objeto del tipo BeanResGenArchCDAHistDetDAO
    */
    public BeanResGenArchCDAHistDetDAO generarArchCDAHistDet(BeanReqGenArchCDAHistDet beanReqGenArchCDAHistDet,
    		ArchitechSessionBean architechSessionBean);


}
