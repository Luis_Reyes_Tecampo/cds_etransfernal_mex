/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOConsultaMovHistCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   17/12/2013 0:17:59 Jessica Lizeth Leon Ruiz  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovMonCDADet;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsMovMonHistCDA;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovDetHistCdaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMovHistCdaDAO;


/**
 * 
 * Interfaz del tipo DAO que se encarga obtener la informacion para la consulta
 * de Movimientos Historicos CDA's
 *
 */
@Local
public interface DAOConsultaMovMonHistCDA {

	/**
     * Metodo que se encarga de realizar la consulta de movimientos historicos CDA
     * @param beanConsMovHistCDA  Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	BeanResConsMovHistCdaDAO consultarMovHistCDA(
			BeanReqConsMovMonHistCDA beanConsMovHistCDA,
			ArchitechSessionBean sessionBean);

	/**
     * Metodo encargado de generar la consulta de para insertar la solicitud de generacion
     * del archivo con todos los movimientos CDA.
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @param sessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMovCdaDAO Objeto del tipo @see BeanResConsMovCdaDAO
     */
	BeanResConsMovHistCdaDAO guardarConsultaMovHistExpTodo(
			BeanReqConsMovMonHistCDA beanReqConsMovHistCDA,
			ArchitechSessionBean sessionBean);

	 /**
     * Metodo encargado de consultar el detalle de un movimiento CDA
     * @param beanReqConsMovCDA Objeto del tipo @see BeanReqConsMovMonCDADet
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean 
     * @return BeanResConsMovDetHistCdaDAO Objeto del tipo @see BeanResConsMovDetHistCdaDAO
     */
	BeanResConsMovDetHistCdaDAO consultarMovDetHistCDA(BeanReqConsMovMonCDADet beanReqConsMovCDA,
			ArchitechSessionBean architechSessionBean);
	
	/**
     * Metodo para generar la consulta de exportar todo
     * @param beanReqConsMovHistCDA Objeto del tipo @see BeanReqConsMovMonHistCDA
     * @return String Objeto del tipo @see String
     */
    String generarConsultaExportarTodo(BeanReqConsMovMonHistCDA beanReqConsMovHistCDA);
}
