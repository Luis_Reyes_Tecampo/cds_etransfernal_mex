/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOPerfilamiento.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:31:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqConsPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsCatPerfilDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsPerfilDAO;

/**
 *Interface del tipo DAO que se encarga  obtener la informacion para
 * el perfilamiento
**/
@Local
public interface DAOPerfilamiento {
	
	/**
	 * Metodo que permite consultar los servicios y tareas que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsPerfilDAO consultaServicio(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que permite consultar las tareas en un servicios que contiene un perfil
	 * @param beanReqConsPerfil Objeto del tipo BeanReqConsPerfil que almacena los parametros de la consulta
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsPerfilDAO consultaServicioTareas(BeanReqConsPerfil beanReqConsPerfil, 
			ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que permite consultar el catalogo de perfiles
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean parte de la arquitectura
	 * @return BeanResConsPerfilDAO Objeto del tipo BeanResConsPerfilDAO
	 */
	public BeanResConsCatPerfilDAO consultaPerfiles(ArchitechSessionBean architechSessionBean);

}
