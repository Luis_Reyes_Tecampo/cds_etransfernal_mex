/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorArchExp.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Tue Dec 17 18:47:01 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMonitorArchExpDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el Monitor de archivos a exportar
**/
@Local
public interface DAOMonitorArchExp{

   /**Metodo que permite consultar el monitor de archivos exportar
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsMonitorArchExpDAO Objeto del tipo BeanResConsMonitorArchExpDAO
   * 
   */
   public BeanResConsMonitorArchExpDAO consultarMonitorArchExp(BeanPaginador beanPaginador, 
		   ArchitechSessionBean architechSessionBean);



}
