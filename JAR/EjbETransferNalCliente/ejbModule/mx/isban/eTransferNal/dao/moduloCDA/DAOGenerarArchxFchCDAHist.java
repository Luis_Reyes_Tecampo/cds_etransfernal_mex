/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOGenerarArchCDAHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 18:24:31 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqGenArchXFchCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGenArchCDAHistDAO;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad de Generar Archivo CDA Historico.
 *
 * @author FSW-Vector
 * @since 14/01/2019
 */
@Local
public interface DAOGenerarArchxFchCDAHist{

   /**
    * Metodo DAO para obtener la fecha de operacion.
    *
    * @param modulo El objeto: modulo
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
    */
   BeanResConsFechaOpDAO consultaFechaOperacion(String modulo, ArchitechSessionBean architechSessionBean);
   
   /**
    * Metodo DAO para registrar el archivo a generar del CDA historico.
    *
    * @param beanReqGenArchCDAHist Objeto del tipo @see BeanReqGenArchXFchCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResGenArchCDAHistDAO Objeto del tipo BeanResGenArchCDAHistDA
    */
    BeanResGenArchCDAHistDAO genArchCDAHist(BeanReqGenArchXFchCDAHist beanReqGenArchCDAHist,
    		ArchitechSessionBean architechSessionBean);



}
