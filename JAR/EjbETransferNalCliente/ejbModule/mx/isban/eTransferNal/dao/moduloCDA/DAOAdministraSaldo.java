/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOAdministraSaldo.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   25/09/2015     INDRA 		ISBAN   Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;


/**
 * Clase del tipo DAO que se encarga  obtener la informacion 
 * para la administracion de saldos.
 */
@Local
public interface DAOAdministraSaldo {

	 /**
 	 * Metodo que sirve para consultar los saldos.
 	 *
 	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
 	 */
	public BeanAdministraSaldoDAO obtenerSaldoPrincipal(ArchitechSessionBean architechSessionBean);

	/**
	 * Metodo que sirve para consultar los saldos.
	 *
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	 */
	public BeanAdministraSaldoDAO obtenerSaldoAlterno(ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que sirve para consultar los saldos.
	 *
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanAdministraSaldoBO Objeto del tipo BeanAdministraSaldoBO
	 */
	public BeanAdministraSaldoDAO obtenerSaldoCalculado(ArchitechSessionBean architechSessionBean);
	
	/**
	 * Obtener saldo devoluciones.
	 *
	 * @param architechSessionBean El objeto: architech session bean
	 * @return Objeto bean administra saldo dao
	 */
	BeanAdministraSaldoDAO obtenerSaldoDevoluciones(ArchitechSessionBean architechSessionBean);
	
}
