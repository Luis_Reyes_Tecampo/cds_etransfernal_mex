package mx.isban.eTransferNal.dao.moduloCDA;

/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOUsuario.java
*
* Control de versiones:
*
* Version Date/Hour           By       Company Description
* ------- ------------------- -------- ------- --------------
* 1.0     22/06/2011 17:57:14 O Acosta ISBAN   Creacion
*
*/

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPerfil;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResUsuarioPerfilesDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanUsuario;

/**
 * Interfaz DAO para los usuarios con acceso a Transfer.
 *
 */
@Local
public interface DAOUsuario {
   /**
    * Metodo de negocio que se utiliza para obtener los datos del usuario por el id o clave
    * @param clave Objeto del tipo String que tiene la clave o id
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @return BeanResUsuarioDAO Objeto de respuesta
    * @throws BusinessException Exception de negocio
    */
   public BeanResUsuarioDAO consultaUsuarioPorClave(String clave,ArchitechSessionBean sessionBean) throws BusinessException;
   
   /**
    * Metodo que permite actualizar los datos del usuario
    * @param usuario Objeto del tipo BeanUsuario
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @throws BusinessException Exception de negocio
    */
   public void actualizaUsuario(BeanUsuario usuario,
            ArchitechSessionBean sessionBean) throws BusinessException;
   /**
    * Metodo que permite insertar los datos del usuario
    * @param usuario Objeto del tipo BeanUsuario
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @return BeanResUsuarioDAO Objeto de respuesta de la insercion
    * @throws BusinessException Exception de negocio
    */
   public BeanResUsuarioDAO insertaUsuario(BeanUsuario usuario,
           ArchitechSessionBean sessionBean) throws BusinessException;
   
   /***
    * Metodo que permite insertar los perfiles que contiene un usuario
    * @param usuario Objeto del tipo BeanUsuario
    * @param perfiles Listado de objetos de BeanPerfiles
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @return BeanResUsuarioDAO Objeto de respuesta de la insercion
    * @throws BusinessException Exception de negocio
    */
   public BeanResUsuarioDAO insertaUsuarioPerfiles(BeanUsuario usuario,List<BeanPerfil> perfiles,
           ArchitechSessionBean sessionBean) throws BusinessException;
   
   /***
    * Metodo que permite eliminar los perfiles que contiene un usuario
    * @param usuario Objeto del tipo BeanUsuario
    * @param perfiles Listado de objetos de BeanPerfiles
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @return BeanResUsuarioDAO Objeto de respuesta de la insercion
    * @throws BusinessException Exception de negocio
    */
   public BeanResUsuarioDAO eliminarUsuarioPerfiles(BeanUsuario usuario,List<BeanPerfil> perfiles,
           ArchitechSessionBean sessionBean) throws BusinessException;
   /***
    * Metodo que permite consultar los perfiles que contiene un usuario
    * @param cveUsuario Es el id o clave del usuario
    * @param sessionBean Objeto del tipo ArchitechSessionBean
    * @return BeanResUsuarioPerfilesDAO Objeto de respuesta de la insercion
    * @throws BusinessException Exception de negocio
    */
   public BeanResUsuarioPerfilesDAO consultaUsuarioPerfiles(String cveUsuario,
           ArchitechSessionBean sessionBean) throws BusinessException;

}
