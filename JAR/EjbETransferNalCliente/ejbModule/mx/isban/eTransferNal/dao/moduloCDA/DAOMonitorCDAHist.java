/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCDA.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 11 09:59:16 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqMonCDAHist;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonitorCdaHistDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * el monitor CDA
**/
@Local
public interface DAOMonitorCDAHist{

 
   /**
    * Metodo que sirve para consultar los movimientos CDA agrupados por monto y volumen
    * @param beanReqMonCDAHist Objeto del tipo @see BeanReqMonCDAHist
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResMonitorCdaHistDAO Objeto del tipo @see BeanResMonitorCdaHistDAO
    */
	public BeanResMonitorCdaHistDAO consultarCDAAgrupadas(final BeanReqMonCDAHist beanReqMonCDAHist,
			final ArchitechSessionBean architechSessionBean);

}
