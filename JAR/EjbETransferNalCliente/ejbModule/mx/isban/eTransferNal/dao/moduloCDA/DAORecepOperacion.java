package mx.isban.eTransferNal.dao.moduloCDA;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanTranSpeiRecOper;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion para
 * el recepcion de operaciones.
 *
 * @author FSW-Vector
 * @since 9/10/2018
 */

@Local
public interface DAORecepOperacion {
	
	/**
	 * Metodo que sirve para consultar la institucion.
	 *
	 * @param clave String con la clave
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMiInstDAO Objeto bean de respuesta
	 */
	 BeanResConsMiInstDAO consultaMiInstitucion(String clave,ArchitechSessionBean sessionBean) ;
	
	/**
	 * Metodo que se encarga de realizar la consulta de todas las transferencias.
	 *
	 * @param beanReqTranSpeiRec Objeto del tipo @see BeanReqConsTranSpeiRec
	 * @param parametros objeto del tipo List<Object> con los parametros
	 * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @param historico  parametro para el historico
	 * @param beanTranSpeiRecOper El objeto: bean tran spei rec oper
	 * @return BeanResConsTranSpeiRecDAO Objeto del tipo @see BeanResConsTranSpeiRecDAO
	 */
	 BeanResConsTranSpeiRecDAO consultaTransferencia(BeanReqTranSpeiRec beanReqTranSpeiRec,List<Object> parametros,
    		ArchitechSessionBean sessionBean, boolean historico,BeanTranSpeiRecOper beanTranSpeiRecOper) ;
    
	/**
	 * Metodo que se encarga de realizar la consulta de los motivos de devoluciones.
	 *
	 * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @param obQuery El objeto: ob query
	 * @param hto El objeto: hto
	 * @param beanReqTranSpeiRec El objeto: bean req tran spei rec
	 * @return BeanResConsMotDevolucionDAO Objeto del tipo @see BeanResConsMotDevolucionDAO
	 */
	 BeanResConsMotDevolucionDAO consultaDevoluciones(ArchitechSessionBean sessionBean,int obQuery,boolean hto,BeanReqTranSpeiRec beanReqTranSpeiRec) ;
    
	 
		 
	 /**
 	 * Metodo DAO para obtener la fecha de operacion.
 	 *
 	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
 	 * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
 	 */	
	 BeanResConsFechaOpDAO consultaFechaOperacion(ArchitechSessionBean architechSessionBean);
	 
	 
}