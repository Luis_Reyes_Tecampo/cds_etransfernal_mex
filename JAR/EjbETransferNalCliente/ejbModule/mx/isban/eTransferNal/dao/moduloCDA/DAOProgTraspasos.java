/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOProgTraspasos.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   26/09/2015     INDRA 		ISBAN   Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosBO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanProgTraspasosDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la programacion de transferencias
**/
 @Local
public interface DAOProgTraspasos {
	
	 /**Metodo que sirve para consultar la informacion del catalogo
	   * de horarios  de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosDAO Objeto del tipo BeanProgTraspasosDAO
	   */
	public BeanProgTraspasosDAO consultaHorariosTransferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para agregar  informacion al catalogo
	   * de horarios de trasnferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosDAO Objeto del tipo BeanProgTraspasosDAO
	   */
	public BeanProgTraspasosDAO agregaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para eliminar la informacion del catalogo
	   * de horarios de transferencias
	   * @param bean Objeto del tipo @see BeanProgTraspasosBO
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return BeanProgTraspasosDAO Objeto del tipo BeanProgTraspasosDAO
	   */
	public BeanProgTraspasosDAO eliminaHorarioTrasnferencias(BeanProgTraspasosBO bean,ArchitechSessionBean architechSessionBean);

}
