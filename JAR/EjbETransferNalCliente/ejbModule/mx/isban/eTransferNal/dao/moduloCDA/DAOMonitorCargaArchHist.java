/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorCargaArchHist.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Dec 13 10:31:59 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResMonCargaArchHistDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la funcionalidad del Monitor de carga de archivos historico
**/
@Local
public interface DAOMonitorCargaArchHist{

   /**Metodo DAO para obtener la informacion del monitor de carga de
   * archivos historicos
   * @param beanPaginador Objeto del tipo @see BeanPaginador
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResMonCargaArchHistDAO Objeto del tipo BeanResMonCargaArchHistDAO
   * 
   */
   public BeanResMonCargaArchHistDAO consultarMonitorCargaArchHist(BeanPaginador beanPaginador, 
		   ArchitechSessionBean architechSessionBean);



}
