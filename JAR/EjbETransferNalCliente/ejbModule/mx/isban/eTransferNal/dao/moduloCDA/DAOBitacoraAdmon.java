/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOBitacoraAdmon.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Wed Dec 18 10:39:58 CST 2013 Carlos Alberto Chong Antonio  ISBAN 		Creacion
 *
 */

package mx.isban.eTransferNal.dao.moduloCDA;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanConsReqBitacoraAdmon;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqInsertBitacora;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsBitAdmonDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResExpBitAdmonDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;


/**
 *Clase del tipo DAO que se encarga  obtener la informacion para
 * la bitacora administrativa
**/
@Local
public interface DAOBitacoraAdmon{

   /**Metodo que permite consultar la bitacora administrativa
   * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsBitAdmonDAO Objeto del tipo BeanResConsBitAdmonDAO
   * 
   */
   public BeanResConsBitAdmonDAO consultaBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean);

   /**Metodo que sirve para realizar el exportar todo
    * @param beanConsReqBitacoraAdmon Objeto del tipo @see BeanConsReqBitacoraAdmon
    * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
    * @return BeanResExpBitAdmonDAO Objeto del tipo BeanResExpBitAdmonDAO
    */
    public BeanResExpBitAdmonDAO expTodosBitacoraAdmon(BeanConsReqBitacoraAdmon beanConsReqBitacoraAdmon, ArchitechSessionBean architechSessionBean);
    
    /**Metodo que permite guardar en la bitacora administratiiva
     * @param beanReqInsertBitacora Objeto del tipo @see BeanReqInsertBitacora
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     */
     public BeanResGuardaBitacoraDAO guardaBitacora(BeanReqInsertBitacora beanReqInsertBitacora, ArchitechSessionBean architechSessionBean);

}
