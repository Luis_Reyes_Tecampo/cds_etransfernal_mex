/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORecepOperacionSPID2.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanReqActTranSpeiRec;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;

@Local	    
public interface DAORecepOperacionSPID2 {
    /**
     * Metodo que actualiza
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    BeanResActTranSpeiRecDAO actualizaTransSpeiRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean);
    
    
    /**
     * Metodo que actualiza el motivo de rechazo
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    BeanResActTranSpeiRecDAO actualizaMotivoRec(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
      		 ArchitechSessionBean architechSessionBean);
    
    /**
     * @param cveTransfe String con la cve de transferencia
     * @param beanReceptoresSPID bean del tipo BeanReceptoresSPID
     * @param beanReqActTranSpeiRec bean del tipo BeanReqActTranSpeiRec
     * @param tipoOperacion String con el tipo de operacion
     * @param architechSessionBean Objeto de ArchitechSessionBean
     */
    void guardaTranSpeiHorasMan(String cveTransfe,BeanReceptoresSPID beanReceptoresSPID,BeanReqActTranSpeiRec beanReqActTranSpeiRec, String tipoOperacion,
      		 ArchitechSessionBean architechSessionBean);
    /**
     * Metodo que rechaza una operacion
     * @param beanReqActTranSpeiRec bean con los datos actualizar
     * @param architechSessionBean Objeto de la arquitectua
     * @return BeanResActTranSpeiRecDAO bean de respuesta
     */
    public BeanResActTranSpeiRecDAO rechazar(BeanReqActTranSpeiRec beanReqActTranSpeiRec,
     		 ArchitechSessionBean architechSessionBean);

}
