/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOAvisoTraspasos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqAvisoTraspasos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResAvisoTraspasosDAO;

/**
 * Clase del tipo DAO que se encarga obtener la informacion para la consultad
 * aviso traspasos
 **/
@Local
public interface DAOAvisoTraspasos {

	
	/**
	 * Metodo para obtener los avisos
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo String
	 * @param sortType Objeto del tipo String
	 * @param cveInst Objeto del tipo @see String
	 * @param fch Objeto del tipo @see String
	 * @return fch Objeto del tipo  String
	 */
	public BeanResAvisoTraspasosDAO obtenerAviso(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean,
			String sortField, String sortType, String cveInst, String fch);

	
	/**
	 * Metodo para exportar todos los avisos de traspasos
	 * @param beanReqAvisoTraspasos Objeto del tipo @see BeanReqAvisoTraspasos
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param cveInst Objeto del tipo @see String
	 * @param fch Objeto del tipo @see String
	 * @return fch Objeto del tipo  String
	 */
	public BeanResAvisoTraspasosDAO exportarTodoAvisoTraspasos(BeanReqAvisoTraspasos beanReqAvisoTraspasos,
			ArchitechSessionBean architechSessionBean, String cveInst, String fch);
	
	/**
	 * 
	 * @param architechSessionBean tipo architechSessionBean
	 * @param sortField tipo String
	 * @param sortType tipo String
	 * @param cveInst tipo String
	 * @param fch  tipo String
	 * @return BigDecimal
	 */
	public BigDecimal obtenerImporteTotal(
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType, String cveInst, String fch);

}
