/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAORecepcion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * @author IDS
 * Interfaz de DAORecepcion
 */
@Local
public interface DAORecepcion {
	
	/**
	 * @param beanLlave Objeto del tipo BeanSPIDLlave
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResRecepcionDAO
	 */
	public BeanResRecepcionDAO obtenerDetalleRecepcion(
			BeanSPIDLlave beanLlave, ArchitechSessionBean architechSessionBean);
}
