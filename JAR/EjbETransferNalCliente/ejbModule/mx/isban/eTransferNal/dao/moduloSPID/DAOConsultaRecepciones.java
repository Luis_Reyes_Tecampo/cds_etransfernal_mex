/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaRecepciones.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaRecepciones;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaRecepcionesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResRecepcionDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanSPIDLlave;

/**
 * Clase del tipo DAO que se encarga  obtener la informacion 
 * @author IDS
 *
 */
@Local
public interface DAOConsultaRecepciones {
	/**
	 * Metodo para obtener la consulta de las recepciones
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param beanReqConsultaRecepciones Objeto del tipo @see BeanReqConsultaRecepciones
	 * @param cveInst Objeto del tipo @see String
	 * @param fch Objeto del tipo @see String
	 * @return fch Objeto del tipo  String
	 */
	public BeanResConsultaRecepcionesDAO obtenerConsultaRecepciones(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, BeanReqConsultaRecepciones beanReqConsultaRecepciones, String cveInst, String fch);

	/**
	 * @param beanLlave Objeto del tipo BeanSPIDLlave
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return
	 */
	public BeanResRecepcionDAO obtenerDetRecepcion(BeanSPIDLlave beanLlave,
			ArchitechSessionBean architechSessionBean);

	/**Metodo que sirve para exportar todo de las recepciones
	 * @param beanReqConsultaRecepciones Objeto del tipo @see BeanReqConsultaRecepciones
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param cveInst cadena para obtener la clave instutucion
	 * @param fch cadena para obtener la fecha operacion
	 * @return BeanResConsultaRecepcionesDAO Objeto del tipo BeanResConsultaRecepciones
	 */
	public BeanResConsultaRecepcionesDAO exportarTodoConsultaRecepciones(BeanReqConsultaRecepciones beanReqConsultaRecepciones, 
    		ArchitechSessionBean architechSessionBean, String cveInst, String fch);
	
	/**Metodo que sirve para el monto total
	   * @param String Objeto del tipo 
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return BigDecimal
	   */
	public BigDecimal obtenerImporteTotal( ArchitechSessionBean architechSessionBeans, String tipoOrden, String cveInst, String fch) ;
}
