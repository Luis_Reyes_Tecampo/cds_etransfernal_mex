/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOComunesSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsFechaOpDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMiInstDAO;

@Local
public interface DAOComunesSPID {

   /**Metodo DAO para obtener la fecha de operacion
   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
   * @return BeanResConsFechaOpDAO Objeto del tipo BeanResConsFechaOpDAO
   */
	BeanResConsFechaOpDAO consultaFechaOperacionSPID(ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que sirve para consultar la institucion
	 * @param clave String con la clave
	 * @param sessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return BeanResConsMiInstDAO Objeto bean de respuesta
	 */
	public BeanResConsMiInstDAO consultaMiInstitucion(String clave,ArchitechSessionBean sessionBean);
}
