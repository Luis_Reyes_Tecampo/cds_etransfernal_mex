/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOBitacoraEnvio.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	      Company 	 Description
 * ------- -----------   ----------- ---------- ----------------------
 *   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqBitacoraEnvio;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResBitacoraEnvioDAO;

/**
 * Clase del tipo DAO que se encarga obtener la informacion
 * 
 * @author IDS
 *
 */
@Local
public interface DAOBitacoraEnvio {

	/**
	 * Metodo que sirve para consultar los envios
	 * 
	 * @param beanPaginador
	 *            Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @param sortField
	 *            Objeto del tipo @see String
	 * @param sortType
	 *            Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResBitacoraEnvioDAO obtenerBitacoraEnvio(
			BeanPaginador beanPaginador,
			ArchitechSessionBean architechSessionBean, String sortField,
			String sortType);

	/**
	 * Metodo que sirve para exportar todos los envios
	 * 
	 * @param beanReqBitacoraEnvio
	 *            Objeto del tipo @see BeanReqBitacoraEnvio
	 * @param architechSessionBean
	 *            Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResBitacoraEnvioDAO exportarTodoBitacoraEnvio(
			BeanReqBitacoraEnvio beanReqBitacoraEnvio,
			ArchitechSessionBean architechSessionBean);

	/**
	 * 
	 * @param architechSessionBeans tipo architechSessionBeans
	 * @param sortField tipo String
	 * @param sortType tipo String
	 * @return BigDecimal
	 */
	public BigDecimal obtenerImporteTotalTopo(
			ArchitechSessionBean architechSessionBeans, String sortField,
			String sortType);

}
