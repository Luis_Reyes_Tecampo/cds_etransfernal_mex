/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOEncripta.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Lunes 29/08/2016 Carlos Alberto ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResDesencripta;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEncripta;

/**
 * @author cchong
 *
 */
@Local
public interface DAOEncripta {
	/**
	 * Metodo que permite encriptar un mensaje
	 * @param msg String con la cadena a encriptar
	 * @param architechSessionBean Objeto de la arquitectura
	 * @return BeanResEncripta bean de respuesta 
	 */
	BeanResEncripta encripta(String msg,ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo que permite encriptar un mensaje
	 * @param msg String con la cadena a encriptar
	 * @param architechSessionBean Objeto de la arquitectura
	 * @return BeanResDesencripta bean de respuesta 
	 */
	BeanResDesencripta desEncripta(String msg,ArchitechSessionBean architechSessionBean);
}
