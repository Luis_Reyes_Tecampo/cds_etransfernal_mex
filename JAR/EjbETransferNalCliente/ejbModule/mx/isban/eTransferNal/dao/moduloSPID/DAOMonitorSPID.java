/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOMonitorSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqMonitorSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMonitorOpSPID;

@Local
public interface DAOMonitorSPID {

	/**
	 * Metodo que permite consultar los datos del monitor
	 * @param beanReqMonitorSPID Bean del tipo BeanReqMonitorSPID
	 * @param architechSessionBean Objeto de la arquitectura
	 * @return BeanResMonitorOpSPID Bean de respuesta
	 */
	BeanResMonitorOpSPID consultarMonitorSPID(BeanReqMonitorSPID beanReqMonitorSPID, ArchitechSessionBean architechSessionBean);
}
