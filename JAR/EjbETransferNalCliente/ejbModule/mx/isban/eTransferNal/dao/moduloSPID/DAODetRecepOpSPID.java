/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAODetRecepOpSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResActTranSpeiRecDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;

@Local
public interface DAODetRecepOpSPID {
	
	/**
	 * @param beanReceptoresSPID bean con los datos de la operacion
	 * @param architechSessionBean Objeto de la arquitectura
	 * @return BeanResActTranSpeiRecDAO Bean de respuesta de la actualizacion
	 */
	public BeanResActTranSpeiRecDAO actualizaDetTransSpeiRec(BeanReceptoresSPID beanReceptoresSPID,
			   ArchitechSessionBean architechSessionBean);
	
	/**
     * Metodo que se encarga de realizar la consulta de todas las transferencias
     * @param parametros listado de parametros
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPIDDAO bean de respuesta
     */
    public BeanResReceptoresSPIDDAO consultaTransferencia(List<Object> parametros, ArchitechSessionBean sessionBean);

}
