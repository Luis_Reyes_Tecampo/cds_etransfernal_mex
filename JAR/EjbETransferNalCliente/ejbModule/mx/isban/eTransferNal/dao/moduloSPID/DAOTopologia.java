/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOTopologia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResTopologiaDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanTopologia;

/**
 * 
 * 
 * @author IDS
 *
 */
@Local
public interface DAOTopologia {
	
	/**
	 * Metodo para obtener las topologias
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResTopologiaDAO obtenerTopologias(BeanPaginador beanPaginador, 
			ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**
	 * @param cveMedioEnt Objeto del tipo String
	 * @param cveOperacion Objeto del tipo String
	 * @param topoPri Objeto del tipo String
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResTopologiaDAO
	 */
	public BeanResTopologiaDAO obtenerTopologia(String cveMedioEnt,
			String cveOperacion, String topoPri, 
			ArchitechSessionBean architechSessionBean);

	/**
	 * Metodo para obtener los combos de la topologia
	 * @param beanReqTopologia Objeto del tipo @see BeanReqTopologia
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResTopologiaDAO obtenerCombosTopologia(ArchitechSessionBean architechBean);

	/**
	 * Metodo para guardar las topologias
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResTopologiaDAO guardarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean);

	/**
	 * Metodo para obtener si es repetido o no
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
   	* @return architechSessionBean Objeto del tipo BeanTopologia
	 */
	public boolean isRepetido(BeanTopologia beanTopologia);

	/**
	 * Metodo para eliminar las topologias
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo BeanTopologia
	 */
	public BeanResTopologiaDAO eliminarTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean);

	/**
	 * Metodo para guardar las topologias
	 * @param beanTopologia Objeto del tipo @see BeanTopologia
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param cveMedio Objeto del tipo @see String
	 * @param cveOperacion Objeto del tipo @see String
	 * @param toPri toPri de tipo string
	 * @return toPri toPri de tipo string

	 */
	public BeanResTopologiaDAO guardarEdicionTopologia(BeanTopologia beanTopologia, 
			ArchitechSessionBean architechBean, String cveMedio, 
			String cveOperacion, String toPri);
	
	/**
	 * 
	 * @param architechSessionBeans tipo ArchitechSessionBean
	 * @return BigDecimal
	 */
	public BigDecimal obtenerImporteTotalTopo(ArchitechSessionBean architechSessionBeans);
}
