/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOInitModuloSPID.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResInitModuloSPIDDAO;

/**
 * Metodo que integra el modulo SPID
 * @author IDS
 *
 */
@Local
public interface DAOInitModuloSPID {
	
	/**
	 * Metodo encargado de obtener la informacion.
	 *
	 * @param tipo El objeto: tipo SPEI o SPID
	 * @return obtenerInformacionHeaders Objeto del tipo @see BeanResInitModuloSPIDDAO
	 * @throws BusinessException exception
	 */
	BeanResInitModuloSPIDDAO obtenerInformacionHeaders(String tipo)  throws BusinessException ;
}
