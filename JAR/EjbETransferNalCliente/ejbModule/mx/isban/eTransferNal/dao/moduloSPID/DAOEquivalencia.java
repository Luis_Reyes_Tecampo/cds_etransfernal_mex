/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOEquivalencia.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResEquivalenciaDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanClaveOperacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanEquivalencia;
import java.util.List;

import javax.ejb.Local;

/**
 * Interfaz para la equivalencia de datos
 * @author IDS
 *
 */
@Local
public interface DAOEquivalencia {
	
	/**
	 * @param cveOperacion Objeto del tipo String
	 * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	 * @return BeanResEquivalenciaDAO
	 */
	public BeanResEquivalenciaDAO obtenerEquivalencia(String cveOperacion, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para el ResEquivalencia
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo String
	 * @param sortType Objeto del tipo String
	 * @return architechSessionBean Objeto del tipo  ArchitechSessionBean
	 */
	public BeanResEquivalenciaDAO 
	listarEquivalencias(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**
	 * Metodo para el Res Equivalencia de DAO
	 * @param beanEquivalencia Objeto del tipo @see BeanEquivalencia
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo  ArchitechSessionBean
	 */
	public BeanResEquivalenciaDAO crearEquivalencia(BeanEquivalencia beanEquivalencia, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para la obtencion de lista claves de operaciones
	 * @return  List Objeto del tipo  BeanClaveOperacion
	 */
	public List<BeanClaveOperacion> obtenerListaClaveOperacion();
	
	/**
	 * Metodo para eliminar las Equivalencia
	 * @param beanEquivalencia Objeto del tipo @see BeanEquivalencia
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo  ArchitechSessionBean
	 */
	public BeanResEquivalenciaDAO eliminarEquivalencia
	(BeanEquivalencia beanEquivalencia, BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para editar las equivalencias
	 * @param beanEquivalencia Objeto del tipo @see BeanEquivalencia
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param cveOperacion Objeto del tipo @see String
	 * @param tipoPago Objeto del tipo @see String
	 * @param clasificacion Objeto del tipo @see String
	 * @return clasificacion Objeto del tipo  String
	 */
	public BeanResEquivalenciaDAO editarEquivalencia
	(BeanEquivalencia beanEquivalencia, BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, 
			String cveOperacion, String tipoPago, String clasificacion);

}
