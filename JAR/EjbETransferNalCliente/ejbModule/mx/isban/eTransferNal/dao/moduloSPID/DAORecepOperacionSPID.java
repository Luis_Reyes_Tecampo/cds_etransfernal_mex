/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAORecepOperacionSPID.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResConsMotDevolucionDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanResGuardaBitacoraDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqReceptoresSPID;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResReceptoresSPIDDAO;
@Local
public interface DAORecepOperacionSPID {
	

	/**
     * Metodo que se encarga de realizar la consulta de todas las transferencias
     * @param beanReqReceptoresSPID Objeto del tipo @see BeanReqReceptoresSPID
     * @param parametros objeto del tipo List<Object> con los parametros
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResReceptoresSPIDDAO Objeto del tipo @see BeanResReceptoresSPIDDAO
     */
    public BeanResReceptoresSPIDDAO consultaTransferencia(BeanReqReceptoresSPID beanReqReceptoresSPID,List<Object> parametros,
    		ArchitechSessionBean sessionBean);
    
	/**
     * Metodo que se encarga de realizar la consulta de los motivos de devoluciones
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanResConsMotDevolucionDAO Objeto del tipo @see BeanResConsMotDevolucionDAO
     */
    public BeanResConsMotDevolucionDAO consultaDevoluciones(ArchitechSessionBean sessionBean);
    
    /**Metodo que permite apartar una operacion
     * @param beanReceptoresSPID Objeto del tipo @see BeanReceptoresSPID
     * @param funcionalidad String con la funcionalidad
     * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
     * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
     **/
     public BeanResGuardaBitacoraDAO apartar(BeanReceptoresSPID beanReceptoresSPID, String funcionalidad,ArchitechSessionBean architechSessionBean);
     
     /**Metodo que permite desapartar una operacion
      * @param beanReceptoresSPID Objeto del tipo @see BeanReceptoresSPID
      * @param funcionalidad String con la funcionalidad
      * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
      * @return BeanResGuardaBitacoraDAO Objeto del tipo BeanResGuardaBitacoraDAO
      **/
      public BeanResGuardaBitacoraDAO desApartar(BeanReceptoresSPID beanReceptoresSPID, String funcionalidad,ArchitechSessionBean architechSessionBean);
    
 

}
