/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOOrdenesReparacion.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanOrdenReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqOrdeReparacion;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResOrdenReparacionDAO;

/**
 * Interfaz de DAOOrdenesReparacion
 * @author IDS
 * 
 */
@Local
public interface DAOOrdenesReparacion {
	/**
	 * Metodo para la obtencion de las ordenes de reparacion
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return BeanResOrdenReparacionDAO
	 */
	public BeanResOrdenReparacionDAO obtenerOrdenesReparacion(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**
	 * Metodo para la obtencion de las ordenes de reparacion Montos
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return BeanResOrdenReparacionDAO
	 */
	public BigDecimal obtenerOrdenesReparacionMontos(
			 ArchitechSessionBean architechSessionBean, String sortField, String sortType);

	/**
	 * Metodo para  la actualizacion de ordenes de reparacion
	 * @param beanOrdenReparacion Objeto del tipo @see BeanOrdenReparacion
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResOrdenReparacionDAO actualizarOdenesReparacion(
			BeanOrdenReparacion beanOrdenReparacion, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para exportar todas las ordenes de reparacion
	 * @param beanReqOrdeReparacion Objeto del tipo @see BeanReqOrdeReparacion
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResOrdenReparacionDAO exportarTodoOrdenReparacion(BeanReqOrdeReparacion beanReqOrdeReparacion, 
    		ArchitechSessionBean architechSessionBean);
}
