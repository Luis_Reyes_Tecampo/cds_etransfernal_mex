/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOPago.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanDetallePago;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqListadoPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResListadoPagosDAO;

/**
 * @author IDS
 * Interfaz de DAOpago
 */
@Local
public interface DAOPago {
	/**
	 * Metodo para obtener el listado de los pagos
	 * @param cveMecanismo Objeto del tipo @see String
	 * @param estatus Objeto del tipo @see string
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see architechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResListadoPagosDAO obtenerListadoPagos(
			String cveMecanismo, String estatus, BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para obtener el listado de los pagos
	 * @param cveMecanismo Objeto del tipo @see String
	 * @param estatus Objeto del tipo @see string
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see architechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BigDecimal obtenerListadoPagosMontos(
			String cveMecanismo, String estatus, ArchitechSessionBean architechSessionBean);

	/**
	 * Metodo para la exportacion de todas las listas de pagos
	 * @param tipoOrden Objeto del tipo String
	 * @param cveMecanismo Objeto del tipo String
	 * @param estatus Objeto del tipo String
	 * @param beanReqListadoPagos Objeto de tipo @see BeanReqListadoPagos
	 * @param architechSessionBean Objeto de tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResListadoPagosDAO exportarTodoListadoPagos( String tipoOrden,
			String cveMecanismo, String estatus, BeanReqListadoPagos beanReqListadoPagos, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para consultar los detalles del pago
	 * @param referencia Objeto del tipo @see string
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanDetallePago consultarDetallePago(String referencia);
	
	/**
	 * Metodo para la busqueda de pagos
	 * @param field Objeto del tipo @see string
	 * @param valor Objeto del tipo @see string
	 * @param cveMecanismo Objeto del tipo @see string
	 * @param estatus Objeto del tipo @see string
	 * @param paginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	 */
	public BeanResListadoPagosDAO buscarPagos(String field, String valor, String cveMecanismo, String estatus, BeanPaginador paginador, ArchitechSessionBean architechSessionBean);
	
	/**
	 * Metodo para la busqueda de pagos Montos
	 * @param field Objeto del tipo @see string
	 * @param valor Objeto del tipo @see string
	 * @param cveMecanismo Objeto del tipo @see string
	 * @param estatus Objeto del tipo @see string
	 * @param paginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @return BigDecimal Objeto del tipo ArchitechSessionBean
	 */
	public BigDecimal buscarPagosMontos(String field, String valor, String cveMecanismo, String estatus,ArchitechSessionBean architechSessionBean);
	
}
