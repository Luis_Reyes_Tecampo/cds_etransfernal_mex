/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaReceptorHist.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsReceptHistSum;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConsultaReceptorHistDAO;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsReceptHist;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para la consultad de receptores historicos
**/
@Local
public interface DAOConsultaReceptorHist {
	
	/**Metodo que sirve para consultar los receptores historicos
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanConsultaReceptorHistDAO Objeto del tipo BeanConsultaReceptorHistDAO
	   */
	public BeanConsultaReceptorHistDAO obtenerConsultaReceptorHist(BeanPaginador beanPaginador,BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBean);

	/**Metodo que sirve para consultar los receptores Ini
	 * * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo ArchitechSessionBean
	   * @return BeanConsultaReceptorHistDAO Objeto del tipo BeanConsultaReceptorHistDAO
	   */
	public BeanConsultaReceptorHistDAO obtenerConsultaReceptorHistIni(BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para consultar los receptores historicos
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return beanConsReceptHistSum Objeto del tipo BeanConsReceptHistSum
	   */
	public BeanConsReceptHistSum obtenerSumatoriaHist(BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para consultar los receptores historicos
	   * @param beanReqConsReceptHist Objeto del tipo @see BeanReqConsReceptHist
	   * @param architechSessionBeans Objeto del tipo ArchitechSessionBean
	   * @return beanConsReceptHistSum Objeto del tipo BeanConsReceptHistSum
	   */
	public BigDecimal obtenerImporteTotalHist(BeanReqConsReceptHist beanReqConsReceptHist, ArchitechSessionBean architechSessionBean);
}
