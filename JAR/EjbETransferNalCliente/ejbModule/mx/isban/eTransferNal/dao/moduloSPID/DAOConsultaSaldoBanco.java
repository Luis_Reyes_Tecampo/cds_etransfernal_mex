/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConsultaSaldoBanco.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqConsultaSaldoBanco;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConsultaSaldoBancoDAO;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para la consulta de saldos por banco
**/
@Local
public interface DAOConsultaSaldoBanco {
	
	/**Metodo que sirve para consultar los saldos
	   * @param beanPaginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanResConsultaSaldoBancoDAO obtenerConsultaSaldoBanco(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**Metodo que sirve para consultar los saldos Montos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BigDecimal obtenerConsultaSaldoBancoMontos(
			ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**Metodo que sirve para exportar todos los saldos
	   * @param beanReqConsultaSaldoBanco Objeto del tipo @see BeanReqConsultaSaldoBanco
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanResConsultaSaldoBancoDAO exportarTodoConsultaSaldoBanco(
			BeanReqConsultaSaldoBanco beanReqConsultaSaldoBanco, ArchitechSessionBean architechSessionBean);
}
