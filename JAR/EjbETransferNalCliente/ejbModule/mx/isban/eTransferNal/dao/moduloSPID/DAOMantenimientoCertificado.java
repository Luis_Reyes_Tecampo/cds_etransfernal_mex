/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOMantenimientoCertificado.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMantenimientoCertificado;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResMantenimientoCertificadoDAO;

/**
 * Interfaz para el mantenimiento de el certificado
 * @author IDS
 *
 */
@Local
public interface DAOMantenimientoCertificado {

	/**
	 * Metodo que obtiene los certificados registrados
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificadoDAO
	 */
	BeanResMantenimientoCertificadoDAO obtenerTopologias(BeanPaginador beanPaginador,
			ArchitechSessionBean architechBean, String nomPantalla);
	
	/**
	 * M�todo que obtiene el certificado seleccionado
	 * @param cveMiInstituc Objeto del tipo String
	 * @param numCertificado Objeto del tipo String
	 * @param architechBean Objeto del tipo ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificadoDAO
	 */
	BeanResMantenimientoCertificadoDAO obtenerMantenimientoCertificado(String cveMiInstituc,
			String numCertificado,
			ArchitechSessionBean architechBean, String nomPantalla);

	/**
	 * Metodo que elimina un certificado
	 * @param beanMantenimientoCertificado Objeto del tipo @see BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificadoDAO
	 */
	BeanResMantenimientoCertificadoDAO eliminarTopologia(BeanMantenimientoCertificado beanMantenimientoCertificado,
			ArchitechSessionBean architechBean, String nomPantalla);

	/**
	 * Metodo que verifica si hay elementos repetidos en la BD
	 * @param beanMantenimientoCertificado Obejto del tipo @see BeanMantenimientoCertificado
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return boolean
	 */
	boolean isRepetido(BeanMantenimientoCertificado beanMantenimientoCertificado, String nomPantalla);

	/**
	 * Metodo que guarda un ceritifcado 
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificadoDAO
	 */
	BeanResMantenimientoCertificadoDAO guardarMantenimiento(BeanMantenimientoCertificado beanMantenimientoCertificado,
			ArchitechSessionBean architechBean, String nomPantalla);

	/**
	 * Metodo que guarda la edicion de un certificado
	 * @param beanMantenimientoCertificado  Objeto del tipo BeanMantenimientoCertificado
	 * @param architechBean Objeto del tipo @see ArchitechSessionBean
	 * @param numeroCertOld Objeto del tipo @see String
	 * @param cveInsOld  Objeto del tipo @see String
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return BeanResMantenimientoCertificadoDAO
	 */
	BeanResMantenimientoCertificadoDAO guardarEdicionMantenimiento(
			BeanMantenimientoCertificado beanMantenimientoCertificado, ArchitechSessionBean architechBean,
			String numeroCertOld, String cveInsOld, String nomPantalla);
	
	
	/**
	 * Metodo que verifica si el certificado no esta activo.
	 * De esta forma se podra eliminar el registro.
	 * 
	 * @param beanMantenimientoCertificado Obejto del tipo @see BeanMantenimientoCertificado
	 * @param nomPantalla parametro que trae el nombre del m�dulo SPEI o SPID
	 * @return boolean
	 */
	boolean permiteEliminar(BeanMantenimientoCertificado beanMantenimientoCertificado, String nomPantalla);

}
