/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOHistorialMensajes.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanHistorialMensajesDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqHistorialMensajes;

/**
 * interfaz del hostorial de los mensajes
 * @author IDS
 *
 */
@Local
public interface DAOHistorialMensajes {


	/**Metodo que sirve para consultar el historial del mensaje
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param beanReqHistorialMensajes Objeto del tipo @see BeanReqHistorialMensajes
	   * @return beanReqHistorialMensajes Objeto del tipo  BeanReqHistorialMensajes
	   */
	
	public BeanHistorialMensajesDAO obtenerHistorial(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, BeanReqHistorialMensajes beanReqHistorialMensajes);
}
