/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOListadoPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import java.util.List;

import javax.ejb.Local;

import mx.isban.eTransferNal.beans.moduloSPID.BeanPagoDAO;

/**
 * @author IDS
 *
 */
@Local
public interface DAOListadoPagos {
	/**
	 * @return List<BeanPagoDAO>
	 */
	public List<BeanPagoDAO> obtenerListadoPagos();
}
