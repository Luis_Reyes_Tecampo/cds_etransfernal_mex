/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOMonitorBancos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanMonitorBancosDAO;

/**
 *Clase del tipo DAO que se encarga  obtener la informacion 
 *para la administracion de bancos
**/
@Local
public interface DAOMonitorBancos {
	/** 
	 * Metodo para obtener el monitor de bancos
	 * @param beanPaginador Objeto del tipo @see BeanPaginador
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param sortField Objeto del tipo @see String
	 * @param sortType Objeto del tipo @see String
	 * @return architechSessionBean Objeto del tipo  ArchitechSessionBean
	 */
	public BeanMonitorBancosDAO obtenerMonitorBancos(
			BeanPaginador beanPaginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
}
