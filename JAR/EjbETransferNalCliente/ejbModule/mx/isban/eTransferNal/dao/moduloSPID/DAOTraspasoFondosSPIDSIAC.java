/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * DAOTraspasoFondosSPIDSIAC.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	   By 	  Company 	 Description
 * ------- ----------- ----------- -------- ----------------------
 *   1.0    08/02/20156 	INDRA		ISBAN 		Creacion
 *
 */
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanAdministraSaldoDAO;

/**
 *Interfaz del tipo DAO que se encarga  obtener la informacion 
 *para el traspaso de Fondos SPID SIAC
 *
 */
@Local
public interface DAOTraspasoFondosSPIDSIAC {

	/**
     * Metodo que se encarga de realizar la consulta de los motivos de devoluciones
     * @param sessionBean  Objeto del tipo @see ArchitechSessionBean
     * @return BeanAdministraSaldoDAO Objeto del tipo @see BeanResConsMotDevolucionDAO
     */
    BeanAdministraSaldoDAO consultaSaldo(ArchitechSessionBean sessionBean);
}
