/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOCancelacionPagos.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;


import java.math.BigDecimal;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloCDA.BeanPaginador;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagos;
import mx.isban.eTransferNal.beans.moduloSPID.BeanCancelacionPagosDAO;
import mx.isban.eTransferNal.beans.moduloSPID.BeanReqCancelacionPagos;


/**
 * @author IDS
 * Interfaz para la cancelacion de pagos
 */
@Local
public interface DAOCancelacionPagos {

	
	/**Metodo que sirve para consultar cancelacion de pagos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanCancelacionPagosDAO obtenerCancelacionPagos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	/**Metodo que sirve para consultar cancelacion de pagos filtrados por traspasos
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanCancelacionPagosDAO obtenerCancelacionPagosTraspasos(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**Metodo que sirve para consultar cancelacion de pagos filtrados por orden de pago
	   * @param paginador Objeto del tipo @see BeanPaginador
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param sortField Objeto del tipo @see String
	   * @param sortType Objeto del tipo @see String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanCancelacionPagosDAO obtenerCancelacionPagosOrden(BeanPaginador paginador, ArchitechSessionBean architechSessionBean, String sortField, String sortType);
	
	/**Metodo que sirve para consultar cancelacion de pagos
	   * @param beanCancelacionPagos Objeto del tipo @see BeanCancelacionPagos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanCancelacionPagosDAO actualizarCancelacionPagos(BeanCancelacionPagos beanCancelacionPagos,  ArchitechSessionBean architechSessionBean);
	
	/**Metodo que sirve para hacer insert de las cancelaciones de pagos
	   * @param beanReqCancelacionPagos Objeto del tipo @see BeanReqCancelacionPagos
	   * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	   * @param opcion Objeto del tipo String
	   * @return architechSessionBean Objeto del tipo ArchitechSessionBean
	   */
	public BeanCancelacionPagosDAO exportarTodoCancelacionPagos(BeanReqCancelacionPagos beanReqCancelacionPagos,
			ArchitechSessionBean architechSessionBean, String opcion);
	/**
	 * Metodo que obtiene el monto total de cancelados
	 * @param architechSessionBeans
	 * @return
	 */
	public BigDecimal obtenerImporteTotal( ArchitechSessionBean architechSessionBeans) ;
	
	/**
	 * Metodo que obtiene el monto total de cancelados
	 * @param architechSessionBeans
	 * @return
	 */
	public BigDecimal obtenerImporteTotalOrd( ArchitechSessionBean architechSessionBeans) ;
	/**
	 * Metodo que obtiene el monto total de cancelados
	 * @param architechSessionBeans
	 * @return
	 */
	public BigDecimal obtenerImporteTotalTras( ArchitechSessionBean architechSessionBeans) ;
}