/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOConfiguracionParametros.java
*
* Control de versiones:
*
* Version  Date/Hour 	   By 	      Company 	 Description
* ------- -----------   ----------- ---------- ----------------------
*   1.0   Fri Mar 18 11:30:00 CST 2016 Jorge Alfredo Niebla Ojeda  ISBAN 		Creacion
*
*/
package mx.isban.eTransferNal.dao.moduloSPID;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.eTransferNal.beans.moduloSPID.BeanConfiguracionParametros;
import mx.isban.eTransferNal.beans.moduloSPID.BeanResConfiguracionParametrosDAO;

/**
 * Inrerfaz para configurar parametros
 * @author IDS
 *
 */
@Local
public interface DAOConfiguracionParametros {
	
	/**
	 * Metodo para obtener configuracion de parameros.
	 *
	 * @param cveInstitucion   Objeto del tipo @see Long
	 * @param architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean Objeto del tipo @see ArchitechSessionBean
	 */
	BeanResConfiguracionParametrosDAO obtenerConfiguracionParametros(
		Long cveInstitucion, ArchitechSessionBean architechSessionBean, String nomPantalla);

	/**
	 * Metodo para guardar configuracion de parametros.
	 *
	 * @param configuracionParametros  Objeto del tipo @see BeanConfiguracionParametros
	 * @param architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 * @param nomPantalla El objeto: nom pantalla
	 * @return architechSessionBean  Objeto del tipo @see ArchitechSessionBean
	 */
	BeanResConfiguracionParametrosDAO guardarConfiguracionParametros(
		BeanConfiguracionParametros configuracionParametros, ArchitechSessionBean architechSessionBean, String nomPantalla);
}
