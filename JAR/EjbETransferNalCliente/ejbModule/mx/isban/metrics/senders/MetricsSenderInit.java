/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * MetricsSenderInit.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.senders;

import java.util.UUID;

import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.dao.util.UtileriasDAO;
import mx.isban.metrics.dto.DTOMetricsBitacora;
import mx.isban.metrics.dto.DTOMetricsError;
import mx.isban.metrics.dto.DTOMetricsRequest;
import mx.isban.metrics.dto.DTOMetricsRequestWeb;
import mx.isban.metrics.dto.DTOMetricsWebService;
import mx.isban.metrics.util.MetricsUtil;


/**
 * Clase utileria para inicializar cmp DTO de metricas.
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 -05 Plan Delta Monitores Sucurasales
 */
public class MetricsSenderInit {
	
	/** DTO contenedor para metricas */
	private final DTOMetricsBitacora bitacora = new DTOMetricsBitacora();
	
	/**
	 * metodo que inicializa las metricas
	 */
	public void initMetrics(){
		bitacora.setAppName(Constantes.APP_NAME);
		bitacora.setServerId(MetricsUtil.getHostName());
		bitacora.setRequestId(String.format("%08X",Thread.currentThread().getId()));
		
		bitacora.setRequest(new DTOMetricsRequest());
		bitacora.setRequestWeb(new DTOMetricsRequestWeb());
		bitacora.setError(new DTOMetricsError());
		bitacora.setWebService(new DTOMetricsWebService());
	}
	
	/**
	 * Metodo para inicializar DTORequest para metrics.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param servicio wsdl del servicio
	 * @param time1 tiempo de inicio
	 * @param time2 tiempo fin
	 * @param ipClient ip de origen
	 * @param instancia instancia que atiende la peticion 
	 * @param error S / N si hubo error
	 * 
	 * @return bitacora monitoreo para request
	 */
	public DTOMetricsBitacora getMetricRequest(String servicio,String time1, String time2, 
			String ipClient, String instancia, String error){
		bitacora.setInTimestamp(time1);
		bitacora.setOuTtimestamp(time2);
		bitacora.getError().setError(error);
		bitacora.getRequest().setThreadId(String.valueOf(Thread.currentThread().getName()));
		bitacora.getRequest().setUrl(UtileriasDAO.getURL(servicio));
		bitacora.getRequestWeb().setAcceptLanguage(Constantes.ACCEPTLANGUAGE);
		bitacora.getRequestWeb().setUserAgent(Constantes.TF_CLIENT);
		bitacora.getRequestWeb().setIpClient(ipClient);
		bitacora.getRequestWeb().setSessionId(String.valueOf(UUID.randomUUID()));
		bitacora.getRequestWeb().setUserId(instancia);
		bitacora.getRequest().setMethod(Constantes.POST);
		
		return this.bitacora;
	}
	
	/**
	 * Metodo para inicializar el DTO para error.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param codigo codigo de error
	 * @param desc descripcion del error
	 * @param trace traza completa del error.
	 * @param wsContext contexto del aplicativo
	 * 
	 * @return bitacora monitoreo para error
	 */
	public DTOMetricsBitacora getDTOError(String codigo, String desc, String trace){
		bitacora.setInTimestamp(String.valueOf(System.currentTimeMillis()));
		bitacora.getError().setReturnCode(codigo);
		bitacora.getError().setDescription(desc);
		bitacora.getError().setTrace(trace);
		return this.bitacora;
	}
	
	/**
	 * Metodo para inicializar DTO para servicios web.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param returnCode codigo de retorno
	 * @param wsAlias alias del servicio externo
	 * @param wsUrl url servicio externo
	 * @param wsMethod metodo de peticion
	 * @param wsType tipo de peticion
	 * @param time1 tiempo de inicio
	 * @param time2 tiempo fin
	 * @param error S / N si hubo error
	 * 
	 * @return bitacora monitoreo para WS externo
	 */
	public DTOMetricsBitacora getMetricsWS(String returnCode, String wsAlias, String wsUrl,
			String wsMethod, String wsType, String time1, String time2, String error){
		bitacora.setInTimestamp(time1);
		bitacora.setOuTtimestamp(time2);
		bitacora.getError().setError(error);
		bitacora.getWebService().setWsReturnCode(returnCode);
		bitacora.getWebService().setWsAlias(wsAlias);
		bitacora.getWebService().setWsUrl(wsUrl);
		bitacora.getWebService().setWsMethod(wsMethod);
		bitacora.getWebService().setWsType(wsType);
		return this.bitacora;
	}
}
