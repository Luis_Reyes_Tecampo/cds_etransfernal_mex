package mx.isban.metrics.util;

/**
 * Clase utileria para codigo de error Metrics.
 * 
 * @author ING. Juan Manuel Fuentes Ramos.
 * CSA Plan Delta Monitoreo Sucursales.
 * 2018 - 06
 */
public enum EnumMetricsErrors {

	//CATALOGO DE ERRORES
	/** ERROR DE NEGOCIO ND001*/
	DELTA_NEGOCIO_WS("ND001","Error de negocio en componente:"),
	/** ERROR DE NEGOCIO ND002*/
	DELTA_NEGOCIO_BO("ND002","Error de negocio en componente: "),
	/** ERROR DE NEGOCIO ND003*/
	DELTA_NEGOCIO_DAO("ND003","Error de negocio en componente: "),
	/** ERROR CRITICO DELTA001*/
	DELTA_INFRA_WS("DELTA001","Error CRITICO en componente:"),
	/** ERROR CRITICO DELTA002*/
	DELTA_INFRA_BO("DELTA002","Error CRITICO en componente: "),
	/** ERROR CRITICO DELTA999*/
	DELTA_INFRA_DAO("DELTA999","Error CRITICO en componente: ")
	;
	
	/** Variable para codigo de error.*/
	private String codeError;
	/** Variable para mensaje de error*/
	private String mensaje;
	
	/**
	 * Constructor. 
	 * 
	 * @author ING. Juan Manuel Fuentes Ramos
	 * CSA Plan Delta Monitoreo Sucursales.
	 * 2018 - 06
	 * 
	 * @param codeError1 codigo de error.
	 * @param mensaje1 mensaje de error.
	 */
	private EnumMetricsErrors(String codeError1, String mensaje1){
		this.codeError = codeError1;
		this.mensaje = mensaje1;
	}
	
	/**
	 * Obtiene el codigo de error.
	 * @return codeError de tipo String.
	 */
	public String getCodeError(){
		return codeError;
	}
	
	/**
	 * Obtiene el Mensaje de error.
	 * @return mensaje de tipo String.
	 */
	public String getMensaje(){
		return mensaje;
	}
}
