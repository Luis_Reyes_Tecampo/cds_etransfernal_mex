/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * MetricsSingleton.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */
package mx.isban.metrics.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import mx.isban.metrics.constantes.CFGConstants;
import mx.isban.metrics.dto.DTOConfiguracion;

import org.apache.log4j.Logger;

/**
 * Class singleton 
 * Para control de metrics en runtime.
 * Genera un solo objecto para la obtencion del estatus del modulo metrics,
 * ademas de modificar su valor sin necesidad de reiniciar la app.
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales
 * 
 */
public final class MetricsSingleton {
	
	/** The constant LOG*/
	private static final Logger LOG = Logger.getLogger(MetricsSingleton.class);
	/** Instancia Singleton*/
	private static MetricsSingleton instance;
	/** The mutex for synchronize with threads */
	private static Object mutex = new Object();
	/**Properties props PARA CARGA DE PROPERTIES*/
	private Properties props = null;
	/** DTO de configuracion*/
	private final DTOConfiguracion cfg = new DTOConfiguracion(); 
	
	/** CONSTRUCTOR */
	private MetricsSingleton(){
		leerConfiguracion();
	}
	
	/**
	 * Obtiene unica instancia de MetricsSingleton.
	 *
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return unica instancia de MetricsSingleton
	 */
	public static MetricsSingleton getInstance(){
		MetricsSingleton result = instance;
		if(result == null){
			synchronized(mutex){
				result = instance;
				if(result == null){
					instance = result = new MetricsSingleton();
				}
			}
		}
		return result;
	}

	/**
	 * Funcion para lectura de propiedades de configuracion.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 */
	public void leerConfiguracion(){
		FileInputStream fis = null;
		try {
			LOG.debug("############################## [INI] Configuracion Metrics ##############################");
			LOG.info("LEYENDO ARCHIVO DE CONFIGURACION PARA METRICS. . . ");
			props =  new Properties();
			fis = new FileInputStream(CFGConstants.PROPERTIES);
			props.load(fis);
			initDTO(props);
			LOG.debug(dtoToString());
			if(cfg.isActivarMetrics()){
				LOG.debug(CFGConstants.CFG_OK_A_INIT);
			}else{
				LOG.debug(CFGConstants.CFG_OK_D_INIT);
			}
		} catch (IOException ioe) {
			cfg.setActivarMetrics(false);//En caso de ocurrir un error en la lectura del archivo desactiva el modulo de metrics
			LOG.error(CFGConstants.CFG_ERROR_LOAD);
			LOG.error(ioe);
		} finally{
			try {
				if(fis != null){
					fis.close();
				}
				LOG.debug(CFGConstants.CLOSE_FILE);
				LOG.debug("############################## [END] Configuracion Metrics ##############################");
			} catch (IOException ioe2) {
				LOG.error(CFGConstants.CLOSE_FILE_ERROR);
				LOG.error(ioe2);
			}
		}
	}
	
	/**
	 * Funcion para LOG.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return str con los parametros leidos.
	 */
	public String dtoToString() {
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getCanonicalName());
		str.append("CFGMetrics ::: ");
		str.append(" [[[");
		str.append(" Metrics is=").append(cfg.isActivarMetrics());
		str.append(", Instancia DB=").append(cfg.getInstancia());
		str.append("]]]");
		return str.toString();
	}
	
	/**
	 * Metodo para inicializar DTO de configuracion, modulo metrics.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param props parametros leidos del archivo cfgMetrics.properties
	 */
	private void initDTO(Properties props){
		cfg.setActivarMetrics(validaEstatus(props.getProperty(CFGConstants.CFG_METRICS)));
		cfg.setInstancia(validaProp(props.getProperty(CFGConstants.INSTANCIA)));
	}
	
	/**
	 * Metodo para validar los parametros leidos del archivo cfgMetrics.properties
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param param propiedad del archivo cfgMetrics.properties
	 * @return valor de tipo String para el DTO de configuracion.
	 */
	private String validaProp(String param){
		if("".equals(param) || param == null){
			return "";
		}else{
			return param;
		}
	}
	
	/**
	 * Metodo para validar el estatus leido del archivo cfgMetrics.properties
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param param propiedad del archivo cfgMetrics.properties
	 * @return estatus a asignar para modulo metrics.
	 */
	private boolean validaEstatus(String param){
		if("".equals(param) || param == null){
			return false;
		}else{
			return Boolean.valueOf(param);
		}
	}
	
	/**
	 * Funcion para modificar configuracion de metrics desde peticion a servicio web.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param config true / false valor para activar o desactivar metrics.
	 * @return strEstatus estatus de la operacion.
	 */
	public String guardarConfiguracion(boolean config){
		String strEstatus = CFGConstants.CFG_ERROR;
		FileOutputStream fos = null;
		LOG.debug("############################## [INI] Actualizando configuracion Metrics ##############################");
		try {
			fos = new FileOutputStream(CFGConstants.PROPERTIES);
			props.setProperty(CFGConstants.CFG_METRICS, String.valueOf(config));
			props.store(fos,"Update CFG Metrics. . .");
			cfg.setActivarMetrics(config);
			if(config){
				strEstatus = CFGConstants.CFG_OK_A;
			}else{
				strEstatus = CFGConstants.CFG_OK_D;
			}
			LOG.debug(strEstatus);
			LOG.debug("Se actualiza singleton correctamente");
		} catch (IOException ioe3) {
			LOG.error("Ocurrio un error al actualizar el archivo de configuracion");
			LOG.error(ioe3);
		}finally{
			try {
				if(fos != null){
					fos.close();
				}
				LOG.debug(CFGConstants.CLOSE_FILE);
				LOG.debug("############################## [END] Actualizando configuracion Metrics ##############################");
			} catch (IOException ioe4) {
				LOG.error(CFGConstants.CLOSE_FILE_ERROR);
				LOG.error(ioe4);
			}
		}
		return strEstatus;
	}
	
	/**
	 * Metodo para obtener el obj de configuracion.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return cfg de tipo DTOConfiguracions
	 */
	public DTOConfiguracion getCFG(){
		return cfg;
	}
}
