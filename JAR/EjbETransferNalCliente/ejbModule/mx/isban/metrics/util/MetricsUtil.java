/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * MetricsUtil.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2017 - 10			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;

import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.dto.DTOMetricsBitacora;

import org.apache.log4j.Logger;

/**
 * The class MetricsUtil.
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales
 */
public final class MetricsUtil {
	
	/** The constant //LOG*/
	private static final Logger LOG = Logger.getLogger(MetricsUtil.class);

	/**
	 * Constructor privado.
	 * */
	private MetricsUtil(){}
	
	/**
	 * Metodo para obtener el host
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return hostName nombre del host
	 */
	public static String getHostName() {
		InetAddress ip = null;
		StringBuilder hostName = new StringBuilder();
		try {
			ip = InetAddress.getLocalHost();
			hostName.append(ip.getHostName());
			if(hostName.indexOf(".")>3){
				hostName = hostName.replace(hostName.indexOf("."), hostName.indexOf(".") + hostName.length(), "");
			}
		} catch (UnknownHostException e) {
			LOG.error(e);
		}
		return hostName.toString();
	}
	
	/**
	 * Metodo para obtener el host
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return hostName nombre del host
	 */
	public static String getHostNameFull() {
		InetAddress ip = null;
		StringBuilder hostName = new StringBuilder();
		try {
			ip = InetAddress.getLocalHost();
			hostName.append(ip.getHostName());
		} catch (UnknownHostException e) {
			LOG.error(e);
		}
		return hostName.toString();
	}
	
	/**
	 * METODO PARA OBTENER LA TABLA DIA PAR O IMPAR.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return numero de tabla por dia.
	 */
	public static String getTabla(){
		Calendar now = Calendar.getInstance();
		int dia = now.get(Calendar.DAY_OF_YEAR);
		int tabla = 0;
		if(dia%2==0){
            tabla = 2;
		}else{
			tabla = 1;
		} 		
		return String.valueOf(tabla);
	}
	
	/**
	 * Validar longitud de campo a insertar.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param campo
	 *            campo a insertar.
	 * @param longitud
	 *            longitud del campo.
	 * @return cadena con longitud correcta.
	 */
	public static String validaCampo(final String campo, final int longitud) {
		String cadena = campo;
		if (campo == null || "".equals(campo)) {
			cadena = "";
		} else if (campo != null && !"".equals(campo) && campo.length() > longitud && longitud > 0) {
			cadena = campo.substring(0, longitud);
		}
		return cadena;
	}
	
	/**
	 * Valida campos obligatorios para insercion BD Request.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param bitacora
	 *            the DTOMetrica
	 * @return true, si estan correctos los valores.
	 */
	public static Boolean validaRequest(DTOMetricsBitacora bitacora) {
		Boolean valido = true;
		if (isEmptyOrNull(bitacora.getInTimestamp())) {
			valido = false;
			LOG.info("El valor de RQ_INTIMESTAMP no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getOutTimestamp())) {
			valido = false;
			LOG.info("El valor de RQ_OUTTIMESTAMP no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getRequestId())) {
			valido = false;
			LOG.info(Constantes.MSG_REQUEST);
		} else if (isEmptyOrNull(bitacora.getError().getError())) {
			valido = false;
			LOG.info("El valor de ERROR no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getRequest().getThreadId())) {
			valido = false;
			LOG.info("El valor de THREADID no debe ir vacio o nulo \n No se realizara el insert");
		} 
		return valido;
	}
	
	/**
	 * Valida campos obligatorios para insercion BD Error.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param bitacora
	 *            the DTOMetrica
	 * @return true, si estan correctos los valores.
	 */
	public static Boolean validaError(DTOMetricsBitacora bitacora) {
		Boolean valido = true;
		if (isEmptyOrNull(bitacora.getInTimestamp())) {
			valido = false;
			LOG.info("El valor de ER_INTIMESTAMP no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getRequestId())) {
			valido = false;
			LOG.info(Constantes.MSG_REQUEST);
		} 
		return valido;
	}
	
	/**
	 * Valida campos obligatorios para insercion BD Servicios.
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param bitacora
	 *            the DTOMetrica
	 * @return true, si estan correctos los valores.
	 */
	public static Boolean validaWS(DTOMetricsBitacora bitacora) {
		Boolean valido = true;
		if (isEmptyOrNull(bitacora.getError().getError())) {
			valido = false;
			LOG.info("El valor de WSERROR no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getInTimestamp())) {
			valido = false;
			LOG.info("El valor de WSINTIMESTAMP no debe ir vacio o nulo \n No se realizara el insert");
		} else if (isEmptyOrNull(bitacora.getRequestId())) {
			valido = false;
			LOG.info(Constantes.MSG_REQUEST);
		} else if (isEmptyOrNull(bitacora.getOutTimestamp())) {
			valido = false;
			LOG.info("El valor de OUTTIMESTAMP no debe ir vacio o nulo \n No se realizara el insert");
		} 
		return valido;
	}
	
	/**
	 * Valida nulo y vacio
	 * 
	 * @autor ING. Juan Manuel Fuentes Ramos.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param value
	 *            parametro que sera sometido a validacon null / empity
	 * @return true si el valor es null o vacio
	 */
	private static boolean isEmptyOrNull(final String value) {
		boolean isEmptyOrNull = false;
		if (value == null || value.isEmpty()) {
			isEmptyOrNull = true;
		}else{
			value.trim();
		}
		return isEmptyOrNull;
	}
}