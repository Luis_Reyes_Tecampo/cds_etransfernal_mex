/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * UtileriasDAO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dao.util;

import org.apache.log4j.Logger;

import mx.isban.metrics.constantes.Constantes;
import mx.isban.metrics.dto.DTOMetricsBitacora;
import mx.isban.metrics.util.MetricsUtil;

/**
 * Clase utileria para servicios en capa DAO
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 -05 Plan Delta Monitores Sucurasales
 */
public final class UtileriasDAO {

	/** The constant LOG */
	private static final Logger LOG = Logger.getLogger(UtileriasDAO.class);

	/**
	 * Constructor privado.
	 */
	private UtileriasDAO() {
		throw new UnsupportedOperationException("No instanciar");
	}

	/**
	 * Metodo para armar la url del servicio
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param servicio url del servicio a bitacorizar.
	 *            
	 * @return url del servicio a bitacorizar
	 */
	public static String getURL(String servicio) {
		StringBuilder url = new StringBuilder(servicio);
		url.replace(url.indexOf("?"), url.indexOf("?") + 1, MetricsUtil.getHostNameFull());// Equipo
		return url.toString();
	}

	/**
	 * Metodo para imprimir en LOG los DTO que se insertan a BD.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param msj
	 *            estatus de operacion
	 * @param obj
	 *            DTO a insertar
	 * @param dto
	 *            identificador para impresion de parametros DTO (request-1,
	 *            error-2, ws-3)
	 */
	public static void printBitacora(String msj, Object obj, int dto) {
		LOG.debug(msj);
		if (dto == 1) {
			LOG.debug(Constantes.MSG_PARAMS + requestToString(obj));
		} else if (dto == 2) {
			LOG.debug(Constantes.MSG_PARAMS + errorToString(obj));
		} else {
			LOG.debug(Constantes.MSG_PARAMS + wsToString(obj));
		}
	}

	/**
	 * Funcion para LOG_ERROR.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param obj
	 *            DTO a imprimir
	 *            
	 * @return str cadena de valores en DTO
	 */
	public static String errorToString(Object obj) {
		StringBuilder str = new StringBuilder();
		DTOMetricsBitacora bitacora = (DTOMetricsBitacora) obj;
		str.append(" LOG_ERROR [[[");
		str.append(" erInTimestamp=").append(bitacora.getInTimestamp());
		str.append(", serverId=").append(bitacora.getServerId());
		str.append(", requestId=").append(bitacora.getRequestId());
		str.append(", appName=").append(bitacora.getAppName());
		str.append(", description=").append(
				bitacora.getError().getDescription());
		str.append(", returnCode=").append(bitacora.getError().getReturnCode());
		str.append(", trace=").append(bitacora.getError().getTrace());
		str.append("]]]");
		return str.toString();
	}

	/**
	 * Funcion para LOG_REQUEST
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param obj
	 *            DTO a imprimir
	 *            
	 * @return str cadena de valores en DTO
	 */
	public static String requestToString(Object obj) {
		StringBuilder str = new StringBuilder();
		DTOMetricsBitacora bitacora = (DTOMetricsBitacora) obj;
		str.append(" LOG_REQUEST [[[");
		str.append(" serverId=").append(bitacora.getServerId());
		str.append(", requestId=").append(bitacora.getRequestId());
		str.append(", appName=").append(bitacora.getAppName());
		str.append(", rqInTimestamp=").append(bitacora.getInTimestamp());
		str.append(", rqOuTtimestamp=").append(bitacora.getOutTimestamp());
		str.append(", error=").append(bitacora.getError().getError());
		str.append(", threadId=").append(bitacora.getRequest().getThreadId());
		str.append(", userId=").append(bitacora.getRequestWeb().getUserId());
		str.append(", sessionId=").append(
				bitacora.getRequestWeb().getSessionId());
		str.append(", ipClient=")
				.append(bitacora.getRequestWeb().getIpClient());
		str.append(", userAgent=").append(
				bitacora.getRequestWeb().getUserAgent());
		str.append(", acceptLanguage=").append(
				bitacora.getRequestWeb().getAcceptLanguage());
		str.append(", method=").append(bitacora.getRequest().getMethod());
		str.append(", url=").append(bitacora.getRequest().getUrl());
		str.append("]]]");
		return str.toString();
	}

	/**
	 * Funcion para LOG_WS.
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -05 Plan Delta Monitores Sucurasales
	 * 
	 * @param obj
	 *            DTO a imprimir
	 *            
	 * @return str cadena de valores en DTO
	 */
	public static String wsToString(Object obj) {
		StringBuilder str = new StringBuilder();
		DTOMetricsBitacora bitacora = (DTOMetricsBitacora) obj;
		str.append(" LOG_WS [[[");
		str.append(" wsRequestId=").append(bitacora.getRequestId());
		str.append(", wsInTimestamp=").append(bitacora.getInTimestamp());
		str.append(", wsOutTimestamp=").append(bitacora.getOutTimestamp());
		str.append(", wsReturnCode=").append(
				bitacora.getWebService().getWsReturnCode());
		str.append(", wsError=").append(bitacora.getError().getError());
		str.append(", wsAlias=").append(bitacora.getWebService().getWsAlias());
		str.append(", wsUrl=").append(bitacora.getWebService().getWsUrl());
		str.append(", wsMethod=")
				.append(bitacora.getWebService().getWsMethod());
		str.append(", wsType").append(bitacora.getWebService().getWsType());
		str.append("]]]");
		return str.toString();
	}
}
