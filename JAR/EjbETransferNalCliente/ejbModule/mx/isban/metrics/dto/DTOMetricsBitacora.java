/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsBitacora.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsBitacora Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsBitacora extends DTOMetrics implements Serializable {

	/** Serial Version */
	private static final long serialVersionUID = -5222572544235093905L;
	/** DTO para request */
	private DTOMetricsRequest request;
	/** DTO para bitacorizar requestWeb */
	private DTOMetricsRequestWeb requestWeb;
	/** DTO para bitacorizar errores */
	private DTOMetricsError error;
	/** DTO para bitacorizar peticiones a WebService */
	private DTOMetricsWebService webService;
	/** Nombre del servicio invocado*/
	private String servicio;

	/**
	 * Metodo para obtener request
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return request bean a bitacorizar
	 */
	public DTOMetricsRequest getRequest() {
		return request;
	}

	/**
	 * Metodo para asignar request
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param request
	 *            bean a bitacorizar
	 */
	public void setRequest(DTOMetricsRequest request) {
		this.request = request;
	}

	/**
	 * Metodo para obtener requestWeb
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return requestWeb bean a bitacorizar
	 */
	public DTOMetricsRequestWeb getRequestWeb() {
		return requestWeb;
	}

	/**
	 * Metodo para asignar requestWeb
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param requestWeb
	 *            bean a bitacorizar
	 */
	public void setRequestWeb(DTOMetricsRequestWeb requestWeb) {
		this.requestWeb = requestWeb;
	}

	/**
	 * Metodo para obtener error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return error bean a bitacorizar
	 */
	public DTOMetricsError getError() {
		return error;
	}

	/**
	 * Metodo para asignar error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param error
	 *            bean a bitacorizar
	 */
	public void setError(DTOMetricsError error) {
		this.error = error;
	}

	/**
	 * Metodo para obtener webService
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return webService bean a bitacorizar
	 */
	public DTOMetricsWebService getWebService() {
		return webService;
	}

	/**
	 * Metodo para asignar webService
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param webService
	 *            bean a bitacorizar
	 */
	public void setWebService(DTOMetricsWebService webService) {
		this.webService = webService;
	}

	/**
	 * Metodo para obtener servicio 
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return servicio de tipo String
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * Metodo para asignar servicio 
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param servicio de tipo String.
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
}
