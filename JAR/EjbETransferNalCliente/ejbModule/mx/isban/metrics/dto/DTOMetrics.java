/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetrics.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetrics Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetrics implements Serializable {

	/**
	 * SERIAL VERSION
	 */
	private static final long serialVersionUID = 6802037831676233941L;
	/** Momento en el que se inicia la peticion */
	private String inTimestamp;
	/** Momento en el que se finaliza la peticion */
	private String outTimestamp;
	/** Nombre o identificador del servidor */
	private String serverId;
	/** ID de solicitud */
	private String requestId;
	/** Nompre de la aplicacion */
	private String appName;

	/**
	 * Metodo para obtner inTimestamp
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @return the inTimestamp
	 */
	public String getInTimestamp() {
		return inTimestamp;
	}

	/**
	 * Metodo para asingar inTimestamp
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @param inTimestamp
	 *            the inTimestamp to set
	 */
	public void setInTimestamp(String inTimestamp) {
		this.inTimestamp = inTimestamp;
	}

	/**
	 * Metodo para obtener ouTtimestamp
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @return ouTtimestamp tiempo de respuesta de la peticion
	 */
	public String getOutTimestamp() {
		return outTimestamp;
	}

	/**
	 * Metodo para asignar ouTtimestamp
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @param ouTtimestamp
	 *            the ouTtimestamp to set
	 */
	public void setOuTtimestamp(String ouTtimestamp) {
		this.outTimestamp = ouTtimestamp;
	}

	/**
	 * Metodo para obtener requestId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * Metodo para asignar requestId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	/**
	 * Metodo para obtener serverId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @return the serverId
	 */
	public String getServerId() {
		return serverId;
	}

	/**
	 * Metodo para asignar serverId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @param serverId
	 *            the serverId to set
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	/**
	 * Metodo para obtener appNema
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @return appName nombre de la aplicacion
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * Metodo para asignar appName
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2017 -10 Plan Delta Monitores Sucurasales
	 * 
	 * @param appName
	 *            nombre de la aplicacion
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
}
