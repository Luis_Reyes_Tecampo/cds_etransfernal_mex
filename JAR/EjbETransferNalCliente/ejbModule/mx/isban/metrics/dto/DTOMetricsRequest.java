/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsRequest.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsRequest Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsRequest implements Serializable {

	/** SERIAL VERSION */
	private static final long serialVersionUID = 6340057790159559626L;
	/** Indica el identificador del "hilo" que se quiere usar. */
	private String threadId;
	/** forma de enviar los datos , metodos get y post */
	private String method;
	/** URL del servicio a bitacorizar */
	private String url;

	/**
	 * Metodo para obtener threadId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return the threadId
	 */
	public String getThreadId() {
		return threadId;
	}

	/**
	 * Metodo para asignar threadId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param threadId
	 *            the threadId to set
	 */
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	/**
	 * Metodo para obtener method
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Metodo para asignar method
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param method
	 *            the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Metodo para obtener url
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return url del servicio a bitacorizar
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Metodo para asignar url
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param url
	 *            del servicio a bitacorizar
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}