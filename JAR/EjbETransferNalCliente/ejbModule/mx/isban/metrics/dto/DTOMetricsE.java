/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsE.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 06			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsE Clase para obtener el estado de error
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 06 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsE implements Serializable {

	/**
	 * SERIAL VERSION
	 */
	private static final long serialVersionUID = 6802037831676233941L;
	/** Codigo de error*/
	private String codeError;
	/** Mensaje de error*/
	private String msgError;
	/** Traza completa del error*/
	private String traza;
	/** Bandera para verificar tipo de error (Negocio/Critico)*/
	private boolean error;
	
	/**
	 * Obtiene codigo de error
	 * @return codeError codigo de error
	 */
	public String getCodeError() {
		return codeError;
	}
	
	/**
	 * Asigna codigo de error
	 * @param codeError codigo de error
	 */
	public void setCodeError(String codeError) {
		this.codeError = codeError;
	}
	
	/**
	 * Obtiene mensaje de error
	 * @return msgError mensaje de error
	 */
	public String getMsgError() {
		return msgError;
	}
	
	/**
	 * Asigna mensaje de error
	 * @param msgError mensaje de error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	/**
	 * Obtiene traza completa del error
	 * @return traza traza completa del error
	 */
	public String getTraza() {
		return traza;
	}
	
	/**
	 * Asigna traza completa del error
	 * @param traza traza completa del error
	 */
	public void setTraza(String traza) {
		this.traza = traza;
	}
	
	/**
	 * Valida si existe error critico o de negocio
	 * @return true error critico false error negocio
	 */
	public boolean isError() {
		return error;
	}
	
	/**
	 * Asigna bandera de error
	 * @param error true error critico false error de negocio
	 */
	public void setError(boolean error) {
		this.error = error;
	}
}
