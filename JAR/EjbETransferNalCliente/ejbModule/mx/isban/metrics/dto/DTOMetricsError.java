/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsError.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsError Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsError implements Serializable {

	/** SERIAL VERSION */
	private static final long serialVersionUID = 6340057790159559626L;
	/**
	 * Valores S o N. S-->Se ha producido un error en la peticion y la excepcion
	 * no ha sido tratada. N-->Si la excepcion ha sido tratada.(Error
	 * controlado)
	 */
	private String error;
	/** descripcion del error */
	private String description;
	/** codigo de error */
	private String returnCode;
	/** Traza de error */
	private String trace;

	/**
	 * Metodo para obtener error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Metodo para asignar error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param error
	 *            the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Metodo para obtener descripcion
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return description descripcion del error
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Metodo para asignar descripcion
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Metodo para obtener codigo de retorno
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return returnCode codigo de retorno
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * Metodo para asignar codigo de retorno
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param returnCode
	 *            the returnCode to set
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Metodo para obtener traza del error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return trace la traza completa del error
	 */
	public String getTrace() {
		return trace;
	}

	/**
	 * Metodo para asignar la traza del error
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param trace
	 *            the trace to set
	 */
	public void setTrace(String trace) {
		this.trace = trace;
	}
}
