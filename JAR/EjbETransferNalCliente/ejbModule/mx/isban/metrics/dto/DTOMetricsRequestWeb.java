/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsRequestWeb.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsRequestWeb Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsRequestWeb implements Serializable {

	/** Serial Version */
	private static final long serialVersionUID = 2687739688210481042L;
	/** Identificador del usuario */
	private String userId;
	/** Identificador de la sesion. */
	private String sessionId;
	/** Direccion IP del cliente */
	private String ipClient;
	/** informacion del navegador del usuario */
	private String userAgent;
	/** Idioma aceptado para la respuesta de la peticion HTTP */
	private String acceptLanguage;

	/**
	 * Metodo para obtener userId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return userId parametro a bitacorizar
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Metodo para asignar
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param userId
	 *            parametro a bitacorizar
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Metodo para obtener sessionId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return sessionId parametro a bitacorizar
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Metodo para asignar sessionId
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param sessionId
	 *            parametro a bitacorizar
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Metodo para obtener ipClient
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return ipClient parametro a bitacorizar
	 */
	public String getIpClient() {
		return ipClient;
	}

	/**
	 * Metodo para asignar ipClient
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param ipClient
	 *            parametro a bitacorizar
	 */
	public void setIpClient(String ipClient) {
		this.ipClient = ipClient;
	}

	/**
	 * Metodo para obtener userAgent
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return userAgent parametro a bitacorizar
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * Metodo para asignar userAgent
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param userAgent
	 *            parametro a bitacorizar
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * Metodo para obtener acceptLanguage
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return acceptLanguage parametro a bitacorizar
	 */
	public String getAcceptLanguage() {
		return acceptLanguage;
	}

	/**
	 * Metodo para asignar acceptLanguage
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param acceptLanguage
	 *            parametro a bitacorizar
	 */
	public void setAcceptLanguage(String acceptLanguage) {
		this.acceptLanguage = acceptLanguage;
	}
}
