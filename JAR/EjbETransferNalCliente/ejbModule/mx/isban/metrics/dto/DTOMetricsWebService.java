/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOMetricsWebService.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOMetricsWebService Clase dto para bitacora a DB2
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOMetricsWebService extends DTOMetrics implements Serializable {

	/** SERIAL VERSION */
	private static final long serialVersionUID = 6340057790159559626L;
	/** codigo de error devuelto */
	private String wsReturnCode;
	/** Alias WS */
	private String wsAlias;
	/** url WS */
	private String wsUrl;
	/** forma de enviar los datos , metodos get y post */
	private String wsMethod;
	/** Tipo de web service SOAP , REST */
	private String wsType;
	
	/**
	 * Metodo para obtener wsReturnCode
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return wsReturnCode Obtiene codigo de error.
	 */
	public String getWsReturnCode() {
		return wsReturnCode;
	}

	/**
	 * Metodo para asignar wsReturnCode
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param wsReturnCode
	 * 			  Asigna codigo de error.
	 */
	public void setWsReturnCode(String wsReturnCode) {
		this.wsReturnCode = wsReturnCode;
	}

	/**
	 * Metodo para obtener wsAlias
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return Obtiene Servicio de peticion.
	 */
	public String getWsAlias() {
		return wsAlias;
	}

	/**
	 * Metodo para asignnar wsAlias
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param wsAlias
	 * 			  Asigna Servicio de peticion.
	 */
	public void setWsAlias(String wsAlias) {
		this.wsAlias = wsAlias;
	}

	/**
	 * Metodo para obtener wsUrl
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return Obtiene URL de peticion.
	 */
	public String getWsUrl() {
		return wsUrl;
	}

	/**
	 * Metodo para asignar wsUrl
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param wsUrl
	 * 			  Asigna URL de peticion.
	 */
	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}

	/**
	 * Metodo para obtener wsMethod
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return Obtiene metodo de peticion Post / Get.
	 */
	public String getWsMethod() {
		return wsMethod;
	}

	/**
	 * Metodo para asingar wsMethod
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param wsMethod
	 * 			  Asigna metodo de peticion Post / Get.
	 */
	public void setWsMethod(String wsMethod) {
		this.wsMethod = wsMethod;
	}

	/**
	 * Metodo para obtener wsType
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return Obtiene tipo de servicio llamado SOAP / REST.
	 */
	public String getWsType() {
		return wsType;
	}

	/**
	 * Metodo para asingar wsType
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param wsType
	 * 			  Asigna tipo de servicio llamado SOAP / REST.
	 */
	public void setWsType(String wsType) {
		this.wsType = wsType;
	}
}
