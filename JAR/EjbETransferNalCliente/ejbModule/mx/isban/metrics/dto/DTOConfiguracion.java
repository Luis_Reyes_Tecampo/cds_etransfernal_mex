/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * DTOConfiguracion.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.dto;

import java.io.Serializable;

/**
 * Class: DTOConfiguracion Clase dto cargar parametros de configuracion.
 * 
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public class DTOConfiguracion implements Serializable{
	
	/**
	 * SERIAL VERSION
	 */
	private static final long serialVersionUID = 920037431403385554L;
	/** Bandera para activar o desactivar metrica*/
	private boolean activarMetrics;
	/** Nombre del WS*/
	private String appName;
	/** Nombre de la instancia Delta */
	private String instancia;
	
	/**
	 * Metodo de obtencion para activarMetrics
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return activarMetrics estatus del modulo metrics
	 */
	public boolean isActivarMetrics() {
		return activarMetrics;
	}
	
	/**
	 * Metodo de asignacion para activarMetrics
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param activarMetrics estatus del modulo metrics
	 */
	public void setActivarMetrics(boolean activarMetrics) {
		this.activarMetrics = activarMetrics;
	}
	
	/**
	 * Metodo de obtencion para appName
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return appName nombre de la aplicacion
	 */
	public String getAppName() {
		return appName;
	}
	
	/**
	 * Metodo de asignacion para appName
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param appName nombre de la aplicacion
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	/**
	 * Metodo de obtencion para instancia
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @return instancia de la base de datos para metrics.
	 */
	public String getInstancia() {
		return instancia;
	}
	
	/**
	 * Metodo de asignacion para instancia
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @since 2018 - 05 Plan Delta Monitores Sucurasales
	 * 
	 * @param instancia de la base de datos para metrics.
	 */
	public void setInstancia(String instancia) {
		this.instancia = instancia;
	}
}
