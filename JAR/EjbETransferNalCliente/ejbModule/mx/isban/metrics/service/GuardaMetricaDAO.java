/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BitacorizaMetrics.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.service;

import javax.ejb.Local;

import mx.isban.metrics.dto.DTOMetricsBitacora;

/**
 * IFace BitacorizaMetrics para la logica de insercion a Metrics.
 * @author ING. JUAN MANUEL FUENTES RAMOS.
 * @since 2018 -05 Plan Delta Monitores Sucurasales
 */
@Local
public interface GuardaMetricaDAO {

	/**
	 * Metodo para insercion a tabla LOG_REQUEST_PH
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @param bitacora DTOMetricaRequest con los valores a insertar
	 * @param tabla indicador para la tabla a la que se le ejecutara el insert.
	 */
	void insertaRequest(DTOMetricsBitacora bitacora, String tabla);
	
	/**
	 * Metodo para insercion a tabla LOG_ERROR_PH
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @param bitacora DTOMetricaError con los valores a insertar
	 * @param tabla indicador para la tabla a la que se le ejecutara el insert.
	 */
	void insertaError(DTOMetricsBitacora bitacora, String tabla);
	
	/**
	 * Metodo para insercion a tabla LOG_WS_PH
	 * 
	 * @author ING. JUAN MANUEL FUENTES RAMOS.
	 * @param bitacora DTOMetricaServicios con los valores a insertar
	 * @param tabla indicador para la tabla a la que se le ejecutara el insert.
	 */
	void insertaServicios(DTOMetricsBitacora bitacora, String tabla);
	
}
