/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BitacorizaMetrics.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.service;

import javax.ejb.Remote;

import mx.isban.metrics.dto.DTOMetricsBitacora;

/**
 * Interface para servicio de bitacorizacion a metricas
 * 
 * @author ING JUAN MANUEL FUENTES RAMOS
 * @since 2018 - 05 PLAN DELTA SUCURSALES
 */
@Remote
public interface BitacorizaMetrics {

	/**
	 * Metodo para bitacorizar request
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param bitacora
	 * 			  bean con los parametros a bitacorizar
	 */
	void bitacorizaRequest(DTOMetricsBitacora bitacora);
	
	/**
	 * Metodo para bitacorizar error
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param bitacora
	 * 			  bean con los parametros a bitacorizar
	 */
	void bitacorizaError(DTOMetricsBitacora bitacora);
	
	/**
	 * Metodo para bitacorizar Ws
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param bitacora
	 * 			  bean con los parametros a bitacorizar
	 * @param flag
	 * 			  bandera para error.
	 */
	void bitacorizaWs(DTOMetricsBitacora bitacora);
	
	/**
	 * Metodo para actualizar modulo metrics
	 * 
	 * @author ING JUAN MANUEL FUENTES RAMOS
	 * @since 2018 - 05 PLAN DELTA SUCURSALES
	 * 
	 * @param estatus de tipo String.
	 * @return estatus del modulo
	 */
	String configMetrics(String estatus);
}
