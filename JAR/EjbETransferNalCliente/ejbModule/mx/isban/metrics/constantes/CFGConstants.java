/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * CFGConstants.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	"JMFR"				Creacion de archivo
 */

package mx.isban.metrics.constantes;


/**
* Clase utileria CFGConstants 
* 
* Contiene las constantes para metrics modulo de configuracion. 
* 
* @author ING. JUAN MANUEL FUENTES RAMOS.
* @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
*/
public final class CFGConstants {
	
	/** The constant PROPERTIES */
	public static final String PROPERTIES = "/proarchivapp/WebSphere85/was85/cfgMetrics/cfgMetrics.properties";

	/** The constant CFG_METRICS */
	public static final String CFG_METRICS = "cfgMetrics";
	
	/** Nombre de la instancia Delta */
	public static final String INSTANCIA = "esquemaBD";

	/** The constant CFG_ERROR */
	public static final String CFG_ERROR = "Se produjo un error al intentar actualizar CFG Metrics";

	/** The constant CFG_ERROR_LOAD */
	public static final String CFG_ERROR_LOAD = "Ocurrio un error al leer archivo properties!!";

	/** The constant CFG_OK_D */
	public static final String CFG_OK_D = "Se desactivo modulo metrics correctamente.";

	/** The constant CFG_OK_A */
	public static final String CFG_OK_A = "Se activo modulo metrics correctamente.";

	/** The constant CFG_OK_D_INIT */
	public static final String CFG_OK_D_INIT = "Modulo metrics apagado!!!";

	/** The constant CFG_OK_A_INIT */
	public static final String CFG_OK_A_INIT = "Modulo metrics encendido!!!";

	/** The constant CLOSE_FILE */
	public static final String CLOSE_FILE = "Se cierra lectura de archivo cfgMetrics.propeties. . .";

	/** The constant CLOSE_FILE_ERROR */
	public static final String CLOSE_FILE_ERROR = "Ocurrio un error al cerrar archivo cfgMetrics.properties!";

	
	/**
	 * Constructor privado.
	 */
	private CFGConstants(){	}
}
