/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Constantes.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 				By 					Description
 * ------- 	------------------- 	----------------	-----------------------------------------------------------------
 * 1.0 		2018 - 05			 	Indra FSW			Creacion de archivo
 */

package mx.isban.metrics.constantes;

/**
 * Clase para constantes de metricas.
 * 
 * @author Indra FSW
 * @since 2018 - 05 Plan Delta Monitores Sucurasales [[ CSA ]]
 */
public final class Constantes {
	
	/** The constant URL*/
	public static final String SERVICE_SPEI = "https://?:1443/eTransferNal/ConsultaImporteSpeiService";
	
	/** The constant URL*/
	public static final String SERVICE_FIEL = "https://?:1443/eTransferNal/ConsultaCertificadoService";
	
	/** The constant URL*/
	public static final String SERVICE_SPID = "https://?:1443/eTransferNal/ConsultaDetalladaService";
	
	/** The constant */
	public static final String MSG_INSERT_ERR = "Se produjo un error al intentar bitacorizar metrica.";
	
	/** The constant */
	public static final String MSG_INSERT_OK = "Registro insertado correctamente";

	/** The constant MSG_PARAMS*/
	public static final String MSG_PARAMS = "Params to insert: ";
	
	/** Error de metricas */
	public static final String ERRDELTA = "DELTA999";
	
	/** Cadena vacia */
	public static final String VACIA = "";
	
	/** The constant MSG_REQUEST*/
	public static final String MSG_REQUEST = "El valor de REQUESTID no debe ir vacio o nulo \n No se realizara el insert";
	
	/** The constant ACCEPTLANGUAGE*/
	public static final String ACCEPTLANGUAGE = "ES-MX";
	
	/** The constant POST*/
	public static final String POST = "POST";
	
	/** The constant TF_CLIENT*/
	public static final String TF_CLIENT = "TF- MQTUXSRV";

	/** The constant APP_NAME*/
	public static final String APP_NAME = "eTransferNal";
	
	/**
	 * constructor
	 */
	private Constantes() {
	}
}
