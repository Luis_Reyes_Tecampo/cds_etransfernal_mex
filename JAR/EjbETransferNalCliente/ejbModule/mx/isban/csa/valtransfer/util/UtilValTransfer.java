/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.util.UtilValTransfer.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.util;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.ejb.BOValidacion;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;

/**
 * The Class UtilValTransfer.
 */
public final class UtilValTransfer {

	/**
	 * Instantiates a new util val transfer.
	 */
	private UtilValTransfer() {
		throw new UnsupportedOperationException("No instanciar");
	}

	/**
	 * Crea bitacora.
	 *
	 * @param bitacora the bitacora
	 * @param usuario  the usuario
	 * @param ipRemota the ip remota
	 */
	public static void creaBitacora(BitacoraDTO bitacora, String usuario, String ipRemota) {
		// valida si se obtuvo la ip del solicitante si no se deja el default
		if (ipRemota != null && !"".equalsIgnoreCase(ipRemota)) {
			bitacora.setIpClient(ipRemota);
		}
		// se asigna el usuario
		bitacora.getOperacion().setUsuario(usuario);
	}

	/**
	 * Invoca peticion.
	 *
	 * @param boValidacion the bo validacion
	 * @param bitacora the bitacora
	 * @param reqTransfer the req transfer
	 * @param asb the asb
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	public static String invocaPeticion(BOValidacion boValidacion, BitacoraDTO bitacora,
			ReqValidacionDTO reqTransfer, ArchitechSessionBean asb) throws BusinessException {
		String validacion = null;
		// Si se solicita validacion de inicio
		if (KtesGlobal.INICIO.equalsIgnoreCase(reqTransfer.getValidacion())) {
			validacion = boValidacion.getInicioTransfer(asb, reqTransfer, bitacora);
		}
		// Si se solicita monitoroe de operativa
		if (KtesGlobal.MONITOREO.equalsIgnoreCase(reqTransfer.getValidacion())) {
			validacion = boValidacion.getMonitoreo(asb, reqTransfer, bitacora);
		}
		// Si se solicita validacion de cierre
		if (KtesGlobal.CIERRE.equalsIgnoreCase(reqTransfer.getValidacion())) {
			validacion = boValidacion.getCierreTransfer(asb, reqTransfer, bitacora);
		}
		return validacion;
	}
}
