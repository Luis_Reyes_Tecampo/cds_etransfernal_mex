/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.util.UtilCoreValidacion.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.ValInitCoreDTO;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;

/**
 * The Class UtilCoreValidacion.
 */
public final class UtilCoreValidacion {

	/**
	 * Instantiates a new util core validacion.
	 */
	private UtilCoreValidacion() {
		throw new UnsupportedOperationException("No instanciar");
	}
	
	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public static String getTime() {
		//obtiene el tiempo en formato hora minuto segundo 
		//para validar cada minuto los querys programados 
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date resultdate = new Date(yourmilliseconds);
		return sdf.format(resultdate);
	}
	
	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public static String getDay() {
		//obtiene el tiempo en formato hora minuto segundo 
		//para validar cada minuto los querys programados 
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date resultdate = new Date(yourmilliseconds);
		return sdf.format(resultdate);
	}

	/**
	 * Llena bit carga bd.
	 *
	 * @param cfg the cfg
	 * @return the bitacora DTO
	 */
	public static BitacoraDTO llenaBitCargaBd(ValInitCoreDTO cfg) {
		//se crea un obj de bitacora para peticion programada o carga de querys 
		BitacoraDTO bit = cfg.getBitacora();
		bit.getOperacion().setComentarios("Cargando querys de monitoreo");
		bit.getTablaAfecatada().setDatoModi("INIMON01");
		bit.getTablaAfecatada().setTablaAfect("TRAN_VALINI_CONFIG");
		bit.getTablaAfecatada().setValNvo("INICIO");
		UtilValidador.validaLongitud(KtesGlobal.QUERY_TRANVAL, "N/A", bit);
		return bit;
	}
}
