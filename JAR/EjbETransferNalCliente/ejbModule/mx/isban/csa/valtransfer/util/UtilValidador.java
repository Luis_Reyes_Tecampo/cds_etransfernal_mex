/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.util.UtilValidador.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.util;

import java.util.List;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.QuerysListDTO;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.ktes.ECatalogoError;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;

/**
 * The Class UtilValidador.
 */
public final class UtilValidador {

	/**
	 * Instantiates a new util validador.
	 */
	private UtilValidador() {
		throw new UnsupportedOperationException("No instanciar");
	}

	/**
	 * Valida modulos.
	 *
	 * @param validacion the validacion
	 * @param modulo     the modulo
	 * @param usuario    the usuario
	 * @throws BusinessException the business exception
	 */
	public static void validaModulos(String validacion, String modulo, String usuario) throws BusinessException {
		if ("".equalsIgnoreCase(validacion) || validacion == null) {
			throw new BusinessException(ECatalogoError.VTVAL001.getCodeError(),
					String.format(ECatalogoError.VTVAL001.getMsgError(), "VALIDACION"));
		}

		if ("".equalsIgnoreCase(modulo) || modulo == null) {
			throw new BusinessException(ECatalogoError.VTVAL001.getCodeError(),
					String.format(ECatalogoError.VTVAL001.getMsgError(), "MODULO"));
		}

		if ("".equalsIgnoreCase(usuario) || usuario == null) {
			throw new BusinessException(ECatalogoError.VTVAL001.getCodeError(),
					String.format(ECatalogoError.VTVAL001.getMsgError(), "USUARIO"));
		}
	}

	/**
	 * Valida parametros entrada.
	 *
	 * @param peticion the peticion
	 * @throws BusinessException the business exception
	 */
	public static void validaParametrosEntrada(ReqConsultaDTO peticion) throws BusinessException {

		// Valida nombre de base de datos
		if ("".equalsIgnoreCase(peticion.getDataBase()) || peticion.getDataBase() == null) {
			throw new BusinessException(ECatalogoError.VTVAL001.getCodeError(),
					String.format(ECatalogoError.VTVAL001.getMsgError(), "DataBase"));
		}

		// Valida query
		if ("".equalsIgnoreCase(peticion.getQuery()) || peticion.getQuery() == null) {
			throw new BusinessException(ECatalogoError.VTVAL001.getCodeError(),
					String.format(ECatalogoError.VTVAL001.getMsgError(), "Query a ejecutar"));
		}

		validaSentenciaSQL(peticion);

	}

	/**
	 * Valida sentencia SQL.
	 *
	 * @param peticion the peticion
	 * @throws BusinessException the business exception
	 */
	private static void validaSentenciaSQL(ReqConsultaDTO peticion) throws BusinessException {
		StringBuilder sbValidaFiltro = new StringBuilder(peticion.getQuery());
		// VALIDA QUE LA PETICION NO CONTENGA SENTENCIAS INSERT UPDATE Y DELETE
		if (peticion.getQuery().toUpperCase().contains("INSERT") || peticion.getQuery().toUpperCase().contains("DELETE")
				|| peticion.getQuery().toUpperCase().contains("UPDATE")) {
			throw new BusinessException(ECatalogoError.VTVAL003.getCodeError(), ECatalogoError.VTVAL003.getMsgError());
		}

		// VALIDA QUE EL QUERY NO CONTENGA * PARA OPTIMIZAR EL QUERY Y SOLO OBTENER LOS
		// CAMPOS REQUERIDOS
		if (!peticion.getQuery().toUpperCase().contains("COUNT(*)") && peticion.getQuery().contains("*")) {
			throw new BusinessException(ECatalogoError.VTVAL002.getCodeError(), ECatalogoError.VTVAL002.getMsgError());
		}

		// VALIDA QUE NO SE USE EL TRUNC
		if (peticion.getQuery().toUpperCase().contains("TRUNC")) {
			throw new BusinessException(ECatalogoError.VTVAL006.getCodeError(), ECatalogoError.VTVAL006.getMsgError());
		}

		// VALIDA SI EL QUERY CONTIENE LA PALABRA MENOR_IGUAL LA REMPLAZA POR EL LA
		// EXPRESION <=
		if (peticion.getQuery().toUpperCase().contains(KtesGlobal.MAYOR)) {
			sbValidaFiltro.replace(sbValidaFiltro.indexOf(KtesGlobal.MAYOR),
					sbValidaFiltro.indexOf(KtesGlobal.MAYOR) + 6, ">");
		}

		// VALIDA SI EL QUERY CONTIENE LA PALABRA MENOR LA REMPLAZA POR EL LA EXPRESION
		// <
		if (peticion.getQuery().toUpperCase().contains(KtesGlobal.MENOR)) {
			sbValidaFiltro.replace(sbValidaFiltro.indexOf(KtesGlobal.MENOR),
					sbValidaFiltro.indexOf(KtesGlobal.MENOR) + 6, "<");
		}

		peticion.setQuery(sbValidaFiltro.toString());
	}

	/**
	 * Valida usuario.
	 *
	 * @param usuarios the usuarios
	 * @param usrCSA   the usr CSA
	 * @return true, if successful
	 */
	public static boolean validaUsuario(List<QuerysListDTO> usuarios, String usrCSA) {
		boolean resp = false;
		for (QuerysListDTO user : usuarios) {
			if (usrCSA.equalsIgnoreCase(user.getQuery())) {
				resp = true;
			}
		}
		//se agrega usuario default 
		if(!resp && KtesGlobal.USER_DEF.equalsIgnoreCase(usrCSA)) {
			resp = true;
		}
		return resp;
	}

	/**
	 * Valida longitud.
	 *
	 * @param query     the query
	 * @param descQuery the desc query
	 * @param bit       the bit
	 */
	public static void validaLongitud(String query, String descQuery, BitacoraDTO bit) {
		if (query.length() > 4000) {
			bit.getTablaAfecatada().setValAnt(query.substring(0, 40000));
		} else {
			bit.getTablaAfecatada().setValAnt(query);
		}
		if (descQuery.length() > 4000) {
			bit.getTablaAfecatada().setDatoFijo(descQuery.substring(0, 40000));
		} else {
			bit.getTablaAfecatada().setDatoFijo(descQuery);
		}
	}

	/**
	 * Gets the tiempo ejec.
	 *
	 * @param inTime the in time
	 * @return the tiempo ejec
	 */
	public static String getTiempoEjec(long inTime) {
		long tiempoRespuesta = System.currentTimeMillis() - inTime;
		return String.valueOf(String.format("Tiempo de respuesta [%d] seg", tiempoRespuesta));
	}
}
