/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.util.UtilBitacora.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.util;

import java.util.Date;

import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.BitacoraOperacionDTO;
import mx.isban.csa.valtransfer.beans.BitacoraTblAfectadaDTO;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdmin;
import mx.isban.eTransferNal.beans.comunes.BeanReqInsertBitAdminDos;
import mx.isban.eTransferNal.utilerias.moduloCDA.Utilerias;

/**
 * The Class UtilBitacora.
 */
public final class UtilBitacora {

	/**
	 * Instantiates a new util bitacora.
	 */
	private UtilBitacora() {
		throw new UnsupportedOperationException("No instanciar");
	}

	/**
	 * Creates the bitacora bit admin.
	 *
	 * @param bitacora the bitacora
	 * @return the bean req insert bit admin
	 */
	public static BeanReqInsertBitAdmin createBitacoraBitAdmin(BitacoraDTO bitacora) {

		Date fecha = new Date();
		Utilerias utilerias = Utilerias.getUtilerias();

		// CREA EL OBJ DE BITACORA USADO POR EL SISTEMA PARA BITACORA ADMINISTRATIVA
		BeanReqInsertBitAdmin beanReqInsertBitAdmin = new BeanReqInsertBitAdmin();
		beanReqInsertBitAdmin.setBeanReqInsertBitAdminDos(new BeanReqInsertBitAdminDos());

		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setCodOper(bitacora.getOperacion().getCodOper());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setTablaAfec(bitacora.getTablaAfecatada().getTablaAfect());
		beanReqInsertBitAdmin.setTipoOper(bitacora.getOperacion().getTipoOper());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setServTran(bitacora.getOperacion().getServTran());
		beanReqInsertBitAdmin.setValAnt(bitacora.getTablaAfecatada().getValAnt());
		beanReqInsertBitAdmin.setValNvo(bitacora.getTablaAfecatada().getValNvo());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDatoMobi(bitacora.getTablaAfecatada().getDatoModi());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDatoFijo(bitacora.getTablaAfecatada().getDatoFijo());
		beanReqInsertBitAdmin.setComentarios(bitacora.getOperacion().getComentarios());

		beanReqInsertBitAdmin.setUserOper(bitacora.getOperacion().getUsuario());
		beanReqInsertBitAdmin.setIdToken("0");
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setCanApli("Transfer Nacional");

		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setDirIp(bitacora.getIpClient());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setInstWeb(bitacora.getInstWeb());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setHostWeb(bitacora.getHostname());
		beanReqInsertBitAdmin.getBeanReqInsertBitAdminDos().setIdSession("N/A peticion via SOAP");
		beanReqInsertBitAdmin.setFchOper(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanReqInsertBitAdmin.setFchAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY));
		beanReqInsertBitAdmin
				.setHoraAct(utilerias.formateaFecha(fecha, Utilerias.FORMATO_DD_GUION_MM_GUION_YYYY_HH_MM_SS));
		return beanReqInsertBitAdmin;
	}

	/**
	 * Gets the bita oper.
	 *
	 * @param codOper     the cod oper
	 * @param comentarios the comentarios
	 * @param servTran    the serv tran
	 * @param tipoOper    the tipo oper
	 * @param usuario     the usuario
	 * @return the bita oper
	 */
	public static BitacoraOperacionDTO getBitaOper(String codOper, String comentarios, String servTran, String tipoOper,
			String usuario) {
		// CREA OBJ DE BITACORA PARA LA OPERACION A EJECUTAR
		BitacoraOperacionDTO operacion = new BitacoraOperacionDTO();
		operacion.setCodOper(codOper);
		operacion.setComentarios(comentarios);
		operacion.setServTran(servTran);
		operacion.setTipoOper(tipoOper);
		operacion.setUsuario(usuario);
		return operacion;
	}

	/**
	 * Gets the bita tbl afect.
	 *
	 * @param datoFijo   the dato fijo
	 * @param datoModi   the dato modi
	 * @param tablaAfect the tabla afect
	 * @param valAnt     the val ant
	 * @param valNvo     the val nvo
	 * @return the bita tbl afect
	 */
	public static BitacoraTblAfectadaDTO getBitaTblAfect(String datoFijo, String datoModi, String tablaAfect,
			String valAnt, String valNvo) {
		// CREA OBJ DE BITACORA CON LA TABLA AFECTADA
		BitacoraTblAfectadaDTO tblAfectada = new BitacoraTblAfectadaDTO();
		tblAfectada.setDatoFijo(datoFijo);
		tblAfectada.setDatoModi(datoModi);
		tblAfectada.setTablaAfect(tablaAfect);
		tblAfectada.setValAnt(valAnt);
		tblAfectada.setValNvo(valNvo);
		return tblAfectada;
	}
}
