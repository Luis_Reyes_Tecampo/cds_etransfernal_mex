/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [WarETransferNal_Ejb]
 * mx.isban.eTransferNal.listener.ValidadorCsaListener.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		9/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.BitacoraOperacionDTO;
import mx.isban.csa.valtransfer.beans.BitacoraTblAfectadaDTO;
import mx.isban.csa.valtransfer.ejb.BOValTransfer;
import mx.isban.csa.valtransfer.ktes.ECatalogoError;
import mx.isban.csa.valtransfer.ktes.KtesGlobal;

/**
 * The listener interface for receiving validadorCsa events.
 * The class that is interested in processing a validadorCsa
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addValidadorCsaListener<code> method. When
 * the validadorCsa event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ValidadorCsaEvent
 */
public class ValidadorCsaListener extends Architech implements ServletContextListener{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1421469273069211743L;

	/** The bo valini. */
	@EJB
	private BOValTransfer boValini;
	
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		info("Se destruye componente de validacion");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext servletContext = sce.getServletContext();
		if (servletContext != null) {
			String instanciaWeb = null;
			//Obtiene la instancia en la cual se levanta el aplicativo. 
			instanciaWeb = servletContext.getAttribute(KtesGlobal.GET_INSTANCIA).toString();
			//crea el obj de bitacora para peticiones desde el componente de control 
			BitacoraDTO bitacora = new BitacoraDTO();
			//inicializa DTO para operaciones 
			bitacora.setOperacion(new BitacoraOperacionDTO());
			//inicializa el DTO para tabla afectada
			bitacora.setTablaAfecatada(new BitacoraTblAfectadaDTO());
			//asigna la instancia de ejecucion 
			bitacora.setInstWeb(instanciaWeb);
			//obtiene datos del equipo 
			getHostName(bitacora);
			//envia la bitacora para guardarla en memoria 
			boValini.setBitacoraSingle(getArchitechBean(), bitacora, getEsquema(bitacora));
		}
	}
	
	
	/**
	 * Gets the esquema.
	 *
	 * @param fullhostname the fullhostname
	 * @return the esquema
	 */
	private String getEsquema(BitacoraDTO bitacora) {
		String host = KtesGlobal.HOSTNAME;
		info("Validando esquema para equipo: "+bitacora.getHostname());
		String esquema = "BIATUX";
		if(bitacora.getHostname().contains(".dev")) {
			esquema = "BIATUX";
		}
		
		if(bitacora.getHostname().contains(".cert") || bitacora.getHostname().contains(".pre") || bitacora.getHostname().contains(".pro")) {
			esquema = "STPROD";
		}
		info("Esquema ::" + esquema);
		
		if(bitacora.getHostname().contains(KtesGlobal.PUNTO)) {
			host = bitacora.getHostname().substring(0, bitacora.getHostname().indexOf(KtesGlobal.PUNTO));
		}
		bitacora.setHostname(host);
		return esquema;
	}
	
	
	/**
	 * Gets the host name.
	 *
	 * @param bitacora the bitacora
	 * @param fullhostname the fullhostname
	 * @return the host name
	 */
	private void getHostName(BitacoraDTO bitacora) {
		try {
			//obtiene el obj inetaddress
			InetAddress localhost = InetAddress.getLocalHost();
			//obtiene el hostname
			bitacora.setHostname(localhost.getHostName());
			//obtiene la ip del servidor 
			bitacora.setIpClient(localhost.getHostAddress());
			//asigna nombre del servicio 
			bitacora.getOperacion().setServTran(KtesGlobal.SERVICIO_WEB);
			//asigna codigo de operacion 
			bitacora.getOperacion().setCodOper(ECatalogoError.VTMON000.getCodeError());
			//Asigna tipo de operacion
			bitacora.getOperacion().setTipoOper(KtesGlobal.SELECT);
			//asigna usuario default para peticiones desde el aplicativo 
			bitacora.getOperacion().setUsuario(KtesGlobal.USUARIO_DEFAULT);
		} catch (UnknownHostException e) {
			//se crea obj para ip SONAR
			StringBuilder ip = new StringBuilder("127.0.0.0");
			ip.append("127.0").append("0.0");
			//imprime en log en caso de error
			showException(e);
			//asigna el hostname default  
			bitacora.setHostname(KtesGlobal.HOSTNAME);
			//obtiene la ip del servidor 
			bitacora.setIpClient(ip.toString());
			//asigna nombre del servicio 
			bitacora.getOperacion().setServTran(KtesGlobal.SERVICIO_WEB);
			//asigna codigo de operacion 
			bitacora.getOperacion().setCodOper(ECatalogoError.VTMON000.getCodeError());
			//Asigna tipo de operacion
			bitacora.getOperacion().setTipoOper(KtesGlobal.SELECT);
			//asigna usuario default para peticiones desde el aplicativo 
			bitacora.getOperacion().setUsuario(KtesGlobal.USUARIO_DEFAULT);
		}
	}

}
