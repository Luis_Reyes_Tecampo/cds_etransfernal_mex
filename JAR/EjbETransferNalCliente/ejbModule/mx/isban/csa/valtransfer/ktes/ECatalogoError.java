/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.ktes.ECatalogoError.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ktes;

/**
 * The Enum ECatalogoError.
 */
public enum ECatalogoError {

	/** The vtini000. */
	VTINI000("VTINI000", "Validacion exitosa"),

	/** The vtini999. */
	VTINI999("VTINI999", "No se pudo inicializar el componente @EJB"),

	/** The vtdae001. */
	VTDAE001("VTDAE001", "--NO ROWS SELECTED"),

	/** The vtval000. */
	VTVAL000("VTVAL000", "Error, sentencia SELECT inforrecta favor de validar."),
	
	/** The vtval001. */
	VTVAL001("VTVAL001", "Error, campo obligatorio nulo [%s]"),

	/** The vtval002. */
	VTVAL002("VTVAL002", "Error, no se permite uso de * "),

	/** The vtval003. */
	VTVAL003("VTVAL003",
			"Ejecucion no autorizada, sentencias [INSERT DELETE UPDATE] no permitidas ejecutar mediante QOM"),

	/** The vtval004. */
	VTVAL004("VTVAL004", "Ultima pagina disponible => %s"),

	/** The vtval005. */
	VTVAL005("VTVAL005", "Pagina [%d] de [%d]"),

	/** The vtval006. */
	VTVAL006("VTVAL006", "No ser permite el uso de TRUNC"),

	/** The vtval007. */
	VTVAL007("VTVAL007", "Usuario sin permisos para uso del servicio de validacion."),

	/** The vtval008. */
	VTVAL008("VTVAL008", "Para consultas a tablas historicas se debe activar la paginacion."),

	/** The vtmon000. */
	VTMON000("VTMON000", "Ejecuta querys de monitoreo para transfer."),

	/** The vtday000. */
	VTDAY000("VTDAY000", "No se valida modulo TEF por dia inhabil."),

	/** The vtdef999. */
	VTDEF999("VTDEF999", "Error no catalogado.");

	/** The code error. */
	private String codeError;

	/** The msg error. */
	private String msgError;

	/**
	 * Instantiates a new e catalogo error.
	 *
	 * @param pStrCode    the str code
	 * @param pStrMessage the str message
	 */
	ECatalogoError(String pStrCode, String pStrMessage) {
		this.codeError = pStrCode;
		this.msgError = pStrMessage;
	}

	/**
	 * Gets the code error.
	 *
	 * @return the code error
	 */
	public String getCodeError() {
		return codeError;
	}

	/**
	 * Gets the msg error.
	 *
	 * @return the msg error
	 */
	public String getMsgError() {
		return msgError;
	}
}
