/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.ktes.KtesGlobal.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		10/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ktes;

/**
 * The Class KtesGlobal.
 */
public final class KtesGlobal {

	/** The Constant GFI. */
	public static final String GFI = "GFI";

	/** The Constant TRPLUS. */
	public static final String TRPLUS = "TRPLUS";

	/** The Constant MONPLUS. */
	public static final String MONPLUS = "MONPLUS";

	/** The Constant MAYOR. */
	public static final String MAYOR = "@MAYOR";

	/** The Constant MENOR. */
	public static final String MENOR = "@MENOR";

	/** The Constant INICIO. */
	public static final String INICIO = "INICIO";

	/** The Constant CIERRE. */
	public static final String CIERRE = "CIERRE";

	/** The Constant MONITOREO. */
	public static final String MONITOREO = "MONITOREO";
	
	/** The Constant PROGRAMADO. */
	public static final String PROGRAMADO = "PROGRAMADO";
	
	/** The Constant USER_DEF. */
	public static final String USER_DEF = "IAMIRONMAN";

	/** The Constant SHOW_QUERY. */
	public static final String SHOW_QUERY = "Descripcion [%s] Producto => Query => [%s || %s]";

	/** The Constant GET_INSTANCIA. */
	public static final String GET_INSTANCIA = "com.ibm.websphere.servlet.application.host";

	/** The Constant HOSTNAME. */
	public static final String HOSTNAME = "localhost";
	
	/** The Constant PUNTO. */
	public static final String PUNTO = ".";

	/** The Constant USUARIO_DEFAULT. */
	public static final String USUARIO_DEFAULT = "CSA";

	/** The Constant SERVICIO_WEB. */
	public static final String SERVICIO_WEB = "ValTransferService";

	/** The Constant SERVICIO. */
	public static final String SERVICIO = "TRAN_VALINI";

	/** The Constant SELECT. */
	public static final String SELECT = "SELECT";

	/** The Constant FROM. */
	public static final String FROM = "FROM";

	/** The Constant N_A. */
	public static final String N_A = "N/A";

	/** The Constant COMENTARIOS. */
	public static final String COMENTARIOS = "Ejecutando query de monitoreo Id: [%s] ";

	/** The Constant QUERY_INHABIL_DAY. */
	public static final String QUERY_INHABIL_DAY = "SELECT CASE WHEN COUNT(*) > 0 THEN 'KO' ELSE 'OK' END AS VALIDACION FROM comu_dia_inhabil WHERE fecha = TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') AND cve_pais LIKE 'MEX%'";

	/** The Constant QUERY_TRANVAL. */
	public static final String QUERY_TRANVAL = "select orden, Producto, Query_Princ, Desc_Query, Ejecucion, Grupo_Ejec, Hora_Ejec from ?.tran_valini_config WHERE activo = 1 order by 6,2,1,5";

	/**
	 * Instantiates a new ktes global.
	 */
	private KtesGlobal() {
		throw new UnsupportedOperationException("No instanciar " + this.getClass().getCanonicalName());
	}
}
