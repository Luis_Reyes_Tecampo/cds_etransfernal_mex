/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.BitacoraOperacionDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class BitacoraOperacionDTO.
 */
public class BitacoraOperacionDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -648008930453598303L;

	/** The usuario. */
	private String usuario;

	/** The cod oper. */
	private String codOper;

	/** The tipo oper. */
	private String tipoOper;

	/** The serv tran. */
	private String servTran;

	/** The comentarios. */
	private String comentarios;

	/**
	 * Gets the usuario.
	 *
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Sets the usuario.
	 *
	 * @param usuario the new usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Gets the cod oper.
	 *
	 * @return the cod oper
	 */
	public String getCodOper() {
		return codOper;
	}

	/**
	 * Sets the cod oper.
	 *
	 * @param codOper the new cod oper
	 */
	public void setCodOper(String codOper) {
		this.codOper = codOper;
	}

	/**
	 * Gets the tipo oper.
	 *
	 * @return the tipo oper
	 */
	public String getTipoOper() {
		return tipoOper;
	}

	/**
	 * Sets the tipo oper.
	 *
	 * @param tipoOper the new tipo oper
	 */
	public void setTipoOper(String tipoOper) {
		this.tipoOper = tipoOper;
	}

	/**
	 * Gets the serv tran.
	 *
	 * @return the serv tran
	 */
	public String getServTran() {
		return servTran;
	}

	/**
	 * Sets the serv tran.
	 *
	 * @param servTran the new serv tran
	 */
	public void setServTran(String servTran) {
		this.servTran = servTran;
	}

	/**
	 * Gets the comentarios.
	 *
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Sets the comentarios.
	 *
	 * @param comentarios the new comentarios
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
}
