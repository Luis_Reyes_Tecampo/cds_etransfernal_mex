/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ReqValidacionDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class ReqValidacionDTO.
 */
public class ReqValidacionDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2340096807094487583L;

	/** The validacion. */
	private String validacion;

	/** The modulo. */
	private String modulo;

	/** The usuario. */
	private String usuario;

	/**
	 * Gets the validacion.
	 *
	 * @return the validacion
	 */
	public String getValidacion() {
		return validacion;
	}

	/**
	 * Sets the validacion.
	 *
	 * @param validacion the new validacion
	 */
	public void setValidacion(String validacion) {
		this.validacion = validacion;
	}

	/**
	 * Gets the modulo.
	 *
	 * @return the modulo
	 */
	public String getModulo() {
		return modulo;
	}

	/**
	 * Sets the modulo.
	 *
	 * @param modulo the new modulo
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	/**
	 * Gets the usuario.
	 *
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Sets the usuario.
	 *
	 * @param usuario the new usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
