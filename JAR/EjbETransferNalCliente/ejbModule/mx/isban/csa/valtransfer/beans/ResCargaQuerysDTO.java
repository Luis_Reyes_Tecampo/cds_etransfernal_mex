/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ResCargaQuerysDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ResCargaQuerysDTO.
 */
public class ResCargaQuerysDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9177048266664677728L;

	/** The code error. */
	private String codeErrCarga;

	/** The msg error. */
	private String msgErrCarga;

	/** The tiempo ejecucion. */
	private String tiempoEjecucion;

	/** The querys. */
	private List<QuerysListDTO> querys;

	/**
	 * Gets the code error.
	 *
	 * @return the code error
	 */
	public String getCodeError() {
		return codeErrCarga;
	}

	/**
	 * Sets the code error.
	 *
	 * @param codeError the new code error
	 */
	public void setCodeError(String codeError) {
		this.codeErrCarga = codeError;
	}

	/**
	 * Gets the msg error.
	 *
	 * @return the msg error
	 */
	public String getMsgError() {
		return msgErrCarga;
	}

	/**
	 * Sets the msg error.
	 *
	 * @param msgError the new msg error
	 */
	public void setMsgError(String msgError) {
		this.msgErrCarga = msgError;
	}

	/**
	 * Gets the tiempo ejecucion.
	 *
	 * @return the tiempo ejecucion
	 */
	public String getTiempoEjecucion() {
		return tiempoEjecucion;
	}

	/**
	 * Sets the tiempo ejecucion.
	 *
	 * @param tiempoEjecucion the new tiempo ejecucion
	 */
	public void setTiempoEjecucion(String tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}

	/**
	 * Gets the querys list.
	 *
	 * @return the querys list
	 */
	public List<QuerysListDTO> getQuerysList() {
		if (this.querys != null) {
			return new ArrayList<QuerysListDTO>(querys);
		} else {
			return new ArrayList<QuerysListDTO>();
		}
	}

	/**
	 * Sets the querys list.
	 *
	 * @param querysList the new querys list
	 */
	public void setQuerysList(List<QuerysListDTO> querysList) {
		if (querysList != null) {
			this.querys = new ArrayList<QuerysListDTO>(querysList);
		} else {
			this.querys = new ArrayList<QuerysListDTO>();
		}
	}

}
