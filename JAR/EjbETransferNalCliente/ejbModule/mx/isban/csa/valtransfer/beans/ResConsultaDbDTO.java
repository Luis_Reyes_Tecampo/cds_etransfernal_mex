/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ResConsultaDbDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class ResConsultaDbDTO.
 */
public class ResConsultaDbDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9177048266664677728L;

	/** The code err consulta. */
	private String codeErrConsulta;

	/** The msg err consulta. */
	private String msgErrConsulta;

	/** The tiempo ejecucion. */
	private String tiempoEjecucion;

	/** The validacion. */
	private String validacion;

	/** The paginas. */
	private String paginas;
	
	
	/**
	 * Gets the code err consulta.
	 *
	 * @return the code err consulta
	 */
	public String getCodeErrConsulta() {
		return codeErrConsulta;
	}

	/**
	 * Sets the code err consulta.
	 *
	 * @param codeErrConsulta the new code err consulta
	 */
	public void setCodeErrConsulta(String codeErrConsulta) {
		this.codeErrConsulta = codeErrConsulta;
	}

	/**
	 * Gets the msg err consulta.
	 *
	 * @return the msg err consulta
	 */
	public String getMsgErrConsulta() {
		return msgErrConsulta;
	}

	/**
	 * Sets the msg err consulta.
	 *
	 * @param msgErrConsulta the new msg err consulta
	 */
	public void setMsgErrConsulta(String msgErrConsulta) {
		this.msgErrConsulta = msgErrConsulta;
	}

	/**
	 * Gets the tiempo ejecucion.
	 *
	 * @return the tiempo ejecucion
	 */
	public String getTiempoEjecucion() {
		return tiempoEjecucion;
	}

	/**
	 * Sets the tiempo ejecucion.
	 *
	 * @param tiempoEjecucion the new tiempo ejecucion
	 */
	public void setTiempoEjecucion(String tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}

	/**
	 * Gets the validacion.
	 *
	 * @return the validacion
	 */
	public String getValidacion() {
		return validacion;
	}

	/**
	 * Sets the validacion.
	 *
	 * @param validacion the new validacion
	 */
	public void setValidacion(String validacion) {
		this.validacion = validacion;
	}

	/**
	 * Gets the paginas.
	 *
	 * @return the paginas
	 */
	public String getPaginas() {
		return paginas;
	}

	/**
	 * Sets the paginas.
	 *
	 * @param paginas the new paginas
	 */
	public void setPaginas(String paginas) {
		this.paginas = paginas;
	}

}
