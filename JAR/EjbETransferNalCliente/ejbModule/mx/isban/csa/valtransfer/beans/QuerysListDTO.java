/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.QuerysListDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class QuerysListDTO.
 */
public class QuerysListDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8551194306560291736L;

	/** The detalle. */
	private QuerysDetDTO detalle;

	/** The producto. */
	private String producto;

	/** The query. */
	private String query;

	/** The desc query. */
	private String descQuery;

	/**
	 * Gets the detalle.
	 *
	 * @return the detalle
	 */
	public QuerysDetDTO getDetalle() {
		return detalle;
	}

	/**
	 * Sets the detalle.
	 *
	 * @param detalle the new detalle
	 */
	public void setDetalle(QuerysDetDTO detalle) {
		this.detalle = detalle;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query.
	 *
	 * @param query the new query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the producto.
	 *
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * Sets the producto.
	 *
	 * @param producto the new producto
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * Gets the desc query.
	 *
	 * @return the desc query
	 */
	public String getDescQuery() {
		return descQuery;
	}

	/**
	 * Sets the desc query.
	 *
	 * @param descQuery the new desc query
	 */
	public void setDescQuery(String descQuery) {
		this.descQuery = descQuery;
	}
}
