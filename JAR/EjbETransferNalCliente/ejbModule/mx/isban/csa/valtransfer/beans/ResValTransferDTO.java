/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ResValTransferDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class ResValTransferDTO.
 */
public class ResValTransferDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9177048266664677728L;

	/** The code error. */
	private String codeError;

	/** The msg error. */
	private String msgError;

	/** The tiempo ejecucion. */
	private String tiempoEjecucion;

	/** The validacion. */
	private String validacion;

	/**
	 * Gets the code error.
	 *
	 * @return the code error
	 */
	public String getCodeError() {
		return codeError;
	}

	/**
	 * Sets the code error.
	 *
	 * @param codeError the new code error
	 */
	public void setCodeError(String codeError) {
		this.codeError = codeError;
	}

	/**
	 * Gets the msg error.
	 *
	 * @return the msg error
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Sets the msg error.
	 *
	 * @param msgError the new msg error
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Gets the tiempo ejecucion.
	 *
	 * @return the tiempo ejecucion
	 */
	public String getTiempoEjecucion() {
		return tiempoEjecucion;
	}

	/**
	 * Sets the tiempo ejecucion.
	 *
	 * @param tiempoEjecucion the new tiempo ejecucion
	 */
	public void setTiempoEjecucion(String tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}

	/**
	 * Gets the validacion.
	 *
	 * @return the validacion
	 */
	public String getValidacion() {
		return validacion;
	}

	/**
	 * Sets the validacion.
	 *
	 * @param validacion the new validacion
	 */
	public void setValidacion(String validacion) {
		this.validacion = validacion;
	}
}
