/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.QuerysDetDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class QuerysDetDTO.
 */
public class QuerysDetDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3209007733609232107L;

	/** The grp ejec. */
	private String grpEjec;

	/** The hr ejec. */
	private String hrEjec;

	/** The ejecucion. */
	private String ejecucion;

	/**
	 * Gets the grp ejec.
	 *
	 * @return the grp ejec
	 */
	public String getGrpEjec() {
		return grpEjec;
	}

	/**
	 * Sets the grp ejec.
	 *
	 * @param grpEjec the new grp ejec
	 */
	public void setGrpEjec(String grpEjec) {
		this.grpEjec = grpEjec;
	}

	/**
	 * Gets the hr ejec.
	 *
	 * @return the hr ejec
	 */
	public String getHrEjec() {
		return hrEjec;
	}

	/**
	 * Sets the hr ejec.
	 *
	 * @param hrEjec the new hr ejec
	 */
	public void setHrEjec(String hrEjec) {
		this.hrEjec = hrEjec;
	}

	/**
	 * Gets the ejecucion.
	 *
	 * @return the ejecucion
	 */
	public String getEjecucion() {
		return ejecucion;
	}

	/**
	 * Sets the ejecucion.
	 *
	 * @param ejecucion the new ejecucion
	 */
	public void setEjecucion(String ejecucion) {
		this.ejecucion = ejecucion;
	}
}
