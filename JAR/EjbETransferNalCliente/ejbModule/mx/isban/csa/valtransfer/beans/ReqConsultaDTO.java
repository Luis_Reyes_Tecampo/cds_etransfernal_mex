/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ReqConsultaDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class ReqConsultaDTO.
 */
public class ReqConsultaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6869961880467257418L;

	/** The data base. */
	private String dataBase;

	/** The query. */
	private String query;

	/** The paginacion. */
	private int paginacion;

	/** The user. */
	private String user;

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the data base.
	 *
	 * @return the data base
	 */
	public String getDataBase() {
		return dataBase;
	}

	/**
	 * Sets the data base.
	 *
	 * @param dataBase the new data base
	 */
	public void setDataBase(String dataBase) {
		this.dataBase = dataBase;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query.
	 *
	 * @param query the new query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the paginacion.
	 *
	 * @return the paginacion
	 */
	public int getPaginacion() {
		return paginacion;
	}

	/**
	 * Sets the paginacion.
	 *
	 * @param paginacion the new paginacion
	 */
	public void setPaginacion(int paginacion) {
		this.paginacion = paginacion;
	}
}
