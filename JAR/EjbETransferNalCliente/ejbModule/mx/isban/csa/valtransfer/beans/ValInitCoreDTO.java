/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.ValInitCoreDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ValInitCoreDTO.
 */
public class ValInitCoreDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5982139981552504508L;

	/** The bitacora. */
	private BitacoraDTO bitacora;

	/** The list querys. */
	private List<QuerysListDTO> listQuerys;

	/**
	 * Gets the bitacora.
	 *
	 * @return the bitacora
	 */
	public BitacoraDTO getBitacora() {
		return bitacora;
	}

	/**
	 * Sets the bitacora.
	 *
	 * @param bitacora the new bitacora
	 */
	public void setBitacora(BitacoraDTO bitacora) {
		this.bitacora = bitacora;
	}

	/**
	 * Gets the list querys.
	 *
	 * @return the list querys
	 */
	public List<QuerysListDTO> getListQuerys() {
		//se valida que el obj no sea nulo para retornar una copia 
		if (this.listQuerys != null) {
			return new ArrayList<QuerysListDTO>(this.listQuerys);
		} else {
			return new ArrayList<QuerysListDTO>();
		}
	}

	/**
	 * Sets the list querys.
	 *
	 */
	public void setListQuerys(List<QuerysListDTO> querys) {
		//se valida que el parametro que se va asignar no sea nulo esto para generar una copia 
		if(querys != null) {
			this.listQuerys = new ArrayList<QuerysListDTO>(querys);
		} else {
			this.listQuerys = new ArrayList<QuerysListDTO>();
		}
	}
}
