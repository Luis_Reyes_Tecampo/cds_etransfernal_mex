/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.BitacoraTblAfectadaDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class BitacoraTblAfectadaDTO.
 */
public class BitacoraTblAfectadaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5459387884290361826L;

	/** The tabla afect. */
	private String tablaAfect;

	/** The val ant. */
	private String valAnt;

	/** The val nvo. */
	private String valNvo;

	/** The dato modi. */
	private String datoModi;

	/** The dato fijo. */
	private String datoFijo;

	/**
	 * Gets the tabla afect.
	 *
	 * @return the tabla afect
	 */
	public String getTablaAfect() {
		return tablaAfect;
	}

	/**
	 * Sets the tabla afect.
	 *
	 * @param tablaAfect the new tabla afect
	 */
	public void setTablaAfect(String tablaAfect) {
		this.tablaAfect = tablaAfect;
	}

	/**
	 * Gets the val ant.
	 *
	 * @return the val ant
	 */
	public String getValAnt() {
		return valAnt;
	}

	/**
	 * Sets the val ant.
	 *
	 * @param valAnt the new val ant
	 */
	public void setValAnt(String valAnt) {
		this.valAnt = valAnt;
	}

	/**
	 * Gets the val nvo.
	 *
	 * @return the val nvo
	 */
	public String getValNvo() {
		return valNvo;
	}

	/**
	 * Sets the val nvo.
	 *
	 * @param valNvo the new val nvo
	 */
	public void setValNvo(String valNvo) {
		this.valNvo = valNvo;
	}

	/**
	 * Gets the dato modi.
	 *
	 * @return the dato modi
	 */
	public String getDatoModi() {
		return datoModi;
	}

	/**
	 * Sets the dato modi.
	 *
	 * @param datoModi the new dato modi
	 */
	public void setDatoModi(String datoModi) {
		this.datoModi = datoModi;
	}

	/**
	 * Gets the dato fijo.
	 *
	 * @return the dato fijo
	 */
	public String getDatoFijo() {
		return datoFijo;
	}

	/**
	 * Sets the dato fijo.
	 *
	 * @param datoFijo the new dato fijo
	 */
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}
}
