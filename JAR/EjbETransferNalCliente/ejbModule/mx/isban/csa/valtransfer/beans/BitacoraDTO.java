/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.beans.BitacoraDTO.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		6/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.beans;

import java.io.Serializable;

/**
 * The Class BitacoraDTO.
 */
public class BitacoraDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1680478385380335482L;

	/** The inst web. */
	private String instWeb;

	/** The ip client. */
	private String ipClient;

	/** The hostname. */
	private String hostname;

	/** The operacion. */
	private BitacoraOperacionDTO operacion;

	/** The tabla afecatada. */
	private BitacoraTblAfectadaDTO tablaAfecatada;

	/**
	 * Gets the operacion.
	 *
	 * @return the operacion
	 */
	public BitacoraOperacionDTO getOperacion() {
		return operacion;
	}

	/**
	 * Sets the operacion.
	 *
	 * @param operacion the new operacion
	 */
	public void setOperacion(BitacoraOperacionDTO operacion) {
		this.operacion = operacion;
	}

	/**
	 * Gets the tabla afecatada.
	 *
	 * @return the tabla afecatada
	 */
	public BitacoraTblAfectadaDTO getTablaAfecatada() {
		return tablaAfecatada;
	}

	/**
	 * Sets the tabla afecatada.
	 *
	 * @param tablaAfecatada the new tabla afecatada
	 */
	public void setTablaAfecatada(BitacoraTblAfectadaDTO tablaAfecatada) {
		this.tablaAfecatada = tablaAfecatada;
	}

	/**
	 * Gets the inst web.
	 *
	 * @return the inst web
	 */
	public String getInstWeb() {
		return instWeb;
	}

	/**
	 * Sets the inst web.
	 *
	 * @param instWeb the new inst web
	 */
	public void setInstWeb(String instWeb) {
		this.instWeb = instWeb;
	}

	/**
	 * Gets the ip client.
	 *
	 * @return the ip client
	 */
	public String getIpClient() {
		return ipClient;
	}

	/**
	 * Sets the ip client.
	 *
	 * @param ipClient the new ip client
	 */
	public void setIpClient(String ipClient) {
		this.ipClient = ipClient;
	}

	/**
	 * Gets the hostname.
	 *
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * Sets the hostname.
	 *
	 * @param hostname the new hostname
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}
