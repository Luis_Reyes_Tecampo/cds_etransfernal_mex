/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.ejb.BOValidacion.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		8/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;

/**
 * The Interface BOValidacion.
 */
public interface BOValidacion {

	/**
	 * Gets the inicio transfer.
	 *
	 * @param asb        the asb
	 * @param validacion the validacion
	 * @param bitacora   the bitacora
	 * @return the inicio transfer
	 * @throws BusinessException the business exception
	 */
	String getInicioTransfer(ArchitechSessionBean asb, ReqValidacionDTO validacion,
			BitacoraDTO bitacora) throws BusinessException;

	/**
	 * Gets the cierre transfer.
	 *
	 * @param asb        the asb
	 * @param validacion the validacion
	 * @param bitacora   the bitacora
	 * @return the cierre transfer
	 * @throws BusinessException the business exception
	 */
	String getCierreTransfer(ArchitechSessionBean asb, ReqValidacionDTO validacion,
			BitacoraDTO bitacora) throws BusinessException;

	/**
	 * Gets the monitoreo.
	 *
	 * @param asb the asb
	 * @param validacion the validacion
	 * @param bitacora the bitacora
	 * @return the monitoreo
	 * @throws BusinessException the business exception
	 */
	String getMonitoreo(ArchitechSessionBean asb, ReqValidacionDTO validacion, BitacoraDTO bitacora)
			throws BusinessException;
}
