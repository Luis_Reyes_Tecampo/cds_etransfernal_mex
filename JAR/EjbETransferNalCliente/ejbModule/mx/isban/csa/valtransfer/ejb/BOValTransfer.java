/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * [EjbETransferNalCliente]
 * mx.isban.csa.valtransfer.ejb.BOValTransfer.java
 *
 * Control de versiones:
 *
 * Version 	Date/Hour 		By 									FSW			Description
 * ------- 	--------------	-------------------------------		--------	---------------------------------------------------------
 * 1.0 		10/07/2020 		Ing. Juan Manuel Fuentes Ramos		CSA			Creacion de archivo
 */
package mx.isban.csa.valtransfer.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.csa.valtransfer.beans.BitacoraDTO;
import mx.isban.csa.valtransfer.beans.ReqConsultaDTO;
import mx.isban.csa.valtransfer.beans.ReqValidacionDTO;
import mx.isban.csa.valtransfer.beans.ResConsultaDbDTO;
import mx.isban.csa.valtransfer.beans.ResValTransferDTO;

/**
 * The Interface BOValTransfer.
 */
public interface BOValTransfer {

	/**
	 * Monitor.
	 *
	 * @param asb         the asb
	 * @param reqTransfer the req transfer
	 * @param ipRemota    the ip remota
	 * @return the res val transfer DTO
	 * @throws BusinessException the business exception
	 */
	ResValTransferDTO monitor(ArchitechSessionBean asb, ReqValidacionDTO reqTransfer, String ipRemota)
			throws BusinessException;

	/**
	 * Consulta.
	 *
	 * @param asb         the asb
	 * @param reqTransfer the req transfer
	 * @param ipRemota    the ip remota
	 * @return the res consulta db DTO
	 * @throws BusinessException the business exception
	 */
	ResConsultaDbDTO consulta(ArchitechSessionBean asb, ReqConsultaDTO reqTransfer, String ipRemota)
			throws BusinessException;

	/**
	 * Sets the bitacora single.
	 *
	 * @param asb      the asb
	 * @param bitacora the bitacora
	 */
	void setBitacoraSingle(ArchitechSessionBean asb, BitacoraDTO bitacora, String esquema);

	/**
	 * Carga querys bd.
	 *
	 * @param architechBean the architech bean
	 * @param ipRemota the ip remota
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String cargaQuerysBd(ArchitechSessionBean architechBean, String ipRemota) throws BusinessException;

	/**
	 * Consulta programados.
	 *
	 * @param architechBean the architech bean
	 * @return the res val transfer DTO
	 */
	ResValTransferDTO consultaProgramados(ArchitechSessionBean architechBean);
}
