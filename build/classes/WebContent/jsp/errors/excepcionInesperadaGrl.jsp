<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../private/myHeader.jsp" flush="true"/>
<jsp:include page="../private/myMenu.jsp" flush="true">
	<jsp:param name="menuItem"    value="principal" />
</jsp:include>

<spring:message code="codError.EC00011B" var="EC00011B"/>		
	
		<div class="pageTitleContainer">
			<span class="pageTitle">Error</span> - ${codeError}
		</div>
		${EC00011B}
		<!-- 
		${exception.message}
		-->
<jsp:include page="../private/myFooter.jsp" flush="true"/>


