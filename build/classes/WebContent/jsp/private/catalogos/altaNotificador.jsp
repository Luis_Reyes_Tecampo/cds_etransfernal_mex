<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="notificador" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/notificador.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<%--Inicio de llamado de datos de Properties--%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="general.nombreAplicacion" 						var="app"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.eliminar" 								var="lblEliminar"/>
<spring:message code="general.guardar" 									var="lblGuardar"/>
<spring:message code="general.cancelar" 								var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.asteriscoHelper"                          var="asteriscoHelper" />
<spring:message code="general.dosPuntosHelper"                          var="DosPuntosHelper" />
<spring:message code="catalogos.certificadosCifrado.alta" 				var="alta"/>
<spring:message code="catalogos.certificadosCifrado.modificar" 			var="modificar"/>
<spring:message code="catalogos.notificador.titulo"var="tituloFuncionalidad" />
<spring:message code="catalogos.notificador.lblAgregarView"var="lblAgregarView" />
<spring:message code="catalogos.notificador.lblEditarView"var="lblEditarView" />
<spring:message code="catalogos.notificador.lblTipoNot" var="lblTipoNot"/>
<spring:message code="catalogos.notificador.lblClaveTrans" var="lblClaveTrans"/>
<spring:message code="catalogos.notificador.lblMedioEntrega" var="lblMedioEntrega"/>
<spring:message code="catalogos.notificador.lblEstatus" var="lblEstatus"/>
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na" var="na" />
<spring:message code="catalogos.notificador.lblTipoNotDesc" var="lblTipoNotDesc"/>
<spring:message code="catalogos.notificador.lblClaveTransDesc" var="lblClaveTransDesc"/>
<spring:message code="catalogos.notificador.lblMedioEntregaDesc" var="lblMedioEntregaDesc"/>
<spring:message code="catalogos.notificador.lblEstatusDesc" var="lblEstatusDesc"/>
<%-- Fin de Variables Constantes --%>

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00130V" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',"ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}'  };</script>	
 <form id="mform" method="post">
 	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
 	<input type="hidden" name="accion" id="accion" value="${accion}">
 	<input type="hidden" name="canal" id="canal" value="${beanNotificacion.tipoNotif}" ${accion eq 'alta' ? 'disabled="disabled"' : ''}>
	<div class="pageTitleContainer">
	 <c:choose>
	  <c:when test="${accion eq 'alta'}">
	  <span class="pageTitle">${tituloModulo}</span> - ${lblAgregarView} ${tituloFuncionalidad}
	  </c:when>
	  <c:otherwise>
	  <span class="pageTitle">${tituloModulo}</span> - ${lblEditarView} ${tituloFuncionalidad}
	  </c:otherwise>
	 </c:choose>
		
	</div>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblTipoNot}${DosPuntosHelper}</td>
					<td>
					<label for="paramTipoNot"></label>
					<select class="Campos comboDes required objFrm focusCombo comboWidth"
						id="paramTipoNot" name="tipoNotif">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${combos.notificadores}">
								<option value="${reg.cve}"
									${reg.cve eq beanNotificacion.tipoNotif?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select> <c:if test="${accion eq 'modificar'}">
					<label for="tipoNotifOld"></label>
							<input type="hidden" name="tipoNotifOld"
								value="${beanNotificacion.tipoNotif}" />
						</c:if></td>
				</tr>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${lblClaveTrans}${DosPuntosHelper}</td>
					<td>
					<label for="paramClaveTrans"></label>
					<select class="Campos comboDes required objFrm focusCombo comboWidth"
						id="paramClaveTrans" name="cveTransfer">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${combos.trasferencias}">
								<option value="${reg.cve}"
									${reg.cve eq beanNotificacion.cveTransfer?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select> <c:if test="${accion eq 'modificar'}">
							<label for="cveTransferOld"></label>
							<input type="hidden" name="cveTransferOld"
								value="${beanNotificacion.cveTransfer}" />
						</c:if></td>
				</tr>
				<%@ include file="altaNotificadorAux.jsp" %>
			</table>			
	    </div> 
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		        <caption></caption>
		             <tr>
		                 <td class="izq">
		                     <a id="btnGuardar" href="javascript:;" >${lblGuardar}</a>
		                 </td>
		                 <td class="odd">${espacioEnBlanco}</td>
		                 <td class="der">
		                     <a id="btnReturn" href="javascript:;" >${lblRegresar}</a>
		                 </td>
		             </tr>
		        </table>
		    </div>
		</div>
	</div>
	<input type="hidden" id="tipoNotifF" name="tipoNotifF" class="filField"value="${beanFilter.tipoNotif}">
	<input type="hidden" id="cveTransferF" name="cveTransferF" class="filField" value="${beanFilter.cveTransfer}">
	<input type="hidden" id="medioEntregaF" name="medioEntregaF" class="filField" value="${beanFilter.medioEntrega}">
	<input type="hidden" id="estatusTransferF" name="estatusTransferF" class="filField" value="${beanFilter.estatusTransfer}">
	<input type="hidden" name="pagina" id="pagina" value="${beanPaginador.pagina}" /> 
	<input type="hidden" name="paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}" /> 
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}" />
</form>

<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script></c:if>