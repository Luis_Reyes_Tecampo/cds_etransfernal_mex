<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuSubCatalagos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatIntFinancieros" />
</jsp:include>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.cveInterme" var="cveInterme"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoInterme" var="tipoInterme"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numCecoban" var="numCecoban"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreCorto" var="nombreCorto"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreLargo" var="nombreLargo"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numPersona" var="numPersona"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchAlta" var="fchAlta"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchBaja" var="fchBaja"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchUltModif" var="fchUltModif"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioModif" var="usuarioModif"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioRegistro" var="usuarioRegistro"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.status" var="status"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numBanxico" var="numBanxico"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numIndeval" var="numIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.idIntIndeval" var="idIntIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.folIntIndeval" var="folIntIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.cveSwiftCor" var="cveSwiftCor"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloOperacion" var="tituloOperacion"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloOperacionEdit" var="tituloOperacionEdit"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.validar" var="validar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.limpiar" var="limpiar"/>

<spring:message code="catalogos.consultaCatIntFinancieros.link.editar" var="actualizar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.alta" var="aceptar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.aceptarAlta" var="aceptarAlta"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.cancelar" var="cancelar"/>

<spring:message code="general.inicio" var="inicio" />
<spring:message code="general.fin" var="fin" />

<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.ED00022V" var="ED00022V" />
<spring:message code="codError.ED00023V" var="ED00023V" />
<spring:message code="codError.ED00027V" var="ED00027V" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.ED00086V" var="ED00086V"/>
	 
<script type="text/javascript">
var mensajes = {"aplicacion": '${aplicacion}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',
                        "ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"CD00010V":'${CD00010V}'};
</script>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/lf/default/css/dialogBox/jquery.alerts.css" />


<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>  
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/altaCatIntFinancieros.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>	

<c:set var="searchString" value="${seguTareas}" />

<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>

<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="paginador.accion" id="accion" value=""/> 
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsMovCDA.beanPaginador.paginaIni}"/> 
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsMovCDA.beanPaginador.pagina}"/> 
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsMovCDA.beanPaginador.paginaFin}"/>	
	<input type="hidden" name="cveInterme" id="cveInterme" value="${beanResIntFinancieros.cveInterme}"/>
	<input type="hidden" name="tipoInterme" id="tipoInterme" value="${beanResIntFinancieros.tipoInterme}"/>
	<input type="hidden" name="numCecoban" id="numCecoban" value="${beanResIntFinancieros.numCecoban}"/>
	<input type="hidden" name="nombreCorto" id="nombreCorto" value="${beanResIntFinancieros.nombreCorto}"/>
	<input type="hidden" name="nombreLargo" id="nombreLargo" value="${beanResIntFinancieros.nombreLargo}"/>
	<input type="hidden" name="numPersona" id="numPersona" value="${beanResIntFinancieros.numPersona}"/>
	<input type="hidden" name="status" id="status" value="${beanResIntFinancieros.status}"/>
	<input type="hidden" name="numBanxico" id="numBanxico" value="${beanResIntFinancieros.numBanxico}"/>
	<input type="hidden" name="numIndeval" id="numIndeval" value="${beanResIntFinancieros.numIndeval}"/>
	<input type="hidden" name="idIntIndeval" id="idIntIndeval" value="${beanResIntFinancieros.idIntIndeval}"/>
	<input type="hidden" name="folIntIndeval" id="folIntIndeval" value="${beanResIntFinancieros.folIntIndeval}"/>
	<input type="hidden" name="cveSwiftCor" id="cveSwiftCor" value="${beanResIntFinancieros.cveSwiftCor}"/>
	<input type="hidden" name="fchBaja" id="fchBaja" value="${beanResIntFinancieros.fchBaja}"/>
	
	<input id="checkSeleccionados" name="checkSeleccionados" type="hidden" value="0" />

<div class="frameBuscadorSimple">
<div class="titleBuscadorSimple">
	<c:choose>
		<c:when test="${beanResTiposIntFin.totalReg > 0}">
			${tituloOperacionEdit}
		</c:when>
		<c:otherwise>
			${tituloOperacion}
		</c:otherwise>
	</c:choose>
</div>
<div class="contentBuscadorSimple">
<table>
	<tr>
		<td  class="text_izquierda">${cveInterme}</td>
		<td><input type="text" class="Campos" id="paramCveInterme" name="paramCveInterme"  size="20" maxlength="5"	${beanResTiposIntFin.totalReg > 0 ? 'disabled="disabled"' : ''} value="${beanResTiposIntFin.cveInterme}"/></td>
		<td  class="text_izquierda">${numIndeval}</td>
		<td><input type="text" class="Campos" id="paramNumIndeval" name="paramNumIndeval"  size="20" maxlength="12"  value="${beanResTiposIntFin.numIndeval}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${tipoInterme}</td>
		<td>
			<select name="select" class="Campos" id="selectTipoInterme" >
				<option value="">${seleccionarOpcion}</option>
				<c:forEach var="tipoIntFin" items="${beanResTiposIntFin.listTiposInterviniente}" varStatus="rowCounter">
					<c:choose>
						<c:when test="${tipoIntFin == beanResTiposIntFin.tipoInterme}">
							<option value="${tipoIntFin}" selected="selected">${tipoIntFin}</option>
						</c:when>
						<c:otherwise>
							<option value="${tipoIntFin}">${tipoIntFin}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</td>
		<td  class="text_izquierda">${idIntIndeval}</td>
		<td><input type="text" class="Campos" id="paramIdIntIndeval" name="paramIdIntIndeval"  size="20" maxlength="7"  value="${beanResTiposIntFin.idIntIndeval}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${numCecoban}</td>
		<td><input type="text" class="Campos" id="paramNumCecoban" name="paramNumCecoban"  size="20" maxlength="7"  value="${beanResTiposIntFin.numCecoban}"/></td>
		<td  class="text_izquierda">${folIntIndeval}</td>
		<td><input type="text" class="Campos" id="paramFolIntIndeval" name="paramFolIntIndeval"  size="20" maxlength="7"  value="${beanResTiposIntFin.folIntIndeval}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${nombreCorto}</td>
		<td><input type="text" class="Campos" id="paramNombreCorto" name="paramNombreCorto"  size="20" maxlength="16"  value="${beanResTiposIntFin.nombreCorto}"/></td>
		<td  class="text_izquierda">${cveSwiftCor}</td>
		<td><input type="text" class="Campos" id="paramCveSwiftCor" name="paramCveSwiftCor"  size="20" maxlength="12"  value="${beanResTiposIntFin.cveSwiftCor}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${nombreLargo}</td>
		<td colspan=""><input type="text" class="Campos" id="paramNombreLargo" name="paramNombreLargo"  size="30" maxlength="40"  value="${beanResTiposIntFin.nombreLargo}"/></td>
		
		<td  class="text_izquierda"><label for="paramFchBaja">${fchBaja}</label></td>		
		<td>
		<input name="paramFchBaja" type="text" class="Campos" id="paramFchBaja" size="20" value="${beanResTiposIntFin.fchBaja}"/>
		<img id="cal2" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
		</td>
	</tr>
	<tr>
		<td  class="text_izquierda">${numPersona}</td>
		<td><input type="text" class="Campos" id="paramNumPersona" name="paramNumPersona"  size="20" maxlength="7"  value="${beanResTiposIntFin.numPersona}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${status}</td>
		<td><input type="text" class="Campos" id="paramStatus" name="paramStatus"  size="20" maxlength="4"  value="${beanResTiposIntFin.status}"/></td>
	</tr>
	<tr>
		<td  class="text_izquierda">${numBanxico}</td>
		<td><input type="text" class="Campos" id="paramNumBanxico" name="paramNumBanxico"  size="20" maxlength="7"  value="${beanResTiposIntFin.numBanxico}"/></td>
	</tr>
	
	<tr>
		<td class="text_centro">
			<span><a id="idLimpiar" href="javascript:;">${limpiar}</a></span>
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</div>
</div>

<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,\"AGREGAR\") && beanResTiposIntFin.totalReg == 0}">
				<td width="279" class="izq"><a id="idAceptar" href="javascript:;">${aceptar}</a></td>
			</c:when>
			<c:otherwise>
				<td  width="279" class="izq_Des"><a href="javascript:;">${aceptar}</a></td>
			</c:otherwise>
		</c:choose>
		<td width="279" class="der"><a id="idCancelar" href="javascript:;">${cancelar}</a></td>
		</tr>
		<tr>
			<c:choose>
				<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') && 
					beanResTiposIntFin.totalReg > 0}">
					<td width="279" class="izq">
						<a id="idEditar" href="javascript:;">
							<c:choose>
								<c:when test="${empty beanResTiposIntFin.fchBaja}">
									${actualizar}
								</c:when>
								<c:otherwise>
									${aceptarAlta}
								</c:otherwise>
							</c:choose>
						</a>
					</td>
				</c:when>
				<c:otherwise>
					<td width="279" class="izq_Des">
						<a href="javascript:;">
							<c:choose>
								<c:when test="${empty beanResTiposIntFin.fchBaja}">
									${actualizar}
								</c:when>
								<c:otherwise>
									${aceptarAlta}
								</c:otherwise>
							</c:choose>
						</a>
					</td>
				</c:otherwise>
			</c:choose>
				
		</tr>
</table>

</div>
</div>
</form>
<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">${e:forHtml(tipoError)}('${e:forHtml(descError)}','${e:forHtml(aplicacion)}','${e:forHtml(codError)}','');</script>
</c:if>

<c:if test="${beanResTiposIntFin.totalReg > 0}">
	<script type="text/javascript">
		$('#checkSeleccionados').val("1");
	</script>
</c:if>