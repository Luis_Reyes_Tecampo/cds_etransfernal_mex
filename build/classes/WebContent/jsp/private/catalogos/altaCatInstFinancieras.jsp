<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatInstFinancieras" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="catalogos.altaCatIntsFinancieras.link.aceptar" var="aceptar"/>
<spring:message code="catalogos.altaCatIntsFinancieras.link.cancelar" var="cancelar"/>

<spring:message code="general.inicio" var="inicio" />
<spring:message code="general.fin" var="fin" />


<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="codError.ED00022V" var="ED00022V" />
<spring:message code="codError.ED00023V" var="ED00023V" />
<spring:message code="codError.ED00027V" var="ED00027V" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.ED00086V" var="ED00086V"/>

	 
<script type="text/javascript">
var mensajes = {"aplicacion": '${aplicacion}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',
                        "ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"CD00010V":'${CD00010V}'};

</script>


<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/altaCatInstFinancieras.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}" />


<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>

<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="paginador.accion" id="accion" value=""/> 
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsMovCDA.beanPaginador.paginaIni}"/> 
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsMovCDA.beanPaginador.pagina}"/> 
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsMovCDA.beanPaginador.paginaFin}"/>
	<input type="hidden" name="numBanxico" id="numBanxico" value=""/>
	<input type="hidden" name="cveInterme" id="cveInterme" value=""/>
	<input type="hidden" name="nombre" id="nombre" value=""/>

<div class="frameBuscadorSimple">
<div class="titleBuscadorSimple">Alta Inst. Financieras</div>
<div class="contentBuscadorSimple">
<table>
	<tr>
		<td class="text_izquierda">Numero Banxico</td>
		

		
		<td>
		
			<select name="select" class="Campos" id="selectInstitucion" >
				<option value="">${seleccionarOpcion}</option>
				<c:forEach var="beanInstitucion" items="${beanResInst.listBeanInstitucion}" varStatus="rowCounter">
				<option value="${beanInstitucion.numBanxico}-${beanInstitucion.cveInterme}-${beanInstitucion.nombre}">
					${beanInstitucion.numBanxico}-${beanInstitucion.cveInterme}-${beanInstitucion.nombre}</option>
				</c:forEach>
			</select>
		</td>
	</tr>

	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</div>
</div>

<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>

		<td width="279" class="izq"><a id="idAceptar" href="javascript:;">${aceptar}</a></td>
		<td class="odd">${espacioEnBlanco}</td>
		<td width="279" class="der"><a id="idCancelar" href="javascript:;">${cancelar}</a></td>
	</tr>
</table>

</div>
</div>
</form>

<c:if test="${codError!=''}">
	<script type="text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>