<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanBloques.bloques}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanBloques.beanPaginador.regIni} al
						${beanBloques.beanPaginador.regFin} de un total de
						${beanBloques.totalReg} registros</label>
				</div>
				<c:if
					test="${beanBloques.beanPaginador.paginaIni == beanBloques.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaIni != beanBloques.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaAfectacionBloques.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaAnt!='0' && beanBloques.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaAfectacionBloques.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaAnt=='0' || beanBloques.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanBloques.beanPaginador.pagina}
					- ${beanBloques.beanPaginador.paginaFin}</label>
				<c:if
					test="${beanBloques.beanPaginador.paginaFin != beanBloques.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaAfectacionBloques.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaFin == beanBloques.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaFin != beanBloques.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaAfectacionBloques.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanBloques.beanPaginador.paginaFin == beanBloques.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>