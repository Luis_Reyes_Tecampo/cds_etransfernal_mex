<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanResRFC.rfcs}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanResRFC.beanPaginador.regIni} al
						${beanResRFC.beanPaginador.regFin} de un total de
						${beanResRFC.totalReg} registros</label>
				</div>
				<c:if
					test="${beanResRFC.beanPaginador.paginaIni == beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaIni != beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaExcepcionRFC.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaAnt!='0' && beanResRFC.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaExcepcionRFC.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaAnt=='0' || beanResRFC.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanResRFC.beanPaginador.pagina}
					- ${beanResRFC.beanPaginador.paginaFin}</label>
				<c:if
					test="${beanResRFC.beanPaginador.paginaFin != beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaExcepcionRFC.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaFin == beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaFin != beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaExcepcionRFC.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanResRFC.beanPaginador.paginaFin == beanResRFC.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>