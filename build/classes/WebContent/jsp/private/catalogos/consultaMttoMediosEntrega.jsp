<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="mantenimientoMediosEntrega" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mediosEntrega.js" type="text/javascript"></script>

<spring:message code="catalogos.general.text.tituloModulo" 				var="tituloModulo"/>
<spring:message code="catalogos.certificadosCifrado.titulo" 			var="tituloFuncionalidad"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtMantoEntrega" 	var="MantoEntrega"/>

<spring:message code="catalogos.mttomedioentrega.txt.txtcvemedioent"  	var="cme"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtuversion"     	var="version"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtdescripcion"  	var="desc"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcomanualpf"   	var="compf"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcomanualpm"   	var="compm"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtckkseguridad" 	var="seguridad"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcanal"        	var="canal"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtimporteMax"     var="importeMax"/>


<spring:message code="catalogos.mttomedioentrega.nuevo" 				var="nuevo"/>
<spring:message code="catalogos.mttomedioentrega.exportar" 				var="exportar"/>
<spring:message code="catalogos.mttomedioentrega.exportarTodo" 			var="exportarTodo"/>
<spring:message code="catalogos.mttomedioentrega.actualizar" 			var="actualizar"/>
<spring:message code="catalogos.mttomedioentrega.editar" 				var="editar"/>
<spring:message code="catalogos.mttomedioentrega.eliminar" 				var="eliminar"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',
                    "CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};
</script>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${MantoEntrega}
</div>

<c:set var="searchString" value="${seguTareas}"/>

<form id="idForm" name="idForm" method="post" action="">
<!-- 	<input type="hidden" name="cveMedioEntrega" id="cveMedioEntrega" value="" /> -->
	<input type="hidden" name="pagina" 		id="pagina" 	value="${beanMediosEntrega.beanPaginador.pagina}">
	<input type="hidden" name="paginaIni" 	id="paginaIni" 	value="${beanMediosEntrega.beanPaginador.paginaIni}">
    <input type="hidden" name="paginaFin" 	id="paginaFin" 	value="${beanMediosEntrega.beanPaginador.paginaFin}">
    <input type="hidden" name="field" 		id="field" 		value="${beanMediosEntrega.beanFilter.field}">
	<input type="hidden" name="fieldValue" 	id="fieldValue" value="${beanMediosEntrega.beanFilter.fieldValue}">
    <input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="opcion" id="opcion" value="">

    <div class="frameTablaVariasColumnas">
	    <div class="titleTablaVariasColumnas">${MantoEntrega}</div>
		    <div class="contentTablaVariasColumnas">
		        <table class="table-cons table" id="tblConsulta">
		        	<caption></caption>
		            <thead>
		                <tr>
							<th class="text_centro filField" scope="col">
		                    	${cme}<br/><input type="text" id="cveMedioEntregaInput" name="cveMedioEntrega" class="filField" data-field="CVE_MEDIO_ENT" maxlength="6"
		                    		value="${beanMedioEntrega.cveMedioEntrega}">
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${desc}<br/><input type="text" id="descripcionInput" name="desc" data-field="DESCRIPCION" class="filField" maxlength="20"
		                    		value="${beanMedioEntrega.desc}">
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${compf}<br/><input type="text" id="compfInput" name="comAnualPF" data-field="COM_ANUAL_PF" class="filField" maxlength="16"
		                    		value="${beanMedioEntrega.comAnualPF}">
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${compm}<br/><input type="text" id="compmInput" name="comAnualPM" data-field="COM_ANUAL_PM" class="filField" maxlength="40"
		                    		value="${beanMedioEntrega.comAnualPM}">
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${seguridad}<br/><input type="text" id="seguridadInput" name="seguridad" data-field="CKK_SEGURIDAD" class="filField" maxlength="10"
		                    		value="${beanMedioEntrega.seguridad}">
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${canal}<br/><input type="text" id="canalInput" name="canal" data-field="CANAL" class="filField" maxlength="10"
		                    		value="${beanMedioEntrega.canal}">
		                    </th>
							<th class="text_centro filField" scope="col">
		                    	<label for="importeMaxInput">${importeMax}</label><br/>
								<input type="text" id="importeMaxInput" name="importeMax" data-field="IMPORTEMAX" class="filField" maxlength="10"
		                    		value="${beanMedioEntrega.importeMax}">
		                    </th>
		                </tr>
		            </thead>
		            <tbody>
						<c:forEach var="medio" items="${beanMediosEntrega.mediosEntrega}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td>
		                        <label>
			                        <input type="checkbox" id="check" name="mediosEntrega[${rowCounter.index}].seleccionado" id="seleccionado${rowCounter.index}" />
			                        ${medio.cveMedioEntrega}
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].cveMedioEntrega" id="cveMedioEntrega"  value="${medio.cveMedioEntrega}" />
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].desc" id="desc"  value="${medio.desc}" />
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].comAnualPF" id="comAnualPF"  value="${medio.comAnualPF}" />
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].comAnualPM" id="comAnualPM"  value="${medio.comAnualPM}" />
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].seguridad" id="seguridad"  value="${medio.seguridad}" />
			                        <input type="hidden" name="mediosEntrega[${rowCounter.index}].canal" id="canal"  value="${medio.canal}" />
									<input type="hidden" name="mediosEntrega[${rowCounter.index}].importeMax" id="importeMax"  value="${medio.importeMax}" />
			                    </label>
		                    </td>
		                    <td class="text_centro">${medio.desc}</td>
		                    <td class="text_centro">${medio.comAnualPF}</td>
		                    <td class="text_centro">${medio.comAnualPM}</td>
		                    <td class="text_centro">${medio.seguridad}</td>
		                    <td class="text_centro">${medio.canal}</td>
							<td class="text_derecha">
							<fmt:formatNumber value="${medio.importeMax}" type="currency" currencySymbol="$" var="importeMaxim"/>
							${importeMaxim}
							</td>
							</tr>
						</c:forEach>
						<tr style="display:none;" id="noresults">
						<td>No Hay Resultados</td>
					</tr>
					</tbody>
		        </table>
		    </div>
	        <c:if test="${not empty beanMediosEntrega.mediosEntrega}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${beanMediosEntrega.beanPaginador.regIni} al ${beanMediosEntrega.beanPaginador.regFin} de un total de ${beanMediosEntrega.totalReg} registros</label>
				</div>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaIni == beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaIni != beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaMttoMediosEntrega.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaAnt!='0' && beanMediosEntrega.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaMttoMediosEntrega.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaAnt=='0' || beanMediosEntrega.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanMediosEntrega.beanPaginador.pagina} - ${beanMediosEntrega.beanPaginador.paginaFin}</label>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaFin != beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaMttoMediosEntrega.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaFin == beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaFin != beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaMttoMediosEntrega.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanMediosEntrega.beanPaginador.paginaFin == beanMediosEntrega.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		</c:if>
	    <div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		        	<caption></caption>
		        	<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a class="btnSubMenu" id="btnAgregar" href="${pageContext.servletContext.contextPath}/catalogos/mantenimientoMediosEntrega.do">${nuevo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a  href="javascript:;" >${nuevo}</a></td>
							</c:otherwise>
						</c:choose>
		                 <td class="odd" width="6">${espacioEnBlanco}</td>
		                 <td class="der" width="279">
		                     <a id="btnActualizar" href="javascript:;" >${actualizar}</a>
		                 </td>
		            </tr>
		            <tr>
		                <c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanMediosEntrega.mediosEntrega}">
								<td width="279" class="izq"><a id="btnExportar" href="javascript:;" >${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
		                <td class="odd" width="6">${espacioEnBlanco}</td>

		                <c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanMediosEntrega.mediosEntrega}">
								<td width="279" class="der"><a id="btnEditar" href="javascript:;" >${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a  href="javascript:;" >${editar}</a></td>
							</c:otherwise>
						 </c:choose>
		            </tr>
		            <tr>
		            	<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanMediosEntrega.mediosEntrega}">
								<td width="279" class="izq"><a id="btnExportarTodo" href="javascript:;" >${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
		                <td class="odd" width="6">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR') and not empty beanMediosEntrega.mediosEntrega}">
								<td class="der" width="279"><a id="btnEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
		        </table>
		    </div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>