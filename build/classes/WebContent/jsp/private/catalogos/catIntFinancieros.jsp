<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuSubCatalagos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatIntFinancieros" />
</jsp:include>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.cveInterme" var="cveInterme"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoInterme" var="tipoInterme"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numCecoban" var="numCecoban"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreCorto" var="nombreCorto"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreLargo" var="nombreLargo"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numPersona" var="numPersona"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchAlta" var="fchAlta"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchBaja" var="fchBaja"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchUltModif" var="fchUltModif"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioModif" var="usuarioModif"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioRegistro" var="usuarioRegistro"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.status" var="status"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numBanxico" var="numBanxico"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numIndeval" var="numIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.idIntIndeval" var="idIntIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.folIntIndeval" var="folIntIndeval"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.cveSwiftCor" var="cveSwiftCor"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.soambos" 	var="ambos"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.sospid"	 	var="spid"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.sospei" 		var="spei"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.selopc" 		var="selOpc"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.operaSpei" 	var="operaSpei"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.operaSpid" 	var="operaSpid"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.usuario" var="usuario"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fechaMod" var="fechaMod"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.buscar" var="buscar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.alta" var="alta"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.eliminar" var="eliminar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.editar" var="actualizar"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.limpiar" var="limpiar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.exportar" var="exportar"/>

<spring:message code="catalogos.consultaCatIntFinancieros.text.exportarTodo" var="exportarTodo"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.CD00161V" var="CD00161V"/>
<spring:message code="codError.CD00162V" var="CD00162V"/>
<spring:message code="codError.CD00163V" var="CD00163V"/>
<spring:message code="codError.CD00164V" var="CD00164V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>

<spring:message code="codError.OK00002V" var="OK00002V" />

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/detallePago.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"CD00010V":'${CD00010V}', "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}', "CD00011V":'${CD00011V}',"CD00161V":'${CD00161V}',"CD00162V":'${CD00162V}',"CD00163V":'${CD00163V}',"CD00164V":'${CD00164V}',"ED00029V":'${ED00029V}',"ED00068V":'${ED00068V}',"OK00002V":'${OK00002V}'};</script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/catIntFinancieros.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>
 
	<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="paginador.accion"    id="accion"       value=""/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni"    value="${beanResIntFinancieros.beanPaginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina"    id="pagina"       value="${beanResIntFinancieros.beanPaginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin"    value="${beanResIntFinancieros.beanPaginador.paginaFin}"/>
		<input type="hidden" name="cveInterme"          id="cveInterme"   value="${beanResIntFinancieros.cveInterme}"/>
		<input type="hidden" name="tipoInterme"         id="tipoInterme"  value="${beanResIntFinancieros.tipoInterme}"/>
		<input type="hidden" name="numCecoban"          id="numCecoban"   value="${beanResIntFinancieros.numCecoban}"/>
		<input type="hidden" name="nombreCorto"         id="nombreCorto"  value="${beanResIntFinancieros.nombreCorto}"/>
		<input type="hidden" name="nombreLargo"         id="nombreLargo"  value="${beanResIntFinancieros.nombreLargo}"/>
		<input type="hidden" name="numPersona"          id="numPersona"   value="${beanResIntFinancieros.numPersona}"/>
		<input type="hidden" name="fchAlta"             id="fchAlta"      value="${beanResIntFinancieros.fchAlta}"/>
		<input type="hidden" name="fchBaja"             id="fchBaja"      value="${beanResIntFinancieros.fchBaja}"/>
		<input type="hidden" name="fchUltModif"         id="fchUltModif"  value="${beanResIntFinancieros.fchUltModif}"/>
		<input type="hidden" name="usuarioModif"        id="usuarioModif" value="${beanResIntFinancieros.usuarioModif}"/>
		<input type="hidden" name="numBanxico"          id="numBanxico"   value="${beanResIntFinancieros.numBanxico}"/>
		<input type="hidden" name="cveSwiftCor"         id="cveSwiftCor"  value="${beanResIntFinancieros.cveSwiftCor}"/>
		<input type="hidden" name="selecOpcion"         id="selecOpcion"  value="${beanResIntFinancieros.selecOpcion}"/>
		<input type="hidden" name="operaSpei"           id="operaSpei"    value="${beanResIntFinancieros.operaSpei}"/>
		<input type="hidden" name="operaSpid"           id="operaSpid"    value="${beanResIntFinancieros.operaSpid}"/>		
		<input type="hidden" name="checkSeleccionados"  id="checkSeleccionados"   value="0" />
		
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${subtituloFuncion}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda"><label for="paramCveInterme">${cveInterme}</label></td>
					<td><input type="text" class="Campos" id="paramCveInterme" name="paramCveInterme"  size="20" maxlength="5"  value="${beanResIntFinancieros.cveInterme}"/></td>
					<td  class="text_izquierda"><label for="paramNumPersona">${numPersona}</label></td>
					<td><input type="text" class="Campos" id="paramNumPersona" name="paramNumPersona"  size="20" maxlength="7"  value="${beanResIntFinancieros.numPersona}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda"><label for="select">${tipoInterme}</label></td>
					<td>
						<select name="select" class="Campos" id="selectTipoInterme" >
							<option value="">${seleccionarOpcion}</option>
							<c:forEach var="tipoIntFin" items="${beanResIntFinancieros.listTiposInterviniente}" varStatus="rowCounter">
								<c:choose>
									<c:when test="${tipoIntFin == beanResIntFinancieros.tipoInterme}">
										<option value="${tipoIntFin}" selected="selected">${tipoIntFin}</option>
									</c:when>
									<c:otherwise>
										<option value="${tipoIntFin}">${tipoIntFin}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
					<td  class="text_izquierda"><label for="paramCveSwiftCor">${cveSwiftCor}</label></td>
					<td><input type="text" class="Campos" id="paramCveSwiftCor" name="paramCveSwiftCor"  size="20" maxlength="12"  value="${beanResIntFinancieros.cveSwiftCor}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda"><label for="paramNumCecoban">${numCecoban}</label></td>
					<td><input type="text" class="Campos" id="paramNumCecoban" name="paramNumCecoban"  size="20" maxlength="7"  value="${beanResIntFinancieros.numCecoban}"/></td>
					<td  class="text_izquierda"><label for="paramFchAlta">${fchAlta}</label></td>
					<td><input name="paramFchAlta" type="text" class="Campos" id="paramFchAlta" size="20" readonly="readonly" value="${beanResIntFinancieros.fchAlta}"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda"><label for="paramNumBanxico">${numBanxico}</label></td>
					<td><input type="text" class="Campos" id="paramNumBanxico" name="paramNumBanxico"  size="20" maxlength="7"  value="${beanResIntFinancieros.numBanxico}"/></td>
					
					<td  class="text_izquierda"><label for="paramFchBaja">${fchBaja}</label></td>
					<td><input name="paramFchBaja" type="text" class="Campos" id="paramFchBaja" size="20" readonly="readonly" value="${beanResIntFinancieros.fchBaja}"/>
						<img id="cal2" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda"><label for="paramNombreCorto">${nombreCorto}</label></td>
					<td><input type="text" class="Campos" id="paramNombreCorto" name="paramNombreCorto"  size="20" maxlength="16"  value="${beanResIntFinancieros.nombreCorto}"/></td>
					<td  class="text_izquierda"><label for="paramFchUltModif">${fchUltModif}</label></td>
					<td><input name="paramFchUltModif" type="text" class="Campos" id="paramFchUltModif" size="20" readonly="readonly" value="${beanResIntFinancieros.fchUltModif}"/>
						<img id="cal3" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda"><label for="paramNombreLargo">${nombreLargo}</label></td>
					<td><input type="text" class="Campos" id="paramNombreLargo" name="paramNombreLargo"  size="20" maxlength="40"  value="${beanResIntFinancieros.nombreLargo}"/></td>
					<td  class="text_izquierda"><label for="paramUsuarioModif">${usuarioModif}</label></td>
					<td><input type="text" class="Campos" id="paramUsuarioModif" name="paramUsuarioModif"  size="20" maxlength="7"  value="${beanResIntFinancieros.usuarioModif}"/></td>
				</tr>
				
				<tr>   
					<td class="text_izquierda" ><label for="tipoSeleccion">${selOpc}</label></td>
					<td>		
						<select name="tipoSeleccion" class="Campos" id="tipoSeleccion" >
							<c:if test="${empty beanResIntFinancieros.selecOpcion}">
								<option value="" selected> </option>
							</c:if>
							<option value="AMBOS" ${"AMBOS" eq beanResIntFinancieros.selecOpcion ? "selected" : ""}>${ambos}</option>
							<option value="SPID" ${"SPID" eq beanResIntFinancieros.selecOpcion ? "selected" : ""}>${spid}</option>
							<option value="SPEI" ${"SPEI" eq beanResIntFinancieros.selecOpcion ? "selected" : ""}>${spei}</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td class="text_centro">
					<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
					</c:choose>  
					</td>
					<td class="text_centro">
						<span><a id="idLimpiar" href="javascript:;">${limpiar}</a></span>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
			<caption></caption>
				<tr>
					<th class="text_centro tamanocentrarradiobton" scope="col" colspan="2">${cveInterme}</th>
					<th class="text_centro anchocolumanafin" scope="col">${tipoInterme}</th>
					<th class="text_centro anchocolumanafin" scope="col">${numCecoban}</th>
					<th class="text_centro anchocolumanafin" scope="col">${nombreCorto}</th>
					<th class="text_centro anchocolumanafin" scope="col">${nombreLargo}</th>
					<th class="text_centro anchocolumanafin" scope="col">${numPersona}</th>
					<th class="text_centro anchocolumanafin" scope="col">${fchAlta}</th>
					<th class="text_centro anchocolumanafin" scope="col">${fchBaja}</th>
					<th class="text_centro anchocolumanafin" scope="col">${fchUltModif}</th>
					<th class="text_centro anchocolumanafin" scope="col">${usuarioModif}</th>
					<th class="text_centro anchocolumanafin" scope="col">${usuarioRegistro}</th>
					<th class="text_centro anchocolumanafin" scope="col">${status}</th>
					<th class="text_centro anchocolumanafin" scope="col">${numBanxico}</th>
					<th class="text_centro anchocolumanafin" scope="col">${numIndeval}</th>
					<th class="text_centro anchocolumanafin" scope="col">${idIntIndeval}</th>
					<th class="text_centro anchocolumanafin " scope="col">${folIntIndeval}</th>
					<th class="text_centro anchocolumanafin" scope="col">${cveSwiftCor}</th>
					<th class="text_centro anchocolumanafin" scope="col">${operaSpei}</th>
					<th class="text_centro anchocolumanafin" scope="col">${operaSpid}</th>
				</tr>
				<tbody>				
					<c:forEach var="beanIntFinanciero" items="${beanResIntFinancieros.listBeanIntFinanciero}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">
							    <input type="checkbox" id="check" name="listBeanIntFinanciero[${rowCounter.index}].seleccionado" value="true"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].cveInterme" value="${beanIntFinanciero.cveInterme}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].tipoInterme" value="${beanIntFinanciero.tipoInterme}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].numCecoban" value="${beanIntFinanciero.numCecoban}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].nombreCorto" value="${beanIntFinanciero.nombreCorto}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].nombreLargo" value="${beanIntFinanciero.nombreLargo}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].numPersona" value="${beanIntFinanciero.numPersona}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].fchAlta" value="${beanIntFinanciero.fchAlta}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].fchBaja" value="${beanIntFinanciero.fchBaja}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].fchUltModif" value="${beanIntFinanciero.fchUltModif}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].usuarioModif" value="${beanIntFinanciero.usuarioModif}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].usuarioRegistro" value="${beanIntFinanciero.usuarioRegistro}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].status" value="${beanIntFinanciero.status}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].numBanxico" value="${beanIntFinanciero.numBanxico}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].numIndeval" value="${beanIntFinanciero.numIndeval}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].idIntIndeval" value="${beanIntFinanciero.idIntIndeval}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].folIntIndeval" value=	"${beanIntFinanciero.folIntIndeval}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].cveSwiftCor" value="${beanIntFinanciero.cveSwiftCor}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].operaSpei" value= "${beanResIntFinanciero.operaSpei}"/>
								<input type="hidden" name="listBeanIntFinanciero[${rowCounter.index}].operaSpid" value= "${beanResIntFinancieros.operaSpid}"/> 							
							</td>
							<td class="text_izquierda">${beanIntFinanciero.cveInterme}</td>
							<td class="text_izquierda">${beanIntFinanciero.tipoInterme}</td>
							<td class="text_izquierda">${beanIntFinanciero.numCecoban}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreCorto}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreLargo}</td>
							<td class="text_izquierda">${beanIntFinanciero.numPersona}</td>
							<td class="text_izquierda">${beanIntFinanciero.fchAlta}</td>
							<td class="text_izquierda">${beanIntFinanciero.fchBaja}</td>
							<td class="text_izquierda">${beanIntFinanciero.fchUltModif}</td>
							<td class="text_izquierda">${beanIntFinanciero.usuarioModif}</td>
							<td class="text_izquierda">${beanIntFinanciero.usuarioRegistro}</td>
							<td class="text_izquierda">${beanIntFinanciero.status}</td>
							<td class="text_izquierda">${beanIntFinanciero.numBanxico}</td>
							<td class="text_izquierda">${beanIntFinanciero.numIndeval}</td>
							<td class="text_izquierda">${beanIntFinanciero.idIntIndeval}</td>
							<td class="text_izquierda">${beanIntFinanciero.folIntIndeval}</td>
							<td class="text_izquierda">${beanIntFinanciero.cveSwiftCor}</td>
							<td class="text_izquierda">${beanIntFinanciero.operaSpei}</td>
							<td class="text_izquierda">${beanIntFinanciero.operaSpid}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanResIntFinancieros.listBeanIntFinanciero}">
				<div class="paginador">
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaIni == beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaIni != beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarIntFinancieros.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaAnt!='0' && beanResIntFinancieros.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','buscarIntFinancieros.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaAnt=='0' || beanResIntFinancieros.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResIntFinancieros.beanPaginador.pagina} - ${beanResIntFinancieros.beanPaginador.paginaFin}</label>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaFin != beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarIntFinancieros.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaFin == beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaFin != beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarIntFinancieros.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResIntFinancieros.beanPaginador.paginaFin == beanResIntFinancieros.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>
		</div>

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
				<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"AGREGAR\")}">
								<td class="izq tamanobtonexportar"><a id="idAlta" href="javascript:;">${alta}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des tamanobtonexportar"><a href="javascript:;">${alta}</a></td>
							</c:otherwise>
						</c:choose>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') && 	beanResIntFinancieros.totalReg > 0}">
								<td class="der tamanobtonexportar"><a id="idEditar" href="javascript:;">${actualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="der_Des tamanobtonexportar"><a href="javascript:;">${actualizar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') && beanResIntFinancieros.totalReg > 0}">
								<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"ELIMINAR\") && beanResIntFinancieros.totalReg > 0}">
								<td class="der tamanobtonexportar"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des tamanobtonexportar"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>	
						
					<tr>
					<c:choose>
					  <c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') && beanResIntFinancieros.totalReg > 0}">
							<td class="izq tamanobtonexportar"><a id="btnExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							<td class="odd" >${espacioEnBlanco}</td>
					  </c:when>
					  <c:otherwise>
						<td class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
					  </c:otherwise>
					</c:choose>
					</tr>
				</table>
			</div>
		</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">${e:forHtml(tipoError)}('${e:forHtml(descError)}','${e:forHtml(aplicacion)}','${e:forHtml(codError)}','');</script>
</c:if>