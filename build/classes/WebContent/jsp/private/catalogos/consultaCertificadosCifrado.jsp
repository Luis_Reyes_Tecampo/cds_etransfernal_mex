<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="certifCifrado" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/certificadosCifrado.js" type="text/javascript"></script>

<%-- Inicio de llamado de datos de Properties --%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="catalogos.certificadosCifrado.titulo" var="tituloFuncionalidad"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<%-- Opciones de la Pantalla --%>
<spring:message code="catalogos.certificadosCifrado.nuevo" 				var="nuevo"/>
<spring:message code="catalogos.certificadosCifrado.exportar" 			var="exportar"/>
<spring:message code="catalogos.certificadosCifrado.exportarTodo" 		var="exportarTodo"/>
<spring:message code="catalogos.certificadosCifrado.actualizar" 		var="actualizar"/>
<spring:message code="catalogos.certificadosCifrado.editar" 			var="editar"/>
<spring:message code="catalogos.certificadosCifrado.eliminar" 			var="eliminar"/>
<%-- Campos de la Pantalla --%>
<spring:message code="catalogos.certificadosCifrado.canal" 				var="lblCanal"/>
<spring:message code="catalogos.certificadosCifrado.alias" 				var="lblAlias"/>
<spring:message code="catalogos.certificadosCifrado.descripcionCanal" 	var="lblDescripcionCanal"/>
<spring:message code="catalogos.certificadosCifrado.numero" 			var="lblNumero"/>
<spring:message code="catalogos.certificadosCifrado.tecnologiaOrigen" 	var="lblTecnologia"/>
<spring:message code="catalogos.certificadosCifrado.firma" 				var="lblFirma"/>
<spring:message code="catalogos.certificadosCifrado.cifrado" 			var="lblCifrado"/>
<spring:message code="catalogos.certificadosCifrado.estatus" 			var="lblEstatus"/>
<spring:message code="catalogos.certificadosCifrado.vigencia" 			var="lblVigencia"/>

<%-- Constantes de Error --%>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>

<%-- Fin de Variables Constantes --%>
<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',
                    "CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};
</script>
<%-- Fin de Variables Constantes --%>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>

<c:set var="searchString" value="${seguTareas}"/>

<form id="idForm" name="idForm" method="post" action="">
	<input type="hidden" name="cveMedioEntrega" id="cveMedioEntrega" value="" />
	<input type="hidden" name="pagina" 		id="pagina" 	value="${beanCertificados.beanPaginador.pagina}">
	<input type="hidden" name="paginaIni" 	id="paginaIni" 	value="${beanCertificados.beanPaginador.paginaIni}">
    <input type="hidden" name="paginaFin" 	id="paginaFin" 	value="${beanCertificados.beanPaginador.paginaFin}">
    <input type="hidden" name="field" 		id="field" 		value="${beanCertificados.beanFilter.field}">
	<input type="hidden" name="fieldValue" 	id="fieldValue" value="${beanCertificados.beanFilter.fieldValue}">
    <input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="opcion" id="opcion" value="">
	
    <div class="frameTablaVariasColumnas">
	    <div class="titleTablaVariasColumnas">${tituloFuncionalidad}</div>
		    <div class="contentTablaVariasColumnas">
		        <table class="table-cons table" id="tblConsulta">
		        	<caption></caption>
		            <thead>
		                <tr>
							<th class="text_centro filField" scope="col">
		                    	${lblCanal}<br/><input type="text" id="canalInput" name="canal" class="filField" data-field="CANAL" maxlength="6" 
		                    		value="${beanFilter.canal}">
		                    </th>
							<th class="text_centro filField" scope="col">
		                    	${lblAlias}<br/><input type="text" id="aliasInput" name="alias" data-field="ALIAS_KS" class="filField" maxlength="20" 
		                    		value="${beanFilter.alias}"/>
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${lblNumero}<br/><input type="text" id="numeroInput" name="numCertificado" data-field="NUM_CERTIF" class="filField" maxlength="20" 
		                    		value="${beanFilter.numCertificado}"/>
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${lblFirma}<br/><input type="text" id="firmaInput" name="algDigest" data-field="ALG_DIGEST" class="filField" maxlength="16" 
		                    		value="${beanFilter.algDigest}"/>
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${lblCifrado}<br/><input type="text" id="cifradoInput" name="algSimetrico" data-field="ALG_SIMETRICO" class="filField" maxlength="40" 
		                    		value="${beanFilter.algSimetrico}"/>
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${lblTecnologia}<br/><input type="text" id="tecnologiaInput" name="tecOrigen" data-field="TEC_ORIGEN" class="filField" maxlength="10" 
		                    		value="${beanFilter.tecOrigen}"/>
		                    </th>
		                    <th class="text_centro filField" scope="col">
		                    	${lblEstatus}
		                    </th>
		                    
		                </tr>
		            </thead>
		            <tbody>				
						<c:forEach var="certificado" items="${beanCertificados.certificados}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td>
		                        <label>
			                        <input type="checkbox" id="check" name="certificados[${rowCounter.index}].seleccionado" onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}" />
			                        ${certificado.canal}
			                        <input type="hidden" name="certificados[${rowCounter.index}].canal" id="canal"  value="${certificado.canal}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].alias" id="alias"  value="${certificado.alias}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].numCertificado" id="numCertificado"  value="${certificado.numCertificado}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].algDigest" id="algDigest"  value="${certificado.algDigest}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].algSimetrico" id="algSimetrico"  value="${certificado.algSimetrico}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].tecOrigen" id="tecOrigen"  value="${certificado.tecOrigen}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].fchVencimiento" id="fchVencimiento"  value="${certificado.fchVencimiento}" />
			                        <input type="hidden" name="certificados[${rowCounter.index}].estatus" id="estatus"  value="${certificado.estatus}" />
		                        </label>
		                    </td>
		                    <td class="text_centro">${certificado.alias}</td>
		                    <td class="text_centro">${certificado.numCertificado}</td>
		                    <td class="text_centro">${certificado.algDigest}</td>
		                    <td class="text_centro">${certificado.algSimetrico}</td>
		                    <td class="text_centro">${certificado.tecOrigen}</td>
		                    <td class="text_centro">${certificado.estatus}</td>
							</tr>
						</c:forEach>
						<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
					</tbody>
		        </table>
		    </div>
	        <c:if test="${not empty beanCertificados.certificados}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${beanCertificados.beanPaginador.regIni} al ${beanCertificados.beanPaginador.regFin} de un total de ${beanCertificados.totalReg} registros</label>
				</div>
				<c:if test="${beanCertificados.beanPaginador.paginaIni == beanCertificados.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaIni != beanCertificados.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCertificadosCifrado.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaAnt!='0' && beanCertificados.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaCertificadosCifrado.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaAnt=='0' || beanCertificados.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanCertificados.beanPaginador.pagina} - ${beanCertificados.beanPaginador.paginaFin}</label>
				<c:if test="${beanCertificados.beanPaginador.paginaFin != beanCertificados.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCertificadosCifrado.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaFin == beanCertificados.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaFin != beanCertificados.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCertificadosCifrado.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanCertificados.beanPaginador.paginaFin == beanCertificados.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		</c:if>
	    <div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		        	<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a class="btnSubMenu" id="btnAgregar" href="${pageContext.servletContext.contextPath}/catalogos/mantenimientoCertificado.do">${nuevo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a  href="javascript:;" >${nuevo}</a></td>
							</c:otherwise>
						</c:choose>
		                 <td class="odd" width="6">${espacioEnBlanco}</td>
		                 <td class="der" width="279">
		                     <a id="btnActualizar" href="javascript:;" >${actualizar}</a>
		                 </td>
		            </tr>
		            <tr>
		                <c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanCertificados.certificados}">
								<td width="279" class="izq"><a id="btnExportar" href="javascript:;" >${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
		                <td class="odd" width="6">${espacioEnBlanco}</td>
		                
		                <c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') and not empty beanCertificados.certificados}">
								<td width="279" class="der"><a id="btnEditar" href="javascript:;" >${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a  href="javascript:;" >${editar}</a></td>
							</c:otherwise>
						 </c:choose>
		            </tr>
		            <tr>
		            	<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanCertificados.certificados}">
								<td width="279" class="izq"><a id="btnExportarTodo" href="javascript:;" >${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
		                <td class="odd" width="6">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR') and not empty beanCertificados.certificados}">
								<td class="der" width="279"><a id="btnEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
		        </table>
		    </div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>
