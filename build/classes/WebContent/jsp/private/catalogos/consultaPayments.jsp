<%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<jsp:include page="../myHeader.jsp"/>

<jsp:include page="../myMenuCatalogos.jsp">
	 <jsp:param name="menuItem"    value="catalogos" />
	 <jsp:param value="consultaPayments" name="menuSubitem"/>
</jsp:include>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />

<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/ConsultaPayments.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>


<spring:message code="general.nombreAplicacion" 					var="app"/>
<spring:message code="general.bienvenido"       					var="welcome"/>
<spring:message code="general.guardar" 								var="lblGuardar"/>
<spring:message code="general.espacioEnBlanco" 						var="espacioEnBlanco"/>
<spring:message code="general.espacioEnBlanco" 						var="espacioEnBlanco"/>
<spring:message code="general.cancelar" 							var="lblRegresar"/>
<spring:message code="general.fin" 									var="fin"/>
<spring:message code="general.inicio" 								var="inicio"/>
<spring:message code="general.siguiente" 							var="siguiente"/>
<spring:message code="general.anterior" 							var="anterior"/>
<spring:message code="catalogos.payments.exportarTodo"     		   	var="lblExportartodo"/>
<spring:message code="catalogos.payments.exportar"          	  	var="lblExportar"/>
<spring:message code="catalogos.payments.modificar"         		var="lblModificar"/>
<spring:message code="catalogos.payments.buscar"           			var="lblBuscar"/>
<spring:message code="catalogos.payments.filtro"					var="lblFiltro" />
<spring:message code="catalogos.payments.filtroBusqueda"			var="lblFiltroBusqueda" />
<spring:message code="catalogos.payments.seleccionarOpc"			var="seleccionarOpc" />
<spring:message code="catalogos.payments.seleccioneOpcion"			var="lblSeleccioneOpcion" />
<spring:message code="catalogos.payments.seleccionarActivado"		var="seleccionarActivado" />
<spring:message code="catalogos.payments.seleccionarTodos"			var="seleccionarTodos" />
<spring:message code="catalogos.payments.clave" 					var="lblClave" />
<spring:message code="catalogos.payments.seleccionarDesactivado"	var="seleccionarDesactivado" />
<spring:message code="catalogos.payments.actdes"					var="lblActDes" />
<spring:message code="catalogos.payments.descripcion"				var="lblDescripcion" />
<spring:message code="catalogos.payments.titulo"					var="tituloFuncionalidad" />
<spring:message code="catalogos.general.text.tituloModulo" 			var="tituloModulo"/>

<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00011V" var="ED00011V" />
<spring:message code="codError.OK00001V" var="OK00001V" />
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.ED00068V" var="ED00068V" />
<spring:message code="codError.OK00002V" var="OK00002V" />
<spring:message code="codError.ED00029V" var="ED00029V" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.ED00023V" var="ED00023V" />
<spring:message code="codError.ED00022V" var="ED00022V" />
<spring:message code="codError.ED00086V" var="ED00086V" />
<spring:message code="codError.ED00027V" var="ED00027V" />
<spring:message code="codError.OK00020V" var="OK00020V" />
<spring:message code="codError.ED00126V" var="ED00126V" />

<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};</script>


<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> ${tituloFuncionalidad}
</div>
<c:set var="searchString" value="${seguTareas}"/>
<form id="mform" name="mform" method="post" action="">
	<input type="hidden" name="pagina" 		id="pagina" 	value="${e:forHtml(beanConsultasPayme.beanPaginador.pagina)}">
	<input type="hidden" name="paginaIni" 	id="paginaIni" 	value="${e:forHtml(beanConsultasPayme.beanPaginador.paginaIni)}">
    <input type="hidden" name="paginaFin" 	id="paginaFin" 	value="${e:forHtml(beanConsultasPayme.beanPaginador.paginaFin)}">
    <input type="hidden" name="field" 		id="field" 		value="${e:forHtml(beanConsultasPayme.beanFilter.field)}">
	<input type="hidden" name="fieldValue" 	id="fieldValue" value="${e:forHtml(beanConsultasPayme.beanFilter.fieldValue)}">
    <input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="opcion" id="opcion" value="">
	
	<div class="frameTablaVariasColumnas">
		<div class="titleBuscadorSimple">
			<span>${lblFiltroBusqueda}</span> - ${lblFiltro}
		</div>
		<div class="contentBuscadorSimple">
			<table class="table-cons table" id="tblConsulta">
				<caption></caption>
				<tr>
					<td class="text_izquierda" style="width: 150px">
						<label for="tipoOperacion">${seleccionarOpc}</label>
					</td>
					<td><select name="tipoOperacion" style="width: 100px" class="text_centro" id="tipoOperacion" onchange="onChangeCombo()" data-field="tipoOperacion">
						<c:if test="${beanFilter.tipoOperacion =='TODOS'}">
							<option value="" selected> </option>
						</c:if>		
						<option value=""	${"" eq beanFilter.tipoOperacion ? "selected" : ""}>			${seleccionarTodos} </option>
						<option value="1"	${"1" eq beanFilter.tipoOperacion ? "selected" : ""}>		${seleccionarActivado}</option>
						<option value="0"	${"0" eq beanFilter.tipoOperacion ? "selected" : ""}> 	${seleccionarDesactivado}</option>
					</select></td>
					<td class="text_izquierda" style="width: 150px">
						<label for="tipoOperacion">${lblSeleccioneOpcion}</label>
					</td>
					<td><label for="clave"></label><select name="clave" style="width: 100px" class="text_centro" id="clave" onchange="onChangeCombo()" data-field="clave">
							<option  value = "" selected> ${seleccionarOpc} </option>
							<c:forEach var="registro" items="${beanConsult.listaCMEnt}">
								<option value="${registro.clave}" ${registro.clave eq beanFilter.clave ? "selected" : "" }>${registro.clave}</option>
							</c:forEach>
							
						</select>	
					</td>
				</tr>
			</table>
		</div>
		<div class="contentTablaVariasColumnas">
			<table class="table-cons table" id="tblConsulta">
				<caption></caption>
				<thead>
					<tr>
						<th class="text_centro filField" scope="col">${lblClave}<br/>
						</th>
						<th class="text_centro filField" scope="col">${lblDescripcion}<br/>
						</th>
						<th class="text_centro filField" scope="col">${lblActDes}<br/>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="medEntrega" items="${beanConsultasPayme.medEntrega}"
						varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}" >
							<td>
								<label for="check"> 
									<input type="checkbox" id="check" name="medEntrega[${rowCounter.index}].seleccionado" onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}" />
				                    ${medEntrega.clave}
				                    <input type="hidden" name="medEntrega[${rowCounter.index}].clave"		  id="clave" 		 value="${medEntrega.clave}" /> 
									<input type="hidden" name="medEntrega[${rowCounter.index}].descripcion"   id="descripcion"	 value="${medEntrega.descripcion}" /> 
									<input type="hidden" name="medEntrega[${rowCounter.index}].actDes" 		  id="actDes" 	 	 value="${medEntrega.actDes}" />
									<input type="hidden" name="medEntrega[${rowCounter.index}].detalles"   	  id="detalles"	 	 value="${medEntrega.detalles}" /> 
								</label>
							</td>
							<td class="text_centro">${medEntrega.descripcion}</td>
							<td class="text_centro">${medEntrega.actDes}</td>
						</tr>
					</c:forEach>
					<c:if test="${empty beanConsultasPayme.medEntrega}">
					<tr style="display: true;" id="noresults">
						<td>No Hay Resultados</td>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<c:if test="${not empty beanConsultasPayme.medEntrega}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${e:forHtml(beanConsultasPayme.beanPaginador.regIni)} al
						${e:forHtml(beanConsultasPayme.beanPaginador.regFin)} de un total de
						${e:forHtml(beanConsultasPayme.totalReg)} registros</label>
				</div>
				<jsp:include page="pagconsulPayments.jsp" >	  					
  					<jsp:param name="paginaIni" value="${beanRes.paginador.paginaIni}" />
  					<jsp:param name="pagina" value="${beanRes.paginador.pagina}" />
  					<jsp:param name="paginaAnt" value="${beanRes.paginador.paginaAnt}" />
  					<jsp:param name="paginaFin" value="${beanRes.paginador.paginaFin}" />
  					<jsp:param name="servicio" value="consultaPayments.do" />
				</jsp:include>
			</div>
		</c:if>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<td class="izq"><a id="btnEditar" href="javascript:;">${lblModificar}</a></td><td class="odd" >${espacioEnBlanco}	</td>
						<td class="der" ><a id="btnExportarTodo" href="javascript:;">${lblExportartodo}</a></td>
					</tr>
					<tr>
						<td  class="izq"><a id="btnExportar" href="javascript:;">${lblExportar}</a></td>
						<td class="odd" >${espacioEnBlanco}	</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer"> ${e:forHtml(tipoError)}('${e:forHtml(descError)}','${e:forHtml(aplicacion)}','${e:forHtml(codError)}',''); </script>
</c:if>

