<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:choose>
	<c:when
		test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResRFC.rfcs}">
		<td class="izq widthx279"><a id="btnExportarTodo"
			href="javascript:;">${lblExportarTodo}</a></td>
	</c:when>
	<c:otherwise>
		<td class="izq_Des widthx279"><a href="javascript:;">${lblExportarTodo}</a></td>
	</c:otherwise>
</c:choose>
<td class="odd widthx6">${espacioEnBlanco}</td>
<c:choose>
	<c:when
		test="${fn:containsIgnoreCase(searchString,'ELIMINAR') and not empty beanResRFC.rfcs}">
		<td class="der widthx279"><a id="btnEliminar" href="javascript:;">${lblEliminar}</a></td>
	</c:when>
	<c:otherwise>
		<td class="der_Des widthx279"><a href="javascript:;">${lblEliminar}</a></td>
	</c:otherwise>
</c:choose>