<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ include file="../myHeader.jsp" %>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${modulo eq 'actual'}">
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="parametrosPOACOA" name="menuSubitem"/>
</jsp:include>
</c:if>
<c:if test="${modulo eq 'spid'}">
<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOASPID" />
    <jsp:param value="paramsPoaCoaSpid" name="menuSubitem"/>
</jsp:include>
</c:if>


<spring:message code="general.buscar" var="buscar" />

<spring:message code="catalogos.parametrosPOACOA.text.txtActivar" var="txtActivar"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtDesactivar" var="txtDesactivar"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtEnvRecArch" var="txtEnvRecArch"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtFinalDev" var="txtFinalDev"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtLiqFinal" var="txtLiqFinal"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtGenPagosDevTras" var="txtGenPagosDevTras"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtGenDev" var="txtGenDev"/>
<spring:message code="catalogos.parametrosPOACOA.text.txtGenTras" var="txtGenTras"/>
<spring:message code="catalogos.parametrosPOACOA.text.actualizar" var="actualizar"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOASPID.lblViewParam" var="lblViewParam"/>
<spring:message code="moduloPOACOASPID.lblViewParam1" var="lblViewParam1"/>
<spring:message code="moduloPOACOASPID.lblViewCarga" var="lblViewCarga"/>
<spring:message code="moduloPOACOASPID.lblViewMonitor" var="lblViewMonitor"/>
<spring:message code="moduloPOACOASPID.lblPOA" var="POA"/>
<spring:message code="moduloPOACOASPID.lblCOA" var="COA"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />

<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script	src="${pageContext.servletContext.contextPath}/js/private/paramsPOACOA.js" type="text/javascript" defer="defer"></script>
	

<c:if test="${modulo eq 'actual'}">
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>-${lblViewParam1}</div></c:if>
<c:if test="${modulo eq 'spid'}">
<div class="pageTitleContainer"><span class="pageTitle">${moduloPOACOASPID}</span>-${lblViewParam}</div></c:if>

<form name="formParamPOACOA" id="formParamPOACOA" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input id="parametroCifrado" name="parametroCifrado" type="hidden" value="" />	
	<input id="procesoPOA" name="procesoPOA" type="hidden" value="" />
	<input id="procesoCOA" name="procesoCOA" type="hidden" value="" />
	<input id="numeroFase" name="numeroFase" type="hidden" value="" />
	<input id="tipoArchivoGenerar" name="tipoArchivoGenerar" type="hidden" value="" />
	<input id="liqFinal" name="liqFinal" type="hidden" value="0" />
	<input id="accion" name="accion" type="hidden" />

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
			<caption></caption>
				
	<%@ include file="parametrosPOACOAPOA.jsp" %>
	<%@ include file="parametrosPOACOACOA.jsp" %>
								
					<%@ include file="parametrosPOACOAFases.jsp" %>
					<%@ include file="parametrosPOACOAArch.jsp" %>
			  
			</table>
	</div>
</div>
  
 
<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
<caption></caption>
	<tr>
		<td class="izq_Des classWidth279"><a href="javascript:;" ></a></td>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
			<c:choose>
			<c:when test="${modulo eq 'spid'}">
				<td class="der classWidth279"><a id="idActualizar" class="spid" href="javascript:;" >${actualizar}</a></td>
				</c:when>
				<c:otherwise>
				<td class="der classWidth279"><a id="idActualizar" class="actual" href="javascript:;" >${actualizar}</a></td>
				</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<td class="der_Des classWidth279"><a href="javascript:;" >${actualizar}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>

</table>
</div>
</div>

</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');
	</script>
</c:if>

  

