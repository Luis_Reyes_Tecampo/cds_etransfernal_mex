<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="catalogoParametros" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.nombreAplicacion" var="aplicacion" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCatalogoParametros.myMenu.title.catalogoParametros" var="catalogoParametros"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.button.limpiar" var="btnLimpiar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.button.buscar" var="btnBuscar"/>

<spring:message code="moduloCatalogo.mantenimientoParametros.label.campoObligatorio" var="campoObligatorio"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.tipoParametro"var="lbltipoParametro"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.parametroFalta" var="lblParamFalta"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.LongitudDato" var="lblLogitudDato"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.descripcion" var="lblDescripcion"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.claveParametro" var="lblCveParam"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.valorFalta" var="lblValorFalta"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.parametro" var="lblParametro"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.mecanismo" var="lblMecanismo"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.posicion" var="lblPosicion"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.programa" var="lblPrograma"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.tipoDato" var="lblTipoDato"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.define" var="lblDefine"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.trama" var="lblTrama"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.label.cola" var="lblCola"/>

<spring:message code="moduloCatalogo.mantenimientoParametros.link.editar" var="lnkEditar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.guardar" var="lnkGuardar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.agregar" var="lnkAgregar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.cancelar" var="lnkCancelar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.eliminar" var="lnkEliminar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.exportar" var="lnkExportar"/>
<spring:message code="moduloCatalogo.mantenimientoParametros.link.exportarTodo" var="lnkExportarTodo"/>

<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.ED00125V" var="ED00125V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>

<script type="text/javascript">
	var mensajesMov = new Array();
	mensajesMov["aplicacion"] = '${aplicacion}';
	mensajesMov["CD00010V"] = '${CD00010V}';
	mensajesMov["ED00068V"] = '${ED00068V}';
	mensajesMov["ED00125V"] = '${ED00125V}';
	mensajesMov["ED00126V"] = '${ED00126V}';
</script>

<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mantenimientoParametros.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

<form id="idForm" name="idForm" action="" method="post">
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResponse.paginador.pagina}"/>
	<input type="hidden" name="paginador.regIni" id="regIni" value="${beanResponse.paginador.regIni}"/>
	<input type="hidden" name="paginador.regFin" id="regFin" value="${beanResponse.paginador.regFin}"/>
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResponse.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResponse.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.paginaAnt" id="regIni" value="${beanResponse.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.paginaSig" id="regFin" value="${beanResponse.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.accion" id="accion" value="${beanResponse.paginador.accion}"/>
	
	<input type="hidden" id="parametro" name="parametro" value="${beanResponse.parametro}"/>
	<input type="hidden" id="descripParam" name="descripParam" value="${beanResponse.descripParam}"/>
	<input type="hidden" id="tipoParametro" name="tipoParametro" value="${beanResponse.tipoParametro}"/>
	<input type="hidden" id="tipoDato" name="tipoDato" value="${beanResponse.tipoDato}"/>
	<input type="hidden" id="longitudDato" name="longitudDato" value="${beanResponse.longitudDato}"/>
	<input type="hidden" id="valorFalta" name="valorFalta" value="${beanResponse.valorFalta}"/>
	<input type="hidden" id="trama" name="trama" value="${beanResponse.trama}"/>
	<input type="hidden" id="cola" name="cola" value="${beanResponse.cola}"/>
	<input type="hidden" id="mecanismo" name="mecanismo" value="${beanResponse.mecanismo}"/>
	<input type="hidden" id="posicion" name="posicion" value="${beanResponse.posicion}"/>
	<input type="hidden" id="parametroFalta" name="parametroFalta" value="${beanResponse.parametroFalta}"/>
	<input type="hidden" id="programa" name="programa" value="${beanResponse.programa}"/>

	<input type="hidden" name="countListParamSeleccionados" value="${fn:length(beanResponse.listaParametros)}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${catalogoParametros}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">${lblParametro}:</td>
					<td>
						<input type="text" class="Campos" id="idParametro" name="idParametro"  size="20" maxlength="16" value="${beanResponse.parametro}"/>
						<span><a style="color:red;">${campoObligatorio}</a></span>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblDescripcion}:</td>
					<td><input type="text" class="Campos" id="idDescripParam" name="idDescripParam"  size="60" maxlength="60" value="${beanResponse.descripParam}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lbltipoParametro}:</td>
					<td>
						<select name="idTipoParametro" class="Campos" id="idTipoParametro" >
							<c:forEach var="opcionParam" items="${beanResponse.opcionesTipoParam}" varStatus="rowCounter">
								<c:choose>
									<c:when test="${opcionParam.valorOpcion == beanResponse.tipoParametro}">
										<option value="${opcionParam.valorOpcion}" selected="selected">${opcionParam.opcion}</option>
									</c:when>
									<c:otherwise>
										<option value="${opcionParam.valorOpcion}">${opcionParam.opcion}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblTipoDato}:</td>
					<td>
						<select name="idTipoDato" class="Campos" id="idTipoDato" >
							<c:forEach var="opcionDato" items="${beanResponse.opcionesTipoDato}" varStatus="rowCounter">
								<c:choose>
									<c:when test="${opcionDato.valorOpcion == beanResponse.tipoDato}">
										<option value="${opcionDato.valorOpcion}" selected="selected">${opcionDato.opcion}</option>
									</c:when>
									<c:otherwise>
										<option value="${opcionDato.valorOpcion}">${opcionDato.opcion}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblLogitudDato}:</td>
					<td><input type="text" class="Campos" id="idLongitudDato" name="idLongitudDato"  size="5"  maxlength="5" value="${beanResponse.longitudDato}"/></td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblValorFalta}:</td>
					<td>
						<input type="text" class="Campos" id="idValorFalta" name="idValorFalta"  size="20" maxlength="20" value="${beanResponse.valorFalta}"/>
						<span><a style="color:red;">${campoObligatorio}</a></span>
					</td>
				</tr>
			</table>
			<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">${lblDefine}:</td>
					<td  class="text_izquierda">${lblTrama}:
						<select name="idTrama" class="Campos" id="idTrama" onclick="onChangeComboDefineTrama()" onKeyPress="">
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion == beanResponse.trama}">
										<option value="${opcion.valorOpcion}" selected="selected">${opcion.opcion}</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion}">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
					<td width="1"></td>
					<td  class="text_izquierda">${lblCola}:
						<select name="idCola" class="Campos" id="idCola" onclick="onChangeComboDefineCola()" onKeyPress="">
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion == beanResponse.cola}">
										<option value="${opcion.valorOpcion}" selected="selected">${opcion.opcion}</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion}">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
					<td width="1"></td>
					<td  class="text_izquierda">${lblMecanismo}:
						<select name="idMecanismo" class="Campos" id="idMecanismo" >
							<c:forEach var="opcion" items="${defineTramaColaMecanismo}">
								<c:choose>
		    						<c:when test="${opcion.valorOpcion == beanResponse.mecanismo}">
										<option value="${opcion.valorOpcion}" selected="selected">${opcion.opcion}</option>
		    						</c:when>    
		    						<c:otherwise>
										<option value="${opcion.valorOpcion}">${opcion.opcion}</option>
		    						</c:otherwise>
	    						</c:choose>
  							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblPosicion}:</td>
					<td>
						<c:choose>
    						<c:when test="${beanResponse.trama == 'S'}">
								<input type="text" class="Campos" id="idPosicion" name="idPosicion" size="5" maxlength="5" value="${beanResponse.posicion}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idPosicion" name="idPosicion" size="5" maxlength="5" disabled value="${beanResponse.posicion}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblParamFalta}:</td>
					<td>
						<c:choose>
    						<c:when test="${beanResponse.trama == 'S'}">
								<input type="text" class="Campos" id="idParamFalta" name="idParamFalta" size="16" maxlength="16" value="${beanResponse.parametroFalta}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idParamFalta" name="idParamFalta" size="16" maxlength="16" disabled value="${beanResponse.parametroFalta}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">${lblPrograma}:</td>
					<td></td>
					<td width="1"></td>
					<td>
						<c:choose>
    						<c:when test="${beanResponse.cola == 'S'}">
								<input type="text" class="Campos" id="idPrograma" name="idPrograma" size="16" maxlength="16" value="${beanResponse.programa}"/>
    						</c:when>    
    						<c:otherwise>
								<input type="text" class="Campos" id="idPrograma" name="idPrograma" size="16" maxlength="16" disabled value="${beanResponse.programa}"/>
    						</c:otherwise>
   						</c:choose>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr>
					<td></td>
					<td></td>
					<td width="1"></td>
					<td></td>
					<td width="1"></td>
					<td class="text_derecha">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${btnBuscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${btnBuscar}</a></span>
							</c:otherwise>
						</c:choose>
						<span><a>${espacioEnBlanco}</a></span>
						<span><a id="idLimpiar" href="javascript:;">${btnLimpiar}</a></span>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<caption></caption>
				<tr>
					<th width="90" class="text_centro" scope="col" colspan="2">${lblCveParam}</th>
					<th width="90" class="text_centro" scope="col">${lblParamFalta}</th>
					<th width="90" class="text_centro" scope="col">${lblValorFalta}</th>
					<th width="90" class="text_centro" scope="col">${lbltipoParametro}</th>
					<th width="200" class="text_centro" scope="col">${lblDescripcion}</th>
				</tr>
				<tbody>				
					<c:forEach var="beanCatalogoParam" items="${beanResponse.listaParametros}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_centro">
								<input type="checkbox" id="seleccionado${rowCounter.index}" onchange="verificarSeleccion(${rowCounter.index})" name="listSelecParam[${rowCounter.index}].seleccionado" />
								<input type="hidden" name="listSelecParam[${rowCounter.index}].cveParametro" value="${beanCatalogoParam.cveParametro}"/>
							</td>
							<td class="text_centro">${beanCatalogoParam.cveParametro}</td>
							<td class="text_centro">${beanCatalogoParam.parametroFalta}</td>
							<td class="text_centro">${beanCatalogoParam.valorFalta}</td>
							<td class="text_centro">${beanCatalogoParam.tipoParametro}</td>
							<td class="text_centro">${beanCatalogoParam.descripParam}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<c:if test="${not empty beanResponse.listaParametros}">
			<jsp:include page="../paginador.jsp" >	  					
						<jsp:param name="paginaIni" value="${beanResponse.paginador.paginaIni}" />
						<jsp:param name="pagina" value="${beanResponse.paginador.pagina}" />
						<jsp:param name="paginaAnt" value="${beanResponse.paginador.paginaAnt}" />
						<jsp:param name="paginaFin" value="${beanResponse.paginador.paginaFin}" />
						<jsp:param name="servicio" value="consultarParametros.do" />
			</jsp:include>
		</c:if>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"AGREGAR\")}">
								<td width="279" class="izq"><a id="idAlta" href="javascript:;">${lnkAgregar}</a></td>
							</c:when>
							<c:otherwise>
								<td  width="279" class="izq_Des"><a href="javascript:;">${lnkAgregar}</a></td>
							</c:otherwise>
						</c:choose>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR') && not empty beanResponse.listaParametros}">
								<td width="279" class="der"><a id="idEditar" href="javascript:;">${lnkEditar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${lnkEditar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') && not empty beanResponse.listaParametros}">
								<td class="izq"><a id="idExportar" href="javascript:;">${lnkExportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${lnkExportar}</a></td>
							</c:otherwise>
						</c:choose>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"ELIMINAR\") && not empty beanResponse.listaParametros}">
								<td width="279" class="der"><a id="idEliminar" href="javascript:;">${lnkEliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${lnkEliminar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') && not empty beanResponse.listaParametros}">
								<td class="izq"><a id="idExportarTodo" href="javascript:;">${lnkExportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${lnkExportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="279" class="der_Des"><a href="javascript:;"></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>