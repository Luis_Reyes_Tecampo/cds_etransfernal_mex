<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatInstFinancieras" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.numBanxico" var="numBanxico"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.cveInterme" var="cveInterme"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.nombre" var="nombre"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.participantePOA" var="participantePOA"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.validarCuenta" var="validarCuenta"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.usuario" var="usuario"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.fechaMod" var="fechaMod"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.text.buscar" var="buscar"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.link.alta" var="alta"/>
<spring:message code="catalogos.consultaCatIntsFinancieras.link.eliminar" var="eliminar"/>




<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>


<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.CD00161V" var="CD00161V"/>
<spring:message code="codError.CD00162V" var="CD00162V"/>
<spring:message code="codError.CD00163V" var="CD00163V"/>
<spring:message code="codError.CD00164V" var="CD00164V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>


	 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"CD00010V":'${CD00010V}',
                        "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}', "CD00011V":'${CD00011V}'
                        ,"CD00161V":'${CD00161V}',"CD00162V":'${CD00162V}',"CD00163V":'${CD00163V}',"CD00164V":'${CD00164V}',"ED00029V":'${ED00029V}'
                        };
	 
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/catInstFinancieras.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>
 
	<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value=""/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResInstPOACOA.beanPaginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResInstPOACOA.beanPaginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResInstPOACOA.beanPaginador.paginaFin}"/>
		<input type="hidden" name="numBanxico" id="numBanxico" value="${beanResInstPOACOA.numBanxico}"/>
		<input type="hidden" name="nombre" id="nombre" value="${beanResInstPOACOA.nombre}"/>
		<input type="hidden" name="habDesNumBanxico" id="habDesNumBanxico" value=""/>
		<input type="hidden" name="habDesNombre" id="habDesNombre" value=""/>
		<input type="hidden" name="habDesValor" id="habDesValor" value=""/>
		


		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${subtituloFuncion}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
					${numBanxico}
					</td>
					<td>
						<input type="text" class="Campos" id="paramNumBanxico" name="paramNumBanxico"  size="15" maxlength="40"  value="${beanResInstPOACOA.numBanxico}"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
					${nombre}
					</td>
					<td>
						<input type="text" class="Campos" id="paramNombre" name="paramNombre"  size="40" maxlength="40"  value="${beanResInstPOACOA.nombre}"/>
					</td>
				</tr>
				<tr><td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" class="text_centro" scope="col" colspan="2">${numBanxico}</th>
					<th width="122" class="text_centro" scope="col">${cveInterme}</th>
					<th width="122" class="text_centro" scope="col">${nombre}</th>
					<th width="122" class="text_centro" scope="col">${participantePOA}</th>
					<th width="122" class="text_izquierda">${validarCuenta}</th>
					<th width="122" class="text_izquierda">${usuario}</th>
					<th width="122" class="text_izquierda">${fechaMod}</th>
				</tr>
				<tbody>				
					<c:forEach var="beanInstitucionPOACOA" items="${beanResInstPOACOA.listBeanInstitucionPOACOA}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">
								<input type="checkbox" name="listBeanInstitucionPOACOA[${rowCounter.index}].seleccionado" value="true"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].numBanxico" value="${beanInstitucionPOACOA.numBanxico}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].cveInterme" value="${beanInstitucionPOACOA.cveInterme}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].nombre" value="${beanInstitucionPOACOA.nombre}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].participantePOA" value="${beanInstitucionPOACOA.participantePOA}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].validarCuenta" value="${beanInstitucionPOACOA.validarCuenta}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].cveUsuario" value="${beanInstitucionPOACOA.cveUsuario}"/>
								<input type="hidden" name="listBeanInstitucionPOACOA[${rowCounter.index}].fechaMod" value="${beanInstitucionPOACOA.fechaMod}"/>
								
							</td>
							<td class="text_izquierda">${beanInstitucionPOACOA.numBanxico}</td>
							<td class="text_izquierda">${beanInstitucionPOACOA.cveInterme}</td>
							<td class="text_centro">${beanInstitucionPOACOA.nombre}</td>
							<td class="text_centro">
								<c:choose>
									<c:when test="${fn:containsIgnoreCase(searchString,\"MODIFICAR\")}">
										<input type="checkbox" onclick="catInstFinancieras.habilitarDeshabilitarPOACOA(${beanInstitucionPOACOA.numBanxico},'${beanInstitucionPOACOA.nombre}',${beanInstitucionPOACOA.participantePOA},this);" 
									${beanInstitucionPOACOA.participantePOA ==1? "checked":""}/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" onclick="catInstFinancieras.habilitarDeshabilitarPOACOA(${beanInstitucionPOACOA.numBanxico},'${beanInstitucionPOACOA.nombre}',${beanInstitucionPOACOA.participantePOA},this);" 
									${beanInstitucionPOACOA.participantePOA ==1? "checked":""} disabled="disabled"/>
									</c:otherwise>
								</c:choose>
							</td>
								
							<td class="text_centro">
								<c:choose>
									<c:when test="${fn:containsIgnoreCase(searchString,\"MODIFICAR\")}">
										<input type="checkbox" onclick="catInstFinancieras.habilitarDeshabilitarValidarCuenta(${beanInstitucionPOACOA.numBanxico},'${beanInstitucionPOACOA.nombre}',${beanInstitucionPOACOA.validarCuenta},this);"   
									${beanInstitucionPOACOA.validarCuenta   ==1? "checked":""}/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" onclick="catInstFinancieras.habilitarDeshabilitarValidarCuenta(${beanInstitucionPOACOA.numBanxico},'${beanInstitucionPOACOA.nombre}',${beanInstitucionPOACOA.validarCuenta},this);"   
									${beanInstitucionPOACOA.validarCuenta   ==1? "checked":""} disabled="disabled"/>
									</c:otherwise>
								</c:choose>
								</td>
							<td class="text_centro">${beanInstitucionPOACOA.cveUsuario}</td>
							<td class="text_centro">${beanInstitucionPOACOA.fechaMod}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanResInstPOACOA.listBeanInstitucionPOACOA}">
			<div class="paginador">
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaIni == beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaIni != beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarInstitucionesPOACOA.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaAnt!='0' && beanResInstPOACOA.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','buscarInstitucionesPOACOA.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaAnt=='0' || beanResInstPOACOA.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResInstPOACOA.beanPaginador.pagina} - ${beanResInstPOACOA.beanPaginador.paginaFin}</label>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaFin != beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarInstitucionesPOACOA.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaFin == beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaFin != beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','buscarInstitucionesPOACOA.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResInstPOACOA.beanPaginador.paginaFin == beanResInstPOACOA.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>

		</div>

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"AGREGAR\")}">
								<td width="279" class="izq"><a id="idAlta" href="javascript:;">${alta}</a></td>
							</c:when>
							<c:otherwise>
								<td  width="279" class="izq_Des"><a href="javascript:;">${alta}</a></td>
							</c:otherwise>
						</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,\"ELIMINAR\")}">
									<td width="279" class="der"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
								</c:otherwise>
							</c:choose>
					</tr>
				</table>

			</div>
		</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>