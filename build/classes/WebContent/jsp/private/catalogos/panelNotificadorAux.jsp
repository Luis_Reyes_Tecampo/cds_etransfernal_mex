<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:choose>
	<c:when
		test="${fn:containsIgnoreCase(searchString,'ELIMINAR') and not empty beanNotificaciones.notificaciones}">
		<td class="der widthx279"><a id="btnEliminar" href="javascript:;">${lblEliminar}</a></td>
	</c:when>
	<c:otherwise>
		<td class="der_Des widthx279"><a href="javascript:;">${lblEliminar}</a></td>
	</c:otherwise>
</c:choose>