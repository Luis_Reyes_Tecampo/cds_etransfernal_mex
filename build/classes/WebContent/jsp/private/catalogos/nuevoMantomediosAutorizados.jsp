<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="catalogos.menuCatalogos.txt.txtModuloSPEI" var="tituloModulo"/>
<spring:message code="catalogos.menuCatalogos.txt.txtMantoMedios" var="tituloFuncionalidad"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveMedioEntrega" var="ClaveMedioEntrega"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescripcionMedioE" var="DescripcionMedioE"/>
<spring:message code="catalogos.mantoMedios.txt.txtDosPuntosHelper" var="DosPuntosHelper"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveTransfer" var="ClaveTransfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveOperacion" var="ClaveOperacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtCLACON" var="CLACON"/>
<spring:message code="catalogos.mantoMedios.txt.txtSucursalOperan" var="SucursalOperan"/>
<spring:message code="catalogos.mantoMedios.txt.txtHorarioMay" var="HorarioMay"/>
<spring:message code="catalogos.mantoMedios.txt.txtHorarioMin" var="HorarioMin"/>
<spring:message code="catalogos.mantoMedios.txt.txtInicio" var="Inicio"/>
<spring:message code="catalogos.mantoMedios.txt.txtCierre" var="Cierre"/>
<spring:message code="catalogos.mantoMedios.txt.txtInhabPregunta" var="InhabP"/>
<spring:message code="catalogos.mantoMedios.txt.txtCLACONTEF" var="CLACONTEF"/>
<spring:message code="catalogos.mantoMedios.txt.txtMecanismo" var="Mecanismo"/>
<spring:message code="catalogos.mantoMedios.txt.txtLimiteImporte" var="LimiteImporte"/>
<spring:message code="catalogos.mantoMedios.txt.txtNivel" var="Canal"/>
<spring:message code="catalogos.mantoMedios.txt.txtOperacion" var="Operacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtSucOperacion" var="SucOperacion"/>
<spring:message code="catalogos.mantoMedios.txt.txtSucOperante" var="SucOperante"/>
<spring:message code="catalogos.mantoMedios.txt.txtInHabN" var="InHabN"/>
<spring:message code="catalogos.mantoMedios.txt.txtAlta" var="tituloFuncionalidad"/>
<spring:message code="catalogos.mantoMedios.txt.txtModificar" var="tituloFuncionalidadMod"/>
<spring:message code="catalogos.mantoMedios.txt.txtAbierto" var="abierto"/>
<spring:message code="catalogos.mantoMedios.txt.txtCerrado" var="cerrado"/>
<spring:message code="general.eliminar" var="eliminar"/>
<spring:message code="general.asteriscoHelper" var="asteriscoHelper"/>
<spring:message code="general.guardar" var="guardar"/>
<spring:message code="general.cancelar" var="cancelar"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescripcionTrasfer" var="DescripcionTrasfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtNoSeguro" var="NoSeguro"/>
<spring:message code="catalogos.mantoMedios.txt.txtTiempo" var="Tiempo"/>
<spring:message code="catalogos.mantoMedios.txt.txtSi" var="Si"/>
<spring:message code="catalogos.mantoMedios.txt.txtNo" var="No"/>
<spring:message code="catalogos.mantoMedios.txt.txtSeguro" var="Seguro"/>
<spring:message code="catalogos.mantoMedios.txt.txtClaveTransfer" var="ClaveTransfer"/>
<spring:message code="catalogos.mantoMedios.txt.txtDescOperacion" var="DescOperacion"/>
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na" var="na" />

<jsp:include page="../myHeader.jsp" flush="true"/> 
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="mantenimientoMediosAutorizados" />
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" >
 <script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
 <script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mttoMediosAutorizados.js" type="text/javascript"></script>

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00130V" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',"ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}'  };</script>

<div class="pageTitleContainer">
	<c:if test="${accion eq 'INSERT'}">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</c:if>
	<c:if test="${accion eq 'UPDATE'}">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidadMod}
	</c:if>
</div>
<form id="mform" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">
			<c:if test="${accion eq 'INSERT'}">
				${tituloFuncionalidad}
			</c:if>
			<c:if test="${accion eq 'UPDATE'}">
				${tituloFuncionalidadMod}
			</c:if>
		</div>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" id="version" name="version" value="!" /> <input
			type="hidden" name="paginaIni" id="paginaIni"
			value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.accion}">
		<input type="hidden" id="accion" name="accion" value="${accion}" />
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${ClaveMedioEntrega}${DosPuntosHelper}</td>
					<td><label for="paramClaveMedioEntrega"></label><select class="Campos comboDes required objFrm focusCombo"
						id="paramClaveMedioEntrega" name="cveMedioEnt" style="width: 510px">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${lstMedEntrega}">
									<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveMedioEnt?"selected":""}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select> <c:if test="${tipo eq '2'}">
							<input type="hidden" name="cveMedioEnt"
								value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveMedioEnt}" />
						</c:if></td>
				</tr>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${ClaveTransfer}${DosPuntosHelper}</td>
					<td><label for="paramClaveTrasfer"></label><select class="Campos comboDes required objFrm focusCombo"
						id="paramClaveTrasfer" name="cveTransfe" style="width: 510px">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${lstTransferencias}">
									<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveTransfe ? 'selected' : ''}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select> <c:if test="${tipo eq '2'}">
							<input type="hidden" name="cveTransfe" size="20"
								value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveTransfe}" />
						</c:if></td>
				</tr>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${ClaveOperacion}${DosPuntosHelper}</td>
					<td><label for="paramClaveOperacion"></label><select class="Campos comboDes required objFrm focusCombo"
						id="paramClaveOperacion" name="cveOperacion" style="width: 510px">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${lstOperaciones}">
									<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveOperacion ? 'selected' : ''}>${reg.cve} - ${reg.descripcion}</option>
							</c:forEach>
					</select></td>
					<c:if test="${tipo eq '2'}">
						<input type="hidden" name="cveOperacion" size="20"
							value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveOperacion}" />
					</c:if>
				</tr>
			</table>
			<table>
			<caption></caption>
				<tr>
					<td class="text_izquierda">${CLACON}${DosPuntosHelper}</td>
					<td><label for="paramCLACON"></label><input type="text" class="Campos objFrm onlyNumero" size="20"
						maxlength="4" id="paramCLACON" name="cveCLACON"
						value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveCLACON}" /></td>
					<td class="text_izquierda">${CLACONTEF}${DosPuntosHelper}</td>
					<td><label for="paramCLACONTEF"></label><input type="text" class="Campos objFrm onlyNumero" size="20"
						maxlength="4" id="paramCLACONTEF" name="cveCLACONTEF"
						value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveCLACONTEF}" /></td>
				</tr>
				<tr>
					<td class="text_izquierda">${InhabP}${DosPuntosHelper}</td>
					<td><label for="paramInHabN"></label><select class="Campos objFrm" id="paramInHabN"
						name="flgInHab" style="width: 140px">
							<c:forEach var="reg" items="${lstInHab}">
								<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.flgInHab ? 'selected' : ''}>${reg.descripcion}</option>
							</c:forEach>
					</select></td>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${Mecanismo}${DosPuntosHelper}</td>
					<td><label for="paramMecanismo"></label><select class="Campos comboDes objFrm focusCombo required"
						id="paramMecanismo" name="cveMecanismo" style="width: 140px">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${lstMecanismos}">
								<option value="${reg.cve}[${reg.descripcion}]"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveMecanismo ? 'selected' : ''}>${reg.cve}</option>
							</c:forEach>
					</select></td>
					<c:if test="${tipo eq '2'}">
						<input type="hidden" name="cveMecanismo" size="20"
							value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.cveMecanismo}" />
					</c:if>
				</tr>
				<tr>
					<td class="text_izquierda">${Canal}${DosPuntosHelper}</td>
					<td><label for="paramNivelSeguridad"></label><select class="Campos objFrm" id="paramNivelSeguridad"
						name="canal" style="width: 140px">
							<c:forEach var="reg" items="${lstCanales}">
								<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.canal ? 'selected' : ''}>${reg.descripcion}</option>
							</c:forEach>
					</select></td>
					<td class="text_izquierda">${LimiteImporte}${DosPuntosHelper}</td>
						<td><label for="paramLimiteImporte"></label><input type="text" class="Campos objFrm onlyDecimalNEgativos" size="20"
							id="paramLimiteImporte" name="limiteImporte" maxlength="18"
							value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.limiteImporte}" /></td>
				</tr>
				<tr>
					<td class="text_izquierda">${SucOperante}${DosPuntosHelper}</td>
					<td><label for="paramSucOperante"></label><input type="text" class="Campos objFrm onlyNumero" size="20"
						maxlength="7" id="paramSucOperante" name="sucOperante"
						value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.sucOperante}" />
					</td>
				</tr>

			</table>
		</div>
	</div>
	<div class="frameBuscadorSimple espaciadoFrame">
		<div class="titleBuscadorSimple">${HorarioMin}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tr>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${Inicio}${DosPuntosHelper}</td>
					<td><label for="paramInicio"></label><input type="text"
						class="Campos required objFrm onlyNumero" maxlength="4" size="20"
						id="paramInicio" name="horaInicio"
						value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.horaInicio}" /> ${Tiempo}</td>
					<td class="text_izquierda"><span class="asterisco">${asteriscoHelper}</span>${Cierre}${DosPuntosHelper}</td>
					<td><label for="paramCierre"></label><input type="text"
						class="Campos required objFrm onlyNumero" maxlength="4" size="20"
						id="paramCierre" name="horaCierre"
						value="${beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.horaCierre}" /> ${Tiempo}</td>
				</tr>
				<tr>
					<td class="text_izquierda">${HorarioMin}</td>
					<td><label for="paramHorario"></label><select class="Campos objFrm" id="paramHorario"
						name="horarioEstatus" style="width: 140px">
							<option value="">${na}</option>
							<c:forEach var="reg" items="${lstHorarios}">
								<option value="${reg.cve}"
									${reg.cve eq beanMttoMediosAutorizadosRes.beanMttoMediosAutoReq.horarioEstatus ? 'selected' : ''}>${reg.descripcion}</option>
							</c:forEach>
					</select></td>
				</tr>
			</table>
		</div>
		<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
				<caption></caption>
				<tr>
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString, 'AGREGAR')}"> -->
						<td  class="izq widthx279"><a id="idGuardar"
								href="javascript:;">${guardar}</a></td>
						</c:when>
						<c:otherwise>
							<td  class="izq widthx279"><a id="idGuardar"
								href="javascript:;">${guardar}</a></td>
						</c:otherwise>
					</c:choose>
					
				    <td  class="odd widthx6"> &nbsp;</td>
					<td  class="der widthx279"><a id="idRegresar" href="${pageContext.servletContext.contextPath}/catalogos/mantenimientoMediosAutorizados.do">${cancelar}</a></td>
				</tr>		 
			</table>
		</div>
	</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<c:if test="${mensaje ne false}">
		<script type="text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
	</c:if>
	<c:if test="${disabled eq true}">
		<script type="text/javascript" defer="defer">desactivarCombos();</script>
	</c:if>
</c:if>