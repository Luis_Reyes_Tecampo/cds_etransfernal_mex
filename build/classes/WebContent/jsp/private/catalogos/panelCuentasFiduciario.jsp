<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="framePieContenedor">
	<div class="contentPieContenedor">
		<table>
			<caption></caption>

			<tr>
				<c:choose>
					<c:when
						test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanCuentas.cuentas}">
						<td class="izq widthx279"><a id="btnExportar"
							href="javascript:;">${lblExportar}</a></td>
					</c:when>
					<c:otherwise>
						<td class="izq_Des widthx279"><a href="javascript:;">${lblExportar}</a></td>
					</c:otherwise>
				</c:choose>
				<td class="odd widthx6">${espacioEnBlanco}</td>
				<td class="der widthx279"><a id="btnActualizar"
					href="javascript:;">${lblActualizar}</a></td>
			</tr>
			<tr>
				<c:choose>
					<c:when
						test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanCuentas.cuentas}">
						<td class="izq widthx279"><a id="btnExportarTodo"
							href="javascript:;">${lblExportarTodo}</a></td>
					</c:when>
					<c:otherwise>
						<td class="izq_Des widthx279"><a href="javascript:;">${lblExportarTodo}</a></td>
					</c:otherwise>
				</c:choose>
				<td class="odd widthx6">${espacioEnBlanco}</td>
				<td class="hidden"><a id="" href="javascript:;"></a></td>
			</tr>

		</table>
	</div>
</div>