<%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<jsp:include page="../myHeader.jsp"/>
<jsp:include page="../myMenuCatalogos.jsp">
	 <jsp:param name="menuItem"    value="catalogos" />
	 <jsp:param value="consultaPayments" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/ConsultaPayments.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<spring:message code="general.bienvenido"       					var="welcome"/>
<spring:message code="general.nombreAplicacion" 					var="app"/>
<spring:message code="general.espacioEnBlanco" 						var="espacioEnBlanco"/>

<spring:message code="general.guardar" 								var="lblGuardar"/>
<spring:message code="general.cancelar" 							var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 						var="espacioEnBlanco"/>

<spring:message code="general.inicio" 								var="inicio"/>
<spring:message code="general.fin" 									var="fin"/>
<spring:message code="general.anterior" 							var="anterior"/>
<spring:message code="general.siguiente" 							var="siguiente"/>

<spring:message code="catalogos.payments.exportar"          	  	var="lblExportar"/>
<spring:message code="catalogos.payments.exportarTodo"     		   	var="lblExportartodo"/>
<spring:message code="catalogos.payments.buscar"           			var="lblBuscar"/>
<spring:message code="catalogos.payments.modificar"         		var="lblModificar"/>

<spring:message code="catalogos.payments.filtroBusqueda"			var="lblFiltroBusqueda" />
<spring:message code="catalogos.payments.filtro"					var="lblFiltro" />
<spring:message code="catalogos.payments.seleccioneOpcion"			var="lblSeleccioneOpcion" />
<spring:message code="catalogos.payments.seleccionarOpc"			var="seleccionarOpc" />
<spring:message code="catalogos.payments.seleccionarTodos"			var="seleccionarTodos" />
<spring:message code="catalogos.payments.seleccionarActivado"		var="seleccionarActivado" />
<spring:message code="catalogos.payments.seleccionarDesactivado"	var="seleccionarDesactivado" />



<spring:message code="catalogos.payments.clave" 					var="lblClave" />
<spring:message code="catalogos.payments.descripcion"				var="lblDescripcion" />
<spring:message code="catalogos.payments.actdes"					var="lblActDes" />

<spring:message code="catalogos.payments.mediosEntrega"				var="lblMediosEntrega" />
<spring:message code="catalogos.payments.actdesCanal"				var="lblActDesCanal" />
<spring:message code="catalogos.payments.aceptar"					var="lblAceptar" />
<spring:message code="catalogos.payments.cancelar"					var="lblCancelar" />

<spring:message code="catalogos.general.text.tituloModulo" 			var="tituloModulo"/>
<spring:message code="catalogos.payments.titulo"					var="tituloFuncionalidad" />

<spring:message code="codError.ED00011V" var="ED00011V" />
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.OK00001V" var="OK00001V" />
<spring:message code="codError.OK00002V" var="OK00002V" />
<spring:message code="codError.ED00068V" var="ED00068V" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.ED00029V" var="ED00029V" />
<spring:message code="codError.ED00022V" var="ED00022V" />
<spring:message code="codError.ED00023V" var="ED00023V" />
<spring:message code="codError.ED00027V" var="ED00027V" />
<spring:message code="codError.ED00086V" var="ED00086V" />
<spring:message code="codError.ED00126V" var="ED00126V" />
<spring:message code="codError.OK00020V" var="OK00020V" />


<script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};</script>


<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> ${tituloFuncionalidad}
</div>

<form id="mform" method="post">
	<input type="hidden" name="accion" id="accion" value="${e:forHtmlAttribute(accion}">	
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> ${tituloFuncionalidad}
	</div>
	<div class="frameTablaVariasColumnas">	
		<div class="contentBuscadorSimple">
			<table class="table-cons table" id="tblConsulta">
				<caption></caption>
				<tr>
					<td class="text_izquierda" style="width: 150px"><label for="clave">${lblMediosEntrega}</label></td>
					<td><label for="claveMed"></label><select id="claveMed" name="clave"  disabled=true>
						<option value="">${seleccionarOpc}</option>
						<c:forEach var="registro" items="${beanConsultasPayme.medEntrega}">
							<option value="${registro.detalles}" ${registro.detalles eq beanConsultaPayme.detalles ? "selected" : "" }>${registro.detalles}</option>
						</c:forEach>
					</select>
					<input type="hidden"  id="clave" name="clave" value="${e:forHtmlAttribute(beanConsultaPayme.clave)}">	
					</td>
					
				</tr>
				
				
				<tr>
					<td class="text_izquierda" style="width: 150px"><label for="tipoOperacion">${lblActDesCanal}</label></td>
					
					<td><label for="actDes"></label><select id="actDes" name="actDes">
						<option value="ACTIVADO"	${"ACTIVADO" eq beanConsultaPayme.actDes ? "selected" : ""}>		${seleccionarActivado}</option>
						<option value="DESACTIVADO"	${"DESACTIVADO" eq beanConsultaPayme.actDes ? "selected" : ""}> 	${seleccionarDesactivado}</option>
					</select></td>
				</tr>
			</table>
		</div>
	</div>	
	
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
				<caption></caption>
				<tr>
					<td class="izq"><a id="idGuardar" href="javascript:;">${lblAceptar}</a></td>
					<td class="odd"> &nbsp;</td>
					<td class="der"><a id="idRegresar" href="javascript:;">${lblCancelar}</a></td>
				</tr>
			</table>
		</div>
	</div>
		
	
</form>
