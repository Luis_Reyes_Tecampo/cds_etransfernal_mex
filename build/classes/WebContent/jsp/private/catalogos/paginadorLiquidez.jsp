<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanLiquidez.listaBeanLiquidezIntradia}"> 
			<div class="paginador"> 
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${beanLiquidez.beanPaginador.regIni} al ${beanLiquidez.beanPaginador.regFin} de un total de ${beanLiquidez.totalReg} registros</label>
				</div>
				<c:if test="${beanLiquidez.beanPaginador.paginaIni == beanLiquidez.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaIni != beanLiquidez.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatLiquidezIntradia.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaAnt!='0' && beanLiquidez.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatLiquidezIntradia.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaAnt=='0' || beanLiquidez.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanLiquidez.beanPaginador.pagina} - ${beanLiquidez.beanPaginador.paginaFin}</label>
				<c:if test="${beanLiquidez.beanPaginador.paginaFin != beanLiquidez.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatLiquidezIntradia.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaFin == beanLiquidez.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaFin != beanLiquidez.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatLiquidezIntradia.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanLiquidez.beanPaginador.paginaFin == beanLiquidez.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
		</c:if>