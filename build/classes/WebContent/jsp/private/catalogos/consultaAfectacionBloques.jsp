<%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="afectacionBloques" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script
	src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/global.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/utilerias.js"
	type="text/javascript"></script> 
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/js/private/catalogos/afectacionBloques.js"
	type="text/javascript"></script>
<%-- Inicio de llamado de datos de Properties --%>
<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="catalogos.afectacionBloques.titulo"var="tituloFuncionalidad" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco" />
<%-- Opciones de la Pantalla --%>
<spring:message code="catalogos.afectacionBloques.titulo" var="afectacionBloques"/>
<spring:message code="catalogos.afectacionBloques.lblNumBloque"var="lblNumBloque" />
<spring:message code="catalogos.afectacionBloques.lblValor"var="lblValor" />
<spring:message code="catalogos.afectacionBloques.lblDescripcion"var="lblDescripcion" />
<spring:message code="catalogos.afectacionBloques.lblAgregar"var="lblAgregar" />
<spring:message code="catalogos.afectacionBloques.lblActualizar"var="lblActualizar" />
<spring:message code="catalogos.afectacionBloques.lblExportar"var="lblExportar" />
<spring:message code="catalogos.afectacionBloques.lblExportarTodo"var="lblExportarTodo" />
<spring:message code="catalogos.afectacionBloques.lblEditar"var="lblEditar" />
<spring:message code="catalogos.afectacionBloques.lblEliminar"var="lblEliminar" />
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<%-- Constantes de Error --%>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>

<%-- Fin de Variables Constantes --%>
<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"ED00000V":'${ED00000V}',"OK00001V":'${OK00001V}',"OK00002V":'${OK00002V}',"ED00068V":'${ED00068V}',"EC00011B":'${EC00011B}',"ED00126V":'${ED00126V}',"CD00010V":'${CD00010V}',"ED00029V":'${ED00029V}',"ED00021V":'${ED00021V}',"ED00086V":'${ED00086V}',"ED00171V":'${ED00171V}',"ED00181V":'${ED00181V}',"ED00191V":'${ED00191V}',"OK00020V":'${OK00020V}'};</script>
<%-- Fin de Variables Constantes --%>
 
<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
</div>
<c:set var="searchString" value="${seguTareas}" />
<form id="idForm" name="idForm" method="post" action="">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="pagina" id="pagina"
		value="${beanBloques.beanPaginador.pagina}"> <input
		type="hidden" name="paginaIni" id="paginaIni"
		value="${beanBloques.beanPaginador.paginaIni}"> <input
		type="hidden" name="paginaFin" id="paginaFin"
		value="${beanBloques.beanPaginador.paginaFin}"> <input
		type="hidden" name="field" id="field"
		value="${beanBloques.beanFilter.field}"> <input
		type="hidden" name="fieldValue" id="fieldValue"
		value="${beanBloques.beanFilter.fieldValue}"> <input
		type="hidden" name="accion" id="accion" value=""> <input
		type="hidden" name="opcion" id="opcion" value="">
  
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloFuncionalidad}</div>
		<div class="contentTablaVariasColumnas">
			<table class="table-cons table" id="tblConsulta">
				<caption></caption>
				<thead>
					<tr>
						<th class="text_centro filField" scope="col">${lblNumBloque}<br/>
						<label for="numBloqueInput"></label>
						<input type="text" id="numBloqueInput" name="numBloque" class="filField"
							data-field="NUM_BLOQUE" maxlength="5" value="${beanFilter.numBloque}">
						</th>
						<th class="text_centro filField valid-letrasNum" scope="col">${lblValor}<br />
						<label for="valorInput"></label>
						<input type="text" id="valorInput" name="valor"
							data-field="VALOR" class="filField" maxlength="30"
							value="${beanFilter.valor}" />
						</th>
						<th class="text_centro filField valid-letrasNum" scope="col">${lblDescripcion}<br/>
						<label for="descripcionInput"></label>
						<input type="text" id="descripcionInput" name="descripcion"
							data-field="DESCRIPCION" class="filField" maxlength="60"
							value="${beanFilter.descripcion}" />
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="bloque"
						items="${beanBloques.bloques}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td><label for="check"> <input type="checkbox" id="check" name="bloques[${rowCounter.index}].seleccionado" onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}" /> 
							${bloque.numBloque} 
							<input type="hidden" name="bloques[${rowCounter.index}].numBloque" id="numBloque" value="${bloque.numBloque}" /> 
							<input type="hidden" name="bloques[${rowCounter.index}].valor" id="valor" value="${bloque.valor}" /> 
							<input type="hidden" name="bloques[${rowCounter.index}].descripcion" id="descripcionB" value="${bloque.descripcion}" />
							</label></td>
							<td class="text_centro">${bloque.valor}</td>
							<td class="text_centro">${bloque.descripcion}</td>
						</tr>
					</c:forEach>
					<tr style="display: none;" id="noresults">
						<td>No Hay Resultados</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%@ include file="navegacionAfectacionBloques.jsp" %>
		<%@ include file="panelAfectacionBloques.jsp" %>
	</div>
</form>

<c:if test="${codError!=''}">
<script type="text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
</c:if>
