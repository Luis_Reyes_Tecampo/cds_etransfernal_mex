<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
				<tr>
					<td>${COA}</td>
					<td></td>
					<c:choose>
						<c:when test="${esActivoCOA}">
						<c:choose>
							<c:when test="${modulo eq 'spid'}">
							<input id="procesoCOAParaPOA" name="procesoCOAParaPOA" type="hidden" value="0" />
							<td><span><a id="idActivarCOA" href="javascript:activarCOA('spid');">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:when>
							<c:otherwise>
							<input id="procesoCOAParaPOA" name="procesoCOAParaPOA" type="hidden" value="0" />
							<td><span><a id="idActivarCOA" href="javascript:activarCOA('actual');">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<input id="procesoCOAParaPOA" name="procesoCOAParaPOA" type="hidden" value="1" />
							<td><span class="btn_Des"><a id="idActivarCOA" href="javascript:;">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
						</c:otherwise>
					</c:choose>
					<td></td>
					<c:choose>
						<c:when test="${esdesActivacionCOA}">
						<c:choose>
						<c:when test="${modulo eq 'spid'}">
							<td><span><a id="idDesactivarCOA" href="javascript:desactivarCOA('spid');">${txtDesactivar}</a></span></td>
							</c:when>
							<c:otherwise>
							<td><span><a id="idDesactivarCOA" href="javascript:desactivarCOA('actual');">${txtDesactivar}</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td><span class="btn_Des"><a id="idDesactivarCOA" href="javascript:;">${txtDesactivar}</a></span></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
