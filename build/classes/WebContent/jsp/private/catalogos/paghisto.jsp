<%-- 
  * ------------------------------------------------------------------------------------------
  * Isban Mexico
  * paghisto.jsp
  * Descripcion: Archivo de definicion de la pantalla "Catalogo Intermediario - Vigente/Historico".
  *
  * Version  Date        By                                         Description
  * -------  ----------  ---------------------------------          ------------------------------------------
  * 1.0      24/12/2019  [1565817: Alberto Olvera Juarez]           Creacion de JSP Pantalla de Catalogo Intermediario
  * ------------------------------------------------------------------------------------------
 --%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<div class="paginador">	
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaIni == beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaIni != beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatIntFinancierosHis.do','INI');">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaAnt!='0' && beanResIntFinancierosHist.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatIntFinancierosHis.do','ANT');">&lt;${anterior}</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaAnt=='0' || beanResIntFinancierosHist.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
	<label id="txtPagina">${beanResIntFinancierosHist.beanPaginador.pagina} - ${beanResIntFinancierosHist.beanPaginador.paginaFin}</label>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaFin != beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatIntFinancierosHis.do','SIG');">${siguiente}&gt;</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaFin == beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaFin != beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaCatIntFinancierosHis.do','FIN');">${fin}&gt;&gt;</a></c:if>
	<c:if test="${beanResIntFinancierosHist.beanPaginador.paginaFin == beanResIntFinancierosHist.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
</div>