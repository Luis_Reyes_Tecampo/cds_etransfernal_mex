<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

				 
	<%@ include file="parametrosPOACOAArch2.jsp" %>
				<tr>
					<td></td>
					<td></td>
					<c:choose>
							<c:when test="${esActGenDev}">
							<c:choose>
							<c:when test="${modulo eq 'spid'}">
								<td colspan="2"><span><a id="idGenerarDevoluciones" class="spid" href="javascript:;">${txtGenDev}</a></span></td>
								</c:when>
								<c:otherwise>
								<td colspan="2"><span><a id="idGenerarDevoluciones" class="actual" href="javascript:;">${txtGenDev}</a></span></td>
								</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<td colspan="2"><span class="btn_Des"><a href="javascript:;">${txtGenDev}</a></span></td>
							</c:otherwise>
						</c:choose>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<c:choose>
							<c:when test="${esActGenTras}">
							<c:choose>
							<c:when test="${modulo eq 'spid'}">
								<td colspan="2"><span><a id="idGenerarTranspasos" class="spid" href="javascript:;">${txtGenTras}</a></span></td>
								</c:when>
								<c:otherwise>
								<td colspan="2"><span><a id="idGenerarTranspasos" class="actual" href="javascript:;">${txtGenTras}</a></span></td>
								</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<td colspan="2"><span class="btn_Des"><a href="javascript:;">${txtGenTras}</a></span></td>
							</c:otherwise>
						</c:choose>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>