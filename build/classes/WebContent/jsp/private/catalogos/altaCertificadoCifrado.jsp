<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="certifCifrado" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/certificadosCifrado.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<!-- Inicio de llamado de datos de Properties -->
<spring:message code="general.nombreAplicacion" 						var="app"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.eliminar" 								var="lblEliminar"/>
<spring:message code="general.guardar" 									var="lblGuardar"/>
<spring:message code="general.cancelar" 								var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="catalogos.certificado.cifrado.cveMedEntrega" 		var="lblCveMedEntrega"/>
<spring:message code="catalogos.certificado.cifrado.descMedEntrega" 	var="lblDescMedEntrega"/>
<spring:message code="catalogos.certificado.cifrado.numCertif" 			var="lblNumCertif"/>
<spring:message code="catalogos.certificado.cifrado.cveCertif" 			var="lblCveCertificado"/>
<spring:message code="catalogos.certificado.cifrado.ptrFirma" 			var="lblPtrFirma"/>
<spring:message code="catalogos.certificado.cifrado.ptrCifrado" 		var="lblPtrCifrado"/>
<spring:message code="catalogos.certificado.cifrado.estatus" 			var="lblEstatus"/>
<spring:message code="catalogos.general.text.tituloModulo" 				var="tituloModulo"/>
<spring:message code="catalogos.menuCatalogos.txt.txtMantoCertificados" var="tituloFuncionalidad"/>

<spring:message code="catalogos.certificadosCifrado.canal" 				var="lblCanal"/>
<spring:message code="catalogos.certificadosCifrado.alias" 				var="lblAlias"/>
<spring:message code="catalogos.certificadosCifrado.descripcionCanal" 	var="lblDescripcionCanal"/>
<spring:message code="catalogos.certificadosCifrado.numero" 			var="lblNumero"/>
<spring:message code="catalogos.certificadosCifrado.tecnologiaOrigen" 	var="lblTecnologia"/>
<spring:message code="catalogos.certificadosCifrado.firma" 				var="lblFirma"/>
<spring:message code="catalogos.certificadosCifrado.cifrado" 			var="lblCifrado"/>
<spring:message code="catalogos.certificadosCifrado.estatus" 			var="lblEstatus"/>
<spring:message code="catalogos.certificadosCifrado.vigencia" 			var="lblVigencia"/>
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na"           var="na" />
<spring:message code="general.asteriscoHelper"                          var="asteriscoHelper" />
<spring:message code="general.dosPuntosHelper"                          var="DosPuntosHelper" />

<spring:message code="catalogos.certificadosCifrado.alta" 				var="alta"/>
<spring:message code="catalogos.certificadosCifrado.modificar" 			var="modificar"/>

<!-- Fin de Variables Constantes -->

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00130V" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',
           "ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}'
        	   ,"ED00029V":'${ED00029V}'  };
</script>

 <form id="mform" method="post">
 	<input type="hidden" name="accion" id="accion" value="${accion}">
 	<input type="hidden" name="canal" id="canal" value="${beanCertificadoCifrado.canal}" ${accion eq 'alta' ? 'disabled="disabled"' : ''}>
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloFuncionalidad}</span>
	</div>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${tituloFuncionalidad}</div>
		<div class="contentBuscadorSimple">
	        <div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblCanal}${DosPuntosHelper}
			    </label>
			    <select id="paramCanal" name="canal" class="objFrm focusCombo Campos required" ${accion eq 'alta' ? '' : 'disabled="disabled"'}>
					<option value="">${na}</option>
					<c:forEach var="registro" items="${listas.listaCanales}">
						<option value="${registro.cve}[${registro.descripcion}]"
							${registro.cve eq beanCertificadoCifrado.canal ? "selected" : ""}>${registro.cve}</option>
					</c:forEach>
				</select>
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblAlias}${DosPuntosHelper}
			    </label>
			    <input type="text" id="paramAlias" name="alias" maxlength="20" value="${beanCertificadoCifrado.alias}" class="letrasNum objFrm Campos required" />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblNumero}${DosPuntosHelper}
			    </label>
			    <input type="text" name="numCertificado" id="paramNumeroCertificado" maxlength="20" value="${beanCertificadoCifrado.numCertificado}" class="letrasNum objFrm Campos required" />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        ${lblVigencia}${DosPuntosHelper}
			    </label>
			   <input id="fchVencimiento" name="fchVencimiento" class="Campos" type="text" size="14" readonly="readonly" value="${beanCertificadoCifrado.fchVencimiento}"/>
							<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
				<label>${beanCertificadoCifrado.estatus}</label>
				<script type="text/javascript">
					createCalendarGuion('fchVencimiento', 'cal1');
					obtenerDescripcion();
				</script>
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${lblFirma}${DosPuntosHelper}
			    </label>
			    <select id="paramFirma" name="algDigest" class="objFrm focusCombo Campos required">
					<option value="">${na}</option>
					<c:forEach var="registro" items="${listas.listaFirmas}" varStatus="rowCounter">
						<c:choose>
							<c:when test="${registro.cve == beanCertificadoCifrado.algDigest}">
								<option value="${registro.cve}" selected="selected">${registro.descripcion}</option>
							</c:when>
							<c:otherwise>
								<option value="${registro.cve}">${registro.descripcion}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			    
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        ${lblCifrado}${DosPuntosHelper}
			    </label>
			    
			    <select id="paramCifrado" name="algSimetrico" class="objFrm focusCombo Campos">
					<option value="">${na}</option>
					<c:forEach var="registro" items="${listas.listaCifrados}" varStatus="rowCounter">
						<c:choose>
							<c:when test="${registro.cve == beanCertificadoCifrado.algSimetrico}">
								<option value="${registro.cve}" selected="selected">${registro.descripcion}</option>
							</c:when>
							<c:otherwise>
								<option value="${registro.cve}">${registro.descripcion}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			    
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        ${lblTecnologia}${DosPuntosHelper}
			    </label>
			    <select id="paramTecnologia" name="tecOrigen" class="objFrm focusCombo Campos">
					<option value="">${na}</option>
					<c:forEach var="registro" items="${listas.listaTecnologias}" varStatus="rowCounter">
						<c:choose>
							<c:when test="${registro.cve == beanCertificadoCifrado.tecOrigen}">
								<option value="${registro.cve}" selected="selected">${registro.descripcion}</option>
							</c:when>
							<c:otherwise>
								<option value="${registro.cve}">${registro.descripcion}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</div>
	    </div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		             <tr>
		                 <td class="izq">
		                     <a id="btnGuardar" href="javascript:;" >${lblGuardar}</a>
		                 </td>
		                 <td class="odd">${espacioEnBlanco}</td>
		                 <td class="der">
		                     <a id="btnReturn" href="${pageContext.servletContext.contextPath}/catalogos/consultaCertificadosCifrado.do" >${lblRegresar}</a>
		                 </td>
		             </tr>
		        </table>
		    </div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>