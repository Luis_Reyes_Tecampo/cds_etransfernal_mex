<%-- 
  * ------------------------------------------------------------------------------------------
  * Isban Mexico
  * catIntFinancierosHis.jsp
  * Descripcion: Archivo de definicion de la pantalla "Catalogo Intermediario - Vigente/Historico".
  *
  * Version  Date        By                                         Description
  * -------  ----------  ---------------------------------          ------------------------------------------
  * 1.0      24/12/2019  [1565817: Alberto Olvera Juarez]           Creacion de JSP Pantalla de Catalogo Intermediario
  * ------------------------------------------------------------------------------------------
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<%@ include file="../myHeader.jsp" %>
<jsp:include   page="../myMenuSubCatalagos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatIntFinancierosHis" />
</jsp:include>

<spring:message code="catalogos.consultaCatIntFinancieros.text.buscar"              var="buscar"/>
<spring:message code="catalogos.general.text.tituloModulo"                          var="tituloModulo"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.alta"                var="alta"/>
<spring:message code="general.espacioEnBlanco"                                      var="espacioEnBlanco"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.eliminar"            var="eliminar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="catalogos.consultaCatIntFinancieros.link.editar"              var="actualizar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloH" 			var="tituloH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tituloFunH" 			var="tituloFunH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.limpiar"             var="limpiar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.cveIntermeHV"        var="cveIntermeHV"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.exportar"            var="exportar"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchModifHV" 			var="fchModifHV"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.exportarTodo"        var="exportarTodo"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoModifHV" 		var="tipoModifHV"/>
<spring:message code="general.inicio"                                               var="inicio"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioModifHV" 		var="usuarioModifHV"/>
<spring:message code="general.fin"                                                  var="fin"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoIntermeHV" 		var="tipoIntermeHV"/>
<spring:message code="general.anterior"                                             var="anterior"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numCecobanHV" 		var="numCecobanHV"/>
<spring:message code="general.siguiente"                                            var="siguiente"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numBanxicoHV" 		var="numBanxicoHV"/>
<spring:message code="general.nombreAplicacion"                                     var="aplicacion"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreCortoHV" 		var="nombreCortoHV"/>
<spring:message code="codError.ED00022V"                                            var="ED00022V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreLargoHV" 		var="nombreLargoHV"/>
<spring:message code="codError.ED00023V"                                            var="ED00023V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.cveIntermeH" 		var="cveIntermeH"/>
<spring:message code="codError.ED00027V"                                            var="ED00027V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.fchModifH" 			var="fchModifH"/>
<spring:message code="codError.OK00013V"                                            var="OK00013V"/>
<spring:message code="codError.CD00161V"                                            var="CD00161V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoModifH" 			var="tipoModifH"/>
<spring:message code="codError.CD00162V"                                            var="CD00162V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.usuarioModifH" 		var="usuarioModifH"/>
<spring:message code="codError.CD00163V"                                            var="CD00163V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoIntermeORIH" 	var="tipoIntermeORIH"/>
<spring:message code="codError.CD00164V"                                            var="CD00164V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.tipoIntermeNVOH" 	var="tipoIntermeNVOH"/>
<spring:message code="codError.ED00029V"                                            var="ED00029V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numCecobanORIH" 		var="numCecobanORIH"/>
<spring:message code="codError.CD00010V"                                            var="CD00010V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numCecobanNVOH" 		var="numCecobanNVOH"/>
<spring:message code="codError.ED00068V"                                            var="ED00068V"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreCortoORIH" 	var="nombreCortoORIH"/>
<spring:message code="codError.OK00002V"                                            var="OK00002V" />
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreCortoNVOH" 	var="nombreCortoNVOH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreLargoORIH" 	var="nombreLargoORIH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.nombreLargoNVOH" 	var="nombreLargoNVOH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numBanxicoORIH" 		var="numBanxicoORIH"/>
<spring:message code="catalogos.consultaCatIntFinancieros.text.numBanxicoNVOH" 		var="numBanxicoNVOH"/>

    <link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/detallePago.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript"> var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"CD00010V":'${CD00010V}', "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}', "CD00011V":'${CD00011V}' ,"CD00161V":'${CD00161V}',"CD00162V":'${CD00162V}',"CD00163V":'${CD00163V}',"CD00164V":'${CD00164V}',"ED00029V":'${ED00029V}',"ED00068V":'${ED00068V}',"OK00002V":'${OK00002V}' };    </script> 
    <script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js"      type="text/javascript"></script> 
	<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/catIntFinancierosHis.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js"     type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js"  type="text/javascript"></script>
	
	<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloH} - ${tituloFunH}
	</div>

	<form name="idForm" id="idForm" method="post">
	
		<input type="hidden" name="pagina" 		   id="pagina" 	        value="${beanResIntFinancierosHist.beanPaginador.pagina}">
		<input type="hidden" name="paginaIni" 	   id="paginaIni" 	    value="${beanResIntFinancierosHist.beanPaginador.paginaIni}">
	    <input type="hidden" name="paginaFin" 	   id="paginaFin" 	    value="${beanResIntFinancierosHist.beanPaginador.paginaFin}">
	    <input type="hidden" name="field" 		   id="field" 		    value="${beanResIntFinancierosHist.beanFilter.field}">
		<input type="hidden" name="fieldValue" 	   id="fieldValue"      value="${beanResIntFinancierosHist.beanFilter.fieldValue}">
	    <input type="hidden" name="accion"         id="accion"          value="">
		<input type="hidden" name="opcion"         id="opcion"          value="">				
		<input type="hidden" name="cveInterme"     id="cveInterme" 		value="${beanFilter.cveInterme}"/>
		<input type="hidden" name="fchModif"       id="fchModif" 		value="${beanFilter.fchModif}"/>
		<input type="hidden" name="tipoModif"      id="tipoModif" 		value="${beanFilter.tipoModif}"/>
		<input type="hidden" name="usuario"        id="usuario" 		value="${beanFilter.usuario}"/>
		<input type="hidden" name="tipoIntermeOri" id="tipoIntermeOri" 	value="${beanFilter.tipoIntermeOri}"/>
		<input type="hidden" name="numCecobanOri"  id="numCecobanOri" 	value="${beanFilter.numCecobanOri}"/>		
		<input type="hidden" name="numBanxicoOri"  id="numBanxicoOri" 	value="${beanFilter.numBanxicoOri}"/>
		<input type="hidden" name="nombreCortoOri" id="nombreCortoOri" 	value="${beanFilter.nombreCortoOri}"/>	
		<input type="hidden" name="nombreLargoOri" id="nombreLargoOri" 	value="${beanFilter.nombreLargoOri}"/>
		
	 	<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${subtituloFuncion}</div>
			<div class="contentBuscadorSimple">
				<table>
					<caption></caption>
					<tr>
						<td class=text_izquierda>
							<label for="paramCveIntermeHV">${cveIntermeHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramCveIntermeHV" name="paramCveIntermeHV"  size="20" maxlength="5"  value="${beanFilter.cveInterme}"/></td><td class=text_izquierda></td>
						<td class="text_izquierda">
							<label for="paramNumBanxicoHV">${numBanxicoHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramNumBanxicoHV" name="paramNumBanxicoHV"  size="20" maxlength="7"  value="${beanFilter.numBanxicoOri}"/></td>
					</tr>
					<tr>
						<td  class="text_izquierda"  >
							<label for="paramFchModifHV">${fchModifHV}</label>
						</td>
						<td colspan="2"><input name="paramFchModifHV" type="text" class="Campos" id="paramFchModifHV" size="20" readonly="readonly" value="${beanFilter.fchModif}"/> 
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td  class="text_izquierda">
							<label for="paramNombreCortoHV">${nombreCortoHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramNombreCortoHV" name="paramNombreCortoHV"  size="20" maxlength="16"  value="${beanFilter.nombreCortoOri}"/></td>
					</tr>
					<tr>
						<td  class="text_izquierda">
							<label for="paramTipoModifHV">${tipoModifHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramTipoModifHV" name="paramTipoModifHV"  size="20" maxlength="1"  value="${beanFilter.tipoModif}"/></td><td class=text_izquierda></td>
						<td  class="text_izquierda">
							<label for="paramNombreLargoHV">${nombreLargoHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramNombreLargoHV" name="paramNombreLargoHV"  size="20" maxlength="40"  value="${beanFilter.nombreLargoOri}"/></td>
					</tr>
					<tr>
						<td  class="text_izquierda">
							<label for="paramUsuarioModifHV">${usuarioModifHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramUsuarioModifHV" name="paramUsuarioModifHV"  size="20" maxlength="7"  value="${beanFilter.usuario}"/></td>
					</tr>
					<tr>
						<td  class="text_izquierda">
							<label for="paramTipoIntermeHV">${tipoIntermeHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramTipoIntermeHV" name="paramTipoIntermeHV"  size="20" maxlength="4"  value="${beanFilter.tipoIntermeOri}"/></td>
					</tr>
					<tr>
						<td  class="text_izquierda">
							<label for="paramNumCecobanHV">${numCecobanHV}</label>
						</td>
						<td><input type="text" class="Campos" id="paramNumCecobanHV" name="paramNumCecobanHV"  size="20" maxlength="7"  value="${beanFilter.numCecobanOri}"/></td>
					</tr>
					<tr>
						<td class="text_centro">
							
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<span><a id="idBuscar" href="javascript:;">${buscar} </a></span>
								</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<span><a id="idLimpiar" href="javascript:;">${limpiar}</a></span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
	 	</div>
		
		
		<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<caption></caption>
				<thead>
					<tr>
						<th class="text_centro" scope="col" colspan="2">${cveIntermeH}</th>
						<th class="text_centro" scope="col">${fchModifH}</th>
						<th class="text_centro" scope="col">${tipoModifH}</th>
						<th class="text_centro" scope="col">${usuarioModifH}</th>
						<th class="text_centro" scope="col">${tipoIntermeORIH}</th>
						<th class="text_centro" scope="col">${tipoIntermeNVOH}</th>
						<th class="text_centro" scope="col">${numCecobanORIH}</th>
						<th class="text_centro" scope="col">${numCecobanNVOH}</th>
						<th class="text_centro" scope="col">${nombreCortoORIH}</th>
						<th class="text_centro" scope="col">${nombreCortoNVOH}</th>
						<th class="text_centro" scope="col">${nombreLargoORIH}</th>
						<th class="text_centro" scope="col">${nombreLargoNVOH}</th>
						<th class="text_centro" scope="col">${numBanxicoORIH}</th>
						<th class="text_centro" scope="col">${numBanxicoNVOH}</th>
					</tr>
				</thead>
				<tbody>				
					<c:forEach var="beanIntFinanciero" items="${beanResIntFinancierosHist.intFinancieroHistoricos}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td>								
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].cveInterme" value="${beanIntFinanciero.cveInterme}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].fchModif" value="${beanIntFinanciero.fchModif}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].tipoModif" value="${beanIntFinanciero.tipoModif}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].usuario" value="${beanIntFinanciero.usuario}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].tipoIntermeOri" value="${beanIntFinanciero.tipoIntermeOri}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].tipoIntermeNvo" value="${beanIntFinanciero.tipoIntermeNvo}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].numCecobanOri" value="${beanIntFinanciero.numCecobanOri}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].numCecobanNvo" value="${beanIntFinanciero.numCecobanNvo}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].nombreCortoOri" value="${beanIntFinanciero.nombreCortoOri}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].nombreCortoNvo" value="${beanIntFinanciero.nombreCortoNvo}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].nombreLargoOri" value="${beanIntFinanciero.nombreLargoOri}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].nombreLargoNvo" value="${beanIntFinanciero.nombreLargoNvo}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].numBanxicoOri" value="${beanIntFinanciero.numBanxicoOri}"/>
								<input type="hidden" name="intFinancieroHistoricos[${rowCounter.index}].numBanxicoNvo" value="${beanIntFinanciero.numBanxicoNvo}"/>								
							</td>
							<td class="text_izquierda">${beanIntFinanciero.cveInterme}</td>
							<td class="text_izquierda">${beanIntFinanciero.fchModif}</td>
							<td class="text_izquierda">${beanIntFinanciero.tipoModif}</td>
							<td class="text_izquierda">${beanIntFinanciero.usuario}</td>
							<td class="text_izquierda">${beanIntFinanciero.tipoIntermeOri}</td>
							<td class="text_izquierda">${beanIntFinanciero.tipoIntermeNvo}</td>
							<td class="text_izquierda">${beanIntFinanciero.numCecobanOri}</td>
							<td class="text_izquierda">${beanIntFinanciero.numCecobanNvo}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreCortoOri}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreCortoNvo}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreLargoOri}</td>
							<td class="text_izquierda">${beanIntFinanciero.nombreLargoNvo}</td>
							<td class="text_izquierda">${beanIntFinanciero.numBanxicoOri}</td>
							<td class="text_izquierda">${beanIntFinanciero.numBanxicoNvo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<c:if test="${not empty beanResIntFinancierosHist.intFinancieroHistoricos}">
				<div class="paginador">
					<div style="text-align: left; float: left; margin-left: 5px;">
						<label>Mostrando del
							${beanResIntFinancierosHist.beanPaginador.regIni} al
							${beanResIntFinancierosHist.beanPaginador.regFin} de un total de
							${beanResIntFinancierosHist.totalReg} registros</label>
					</div>
					<jsp:include page="paghisto.jsp" >	  					
		  					<jsp:param name="paginaIni" value="${beanRes.paginador.paginaIni}" />
		  					<jsp:param name="pagina"    value="${beanRes.paginador.pagina}" />
		  					<jsp:param name="paginaAnt" value="${beanRes.paginador.paginaAnt}" />
		  					<jsp:param name="paginaFin" value="${beanRes.paginador.paginaFin}" />
		  					<jsp:param name="servicio"  value="consultaCatIntFinancierosHis.do" />
					</jsp:include>
				</div>
			</c:if>
		</div>

		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
				<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${beanResIntFinancierosHist.totalReg > 0}">
								<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>								
							</c:when>
							<c:otherwise>
								<td class="izq_Des tamanobtonexportar"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd" >${espacioEnBlanco}</td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${beanResIntFinancierosHist.totalReg > 0}">
								<td class="izq tamanobtonexportar"><a id="btnExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							<td class="odd" >${espacioEnBlanco}</td>							
							</c:when>
							<c:otherwise>
								<td class="izq_Des tamanobtonexportar"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd" >${espacioEnBlanco}</td>
					</tr>
				</table>
			</div>
		</div>
	</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">${e:forHtml(tipoError)}('${e:forHtml(descError)}','${e:forHtml(aplicacion)}','${e:forHtml(codError)}','');</script>
</c:if>