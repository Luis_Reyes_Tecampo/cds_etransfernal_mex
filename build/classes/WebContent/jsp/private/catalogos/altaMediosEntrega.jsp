<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
    <jsp:param name="menuItem"    value="catalogos" />
    <jsp:param value="mantenimientoMediosEntrega" name="menuSubitem"/>
</jsp:include>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/private/formutils.css" />
<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/tools.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/validaciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/catalogos/mediosEntrega.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<!-- Inicio de llamado de datos de Properties -->
<spring:message code="general.nombreAplicacion" 						var="app"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="general.eliminar" 								var="lblEliminar"/>
<spring:message code="general.guardar" 									var="lblGuardar"/>
<spring:message code="general.cancelar" 								var="lblRegresar"/>
<spring:message code="general.espacioEnBlanco" 							var="espacioEnBlanco"/>
<spring:message code="catalogos.certificado.cifrado.cveMedEntrega" 		var="lblCveMedEntrega"/>
<spring:message code="catalogos.certificado.cifrado.descMedEntrega" 	var="lblDescMedEntrega"/>
<spring:message code="catalogos.certificado.cifrado.numCertif" 			var="lblNumCertif"/>
<spring:message code="catalogos.certificado.cifrado.cveCertif" 			var="lblCveCertificado"/>
<spring:message code="catalogos.certificado.cifrado.ptrFirma" 			var="lblPtrFirma"/>
<spring:message code="catalogos.certificado.cifrado.ptrCifrado" 		var="lblPtrCifrado"/>
<spring:message code="catalogos.certificado.cifrado.estatus" 			var="lblEstatus"/>
<spring:message code="catalogos.general.text.tituloModulo" 				var="tituloModulo"/>
<spring:message code="catalogos.menuCatalogos.txt.txtMantoCertificados" var="tituloFuncionalidad"/>

<spring:message code="catalogos.mttomedioentrega.txt.txtMantoEntrega" 	var="MantoEntrega"/>

<%-- Titulos de la tabla --%>
<spring:message code="catalogos.mttomedioentrega.txt.txtcvemedioent"  	var="cme"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtuversion"     	var="version"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtdescripcion"  	var="desc"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcomanualpf"   	var="compf"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcomanualpm"   	var="compm"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtckkseguridad" 	var="seguridad"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtcanal"        	var="canal"/>
<spring:message code="catalogos.mttomedioentrega.txt.txtimporteMax"     var="importeMax"/>
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na"           var="na" />
<spring:message code="general.asteriscoHelper"                          var="asteriscoHelper" />
<spring:message code="general.dosPuntosHelper"                          var="DosPuntosHelper" />

<spring:message code="catalogos.certificadosCifrado.alta" 				var="alta"/>
<spring:message code="catalogos.certificadosCifrado.modificar" 			var="modificar"/>

<!-- Fin de Variables Constantes -->

<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="codError.ED00130V" var="ED00130V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',
           "ED00130V":'${ED00130V}',"OK00000V":'${OK00000V}',"CD00010V":'${CD00010V}'
        	   ,"ED00029V":'${ED00029V}'  };
</script>

<form id="mform" method="post">
 	<input type="hidden" name="accion" id="accion" value="${accion}">
 	<input type="hidden" name="cveMedioEntrega" id="cveMedioEntrega" value="${beanMedioEntrega.cveMedioEntrega}" ${accion eq 'alta' ? 'disabled="disabled"' : ''}>
	<div class="pageTitleContainer">
		<span class="pageTitle">${MantoEntrega}</span>
	</div>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${MantoEntrega}</div>
		<div class="contentBuscadorSimple">
	        <div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${cme}${DosPuntosHelper}
			    </label>
			    <input type="text" id="paramCveMedioEntrega" name="cveMedioEntrega" maxlength="6" value="${beanMedioEntrega.cveMedioEntrega}" class="letrasNum objFrm Campos required" ${accion eq 'alta' ? '' : 'disabled="disabled"'} />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco"></span>${desc}${DosPuntosHelper}
			    </label>
			    <input type="text" id="paramDescripcion" name="desc" maxlength="30" value="${beanMedioEntrega.desc}" class="letrasNum objFrm Campos" />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco"></span>${compf}${DosPuntosHelper}
			    </label>
			    <input type="text" id="paramComAnualPF" name="comAnualPF" maxlength="19" value="${beanMedioEntrega.comAnualPF}" class=" onlyDecimal objFrm Campos " />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        ${compm}${DosPuntosHelper}
			    </label>
			   <input type="text" id="paramComAnualPM" name="comAnualPM" maxlength="19" value="${beanMedioEntrega.comAnualPM}" class="onlyDecimal objFrm Campos " />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class="requerido"></span>
			        <span class="asterisco">${asteriscoHelper}</span>${seguridad}${DosPuntosHelper}
			    </label>
				<input type="text" name="seguridad" id="paramSeguridad" maxlength="7" value="${beanMedioEntrega.seguridad}" class="onlyNumero objFrm Campos required" />
			</div>
			<div class="divFrm">
			    <label class="lblForm"> <span class=""></span>
			        <span class="asterisco">${asteriscoHelper}</span>${canal}${DosPuntosHelper}
			    </label>
			    <input type="text" name="canal" id="paramCanal" maxlength="6" value="${beanMedioEntrega.canal}" class="letrasNum objFrm Campos required" />
			</div>
			<div class="divFrm">
			    <label class="lblForm" for="paramImpMax"> <span class=""></span>
			      ${importeMax}${DosPuntosHelper}
			    </label>
			    <input type="text" name="importeMax" id="paramImpMax" maxlength="100" value="${beanMedioEntrega.importeMax}" class="objFrm Campos" />
			</div>

	    </div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
		    <div class="contentPieContenedor">
		        <table>
		             <tr>
		                 <td class="izq">
		                     <a id="btnGuardar" href="javascript:;" >${lblGuardar}</a>
		                 </td>
		                 <td class="odd">${espacioEnBlanco}</td>
		                 <td class="der">
		                     <a id="btnReturn" href="${pageContext.servletContext.contextPath}/catalogos/consultaMttoMediosEntrega.do" >${lblRegresar}</a>
		                 </td>
		             </tr>
		        </table>
		    </div>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>