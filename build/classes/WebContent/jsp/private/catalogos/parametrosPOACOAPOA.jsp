<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
		<tr>
					<td>${POA}</td>
					<td></td>
					<c:choose>
						<c:when test="${esActivoPOA}">
						<c:choose>
							<c:when test="${modulo eq 'spid'}">
							<td><span><a id="idActivarPOA" href="javascript:activarPOA('spid');">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:when>
							<c:otherwise>
							<td><span><a id="idActivarPOA" href="javascript:activarPOA('actual');">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td><span class="btn_Des"><a id="idActivarPOA" href="javascript:;">${txtActivar}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span></td>
						</c:otherwise>
					</c:choose>
					<td></td>
					<c:choose>
						<c:when test="${esdesActivacionPOA}">
						<c:choose>
							<c:when test="${modulo eq 'spid'}">
							<td><span><a id="idDesactivarPOA" href="javascript:desactivarPOA('spid');">${txtDesactivar}</a></span></td>
							</c:when>
							<c:otherwise>
							<td><span><a id="idDesactivarPOA" href="javascript:desactivarPOA('actual');">${txtDesactivar}</a></span></td>
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td><span class="btn_Des"><a id="idDesactivarPOA" href="javascript:;">${txtDesactivar}</a></span></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>