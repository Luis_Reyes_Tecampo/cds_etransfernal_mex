<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${not empty beanCuentas.cuentas}">
			<div class="paginador">
				<div style="text-align: left; float: left; margin-left: 5px;">
					<label>Mostrando del
						${beanCuentas.beanPaginador.regIni} al
						${beanCuentas.beanPaginador.regFin} de un total de
						${beanCuentas.totalReg} registros</label>
				</div>
				<c:if
					test="${beanCuentas.beanPaginador.paginaIni == beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaIni != beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaCuentasFideicomisoSPID.do','INI');">&lt;&lt;${inicio}</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaAnt!='0' && beanCuentas.beanPaginador.paginaAnt!=null}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaCuentasFideicomisoSPID.do','ANT');">&lt;${anterior}</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaAnt=='0' || beanCuentas.beanPaginador.paginaAnt==null}">
					<a href="javascript:;">&lt;${anterior}</a>
				</c:if>
				<label id="txtPagina">${beanCuentas.beanPaginador.pagina}
					- ${beanCuentas.beanPaginador.paginaFin}</label>
				<c:if
					test="${beanCuentas.beanPaginador.paginaFin != beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaCuentasFideicomisoSPID.do','SIG');">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaFin == beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;">${siguiente}&gt;</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaFin != beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;"
						onclick="nextPage('idForm','consultaCuentasFideicomisoSPID.do','FIN');">${fin}&gt;&gt;</a>
				</c:if>
				<c:if
					test="${beanCuentas.beanPaginador.paginaFin == beanCuentas.beanPaginador.pagina}">
					<a href="javascript:;">${fin}&gt;&gt;</a>
				</c:if>
			</div>
		</c:if>