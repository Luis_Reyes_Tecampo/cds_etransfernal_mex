<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPEI" />
	<jsp:param name="menuSubitem" value="recepcionOperaSPEI" />
</jsp:include>
	
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCDA.myMenu.text.moduloSPEI" var="tituloModulo"/>
<spring:message code="moduloSPEI.recepOperacion.text.titulo" var="tituloFuncionalidad"/>

<script src="${pageContext.servletContext.contextPath}/js/private/SPEI/recepcionOperacion.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/SPEI/recepcionOperacionApartadas.js" type="text/javascript"></script>
	<div class="pageTitleContainer">
		<span class="pageTitle" >${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>
<jsp:include page="../admonSaldo/estructura/recepcionOperacionPrincipal.jsp" flush="true"/>