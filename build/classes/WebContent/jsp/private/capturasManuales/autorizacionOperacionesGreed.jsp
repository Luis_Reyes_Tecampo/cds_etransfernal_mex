<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.template" var="template"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.fechacaptura" var="fechaCaptura"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.seleccion" var="seleccion"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.folio" var="folio"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.importe" var="importe"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.usr.captura" var="usrCaptura"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.usr.aut.cancel" var="usrAutCancel"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.estatus" var="estatus"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.apartado" var="apartado"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.refTransfer" var="refTransfer"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.codErrorOpe" var="codErrorOpe"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.msgErrorOpe" var="msgErrorOpe"/>

<c:set var="searchString" value="${seguTareas}"/>
<input type="hidden" name="countlistOperacionesSeleccionadas" value="${fn:length(beanResponse.listOperacionesPendAutorizar)}"/>

<table>
	<caption></caption>
	<tr>
		<th style="white-space:nowrap; width:122px" class="text_centro">${seleccion}</th>
		<th style="white-space:nowrap; width:223px" class="text_centro" scope="col">${folio}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${importe}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${template}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${usrCaptura}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${usrAutCancel}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${estatus}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${fechaCaptura}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${apartado}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${refTransfer}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${codErrorOpe}</th>
		<th style="white-space:nowrap; width:122px" class="text_centro" scope="col">${msgErrorOpe}</th>
	</tr>
	<tr>
		<Td colspan="4" class="special"></Td>
	</tr>
	<tbody>
		<c:forEach var="beanOperacionesPend" items="${beanResponse.listOperacionesPendAutorizar}" varStatus="rowCounter">
		   	<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
				<td class="text_centro">
					<input type="checkbox" name="listOperacionesSeleccionadas[${rowCounter.index}].seleccionado" />
				</td>
				<td class="text_centro">
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].folio" value="${beanOperacionesPend.folio}"/>
					<c:choose>
	  						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR') && beanOperacionesPend.apartado==true && beanOperacionesPend.statusOperacionApartada==2}">
							<a id="idFolio${beanOperacionesPend.folio}" href="javascript:;" onKeyPress="" 
								onclick="autorizarOperaciones.despliegaDetalle(${beanOperacionesPend.folio}, '${beanOperacionesPend.template}')">${beanOperacionesPend.folio}</a>
						</c:when>
						<c:otherwise>
							<a>${beanOperacionesPend.folio}</a>
						</c:otherwise>
					</c:choose>
				</td>
				<td class="text_centro">${beanOperacionesPend.importe}</td>
				<td class="text_centro">${beanOperacionesPend.template}</td>
				<td class="text_centro">${beanOperacionesPend.usrCaptura}</td>
				<td class="text_centro">
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].usrAutoriza" value="${beanOperacionesPend.usrAutoriza}"/>
					${beanOperacionesPend.usrAutoriza}</td>
				<td class="text_centro">
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].estatus" value="${beanOperacionesPend.estatus}"/>
					${beanOperacionesPend.estatus}</td>
				<td class="text_centro">${beanOperacionesPend.fechaCaptura}</td>
				<td class="text_centro">
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].statusOperacionApartada" value="${beanOperacionesPend.statusOperacionApartada}"/>
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].usrsAutorizanTemplate" value="${beanOperacionesPend.usrsAutorizanTemplate}"/>
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].usrApartado" value="${beanOperacionesPend.usrApartado}"/>
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].apartado" value="${beanOperacionesPend.apartado}"/>
					
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].cuentaOrdenante" value="${beanOperacionesPend.cuentaOrdenante}"/>
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].cuentaReceptora" value="${beanOperacionesPend.cuentaReceptora}"/>
					<input type="hidden" name="listOperacionesSeleccionadas[${rowCounter.index}].monto" value="${beanOperacionesPend.monto}"/>
					${beanOperacionesPend.usrApartado}</td>
				<td class="text_centro">${beanOperacionesPend.refTransfer}</td>
				<td class="text_centro">${beanOperacionesPend.msgErrorOpe}</td>
				<td class="text_centro">${beanOperacionesPend.codErrorOpe}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>