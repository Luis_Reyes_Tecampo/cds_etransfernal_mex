<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="AutorizarOperaciones" />
</jsp:include>

<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.titulo" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.detalle.autorizaroperaciones.label.detalle.subtituloFun" var="subtitulo"/>
<spring:message code="moduloCapturasManuales.detalle.autorizaroperaciones.label.claveTemplate" var="claveTemplate"/>
<spring:message code="moduloCapturasManuales.autorizaroperaciones.label.folio" var="lblFolio"/>
<spring:message code="moduloCapturasManuales.detalle.autorizaroperaciones.link.regresar" var="regresar"/>

<script src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/detalleAutorizacionOperaciones.js"
		type="text/javascript"></script>

<form id="idForm" action="" method="post">
	<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanFiltroAutorizarOpe.paginador.paginaIni}"/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanFiltroAutorizarOpe.paginador.pagina}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanFiltroAutorizarOpe.paginador.paginaFin}"/>
	<input type="hidden" name="paginador.regIni" id="regIni" value="${beanFiltroAutorizarOpe.paginador.regIni}"/>
	<input type="hidden" name="paginador.regFin" id="regFin" value="${beanFiltroAutorizarOpe.paginador.regFin}"/>
	<input type="hidden" name="paginador.paginaAnt" id="regIni" value="${beanFiltroAutorizarOpe.paginador.paginaAnt}"/>
	<input type="hidden" name="paginador.paginaSig" id="regFin" value="${beanFiltroAutorizarOpe.paginador.paginaSig}"/>
	<input type="hidden" name="paginador.accion" id="accion" value="${beanFiltroAutorizarOpe.paginador.accion}"/>
	
	<input type="hidden" id="template" name="template" value="${beanFiltroAutorizarOpe.template}"/>
	<input type="hidden" id="FechaCaptura" name="FechaCaptura" value="${beanFiltroAutorizarOpe.fechaOperacion}"/>
	<input type="hidden" id="usrAdmin" name="usrAdmin" value="${beanFiltroAutorizarOpe.usrAdmin}"/>
	
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span>
	</div>
	
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtitulo}</div>
		<div class="contentBuscadorSimple" >
			<table>
				<caption></caption>
				<tr>
					<td class="text_derecha" width="120px">
						<label style="font-size: 11px;">${claveTemplate}: </label>
					</td>
					<td class="text_izquierda">
						<input type="text" id="idUsuarioAutoriza" name="idUsuarioAutoriza" value="${paramCveTemplate}" readonly="readonly" class="Campos" style="font-size: 14px; text-align: center;"/>
					</td>
					
					<td class="text_derecha"></td>
					<td class="text_derecha" width="80px">
						<label style="font-size: 11px;">${lblFolio}: </label>
					</td>
					<td class="text_izquierda">
						<input type="text" id="idTemplate" name="idTemplate" value="${paramFolio}" readonly="readonly"class="Campos" style="font-size: 14px;  text-align: center;" />
					</td>
				</tr>
			</table>
			<jsp:include page="layoutOperacion.jsp" />
		</div>
	</div>
	
	<br/>
	<br/>

	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<td class="izq"><a id="idRegresarDetalle" href="javascript:;">${regresar}</a></td>
						<td class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der_Des"></td>
					</tr>
					<tr>
						<td width="279" class="izq_Des"></td>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der_Des"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>