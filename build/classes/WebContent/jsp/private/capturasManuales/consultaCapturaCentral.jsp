<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* consultaCapturaCentral.jsp
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     21/03/2017 12:00 Creacion de la pantalla
*
***********************************************************************************************
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCargaManual.jsp" flush="true">
	<jsp:param name="menuItem" value="capturasManuales" />
	<jsp:param name="menuSubitem" value="capturaCentral" />
</jsp:include>

<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="tituloModulo"/>
<spring:message code="moduloCapturasManuales.myMenu.text.capturaCentral" var="titulo"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.subtituloFun" var="txtSubtitulo"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.template" var="txtTemplate"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.layout" var="txtLayout"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.usuario" var="txtUsuario"/>
<spring:message code="moduloCapturasManuales.capturaCentral.link.cargar" var="txtCargar"/>
<spring:message code="moduloCapturasManuales.capturaCentral.link.regresar" var="txtRegresar"/>
<spring:message code="moduloCapturasManuales.capturaCentral.link.procesar" var="txtProcesar"/>
<spring:message code="moduloCapturasManuales.capturaCentral.link.limpiar" var="txtLimpiar"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.usuario" var="txtUsuario"/>
<spring:message code="moduloCapturasManuales.capturaCentral.label.folio" var="txtFolio"/>

<%-- Generales --%>
<spring:message code="general.espacioEnBlanco" 	var="espacioEnBlanco"/>

<%-- Errores --%>
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.ED00123V" var="ED00123V" />
<spring:message code="codError.ED00011V" var="ED00011V" />
<spring:message code="codError.CD00041V" var="CD00041V" />
<spring:message code="codError.CMCO021V" var="CMCO021V" />
<spring:message code="general.nombreAplicacion" var="aplicacion" />
	<script type="text/javascript">
       var mensajes = {"aplicacion":'${aplicacion}', "EC00011B":'${EC00011B}', "ED00011V":'${ED00011V}', "OK00000V":'${OK00000V}', 
       "CD00041V":'${CD00041V}',"CMCO021V":'${CMCO021V}'};
    </script>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>
<form id="idForm" action="" method="post">

	<input type="hidden" name="step" id="step" value="${beanRes.step}"/>
	<input type="hidden" name="countListTemplates" value="${fn:length(beanRes.listTemplates)}"/>
	<input type="hidden" name="countListLayouts" value="${fn:length(beanRes.listLayouts)}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${txtSubtitulo}</div>
		<div class="contentBuscadorSimple">
			${instruccionBuscador}
			<table>
				<caption></caption>
				<tbody>
					<c:choose>
						<c:when test="${'consulta' == beanRes.step}">
							<tr>
								<td class="text_derecha" width="80px">${txtTemplate}</td>
								<td class="text_izquierda" >
									<input type="hidden" id="template" name="template" value="${beanRes.template}"/>
									<c:forEach var="item" items="${beanRes.listLayouts}" varStatus="rowCounter">
										<input type="hidden" id="idLayout${rowCounter.index}" value="${item}"/>
										<input type="hidden" name="listLayouts[${rowCounter.index}]" value="${item}"/>
									</c:forEach>
									<c:forEach var="item" items="${beanRes.listTemplates}" varStatus="rowCounter">
										<input type="hidden" name="listTemplates[${rowCounter.index}]" value="${item}"/>
									</c:forEach>
									<select class="Campos" id="selectListTemplates" onChange="changeSelectedTemplate()" style="font-size: 14px;  text-align: center; width:154px;">
										<c:forEach var="item" items="${beanRes.listTemplates}" varStatus="rowCounter">
											<c:choose>
    											<c:when test="${item == beanRes.template}">
													<option value="${item}" selected="selected">
														${item}
													</option>
    											</c:when>
    											<c:otherwise>
													<option value="${item}">
														${item}
													</option>
    											</c:otherwise>
    										</c:choose>
  										</c:forEach>
									</select> 
								</td>
								<td class="text_derecha"></td>
								<td class="text_derecha" width="100px">${txtLayout}</td>
								<td class="text_izquierda">
									<input type="text" id="layout" value="${beanRes.idLayout}" readonly="readonly"/>
								</td>
								<td class="text_derecha">
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
			                        		<span><a id="idBuscar" href="javascript:;">${txtCargar}</a></span>
			                        	</c:when>
										<c:otherwise>
											<span class="btn_Des"><a href="javascript:;">${txtCargar}</a></span>
										</c:otherwise>
									</c:choose>
		                    	</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td class="text_derecha" width="80px">${txtFolio}</td>
								<td class="text_izquierda">
									<input type="text" id="idFol" name="idFol" value="${beanRes.folio}" readonly="readonly"/>
								</td>
								<td class="text_derecha"></td>
								<td class="text_derecha" width="80px">${txtTemplate}</td>
								<td class="text_izquierda">
									<input type="text" id="template" name="template" value="${beanRes.template}" readonly="readonly"/>
								</td>
							</tr>
							<tr>
								<td class="text_derecha" width="80px">${txtUsuario}</td>
								<td class="text_izquierda">
									<input type="text" id="idUser" name="idUser" value="${beanRes.usrCaptura}" readonly="readonly"/>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<jsp:include page="layoutOperacion.jsp" />
		</div>
	</div>
	<%-- Componente tabla estandar --%>
	<div class="frameTablaVariasColumnas">
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,\"AGREGAR\") && not empty beanRes.layout}">
								<td width="279" class="izq"><a id="idAlta" href="javascript:;">${txtProcesar}</a></td>
							</c:when>
							<c:otherwise>
								<td  width="279" class="izq_Des"><a href="javascript:;">${txtProcesar}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der">
							<span><a id="idRegresar" href="javascript:;">${txtRegresar}</a></span>
						</td>
					</tr>
					<tr>
						<td width="279" class="izq">
							<span><a id="idLimpiar" href="javascript:;">${txtLimpiar}</a></span>
						</td>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>	

<jsp:include page="../myFooter.jsp" flush="true"/>
<%-- Valida Error --%>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>