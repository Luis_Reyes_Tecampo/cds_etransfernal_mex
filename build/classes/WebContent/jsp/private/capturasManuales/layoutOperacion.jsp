<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* consultaCapturaCentral.jsp 
*
* Control de versiones:
*
* Version Date/Hour        Description
* 1.0     21/03/2017 12:00 Creacion de la pantalla
*
***********************************************************************************************
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<%-- Indicador Obligatorio --%>
<spring:message code="moduloCapturasManuales.dinamica.label.obligatorio" var="txtObl"/>
<spring:message code="moduloCapturasManuales.capturaCentral.generarUetr" var="generarUetr"/>
<%-- BeanOperacion --%>
<input type="hidden" name="idFolio" id="idFolio" value="${beanOperacion.idFolio}"/>
<input type="hidden" name="estatus" id="estatus" value="${beanOperacion.estatus}"/>
<input type="hidden" name="codError" id="codError" value="${beanOperacion.codError}"/>
<input type="hidden" name="msgError" id="msgError" value="${beanOperacion.msgError}"/>
<input type="hidden" name="idTemplate" id="idTemplate" value="${beanOperacion.idTemplate}"/>
<input type="hidden" name="idLayout" id="idLayout" value="${beanOperacion.idLayout}"/>
<input type="hidden" name="usrAutoriza" id="usrAutoriza" value="${beanOperacion.usrAutoriza}"/>
<input type="hidden" name="usrModifica" id="usrModifica" value="${beanOperacion.usrModifica}"/>
<input type="hidden" name="operacionApartada" id="operacionApartada" value="${beanOperacion.operacionApartada}"/>
<input type="hidden" name="codPostalOrd" id="codPostalOrd" value="${beanOperacion.codPostalOrd}"/>
<input type="hidden" name="comentario1" id="comentario1" value="${beanOperacion.comentario1}"/>
<input type="hidden" name="comentario2" id="comentario2" value="${beanOperacion.comentario2}"/>
<input type="hidden" name="comentario3" id="comentario3" value="${beanOperacion.comentario3}"/>
<input type="hidden" name="comentario4" id="comentario4" value="${beanOperacion.comentario4}"/>
<input type="hidden" name="conceptoPago" id="conceptoPago" value="${beanOperacion.conceptoPago}"/>
<input type="hidden" name="conceptoPago2" id="conceptoPago2" value="${beanOperacion.conceptoPago2}"/>
<input type="hidden" name="cveDivisaOrd" id="cveDivisaOrd" value="${beanOperacion.cveDivisaOrd}"/>
<input type="hidden" name="cveDivisaRec" id="cveDivisaRec" value="${beanOperacion.cveDivisaRec}"/>
<input type="hidden" name="cveEmpresa" id="cveEmpresa" value="${beanOperacion.cveEmpresa}"/>
<input type="hidden" name="cveIntermeOrd" id="cveIntermeOrd" value="${beanOperacion.cveIntermeOrd}"/>
<input type="hidden" name="cveIntermeRec" id="cveIntermeRec" value="${beanOperacion.cveIntermeRec}"/>
<input type="hidden" name="cveMedioEnt" id="cveMedioEnt" value="${beanOperacion.cveMedioEnt}"/>
<input type="hidden" name="cveOperacion" id="cveOperacion" value="${beanOperacion.cveOperacion}"/>
<input type="hidden" name="cvePtoVta" id="cvePtoVta" value="${beanOperacion.cvePtoVta}"/>
<input type="hidden" name="cvePtoVtaOrd" id="cvePtoVtaOrd" value="${beanOperacion.cvePtoVtaOrd}"/>
<input type="hidden" name="cvePtoVtaRec" id="cvePtoVtaRec" value="${beanOperacion.cvePtoVtaRec}"/>
<input type="hidden" name="cveRastreo" id="cveRastreo" value="${beanOperacion.cveRastreo}"/>
<input type="hidden" name="cveTransfe" id="cveTransfe" value="${beanOperacion.cveTransfe}"/>
<input type="hidden" name="cveUsuariocap" id="cveUsuariocap" value="${beanOperacion.cveUsuarioCap}"/>
<input type="hidden" name="cveUsuarioSol" id="cveUsuarioSol" value="${beanOperacion.cveUsuarioSol}"/>
<input type="hidden" name="direccionIp" id="direccionIp" value="${beanOperacion.direccionIp}"/>
<input type="hidden" name="domicilioOrd" id="domicilioOrd" value="${beanOperacion.domicilioOrd}"/>
<input type="hidden" name="fchConstitOrd" id="fchConstitOrd" value="${beanOperacion.fchConstitOrd}"/>
<input type="hidden" name="fchInstrucPago" id="fchInstrucPago" value="${beanOperacion.fchInstrucPago}"/>
<input type="hidden" name="folioPago" id="folioPago" value="${beanOperacion.folioPago}"/>
<input type="hidden" name="folioPaquete" id="folioPaquete" value="${beanOperacion.folioPaquete}"/>
<input type="hidden" name="horaInstrucPago" id="horaInstrucPago" value="${beanOperacion.horaInstrucPago}"/>
<input type="hidden" name="importeAbono" id="importeAbono" value="${beanOperacion.importeAbono}"/>
<input type="hidden" name="importeCargo" id="importeCargo" value="${beanOperacion.importeCargo}"/>
<input type="hidden" name="motivoDevol" id="motivoDevol" value="${beanOperacion.motivoDevol}"/>
<input type="hidden" name="nombreOrd" id="nombreOrd" value="${beanOperacion.nombreOrd}"/>
<input type="hidden" name="nombreRec" id="nombreRec" value="${beanOperacion.nombreRec}"/>
<input type="hidden" name="nombreRec2" id="nombreRec2" value="${beanOperacion.nombreRec2}"/>
<input type="hidden" name="numCuentaOrd" id="numCuentaOrd" value="${beanOperacion.numCuentaOrd}"/>
<input type="hidden" name="numCuentaRec" id="numCuentaRec" value="${beanOperacion.numCuentaRec}"/>
<input type="hidden" name="refeNumerica" id="refeNumerica" value="${beanOperacion.refeNumerica}"/>
<input type="hidden" name="referenciaMed" id="referenciaMed" value="${beanOperacion.referenciaMed}"/>
<input type="hidden" name="rfcOrd" id="rfcOrd" value="${beanOperacion.rfcOrd}"/>
<input type="hidden" name="rfcrec" id="rfcrec" value="${beanOperacion.rfcRec}"/>
<input type="hidden" name="rfcrec2" id="rfcrec2" value="${beanOperacion.rfcRec2}"/>
<input type="hidden" name="tipoCambio" id="tipoCambio" value="${beanOperacion.tipoCambio}"/>
<input type="hidden" name="tipoCuentaOrd" id="tipoCuentaOrd" value="${beanOperacion.tipoCuentaOrd}"/>
<input type="hidden" name="tipoCuentaRec" id="tipoCuentaRec" value="${beanOperacion.tipoCuentaRec}"/>
<input type="hidden" name="ciudadBcoRec" id="ciudadBcoRec" value="${beanOperacion.ciudadBcoRec}"/>
<input type="hidden" name="cveAba" id="cveAba" value="${beanOperacion.cveAba}"/>
<input type="hidden" name="cvePaisBcoRec" id="cvePaisBcoRec" value="${beanOperacion.cvePaisBcoRec}"/>
<input type="hidden" name="formaLiq" id="formaLiq" value="${beanOperacion.formaLiq}"/>
<input type="hidden" name="importeDls" id="importeDls" value="${beanOperacion.importeDls}"/>
<input type="hidden" name="nombreBcoRec" id="nombreBcoRec" value="${beanOperacion.nombreBcoRec}"/>
<input type="hidden" name="numCuentaRec2" id="numCuentaRec2" value="${beanOperacion.numCuentaRec2}"/>
<input type="hidden" name="plazaBanxico" id="plazaBanxico" value="${beanOperacion.plazaBanxico}"/>
<input type="hidden" name="sucursalBcoRec" id="sucursalBcoRec" value="${beanOperacion.sucursalBcoRec}"/>
<input type="hidden" name="tipoCuentaRec2" id="tipoCuentaRec2" value="${beanOperacion.tipoCuentaRec2}"/>

<input type="hidden" name="bucCliente" id="bucCliente" value="${beanOperacion.beanDatosGenerales.bucCliente}"/>
<input type="hidden" name="uetrSwift" id="uetrSwift" value="${beanOperacion.beanDatosGenerales.uetrSwift}"/>
<input type="hidden" name="campoSwift1" id="campoSwift1" value="${beanOperacion.beanDatosGenerales.campoSwift1}"/>
<input type="hidden" name="campoSwift2" id="campoSwift2" value="${beanOperacion.beanDatosGenerales.campoSwift2}"/>
<input type="hidden" name="importeMiva" id="importeMiva" value="${beanOperacion.beanDatosGenerales.importeIva}"/>
<input type="hidden" name="dirIpGenTran" id="dirIpGenTran" value="${beanOperacion.beanDatosIp.dirIpGenTran}"/>
<input type="hidden" name="dirIpFirma" id="dirIpFirma" value="${beanOperacion.beanDatosIp.dirIpFirma}"/>
<input type="hidden" name="refeAdicional1" id="refeAdicional1" value="${beanOperacion.beanDatosGenerales.refeAdicional1}"/>
<input type="hidden" name="path" id="path" value="${pageContext.servletContext.contextPath}"/>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/capturasManuales/capturaCentral.js"></script>

<table>
	<caption></caption>
	<tbody>
		<tr>
			<c:forEach var="layout" items="${beanRes.layout}">
				<c:choose>
					<c:when test="${'cveempresa' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda" style="width: 38%">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_empresa" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_empresa" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_empresa"></label>
									<input type="text" class="Campos" id="paramCve_empresa" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="3"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveptovta' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_pto_vta" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_pto_vta" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_pto_vta"></label>
									<input type="text" class="Campos" id="paramCve_pto_vta" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cvemedioent' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda" colspan="2">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_medio_ent" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_medio_ent" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_medio_ent"></label>
									<input type="text" class="Campos" id="paramCve_medio_ent" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						
						<tr></tr>
					</c:when>
					<c:when test="${'referenciamed' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramReferencia_med" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramReferencia_med" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramReferencia_med"></label>
									<input type="text" class="Campos" id="paramReferencia_med" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="12"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveusuariosol' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_usuario_sol" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_usuario_sol" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_usuario_sol"></label>
									<input type="text" class="Campos" id="paramCve_usuario_sol" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="7"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cvetransfe' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_transfe" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_transfe" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_transfe"></label>
									<input type="text" class="Campos" id="paramCve_transfe" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="3"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveoperacion' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_operacion" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_operacion" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_operacion"></label>
									<input type="text" class="Campos" id="paramCve_operacion" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="8"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'refenumerica' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramRefe_numerica" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramRefe_numerica" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramRefe_numerica"></label>
									<input type="text" class="Campos" id="paramRefe_numerica" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="20"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'direccionip' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramDireccion_ip" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramDireccion_ip" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramDireccion_ip"></label>
									<input type="text" class="Campos" id="paramDireccion_ip" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="39"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'fchinstrucpago' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramFch_instruc_pago" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramFch_instruc_pago" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramFch_instruc_pago"></label>
									<input type="text" class="Campos" id="paramFch_instruc_pago" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="8"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'horainstrucpago' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramHora_instruc_pago" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramHora_instruc_pago" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramHora_instruc_pago"></label>
									<input type="text" class="Campos" id="paramHora_instruc_pago" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="11"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cverastreo' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_rastreo" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_rastreo" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_rastreo"></label>
									<input type="text" class="Campos" id="paramCve_rastreo" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="30"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cvedivisaord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_divisa_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_divisa_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_divisa_ord"></label>
									<input type="text" class="Campos" id="paramCve_divisa_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cvedivisarec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_divisa_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_divisa_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_divisa_rec"></label>
									<input type="text" class="Campos" id="paramCve_divisa_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'importecargo' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramImporte_cargo" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramImporte_cargo" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramImporte_cargo"></label>
									<input type="text" class="Campos" id="paramImporte_cargo" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="22"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'importeabono' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramImporte_abono" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramImporte_abono" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramImporte_abono"></label>
									<input type="text" class="Campos" id="paramImporte_abono" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="22"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveptovtaord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_pto_vta_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_pto_vta_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_pto_vta_ord"></label>
									<input type="text" class="Campos" id="paramCve_pto_vta_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveintermeord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_interme_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_interme_ord"  onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_interme_ord"></label>
									<input type="text" class="Campos" id="paramCve_interme_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="5"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'tipocuentaord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramTipo_cuenta_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramTipo_cuenta_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramTipo_cuenta_ord"></label>
									<input type="text" class="Campos" id="paramTipo_cuenta_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="3"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'numcuentaord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNum_cuenta_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNum_cuenta_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNum_cuenta_ord"></label>
									<input type="text" class="Campos" id="paramNum_cuenta_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="20"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'nombreord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNombre_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNombre_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNombre_ord"></label>
									<input type="text" class="Campos" id="paramNombre_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="120"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'rfcord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramRfc_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramRfc_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramRfc_ord"></label>
									<input type="text" class="Campos" id="paramRfc_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="18"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'domicilioord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramDomicilio_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramDomicilio_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramDomicilio_ord"></label>
									<input type="text" class="Campos" id="paramDomicilio_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="120"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'codpostalord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCod_postal_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCod_postal_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCod_postal_ord"></label>
									<input type="text" class="Campos" id="paramCod_postal_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="5"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'fchconstitord' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramFch_constit_ord" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramFch_constit_ord" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramFch_constit_ord"></label>
									<input type="text" class="Campos" id="paramFch_constit_ord" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="8"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveptovtarec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_pto_vta_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_pto_vta_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_pto_vta_rec"></label>
									<input type="text" class="Campos" id="paramCve_pto_vta_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveintermerec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_interme_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_interme_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_interme_rec"></label>
									<input type="text" class="Campos" id="paramCve_interme_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="5"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'tipocuentarec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramTipo_cuenta_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramTipo_cuenta_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramTipo_cuenta_rec"></label>
									<input type="text" class="Campos" id="paramTipo_cuenta_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="2"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'tipocuentarec2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramTipo_cuenta_rec2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramTipo_cuenta_rec2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramTipo_cuenta_rec2"></label>
									<input type="text" class="Campos" id="paramTipo_cuenta_rec2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="2"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'ciudadbcorec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCiudad_bco_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCiudad_bco_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCiudad_bco_rec"></label>
									<input type="text" class="Campos" id="paramCiudad_bco_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="30"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveaba' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_aba" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_aba" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_aba"></label>
									<input type="text" class="Campos" id="paramCve_aba" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="9"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cvepaisbcorec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_pais_bco_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_pais_bco_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_pais_bco_rec"></label>
									<input type="text" class="Campos" id="paramCve_pais_bco_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="4"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'formaliq' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramForma_liq" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramForma_liq" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramForma_liq"></label>
									<input type="text" class="Campos" id="paramForma_liq" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="1"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'importedls' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramImporte_dls" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramImporte_dls" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramImporte_dls"></label>
									<input type="text" class="Campos" id="paramImporte_dls" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="22"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'nombrebcorec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNombre_bco_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNombre_bco_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNombre_bco_rec"></label>
									<input type="text" class="Campos" id="paramNombre_bco_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="40"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'numcuentarec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNum_cuenta_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNum_cuenta_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNum_cuenta_rec"></label>
									<input type="text" class="Campos" id="paramNum_cuenta_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="20"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'numcuentarec2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNum_cuenta_rec2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNum_cuenta_rec2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNum_cuenta_rec2"></label>
									<input type="text" class="Campos" id="paramNum_cuenta_rec2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="35"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'plazabanxico' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramPlaza_banxico" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramPlaza_banxico" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramPlaza_banxico"></label>
									<input type="text" class="Campos" id="paramPlaza_banxico" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="5"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'sucursalbcorec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramSucursal_bco_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramSucursal_bco_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramSucursal_bco_rec"></label>
									<input type="text" class="Campos" id="paramSucursal_bco_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="30"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'nombrerec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNombre_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNombre_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNombre_rec"></label>
									<input type="text" class="Campos" id="paramNombre_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="120"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'nombrerec2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramNombre_rec2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramNombre_rec2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramNombre_rec2"></label>
									<input type="text" class="Campos" id="paramNombre_rec2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="40"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'rfcrec' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramRfc_rec" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramRfc_rec" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramRfc_rec"></label>
									<input type="text" class="Campos" id="paramRfc_rec" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="18"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'rfcrec2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramRfc_rec2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramRfc_rec2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramRfc_rec2"></label>
									<input type="text" class="Campos" id="paramRfc_rec2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="18"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'motivodevol' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramMotivo_devol" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramMotivo_devol" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramMotivo_devol"></label>
									<input type="text" class="Campos" id="paramMotivo_devol" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="2"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'foliopaquete' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramFolio_paquete" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramFolio_paquete" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramFolio_paquete"></label>
									<input type="text" class="Campos" id="paramFolio_paquete" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="12"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'foliopago' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramFolio_pago" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramFolio_pago" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramFolio_pago"></label>
									<input type="text" class="Campos" id="paramFolio_pago" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="12"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'tipocambio' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramTipo_cambio" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramTipo_cambio" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramTipo_cambio"></label>
									<input type="text" class="Campos" id="paramTipo_cambio" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="22"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'comentario1' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramComentario1" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramComentario1" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramComentario1"></label>
									<input type="text" class="Campos" id="paramComentario1" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="120"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'comentario2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramComentario2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramComentario2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramComentario2"></label>
									<input type="text" class="Campos" id="paramComentario2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="30"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'comentario3' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramComentario3" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramComentario3" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramComentario3"></label>
									<input type="text" class="Campos" id="paramComentario3" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="30"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'comentario4' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramComentario4" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramComentario4" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramComentario4"></label>
									<input type="text" class="Campos" id="paramComentario4" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="120"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'conceptopago' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramConcepto_pago" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramConcepto_pago" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramConcepto_pago"></label>
									<input type="text" class="Campos" id="paramConcepto_pago" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="210"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'conceptopago2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramConcepto_pago2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramConcepto_pago2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramConcepto_pago2"></label>
									<input type="text" class="Campos" id="paramConcepto_pago2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="40"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'cveusuariocap' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCve_usuario_cap" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCve_usuario_cap" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCve_usuario_cap"></label>
									<input type="text" class="Campos" id="paramCve_usuario_cap" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="7"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<%-- Se agregan los nuevos campos --%>
					<c:when test="${'buccliente' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramBuc_cliente" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramBuc_cliente" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramBuc_cliente"></label>
									<input type="text" class="Campos" id="paramBuc_cliente" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="8"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'importeiva' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramImporte_iva" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramImporte_iva" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramImporte_iva"></label>
									<input type="text" class="Campos" id="paramImporte_iva" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="15"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'diripgentran' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramDir_ip_gen_tran" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramDir_ip_gen_tran" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramDir_ip_gen_tran"></label>
									<input type="text" class="Campos" id="paramDir_ip_gen_tran" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="15"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'diripfirma' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramDir_ip_firma" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramDir_ip_firma" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramDir_ip_firma"></label>
									<input type="text" class="Campos" id="paramDir_ip_firma" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="15"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'uetrswift' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramUetr_swift" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramUetr_swift" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramUetr_swift"></label>
									<input type="text" class="Campos" id="paramUetr_swift" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="100"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_izquierda" width="20">
							<span><a id="generarUETR" href="javascript:;">${generarUetr}</a></span>
						</td>
						
						<tr></tr>
					</c:when>
					<c:when test="${'camposwift1' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCampo_swift1" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCampo_swift1" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCampo_swift1"></label>
									<input type="text" class="Campos" id="paramCampo_swift1" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="100"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'camposwift2' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramCampo_swift2" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramCampo_swift2" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramCampo_swift2"></label>
									<input type="text" class="Campos" id="paramCampo_swift2" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="100"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
					<c:when test="${'refeadicional1' == layout.campo}">
						<td class="text_izquierda"></td>
						<td class="text_izquierda">${layout.obligatorio == 1 ? layout.etiqueta.concat(txtObl) : layout.etiqueta}</td>
						<td class="text_izquierda">
							<c:choose>
								<c:when test="${not empty layout.constante}">
									<input type="text" class="Campos" id="paramRefe_adicional1" onblur="refreshSession()" value="${layout.constante}" readonly="readonly" size="45"/>
								</c:when>
								<c:when test="${not empty layout.query && not empty layout.comboValores}">
									<select name="select" class="Campos" id="paramRefe_adicional1" onblur="refreshSession()" >
										<c:forEach var="valorCombo" items="${layout.comboValores}">
											<c:choose>
												<c:when test="${valorCombo.id == layout.valorActual}">
													<option value="${valorCombo.id}" selected="selected">${valorCombo.valor}</option>
												</c:when>
												<c:otherwise>
													<option value="${valorCombo.id}">${valorCombo.valor}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
								<label for="paramRefe_adicional1"></label>
									<input type="text" class="Campos" id="paramRefe_adicional1" onblur="refreshSession()" value="${layout.valorActual}" size="45" maxlength="40"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="text_derecha"></td>
						<tr></tr>
					</c:when>
				</c:choose>
			</c:forEach>
		</tr>
	</tbody>
</table>