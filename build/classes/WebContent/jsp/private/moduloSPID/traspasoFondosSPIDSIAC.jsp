<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="muestraTraspasoFondosSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
       
       
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="spid.menuSPID.txt.txtTraspasosFondos"           var="txtTraspasosFondos"/>

<spring:message code="spid.trasFondosSPIDSIAC.txt.txtImporte" var="txtImporte"/>
<spring:message code="spid.trasFondosSPIDSIAC.txt.txtComentario" var="txtComentario"/>
<spring:message code="spid.trasFondosSPIDSIAC.txt.txtEnviar" var="txtEnvia"/>
<spring:message code="spid.trasFondosSPIDSIAC.txt.txtEnviar" var="txtEnvia"/>




<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.CD00170V" var="CD00170V"/>



	 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"CD00170V":'${CD00170V}'};
	 
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/traspasoFondosSPIDSIAC.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${txtModuloSPID}</span> - ${txtTraspasosFondos}
		<span>
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.cveMiInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.fchOperacion}</span>
	</div>
<form name="idForm" id="idForm" method="post" action="">
	<div class="SPID">
		<div class="SPID1">
			<div class="titleBuscadorSimple"></div>
			<div class="contentBuscadorSimple">
				<table>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtImporte}</span></td>
						<td class="tdSPID2"><input class="text_derechaSPID" type="text" name="importe" maxlength="19" size="19" value=""/></td>
						<td class="tdSPID3"></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtComentario}</span></td>
						<td class="tdSPID2"><input class="text_izquierdaSPID" type="text" name="comentario" maxlength="60" size="60" value=""/></td>
					</tr>
					
				</table>
			</div>
		
		
			
			
		</div>

	</div>

	<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>							
						<td class="izq_Des"  width="279">${espacioEnBlanco}</td>
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="der"  width="279"><a id="idEnviar" href="javascript:;">${txtEnvia}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"  width="279"><a href="javascript:;">${txtEnvia}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					
				</table>

			</div>
		</div>

</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>