<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraMonitor" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloSPID.myMenu.text.moduloSPID" var="tituloModulo"/>
<spring:message code="moduloSPID.listadoPagos.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.listadoPagos.text.refe" var="refe"/>
<spring:message code="moduloSPID.listadoPagos.text.cola" var="cola"/>
<spring:message code="moduloSPID.listadoPagos.text.claveTran" var="claveTran"/>
<spring:message code="moduloSPID.listadoPagos.text.macan" var="macan"/>
<spring:message code="moduloSPID.listadoPagos.text.medioEnt" var="medioEnt"/>
<spring:message code="moduloSPID.listadoPagos.text.fchCap" var="fchCap"/>
<spring:message code="moduloSPID.listadoPagos.text.ord" var="ord"/>
<spring:message code="moduloSPID.listadoPagos.text.rec" var="rec"/>
<spring:message code="moduloSPID.listadoPagos.text.importeAbono" var="importeAbono"/>
<spring:message code="moduloSPID.listadoPagos.text.est" var="est"/>
<spring:message code="moduloSPID.listadoPagos.text.folioPaqPag" var="folioPaqPag"/>
<spring:message code="moduloSPID.listadoPagos.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.listadoPagos.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.listadoPagos.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.listadoPagos.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.listadoPagos.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.listadoPagos.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.listadoPagos.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>


<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',
                    "ED00011V":'${ED00011V}',
                    "OK00002V":'${OK00002V}',
                    "OK00000V":'${OK00000V}',
                    "EC00011B":'${EC00011B}',
                    "OK00001V":'${OK00001V}'
                    };
                    
    var tipoOrden = '${tipoOrden}';	 
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/listadoPagos.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/listadoPagos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${titulo}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 ${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResListadoPagos.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResListadoPagos.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResListadoPagos.beanPaginador.paginaFin}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="field" id="field" value="${field}">
	<input type="hidden" name="fieldValue" id="fieldValue" value="${fieldValue}">
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table id="tablaDatosListado" name="tablaDatosListado">
					<tr>
						<th class="text_centro filField" scope="col">${refe}<br/><input name="refeInput" id="refeInput" type="text" data-field="referencia" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${cola}<br/><input name="colaInput" id="colaInput" type="text" data-field="cola" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${claveTran}<br/><input name="claveTranInput" id="claveTranInput" type="text" data-field="cveTran" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${macan}<br/><input name="macanInput" id="macanInput" type="text" data-field="mecan" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${medioEnt}</th>
						<th class="text_centro filField" scope="col">${fchCap}</th>
						<th class="text_centro filField" scope="col">${ord}<br/><input name="ordInput" id="ordInput" type="text" data-field="ord" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${rec}<br/><input name="recInput" id="recInput" type="text" data-field="rec" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${importeAbono}<br/><input name="importeAbonoInput" id="importeAbonoInput" type="text" data-field="imp" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${est}</th>
					</tr>
					<tbody>				
						<c:forEach var="pago" items="${beanResListadoPagos.listadoPagos}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td>${pago.referencia}</td>
								<td>${pago.cveCola}</td>
								<td>${pago.cveTran}</td>
								<td>${pago.cveMecanismo}</td>
								<td>${pago.cveMedioEnt}</td>
								<td>${pago.fechaCaptura}</td>
								<td>${pago.cveIntermeOrd}</td>
								<td>${pago.cveIntermeRec}</td>
								<td>${pago.importeAbono}</td>
								<td>${pago.estatus}</td>
								<td><a class="detalle" data-ref="${pago.referencia}" href="javascript:;">Detalle</a></td>
							</tr>
						</c:forEach>
						<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
					</tbody>
				</table>
			</div>
		<c:if test="${not empty beanResListadoPagos.listadoPagos}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResListadoPagos.totalReg} registros | Importe p�gina: ${importePagina}</label>
				</div>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaIni == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaIni != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listadoPagos.do?tipo=${tipoOrden}','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaAnt!='0' && beanResListadoPagos.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','listadoPagos.do?tipo=${tipoOrden}','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaAnt=='0' || beanResListadoPagos.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResListadoPagos.beanPaginador.pagina} - ${beanResListadoPagos.beanPaginador.paginaFin}</label>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listadoPagos.do?tipo=${tipoOrden}','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin != beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listadoPagos.do?tipo=${tipoOrden}','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResListadoPagos.beanPaginador.paginaFin == beanResListadoPagos.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${importeTotal}</label>
		</c:if>
		<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td width="279" class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${exportar}</a></td>
								</c:otherwise>
							</c:choose>							
							<td width="6" class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>							
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${exportarTodo}</a></td>
								</c:otherwise>
							</c:choose>							
							<td width="6" class="odd"></td>
							<td width="279" class="der"><a id="idRegresarMain" href="javascript:;">${Regresar}</a></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>