<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntCancelacionPagos" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="moduloSPID.cancelacionPagos.text.tit" var="tituloFuncionalidad"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00019V" var="ED00019V"/>
<spring:message code="codError.ED00020V" var="ED00020V"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>

<spring:message code="moduloSPID.cancelacionPagos.text.referencia" var="referencia"/>
<spring:message code="moduloSPID.cancelacionPagos.text.cola" var="cola"/>
<spring:message code="moduloSPID.cancelacionPagos.text.tran" var="tran"/>
<spring:message code="moduloSPID.cancelacionPagos.text.mecan" var="mecan"/>
<spring:message code="moduloSPID.cancelacionPagos.text.medioEnt" var="medio"/>
<spring:message code="moduloSPID.cancelacionPagos.text.ord" var="ord"/>
<spring:message code="moduloSPID.cancelacionPagos.text.rec" var="rec"/>
<spring:message code="moduloSPID.cancelacionPagos.text.paq" var="paq"/>
<spring:message code="moduloSPID.cancelacionPagos.text.pago" var="pago"/>
<spring:message code="moduloSPID.cancelacionPagos.text.est" var="est"/>
<spring:message code="moduloSPID.cancelacionPagos.text.topo" var="topo"/>
<spring:message code="moduloSPID.cancelacionPagos.text.pri" var="pri"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.bitacoraEnvio.text.cveInstitucion" var="cveInstitucion"/>

<spring:message code="moduloSPID.cancelacionPagos.text.abono" var="imp"/>

<spring:message code="moduloSPID.avisosTraspasos.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.regresar" var="regresar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.exportarTodo" var="exportarTodo"/>


  <script type="text/javascript">
       var mensajes = {"aplicacion": '${aplicacion}', "OK00002V": '${OK00002V}', "ED00019V": '${ED00019V}',	"ED00020V": '${ED00020V}', "ED00021V": '${ED00021V}', "ED00022V": '${ED00022V}', "ED00023V": '${ED00023V}',
		"ED00027V": '${ED00027V}', "ED00011V": '${ED00011V}'};
		
	   var tipoFiltro = '${tipoFiltro}';
	   var tipo = '${tipo}';
	   var url = '${url}';
    </script>
    
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/cancelacionPagos.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/cancelPagos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form name="idForm" id="idForm" method="post">

  <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResCancelacionPagos.beanPaginador.paginaIni}"/>
	<input type="hidden" name="pagina" id="pagina" value="${beanResCancelacionPagos.beanPaginador.pagina}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResCancelacionPagos.beanPaginador.paginaFin}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	<input type="hidden" name="opcion" id="opcion" value="${tipoFiltro}">
	<input type="hidden" name="tipo" id="tipo" value="${tipo}">
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas"></div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table >
					<tr>
						<c:choose>
							<c:when test="${tipo eq 1}">
								<td ><input value="1" disabled class="Campos Red" type="radio"   name="filtra" onclick="nextPage('idForm','cancelacionPagosOrden.do');" >Orden</td>
								<td ><input  value="2" disabled class="Campos Red" type="radio"   name="filtra" onclick="nextPage('idForm', 'cancelacionPagosTraspasos.do');">Traspasos</td>
								<td ><input  value="3" disabled class="Campos Red" type="radio"   name="filtra" onclick="nextPage('idForm','cancelacionPagos.do');" >Ambos</td>
							</c:when>
						<c:otherwise>
						<c:choose>
							<c:when test="${tipoFiltro eq 1}">
								<td ><input value="1" class="Campos Red opcion" type="radio"  checked="checked" name="filtra">Orden de pago</td>
							</c:when>
							<c:otherwise>
								<td ><input value="1" class="Campos Red opcion" type="radio"   name="filtra">Orden de pago</td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${tipoFiltro eq 2}">
								<td ><input  value="2" class="Campos Red opcion" type="radio"  checked="checked" name="filtra">Traspasos</td>
							</c:when>
							<c:otherwise>
								<td ><input  value="2" class="Campos Red opcion" type="radio"   name="filtra">Traspasos</td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${tipoFiltro eq 3}">
								<td ><input  value="3" class="Campos Red opcion" type="radio" checked="checked"  name="filtra">Ambos</td>
							</c:when>
							<c:otherwise>
								<td ><input  value="3" class="Campos Red opcion" type="radio"   name="filtra">Ambos</td>
							</c:otherwise>
						</c:choose>
						</c:otherwise>
					</c:choose>
					</tr>
				</table>
				<table>
					<tr>
						<th width="122" class="text_izquierda nowrap"></th>
						<th width="122" data-id="referencia" class="text_izquierda nowrap sortField">Referencia
							<c:if test="${sortField == 'referencia'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cola" class="text_centro nowrap sortField" scope="col">Cola
							<c:if test="${sortField == 'cola'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="cvnTran" class="text_centro nowrap sortField" scope="col">Cvn.Tran
							<c:if test="${sortField == 'cvnTran'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="mecan" class="text_centro nowrap sortField" scope="col">Mecan
							<c:if test="${sortField == 'mecan'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="medioEnt" class="text_centro nowrap sortField" scope="col">Medio Ent
							<c:if test="${sortField == 'medioEnt'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="ord" class="text_centro nowrap sortField" scope="col">Ord
							<c:if test="${sortField == 'ord'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="rec" class="text_centro nowrap sortField" scope="col">Rec
							<c:if test="${sortField == 'rec'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="importeAbono" class="text_centro nowrap sortField" scope="col">Importe Abono
							<c:if test="${sortField == 'importeAbono'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="paq" class="text_centro nowrap sortField" scope="col">Paq
							<c:if test="${sortField == 'paq'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="pago" class="text_centro nowrap sortField" scope="col">Pago
							<c:if test="${sortField == 'pago'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="est" class="text_centro nowrap sortField" scope="col">Est.
							<c:if test="${sortField == 'est'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="topo" class="text_centro nowrap sortField" scope="col">Topo
							<c:if test="${sortField == 'topo'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="122" data-id="pri" class="text_centro nowrap sortField" scope="col">Pri
							<c:if test="${sortField == 'pri'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
					</tr>
					<body>
						<c:forEach var="beanCancelPagos" items="${beanResCancelacionPagos.listaCancelacionPagos}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">
									<input class="seleccionCheck" name="listaCancelacionPagos[${rowCounter.index}].seleccionado" value="true" type="checkbox"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.referencia" value="${beanCancelPagos.beanCancelacionPagosDos.referencia}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.cola" value="${beanCancelPagos.beanCancelacionPagosDos.cola}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.cvnTran" value="${beanCancelPagos.beanCancelacionPagosDos.cvnTran}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.mecan" value="${beanCancelPagos.beanCancelacionPagosDos.mecan}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.medioEnt" value="${beanCancelPagos.beanCancelacionPagosDos.medioEnt}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.ord" value="${beanCancelPagos.beanCancelacionPagosDos.ord}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.rec" value="${beanCancelPagos.beanCancelacionPagosDos.rec}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.importeAbono" value="${beanCancelPagos.beanCancelacionPagosDos.importeAbono}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.paq" value="${beanCancelPagos.beanCancelacionPagosDos.paq}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.pago" value="${beanCancelPagos.beanCancelacionPagosDos.pago}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.est" value="${beanCancelPagos.beanCancelacionPagosDos.est}"/>
									<input class="topologia" type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.topo" value="${beanCancelPagos.beanCancelacionPagosDos.topo}"/>
									<input type="hidden" name="listaCancelacionPagos[${rowCounter.index}].beanCancelacionPagosDos.pri" value="${beanCancelPagos.beanCancelacionPagosDos.pri}"/>									
								</td>
							
							
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.referencia}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.cola}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.cvnTran}</td>
								  <td class="text_izquierda mecan">${beanCancelPagos.beanCancelacionPagosDos.mecan}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.medioEnt}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.ord}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.rec}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.importeAbono}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.paq}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.pago}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.est}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.topo}</td>
								  <td class="text_izquierda">${beanCancelPagos.beanCancelacionPagosDos.pri}</td>
							</tr>
						</c:forEach>
					</body>
				</table>
			</div>
			<c:if test="${not empty beanResCancelacionPagos.listaCancelacionPagos}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResCancelacionPagos.totalReg} registros | Importe p�gina: ${importePagina}</label>
					</div>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaIni == beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaIni != beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${url}','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaAnt!='0' && beanResCancelacionPagos.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','${url}','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaAnt=='0' || beanResCancelacionPagos.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResCancelacionPagos.beanPaginador.pagina} - ${beanResCancelacionPagos.beanPaginador.paginaFin}</label>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaFin != beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${url}','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaFin == beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaFin != beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${url}','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResCancelacionPagos.beanPaginador.paginaFin == beanResCancelacionPagos.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
				<label>Importe Total: ${beanResCancelacionPagos.importeTotal}</label>
			</c:if>
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
					<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
								</c:otherwise>
							</c:choose>
						<td class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							 </c:choose>
					
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						
						
						<td class="odd">&nbsp;</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="der"><a id="idCancelar" href="javascript:cancelar('${tipoRadio}');" >Cancelar</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:cancelar('${tipoRadio}');" >Cancelar</a></td>
								</c:otherwise>
							 </c:choose>
						
					</tr>
				</table>
				</div>
			</div>
		</div>
	</div>	
</form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>