<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloSPID.jsp" flush="true" />

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
       
<spring:message code="moduloSPID.myMenu.text.moduloSPID" var="tituloModulo"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.consultaRecepciones.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="moduloSPID.consultaRecepciones.text.txtTopo"      var="txtTopo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtTipo"      var="txtTipo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtEstatus"   var="txtEstatus"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFchOperacion" var="txtFchOperacion"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtHora"      var="txtHora"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtIntsOrd"   var="txtIntsOrd"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtInterOrd"   var="txtInterOrd"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFoliosPaquete" var="txtFoliosPaquete"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFoliosPago" var="txtFoliosPago"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCuentaRec" var="txtCuentaRec"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCuentaTran" var="txtCuentaTran"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtImporte"   var="txtImporte"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCodErr"    var="txtCodErr"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtRefTrans"  var="txtRefTrans"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtDevol"     var="txtDevol"/>
<spring:message code="moduloSPID.consultaRecepciones.link.verDetalle"   var="verDetalle"/>
<spring:message code="moduloSPID.consultaRecepciones.link.enviaTransf"   var="enviaTransf"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtTotales"   var="txtTotales"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtSumatoria"   var="txtSumatoria"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtApart"   var="txtApart"/>


<spring:message code="moduloSPID.consultaRecepciones.check.txtTodas"           var="txtTodas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoVLiquidadas" var="txtTopoVLiquidadas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoTLiquidadas" var="txtTopoTLiquidadas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoTXLiquidar"  var="txtTopoTXLiquidar"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtRechazos"        var="txtRechazos"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtApTransfer"      var="txtApTransfer"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtDevoluciones"    var="txtDevoluciones"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtRechazoCMotDev"  var="txtRechazoCMotDev"/>
<spring:message code="moduloSPID.consultaRecepciones.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="moduloSPID.consultaRecepciones.link.rechazar"            var="txtRechazar"/>
<spring:message code="moduloSPID.consultaRecepciones.link.actMotRechazo"       var="txtActMotRechazo"/>

<spring:message code="moduloSPID.consultaRecepciones.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.consultaRecepciones.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.consultaRecepciones.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.consultaRecepciones.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.consultaRecepciones.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.detalle" var="detalle"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>

<script type="text/javascript">
       var mensajes = {"aplicacion" : '${aplicacion}',
		"OK00002V" : '${OK00002V}', "ED00019V" : '${ED00019V}', "ED00020V" : '${ED00020V}',
		"ED00021V" : '${ED00021V}', "ED00022V" : '${ED00022V}', "ED00023V" : '${ED00023V}',
		"ED00027V" : '${ED00027V}', "ED00011V" : '${ED00011V}', "EC00011B" : '${EC00011B}'};
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/consultaRecepciones.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${beanResConsultaRecepciones.titulo}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
	 
	<form name="idForm" id="idForm" method="post">
	    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResConsultaRecepciones.beanPaginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${beanResConsultaRecepciones.beanPaginador.pagina}"/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResConsultaRecepciones.beanPaginador.paginaFin}"/>
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" name="sortField" id="sortField" value="${sortField}">
		<input type="hidden" name="sortType" id="sortType" value="${sortType}">
		<input type="hidden" id=ref value="${beanResConsultaRecepciones.tipoOrden}"  type="text" size="40"  name="ref">
		<input type="hidden" name="fchOperacion" id="fchOperacion"/>				
		<input type="hidden" name="cveMiInstituc" id="cveMiInstituc"/>
		<input type="hidden" name="cveInstOrd" id="cveInstOrd"/>
		<input type="hidden" name="folioPaquete" id="folioPaquete"/>
		<input type="hidden" name="folioPago" id="folioPago"/>
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" data-id="topologia" class="text_centro sortField" scope="col">${txtTopo}
						<c:if test="${sortField == 'topologia'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="tipoPago" class="text_centro sortField" scope="col">${txtTipo}
						<c:if test="${sortField == 'tipoPago'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="estatusBanxico" class="text_centro sortField" scope="col" colspan="2">${txtEstatus}
						<c:if test="${sortField == 'estatusBanxico'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="fechOperacion" class="text_centro sortField" scope="col">${txtFchOperacion}
						<c:if test="${sortField == 'fechOperacion'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="fechCaptura" class="text_centro sortField" scope="col">${txtHora}
						<c:if test="${sortField == 'fechCaptura'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="cveInsOrd" class="text_izquierda sortField">${txtIntsOrd}
						<c:if test="${sortField == 'cveInsOrd'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="cveIntermeOrd" class="text_izquierda sortField">${txtInterOrd}
						<c:if test="${sortField == 'cveIntermeOrd'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="folioPaquete" class="text_izquierda sortField">${txtFoliosPaquete}
						<c:if test="${sortField == 'folioPaquete'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="folioPago" class="text_izquierda sortField">${txtFoliosPago}
						<c:if test="${sortField == 'folioPago'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="numCuentaRec" class="text_izquierda sortField">${txtCuentaRec}
						<c:if test="${sortField == 'numCuentaRec'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="monto" class="text_izquierda sortField">${txtImporte}
						<c:if test="${sortField == 'monto'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="motivoDevol" class="text_izquierda sortField">${txtDevol}
						<c:if test="${sortField == 'motivoDevol'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="codigoError" class="text_izquierda sortField">${txtCodErr}
						<c:if test="${sortField == 'codigoError'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" data-id="numCuentaTran" class="text_izquierda sortField">${txtCuentaTran}
						<c:if test="${sortField == 'numCuentaTran'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="122" class="text_izquierda"> </th>
				</tr>
				<tbody>				
					<c:forEach var="consultaRecepciones" items="${beanResConsultaRecepciones.listaConsultaRecepciones}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">${consultaRecepciones.beanConsultaRecepcionesDos.topologia}</td>
							<td class="text_izquierda">${consultaRecepciones.beanConsultaRecepcionesDos.tipoPago}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.estatusBanxico}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.estatusTransfer}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.fechOperacion}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.fechCaptura}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.cveInsOrd}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.cveIntermeOrd}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.folioPaquete}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.folioPago}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaRec}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.monto}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.motivoDevol}</td>
							<td class="text_centro">${consultaRecepciones.codigoError}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaTran}</td>
							<td class="text_centro campos"><a class="idVerDetalle" href="javascript:;">${detalle}</a>
								<input type="hidden" class="fchOperacion" value="${consultaRecepciones.beanConsultaRecepcionesDos.fechOperacion}"/>				
								<input type="hidden" class="cveMiInstituc" value="${sessionSPID.cveInstitucion}"/>
								<input type="hidden" class="cveInstOrd" value="${consultaRecepciones.beanConsultaRecepcionesDos.cveInsOrd}"/>
								<input type="hidden" class="folioPaquete" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPaquete}"/>
								<input type="hidden" class="folioPago" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPago}"/>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
			<c:if test="${not empty beanResConsultaRecepciones.listaConsultaRecepciones}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResConsultaRecepciones.totalReg} registros | Importe p�gina: ${importePagina}</label>
					</div>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaIni == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaIni != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepciones.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaAnt!='0' && beanResConsultaRecepciones.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepciones.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaAnt=='0' || beanResConsultaRecepciones.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResConsultaRecepciones.beanPaginador.pagina} - ${beanResConsultaRecepciones.beanPaginador.paginaFin}</label>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepciones.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepciones.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
				<label>Importe Total: ${beanResConsultaRecepciones.importeTotal}</label>
			</c:if>
	</div>
			<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
									<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
								</c:otherwise>
							</c:choose>
						<td class="odd"></td>
						<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
						</c:choose>
					
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd"></td>
						<td width="279" id="idRegresar" class="der"><a href="javascript:;" >${Regresar}</a></td>					
					</tr>
				</table>

			</div>
		</div>
			
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>