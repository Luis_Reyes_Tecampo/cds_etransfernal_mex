<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntListarEquivalencias" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<!-- Titulo pagina -->
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.listadoEquivalencias.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<!-- Etiquetas para pagina -->
<spring:message code="moduloSPID.listadoEquivalencias.text.claveOper" var="claveOper"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.clasificacion" var="clasificacion"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.descripcion" var="descripcion"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.horario" var="horario"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.agregar" var="agregar"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.editar" var="editar"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.listadoEquivalencias.text.eliminar" var="eliminar"/>

<!-- Mensajes Generales de la pantalla -->
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<!-- Menasajes para la aplicacion -->
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}', "ED00065V":'${ED00065V}',
                    "CD00167V":'${CD00167V}',
                    "ED00068V":'${ED00068V}'
                    };
	 
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/confEquivalencia.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResEquivalencia.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResEquivalencia.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResEquivalencia.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" name="sortField" id="sortField" value="${sortField}">
		<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
					<tr>
						<th width="110" class="text_centro" scope="col"></th>
						<th width="150" data-id="claveOper" class="text_centro sortField" scope="col">${claveOper}
							<c:if test="${sortField == 'claveOper'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="tipoPago" class="text_centro sortField" scope="col">${tipoPago}
							<c:if test="${sortField == 'tipoPago'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="clasificacion" class="text_centro sortField" scope="col">${clasificacion}
							<c:if test="${sortField == 'clasificacion'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="descripcion" class="text_centro sortField" scope="col">${descripcion}
							<c:if test="${sortField == 'descripcion'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="270" data-id="horario" class="text_centro sortField" scope="col">${horario}
							<c:if test="${sortField == 'horario'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
					</tr>
					<tbody>				
						<c:forEach var="beanEquivalencia" items="${beanResEquivalencia.beanEquivalencias}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td class="text_centro">
									<input name="listaEquivalencias[${rowCounter.index}].seleccionado" value="true" type="checkbox"/>
									<input type="hidden" name="listaEquivalencias[${rowCounter.index}].claveOper" value="${beanEquivalencia.claveOper}"/>
									<input type="hidden" name="listaEquivalencias[${rowCounter.index}].tipoPago" value="${beanEquivalencia.tipoPago}"/>
									<input type="hidden" name="listaEquivalencias[${rowCounter.index}].clasificacion" value="${beanEquivalencia.clasificacion}"/>
									<input type="hidden" name="listaEquivalencias[${rowCounter.index}].descripcion" value="${beanEquivalencia.descripcion}"/>
									<input type="hidden" name="listaEquivalencias[${rowCounter.index}].horario" value="${beanEquivalencia.horario}"/>																	
								</td>
								<td class="text_centro">${beanEquivalencia.claveOper}</td>
								<td class="text_centro">${beanEquivalencia.tipoPago}</td>
								<td class="text_centro">${beanEquivalencia.clasificacion}</td>
								<td class="text_centro">${beanEquivalencia.descripcion}</td>
								<td style="text-align: right;">${beanEquivalencia.horario}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<c:if test="${not empty beanResEquivalencia.beanEquivalencias}">
				<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResEquivalencia.totalReg} registros</label>
				</div>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaIni == beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaIni != beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listarEquivalencias.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaAnt!='0' && beanResEquivalencia.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','listarEquivalencias.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaAnt=='0' || beanResEquivalencia.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResEquivalencia.beanPaginador.pagina} - ${beanResEquivalencia.beanPaginador.paginaFin}</label>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaFin != beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listarEquivalencias.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaFin == beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaFin != beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','listarEquivalencias.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResEquivalencia.beanPaginador.paginaFin == beanResEquivalencia.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
			</c:if>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a id="idAgregar" href="javascript:;" >${agregar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${agregar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class=""></td>
						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
								<td class="izq"><a id="idEditar" href="javascript:;">${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${editar}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class=""></td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
									<td width="279" class="der"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
								</c:otherwise>	
							</c:choose>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
	</c:if>