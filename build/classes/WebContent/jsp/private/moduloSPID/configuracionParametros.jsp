<%@ include file="../myHeader.jsp" %>

<c:choose>
    <c:when test="${nomPantalla == 'parametrosOperativosSPEI'}">
		<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloSPEI" />
			<jsp:param name="menuSubitem" value="paramOperaSPEI" />
		</jsp:include> 
        <br />
    </c:when>    
    <c:otherwise>
		
       		<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
				<jsp:param name="menuItem"    value="moduloSPID" />
				<jsp:param name="menuSubitem" value="IntConfiguracionParametros" />
			</jsp:include> 
        <br />
    </c:otherwise>
</c:choose>


<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.myMenu.text.moduloSPID" var="tituloModulo"/>
<spring:message code="moduloSPID.configuracionParametros.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloSPID.configuracionParametros.text.Institucion" var="Institucion"/>
<spring:message code="moduloSPID.configuracionParametros.text.CveCESIF" var="cveCESIF"/>
<spring:message code="moduloSPID.configuracionParametros.text.NomInstit" var="nomInstit"/>
<spring:message code="moduloSPID.configuracionParametros.text.OrdPago" var="ordPago"/>
<spring:message code="moduloSPID.configuracionParametros.text.Devoluciones" var="devoluciones"/>
<spring:message code="moduloSPID.configuracionParametros.text.TrasSPID_SIAC" var="trasSPID_SIAC"/>
<spring:message code="moduloSPID.configuracionParametros.text.TrasSPEI_SIAC" var="trasSPEI_SIAC"/>
<spring:message code="moduloSPID.configuracionParametros.text.Mecanismos" var="mecanismos"/>
<spring:message code="moduloSPID.configuracionParametros.text.FEC" var="fec"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECLogin" var="fecLogin"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECPassword" var="fecPassword"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECDestinatario" var="fecDestinatario"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECDireccionIP" var="fecDireccionIP"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECPuertos" var="fecPuertos"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECInicial" var="fecInicial"/>
<spring:message code="moduloSPID.configuracionParametros.text.FECFinal" var="fecFinal"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARA" var="ara"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARADestinatario" var="araDestinatario"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARACertificado" var="araCertificado"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARADireccionIP" var="araDireccionIP"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARAPuertos" var="araPuertos"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARAInicial" var="araInicial"/>
<spring:message code="moduloSPID.configuracionParametros.text.ARAFinal" var="araFinal"/>
<spring:message code="moduloSPID.configuracionParametros.text.guardar" var="guardar"/>
<spring:message code="moduloSPID.configuracionParametros.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.configuracionParametros.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.configuracionParametros.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.configuracionParametros.text.txtModuloSPEI" var="txtModuloSPEI"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.regresar" var="regresar"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.ED00026V" var="ED00026V"/>
<spring:message code="codError.OK00003V" var="OK00003V"/>




<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/configuracionParametros.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"EC00011B":'${EC00011B}',"ED00011V":'${ED00011V}',"ED00026V":'${ED00026V}',"OK00003V":'${OK00003V}'};</script>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/configuracionParametros.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>



<c:set var="searchString" value="${seguTareas}"/>
<c:set var="searchString" value="CONSULTAR, AGREGAR, MODIFICAR, EXPORTAR, ELIMINAR"/>

<div class="pageTitleContainer">
	<c:choose>
    <c:when test="${nomPantalla == 'parametrosOperativosSPEI'}">
		<span class="pageTitle">${txtModuloSPEI}</span> - ${tituloFuncionalidad}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
        <br />
    </c:when>    
    <c:otherwise>
		<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
        <br />
    </c:otherwise>
</c:choose>
</div>
<input id="nomPantalla" name="nomPantalla" value="${nomPantalla}" type="hidden"></input>

<form name="idForm" id="idForm" method="post">
	<div class="contentBuscadorSimple">
		<table>
			<tr>
			<td>
				<table class="groupContent institucion">
				<caption></caption> 
					<thead>
						<tr><td colspan="25">${Institucion}</td></tr>
					</thead>
					<tbody>
						<tr>
							<td>${cveCESIF}</td>
							<td>
								<input id="cveCESIF" value="${configuracionParametros.cveCESIF}" readonly="readonly" type="text" size = 42.9%></input>
								<input id="cveCESIF" name="cveCESIF" value="${configuracionParametros.cveCESIF}" type="hidden"></input>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="cveCESIF"></label></td>
							
							<tr>
								<td>${nomInstit}</td>
								<td><input id="nomInstit" name="nomInstit" value="${configuracionParametros.nomInstit}" type="text" maxlength="50" size = 42.9%/></td>
								<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="nomInstit"></label></td>
							
							</tr>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			</tr>
			<td>
				<table class="groupContent mecanismos">
				<caption></caption>
					<thead>
						<tr><td colspan="25">${mecanismos}</td></tr>
					</thead>
					<tbody>
						<tr>
							<td>${ordPago}</td>
							<td><input id="ordPago" name="ordPago" type="text" value="${configuracionParametros.ordPago}" maxlength="8" size = 42.9%></input></td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ordPago"></label></td>
						
						</tr>
						<tr>
							<td>${devoluciones}</td>
							<td><input id="devoluciones" name="devoluciones" value="${configuracionParametros.devoluciones}" type="text" maxlength="8" size = 42.9%></input></td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="devoluciones"></label></td>
							
						</tr>
						<tr>
						<c:choose>
						<c:when test="${nomPantalla == 'parametrosOperativosSPEI'}">
							<td>${trasSPEI_SIAC}</td>
						</c:when>
						<c:otherwise>
							<td>${trasSPID_SIAC}</td>						
						</c:otherwise>
						</c:choose>
							<td><input id="trasSPIDSIAC" name="trasSPIDSIAC" value="${configuracionParametros.trasSPIDSIAC}" type="text" maxlength="8" size = 42.9%></input></td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
							<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="trasSPIDSIAC"></label></td>
							
						</tr>							
					</tbody>
				</table>
			</td>
		</table>
		<table class="groupContent fec">
		<caption></caption>
			<thead>
				<tr><td colspan="25">${fec}</td></tr>
			</thead>
			<tbody>
				<tr>
					<td>${fecLogin}</td>				
					<td><input id="fec.fecLogin" name="fec.fecLogin" type="text" value="${configuracionParametros.fec.fecLogin}" maxlength="50" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecLogin"></label></td>
				</tr>
				<tr>
					<td>${fecPassword}</td>
					<td><input id="fec.fecPassword" name="fec.fecPassword" type="password" value="${configuracionParametros.fec.fecPassword}" maxlength="50" size = 42.9% onfocus="javascript:limpia(this);"></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecPassword"></label></td>
				</tr>
				<tr>
					<td>${fecDestinatario}</td>
					<td><input id="fec.fecDestinatario" name="fec.fecDestinatario" value="${configuracionParametros.fec.fecDestinatario}" type="text" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecDestinatario"></label></td>
				</tr>
				<tr>	
					<td>${fecDireccionIP}</td>
					<td><input id="fec.fecDireccionIP" name="fec.fecDireccionIP" value="${configuracionParametros.fec.fecDireccionIP}" type="text" maxlength="30" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecDireccionIP"></label></td>
				</tr>
				<tr>
					<td><strong>${fecPuertos}</strong></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td>
				</tr>
				<tr>
					<td>${fecInicial}</td>
					<td><input id="fec.fecInicial" name="fec.fecInicial" value="${configuracionParametros.fec.fecInicial}" type="text" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecInicial"></label></td>
				</tr>
				<tr>		
					<td>${fecFinal}</td>
					<td><input id="fec.fecFinal" name="fec.fecFinal" value="${configuracionParametros.fec.fecFinal}" type="text" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="fec.fecFinal"></label></td>
				</tr>
			</tbody>
		</table>
		<table class="groupContent ara">
		<caption></caption>
			<thead>
				<tr><td colspan="25">${ara}</td></tr>
			</thead>
			<tbody>
				<tr>
					<td>${araDestinatario}</td>
					<td><input id="ara.araDestinatario" name="ara.araDestinatario" type="text" value="${configuracionParametros.ara.araDestinatario}" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ara.araDestinatario"></label></td>
				</tr>
				<tr>	
					<td>${araDireccionIP}</td>
					<td><input id="ara.araDireccionIP" name="ara.araDireccionIP" type="text" value="${configuracionParametros.ara.araDireccionIP}" maxlength="30" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ara.araDireccionIP"></label></td>
				</tr>
				<tr>
					<td>${araCertificado}</td>
					<td><input id="ara.araCertificado" name="ara.araCertificado" value="${configuracionParametros.ara.araCertificado}" type="text" maxlength="50" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ara.araCertificado"></label></td>
				</tr>
				<tr>
					<td><strong>${araPuertos}</strong></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td>
				</tr>
				<tr>
					<td>${araInicial}</td>
					<td><input id="ara.araInicial" name="ara.araInicial" type="text" value="${configuracionParametros.ara.araInicial}" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ara.araInicial"></label></td>
				</tr>
				<tr>		
					<td>${araFinal}</td>
					<td><input id="ara.araFinal" name="ara.araFinal" type="text" value="${configuracionParametros.ara.araFinal}" maxlength="7" size = 42.9%></input></td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td>
					<td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td>${espacioEnBlanco}</td><td><label for="ara.araFinal"></label></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
			<caption></caption>
				<tr>
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
							<td  class="izq"><a id="guardar" href="javascript:;" >${guardar}</a></td>
						</c:when>
						<c:otherwise>
							<td  class="izq_Des"><a href="javascript:;" >${guardar}</a></td>
						</c:otherwise>
					</c:choose>
					<td class=""></td>
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
							<td  class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
						</c:when>
						<c:otherwise>
							<td class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
		</div>
	</div>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">${tipoError}(mensajes['${descError}'],'${aplicacion}','${codError}','');</script>
</c:if>