<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntInicioConsultaReceptorHist" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloSPID.recepcionOperacionHist.text.tituloFuncionalidad"      var="tituloFuncionalidad"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtTopo"      var="txtTopo"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtTipo"      var="txtTipo"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtEstatus"   var="txtEstatus"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtFchOperacion" var="txtFchOperacion"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtHora"      var="txtHora"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtIntsOrd"   var="txtIntsOrd"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtFoliosPaquete" var="txtFoliosPaquete"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtFoliosPago" var="txtFoliosPago"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtCuentaRec" var="txtCuentaRec"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtImporte"   var="txtImporte"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtUsuario"   var="txtUsuario"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtCodOrd"   var="txtCodOrd"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtCodErr"    var="txtCodErr"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtRefTrans"  var="txtRefTrans"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtDevol"     var="txtDevol"/>
<spring:message code="moduloSPID.recepcionOperacionHist.link.verDetalle"   var="verDetalle"/>
<spring:message code="moduloSPID.recepcionOperacionHist.link.enviaTransf"   var="enviaTransf"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtTotales"   var="txtTotales"/>
<spring:message code="moduloSPID.recepcionOperacionHist.text.txtSumatoria"   var="txtSumatoria"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.recepcionOperacionHist.check.txtTodas"           var="txtTodas"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtTopoVLiquidadas" var="txtTopoVLiquidadas"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtTopoTLiquidadas" var="txtTopoTLiquidadas"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtTopoTXLiquidar"  var="txtTopoTXLiquidar"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtRechazos"        var="txtRechazos"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtApTransfer"      var="txtApTransfer"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtDevoluciones"    var="txtDevoluciones"/>
<spring:message code="moduloSPID.recepcionOperacionHist.check.txtRechazoCMotDev"  var="txtRechazoCMotDev"/>
<spring:message code="moduloSPID.recepcionOperacionHist.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="moduloSPID.recepcionOperacionHist.link.rechazar"           var="txtRechazar"/>
<spring:message code="moduloSPID.recepcionOperacionHist.link.actMotRechazo"           var="txtActMotRechazo"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>


<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>

 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                        "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                        "ED00064V":'${ED00064V}',
                        "ED00065V":'${ED00065V}',
                        "CD00167V":'${CD00167V}',
                        "CD00168V":'${CD00168V}'
                        };
	 
    </script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/consultaReceptorHist.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/consReceptorHist.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>
 
<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
	
<form name="idForm" id="idForm" method="post">

		<input type="hidden" name="accion" id="accion" value=""/>
		<input type="hidden" name="paginaIni" id="paginaIni" value="${beanResConsRecepHist.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResConsRecepHist.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResConsRecepHist.beanPaginador.paginaFin}">
		<input type="hidden" name="fchOperacion" id="fchOperacion"/>				
		<input type="hidden" name="cveMiInstituc" id="cveMiInstituc"/>
		<input type="hidden" name="cveInstOrd" id="cveInstOrd"/>
		<input type="hidden" name="folioPaquete" id="folioPaquete"/>
		<input type="hidden" name="folioPago" id="folioPago"/>
		
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${subtituloFuncion}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						<input type="radio" name="opcion" checked="checked" value="TODAS" ${"TODAS" eq beanResConsRecepHist.opcion?"checked":""} />${txtTodas}
					</td>
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="TVL" ${'TVL' eq beanResConsRecepHist.opcion?"checked":""}/>${txtTopoVLiquidadas}
					</td>		
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="TTL" ${"TTL" eq beanResConsRecepHist.opcion?"checked":""}/>${txtTopoTLiquidadas}
					</td>
				</tr>
				<tr>
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="APT" ${"APT" eq beanResConsRecepHist.opcion?"checked":""}/>${txtApTransfer}
					</td>
					
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="R" ${"R" eq beanResConsRecepHist.opcion?"checked":""}/>${txtRechazos}
					</td>
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="RMD" ${"RMD" eq beanResConsRecepHist.opcion?"checked":""}/>${txtRechazoCMotDev}
					</td>					
				</tr>
				<tr>
					<td class="text_izquierda">
						<input type="radio" name="opcion" value="DEV" ${"DEV" eq beanResConsRecepHist.opcion?"checked":""}/>${txtDevoluciones}
					</td>					
					<td>
						<span><a id="idBuscar" href="javascript:;">${txtBuscar}</a></span>	
					</td>
				</tr>
				
				
			</table>
		</div>
	</div>



	<div class="frameTablaVariasColumnas">
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table id="tablaDatos" name="tablaDatos">
				<tr>
					<th class="text_centro chkTodos" scope="col">${txtTopo}</th>
					<th class="text_centro" scope="col">${txtTipo}</th>
					<th class="text_centro" scope="col" colspan="2">${txtEstatus}</th>
					<th class="text_centro" scope="col">${txtFchOperacion}<br><input name="fecha" placeholder="DD/MM/YYYY" id="fecha" value="${beanResConsRecepHist.fecha}" class="text_derecha fecha"/></th>
					<th class="text_centro" scope="col">${txtHora}</th>
					<th class="text_centro" scope="col">${txtCodOrd}</th>
					<th class="text_izquierda">${txtIntsOrd}<br><input name="instOrd" id="instOrd" class="text_derecha instOrd"onkeyup="doSearch(this,7);"/></th>
					<th class="text_izquierda">${txtFoliosPaquete}<br><input name="paq" id="paq" class="text_derecha paq" onkeyup="doSearch(this,8);"/></th>
					<th class="text_izquierda">${txtFoliosPago}<br><input name="pago" id="pago" class="text_derecha pago" onkeyup="doSearch(this,9);" /></th>
					<th class="text_izquierda">${txtCuentaRec}<br><input name="cuentaR" id="cuentaR" class="text_derecha cuentaR" onkeyup="doSearch(this,10);"/></th>
					<th class="text_izquierda">${txtImporte}<br><input name="importe" id="importe" class="text_derecha importe" onkeyup="doSearch(this,11);"/></th>
					<th class="text_izquierda">${txtUsuario}<br><input name="usuario" id="usuario" class="text_derecha usuario" onkeyup="doSearch(this,12);"/></th>
					<th class="text_izquierda">${txtCodErr}<br><input name="codErr" id="codErr" class="text_derecha codErr" onkeyup="doSearch(this,13);"/></th>
					<th class="text_izquierda">${txtDevol}<br><input name="devol" id="devol" class="text_derecha devol" onkeyup="doSearch(this,14);"/></th>
					<th class="text_izquierda">${verDetalle}</th>
				</tr>
				<tbody>				
					<c:forEach var="datosConsulta" items="${beanResConsRecepHist.listaDatos}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="campos">
								${datosConsulta.beanConsRecepHistDos.topologia}
								<input type="hidden" id="chkbox${rowCounter.index}.topologia" name="chkbox${rowCounter.index}.topologia" value="${datosConsulta.beanConsRecepHistDos.topologia}"/>
								<input type="hidden" id="chkbox${rowCounter.index}.estatusBanxico" name="chkbox${rowCounter.index}.estatusBanxico" value="${datosConsulta.beanConsRecepHistDos.estatusBanxico}"/>
								<input type="hidden" id="chkbox${rowCounter.index}.estatusTransfer" name="chkbox${rowCounter.index}.estatusTransfer" value="${datosConsulta.beanConsRecepHistDos.estatusTransfer}"/>
								<input type="hidden" id="chkbox${rowCounter.index}.monto" name="chkbox${rowCounter.index}.monto" value="${datosConsulta.beanConsRecepHistDos.monto}"/>
								<input type="hidden" class="fchOperacion" value="${datosConsulta.fechaOperacion}"/>				
								<input type="hidden" class="cveMiInstituc" value="${datosConsulta.cveMiInstituc}"/>
								<input type="hidden" class="cveInstOrd" value="${datosConsulta.beanConsRecepHistDos.cveInstOrd}"/>
								<input type="hidden" class="folioPaquete" value="${datosConsulta.beanConsRecepHistDos.folioPaquete}"/>
								<input type="hidden" class="folioPago" value="${datosConsulta.beanConsRecepHistDos.folioPago}"/>
								<input type="hidden" class="opcion" value="${opcion}"/>
							</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.tipoPago}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.estatusBanxico}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.estatusTransfer}</td>
							<td class="text_centro">${datosConsulta.fechaOperacion}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.hora}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.cveInstOrd}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.cveIntermeOrd}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.folioPaquete}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.folioPago}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.numCuentaTran}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.monto}</td>
							<td class="text_centro">${datosConsulta.usuario}</td>
							<td class="text_centro">${datosConsulta.codError}</td>
							<td class="text_centro">${datosConsulta.beanConsRecepHistDos.motivoDevol} - ${datosConsulta.beanConsRecepHistDos.motivoDevolDesc}</td>
							<td><a class="idVerDetalle" href="javascript:;">${verDetalle}</a></td>
						</tr>
					</c:forEach>
					<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanResConsRecepHist.listaDatos}">
			<div class="paginador">
				<div style="text-align: left; float:left; margin-left: 5px;">
					<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResConsRecepHist.totalReg} registros | Importe p�gina: ${importePagina}</label>
				</div>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaIni == beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaIni != beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','realizaConsultaReceptorHist.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaAnt!='0' && beanResConsRecepHist.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','realizaConsultaReceptorHist.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaAnt=='0' || beanResConsRecepHist.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResConsRecepHist.beanPaginador.pagina} - ${beanResConsRecepHist.beanPaginador.paginaFin}</label>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaFin != beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','realizaConsultaReceptorHist.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaFin == beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaFin != beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','realizaConsultaReceptorHist.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResConsRecepHist.beanPaginador.paginaFin == beanResConsRecepHist.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			<label>Importe Total: ${beanResConsRecepHist.importeTotal}</label>
			</c:if>

		</div>
		
		
		<div class="frameTablaVariasColumnas">
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" class="text_centro" scope="col"></th>
					<th width="223" class="text_centro" scope="col">${txtTopoVLiquidadas}</th>
					<th width="122" class="text_centro" scope="col">${txtTopoTLiquidadas}</th>
					<th width="122" class="text_centro" scope="col">${txtRechazos}</th>
				</tr>
				<tbody>	
						<tr class="odd1">
							<td class="text_izquierda">${txtSumatoria}</td>
							<td class="text_centro"><label class="Campos_des" name="regsTopoVLiq" id="regsTopoVLiq"/>${beanResConsRecepHist.beanConsReceptHistSum.topoVLiqCount}</td>
							<td class="text_centro"><label class="Campos_des" name="regsTopoLiq" id="regsTopoLiq"/>${beanResConsRecepHist.beanConsReceptHistSum.topoTLiqCount}</td>
							<td class="text_centro"><label class="Campos_des" name="regsRechazos" id="regsRechazos"/>${beanResConsRecepHist.beanConsReceptHistSum.rechazadasCount}</td>
						</tr>			
						<tr class="odd2">
							<td class="text_izquierda">${txtTotales}</td>
							<td class="text_centro"	><label name="sumaTopoVLiq" id="sumaTopoVLiq"/>${beanResConsRecepHist.beanConsReceptHistSum.topoVLiqSum}</td>
							<td class="text_centro" ><label name="sumaTopoLiq" id="sumaTopoLiq"/>${beanResConsRecepHist.beanConsReceptHistSum.topoTLiqSum}</td>
							<td class="text_centro" ><label name="sumaRechazos" id="sumaRechazos"/>${beanResConsRecepHist.beanConsReceptHistSum.rechazadasSum}</td>
						</tr>
				</tbody>
			</table>
			</div>
		</div>	
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>	