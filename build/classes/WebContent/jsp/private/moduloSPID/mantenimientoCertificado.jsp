<%@ include file="../myHeader.jsp" %>

<c:choose>
    <c:when test="${nomPantalla == 'mantenimientoCertificadoSPEI'}">
		<jsp:include page="../myMenuModuloSPEI.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloSPEI" />
			<jsp:param name="menuSubitem" value="mantoCerti" />
			        </jsp:include> 
        <br />
    </c:when>    
    <c:otherwise>
       	<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
			<jsp:param name="menuItem"    value="moduloSPID" />
			<jsp:param name="menuSubitem" value="IntMantenimientoCertificados" />
			        </jsp:include> 
        <br />
    </c:otherwise>
</c:choose>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<%-- Titulo pagina --%>
<spring:message code="moduloSPID.mantenimiento.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="moduloSPID.configuracionParametros.text.txtModuloSPEI" var="txtModuloSPEI"/>

<%-- Etiquetas para pagina --%>
<spring:message code="moduloSPID.mantenimiento.text.numeroCertificado" var="numeroCertificado"/>
<spring:message code="moduloSPID.mantenimiento.text.estado" var="estado"/>
<spring:message code="moduloSPID.mantenimiento.text.palabraClave" var="palabraClave"/>
<spring:message code="moduloSPID.mantenimiento.text.mantenimiento" var="mantenimiento"/>
<spring:message code="moduloSPID.mantenimiento.text.facultades" var="facultades"/>
<spring:message code="moduloSPID.mantenimiento.text.usuario" var="usuario"/>
<spring:message code="moduloSPID.mantenimiento.text.fechaAlta" var="fechaAlta"/>
<spring:message code="moduloSPID.mantenimiento.text.fechaModificacion" var="fechaModificacion"/>
<spring:message code="moduloSPID.mantenimiento.text.mensaje" var="mensaje"/>
<spring:message code="moduloSPID.mantenimiento.text.agregar" var="agregar"/>
<spring:message code="moduloSPID.mantenimiento.text.editar" var="editar"/>
<spring:message code="moduloSPID.mantenimiento.text.eliminar" var="eliminar"/>
<spring:message code="moduloSPID.mantenimiento.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.mantenimiento.text.mant" var="mant"/>
<spring:message code="moduloSPID.mantenimiento.text.firma" var="firma"/>
<spring:message code="moduloSPID.mantenimiento.text.Default" var="Default"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>


<%-- Mensajes Generales de la pantalla --%>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<%-- Menasajes para la aplicacion --%>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.ED00061V" var="ED00061V"/>
<spring:message code="codError.ED00078V" var="ED0078V"/>
<spring:message code="codError.ED00139V" var="ED00139V"/>

<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}',"ED00065V":'${ED00065V}',"ED00061V":'${ED00061V}',"ED00078V":'${ED00078V}',"ED00139V":'${ED00139V}'};</script>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/mantenimientoCertificado.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<c:choose>
		<c:when test="${nomPantalla =='mantenimientoCertificadoSPEI'}">
			<span class="pageTitle">${txtModuloSPEI}</span> - ${tituloFuncionalidad}
			<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 		</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
		<br/>
		</c:when>
		<c:otherwise>
			<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
			<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
			${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 		</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
		<br/>
		</c:otherwise>
	</c:choose>
</div>

<input id="nomPantalla" name="nomPantalla" value="${nomPantalla}" type="hidden"></input>

<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResMatenimientoCertificado.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResMatenimientoCertificado.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResMatenimientoCertificado.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<div class="frameTablaVariasColumnas">
		<%--  <div class="titleTablaVariasColumnas">${infEncontrada}</div> --%>
			<div style="background-color: #F2F2F2;">				
				<table style="th: 100%;">					
					<c:forEach var="beanMantenimientoCertificado" items="${beanResMatenimientoCertificado.listaMantenimientoCertificado}" varStatus="rowCounter">
						<tr>
							<div style="background-color: #F2F2F2; margin-top:5px;">
								<div class="titleBuscadorSimple" style="padding-right: 5px; padding-top:6px; width: 98.5%;">
									<p style="float: left;">${numeroCertificado}</p>
									<label for="numCert"></label><input class="inputT Campos" size="20" type="text" id="numCert" name="numeroCertificado" disabled="disabled" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.numeroCertificado}">
									<label for="numCertList"></label><input style="float: right;" id="numCertList" name="listaMantenimientoCertificado[${rowCounter.index}].seleccionado" value="true" type="checkbox"/>
								</div>
								<div>	
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].claveIns" value="${beanMantenimientoCertificado.claveIns}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.numeroCertificado" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.numeroCertificado}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.estado" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.estado}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.palabraClave" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.palabraClave}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.mantenimiento" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].facultadFirma" value="${beanMantenimientoCertificado.facultadFirma}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].facultadMant" value="${beanMantenimientoCertificado.facultadMant}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.usuario" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuario}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.fechaAlta" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaAlta}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.fechaModificacion" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaModificacion}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.usuarioModifica" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuarioModifica}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].defaultFirma" value="${beanMantenimientoCertificado.defaultFirma}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].defaultMant" value="${beanMantenimientoCertificado.defaultMant}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].beanMantenimientoCertificadoDos.mensaje" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mensaje}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].SFacultadFirma" value="${beanMantenimientoCertificado.SFacultadFirma}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].SFacultadMant" value="${beanMantenimientoCertificado.SFacultadMant}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].SDefaultFirma" value="${beanMantenimientoCertificado.SDefaultFirma}"/>
									<input type="hidden" name="listaMantenimientoCertificado[${rowCounter.index}].SDefaultMant" value="${beanMantenimientoCertificado.SDefaultMant}"/>
								</div>
								
								<table style="th: 110%;">
									<tr>
										<div style="margin-top:5px;">
											<p style="float: left; th:10%;">${estado}</p>
											<label for="edo"></label><input class="inputT Campos" size="20" id="edo" type="text" disabled="disabled" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.estado}"/>
											<br/>
											<p style="float: left; th:10%;">${palabraClave}</p>
											<label for="wordKey"></label><input class="inputT Campos" maxlength="50" id="wordKey" size="20" type="password" autocomplete="off" disabled="disabled" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.palabraClave}"/>
											<br/>
											<p style="float: left; th:10%;">${mantenimiento}</p>
								  		<div>	
									  		<label for="field"></label><select class="inputT Campos" id="field" disabled="disabled">
									  			<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq '1'}">
	  												<option value="">Alta Cert. con Facul. Firma</option>
	  											</c:if>
	  											<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq '0'}">
													<option value="">Alta Cert. con Facul. Mant.</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'B'}">
													<option value="">Baja Certificado</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'F'}">
													<option value="">Alta Facultad Firma</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq '2'}">
													<option value="">Baja Facultad Firma</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'M'}">
													<option value="">Alta Facultad Mant.</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq '3'}">
													<option value="">Baja Facultad Mant.</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'A'}">
													<option value="">No hay Mant. Pendiente</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'E'}">
													<option value="">Error Ultimo Mant.</option>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mantenimiento eq 'T'}">
													<option value="">Mant. en Transito</option>
												</c:if>
									  		</select>
								  		</div>
										</div>
									</tr>
									<tr>
									<table style="th: 100%;">
										<caption><%--Registro de certificados--%></caption>
										<tr>
											<td style="float: left; th:10%;">${facultades}</td>
											<td style="float: left; th:12%;">
												<c:if test="${beanMantenimientoCertificado.facultadFirma == true}">
												<label for="facult"></label>
													<input class="inputT Campos" size="1" type="text" id="facult" disabled="disabled" value="${beanMantenimientoCertificado.SFacultadFirma}"/>${firma}<br/>
								  				</c:if>
								  				<c:if test="${beanMantenimientoCertificado.facultadFirma == false}">
								  					<label for="firma"></label><input class="inputT Campos" size="1" id="firma" type="text" disabled="disabled" value="" />${firma}<br/>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.facultadMant == true}">
													<label for="facultM"></label><input class="inputT Campos" size="1" type="text" id="facultM" disabled="disabled" value="${beanMantenimientoCertificado.SFacultadMant}"/>${mant}<br/>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.facultadMant == false}">
													<label for="facultM2"></label><input class="inputT Campos" size="1" type="text" id="facultM2" disabled="disabled" value="" />${mant}<br/>
												</c:if>
											</td>
											<td style="float: left;">${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}</td>
											<td style="float: left; th:8%;">${Default}</td>
								   			<td style="float: left; th:10%;">
												<c:if test="${beanMantenimientoCertificado.defaultFirma == true}">
													<label for="defFirmaT"></label><input size="1" class="inputT Campos" type="text" id="defFirmaT" disabled="disabled" value="${beanMantenimientoCertificado.SDefaultFirma}" />${firma}<br/>
								  				</c:if>
								  				<c:if test="${beanMantenimientoCertificado.defaultFirma == false}">
								  					<label for="defFirmaF"></label><input size="1" class="inputT Campos" type="text" id="defFirmaF" disabled="disabled" value=""/>${firma}<br/>
												</c:if>
												<c:if test="${beanMantenimientoCertificado.defaultMant == true}">
													<label for="defmT"></label><input size="1" class="inputT Campos" type="text" id="defmT" disabled="disabled" value="${beanMantenimientoCertificado.SDefaultMant}"/>${mant}<br/>
							  					</c:if>
							  					<c:if test="${beanMantenimientoCertificado.defaultMant == false}">
							  						<label for="defmF"></label><input size="1" class="inputT Campos" type="text" id="defmF" disabled="disabled" value=""/>${mant}<br/>
							 			 		</c:if>
											</td>
										</tr>
									</table>
									</tr>
									<tr>
										<table style="th: 60%;">
										 <caption><%--Panel de acciones--%></caption>
										  <tr class="odd" style="background: #BDBDBD;">
										    <th id="usuario" th="50">${usuario}</th>
										    <th id="fechaAlta" th="50">${fechaAlta}</th>
										    <th id="fechaModificacion" th="50">${fechaModificacion}</th>
										  </tr>
										  <tr style="background-color: white;">
										  	<c:if test="${not empty beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuario}">
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuario}</td>
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaAlta}</td>
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaModificacion}</td>
										    </c:if>
										    <c:if test="${not empty beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuarioModifica}">
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.usuarioModifica}</td>
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaAlta}</td>
											    <td>${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.fechaModificacion}</td>
										    </c:if>
										  </tr>
										</table>
									</tr>
									<br/>
									<tr>
									<%-- <td  align="center">
								  		 <input type="checkbox" disabled="disabled"/><input type="checkbox" disabled="disabled"/>
								 	</td> --%>
								 		<td style="float: left; wigth:10%" >${mensaje}</td>
								 		<td><label for="message"></label><input type="text" class="inputT Campos" id="message" size="80" disabled="disabled" value="${beanMantenimientoCertificado.beanMantenimientoCertificadoDos.mensaje}"/></td>
								 		<br/>
									</tr>
								</table>
							</div>
						</tr>	
					</c:forEach>					
				</table>
			</div>
			<c:if test="${not empty beanResMatenimientoCertificado.listaMantenimientoCertificado}">
				<c:choose>
					<c:when test="${nomPantalla =='mantenimientoCertificadoSPEI'}">
						<div class="paginador" style="width: 98.9%;">
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaIni == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaIni != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI','INI');">&lt;&lt;${inicio}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaAnt!='0' && beanResMatenimientoCertificado.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI','ANT');">&lt;${anterior}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaAnt=='0' || beanResMatenimientoCertificado.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
							<label id="txtPagina">${beanResMatenimientoCertificado.beanPaginador.pagina} - ${beanResMatenimientoCertificado.beanPaginador.paginaFin}</label>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI','SIG');">${siguiente}&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do?nomPantalla=mantenimientoCertificadoSPEI','FIN');">${fin}&gt;&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
						</div>
					</c:when>
					<c:otherwise>
						<div class="paginador" style="width: 98.9%;">
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaIni == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaIni != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do','INI');">&lt;&lt;${inicio}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaAnt!='0' && beanResMatenimientoCertificado.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do','ANT');">&lt;${anterior}</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaAnt=='0' || beanResMatenimientoCertificado.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
							<label id="txtPagina">${beanResMatenimientoCertificado.beanPaginador.pagina} - ${beanResMatenimientoCertificado.beanPaginador.paginaFin}</label>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do','SIG');">${siguiente}&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin != beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','mantenimientoCertificados.do','FIN');">${fin}&gt;&gt;</a></c:if>
							<c:if test="${beanResMatenimientoCertificado.beanPaginador.paginaFin == beanResMatenimientoCertificado.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
						</div>
					</c:otherwise>
				</c:choose>
			</c:if>
		
			<div class="framePieContenedor" style="width: 97.5%;">
				<div class="contentPieContenedor">
					<table>
						<caption><%--Panel de acciones--%></caption>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td th="279" class="izq"><a id="idAgregar" href="javascript:;" >${agregar}</a></td>
								</c:when>
								<c:otherwise>
									<td th="279" class="izq_Des"><a href="javascript:;" >${agregar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class=""></td>
						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td th="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td th="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
								<td class="izq"><a id="idEditar" href="javascript:;">${editar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${editar}</a></td>
							</c:otherwise>
						</c:choose>
							<td th="6" class=""></td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
									<td th="279" class="der"><a id="idEliminar" href="javascript:;" >${eliminar}</a></td>
								</c:when>
								<c:otherwise>
									<td th="279" class="der_Des"><a href="javascript:;">${eliminar}</a></td>
								</c:otherwise>	
							</c:choose>
						</tr>
						<tr>
							<td th="279"></td>
						</tr> 
					</table>
				</div>
			</div>
		</div>
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');</script>
	</c:if>