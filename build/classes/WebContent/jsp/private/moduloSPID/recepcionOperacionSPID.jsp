<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="muestraRecepOpSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="spid.recepOperacion.titulo.txtRecepcionesSpid" var="txtRecepcionesSpid"/>
<spring:message code="spid.recepOperacion.txt.txtTodas" var="txtTodas"/>
<spring:message code="spid.recepOperacion.txt.txtTopoVLiq" var="txtTopoVLiq"/>
<spring:message code="spid.recepOperacion.txt.txtTopoTLiq" var="txtTopoTLiq"/>
<spring:message code="spid.recepOperacion.txt.txtTopoTPorLiq" var="txtTopoTPorLiq"/>
<spring:message code="spid.recepOperacion.txt.txtRechazos" var="txtRechazos"/>
<spring:message code="spid.recepOperacion.txt.txtAplicadasEnTransfer" var="txtAplicadasEnTransfer"/>
<spring:message code="spid.recepOperacion.txt.txtDevoluciones" var="txtDevoluciones"/>
<spring:message code="spid.recepOperacion.txt.txtRechazosMotDev" var="txtRechazosMotDev"/>
<spring:message code="spid.recepOperacion.txt.txtBuscar" var="txtBuscar"/>
<spring:message code="spid.recepOperacion.txt.txtTopo" var="txtTopo"/>
<spring:message code="spid.recepOperacion.txt.txtTip" var="txtTip"/>
<spring:message code="spid.recepOperacion.txt.txtEst" var="txtEst"/>
<spring:message code="spid.recepOperacion.txt.txtFchOper" var="txtFchOper"/>
<spring:message code="spid.recepOperacion.txt.txtHora" var="txtHora"/>
<spring:message code="spid.recepOperacion.txt.txtInstOrd" var="txtInstOrd"/>
<spring:message code="spid.recepOperacion.txt.txtPaquete" var="txtPaquete"/>
<spring:message code="spid.recepOperacion.txt.txtPago" var="txtPago"/>
<spring:message code="spid.recepOperacion.txt.txtCuentaRec" var="txtCuentaRec"/>
<spring:message code="spid.recepOperacion.txt.txtImporte" var="txtImporte"/>
<spring:message code="spid.recepOperacion.txt.txtCodError" var="txtCodError"/>
<spring:message code="spid.recepOperacion.txt.txtDevol" var="txtDevol"/>
<spring:message code="spid.recepOperacion.txt.txtTotalesTopoVLiq" var="txtTotalesTopoVLiq"/>
<spring:message code="spid.recepOperacion.txt.txtTotalesTopoTLiq" var="txtTotalesTopoTLiq"/>
<spring:message code="spid.recepOperacion.txt.txtTotalesTopoTPorLiq" var="txtTotalesTopoTPorLiq"/>
<spring:message code="spid.recepOperacion.txt.txtTotalesRechazos" var="txtTotalesRechazos"/>
<spring:message code="spid.recepOperacion.txt.txtEnvTransferencia" var="txtEnvTransferencia"/>
<spring:message code="spid.recepOperacion.txt.txtRechazar" var="txtRechazar"/>
<spring:message code="spid.recepOperacion.txt.txtApartar" var="txtApartar"/>
<spring:message code="spid.recepOperacion.txt.txtVerDetalle" var="txtVerDetalle"/>
<spring:message code="spid.recepOperacion.txt.txtActualizarMotRechazo" var="txtActualizarMotRechazo"/>
<spring:message code="spid.recepOperacion.txt.txtDesapartar" var="txtDesapartar"/>
<spring:message code="spid.recepOperacion.txt.txtUsuario" var="txtUsuario"/>
<spring:message code="spid.recepOperacion.txt.txtSumatoria" var="txtSumatoria"/>
<spring:message code="spid.recepOperacion.txt.txtTotales" var="txtTotales"/>



<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>
<spring:message code="codError.CD00169V" var="CD00169V"/>



	 <script type="text/javascript">
	 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                        "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                        "ED00064V":'${ED00064V}',
                        "ED00065V":'${ED00065V}',
                        "CD00167V":'${CD00167V}',"CD00168V":'${CD00168V}',"CD00169V":'${CD00169V}'};
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/recepcionOperacionSPID.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${txtModuloSPID}</span> - ${txtRecepcionesSpid}
		<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
		 </span>${institucion}<span class="pageTitle">  ${beanRes.cveMiInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanRes.fchOperacion}</span>
	</div>

	<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value=""/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanRes.paginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="pagina" value="${beanRes.paginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanRes.paginador.paginaFin}"/>
		<input type="hidden" name="opcion" id="opcion" value="${beanRes.opcion}"/>
		<input type="hidden" name="servicio" id="servicio" value="consSPIDRecOp.do"/>
		<input type="hidden" name="opciones.opcion"  value="${beanRes.opcion}"/>
		<input type="hidden" name="opciones.servicio"  value="consSPIDRecOp.do"/>



		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${subtituloFuncion}</div>
			<div class="contentBuscadorSimple">
				<table>
				<tr>
					<td  class="text_izquierda">
						<input type="radio" name="opcionParam" value="TODAS" ${"TODAS" eq beanRes.opcion?"checked":""} />${txtTodas}
					</td>
					<td class="text_izquierda">
						<input type="radio" name="opcionParam" value="TVL" ${'TVL' eq beanRes.opcion?"checked":""}/>${txtTopoVLiq}
					</td>
					<td class="text_izquierda">
						<input type="radio" name="opcionParam" value="TTL" ${"TTL" eq beanRes.opcion?"checked":""}/>${txtTopoTLiq}
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						<input type="radio" name="opcionParam" value="TTXL" ${"TTXL" eq beanRes.opcion?"checked":""}/>${txtTopoTPorLiq}
					</td>

					<td class="text_izquierda">
						<input type="radio" name="opcionParam" value="R" ${"R" eq beanRes.opcion?"checked":""}/>${txtRechazos}
					</td>

					<td class="text_izquierda">
						<input type="radio" name="opcionParam" value="APT" ${"APT" eq beanRes.opcion?"checked":""}/>${txtAplicadasEnTransfer}
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						<input type="radio" name="opcionParam" value="DEV" ${"DEV" eq beanRes.opcion?"checked":""}/>${txtDevoluciones}
					</td>

					<td class="text_izquierda">
						<input type="radio" name="opcionParam" value="RMD" ${"RMD" eq beanRes.opcion?"checked":""}/>${txtRechazosMotDev}
					</td>
					<td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${txtBuscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${txtBuscar}</a></span>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>


			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" class="text_centro" scope="col"><input name="chkTodos" id="chkTodos" type="checkbox"/> ${txtTopo}</th>
					<th width="122" class="text_centro" scope="col">${txtTip}</th>
					<th width="122" class="text_centro" scope="col" colspan="2">${txtEst}</th>
					<th width="122" class="text_centro" scope="col">${txtFchOper}</th>
					<th width="122" class="text_centro" scope="col">${txtHora}</th>
					<th width="122" class="text_izquierda" colspan="2">${txtInstOrd}</th>
					<th width="122" class="text_izquierda">${txtPaquete}</th>
					<th width="122" class="text_izquierda">${txtPago}</th>
					<th width="122" class="text_izquierda">${txtCuentaRec}</th>
					<th width="122" class="text_izquierda">${txtImporte}</th>
					<th width="122" class="text_izquierda">${txtUsuario}</th>
					<th width="122" class="text_izquierda">${txtCodError}</th>
					<c:if test="${'APT' eq beanRes.opcion || 'DEV' eq beanRes.opcion}">
					<th width="122" class="text_izquierda" colspan="2">${txtRefTrans}</th>
					</c:if>
					<c:if test="${!('APT' eq beanRes.opcion || 'DEV' eq beanRes.opcion)}">
					<th width="122" class="text_izquierda" colspan="2">${txtDevol}</th>

					</c:if>
				</tr>




				<tbody>
					<c:forEach var="beanTranSpeiRec" items="${beanRes.listReceptoresSPID}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td>
						<c:choose>
							<c:when test="${!beanRes.bloqueado && beanTranSpeiRec.bloqueo.bloqueado}" >
								<input name="listBeanReceptoresSPID[${rowCounter.index}].opciones.seleccionado" value="true" type="checkbox" disabled="disabled"/>

							</c:when>
							<c:otherwise>
								<input name="listBeanReceptoresSPID[${rowCounter.index}].opciones.seleccionado" value="true" type="checkbox"/>
							</c:otherwise>
						</c:choose>


							${beanTranSpeiRec.datGenerales.topologia}
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].agrupacion" value="${beanTranSpeiRec.agrupacion}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].llave.fchOperacion" value="${beanTranSpeiRec.llave.fchOperacion}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].llave.cveMiInstituc" value="${beanTranSpeiRec.llave.cveMiInstituc}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].llave.cveInstOrd" value="${beanTranSpeiRec.llave.cveInstOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].llave.folioPaquete" value="${beanTranSpeiRec.llave.folioPaquete}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].llave.folioPago" value="${beanTranSpeiRec.llave.folioPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.topologia" value="${beanTranSpeiRec.datGenerales.topologia}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.enviar" value="${beanTranSpeiRec.datGenerales.enviar}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.cda" value="${beanTranSpeiRec.datGenerales.cda}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].estatus.estatusBanxico" value="${beanTranSpeiRec.estatus.estatusBanxico}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].estatus.estatusTransfer" value="${beanTranSpeiRec.estatus.estatusTransfer}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.tipoCuentaOrd" value="${beanTranSpeiRec.bancoOrd.tipoCuentaOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoRec.tipoCuentaRec" value="${beanTranSpeiRec.bancoRec.tipoCuentaRec}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.clasifOperacion" value="${beanTranSpeiRec.datGenerales.clasifOperacion}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.tipoOperacion" value="${beanTranSpeiRec.datGenerales.tipoOperacion}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.cveIntermeOrd" value="${beanTranSpeiRec.bancoOrd.cveIntermeOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.codPostalOrd" value="${beanTranSpeiRec.bancoOrd.codPostalOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].estatus.codigoError" value="${beanTranSpeiRec.estatus.codigoError}"/>

							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.fchConstitOrd" value="${beanTranSpeiRec.bancoOrd.fchConstitOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].fchPagos.fchInstrucPago" value="${beanTranSpeiRec.fchPagos.fchInstrucPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].fchPagos.horaInstrucPago" value="${beanTranSpeiRec.fchPagos.horaInstrucPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].fchPagos.fchAceptPago" value="${beanTranSpeiRec.fchPagos.fchAceptPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].fchPagos.horaAceptPago" value="${beanTranSpeiRec.fchPagos.horaAceptPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.rfcOrd" value="${beanTranSpeiRec.bancoOrd.rfcOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoRec.rfcRec" value="${beanTranSpeiRec.bancoRec.rfcRec}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.refeNumerica" value="${beanTranSpeiRec.bancoOrd.refeNumerica}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoRec.numCuentaTran" value="${beanTranSpeiRec.bancoRec.numCuentaTran}"/>

							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.numCuentaOrd" value="${beanTranSpeiRec.bancoOrd.numCuentaOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoRec.numCuentaRec" value="${beanTranSpeiRec.bancoRec.numCuentaRec}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.tipoPago" value="${beanTranSpeiRec.datGenerales.tipoPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.prioridad" value="${beanTranSpeiRec.datGenerales.prioridad}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].rastreo.longRastreo" value="${beanTranSpeiRec.rastreo.longRastreo}"/>

							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].envDev.folioPaqDev" value="${beanTranSpeiRec.envDev.folioPaqDev}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].envDev.folioPagoDev" value="${beanTranSpeiRec.envDev.folioPagoDev}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.refeTransfer" value="${beanTranSpeiRec.datGenerales.refeTransfer}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.monto" value="${beanTranSpeiRec.datGenerales.monto}"/>
					   		<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.fchCaptura" value="${beanTranSpeiRec.datGenerales.fchCaptura}"/>
					   		<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].rastreo.cveRastreo" value="${beanTranSpeiRec.rastreo.cveRastreo}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].rastreo.cveRastreoOri" value="${beanTranSpeiRec.rastreo.cveRastreoOri}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].rastreo.direccionIp" value="${beanTranSpeiRec.rastreo.direccionIp}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.cde" value="${beanTranSpeiRec.datGenerales.cde}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.nombreOrd" value="${beanTranSpeiRec.bancoOrd.nombreOrd}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoRec.nombreRec" value="${beanTranSpeiRec.bancoRec.nombreRec}"/>
					   		<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bancoOrd.domicilioOrd" value="${beanTranSpeiRec.bancoOrd.domicilioOrd}"/>
					   		<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.conceptoPago" value="${beanTranSpeiRec.datGenerales.conceptoPago}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].envDev.motivoDevAnt" value="${beanTranSpeiRec.envDev.motivoDev}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].bloqueo.usuario" value="${beanTranSpeiRec.bloqueo.usuario}"/>
							<input type="hidden" name="listBeanReceptoresSPID[${rowCounter.index}].datGenerales.fechaCaptura" value="${beanTranSpeiRec.datGenerales.fechaCaptura}"/>


							<input type="hidden" id="selecDevolucion${rowCounter.index}" name="listBeanReceptoresSPID[${rowCounter.index}].opciones.selecDevolucion" value="${beanTranSpeiRec.opciones.selecDevolucion}"/>
							<input type="hidden" id="selecRecuperar${rowCounter.index}" name="listBeanReceptoresSPID[${rowCounter.index}].opciones.selecRecuperar" value="${beanTranSpeiRec.opciones.selecRecuperar}"/>


							</td>
							<td class="text_izquierda">${beanTranSpeiRec.datGenerales.tipoPago}</td>
							<td class="text_centro">${beanTranSpeiRec.estatus.estatusBanxico}</td>
							<td class="text_centro">${beanTranSpeiRec.estatus.estatusTransfer}</td>
							<td class="text_centro">${beanTranSpeiRec.llave.fchOperacion}</td>
							<td class="text_centro">${beanTranSpeiRec.datGenerales.fchCaptura}</td>
							<td class="text_centro">${beanTranSpeiRec.llave.cveInstOrd}</td>
							<td class="text_centro">${beanTranSpeiRec.llave.cveInstOrd}</td>
							<td class="text_centro">${beanTranSpeiRec.llave.folioPaquete}</td>
							<td class="text_centro">${beanTranSpeiRec.llave.folioPago}</td>
							<td class="text_centro">${beanTranSpeiRec.bancoRec.numCuentaTran}</td>
							<td class="text_centro">${beanTranSpeiRec.datGenerales.strMonto}</td>
							<td class="text_centro">${beanTranSpeiRec.bloqueo.usuario}</td>
							<td class="text_centro">${beanTranSpeiRec.estatus.codigoError}</td>


							<c:choose>
								<c:when test="${'APT' eq beanRes.opcion || 'DEV' eq beanRes.opcion}" >
									<td class="text_centro">${beanTranSpeiRec.datGenerales.refeTransfer}</td>
									<td>
										<input type="button" id="idRec" value="Rec" onclick="recepcionOperacionSPID.recuparar(${rowCounter.index})"/>
									</td>
								</c:when>
								<c:otherwise>
									<td class="text_centro">
										<select name="listBeanReceptoresSPID[${rowCounter.index}].envDev.motivoDev" style=" width:125px" class="Campos" id="selectMotivo">
											<option value="">${seleccionarOpcion}</option>
											<c:forEach var="beanMotDev" items="${beanRes.beanMotDevolucionList}" varStatus="row">
											<option value="${beanMotDev.secuencial}" ${beanMotDev.secuencial eq beanTranSpeiRec.envDev.motivoDev?"selected":""}>
												${beanMotDev.secuencial}-${beanMotDev.descripcion}</option>
											</c:forEach>
										</select>
									</td>
								</c:otherwise>
							</c:choose>

							<c:if test="${'RE' eq beanTranSpeiRec.estatus.estatusTransfer && beanRes.bloqueado}">
								<td>
									<input type="button" id="idDevolver" value="Dev" onclick="recepcionOperacionSPID.devolver('${rowCounter.index}')"/>
								</td>
							</c:if>

						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanRes.listReceptoresSPID}">
					<jsp:include page="../paginador.jsp" >
	  					<jsp:param name="paginaIni" value="${beanRes.paginador.paginaIni}" />
	  					<jsp:param name="pagina" value="${beanRes.paginador.pagina}" />
	  					<jsp:param name="paginaAnt" value="${beanRes.paginador.paginaAnt}" />
	  					<jsp:param name="paginaFin" value="${beanRes.paginador.paginaFin}" />
	  					<jsp:param name="servicio" value="consSPIDRecOp.do" />
					</jsp:include>
				</c:if>

		</div>



			<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" class="text_centro" scope="col"></th>
					<th width="223" class="text_centro" scope="col" colspan="2">${txtTotalesTopoVLiq}</th>
					<th width="122" class="text_centro" scope="col" colspan="2">${txtTotalesTopoTLiq}</th>
					<th width="122" class="text_centro" scope="col" colspan="2">${txtTotalesTopoTPorLiq}</th>
					<th width="122" class="text_centro" scope="col" colspan="2">${txtTotalesRechazos}</th>
				</tr>
				<tbody>
						<tr class="odd1">
							<td class="text_izquierda">${txtSumatoria}</td>
							<td class="text_izquierda" id="idVolumenTopoVLiquidadas">${beanRes.volumenSubTotalPag.topoVLiquidadas} </td>
							<td class="text_centro"    id="idMontoTopoVLiquidadas">${beanRes.saldoSubTotalPag.topoVLiquidadas}</td>
							<td class="text_centro"    id="idVolumenTopoTLiquidadas">${beanRes.volumenSubTotalPag.topoTLiquidadas}</td>
							<td class="text_centro"    id="idMontoTopoTLiquidadas">${beanRes.saldoSubTotalPag.topoTLiquidadas}</td>
							<td class="text_centro"    id="idVolumenTopoTXLiquidar">${beanRes.volumenSubTotalPag.topoTPorLiquidar}</td>
							<td class="text_centro"    id="idMontoTopoTXLiquidar">${beanRes.saldoSubTotalPag.topoTPorLiquidar}</td>
							<td class="text_centro"    id="idVolumenRechazos">${beanRes.volumenSubTotalPag.rechazos}</td>
							<td class="text_centro"    id="idMontoRechazos">${beanRes.saldoSubTotalPag.rechazos}</td>
						</tr>
						<tr class="odd2">
							<td class="text_izquierda">${txtTotales}</td>
							<td class="text_izquierda">${beanRes.volumenTotal.topoVLiquidadas}</td>
							<td class="text_centro">${beanRes.saldoTotal.topoVLiquidadas}</td>
							<td class="text_centro">${beanRes.volumenTotal.topoTLiquidadas}</td>
							<td class="text_centro">${beanRes.saldoTotal.topoTLiquidadas}</td>
							<td class="text_centro">${beanRes.volumenTotal.topoTPorLiquidar}</td>
							<td class="text_centro">${beanRes.saldoTotal.topoTPorLiquidar}</td>
							<td class="text_centro">${beanRes.volumenTotal.rechazos}</td>
							<td class="text_centro">${beanRes.saldoTotal.rechazos}</td>
						</tr>

				</tbody>
			</table>
			</div>
		</div>



        <c:if test="${not empty beanRes.listReceptoresSPID}">
			<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && beanRes.bloqueado}">
								<td class="izq"><a id="idEnvio" href="javascript:;">${txtEnvTransferencia}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${txtEnvTransferencia}</a></td>
							</c:otherwise>
						</c:choose>


						<td class="odd">${espacioEnBlanco}</td>


						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR') && beanRes.bloqueado}">
								<td class="der"><a id="idVerDetalle" href="javascript:;">${txtVerDetalle}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"><a href="javascript:;">${txtVerDetalle}</a></td>
							</c:otherwise>
						</c:choose>



					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && beanRes.bloqueado}" >
								<td class="izq"><a id="idRechazar" href="javascript:;">${txtRechazar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${txtRechazar}</a></td>
							</c:otherwise>
						</c:choose>



						<td class="odd">${espacioEnBlanco}</td>

						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && beanRes.bloqueado}">
								<td class="der"><a id="idActMotRechazo" href="javascript:;">${txtActualizarMotRechazo}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"><a href="javascript:;">${txtActualizarMotRechazo}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && !beanRes.bloqueado}">
								<td class="izq"><a id="idApartar" href="javascript:;">${txtApartar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${txtApartar}</a></td>
							</c:otherwise>
						</c:choose>



						<td class="odd">${espacioEnBlanco}</td>

						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') && beanRes.bloqueado}">
								<td class="der"><a id="idDesapartar" href="javascript:;">${txtDesapartar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"><a href="javascript:;">${txtDesapartar}</a></td>
							</c:otherwise>
						</c:choose>

					</tr>
				</table>

			</div>
		</div>
		</c:if>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>