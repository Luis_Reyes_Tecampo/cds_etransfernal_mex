<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMuestraMonitor" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloSPID.myMenu.text.envioRecepcionOperacion" var="tituloModulo"/>
<spring:message code="moduloSPID.recepcionOperacion.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.recepcionOperacion.text.refe" var="refe"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cola" var="cola"/>
<spring:message code="moduloSPID.recepcionOperacion.text.claveTran" var="claveTran"/>
<spring:message code="moduloSPID.recepcionOperacion.text.macan" var="macan"/>
<spring:message code="moduloSPID.recepcionOperacion.text.medioEnt" var="medioEnt"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fchCap" var="fchCap"/>
<spring:message code="moduloSPID.recepcionOperacion.text.ord" var="ord"/>
<spring:message code="moduloSPID.recepcionOperacion.text.rec" var="rec"/>
<spring:message code="moduloSPID.recepcionOperacion.text.importeAbono" var="importeAbono"/>
<spring:message code="moduloSPID.recepcionOperacion.text.est" var="est"/>
<spring:message code="moduloSPID.recepcionOperacion.text.folioPaquete" var="folioPaquete"/>
<spring:message code="moduloSPID.recepcionOperacion.text.folioPago" var="folioPago"/>
<spring:message code="moduloSPID.recepcionOperacion.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.recepcionOperacion.text.regresar" var="regresar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cveInstitucion" var="cveInstitucion"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',
					"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',
                    "OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}',"OK00002V":'${OK00002V}',"ED00065V":'${ED00065V}',
                    "CD00167V":'${CD00167V}',"ED00068V":'${ED00068V}',"CD00167V":'${CD00167V}'
                    };
	 
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/ordenReparacion.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResOrdenReparacion.beanPaginador.paginaIni}">
	<input type="hidden" name="pagina" id="pagina" value="${beanResOrdenReparacion.beanPaginador.pagina}">
	<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResOrdenReparacion.beanPaginador.paginaFin}">
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${sortField}">
	<input type="hidden" name="sortType" id="sortType" value="${sortType}">
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
					<tr>
						<th width="223" class="text_centro" scope="col"><input name="chkTodos" id="chkTodos" type="checkbox"/></th>
						<th width="223" data-id="referencia" class="text_centro sortField" scope="col">${refe}
							<c:if test="${sortField == 'referencia'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveCola" class="text_centro sortField" scope="col">${cola}
							<c:if test="${sortField == 'cveCola'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveTran" class="text_centro sortField" scope="col">${claveTran}
							<c:if test="${sortField == 'cveTran'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveMecanismo" class="text_centro sortField" scope="col">${macan}
							<c:if test="${sortField == 'cveMecanismo'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveMedioEnt" class="text_centro sortField" scope="col">${medioEnt}
							<c:if test="${sortField == 'cveMedioEnt'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="fechaCaptura" class="text_centro sortField" scope="col">${fchCap}
							<c:if test="${sortField == 'fechaCaptura'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveIntermeOrd" class="text_centro sortField" scope="col">${ord}
							<c:if test="${sortField == 'cveIntermeOrd'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="cveIntermeRec" class="text_centro sortField" scope="col">${rec}
							<c:if test="${sortField == 'cveIntermeRec'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="importeAbono" class="text_centro sortField" scope="col">${importeAbono}
							<c:if test="${sortField == 'importeAbono'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="estatus" class="text_centro sortField" scope="col">${est}
							<c:if test="${sortField == 'estatus'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="folioPaquete" class="text_centro sortField" scope="col">${folioPaquete}
							<c:if test="${sortField == 'folioPaquete'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="foloPago" class="text_centro sortField" scope="col">${folioPago}
							<c:if test="${sortField == 'foloPago'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th>
						<th width="223" data-id="mensajeError" class="text_centro sortField" scope="col">${mensajeError}
							<c:if test="${sortField == 'mensajeError'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th> 
					</tr>
					<tbody>				
						<c:forEach var="beanOrdenReparacion" items="${beanResOrdenReparacion.listaOrdenesReparacion}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td class="text_centro">
									<input name="listaOrdenesReparacion[${rowCounter.index}].seleccionado" value="true" type="checkbox"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.referencia" value="${beanOrdenReparacion.beanOrdenReparacionDos.referencia}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveCola" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveCola}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveTran" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveTran}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveMecanismo" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveMecanismo}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveMedioEnt" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveMedioEnt}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.fechaCaptura" value="${beanOrdenReparacion.beanOrdenReparacionDos.fechaCaptura}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveIntermeOrd" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeOrd}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveIntermeRec" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeRec}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.importeAbono" value="${beanOrdenReparacion.beanOrdenReparacionDos.importeAbono}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.estatus" value="${beanOrdenReparacion.beanOrdenReparacionDos.estatus}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.folioPaquete" value="${beanOrdenReparacion.beanOrdenReparacionDos.folioPaquete}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.foloPago" value="${beanOrdenReparacion.beanOrdenReparacionDos.foloPago}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].mensajeError" value="${beanOrdenReparacion.mensajeError}"/>									
								</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.referencia}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveCola}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveTran}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveMecanismo}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveMedioEnt}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.fechaCaptura}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeOrd}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeRec}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.importeAbono}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.estatus}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.folioPaquete}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.foloPago}</td>
								<td class="text_centro">${beanOrdenReparacion.mensajeError}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<c:if test="${not empty beanResOrdenReparacion.listaOrdenesReparacion}">
				<div class="paginador">
					<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResOrdenReparacion.totalReg} registros | Importe p�gina: ${importePagina}</label>
					</div>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaIni == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaIni != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaAnt!='0' && beanResOrdenReparacion.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaAnt=='0' || beanResOrdenReparacion.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${beanResOrdenReparacion.beanPaginador.pagina} - ${beanResOrdenReparacion.beanPaginador.paginaFin}</label>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin != beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','ordenesReparacion.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${beanResOrdenReparacion.beanPaginador.paginaFin == beanResOrdenReparacion.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
				<label>Importe Total: ${importeTotal}</label>
			</c:if>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a id="idEnviar" href="javascript:;">${enviar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;" >${enviar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class="odd"></td>
						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class="odd"></td>
							<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
							<td width="6" class="odd">&nbsp;</td>
							<td width="279" class="cero">&nbsp;</td>
						</tr> 
					</table>
				</div>
			</div>
		</div>
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>