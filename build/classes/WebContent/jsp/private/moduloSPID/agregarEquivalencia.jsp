<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntListarEquivalencias" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<!-- Titulo pagina -->
<spring:message code="moduloSPID.listadoEquivalencias.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<!-- Etiquetas para pagina -->


<!-- Mensajes Generales de la pantalla -->
<!-- Menasajes para la aplicacion -->
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.CD00041V" var="CD00041V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/agregarEquivalencia.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/confEquivalencia.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}',
                    "ED00065V":'${ED00065V}',
                    "CD00041V":'${CD00041V}',
                    "EC00011B":'${EC00011B}'
                    };
</script>


<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form:form name="idForm" id="idForm" method="post" commandName="beanEquivalencia">
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">
			<c:if test="${isEditar ne 'editar' }">Alta Equivalencia</c:if>
			<c:if test="${isEditar eq 'editar' }">Editar Equivalencia<td class="izq"></c:if>
		</div>
			<div class="contentBuscadorSimple">
			<input type="hidden" name="cveOperacionOld" value="${beanOldEquivalencia.claveOper}"/>
				<input type="hidden" name="tipoPagoOld" value="${beanOldEquivalencia.tipoPago}"/>
				<input type="hidden" name="clasificacionOld" value="${beanOldEquivalencia.clasificacion}"/>
				<c:if test="${'editar' eq isEditar}">
					<input type="hidden" id="cveOperacion" name="claveOper" value="${beanEquivalencia.claveOper}">
				</c:if>
				<table>
					<tr>
						<td class="text_izquierda">Tipo de Pago:</td>
						<td colspan="2">
						 	<form:select id="tipoPago" path="tipoPago" style="width: 140px;" itemValue="beanEquivalencia.tipoPago" >
    							<form:option value=""> --Selecciona--</form:option>
    							<form:options items="${beanResEquivalencia.listaTipoPago}" itemValue="claveTipoPago" itemLabel="tipoPago"></form:options>
  							</form:select>
						</td>
						
						
						<c:choose>
							<c:when test="${'editar' eq isEditar}">
								<td class="text_izquierda">Clave Operaci&oacute;n:</td>
								<td colspan="2">
									<form:select disabled="true" path="claveOper" style="width: 140px;" itemValue="">
   										<form:option value=""> --Selecciona--</form:option>
   										<form:options items="${beanResEquivalencia.listaClaveOperacion}" itemValue="claveOperacion"></form:options>
 										</form:select>
								</td>
							</c:when>
							<c:otherwise>
								<td class="text_izquierda">Clave Operaci&oacute;n:</td>
								<td colspan="2">
									<form:select id="cveOperacion" path="claveOper" style="width: 140px;" itemValue="">
   										<form:option value=""> --Selecciona--</form:option>
   										<form:options items="${beanResEquivalencia.listaClaveOperacion}" itemValue="claveOperacion"></form:options>
 										</form:select>
								</td>
							</c:otherwise>
						</c:choose>
						
					</tr>
					<tr class="text_izquierda">
						<td>Horario</td>
						<td colspan="2">
						<form:select id="horario" path="horario" style="width: 140px;" itemValue="beanEquivalencia.horario">
    							<form:option value=""> --Selecciona--</form:option>
    							<form:option value="D"> Diurno</form:option>
    							<form:option value="M"> Mixto</form:option>
    							<form:option value="N"> Noturno</form:option>
    					</form:select> 
						</td>
						
						<td>Clasificaci&oacute;n</td>
						<td colpan="2"><form:input id="clasificacion" path="clasificacion" maxlength="2" class="campos" type="text" name="clasificacion"/> </td>
					</tr>
					<tr class="text_izquierda">
						<td>Descripci&oacute;n</td>
						<td colspan="5">
							<input id="descripcionEq" class="descriptionText" value="${beanEquivalencia.descripcion}" maxlength="60" class="campos" type="text" name="descripcion"/>
						</td>						
					</tr>
				</table>
				
			</div>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table>
						<tr>
							<c:choose>
								<c:when test="${'editar' eq isEditar}">
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
											<td class="izq"><a href="javascript:;" id="idEditarG">Guardar</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">Guardar</a></td>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td class="izq"><a href="javascript:;" id="idGuardar">Guardar</a></td>
										</c:when>
										<c:otherwise>
											<td class="izq_Des"><a href="javascript:;">Guardar</a></td>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							<td class="der"><a href="javascript:;" id="idRegresar">Regresar</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form:form>
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
	</c:if>