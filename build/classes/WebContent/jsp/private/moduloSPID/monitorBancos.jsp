<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloSPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloSPID" />
	<jsp:param name="menuSubitem" value="IntMonitorBancos" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
       
<spring:message code="moduloSPID.myMenu.text.envioRecepcionOperacion" var="tituloModulo"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.monitorBancos.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="moduloSPID.monitorBancos.text.txtBanco"      var="txtBanco"/>
<spring:message code="moduloSPID.monitorBancos.text.txtInterme"      var="txtInterme"/>
<spring:message code="moduloSPID.monitorBancos.text.txtNombre"   var="txtNombre"/>
<spring:message code="moduloSPID.monitorBancos.text.txtConectado" var="txtConectado"/>
<spring:message code="moduloSPID.monitorBancos.text.txtReceptivo" var="txtReceptivo"/>
<spring:message code="moduloSPID.monitorBancos.text.txtConectados"      var="txtConectados"/>
<spring:message code="moduloSPID.monitorBancos.text.txtDesconectados"   var="txtDesconectados"/>
<spring:message code="moduloSPID.monitorBancos.text.txtBajas"   var="txtBajas"/>
<spring:message code="moduloSPID.monitorBancos.text.txtBloqueados" var="txtBloqueados"/>
<spring:message code="moduloSPID.monitorBancos.text.txtReceptivos" var="txtReceptivos"/>
<spring:message code="moduloSPID.monitorBancos.text.txtNoReceptivos" var="txtNoReceptivos"/>
<spring:message code="moduloSPID.monitorBancos.text.txtIntermeNoReg"   var="txtIntermeNoReg"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.consultaRecepciones.text.actualizar" var="actualizar"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>


<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                    "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',
                    "ED00064V":'${ED00064V}',
                    "ED00065V":'${ED00065V}',
                    "CD00167V":'${CD00167V}'
                    };

</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/monitorBancos.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloSPID/monitorBancos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${txtModuloSPID}</span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
		 
	<form name="idForm" id="idForm" method="post">
	    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResMonitorBancos.beanPaginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${beanResMonitorBancos.beanPaginador.pagina}"/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResMonitorBancos.beanPaginador.paginaFin}"/>
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" name="sortField" id="sortField" value="${sortField}">
		<input type="hidden" name="sortType" id="sortType" value="${sortType}">
		
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th width="223" data-id="cveBanco" class="text_centro sortField" scope="col">${txtBanco}
						<c:if test="${sortField == 'cveBanco'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="223" data-id="cveInterme" class="text_centro sortField" scope="col">${txtInterme}
						<c:if test="${sortField == 'cveInterme'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="223" data-id="nombreBanco" class="text_centro sortField" scope="col">${txtNombre}
						<c:if test="${sortField == 'nombreBanco'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="223" data-id="estadoInstit" class="text_centro sortField" scope="col">${txtConectado}
						<c:if test="${sortField == 'estadoInstit'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					<th width="223" data-id="estadoRecep" class="text_centro sortField" scope="col">${txtReceptivo}
						<c:if test="${sortField == 'estadoRecep'}">
							<c:choose>
								<c:when test="${sortType == 'DESC'}">
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
								</c:when>
								<c:otherwise>
									<img src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
								</c:otherwise>
							</c:choose>
						</c:if>
					</th>
					
				</tr>
				<tbody>				
					<c:forEach var="monitorBancos" items="${beanResMonitorBancos.listaMonitorBancos}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">${monitorBancos.cveBanco}</td>
							<c:choose>
							<c:when test="${monitorBancos.cveInterme == 'NR'}">
									<td>
									 <div class="margin-3 yellow">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								        sumaNoReg();
								      
									</script>
									
							</c:when>
							<c:otherwise>

								<td class="text_centro">${monitorBancos.cveInterme}</td>
							</c:otherwise>

						    </c:choose>						   
							<td class="text_centro">${monitorBancos.nombreBanco}</td>
							<c:choose>
								<c:when test="${monitorBancos.estadoInstit == 'C'}">
									<td>
									 <div class="margin-3 green">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								   	 sumaConectados();
								    </script>
								</c:when>
								<c:when test="${monitorBancos.estadoInstit == 'D'}">
									<td>
									 <div class="margin-3 red">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								      sumaDesonectados();
								      
									</script>
									
								</c:when>
								<c:when test="${monitorBancos.estadoInstit == 'L'}">
									<td>
									 <div class="margin-3 blue">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								      sumaBloqueados();
								      
									</script>
									
								</c:when>
								<c:when test="${monitorBancos.estadoInstit == 'B'}">
									<td>
									 <div class="margin-3 yellow">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								      sumaBajas();
								      
									</script>
									
								</c:when>
							
							</c:choose>
							
<%-- 							<td class="text_centro">${monitorBancos.estadoInstit}</td> --%>
							<c:choose>
								<c:when test="${monitorBancos.estadoRecep == 'R'}">
									<td>
									 <div class="margin-3 green">
										&nbsp;&nbsp;
										</div>
								    </td>
								    <script type="text/javascript">
								     sumaReceptivos();
								      
									</script>
									
								</c:when>
								<c:when test="${monitorBancos.estadoRecep == 'N'}">
									<td>
									 <div class="margin-3 red">
										&nbsp;&nbsp;
									</div>
								    </td>
								    <script type="text/javascript">
								      sumaNoReceptivos();
								      
									</script>
									
								</c:when>
							</c:choose>
<%-- 							<td class="text_centro">${monitorBancos.estadoRecep}</td> --%>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
			<c:if test="${not empty beanResMonitorBancos.listaMonitorBancos}">
			<div class="paginador">
			<div style="text-align: left; float:left; margin-left: 5px;">
						<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResMonitorBancos.totalReg} registros </label>
					</div>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaIni == beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaIni != beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','monitorBancos.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaAnt!='0' && beanResMonitorBancos.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','MuestramonitorBancos.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaAnt=='0' || beanResMonitorBancos.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResMonitorBancos.beanPaginador.pagina} - ${beanResMonitorBancos.beanPaginador.paginaFin}</label>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaFin != beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','monitorBancos.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaFin == beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaFin != beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','monitorBancos.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResMonitorBancos.beanPaginador.paginaFin == beanResMonitorBancos.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>

		</div>
		
		<div class="frameTablaVariasColumnas">
		<div class="contentTablaVariasColumnas">
		<table>
					<tr>
						<td>${txtConectados}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="conectados" name="conectados" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT green">&nbsp;</div>
							</div>
					    </td>

						
						<td>${txtReceptivos}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="receptivos" name="receptivos" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT green">&nbsp;</div>
							</div>
						</td>
						
						
					
					</tr>
					<tr>
						
						<td>${txtDesconectados}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="desconectados" name="desconectados" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT red">&nbsp;</div>
							</div>
						</td>
						
						<td>${txtNoReceptivos}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="noReceptivos" name="noReceptivos" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT red">&nbsp;</div>
							</div>
						</td>
						
							
					   	
						
					</tr>
						
					<tr>
						<td>${txtBloqueados}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="bloqueados" name="bloqueados" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT blue">&nbsp;</div>
							</div>
						</td>
						
						<td>${txtIntermeNoReg}</td>
						<td>
							<div>
								<input type="text" readonly="readonly" id="intermeNoReg" name="intermeNoReg" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT yellow">&nbsp;</div>
							</div>
						</td>
						
					</tr>
					
					<tr>
						<td>${txtBajas}</td>
						<td>
						
							<div>
								<input type="text" readonly="readonly" id="bajas" name="bajas" value="" class="inputT Campos" size="20" maxlength="12">
  								<div  class="divT yellow">&nbsp;</div>
							</div>
						</td>
						
					</tr>
				</table>
				</div>
				</div>
	
					<div class="framePieContenedor">
			<div class="contentPieContenedor">
				
				<table>
					<tr>
						<td class="odd">&nbsp;</td>
						<td class="odd">&nbsp;</td>
						<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
						</c:choose>
					
					</tr>
				
			 </table>

			</div>
		</div>
		
			<script type="text/javascript">
				document.getElementById('conectados').value = conectados;
				document.getElementById('desconectados').value = desconectados;
				document.getElementById('receptivos').value = receptivos;
				document.getElementById('noReceptivos').value = noReceptivos;
				document.getElementById('bloqueados').value = bloqueados;
				document.getElementById('bajas').value = bajas;
				document.getElementById('intermeNoReg').value = noReg;
				
								      
			</script>
		
		
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>