<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuAdmonSaldo.jsp" flush="true">
	<jsp:param name="menuItem" value="admonSaldo" />
	<jsp:param name="menuSubitem" value="muestraAdminSaldo" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
	
<spring:message code="moduloCDA.administraSaldos.text.tituloPT" var="titulo" />
<spring:message code="moduloCDA.administraSaldos.text.hora" var="hora" />
<spring:message code="moduloCDA.administraSaldos.text.texto" var="texto" />
<spring:message code="moduloCDA.administraSaldos.text.importe" var="importe" />
<spring:message code="moduloCDA.administraSaldos.text.agregar" var="agregar" />

<spring:message code="moduloCDA.administraSaldos.text.agregar" var="agregar" />
<spring:message code="moduloCDA.administraSaldos.text.hr8" var="hr8" />
<spring:message code="moduloCDA.administraSaldos.text.hr9" var="hr9" />
<spring:message code="moduloCDA.administraSaldos.text.hr10" var="hr10" />
<spring:message code="moduloCDA.administraSaldos.text.hr11" var="hr11" />
<spring:message code="moduloCDA.administraSaldos.text.hr12" var="hr12" />
<spring:message code="moduloCDA.administraSaldos.text.hr13" var="hr13" />
<spring:message code="moduloCDA.administraSaldos.text.hr14" var="hr14" />
<spring:message code="moduloCDA.administraSaldos.text.hr15" var="hr15" />
<spring:message code="moduloCDA.administraSaldos.text.hr16" var="hr16" />
<spring:message code="moduloCDA.administraSaldos.text.hr17" var="hr17" />
<spring:message code="moduloCDA.administraSaldos.text.hr18" var="hr18" />

<spring:message code="general.eliminar" var="eliminar" />
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.regresar" var="regresar"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="codError.CD00040V" var="CD00040V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.CD00041V" var="CD00041V"/>
<spring:message code="codError.CD00043V" var="CD00043V"/>
<spring:message code="codError.OK00003V" var="OK00003V"/>

	<c:set var="searchString" value="${seguTareas}"/>
	<script src="${pageContext.servletContext.contextPath}/js/private/programaTraspasos.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
	
	<script type="text/javascript">
      var mensajes = {"aplicacion": '${aplicacion}',"CD00040V":'${CD00040V}',"CD00041V":'${CD00041V}'
      				 ,"CD00043V":'${CD00043V}' ,"OK00003V":'${OK00003V}'};
    </script>
    

<c:set var="searchString" value="${seguTareas}"/>

	<%-- Componente titulo de p�gina --%>
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>

		<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="paginaIni" id="paginaIni" value="${datosHTR.paginador.paginaIni}"/>
			<input type="hidden" name="pagina" id="pagina" value="${datosHTR.paginador.pagina}"/>
			<input type="hidden" name="paginaFin" id="paginaFin" value="${datosHTR.paginador.paginaFin}"/>
			<input type="hidden" name="accion" id="accion" value=""/>
			<input type="hidden" name="regMedEntrega" value="${strUsuario}"/>
			<input type="hidden" name="regTransfer" value="${strUsuario}"/>
			<input type="hidden" name="regOperacion" value="${strUsuario}"/>
			<input type="hidden" name="importeTR" id="importeTR" value="${datosHTR.importeProgTran}"/>

	<%-- Componente buscador simple --%>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>
						<tr>
							<td class="text_derecha" >${hora}:</td>
							<td class="text_izquierda">
								<select name="selHora" class="Campos" id="selHora">
		          					<option value="">${na}</option> 
		          					<option value="${hr8}" ${hr8 == horarioSPEI.cveTopoPri ?   "selected" : ""}>${hr8}</option> 
		          					<option value="${hr9}" ${hr9 == horarioSPEI.cveTopoPri ?   "selected" : ""}>${hr9}</option> 
		          					<option value="${hr10}" ${hr10 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr10}</option> 
		          					<option value="${hr11}" ${hr11 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr11}</option>  
		          					<option value="${hr12}" ${hr12 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr12}</option> 
		          					<option value="${hr13}" ${hr13 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr13}</option> 
		          					<option value="${hr14}" ${hr14 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr14}</option> 
		          					<option value="${hr15}" ${hr15 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr15}</option>  
		          					<option value="${hr16}" ${hr16 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr16}</option>  
		          					<option value="${hr17}" ${hr17 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr17}</option>  
		          					<option value="${hr18}" ${hr18 == horarioSPEI.cveTopoPri ? "selected" : ""}>${hr18}</option>            				                        		
		             			</select>
		             				<input type="text" class="Campos" maxlength="50" size="50"  name="descripTR" id="descripTR" value="${texto}"/>
		             		</td>
						</tr>
						<tr>
							<td class="text_derecha">${importe}:</td>
							<td class="text_izquierda">${datosHTR.importeProgTran}</td>
						</tr>
				</tbody>
			</table>
		</div>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>	
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td width="279" class="izq"><a id="idAgregar" href="javascript:;">${agregar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${agregar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	


	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infoEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table >
				<tr>
					<th  width="132" class="text_centro" colspan=2 >${hora}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanHorariosTR" items="${datosHTR.listHorariosTr}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td width="100" class="text_centro">
							<input type="checkbox" name="listHorariosTr[${rowCounter.index}].seleccionado"/>
						</td>
						<input type="hidden" name="listHorariosTr[${rowCounter.index}].hora" value="${beanHorariosTR.hora}"/>
						<td width="250" class="text_izquierda">${beanHorariosTR.hora} ${beanHorariosTR.descripcion}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
			
		</div>
		<c:if test="${not empty datosHTR.listHorariosTr}">
				<div class="paginador">
					<c:if test="${datosHTR.paginador.paginaIni == datosHTR.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${datosHTR.paginador.paginaIni != datosHTR.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraProgTraspasos.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${datosHTR.paginador.paginaAnt!='0' && datosHTR.paginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','muestraProgTraspasos.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${datosHTR.paginador.paginaAnt=='0' || datosHTR.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${datosHTR.paginador.pagina} - ${datosHTR.paginador.paginaFin}</label>
					<c:if test="${datosHTR.paginador.paginaFin != datosHTR.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraProgTraspasos.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${datosHTR.paginador.paginaFin == datosHTR.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${datosHTR.paginador.paginaFin != datosHTR.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','muestraProgTraspasos.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${datosHTR.paginador.paginaFin == datosHTR.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
			</c:if>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
			
				<table>
					<tr>
							
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
								<td width="279" class="izq"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279"  class="izq_Des"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
					</tr>
				</table>
				
			</div>
		</div>
		
	</div>

		</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
	