<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<spring:message code="moduloCDA.recepcionOperacion.text.txtTopo"      var="txtTopo"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtTipo"      var="txtTipo"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtEstTrans"   var="txtEstTrans"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtEstBanxico"   var="txtEstBanxico"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtFchOperacion" var="txtFchOperacion"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtHora"      var="txtHora"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtIntsOrd"   var="txtIntsOrd"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtFoliosPaquete" var="txtFoliosPaquete"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtFoliosPago" var="txtFoliosPago"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtCuentaRec" var="txtCuentaRec"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtImporte"   var="txtImporte"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtCodErr"    var="txtCodErr"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtRefTrans"  var="txtRefTrans"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtDevol"     var="txtDevol"/>
<spring:message code="moduloSPEI.recepOperacion.text.apartado"     var="txtApartado"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtIntitu"   var="txtIntitu"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtCveRastreo"   var="txtCveRastreo"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtDesError"   var="txtDesError"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtTotales"   var="txtTotales"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtSumatoria"   var="txtSumatoria"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTodas"           var="txtTodas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoVLiquidadas" var="txtTopoVLiquidadas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoTLiquidadas" var="txtTopoTLiquidadas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoTXLiquidar"  var="txtTopoTXLiquidar"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTodas"           var="txtTodas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoVLiquidadas" var="txtTopoVLiquidadas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoTLiquidadas" var="txtTopoTLiquidadas"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtTopoTXLiquidar"  var="txtTopoTXLiquidar"/>
<spring:message code="moduloCDA.recepcionOperacion.link.rechazo"        var="txtRechazos"/>
<spring:message code="moduloCDA.recepcionOperacion.text.descTipoPago"        var="descTipoPago"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtDevoluciones"    var="txtDevoluciones"/>
<spring:message code="moduloCDA.recepcionOperacion.check.txtRechazoCMotDev"  var="txtRechazoCMotDev"/>
<spring:message code="moduloSPEI.myMenu.text.txtAplicadaTrans"      var="txtAplicadaTrans"/>
<spring:message code="moduloCDA.recepcionOperacion.text.estatusOpe"      var="estatusOpe"/>
<spring:message code="moduloCDA.recepcionOperacion.text.cveTransfer"      var="cveTransfer"/>
<spring:message code="moduloCDA.recepcionOperacion.text.desCveTransf"      var="desCveTransf"/>
<spring:message code="general.asteriscoHelper"      var="asterisco"/>

<spring:message code="general.RepOperacion.rechazar"        var="txtRechazos"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<c:set var="searchString" value="${seguTareas}"/>
		<div class="frameTablaVariasColumnas" style="height: 350px" id="tableRecepOperacion">
			<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden; height: 320px">
				<table style="width: 100%;">
					<caption> </caption>
					<thead style="display: block; table-layout: fixed; width: 2676px; height: 100%;">
					<tr>
						<th class="text_centro" scope="col" style="width: 24px"><input name="chkTodos" id="chkTodos" type="checkbox"  style=" width:20px"/> </th>
						 <th  class="text_centro" scope="col" style="width: 162px">${asterisco}${txtEstTrans}
						<br/>
		                	<select name="beanDetalle.detEstatus.estatusTransfer" style=" width:160px" class="text_centro " onchange="recepcionOperacion.esTransfer()"
		                	id="selectEstatusTrans"  >
								<c:forEach var="beanBusqueda" items="${beanResTranSpeiRec.beanBusquedaList}" varStatus="row">
									<option value="${beanBusqueda.secuencial}" ${beanBusqueda.secuencial eq beanTranSpeiRecOper.beanDetalle.detEstatus.estatusTransfer?"selected":""}>
										${beanBusqueda.descripcion}</option>
								</c:forEach>
							</select>
		                </th>
						<th  class="text_centro" scope="col" style="width: 130px">${txtRefTrans}
						<br/><input type="text" id="refeTransfer" name="beanDetalle.detEstatus.refeTransfer" class="filField" data-field="REFE_TRANSFER" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detEstatus.refeTransfer}"  style=" width:125px"></th>
						<th  class="text_centro" style="width: 108px">${txtIntsOrd}
						<br/><input type="text" id="cveInstOrd" name="beanDetalle.detalleGral.cveInstOrd" class="filField" data-field="CVE_INST_ORD" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveInstOrd}"  style=" width:80px"></th>
		                <th  class="text_centro" style="width: 106px">${txtIntitu}
						<br/><input type="text" id="cveMiInstitucion" name="beanDetalle.detalleGral.cveMiInstitucion" class="filField" data-field="CVE_MI_INSTITUC" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveMiInstitucion}"  style=" width:80px"></th>
		                <th  class="text_centro" style="width: 102px">${txtImporte}
						<br/><input type="text" id="monto" name="beanDetalle.ordenante.monto" class="filField" data-field="MONTO" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.ordenante.monto}" style=" width:90px"></th>
		                <th  class="text_centro" scope="col" style="width: 102px">${txtFchOperacion}
		                <c:if test="${beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperacion'||beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperaSPEI'}">
		                	<br/><input type="text" id="fechOper" name="beanDetalle.detalleGral.fchOperacion" class="filField" data-field="FCH_OPERACION" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.fchOperacion}" style=" width: 96px">
		                </c:if>
		                </th>
						<th  class="text_centro" scope="col" style="width: 37px">${txtHora}</th>
						<th  class="text_centro" style="width: 206px">${txtCuentaRec}
						<br/><input type="text" id="numCuentaRec" name="beanDetalle.receptor.numCuentaRec" class="filField" data-field="NUM_CUENTA_REC" maxlength="20"
		                    		value="${beanTranSpeiRecOper.beanDetalle.receptor.numCuentaRec}" style=" width:200px"></th>
		                <th  class="text_centro" colspan="2" style="width: 285px">${txtDevol}
		                <br/>
		                	<select name="beanDetalle.beanCambios.motivoDevol" style=" width:250px" class="text_centro" id="selectMotivoDev" onchange="recepcionOperacion.esTransfer()">
								<option value="TODAS" ${"TODAS" eq beanTranSpeiRecOper.beanDetalle.beanCambios.motivoDevol?"selected":""}>${seleccionarOpcion}</option>
								<c:forEach var="beanMotDet" items="${beanResTranSpeiRec.beanMotDevolucionList}" varStatus="row">
									<option value="${beanMotDet.secuencial}" ${beanMotDet.secuencial eq beanTranSpeiRecOper.beanDetalle.beanCambios.motivoDevol?"selected":""}>
										${beanMotDet.secuencial}-${beanMotDet.descripcion}</option>
								</c:forEach>
							</select>
		                </th>
		                <th  class="text_centro" style="width: 67px">${txtCodErr}
						<br/><input type="text" id="codigoError" name="beanDetalle.codigoError" class="filField" data-field="CODIGO_ERROR" maxlength="8"
		                    		value="${beanTranSpeiRecOper.beanDetalle.codigoError}" style=" width:60px"></th>
		                <th  class="text_centro" style="width: 87px">${txtDesError}</th>
		                <th  class="text_centro" scope="col" style="width: 226px">${txtCveRastreo}
						<br/><input type="text" id="cveRastreo" name="beanDetalle.detalleGral.cveRastreo" class="filField" data-field="CVE_RASTREO" maxlength="30"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveRastreo}" style=" width:220px"></th>
		                <th  class="text_centro" style="width: 127px">${txtApartado}
						<br/>
		                	<select name="usuario" style=" width:125px" class="text_centro" id="usuario" onchange="recepcionOperacion.esTransfer()">
								<option value="TODAS" ${"TODAS" eq beanTranSpeiRecOper.usuario?"selected":""}> </option>
								<c:forEach var="beanUsuario" items="${beanResTranSpeiRec.beanListUsuarios}" varStatus="row">
									<option value="${beanUsuario.secuencial}" ${beanUsuario.secuencial eq beanTranSpeiRecOper.usuario?"selected":""}>
									${beanUsuario.descripcion}</option>
								</c:forEach>
							</select>
		                </th>
		                <th  class="text_centro" style="width: 71px">${txtFoliosPaquete}
						<br/><input type="text" id="folioPaquete" name="beanDetalle.detalleGral.folioPaquete" class="filField" data-field="FOLIO_PAQUETE" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.folioPaquete}" style=" width:50px"></th>
						<th  class="text_centro" style="width: 56px">${txtFoliosPago}
						<br/><input type="text" id="folioPago" name="beanDetalle.detalleGral.folioPago" class="filField" data-field="FOLIO_PAGO" maxlength="12"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.folioPago}" style=" width:50px"></th>
		                <th  class="text_centro" scope="col" style="width: 68px">${txtTipo}
						<br/><input type="text" id="tipoPago" name="beanDetalle.detalleTopo.tipoPago" class="filField" data-field="TIPO_PAGO" maxlength="7"
		                    		width="5" value="${beanTranSpeiRecOper.beanDetalle.detalleTopo.tipoPago}" style=" width:50px"></th>
		                <th  class="text_centro" scope="col" style="width: 113px">${descTipoPago}
<!-- 						<br/> -->
<!-- 		                    <select name="beanDetalle.beanCambios.otros.descTipoPgo" style=" width:111px" class="text_centro" id="desTipoPgo" onchange="recepcionOperacion.esTransfer()"> -->
<%-- 								<option value="TODAS" ${"TODAS" eq beanTranSpeiRecOper.beanDetalle.beanCambios.otros.descTipoPgo?"selected":""}>${seleccionarOpcion}</option> --%>
<%-- 								<c:forEach var="beandescTipoPago" items="${beanResTranSpeiRec.beanListDescTipoPago}" varStatus="row"> --%>
<%-- 									<option value="${beandescTipoPago.descripcion}" ${beandescTipoPago.descripcion eq beanTranSpeiRecOper.beanDetalle.beanCambios.otros.descTipoPgo?"selected":""}> --%>
<%-- 									${beandescTipoPago.descripcion}</option> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
		                </th>
		                <th  class="text_centro" scope="col" style="width: 83px">${txtEstBanxico}
						<br/><input type="text" id="estatusBanxico" name="beanDetalle.detEstatus.estatusBanxico" class="filField" data-field="ESTATUS_BANXICO" maxlength="2"
		                    		value="${beanTranSpeiRecOper.beanDetalle.detEstatus.estatusBanxico}" style=" width:50px"></th>

		                <th  class="text_centro" scope="col" style="width: 94px">${estatusOpe}
<!-- 						<br/><input type="text" id="estatusOpe" name="beanDetalle.detalleGral.estatusOpe" class="filField" data-field="ESTATUS" maxlength="2"  -->
<%-- 		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.estatusOpe}" style=" width:50px"> --%>
		                </th>
		                <th  class="text_centro" scope="col" style="width: 74px">${cveTransfer}
<!-- 						<br/><input type="text" id="cveTransfer" name="beanDetalle.detalleGral.cveTransfer" class="filField" data-field="CVE_TRANSFE" maxlength="3"  -->
<%-- 		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.cveTransfer}" style=" width:68px"> --%>
		                 </th>
		                <th  class="text_centro" scope="col" style="width: 240px">${desCveTransf}
<!-- 						<br/><input type="text" id="desCveTransf" name="beanDetalle.detalleGral.desCveTransf" class="filField" data-field="DESCRIPCION" maxlength="60"  -->
<%-- 		                    		value="${beanTranSpeiRecOper.beanDetalle.detalleGral.desCveTransf}" style=" width:230px"> --%>
		                </th>

					</tr>
					</thead>
					<tbody style="display: block; height: 270px; overflow-y: scroll; margin-top: 2px;">
						<c:forEach var="beanTranSpeiRec" items="${beanResTranSpeiRec.listBeanTranSpeiRec}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"} seleccionaReg" style="height: 40px;" id="lineaRegistro${rowCounter.index}">
								<td class="text_centro"  bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 26px">
									<input name="listBeanTranSpeiRec[${rowCounter.index}].seleccionado" value="true"  type="checkbox"
										onchange="verificarSeleccion(${rowCounter.index})" id="seleccionado${rowCounter.index}"/>
						            <input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].agrupacion" value="${beanTranSpeiRec.agrupacion}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].numCuentaTran" value="${beanTranSpeiRec.numCuentaTran}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].usuario" id="usuario${rowCounter.index}" value="${beanTranSpeiRec.usuario}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].motivoDevolAnt" value="${beanTranSpeiRec.beanDetalle.beanCambios.motivoDevol}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].selecDevolucion" id="selecDevolucion${rowCounter.index}" value="${beanTranSpeiRec.selecDevolucion}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].selecRecuperar" id="selecRecuperar${rowCounter.index}" value="${beanTranSpeiRec.selecRecuperar}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.codigoError" value="${beanTranSpeiRec.beanDetalle.codigoError}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.fchOperacion" value="${beanTranSpeiRec.beanDetalle.detalleGral.fchOperacion}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveMiInstitucion" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveMiInstitucion}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveInstOrd" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveInstOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.folioPaquete" id="folioPqte${rowCounter.index}" value="${beanTranSpeiRec.beanDetalle.detalleGral.folioPaquete}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.folioPago" id="folioPgo${rowCounter.index}" value="${beanTranSpeiRec.beanDetalle.detalleGral.folioPago}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveRastreo" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveRastreo}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.refeNumerica" value="${beanTranSpeiRec.beanDetalle.detalleGral.refeNumerica}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveRastreoOri" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveRastreoOri}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.estatusOpe" id="estatusOpe" value="${beanTranSpeiRec.beanDetalle.detalleGral.estatusOpe}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveTransfer" id="cveTransfer" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveTransfer}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.desCveTransf" id="desCveTransf" value="${beanTranSpeiRec.beanDetalle.detalleGral.desCveTransf}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.folioPaqueteDev" id="folioPaqueteDev" value="${beanTranSpeiRec.beanDetalle.detalleGral.folioPaqueteDev}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.folioPagoDev" id="folioPagoDev" value="${beanTranSpeiRec.beanDetalle.detalleGral.folioPagoDev}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.cveOperacion" id="cveOperacion" value="${beanTranSpeiRec.beanDetalle.detalleGral.cveOperacion}"/>
                                    <input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleGral.fchCaptura" id="fchCaptura" value="${beanTranSpeiRec.beanDetalle.detalleGral.fchCaptura}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.cde" value="${beanTranSpeiRec.beanDetalle.detalleTopo.cde}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.prioridad" value="${beanTranSpeiRec.beanDetalle.detalleTopo.prioridad}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.tipoOperacion" value="${beanTranSpeiRec.beanDetalle.detalleTopo.tipoOperacion}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.topologia" value="${beanTranSpeiRec.beanDetalle.detalleTopo.topologia}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.tipoPago" value="${beanTranSpeiRec.beanDetalle.detalleTopo.tipoPago}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.cda" id="cda" value="${beanTranSpeiRec.beanDetalle.detalleTopo.cda}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.descripcionPrioridad" id="descripcionPrioridad" value="${beanTranSpeiRec.beanDetalle.detalleTopo.descripcionPrioridad}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detalleTopo.descTipoOperacion" id="descTipoOperacion" value="${beanTranSpeiRec.beanDetalle.detalleTopo.descTipoOperacion}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.conceptoPago2" value="${beanTranSpeiRec.beanDetalle.detEstatus.conceptoPago2}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.conceptoPago1" value="${beanTranSpeiRec.beanDetalle.detEstatus.conceptoPago1}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.refeTransfer" value="${beanTranSpeiRec.beanDetalle.detEstatus.refeTransfer}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.cvePago" value="${beanTranSpeiRec.beanDetalle.detEstatus.cvePago}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.estatusBanxico" value="${beanTranSpeiRec.beanDetalle.detEstatus.estatusBanxico}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.estatusTransfer" value="${beanTranSpeiRec.beanDetalle.detEstatus.estatusTransfer}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.refeCobranza" id="refeCobranza" value="${beanTranSpeiRec.beanDetalle.detEstatus.refeCobranza}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.descripcionBanxico" id="descripcionBanxico" value="${beanTranSpeiRec.beanDetalle.detEstatus.descripcionBanxico}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.detEstatus.descripcionTrasnfer" id="descripcionTrasnfer" value="${beanTranSpeiRec.beanDetalle.detEstatus.descripcionTrasnfer}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.fchCaptura" value="${beanTranSpeiRec.beanDetalle.beanCambios.fchCaptura}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.refeNumerica" id="refeNumerica" value="${beanTranSpeiRec.beanDetalle.beanCambios.refeNumerica}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.version" id="version" value="${beanTranSpeiRec.beanDetalle.beanCambios.version}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.longRastreo" id="longRastreo" value="${beanTranSpeiRec.beanDetalle.beanCambios.longRastreo}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.motivoDevolDesc" id="motivoDevolDesc" value="${beanTranSpeiRec.beanDetalle.beanCambios.motivoDevolDesc}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.aplicativo" id="aplicativo" value="${beanTranSpeiRec.beanDetalle.beanCambios.aplicativo}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.bucCliente" id="" value="${beanTranSpeiRec.beanDetalle.beanCambios.bucCliente}"/>

											<!-- Se agregan camos para persistencia cuando se navega al detalle -->
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.ctaReceptoraOri" id="" value="${beanTranSpeiRec.beanDetalle.beanCambios.ctaReceptoraOri}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.ctaReceptoraInt" id="" value="${beanTranSpeiRec.beanDetalle.beanCambios.ctaReceptoraInt}"/>


									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.descTipoPgo" id="descTipoPgo" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.descTipoPgo}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.swift" id="swift" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.swift}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.desError" id="desError" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.desError}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.swiftUno" id="swiftUno" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.swiftUno}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.swiftDos" id="swiftDos" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.swiftDos}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.fchOperOri" id="fchOperOri" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.fchOperOri}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.indBenefRec" id="indBenefRec" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.indBenefRec}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.montoPgoOri" id="montoPgoOri" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.montoPgoOri}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.refeAdi" id="refeAdi" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.refeAdi}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.otros.numCuentClv" id="" value="${beanTranSpeiRec.beanDetalle.beanCambios.otros.numCuentClv}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.tipoCuentaOrd" value="${beanTranSpeiRec.beanDetalle.ordenante.tipoCuentaOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.nombreOrd" value="${beanTranSpeiRec.beanDetalle.ordenante.nombreOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.numCuentaOrd" value="${beanTranSpeiRec.beanDetalle.ordenante.numCuentaOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.cveIntermeOrd" value="${beanTranSpeiRec.beanDetalle.ordenante.cveIntermeOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.monto" value="${beanTranSpeiRec.beanDetalle.ordenante.monto}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.rfcOrd" id="rfcOrd" value="${beanTranSpeiRec.beanDetalle.ordenante.rfcOrd}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.iva" id="iva" value="${beanTranSpeiRec.beanDetalle.ordenante.iva}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.ordenante.importeCargo" id="importeCargo" value="${beanTranSpeiRec.beanDetalle.ordenante.importeCargo}"/>

									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.tipoCuentaRec" value="${beanTranSpeiRec.beanDetalle.receptor.tipoCuentaRec}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.nombreRec" value="${beanTranSpeiRec.beanDetalle.receptor.nombreRec}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.numCuentaRec" value="${beanTranSpeiRec.beanDetalle.receptor.numCuentaRec}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.numCuentaRec2" value="${beanTranSpeiRec.beanDetalle.receptor.numCuentaRec2}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.nombreRec2" value="${beanTranSpeiRec.beanDetalle.receptor.nombreRec2}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.rfcRec2" value="${beanTranSpeiRec.beanDetalle.receptor.rfcRec2}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.tipoCuentaRec2" value="${beanTranSpeiRec.beanDetalle.receptor.tipoCuentaRec2}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.rfcRec" id="rfcRec" value="${beanTranSpeiRec.beanDetalle.receptor.rfcRec}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.importeDls" id="importeDls" value="${beanTranSpeiRec.beanDetalle.receptor.importeDls}"/>
									<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.receptor.importeAbono" id="importeAbono" value="${beanTranSpeiRec.beanDetalle.receptor.importeAbono}"/>

								</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 163px">${beanTranSpeiRec.beanDetalle.detEstatus.estatusTransfer}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 132px">${beanTranSpeiRec.beanDetalle.detEstatus.refeTransfer}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 110px">${beanTranSpeiRec.beanDetalle.detalleGral.cveInstOrd}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 108px">${beanTranSpeiRec.beanDetalle.detalleGral.cveMiInstitucion}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 103px">${beanTranSpeiRec.beanDetalle.ordenante.monto}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 104px">${beanTranSpeiRec.beanDetalle.detalleGral.fchOperacion}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 39px">${beanTranSpeiRec.beanDetalle.beanCambios.fchCaptura}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 208px">${beanTranSpeiRec.beanDetalle.receptor.numCuentaRec}</td>
								<c:choose>
									<c:when test="${(sessionBean.usuario eq beanTranSpeiRec.usuario || sessionBean.perfil eq 'grp_appnal_admin') && beanTranSpeiRec.usuario ne ''}">
										<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style=" width:220px">
											<select style=" width:200px" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.motivoDevol" style=" width:125px" class="Campos"
											id="selectMotivo${rowCounter.index}"  >
												<option value="">${seleccionarOpcion}</option>
												<c:forEach var="beanMotDe" items="${beanResTranSpeiRec.beanMotDevolucionList}" varStatus="row">
												<option value="${beanMotDe.secuencial}" ${beanMotDe.secuencial eq beanTranSpeiRec.beanDetalle.beanCambios.motivoDevol?"selected":""}>
													${beanMotDe.secuencial}-${beanMotDe.descripcion}</option>
												</c:forEach>
											</select>
										</td>
										<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style=" width: 66px">
											<c:choose>
												<c:when test="${('RE' ne beanTranSpeiRec.beanDetalle.detEstatus.estatusTransfer) }">
													<input type="button" id="idRec" value="Rec" onclick="recepcionOperacion.recuparar(${rowCounter.index})"
													onKeyPress="recepcionOperacion.recuparar(${rowCounter.index})" style=" width:30px" ${sessionBean.usuario eq beanTranSpeiRec.usuario ?"":"disabled"} />
												</c:when>
												<c:otherwise>
													<%--Si son pantalla apartadas  del dia se podra realizar una devolucion --%>
													<c:if test="${(beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperacion'||beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperaSPEI')
														&& ('RE' eq beanTranSpeiRec.beanDetalle.detEstatus.estatusTransfer && '0' ne beanTranSpeiRec.beanDetalle.detalleTopo.tipoPago)}">
															<input type="button" id="idDevolver" value="Dev" onclick="recepcionOperacionApartadas.devolver('${rowCounter.index}')"
																	onKeyPress="recepcionOperacion.devolver('${rowCounter.index}')" ${sessionBean.usuario eq beanTranSpeiRec.usuario ?"":"disabled"} />
													</c:if>
													<%-- Si son pantallas apartadas historicas se hara un devolucion extemporanea--%>
													<c:if test="${(beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperacionHto'||beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperaHistoricaSPEI')
														&& ('RE' eq beanTranSpeiRec.beanDetalle.detEstatus.estatusTransfer && ('16' ne beanTranSpeiRec.beanDetalle.detalleTopo.tipoPago  || '0' ne beanTranSpeiRec.beanDetalle.detalleTopo.tipoPago)) }">
															<input type="button" id="idDevolvera" value="DevExtem" onclick="recepcionOperacionHto.devolverHTO('${rowCounter.index}')"
																	onKeyPress= "recepcionOperacionHto.devolver('${rowCounter.index}')" ${ beanTranSpeiRec.beanDetalle.detalleGral.folioPaquete.substring(0,1) eq '-' || beanTranSpeiRec.beanDetalle.detalleGral.folioPago.substring(0,1) eq '-' ?"disabled":""}
																	${sessionBean.usuario eq beanTranSpeiRec.usuario ?"":"disabled"} />
													</c:if>
												</c:otherwise>
											</c:choose>
										</td>
					                </c:when>
									<c:otherwise>
										<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style=" width:220px">
											<input type="hidden" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.motivoDevol" id="motivoDevol" value="${beanTranSpeiRec.beanDetalle.beanCambios.motivoDevol}"/>
											<select style=" width:200px" name="listBeanTranSpeiRec[${rowCounter.index}].beanDetalle.beanCambios.motivoDevol" style=" width:125px" class="Campos"
											id="selectMotivoInmov" disabled="disabled" >
												<option value="">${seleccionarOpcion}</option>
												<c:forEach var="beanMotDev" items="${beanResTranSpeiRec.beanMotDevolucionList}" varStatus="row">
												<option value="${beanMotDev.secuencial}" ${beanMotDev.secuencial eq beanTranSpeiRec.beanDetalle.beanCambios.motivoDevol?"selected":""}> ${beanMotDev.secuencial}-${beanMotDev.descripcion}</option>

												</c:forEach>
											</select>
										</td>
										<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 66px"> </td>
									</c:otherwise>
								</c:choose>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 69px">${beanTranSpeiRec.beanDetalle.codigoError}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 89px">${beanTranSpeiRec.beanDetalle.beanCambios.otros.desError}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 228px">${beanTranSpeiRec.beanDetalle.detalleGral.cveRastreo}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 129px">${beanTranSpeiRec.usuario}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 73px">${beanTranSpeiRec.beanDetalle.detalleGral.folioPaquete}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 58px">${beanTranSpeiRec.beanDetalle.detalleGral.folioPago}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 70px">${beanTranSpeiRec.beanDetalle.detalleTopo.tipoPago}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 115px">${beanTranSpeiRec.beanDetalle.beanCambios.otros.descTipoPgo}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 85px">${beanTranSpeiRec.beanDetalle.detEstatus.estatusBanxico}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 96px">${beanTranSpeiRec.beanDetalle.detalleGral.estatusOpe}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 76px">${beanTranSpeiRec.beanDetalle.detalleGral.cveTransfer}</td>
								<td class="text_centro" bgcolor="${beanTranSpeiRec.usuario ne '' && sessionBean.usuario ne beanTranSpeiRec.usuario ?"#dddddd":""}" style="width: 242px">${beanTranSpeiRec.beanDetalle.detalleGral.desCveTransf}</td>
								</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<c:if test="${not empty beanResTranSpeiRec.listBeanTranSpeiRec}">
				<div class="paginador">
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaIni == beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;">&lt;&lt;${inicio}</a>
					</c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaIni != beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','consTranSpeiRec.do','INI');"
						onKeyPress="nextPage('idForm','consTranSpeiRec.do','INI');">&lt;&lt;${inicio}</a>
					</c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaAnt!='0' && beanResTranSpeiRec.beanPaginador.paginaAnt!=null}">
						<a href="javascript:;" onclick="nextPage('idForm','consTranSpeiRec.do','ANT');"
						onKeyPress="nextPage('idForm','consTranSpeiRec.do','ANT');">&lt;${anterior}</a>
					</c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaAnt=='0' || beanResTranSpeiRec.beanPaginador.paginaAnt==null}">
						<a href="javascript:;">&lt;${anterior}</a>
					</c:if>
					<label id="txtPagina">${beanResTranSpeiRec.beanPaginador.pagina} - ${beanResTranSpeiRec.beanPaginador.paginaFin}</label>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaFin != beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','consTranSpeiRec.do','SIG');"
						onKeyPress="nextPage('idForm','consTranSpeiRec.do','SIG');">${siguiente}&gt;</a>
					</c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaFin == beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaFin != beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;" onclick="nextPage('idForm','consTranSpeiRec.do','FIN');"
						onKeyPress="nextPage('idForm','consTranSpeiRec.do','FIN');">${fin}&gt;&gt;</a>
					</c:if>
					<c:if test="${beanResTranSpeiRec.beanPaginador.paginaFin == beanResTranSpeiRec.beanPaginador.pagina}">
						<a href="javascript:;">${fin}&gt;&gt;</a>
					</c:if>
				</div>
			</c:if>
		</div>
