<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="moduloCDA.recepcionOperacion.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="moduloCDA.recepcionOperacion.link.rechazar"           var="txtRechazar"/>
<spring:message code="general.RepOperacion.apartadas"      var="txtapartadas"/>
<spring:message code="general.RepOperacion.autorizar"      var="txtApTransfer"/>


<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00063V" var="ED00063V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>
<spring:message code="codError.CD00169V" var="CD00169V"/>
<spring:message code="codError.CD00172V" var="CD00172V"/>

<spring:message code="codError.ED00067V" var="ED00067V"/>
<spring:message code="codError.ED00069V" var="ED00069V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.ED00128V" var="ED00128V"/>
<spring:message code="codError.ED00135V" var="ED00135V"/>
<spring:message code="codError.ETRAMA001" var="ETRAMA001"/>
<spring:message code="codError.repOperacion.CD00001V" var="CD00001V"/>
<spring:message code="codError.repOperacion.CD00002V" var="CD00002V"/>
<spring:message code="codError.CD00003V" var="CD00003V"/>
<spring:message code="codError.CD00004V" var="CD00004V"/>
<spring:message code="codError.OK00021V" var="OK00021V"/>
<spring:message code="codError.OK00022V" var="OK00022V"/>
<spring:message code="codError.OK00023V" var="OK00023V"/>
<spring:message code="codError.CMCO022V" var="CMCO022V"/>
<spring:message code="codError.ED00030V" var="ED00030V"/>
<spring:message code="codError.ED00131V" var="ED00131V"/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script type="text/javascript">
		 var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',
                        "ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',
                        "OK00013V":'${OK00013V}',"ED00128V":'${ED00128V}',"CD00004V":'${CD00004V}',
                        "ED00063V":'${ED00063V}',"ED00064V":'${ED00064V}',"ED00065V":'${ED00065V}',
                        "CD00167V":'${CD00167V}',"CD00168V":'${CD00168V}',"CD00169V":'${CD00169V}',
                        "CD00172V":'${CD00172V}',"CD00003V":'${CD00003V}',"OK00021V":'${OK00021V}',
                        "ED00067V":'${ED00067V}',"ED00069V":'${ED00069V}',"ED00068V":'${ED00068V}',
                        "ED00029V":'${ED00029V}',"OK00000V":'${OK00000V}',"ETRAMA001":'${ETRAMA001}',
                        "CD00001V":'${CD00001V}',"CD00002V":'${CD00002V}',"ED00135V":'${ED00135V}',   
                        "OK00022V":'${OK00022V}',"OK00023V":'${OK00023V}',"CMCO022V":'${CMCO022V}',
                        "ED00030V":'${ED00030V}',"ED00131V":'${ED00131V}'
                        };
    </script>
<c:set var="searchString" value="${seguTareas}"/>
	<form name="idForm" id="idForm" method="post">
		<input type="hidden" name="paginador.accion" id="accion" value=""/>
	    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResTranSpeiRec.beanPaginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResTranSpeiRec.beanPaginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResTranSpeiRec.beanPaginador.paginaFin}"/>
		<input type="hidden" name="beanComun.opcion" id="opcion" value="${beanResTranSpeiRec.beanComun.opcion}"/>
		<input type="hidden" name="beanComun.nombrePantalla" id="nombrePantalla" value="${beanResTranSpeiRec.beanComun.nombrePantalla}"/>
		<input type="hidden" name="nomPantalla" id="nomPantalla" value=""/>
		<input type="hidden" id="idFechaOperacion" name="beanComun.fechaOperacion" value="${beanResTranSpeiRec.beanComun.fechaOperacion}"/> 	
		<input type="hidden" id="cveMiInstitucion" name="beanComun.cveMiInstitucion" value="${beanResTranSpeiRec.beanComun.cveMiInstitucion}"/>
		<input type="hidden" id="fechaActual" name="beanComun.fechaActual" value="${beanResTranSpeiRec.beanComun.fechaActual}"/>
		<input type="hidden" id="eliminarTodo" name="beanComun.eliminarTodo" value="${beanResTranSpeiRec.beanComun.eliminarTodo}"/>
		<input type="hidden" id="sessionUsu" name="sessionUsu" value="${sessionBean.usuario}"/>
		<input type="hidden" id="perfil" name="perfil" value="${sessionBean.perfil}"/>
		
		
		<c:forEach items="${beanResTranSpeiRec.beanBusquedaList}" var="estatus" varStatus="rowCounter">
			<input type="hidden" name="beanBusquedaList[${rowCounter.index}].secuencial" value="${estatus.secuencial}"/>
			<input type="hidden" name="beanBusquedaList[${rowCounter.index}].descripcion" value="${estatus.descripcion}"/>
		</c:forEach>
		
		<c:forEach items="${beanResTranSpeiRec.beanListUsuarios}" var="estatus" varStatus="rowCounter">
			<input type="hidden" name="beanListUsuarios[${rowCounter.index}].secuencial" value="${estatus.secuencial}"/>
			<input type="hidden" name="beanListUsuarios[${rowCounter.index}].descripcion" value="${estatus.descripcion}"/>
		</c:forEach>
		
		<c:forEach items="${beanResTranSpeiRec.beanMotDevolucionList}" var="estatus" varStatus="rowCounter">
			<input type="hidden" name="beanMotDevolucionList[${rowCounter.index}].secuencial" value="${estatus.secuencial}"/>
			<input type="hidden" name="beanMotDevolucionList[${rowCounter.index}].descripcion" value="${estatus.descripcion}"/>
		</c:forEach>
		
		<c:forEach items="${beanResTranSpeiRec.beanListDescTipoPago}" var="estatus" varStatus="rowCounter">
			<input type="hidden" name="beanListDescTipoPago[${rowCounter.index}].secuencial" value="${estatus.secuencial}"/>
			<input type="hidden" name="beanListDescTipoPago[${rowCounter.index}].descripcion" value="${estatus.descripcion}"/>
		</c:forEach>
		
<%-- 		<input type="hidden" id="beanListUsuarios" name="beanListUsuarios" value="${beanResTranSpeiRec.beanListUsuarios}"/> --%>
<%-- 		<input type="hidden" id="beanBusquedaList" name="beanBusquedaList" value="${beanResTranSpeiRec.beanBusquedaList}"/> --%>
<%-- 		<input type="hidden" id="beanMotDevolucionList" name="beanMotDevolucionList" value="${beanResTranSpeiRec.beanMotDevolucionList}"/> --%>
		
		<c:if test="${beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperacionHto' || beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperaHistoricaSPEI'}">
			<div class="frameBuscadorSimple">
				<div class="titleBuscadorSimple">${subtituloFuncion}</div>  
				<div class="contentBuscadorSimple">
					<table>
						<caption> </caption>
						<%-- opcion para monstrar la busqueda por fecha --%>
						<jsp:include page="repcepcionFechaOp.jsp" flush="true" />
					</table>
				</div>
			</div>
		</c:if>
		<jsp:include page="repOpTablaContenido.jsp" flush="true" />
		
		<jsp:include page="repOpcionBotonesPrincipal.jsp" flush="true" />
				
							
	
	</form>
	
<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>