<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.recepcionOperacion.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="general.RepOperacion.apartadas"      var="txtapartadas"/>
<c:set var="searchString" value="${seguTareas}"/>
	<td>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
				<span><a id="idBuscar" href="javascript:;">${txtBuscar}</a></span>
				
			</c:when>
			<c:otherwise>
				<span class="btn_Des"><a href="javascript:;">${txtBuscar}</a></span>
							
			</c:otherwise>
		</c:choose>	
	</td>
	
	
	