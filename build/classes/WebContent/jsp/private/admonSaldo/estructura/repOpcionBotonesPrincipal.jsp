<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.RepOperacion.apartar"      var="txtapartar"/>
<spring:message code="general.RepOperacion.exportar"      var="txtexportar"/>
<spring:message code="general.RepOperacion.exportarAll"      var="txtexportarAll"/>
<spring:message code="general.RepOperacion.verdetalle"   var="verDetalle"/>
<spring:message code="general.RepOperacion.autorizar"   var="enviaTransf"/>
<spring:message code="general.RepOperacion.autorizar"      var="txtApTransfer"/>

<spring:message code="general.RepOperacion.actMotivo"           var="txtActMotRechazo"/>
<spring:message code="general.RepOperacion.rechazar"        var="txtRechazos"/>
<spring:message code="general.RepOperacion.verdetalle"   var="verDetalle"/>
<spring:message code="general.RepOperacion.autorizar"      var="txtApTransfer"/>
<spring:message code="general.RepOperacion.liberarTodo"      var="txtliberarTodo"/>
<spring:message code="general.RepOperacion.liberar"      var="txtliberar"/>
<spring:message code="general.RepOperacion.regresar"      var="txtregresar"/>
<spring:message code="general.RepOperacion.actMotivo"           var="txtActMotRechazo"/>
<spring:message code="moduloCDA.recepcionOperacion.link.rechazar"           var="txtRechazar"/>
<spring:message code="general.RepOperacion.autorizar"   var="enviaTransf"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtActualizar"  var="txtActualizar"/>
<spring:message code="moduloCDA.recepcionOperacion.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtEnviarHto"           var="txtEnviarHtor"/>
<spring:message code="general.RepOperacion.apartadas"      var="txtapartadas"/>

<c:set var="searchString" value="${seguTareas}"/>
		<!-- Componente tabla estandar -->
		<div class="frameTablaVariasColumnas"> 	
			 <c:choose>
				<c:when test="${not empty beanResTranSpeiRec.listBeanTranSpeiRec}">
					<div class="framePieContenedor">
						<div class="contentPieContenedor">
							<table>
								<caption> </caption>
								<tr>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td width="279" class="izq"><a id="idApartar" href="javascript:;" ref="SALDOS" >${txtapartar}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="izq_Des"><a href="javascript:;">${txtapartar}</a></td>
										</c:otherwise>
									</c:choose>								
									<td width="279" class="odd">${espacioEnBlanco}</td>								
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
											<td width="279" class="der"><a id="idExportarTodo" href="javascript:;" ref="SALDOS">${txtexportarAll}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="der_Des"><a href="javascript:;">${txtexportarAll}</a></td>
										</c:otherwise>
									</c:choose>								
								</tr>							
								<tr>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
											<td width="279" class="izq"><a id="idLiberar" href="javascript:;"  ref="NO">${txtliberar}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="izq_Des"><a href="javascript:;">${txtliberar}</a></td>
										</c:otherwise>
									</c:choose>								
									<td width="279" class="odd">${espacioEnBlanco}</td>								
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td width="279" class="der"><a id="idActMotRechazo" href="javascript:;" ref="SALDOS">${txtActMotRechazo}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="der_Des"><a href="javascript:;">${txtActMotRechazo}</a></td>
										</c:otherwise>
									</c:choose>								
								</tr>
								<tr>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
											<td width="279" class="izq"><a id="idLiberarTodo" href="javascript:;" ref="TODO" >${txtliberarTodo}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="izq_Des"><a href="javascript:;">${txtliberarTodo}</a></td>
										</c:otherwise>
									</c:choose>								
									<td width="279" class="odd">${espacioEnBlanco}</td>								
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
											<td width="279" class="der"><a id="idVerDetalle" href="javascript:;">${verDetalle}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="der_Des"><a href="javascript:;">${verDetalle}</a></td>
										</c:otherwise>
									</c:choose>						
								</tr>
								<tr>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
											<td width="279" class="izq"><a id="idEnvio" href="javascript:;" >${enviaTransf}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="izq_Des"><a href="javascript:;">${enviaTransf}</a></td>
										</c:otherwise>
									</c:choose>
									<td width="279" class="odd">${espacioEnBlanco}</td>
									<td width="279" class="der"><a id="idActualizar" href="javascript:;">${txtActualizar}</a></td>
								</tr>
								<tr>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
											<td width="279" class="izq"><a id="idExportar" href="javascript:;" ref="SALDOS">${txtexportar}</a></td>
										</c:when>
										<c:otherwise>
											<td width="279" class="izq_Des"><a href="javascript:;">${txtexportar}</a></td>
										</c:otherwise>
									</c:choose> 								
									<td width="279" class="odd">${espacioEnBlanco}</td>
									<c:if test="${beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperacion' || beanResTranSpeiRec.beanComun.nombrePantalla eq 'recepcionOperaSPEI'}">
										<c:choose>
											<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
												<td width="279" class="der"><a id="idEnviarHto" href="javascript:;" >${txtEnviarHtor}</a></td>
											</c:when>
											<c:otherwise>
												<td width="279" class="der_Des"><a href="javascript:;">${txtEnviarHtor}</a></td>
											</c:otherwise>
										</c:choose> 	
									</c:if>
								</tr>
								
							</table>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="framePieContenedor">
						<div class="contentPieContenedor">
							<table>
								<caption> </caption>
								<tr>
									<td width="279" class="cero">${espacioEnBlanco}</td>						
									<td width="279" class="odd">${espacioEnBlanco}</td>
									<td width="279" class="der"><a id="idActualizar" href="javascript:;">${txtActualizar}</a></td>
								</tr>
							</table>
						</div>
					</div>
				</c:otherwise>
			</c:choose> 	
		</div>