<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.recepcionOperacion.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="general.RepOperacion.apartadas"      var="txtapartadas"/>
<spring:message code="moduloCDA.recepcionOperacion.text.txtFchOperacion" var="txtFchOperacion"/>
<c:set var="searchString" value="${seguTareas}"/>
	<tr>
		<td class="text_izquierda">${txtFchOperacion} </td>
		<td class="text_izquierda"><input name="fchOperacion" type="text" class="Campos" id="fchOperacion" size="15" readonly="readonly" value="${beanResTranSpeiRec.beanComun.fechaOperacion}">
			<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
		</td>
		<td class="text_izquierda">
			<c:choose>
				<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
					<span><a id="idBuscarHto" href="javascript:;">${txtBuscar}</a></span>
                    
                </c:when>
				<c:otherwise>
					<span class="btn_Des"><a href="javascript:;">${txtBuscar}</a></span>
					
				</c:otherwise>
			</c:choose>					
		</td>
		<td class="text_derecha" ></td>
	</tr>