	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuAdmonSaldo.jsp" flush="true">
	<jsp:param name="menuItem" value="admonSaldo" />
	<jsp:param name="menuSubitem" value="muestraAdminSaldo" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
	

<spring:message code="moduloCDA.myMenu.text.adminSaldos" var="tituloModulo"/>
<spring:message code="moduloCDA.administraSaldos.text.titulo" var="titulo"/>
<spring:message code="moduloCDA.administraSaldos.text.edoConexion" var="edoConexion"/>
<spring:message code="moduloCDA.administraSaldos.text.ctaAlterna" var="ctaAlterna"/>
<spring:message code="moduloCDA.administraSaldos.text.monto" var="monto"/>
<spring:message code="moduloCDA.administraSaldos.text.cantOper" var="cantOper"/>
<spring:message code="moduloCDA.administraSaldos.text.totMiInstitucion" var="totMiInstitucion"/>
<spring:message code="moduloCDA.administraSaldos.text.totCuentaAlterna" var="totCuentaAlterna"/>
<spring:message code="moduloCDA.administraSaldos.text.totCalculado" var="totCalculado"/>
<spring:message code="moduloCDA.administraSaldos.text.traspaso" var="traspaso"/>
<spring:message code="moduloCDA.administraSaldos.text.importe" var="importe"/>
<spring:message code="moduloCDA.administraSaldos.text.ctaPrincipal" var="ctaPrincipal"/>
<spring:message code="moduloCDA.administraSaldos.text.saldoNoReservado" var="saldoNoReservado"/>
<spring:message code="moduloCDA.administraSaldos.text.concepto" var="concepto"/>
<spring:message code="moduloCDA.administraSaldos.text.progTraspasos" var="progTraspasos"/>
<spring:message code="moduloCDA.administraSaldos.text.enviar" var="enviar"/>
<spring:message code="moduloCDA.administraSaldos.text.actualizar" var="actualizar"/>
<spring:message code="moduloCDA.administraSaldos.text.devoluciones" var="devoluciones"/>
<spring:message code="moduloCDA.administraSaldos.text.saldoDevoluciones" var="saldoDevoluciones"/>
<spring:message code="codError.ED00078V" var="ED00078V"/>


<spring:message code="codError.CD00040V" var="CD00040V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
	
    <c:set var="searchString" value="${seguTareas}"/>
	<script src="${pageContext.servletContext.contextPath}/js/private/administraSaldos.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
    <script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
	
	<script type="text/javascript">
      var mensajes = {"aplicacion": '${aplicacion}',"CD00040V":'${CD00040V}',"ED00078V":'${ED00078V}'};
    </script>
    
<%--  Componente titulo de pagina --%>
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${titulo}</div>

<form name="idForm" id="idForm" method="post" action="">
	<input type="hidden" name="opcion" id="opcion"/>
	<input type="hidden" name="importe" id="importe"/>
	<input type="hidden" name="concepto" id="concepto"/>
	

	<%--  Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas"><%--  <div class="titleTablaVariasColumnas"></div>  --%>
	<div class="contentTablaVariasColumnas">
	<table>
		<tr>
			<th width="300">${edoConexion}</th>
	        <td width="100"></td>
			<td  width="100" bgcolor="${saldos.semaforo}">	
					
			</td>
		</tr>
	
	</table>
	</div>
	</div>

<%-- Componente tabla de varias columnas --%>
<div class="frameBuscadorSimple"><%--  <div class="titleTablaVariasColumnas"></div>  --%>
<div class="contentBuscadorSimple">
	<table>
		<tr>
			<th style="text-align:left;" width="35%">${ctaAlterna}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${monto}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${cantOper}</th>
		</tr>
		
		<tr>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${totMiInstitucion}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoPrincipal}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoPrincipalCount}</td>
			<!-- <input type="hidden" name="importeTR" id="importeTR" value="${saldos.saldoPrincipal}"/> -->
		</tr>
		<tr>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${totCuentaAlterna}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoAlterno}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoAlternoCount}</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${totCalculado}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoCalculado}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoCalculadoCount}</td>
		</tr>
	
	
		<tr>
			<!-- <td colspan="4"></td> -->
			<td ></td>
		</tr>
	</table>
	<table>
		<tr>
			<th style="text-align:left;" width="35%">${traspaso}</th>
			<th style="text-align:left;" width="35%">${importe} ${traspaso}</th>
		</tr>
		<tbody>
			<tr>
				<td>
					<input name="conceptoAlterna" id="conceptoAlterna" value="Traspaso cuenta alterna" readonly="readonly" type="text" size="32"/>
				</td>
				<!-- <td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
					Alterna Principal
				</td>  -->
				<td>
					<input name="importeAlterna" type="text" id="importeAlterna" size="32" maxlength="99"/>
				</td>
				<td>
					<span><a id="idEnvioAlternaToPrincipal" href="javascript:;">${enviar}</a></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table>
		<tr>
			<th style="text-align:left;" width="35%">${devoluciones}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${monto}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${cantOper}</th>
		</tr>
	
		<tr>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${saldoDevoluciones}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoDevoluciones}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoDevolucionesCount}</td>
		</tr>
	
	</table>
	<hr/>
	<table>
		<tr>
			<th style="text-align:left;" width="35%">${ctaPrincipal}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${monto}</th>
			<th width="100"></th>
			<th style="text-align:left;" width="35%">${cantOper}</th>
		</tr>
	
		<tr>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${saldoNoReservado}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoNoReservado}</td>
			<td></td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; text-align:right">${saldos.saldoNoReservadoCount}</td>
		</tr>
	
	</table>
	<table>
		<tr>
			<th style="text-align:left;" width="35%">${traspaso}</th>
			<th style="text-align:left;" width="35%">${importe} ${traspaso}</th>
		</tr>
		<tbody>
			<tr>
				<td>
					<input name="conceptoPrincipal" id="conceptoPrincipal" value="SPEI-SIAC" readonly="readonly" type="text" size="32"/>
				</td>
				<td>
					<input name="importePrincipal" type="text" id="importePrincipal" size="32" maxlength="99"/>
				</td>
				<td width="40%"></td>
			</tr>
			<tr>
				<th style="text-align:left;">
					${concepto}:
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<input name="conceptoPrincipal" id="conceptoPrincipal" value="SPEI-SIAC" type="text" size="70"/>
				</td>
				<td style="text-align:left;">
					<span><a id="idEnvioPrincipalToAlterna" href="javascript:;">${enviar}</a></span>
				</td>
			</tr>
		</tbody>
	</table>

</div>

</div>





<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
				<td width="279" class="izq"><a id="idActualizar" href="javascript:;">${actualizar}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="izq_Des"><a href="javascript:;" >${actualizar}</a></td>
			</c:otherwise>
		</c:choose>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
				<td width="279" class="der"><a id="idProgHorarios" href="javascript:;">${progTraspasos}</a></td>
			</c:when>
			<c:otherwise>
				<td width="279" class="der_Des"><a href="javascript:;" >${progTraspasos}</a></td>
			</c:otherwise>
		</c:choose>
	</tr>

</table>
</div>
</div>
		<input type="hidden" name="accion" id="accion" value=""/>
				
	</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>