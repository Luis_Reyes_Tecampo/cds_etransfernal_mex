<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraConsMovHistCDA" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.fechaOperacionIni" var="fechaOperacionIni"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.fechaOperacionFin" var="fechaOperacionFin"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrAbonoIni" var="hrAbonoIni"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrAbonoFin" var="hrAbonoFin"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrEnvioIni" var="hrEnvioIni"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrEnvioFin" var="hrEnvioFin"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.claveSPEIOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.descripcion" var="descripcion"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.claveRastreo" var="claveRastreo"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.refTransfer" var="refTransfer"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.ctaBeneficiario" var="ctaBeneficiario"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.montoPago" var="montoPago"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.opeContingencia" var="opeContingencia"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.estatus" var="estatus"/>
<spring:message code="moduloCDA.consultaMovHistCDA.botonLink.buscar" var="buscar"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.infEncontrada" var="infEncontrada"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.refere" var="refere"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.fechaOpe" var="fechaOpe"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.folioPaq" var="folioPaq"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.folioPag" var="folioPag"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.claveSPEIOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.instEmisora" var="instEmisora"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.cveRastreo" var="cveRastreo"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.ctaBenef" var="ctaBenef"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.monto" var="monto"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrAbono" var="hrAbono"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.hrEnvio" var="hrEnvio"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.estatusCDA" var="estatusCDA"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.codError" var="codErrorLabel"/>
<spring:message code="moduloCDA.consultaMovHistCDA.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloCDA.consultaMovHistCDA.botonLink.limpiar" var="limpiar"/>
<spring:message code="moduloCDA.consultaMovHistCDA.botonLink.exportar" var="exportar"/>
<spring:message code="moduloCDA.consultaMovHistCDA.botonLink.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloCDA.consultaMovHistCDA.mensaje" var="mensaje"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaMovHistCDA.text.fechaAbono" var="fechaAbono"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00019V" var="ED00019V"/>
<spring:message code="codError.ED00020V" var="ED00020V"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>

	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["ED00019V"] = '${ED00019V}';
		mensajes["ED00020V"] = '${ED00020V}';
		mensajes["ED00021V"] = '${ED00021V}';
		mensajes["ED00022V"] = '${ED00022V}';
		mensajes["ED00023V"] = '${ED00023V}';
		mensajes["ED00027V"] = '${ED00027V}';
    </script>

<script src="${pageContext.servletContext.contextPath}/js/private/consultaMovHistCDA.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>


	<!-- Componente titulo de p�gina -->
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<form name="idForm" id="idForm" method="post">
		    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsMovHistCDA.beanPaginador.paginaIni}">
			<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsMovHistCDA.beanPaginador.pagina}">
			<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsMovHistCDA.beanPaginador.paginaFin}">
			<input type="hidden" name="paginador.accion" id="accion" value="">
			<input type="hidden" name="fechaOperacion" id="fechaOperacion" value="${fechaOperacion}">
			<input type="hidden" name="referencia" id="referencia" value="">
			<input type="hidden" name="fechaHoy" id="fechaHoy" value="${fechaHoy}">
			<input type="hidden" name="paramFecha3Antes" id="paramFecha3Antes" value="${fecha3Antes}">
			<input type="hidden" name="paramFechaFin" id="paramFechaFin" value="${fechaFin}">
			<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}">

			<input type="hidden" name="fechaOpeInicio" id="fechaOpeInicio" value="${paramFechaOpeInicio}">
			<input type="hidden" name="fechaOpeFin" id="fechaOpeFin" value="${paramFechaOpeFin}">
			<input type="hidden" name="hrAbonoIni" id="hrAbonoIni" value="${paramHrAbonoIni}">
			<input type="hidden" name="hrAbonoFin" id="hrAbonoFin" value="${paramHrAbonoFin}">
			<input type="hidden" name="hrEnvioIni" id="hrEnvioIni" value="${paramHrEnvioIni}">
			<input type="hidden" name="hrEnvioFin" id="hrEnvioFin" value="${paramHrEnvioFin}">
			<input type="hidden" name="cveSpeiOrdenanteAbono" id="cveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}">
			<input type="hidden" name="nombreInstEmisora" id="nombreInstEmisora" value="${paramNombreInstEmisora}">
			<input type="hidden" name="cveRastreo" id="cveRastreo" value="${paramCveRastreo}">
			<input type="hidden" name="ctaBeneficiario" id="ctaBeneficiario" value="${paramCtaBeneficiario}">
			<input type="hidden" name="montoPago" id="montoPago" value="${paramMontoPago}">
			<input type="hidden" name="tipoPago" id="tipoPago" value="${paramTipoPago}">
			<input type="hidden" name="cveMiInstitucLink" id="cveMiInstitucLink" value="">
			<input type="hidden" name="fechaOpeLink" id="fechaOpeLink" value="">
			<input type="hidden" name="cveSpeiLink" id="cveSpeiLink" value="">
			<input type="hidden" name="folioPaqLink" id="folioPaqLink" value="">
			<input type="hidden" name="folioPagoLink" id="folioPagoLink" value="">
			<input type="hidden" name="fchCDALink" id="fchCDALink" value="">



	<!-- Componente buscador simple -->
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table border="1">
				<tbody>

					<tr>
						<td width="150" >
						<font color="RED">${fechaOperacionIni}</font></td>
						<td width="150"><input id="paramFechaOpeInicio" value="${paramFechaOpeInicio}" class="Campos" type="text" size="10" readonly="readonly" name="paramFechaOpeInicio">
							<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"></td>
						<td width="160">${claveSPEIOrdAbono}</td>
						<td width="104"><input id="paramCveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}" name="paramCveSpeiOrdenanteAbono" type="text" class="Campos"  size="20" maxlength="12"></td>

						<td >
						</td>
					</tr>
					<tr>
						<td>
						<font color="RED">${fechaOperacionFin}</font>
						</td>
						<td width="150">
						<input id="paramFechaOpeFin" value="${paramFechaOpeFin}" class="Campos" type="text" size="10" readonly="readonly" name="paramFechaOpeFin">
							<img id="cal2" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar">
						</td>
						<td width="74">${instEmisora}</td>
						<td width="104"><input name="paramNombreInstEmisora" value="${paramNombreInstEmisora}" type="text" class="Campos" id="paramNombreInstEmisora" size="20" maxlength="40"></td>
						<td >
						</td>
					</tr>

					<tr>
						<td>${hrAbonoIni}</td>
						<td width="191">
						<input id="paramHrAbonoIni" value="${paramHrAbonoIni}" class="Campos" type="text" size="10" readonly="readonly" name="paramHrAbonoIni"><img id="cal3" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"></td>
						<td width="74">${claveRastreo}</td>
						<td width="104"><input name="paramCveRastreo" value="${paramCveRastreo}" type="text" class="Campos" id="paramCveRastreo" size="20"  maxlength="30"></td>

						<td>
						</td>
					</tr>
					<tr>
						<td>
						${hrAbonoFin}
						</td>
						<td width="191">
							<input id="paramHrAbonoFin" value="${paramHrAbonoFin}" class="Campos" type="text" size="10" readonly="readonly" name="paramHrAbonoFin"><img id="cal4" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar">
						</td>
						<td width="74">${ctaBeneficiario}</td><td width="104"><input name="paramCtaBeneficiario" value="${paramCtaBeneficiario}" type="text" class="Campos" id="paramCtaBeneficiario" size="20" maxlength="20"></td>

						<td>
						</td>
					</tr>

					<tr>
						<td height="20">${hrEnvioIni}</td>
						<td height="20" width="191"><input id="paramHrEnvioIni" value="${paramHrEnvioIni}" class="Campos" type="text" size="10" readonly="readonly" name="paramHrEnvioIni"><img id="cal5" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"></td>
						<td width="74">${montoPago}</td><td width="104"><input name="paramMontoPago" value="${paramMontoPago}" type="text" class="Campos" id="paramMontoPago" size="20" maxlength="19"></td>
						<td></td>
					</tr>

					<tr>
						<td height="29">${hrEnvioFin}</td>
						<td height="29" width="191"><input id="paramHrEnvioFin" value="${paramHrEnvioFin}" class="Campos" type="text" size="10" readonly="readonly" name="paramHrEnvioFin"><img id="cal6" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"></td>
						<td width="74">${tipoPago}</td><td width="104"><input name="paramTipoPago" value="${paramTipoPago}" type="text" class="Campos" id="paramTipoPago" size="7" maxlength="7"></td>
						<td></td>
						
					</tr>
					<tr>
						<td/>
						<td/>
						<td height="20" width="74">${opeContingencia}</td><td height="20" width="104"><input type="checkbox" id="opeContingencia" name="opeContingencia">
						</td>

						<td height="29" width="121" class="izq">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
						</td>
					</tr>

					<tr>
						<td  colspan="5"><font color="RED">${mensaje}</font></td>

					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Componente tabla de varias columnas -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th nowrap width="122" class="text_izquierda">${refere}</th>
					<th nowrap width="223" class="text_centro" scope="col">${fechaOpe}</th>
					<th nowrap width="122" class="text_centro" scope="col">${folioPaq}</th>
					<th nowrap width="122" class="text_centro" scope="col">${folioPag}</th>
					<th nowrap width="122" class="text_centro" scope="col">${claveSPEIOrdAbono}</th>
					<th nowrap width="122" class="text_centro" scope="col">${instEmisora}</th>
					<th nowrap width="122" class="text_centro" scope="col">${cveRastreo}</th>
					<th nowrap width="122" class="text_centro" scope="col">${ctaBeneficiario}</th>
					<th nowrap width="122" class="text_centro" scope="col">${monto}</th>
					<th nowrap width="122" class="text_centro" scope="col">${fechaAbono}</th>
					<th nowrap width="122" class="text_centro" scope="col">${hrAbono}</th>
					<th nowrap width="122" class="text_centro" scope="col">${hrEnvio}</th>
					<th nowrap width="122" class="text_centro" scope="col">${estatusCDA}</th>
					<th nowrap width="122" class="text_centro" scope="col">${codErrorLabel}</th>
					<th nowrap width="122" class="text_centro" scope="col">${tipoPago}</th>
				</tr>

				<tr>

					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>

				   <c:forEach var="beanMovimientoHistCDA" items="${beanResConsMovHistCDA.listBeanMovimientoCDA}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda"><a id="${beanMovimientoHistCDA.referencia}" href="javascript:;" onclick="consultaMovHistCDA.despliegaDetalle('${beanMovimientoHistCDA.referencia}','${beanMovimientoHistCDA.fechaOpe}','${beanMovimientoHistCDA.cveMiInstituc}', '${beanMovimientoHistCDA.cveSpei}','${beanMovimientoHistCDA.folioPaq}','${beanMovimientoHistCDA.folioPago}','${beanMovimientoHistCDA.fchCDA}')">${beanMovimientoHistCDA.referencia}</a></td>
						<td class="text_izquierda">${beanMovimientoHistCDA.fechaOpe}</td>
						<td class="text_centro">${beanMovimientoHistCDA.folioPaq}</td>
						<td class="text_centro">${beanMovimientoHistCDA.folioPago}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.cveSpei}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.nomInstEmisora}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.cveRastreo}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.ctaBenef}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.monto}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.fechaAbono}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.hrAbono}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.hrEnvio}</td>
						<td class="text_centro">${beanMovimientoHistCDA.estatusCDA}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.codError}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.tipoPago}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanResConsMovHistCDA.listBeanMovimientoCDA}">
			<div class="paginador">
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaIni == beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaIni != beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMovHistCDA.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaAnt!='0' && beanResConsMovHistCDA.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consMovHistCDA.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaAnt=='0' || beanResConsMovHistCDA.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResConsMovHistCDA.beanPaginador.pagina} - ${beanResConsMovHistCDA.beanPaginador.paginaFin}</label>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaFin != beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMovHistCDA.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaFin == beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaFin != beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consMovHistCDA.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResConsMovHistCDA.beanPaginador.paginaFin == beanResConsMovHistCDA.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>

		</div>



		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>

						<td class="odd">&nbsp;</td>
						<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
							<td width="279" class="der"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
						</c:when>
						<c:otherwise>
							<td width="279" class="der_Des"><a href="javascript:;" >${actualizar}</a></td>
						</c:otherwise>
					</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td width="279" class="izq"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">&nbsp;</td>
						<td width="279" class="der"><a id="idLimpiar" href="javascript:;">${limpiar}</a></td>
					</tr>
				</table>
			</div>
		</div>

</form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>