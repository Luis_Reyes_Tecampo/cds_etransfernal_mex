<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas ne '0'}">
				<a id="idCDARechazadas" href="javascript:;" >${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas}</a>
			</c:if>
			<c:if test="${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas eq '0'}">
				${beanResMonitorCDA.beanResMonitorCdaVolumen.volumenCDARechazadas}
			</c:if>	