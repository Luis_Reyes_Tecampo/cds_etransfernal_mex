<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraContingCDAMismoDia" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.nomArchivoOrig" var="nomArchivoOrig"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.fechaHora" var="fechaHora"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.usuario" var="usuario"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.estatus" var="estatus"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.numOpeGeneradas" var="numOpeGeneradas"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.numOpeGenerar" var="numOpeGenerar"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.generarNuevoArchivo" var="generarNuevoArchivo"/>
<spring:message code="moduloCDA.contingenciaCDAMismoDia.text.actualizar" var="actualizar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.CD00020V" var="CD00020V" />
	
	    <script>
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["CD00020V"] = '${CD00020V}';
	 
    </script>


<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/contingenciaCDAMismoDia.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>



	<!-- Componente titulo de p�gina -->
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	



	<!-- Componente tabla de varias columnas -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${subtituloFuncion}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th nowrap width="122" class="text_izquierda">${nomArchivo}</th>
					<th nowrap width="122" class="text_izquierda">${nomArchivoOrig}</th>
					<th nowrap width="223" class="text_centro" scope="col">${fechaHora}</th>
					<th nowrap width="122" class="text_centro" scope="col">${usuario}</th>
					<th nowrap width="122" class="text_centro" scope="col">${estatus}</th>
					<th nowrap width="122" class="text_centro" scope="col">${numOpeGeneradas}</th>
					<th nowrap width="122" class="text_centro" scope="col">${numOpeGenerar}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanContingMismoDia" items="${beanResContingMismoDia.listBeanContingMismoDia}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda">${beanContingMismoDia.nomArchivo}</td>
						<td class="text_izquierda">${beanContingMismoDia.nomArchivoOrig}</td>
						<td class="text_izquierda">${beanContingMismoDia.fechaHora}</td>
						<td class="text_izquierda">${beanContingMismoDia.usuario}</td>
						<td class="text_izquierda">${beanContingMismoDia.estatus}</td>
						<td class="text_izquierda">${beanContingMismoDia.numOpGeneradas}</td>
						<td class="text_derecha">${beanContingMismoDia.numOpGenerar}</td>
					</tr>
				</c:forEach>
				
					
				</tbody>
			</table>
			</div>
			<c:if test="${not empty beanResContingMismoDia.listBeanContingMismoDia}">
			<div class="paginador">
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaIni == beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaIni != beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarContingCDAMismoDia.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaAnt!='0' && beanResContingMismoDia.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultarContingCDAMismoDia.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaAnt=='0' || beanResContingMismoDia.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResContingMismoDia.beanPaginador.pagina} - ${beanResContingMismoDia.beanPaginador.paginaFin}</label>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaFin != beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarContingCDAMismoDia.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaFin == beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaFin != beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarContingCDAMismoDia.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResContingMismoDia.beanPaginador.paginaFin == beanResContingMismoDia.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<c:if test="${beanResContingMismoDia.hayOperacionespendientes}">
									<td width="279" class="izq"><a id="idGeneracionNuevoArchivo" href="javascript:;">${generarNuevoArchivo}</a></td>
								</c:if>
								<c:if test="${!beanResContingMismoDia.hayOperacionespendientes}">
									<td width="279" class="izq_Des"><a href="javascript:;">${generarNuevoArchivo}</a></td>
								</c:if>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${generarNuevoArchivo}</a></td>
							</c:otherwise>
						</c:choose>
					
						
						<td width="6" class="odd"></td>
						
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td width="279" class="der"><a id="idActualizar" href="javascript:;">${actualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${actualizar}</a></td>
							</c:otherwise>
						</c:choose>
						
						
					</tr>
				</table>
			</div>
		</div>
	</div>

<form name="idForm" id="idForm" method="post" action="">
		<input type="hidden" name="paginaIni" id="paginaIni" value="${beanResContingMismoDia.beanPaginador.paginaIni}">
		<input type="hidden" name="pagina" id="pagina" value="${beanResContingMismoDia.beanPaginador.pagina}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResContingMismoDia.beanPaginador.paginaFin}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		
	</form>

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>

		
		
	
