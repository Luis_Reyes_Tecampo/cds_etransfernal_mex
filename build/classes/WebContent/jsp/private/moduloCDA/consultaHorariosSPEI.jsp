<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuCatalogos.jsp" flush="true">
	<jsp:param name="menuItem" value="catalogos" />
	<jsp:param name="menuSubitem" value="muestraCatHorariosSPEI" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="catalogos.general.text.tituloModulo" var="tituloModulo"/>
	
<spring:message code="moduloCDA.consultaHorariosSPEI.text.titulo" var="titulo" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveMedEntrega" var="cveMedEntrega" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveTransfer" var="cveTransfer" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveOperacion" var="cveOperacion" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.na" var="na" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.default" var="lbDefault" />

<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveMedEnt" var="cveMedEnt" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveTransfer" var="cveTransfer" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.cveOpera" var="cveOpera" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.topologiaOp" var="topologiaOp" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.horaInicio" var="horaInicio" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.horaCierre" var="horaCierre" />
<spring:message code="moduloCDA.consultaHorariosSPEI.text.banderaDiaHabil" var="banderaDiaHabil" />

<spring:message code="general.buscar" var="buscar" />
<spring:message code="general.alta" var="alta" />
<spring:message code="general.modificar" var="modificar" />
<spring:message code="general.eliminar" var="eliminar" />
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="codError.CD00040V" var="CD00040V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.CD00044V" var="CD00044V"/>


	<c:set var="searchString" value="${seguTareas}"/>
	<script src="${pageContext.servletContext.contextPath}/js/private/consultaHorariosSPEI.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
	
	<script type="text/javascript">
      var mensajes = {"aplicacion": '${aplicacion}',"CD00040V":'${CD00040V}',"ED00029V":'${ED00029V}',"CD00044V":'${CD00044V}'};
    </script>
    

<c:set var="searchString" value="${seguTareas}"/>

	<%-- Componente titulo de pagina --%>
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${titulo}
	</div>

		<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="paginaIni" id="paginaIni" value="${horariosSPEI.paginador.paginaIni}"/>
			<input type="hidden" name="pagina" id="pagina" value="${horariosSPEI.paginador.pagina}"/>
			<input type="hidden" name="paginaFin" id="paginaFin" value="${horariosSPEI.paginador.paginaFin}"/>
			<input type="hidden" name="accion" id="accion" value=""/>
			<input type="hidden" name="regMedEntrega" value="${strUsuario}"/>
			<input type="hidden" name="regTransfer" value="${strUsuario}"/>
			<input type="hidden" name="regOperacion" value="${strUsuario}"/>

	<%-- Componente buscador simple --%>
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>
					<tr>
						<td class="text_izquierda">
						${cveMedEntrega}
						</td>
						<td>
							<select name="selMedEntrega" class="Campos" id="selMedEntrega">
	          					<option value="">${na}</option> 
	          					<option value="00" ${'00' == selMedEntrega ? 'selected' : ''}>${lbDefault}</option> 
	          					<c:forEach var="reg" items="${lstMedEntrega}">
	           						<option value="${reg.cve}" ${reg.cve == selMedEntrega ? 'selected' : ''} >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
							${cveTransfer} 
						</td>
						<td>     
							<select name="selTransfer" class="Campos" id="selTransfer">
	          					<option value="">${na}</option> 
	          					<option value="00" ${'00' == selTransfer ? 'selected' : ''}>${lbDefault}</option>
	          					<c:forEach var="reg" items="${lstTransfer}">
	           						<option value="${reg.cve}" ${reg.cve == selTransfer ? 'selected' : ''} >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda">
						${cveOperacion}
						</td>
						<td>
							<select name="selOperacion" class="Campos" id="selOperacion">
	          					<option value="">${na}</option> 
	          					<option value="00"  ${'00' == selOperacion ? 'selected' : ''}>${lbDefault}</option>
	          					<c:forEach var="reg" items="${lstOperacion}">
	           						<option value="${reg.cve}" ${reg.cve == selOperacion ? 'selected' : ''} >
	           							${reg.cve}
	           						</option>
	              				</c:forEach>              				                        		
	             			</select>
										&nbsp;
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose> 
						</td>	
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	


	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infoEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table >
				<tr>
					<th  width="132" class="text_centro" colspan=2 >${cveMedEnt}</th>
					<th  width="132" class="text_centro" scope="col">${cveTransfer}</th>
					<th  width="132" class="text_centro" scope="col">${cveOpera}</th>
					<th  width="122" class="text_centro" scope="col">${topologiaOp}</th>
					<th  width="122" class="text_centro" scope="col">${horaInicio}</th>
					<th  width="122" class="text_centro" scope="col">${horaCierre}</th>
					<th  width="122" class="text_centro" scope="col">${banderaDiaHabil}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				<c:forEach var="beanConsHorarios" items="${horariosSPEI.listHorariosSPEI}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td width="100" class="text_centro">
							<input type="checkbox" name="listHorariosSPEI[${rowCounter.index}].seleccionado" />
						</td>
						<input type="hidden" name="listHorariosSPEI[${rowCounter.index}].cveMedioEnt" value="${beanConsHorarios.cveMedioEnt}"/>
						<input type="hidden" name="listHorariosSPEI[${rowCounter.index}].cveTransfe" value="${beanConsHorarios.cveTransfe}"/>
						<input type="hidden" name="listHorariosSPEI[${rowCounter.index}].cveOperacion" value="${beanConsHorarios.cveOperacion}"/>
						<td width="250" class="text_izquierda">${beanConsHorarios.cveMedioEnt}</td>
						<td class="text_izquierda">${beanConsHorarios.cveTransfe}</td>
						<td class="text_izquierda">${beanConsHorarios.cveOperacion}</td>
						<td class="text_derecha">${beanConsHorarios.cveTopoPri}</td>
						<td class="text_izquierda">${beanConsHorarios.horaIncio}</td>
						<td class="text_derecha">${beanConsHorarios.horaCierre}</td>
						<td class="text_derecha">${beanConsHorarios.flgInhabil}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
			
		</div>
		<c:if test="${not empty horariosSPEI.listHorariosSPEI}">
				<div class="paginador">
					<c:if test="${horariosSPEI.paginador.paginaIni == horariosSPEI.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaIni != horariosSPEI.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarHorarioSPEI.do','INI');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaAnt!='0' && horariosSPEI.paginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultarHorarioSPEI.do','ANT');">&lt;${anterior}</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaAnt=='0' || horariosSPEI.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${horariosSPEI.paginador.pagina} - ${horariosSPEI.paginador.paginaFin}</label>
					<c:if test="${horariosSPEI.paginador.paginaFin != horariosSPEI.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarHorarioSPEI.do','SIG');">${siguiente}&gt;</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaFin == horariosSPEI.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaFin != horariosSPEI.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarHorarioSPEI.do','FIN');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${horariosSPEI.paginador.paginaFin == horariosSPEI.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
				</div>
			</c:if>
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
			
				<table>
					<tr>
							
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idAlta" href="javascript:;">${alta}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${alta}</a></td>
							</c:otherwise>
						</c:choose>
				
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'MODIFICAR')}">
								<td width="279" class="der"><a id="idModificar" href="javascript:;">${modificar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="der_Des"><a href="javascript:;">${modificar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
								<td width="279" class="izq"><a id="idEliminar" href="javascript:;">${eliminar}</a></td>
							</c:when>
							<c:otherwise>
								<td width="279" class="izq_Des"><a href="javascript:;">${eliminar}</a></td>
							</c:otherwise>
						</c:choose>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero"></td>
					</tr>
					
				</table>
				
			</div>
		</div>
		
	</div>

		</form>
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
	