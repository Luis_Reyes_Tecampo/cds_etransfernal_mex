
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.servicio" var="servicio"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.usuario" var="usuario"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.FechaAccionIni" var="FechaAccionIni"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.FechaAccionFin" var="FechaAccionFin"/>
<spring:message code="moduloCDA.bitacoraAdmon.botonLink.Buscar" var="Buscar"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.infoEncontrada" var="infoEncontrada"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.fechaOp" var="fechaOp"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.hrOpe" var="hrOpe"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.ip" var="ip"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.modCanalOpe" var="modCanalOpe"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.datoAfectado" var="datoAfectado"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.valorAnt" var="valorAnt"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.valorNuevo" var="valorNuevo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.descOp" var="descOp"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.numReg" var="numReg"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.codError" var="codError"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.descCodError" var="descCodError"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.folioOper" var="folioOper"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idToken" var="idToken"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idOperacion" var="idOperacion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.estatusOperacion" var="estatusOperacion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.codOpera" var="codOpera"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.idSesion" var="idSesion"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.tablaAfectada" var="tablaAfectada"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.instanciaWeb" var="instanciaWeb"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.hostWeb" var="hostWeb"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.datoFijo" var="datoFijo"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.exportar" var="exportar"/>
<spring:message code="moduloCDA.bitacoraAdmon.text.exportarTodo" var="exportarTodo"/>


	
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

<head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>reporte</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->

</head>
		
<body>

						<table >
				<tr>
					<th nowrap width="132" class="text_izquierda">${servicio}</th>
					<th nowrap width="132" class="text_centro" scope="col">${fechaOp}</th>
					<th nowrap width="132" class="text_centro" scope="col">${hrOpe}</th>
					<th nowrap width="122" class="text_centro" scope="col">${ip}</th>
					<th nowrap width="122" class="text_centro" scope="col">${modCanalOpe}</th>
					<th nowrap width="122" class="text_centro" scope="col">${usuario}</th>
					<th nowrap width="122" class="text_centro" scope="col">${datoAfectado}</th>
					<th nowrap width="122" class="text_centro" scope="col">${valorAnt}</th>
					<th nowrap width="122" class="text_centro" scope="col">${valorNuevo}</th>
					<th nowrap width="122" class="text_centro" scope="col">${descOp}</th>
					<th nowrap width="122" class="text_centro" scope="col">${numReg}</th>
					<th nowrap width="122" class="text_centro" scope="col">${nomArchivo}</th>
					<th nowrap width="122" class="text_centro" scope="col">${codError}</th>
					<th nowrap width="122" class="text_centro" scope="col">${descCodError}</th>
					<th nowrap width="122" class="text_centro" scope="col">${folioOper}</th>
					<th nowrap width="122" class="text_centro" scope="col">${idToken}</th>
					<th nowrap width="122" class="text_centro" scope="col">${idOperacion}</th>
					<th nowrap width="122" class="text_centro" scope="col">${estatusOperacion}</th>
					<th nowrap width="122" class="text_centro" scope="col">${codOpera}</th>
					<th nowrap width="122" class="text_centro" scope="col">${idSesion}</th>
					<th nowrap width="122" class="text_centro" scope="col">${tablaAfectada}</th>
					<th nowrap width="122" class="text_centro" scope="col">${instanciaWeb}</th>
					<th nowrap width="122" class="text_centro" scope="col">${hostWeb}</th>
					<th nowrap width="122" class="text_centro" scope="col">${datoFijo}</th>		
				</tr>
			
				<tbody>
				<c:forEach var="beanConsBitAdmon" items="${bitacoraAdmon.listBeanConsBitAdmon}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td width="250" class="text_izquierda">${beanConsBitAdmon.servicio}</td>
						<td class="text_izquierda">${beanConsBitAdmon.fechaOpe}</td>
						<td class="text_izquierda">${beanConsBitAdmon.hrOpe}</td>
						<td class="text_derecha">${beanConsBitAdmon.ip}</td>
						<td class="text_izquierda">${beanConsBitAdmon.moduloCanalOp}</td>
						<td class="text_derecha">${beanConsBitAdmon.usuario}</td>
						<td class="text_derecha">${beanConsBitAdmon.datoAfectado}</td>
						<td class="text_derecha">${beanConsBitAdmon.valAnterior}</td>
						<td class="text_derecha">${beanConsBitAdmon.valNuevo}</td>
						<td class="text_derecha">${beanConsBitAdmon.descOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.numeroRegistros}</td>
						<td class="text_derecha">${beanConsBitAdmon.nomArchivo}</td>
						<td class="text_derecha">${beanConsBitAdmon.codError}</td>
						<td class="text_derecha">${beanConsBitAdmon.descCodError}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.folioOper}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idToken}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.estatusOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.codOperacion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.idSesion}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.tablaAfectada}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.instanciaWeb}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.hostWeb}</td>
						<td class="text_derecha">${beanConsBitAdmon.beanConsBitAdmon2.datoFijo}</td>
					</tr>
				</c:forEach>
					
				</tbody>
			</table>
</body>			
</html>


		
	

	
