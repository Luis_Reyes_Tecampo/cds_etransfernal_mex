<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<input type="hidden" id="esSPID" name="esSPID" value="${esSPID}" />
<jsp:include page="../myHeader.jsp" flush="true" />
<c:choose>
	<c:when test="${esSPID}">
		<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloCDASPID" />
			<jsp:param name="menuSubitem" value="muestraConsMovCDA" />
		</jsp:include>
	</c:when>
	<c:otherwise>
		<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloCDA" />
			<jsp:param name="menuSubitem" value="muestraConsMovCDA" />
		</jsp:include>
	</c:otherwise>
</c:choose>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tituloFuncionalidad" var="tituloFuncionalidad" />
<spring:message code="moduloCDASPID.consultaDetMovCDA.text.tituloFuncionalidad" var="tituloFuncionalidadSPID" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.referencia"	var="referencia" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.datGenerales"	var="datGenerales" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.cveRastreo"	var="cveRastreo" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.fechaOpe"	var="fechaOpe" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.hrAbonoCh"	var="hrAbonoCh" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.hrEnvCDA"	var="hrEnvCDA" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.estatusCDA"	var="estatusCDA" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.emisorOrdenante" var="emisorOrdenante" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.cveSPEI" var="cveSPEI" />
<spring:message code="moduloCDASPID.consultaDetMovCDA.text.cveSPID" var="cveSPID" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.instEmisora" var="instEmisora" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.nomOrden" 	var="nomOrden" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tipoCtaOrd"	var="tipoCtaOrd" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.ctaOrdenan"	var="ctaOrdenan" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.rfcCurp" var="rfcCurp" />
<spring:message	code="moduloCDA.consultaDetMovCDA.text.receptorBeneficiario" var="receptorBeneficiario" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.nomInstRe" var="nomInstRe" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.nomBenef" var="nomBenef" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tipoCtaBen"	var="tipoCtaBen" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.ctaBenef" var="ctaBenef" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.conceptoPago" var="conceptoPago" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.importeIVA" var="importeIVA" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.montoPago" var="montoPago" />
<spring:message code="moduloCDA.consultaDetMovCDA.botonLink.regresar" var="regresar" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaDetMovCDA.text.fechaAbono" var="fechaAbono"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>



<script src="${pageContext.servletContext.contextPath}/js/private/consultaDetMovCDA.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>


<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span>
	- ${tituloFuncionalidad}
</div>

<c:choose>
	<c:when test="${esSPID}">
		<div class="pageTitleContainer">
			<span class="pageTitle">${tituloModulo}</span>
			- ${tituloFuncionalidadSPID}
		</div>
	</c:when>
	<c:otherwise>
		<div class="pageTitleContainer">
			<span class="pageTitle">${tituloModulo}</span>
			- ${tituloFuncionalidad}
		</div>
	</c:otherwise>
</c:choose>

<form:form name="idForm" id="idForm" method="post">
			<input type="hidden" name="paginador.accion" id="accion" value="" />
		    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}" />
			<input type="hidden" name="paginador.pagina" id="pagina" value="${beanPaginador.pagina}" />
			<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}" />
			<input type="hidden" name="fechaOperacion" id="fechaOperacion" value="${paramFechaOperacion}" />
			<input type="hidden" name="accion" id="accion" value="" />
			<input type="hidden" name="paramFechaOpLarga" id="paramFechaOpLarga" value="${fechaOperacionLetra}" />
			<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}" />
			<input type="hidden" name="paramHoraAbonoIni" id="paramHoraAbonoIni" value="${paramHoraAbonoIni}" />
			<input type="hidden" name="paramHoraAbonoFin" id="paramHoraAbonoFin" value="${paramHoraAbonoFin}" />
			<input type="hidden" name="paramHoraEnvioIni" id="paramHoraEnvioIni" value="${paramHoraEnvioIni}" />
			<input type="hidden" name="paramHoraEnvioFin" id="paramHoraEnvioFin" value="${paramHoraEnvioFin}" />
			<input type="hidden" name="paramCveSpei" id="paramCveSpei" value="${paramCveSpei}" />
			<input type="hidden" name="paramDescripcion" id="paramDescripcion" value="${paramDescripcion}" />
			<input type="hidden" name="paramCveRastreo" id="paramCveRastreo" value="${paramCveRastreo}" />
			<input type="hidden" name="paramRefTransfer" id="paramRefTransfer" value="${paramRefTransfer}" />
			<input type="hidden" name="paramCtaBeneficiario" id="paramCtaBeneficiario" value="${paramCtaBeneficiario}" />
			<input type="hidden" name="paramMonto" id="paramMonto" value="${paramMonto}" />
			<input type="hidden" name="paramTipoPago" id="paramTipoPago" value="${paramTipoPago}" />
			<input type="hidden" name ="paramEstatusCda" id="paramEstatusCda" value="${paramEstatusCda}" />


<div class="frameFormularioB">
		<div class="contentFormularioB">
			<div class="titleFormularioB">${referencia} - ${beanConsMovDetCDA.beanMovCdaDatosGenerales.referencia}</div>
<table >

	<tr>
		<th colspan="5" class="text_izquierda">${datGenerales}</th>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: Center;">${referencia}</td>
		<td width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.referencia}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${hrAbonoCh}</td>
		<td width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.hrAbono}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${cveRastreo}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.cveRastreo}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${hrEnvCDA}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.hrEnvio}</td>
	</tr>
	
	
	<tr>
		<td style="background: #F1F1F1; text-align: center;">${fechaOpe}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.fechaOpe}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${estatusCDA}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.estatusCDA}</td>
	</tr>
	
	<tr>
		<td style="background: #F1F1F1; text-align: center;">${tipoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.tipoPago}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${fechaAbono}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanMovCdaDatosGenerales.fechaAbono}</td>
	</tr>

	<tr>
		<th colspan="5" class="text_izquierda">${emisorOrdenante}</th>
	</tr>

	<tr>
		<c:choose>
			<c:when test="${esSPID}">
				<td style="background: #F1F1F1; text-align: center;">${cveSPID}</td>
			</c:when>
			<c:otherwise>
				<td style="background: #F1F1F1; text-align: center;">${cveSPEI}</td>
			</c:otherwise>
		</c:choose>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.cveSpei}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${tipoCtaOrd}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.tipoCuentaOrd}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${instEmisora}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.nomInstEmisora}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${ctaOrdenan}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.ctaOrd}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomOrden}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.nombreOrd}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${rfcCurp}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaOrdenante.rfcCurpOrdenante}</td>
	</tr>

	<tr>
		<th colspan="5" class="izq">${receptorBeneficiario}</th>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomInstRe}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.nomInstRec}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${tipoCtaBen}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.tipoCuentaBened}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomBenef}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.nombreBened}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${ctaBenef}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.ctaBenef}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${rfcCurp}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.rfcCurpBeneficiario}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${conceptoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.conceptoPago}</td>
	</tr>

	<tr>
		<c:if test="${not esSPID}">
			<td style="background: #F1F1F1; text-align: center;">${importeIVA}</td>
			<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
			${beanConsMovDetCDA.beanConsMovCdaBeneficiario.importeIVA}</td>
			<td width="50">${espacioEnBlanco}</td>
		</c:if>
		<td style="background: #F1F1F1; text-align: center;">${montoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanConsMovDetCDA.beanConsMovCdaBeneficiario.monto}</td>
	</tr>
</table>

</div>
</div>

<br/>
<br/>

<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>
		<td class="izq">${espacioEnBlanco}</td>
		<td class="odd">${espacioEnBlanco}</td>
		<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
	</tr>
	<tr>
		<td width="279" class="izq">${espacioEnBlanco}</td>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<td width="279" class="der">${espacioEnBlanco}</td>
	</tr>
</table>


</div>
</div>

</form:form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>