<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraMonitorCargaArchHist" />
</jsp:include>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>



<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>



<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.claveRastreo" var="claveRastreo"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.numeroCuentaBeneficiario" var="numeroCuentaBeneficiario"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.montoPago" var="montoPago"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.nombreInstEmisora" var="nombreInstEmisora"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.seleccionar" var="seleccionar"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.noRegistrosArchivo" var="noRegistrosArchivo"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.text.noRegistrosEncontrados" var="noRegistrosEncontrados"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.botonLink.generarArchivo" var="generarArchivo"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.botonLink.eliminarSeleccion" var="eliminarSeleccion"/>
<spring:message code="moduloCDA.generarArchCDAHistDet.botonLink.regresar" var="regresar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.CD00030V" var="CD00030V" />
<spring:message code="codError.CD00040V" var="CD00040V" />
	    <script>
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["CD00030V"] = '${CD00030V}';
		mensajes["CD00040V"] = '${CD00040V}';
	 
    </script>
    
<script src="${pageContext.servletContext.contextPath}/js/private/generarArchivoCDAHistDet.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>


	<!-- Componente titulo de p�gina -->
	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<form:form name="idForm" id="idForm" method="post" modelAttribute="beanReqConsGenArchCDAHistDet">
		<input type="hidden" name="paginador.paginaIni" id="paginador.paginaIni" value="${archivoCDAHistDet.paginador.paginaIni}"/>
		<input type="hidden" name="paginador.pagina" id="paginador.pagina" value="${archivoCDAHistDet.paginador.pagina}"/>
		<input type="hidden" name="paginador.paginaFin" id="paginador.paginaFin" value="${archivoCDAHistDet.paginador.paginaFin}"/>
		<input type="hidden" name="paginador.accion" id="paginador.accion" value=""/>
		<input type="hidden" name="paginaIni" id="paginaIni" value="${paginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${paginador.pagina}"/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${paginador.paginaFin}"/>
		<input type="hidden" name="accion" id="accion" value=""/>
		<input type="hidden" name="idPeticion" id="idPeticion" value="${idPeticion}"/>
		<input type="hidden" name="numRegArchivo" id="numRegArchivo" value="${archivoCDAHistDet.numRegEncontrados}"/>
		
		


		<!-- Componente tabla de varias columnas -->
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${subtituloFuncion}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th nowrap width="122" class="text_izquierda">${claveRastreo}</th>
					<th nowrap width="223" class="text_centro" scope="col">${numeroCuentaBeneficiario}</th>
					<th nowrap width="223" class="text_centro" scope="col">${montoPago}</th>
					<th nowrap width="223" class="text_centro" scope="col">${nombreInstEmisora}</th>
					<th nowrap width="223" class="text_centro" scope="col">${seleccionar}</th>
				</tr>
			
				<tr>
			
					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>
				
					<c:forEach var="beanGenArchHistDet" items="${archivoCDAHistDet.listBeanGenArchHistDet}" varStatus="rowCounter">
						<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">${beanGenArchHistDet.claveRastreo}
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].idPeticion" value="${beanGenArchHistDet.idPeticion}"/>
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].claveRastreo" value="${beanGenArchHistDet.claveRastreo}"/>
							
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].fchOperacion" value="${beanGenArchHistDet.fchOperacion}"/>
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].cveMiInstitucion" value="${beanGenArchHistDet.cveMiInstitucion}"/>
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].cveInstOrd" value="${beanGenArchHistDet.cveInstOrd}"/>
							
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].folioPaquete" value="${beanGenArchHistDet.folioPaquete}"/>
							<input type="hidden" name="listBeanGenArchHistDet[${rowCounter.index}].folioPago" value="${beanGenArchHistDet.folioPago}"/>							
							</td>
							
							<td class="text_izquierda">${beanGenArchHistDet.numCuentaBeneficiario}</td>
							<td class="text_izquierda">${beanGenArchHistDet.montoPago}</td>
							<td class="text_izquierda">${beanGenArchHistDet.nomInstEmisora}</td>
							<td class="text_centro"><input type="checkbox" name="listBeanGenArchHistDet[${rowCounter.index}].seleccionado" value="true"></td>
						</tr>
					</c:forEach>
				
					<tr>
						<td class="text_izquierda" colspan="3"></td>						
						<td align="right" colspan="1">
						<table style="width: 132px;">
							<tr>
								<th class="text_centro">${noRegistrosArchivo}</th>
							</tr>
						</table>
						</td>
						<td class="odd" align="CENTER">${archivoCDAHistDet.numRegArchivo}</td>
					</tr>
					<tr>
						<td class="text_izquierda" colspan="3"></td>						
						<td align="right" colspan="1">
						<table style="width: 132px;">
							<tr>
								<th class="text_centro">${noRegistrosEncontrados}</th>
							</tr>
						</table>
						</td>
						<td class="odd" align="CENTER">${archivoCDAHistDet.numRegEncontrados}</td>
					</tr>
				</tbody>
				
			</table>
			
			</div>
			<c:if test="${not empty archivoCDAHistDet.listBeanGenArchHistDet}">
			
				<div class="paginador">
					<c:if test="${archivoCDAHistDet.paginador.paginaIni == archivoCDAHistDet.paginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaIni != archivoCDAHistDet.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarGenerarArchHistDet.do','INI','paginador.accion');">&lt;&lt;${inicio}</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaAnt!='0' && archivoCDAHistDet.paginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultarGenerarArchHistDet.do','ANT','paginador.accion');">&lt;${anterior}</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaAnt=='0' || archivoCDAHistDet.paginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
					<label id="txtPagina">${archivoCDAHistDet.paginador.pagina} - ${archivoCDAHistDet.paginador.paginaFin}</label>
					<c:if test="${archivoCDAHistDet.paginador.paginaFin != archivoCDAHistDet.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarGenerarArchHistDet.do','SIG','paginador.accion');">${siguiente}&gt;</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaFin == archivoCDAHistDet.paginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaFin != archivoCDAHistDet.paginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultarGenerarArchHistDet.do','FIN','paginador.accion');">${fin}&gt;&gt;</a></c:if>
					<c:if test="${archivoCDAHistDet.paginador.paginaFin == archivoCDAHistDet.paginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>
				
	
	<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
				<tr>
					
					<c:if test="${not empty archivoCDAHistDet.listBeanGenArchHistDet}">
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
							<td class="izq"><a id="idGenerarArch" href="javascript:;" >${generarArchivo}</a></td>
						</c:when>
						<c:otherwise>
							<td class="izq_Des"><a href="javascript:;" >${generarArchivo}</a></td>
						</c:otherwise>
					</c:choose>
					</c:if>
					<c:if test="${empty archivoCDAHistDet.listBeanGenArchHistDet}">
							<td class="izq_Des"><a href="javascript:;" >${generarArchivo}</a></td>
					</c:if>
					
					<td class="odd">&nbsp;</td>
					<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
				</tr>
				<tr>
					<c:if test="${not empty archivoCDAHistDet.listBeanGenArchHistDet}">
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
							<td width="279" class="izq"><a id="idEliminar" href="javascript:;" >${eliminarSeleccion}</a></td>
						</c:when>
						<c:otherwise>
							<td width="279" class="izq_Des"><a href="javascript:;" >${eliminarSeleccion}</a></td>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${empty archivoCDAHistDet.listBeanGenArchHistDet}">
					<td width="279" class="izq_Des"><a href="javascript:;" >${eliminarSeleccion}</a></td>
				</c:if>	
					<td width="6" class="odd">&nbsp;</td>
					<td width="279" class="cero">&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	</div>
	</form:form>
	
	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>