<!DOCTYPE jsp:include PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<input type="hidden" id="esSPID" name="esSPID" value="${esSPID}"/>
<c:choose>
	<c:when test="${esSPID}">
		<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloCDASPID" />
			<jsp:param name="menuSubitem" value="muestraConsMovCDA" />
		</jsp:include>
	</c:when>
	<c:otherwise>
		<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
			<jsp:param name="menuItem" value="moduloCDA" />
			<jsp:param name="menuSubitem" value="muestraConsMovCDA" />
		</jsp:include>
	</c:otherwise>
</c:choose>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.consultaMovCDA.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.consultaMovCDA.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrAbonoIni" var="rangoHrAbonoIni"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrAbonoFin" var="rangoHrAbonoFin"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrAbonoDesde" var="rangoHrAbonoDesde"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrAbonoHasta" var="rangoHrAbonoHasta"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrEnvioIni" var="rangoHrEnvioIni"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrEnvioFin" var="rangoHrEnvioFin"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrEnvioIniDesde" var="rangoHrEnvioIniDesde"/>
<spring:message code="moduloCDA.consultaMovCDA.text.rangoHrEnvioFinHasta" var="rangoHrEnvioFinHasta"/>
<spring:message code="moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.descripcion" var="descripcion"/>
<spring:message code="moduloCDA.consultaMovCDA.text.claveRastreo" var="claveRastreo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.refTransfer" var="refTransfer"/>
<spring:message code="moduloCDA.consultaMovCDA.text.ctaBeneficiario" var="ctaBeneficiario"/>
<spring:message code="moduloCDA.consultaMovCDA.text.montoPago" var="montoPago"/>
<spring:message code="moduloCDA.consultaMovCDA.text.opeContingencia" var="opeContingencia"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatus" var="estatus"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.buscar" var="buscar"/>
<spring:message code="moduloCDA.consultaMovCDA.text.infEncontrada" var="infEncontrada"/>
<spring:message code="moduloCDA.consultaMovCDA.text.refere" var="refere"/>
<spring:message code="moduloCDA.consultaMovCDA.text.fechaOpe" var="fechaOpe"/>
<spring:message code="moduloCDA.consultaMovCDA.text.folioPaq" var="folioPaq"/>
<spring:message code="moduloCDA.consultaMovCDA.text.folioPag" var="folioPag"/>
<spring:message code="moduloCDA.consultaMovCDA.text.claveSPEIOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.claveSPIDOrdAbono" var="claveSPIDOrdAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.instEmisora" var="instEmisora"/>
<spring:message code="moduloCDA.consultaMovCDA.text.cveRastreo" var="cveRastreo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.ctaBenef" var="ctaBenef"/>
<spring:message code="moduloCDA.consultaMovCDA.text.monto" var="monto"/>
<spring:message code="moduloCDA.consultaMovCDA.text.hrAbono" var="hrAbono"/>
<spring:message code="moduloCDA.consultaMovCDA.text.hrEnvio" var="hrEnvio"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusCDA" var="estatusCDA"/>
<spring:message code="moduloCDA.consultaMovCDA.text.codError" var="codErrorLabel"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.limpiar" var="limpiar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.exportar" var="exportar"/>
<spring:message code="moduloCDA.consultaMovCDA.botonLink.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusPendiente" var="estatusPendiente"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusEnviado" var="estatusEnviado"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusConfirmado" var="estatusConfirmado"/>
<spring:message code="moduloCDA.consultaMovCDA.text.estatusRechazado" var="estatusRechazado"/>
<spring:message code="moduloCDA.consultaMovCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaMovCDA.text.fechaAbono" var="fechaAbono"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>

	 <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["ED00022V"] = '${ED00022V}';
		mensajes["ED00023V"] = '${ED00023V}';
		mensajes["ED00027V"] = '${ED00027V}';
    </script>

<script src="${pageContext.servletContext.contextPath}/js/private/consultaMovCDA.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}<c:if test="${esSPID}">${espacioEnBlanco}SPID</c:if>
	</div>

	<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="paginador.accion" id="accion" value="" />
		    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsMovCDA.beanPaginador.paginaIni}" />
			<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsMovCDA.beanPaginador.pagina}" />
			<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsMovCDA.beanPaginador.paginaFin}" />
			<input type="hidden" name="paramFechaOperacion" id="paramFechaOperacion" value="${beanResConsMovCDA.fechaOperacion}" />
			<input type="hidden" name="referencia" id="referencia" value="" />
			<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}" />
			<input type="hidden" name="paramHoraAbonoIni" id="paramHoraAbonoIni" value="${paramHoraAbonoIni}" />
			<input type="hidden" name="paramHoraAbonoFin" id="paramHoraAbonoFin" value="${paramHoraAbonoFin}" />
			<input type="hidden" name="paramHoraEnvioIni" id="paramHoraEnvioIni" value="${paramHoraEnvioIni}" />
			<input type="hidden" name="paramHoraEnvioFin" id="paramHoraEnvioFin" value="${paramHoraEnvioFin}" />
			<input type="hidden" name="paramCveSpei" id="paramCveSpei" value="${paramCveSpei}" />
			<input type="hidden" name="paramDescripcion" id="paramDescripcion" value="${paramDescripcion}" />
			<input type="hidden" name="paramCveRastreo" id="paramCveRastreo" value="${paramCveRastreo}" />
			<input type="hidden" name="paramRefTransfer" id="paramRefTransfer" value="${paramRefTransfer}" />
			<input type="hidden" name="paramCtaBeneficiario" id="paramCtaBeneficiario" value="${paramCtaBeneficiario}" />
			<input type="hidden" name="paramMonto" id="paramMonto" value="${paramMonto}" />
			<input type="hidden" name="paramTipoPago" id="paramTipoPago" value="${paramTipoPago}" />
			<input type="hidden" name ="paramEstatusCda" id="paramEstatusCda" value="${paramEstatusCda}" />
			<input type="hidden" id="paramFechaOpLarga" name="paramFechaOpLarga" value="${fechaOperacionLetra}" />
			<input type="hidden" id="fechaOpeLink" name="fechaOpeLink" value="" />
			<input type="hidden" id="cveMiInstitucLink" name="cveMiInstitucLink" value="" />
			<input type="hidden" id="cveSpeiLink" name="cveSpeiLink" value="" />
			<input type="hidden" id="folioPaqLink" name="folioPaqLink" value="" />
			<input type="hidden" id="folioPagoLink" name="folioPagoLink" value="" />
			<input type="hidden" id="esSPID" name="esSPID" value="${esSPID}" />

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>

					<tr>
						<td class="text_izquierda" colspan="2" >
							<label style="font-size: 11px;">${fechaOperacion} ${fechaOperacionLetra}</label>
						</td>
						<td  class="text_izquierda">
							<label style="font-size: 11px;">${descripcion}</label>
						</td>
						<td>
							<input type="text" class="Campos" id="desc" name="desc"  size="15" maxlength="40"  value="${paramDescripcion}"/>
						</td>
						<td>
						</td>
					</tr>

					<tr>
						<td><label style="font-size: 11px;">${rangoHrAbonoIni}</label></td>
						<td><label style="font-size: 11px;">${rangoHrAbonoFin}</label></td>
						<td><label style="font-size: 11px;">${claveRastreo}</label></td>
						<td><input name="cveRastreo" type="text" class="Campos" id="cveRastreo" size="15"  maxlength="30" value="${paramCveRastreo}" /></td>
						<td>
						</td>
					</tr>
					<tr>
						<td width="190">
						<label style="font-size: 11px;">${rangoHrAbonoDesde}</label>
						<input id="hrAbonoIniDesde" value="${paramHoraAbonoIni}" class="Campos" type="text" size="10" readonly="readonly" name="hrAbonoIniDesde"/>
						<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td width="150"><label style="font-size: 11px;">${rangoHrAbonoHasta}</label>
						<input id="hrAbonoFinHasta" value="${paramHoraAbonoFin}" class="Campos" type="text" size="10" readonly="readonly" name="hrAbonoFinHasta"/>
							<img id="cal2" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td><label style="font-size: 11px;">${refTransfer}</label></td>
						<td><input name="refTransfer" type="text" class="Campos" id="refTransfer" size="15" maxlength="12" value="${paramRefTransfer}" /></td>
						<td>
						</td>
					</tr>

					<tr>
						<td><label style="font-size: 11px;">${rangoHrEnvioIni}</label></td>
						<td><label style="font-size: 11px;">${rangoHrEnvioFin}</label></td>
						<td><label style="font-size: 11px;">${ctaBeneficiario}</label></td>
						<td><input name="ctaBeneficiario" type="text" class="Campos" id="ctaBeneficiario" size="15" maxlength="20"  value="${paramCtaBeneficiario}"/></td>
						<td>
						</td>
					</tr>
					<tr>
						<td width="190">
						<label style="font-size: 11px;">${rangoHrEnvioIniDesde}</label>
						<input id="hrEnvioIniDesde" value="${paramHoraEnvioIni}" class="Campos" type="text" size="10" readonly="readonly" name="hrEnvioIniDesde"/>
						<img id="cal3" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td width="150"><label style="font-size: 11px;">${rangoHrEnvioFinHasta}</label>
						<input id="hrEnvioFinHasta" value="${paramHoraEnvioFin}" class="Campos" type="text" size="10" readonly="readonly" name="hrEnvioFinHasta"/>
							<img id="cal4" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td><label style="font-size: 11px;">${montoPago}</label></td>
						<td><input name="montoPago" type="text" class="Campos" id="montoPago" size="15" maxlength="19" value="${paramMonto}"/></td>
						<td>
						</td>
					</tr>

					<tr>
						<c:choose>
							<c:when test="${esSPID}">
								<td><label style="font-size: 11px;">${claveSPIDOrdAbono}</label></td>
							</c:when>
							<c:otherwise>
								<td><label style="font-size: 11px;">${claveSPEIOrdAbono}</label></td>
							</c:otherwise>
						</c:choose>
						<td><input id="cveSpeiOrdenanteAbono" name="cveSpeiOrdenanteAbono" type="text" value="${paramCveSpei}" class="Campos" size="20" maxlength="12"/></td>
						<td><label style="font-size: 11px;">${opeContingencia}</label></td>
						<td><input type="checkbox" id="opeContingencia" name="opeContingencia" /></td>
						<td></td>
					</tr>

					<tr>
						<td><label style="font-size: 11px;">${tipoPago}</label></td>
						<td><input id="tipoPago" name="tipoPago" type="text" value="${paramTipoPago}" class="Campos" size="7" maxlength="7"/></td>
						<td><label style="font-size: 11px;">${estatus}</label></td>
						<td>
								<select name="select" class="Campos_Des" id="selectEstatus" >
									<option value=""><label style="font-size: 11px;">${seleccionarOpcion}</label></option>
									<option value="3"><label style="font-size: 11px;">${estatusPendiente}</label></option>
									<option value="1"><label style="font-size: 11px;">${estatusEnviado}</label></option>
									<option value="2"><label style="font-size: 11px;">${estatusConfirmado}</label></option>
									<option value="5"><label style="font-size: 11px;">${estatusRechazado}</label></option>
								</select></td>
						<td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a id="idBuscar" href="javascript:;"><label style="font-size: 11px;">${buscar}</label></a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;"><label style="font-size: 11px;">${buscar}</label></a></span>
							</c:otherwise>
						</c:choose>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
			</table>
		</div>
	</div>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<tr>
					<th style="white-space: nowrap;" width="122" class="text_izquierda">${refere}</th>
					<th style="white-space: nowrap;" width="223" class="text_centro" scope="col">${fechaOpe}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${folioPaq}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${folioPag}</th>
					<c:choose>
						<c:when test="${esSPID}">
							<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${claveSPIDOrdAbono}</th>
						</c:when>
						<c:otherwise>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${claveSPEIOrdAbono}</th>
						</c:otherwise>
					</c:choose>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${instEmisora}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${cveRastreo}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${ctaBeneficiario}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${monto}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${fechaAbono}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${hrAbono}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${hrEnvio}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${estatusCDA}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${codErrorLabel}</th>
					<th style="white-space: nowrap;" width="122" class="text_centro" scope="col">${tipoPago}</th>
				</tr>


				<tbody>

				   <c:forEach var="beanMovimientoCDA" items="${beanResConsMovCDA.listBeanMovimientoCDA}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda"><a id="${beanMovimientoCDA.referencia}" href="javascript:;" onclick="consultaMovCDA.despliegaDetalle('${beanMovimientoCDA.referencia}','${beanMovimientoCDA.fechaOpe}','${beanMovimientoCDA.cveMiInstituc}','${beanMovimientoCDA.cveSpei}', '${beanMovimientoCDA.folioPaq}','${beanMovimientoCDA.folioPago}')">${beanMovimientoCDA.referencia}</a></td>
						<td class="text_izquierda">${beanMovimientoCDA.fechaOpe}</td>
						<td class="text_centro">${beanMovimientoCDA.folioPaq}</td>
						<td class="text_centro">${beanMovimientoCDA.folioPago}</td>
						<td class="text_izquierda">${beanMovimientoCDA.cveSpei}</td>
						<td class="text_derecha">${beanMovimientoCDA.nomInstEmisora}</td>
						<td class="text_izquierda">${beanMovimientoCDA.cveRastreo}</td>
						<td class="text_derecha">${beanMovimientoCDA.ctaBenef}</td>
						<td class="text_izquierda">${beanMovimientoCDA.monto}</td>
						<td class="text_izquierda">${beanMovimientoCDA.fechaAbono}</td>
						<td class="text_derecha">${beanMovimientoCDA.hrAbono}</td>
						<td class="text_derecha">${beanMovimientoCDA.hrEnvio}</td>
						<td class="text_centro">${beanMovimientoCDA.estatusCDA}</td>
						<td class="text_derecha">${beanMovimientoCDA.codError}</td>
						<td class="text_derecha">${beanMovimientoCDA.tipoPago}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			</div>
				<c:if test="${not empty beanResConsMovCDA.listBeanMovimientoCDA}">
					<jsp:include page="../paginador.jsp" >	  					
	  					<jsp:param name="paginaIni" value="${beanResConsMovCDA.beanPaginador.paginaIni}" />
	  					<jsp:param name="pagina" value="${beanResConsMovCDA.beanPaginador.pagina}" />
	  					<jsp:param name="paginaAnt" value="${beanResConsMovCDA.beanPaginador.paginaAnt}" />
	  					<jsp:param name="paginaFin" value="${beanResConsMovCDA.beanPaginador.paginaFin}" />
  						<jsp:param name="servicio" value="consMovCDA.do" />
					</jsp:include>
			</c:if>

		</div>

		<jsp:include page="consultaMovBotonesCDA.jsp" flush="true">
			<jsp:param value="${fn:containsIgnoreCase(searchString,'EXPORTAR')}" name="tareaExportar"/>
			<jsp:param value="${fn:containsIgnoreCase(searchString,'CONSULTAR')}" name="tareaConsultar"/>
		</jsp:include>
</form>

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>