<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDA.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDA" />
	<jsp:param name="menuSubitem" value="muestraConsMovHistCDA" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.tituloFuncionalidad"	var="tituloFuncionalidad" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.subtituloFuncionalidad"	var="subtituloFuncionalidad" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.referencia"	var="referencia" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.datGenerales"	var="datGenerales" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.cveRastreo"	var="cveRastreo" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.fechaOpe"	var="fechaOpe" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.hrAbonoCh"	var="hrAbonoCh" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.hrEnvCDA"	var="hrEnvCDA" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.estatusCDA"	var="estatusCDA" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.emisorOrdenante" var="emisorOrdenante" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.cveSPEI" var="cveSPEI" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.instEmisora" var="instEmisora" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.nomOrden" 	var="nomOrden" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.tipoCtaOrd"	var="tipoCtaOrd" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.ctaOrdenan"	var="ctaOrdenan" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.rfcCurp" var="rfcCurp" />
<spring:message	code="moduloCDA.consultaDetHistMovCDA.text.receptorBeneficiario" var="receptorBeneficiario" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.nomInstRe" var="nomInstRe" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.nomBenef" var="nomBenef" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.tipoCtaBen"	var="tipoCtaBen" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.ctaBenef" var="ctaBenef" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.conceptoPago" var="conceptoPago" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.importeIVA" var="importeIVA" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.text.montoPago" var="montoPago" />
<spring:message code="moduloCDA.consultaDetHistMovCDA.botonLink.regresar" var="regresar" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaDetMovCDA.text.fechaAbono" var="fechaAbono"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<script src="${pageContext.servletContext.contextPath}/js/private/consultaDetHistMovCDA.js?rand=1" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>


<!-- Componente titulo de p�gina -->
<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${tituloFuncionalidad}</div>

<form:form name="idForm" id="idForm" method="post">
	<input type="hidden" name="paginador.accion" id="accion" value="">
    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}">
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanPaginador.pagina}">
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}">
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="fechaOpeInicio" id="fechaOpeInicio" value="${paramFechaOpeInicio}">
	<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}">
	<input type="hidden" name="fechaOpeFin" id="fechaOpeFin" value="${paramFechaOpeFin}">
	<input type="hidden" name="hrAbonoIni" id="hrAbonoIni" value="${paramHrAbonoIni}">
	<input type="hidden" name="hrAbonoFin" id="hrAbonoFin" value="${paramHrAbonoFin}">
	<input type="hidden" name="hrEnvioIni" id="hrEnvioIni" value="${paramHrEnvioIni}">
	<input type="hidden" name="hrEnvioFin" id="hrEnvioFin" value="${paramHrEnvioFin}">
	<input type="hidden" name="cveSpeiOrdenanteAbono" id="cveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}">
	<input type="hidden" name="nombreInstEmisora" id="nombreInstEmisora" value="${paramNombreInstEmisora}">
	<input type="hidden" name="cveRastreo" id="cveRastreo" value="${paramCveRastreo}">
	<input type="hidden" name="ctaBeneficiario" id="ctaBeneficiario" value="${paramCtaBeneficiario}">
	<input type="hidden" name="montoPago" id="montoPago" value="${paramMontoPago}">
	<input type="hidden" name="tipoPago" id="tipoPago" value="${paramTipoPago}">

<!-- Componente tabla de varias columnas -->
<div class="frameFormularioB">
		<div class="contentFormularioB">
			<div class="titleFormularioB">${subtituloFuncionalidad} - ${cveRastreo} ${beanConsMovDetCDA.beanMovCdaDatosGenerales.referencia} </div>
<table >

	<tr>
		<th colspan="5" class="text_izquierda">${datGenerales}</th>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${referencia}</td>
		<td width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.referencia}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${hrAbonoCh}</td>
		<td width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.hrAbono}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${cveRastreo}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.cveRastreo}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${hrEnvCDA}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.hrEnvio}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${fechaOpe}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.fechaOpe}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${estatusCDA}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.estatusCDA}</td>
	</tr>
	<tr>
		<td style="background: #F1F1F1; text-align: center;">${tipoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.tipoPago}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${fechaAbono}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.fechaAbono}</td>
	</tr>

	<tr>
		<th colspan="5" class="text_izquierda">${emisorOrdenante}</th>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${cveSPEI}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.cveSpei}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${tipoCtaOrd}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.tipoCuentaOrd}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${instEmisora}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.nomInstEmisora}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${ctaOrdenan}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.ctaOrd}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomOrden}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.nombreOrd}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${rfcCurp}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.rfcCurpOrdenante}</td>
	</tr>

	<tr>
		<th colspan="5" class="izq">${receptorBeneficiario}</th>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomInstRe}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.nomInstRec}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${tipoCtaBen}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.tipoCuentaBened}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${nomBenef}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.nombreBened}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${ctaBenef}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.ctaBenef}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${rfcCurp}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.rfcCurpBeneficiario}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${conceptoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.conceptoPago}</td>
	</tr>

	<tr>
		<td style="background: #F1F1F1; text-align: center;">${importeIVA}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.importeIVA}</td>
		<td width="50">${espacioEnBlanco}</td>
		<td style="background: #F1F1F1; text-align: center;">${montoPago}</td>
		<td style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;" class="text_izquierda">
		${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.monto}</td>
	</tr>
</table>

</div>
</div>

<br>
<br>

<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<tr>
		<td class="izq">${espacioEnBlanco}</td>
		<td class="odd">${espacioEnBlanco}</td>
		<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
	</tr>
	<tr>
		<td width="279" class="izq">${espacioEnBlanco}</td>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<td width="279" class="der">${espacioEnBlanco}</td>
	</tr>
</table>


</div>
</div>

</form:form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>