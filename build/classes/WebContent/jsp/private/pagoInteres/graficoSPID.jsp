<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* graficoSPID.jsp
*
* Control de versiones:
*
* Version Date/Hour        	  Description
* 1.0     11/09/2016 12:00 			Creaci�n
*
***********************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuPagoInteres.jsp" flush="true">
	<jsp:param name="menuItem" value="pagoInteres" />
	<jsp:param name="menuSubitem" value="muestraGraficoSPID" />
</jsp:include>

<spring:message code="pagoInt.pagoIntSPEI.tipoPago"                 var="labelTipoPago" />
<spring:message code="pagoInteres.graficoSPID.titulo.txtModulo"           		var="txtModulo"/>
<spring:message code="pagoInteres.graficoSPID.titulo.txtTitulo"           	    var="txtTitulo"/>
<spring:message code="pagoInteres.graficoSPID.busqueda.txtInicioFecha"          var="txtInicioFecha"/>
<spring:message code="pagoInteres.graficoSPID.busqueda.txtFinFecha"           	var="txtFinFecha"/>
<spring:message code="pagoInteres.graficoSPID.busqueda.btnBuscar"           	var="btnBuscar"/>
<spring:message code="pagoInteres.graficoSPID.resultado.rango"           		var="rango"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoEnv"           var="txtPagadoEnv"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoRec"           var="txtPagadoRec"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoDevEnv"        var="txtPagadoDevEnv"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoDevRec"        var="txtPagadoDevRec"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoRecMov"        var="txtPagadoRecMov"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtPagadoDevMov"        var="txtPagadoDevMov"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpEnv"          var="txtTotalOpEnv"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpRec"          var="txtTotalOpRec"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpDevEnv"       var="txtTotalOpDevEnv"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpDevRec"       var="txtTotalOpDevRec"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpRecMov"       var="txtTotalOpRecMov"/>
<spring:message code="pagoInteres.graficoSPID.resultado.txtTotalOpDevMov"       var="txtTotalOpDevMov"/>
<spring:message code="pagoInteres.graficoSPID.opcionces.btnRefrescar"           var="btnRefrescar"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/excanvas.js"></script><![endif]-->
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jqplot.dateAxisRenderer.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jqplot.canvasAxisTickRenderer.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jqplot.enhancedLegendRenderer.js"></script>


<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/private/pagoInteres/graficoSPID.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/css/dialogBox/jquery.jqplot.css" />

<c:set var="searchString" value="${seguTareas}"/>
<form name="idForm" id="idForm" action="" method="post">
	<input name="horaInicio" type="hidden" class="CamposCompletar" id="horaInicio" value="" />
	<input name="horaFin" type="hidden" class="CamposCompletar" id="horaFin" value="" />
	<input name="minutoInicio" type="hidden" class="CamposCompletar" id="minutoInicio" value="" />
	<input name="minutoFin" type="hidden" class="CamposCompletar" id="minutoFin" value="" />
	<input name="tipoPago" type="hidden" class="CamposCompletar" id="tipoPago" value="" />
</form>
	<div class="pageTitleContainer">
		<span class="pageTitle">${txtModulo}</span> - ${txtTitulo}
	</div>

	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${filtroBusqueda}</div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>	
					<tr>
						<td class="text_derecha" width="80">${labelTipoPago} </td>
						<td class="text_izquierda" > 
							<select name="selTipoPago" class="Campos" id="selTipoPago" style="width:150px">
	                        	<option value="">---</option>
	                    		<c:forEach var="tipoPago" items="${resBean.tipoPagoList}">	
	      							<option value="${tipoPago.cveTipoPago}">${tipoPago.descripcion}</option>
			    				</c:forEach>
	                        </select>
						</td>
					
						<td class="text_derecha">${txtInicioFecha}</td>
                        <td class="text_izquierda">
                        	<select name="selHoraInicio" class="Campos" id="selHoraInicio" style=" width:60px">
	                    		<c:forEach var="hora" items="${horas}">
	      							<option value="${hora}">${hora}</option>
			    				</c:forEach>
	                        </select>
	                        <select name="selMinutoInicio" class="Campos" id="selMinutoInicio" style=" width:60px">
	                    		<c:forEach var="minuto" items="${minutos}">
	      									<option value="${minuto}">${minuto}</option>
			    				</c:forEach>
	                        </select>
	                        

	                         
                        </td>
                    </tr>
                    <tr> 
                    	<td class="text_derecha" width="80"></td>
						<td class="text_izquierda" >
						</td>
                        <td class="text_derecha" width="80" >${txtFinFecha}</td>
                        <td class="text_izquierda">
                        
                        	<select name="selHoraFin" class="Campos" id="selHoraFin" style=" width:60px">
	                    		<c:forEach var="hora" items="${horas}">
	      							<option value="${hora}">${hora}</option>
			    				</c:forEach>
	                        </select>
                            <select name="selMinutoFin" class="Campos" id="selMinutoFin" style=" width:60px">
	                    		<c:forEach var="minuto" items="${minutos}">
	      									<option value="${minuto}">${minuto}</option>
			    				</c:forEach>
	                        </select>
                            
                             
                             	         
                        </td>
					</tr>
					
					<tr> 
                    	<td class="text_derecha" width="80"></td>
						<td class="text_izquierda" >
						</td>
                        <td class="text_derecha" colspan="2" >${LEYENDA_HORA}</td>
					</tr>
					
					<tr>
						<td>
						</td>
						<td>
						</td>
						<td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(  searchString,'CONSULTAR')}">
									<span><a href="javascript:;" id="idBuscar">${btnBuscar}</a></span>
								</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${btnBuscar}</a></span>
								</c:otherwise>
							</c:choose>
						<td>
					</tr>
					
					<tr>
						<td class="text_centro" colspan="4">
	                               <span id="textFechaPeriodo"></span>         
						</td>
					</tr>

					<tr>
						<td class="text_centro" align="center" colspan="4">
	                       <div id="chartdiv" style="height:350px;width:600px;"></div>                      
						</td>
					</tr>
										<tr>
						<td class="text_izquierda" colspan="2">
	                                   <br/>
						</td>
					</tr>
					<tr>
						<td class="text_izquierda" colspan="2">
	                                   <br/>      
						</td>
					</tr>
					
					<tr>
						<td class="text_derecha" width="100" ><span id="textPagado"></span></td>
                        <td class="text_izquierda">
                        	<span id="idPagadoSpan">
                            	<input name="pagado" type="text" class="Campos" id="pagado" value="" maxlength="255" readonly/>
                            </span>
                        </td>
                        <td class="text_derecha" width="100" ><span id="textTotalOp"></span></td>
                        <td class="text_izquierda">
                        	<span id="idOperaSpan">
                            	<input name="opera" type="text" class="Campos" id="opera" value="" maxlength="255" readonly/>
                            </span>
                        </td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="framePieContenedor" class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<tr>
						<td width="279" class="cero">${espacioEnBlanco}</td>
						<td width="279" class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td class="der"><a id="idRefrescar" href="javascript:;">${btnRefrescar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des"><a href="javascript:;">${btnRefrescar}</a></td>
							</c:otherwise>
						</c:choose>
						
						
						
					</tr>

				</table>

			</div>
		</div>
	


	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
    </script>
 
    
    <c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');			
		   	   
	
	</script>
</c:if>