<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuPagoInteres.jsp" flush="true">
	<jsp:param name="menuItem"    value="pagoInteres" />
</jsp:include>

	<script src="${pageContext.servletContext.contextPath}/lf/${LyFBean.lookAndFeel}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>

	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


		
		<spring:message code="general.nombreAplicacion" var="app"/>
		<spring:message code="general.bienvenido"       var="welcome"/>
		
	
		<div class="pageTitleContainer">
			<span class="pageTitle">${welcome}</span> - ${app}
		</div>
		

