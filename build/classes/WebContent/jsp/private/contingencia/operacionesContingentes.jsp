<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../myHeader.jsp" flush="true"/>
<c:if  test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }" >
	<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
		<jsp:param name="menuItem"    value="contingencia" />				
		<jsp:param name="menuSubitem" value="operacionesContingentes" />			
	</jsp:include>
</c:if>	

<c:if  test="${beanResConsultaOperContingente.nomPantalla eq 'historica' }" >
	<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
		<jsp:param name="menuItem"    value="contingencia" />				
		<jsp:param name="menuSubitem" value="operacionesContingentesHto" />			
	</jsp:include>
</c:if>	



<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.nomArchivo" var="nomArchivo"/>
<spring:message code="moduloCDA.monitorCargaArchivosHist.text.estatusCargaArchivo" var="estatusCargaArchivo"/>
<spring:message code="moduloPOACOA.myMenu.text.monitorContingCanales" var="monitorContingCanales"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.textNomArchivo" var="textNomArchivo"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.textMedEnt" var="textMedEnt"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.nomArchivo" var="labNomArchivo"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.estatus" var="labEstatus"/> 
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.numPagos" var="labNumPagos"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.numPagosErr" var="labNumPagosErr"/>
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.totMonto" var="labTotMonto"/> 
<spring:message code="moduloPOACOA.monCargaArchCanContig.text.totMontoErr" var="labTotMontoErr"/> 

<spring:message code="moduloPOACOA.cargaArchEnv.text.txtVerDetalle" var="labVerDetalle"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtExportar" var="labExportar"/> 
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtExportarTodo" var="labExportarTodo"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtProcesar" var="labProcesar"/>

<spring:message code="moduloPOACOA.cargaArchEnv.text.txtFechaCaptura" var="labFechaCaptura"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtClaveUsuario" var="labClaveUsuario"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtFechaProceso" var="labFechaProceso"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtMensaje" var="labMensaje"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtClaveMedioEntrega" var="labClaveMedioEntrega"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtMediodeEntrega" var="labMediodeEntrega"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="labtituloModulo"/>
<spring:message code="menu.operacionesContingentes.text.titulo" var="labtituloOper"/>
<spring:message code="moduloPOACOA.cargaArchEnv.text.txtCargarArchivo" var="labCargarArchivo"/> 
<spring:message code="menu.operacionesContingentes.text.tituloHto" var="labtituloOperHto"/>


<spring:message code="moduloPOACOA.catCanales.botonLink.buscar"           var="txtBuscar"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00068V" var="ED00068V"/>
<spring:message code="codError.CD00010V" var="CD00010V"/>
<spring:message code="codError.ED00029V" var="ED00029V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00086V" var="ED00086V"/>
<spring:message code="codError.ED00126V" var="ED00126V"/>
<spring:message code="codError.OK00020V" var="OK00020V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00173V" var="CD00173V"/>

<spring:message code="moduloContingencia.codError.ED00003V" var="ED00003V"/>
<spring:message code="moduloContingencia.codError.ED00004V" var="ED00004V"/>

<spring:message code="moduloCDA.monitorCargaArchivosHist.botonLink.actualizar" var="actualizar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<script src="${pageContext.servletContext.contextPath}/js/private/contingencia/operacionesContingentes.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<c:if  test="${beanResConsultaOperContingente.nomPantalla eq 'historica' }" >
	<script src="${pageContext.servletContext.contextPath}/js/private/contingencia/operacionHto.js" type="text/javascript"></script>
</c:if>



<script type="text/javascript">
	var mensajes = {"aplicacion": 
	'${aplicacion}',
	"ED00011V":'${ED00011V}',
	"ED00000V":'${ED00000V}',
	"OK00001V":'${OK00001V}',
	"OK00002V":'${OK00002V}',
	"ED00068V":'${ED00068V}',
	"EC00011B":'${EC00011B}',
	"ED00126V":'${ED00126V}',
	"CD00010V":'${CD00010V}',
	"ED00029V":'${ED00029V}',
	"ED00021V":'${ED00021V}',
	"ED00086V":'${ED00086V}',
	"ED00171V":'${ED00171V}',
	"ED00181V":'${ED00181V}',
	"ED00191V":'${ED00191V}',
	"OK00020V":'${OK00020V}',
	"ED00065V":'${ED00065V}',
	"CD00173V":'${CD00173V}',
	"ED00003V":'${ED00003V}',
	"ED00004V":'${ED00004V}'};
	
	 var tipoOrden = '${tipoOrden}';	
</script>
	<c:set var="searchString" value="${seguTareas}"/>
	
	<%-- Componente titulo de p�gina --%>
	
	<div class="pageTitleContainer">
		<c:choose>								
			<c:when test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">
				<span class="pageTitle">${labtituloModulo}</span> - ${labtituloOper}			
			</c:when>
			<c:otherwise>			
				<span class="pageTitle">${labtituloModulo}</span> - ${labtituloOperHto}			
			</c:otherwise>
		</c:choose>	
	</div>
	<form name="formArchProc" id="formArchProc" method="post" action="">
	<input type="hidden" name="nombreArch" id="nombreArch">
	<input type="hidden" name="cveMedEntregaOculto" id="cveMedEntregaOculto" value="${cveMedioEntregaHide}">
	<input type="hidden" id="nomPantalla" name="nomPantalla" value="${beanResConsultaOperContingente.nomPantalla}"/>
	<input type="hidden" id="fechaOperacion" name="fechaOperacion" value="${beanResConsultaOperContingente.fechaOperacion}"/>
		
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
				<tbody>
					<c:choose>								
						<c:when test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">
							<tr>
								<td>${textNomArchivo}<input id="archProc" name="archProc" type="file"/></td>
							</tr>
							<tr>
									<td>${textMedEnt} 
										<select id="cmbCanal" name="cmbCanal" class="CamposCompletar" >
											<c:if test="${listaCanales!=null}">						
												<c:forEach var="canalBean" items="${listaCanales}">
													<option  value="${canalBean.idCanal}">${canalBean.idCanal} - ${canalBean.nombreCanal}</option>
												</c:forEach>
											</c:if>
										</select>
									</td>
									<td class="text_derecha"><span><a id="cargaArchivo" href="javascript:cargaArchivo();">${labCargarArchivo}</a></span>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td class="text_izquierda" >${labFechaCaptura} 
									<input name="fchOperacion" type="text" class="Campos" id="fchOperacion" size="15" readonly="readonly" value="${beanResConsultaOperContingente.fechaOperacion}">
									<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
								</td>
							</tr>
							<tr>
								<td class="text_izquierda"  width="279" >
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
											<span><a id="idBuscar" href="javascript:;">${txtBuscar}</a></span>											
										</c:when>
										<c:otherwise>
											<span class="btn_Des"><a href="javascript:;">${txtBuscar}</a></span>														
										</c:otherwise>
									</c:choose>	
								</td>
							</tr>
							
						</c:otherwise>
					</c:choose>	
				</tbody>
			</table>
			
		</div>
		
		</div>

	<%-- Componente tabla de varias columnas --%>
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas"></div>
		<div class="contentTablaEstandar "style="overflow-x: scroll; overflow-y: hidden">
			<table class="table-cons table" id="tableConsulta">
			<caption></caption>
		      <thead>
				<tr>
					<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">
						<th class="text_centro"  width="5"> </th>
					</c:if>
					<th class="text_centro"  width="80">${labNomArchivo}</th>
					<th class="text_centro"  width="30">${labEstatus}</th>
					<th class="text_centro"  width="80">${labNumPagos}</th>
					<th class="text_centro"  width="80">${labNumPagosErr}</th>					
					<th class="text_centro"  width="80">${labTotMonto}</th>
					<th class="text_centro"  width="80">${labTotMontoErr}</th>
					<th class="text_centro"  width="80">${labClaveUsuario}</th>
					<th class="text_centro"  width="80">${labFechaCaptura}</th>
					<th class="text_centro"  width="80">${labFechaProceso}</th>
					<th class="text_centro"  width="80">${labMensaje}</th>
					<th class="text_centro"  width="80">${labClaveMedioEntrega}</th>
					<th class="text_centro"  width="80">${labMediodeEntrega}</th>
					<th class="text_centro"  width="80"> </th>
				</tr>
			
				</thead>
				<tbody>
					<c:forEach var="beanMonCargaArchCanConting" items="${beanResConsultaOperContingente.listaBeanResConsOper}" varStatus="rowCounter">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}"> 
						<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">	
							<td>
							<input type="checkbox" id="check" name="listaBeanResConsOper[${rowCounter.index}].seleccionado" id="seleccionado${rowCounter.index}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].nombreArchivo" id="nombreArchivo"  value="${beanMonCargaArchCanConting.nombreArchivo}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].estatus" id="estatus"  value="${beanMonCargaArchCanConting.estatus}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].numPagos" id="numPagos"  value="${beanMonCargaArchCanConting.numPagos}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].numPagosErr" id="numPagosErr"  value="${beanMonCargaArchCanConting.numPagosErr}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].totalMonto" id="totalMonto"  value="${beanMonCargaArchCanConting.totalMonto}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].totalMontoErr" id="totalMontoErr"  value="${beanMonCargaArchCanConting.totalMontoErr}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].cveUsuarioAlta" id="cveUsuarioAlta"  value="${beanMonCargaArchCanConting.cveUsuarioAlta}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].fchCaptura" id="fchCaptura"  value="${beanMonCargaArchCanConting.fchCaptura}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].fchProceso" id="fchProceso"  value="${beanMonCargaArchCanConting.fchProceso}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].mensaje" id="mensaje"  value="${beanMonCargaArchCanConting.mensaje}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].cveMedioEnt" id="cveMedioEnt"  value="${beanMonCargaArchCanConting.cveMedioEnt}" />
							<input type="hidden" name="listaBeanResConsOper[${rowCounter.index}].descripcion" id="descripcionO"  value="${beanMonCargaArchCanConting.descripcion}" /></td>
						</c:if>
						<td class="text_izquierda">${beanMonCargaArchCanConting.nombreArchivo}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.estatus}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.numPagos}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.numPagosErr}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.totalMonto}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.totalMontoErr}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.cveUsuarioAlta}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.fchCaptura}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.fchProceso}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.mensaje}</td>
						<td class="text_centro">${beanMonCargaArchCanConting.cveMedioEnt}</td>
						<td class="text_izquierda">${beanMonCargaArchCanConting.descripcion}</td>
						<td class="text_izquierda">
							<a id="idVerDetalle" href="javascript:verDetalle('${beanMonCargaArchCanConting.nombreArchivo}');">${labVerDetalle}</a>
						</td>
						
					</tr>
					</c:forEach>
						<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
				</tbody>
			</table>
		</div>
		<c:if test="${not empty beanResConsultaOperContingente.listaBeanResConsOper}">
			<div class="paginador">
			<div style="text-align: left; float:left; margin-left: 5px;">
			</div>
		</c:if>
	</div>
	
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<td width="279" class="izq">
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<span>
										<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">
											<a id="idActualizar" href="javascript:;">${actualizar}</a>
										</c:if>
										<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'historica' }">
											<a id="idActualizarHto" href="javascript:;">${actualizar}</a>
										</c:if>
									</span>											
								</c:when>
								<c:otherwise>
									<span class="btn_Des"><a href="javascript:;">${actualizar}</a></span>														
								</c:otherwise>
							</c:choose>	
						</td>
						<td width="279"  class="odd">${espacioEnBlanco}</td>
						
						<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'normal' }">
							<td width="279" class="der"><a id="idProcesar" href="javascript:;">${labProcesar}</a></td>
						</c:if>	
						<c:if test="${beanResConsultaOperContingente.nomPantalla eq 'historica' }">
							<td width="279"  class="cero"><a href="javascript:;"> </a></td>
						</c:if>	
						
					</tr>
				</table>
			</div>
		</div>
	

	

</form>
<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>