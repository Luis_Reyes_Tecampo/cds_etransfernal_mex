<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloSPEICero.jsp" flush="true">
    <jsp:param name="menuItem" value="moduloSPEICero" />
    <jsp:param name="menuSubitem" value="moduloReporteDia" />
</jsp:include>


<script src="${pageContext.servletContext.contextPath}/js/private/SPEICero/reporteDia.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


<spring:message code="SPEICero.formulario.title" var="tituloModulo"/>
<spring:message code="SPEICero.reporteEmision.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<%@include file="estructura/moduloReportes.jsp" %>
	

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>