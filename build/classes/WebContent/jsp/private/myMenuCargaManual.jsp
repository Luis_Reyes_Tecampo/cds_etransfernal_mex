<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="mx.isban.agave.configuracion.Configuracion"%>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"           type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css"/>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css"/>

<spring:message code="moduloCDA.myMenu.text.moduloCDA" var="moduloCDA"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI" />
<spring:message code="moduloCatalogo.myMenu.text.moduloCatalogos" var="moduloCatalogos"/>
<spring:message code="moduloAdmonSaldo.myMenu.text.admonSaldo" var="adminSaldos"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID" var="txtModuloSPID"/>
<spring:message code="pagoInteres.myMenu.text.moduloPagoInteres" var="txtModuloPagoInteres"/>
<spring:message code="moduloCDA.myMenu.text.moduloCDASPID" var="moduloCDASPID"/>

<spring:message code="moduloCapturasManuales.myMenu.text.principal" var="principal"/>
<spring:message code="moduloCapturasManuales.myMenu.text.capturasManuales" var="capturasManuales"/>
<spring:message code="moduloCapturasManuales.myMenu.text.capturaCentral" var="capturaCentral"/>
<spring:message code="moduloCapturasManuales.myMenu.text.consultaOperaciones" var="consultaOperaciones"/>
<spring:message code="moduloCapturasManuales.myMenu.text.autorizarOperaciones" var="autorizarOperaciones"/>
<spring:message code="moduloCapturasManuales.myMenu.text.mantenimientoTemp" var="mantenimientoTemp"/>
<spring:message code="moduloCatalogo.myMenu.text.moduloSPEI" var="moduloSPEI"/>
<spring:message code="SPEICero.formulario.title" var="moduloSPEICero"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="moduloContingencia"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<c:set var="searchString" value="${seguServicios}" />
	
<body onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
					<li id="principal"   	  class="startMenuGroup">              <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">             	<span>${principal}</span></a></li>
					<li id="admonSaldo"   	  class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/admonSaldo/admonSaldoInit.do">             	<span>${adminSaldos}</span></a></li>					
					<li id="capturasManuales" class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">   <span>${capturasManuales}</span></a>
						<ul>
						    <c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'muestraCapturaCentral.do')}">
									<li id="capturaCentral">
										<a href="${pageContext.servletContext.contextPath}/capturasManuales/muestraCapturaCentral.do">&gt;
											<span class="subMenuText">${capturaCentral}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="capturaCentral">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${capturaCentral}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'consultaOperaciones.do')}">
									<li id="ConsultaOperaciones">
										<a href="${pageContext.servletContext.contextPath}/capturasManuales/consultaOperaciones.do">&gt;
											<span class="subMenuText">${consultaOperaciones}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="ConsultaOperaciones">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${consultaOperaciones}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'mostrarOperacionesPendientes.do')}">
									<li id="AutorizarOperaciones">
										<a href="${pageContext.servletContext.contextPath}/capturasManuales/mostrarOperacionesPendientes.do">&gt;
		                            		<span class="subMenuText">${autorizarOperaciones}</span>
		                            	</a>
		                            </li>
								</c:when>
								<c:otherwise>
									<li id="AutorizarOperaciones">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${autorizarOperaciones}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'consultarMtoTemplates.do')}">
									<li id="mantenimientoTemplates">
										<a href="${pageContext.servletContext.contextPath}/capturasManuales/consultarMtoTemplates.do">&gt;
											<span class="subMenuText">${mantenimientoTemp}</span>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="mantenimientoTemplates">
										<a href="javascript:;">&gt;
											<span class="subMenuText">${mantenimientoTemp}</span>
										</a>
									</li>								
								</c:otherwise>
							</c:choose>
						</ul>	
						</li>
					<li id="catalogos"        class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/catalogos/catalogosInit.do">           <span>${moduloCatalogos}</span></a></li>
					<li id="moduloCDA"        class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloCDA/moduloCDAInit.do">           <span>${moduloCDA}</span></a></li>
					<li id="moduloCDASPID"    class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloCDASPID/moduloCDASPIDInit.do">   <span>${moduloCDASPID}</span></a></li>
					<li id="moduloPOACOA"     class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOAInit.do">     <span>${moduloPOACOASPEI}</span></a></li>
					<li id="moduloPOACOASPID"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOASPIDInit.do">         <span>${moduloPOACOASPID}</span></a></li>
					<li id="moduloSPID"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPID/moduloSPIDInit.do">         <span>${txtModuloSPID}</span></a></li>
					<li id="moduloSPEI"       class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEI/moduloSPEIInit.do">             <span>${moduloSPEI}</span></a></li>
					<li id="moduloSPEICero"   class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/moduloSPEICero/moduloSPEICero.do">         <span>${moduloSPEICero}</span></a></li>
					<li id="pagoInteres"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/pagoInteres/moduloPagoInteresInit.do"> <span>${txtModuloPagoInteres}</span></a></li>
					<li id="contingencia"      class="withSubMenus startMenuGroup"> <a href="${pageContext.servletContext.contextPath}/contingencia/contingenciaInit.do">         <span>${moduloContingencia}</span></a></li>
				</ul>
			
				<div id="menuFooter">
					<div></div>
				</div>
	
			</div>
		</c:if>
	</div>
</body>
<div id="content_container"/>