<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<tr>
	<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">${param.cdaPendEnviar}</td>
	<td></td>

	<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
	${param.montoCDAPendEnviar}
	</td>
	<td style="border-width: 1px; border: solid; border-color: #BFBFBF;">
		<c:if test="${param.volumenCDAPendEnviarNe0}">
			<a id="idCDAPendEnviar" href="javascript:;" >${param.volumenCDAPendEnviar}</a>
		</c:if>
		<c:if test="${param.volumenCDAPendEnviarEq0}">
			${param.volumenCDAPendEnviar}
		</c:if>
	</td>
</tr>