<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="mx.isban.eTransferNal.beans.moduloCDASPID.BeanDetalleArchivoCDASPID"%>
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="monitorCargArchHistCDASPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.regresar" var="regresar" />
<spring:message code="general.generarArchivo" var="generarArchivo" />
<spring:message code="general.regresar" var="regresar" />
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.eliminarSeleccion" var="eliminarSeleccion" />
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.tituloFuncionalidad" var="tituloFuncionalidad" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.subTitulo" var="tituloTabla" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.claveRastreo" var="claveRastreo" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.numeroBenefinario" var="numeroBeneficiario" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.montoPago" var="montoPago" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.institucionEmisora" var="institucionEmisora" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.selecionar" var="seleccionar" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.registrosArchivos" var="registrosArchivos" />
<spring:message code="moduloCDASPID.cargaArchivosHistoricosDetalleCDASPID.registrosEncontrados" var="registrosEncontrados" />
<spring:message code="codError.CD00010V" var="CD00010V" />
<spring:message code="codError.CD00040V" var="CD00040V" />
<spring:message code="codError.CD00030V" var="CD00030V" />
<spring:message code="codError.ED00026V" var="ED00026V" />
<spring:message code="codError.EC00011B" var="EC00011B" />
<spring:message code="codError.OK00000V" var="OK00000V" />
<spring:message code="general.nombreAplicacion" var="aplicacion" />
	    <script type="text/javascript">
       var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["CD00010V"] = '${CD00010V}';
		mensajes["CD00040V"] = '${CD00040V}';
		mensajes["CD00030V"] = '${CD00030V}';
		mensajes["ED00026V"] = '${ED00026V}';
	 	mensajes["EC00011B"] = '${EC00011B}';
	 	mensajes["OK00000V"] = '${OK00000V}';
    </script>

<c:set var="searchString" value="${seguTareas}"/>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/cargaArchivosHistoricosDetalleCDASPID.js"
		type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>

<form id="archivosHistoricosDetalleCADSPID" name="archivosHistoricosDetalleCADSPID" action="" method="post">
<input type="hidden" id="codigoError" name="codigoError" value="${codigoError}"/>
<input type="hidden" id="mensajeError" name="mensajeError" value="${mensajeError}"/>
<input type="hidden" name="paginaIni" id="paginaIni" value="${respuesta.paginador.paginaIni}"/>
<input type="hidden" name="pagina" id="pagina" value="${respuesta.paginador.pagina}"/>
<input type="hidden" name="paginaFin" id="paginaFin" value="${respuesta.paginador.paginaFin}"/>
<input type="hidden" name="accion" id="accion" value=""/>
<input type="hidden" name="idArchivo" id="idArchivo" value="${idArchivo}"/>
<input type="hidden" id="elimina" name="elimina" value=""/>
<input type="hidden" name="countlistBeanConsOperaciones" value="${fn:length(respuesta.listaDetalle)}"/>

<div class="pageTitleContainer"><span class="pageTitle">${tituloModulo}</span>
- ${tituloFuncionalidad}</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${tituloTabla}</div>
			<div class="contentTablaVariasColumnas">
				<table>
					<caption></caption>
					<tr>
						<th style="font-size:11px; text-align:left; width:30%;">${claveRastreo}</th>
						<th style="font-size:11px; width:25%;">${numeroBeneficiario}</th>
						<th style="font-size:11px; width:10%;">${montoPago}</th>
						<th style="font-size:11px; width:25%;">${institucionEmisora}</th>
						<th style="font-size:11px; width:10%;">${seleccionar}</th>
					</tr>
					<c:forEach var="respuesta" items="${respuesta.listaDetalle}" varStatus="rowCounter">
						<tr class="${rowCounter.count % 2 eq 0 ? 'odd3' : 'odd'}">
							<td class="text_izquierda"><label style="font-size:11px;">${respuesta.claveRastreo}</label>						
							</td>
							<td class="text_izquierda"><label style="font-size:11px;">${respuesta.cuentaBeneficiario}</label></td>
							<td class="text_izquierda"><label style="font-size:11px;">${respuesta.montoPago}</label></td>
							<td class="text_izquierda"><label style="font-size:11px;">${respuesta.institucionEmisora}</label></td>
							<td class="text_centro">
								<input type="hidden" id="idArchivo${rowCounter.index}"name="idArchivo${rowCounter.index}" 
									value="${respuesta.idArchivo}"/>
								<input type="hidden" id="claveRastreo${rowCounter.index}"  name="claveRastreo${rowCounter.index}" 
									value="${respuesta.claveRastreo}"/>
								<input type="hidden" id="fechaOperacion${rowCounter.index}"  name="fechaOperacion${rowCounter.index}" 
									value="${respuesta.fechaOperacion}"/>
								<input type="hidden" id="claveInstitucion${rowCounter.index}" name="claveInstitucion${rowCounter.index}" 
									value="${respuesta.claveInstitucion}"/>
								<input type="hidden" id="institucionOrdinaria${rowCounter.index}" name="institucionOrdinaria${rowCounter.index}" 
									value="${respuesta.institucionOrdinaria}"/>
								<input type="hidden" id="folioPaquete${rowCounter.index}" name="folioPaquete${rowCounter.index}" 
									value="${respuesta.folioPaquete}"/>
								<input type="hidden" id="folioPago${rowCounter.index}" name="folioPago${rowCounter.index}" 
									value="${respuesta.folioPago}" />
								<input type="checkbox" 
								name="listaDetalle[${rowCounter.index}].seleccionado" value="true" onclick="guardaTemporal('${rowCounter.index}');" onKeyPress=""
								/>
							</td>
						</tr>
					</c:forEach>
					<tr></tr>
					<tr></tr>
					<tr >
						<td></td>
						<td></td>
						<td></td>
						<th colspan="1" style="text-align:left; width:;"><label style="font-size:11px;">${registrosArchivos}</label></th>
						<td class="text_centro" style="background:white;">
							<label style="font-size:11px;">${respuesta.registrosArchivo}</label></td>
					</tr>
					<tr >
						<td></td>
						<td></td>
						<td></td>
						<th colspan="1" style="text-align:left;"><label style="font-size:11px;">${registrosEncontrados}</label></th>
						<td class="text_centro" style="background: white;">
							<label style="font-size:11px;">${respuesta.registrosEncontrados}</label></td>
					</tr>
				</table>
			</div>
		
		<c:if test="${not empty respuesta.listaDetalle}">
			<jsp:include page="cargaArchivosHistoricosDetallePagCDASPID.jsp" flush="true" />
		</c:if>
			
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:if test="${not empty respuesta.listaDetalle}">
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
									<td width="279" class="izq"><a id="idGenerarArchivon" href="javascript:;"
											><label style="font-size:11px;">${generarArchivo}</label></a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;"
											><label style="font-size:11px;">${generarArchivo}</label></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${empty respuesta.listaDetalle}">
							<td width="279" class="izq_Des"><a id="" href="javascript:;" >
								<label style="font-size:11px;">${generarArchivo}</label></a></td>
						</c:if>
						
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="der"><a id="idActualizar" href="javascript:;" onKeyPress=""
							onclick="hacerSubmit('archivosHistoricosDetalleCADSPID','monitorCargArchHistCDASPID.do');"
								><label style="font-size:11px;">${regresar}</label></a></td>
					</tr>
					<tr>
						<c:if test="${not empty respuesta.listaDetalle}">
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'ELIMINAR')}">
									<td width="279" class="izq"><a id="idEliminarSeleccion" href="javascript:;">
										<label style="font-size:11px;">${eliminarSeleccion}</label></a></td>
								</c:when>
								<c:otherwise>
									<td width="279" class="izq_Des"><a href="javascript:;">
										<label style="font-size:11px;">${eliminarSeleccion}</label></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${empty respuesta.listaDetalle}">
							<td width="279" class="izq_Des"><a href="javascript:;" >
								<label style="font-size:11px;">
								${eliminarSeleccion}</label></a></td>
						</c:if>
						<td width="6" class="odd">${espacioEnBlanco}</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>

<jsp:include page="cargaArchivosHistoricosDetalleMegCDASPID.jsp" flush="true"/>
