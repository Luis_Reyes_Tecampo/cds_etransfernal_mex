<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="generarArchHisCDASPID" />
</jsp:include>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.SPID.generaciˇnArchivoCDAHist.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.generaciˇnArchivoCDAHist.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.generaciˇnArchivoCDAHist.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloCDA.generaciˇnArchivoCDAHist.text.nombreArchivo" var="nombreArchivo"/>
<spring:message code="moduloCDA.generaciˇnArchivoCDAHist.botonLink.procesar" var="procesar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/> 
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="codError.ED00013V" var="ED00013V"/>
<spring:message code="codError.ED00014V" var="ED00014V"/>
<spring:message code="codError.ED00025V" var="ED00025V"/>
<spring:message code="codError.ED00028V" var="ED00028V"/>

	    <script type="text/javascript">
        var mensaje = new Array();
		mensaje["aplicacion"] = '${aplicacion}';
		mensaje["ED00013V"] = '${ED00013V}';
		mensaje["ED00014V"] = '${ED00014V}';
		mensaje["ED00025V"] = '${ED00025V}';
		mensaje["ED00028V"] = '${ED00028V}';
    </script>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/generarArchivoCDASPIDHist.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="fechaOperacionBancoMex" id="fechaOperacionBancoMex" value="${generarArchCDAHistSPID.fechaOperacion}"/>
			<input type="hidden" name="accion" id="accion" value=""/>
		
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tbody>
					<tr>
						<td class="text_izquierda">
							${fechaOperacion}<input id="fechaOperacion" value="" class="Campos" type="text" size="14" readonly="readonly" name="fechaOperacion"/>
							<img id="cal1" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
							${nombreArchivo} <input name="nombreArchivo" type="text" class="Campos" id="nombreArchivo" size="30" maxlength="99"/>	
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		
	</div>
	</form>

	<div>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>		
	</div>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idProcesar" href="javascript:;">${procesar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${procesar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td class="cero">${espacioEnBlanco}</td>
					</tr>
					<tr>
						<td width="279" class="cero">${espacioEnBlanco}</td>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero">${espacioEnBlanco}</td>
					</tr>
				</table>
			</div>
		</div>
		

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
		