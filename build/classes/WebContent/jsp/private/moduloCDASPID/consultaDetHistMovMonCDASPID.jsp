<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="monitorCDAHistoricoSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.tituloFuncionalidad"	var="tituloFuncionalidad" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.subtituloFuncionalidad"	var="subtituloFuncionalidad" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.referencia"	var="referencia" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.datGenerales"	var="datGenerales" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.cveRastreo"	var="cveRastreo" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.fechaOpe"	var="fechaOpe" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.hrAbonoCh"	var="hrAbonoCh" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.hrEnvCDA"	var="hrEnvCDA" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.estatusCDA"	var="estatusCDA" />
<spring:message code="moduloCDASPID.consultaDetHistMovMonCDA.text.cveSPID" var="cveSPEI" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.emisorOrdenante" var="emisorOrdenante" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.instEmisora" var="instEmisora" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.nomOrden" 	var="nomOrden" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.ctaOrdenan"	var="ctaOrdenan" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.tipoCtaOrd"	var="tipoCtaOrd" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.rfcCurp" var="rfcCurp" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.nomBenef" var="nomBenef" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.nomInstRe" var="nomInstRe" />
<spring:message	code="moduloCDA.consultaDetHistMovMonCDA.text.receptorBeneficiario" var="receptorBeneficiario" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.tipoCtaBen"	var="tipoCtaBen" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.ctaBenef" var="ctaBenef" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.montoPago" var="montoPago" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.conceptoPago" var="conceptoPago" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.text.importeIVA" var="importeIVA" />
<spring:message code="moduloCDA.consultaDetHistMovMonCDA.botonLink.regresar" var="regresar" />
<spring:message code="moduloCDA.consultaDetMovCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaDetMovCDA.text.fechaAbono" var="fechaAbono"/>

<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/consultaDetHistMovMonCDASPID.js?rand=1" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span>- ${tituloFuncionalidad}</div>

<form:form name="idForm" id="idForm" method="post">
	<input type="hidden" name="fechaOpeInicio" id="fechaOpeInicio" value="${paramFechaOpeInicio}"/>
	<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}"/>
	<input type="hidden" name="fechaOpeFin" id="fechaOpeFin" value="${paramFechaOpeFin}"/>
	<input type="hidden" name="cveSpeiOrdenanteAbono" id="cveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}"/>
	<input type="hidden" name="nombreInstEmisora" id="nombreInstEmisora" value="${paramNombreInstEmisora}"/>
	<input type="hidden" name="cveRastreo" id="cveRastreo" value="${paramCveRastreo}"/>
	<input type="hidden" name="hrAbonoIni" id="hrAbonoIni" value="${paramHrAbonoIni}"/>
	<input type="hidden" name="hrAbonoFin" id="hrAbonoFin" value="${paramHrAbonoFin}"/>
	<input type="hidden" name="hrEnvioIni" id="hrEnvioIni" value="${paramHrEnvioIni}"/>
	<input type="hidden" name="hrEnvioFin" id="hrEnvioFin" value="${paramHrEnvioFin}"/>
	<input type="hidden" name="ctaBeneficiario" id="ctaBeneficiario" value="${paramCtaBeneficiario}"/>
	<input type="hidden" name="montoPago" id="montoPago" value="${paramMontoPago}"/>
	<input type="hidden" name="tipoPago" id="tipoPago" value="${paramTipoPago}"/>
	<input type="hidden" name="estatusCDA" id="estatusCDA" value="${paramEstatusCDA}"/>
	
	<input type="hidden" name="accion" id="accion" value=""/>
	<input type="hidden" name="paginador.accion" id="accion" value=""/>
	<input type="hidden" name="paginador.pagina" id="pagina" value="${beanPaginador.pagina}"/>
    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanPaginador.paginaIni}"/>
	<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanPaginador.paginaFin}"/>

<div class="frameFormularioB">
	<div class="contentFormularioB">
		<div class="titleFormularioB">${subtituloFuncionalidad} - ${cveRastreo} ${beanConsMovDetCDA.beanMovCdaDatosGenerales.referencia} </div>
	<table>
		<caption></caption>
		<tr>
			<th colspan="5" class="text_izquierda">${datGenerales}</th>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${referencia}</td>
			<td class="text_izquierda" width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
			${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.referencia}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="background: #F1F1F1; text-align: center;">${hrAbonoCh}</td>
			<td class="text_izquierda" width="150" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
			${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.hrAbono}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${cveRastreo}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
			${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.cveRastreo}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${hrEnvCDA}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
			${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.hrEnvio}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${fechaOpe}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.fechaOpe}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${estatusCDA}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.estatusCDA}</td>
		</tr>
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${tipoPago}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.tipoPago}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${fechaAbono}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanMovCdaDatosGenerales.fechaAbono}</td>
		</tr>
	
		<tr>
			<th class="text_izquierda" colspan="5">${emisorOrdenante}</th>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${cveSPEI}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.cveSpei}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${tipoCtaOrd}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.tipoCuentaOrd}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${instEmisora}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.nomInstEmisora}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${ctaOrdenan}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.ctaOrd}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${nomOrden}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.nombreOrd}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${rfcCurp}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaOrdenante.rfcCurpOrdenante}</td>
		</tr>
	
		<tr>
			<th class="izq" colspan="5">${receptorBeneficiario}</th>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${nomInstRe}</td>
			<td  class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.nomInstRec}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${tipoCtaBen}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.tipoCuentaBened}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${nomBenef}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.nombreBened}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${ctaBenef}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.ctaBenef}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${rfcCurp}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.rfcCurpBeneficiario}</td>
			<td width="50">${espacioEnBlanco}</td>
			<td style="text-align: center; background: #F1F1F1;">${conceptoPago}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.conceptoPago}</td>
		</tr>
	
		<tr>
			<td style="text-align: center; background: #F1F1F1;">${montoPago}</td>
			<td class="text_izquierda" style="border-width: 1px; border: solid; border-color: #BFBFBF; background: #F1F1F1;">
				${beanResConsMovDetHistCDA.beanConsMovCdaBeneficiario.monto}</td>
			<td width="50">${espacioEnBlanco}</td>
		</tr>
	</table>

</div>
</div>

<br/>
<br/>

<div class="framePieContenedor">
<div class="contentPieContenedor">
<table>
	<caption></caption>
	<tr>
		<td class="izq">${espacioEnBlanco}</td>
		<td class="odd">${espacioEnBlanco}</td>
		<td class="der"><a id="idRegresar" href="javascript:;" >${regresar}</a></td>
	</tr>
	<tr>
		<td width="279" class="izq">${espacioEnBlanco}</td>
		<td width="6" class="odd">${espacioEnBlanco}</td>
		<td width="279" class="der">${espacioEnBlanco}</td>
	</tr>
</table>


</div>
</div>

</form:form>
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>