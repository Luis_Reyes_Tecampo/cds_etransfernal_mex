<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true"/>
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="muestraGenerarArchCDAxFchHistSPID" />
</jsp:include>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<spring:message code="moduloCDA.generaci�nArchivoCDAxFchHistSPID.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.generaci�nArchivoCDAxFchHist.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.generaci�nArchivoCDAxFchHist.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloCDA.generaci�nArchivoCDAxFchHist.text.nombreArchivo" var="nombreArchivo"/>
<spring:message code="moduloCDA.generaci�nArchivoCDAxFchHist.botonLink.procesar" var="procesar"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00013V" var="ED00013V"/>
<spring:message code="codError.ED00014V" var="ED00014V"/>
<spring:message code="codError.ED00025V" var="ED00025V"/>
<spring:message code="codError.ED00028V" var="ED00028V"/>

	    <script type="text/javascript">
        var mensajes = new Array();
		mensajes["aplicacion"] = '${aplicacion}';
		mensajes["ED00013V"] = '${ED00013V}';
		mensajes["ED00014V"] = '${ED00014V}';
		mensajes["ED00025V"] = '${ED00025V}';
		mensajes["ED00028V"] = '${ED00028V}';
    </script>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/generarArchivoxFchCDAHistSPID.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="fechaOperacionBancoMex" id="fechaOperacionBancoMex" value="${generarArchCDAHist.fechaOperacion}"/>
			<input type="hidden" name="accion" id="accion" value=""/>
		
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table>
				<caption></caption>
				<tbody>
					<tr>
						<td class="text_izquierda">
							${fechaOperacion}<input id="fechaOp" value="" class="Campos" type="text" size="14" readonly="readonly" name="fechaOp"/>
							<img id="calSPID" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
							</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		
	</div>
	</form>

	<div>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>		
	</div>
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR')}">
								<td class="izq"><a id="idProcesarSPID" href="javascript:;">${procesar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${procesar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<td class="cero">${espacioEnBlanco}</td>
					</tr>
					<tr>
						<td width="279" class="cero">${espacioEnBlanco}</td>
						<td width="6" class="odd">${espacioEnBlanco}</td>
						<td width="279" class="cero">${espacioEnBlanco}</td>
					</tr>
				</table>
			</div>
		</div>
		

<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');			
		</script>
	</c:if>
		