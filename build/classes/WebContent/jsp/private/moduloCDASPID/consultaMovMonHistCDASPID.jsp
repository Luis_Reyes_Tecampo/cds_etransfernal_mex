<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloCDASPID.jsp" flush="true">
	<jsp:param name="menuItem" value="moduloCDASPID" />
	<jsp:param name="menuSubitem" value="monitorCDAHistoricoSPID" />
</jsp:include>

<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.siguiente" var="siguiente"/>

<spring:message code="moduloCDA.general.text.tituloModulo" var="tituloModulo"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.subtituloFuncion" var="subtituloFuncion"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.fechaOperacionFin" var="fechaOperacionFin"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.fechaOperacionIni" var="fechaOperacionIni"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrEnvioFin" var="hrEnvioFin"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrEnvioIni" var="hrEnvioIni"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrAbonoFin" var="hrAbonoFin"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrAbonoIni" var="hrAbonoIni"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.claveSPIDOrdAbono" var="claveSPEIOrdAbono"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.descripcion" var="descripcion"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.claveRastreo" var="claveRastreo"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.refTransfer" var="refTransfer"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.ctaBeneficiario" var="ctaBeneficiario"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.montoPago" var="montoPago"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.opeContingencia" var="opeContingencia"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.estatus" var="estatus"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.botonLink.buscar" var="buscar"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.infEncontrada" var="infEncontrada"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.refere" var="refere"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.folioPaq" var="folioPaq"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.folioPag" var="folioPag"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.fechaOpe" var="fechaOpe"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.instEmisora" var="instEmisora"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.cveRastreo" var="cveRastreo"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.ctaBenef" var="ctaBenef"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.monto" var="monto"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrAbono" var="hrAbono"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.hrEnvio" var="hrEnvio"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.estatusCDA" var="estatusCDA"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.codError" var="codErrorLabel"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.botonLink.regresar" var="regresar" />
<spring:message code="moduloCDA.consultaMovMonHistCDA.botonLink.limpiar" var="limpiar"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.botonLink.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.botonLink.exportar" var="exportar"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.mensaje" var="mensaje"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.espacio" var="espacio"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.tipoPago" var="tipoPago"/>
<spring:message code="moduloCDA.consultaMovMonHistCDA.text.fechaAbono" var="fechaAbono"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.ED00023V" var="ED00023V"/>
<spring:message code="codError.ED00022V" var="ED00022V"/>
<spring:message code="codError.ED00021V" var="ED00021V"/>
<spring:message code="codError.ED00020V" var="ED00020V"/>
<spring:message code="codError.ED00019V" var="ED00019V"/>

<c:set var="searchString" value="${seguTareas}"/>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloCDASPID/consultaMovMonHistCDASPID.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<script type="text/javascript">
      var mensajes = new Array();
	mensajes["aplicacion"] = '${aplicacion}';
	mensajes["ED00019V"] = '${ED00019V}';
	mensajes["ED00020V"] = '${ED00020V}';
	mensajes["ED00021V"] = '${ED00021V}';
	mensajes["ED00022V"] = '${ED00022V}';
	mensajes["ED00023V"] = '${ED00023V}';
	mensajes["ED00027V"] = '${ED00027V}';
   </script>

	<div class="pageTitleContainer">
		<span class="pageTitle">${tituloModulo}</span> - ${tituloFuncionalidad}
	</div>

	<form name="idForm" id="idForm" method="post">
			<input type="hidden" name="paginador.accion" id="accion" value=""/>
		    <input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsMovHistCDA.beanPaginador.paginaIni}"/>
			<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsMovHistCDA.beanPaginador.pagina}"/>
			<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsMovHistCDA.beanPaginador.paginaFin}"/>
			<input type="hidden" name="fechaOperacion" id="fechaOperacion" value="${fechaOperacion}"/>
			<input type="hidden" name="referencia" id="referencia" value=""/>
			<input type="hidden" name="fechaHoy" id="fechaHoy" value="${fechaHoy}"/>
			<input type="hidden" name="paramOpeContingencia" id="paramOpeContingencia" value="${paramOpeContingencia}"/>
			<input type="hidden" name="paramFechaOperacion" id="paramFechaOperacion" value="${paramFechaOpeInicio}"/>
			<input type="hidden" name="paramFecha3Antes" id="paramFecha3Antes" value="${fecha3Antes}"/>
			<input type="hidden" name="paramFechaFin" id="paramFechaFin" value="${fechaFin}"/>
			<input type="hidden" name="fechaOpeInicio" id="fechaOpeInicio" value="${paramFechaOpeInicio}"/>
			<input type="hidden" name="fechaOpeFin" id="fechaOpeFin" value="${paramFechaOpeFin}"/>
			<input type="hidden" name="hrEnvioIni" id="hrEnvioIni" value="${paramHrEnvioIni}"/>
			<input type="hidden" name="hrEnvioFin" id="hrEnvioFin" value="${paramHrEnvioFin}"/>
			<input type="hidden" name="hrAbonoIni" id="hrAbonoIni" value="${paramHrAbonoIni}"/>
			<input type="hidden" name="hrAbonoFin" id="hrAbonoFin" value="${paramHrAbonoFin}"/>
			<input type="hidden" name="cveSpeiOrdenanteAbono" id="cveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}"/>
			<input type="hidden" name="nombreInstEmisora" id="nombreInstEmisora" value="${paramNombreInstEmisora}"/>
			<input type="hidden" name="cveRastreo" id="cveRastreo" value="${paramCveRastreo}"/>
			<input type="hidden" name="ctaBeneficiario" id="ctaBeneficiario" value="${paramCtaBeneficiario}"/>
			<input type="hidden" name="montoPago" id="montoPago" value="${paramMontoPago}"/>
			<input type="hidden" name="tipoPago" id="tipoPago" value="${paramTipoPago}"/>
			<input type="hidden" name="estatusCDA" id="estatusCDA" value="${paramEstatusCDA}"/>
			<input type="hidden" name="cveMiInstitucLink" id="cveMiInstitucLink" value=""/>
			<input type="hidden" name="fechaOpeLink" id="fechaOpeLink" value=""/>
			<input type="hidden" name="cveSpeiLink" id="cveSpeiLink" value=""/>
			<input type="hidden" name="fchCDALink" id="fchCDALink" value=""/>
			<input type="hidden" name="estatusCDALink" id="estatusCDALink" value=""/>
			<input type="hidden" name="folioPaqLink" id="folioPaqLink" value=""/>
			<input type="hidden" name="folioPagoLink" id="folioPagoLink" value=""/>


	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple">
				${subtituloFuncion}</div>
		<div class="contentBuscadorSimple">
			<table border="1">
				<caption></caption>
				<tbody>
					<tr>
						<td width="150" ><span class="red">${fechaOperacionIni}</span></td>
						<td width="150"><input class="Campos" type="text" id="paramFechaOpeInicio" value="${paramFechaOpeInicio}" size="10" readonly="readonly" name="paramFechaOpeInicio"/></td>
						<td width="160">${claveSPEIOrdAbono}</td>
						<td width="104"><input id="paramCveSpeiOrdenanteAbono" value="${paramCveSpeiOrdenanteAbono}" name="paramCveSpeiOrdenanteAbono" type="text" class="Campos"  size="20" maxlength="12"/></td>

						<td >
						</td>
					</tr>
					<tr>
						<td>
						<span class="red">${fechaOperacionFin}</span>
						</td>
						<td width="150">
						<input class="Campos" type="text" id="paramFechaOpeFin" value="${paramFechaOpeFin}" size="10" readonly="readonly" name="paramFechaOpeFin"/>
						</td>
						<td width="74">${instEmisora}</td>
						<td width="104"><input type="text" class="Campos" name="paramNombreInstEmisora" value="${paramNombreInstEmisora}" id="paramNombreInstEmisora" size="20" maxlength="40"/></td>
						<td >
						</td>
					</tr>

					<tr>
						<td>${hrAbonoIni}</td>
						<td width="191">
						<input class="Campos" type="text" id="paramHrAbonoIni" value="${paramHrAbonoIni}" size="10" readonly="readonly" name="paramHrAbonoIni"/><img id="cal3" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/></td>
						<td width="74">${claveRastreo}</td>
						<td width="104"><input name="paramCveRastreo" value="${paramCveRastreo}" type="text" class="Campos" id="paramCveRastreo" size="20"  maxlength="30"/></td>

						<td>
						</td>
					</tr>
					<tr>
						<td>
						${hrAbonoFin}
						</td>
						<td width="191">
							<input class="Campos" type="text" id="paramHrAbonoFin" value="${paramHrAbonoFin}" size="10" readonly="readonly" name="paramHrAbonoFin"/><img id="cal4" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/>
						</td>
						<td width="74">${ctaBeneficiario}</td><td width="104"><input name="paramCtaBeneficiario" value="${paramCtaBeneficiario}" type="text" class="Campos" id="paramCtaBeneficiario" size="20" maxlength="20"/></td>

						<td>
						</td>
					</tr>

					<tr>
						<td>${hrEnvioIni}</td>
						<td width="191"><input class="Campos" type="text" id="paramHrEnvioIni" value="${paramHrEnvioIni}" size="10" readonly="readonly" name="paramHrEnvioIni"/><img id="cal5" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/></td>
						<td width="74">${montoPago}</td><td width="104"><input name="paramMontoPago" value="${paramMontoPago}" type="text" class="Campos" id="paramMontoPago" size="20" maxlength="19"/></td>
						<td></td>
					</tr>

					<tr>
						<td>${hrEnvioFin}</td>
						<td width="191"><input class="Campos" type="text" id="paramHrEnvioFin" value="${paramHrEnvioFin}" size="10" readonly="readonly" name="paramHrEnvioFin"/><img id="cal6" src="${pageContext.servletContext.contextPath}/lf/default/img/calendar/calendar.png" alt="img calendar"/></td>
						<td width="74">${tipoPago}</td><td width="104"><input name="paramTipoPago" value="${paramTipoPago}" type="text" class="Campos" id="paramTipoPago" size="7" maxlength="7"/></td>
						<td></td>
						
					</tr>
					<tr>
						<td/>
						<td/>
						<td width="74">${opeContingencia}</td><td width="104"><input id="opeContingencia" type="checkbox" name="opeContingencia"/>
						</td>

						<td class="izq" width="121">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<span><a href="javascript:;" id="idBuscar">${buscar}</a></span>
							</c:when>
							<c:otherwise>
								<span class="btn_Des"><a href="javascript:;">${buscar}</a></span>
							</c:otherwise>
						</c:choose>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span class="red">${mensaje}</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
				<caption></caption>
				<tr>
					<th class="text_izquierda" width="122">${refere}</th>
					<th class="text_centro" width="223" scope="col">${fechaOpe}</th>
					<th class="text_centro" width="122" scope="col">${folioPaq}</th>
					<th class="text_centro" width="122" scope="col">${folioPag}</th>
					<th class="text_centro" width="122" scope="col">${claveSPEIOrdAbono}</th>
					<th class="text_centro" width="122" scope="col">${instEmisora}</th>
					<th class="text_centro" width="122" scope="col">${cveRastreo}</th>
					<th class="text_centro" width="122" scope="col">${ctaBeneficiario}</th>
					<th class="text_centro" width="122" scope="col">${monto}</th>
					<th class="text_centro" width="122" scope="col">${fechaAbono}</th>
					<th class="text_centro" width="122" scope="col">${hrAbono}</th>
					<th class="text_centro" width="122" scope="col">${hrEnvio}</th>
					<th class="text_centro" width="122" scope="col">${estatusCDA}</th>
					<th class="text_centro" width="122" scope="col">${codErrorLabel}</th>
					<th class="text_centro" width="122" scope="col">${tipoPago}</th>
				</tr>

				<tr>

					<Td colspan="4" class="special"></Td>
				</tr>
				<tbody>

				   <c:forEach varStatus="rowCounter" var="beanMovimientoHistCDA" items="${beanResConsMovHistCDA.listBeanMovimientoCDA}">
					<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
						<td class="text_izquierda">
							<a id="${beanMovimientoHistCDA.referencia}" href="javascript:;" onKeyPress=""
							onclick="consultaMovMonHistCDASPID.despliegaDetalle({'referencia':'${beanMovimientoHistCDA.referencia}','fechaOpe':'${beanMovimientoHistCDA.fechaOpe}' ,'cveMiInstituc':'${beanMovimientoHistCDA.cveMiInstituc}','cveSpei':'${beanMovimientoHistCDA.cveSpei}','folioPaq':'${beanMovimientoHistCDA.folioPaq}','folioPago':'${beanMovimientoHistCDA.folioPago}','fchCDA':'${beanMovimientoHistCDA.fchCDA}','estatusCDA':'${beanMovimientoHistCDA.estatusCDA}'})">${beanMovimientoHistCDA.referencia}</a></td>
						<td class="text_izquierda">${beanMovimientoHistCDA.fechaOpe}</td>
						<td class="text_centro">${beanMovimientoHistCDA.folioPaq}</td>
						<td class="text_centro">${beanMovimientoHistCDA.folioPago}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.cveSpei}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.nomInstEmisora}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.cveRastreo}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.ctaBenef}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.monto}</td>
						<td class="text_izquierda">${beanMovimientoHistCDA.fechaAbono}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.hrAbono}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.hrEnvio}</td>
						<td class="text_centro">${beanMovimientoHistCDA.estatusCDA}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.codError}</td>
						<td class="text_derecha">${beanMovimientoHistCDA.tipoPago}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			</div>
			<c:if test="${not empty beanResConsMovHistCDA.listBeanMovimientoCDA}">
				<jsp:include page="../paginador.jsp" >
  					<jsp:param name="paginaFin" value="${beanResConsMovHistCDA.beanPaginador.paginaFin}" />
  					<jsp:param name="paginaAnt" value="${beanResConsMovHistCDA.beanPaginador.paginaAnt}" />
  					<jsp:param name="servicio" value="consMovHistCDASPID.do" />
  					<jsp:param name="pagina" value="${beanResConsMovHistCDA.beanPaginador.pagina}" />
  					<jsp:param name="paginaIni" value="${beanResConsMovHistCDA.beanPaginador.paginaIni}" />
				</jsp:include>
			</c:if>

		</div>



		<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table>
					<caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td class="izq"><a href="javascript:;" id="idExportar">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>

						<td class="odd">${espacio}</td>
						
							<td class="der" width="279"><a href="javascript:;" id="idRegresar">${regresar}</a></td>
						
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR')}">
								<td class="izq" width="279"><a href="javascript:;" id="idExportarTodo">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des" width="279"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd" width="6">${espacioEnBlanco}</td>
						<td class="der" width="279"><a id="idLimpiar" href="javascript:;">${limpiar}</a></td>
					</tr>
				</table>
			</div>
		</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${tipoError}('${descError}',
		   	   '${aplicacion}',
		   	   '${codError}',
		   	   '');
	</script>
</c:if>