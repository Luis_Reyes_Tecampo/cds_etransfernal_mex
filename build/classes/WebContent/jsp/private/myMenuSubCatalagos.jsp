<%-- 
  * ------------------------------------------------------------------------------------------
  * Isban Mexico
  * myMenuSubCatalagos.jsp
  * Descripcion: Archivo de definicion de la pantalla "Catalogo Intermediario - Vigente/Historico".
  *
  * Version  Date        By                                         Description
  * -------  ----------  ---------------------------------          ------------------------------------------
  * 1.0      24/12/2019  [1565817: Alberto Olvera Juarez]           Creacion de JSP Pantalla de Catalogo Intermediario
  * ------------------------------------------------------------------------------------------
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="mx.isban.agave.configuracion.Configuracion"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"  %>

<script
	src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/lf/default/js/menu/dynamicMenu.js"
	type="text/javascript"></script>
<link
	href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css"
	rel="stylesheet" type="text/css" />

<spring:message code="moduloCatalogo.myMenu.text.moduloCDA"	var="moduloCDA" />
<spring:message code="moduloCatalogo.myMenu.text.moduloPOACOA"	var="moduloPOACOA" />
<spring:message code="moduloCatalogo.myMenu.text.catIntsFinancieras" var="catIntsFinancieras" />
<spring:message	code="moduloCapturasManuales.myMenu.text.capturasManuales"	var="capturasManuales" />
<%-- JBR - Observaciones Banxico[08122016]--%>
<spring:message code="moduloCatalogo.myMenu.text.catIntFinancieros"	var="catIntFinancieros" />
<%-- JBR - Observaciones Banxico[08122016]--%>
<spring:message code="moduloCatalogo.myMenu.text.parametrosPOACOA"	var="parametrosPOACOA" />
<spring:message code="moduloCatalogo.myMenu.text.horaEnvioSPEI"	var="horaEnvioSPEI" />
<spring:message code="moduloCatalogo.myMenu.text.moduloCatalogos"	var="moduloCatalogos" />
<spring:message code="moduloCatalogo.myMenu.text.catIntFinancierosVig"	var="vigente" />
<spring:message code="moduloCatalogo.myMenu.text.catIntFinancierosHis"	var="historico" />
<spring:message code="moduloCatalogo.myMenu.text.principal"	var="principal" />
<spring:message code="moduloPOACOA.myMenu.text.contingCanales"	var="catCanales" />
<spring:message code="moduloPOACOA.myMenu.text.catParticipantesSPID"	var="catParticipantesSPID" />
<spring:message	code="moduloCatalogoParametros.myMenu.title.catalogoParametros"	var="catalogoParametros" />
<spring:message code="spid.menuSPID.txt.txtModuloSPID"	var="txtModuloSPID" />
<spring:message code="pagoInteres.myMenu.text.moduloPagoInteres"	var="txtModuloPagoInteres" />
<spring:message code="moduloCDA.myMenu.text.moduloCDASPID"	var="moduloCDASPID" />
<spring:message code="moduloCatalogo.myMenu.text.moduloSPEI"	var="moduloSPEI" />
<spring:message code="SPEICero.formulario.title" var="moduloSPEICero" />
<spring:message code="catalogos.menuCatalogos.txt.txtMantoMedios" var="MantoMedios" />
<spring:message code="catalogos.menuCatalogos.txt.txtMantoCertificados"	var="MantoCertificados" />
<spring:message code="catalogos.mttomedioentrega.txt.txtMantoEntrega"	var="MantoEntrega" />
<spring:message code="catalogos.menuCatalogos.txt.txtAdmonSaldo" var="adminSaldos" />
<spring:message code="menu.moduloContingencia.text.titulo" var="moduloContingencia" />
<spring:message code="catalogos.afectacionBloques.titulo" var="afectacionBloques" />
<spring:message code="catalogos.notificador.titulo" var="notificador" />
<spring:message code="catalogos.notificacionLiquidezIn.title" var="lblNotificacionLiq" />
<spring:message code="catalogos.rfc.title" var="lblCatRFC" />
<spring:message code="catalogos.nombresFideico.title" var="lblCatNombresFideico" />
<spring:message code="catalogos.cuentasFideico.title" var="lblCatCuentasFideico" />
<spring:message code="catalogos.excepCtasFideicomiso.titulo" var="lblExcepCuentasFideicomiso" />
<spring:message code="catalogos.excepNomBeneficiarios.titulo" var="lblExcepNomBenef" />
<spring:message code="catalogos.cuentasFiduciario.title" var="lblCatFiduciario" />
<spring:message code="catalogos.notificacionLiquidezIn.title" var="liquidez" />

<c:set var="searchString" value="${seguServicios}" />

<c:set var="menuItem" value='<%=request.getAttribute("menuItem")%>' />
<c:set var="menuSubitem"
	value='<%=request.getAttribute("menuSubitem")%>' />

<body
	onload="initialize('${param.menuItem}', '${param.menuSubitem}'); enabledMenuItems('${LyFBean.idsMenuPerfil}', '${LyFBean.tipoMenu}', '${LyFBean.tipoIdsMenu}'); estableceAyuda('${param.helpPage}')">
	<div id="top04">
		<c:if test="true">
			<div class="frameMenuContainer">
				<ul id="mainMenu">
					<li id="principal" class="startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">
							<span>${principal}</span>
					</a></li>
					<li id="admonSaldo" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/admonSaldo/admonSaldoInit.do">
							<span>${adminSaldos}</span>
					</a></li>
					<li id="capturasManuales" class="withSubMenus startMenuGroup">
						<a
						href="${pageContext.servletContext.contextPath}/capturasManuales/capturasManualesInit.do">
							<span>${capturasManuales}</span>
					</a>
					</li>
					<li id="catalogos" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/principal/moduloCDAInit.do">
							<span>${moduloCatalogos}</span>
					</a>
						<ul>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'parametrosPOACOA.do')}">
									<li id="parametrosPOACOA"><a
										href="${pageContext.servletContext.contextPath}/catalogos/parametrosPOACOA.do">&gt;
											<span class="subMenuText">${parametrosPOACOA}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="parametrosPOACOA"><a
										 href="javascript:; target">&gt; <span class="subMenuText">${parametrosPOACOA}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'muestraCatInstFinancieras.do')}">
									<li id="muestraCatInstFinancieras"><a
										href="${pageContext.servletContext.contextPath}/catalogos/muestraCatInstFinancieras.do">&gt;
											<span class="subMenuText">${catIntsFinancieras}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="muestraCatInstFinancieras"><a
										href="javascript:; target">&gt; <span class="subMenuText">${catIntsFinancieras}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'muestraCatHorariosSPEI.do')}">
									<li id="muestraCatHorariosSPEI"><a
										href="${pageContext.servletContext.contextPath}/moduloCDA/muestraCatHorariosSPEI.do">&gt;
											<span class="subMenuText">${horaEnvioSPEI}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="muestraCatInstFinancieras"><a
										href="javascript:; target">&gt; <span class="subMenuText">${horaEnvioSPEI}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<li id="catIntFinancieros"><a
								href="${pageContext.servletContext.contextPath}/catalogos/catalogosInit.do">&gt;
									<span class="subMenuText">${catIntFinancieros}</span>
							</a>
								<ul>
								</ul></li>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'muestraCatIntFinancieros.do')}">
									<li id="muestraCatIntFinancieros"><a
										href="${pageContext.servletContext.contextPath}/catalogos/muestraCatIntFinancieros.do">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;
											<span class="subMenuText">${vigente}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="muestraCatIntFinancieros"><a
										href="javascript:; target">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt; <span class="subMenuText">${vigente}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'muestraCatIntFinancierosHis.do')}">
									<li id="muestraCatIntFinancierosHis"><a
										href="${pageContext.servletContext.contextPath}/catalogos/muestraCatIntFinancierosHis.do">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;
											<span class="subMenuText">${historico}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="muestraCatIntFinancierosHis"><a
										href="javascript:; target">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt; <span class="subMenuText">${historico}</span>
									</a></li>
								</c:otherwise>
							</c:choose>

							<%-- JBR - Observaciones Banxico[08122016]--%>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'catalogoParticipantesSPID.do')}">
									<li id="catParticipantesSPID"><a
										href="${pageContext.servletContext.contextPath}/catalogos/catalogoParticipantesSPID.do">&gt;
											<span class="subMenuText">${catParticipantesSPID}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="catParticipantesSPID"><a
										href="javascript:; target">&gt; <span class="subMenuText">${catParticipantesSPID}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'muestraMantenimientoParametros.do')}">
									<li id="catalogoParametros"><a
										href="${pageContext.servletContext.contextPath}/catalogos/muestraMantenimientoParametros.do">&gt;
											<span class="subMenuText">${catalogoParametros}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="catalogoParametros"><a 
											href="javascript:; target">&gt; <span class="subMenuText">${catalogoParametros}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'mantenimientoMediosAutorizados.do')}">
									<li id="mantenimientoMediosAutorizados"><a
										href="${pageContext.servletContext.contextPath}/catalogos/mantenimientoMediosAutorizados.do">&gt;
											<span class="subMenuText">${MantoMedios}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="mantenimientoMediosAutorizados"><a
										href="javascript:; target">&gt; <span class="subMenuText">${MantoMedios}</span>
									</a></li>
								</c:otherwise>
							</c:choose>

							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaCertificadosCifrado.do')}">
									<li id="certifCifrado"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaCertificadosCifrado.do">&gt;
											<span class="subMenuText">${MantoCertificados}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="certifCifrado"><a 
											href="javascript:; target">&gt; <span class="subMenuText">${MantoCertificados}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaMttoMediosEntrega.do')}">
									<li id="mantenimientoMediosEntrega"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaMttoMediosEntrega.do">&gt;
											<span class="subMenuText">${MantoEntrega}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="mantenimientoMediosEntrega"><a
										href="javascript:; target">&gt; <span class="subMenuText">${MantoEntrega}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaNotificador.do')}">
									<li id="notificador"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaNotificador.do">&gt;
											<span class="subMenuText">${notificador}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="notificador"><a href="javascript:;">&gt; <span
											class="subMenuText">${notificador}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaAfectacionBloques.do')}">
									<li id="afectacionBloques"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaAfectacionBloques.do">&gt;
											<span class="subMenuText">${afectacionBloques}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="afectacionBloques"><a 
											href="javascript:;">&gt; <span class="subMenuText">${afectacionBloques}</span>
									</a></li>
								</c:otherwise>
							</c:choose>

							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaFiltros.do')}">
									<li id="ctaLiquidezIntradia"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaFiltros.do">&gt;
											<span class="subMenuText">${liquidez}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="ctaLiquidezIntradia"><a 
											href="javascript:; target">&gt; <span class="subMenuText">${liquidez}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when	test="${fn:containsIgnoreCase(searchString, 'excepCuentasFideicomiso.do')}">
									<li id="excepCuentasFideicomiso"><a href="${pageContext.servletContext.contextPath}/catalogos/excepCuentasFideicomiso.do">&gt;
											<span class="subMenuText">${lblExcepCuentasFideicomiso}</span>
									</a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="excepCuentasFideicomiso"><a 
											href="javascript:; target">&gt; <span class="subMenuText">${lblExcepCuentasFideicomiso}</span></a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'excepNomBeneficiarios.do')}">
									<li id="excepNomBeneficiarios"><a
										href="${pageContext.servletContext.contextPath}/catalogos/excepNomBeneficiarios.do">&gt;
											<span class="subMenuText">${lblExcepNomBenef}</span></a>
									</li>
								</c:when>
								<c:otherwise><li id="excepNomBeneficiarios"><a 
								      href="javascript:; target">&gt; <span class="subMenuText">${lblExcepNomBenef}</span>
									</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString, 'consultaExcepcionRFC.do')}">
									<li id="excepRFC"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaExcepcionRFC.do">&gt;
											<span class="subMenuText">${lblCatRFC}</span></a>
									</li>
								</c:when>
								<c:otherwise>
									<li id="excepRFC"><a 
										href="javascript:;">&gt; 
											<span class="subMenuText">${lblCatRFC}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaNombresFideicomiso.do')}">
									<li id="nombresFideico"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaNombresFideicomiso.do">&gt;
											<span class="subMenuText">${lblCatNombresFideico}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="nombresFideico"><a href="javascript:;">&gt; <span
											class="subMenuText">${lblCatNombresFideico}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaCuentasFideicomisoSPID.do')}">
									<li id="cuentasFideico"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaCuentasFideicomisoSPID.do">&gt;
											<span class="subMenuText">${lblCatCuentasFideico}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="cuentasFideico"><a href="javascript:;">&gt; <span
											class="subMenuText">${lblCatCuentasFideico}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(searchString, 'consultaCuentasFiduciario.do')}">
									<li id="cuentasFiduciario"><a
										href="${pageContext.servletContext.contextPath}/catalogos/consultaCuentasFiduciario.do">&gt;
											<span class="subMenuText">${lblCatFiduciario}</span>
									</a></li>
								</c:when>
								<c:otherwise>
									<li id="cuentasFiduciario"><a 
											href="javascript:; target">&gt; <span class="subMenuText">${lblCatFiduciario}</span>
									</a></li>
								</c:otherwise>
							</c:choose>
						</ul></li>
					<li id="moduloCDA" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloCDA/moduloCDAInit.do">
							<span>${moduloCDA}</span>
					</a></li>
					<li id="moduloCDASPID" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloCDASPID/moduloCDASPIDInit.do">
							<span>${moduloCDASPID}</span>
					</a>
					<li id="moduloPOACOA" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloPOACOA/moduloPOACOAInit.do">
							<span>${moduloPOACOA}</span>
					</a></li>
					<li id="moduloSPID" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloSPID/moduloSPIDInit.do">
							<span>${txtModuloSPID}</span>
					</a></li>
					<li id="moduloSPEI" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloSPEI/moduloSPEIInit.do">
							<span>Modulo SPEI</span>
					</a></li>
					<li id="moduloSPEICero" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/moduloSPEICero/moduloSPEICero.do">
							<span>${moduloSPEICero}</span>
					</a></li>
					<li id="pagoInteres" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/pagoInteres/moduloPagoInteresInit.do">
							<span>${txtModuloPagoInteres}</span>
					</a></li>
					<li id="contingencia" class="withSubMenus startMenuGroup"><a
						href="${pageContext.servletContext.contextPath}/contingencia/contingenciaInit.do">
							<span>${moduloContingencia}</span>
					</a></li>
				</ul>
				<div id="menuFooter">
					<div></div>
				</div>
			</div>
		</c:if>
	</div>
	<div id="content_container">