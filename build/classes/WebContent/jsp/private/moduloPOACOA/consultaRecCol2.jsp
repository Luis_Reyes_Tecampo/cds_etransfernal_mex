<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<th scope="col" data-id="fechOperacion"
	class="text_centro sortField widthx122" scope="col">${txtFchOperacion}
	<c:if test="${sortField == 'fechOperacion'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th scope="col" data-id="fechCaptura"
	class="text_centro sortField widthx122" scope="col">${txtHora}<c:if
		test="${sortField == 'fechCaptura'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th scope="col" data-id="cveInsOrd"
	class="text_izquierda sortField widthx122">${txtIntsOrd}<c:if
		test="${sortField == 'cveInsOrd'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>