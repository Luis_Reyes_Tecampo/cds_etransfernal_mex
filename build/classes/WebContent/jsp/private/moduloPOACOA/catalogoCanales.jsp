<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- 
  - Autor: Indra
  - Aplicaci�n: SoftToken
  - M�dulo: Solicitud
  - Fecha: 14/10/2013
  - Descripci�n: Pantalla para Admin. Licencias Softtoken
  --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
	<jsp:param name="menuItem" value="contingencia" />
	<jsp:param name="menuSubitem" value="catCanales" />
</jsp:include>

<script
	src="${pageContext.servletContext.contextPath}/js/private/catalogoCanales.js"
	type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.eliminar" var="eliminar"/>
<spring:message code="general.alta" var="alta"/>
<spring:message code="menu.canalesContingentes.text.titulo"       var="titulo"/>
<spring:message code="moduloPOACOA.catCanales.text.canal"       var="canal"/>
<spring:message code="moduloPOACOA.catCanales.text.desc"       var="descripcion"/>
<spring:message code="moduloPOACOA.catCanales.text.actDes"       var="activarDes"/>
<spring:message code="moduloPOACOA.catCanales.botonLink.buscar"       var="buscar"/>
<spring:message code="menu.moduloContingencia.text.titulo" var="tituloModulo"/>

<spring:message code="moduloPOACOA.catCanales.text.medioEntrega"       var="medioEntrega"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span> - ${titulo}
</div>

<div class="frameBuscadorSimple" >
	<div class="titleBuscadorSimple">
	</div>
	<form name="formAdmCanales" id="formAdmCanales" method="post">
		<input type="hidden" name="paginador.paginaIni" id="paginaIni" value="${beanResConsultaCanales.beanPaginador.paginaIni}" />
		<input type="hidden" name="paginador.pagina" id="pagina" value="${beanResConsultaCanales.beanPaginador.pagina}" />
		<input type="hidden" name="paginador.paginaFin" id="paginaFin" value="${beanResConsultaCanales.beanPaginador.paginaFin}" />
		<input type="hidden" name="paginador.accion" id="accion" value="" />
	
		<input id="chBoxActDesHide" name="chBoxActDesHide" type="hidden" />	
		<input id="hdnCheckSeleccionados" name="hdnCheckSeleccionados" type="hidden" value="" />	
		
		<div class="contentBuscadorSimple" style="width:99.9%">
			<table width="100%" border="0">
				<tr>
					<td width="50%" class="text_derecha">${medioEntrega} 
					<select id="cmbCanal" name="cmbCanal" class="CamposCompletar" >
						<option  value=""></option>
						<c:forEach var="canalBean" items="${listaCanales}">
							<option  value="${canalBean.idCanal}">${canalBean.idCanal} - ${canalBean.nombreCanal}</option>
						</c:forEach>
					</select>
					</td>
					<td width="50%" class="text_izquierda">
						<span><a id="idBuscar" href="javascript:buscarCanal();">${buscar}</a></span>
					</td>									
				</tr>
			</table>
		</div>
		<div class="frameTablaEstandar">
			<div class="contentTablaEstandar" style="overflow-x: auto; overflow-y: hidden;">
				<table>
					<thead>
						<tr>
							<th class="text_centro">${canal}</th>
							<th class="text_centro">${descripcion}</th>
							<th class="text_centro">${activarDes}</th>
						</tr>
					</thead>
					<tr>
						<td colspan="8" class="special"></td>
					</tr>
					<tbody>
						<c:if test="${beanResConsultaCanales.listaBeanResCanal!=null}">	
							<c:forEach var="canalItem" items="${beanResConsultaCanales.listaBeanResCanal}" varStatus="rowCounter">
								<c:choose>
									<c:when test="${canalItem.banderaRenglon}">								
										<tr 
										class="odd1" onmouseover="this.style.background='#B0E2E2'"
										onmouseout="this.style.background='#ffffff'">
											<td class="text_izquierda">
											<input name="chkCierre" type="checkbox" id="chkCierre${rowCounter.count}" value="${canalItem.idCanal}"/>
											${canalItem.idCanal}</td>
											<td class="text_izquierda">${canalItem.nombreCanal}</td>
											<td class="text_centro">
											<c:choose>
												<c:when test="${canalItem.estatusCanal}">
													<input type="checkbox" value="1" checked="checked"
													class="CamposCompletar Campos" name="${canalItem.idCanal}" 
													id="${canalItem.idCanal}" onchange="javascript:actualizaEstatusCanal(this);"/>
												</c:when>
												<c:otherwise>
													<input type="checkbox" value="1" 
													class="CamposCompletar Campos" name="${canalItem.idCanal}" 
													id="${canalItem.idCanal}" onchange="javascript:actualizaEstatusCanal(this);"/>
												</c:otherwise>
											</c:choose>
											</td>								
										</tr>
									</c:when>
									<c:otherwise>
										<tr
										class="odd2" onmouseover="this.style.background='#B0E2E2'"
										onmouseout="this.style.background='#f4f4f4'">
											<td class="text_izquierda">
											<input name="chkCierre" type="checkbox" id="chkCierre${rowCounter.count}" value="${canalItem.idCanal}"/>
											${canalItem.idCanal}</td>
											<td class="text_izquierda">${canalItem.nombreCanal}</td>
											<td class="text_centro">
											<c:choose>
												<c:when test="${canalItem.estatusCanal}">
													<input type="checkbox" value="1" checked="checked"
													class="CamposCompletar Campos" name="${canalItem.idCanal}" 
													id="${canalItem.idCanal}" onclick="javascript:actualizaEstatusCanal(this);"/>
												</c:when>
												<c:otherwise>
													<input type="checkbox" value="1" 
													class="CamposCompletar Campos" name="${canalItem.idCanal}" 
													id="${canalItem.idCanal}" onclick="javascript:actualizaEstatusCanal(this);"/>
												</c:otherwise>
											</c:choose>
											</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>			
		<c:if test="${not empty beanResConsultaCanales.listaBeanResCanal}">
			<div class="paginador">
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaIni == beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaIni != beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formAdmCanales','paginarCanales.do','INI');">&lt;&lt;${inicio}</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaAnt!='0' && beanResConsultaCanales.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('formAdmCanales','paginarCanales.do','ANT');">&lt;${anterior}</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaAnt=='0' || beanResConsultaCanales.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
				<label id="txtPagina">${beanResConsultaCanales.beanPaginador.pagina} - ${beanResConsultaCanales.beanPaginador.paginaFin}</label>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaFin != beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formAdmCanales','paginarCanales.do','SIG');">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaFin == beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaFin != beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('formAdmCanales','paginarCanales.do','FIN');">${fin}&gt;&gt;</a></c:if>
				<c:if test="${beanResConsultaCanales.beanPaginador.paginaFin == beanResConsultaCanales.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
			</div>
			</c:if>
		</div>
		
	</form>
	<div class="framePieContenedor">
		<div class="contentPieContenedor" style="width:99.9%">
			<table style="width:100%">
				<tr>
					<td width="50%" class="izq">
						<span><a href="${pageContext.servletContext.contextPath}/moduloPOACOA/altaCanalesInit.do" id="btnAdd">${alta}</a></span>
					</td>
					<td class="odd" />
					<c:choose>
						<c:when test="${esListaVacia}">	
							<td width="50%" class="der_Des">
							<span><a href="javascript:;" id="btnMod">${eliminar}</a></span>
							</td>						
						</c:when>
						<c:otherwise>
							<td width="50%" class="der">
							<span><a href="javascript:eliminarCanales();" id="btnMod">${eliminar}</a></span>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
		</div>
	</div>
</div>
<jsp:include page="../myFooter.jsp" flush="true" />
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>