<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${not empty beanResConsultaRecepciones.listaConsultaRecepciones}">
	<div class="paginador">
		<div style="text-align: left; float:left; margin-left: 5px;">
			<label>Mostrando del ${regIni} al ${regFin} de un total de ${beanResConsultaRecepciones.totalReg} registros | Importe página: ${importePagina}</label>
		</div>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaIni == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaIni != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepcionesConting.do','INI');">&lt;&lt;${inicio}</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaAnt!='0' && beanResConsultaRecepciones.beanPaginador.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepcionesConting.do','ANT');">&lt;${anterior}</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaAnt=='0' || beanResConsultaRecepciones.beanPaginador.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
		<label id="txtPagina">${e:forHtmlAttribute(beanResConsultaRecepciones.beanPaginador.pagina)} - ${e:forHtmlAttribute(beanResConsultaRecepciones.beanPaginador.paginaFin)}</label>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepcionesConting.do','SIG');">${siguiente}&gt;</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin != beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;" onclick="nextPage('idForm','consultaRecepcionesConting.do','FIN');">${fin}&gt;&gt;</a></c:if>
		<c:if test="${beanResConsultaRecepciones.beanPaginador.paginaFin == beanResConsultaRecepciones.beanPaginador.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
	</div>
	<label>Importe Total: ${e:forHtmlAttribute(beanResConsultaRecepciones.importeTotal)}</label>
	<input type="hidden" name="importeTotal" id="importeTotal" value="${e:forHtmlAttribute(beanResConsultaRecepciones.importeTotal)}" />
</c:if>