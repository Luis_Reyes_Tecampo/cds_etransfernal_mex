<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>

<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table><caption></caption>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'AGREGAR') and not empty beanResOrdenReparacion.listaOrdenesReparacion}">
									<td  class="izq widthx279"><a id="idEnviar" href="javascript:;">${enviar}</a></td>
								</c:when>
								<c:otherwise>
									<td  class="izq_Des widthx279"><a href="javascript:;" >${enviar}</a></td>
								</c:otherwise>
							</c:choose>
							<td class="odd"></td>
						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td  class="der widthx279"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td  class="der_Des widthx279"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResOrdenReparacion.listaOrdenesReparacion}">
								<td class="izq widthx279"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
							<td  class="odd widthx6"></td>
							<td  class="der widthx279"><a id="idRegresar" href="javascript:;">${regresar}</a></td>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResOrdenReparacion.listaOrdenesReparacion}">
								<td class="izq widthx279"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
									<td  class="izq_Des widthx279"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
							<td  class="odd widthx6">${espacioEnBlanco}</td>
							<td class="cero widthx279">${espacioEnBlanco}</td>
						</tr> 
					</table>
				</div>
			</div>