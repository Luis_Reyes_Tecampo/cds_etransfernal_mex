<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="moduloSPID.avisosTraspasos.text.hora" var="hora"/>
<spring:message code="moduloSPID.avisosTraspasos.text.folio" var="folio"/>
<spring:message code="moduloSPID.avisosTraspasos.text.tipo" var="tipo"/>
<spring:message code="moduloSPID.avisosTraspasos.text.importe" var="importe"/>
<spring:message code="moduloSPID.avisosTraspasos.text.referencia" var="referencia"/>
<spring:message code="moduloSPID.avisosTraspasos.text.clave" var="clave"/>
<spring:message code="moduloSPID.avisosTraspasos.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.regresar" var="regresar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.avisosTraspasos.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPID"             var="txtTraspasoSIACSPID"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPEI"             var="txtTraspasoSIACSPEI"/>
<spring:message code="moduloSPID.avisosTraspasos.text.tituloFuncionalidad" var="avisosTitulo"/>
 
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>

<script type="text/javascript">var mensajes = new Array();mensajes["aplicacion"] = '${aplicacion}';mensajes["OK00002V"] = '${OK00002V}';mensajes["ED00019V"] = '${ED00019V}';mensajes["ED00020V"] = '${ED00020V}';mensajes["ED00021V"] = '${ED00021V}';mensajes["ED00022V"] = '${ED00022V}';mensajes["ED00023V"] = '${ED00023V}';mensajes["ED00027V"] = '${ED00027V}';</script>
<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
<input type="hidden" value="${txtTraspasoSIACSPID}" id="txtTraspasoSIACSPID" />
<input type="hidden" value="${txtTraspasoSIACSPEI}" id="txtTraspasoSIACSPEI" />

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/avisoTraspasosPC.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle">  </span> - <span id="monitor">  </span>
	<span id="espaciosBlanco"></span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResAvisoTraspasos.beanPaginador.paginaIni)}"/>
	<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResAvisoTraspasos.beanPaginador.pagina)}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResAvisoTraspasos.beanPaginador.paginaFin)}"/>
	<input type="hidden" name="totalReg" id="totalReg" value="${e:forHtmlAttribute(beanResAvisoTraspasos.totalReg)}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${e:forHtmlAttribute(sortField)}">
	<input type="hidden" name="sortType" id="sortType" value="${e:forHtmlAttribute(sortType)}">
	
		<input type="hidden" id="modulo" name="modulo" value="${e:forHtmlAttribute(beanModulo.modulo)}"/>
        <input type="hidden" id="tipo" name="tipo" value="${e:forHtmlAttribute(beanModulo.tipo)}"/>
		<input type="hidden" id="path" name="path" value="${e:forHtmlAttribute(beanModulo.path)}"/>
		<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
	<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
				<caption></caption>
					<tr>
						<%@ include file="avisosTrasColum1.jsp" %>
						<%@ include file="avisosTrasCol2.jsp" %>
					</tr>
					<tbody>
						<c:forEach var="dato" items="${beanResAvisoTraspasos.listaConAviso}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td>${dato.hora}
								<input type="hidden" name="listaConAviso[${rowCounter.index}].hora" id="hora" value="${dato.hora}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].folio" id="folio" value="${dato.folio}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].typTrasp" id="typTrasp" value="${dato.typTrasp}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].typDsc" id="typDsc" value="${dato.typDsc}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].importe" id="importe" value="${dato.importe}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].sIAC" id="sIAC" value="${dato.SIAC}" />
								<input type="hidden" name="listaConAviso[${rowCounter.index}].sIDV" id="sIDV" value="${dato.SIDV}" /> 
								<input type="hidden" name="totalReg" id="totalReg" value="${beanResAvisoTraspasos.totalReg}" />
								</td>
								<td>${dato.folio}</td>
								<td>${dato.typTrasp}</td>
								<td>${dato.typDsc}</td>
								<td>${dato.importe}</td>
								<td>${dato.SIAC}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<%@ include file="avisosTrasNav.jsp" %>
		
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table><caption></caption>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResAvisoTraspasos.listaConAviso}">
								<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td  class="der widthx279"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="der_Des widthx279"><a href="javascript:;" >${actualizar}</a></td>
							</c:otherwise>
						 </c:choose>					
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResAvisoTraspasos.listaConAviso}">
								<td  class="izq widthx279"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td class="odd widthx6">${espacioEnBlanco}</td>
						<td  class="der widthx279"><a id="idRegresarMain" href="javascript:;">${Regresar}</a></td>
					</tr>
				</table>
				</div>
			</div>
		</div>
	</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');cargaTitulo('${avisosTitulo}',true);acomodaFecha();
	</script>
</c:if>