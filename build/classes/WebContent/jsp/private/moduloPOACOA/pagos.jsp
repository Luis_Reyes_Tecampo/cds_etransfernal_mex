<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>

<spring:message code="moduloSPID.listadoPagos.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.listadoPagos.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.listadoPagos.text.refe" var="refe"/>
<spring:message code="moduloSPID.listadoPagos.text.cola" var="cola"/>
<spring:message code="moduloSPID.listadoPagos.text.medioEnt" var="medioEnt"/>
<spring:message code="moduloSPID.listadoPagos.text.fchCap" var="fchCap"/>
<spring:message code="moduloSPID.listadoPagos.text.claveTran" var="claveTran"/>
<spring:message code="moduloSPID.listadoPagos.text.macan" var="macan"/>
<spring:message code="moduloSPID.listadoPagos.text.ord" var="ord"/>
<spring:message code="moduloSPID.listadoPagos.text.rec" var="rec"/>
<spring:message code="moduloSPID.listadoPagos.text.folioPaqPag" var="folioPaqPag"/>
<spring:message code="moduloSPID.listadoPagos.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.listadoPagos.text.importeAbono" var="importeAbono"/>
<spring:message code="moduloSPID.listadoPagos.text.est" var="est"/>
<spring:message code="moduloSPID.listadoPagos.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.listadoPagos.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.listadoPagos.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.listadoPagos.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>

<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasPorDev"     var="txtOrdRecibidasPorDev"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPIDSIAC"             var="txtTraspasoSPIDSIAC"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPEISIAC"             var="txtTraspasoSPEISIAC"/>


<spring:message code="general.verdetalle" var="detalle"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.OK00000V" var="OK00000V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>
<spring:message code="codError.OK00001V" var="OK00001V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>


<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00011V":'${ED00011V}',"OK00002V":'${OK00002V}',"OK00000V":'${OK00000V}',"EC00011B":'${EC00011B}',"OK00001V":'${OK00001V}'};var tipoOrden = '${tipoOrden}';</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/listadoPagos.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/consultaPagos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle">  </span> - <span id="monitor">${titulo}</span>
	<span id="espaciosBlanco"></span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>

<form name="idForm" id="idForm" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResListadoPagos.beanPaginador.paginaIni)}"/>
	<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResListadoPagos.beanPaginador.pagina)}"/>
	<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResListadoPagos.beanPaginador.paginaFin)}"/>
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="field" id="field" value="${e:forHtmlAttribute(field)}">
	<input type="hidden" name="fieldValue" id="fieldValue" value="${e:forHtmlAttribute(fieldValue)}">
	<input type="hidden" id="modulo" name="modulo" value="${e:forHtmlAttribute(beanModulo.modulo)}"/>
    <input type="hidden" id="tipo" name="tipo" value="${e:forHtmlAttribute(beanModulo.tipo)}"/>
	<input type="hidden" id="path" name="path" value="${e:forHtmlAttribute(beanModulo.path)}"/>
	<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
	<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
	<input type="hidden" name="ref" id="ref" value="">
	<input type="hidden" name="tipoRef" id="tipoRef" value="${e:forHtmlAttribute(refModel)}"/>
		
	<div class="frameTablaVariasColumnas"> 
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table id="tablaDatosListado" name="tablaDatosListado">
				<caption></caption>
					<tr>
						<th class="text_centro filField" scope="col">${refe}<br/><label for="refeInput"></label><input name="refeInput" id="refeInput" type="text" data-field="referencia" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${cola}<br/><label for="colaInput"></label><input name="colaInput" id="colaInput" type="text" data-field="cola" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${claveTran}<br/><label for="claveTranInput"></label><input name="claveTranInput" id="claveTranInput" type="text" data-field="cveTran" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${macan}<br/><label for="macanInput"></label><input name="macanInput" id="macanInput" type="text" data-field="mecan" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${medioEnt}</th>
						<th class="text_centro filField" scope="col">${fchCap}</th>
						<th class="text_centro filField" scope="col">${ord}<br/><label for="ordInput"></label><input name="ordInput" id="ordInput" type="text" data-field="ord" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${rec}<br/><label for="recInput"></label><input name="recInput" id="recInput" type="text" data-field="rec" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${importeAbono}<br/><label for="importeAbonoInput"></label><input name="importeAbonoInput" id="importeAbonoInput" type="text" data-field="imp" class="filField" ></input></th>
						<th class="text_centro filField" scope="col">${est}</th>
					</tr>
					<tbody>				
						<c:forEach var="pago" items="${beanResListadoPagos.listadoPagos}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td>${pago.referencia}
								
								<input type="hidden" name="listadoPagos[${rowCounter.index}].referencia" id="referencia" value="${pago.referencia}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveCola" id="cveCola" value="${pago.cveCola}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveTran" id="cveTran" value="${pago.cveTran}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveMecanismo" id="cveMecanismo" value="${pago.cveMecanismo}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveMedioEnt" id="cveMedioEnt" value="${pago.cveMedioEnt}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].fechaCaptura" id="fechaCaptura" value="${pago.fechaCaptura}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveIntermeOrd" id="cveIntermeOrd" value="${pago.cveIntermeOrd}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].cveIntermeRec" id="cveIntermeRec" value="${pago.cveIntermeRec}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].importeAbono" id="importeAbono" value="${pago.importeAbono}" />
								<input type="hidden" name="listadoPagos[${rowCounter.index}].estatus" id="estatus" value="${pago.estatus}" />
								<input type="hidden" name="totalReg" id="totalReg" value="${beanResListadoPagos.totalReg}" />
								</td>
								<td>${pago.cveCola}</td>
								<td>${pago.cveTran}</td>
								<td>${pago.cveMecanismo}</td>
								<td>${pago.cveMedioEnt}</td>
								<td>${pago.fechaCaptura}</td>
								<td>${pago.cveIntermeOrd}</td>
								<td>${pago.cveIntermeRec}</td>
								<td>${pago.importeAbono}</td>
								<td>${pago.estatus}</td>
								<td><a class="detalle" data-ref="${pago.referencia}" href="javascript:;">${detalle}</a></td>
							</tr>
						</c:forEach>
						<tr style="display:none;" id="noresults"> 
						<td>No Hay Resultados</td> 
					</tr>
					</tbody>
				</table>
			</div>
		<%@ include file="pagosAux.jsp" %> 
		<div class="framePieContenedor"> 
				<div class="contentPieContenedor">
					<table>
					<caption></caption>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResListadoPagos.listadoPagos}">
									<td class="izq widthx279"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td  class="izq_Des widthx279"><a href="javascript:;" >${exportar}</a></td>
								</c:otherwise>
							</c:choose>							
							<td  class="odd widthx6">${espacioEnBlanco}</td>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="der widthx279"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="der_Des widthx279"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
							</c:choose>							
						</tr>
						<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResListadoPagos.listadoPagos}">
									<td class="izq widthx279"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des widthx279"><a href="javascript:;" >${exportarTodo}</a></td>
								</c:otherwise>
							</c:choose>							
							<td  class="odd widthx6"></td>
							<td  class="der widthx279"><a id="idRegresarMain" href="javascript:;">${Regresar}</a></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');cargaTitulo('${refModel}',false);acomodaFecha();
	</script>
</c:if>