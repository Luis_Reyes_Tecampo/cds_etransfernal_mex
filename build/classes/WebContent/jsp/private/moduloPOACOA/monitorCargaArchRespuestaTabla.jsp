<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

			<div class="contentTablaEstandar">
				<table id="tblResultadosMonitorSaldos">
				<caption></caption>
					<thead>
						<tr>
							<th id="colNombre" class="text_centro">${nombre}</th>
							<th id="colEstatus" class="text_centro">${estatus}</th>
							<th id="colDescripcion" class="text_centro">${descr}</th>
						</tr>
					</thead>
					<tr>
						<td colspan="8" class="special"></td>
					</tr>
					<tbody>
						<c:if test="${beanResConsultaArchRespuesta.listaBeanResArchProc!=null}">	
							<c:forEach var="archivoItem" items="${beanResConsultaArchRespuesta.listaBeanResArchProc}" varStatus="rowCounter">
								<tr class="${rowCounter.index%2==0?"odd1":"odd2"}"> 
									<td class="text_izquierda">${archivoItem.nombreArchivo}</td>
									<td class="text_izquierda">${archivoItem.estatus}</td>
									<td class="text_izquierda">${archivoItem.descripcion}</td>								
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>	