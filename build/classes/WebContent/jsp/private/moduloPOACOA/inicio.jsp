<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ include file="../myHeader.jsp" %>
<c:if test="${modulo eq 'actual'}">
<jsp:include page="../myMenuModuloPOACOA.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloPOACOA" />
</jsp:include>
</c:if>
<c:if test="${modulo eq 'spid'}">
<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOASPID" />
</jsp:include>
</c:if>

	<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
	<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>


		
		<spring:message code="general.nombreAplicacion" var="app"/>
		<spring:message code="general.bienvenido"       var="welcome"/>
		
	
		<div class="pageTitleContainer">
			<span class="pageTitle">${welcome}</span> - ${app}
		</div>
		

