<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="moduloSPID.myMenu.text.moduloSPID" var="tituloModulo"/>
<spring:message code="moduloSPID.detalleRecept.text.tituloFuncionalidad" var="tituloFuncionalidad"/>

<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>

<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="txtRegresar"/>
<spring:message code="moduloSPID.detalleRecept.text.txtDatosGenerales" var="txtDatosGenerales"/>
<spring:message code="moduloSPID.detalleRecept.text.txtGuardar" var="txtGuardar"/>
<spring:message code="moduloSPID.detalleRecept.text.txtActualizar" var="txtActualizar"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFolioPaquete" var="txtFolioPaquete"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFolioPago" var="txtFolioPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtOrdenante" var="txtOrdenante"/>
<spring:message code="moduloSPID.detalleRecept.text.txtReceptor" var="txtReceptor"/>
<spring:message code="moduloSPID.detalleRecept.text.txtEnviar" var="txtEnviar"/>
<spring:message code="moduloSPID.detalleRecept.text.txtEstatusBanxico" var="txtEstatusBanxico"/>
<spring:message code="moduloSPID.detalleRecept.text.txtEstatusTransfer" var="txtEstatusTransfer"/>
<spring:message code="moduloSPID.detalleRecept.text.txtTopologia" var="txtTopologia"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCDA" var="txtCDA"/>
<spring:message code="moduloSPID.detalleRecept.text.txtTipoOp" var="txtTipoOp"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCodError" var="txtCodError"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFchInstruPago" var="txtFchInstruPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtModitoDev" var="txtModitoDev"/>
<spring:message code="moduloSPID.detalleRecept.text.txtClasifOp" var="txtClasifOp"/>
<spring:message code="moduloSPID.detalleRecept.text.txtHoraAcepPago" var="txtHoraAcepPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRefeNum" var="txtRefeNum"/>
<spring:message code="moduloSPID.detalleRecept.text.txtTipoPago" var="txtTipoPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtHoraInstruPago" var="txtHoraInstruPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFchAcepPago" var="txtFchAcepPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFolioPaqDev" var="txtFolioPaqDev"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFolioPagoDev" var="txtFolioPagoDev"/>
<spring:message code="moduloSPID.detalleRecept.text.txtPrioridad" var="txtPrioridad"/>
<spring:message code="moduloSPID.detalleRecept.text.txtLongRastreo" var="txtLongRastreo"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFchCaptura" var="txtFchCaptura"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCveRastreo" var="txtCveRastreo"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCveRastreoOri" var="txtCveRastreoOri"/>
<spring:message code="moduloSPID.detalleRecept.text.txtReferenciaTransfer" var="txtReferenciaTransfer"/>
<spring:message code="moduloSPID.detalleRecept.text.txtMonto" var="txtMonto"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCveIntermeOrd" var="txtCveIntermeOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtTipoCtaOrd" var="txtTipoCtaOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCodPostalord" var="txtCodPostalord"/>
<spring:message code="moduloSPID.detalleRecept.text.txtFchConstitOrd" var="txtFchConstitOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtDirIP" var="txtDirIP"/>
<spring:message code="moduloSPID.detalleRecept.text.txtCveInstOrd" var="txtCveInstOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtDomOrd" var="txtDomOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtTipoCtaRec" var="txtTipoCtaRec"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRfcOrd" var="txtRfcOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtNumCtaOrd" var="txtNumCtaOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtNombreOrd" var="txtNombreOrd"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRfcRec" var="txtRfcRec"/>
<spring:message code="moduloSPID.detalleRecept.text.txtNombreRec" var="txtNombreRec"/>
<spring:message code="moduloSPID.detalleRecept.text.txtConceptoPago" var="txtConceptoPago"/>
<spring:message code="moduloSPID.detalleRecept.text.txtNumCtaRec" var="txtNumCtaRec"/>
<spring:message code="moduloSPID.detalleRecept.text.txtNumCtaTran" var="txtNumCtaTran"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPID"             var="txtTraspasoSIACSPID"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPEI"             var="txtTraspasoSIACSPEI"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/detalleRecepcion.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle"> </span> - ${titulo}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	</span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
 
	<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="fchOperacion" id="fchOperacion" value="${e:forHtmlAttribute(beanRes.llave.fchOperacion)}"/>				
		<input type="hidden" name="cveMiInstituc" id="cveMiInstituc" value="${e:forHtmlAttribute(beanRes.llave.cveMiInstituc)}"/>
		<input type="hidden" name="cveInstOrd" id="cveInstOrd" value="${e:forHtmlAttribute(beanRes.llave.cveInstOrd)}"/>
		<input type="hidden" name="folioPaquete" id="folioPaquete" value="${e:forHtmlAttribute(beanRes.llave.folioPaquete)}"/>
		<input type="hidden" name="folioPago" id="folioPago" value="${e:forHtmlAttribute(beanRes.llave.folioPago)}"/>
		<input type="hidden" name="opcion" id="opcion" value="${e:forHtmlAttribute(opcion)}"/>
		<input type="hidden" name="ref" id="ref" value="${e:forHtmlAttribute(ref)}"/>
		<input type="hidden" name="servicio" id="servicio" value="${servicio}"/>
		<input type="hidden" name="fecha" id="fecha" value="${fecha}"/>

		<input type="hidden" id="modulo" name="modulo" value="${beanModulo.modulo}"/>
        <input type="hidden" id="tipo" name="tipo" value="${beanModulo.tipo}"/>
		<input type="hidden" id="path" name="path" value="${beanModulo.path}"/>
		<input type="hidden" value="${beanModulo.modulo}" id="bmModulo" />
		<input type="hidden" value="${beanModulo.tipo}" id="bmTipo" /> 
		<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
		<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
		<input type="hidden" value="${txtTraspasoSIACSPID}" id="txtTraspasoSIACSPID" />
		<input type="hidden" value="${txtTraspasoSIACSPEI}" id="txtTraspasoSIACSPEI" />

		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtDatosGenerales}</div>
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">
						${txtFolioPaquete}
					</td>
					<td  class="text_izquierda">
						<label for="folioPaquete"></label><input class="text_izquierda" type="text" id="folioPaquete" name="beanReq.llave.folioPaquete" value="${beanRes.llave.folioPaquete}" maxlength="12" size="12" readonly="readonly"/>
					</td> 
					<td  class="text_izquierda">
						${txtFolioPago}
					</td>
					<td  class="text_izquierda">
						<label for="folioPago"></label><input class="text_izquierda" type="text" id="folioPago" name ="beanReq.llave.folioPago" value="${beanRes.llave.folioPago}" maxlength="12" size="12" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtTopologia}
					</td>
					<td  class="text_izquierda">
						<label for="topologia"></label><input class="text_izquierda" type="text" id="topologia" name ="beanReq.datGenerales.topologia" value="${beanRes.datGenerales.topologia}" maxlength="1" size="1" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtCDA}
					</td>
					<td  class="text_izquierda">
						<label for="cda"></label><input class="text_izquierda" type="text" id="cda" name ="beanReq.datGenerales.cda" value="${beanRes.datGenerales.cda}" maxlength="1" size="1" readonly="readonly"/>
					</td>
				</tr>
				
				
				<tr>
					<td  class="text_izquierda">
						${txtEnviar}
					</td>
					<td  class="text_izquierda">
						<label for="enviar"></label><input class="text_izquierda" type="text" id="enviar" name ="beanReq.datGenerales.enviar" value="${beanRes.datGenerales.enviar}" maxlength="1" size="1" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtEstatusBanxico}
					</td>
					<td  class="text_izquierda">
						<label for="estatusBanxico"></label><input class="text_izquierda" type="text" id="estatusBanxico" name="beanReq.estatus.estatusBanxico" value="${beanRes.estatus.estatusBanxico}" maxlength="2" size="2" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtEstatusTransfer}
					</td>
					<td  class="text_izquierda">
						<label for="estatusTransfer"></label><input class="text_izquierda" type="text" id="estatusTransfer" name="beanReq.estatus.estatusTransfer" value="${beanRes.estatus.estatusTransfer}" maxlength="2" size="2" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtModitoDev}
					</td>
					<td  class="text_izquierda">
						<label for="motivoDev"></label><input class="text_izquierda" type="text" id="motivoDev" name="beanReq.envDev.motivoDev" value="${beanRes.envDev.motivoDev} - ${beanRes.envDev.motivoDevDesc}" maxlength="2" size="2" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtClasifOp}
					</td>
					<td  class="text_izquierda">
						<label for="clasifOperacion"></label><input class="text_izquierda" type="text" id="clasifOperacion" name="beanReq.datGenerales.clasifOperacion" value="${beanRes.datGenerales.clasifOperacion}" maxlength="2" size="2" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtTipoOp}
					</td>
					<td  class="text_izquierda">
						<label for="tipoOperacion"></label><input class="text_izquierda" type="text" id="tipoOperacion" name="beanReq.datGenerales.tipoOperacion" value="${beanRes.datGenerales.tipoOperacion}" maxlength="2" size="2" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtCodError}
					</td>
					<td  class="text_izquierda">
						<label for="codigoError"></label><input class="text_izquierda" type="text" id="codigoError" name="beanReq.estatus.codigoError" value="${beanRes.estatus.codigoError}" maxlength="8" size="8" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtFchInstruPago}
					</td>
					<td  class="text_izquierda">
						<label for="fchInstrucPago"></label><input class="text_izquierda" type="text" id="fchInstrucPago" name="beanReq.fchPagos.fchInstrucPago" value="${beanRes.fchPagos.fchInstrucPago}" maxlength="8" size="8" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtHoraInstruPago}
					</td>
					<td  class="text_izquierda">
						<label for="horaInstrucPago"></label><input class="text_izquierda" type="text" id="horaInstrucPago" name="beanReq.fchPagos.horaInstrucPago" value="${beanRes.fchPagos.horaInstrucPago}" maxlength="11" size="11" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtFchAcepPago}
					</td>
					<td  class="text_izquierda">
						<label for="fchAceptPago"></label><input class="text_izquierda" type="text" id="fchAceptPago" name="beanReq.fchPagos.fchAceptPago" value="${beanRes.fchPagos.fchAceptPago}" maxlength="8" size="8" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtHoraAcepPago}
					</td>
					<td  class="text_izquierda">
						<label for="horaAceptPago"></label><input class="text_izquierda" type="text" id="horaAceptPago" name="beanReq.fchPagos.horaAceptPago" value="${beanRes.fchPagos.horaAceptPago}" maxlength="11" size="11" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtRefeNum}
					</td>
					<td  class="text_izquierda">
						<label for="refeNumerica"></label><input class="text_izquierda" type="text" id="refeNumerica" name="beanReq.bancoOrd.refeNumerica" value="${beanRes.bancoOrd.refeNumerica}" maxlength="20" size="20" readonly="readonly"/>
					</td>
				</tr>
				
				
				<tr>
					<td  class="text_izquierda">
						${txtTipoPago}
					</td>
					<td  class="text_izquierda">
						<label for="tipoPago"></label><input class="text_izquierda" type="text" id="tipoPago" name="beanReq.datGenerales.tipoPago" value="${beanRes.datGenerales.tipoPago}" maxlength="7" size="7" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtPrioridad}
					</td>
					<td  class="text_izquierda">
						<label for="prioridad"></label><input class="text_izquierda" type="text" id="prioridad" name="beanReq.datGenerales.prioridad" value="${beanRes.datGenerales.prioridad}" maxlength="7" size="7" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtLongRastreo}
					</td>
					<td  class="text_izquierda">
						<label for="longRastreo"></label><input class="text_izquierda" type="text" id="longRastreo" name="beanReq.rastreo.longRastreo" value="${beanRes.rastreo.longRastreo}" maxlength="7" size="7" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtFolioPaqDev}
					</td>
					<td  class="text_izquierda">
						<label for="folioPaqDev"></label><input class="text_izquierda" type="text" id="folioPaqDev" name="beanReq.envDev.folioPaqDev" value="${beanRes.envDev.folioPaqDev}" maxlength="12" size="12" readonly="readonly"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtFolioPagoDev}
					</td>
					<td  class="text_izquierda">
						<label for="folioPagoDev"></label><input class="text_izquierda" type="text" id="folioPagoDev" name="beanReq.envDev.folioPagoDev" value="${beanRes.envDev.folioPagoDev}" maxlength="12" size="12" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtReferenciaTransfer}
					</td>
					<td  class="text_izquierda">
						<label for="refeTransfer"></label><input class="text_izquierda" type="text" id="refeTransfer" name="beanReq.datGenerales.refeTransfer" value="${beanRes.datGenerales.refeTransfer}" maxlength="12" size="12" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtMonto}
					</td>
					<td  class="text_izquierda">
						<label for="monto"></label><input class="text_izquierda" type="text" id="monto" name="beanReq.datGenerales.monto" value="${beanRes.datGenerales.strMonto}" maxlength="17" size="17" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtFchCaptura}
					</td>
					<td  class="text_izquierda">
						<label for="fchCaptura"></label><input class="text_izquierda" type="text" id="fchCaptura" name="beanReq.datGenerales.fchCaptura" value="${beanRes.datGenerales.fchCaptura}" maxlength="10" size="10" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtCveRastreo}
					</td>
					<td  class="text_izquierda">
						<label for="cveRastreo"></label><input class="text_izquierda" type="text" id="cveRastreo" name="beanReq.rastreo.cveRastreo" value="${beanRes.rastreo.cveRastreo}" maxlength="30" size="30" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtCveRastreoOri}
					</td>
					<td  class="text_izquierda">
						<label for="cveRastreoOri"></label><input class="text_izquierda" type="text" id="cveRastreoOri" name="beanReq.rastreo.cveRastreoOri" value="${beanRes.rastreo.cveRastreoOri}" maxlength="30" size="30" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtDirIP}
					</td>
					<td  class="text_izquierda">
						<label for="direccionIp"></label><input class="text_izquierda" type="text" id="direccionIp" name="beanReq.rastreo.direccionIp" value="${beanRes.rastreo.direccionIp}" maxlength="39" size="39" readonly="readonly"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
	
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtOrdenante}</div>
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">
						${txtCveInstOrd}
					</td>
					<td  class="text_izquierda">
						<label for="cveInstOrd"></label><input class="text_izquierda" type="text" id="cveInstOrd" name="beanReq.llave.cveInstOrd" value="${beanRes.llave.cveInstOrd}" maxlength="12" size="12" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtTipoCtaOrd}
					</td>
					<td  class="text_izquierda">
						<label for="tipoCuentaOrd"></label><input class="text_izquierda" type="text" id="tipoCuentaOrd" name="beanReq.bancoOrd.tipoCuentaOrd" value="${beanRes.bancoOrd.tipoCuentaOrd}" maxlength="2" size="2" readonly="readonly"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtCveIntermeOrd}
					</td>
					<td  class="text_izquierda">
						<label for="cveIntermeOrd"></label><input class="text_izquierda" type="text" id="cveIntermeOrd" name="beanReq.bancoOrd.cveIntermeOrd" value="${beanRes.bancoOrd.cveIntermeOrd}" maxlength="5" size="5" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtCodPostalord}
					</td>
					<td  class="text_izquierda">
						<label for="codPostalOrd"></label><input class="text_izquierda" type="text" id="codPostalOrd" name="beanReq.bancoOrd.codPostalOrd" value="${beanRes.bancoOrd.codPostalOrd}" maxlength="5" size="5" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtFchConstitOrd}
					</td>
					<td  class="text_izquierda">
						<label for="fchConstitOrd"></label><input class="text_izquierda" type="text" id="fchConstitOrd" name="beanReq.bancoOrd.fchConstitOrd" value="${beanRes.bancoOrd.fchConstitOrd}" maxlength="8" size="8" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtRfcOrd}
					</td>
					<td  class="text_izquierda">
						<label for="rfcOrd"></label><input class="text_izquierda" type="text" id="rfcOrd" name="beanReq.bancoOrd.rfcOrd" value="${beanRes.bancoOrd.rfcOrd}" maxlength="18" size="18" readonly="readonly"/>
					</td>
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtNumCtaOrd}
					</td>
					<td  class="text_izquierda">
						<label for="numCuentaOrd"></label><input class="text_izquierda" type="text" id="numCuentaOrd" name="beanReq.bancoOrd.numCuentaOrd" value="${beanRes.bancoOrd.numCuentaOrd}" maxlength="20" size="20" readonly="readonly"/>
					</td>
					<td  class="text_izquierda">
						${txtNombreOrd}
					</td>
					<td  class="text_izquierda">
						<label for="nombreOrd"></label><input class="text_izquierda" type="text" id="nombreOrd" name="beanReq.bancoOrd.nombreOrd" value="${beanRes.bancoOrd.nombreOrd}" maxlength="120" size="120" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtDomOrd}
					</td>
					<td  class="text_izquierda">
						<label for="domicilioOrd"></label><input class="text_izquierda" type="text" id="domicilioOrd" name="beanReq.bancoOrd.domicilioOrd" value="${beanRes.bancoOrd.domicilioOrd}" maxlength="120" size="120" readonly="readonly"/>
					</td>
				</tr>
				
			</table>
		</div>
	</div>
		<div class="frameBuscadorSimple">
			<div class="titleBuscadorSimple">${txtReceptor}</div>
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">
						${txtTipoCtaRec}
					</td>
					<td  class="text_izquierda">
						<label for="tipoCuentaRec"></label><input class="text_izquierda" type="text" id="tipoCuentaRec"  name="beanReq.bancoRec.tipoCuentaRec" value="${beanRes.bancoRec.tipoCuentaRec}" maxlength="2" size="2" readonly="readonly"/>
					</td>					
					<td  class="text_izquierda">
						${txtRfcRec}
					</td>
					<td  class="text_izquierda">
						<label for="rfcRec"></label><input class="text_izquierda" type="text" id="rfcRec" name="beanReq.bancoRec.rfcRec" value="${beanRes.bancoRec.rfcRec}" maxlength="18" size="18" readonly="readonly"/>
					</td>
				</tr>
				
				<tr>
					<td  class="text_izquierda">
						${txtNumCtaRec}
					</td>
					<td  class="text_izquierda">
						<label for="numCuentaRec"></label><input class="text_izquierda" type="text" id="numCuentaRec" name="beanReq.bancoRec.numCuentaRec" value="${beanRes.bancoRec.numCuentaRec}" maxlength="20" size="20" readonly="readonly"/>
					</td>
				
				
				<td  class="text_izquierda">
						${txtNumCtaTran}
					</td>
					<td  class="text_izquierda">
						<label for="numCuentaTran"></label><input class="text_izquierda" type="text" id="numCuentaTran" name="beanReq.bancoRec.numCuentaTran" value="${beanRes.bancoRec.numCuentaTran}" maxlength="20" size="20" readonly="readonly"/>
					</td>
					
				</tr>
				<tr>
					<td  class="text_izquierda">
						${txtNombreRec}
					</td>
					<td  class="text_izquierda">
						<label for="nombreRec"></label><input class="text_izquierda" type="text" id="nombreRec" name="beanReq.bancoRec.nombreRec" value="${beanRes.bancoRec.nombreRec}" maxlength="120" size="120" readonly="readonly"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
			<div class="frameBuscadorSimple">
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
				<tr>
					<td  class="text_izquierda">
						${txtConceptoPago}
					</td>
					<td  class="text_izquierda">
						<label for="conceptoPago"></label><input class="text_izquierda" type="text" id="conceptoPago" name="beanReq.datGenerales.conceptoPago" value="${beanRes.datGenerales.conceptoPago}" maxlength="210" size="210" readonly="readonly"/>
					</td>	
					<td  class="text_izquierda">
						
					</td>
					<td  class="text_izquierda">
					</td>					
				</tr>
			</table>
		</div>
	</div>


		

<div class="framePieContenedor">
	<div class="contentPieContenedor">
			<table>
			<caption></caption>
				<tr>
					<td  class="odd widthx279">${espacioEnBlanco}</td>
					<td class="odd">${espacioEnBlanco}</td>
					<td  class="der widthx279"><a id="idRegresar" href="javascript:;">${txtRegresar}</a></td>
				</tr>					
			</table>
		</div>
	</div>
</form>

<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');cargaTitulo('',false);</script></c:if>