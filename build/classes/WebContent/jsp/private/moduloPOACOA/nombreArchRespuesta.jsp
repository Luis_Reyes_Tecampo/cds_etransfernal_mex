<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<%@ include file="../myHeader.jsp" %>
<c:if test="${modulo eq 'actual'}">
<jsp:include page="../myMenuModuloPOACOA.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOA" />
    <jsp:param name="menuSubitem" value="nomArchRespuesta" />
</jsp:include>
</c:if>
<c:if test="${modulo eq 'spid'}">
<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOASPID" />
    <jsp:param name="menuSubitem" value="nomArchRespuestaSpid" />
</jsp:include>
</c:if>

<script	src="${pageContext.servletContext.contextPath}/js/private/cargaArchResp.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

 
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="moduloPOACOA.myMenu.text.txtProcesar" var="txtProcesar"/>
<spring:message code="moduloPOACOA.myMenu.text.nombreArchivoRespuesta" var="nombreArchivoRespuesta"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOASPID.lblViewParam" var="lblViewParam"/>
<spring:message code="moduloPOACOASPID.lblViewCarga" var="lblViewCarga"/>
<spring:message code="moduloPOACOASPID.lblViewMonitor" var="lblViewMonitor"/>

	<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}'};</script>

	<div class="pageTitleContainer">
	<c:if test="${modulo eq 'actual'}">
		<span class="pageTitle">${moduloPOACOA}</span>-${nombreArchivoRespuesta}
	</c:if>
	<c:if test="${modulo eq 'spid'}">
		<span class="pageTitle">${moduloPOACOASPID}</span>-${nombreArchivoRespuesta}
	</c:if>
	</div>

	<form name="formArchResp" id="formArchResp" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			
	<input id="nombreArchOculto" name="nombreArchOculto" type="hidden" value="${e:forHtmlAttribute(archRespSustituirHide)}" />	
	<input id="accion"  name="accion" type="hidden" />
		
	<div class="frameBuscadorSimple">
		<div class="titleBuscadorSimple"></div>
		<div class="contentBuscadorSimple">
			<table>
			<caption></caption>
				<tbody>
					<tr>
							<td>Nombre de Archivo <label for="archResp" class="hide"></label><input id="archResp" name="archResp" type="file"/></td>	
					</tr>
				</tbody>
			</table>
			
		</div>
		
	</div>
	</form>

	<div>
		<br/>
		<br/>		
	</div>
		
							
		
		<div class="framePieContenedor">
			<div class="contentPieContenedor">				
				<table class="classWidth100">
				<caption></caption>
				<tr>
					<td class="cero">${espacioEnBlanco}</td>
					
					<c:choose>
						<c:when test="${INICIO_POA_COA}">
						<c:choose>
							<c:when test="${modulo eq 'spid'}">
									<td class="der classWidth279"><span><a href="javascript:cargaArchivo('spid');" id="btnPocesar1a">${txtProcesar}</a></span></td>
									</c:when>
									<c:otherwise>
									<td  class="classWidth279 der"><span>
									<a href="javascript:cargaArchivo('actual');" 
									id="btnPocesar1a">${txtProcesar}</a></span></td>
									</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<td class="der_Des classWidth279">
								<a href="javascript:;" >${txtProcesar}</a>
							</td>
						</c:otherwise>
					</c:choose>

				</tr>
			</table>
			</div>
		</div>
		 

	<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',''<c:if test="${codError == 'CD00166V'}">,function(e) {if (e) {document.forms['formArchResp'].action = "actualizarNombre.do";document.getElementById('accion').value = '${accion}';document.forms['formArchResp'].submit();}}	</c:if>);</script>
	</c:if>
		