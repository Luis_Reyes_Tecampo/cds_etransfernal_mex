<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>
       
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFchOperacion" var="txtFchOperacion"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtHora"      var="txtHora"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtIntsOrd"   var="txtIntsOrd"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtTopo"      var="txtTopo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtTipo"      var="txtTipo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtEstatus"   var="txtEstatus"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtInterOrd"   var="txtInterOrd"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFoliosPaquete" var="txtFoliosPaquete"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtFoliosPago" var="txtFoliosPago"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCodErr"    var="txtCodErr"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtRefTrans"  var="txtRefTrans"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtDevol"     var="txtDevol"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCuentaRec" var="txtCuentaRec"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtCuentaTran" var="txtCuentaTran"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtImporte"   var="txtImporte"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtTotales"   var="txtTotales"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtSumatoria"   var="txtSumatoria"/>
<spring:message code="moduloSPID.consultaRecepciones.text.txtApart"   var="txtApart"/>
<spring:message code="moduloSPID.consultaRecepciones.link.verDetalle"   var="verDetalle"/>
<spring:message code="moduloSPID.consultaRecepciones.link.enviaTransf"   var="enviaTransf"/>


<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoTXLiquidar"  var="txtTopoTXLiquidar"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtRechazos"        var="txtRechazos"/>
<spring:message code="moduloSPID.consultaRecepciones.link.txtBuscar"           var="txtBuscar"/>
<spring:message code="moduloSPID.consultaRecepciones.link.rechazar"            var="txtRechazar"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtApTransfer"      var="txtApTransfer"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTodas"           var="txtTodas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoVLiquidadas" var="txtTopoVLiquidadas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtTopoTLiquidadas" var="txtTopoTLiquidadas"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtDevoluciones"    var="txtDevoluciones"/>
<spring:message code="moduloSPID.consultaRecepciones.check.txtRechazoCMotDev"  var="txtRechazoCMotDev"/>
<spring:message code="moduloSPID.consultaRecepciones.link.actMotRechazo"       var="txtActMotRechazo"/>

<spring:message code="moduloSPID.consultaRecepciones.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.consultaRecepciones.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.consultaRecepciones.text.detalle" var="detalle"/>
<spring:message code="moduloSPID.consultaRecepciones.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.consultaRecepciones.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.consultaRecepciones.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cveInstitucion" var="cveInstitucion"/>
<spring:message code="moduloSPID.detalleRecept.text.txtRegresar" var="Regresar"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasPorAplic"   var="txtOrdRecibidasPorAplic"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasAplic"      var="txtOrdRecibidasAplic"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibiRechazadas" var="txtOrdRecibidasRechazadas"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00011V" var="ED00011V"/>
<spring:message code="codError.EC00011B" var="EC00011B"/>

<script type="text/javascript">var mensajes = {"aplicacion" : '${aplicacion}',"OK00002V" : '${OK00002V}', "ED00019V" : '${ED00019V}', "ED00020V" : '${ED00020V}',"ED00021V" : '${ED00021V}', "ED00022V" : '${ED00022V}', "ED00023V" : '${ED00023V}',"ED00027V" : '${ED00027V}', "ED00011V" : '${ED00011V}', "EC00011B" : '${EC00011B}'};</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/consultaRecepPC.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>
 
 
<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle">${txtModuloSPID}</span> - <span id="monitor">${beanResConsultaRecepciones.titulo}</span>
	<span id="espaciosBlanco"></span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
	 
	<form name="idForm" id="idForm" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	    <input type="hidden" name="paginaIni" id="paginaIni" value="${beanResConsultaRecepciones.beanPaginador.paginaIni}"/>
		<input type="hidden" name="pagina" id="pagina" value="${beanResConsultaRecepciones.beanPaginador.pagina}"/>
		<input type="hidden" name="paginaFin" id="paginaFin" value="${beanResConsultaRecepciones.beanPaginador.paginaFin}"/>
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" name="sortField" id="sortField" value="${sortField}">
		<input type="hidden" name="sortType" id="sortType" value="${sortType}">
		<label for="ref"></label><input type="hidden" id="ref" value="${beanResConsultaRecepciones.tipoOrden}"  type="text" size="40"  name="ref">
		<input type="hidden" name="fchOperacion" id="fchOperacion"/>				
		<input type="hidden" name="cveMiInstituc" id="cveMiInstituc"/>
		<input type="hidden" name="cveInstOrd" id="cveInstOrd"/>
		<input type="hidden" name="folioPaquete" id="folioPaquete"/>
		<input type="hidden" name="folioPago" id="folioPago"/>
	
	<input type="hidden" id="modulo" name="modulo" value="${beanModulo.modulo}"/>
        <input type="hidden" id="tipo" name="tipo" value="${beanModulo.tipo}"/>
		<input type="hidden" id="path" name="path" value="${beanModulo.path}"/>
		
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
	<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
	<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
	<input type="hidden" value="${txtOrdRecibidasPorAplic}" id="txtOrdRecibidasPorAplic" />
	<input type="hidden" value="${txtOrdRecibidasAplic}" id="txtOrdRecibidasAplic" />
	<input type="hidden" value="${txtOrdRecibidasRechazadas}" id="txtOrdRecibidasRechazadas" />
	<input type="hidden" value="${e:forHtmlAttribute(refModel)}" id="refModel" />

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
		<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
			<table>
			<caption></caption>
				<tr>
				<%@ include file="consultaRecCol1.jsp" %>
				<%@ include file="consultaRecCol2.jsp" %>
				<%@ include file="consultaRecCol3.jsp" %>
				<%@ include file="consultaRecCol4.jsp" %>
				<%@ include file="consultaRecCol5.jsp" %>
					<th scope="col"  class="text_izquierda widthx122"> </th>
				</tr>
				<tbody>				
					<c:forEach var="consultaRecepciones" items="${beanResConsultaRecepciones.listaConsultaRecepciones}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
							<td class="text_izquierda">${consultaRecepciones.beanConsultaRecepcionesDos.topologia}
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.topologia" id="topologia" value="${consultaRecepciones.beanConsultaRecepcionesDos.topologia}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.tipoPago" id="tipoPago" value="${consultaRecepciones.beanConsultaRecepcionesDos.tipoPago}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.estatusBanxico" id="estatusBanxico" value="${consultaRecepciones.beanConsultaRecepcionesDos.estatusBanxico}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.estatusTransfer" id="estatusTransfer" value="${consultaRecepciones.beanConsultaRecepcionesDos.estatusTransfer}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.fechOperacion" id="fechOperacion" value="${consultaRecepciones.beanConsultaRecepcionesDos.fechOperacion}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.fechCaptura" id="fechCaptura" value="${consultaRecepciones.beanConsultaRecepcionesDos.fechCaptura}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.cveInsOrd" id="cveInsOrd" value="${consultaRecepciones.beanConsultaRecepcionesDos.cveInsOrd}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.cveIntermeOrd" id="cveIntermeOrd" value="${consultaRecepciones.beanConsultaRecepcionesDos.cveIntermeOrd}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.folioPaquete" id="folioPaquete" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPaquete}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.folioPago" id="folioPago" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPago}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.numCuentaRec" id="numCuentaRec" value="${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaRec}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.monto" id="monto" value="${consultaRecepciones.beanConsultaRecepcionesDos.monto}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.motivoDevol" id="motivoDevol" value="${consultaRecepciones.beanConsultaRecepcionesDos.motivoDevol}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].codigoError" id="codigoError" value="${consultaRecepciones.codigoError}" /> 
							<input type="hidden" name="listaConsultaRecepciones[${rowCounter.index}].beanConsultaRecepcionesDos.numCuentaTran" id="numCuentaTran" value="${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaTran}" /> 
							<input type="hidden" name="totalReg" id="totalReg" value="${beanResConsultaRecepciones.totalReg}" />
							</td>
							<td class="text_izquierda">${consultaRecepciones.beanConsultaRecepcionesDos.tipoPago}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.estatusBanxico}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.estatusTransfer}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.fechOperacion}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.fechCaptura}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.cveInsOrd}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.cveIntermeOrd}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.folioPaquete}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.folioPago}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaRec}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.monto}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.motivoDevol}</td>
							<td class="text_centro">${consultaRecepciones.codigoError}</td>
							<td class="text_centro">${consultaRecepciones.beanConsultaRecepcionesDos.numCuentaTran}</td>
							<td class="text_centro campos"><a class="idVerDetalle" href="javascript:;">${detalle}</a>
								<input type="hidden" class="fchOperacion" value="${consultaRecepciones.beanConsultaRecepcionesDos.fechOperacion}"/>				
								<input type="hidden" class="cveMiInstituc" value="${sessionSPID.cveInstitucion}"/>
								<input type="hidden" class="cveInstOrd" value="${consultaRecepciones.beanConsultaRecepcionesDos.cveInsOrd}"/>
								<input type="hidden" class="folioPaquete" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPaquete}"/>
								<input type="hidden" class="folioPago" value="${consultaRecepciones.beanConsultaRecepcionesDos.folioPago}"/>
							</td>
						</tr>
					</c:forEach>
				</tbody> 
			</table>
		</div>
			<%@ include file="consultaRecNav.jsp" %>
	</div>
			<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table><caption></caption>
					<tr>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResConsultaRecepciones.listaConsultaRecepciones}">
									<td class="izq"><a id="idExportar" href="javascript:;">${exportar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${exportar}</a></td>
								</c:otherwise>
							</c:choose>
						<td class="odd"></td>
						<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td  class="der widthx279"><a id="idActualizar" href="javascript:;" >${actualizar}</a></td>
								</c:when>
								<c:otherwise>
									<td  class="der_Des widthx279"><a href="javascript:;" >${actualizar}</a></td>
								</c:otherwise>
						</c:choose>
					
					</tr>
					<tr>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'EXPORTAR') and not empty beanResConsultaRecepciones.listaConsultaRecepciones}">
								<td  class="izq widthx279"><a id="idExportarTodo" href="javascript:;">${exportarTodo}</a></td>
							</c:when>
							<c:otherwise>
								<td  class="izq_Des widthx279"><a href="javascript:;">${exportarTodo}</a></td>
							</c:otherwise>
						</c:choose>
						<td  class="odd widthx6"></td>
						<td  id="idRegresar" class="der widthx279"><a href="javascript:;" >${Regresar}</a></td>					
					</tr>
				</table>
 
			</div>
		</div>
			
</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');cargaTitulo('${refModel}',false);acomodaFecha();
	</script>
</c:if>