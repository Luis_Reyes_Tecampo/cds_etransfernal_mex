<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<th scope="col" data-id="topologia"
	class="text_centro sortField widthx223" scope="col">${txtTopo}<c:if
		test="${sortField == 'topologia'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th scope="col" data-id="tipoPago"
	class="text_centro sortField widthx122" scope="col">${txtTipo}<c:if
		test="${sortField == 'tipoPago'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>
<th scope="col" data-id="estatusBanxico"
	class="text_centro sortField widthx122" scope="col" colspan="2">${txtEstatus}
	<c:if test="${sortField == 'estatusBanxico'}">
		<c:choose>
			<c:when test="${sortType == 'DESC'}">
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
			</c:when>
			<c:otherwise>
				<img alt=""
					src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
			</c:otherwise>
		</c:choose>
	</c:if>
</th>