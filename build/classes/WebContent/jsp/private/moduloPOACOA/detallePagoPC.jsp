<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="moduloSPID.detallePago.text.tituloFuncionalidad" var="tituloFuncionalidad"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>


<spring:message code="moduloSPID.detallePago.text.txtEstatus" var="txtEstatus"/>
<spring:message code="moduloSPID.detallePago.text.txtIntermediario" var="txtIntermediario"/>
<spring:message code="moduloSPID.detallePago.text.txtNombre" var="txtNombre"/>

<spring:message code="moduloSPID.detallePago.text.linkHistoriaMensaje" var="historiaMensaje"/>
<spring:message code="moduloSPID.detallePago.text.linkRegresar" var="regresar"/>
<spring:message code="moduloSPID.detallePago.text.txtNumeroCuenta" var="txtNumeroCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtTipoCuenta" var="txtTipoCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtCodigoPostal" var="txtCodigoPostal"/>
<spring:message code="moduloSPID.detallePago.text.txtFechaConstit" var="txtFechaConstit"/>
<spring:message code="moduloSPID.detallePago.text.txtPuntoVenta" var="txtPuntoVenta"/>
<spring:message code="moduloSPID.detallePago.text.txtDivisa" var="txtDivisa"/>
<spring:message code="moduloSPID.detallePago.text.txtImporteCargo" var="txtImporteCargo"/>
<spring:message code="moduloSPID.detallePago.text.txtRfc" var="txtRfc"/>
<spring:message code="moduloSPID.detallePago.text.txtDomicilio" var="txtDomicilio"/>
<spring:message code="moduloSPID.detallePago.text.txtSucursal" var="txtSucursal"/>
<spring:message code="moduloSPID.detallePago.text.txtTipoCuenta" var="txtTipoCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtConceptoPago" var="txtConceptoPago"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario1" var="txtComentario1"/>
<spring:message code="moduloSPID.detallePago.text.txtNumeroCuenta" var="txtNumeroCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtClaveSwiftCuenta" var="txtClaveSwiftCuenta"/>
<spring:message code="moduloSPID.detallePago.text.txtImporteAbono" var="txtImporteAbono"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario2" var="txtComentario2"/>
<spring:message code="moduloSPID.detallePago.text.txtComentario3" var="txtComentario3"/>

<spring:message code="moduloSPID.detalleRecept.text.txtOrdenante" var="lblOrdenante"/>
<spring:message code="moduloSPID.detalleRecept.text.txtReceptor" var="lblReceptor"/>
<spring:message code="moduloSPEI.recepOperacion.text.otros" var="lblOtros"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<script type="text/javascript">
	var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}', "ED00065V":'${ED00065V}', "CD00167V":'${CD00167V}', "ED00068V":'${ED00068V}'};
	var tipoOrden = '${e:forHtmlAttribute(tipoOrden)}';
	var referencia = '${e:forHtmlAttribute(referencia)}';
</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/detallePago.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/consultaPagos.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>

<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle" >  </span> - ${tituloFuncionalidad}
	<span>${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}
	${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco}${espacioEnBlanco} 
	 </span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<div>
<table>
<caption></caption>
	<thead>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
	<tbody>	
	<tr>
		<td>${txtEstatus}</td>
		<td>
			<div>
				<label for="estatus"></label><input type="text" disabled="disabled" id="estatus" name="status" 
					value="${beanDetallePago.beanDatosGeneralesPago.beanDatosGeneralesPagoDos.estatus}" 
					class="inputT Campos" size="20" maxlength="12"/>
			</div>
	    </td>
	</tr>
	</tbody>
</table>
</div>
<form name="idForm" id="idForm" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResTopologia.beanPaginador.paginaIni)}">
		<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResTopologia.beanPaginador.pagina)}">
		<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResTopologia.beanPaginador.paginaFin)}">
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="idPeticion" id="idPeticion" value="">
		<input type="hidden" id="modulo" name="modulo" value="${e:forHtmlAttribute(beanModulo.modulo)}"/>
        <input type="hidden" id="tipo" name="tipo" value="${e:forHtmlAttribute(beanModulo.tipo)}"/>
		<input type="hidden" id="path" name="path" value="${e:forHtmlAttribute(beanModulo.path)}"/>
		<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
		<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
		<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
		<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
		<input type="hidden" name="ref" id="ref" value="">
		<input type="hidden" name="tipoRef" id="tipoRef" value="">
		
			<%@include file="camposDetallePagoPC.jsp" %>
			<div>
				<div class="titleBuscadorSimple">${lblOrdenante}</div>
			
				<table class="tablaContent">
				<caption></caption>
					<thead>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtIntermediario}</td>
						<td>
							<div>
							<label for="intermediario"></label>
								<input type="text" disabled="disabled" id="intermediario"
									name="intermediario"
									value="${beanDetallePago.beanOrdenantePago.intermediario}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNombre}</td>
						<td>
							<div>
							<label for="nombre"></label>
								<input type="text" disabled="disabled" id="nombre" name="nombre"
									value="${beanDetallePago.beanOrdenantePago.nombre}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNumeroCuenta}</td>
						<td>
							<div>
							<label for="numeroCuenta"></label>
								<input type="text" disabled="disabled" id="numeroCuenta"
									name="numeroCuenta"
									value="${beanDetallePago.beanOrdenantePago.numeroCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
			
			
					<tr>
			
						<td>${txtPuntoVenta}</td>
						<td>
							<div>
							<label for="puntoVenta"></label>
								<input type="text" disabled="disabled" id="puntoVenta"
									name="puntoVenta"
									value="${beanDetallePago.beanOrdenantePago.puntoVenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDivisa}</td>
						<td>
							<div>
							<label for="divisa"></label>
								<input type="text" disabled="disabled" id="divisa" name="divisa"
									value="${beanDetallePago.beanOrdenantePago.divisa}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtImporteCargo}</td>
						<td>
							<div>
							<label for="importeCargo"></label>
								<input type="text" disabled="disabled" id="importeCargo"
									name="importeCargo"
									value="${beanDetallePago.beanOrdenantePago.importeCargo}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
			
					</tr>
			
					<tr>
			
						<td>${txtTipoCuenta}</td>
						<td>
							<div>
							<label for="tipoCuenta"></label>
								<input type="text" disabled="disabled" id="tipoCuenta"
									name="tipoCuenta"
									value="${beanDetallePago.beanOrdenantePago.tipoCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtCodigoPostal}</td>
						<td>
							<div>
							<label for="codigoPostal"></label>
								<input type="text" disabled="disabled" id="codigoPostal"
									name="codigoPostal"
									value="${beanDetallePago.beanOrdenantePago.codigoPostal}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtFechaConstit}</td>
						<td>
			
							<div>
							<label for="fechaConstit"></label>
								<input type="text" disabled="disabled" id="fechaConstit"
									name="fechaConstit"
									value="${beanDetallePago.beanOrdenantePago.fechaConstit}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
					</tr>
			
					<tr>
						<td>${txtRfc}</td>
						<td>
			
							<div>
							<label for="rfc"></label>
								<input type="text" disabled="disabled" id="rfc" name="rfc"
									value="${beanDetallePago.beanOrdenantePago.rfc}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDomicilio}</td>
						<td>
			
							<div>
							<label for="domicilio"></label>
								<input type="text" disabled="disabled" id="domicilio"
									name="domicilio"
									value="${beanDetallePago.beanOrdenantePago.domicilio}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			
			<div>
				<div class="titleBuscadorSimple">${lblReceptor}</div>
				<table class="tablaContent">
				<caption></caption>
						<thead>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtIntermediario}</td>
						<td>
							<div>
							<label for="intermediarioDos"></label>
								<input type="text" disabled="disabled" id="intermediarioDos"
									name="intermediarioDos"
									value="${beanDetallePago.beanReceptorPago.intermediario}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtSucursal}</td>
						<td>
							<div>
							<label for="sucursal"></label>
								<input type="text" disabled="disabled" id="sucursal"
									name="sucursal"
									value="${beanDetallePago.beanReceptorPago.sucursal}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtNumeroCuenta}</td>
						<td>
							<div>
							<label for="numeroCuenta"></label>
								<input type="text" disabled="disabled" id="numeroCuenta"
									name="numeroCuenta"
									value="${beanDetallePago.beanReceptorPago.numeroCuentaReceptor}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
					</tr>
			
			
					<tr>
			
						<td>${txtNombre}</td>
						<td>
							<div>
							<label for="nombreDos"></label>
								<input type="text" disabled="disabled" id="nombreDos"
									name="nombreDos"
									value="${beanDetallePago.beanReceptorPago.nombre}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtDivisa}</td>
						<td>
							<div>
							<label for="divisaDos"></label>
								<input type="text" disabled="disabled" id="divisaDos"
									name="divisaDos"
									value="${beanDetallePago.beanReceptorPago.divisa}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtClaveSwiftCuenta}</td>
						<td>
							<div>
							<label for="claveSwiftCuenta"></label>
								<input type="text" disabled="disabled" id="claveSwiftCuenta"
									name="claveSwiftCuenta"
									value="${beanDetallePago.beanReceptorPago.claveSwiftCorres}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
			
					</tr>
			
					<tr>
			
						<td>${txtImporteAbono}</td>
						<td>
							<div>
							<label for="importeAbono"></label>
								<input type="text" disabled="disabled" id="importeAbono"
									name="importeAbono"
									value="${beanDetallePago.beanReceptorPago.importeAbono}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtTipoCuenta}</td>
						<td>
							<div>
							<label for="tipoCuenta"></label>
								<input type="text" disabled="disabled" id="tipoCuenta"
									name="tipoCuenta"
									value="${beanDetallePago.beanReceptorPago.tipoCuenta}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
						<td>${txtRfc}</td>
						<td>
							<div>
							<label for="rfcDos"></label>
								<input type="text" disabled="disabled" id="rfcDos" name="rfcDos"
									value="${beanDetallePago.beanReceptorPago.rfc}"
									class="inputT Campos" size="20" maxlength="12">
							</div>
						</td>
			
					</tr>
			</tbody>
				</table>
			</div>
			
			<div>
				<div class="titleBuscadorSimple">${lblOtros}</div>
				<table class="tablaContent"><caption></caption>
					<thead>
			<td class="anchoColumna"></td>
			<td></td>
		</thead>
		<tbody>
					<tr>
						<td>${txtConceptoPago}</td>
						<td colspan="2">
							<div>
							<label for="conceptoPago"></label>
								<input type="text" disabled="disabled" id="conceptoPago"
									name="conceptoPago"
									value="${beanDetallePago.beanReceptorPago.conceptoPago}"
									class="inputT Campos" size="51">
							</div>
						</td>
			
						<td>${txtComentario1}</td>
						<td colspan="2">
							<div>
							<label for="comentario1"></label>
								<input type="text" disabled="disabled" id="comentario1"
									name="comentario1"
									value="${beanDetallePago.beanReceptorPago.comentario1}"
									class="inputT Campos" size="52">
							</div>
						</td>
					</tr>
			
			
					<tr>
						<td>${txtComentario2}</td>
						<td colspan="2">
							<div>
							<label for="comentario12"></label>
								<input type="text" disabled="disabled" id="comentario12"
									name="comentario2"
									value="${beanDetallePago.beanReceptorPago.comentario2}"
									class="inputT Campos" size="51">
							</div>
						</td>
			
						<td>${txtComentario3}</td>
						<td colspan="2">
							<div>
							<label for="comentario3"></label>
								<input type="text" disabled="disabled" id="comentario3"
									name="comentario3"
									value="${beanDetallePago.beanReceptorPago.comentario3}"
									class="inputT Campos" size="52">
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			
			<div class="framePieContenedor">
				<div class="contentPieContenedor">
					<table><caption></caption>
						<tr>						
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="izq"><a id="idHistoriaMensaje" href="javascript:;">${historiaMensaje}</a></td>
								</c:when>
								<c:otherwise>
									<td class="izq_Des"><a href="javascript:;">${historiaMensaje}</a></td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
									<td class="der"><a  href="javascript:;" id="idRegresar" >${regresar}</a></td>
								</c:when>
								<c:otherwise>
									<td class="der_Des"><a href="javascript:;">${regresar}</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>
				</div>
			</div>

	</form>
<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');cargaTitulo('',false);
	</script>
</c:if>