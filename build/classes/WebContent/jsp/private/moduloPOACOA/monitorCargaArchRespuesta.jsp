<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../myHeader.jsp" %>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<c:if test="${modulo eq 'actual'}">
<jsp:include page="../myMenuModuloPOACOA.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOA" />
    <jsp:param name="menuSubitem" value="monitorArchRespuesta" />
</jsp:include>
</c:if>
<c:if test="${modulo eq 'spid'}">
<jsp:include page="../myMenuModuloPOACOASPID.jsp" flush="true">
    <jsp:param name="menuItem"    value="moduloPOACOASPID" />
    <jsp:param name="menuSubitem" value="monitorArchRespuestaSpid" />
</jsp:include>
</c:if>

<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitorCargaArchResp.js" type="text/javascript"></script>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOA" var="moduloPOACOA"/>
<spring:message code="moduloPOACOA.monitorArchs.text.titulo" var="titulo"/>
<spring:message code="moduloPOACOA.monitorArchs.text.nombre" var="nombre"/>
<spring:message code="moduloPOACOA.monitorArchs.text.desc" var="descr"/>
<spring:message code="moduloPOACOA.monitorArchs.text.estatus" var="estatus"/>
<spring:message code="moduloPOACOA.monitorArchs.text.botonLink.actualizar" var="actualizar"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOASPID.lblViewParam" var="lblViewParam"/>
<spring:message code="moduloPOACOASPID.lblViewCarga" var="lblViewCarga"/>
<spring:message code="moduloPOACOASPID.lblViewMonitor" var="lblViewMonitor"/>

<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">
		<c:if test="${modulo eq 'actual'}">
		<span class="pageTitle">${moduloPOACOA}</span>-${titulo}
		</c:if>
		<c:if test="${modulo eq 'spid'}">
		<span class="pageTitle">${moduloPOACOASPID}</span>-${titulo}
		</c:if>
	</div>

	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas"></div>
			<%@ include file="monitorCargaArchRespuestaPag.jsp" %>	
			<%@ include file="monitorCargaArchRespuestaInf.jsp" %>	
	 
	  
	
	</div>
	
	<form name="idForm" id="idForm" method="post" action="">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.paginaIni)}" />
		<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.pagina)}" />
		<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResConsultaArchRespuesta.beanPaginador.paginaFin)}" />
		<input type="hidden" name="accion" id="accion" value="" />
		<input type="hidden" name="accion-p" id="accion-p" value="${e:forHtmlAttribute(tipo)}" />
	</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');
	</script>
</c:if>
