<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="https://www.owasp.org/index.php/OWASP_Java_Encoder_Project" prefix="e"%>

<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="general.institucion" var="LabelInstitucion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>

<spring:message code="moduloSPID.recepcionOperacion.text.tituloFuncionalidad" var="ordenesReparar"/>
<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>


<spring:message code="moduloSPID.recepcionOperacion.text.claveTran" var="claveTran"/>
<spring:message code="moduloSPID.recepcionOperacion.text.macan" var="macan"/>
<spring:message code="moduloSPID.recepcionOperacion.text.ord" var="ord"/>
<spring:message code="moduloSPID.recepcionOperacion.text.rec" var="rec"/>
<spring:message code="moduloSPID.recepcionOperacion.text.folioPaquete" var="folioPaquete"/>
<spring:message code="moduloSPID.recepcionOperacion.text.folioPago" var="folioPago"/>
<spring:message code="moduloSPID.recepcionOperacion.text.importeAbono" var="importeAbono"/>
<spring:message code="moduloSPID.recepcionOperacion.text.est" var="est"/>
<spring:message code="moduloSPID.recepcionOperacion.text.actualizar" var="actualizar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.enviar" var="enviar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.mensajeError" var="mensajeError"/>
<spring:message code="moduloSPID.recepcionOperacion.text.regresar" var="regresar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.medioEnt" var="medioEnt"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fchCap" var="fchCap"/>
<spring:message code="moduloSPID.recepcionOperacion.text.exportar" var="exportar"/>
<spring:message code="moduloSPID.recepcionOperacion.text.exportarTodo" var="exportarTodo"/>
<spring:message code="moduloSPID.recepcionOperacion.text.refe" var="refe"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cola" var="cola"/>
<spring:message code="moduloSPID.recepcionOperacion.text.fechaOperacion" var="fechaOperacion"/>
<spring:message code="moduloSPID.recepcionOperacion.text.cveInstitucion" var="cveInstitucion"/>

<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00002V" var="OK00002V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.nombreAplicacion" var="aplicacion"/>


<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}',"OK00002V":'${OK00002V}',"ED00065V":'${ED00065V}',"CD00167V":'${CD00167V}',"ED00068V":'${ED00068V}',"CD00167V":'${CD00167V}'};</script>

<link href="${pageContext.servletContext.contextPath}/css/private/moduloSPID/sortField.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/ordenesReparacion.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>
<c:set var="searchString" value="${seguTareas}"/>

<div class="pageTitleContainer">
	<span class="pageTitle" id="moduloTitle">  </span> - <span id="monitor">  </span>
	<span id="espaciosBlanco"></span>${LabelInstitucion}<span class="pageTitle">  ${sessionSPID.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${sessionSPID.fechaOperacion}</span>
</div>
<form name="idForm" id="idForm" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="pagina" id="pagina" value="${e:forHtmlAttribute(beanResOrdenReparacion.beanPaginador.pagina)}">
    <input type="hidden" name="paginaIni" id="paginaIni" value="${e:forHtmlAttribute(beanResOrdenReparacion.beanPaginador.paginaIni)}">
	<input type="hidden" name="accion" id="accion" value="">
	<input type="hidden" name="paginaFin" id="paginaFin" value="${e:forHtmlAttribute(beanResOrdenReparacion.beanPaginador.paginaFin)}">
	<input type="hidden" name="idPeticion" id="idPeticion" value="">
	<input type="hidden" name="sortField" id="sortField" value="${e:forHtmlAttribute(sortField)}">
	<input type="hidden" id="modulo" name="modulo" value="${e:forHtmlAttribute(beanModulo.modulo)}"/>
	<input type="hidden" name="sortType" id="sortType" value="${e:forHtmlAttribute(sortType)}">
	<input type="hidden" id="path" name="path" value="${e:forHtmlAttribute(beanModulo.path)}"/>
	<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
    <input type="hidden" id="tipo" name="tipo" value="${e:forHtmlAttribute(beanModulo.tipo)}"/>
	<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.tipo)}" id="bmTipo" /> 
	<input type="hidden" value="${e:forHtmlAttribute(beanModulo.modulo)}" id="bmModulo" />
	
	<div class="frameTablaVariasColumnas">
		<div class="titleTablaVariasColumnas">${infEncontrada}</div>
			<div class="contentTablaVariasColumnas" style="overflow-x: scroll; overflow-y: hidden">
				<table>
				<caption></caption>
					<tr>
						<th  class="text_centro widthx223" scope="col"><label for="chkTodos"></label><input name="chkTodos" id="chkTodos" type="checkbox"/></th>
						<%@ include file="ordenesRepCol1.jsp" %>
						<%@ include file="ordenesRepCol2.jsp" %>
						<%@ include file="ordenesRepCol3.jsp" %>
						<%@ include file="ordenesRepCol4.jsp" %>
						<th  data-id="mensajeError" class="text_centro sortField widthx223" scope="col">${mensajeError}
							<c:if test="${sortField == 'mensajeError'}">
								<c:choose>
									<c:when test="${sortType == 'DESC'}">
										<img alt="" src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_down.gif">
									</c:when>
									<c:otherwise>
										<img alt="" src="${pageContext.servletContext.contextPath}/lf/default/img/menu/menu_arrow_up.gif">
									</c:otherwise>
								</c:choose>
							</c:if>
						</th> 
					</tr>
					<tbody>				
						<c:forEach var="beanOrdenReparacion" items="${beanResOrdenReparacion.listaOrdenesReparacion}" varStatus="rowCounter">
							<tr class="${rowCounter.index%2==0?"odd1":"odd2"}">
								<td class="text_centro">
								<label for="sele"></label>
									<input name="listaOrdenesReparacion[${rowCounter.index}].seleccionado" id="sele" value="true" type="checkbox"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.referencia" value="${beanOrdenReparacion.beanOrdenReparacionDos.referencia}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveCola" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveCola}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveTran" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveTran}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveMecanismo" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveMecanismo}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveMedioEnt" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveMedioEnt}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.fechaCaptura" value="${beanOrdenReparacion.beanOrdenReparacionDos.fechaCaptura}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveIntermeOrd" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeOrd}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.cveIntermeRec" value="${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeRec}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.importeAbono" value="${beanOrdenReparacion.beanOrdenReparacionDos.importeAbono}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.estatus" value="${beanOrdenReparacion.beanOrdenReparacionDos.estatus}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.folioPaquete" value="${beanOrdenReparacion.beanOrdenReparacionDos.folioPaquete}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].beanOrdenReparacionDos.foloPago" value="${beanOrdenReparacion.beanOrdenReparacionDos.foloPago}"/>
									<input type="hidden" name="listaOrdenesReparacion[${rowCounter.index}].mensajeError" value="${beanOrdenReparacion.mensajeError}"/>									
									<input type="hidden" name="totalReg" id="totalReg" value="${beanResOrdenReparacion.totalReg}" />
								</td> 
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.referencia}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveCola}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveTran}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveMecanismo}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveMedioEnt}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.fechaCaptura}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeOrd}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.cveIntermeRec}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.importeAbono}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.estatus}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.folioPaquete}</td>
								<td class="text_centro">${beanOrdenReparacion.beanOrdenReparacionDos.foloPago}</td>
								<td class="text_centro">${beanOrdenReparacion.mensajeError}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<%@ include file="ordenesRepNav.jsp" %>
			<%@ include file="ordenesRepPanel.jsp" %>
		</div>
	</form>

<c:if test="${codError!=''}">
	<script type = "text/javascript" defer="defer">
		${e:forHtmlAttribute(tipoError)}('${e:forHtmlAttribute(descError)}',
		   	   '${e:forHtmlAttribute(aplicacion)}',
		   	   '${e:forHtmlAttribute(codError)}',
		   	   '');cargaTitulo('${ordenesReparar}',true);acomodaFecha();
	</script>
</c:if>