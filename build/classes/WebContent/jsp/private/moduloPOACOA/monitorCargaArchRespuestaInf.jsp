<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<div class="framePieContenedor">
		<div class="contentPieContenedor">
			<table>
			<caption></caption>
				<tr>
					<td  class="cero classWidth50">${espacioEnBlanco}</td>
					<c:choose>
						<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
							<c:if test="${modulo eq 'actual'}">
							<td class="der classWidth50"><a id="idActualizar" class="actual" href="javascript:;">${actualizar}</a></td>
							</c:if>
							<c:if test="${modulo eq 'spid'}">
							<td class="der classWidth50"><a id="idActualizar" class="spid" href="javascript:;">${actualizar}</a></td>
							</c:if>
						</c:when>
						<c:otherwise>
							<td class="der_Des classWidth50"><a href="javascript:;" >${actualizar}</a></td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
		</div>
	</div>