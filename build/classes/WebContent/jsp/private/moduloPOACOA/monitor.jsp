<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>  
<%@ include file="../myHeader.jsp" %>
<%@ include file="validadorMonitor.jsp"%>
<spring:message code="general.espacioEnBlanco" var="espacioEnBlanco"/> 
<spring:message code="general.institucion" var="institucion"/>
<spring:message code="general.fechaOperacion" var="labelFechaOperacion"/>
<spring:message code="spid.menuSPID.txt.txtModuloSPID"                var="txtModuloSPID"/>
<spring:message code="spid.menuSPID.txt.txtMonitor"                   var="txtMonitor"/>
<spring:message code="spid.monitoOperativo.titulo.txtMonitorOperativo"    var="txtMonitorOperativo"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOperLiquidadas"         var="txtOperLiquidadas"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldoInicial"             var="txtActualizar"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoInicial"           var="txtSaldoInicial"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSIACSPID"        var="txtTraspasoSIACSPID"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasPorAplic"   var="txtOrdRecibidasPorAplic"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasAplic"      var="txtOrdRecibidasAplic"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibiRechazadas" var="txtOrdRecibidasRechazadas"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdRecibidasPorDev"     var="txtOrdRecibidasPorDev"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIAC"        var="txtTraspasoSPIDSIAC"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdEnviadasConf"        var="txtOrdEnviadasConf"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldos"                 var="txtSaldos"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldoCalculad"          var="txtSaldoCalculad"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldoNoReservado"       var="txtSaldoNoReservado"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldoReservado"         var="txtSaldoReservado"/>
<spring:message code="moduloPOACOA.monitor.txt.txtDiferencia"             var="txtDiferencia"/>
<spring:message code="moduloPOACOA.monitor.txt.txtDevolucionesEnvConf"    var="txtDevolucionesEnvConf"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOperNoConfirmadas"      var="txtOperNoConfirmadas"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACEspera"  var="txtTraspasoSPIDSIACEspera"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACEnv"     var="txtTraspasoSPIDSIACEnv"/>
<spring:message code="spid.monitoOperativo.txt.txtTraspasoSPIDSIACReparar" var="txtTraspasoSPIDSIACReparar"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdEspera"              var="txtOrdEspera"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdEnviadas"            var="txtOrdEnviadas"/>
<spring:message code="moduloPOACOA.monitor.txt.txtOrdPorReparar"          var="txtOrdPorReparar"/>
<spring:message code="spid.monitoOperativo.txt.txtCambioEstadoA"          var="txtCambioEstadoA"/>
<spring:message code="spid.monitoOperativo.txt.txtNoReceptivo"            var="txtNoReceptivo"/>
<spring:message code="spid.monitoOperativo.txt.txtReservacionSaldos"      var="txtReservacionSaldos"/>
<spring:message code="spid.monitoOperativo.txt.txtSaldoPorReservar"       var="txtSaldoPorReservar"/>
<spring:message code="spid.monitoOperativo.txt.txtConfirmar"              var="txtConfirmar"/>
<spring:message code="spid.monitoOperativo.txt.txtDesbloquear"            var="txtDesbloquear"/>
<spring:message code="spid.monitoOperativo.txt.txtUltimaReserva"          var="txtUltimaReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtEstadoReserva"          var="txtEstadoReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtMensajeReserva"         var="txtMensajeReserva"/>
<spring:message code="spid.monitoOperativo.txt.txtIncrementos"            var="txtIncrementos"/>
<spring:message code="spid.monitoOperativo.txt.txtDE"                     var="txtDE"/>
<spring:message code="spid.monitoOperativo.txt.txtMaxApart"               var="txtMaxApart"/>
<spring:message code="spid.monitoOperativo.txt.txtPorcentaje"             var="txtPorcentaje"/>
<spring:message code="spid.monitoOperativo.txt.txtHoraReservada"          var="txtHoraReservada"/>
<spring:message code="spid.monitoOperativo.txt.txtUsuario"                var="txtUsuario"/>
<spring:message code="spid.monitoOperativo.txt.txtActualizar"             var="txtActualizar"/>
<spring:message code="moduloPOACOA.monitor.txt.txtSaldosxBanco"             var="linkSaldosxBanco"/>

<spring:message code="general.nombreAplicacion" var="aplicacion"/>
<spring:message code="codError.ED00027V" var="ED00027V"/>
<spring:message code="codError.OK00013V" var="OK00013V"/>
<spring:message code="codError.ED00064V" var="ED00064V"/>
<spring:message code="codError.ED00065V" var="ED00065V"/>
<spring:message code="codError.CD00167V" var="CD00167V"/>
<spring:message code="codError.CD00168V" var="CD00168V"/>
<spring:message code="codError.CD00169V" var="CD00169V"/>

<spring:message code="moduloPOACOASPID.lblMenu" var="moduloPOACOASPID"/>
<spring:message code="moduloPOACOA.myMenu.text.moduloPOACOASPEI" var="moduloPOACOASPEI"/>

<spring:message code="moduloPOACOA.monitor.spei.lblMonitorPOA"             var="lblMonitorPOAspei"/>
<spring:message code="moduloPOACOA.monitor.spei.lblMonitorCOA"             var="lblMonitorCOAspei"/>
<spring:message code="moduloPOACOA.monitor.spid.lblMonitorPOA"             var="lblMonitorPOAspid"/>
<spring:message code="moduloPOACOA.monitor.spid.lblMonitorCOA"             var="lblMonitorCOAspid"/>

<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPID"             var="txtTraspasoSIACSPID"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSIACSPEI"             var="txtTraspasoSIACSPEI"/>

<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPIDSIAC"             var="txtTraspasoSPIDSIAC"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPEISIAC"             var="txtTraspasoSPEISIAC"/>

<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPIDSIACEsp"             var="txtTraspasoSPIDSIACEsp"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPIDSIACEnv"             var="txtTraspasoSPIDSIACEnv"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPIDSIACRep"             var="txtTraspasoSPIDSIACRep"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPEISIACEsp"             var="txtTraspasoSPEISIACEsp"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPEISIACEnv"             var="txtTraspasoSPEISIACEnv"/>
<spring:message code="moduloPOACOA.monitor.txt.txtTraspasoSPEISIACRep"             var="txtTraspasoSPEISIACRep"/>

<script type="text/javascript">var mensajes = {"aplicacion": '${aplicacion}',"ED00022V":'${ED00022V}',"ED00023V":'${ED00023V}',"ED00027V":'${ED00027V}',"ED00191V":'${ED00191V}',"OK00013V":'${OK00013V}',"ED00064V":'${ED00064V}',"ED00065V":'${ED00065V}',"CD00167V":'${CD00167V}',"CD00168V":'${CD00168V}',"CD00169V":'${CD00169V}'};</script>


<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitorPOACOA.js" type="text/javascript"></script>

<script src="${pageContext.servletContext.contextPath}/js/private/global.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/private/utilerias.js" type="text/javascript"></script>

<script src="${pageContext.servletContext.contextPath}/js/private/moduloPOACOA/monitores.js" type="text/javascript"></script>

<input type="hidden" value="${beanModulo.modulo}" id="bmModulo" />
<input type="hidden" value="${beanModulo.tipo}" id="bmTipo" />

<input type="hidden" value="${moduloPOACOASPID}" id="moduloPOACOASPIDmt" />
<input type="hidden" value="${moduloPOACOASPEI}" id="moduloPOACOASPEImt" />
<input type="hidden" value="${lblMonitorPOAspei}" id="lblMonitorPOAspei" />
<input type="hidden" value="${lblMonitorCOAspei}" id="lblMonitorCOAspei" />
<input type="hidden" value="${lblMonitorPOAspid}" id="lblMonitorPOAspid" />
<input type="hidden" value="${lblMonitorCOAspid}" id="lblMonitorCOAspid" />
<input type="hidden" value="${txtTraspasoSIACSPID}" id="txtTraspasoSIACSPID" />
<input type="hidden" value="${txtTraspasoSIACSPEI}" id="txtTraspasoSIACSPEI" />
<input type="hidden" value="${txtTraspasoSPIDSIAC}" id="txtTraspasoSPIDSIAC" />
<input type="hidden" value="${txtTraspasoSPEISIAC}" id="txtTraspasoSPEISIAC" />
<input type="hidden" value="${txtTraspasoSPIDSIACEsp}" id="txtTraspasoSPIDSIACEsp" />
<input type="hidden" value="${txtTraspasoSPIDSIACEnv}" id="txtTraspasoSPIDSIACEnv" />
<input type="hidden" value="${txtTraspasoSPIDSIACRep}" id="txtTraspasoSPIDSIACRep" />
<input type="hidden" value="${txtTraspasoSPEISIACEsp}" id="txtTraspasoSPEISIACEsp" />
<input type="hidden" value="${txtTraspasoSPEISIACEnv}" id="txtTraspasoSPEISIACEnv" />
<input type="hidden" value="${txtTraspasoSPEISIACRep}" id="txtTraspasoSPEISIACRep" />

<c:set var="searchString" value="${seguTareas}"/>

	<div class="pageTitleContainer">

		<span class="pageTitle" id="moduloTitle">  </span> - <span id="monitor">  </span>
		<span id="espaciosBlanco"></span>${institucion}<span class="pageTitle">  ${beanMonitor.session.cveInstitucion} </span>${labelFechaOperacion} <span class="pageTitle"> ${beanMonitor.session.fechaOperacion}</span>
	</div>

	<div class="SPID">
		<div class="SPID1">
			<div class="titleBuscadorSimple">${txtOperLiquidadas}</div>
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoInicial}</span></td>
						<td class="tdSPID2"><label for="saldoInicial"></label><input id="saldoInicial" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"></td>
					</tr>
					<tr> 
						<td class="tdSPID"><span class="text_izquierdaSPID" id="transpaso1">transpaso 1</span></td>
						<td class="tdSPID2"><label for="traspasoSIACSPIDSaldo"></label><input class="text_derechaSPID traspasosSIACTipo" id="traspasoSIACSPIDSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.traspasoSIACTipo.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="traspasoSIACSPIDVolumen"></label><input class="text_derechaSPID2 traspasosSIACTipo" id="traspasoSIACSPIDVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.traspasoSIACTipo.volumen}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasPorAplic}</span></td>
						<td class="tdSPID2"><label for="ordRecibidasPorAplicSaldo"></label><input class="text_derechaSPID ordRecibidasPorAplic" id="ordRecibidasPorAplicSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesPorAplicar.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordRecibidasPorAplicVolumen"></label><input class="text_derechaSPID2 ordRecibidasPorAplic" id="ordRecibidasPorAplicVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesPorAplicar.volumen}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasAplic}</span></td>
						<td class="tdSPID2"><label for="ordRecibidasAplicSaldo"></label><input class="text_derechaSPID ordRecibidasAplicadas" id="ordRecibidasAplicSaldo"  type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesAplicadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordRecibidasAplicVolumen"></label><input class="text_derechaSPID2 ordRecibidasAplicadas" id="ordRecibidasAplicVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesAplicadas.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasRechazadas}</span></td>
						<td class="tdSPID2"><label for="ordRecibidasRechazadasSaldo"></label><input class="text_derechaSPID ordRecibidasRechazadas" id="ordRecibidasRechazadasSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesRechazadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordRecibidasRechazadasVoluemn"></label><input class="text_derechaSPID2 ordRecibidasRechazadas" id="ordRecibidasRechazadasVoluemn" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.recepciones.ordenesRechazadas.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdRecibidasPorDev}</span></td>
						<td class="tdSPID2"><label for="ordRecibidasPorDevSaldo"></label><input class="text_derechaSPID ordRecibidasPorDevolver" id="ordRecibidasPorDevSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.ordenesPorDevolver.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordRecibidasPorDevVolumen"></label><input class="text_derechaSPID2 ordRecibidasPorDevolver" id="ordRecibidasPorDevVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.ordenesPorDevolver.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID" id="transpaso2">transpaso 2</span></td>
						<td class="tdSPID2"><label for="traspasoSPIDSIACSaldo"></label><input class="text_derechaSPID traspasoTipoSIAC" id="traspasoSPIDSIACSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.traspasoTipoSIAC.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="traspasoSPIDSIACVolumen"></label><input class="text_derechaSPID2 traspasoTipoSIAC" id="traspasoSPIDSIACVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.traspasoTipoSIAC.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEnviadasConf}</span></td>
						<td class="tdSPID2"><label for="ordEnviadasConfSaldo"></label><input class="text_derechaSPID ordEnviadasConf" id="ordEnviadasConfSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.ordenesEnvidasConfirmadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordEnviadasConfVolumen"></label><input class="text_derechaSPID2 ordEnviadasConf" id="ordEnviadasConfVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesLiquidadas.traspasoOrden.ordenesEnvidasConfirmadas.volumen}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
			
			<div class="titleBuscadorSimple">${txtSaldos}</div>
			<div class="contentBuscadorSimple">
				<table><caption></caption>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoCalculad}</span></td>
						<td class="tdSPID2"><label for="s1"></label><input id="s1"class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanMonitor.saldos.saldoCalculado}" readonly="readonly"/></td>
						<td rowspan="4"></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoNoReservado}</span></td>
						<td class="tdSPID2"><label for="s2"></label><input id="s2" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanMonitor.saldos.saldoNoReservado}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtSaldoReservado}</span></td>
						<td class="tdSPID2"><label for="s3"></label><input id="s3" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanMonitor.saldos.saldoReservado}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtDiferencia}</span></td>
						<td class="tdSPID2"><label for="s4"></label><input id="s4" class="text_derechaSPID" type="text" maxlength="17" size="17" value="${beanMonitor.saldos.diferencia}" readonly="readonly"/></td>
					</tr>
					
					<tr><td></td></tr>
					<tr><td></td></tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtDevolucionesEnvConf}</span></td>
						<td class="tdSPID2"><label for="devolucionesEnvConfSaldo"></label><input class="text_derechaSPID devolucionesEnvConf" id="devolucionesEnvConfSaldo" type="text" maxlength="17" size="17" value="${beanMonitor.saldos.devolucionesEnvidadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="devolucionesEnvConfVolumen"></label><input class="text_derechaSPID2 devolucionesEnvConf" id="devolucionesEnvConfVolumen" type="text" maxlength="7" size="7" value="${beanMonitor.saldos.devolucionesEnvidadas.volumen}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
			
			<div class="titleBuscadorSimple">${txtOperNoConfirmadas}</div>
			<div class="contentBuscadorSimple">
				<table>
				<caption></caption>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID" id="TrasEspera"> Transpaso Espera</span></td>
						<td class="tdSPID2"><label for="traspasoSPIDSIACEsperaSaldo"></label><input id="traspasoSPIDSIACEsperaSaldo" class="text_derechaSPID traspasosSIACTipoEspera" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosEspera.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="traspasoSPIDSIACEsperaVolumen"></label><input id="traspasoSPIDSIACEsperaVolumen" class="text_derechaSPID2 traspasosSIACTipoEspera" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosEspera.volumen}" readonly="readonly"/></td>
					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID" id="TrasEnviados"> Transpaso Enviado</span></td>
						<td class="tdSPID2"><label for="traspasoSPIDSIACEnvSaldo"></label><input id="traspasoSPIDSIACEnvSaldo" class="text_derechaSPID traspasosSIACTipoEnviados" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosEnviados.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="traspasoSPIDSIACEnvVolumen"></label><input id="traspasoSPIDSIACEnvVolumen" class="text_derechaSPID2 traspasosSIACTipoEnviados" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosEnviados.volumen}" readonly="readonly"/></td>

					</tr>
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID" id="TrasReparar"> Transpaso Reparar</span></td>
						<td class="tdSPID2"><label for="traspasoSPIDSIACRepararSaldo"></label><input id="traspasoSPIDSIACRepararSaldo" class="text_derechaSPID traspasosSIACTipoReparar" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosPorReparar.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="traspasoSPIDSIACRepararVolumen"></label><input id="traspasoSPIDSIACRepararVolumen" class="text_derechaSPID2 traspasosSIACTipoReparar" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.traspasos.traspasosPorReparar.volumen}" readonly="readonly"/></td>
					</tr>
				
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEspera}</span></td>
						<td class="tdSPID2"><label for="ordEsperaSaldo"></label><input id="ordEsperaSaldo" class="text_derechaSPID ordenesEspera" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesEspera.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordEsperaVolumen"></label><input id="ordEsperaVolumen" class="text_derechaSPID2 ordenesEspera" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesEspera.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdEnviadas}</span></td>
						<td class="tdSPID2"><label for="ordEnviadasSaldo"></label><input id="ordEnviadasSaldo" class="text_derechaSPID ordenesEnviadas" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesEnviadas.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordEnviadasVolumen"></label><input id="ordEnviadasVolumen" class="text_derechaSPID2 ordenesEnviadas" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesEnviadas.volumen}" readonly="readonly"/></td>
					</tr>
					
					<tr>
						<td class="tdSPID"><span class="text_izquierdaSPID">${txtOrdPorReparar}</span></td>
						<td class="tdSPID2"><label for="ordPorRepararSaldo"></label><input id="ordPorRepararSaldo" class="text_derechaSPID ordenesReparar" type="text" maxlength="17" size="17" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesPorReparar.saldo}" readonly="readonly"/></td>
						<td class="tdSPID3"><label for="ordPorRepararVolumen"></label><input id="ordPorRepararVolumen" class="text_derechaSPID2 ordenesReparar" type="text" maxlength="7" size="7" value="${beanMonitor.operacionesNoConfirmadas.ordenes.ordenesPorReparar.volumen}" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
		
		
			
			
		</div>

	</div>

	<div class="framePieContenedor">
			<div class="contentPieContenedor">
				<table><caption></caption>
					<tr>							
					
						<td class="odd">${espacioEnBlanco}</td>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(searchString,'CONSULTAR')}">
								<td class="der widthx279" ><a id="idActualizar" href="javascript:;">${txtActualizar}</a></td>
							</c:when>
							<c:otherwise>
								<td class="der_Des widthx279" ><a href="javascript:;">${txtActualizar}</a></td>
							</c:otherwise>
						</c:choose>
					</tr>
					
				</table>

			</div>
		</div>
<form name="idForm" id="idForm" method="post" action="">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="accion" id="accion" value="">
		<input type="hidden" name="ref" id="ref" value="">
		<input type="hidden" name="tipoRef" id="tipoRef" value="">
		
        <input type="hidden" id="modulo" name="modulo" value="${beanModulo.modulo}"/>
        <input type="hidden" id="tipo" name="tipo" value="${beanModulo.tipo}"/>
		<input type="hidden" id="path" name="path" value="${beanModulo.path}"/>
		
		
</form>

<c:if test="${codError!=''}"><script type = "text/javascript" defer="defer">${tipoError}('${descError}','${aplicacion}','${codError}','');valores();acomodaFecha();</script></c:if>
