<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- 
  - Autor: Indra
  - Aplicación: SoftToken
  - Módulo: Solicitud
  - Fecha: 18/10/2013
  - Descripción: Pantalla de Solicitud de SoftToken
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../myHeader.jsp" flush="true" />
<jsp:include page="../myMenuModuloContingencia.jsp" flush="true">
	<jsp:param name="menuItem" value="contingencia" />
	<jsp:param name="menuSubitem" value="catCanales" />
</jsp:include>

<script
	src="${pageContext.servletContext.contextPath}/js/private/catalogoCanales.js"
	type="text/javascript"></script>

<spring:message code="menu.moduloContingencia.text.titulo" var="tituloModulo"/>
<spring:message code="menu.canalesContingentes.text.titulo"       var="titulo"/>
<spring:message code="moduloPOACOA.catCanales.text.canal"       var="canal"/>
<spring:message code="moduloPOACOA.catCanales.text.actDes"       var="activarDes"/>
<spring:message code="general.aceptar"       var="aceptar"/>
<spring:message code="general.cancelar"       var="cancelar"/>
<spring:message code="moduloPOACOA.catCanales.text.medioEntrega"       var="medioEntrega"/>

<div class="pageTitleContainer">
	<span class="pageTitle">${tituloModulo}</span>
</div>
<div class="frameBuscadorSimple">
	<div class="titleBuscadorSimple">
		<span>${titulo}</span>
	</div>
	<div class="contentBuscadorSimple" style="width:99.9%">
		<form name="formValorCanal" id="formValorCanal" method="post">    		
			<table style="width:100%" border="0">
				<tr>
					<td width="50%" class="text_derecha">${medioEntrega}</td>
					<td width="50%" class="text_izquierda">
						<select id="cmbCanal" name="cmbCanal" class="CamposCompletar" >
							<c:if test="${listaMedEntrega!=null}">						
								<c:forEach var="medEntrega" items="${listaMedEntrega}">
									<option value="${medEntrega.cveMedioEntrega}">${medEntrega.cveMedioEntrega} - ${medEntrega.nombreMedioEntrega}</option>
								</c:forEach>
							</c:if>
						</select>
					</td>
				</tr>		
				<tr>
					<td class="text_derecha">${activarDes}</td>
					<td class="text_izquierda"><input type="checkbox" value="1"
						class="CamposCompletar Campos" name="actDesCanal" id="actDesCanal"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div class="framePieContenedor">
		<div class="contentPieContenedor" style="width:99.9%">
			<table style="width:100%">
				<tr>
					<td width="50%" class="izq"><span><a href="javascript:agregarCanal();"
							id="btnAdd">${aceptar}</a> </span></td>
					<td class="odd" />
					<td width="50%" class="der"><span><a href="${pageContext.servletContext.contextPath}/moduloPOACOA/catalogoCanalesInit.do"
							id="btnCancelar">${cancelar}</a> </span></td>
				</tr>
			</table>
		</div>
	</div>

</div>
<jsp:include page="../myFooter.jsp" flush="true" />
<c:if test="${codError!=''}">
		<script type = "text/javascript" defer="defer">
			${tipoError}('${descError}',
			   	   '${aplicacion}',
			   	   '${codError}',
			   	   '');
		</script>
</c:if>