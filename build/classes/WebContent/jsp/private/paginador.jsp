<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="general.inicio" var="inicio"/>
<spring:message code="general.fin" var="fin"/>
<spring:message code="general.anterior" var="anterior"/>
<spring:message code="general.siguiente" var="siguiente"/>

<div class="paginador">
	<c:if test="${param.paginaIni == param.pagina}"><a href="javascript:;">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${param.paginaIni != param.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${param.servicio}','INI');">&lt;&lt;${inicio}</a></c:if>
	<c:if test="${param.paginaAnt!='0' && param.paginaAnt!=null}"><a href="javascript:;" onclick="nextPage('idForm','${param.servicio}','ANT');">&lt;${anterior}</a></c:if>
	<c:if test="${param.paginaAnt=='0' || param.paginaAnt==null}"><a href="javascript:;">&lt;${anterior}</a></c:if>
	<label id="txtPagina">${param.pagina} - ${param.paginaFin}</label>
	<c:if test="${param.paginaFin != param.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${param.servicio}','SIG');">${siguiente}&gt;</a></c:if>
	<c:if test="${param.paginaFin == param.pagina}"><a href="javascript:;">${siguiente}&gt;</a></c:if>
	<c:if test="${param.paginaFin != param.pagina}"><a href="javascript:;" onclick="nextPage('idForm','${param.servicio}','FIN');">${fin}&gt;&gt;</a></c:if>
	<c:if test="${param.paginaFin == param.pagina}"><a href="javascript:;">${fin}&gt;&gt;</a></c:if>
</div>