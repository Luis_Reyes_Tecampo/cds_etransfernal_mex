consultaHorariosSPEI = {

	init:function(){

		$('#idBuscar').click(function(){
			$('#paginaIni').val('1');
			$('#idForm').attr('action', 'consultarHorarioSPEI.do');
			$('#idForm').submit();			
		});
		
		$('#idAlta').click(function(){
			$('#paginaIni').val('1');
			$('#idForm').attr('action','muestraAltaHorarioSPEI.do');
			$('#idForm').submit();
		});
		
		$('#idModificar').click(function(){
			var flag = false;
			var objCheckBoxes = $( "[type=checkbox]" );
			if(objCheckBoxes && objCheckBoxes.length){
				for(var i=0;i<objCheckBoxes.length;i++){
					if(objCheckBoxes[i].checked){
						flag = true;
					}
				}
			}else if(objCheckBoxes && objCheckBoxes.checked){
					flag = true;
			}
			if(flag){
				jConfirm(mensajes["CD00044V"],mensajes["aplicacion"],'CD00044V','', function (respuesta){
					if(respuesta){
						isSubmit = true;
						isSubmitSend = true;
						$('#paginaIni').val('1');
						$('#idForm').attr('action','muestraModificaHorarioSPEI.do');
						$('#idForm').submit();
					} 
				});
			}else{
				jAlert(mensajes["ED00029V"],
						mensajes["aplicacion"],
					   	   'ED00029V',
					   	   '');
			}
		});
		
		$('#idEliminar').click(function(){
			var flag = false;
			var objCheckBoxes = $( "[type=checkbox]" );
			if(objCheckBoxes && objCheckBoxes.length){
				for(var i=0;i<objCheckBoxes.length;i++){
					if(objCheckBoxes[i].checked){
						flag = true;
					}
				}
			}else if(objCheckBoxes && objCheckBoxes.checked){
					flag = true;
			}
			if(flag){
				jConfirm(mensajes["CD00040V"],mensajes["aplicacion"],'CD00040V','', function (respuesta){
					if(respuesta){
						isSubmit = true;
						isSubmitSend = true;
						$('#idForm').attr('action', 'eliminaHorarioSPEI.do');
						$('#idForm').submit();
					} 
				});
			}else{
				jAlert(mensajes["ED00029V"],
						mensajes["aplicacion"],
					   	   'ED00029V',
					   	   '');
			}
		});
		
	}
};

$(document).ready(consultaHorariosSPEI.init);