var recepcionOperacionHto = {
    lock: 0,
    init: function(){
		//funcion para mostrar el calendario
		createCalendarGuion('fchOperacion','cal1');
		document.body.style.cursor = "default";
		//funcion que sealiza la busqueda
		$('#idBuscarHto').click(function(){
			
			//ontienen la fecha
			var valF = $('#fchOperacion').val();
			var referencia = $(this).attr("ref");
//			if(valF)
			//llama la funcion para validar fecha	
			var resul = recepcionOperacionHto.validarFecha();
			
			//llama la funcion para realizar la busqueda
			recepcionOperacionHto.continuaInicio(referencia,valF,resul);
			
		});		
		
		//funcion para realiza la actualizazacion del motivo
		$('#idDevExt').click(function(){
			//se llama a la funcion del controller
			$('#idForm').attr('action','actMotRechazoHist.do');
			$('#idForm').submit();
		});
	  },
	  //funcion para validar la fecha
	  validarFecha:function(){
		  //asigna la fecha actual
		    var fechauno = new Date();
		    //asigna verdadero al resultado	
		    var resul =1;
		    //obntiene seleccionad apor el usuario 
			var fecha_aux = document.getElementById("fchOperacion").value.split("-");
		 	var fechados = new Date(parseInt(fecha_aux[2]),parseInt(fecha_aux[1]-1),parseInt(fecha_aux[0]));
			if(fecha_aux == "" || fecha_aux.length == 1) {
				resul = 3;
			} else if(fechados >= fechauno ) {
				resul = 2;
			}
			
			return resul;
	  },
	  //funcion para hacer la busqueda
	  continuaInicio:function(referencia,valF,resul){	
		  if(resul===1){
				
				document.body.style.cursor = "wait";
				var form = document.getElementById("idForm");
				//se llama a la funcion del controller
				form.action="consTranSpeiRec.do"; 
				//pasa los parametros a la vista
				$('#FOperacion').val(valF);			
				$('#idFechaOperacion').val(valF);
				$('#cveMiInst').val($('#cveMiInstitucion').val());
				$('#fActual').val($('#fechaActual').val());
				form.submit();
			}else{
				//mensajes de error
				if(resul===2){
					jAlert(mensajes["ED00128V"],
							mensajes["aplicacion"],
						   	   'ED00128V',
						   	   '');
				}else{
					jAlert(mensajes["ED00063V"],
						mensajes["aplicacion"],
					   	   'ED00063V',
					   	   '');
				}
			}
	  },
	  //devolucion historica
	  devolverHTO:function(index){	
		  //mensaje de confirmacion
			jConfirm(mensajes["CD00172V"],mensajes["aplicacion"],'CD00172V','', function (respuesta){
				var valF = $('#fchOperacion').val();
				recepcionOperacionHto.continuaDevolver(valF,index, 1,respuesta);				
			});
		},
		//continua con la devolucion
		continuaDevolver:function(valF,index, val,respuesta){
			if(recepcionOperacionHto.lock===0 && respuesta){
				recepcionOperacionHto.lock=1;
				//asigna los datos
				$('#selecDevolucion'+index).val('true');				
				$('#FOperacion').val(valF);			
				$('#idFechaOperacion').val(valF);
				$('#cveMiInst').val($('#cveMiInstitucion').val());
				$('#fActual').val($('#fechaActual').val());
				//se llama a la funcion del controller
				$('#idForm').attr('action','devolucionExtemporanea.do');
				$('#idForm').submit();
			}
		}


		
};
	


$(document).ready(recepcionOperacionHto.init);

