jQuery(document).ready(function(){
   if(codErrorUpdate === "OK00000V"){
      var mensaje = $("input[id='mensajeUpdate']").val();
      $.alerts.info(mensaje, "eTransferNal", codErrorUpdate, "", undefined);
   }

});

/** Variable Global **/
var referencia = "";

$(document).ready(function(){
    //ontiene la cuenta receptora
    $("#ctaReceptora").removeAttr('readonly');  
    var usuSession = $("#sessionUsu").val();
    var usuAparta = $("#usuarioAparta").val();
    var tareaMod = $('#tareaMod').val();
    //pregunta si viene de la pantalla principales
    if((usuSession === usuAparta) && (tareaMod === "true")){
    	//oculta o muestra el boton
    	$("#guardar").css("display", "block");
    	$("#guardarA").css("display", "none");    	
    }else{
    	//oculta o muestra el boton
    	$("#guardar").css("display", "none");
    	$("#guardarA").css("display", "block");
    }
    
    /** Funcion que muestra mensaje de confirmacion para realizar el Update de la Cuenta Receptora **/
    $('#idGuardar').click(function(){
    	var ctaRecep = $("#ctaReceptora").val();
    	$("#cuentaReceptora").val(ctaRecep);
        $("#fchOperacion").val($(this).attr("ref"));
        $("#cveMiInstitucion").val($(this).attr("ref2"));
        $("#cveInstOrd").val($(this).attr("ref3"));
        $("#folioPaquete").val($(this).attr("ref4"));
        $("#folioPago").val($(this).attr("ref5"));
        $("#nombrePantalla").val($(this).attr("ref7"));
        
        if(ctaRecep.length > 0 || ctaRecep == null || ctaRecep == undefined){
        
        referencia = $(this).attr("ref6");
        
        var mensaje = $("input[id='msjConfUpdate']").val();
       
	        //mensaje de confirmacion para guardar los datos
	        jConfirm(mensaje, mensajes["CD00043V"],'CD00043V', '', function (respuesta){
				if(respuesta){
					//se llama a la funcion del controller
			        $('#idForm').attr('action','updateCtaReptora.do');	 
			        $('#idForm').submit();
				} 
			});
        }else{
        	//mensaje de error
			jAlert(mensajes["ED00136V"],
					mensajes["aplicacion"],
				   	   'ED00136V',
				   	   '');
        }
    });
    
    //boton para regresar a la pantalla anterior
    /** Funcion que regresa a la pantalla de Inicio **/
    $('#idRegresar').click(function(){
    	var idform='#idForm';
    	var nomPantalla = $("#nombrePantalla").val();     	
    	//se llama a la funcion del controller
    	$(idform).attr('action','consTranSpeiRec.do?nomPantalla='+nomPantalla);
    	
       $(idform).submit();
	});
    
    
    
    /*FUNCION PARA MOSTRAR MENSAJE AL PASAR EL CURSOR SOBRE LA IMAGEN*/
    $('#idMostrarAlerta').on('mouseover',function(){
   	
   jAlert(mensajes["EMO0002V"]+'CUENTA RECEPTORA',
				mensajes["aplicacion"],
			   	   'EMO0002V',
			   	   '');
	});
    
}
	
	
);