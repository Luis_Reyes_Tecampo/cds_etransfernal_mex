var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var list = [];
var recepcionOperacion = {
     lock:0,
     init:function(){	
    	recepcionOperacion.continuainit();
		
		//funcion que Actualiza la pantalla
		$('#idActualizar').click(function(){	
			recepcionOperacion.esTransfer();
		});
		
		$('.filField').keypress(function(e) {
			var val = $('#nombrePantalla').val();
			var fechOp = $('#fechOper').val();
			var charCode = (e.which) ? e.which : e.keyCode;
			cadena = $(this).val();
			cadenaLength = cadena.length;
			cadenaLenghtMenos = cadenaLength - 1;
			cadenaAux = cadena.substring(0, cadenaLenghtMenos);
			if (charCode === 8 || charCode === 37 || charCode === 39) {
				return;
			}
			if (charCode === 13) {
				if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
					$('#field').val($(this).attr('data-field'));
					$('#fieldValue').val($(this).val());
				}
				$('#fechOper').val(fechOp);
				$('#nombrePantalla').val(val);
				$('#idForm').attr('action', 'consTranSpeiRec.do');
				$('#idForm').submit();
			}
		});
	},	
	continuainit:function(){	
		
		//funcion para exportar 
		$('#idExportarTodo').on('click', function (e) {
			recepcionOperacion.funcionesControlles(1,e);		   
	    });
		
		//funcion para apartar las operaicones
	    $('#idApartados').on('click', function (e) {
	    	recepcionOperacion.funcionesControlles(2,e);
	    });
	    
		//funcion para generar el excel
	    $('#idExportar').on('click', function (e) {
	    	recepcionOperacion.funcionesControlles(3,e);
	    });
	    
	   
	    
	    
	    
	},
	//funcon para ver a que accion del controller se ira
	funcionesControlles:function(tipo,e){
		e.stopPropagation();
	    e.preventDefault();
	    var idform='#idForm';
		if(tipo===1){
			//se llama a la funcion del controller		    
		    $(idform).attr('action', 'exportaTodoRecepOperAlterna.do');
		}
		
		if(tipo===2){
			//se llama a la funcion del controller
		    $(idform).attr('action', 'recepOperaApartadas.do');		
		}
		
		if(tipo===3){
			//se llama a la funcion del controller
		    $(idform).attr('action', 'exportarRecepCtaAlterna.do');
		}
		
		
		
		 $('#idForm').submit();
	},
	
	
	//funcon para recuperar la operacion
	recuparar:function(index){
		if($('#selectMotivo'+index).val().length == 0) {
			//mensaje de error
		jAlert(mensajes["ED00067V"]+'Rechazar',
				mensajes["aplicacion"],
			   	   'ED00067V',
			   	   '');
		} else {
		jConfirm(mensajes["CD00168V"],mensajes["aplicacion"],'CD00168V','', function (respuesta){
			
				if(recepcionOperacion.lock===0 && respuesta){
					recepcionOperacion.lock=1;
					$('#selecRecuperar'+index).val('true');
					//se llama a la funcion del controller
					$('#idForm').attr('action','recuperarTransfSpeiRec.do');
					$('#idForm').submit();
				}
			
		});
		}
	},
	//funcion para hacer una devolucion normal
	devolverSPEI:function(index){
		var val = $('#nombrePantalla').val();
		jConfirm(mensajes["CD00169V"],mensajes["aplicacion"],'CD00169V','', function (respuesta){
			
				if(recepcionOperacion.lock===0 && respuesta){
					recepcionOperacion.lock=1;
					$('#selecDevolucion'+index).val('true');
					$('#nomPantalla').val(val);	
					//se llama a la funcion del controller
					$('#idForm').attr('action','devolverTransfSpei.do');
					$('#idForm').submit();
				}
			
		});
	},
	// funcion para ver dato seleccionado
	esTransfer:function(){
		var nomP = $('#nombrePantalla').val();
		$('#nombrePantalla').val(nomP);
		
		if (nomP === 'recepcionOperaHistoricaSPEI' || nomP === 'recepcionOperacionHto') {
			var valF = $('#fchOperacion').val();
			var resul = validarFechaHto();
			barredorInicio(valF,resul);
		} else {
			//se llama a la funcion del controller
			$('#idForm').attr('action', 'consTranSpeiRec.do');
			$('#idForm').submit();
		}
		
	}
};

function validarFechaHto() {
	//asigna la fecha actual
    var fechauno = new Date();
    //asigna verdadero al resultado	
    var resul =1;
    //obntiene seleccionad apor el usuario 
	var fecha_aux = document.getElementById("fchOperacion").value.split("-");
 	var fechados = new Date(parseInt(fecha_aux[2]),parseInt(fecha_aux[1]-1),parseInt(fecha_aux[0]));
	if(fecha_aux == "" || fecha_aux.length == 1) {
		resul = 3;
	} else if(fechados >= fechauno ) {
		resul = 2;
	}
	
	return resul;
}
function barredorInicio(valF,resul) {
	if(resul===1){
		
		document.body.style.cursor = "wait";
		var form = document.getElementById("idForm");
		//se llama a la funcion del controller
		form.action="consTranSpeiRec.do"; 
		//pasa los parametros a la vista
		$('#FOperacion').val(valF);			
		$('#idFechaOperacion').val(valF);
		$('#cveMiInst').val($('#cveMiInstitucion').val());
		$('#fActual').val($('#fechaActual').val());
		form.submit();
	}else{
		//mensajes de error
		if(resul===2){
			jAlert(mensajes["ED00128V"],
					mensajes["aplicacion"],
				   	   'ED00128V',
				   	   '');
		}else{
			jAlert(mensajes["ED00063V"],
				mensajes["aplicacion"],
			   	   'ED00063V',
			   	   '');
		}
	}
}
$(document).ready(recepcionOperacion.init);

