global = {
	init:function(){
	
	global.creaEvento(document, "keydown", function(event){
		var keyCode = ('which' in event) ? event.which : event.keyCode;
		var target = (event.currentTarget) ? event.target : event.srcElement;
		if(target.nodeName !== "INPUT"  && (keyCode === 8 )){
			validarTecla(event);
		}else if(validarTecla1(target, keyCode)){
				global.cancelEvent(event);
		}else if((target.nodeName === "INPUT" && target.type==="text") && (event.ctrlKey===1)){
			global.cancelEvent(event);
		}
	});

		global.creaEvento(document, "drag",global.cancelEvent);
		global.creaEvento(document, "dragstart",global.cancelEvent);
		global.creaEvento(document.getElementsByTagName("input"), "dragend",global.cancelEvent);
		global.creaEvento(document, "dragenter",global.cancelEvent);
		global.creaEvento(document, "dragleave",global.cancelEvent);
		global.creaEvento(document, "dragover",global.cancelEvent);
		
		global.creaEvento(document.getElementsByTagName("input"), "drop",global.cancelEvent);
		global.creaEvento(document,'contextmenu',global.cancelEvent);
		global.creaEvento(document, "draggesture",global.cancelEvent);
	},
	creaEvento:function(element,evento,callback){
		if(element.length){
			global.addEventLocalList(element,evento,callback);
		}else{
			global.addEventLocal(element,evento,callback);
		}			
	},
	addEventLocalList:function(element,evento,callback){
		var i = 0;
		for(i=0;i<element.length; i++){
			if(element[i].type==="text" && !element[i].readOnly){
				if(element[i].addEventListener){
					element[i].addEventListener(evento,callback,false);
				}else if(element[i].attachEvent){
					element[i].attachEvent('on'+evento,callback);
				}
			}
		}
	},
	
	addEventLocal:function(element,evento,callback){
		if(element.addEventListener){
			element.addEventListener(evento,callback,false);
		}else if(element.attachEvent){
			element.attachEvent('on'+evento,callback);
		}
	},
	cancelEvent:function(event){

		var CTRL = 17;
		var SHIFT = 16;
		//var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
		if(event.preventDefault){
			event.preventDefault();
			if (event.stopPropagation) {
				event.stopPropagation ();
	        }else {
	        	event.cancelBubble = true;
	        }
			event.returnValue = false;
			return false;
		}else{

			if (event.stopPropagation) {

				event.stopPropagation ();
	        }else {
	        	event.cancelBubble = true;
	        	event.returnValue = false;
	        }
			if(event.keyCode!==CTRL && event.keyCode!==SHIFT){
				event.keyCode = 0;
			}
			event.returnValue = false;
			return false;
		}
		return false;
	}
};

function createCalendarHHMMSS(dateField,shooter){
	Calendar.setup({
	    inputField: dateField,
	    ifFormat:   '%d/%m/%Y %H:%M',
	    button:     shooter,
	    weekNumbers: false,
	    showsTime: true,
	    firstDay: 0,
	    electric: false
	  });
}

function createCalendarGuion(dateField,shooter,formato){
	formato = formato;
	Calendar.setup({
	    inputField: dateField,
	    ifFormat:   '%d-%m-%Y',
	    button:     shooter,
	    weekNumbers: false,
	    firstDay: 0,
	    electric: false
	  });
}

function createCalendarTime(dateField,shooter,formato){
	Calendar.setup({
	    inputField: dateField,
	    timeFormat: '%H:%M:00',
	    ifFormat:   '%H:%M:00',
	    button:     shooter,
	    weekNumbers: false,
	    showsTime: true,
	    firstDay: 0,
	    electric: false
	  });
}

function createCalendarTimeHrMn(dateField,shooter){
	Calendar.setup({
	    inputField: dateField,
	    timeFormat: '%H:%M',
	    ifFormat:   '%H:%M',
	    button:     shooter,
	    weekNumbers: false,
	    showsTime: true,
	    firstDay: 0,
	    electric: false
	  });
}
/**Variable global para bloquear darle doble click*/
var isSubmitSend = false;
/**
 * Metodo que sirve para ir a la siguiente pagina, valida si ya se habia hecho el submit o no
 * @author Carlos Chong
 * @return void
 */
function nextPage(idFormName,nextPage,opcion,idOpcion){
	if(!isSubmitSend){
		nextPageOriginal(idFormName,nextPage,opcion,idOpcion);
	}
}

/**
 * Metodo que sirve para ir a la siguiente pagina, no se realizan validaciones
 * @author Carlos Chong
 * @return void
 */
function nextPageOriginal(idFormName,nextPage,opcion,idOpcion){
	var form = document.getElementById(idFormName);
	if(idOpcion !== undefined){
		document.getElementById(idOpcion).value = opcion;
	}else{
		document.getElementById("accion").value = opcion;
	}
	form.action = nextPage;
	isSubmitSend = true;
	form.submit();
}

/**
 * Funcion para realizar submit
 * @param formName
 * @param action
 */
function hacerSubmit(formName,action){
	var form = $('form[name="'+formName+'"]');
	form.attr("action", action);
	form.submit();
}

function validarTecla(event) {
	if(event.preventDefault){
		event.preventDefault();
		event.stopPropagation();
		return false;
	}else{
		event.keyCode = 0;
		event.returnValue = false;
		return false;
	}
}

function validarTecla1(target, keyCode) {
	var c = false;
	if(target.nodeName === "INPUT" && target.type==="text" && target.readOnly && (keyCode === 8 )){
		c = true;
	}
	return c;
}

$(document).ready(global.init);