monitorCargaArchHist = {
	init:function(){

		$('#idActualizar').click(function(e){
			$('#idForm').attr('action', 'consultarMonitorCargaArchHist.do');
			$('#idForm').submit();
		});

	},
	
	despliegaDetalle:function(id){
		$('#idPeticion').val(id);
		$('#idForm').attr('action', 'muestraGenerarArchCDAHistDet.do');
		$('#idForm').submit();
	}

	
};

$(document).ready(monitorCargaArchHist.init);