var formulario = 'idForm';
consultaMovMonHistCDASPID={
        diasMes:{0:31,1:28,2:31,3:30,4:31,5:30,6:31,7:31,8:30,9:31,10:30,11:31},
        regExpCveSpei : new RegExp("^([0-9]{1,6})$"),
        regExpNum : new RegExp("^([0-9]{1,20})$"),
        regExpMontoPago : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}(?!(,|[0-9]{1,2},|/.|[0-9]{1,2}/.))[0-9]{0,3}){0,}([/.]{1}[0-9]{0,2}){0,1})$"),
        regExpMontoPagoValida : new RegExp("^(([0-9]{1,3}){1,1}([,]{1}[0-9]{3,3}){0,4}([/.]{1}[0-9]{1,2}){0,1})$"),
        validarPeriodoOperacion:function(pInicio,pFin) {
            var resMovMon  = "";
            if(!Utilerias.isEmpty(pInicio) && !Utilerias.isEmpty(pFin)) {
                inicio = Utilerias.stringToDate($('#'+pInicio).val());
                fin = Utilerias.stringToDate($('#'+pFin).val());
                if(consultaMovMonHistCDASPID.esFechaFinOperacion(fin)){
                    return consultaMovMonHistCDASPID.validarPeriodoOperacion2(inicio, fin);
                }
                else {
                    resMovMon = "ED00020V";
                    return resMovMon;
                }
            }
            else {
                resMovMon = "ED00021V";
                return resMovMon;
            }
        },

        validarPeriodoOperacion2:function(inicio,fin){
            var resMovMon = "";
            if(consultaMovMonHistCDASPID.esPeriodoCorrecto(inicio,fin)){
                if(consultaMovMonHistCDASPID.esEnElMes(inicio,fin)) {
                    return resMovMon;
                }else {
                    resMovMon = "ED00019V";
                    return resMovMon;
                }
            }
            else {
                resMovMon = "ED00020V";
                return resMovMon;
                }
        },

        validarPeriodoHrs:function(pInicio,pFin) {
            var resMovMon = "";
            periodo=consultaMovMonHistCDASPID.esPeriodoEspecificado(pInicio,pFin);
            if(periodo===1) {
                hrInicio = $('#'+pInicio).val();
                hrFin = $('#'+pFin).val();
                pInicio= Utilerias.stringToTime(hrInicio);
                pFin= Utilerias.stringToTime(hrFin);
                if(consultaMovMonHistCDASPID.esPeriodoCorrecto(pInicio,pFin)){
                    return resMovMon;
                }
                else {
                    resMovMon = "ED00022V";
                    return resMovMon;
                }
            } else if(periodo===2|| periodo=== 3) {
                resMovMon = "ED00023V";
                return resMovMon;
            }
            else {
                return resMovMon;
            }
        },

        esPeriodoCorrecto:function(pInicioMovMon,pFinMovMon) {
            return pInicioMovMon.getTime()<= pFinMovMon.getTime();
        },

        esPeriodoEspecificado:function(pInicioMovMon,pFinMovMon) {
            var intR;
            if(!Utilerias.isEmpty(pInicioMovMon) && !Utilerias.isEmpty(pFin)){
                intR = 1;
                return intR;
            }else if (!Utilerias.isEmpty(pInicioMovMon)){
                intR = 2;
                return intR;
            }else if (!Utilerias.isEmpty(pFinMovMon)){
                intR = 3;
                return intR;
            }
            intR = 4;
            return intR;
        },

        esFechaFinOperacion:function(finMovMon){
            strFechaHoy=$('#fechaHoy').val();
            dFechaHoy = Utilerias.stringToDate(strFechaHoy);
            return finMovMon.getTime()<dFechaHoy.getTime();
        },

        esEnElMes:function(dFechaInicio,dFechaFin) {
            return (dFechaInicio.getFullYear() === dFechaFin.getFullYear() && dFechaInicio.getMonth() === dFechaFin.getMonth());
        },

        esPeriodoMenorTresMeses:function(dFechaInicio,dFechaFin) {
            mes = dFechaInicio.getMonth()+3;
            anio = dFechaInicio.getFullYear();
            if(mes>12){
                mes = mes-12;
                anio = anio+1;
            }
            dia = dFechaInicio.getDate();
            diaBaseMovMon = consultaMovMonHistCDASPID.diasMes[mes];
            if(mes === 1 && ((anio % 4 === 0) && ((anio % 100 !== 0) || (anio % 400 === 0)))){
                diaBaseMovMon =29;
            }
            if(dia>diaBaseMovMon){
                dFecha = new Date(anio,mes,diaBaseMovMon);
            }else{
                dFecha = new Date(anio,mes,dia);
            }
            return !(dFechaFin.getTime()>dFecha.getTime());
        },

        init:function(){
            createCalendarTimeHrMn('paramHrAbonoIni','cal3');
            createCalendarTimeHrMn('paramHrAbonoFin','cal4');
            createCalendarTimeHrMn('paramHrEnvioIni','cal5');
            createCalendarTimeHrMn('paramHrEnvioFin','cal6');

            if(!Utilerias.isEmpty('paramOpeContingencia')){
                $('#opeContingencia').attr("checked", $('#paramOpeContingencia').val());
            }

            $('#opeContingencia').change(function() {
                if ($('#opeContingencia').is(":checked")) {
                    $("#selectEstatus option[value="+ 0 +"]").attr("selected",true);
                    $('#selectEstatus').attr('disabled',true);
                    $('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
                }else {
                    $('#selectEstatus').attr('disabled',false);
                    document.getElementById('paramOpeContingencia').value = "";
                }
            });

            $('#idRegresar').click(function(){
                hacerSubmit(formulario, 'consInfMonCDAHistSPID.do');
            });

            $('#idExportar').click(function(){
                document.getElementById('accion').value = 'ACT';
                hacerSubmit(formulario, 'expConsMovMonHistCDASPID.do');
            });

            $('#idExportarTodo').click(function(){
                hacerSubmit(formulario, 'expTodosConsMovMonHistCDASPID.do');
            });

            $('#idLimpiar').click(function(){
                hacerSubmit(formulario, 'consMovMonHistCDASPID.do');
            });

            consultaMovMonHistCDASPID.init2();
            consultaMovMonHistCDASPID.init3();
            consultaMovMonHistCDASPID.init4();
            consultaMovMonHistCDASPID.init5();
        },

        init2:function(){
            $('#paramCveSpeiOrdenanteAbono').keypress(function(e){
                var cadMovMon;
                var kMovMon;

                if(document.all){
                    kMovMon = e.keyCode;
                } else {
                    kMovMon = e.which;
                }
                consultaMovMonHistCDASPID.cancel2(e);
                cadMovMon = $('#paramCveSpeiOrdenanteAbono').val()+String.fromCharCode(kMovMon);
                if(!(cadMovMon ==="") && !consultaMovMonHistCDASPID.regExpCveSpei.test(cadMovMon) && kMovMon !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        },
        
        init3:function(){
            $('#idBuscar').click(function(){

                if(!Utilerias.isEmpty('paramMontoPago') && !consultaMovMonHistCDASPID.validaCampoMonto('paramMontoPago')){
                    jAlert(mensajes["ED00027V"],mensajes["aplicacion"],'ED00027V','');
                    return;
                }
                var codigoMovMon=consultaMovMonHistCDASPID.validarPeriodoOperacion('paramFechaOpeInicio','paramFechaOpeFin');
                if(codigoMovMon.length===0) {
                    codigoMovMon= consultaMovMonHistCDASPID.validarPeriodoHrs('paramHrAbonoIni','paramHrAbonoFin');
                    if(codigoMovMon.length===0) {
                        codigoMovMon= consultaMovMonHistCDASPID.validarPeriodoHrs('paramHrEnvioIni','paramHrEnvioFin');
                        if(codigoMovMon.length===0){
                            $('#paramOpeContingencia').val($('#opeContingencia').is(":checked"));
                            $('#fechaOpeInicio').val($('#paramFechaOpeInicio').val());
                            $('#fechaOpeFin').val($('#paramFechaOpeFin').val());
                            $('#hrAbonoIni').val($('#paramHrAbonoIni').val());
                            $('#hrAbonoFin').val($('#paramHrAbonoFin').val());
                            $('#hrEnvioIni').val($('#paramHrEnvioIni').val());
                            $('#hrEnvioFin').val($('#paramHrEnvioFin').val());
                            $('#cveSpeiOrdenanteAbono').val($('#paramCveSpeiOrdenanteAbono').val());
                            $('#nombreInstEmisora').val($('#paramNombreInstEmisora').val());
                            $('#cveRastreo').val($('#paramCveRastreo').val());
                            $('#ctaBeneficiario').val($('#paramCtaBeneficiario').val());
                            $('#montoPago').val($('#paramMontoPago').val());
                            $('#tipoPago').val($('#paramTipoPago').val());
                            hacerSubmit(formulario, 'consMovMonHistCDASPID.do');
                        }
                        else {
                            jError(mensajes[codigoMovMon],mensajes["aplicacion"],codigoMovMon,'');
                        }
                    }
                    else {
                        jError(mensajes[codigoMovMon],mensajes["aplicacion"],codigoMovMon,'');
                    }
                }
                else {
                    jError(mensajes[codigoMovMon],mensajes["aplicacion"],codigoMovMon,'');
                }
            });  
        },
        
        init4:function(){
            $('#paramMontoPago').keypress(function(e){
                var cadMovMon;
                var kMovMon;

                if(document.all){
                    kMovMon = e.keyCode;
                } else {
                    kMovMon = e.which;
                }
                consultaMovMonHistCDASPID.cancel(e);
                cadMovMon = $('#paramMontoPago').val()+String.fromCharCode(kMovMon);
                if(!(cadMovMon ==="") && !consultaMovMonHistCDASPID.regExpMontoPago.test(cadMovMon) && kMovMon !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        },
        
        init5:function(){
            $('#paramTipoPago').keypress(function(e){
                var cadMovMon;
                var kMovMon;

                if(document.all){
                    kMovMon = e.keyCode;
                } else {
                    kMovMon = e.which;
                }
                consultaMovMonHistCDASPID.cancel2(e);
                cadMovMon = $('#paramTipoPago').val()+String.fromCharCode(kMovMon);
                if(!(cadMovMon ==="") && !consultaMovCDASPID.regExpNum.test(cadMovMon) && kMovMon !== 8){
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        },
        
        cancel:function(e){
            if(document.all){
                kMovMon = e.keyCode;
            } else {
                kMovMon = e.which;
            }
            var bandKeyMovMon = kMovMon === 8 || kMovMon===46|| kMovMon===44;
            if( !(bandKeyMovMon || (kMovMon>=48 && kMovMon<=57)) ){
                e.preventDefault();
                e.stopPropagation();
            }else{
                consultaMovMonHistCDASPID.cancel_(kMovMon,e);
            }
        },
        cancel_:function(kMovMon,e){
            if(kMovMon===107|| kMovMon===109 || kMovMon=== 111 || kMovMon===18){
                e.preventDefault();
                e.stopPropagation();
            }
        },
        cancel2:function(e){
            if(document.all){
                kMovMon = e.keyCode;
            } else {
                kMovMon = e.which;
            }
            if(!((kMovMon === 8 || kMovMon===190 ) || (kMovMon>=48 && kMovMon<=57)) ){
                e.preventDefault();
                e.stopPropagation();
            }else if(kMovMon===107|| kMovMon===109 || kMovMon=== 111 || kMovMon===18){
                e.preventDefault();
                e.stopPropagation();
            }
        },

        despliegaDetalle:function(valores){
            //referencia,fechaOpe,cveMiInstituc,cveSpei,folioPaq,folioPago,fchCDA,estatusCDA
            $('#referencia').val(valores["referencia"]);
            $('#fechaOpeLink').val(valores["fechaOpe"]);
            $('#cveMiInstitucLink').val(valores["cveMiInstituc"]);
            $('#cveSpeiLink').val(valores["cveSpei"]);
            $('#folioPaqLink').val(valores["folioPaq"]);
            $('#folioPagoLink').val(valores["folioPago"]);
            $('#fchCDALink').val(valores["fchCDA"]);
            $('#estatusCDALink').val(valores["estatusCDA"]);
            $('#accion').val('ACT');
            hacerSubmit('idForm', 'consMovMonHistDetCDASPID.do');
         },

         validaCampoMonto:function(idMonto){
             var monto = $('#'+idMonto).val();
             return consultaMovMonHistCDASPID.regExpMontoPagoValida.test(monto);
             }
         };

$(document).ready(consultaMovMonHistCDASPID.init);