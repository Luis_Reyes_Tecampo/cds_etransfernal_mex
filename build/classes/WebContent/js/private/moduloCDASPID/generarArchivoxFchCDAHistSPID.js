generarArchivoxFchCDAHistSPID = {
        copiar:false,
        inicio:function(){
            var fechaOperacion = 'fechaOp';
            createCalendarGuion(fechaOperacion,'calSPID');
            $('#idProcesarSPID').click(function(){
                if(Utilerias.isEmpty('fechaOp')){
                    jAlert(mensajes["ED00025V"],mensajes["aplicacion"],'ED00025V','');
                    return false;
                }
                strFechaBanMex=$('#fechaOperacionBancoMex').val();
                dFechaBanMex = Utilerias.stringToDate(strFechaBanMex);
                strFechaOp=$('#fechaOp').val();
                dFechaOp = Utilerias.stringToDate(strFechaOp);
                if(dFechaBanMex.getTime()>dFechaOp.getTime()){
                    hacerSubmit('idForm', 'procesarGenerarArchCDAxFchHistSPID.do');
                }else{
                    jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');
                }
            });
        },
        procesar:function(){
            if(Utilerias.isEmpty('fechaOp')){
                jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');
            };
        }
   };

$(document).ready(generarArchivoxFchCDAHistSPID.inicio);