contingenciaCDAMismoDiaSPID ={
        init:function(){
            $('#idActualizar').click(function(){
                $('#idForm').attr('action', 'consultarContingCDAMismoDiaSPID.do');
                $('#idForm').submit();
            });
            
            $('#idGeneracionNuevoArchivo').click(function(){

                jConfirm(mensajes["CD00020V"],mensajes["aplicacion"],'CD00020V','', function (resp){
                    if(resp === true){
                        isSubmit = true;
                        isSubmitSend = true;
                        $('#idForm').attr('action', 'genArchContigMismoDiaSPID.do');
                        $('#idForm').submit();
                    }
                });
            });
        }
};

$(document).ready(contingenciaCDAMismoDiaSPID.init);