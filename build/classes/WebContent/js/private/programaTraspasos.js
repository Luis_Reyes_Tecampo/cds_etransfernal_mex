programaTraspasos = {

		init:function(){
		
			$('#idAgregar').click(function(){
				
				jConfirm(mensajes["CD00043V"],mensajes["aplicacion"],'CD00043V','', function (respuesta){
					if(respuesta){
						isSubmit = true;
						isSubmitSend = true;
						if(Utilerias.isEmpty('selHora')){
							jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
						}else {
							$('#idForm').attr('action', 'altaHorarioTR.do');
							$('#idForm').submit();			
						}
					} 
				}
				);
				
				
			});
			
			$('#idEliminar').click(function(){
				
				jConfirm(mensajes["CD00040V"],mensajes["aplicacion"],'CD00040V','', function (respuesta){
					if(respuesta){
						isSubmit = true;
						isSubmitSend = true;
						$('#idForm').attr('action','eliminaHorarioTR.do');
						$('#idForm').submit();
					} 
				}
				);
				
				
				
			});
			
			$('#idRegresar').click(function(){
				$('#idForm').attr('action','muestraAdminSaldo.do');
				$('#idForm').submit();
			});
			
		}
	};

$(document).ready(programaTraspasos.init);