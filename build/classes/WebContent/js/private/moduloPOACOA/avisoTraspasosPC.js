const SORT = '#sortType';

var avisoTraspasos = {
    init:function(){ 
            
      /** Captura del evento actualizar **/
      $('#idActualizar').click(function(){
          $('#idForm').attr('action', 'consultaAvisosTraspasos.do');
          $('#idForm').submit();
      });
      
      /** Captura del evento regresar **/
      $('#idRegresarMain').click(function(){
         $('#idForm').attr('action', $('#path').val().substring(1));
         $('#idForm').submit();
      });
      
      $('#paginaIni').val('1');       
      
      /** Captura del evento exportar **/
      $('#idExportar').click(function(){
         $('#idForm').attr('action', 'exportarAvisosTraspasos.do');
          $('#idForm').submit(); 
      });
      
      /** Captura del ordenamiento **/
      $('.sortField').click(function(){
         if ($('#sortField').val() !== $(this).attr('data-id')){
            $(SORT).val('');
         }
         $(SORT).val($(SORT).val() === 'ASC' ? 'DESC' : 'ASC');
         $('#sortField').val($(this).attr('data-id'));         
         $('#idForm').attr('action', 'consultaAvisosTraspasos.do');
         $('#idForm').submit();
      });
      
      /** Captura del evento exportar todos **/
      $('#idExportarTodo').click(function(){          
          $('#idForm').attr('action', 'exportarTodoAvisosTraspasos.do');
          $('#accion').val('ACT');
          $('#idForm').submit(); 
      });
      
    }
};


$(document).ready(avisoTraspasos.init);