/** Declaracion de las variables globales **/
var arr = [8,32,48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,
104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;

/** Declaracion de la funcion general **/
var consultaPagos = {
/** Declaracion de lo eventos**/
    init:function(){ 
    /** Evento exportar **/
       $('#idExportar').click(function(){
           if ($(this).attr('data-field') !== '' && $(this).val() !== '')
           {
               $('#field').val($(this).attr('data-field'));
               $('#fieldValue').val($(this).val());
           }
           /** Se envia la accion a realizar*/
           $('#idForm').attr('action', 'exportarPagos.do');
           $('#idForm').submit();
       });
       
       
       
       /** Evento de filtrado **/
       $('.filField').keypress(function (e) {
/** Se crea la variable char*/
    var charCode = (e.which) ? e.which : e.keyCode;

     cadena = $(this).val();
     /** Se establece el valor de la cadena*/
     cadenaLength = cadena.length;
     cadenaLenghtMenos = cadenaLength - 1;
     cadenaAux = cadena.substring(0, cadenaLenghtMenos);

     arrLeng;
     if (charCode === 8 || charCode === 37 || charCode === 39) {
     return true;
     }
     /** si detecta el enter entra a buscar* */
     if (charCode === 13) {
     if ($(this).attr('data-field') !== ''
         && $(this).val() !== '') {
         $('#field').val($(this).attr('data-field'));
         $('#fieldValue').val($(this).val());
     }
     // se llama la funcion del controller
     $('#idForm').attr('action',
         'consultaPagos.do');
     $('#idForm').submit();
     }
     return true;
       });
       
       /** Evento exportar todos **/
       $('#idExportarTodo').click(function(){            
            $('#idForm').attr('action', 'exportarTodoPagos.do');
            $('#accion').val('ACT');
            $('#idForm').submit(); 
       });
       
       /** Evento regresar **/
       $('#idRegresar').click(function(){        
           $('#tipoRef').val(tipoOrden);
           $('#idForm').attr('action', 'consultaPagos.do');
           $('#idForm').submit();
       });
       /** Evento regresar al monitor **/
       $('#idRegresarMain').click(function(){        
           $('#idForm').attr('action', $('#path').val().substring(1));
           $('#idForm').submit();
       });
       /** Evento ver historial de mensajes **/
       $('#idHistoriaMensaje').click(function(){    
           $('#tipoRef').val(tipoOrden);
           $('#ref').val(referencia);
           $('#idForm').attr('action', 'consultaHistorial.do');
           $('#idForm').submit();
       });
       
       /** Evento de detalle **/
       $('.detalle').click(function(){
           var referencia = $(this).attr('data-ref');
           $('#ref').val(referencia);
           $('#idForm').attr('action', 'consultaDetallePago.do');
           $('#idForm').submit();
       });
       /** Evento para aceptar numeros**/
       $('#refeInput').on('keypress change focus', function(e){
           return soloNumeros(e);
       });
       
       /** Evento actualizar para los pagos **/
       $('#idActualizar').click(function(){
            /** Se declara varaible inicio*/
$('#paginaIni').val('1');
    	    $('#field').val('');
            /** Se declara el valor*/
            $('#fieldValue').val('');
            
            /** Se agrega la accion para actualizar pagos*/
            $('#idForm').attr('action', 'consultaPagos.do');
            $('#idForm').submit();
        });
    }
};

/** Funcion solo numeros en la entrada **/
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
        e.preventDefault();
    }
    /** Validador de copiado mediante el historial */
    var numero = $('#refeInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#refeInput').val('');
    }
}
/** Inicializacion de la funcion general **/
$(document).ready(consultaPagos.init);