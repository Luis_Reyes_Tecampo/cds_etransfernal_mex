reporteDia= {
        lock:0,	
        init:function(){
        	document.body.style.cursor = "default";
			reporteDia.activarCps();
			$('#idBuscar').click(function(){
				document.body.style.cursor = "wait";
				var varHtos = $("input[name='historico']").val();
				var valFs = $('#fchOperacion').val();
				reporteDia.validar();
				
				if(varHtos==="1" &&  valFs.length<1 ){
					jAlert(mensajes["ED00003V"],
							mensajes["aplicacion"],
						   	   'ED00003V',
						   	   '');
				}else if(varHtos==="1" && reporteDia.validarFecha()){
					jAlert(mensajes["ED00004V"],
							mensajes["aplicacion"],
						   	   'ED00004V',
						   	   '');
				}else{
					$('#idForm').attr('action','bucarSPEICero.do');		
					$('#idForm').submit();	
				}
				document.body.style.cursor = "default";
			});
			
			$('#idExportar').click(function(){	
				document.body.style.cursor = "wait";
			 	reporteDia.validar();
				var form = document.getElementById("idForm");
					form.action="exportarSPEICer.do";
				form.submit();
				document.body.style.cursor = "default";
			});
			 
			$('#idEdoCta').click(function(){
				document.body.style.cursor = "wait";
				var val = $("input[name='todoFlt']:checked").val();
				if(!val){
					reporteDia.validar();
					var form = document.getElementById("idForm");
						form.action="edoCtaSPEICer.do";
					form.submit();
					document.body.style.cursor = "default";
			 	}else{
					jAlert(mensajes["ED00002V"],
							mensajes["aplicacion"],
						   	   'ED00002V',
						   	   '');
				}
			});
			 
		    $('#idRegresar').click(function(){
				$('#idForm').attr('action','moduloSPEICero.do');
				$('#idForm').submit();
			});
			    
			    
		
		},
		activarCps:function(){
			var val = $("input[name='todoFlt']:checked").val();
			
			if(val){					
				document.getElementById('institucionA').disabled  = true;
				document.getElementById('institucionB').disabled  = true;
				document.getElementById('institucionC').disabled  = true;
				document.getElementById('institucionD').disabled  = true;
				
				document.getElementById('todoFlt').checked  = true;
				
			}else{
				document.getElementById('institucionA').disabled  = false;
				document.getElementById('institucionB').disabled  = false;
				document.getElementById('institucionC').disabled  = false;
				document.getElementById('institucionD').disabled  = false;
				
				document.getElementById('todoFlt').checked  = false;
			}
	    },
	    
	    validar:function(){
	    	var val =  $("input[name='todoFlt']:checked").val();
			var varHto = $("input[name='historico']").val();
			var valOpcion = $("input[name='institucion']:checked").val();
			var valOpcionCero = $("input[name='institucion2']:checked").val();
			var cero = "0";
			var uno ="1";
			
			$('#opcion').val(valOpcion);
			$('#opcionCero').val(valOpcionCero);
			
			if(varHto==="1"){
				var valF = $('#fchOperacion').val();
				$('#horaFecOperacion').val(valF);					
			}
			
			if(val){	
				$('#rptTodo').val(uno);
			}else{
				$('#rptTodo').val(cero);
			}
	    },
	    
	    validarFecha:function(){	   
	    	var fechaValida = true;
	    	/** Obtiene la fecha de consulta*/
	    	var fecha_aux = document.getElementById("fchOperacion").value.split("/");
	     	var fechaConsulta = new Date(parseInt(fecha_aux[2]),parseInt(fecha_aux[1]-1),parseInt(fecha_aux[0]));
	    	var anioConsulta = fechaConsulta.getFullYear();
	    	var mesConsulta = fechaConsulta.getMonth();
	    	var diaConsulta = fechaConsulta.getDate();
	    	
	    	/** Obtiene la fecha actual*/
	    	var fechaActual = new Date();
	    	var anioActual = fechaActual.getFullYear();
	    	var mesActual = fechaActual.getMonth();
	    	var diaActual = fechaActual.getDate();
	    	 
	    	if (anioActual > anioConsulta){
	    		fechaValida = false;
	    	}
	    	if (fechaValida && mesActual > mesConsulta){
	    		fechaValida = false;			
	    	}
	    	if (fechaValida && diaActual > diaConsulta){
	    		fechaValida = false;
	        }
	    	return fechaValida;
	    }
};

$(document).ready(reporteDia.init);