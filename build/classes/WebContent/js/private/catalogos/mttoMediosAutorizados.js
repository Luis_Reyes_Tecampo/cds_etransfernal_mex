/**
 * Funciones activas al cargar la pagina
 * 
 * @returns
 */
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;

var formulario = "idForm";
var lista = [];

const PARAMC = '#paramInicio';

var listadoMtto = {
    init : function() {
    $('.filField').keypress(function(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        cadena = $(this).val();
        cadenaLength = cadena.length;
        cadenaLenghtMenos = cadenaLength - 1;
        cadenaAux = cadena.substring(0, cadenaLenghtMenos);

        arrLeng;
        if (charCode === 8 || charCode === 37 || charCode === 39) {
        return true;
        }
        /** si detecta el enter entra a buscar* */
        if (charCode === 13) {
        if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
            $('#field').val($(this).attr('data-field'));
            $('#fieldValue').val($(this).val());
        }
        $('#accion').val('INI');
        $('#idForm').attr('action', 'mttoMediosAutorizados.do');
        $('#idForm').submit();
        }
    });
    $('.enfocaCombo').on('change',function() {
            cadena = $(this).val();
            if ($(this).attr('data-field') !== '') {
                $('#field').val($(this).attr('data-field'));
                $('#fieldValue').val(cadena);
                $('#pagina').val('1');
            $('#idForm').attr('action', 'mttoMediosAutorizados.do');
            $('#idForm').submit();
            }
            return true; 
        });
    
    $('#idEliminar').click(function() {
        eliminarParametros();
    });

    $('#idMedioNoSeguro').click(function() {
        desaseguraMedios();
    });

    $('#idMedioSeguro').click(function() {
        aseguraMedios();
    });

    $('#chkTodos').click(function() {
        var objCheckBoxes = $("[type=checkbox]");
        if ($('#chkTodos').is(':checked')) {
        for (var i = 1; i < objCheckBoxes.length; i++) {
            objCheckBoxes[i].checked = true;
            verificarSeleccion(i - 1);
        }
        } else {
        for (var j = 1; j < objCheckBoxes.length; j++) {
            objCheckBoxes[j].checked = false;
            verificarSeleccion(i - 1);
        }
        }
    });
    }
};

function desaseguraMedios() {
    if (lista.length > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action',
                'mediosAutorizadosSeguros.do?seguros=false');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"], 'ED00068V', '');
    return;
    }
}

function aseguraMedios() {
    if (lista.length > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action',
                'mediosAutorizadosSeguros.do?seguros=true');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"], 'ED00068V', '');
    return;
    }
}

function verificarSeleccion(index) {
    if ($('#seleccionado' + index).is(':checked')) {
    lista.push('1');
    } else {
    lista.pop();
    }
    
    
}

/** funcion para confirmacion de eliminacion */
function eliminarParametros() {

    if (lista.length > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action', 'eliminarMttoMedios.do');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"], 'ED00068V', '');
    return;
    }
}

/**
 * Funcion que corta el valor de la opcion seleccionada y la retorna
 * 
 * @param comboObj
 * @returns
 */
function obtenDescripcion(comboObj) {
    var descripcion = null;
    var topeInicial = 0;
    var topeFinal = 0;
    descripcion = $('#' + comboObj).val();
    topeInicial = descripcion.indexOf('[');
    topeFinal = descripcion.indexOf(']');
    descripcion = descripcion.slice(topeInicial + 1, topeFinal);
    return descripcion;
}

/**
 * Metodo para realizar la validacion y envio de datos al controller de alta
 * 
 * @returns
 */
function altaMediosAutorizados() {

    $('#idGuardar')
        .on(
            'click',
            function(e) {
            jConfirm(
                mensajes["CD00010V"],
                mensajes["aplicacion"],
                'CD00010V',
                '',
                function(respuesta) {
                    if (respuesta) {
                    e.stopPropagation();
                    e.preventDefault();

                    var resValidation = validarFrm('mform');

                    resValidation.mensajes = validaHorario(resValidation.mensajes);

                    if (resValidation.mensajes === '') {
                        preparaEnvio('addMttoMedios.do');
                    } else {
                        showModal('error',
                            resValidation.mensajes,
                            'Datos No Validos', '', '');
                    }
                    }
                });
            });
}

/**
 * Procedimiento para validar el los campos de Inicio y Cierre
 * 
 * @param mensajes
 * @returns
 */
function validaHorario(mensajes) {
    var mensajeRetorno = mensajes;
    if ($(PARAMC).val() !== null && $(PARAMC).val() !== '') {
    var valorInicio = $(PARAMC).val();
    var valorCierre = $('#paramCierre').val();
    mensajeRetorno = validarHora(mensajeRetorno, valorInicio, valorCierre);
    }
    return mensajeRetorno;
}

/**
 * Procedimiento hacer las validaciones necesarias en los campos y obtener los
 * mensajes de error
 * 
 * @param mensajes
 * @param valorInicio
 * @param valorCierre
 * @returns
 */
function validarHora(mensajes, valorInicio, valorCierre) {
    var mensajeRetorno = mensajes;
    if (valorInicio >= valorCierre) {
    mensajeRetorno += "El campo Inicio no puede ser mayor o igual al campo Cierre."
        + "#";
    addError(PARAMC);
    }
    if (valorInicio.length !== 4) {
    mensajeRetorno += "El campo Inicio no es un formato de hora v\u00e1lido: 0000"
        + "#";
    addError(PARAMC);
    }
    if (valorCierre.length !== 4) {
    mensajeRetorno += "El campo Cierre no es un formato de hora v\u00e1lido: 0000"
        + "#";
    addError('#paramCierre');
    }
    if (valorInicio < 0000 || valorInicio > 2359) {
    mensajeRetorno += "El campo Inicio no esta en un rango de hora v\u00e1lido: 0000 - 2359"
        + "#";
    addError(PARAMC);
    }
    if (valorCierre < 0000 || valorCierre > 2359) {
    mensajeRetorno += "El campo Cierre no esta en un rango de hora v\u00e1lido: 0000 - 2359"
        + "#";
    addError('#paramCierre');
    }
    return mensajeRetorno;
}

/**
 * Procedimiento para agregar el estilo de error a los campos
 * 
 * @param obj
 * @returns
 */
function addError(obj) {
    $(obj).addClass('error-obj');
}
/**
 * Metodo para realizar la validacion y el envio de datos al controller de
 * modificacion
 * 
 * @returns
 */
function modMediosAutorizados() {

    $('#idEditar').on(
        'click',
        function() {

        if (lista.length === 0) {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            return;
        } else if (lista.length === 1) {
            $('#idForm').attr('action', 'editMttoMedios.do');
            $('#idForm').submit();
        }
        if (lista.length > 1) {
            jAlert(mensajes["ED00029V"], mensajes["aplicacion"],
                'ED00029V', '');
            return;
        }
        });

}

/**
 * Procedimiento para quitar la clase de error cuando un select obligatorio
 * tenga el evento activo y sea enfocado
 * 
 * @returns
 */
function enfocarCombo() {
    $('.focusCombo').on('focus', function(e) {
    $('#' + e.target.id).removeClass('error-obj');
    });
}

function desactivarCombos() {
    var objContainer = '#mform';
    $(objContainer).find('.focusCombo').each(function(i, objeto) {
    $(objeto).attr('disabled', true);
    $(objeto).addClass('inactivoCombo');
    });
}
/**
 * Procedimiento que envia el formulario de hacia el controlador de alta
 * 
 * @param pathWeb
 * @returns
 */
function preparaEnvio(pathWeb) {

    if ($('#paramSucOperante').val() === "") {
    $('#paramSucOperante').val(0);
    }
    $('#mform').attr('action', pathWeb);
    $('body #mform').submit();
}

/**
 * Procedimiento para recargar los datos de la tabla cuando se realiza una
 * busqueda
 * 
 * @returns
 */
function recargarDatos() {
    $('#idActualizar').on('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    $('#idForm').attr('action', 'mantenimientoMediosAutorizados.do');
    $('#idForm').submit();
    });
}

function exportar() {
    $('#idExportar').on('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    $('#idForm').attr('action', 'exportarMttoMedios.do');
    $('#idForm').submit();
    });
}

/**
 * Se declaran las funciones que se ejecutan al cargar la pagina
 * 
 * @returns
 */
$(document).ready(function() {
    listadoMtto.init();
    enfocarCombo();
    altaMediosAutorizados();
    modMediosAutorizados();
    recargarDatos();
    exportar();
});