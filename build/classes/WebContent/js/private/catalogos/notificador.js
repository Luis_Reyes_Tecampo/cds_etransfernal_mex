/** Declaracion de las variables a utilizar * */
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

/** Se declaran las funciones* */
var notificador = {
    init : function() {
    /** Evento exportar * */
    $('#btnExportar').click(function() {
        $('#accion').val('ACT');
        $('#idForm').attr('action', 'exportarNotificador.do');
        $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
        $('#idForm').attr('action', 'exportarTodoNotificador.do');
        $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
        $('#paginaIni').val('1');
        $('#field').val('');
        $('#fieldValue').val('');
        $('#idForm').attr('action', 'consultaNotificador.do');
        $('#idForm').submit();
    });
    /** Evento eliminar * */
    $('#btnEliminar').click(
        function() {
            var checkboxCount = 0;
            $('input[id="check"]:checked').each(function() {
            checkboxCount++;
            });
            if (checkboxCount > 0) {
            jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
                'CD00010V', '', function(respuesta) {
                    if (respuesta) {
                    $('#idForm').attr('action',
                        'eliminarNotificador.do');
                    $('#idForm').submit();
                    }
                });
            } else {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            }
        });

    /** Eventos del filtro de busqueda* */
    $('.focusCombo').on('change',function() {
        cadena = $(this).val();
        if ($(this).attr('data-field') !== '') {
            $('#field').val($(this).attr('data-field'));
            $('#fieldValue').val(cadena);
            $('#pagina').val('1');
        $('#idForm').attr('action', 'consultaNotificador.do');
        $('#idForm').submit();
        }
        return true; 
    });
    /** Evento guardar* */ 
    $('#btnGuardar').on('click', function(e) {
        continuaGuardando(e);
    });
    /** Evento para remover clases* */
    $('.focusCombo').on('focus', function(e) {
        $('#' + e.target.id).removeClass('error-obj');
    });
    /** Evento editar* */
    $('#btnEditar').click(
        function() {
            /** Validacion de la seleccion * */
            var checkboxCount = 0;
            $('input[id="check"]:checked').each(function() {
            checkboxCount++;
            });
            if (checkboxCount === 1) {
            $('#idForm').attr('action',
                'mantenimientoNotificador.do');
            $('#idForm').submit();
            } else if (checkboxCount === 0) {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            } else {
            jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
                'ED00126V', '');
            }
        });
    $('#btnAgregar').click(function() {
        $('#idForm').attr('action', 'mantenimientoNotificador.do');
        $('#idForm').submit();
    });
    $('#btnReturn').click(function() {
        $('#mform').attr('action', 'consultaNotificador.do');
        $('#mform').submit();
    });
    }
};
/** Funcion para continuar la funcion inicial * */
function continuaGuardando(e) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
        if (respuesta) {
            e.stopPropagation();
            e.preventDefault();
            var resValidation = validarFrm('mform');
            if (resValidation.seguir) {
            preparaEnvio();
            } else {
            showModal('error', resValidation.mensajes,
                'Datos No Validos', '', '');
            }
        }
        });
}
/** Funcion para enviar los datos al controller * */
function preparaEnvio() {
    /** Validacion * */
    if ($('#accion').val() === 'alta') {
    $('#mform').attr('action', 'agregarNotificador.do');
    } else {
    $('#mform').attr('action', 'editarNotificador.do');
    }
    /** Envio del formulario * */
    $('body #mform').submit();
}

/** Inicializacion de las funciones* */
$(document).ready(notificador.init);