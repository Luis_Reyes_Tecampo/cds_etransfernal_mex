/** Declaracion de las variables a utilizar * */
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var arrLeng = arr.length;
var carVal;
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];
/** **
 * 
 * Funciones declaradas
 * para los flujos
/** Se declaran las funciones* */
var cuentasFideico = {
    init : function() {

    /** Evento exportar * */
    $('#btnExportar').click(function() {
    $('#accion').val('ACT');
    $('#idForm').attr('action', 'exportarCuentasFiduciario.do');
    $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
    $('#idForm').attr('action', 'exportarTodoCuentasFiduciario.do');
    $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
    $('#paginaIni').val('1');
    $('#field').val('');
    $('#fieldValue').val('');
    $('#idForm').attr('action', 'consultaCuentasFiduciario.do');
    $('#idForm').submit();
    });
    /** Eventos del filtro de busqueda* */
    $('.filField').keypress(function(e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    cadena = $(this).val();
    cadenaLength = cadena.length;
    cadenaLenghtMenos = cadenaLength - 1;
    cadenaAux = cadena.substring(0, cadenaLenghtMenos);
    if (charCode === 8 || charCode === 37 || charCode === 39) {
    return true;
    }
    if (charCode === 13) {
    if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
        $('#field').val($(this).attr('data-field'));
        $('#fieldValue').val($(this).val());
        $('#pagina').val('1');
    }
    $('#idForm').attr('action', 'consultaCuentasFiduciario.do');
    $('#idForm').submit();
    }
    return true;
    });
    $('#cuentaInput').on('keypress copy change focus', function(e){
    return soloNumeros(e);
    });
    $('#cuentaInput').on('paste', function(){
    validaPaste();
    });
    }
};

/** Funcion solo numeros en la entrada **/
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
    e.preventDefault(); 
    }
    var numero = $('#cuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#cuentaInput').val('');
    }
}
function validaPaste() {
    var numero = $('#numCuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#numCuentaInput').val('');
    }
}
/** Inicializacion de las funciones* */
$(document).ready(cuentasFideico.init);