var formulario = "idForm";
var lista = [];
mantenimientoParametros = {
        regNum : new RegExp("^([0-9]{1,40})$"),
        inicioMantenimientoParametros:function(){
            $('#idAlta').click(function(){
                evaluarAccionPaginador();
                var form = document.getElementById(formulario);
                form.action="altaParametroNuevo.do";
                form.submit();
            });

            $('#idEditar').click(function(){
                editarParametro();
            });

            $('#idEliminar').click(function(){
                eliminarParametros();
            });

            $('#idExportar').click(function(){
                $('#accion').val('ACT');
                var form = document.getElementById(formulario);
                form.action="exportarParametros.do";
                form.submit();
            });

            $('#idExportarTodo').click(function(){
                $('#accion').val('ACT');
                var form = document.getElementById(formulario);
                form.action="exportarTodoParametros.do";
                form.submit();
            });

            $('#idBuscar').click(function(){
                document.body.style.cursor = "wait";
                var form = document.getElementById(formulario);
                form.action="consultarParametros.do";
                $('#accion').val('ACT');
                $('#pagina').val(1);
                $("#parametro").val($("#idParametro").val());
                $("#descripParam").val($("#idDescripParam").val());
                $("#tipoParametro").val($("#idTipoParametro").val());
                $("#tipoDato").val($("#idTipoDato").val());
                $("#longitudDato").val($("#idLongitudDato").val());
                $("#valorFalta").val($("#idValorFalta").val());
                $("#trama").val($("#idTrama").val());
                $("#cola").val($("#idCola").val());
                $("#mecanismo").val($("#idMecanismo").val());
                $("#posicion").val($("#idPosicion").val());
                $("#parametroFalta").val($("#idParamFalta").val());
                $("#programa").val($("#idPrograma").val());
                form.submit();
            });

            $('#idLimpiar').click(function(){
                hacerSubmit('idForm', 'muestraMantenimientoParametros.do');
            });

            mantenimientoParametros.validacionCaracteresNumericos();
       },

       validacionCaracteresNumericos:function(){
           $('#idLongitudDato').keypress(function(e){
               mantenimientoParametros.soloNumeros('#idLongitudDato', mantenimientoParametros.regNum, 0, e);
           });

           $('#idPosicion').keypress(function(e){
               mantenimientoParametros.soloNumeros('#idPosicion', mantenimientoParametros.regNum, 0, e);
           });
       },
       
       soloNumeros:function(id, expReg, long, e){
           var cad, k;
           if (document.all) {
               k = e.keyCode;
           } else {
               k = e.which;
           }
           if(k !== 8){
               mantenimientoParametros.cancel(e);
               cad = $(id).val()+String.fromCharCode(k);
               if(!(cad ==="") && !expReg.test(cad) && k !== long){
                   e.preventDefault();
                   e.stopPropagation();
               }
           }
       },
        
       cancel:function(ev){
           if (document.all) {
               k = ev.keyCode;
           } else {
               k = ev.which;
           }
           if(!((k>=46 && k<=57) || (k===190 )) ){
               ev.preventDefault();
               ev.stopPropagation();
           }else if(k===107 || k===109 || k=== 111 || k===18){
               ev.preventDefault();
               ev.stopPropagation();
           }
       }
};

$(document).ready(mantenimientoParametros.inicioMantenimientoParametros);

function verificarSeleccion(index){
    var keyParam = $('#cveParamSelect'+index).val();
    if( $('#seleccionado'+index).is(':checked') ) {
        lista.push(keyParam);
    } else {
        lista.pop();
    }
}

function editarParametro(){
    var form = document.getElementById(formulario);
    if(lista.length === 0){
        jAlert(mensajesMov["ED00125V"],mensajesMov["aplicacion"],'ED00125V','');
        return;
    }else if(lista.length > 1){
        jAlert(mensajesMov["ED00126V"],mensajesMov["aplicacion"],'ED00126V','');
        return;
    }else{
        $('#accion').val('ACT');
        form.action="editarParametros.do";
        form.submit();
    }
}

function eliminarParametros(){
    $('#accion').val('ACT');
    if(lista.length>0){
        jConfirm(mensajesMov["CD00010V"],mensajesMov["aplicacion"],'CD00010V','', function (respuesta){
            if(respuesta){
                $('#idForm').attr('action','eliminarParametros.do');
                $('#idForm').submit();
            }
        });
    }else{
        jAlert(mensajesMov["ED00068V"],mensajesMov["aplicacion"],'ED00068V','');
        return;
    }
}

function onChangeComboDefineTrama(){
    $('#idTrama').change(function(){
        if(this.value === "S"){
            $('#idPosicion').removeAttr('disabled');
            $('#idParamFalta').removeAttr('disabled');
        } else {
            $('#idParamFalta').attr('disabled', 'disabled');
            $('#idPosicion').attr('disabled', 'disabled');
            $('#idPosicion').val('');
            $('#idParamFalta').val('');
        }
    });
}

function onChangeComboDefineCola(){
    $('#idCola').change(function(){
        if(this.value === "S"){
            $('#idPrograma').removeAttr('disabled');
        } else {
            $('#idPrograma').attr('disabled', 'disabled');
            $('#idPrograma').val('');
        }
    });
}

function evaluarAccionPaginador(){
    if( $('#accion').val() !== 'ACT' &&  $('#accion').val() !== '' ){
        $('#accion').val('ACT');
    }
}