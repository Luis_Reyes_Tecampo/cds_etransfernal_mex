var catIntFinancierosHis = {
init : function() {
createCalendarGuion('paramFchModifHV','cal1');
/***
 * Evento Click ExportarTodo
 * hace el llamado para realizar la accion para
 * exportar todos los registros que correspondan a
 * la instancia en la cual se esta posicionado
 */
$('#btnExportarTodo').click(function() {
$('#idForm').attr('action','exportarTodoIntFinancierosHis.do');
$('#idForm').submit();
});

/***
 * Evento Click Limpiar
 * hace el llamado para realizar la funcion cporrecpondiente 
 * a limpiar o refrescar la pantalla actual
 */
$('#idLimpiar').click(function() {
$('#idForm').attr('action','muestraCatIntFinancierosHis.do');
$('#idForm').submit();
});

/***
 * Evento Click Buscar
 * hace el llamado para realizar la consulta a la BD
 * para mostrar los registros correspondientes
 */
$('#idBuscar').click(function() {
$('#cveInterme').val($('#paramCveIntermeHV').val());
$('#fchModif').val($('#paramFchModifHV').val());
$('#tipoModif').val($('#paramTipoModifHV').val());
$('#usuario').val($('#paramUsuarioModifHV').val());
$('#tipoIntermeOri').val($('#paramTipoIntermeHV').val());
$('#numCecobanOri').val($('#paramNumCecobanHV').val());
$('#numBanxicoOri').val($('#paramNumBanxicoHV').val());
$('#nombreCortoOri').val($('#paramNombreCortoHV').val());
$('#nombreLargoOri').val($('#paramNombreLargoHV').val());
$('#accion').val('ACT');
$('#idForm').attr('action','consultaCatIntFinancierosHis.do');
$('#idForm').submit();
});

/***
 * Evento Click Exportar
 * hace el llamado para realizar que se genere un Excel 
 * con los registros mostrados en pantalla
 */
$('#idExportar').click(function() {
$('#idForm').attr('action','exportarHistorico.do');
$('#idForm').submit();
});

}
};
/***
 * Escucha la peticion para inicializar 
 * el evento segun sea el caso
 */
$(document).ready(catIntFinancierosHis.init);
