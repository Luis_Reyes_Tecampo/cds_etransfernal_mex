altaCatInstFinancieras = {
	init:function(){	   
		$('#idAceptar').click(function(){
			valores = $(":selected").val();
			
			if(valores){
				valoresArr = valores.split('-');
				$('#numBanxico').val(valoresArr[0]);
				$('#cveInterme').val(valoresArr[1]);
				$('#nombre').val(valoresArr[2]);
				$('#idForm').attr('action','altaCatInstFinancieras.do');
				$('#idForm').submit();
			}else{
				jAlert(mensajes["ED00086V"],
						mensajes["aplicacion"],
					   	   'ED00086V',
					   	   '');
			}
		});
		
		$('#idCancelar').click(function(){
			
			jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V','', function (respuesta){
				if(respuesta){
					$('#idForm').attr('action','muestraCatInstFinancieras.do');
					$('#idForm').submit();
				} 
			});
		});

	}

};

$(document).ready(altaCatInstFinancieras.init);