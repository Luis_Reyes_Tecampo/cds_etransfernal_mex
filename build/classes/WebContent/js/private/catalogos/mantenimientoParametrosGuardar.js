var formulario = "idFormG";
mantenimientoParametrosGuardar = {
        regNum : new RegExp("^([0-9]{1,40})$"),
        inicioMantenimientoParametrosGuardar:function(){
            $('#idGuardarG').click(function(){
                jConfirm(mensajesMov["CD00010V"],mensajesMov["aplicacion"],'CD00010V','', function (respuesta){
                    if(respuesta){
                        guadarParametro();
                    }
                });
            });

            $('#idCancelarG').click(function(){
                var form = document.getElementById("idFormG");
                form.action="cancelarGuardado.do";
                form.submit();
            });

            $('#idLongitudDatoG').keypress(function(e){
                mantenimientoParametrosGuardar.soloNumeros('#idLongitudDatoG', mantenimientoParametrosGuardar.regNum, 0, e);
            });

            $('#idPosicionG').keypress(function(e){
                mantenimientoParametrosGuardar.soloNumeros('#idPosicionG', mantenimientoParametrosGuardar.regNum, 0, e);
            });

            mantenimientoParametrosGuardar.inicioMantenimientoParametrosGuardar2();
       },

        inicioMantenimientoParametrosGuardar2:function(){
           $('#idLimpiarG').click(function(){
               validarEsPantallaAlta();
               $('#idPosicionG').val('');
               $('#idProgramaG').val('');
               $('#idValorFaltaG').val('');
               $('#idParamFaltaG').val('');
               $('#idDescripcionG').val('');
               $('#idLongitudDatoG').val('');
               $('#idPosicionG').attr('disabled', 'disabled');
               $('#idProgramaG').attr('disabled', 'disabled');
               $('#idParamFaltaG').attr('disabled', 'disabled');
               $('#idTipoDatoG > option[value=""]').attr('selected', 'selected');
               $('#idTipoParametroG > option[value=""]').attr('selected', 'selected');
               $('#idColaG > option[value=""]').attr('selected', 'selected');
               $('#idTramaG > option[value=""]').attr('selected', 'selected');
               $('#idMecanismoG > option[value=""]').attr('selected', 'selected');
           });

           /**
            * Evento de change seleccion de trama
            */
           $('#idTramaG').change(function(){
               if(this.value === "S"){
                   $('#idPosicionG').removeAttr('disabled');
                   $('#idParamFaltaG').removeAttr('disabled');
               } else {
                   $('#idPosicionG').val('');
                   $('#idParamFaltaG').val('');
                   $('#idPosicionG').attr('disabled', 'disabled');
                   $('#idParamFaltaG').attr('disabled', 'disabled');
               }
           });

           /**
            * Evento de change seleccion de cola
            */
           $('#idColaG').change(function(){
               if(this.value === "S"){
                   $('#idProgramaG').removeAttr('disabled');
               } else {
                   $('#idProgramaG').val('');
                   $('#idProgramaG').attr('disabled', 'disabled');
               }
           });
       },

       soloNumeros:function(id, expReg, long, e){
           var cad, k;
           if (document.all) {
               k = e.keyCode;
           } else {
               k = e.which;
           }
           if(k !== 8){
               mantenimientoParametrosGuardar.cancel(e);
               cad = $(id).val()+String.fromCharCode(k);
               if(!(cad ==="") && !expReg.test(cad) && k !== long){
                   e.preventDefault();
                   e.stopPropagation();
               }
           }
       },

       cancel:function(e){
           if (document.all) {
               k = e.keyCode;
           } else {
               k = e.which;
           }
           if(!((k===190 ) || (k>=46 && k<=57)) ){
               e.preventDefault();
               e.stopPropagation();
           }else if(k===107|| k===109 || k=== 111 || k===18){
               e.preventDefault();
               e.stopPropagation();
           }
       }
};

$(document).ready(mantenimientoParametrosGuardar.inicioMantenimientoParametrosGuardar);

function guadarParametro(){
    if(Utilerias.isEmpty('idParametroG') || Utilerias.isEmpty('idValorFaltaG')){
        jAlert(mensajesMov["ED00021V"],mensajesMov["aplicacion"],'ED00021V','');
        return;
    }
    $('#parametro').val($('#idParametroG').val());
    $('#descripParam').val($('#idDescripcionG').val());
    $('#tipoParametro').val($('#idTipoParametroG').val());
    $('#tipoDato').val($('#idTipoDatoG').val());
    $('#longitudDato').val($('#idLongitudDatoG').val());
    $('#valorFalta').val($('#idValorFaltaG').val());
    $('#trama').val($('#idTramaG').val());
    $('#cola').val($('#idColaG').val());
    $('#mecanismo').val($('#idMecanismoG').val());
    $('#posicion').val($('#idPosicionG').val());
    $('#parametroFalta').val($('#idParamFaltaG').val());
    $('#programa').val($('#idProgramaG').val());

    $('#idFormG').attr('action','guardarParametro.do');
    $('#idFormG').submit();
}

function validarEsPantallaAlta(){
    if( $('#altaParametro').val() === 'true' ){
        $('#idParametroG').val('');
    }
}