/** Declaracion de las variables a utilizar * */
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var lista = [];

/** Se declaran las funciones* */
var cuentasFideico = {
    init : function() {
    $('#idCargar').click(function() {
        $('#idForm').attr('action', 'cargaArchivos.do');
        $('#idForm').submit();
    });
    /** Evento exportar * */
    $('#btnExportar').click(function() {
        $('#idForm').attr('action', 'exportarExcepCuentasFideicomiso.do');
        $('#idForm').submit();
    });
    /** Evento exportar todos los registros * */
    $('#btnExportarTodo').click(function() {
        $('#idForm').attr('action', 'exporTodoExcepCuentasFideicomiso.do');
        $('#idForm').submit();
    });
    /** Evento actualizar * */
    $('#btnActualizar').click(function() {
        $('#paginaIni').val('1');
        $('#numCuentaInput').val('');
        $('#idForm').attr('action', 'excepCuentasFideicomiso.do');
        $('#idForm').submit();
    });
    /** Evento eliminar * */
    $('#btnEliminar').click(
        function() {
            continuaEliminando();
        });
    /** Eventos del filtro de busqueda* */
    $('.filField').keypress(function(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        cadena = $(this).val();
        cadenaLength = cadena.length;
        cadenaLenghtMenos = cadenaLength - 1;
        cadenaAux = cadena.substring(0, cadenaLenghtMenos);
        if (charCode === 8 || charCode === 37 || charCode === 39) {
        return true;
        }
        if (charCode === 13) {
        if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
            $('#field').val($(this).attr('data-field'));
            $('#fieldValue').val($(this).val());
            $('#pagina').val('1');
        }
        $('#idForm').attr('action', 'excepCuentasFideicomiso.do');
        $('#idForm').submit();
        }
        return true;
    });
    /** Evento guardar* */
    $('#btnGuardar').on('click', function(e) {
        continuaGuardando(e);
    });
    /** Evento para remover clases* */
    $('.focusCombo').on('focus', function(e) {
        $('#' + e.target.id).removeClass('error-obj');
    });
    /** Evento editar* */
    $('#btnEditar').click(
        function() {
    $('#numCuentaInput').val("");
            /** Validacion de la seleccion * */
            var checkboxCount = 0;
            $('input[id="check"]:checked').each(function() {
            checkboxCount++;
            });
            if (checkboxCount === 1) {
            $('#idForm').attr('action', 'cambioExcepCuentasFideicomiso.do');
            $('#idForm').submit();
            } else if (checkboxCount === 0) {
            jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
                'ED00068V', '');
            } else {
            jAlert(mensajes["ED00126V"], mensajes["aplicacion"],
                'ED00126V', '');
            }
        });
    $('#btnReturn').click(function() {
    $('#paramCuenta').val("");
    $('#valorOLdtitu').val("");
    $('#valorOLdComen').val("");
    $('#valorOLd').val("");
        $('#mform').attr('action', 'excepCuentasFideicomiso.do');
        $('#mform').submit();
    });
    $('#btnAgregar').click(function() {
    $('#numCuentaInput').val("");
        $('#idForm').attr('action', 'altaExcepCuentasFideicomiso.do');
        $('#idForm').submit();
    });
    $('#numCuentaInput').on('keypress copy change focus', function(e){
    return soloNumeros(e);
    });
    $('#numCuentaInput').on('paste', function(){
    validaPaste();
    });
    }
};

/** Funcion solo numeros en la entrada **/
function soloNumeros(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
        e.preventDefault();
    }
    var numero = $('#numCuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#numCuentaInput').val('');
    }
}

function validaPaste() {
    var numero = $('#numCuentaInput').val();
    if (!/^([0-9])*$/.test(numero)){
     $('#numCuentaInput').val('');
    }
}
/** Funcion para continuar la funcion inicial * */
function continuaGuardando(e) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"], 'CD00010V', '',
        function(respuesta) {
        if (respuesta) {
            e.stopPropagation();
            e.preventDefault();
            var resValidation = validarFrm('mform');
            if (resValidation.seguir) {
            preparaEnvio();
            } else {
            showModal('error', resValidation.mensajes,
                'Datos No Validos', '', '');
            }
        }
        });
}

/** Funcion para continuar la funcion inicial * */
function continuaEliminando() {
    var checkboxCount = 0;
    $('input[id="check"]:checked').each(function() {
    checkboxCount++;
    });
    if (checkboxCount > 0) {
    jConfirm(mensajes["CD00010V"], mensajes["aplicacion"],
        'CD00010V', '', function(respuesta) {
            if (respuesta) {
            $('#idForm').attr('action','bajaExcepCuentasFideicomiso.do');
            $('#idForm').submit();
            }
        });
    } else {
    jAlert(mensajes["ED00068V"], mensajes["aplicacion"],
        'ED00068V', '');
    }
}
/** Funcion para enviar los datos al controller * */
function preparaEnvio() {
    /** Validacion * */
    if ($('#accion').val() === 'alta') {
    $('#mform').attr('action', 'altaExcepCuentasFideicomiso.do');
    } else {
    $('#mform').attr('action', 'cambioExcepCuentasFideicomiso.do');
    }
    /** Envio del formulario * */
    $('body #mform').submit();
}

/** Inicializacion de las funciones* */
$(document).ready(cuentasFideico.init);