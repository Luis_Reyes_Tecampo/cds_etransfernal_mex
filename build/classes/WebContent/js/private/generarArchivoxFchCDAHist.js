generarArchivoxFchCDAHist = {
		copiar:false,
		init:function(){
	        createCalendarGuion('fechaOp','cal1');			
			$('#idProcesar').click(function(){
				if(Utilerias.isEmpty('fechaOp')){
					jAlert(mensajes["ED00025V"],mensajes["aplicacion"],'ED00025V','');
					return false;
				}	
				strFechaBanMex=$('#fechaOperacionBancoMex').val();
				dFechaBanMex = Utilerias.stringToDate(strFechaBanMex);				
				strFechaOp=$('#fechaOp').val();
				dFechaOp = Utilerias.stringToDate(strFechaOp);
				if(dFechaBanMex.getTime()>dFechaOp.getTime()){
					nextPage('idForm','procesarGenerarArchCDAxFchHist.do','');
				}else{
					jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');

				}
			});
			
			
		},
		procesar:function(){
			if(Utilerias.isEmpty('fechaOp')){
				jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');
			};
		}		
	};

$(document).ready(generarArchivoxFchCDAHist.init);