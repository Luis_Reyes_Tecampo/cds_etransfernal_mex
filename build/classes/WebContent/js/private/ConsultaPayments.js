var catConsultaPayment = {
init : function() {
/** 
* Evento que exportar a excel los elementos 
* presentados por el paginador 
*/	
$('#btnExportar').click(function() {
$('#accion').val('ACT');
$('#mform').attr('action', 'exportarPayments.do');
$('#mform').submit();
});

/**
* Evento que exportar a excel los elementos presentados
* de la consulta consultaPayment
*/
$('#btnExportarTodo').click(function() {
$('#mform').attr('action', 'exportarTodoConsultaPayments.do');
$('#mform').submit();
});
/**
* Evento realiza un insert o update a nivel BD 
* para un elemento ingresado o modificado
*/
$('#btnEditar').click(function() {
var checkboxCount = 0;
$('input[id="check"]:checked').each(function() {
checkboxCount++;
});

if (checkboxCount === 1) {
$('#mform').attr('action', 'editarConsultaPayments.do');
$('#mform').submit();
}else if (checkboxCount === 0) {
jAlert(mensajes["ED00068V"], mensajes["aplicacion"], 'ED00068V', mensajes["ED00068V"]);
}else {
jAlert(mensajes["ED00126V"], mensajes["aplicacion"], 'ED00126V', mensajes["ED00126V"]);
}
});

/** 
* Evento cancelar una accion lanzada previamente
*/
$('#idRegresar').click(function() {
$('#mform').attr('action', 'consultaPayments.do');
$('#mform').submit();
});
/** 
* Evento guardar una accion lanzada previamente
*/
$('#idGuardar').click(
function() {
if ($('#claveMediaEnt').val() !== "") {
//jConfirm(mensajes["CD00010V"],mensajes["aplicacion"],'CD00010V',mensajes["CD00010V"],
//function(respuesta) {
//if (respuesta) {
$('#mform').attr('action', 'modificarConsultaPayments.do');
$('#mform').submit();
//}
//});
}else {
jAlert(mensajes["ED00065V"], mensajes["aplicacion"], 'ED00065V', mensajes["ED00065V"]);
}
});


}
};


/** 
* Inicializa las eventos
*/
$(document).ready(catConsultaPayment.init);
/** 
* Evento para cargar las listas de datos
* a los combos de la vista requerida 
*/
function onChangeCombo() {
$('#accion').val('ACT');
$('#mform').attr('action', 'consultaPayments.do');
$('#mform').submit();
}

