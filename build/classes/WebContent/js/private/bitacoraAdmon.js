bitacoraAdmon = {
	diasMes:{0:31,1:28,2:31,3:30,4:31,5:30,6:31,7:31,8:30,9:31,10:30,11:31},
	regExp : new RegExp("^([z|Z|c|C]{1,1})([0-9]*)$"),
	init:function(){
	   createCalendarGuion('fechaAccionIni','cal1');
	   createCalendarGuion('fechaAccionFin','cal2');
		
	   
	   
	   $('#idExportar').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'expBitacoraAdmon.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'expTodosBitacoraAdmon.do');
			$('#idForm').submit(); 
	   });
	   
		   $('#usuario').keydown(function(e){
				var cad;
				var k;
				document.all ? k = e.keyCode : k = e.which;
				if(!((k === 67 || k === 90 || k === 99 || k === 123 || k === 8 || k===190 ) || (k>=48 && k<=57)) ){
					e.preventDefault();
					e.stopPropagation();
				}else if(k===107|| k===109 || k=== 111 || k===18){
					e.preventDefault();
					e.stopPropagation();
				}
				cad = $('#usuario').val()+String.fromCharCode(k);
				if(!(cad ==="") && !bitacoraAdmon.regExp.test(cad) && k !== 8){
					e.preventDefault();
					e.stopPropagation();
				}
	        });
	   
	   
		$('#idBuscar').click(function(){
			$('#accion').val('');
			strFechaHoy=$('#fechaHoy').val();
			dFechaHoy = Utilerias.stringToDate(strFechaHoy);				
			strFechaAccionIni=$('#fechaAccionIni').val();
			dFechaAccionIni = Utilerias.stringToDate(strFechaAccionIni);
			strFechaAccionFin=$('#fechaAccionFin').val();
			dFechaAccionFin = Utilerias.stringToDate(strFechaAccionFin);
			if(dFechaAccionIni.getTime()>dFechaAccionFin.getTime()){
				jAlert(mensajes["ED00015V"],mensajes["aplicacion"],'ED00015V','');
			}else if(dFechaAccionIni.getTime()>dFechaHoy.getTime()){
				jAlert(mensajes["ED00016V"],mensajes["aplicacion"],'ED00016V','');
			}else if(dFechaAccionFin.getTime()>dFechaHoy.getTime()){
				jAlert(mensajes["ED00017V"],mensajes["aplicacion"],'ED00017V','');
			}else{
				
				mes = dFechaAccionIni.getMonth()+3;
				anio = dFechaAccionIni.getFullYear();
				if(mes>12){
					mes = mes-12;
					anio = anio+1;
				}
				dia = dFechaAccionIni.getDate();
				diaBase = bitacoraAdmon.diasMes[mes];
				if(mes === 1){
					if ((anio % 4 === 0) && ((anio % 100 !== 0) || (anio % 400 === 0))){
						diaBase =29;
					}
				}
				if(dia>diaBase){
					dFecha = new Date(anio,mes,diaBase);
				}else{
					dFecha = new Date(anio,mes,dia);
				}
				
				if(dFechaAccionFin.getTime()>dFecha.getTime()){
					jAlert(mensajes["ED00018V"],mensajes["aplicacion"],'ED00018V','');
				}else{
					$('#idForm').attr('action', 'buscarBitacoraAdmon.do');
					$('#idForm').submit();
				}
			}	
			
		});
		
	},
	
	cancel:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===58 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
}

$(document).ready(bitacoraAdmon.init);