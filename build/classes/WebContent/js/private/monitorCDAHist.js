monitorCDAHist ={
	init:function(){		
	
		createCalendarGuion('fechaOpe','cal1');
		$('#idBuscar').click(function(){			
			if(Utilerias.isEmpty('fechaOpe')){
				jAlert(mensajes["ED00025V"],mensajes["aplicacion"],'ED00025V','');
				return false;
			}	
			var fechaOpe =$('#fechaOpe').val();
			$('#paramFechaOperacion').val(fechaOpe);
			$('#idForm').attr('action','consInfMonCDAHist.do');
			$('#idForm').submit();
		});		
		
		$('#idCDAOrdRecAplicadas').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#paramFechaOperacion').val(fechaOpe);
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#estatusCDA').val('');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		$('#idCDAPendEnviar').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#paramFechaOperacion').val(fechaOpe);
			$('#estatusCDA').val('0,3');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		$('#idCDAEnviadas').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#paramFechaOperacion').val(fechaOpe);
			$('#estatusCDA').val('1');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		$('#idCDAConfirmadas').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#paramFechaOperacion').val(fechaOpe);
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#estatusCDA').val('2');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		$('#idCDAContingencia').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#paramFechaOperacion').val(fechaOpe);
			$('#estatusCDA').val('4');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		$('#idCDARechazadas').click(function(){
			var fechaOpe =$('#fechaOpe').val();
			$('#fechaOpeInicio').val(fechaOpe);
			$('#fechaOpeFin').val(fechaOpe);
			$('#paramFechaOperacion').val(fechaOpe);
			$('#estatusCDA').val('5');
			$('#idForm').attr('action','consMovMonHistCDA.do');
			$('#idForm').submit();
		});
		
		
		
		
	}

};

$(document).ready(monitorCDAHist.init);


