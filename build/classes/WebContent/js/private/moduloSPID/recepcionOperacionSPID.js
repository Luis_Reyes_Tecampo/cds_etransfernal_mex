recepcionOperacionSPID = {
	lock:0,	
	init:function(){	   
		$('#idBuscar').click(function(){		
			var val = $("input[name='opcionParam']:checked").val();
			if(val){
				$('#opcion').val(val);
				$('#idForm').attr('action','consSPIDRecOp.do');
				$('#idForm').submit();
			}else{
				jAlert(mensajes["ED00065V"],
						mensajes["aplicacion"],
					   	   'ED00065V',
					   	   '');
			}
		});	
		
		$('#idEnvio').click(function(){
			jConfirm(mensajes["CD00167V"],mensajes["aplicacion"],'CD00167V','', function (respuesta){
				if(respuesta === true){
					var val = $('[name=opcionParam]:checked').val();
					if(val==='TVL' ||val ==='TTL' || val ==='RMD'||val ==='R'){
						
						if(recepcionOperacionSPID.lock===0){
							recepcionOperacionSPID.lock=1;
							$('#idForm').attr('action','transferirSPIDRecOp.do');
							$('#idForm').submit();
						}
					}else{
						jAlert(mensajes["ED00064V"],
								mensajes["aplicacion"],
							   	   'ED00064V',
							   	   '');
						
					}
				} 
			}
			);
		});
		
		$('#idVerDetalle').click(function(){		
			$('#idForm').attr('action','muestraDetRecepOpSPID.do');
			$('#idForm').submit();
		});
		
		$('#idRechazar').click(function(){
			var val = $('#opcion').val();
			if(val==='TVL' ||val ==='TTL'){
				if(recepcionOperacionSPID.lock===0){
					recepcionOperacionSPID.lock=1;
					$('#idForm').attr('action','rechazarSPIDRecOp.do');
					$('#idForm').submit();
				}
			}else{
				jAlert(mensajes["ED00064V"],
						mensajes["aplicacion"],
					   	   'ED00064V',
					   	   '');	
				
			}
		});
		
		$('#idApartar').click(function(){

			
			$('#idForm').attr('action','apartarSPIDRecOp.do');
			$('#idForm').submit();
			
		});
		
		$('#idDesapartar').click(function(){

			
			$('#idForm').attr('action','desapartarSPIDRecOp.do');
			$('#idForm').submit();
			
		});
		
		
		
		$('#idActMotRechazo').click(function(){		
			if(recepcionOperacionSPID.lock===0){
				recepcionOperacionSPID.lock=1;
				$('#idForm').attr('action','actMotRechazoSPIDRecOp.do');
				$('#idForm').submit();
			}
		});
		$('#chkTodos').click(function(){		
			var objCheckBoxes = $( "[type=checkbox]" );
	        if($('#chkTodos').is(':checked')){
	           for(var i=0;i<objCheckBoxes.length;i++){
		              objCheckBoxes[i].checked = true;
		            
	           }
	        } else{
	           for(var j=0;j<objCheckBoxes.length;j++){
	              objCheckBoxes[j].checked = false;
	           }
	        }
		});
	},
	recuparar:function(index){
		
		jConfirm(mensajes["CD00168V"],mensajes["aplicacion"],'CD00168V','', function (respuesta){
			if(respuesta){
				if(recepcionOperacionSPID.lock===0){
					recepcionOperacionSPID.lock=1;
					$('#selecRecuperar'+index).val('true');
					$('#idForm').attr('action','recuperarSPIDRecOp.do');
					$('#idForm').submit();
				}
			}
		}
		);
	},
	devolver:function(index){
		jConfirm(mensajes["CD00169V"],mensajes["aplicacion"],'CD00169V','', function (respuesta){
			if(respuesta){
				if(recepcionOperacionSPID.lock===0){
					recepcionOperacionSPID.lock=1;
					$('#selecDevolucion'+index).val('true');
					$('#idForm').attr('action','devolverSPIDRecOp.do');
					$('#idForm').submit();
				}
			}
		});
	}
};

$(document).ready(recepcionOperacionSPID.init);

