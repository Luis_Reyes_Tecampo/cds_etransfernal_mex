var ordenReparacion = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarConsultaSaldoBanco.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoConsultaSaldoBanco.do');
			$('#idForm').submit(); 
	   });
	   
	   
	   $('#idActualizar').click(function(){
		   $('#paginaIni').val('1');
			$('#idForm').attr('action', 'muestraConsultaSaldoBanco.do');
			$('#idForm').submit();
		});
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'muestraConsultaSaldoBanco.do');
		   $('#idForm').submit();
	   });
	   
	   $('#paginaIni').val('1');
	   
	   $('#idRegresarMain').click(function(){
		   $('#idForm').attr('action', 'muestraMonitorOpSPID.do');
		   $('#idForm').submit();
	   });
	}
}

$(document).ready(ordenReparacion.init);