var editar = function(){
   var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
   
   if (sizeCheckBoxes > 1) {
	   jAlert("Solo puede seleccionar un registro", "Seleccion Todo", "", "", "", "");
	   var objCheckBoxes = $( "[type=checkbox]" );
	   for(var i=0;i<objCheckBoxes.length;i++){
		   objCheckBoxes[i].checked = false;				            
	   }
   } else {
	   if (sizeCheckBoxes === 0) {
		   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
	   } else {				  
		   $('#idForm').attr('action', 'editarEquivalencia.do');
			$('#idForm').submit(); 
		   }
	   }	
};
		   
var verificarRegistrosPorEliminar = function(sizeCheckBoxes, objCheckBoxes) {
	if (sizeCheckBoxes === 0) {
		   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
	   } else {
		   jConfirm("Confirmar borrado de registro",mensajes["aplicacion"],'CD00167V','', function (respuesta){
				if(respuesta === true){	
					$('#paginaIni').val('1');
				    $('#idForm').attr('action', 'eliminarEquivalencia.do');
					$('#idForm').submit(); 
				} else {
					for(var i=0;i<objCheckBoxes.length;i++){
						   objCheckBoxes[i].checked = false;				            
				   }
				}
		   	});						   
	   }
};
		
var eliminar = function(){
	   var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
	   var objCheckBoxes = $( "[type=checkbox]" );
	   
	   if (sizeCheckBoxes > 1) {
		   jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
		   for(var i=0;i<objCheckBoxes.length;i++){
			   objCheckBoxes[i].checked = false;				            
		   }
	   } else {
		   verificarRegistrosPorEliminar(sizeCheckBoxes, objCheckBoxes);
	   }	
};
var seleccionarTodos = function(){		
   jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
	var objCheckBoxes = $( "[type=checkbox]" );
    if($('#chkTodos').is(':checked')){
       for(var i=0;i<objCheckBoxes.length;i++){
              objCheckBoxes[i].checked = false;
            
       }
    } else{
       for(var j=0;j<objCheckBoxes.length;j++){
          objCheckBoxes[j].checked = false;
       }
    }
};
var guardar = function(){
	if (Utilerias.isEmpty('descripcionEq')){
		jAlert(mensajes["CD00041V"], mensajes["aplicacion"],'CD00041V', "", "", "");
		return;
	}
	
   if(Utilerias.isEmpty('tipoPago') || Utilerias.isEmpty('cveOperacion') ||
			Utilerias.isEmpty('horario')|| Utilerias.isEmpty('clasificacion')) {
		jAlert(mensajes["CD00041V"], mensajes["aplicacion"],'CD00041V', "", "", "");
	}else {
		$('#idForm').attr('action', 'guardarEquivalencia.do');
		$("#idForm").submit();
		}		   
};
var editarG = function(){
		if (Utilerias.isEmpty('descripcionEq')){
			jAlert(mensajes["CD00041V"], mensajes["aplicacion"],'CD00041V', "", "", "");
			return;
		}
	
	    if(Utilerias.isEmpty('tipoPago') || Utilerias.isEmpty('cveOperacion') ||
	    		Utilerias.isEmpty('horario')|| Utilerias.isEmpty('clasificacion')) {
		   jAlert(mensajes["CD00041V"], mensajes["aplicacion"],'CD00041V', "", "", "");
		}else {
			$('#idForm').attr('action', 'guardarEdicionEquivalencia.do');
			$("#idForm").submit();
		}		   
};

var seleccionCveOperacion = function(){
	var texto = $('option:selected', this).text();
	var myRegexp = /.*-\s(.*)/;
	var match = myRegexp.exec(texto);
	$('#descripcionEq').val(match[1]);
};

var confEquivalencia = {
	init:function(){
	   $('#idAgregar').click(function(){
		   window.location = "agregarEquivalencia.do"
	   });

	   $('#idEditar').click(editar);

	   $('#idEliminar').click(eliminar);

	   $('#idActualizar').click(function(){
		   	$('#paginaIni').val('1');
			window.location = 'listarEquivalencias.do';
		});

	   $('#chkTodos').click(seleccionarTodos);

	   $('#idGuardar').click(guardar);

	   $('#idEditarG').click(editarG);

	   $('#idRegresar').click(function(){
		   window.location = "listarEquivalencias.do"
	   });
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'listarEquivalencias.do');
		   $('#idForm').submit();
	   });
	   
	   $('#cveOperacion').change(seleccionCveOperacion);
	}
}

$(document).ready(confEquivalencia.init);