var editar = function(){
		   var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
		   
		   if (sizeCheckBoxes > 1) {
			   jAlert("Solo puede seleccionar un registro", "Seleccion Todo", "", "", "", "");
			   var objCheckBoxes = $( "[type=checkbox]" );
			   for(var i=0;i<objCheckBoxes.length;i++){
				   objCheckBoxes[i].checked = false;				            
			   }
		   } else {
			   if (sizeCheckBoxes === 0) {
				   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
			   } else {				  
				   $('#idForm').attr('action', 'editarTopologia.do');
					$('#idForm').submit(); 
			   }
		   }	
	   };	
var	verificarRegistrosPorEliminar = function(sizeCheckBoxes, objCheckBoxes) {
		if (sizeCheckBoxes === 0) {
			   jAlert("Debes seleccionar un registro", "Selecciona un Registro", "", "", "", "");
		   } else {
			   jConfirm("Confirmar borrado de registro",mensajes["aplicacion"],'CD00167V','', function (respuesta){
					if(respuesta === true){	
						$('#paginaIni').val('1');
					    $('#idForm').attr('action', 'eliminarTopologia.do');
						$('#idForm').submit(); 
					} else {
						for(var i=0;i<objCheckBoxes.length;i++){
						   objCheckBoxes[i].checked = false;			            
					   }
					}
			   	});						   
		   }
	};
var	eliminar = function(){
   var sizeCheckBoxes = $('form input[type=checkbox]:checked').size();
   var objCheckBoxes = $( "[type=checkbox]" );
   
   if (sizeCheckBoxes > 1) {
	   jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
		   for(var i=0;i<objCheckBoxes.length;i++){
			   objCheckBoxes[i].checked = false;				            
		   }
	   } else {
		   verificarRegistrosPorEliminar(sizeCheckBoxes, objCheckBoxes);
	   }	
};
var	seleccionarTodos = function(){		
		   jAlert("Solo puede seleccionar un registro", "Seleccion", "", "", "", "");
			var objCheckBoxes = $( "[type=checkbox]" );
	        if($('#chkTodos').is(':checked')){
	           for(var i=0;i<objCheckBoxes.length;i++){
		              objCheckBoxes[i].checked = false;
		            
	           }
	        } else{
	           for(var j=0;j<objCheckBoxes.length;j++){
	              objCheckBoxes[j].checked = false;
	           }
	        }
		};
var	guardar = function(){
		var eval1 = function(){
			return Utilerias.isEmpty('cveMedio') || Utilerias.isEmpty('cveOperacion') || Utilerias.isEmpty('topologia');
		} 
		
		var eval2 = function() {
			return Utilerias.isEmpty('prioridad') || Utilerias.isEmpty('importeMinimo');
		}
		
		if (isNaN(Number($("#importeMinimo").val()))){
		   jAlert('El Importe Minimo debe ser un valor numerico',mensajes["aplicacion"],'CD00041V','');
		   return;
    	}
		
	   if(eval1() || eval2()){
		   jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
	   } else {
		   $('#idForm').attr('action', 'guardarTopologia.do');
		   $("#idForm").submit();
	   }
   };
var	editarG = function(){
		var eval1 = function(){
			return Utilerias.isEmpty('cveMedio') || Utilerias.isEmpty('cveOperacion') || Utilerias.isEmpty('topologia');
		} 
		
		var eval2 = function() {
			return Utilerias.isEmpty('prioridad') || Utilerias.isEmpty('importeMinimo');
		}
		
		if (isNaN(Number($("#importeMinimo").val()))){
		   jAlert('El Importe Minimo debe ser un valor numerico',mensajes["aplicacion"],'CD00041V','');
		   return;
    	}
		
		if(eval1() || eval2()){
		   	jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
	    } else {
    		$('#idForm').attr('action', 'guardarEdicionTopologia.do');
    		$("#idForm").submit();
	    }
    };

var ordenReparacion = {
	init:function(){ 
	   $('#idAgregar').click(function(){
		   window.location = "agregarTopologia.do"
	   });

	   $('#idEditar').click(editar);
	   
	   $('#idEliminar').click(eliminar);
		   
	   $('#idActualizar').click(function(){
		   	$('#paginaIni').val('1');
			window.location = 'configuracionTopologias.do';
		});
	   
	   $('#chkTodos').click(seleccionarTodos);
	   
	   $('#idGuardar').click(guardar);
	   
	   $('#idEditarG').click(editarG);
	   
	   $('#idRegresar').click(function(){		
		   window.location = "configuracionTopologias.do"
	   });
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'configuracionTopologias.do');
		   $('#idForm').submit();
	   });
	}
}

$(document).ready(ordenReparacion.init);