var enviar = function(){
	var val = $('input[type="checkbox"]:checked').val();
	
	if(val == undefined || val == null || val.length == 0){
		jAlert("Se requiere palomear un registro",
			mensajes["aplicacion"],
			'ED00068V',
   	   '');
		return false;
	}
   	jConfirm(mensajes["CD00167V"], mensajes["aplicacion"], 'CD00167V','', function (respuesta){
   		if (respuesta){
			$('#paginaIni').val('1');
		    $('#idForm').attr('action', 'enviarReparacion.do');
			$('#idForm').submit();
   		}
   	});		  		   
};
var	seleccionarTodos = function(){
		var objCheckBoxes = $( "[type=checkbox]" );
        if($('#chkTodos').is(':checked')){
           for(var i=0;i<objCheckBoxes.length;i++){
	              objCheckBoxes[i].checked = true;
	            
           }
        } else{
           for(var j=0;j<objCheckBoxes.length;j++){
              objCheckBoxes[j].checked = false;
           }
        }
	};

var ordenReparacion = {
	init:function(){ 
	   $('#idExportar').click(function(){
		    $('#idForm').attr('action', 'exportarOrdenReparacion.do');
			$('#idForm').submit(); 
	   });

	   
	   $('#idExportarTodo').click(function(){
		    $('#accion').val('ACT');
		    $('#idForm').attr('action', 'exportarTodoOrdenReparacion.do');
			$('#idForm').submit(); 
	   });
	   
	   $('#idEnviar').click(enviar);
	   
	   $('#idActualizar').click(function(){
		   	$('#paginaIni').val('1');
			$('#idForm').attr('action', 'ordenesReparacion.do');
			$('#idForm').submit();
		});
	   
	   $('#idRegresar').click(function(){
			$('#idForm').attr('action', 'muestraMonitorOpSPID.do');
			$('#idForm').submit();
		});
	   
	   $('.sortField').click(function(){
		   if ($('#sortField').val() !== $(this).attr('data-id')){
			   $('#sortType').val('');
		   }
		   
		   $('#sortField').val($(this).attr('data-id'));
		   $('#sortType').val($('#sortType').val() === 'ASC' ? 'DESC' : 'ASC');
		   $('#idForm').attr('action', 'ordenesReparacion.do');
		   $('#idForm').submit();
	   });
	   
	   $('#chkTodos').click(seleccionarTodos);
	}
}

$(document).ready(ordenReparacion.init);