monitorOperacionSPID = {
	init:function(){	   
		$('#idActualizar').click(function(){		
			$('#idForm').attr('action','muestraMonitorOpSPID.do');
			$('#idForm').submit();
		});
		
		$('#traspasoSIACSPIDSaldo').click(function(){		
			$('#idForm').attr('action','muestraAvisoTraspasos.do');
			$('#idForm').submit();
		});
		
		$('#traspasoSIACSPIDVolumen').click(function(){		
			$('#idForm').attr('action','muestraAvisoTraspasos.do');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasPorAplicSaldo').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('PA');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasPorAplicVolumen').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('PA');
			$('#idForm').submit();
		});
		
		
		$('#ordRecibidasAplicSaldo').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('TR');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasAplicVolumen').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('TR');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasRechazadasSaldo').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('RE');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasRechazadasVolumen').click(function(){		
			$('#idForm').attr('action','consultaRecepciones.do');
			$('#ref').val('RE');
			$('#idForm').submit();
		});

		$('#ordRecibidasPorDevSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OPD');
			$('#idForm').submit();
		});
		
		$('#ordRecibidasPorDevVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OPD');
			$('#idForm').submit();
		});
		
		$('#traspasoSPIDSIACSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TSS');
			$('#idForm').submit();
		});
		
		$('#traspasoSPIDSIACVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TSS');
			$('#idForm').submit();
		});
		
		$('#ordEnviadasConfSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OEC');
			$('#idForm').submit();
		});
		
		$('#ordEnviadasConfVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OEC');
			$('#idForm').submit();
		});
		$('#devolucionesEnvConfSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('DEC');
			$('#idForm').submit();
		});
		
		$('#devolucionesEnvConfVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('DEC');
			$('#idForm').submit();
		});
		
		
		$('#traspasoSPIDSIACEsperaSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TES');
			$('#idForm').submit();
		});
		$('#traspasoSPIDSIACEsperaVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TES');
			$('#idForm').submit();
		});
		
		$('#traspasoSPIDSIACEnvSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TEN');
			$('#idForm').submit();
		});
		$('#traspasoSPIDSIACEnvVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TEN');
			$('#idForm').submit();
		});
		
		$('#traspasoSPIDSIACRepararSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TPR');
			$('#idForm').submit();
		});
		$('#traspasoSPIDSIACRepararVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('TPR');
			$('#idForm').submit();
		});
		
		

		
		$('#ordEsperaSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OES');
			$('#idForm').submit();
		});
		$('#ordEsperaVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OES');
			$('#idForm').submit();
		});
		
		$('#ordEnviadasSaldo').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OEN');
			$('#idForm').submit();
		});
		$('#ordEnviadasVolumen').click(function(){		
			$('#idForm').attr('action','listadoPagos.do');
			$('#tipo').val('OEN');
			$('#idForm').submit();
		});
		
		$('#ordPorRepararSaldo').click(function(){		
			$('#idForm').attr('action','ordenesReparacion.do');
			$('#idForm').submit();
		});
		$('#ordPorRepararVolumen').click(function(){		
			$('#idForm').attr('action','ordenesReparacion.do');
			$('#idForm').submit();
		});

		$('#SaldosPorBanco').click(function(){		
			$('#idForm').attr('action','muestraConsultaSaldoBanco.do');
			$('#idForm').submit();
		});
	}
	

	
};

$(document).ready(monitorOperacionSPID.init);