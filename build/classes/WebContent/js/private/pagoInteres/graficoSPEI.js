
graficoSPEI ={
	plotSPEI:null,
	init:function(){
	$('#chartdiv').hide();
	$('#framePieContenedor').hide();
	$('#idPagadoSpan').hide();
	$('#idOperaSpan').hide();
	$('#idBuscar').click(function(){
		setTimeout(function (){
			graficoSPEI.jsShowWindowLoad("PROCESANDO");
			document.body.style.cursor = "wait";
		},100);
		
		$('#horaInicio').val($('#selHoraInicio').val());
		$('#horaFin').val($('#selHoraFin').val());
		$('#minutoInicio').val($('#selMinutoInicio').val());
		$('#minutoFin').val($('#selMinutoFin').val());
		$('#tipoPago').val($('#selTipoPago').val());
		var jsonurl = "muestraGraficoSPEI.json";
		  var form =  $('#idForm');
		  try{
			  var ret=undefined;
			  setTimeout(function (){
					ret = graficoSPEI.ajaxDataRenderer(jsonurl,form);
				},500);
			   
			  
		  }catch(err){
			  $('#framePieContenedor').hide();
		  }finally{
			  graficoSPEI.jsRemoveWindowLoad();
		  }
	});	

	$('#idRefrescar').click(function(){
		setTimeout(function (){
			graficoSPEI.jsShowWindowLoad("PROCESANDO");
			document.body.style.cursor = "wait";
		},100);
		$('#horaInicio').val($('#selHoraInicio').val());
		$('#horaFin').val($('#selHoraFin').val());
		$('#minutoInicio').val($('#selMinutoInicio').val());
		$('#minutoFin').val($('#selMinutoFin').val());
		$('#tipoPago').val($('#selTipoPago').val());
		var jsonurl = "muestraGraficoSPEI.json";
		var form =  $('#idForm');
		  try{
			  var ret=undefined;
			  setTimeout(function (){
					ret = graficoSPEI.ajaxDataRenderer(jsonurl,form);
				},500);
			   
			  
		  }catch(err){
			  $('#framePieContenedor').hide();
		  }finally{
			  graficoSPEI.jsRemoveWindowLoad();
		  }
	});
	

		},
		
		grafica: function(ret){
			if (graficoSPEI.plotSPEI) {
				graficoSPEI.plotSPEI.destroy();
		    }
			graficoSPEI.plotSPEI = $.jqplot('chartdiv', [ret.datos],{
				
				 legend: {
			   renderer: $.jqplot.EnhancedLegendRenderer,
		        show: true,
		        placement: 'outside',
		        marginTop : "60px",
		        rendererOptions: {
	                numberRows: 1
	            }, 
		        	location :'s'
		    },axes:{
		        xaxis:{
		          renderer:$.jqplot.DateAxisRenderer, 
		          tickRenderer: $.jqplot.CanvasAxisTickRenderer,
		          min: null,
		          max: null,
		          tickOptions:{formatString:'%H:%M:%S',
		    	 	angle: -90,
		    	 	fontSize: '7pt'
		    		}
		        }
		      },
		    
				series: [
				            {
				                label: ret.leyendaTipoTransfe
				            }
				        ]
				
			});
			
		},
		
		
		 // Our ajax data renderer which here retrieves a text file.
		  // it could contact any source and pull data, however.
		  // The options argument isn't used in this renderer.
		  ajaxDataRenderer : function(url,form) {
			
			
		    var ret = null;
		    $.ajax({
		      type: "POST",
		      // have to use synchronous here, else the function 
		      // will return before the data is fetched
		      async: true,
		      url: url,
		      data:form.serialize(),
		      dataType:"json",
		      success: function(data) {
		        ret = data;
		        document.body.style.cursor = 'default'
		        if(ret.tipoError ==='jAlert'){
		        	graficoSPEI.jsRemoveWindowLoad();
					jAlert(ret.msgError,
							mensajes["aplicacion"],
							ret.codError,
						   	   '');
					return;
				  }
					$('#pagado').val(ret.totales.montoTotal);
					  $('#opera').val(ret.totales.totalOp);
					  $('#chartdiv').show();
					  $('#textPagado').text(ret.leyendaMonto);
					  $('#textTotalOp').text(ret.leyendaTotal);
					  $('#textFechaPeriodo').text(ret.leyenda);
					  $('#idPagadoSpan').show();
					  $('#idOperaSpan').show();
					  graficoSPEI.grafica(ret);
					  $('#framePieContenedor').show();
					  graficoSPEI.jsRemoveWindowLoad();
		      },
		      error:function(xhr,status,text){
		    	  jAlert("Hubo un error, revise si la sesi\u00F3n existe dando clic a otra pagina",
							mensajes["aplicacion"],
							"ER00000",
						   	   '');
		      }
		    });
		    return ret;
		  },
		   jsRemoveWindowLoad:function() {
			    // eliminamos el div que bloquea pantalla
			    $("#WindowLoad").remove();
			 
			},
			 
			jsShowWindowLoad:function (mensaje) {
			    //eliminamos si existe un div ya bloqueando
				graficoSPEI.jsRemoveWindowLoad();
			 
			    //si no enviamos mensaje se pondra este por defecto
			    if (mensaje === undefined) mensaje = "Procesando la informaci\u00F3n<br>Espere por favor";
			 
			    //centrar imagen gif
			    height = 20;//El div del titulo, para que se vea mas arriba (H)
			    var ancho = 0;
			    var alto = 0;
			 
			    //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
			    if (window.innerWidth == undefined) ancho = window.screen.width;
			    else ancho = window.innerWidth;
			    if (window.innerHeight == undefined) alto = window.screen.height;
			    else alto = window.innerHeight;
			 
			    //operaci�n necesaria para centrar el div que muestra el mensaje
			    var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
			 
			   //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
			    imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:#000;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img  src='../lf/default/img/load.gif'></div>";
			 
			        //creamos el div que bloquea grande------------------------------------------
			        div = document.createElement("div");
			        div.id = "WindowLoad";
			        div.style.width = ancho + "px";
			        div.style.height = alto + "px";
			        $("body").append(div);
			 
			        //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
			        input = document.createElement("input");
			        input.id = "focusInput";
			        input.type = "text"
			 
			        //asignamos el div que bloquea
			        $("#WindowLoad").append(input);
			 
			        //asignamos el foco y ocultamos el input text
			        $("#focusInput").focus();
			        $("#focusInput").hide();
			 
			        //centramos el div del texto
			        $("#WindowLoad").html(imgCentro);
			 
			}
		 
		  
};

$(document).ready(graficoSPEI.init);