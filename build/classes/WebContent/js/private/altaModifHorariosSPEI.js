altaModifHorariosSPEI = {
		regExpHoraValida : new RegExp("^[0-2]{1}[0-9]{1}(:)[0-5]{1}[0-9]{1}$"),

	init:function(){
		$('#idAceptar').click(function(){
			$('#accion').val('');
			
			idHrInicio=$('#horaInicio').val();
			idHrCierre=$('#horaCierre').val();
			
			accion=$('#altaMod').val();
		
			if(Utilerias.isEmpty('selMedEntrega') || Utilerias.isEmpty('selTransfer') ||
					Utilerias.isEmpty('selOperacion')|| Utilerias.isEmpty('selTopo') ||
					Utilerias.isEmpty('horaInicio') || Utilerias.isEmpty('horaCierre')
					){
				jAlert(mensajes["CD00041V"],mensajes["aplicacion"],'CD00041V','');
			}else {
				altaModifHorariosSPEI.validaValores(idHrInicio,idHrCierre);
			}		
		});
		
		$('#idCancelar').click(function(){
			$('#paginaIni').val('1');
			$('#idForm').attr('action','muestraCatHorariosSPEI.do');
			$('#idForm').submit();
		});		
		
		$('#horaInicio').keypress(function(e){
			var k;
			document.all ? k = e.keyCode : k = e.which;
			altaModifHorariosSPEI.cancel(e);
        });
		
		$('#horaCierre').keypress(function(e){
			var k;
			document.all ? k = e.keyCode : k = e.which;
			altaModifHorariosSPEI.cancel(e);
        });
		
		
	},
	
	valida:function(idHrInicio,idHrCierre){
		var bol = false;
		var hrInicio = idHrInicio.split(":");
		var hrCierre = idHrCierre.split(":");
		if(hrCierre[0]>hrInicio[0] || (hrCierre[0]===hrInicio[0] && hrCierre[1]>hrInicio[1])){
			bol = true;
		}
		return bol;
	},
	
	horarioValido:function(hora){
		var bol = false;
		var res;
		bol = altaModifHorariosSPEI.regExpHoraValida.test(hora);
		if(bol && hora!==''){
			res = hora.split(":");
			if(res[0]>23){
				bol = false;
			}
			if(res[1]>59){
				bol = false;
			}
		}
		return  bol;
	},
	
	validaValores:function(idHrInicio,idHrCierre){
		
		if(!altaModifHorariosSPEI.horarioValido(idHrInicio)){
			jAlert(mensajes["ED00082V"],mensajes["aplicacion"],'ED00082V','');
			return;
		}
		if(!altaModifHorariosSPEI.horarioValido(idHrCierre)){
			jAlert(mensajes["ED00083V"],mensajes["aplicacion"],'ED00083V','');
			return;
		}
		
		if(!altaModifHorariosSPEI.valida(idHrInicio,idHrCierre)){
			jAlert(mensajes["ED00084V"],mensajes["aplicacion"],'ED00084V','');
			return;
		}
		
		
		
		//valida accion a realizar
		if(accion ==="UPDATE"){
			$('#idForm').attr('action', 'modificaHorarioSPEI.do');
		}else{
			$('#idForm').attr('action', 'altaHorarioSPEI.do');
		}
		$('#idForm').submit();
		
	},
	
	cancel:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===58 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
};

$(document).ready(altaModifHorariosSPEI.init);