administraSaldos = {
	//regExpMontoPago : new RegExp("^(((?!(|/.|[0-9]{1,2}/.))[0-9]{0,3}){0,}([/.]{1}[0-9]{0,2}){0,1})$"),
		
	lock:0,
	regExpMontoPago : new RegExp("^([0-9]{1,12}[/.]{0,1}[0-9]{0,2})$"),
	init:function(){
		$('#idActualizar').click(function(){
			$('#idForm').attr('action', 'muestraAdminSaldo.do');
			$('#idForm').submit();		
		});
	
		$('#idEnvioAlternaToPrincipal').click(function(){
			var valor = $('#importeAlterna').val();
			valor = valor.replace(/[$,]+/g,"");
			var valorfloat = parseFloat(valor);
			if(valor!=='' && valorfloat>0){
				if(administraSaldos.lock===0){
					administraSaldos.lock=1;
					$('#opcion').val("TRANS-ALTERNA-PRINC");
					$('#concepto').val($('#conceptoAlterna').val());
					$('#importe').val($('#importeAlterna').val());
//					$('#idForm').attr('action', 'envioAlternaPrincipal.do');
//					$('#idForm').submit();
					hacerSubmit('idForm', 'envioAlternaPrincipal.do');
				}
			}else{
				jAlert(mensajes["ED00078V"],mensajes["aplicacion"],'ED00078V','');
			}
		});
		
		$('#idEnvioPrincipalToAlterna').click(function(){
			var val = $('#importePrincipal').val();
			val = val.replace(/[$,]+/g,"");
			var valfloat = parseFloat(val);
			if(val!=='' && valfloat>0){
				if(administraSaldos.lock===0){
					administraSaldos.lock=1;
					$('#opcion').val("TRANS-PRINC-ALTERNA");
					$('#concepto').val($('#conceptoPrincipal').val());
					$('#importe').val($('#importePrincipal').val());
					$('#idForm').attr('action','envioPrincipalAlterna.do');
					$('#idForm').submit();
				}
			}else{
				jAlert(mensajes["ED00078V"],mensajes["aplicacion"],'ED00078V','');
			}
		});
		
		$('#importeAlterna').keypress(function(ev){
			var cad;
			var l;
			document.all ? l = ev.keyCode : l = ev.which;
			administraSaldos.cancel2(ev);
			cad = $('#importeAlterna').val()+String.fromCharCode(l);
			if(!(cad ==="") && !administraSaldos.regExpMontoPago.test(cad) && l !== 8){
				ev.preventDefault();
				ev.stopPropagation();
			}
        });
		
		$('#importePrincipal').keypress(function(e){
			var cad;
			var k;
			document.all ? k = e.keyCode : k = e.which;
			administraSaldos.cancel2(e);
			cad = $('#importePrincipal').val()+String.fromCharCode(k);
			if(!(cad ==="") && !administraSaldos.regExpMontoPago.test(cad) && k !== 8){
				e.preventDefault();
				e.stopPropagation();
			}
        });

		
		$('#idProgHorarios').click(function(){
			$('#idForm').attr('action','muestraProgTraspasos.do');
			$('#idForm').submit();
		});
		
	},
	cancel:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===190 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}	
	},
	cancel2:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(!((k === 8 || k===46 || k===44 ) || (k>=48 && k<=57)) ){
			e.preventDefault();
			e.stopPropagation();
		}else{
			administraSaldos.cancel3(e);
		}
	},
	cancel3:function(e){
		document.all ? k = e.keyCode : k = e.which;
		if(k===107|| k===109 || k=== 111 || k===18){
			e.preventDefault();
			e.stopPropagation();
		}
	}
};

$(document).ready(administraSaldos.init);