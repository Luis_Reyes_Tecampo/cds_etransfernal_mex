generarArchivoCDAHist = {
		regExp : new RegExp("^[a-z|A-Z|0-9|\d]+([/.]txt)$","i"),
		regExpValCad : RegExp("^[a-z|A-Z|0-9|\d]+([/.]|[/.][a-z|0-9]{1,}){0,1}$","i"),
		regExpCad : new RegExp("^([a-z|A-Z|0-9]{1,}([/.]|[/.]t|[/.]tx|[/.]txt){0,1})$","i"),
		copiar:false,
		init:function(){
	        createCalendarGuion('fechaOperacion','cal1');
			//Utilerias.evitaCortarCopiarPegar('nombreArchivo');
			generarArchivoCDAHist.procesaFileName('nombreArchivo');
			
			$('#idProcesar').click(function(e){
				if(Utilerias.isEmpty('fechaOperacion')){
					jAlert(mensajes["ED00025V"],mensajes["aplicacion"],'ED00025V','');
					return false;
				}	
				strFechaBanMex=$('#fechaOperacionBancoMex').val();
				dFechaBanMex = Utilerias.stringToDate(strFechaBanMex);				
				strFechaOp=$('#fechaOperacion').val();
				dFechaOp = Utilerias.stringToDate(strFechaOp);
				if(dFechaBanMex.getTime()>dFechaOp.getTime()){
					nomArchivo=$('#nombreArchivo').val();
					if(generarArchivoCDAHist.regExpValCad.test(nomArchivo)){
						if(generarArchivoCDAHist.regExp.test(nomArchivo)){
							nextPage('idForm','procesarGenerarArchCDAHist.do','');
						}else{
							jAlert(mensajes["ED00013V"],mensajes["aplicacion"],'ED00013V','');
						}
					}else{
						//Validar caracteres
						jAlert(mensajes["ED00028V"],mensajes["aplicacion"],'ED00028V','');
					}
					
				}else{
					jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');

				}
			});
			
			
		},
		procesar:function(){
			if(Utilerias.isEmpty('fechaOperacion')){
				jAlert(mensajes["ED00014V"],mensajes["aplicacion"],'ED00014V','');
			};
			Utilerias.isEmpty('nombreArchivo');
		},
		
		procesaFileName:function(id){
			$('#'+id).keypress(function(e){	
				var cad;
				var k;
				document.all ? k = e.keyCode : k = e.which;
				if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 46 || k==190 || (k >= 48 && k <= 57)||k ==0)){
					e.preventDefault();
					e.stopPropagation();
				}
				
				if(generarArchivoCDAHist.copiar==false){
					cad = $('#'+id).val()+String.fromCharCode(k);
				}else{
					cad = $('#'+id).val();
				}
				if(generarArchivoCDAHist.copiar && !(k=='v' || k=='c' || k=='V' || k=='C')){
					e.preventDefault();
					e.stopPropagation();
				}else if(!generarArchivoCDAHist.regExpCad.test(cad) && !(k == 8 || k==0 )){					
					e.preventDefault();
					e.stopPropagation();
				}
	        }
				
		    );
			
		}
	
};

$(document).ready(generarArchivoCDAHist.init);