recepcionOperacion = {
	lock:0,	
	init:function(){	   
		$('#idBuscar').click(function(){		
			var val = $("input[name='opcionParam']:checked").val();
			if(val){
				$('#opcion').val(val);
				$('#idForm').attr('action','consTranSpeiRec.do');
				$('#idForm').submit();
			}else{
				jAlert(mensajes["ED00065V"],
						mensajes["aplicacion"],
					   	   'ED00065V',
					   	   '');
			}
		});	
		
		$('#idEnvio').click(function(){
			
			jConfirm(mensajes["CD00167V"],mensajes["aplicacion"],'CD00167V','', function (respuesta){
				if(respuesta === true){
					var val = $('[name=opcionParam]:checked').val();
					if(val==='TVL' ||val ==='TTL' || val ==='RMD'||val ==='R'){
						if(recepcionOperacion.lock===0){
							recepcionOperacion.lock=1;
							$('#idForm').attr('action','transferirSpeiRec.do');
							$('#idForm').submit();
						}
					}else{
						jAlert(mensajes["ED00064V"],
								mensajes["aplicacion"],
							   	   'ED00064V',
							   	   '');
						
					}
				} 
			}
			);
		});
		
		$('#idVerDetalle').click(function(){		
			$('#idForm').attr('action','muestraDetRecepOperacion.do');
			$('#idForm').submit();
		});
		$('#idRechazar').click(function(){
			var val = $('#opcion').val();
			if(val==='TVL' ||val ==='TTL'){
				if(recepcionOperacion.lock===0){
					recepcionOperacion.lock=1;
					$('#idForm').attr('action','rechazarTransfSpeiRec.do');
					$('#idForm').submit();
				}
			}else{
				jAlert(mensajes["ED00064V"],
						mensajes["aplicacion"],
					   	   'ED00064V',
					   	   '');	
				
			}
		});
		$('#idActMotRechazo').click(function(){
			if(recepcionOperacion.lock===0){
				recepcionOperacion.lock=1;
				$('#idForm').attr('action','actMotRechazo.do');
				$('#idForm').submit();
			}
		});
		$('#chkTodos').click(function(){		
			var objCheckBoxes = $( "[type=checkbox]" );
	        if($('#chkTodos').is(':checked')){
	           for(var i=0;i<objCheckBoxes.length;i++){
		              objCheckBoxes[i].checked = true;
		            
	           }
	        } else{
	           for(var j=0;j<objCheckBoxes.length;j++){
	              objCheckBoxes[j].checked = false;
	           }
	        }
		});
	},
	recuparar:function(index){
		
		jConfirm(mensajes["CD00168V"],mensajes["aplicacion"],'CD00168V','', function (respuesta){
			if(respuesta){
				if(recepcionOperacion.lock===0){
					recepcionOperacion.lock=1;
					$('#selecRecuperar'+index).val('true');
					$('#idForm').attr('action','recuperarTransfSpeiRec.do');
					$('#idForm').submit();
				}
			}
		}
		);
	},
	devolver:function(index){
		jConfirm(mensajes["CD00169V"],mensajes["aplicacion"],'CD00169V','', function (respuesta){
			if(respuesta){
				if(recepcionOperacion.lock===0){
					recepcionOperacion.lock=1;
					$('#selecDevolucion'+index).val('true');
					$('#idForm').attr('action','devolverTransfSpeiRec.do');
					$('#idForm').submit();
				}
			}
		});
	}
};

$(document).ready(recepcionOperacion.init);

