/** Declaracion de las variables necesarias**/
var arr = [ 8, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 
    65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
    97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 ];
var cadena;
var cadenaAux;
var cadenaLength;
var cadenaLenghtMenos;
var validaEntradas = '.validaEntrada';

var detalleOperacion = {
        init : function () {
            /** Evento del boton exportar **/
            $('#idExportar').click(function() {
                $('#idForm').attr('action', 'exportarDetalleOperacion.do');
                $('#idForm').submit();
            });
            /** Evento del boton actualizar **/
            $('#idActualizar').click(function() {
                $('#idForm').attr('action', 'detalleOperacion.do');
                $('#field').val('');
                $('#fieldValue').val('');
                $('#idForm').submit();
            });
            /** Evento del boton exportar todos los registros **/
            $('#idExportarTodo').click(function() {
                $('#idForm').attr('action', 'exportarTodoDetalleOperacion.do');
                $('#idForm').submit();
            });
            /** Evento de los campos del busqueda **/
            $('.filField').keypress(function (e) {
                var charCode = (e.which) ? e.which : e.keyCode;
                cadena = $(this).val();
                cadenaLength = cadena.length;
                var cadenaLengthMenos = cadenaLength - 1;
                cadenaAux = cadena.substring(0, cadenaLengthMenos);
                if (charCode === 8 || charCode === 37 || charCode === 39) {
    				return true;
    			}
                if (charCode === 13) {
                    if ($(this).attr('data-field') !== '' && $(this).val() !== '') {
                        $('#field').val($(this).attr('data-field'));
                        $('#fieldValue').val($(this).val());
                    }
                    $('#idForm').attr('action', 'detalleOperacion.do');
                    $('#idForm').submit();
                }
            });
            /** Funcion para evitar copiar en los campos de busqueda **/
            $(validaEntradas).on('copy', function (e) {
                e.preventDefault();
            });
            /** Funcion para evitar pegar en los campos de busqueda **/
            $(validaEntradas).on('cut', function (e) {
                e.preventDefault();
            });
            /** Funcion para evitar pegar en los campos de busqueda **/
            $(validaEntradas).on('paste', function (e) {
                e.preventDefault();
            });
            /** Evento del boton regresar **/
            $('#idRegresar').click(function () {
               /**obtiene el valor de nombre de la pantalla **/
                var nomPantalla = $('#nomPantalla').val();           	
            	$('#nomPantalla').val(nomPantalla);
            	var accion = "";
            	/**si viene normal entra a buscar en la tabla normal **/
            	if(nomPantalla==='normal'){            		
            		accion ='muestraOperacionesContingentes.do';
            	}else{            		
            		var fecha = $('#fechaOperacion').val();
            		$('#fechaOperacion').val(fecha);
            		accion ='buscarOperacionContinHto.do';
            	} 
            	
            	$('#idForm').attr('action', accion); 
                $('#idForm').submit();
            });
        }
};

/** funcion alfanumerica para validar los caracteres permitidos en los filtros de busqueda **/
function validaEntrada(event) {
    var key = event.which || event.keyCode;
    var tecla = String.fromCharCode(key).toString();
    var entrada = 'áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890';
    var especiales = [8, 6];
    var teclaEspecial = false;
    for (var i in especiales) {
        if (key === especiales[i]) {
            teclaEspecial = true;
            break;
        }
    }
    if (entrada.indexOf(tecla) == -1 && !teclaEspecial) {
        return false;
    }
    return true;
}
/** funcion para valdiar la entrada numerica en los filtros de busqueda **/
function validaEntradaNumeros(event) {
    var keynum;
    if (window.event) {
        keynum = event.keyCode;
    } else {
        keynum = event.which;
    }
    if((keynum > 47 && keynum < 58) ){
    	  return true;
    }else if(keynum === 8 || keynum === 13 || keynum === 6){
    	return true;
    }
    	
    return false;
    	
}

/** Inicializacion de las funciones **/
$(document).ready(detalleOperacion.init);