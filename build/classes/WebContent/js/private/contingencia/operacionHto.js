operacionHto = {
   init:function(){
      createCalendarGuion('fchOperacion','cal1');
      document.body.style.cursor = "default";

      /**funcion para buscar los archivos de contingencia **/
      $('#idBuscar').click(function(){	
    	  operacionHto.realizarBusqueda();
      });
      
      /**funcion para buscar actualizar **/
      $('#idActualizarHto').click(function(){	
    	  operacionHto.realizarBusqueda();
      });
        
   },
   
   realizarBusqueda:function(){
	   var valF = $('#fchOperacion').val();
       $('#fechaOperacion').val(valF);
       
       if(valF.length> 0){
       	operacionHto.buscarFecha();
       }else{
       	/**si es mayor igual a la actual manda un mensaje de error**/
    	   jAlert(mensajes["ED00003V"],mensajes["aplicacion"], 'ED00003V','');
       }
       
           	
   },
   
   /**funcion para validar la fecha **/
   buscarFecha:function(){
	   /**valida si la fecha es menos a la actual **/
       var resul = operacionHto.validarFecha();
       /**si es menos a la actual continua**/
       if(resul){
          $('#formArchProc').attr('action','buscarOperacionContinHto.do');
          $('#formArchProc').submit();
       }else{
    	   /**si es mayor igual a la actual manda un mensaje de error**/
    	   jAlert(mensajes["ED00004V"],mensajes["aplicacion"], 'ED00004V','');
       }	
   },
   /**funcion para validar la fecha **/
   validarFecha:function(){
      /**obtiene la fecha actual del sistema **/
      var fechauno = new Date();
      /**obtiene el valor ingresado por el usuario **/
      var fecha_aux = document.getElementById("fchOperacion").value.split("-");
      var fechados = new Date(parseInt(fecha_aux[2]),parseInt(fecha_aux[1]-1),parseInt(fecha_aux[0]));
	
      var resul =true;
      var AnyoFecha = fechados.getFullYear();
      var MesFecha = fechados.getMonth();
      var DiaFecha = fechados.getDate();
 
      var AnyoHoy = fechauno.getFullYear();
      var MesHoy = fechauno.getMonth();
      var DiaHoy = fechauno.getDate();

      /**empieza la validacion**/ 
      if (AnyoFecha > AnyoHoy){
         resul = false;
      }else{
         if (AnyoFecha === AnyoHoy && MesFecha === MesHoy && DiaFecha > DiaHoy){
            resul = false;
         } else if (AnyoFecha === AnyoHoy && MesFecha === MesHoy && DiaFecha === DiaHoy){
            resul = false;
         }
   
      }
		
      return resul;
   }
  
	
};
	

/**incializa el jsp**/
$(document).ready(operacionHto.init);



