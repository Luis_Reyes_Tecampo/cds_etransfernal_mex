consultaOperacion={
        init:function(){
            consultaOperacion.changeEstatusOperacion();
            createCalendarGuion('fechaCaptura','cal1');
            document.body.style.cursor = "default";
            $('#idBuscar').click(function(){
                document.body.style.cursor = "wait";
                $('#idUsuarioCaptura').val($('#inputUsuarioCaptura').val());
                $('#idUsuarioAutoriza').val($('#inputUsuarioAutoriza').val());
                $('#idTemplate').val($('#inputTemplate').val());
                $('#idFechaCaptura').val($('#inputFechaCaptura').val());
                consultaOperacion.irAPagina('consultaOperacionesBusqueda.do');
            });

            $('#idRegresar').click(function(){
                consultaOperacion.irAPagina('capturasManualesInit.do');
            });

            $('#idApartar').click(function(){
                consultaOperacion.irAPagina('apartarOperaciones.do');
            });

            $('#idDesapartar').click(function(){
                consultaOperacion.irAPagina('desapartarOperaciones.do');
            });

            $('#idExportar').click(function(){
                consultaOperacion.irAPagina('exportarOperaciones.do');
            });

            $('#idExportarTodo').click(function(){
                consultaOperacion.irAPagina('exportarTodoOperaciones.do');
            });
            consultaOperacion.init2();
        },
        init2:function(){
            $('#idEliminar').click(function(){
                consultaOperacion.irAPagina('eliminarOperaciones.do');
            });
        },
        changeEstatusOperacion:function() {
            var inputEstatusOperacion = document.getElementById("inputEstatusOperacion");
            var selectedIndex=inputEstatusOperacion.selectedIndex;
            if (selectedIndex>=0){
                $('#estatusOperacion').val(inputEstatusOperacion.options[selectedIndex].value);
            }
        },
        despliegaDetalle:function(folio){
            $('#folio').val(folio);
            consultaOperacion.irAPagina('consultaOperacionesDetalle.do');
        },
        irAPagina:function(pagina){
            $('#idForm').attr('action',pagina);
            $('#idForm').submit();
        }
};

$(document).ready(consultaOperacion.init);