consultaOperacionDetalle={
        init:function(){
            $('#idModificarDetalle').click(function(){
                $('#idForm').attr('action','consultaOperacionesDetalleModificar.do');
                $('#idForm').submit();
            });

            $('#idGuardarDetalle').click(function(){
                capturaCentralSPID.setValues();
                $('#idForm').attr('action','consultaOperacionesDetalleGuardar.do');
                $('#idForm').submit();
            });

            $('#idRegresarDetalle').click(function(){
                $('#idForm').attr('action','consultaOperacionesBusqueda.do');
                $('#idForm').submit();
            });
        }
};



$(document).ready(consultaOperacionDetalle.init);